<?php

use \Mockery as m;

require_once dirname(__FILE__).'/_init.php';

require_once dirname(dirname(__FILE__)).'/vendor/autoload.php';

require_once CONTENT_DIR.'engine/engine_order.php';
require_once CONTENT_DIR.'engine/engine_user.php';
require_once CONTENT_DIR.'engine/engine_functions.php';

class OrderTest extends PHPUnit_Framework_TestCase
{
	/** @var  DB $db */
	protected $db;
	protected $settings;
	/** @var  USER $user */
	protected $user;
	/** @var  ORDER $order */
	protected $order;

	// Mocks
	protected $productsRepository;
	protected $orderRepository;
	protected $discountRepository;
	protected $promoDiscountRepository;
	protected $taxProvider;
	protected $taxRatesProvider;
	protected $shippingRepository;
	protected $currencies;

	// Calculators
	protected $discountCalculator;
	protected $handlingCalculator;
	protected $handlingTaxCalculator;
	protected $promoDiscountCalculator;
	protected $shippingCalculator;
	protected $shippingTaxCalculator;
	protected $subtotalCalculator;
	protected $taxCalculator;
	protected $totalCalculator;

	public $lineItemId;

	public function setUp()
	{
		$this->lineItemId = 1;

		$this->db = null;
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DiscountsActive' => 'YES',
			'DiscountsPromo' => 'YES',
			'DisplayPricesWithTax' => 'NO',
			'WholesaleDiscountType' => 'NO',
			'WholesaleMaxLevels' => 1,
			'WholesaleDiscount1' => '0.00',
			'WholesaleDiscount2' => '0.00',
			'WholesaleDiscount3' => '0.00',

			'TaxDefaultCountry' => 1,
			'TaxDefaultState' => 3,
		);

		$this->setupDependencies($this->settings);
	}

	public function tearDown()
	{
		\Mockery::close();
	}

	protected function setupDependencies(&$settings)
	{
		$this->user = new USER($this->db, $settings, 0);
		$this->order = new ORDER($this->db, $settings, $this->user, 1);

		$this->productsRepository = m::mock('DataAccess_ProductsRepositoryInterface');
		$this->orderRepository = m::mock('DataAccess_OrderRepositoryInterface');
		$this->discountRepository = m::mock('DataAccess_DiscountRepositoryInterface');
		$this->promoDiscountRepository = m::mock('DataAccess_PromoCodeRepositoryInterface');

		$this->order->setProductsRepository($this->productsRepository);
		$this->order->setOrderRepository($this->orderRepository);
		$this->order->setPromoCodeRepository($this->promoDiscountRepository);

		$this->taxRatesProvider = m::mock('TaxRatesInterface');
		$this->taxProvider = new Tax_CustomProvider($this->orderRepository, $settings, $this->taxRatesProvider);
		Tax_ProviderFactory::setTaxProvider($this->taxProvider);

		$this->shippingRepository = m::mock('DataAccess_ShippingRepositoryInterface');
		$this->dobaOrderApi = m::mock('Doba_OrderApiInterface');
		$this->currencies = m::mock('CurrenciesInterface');

		$this->discountCalculator = new Calculator_Discount($settings, $this->orderRepository, $this->discountRepository);
		Calculator_Discount::setInstance($this->discountCalculator);

		$this->handlingCalculator = new Calculator_Handling($settings);
		Calculator_Handling::setInstance($this->handlingCalculator);

		$this->handlingTaxCalculator = new Calculator_HandlingTax($settings, $this->taxProvider);
		Calculator_HandlingTax::setInstance($this->handlingTaxCalculator);

		$this->promoDiscountCalculator = new Calculator_PromoDiscount($this->orderRepository, $this->promoDiscountRepository, $settings);
		Calculator_PromoDiscount::setInstance($this->promoDiscountCalculator);

		$this->shippingCalculator = new Calculator_Shipping($settings, $this->shippingRepository, $this->orderRepository, $this->currencies);
		Calculator_Shipping::setInstance($this->shippingCalculator);

		$this->shippingTaxCalculator = new Calculator_ShippingTax($settings, $this->taxProvider);
		Calculator_ShippingTax::setInstance($this->shippingTaxCalculator);

		$this->subtotalCalculator = new Calculator_Subtotal($this->orderRepository);
		Calculator_Subtotal::setInstance($this->subtotalCalculator);

		$this->taxCalculator = new Calculator_Tax($this->taxProvider);
		Calculator_Tax::setInstance($this->taxCalculator);

		$this->totalCalculator = new Calculator_Total($settings);
		Calculator_Total::setInstance($this->totalCalculator);
	}

	function test_setId()
	{
		$this->order->setId(32);

		$this->assertEquals(32, $this->order->getId());
	}

	function test_getUser()
	{
		$this->assertNotNull($this->order->getUser());
	}

	function test_getUserLevel()
	{
		$this->user->setLevel(3);

		$this->assertEquals(3, $this->order->getUserLevel());
	}

	function test_getOrderNumber()
	{
		$this->order->setOrderNumber(33);

		$this->assertEquals(33, $this->order->getOrderNumber());
	}

	function test_setItemsCount()
	{
		$this->order->setItemsCount(7);

		$this->assertEquals(7, $this->order->getItemsCount());
	}

	function test_setTaxCountryId()
	{
		$this->order->setTaxCountryId(62);

		$this->assertEquals(62, $this->order->getTaxCountryId());
	}

	function test_setTaxStateId()
	{
		$this->order->setTaxStateId(63);

		$this->assertEquals(63, $this->order->getTaxStateId());
	}

	function test_setSubtotalAmount()
	{
		$this->order->setSubtotalAmount(17.97);

		$this->assertEquals(17.97, $this->order->getSubtotalAmount());
		//$this->assertEquals(17.97, $this->order->cartSubtotalAmount);
	}

	function test_setSubtotalAmountWithTax()
	{
		$this->order->setSubtotalAmountWithTax(17.98);
		$this->assertEquals(17.98, $this->order->getSubtotalAmountWithTax());
	}

	function test_setDiscountAmount()
	{
		$this->order->setDiscountAmount(5.97);
		$this->assertEquals(5.97, $this->order->getDiscountAmount());
	}

	function test_setDiscountValue()
	{
		$this->order->setDiscountValue(5.98);
		$this->assertEquals(5.98, $this->order->getDiscountValue());
	}

	function test_setDiscountType()
	{
		$this->order->setDiscountType('Percent');
		$this->assertEquals('Percent', $this->order->getDiscountType());
	}

	function test_getDiscountPercentage_When_Type_percent_Returns_DiscountValue()
	{
		$this->order->setDiscountAmount(4);
		$this->order->setDiscountValue(3);
		$this->order->setDiscountType('percent');
		$this->assertEquals(3, $this->order->getDiscountPercentage());
	}

	function test_getDiscountPercentage_When_Type_amount_Returns_Computed_Percentage()
	{
		$this->order->setDiscountAmount(4);
		$this->order->setDiscountValue(5);
		$this->order->setSubtotalAmount(10);
		$this->order->setDiscountType('amount');

		$this->assertEquals(50.0, $this->order->getDiscountPercentage());
	}

	function test_getDiscountPercentage_When_Type_percent_And_DiscountValue_0_Returns_0()
	{
		$this->order->setDiscountAmount(0);
		$this->order->setDiscountType('percent');

		$this->assertEquals(0, $this->order->getDiscountPercentage());
	}

	function test_setPromoDiscountType()
	{
		$this->order->setPromoDiscountType('percent');
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
	}

	function test_getPromoDiscountPercentage_When_Type_percent_Returns_DiscountValue()
	{
		$this->order->setPromoDiscountAmount(4);
		$this->order->setPromoDiscountValue(3);
		$this->order->setPromoDiscountType('percent');
		$this->assertEquals(3, $this->order->getPromoDiscountPercentage());
	}

	function test_getPromoDiscountPercentage_When_Type_amount_Returns_Computed_Percentage()
	{
		$this->order->setPromoDiscountAmount(4);
		$this->order->setPromoDiscountValue(5);
		$this->order->setSubtotalAmount(10);
		$this->order->setPromoDiscountType('amount');

		$this->assertEquals(50.0, $this->order->getPromoDiscountPercentage());
	}

	function test_getPromoDiscountPercentage_When_Type_percent_And_DiscountValue_0_Returns_0()
	{
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountType('percent');

		$this->assertEquals(0, $this->order->getPromoDiscountPercentage());
	}

	function test_setPromoDiscountValue()
	{
		$this->order->setPromoDiscountValue(10.1);
		$this->assertEquals(10.1, $this->order->getPromoDiscountValue());
	}

	function test_setPromoCampaignId()
	{
		$this->order->setPromoCampaignId(4);
		$this->assertEquals(4, $this->order->getPromoCampaignId());
	}

	function test_setPromoType()
	{
		$this->order->setPromoType('Shipping');
		$this->assertEquals('Shipping', $this->order->getPromoType());
	}

	function test_setHandlingSeparated()
	{
		$this->order->setHandlingSeparated(1);
		$this->assertEquals(1, $this->order->getHandlingSeparated());
	}

	function test_can_addItem()
	{
		$productValues = $this->getProductValues(1, 14.95, 'test_prod');
		$product = new Model_Product($productValues);

		$this->productsRepository->shouldReceive('getProductByProductId')->times(1)->andReturn($product);

		//TODO: Remove check, looks like this was converted to a collection lookup
		//$this->orderRepository->shouldReceive('findExistingLineItemForProduct')->times(1)->andReturn(null);

		$testCase = $this;
		$this->orderRepository->shouldReceive('persistLineItem')->times(1)->andReturnUsing(function($order, $lineItem, $updateAttributes) use ($testCase){
			$lineItem->setId($testCase->lineItemId++);
		});

		$this->orderRepository->shouldReceive('getOrderItemsProducts')->times(1)->andReturn(array(1 => $product));

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->times(2);
		$this->orderRepository->shouldReceive('resetLineItemShippingPrices')->times(1);
		$this->discountRepository->shouldReceive('getActiveDiscount')->times(1);
		$this->orderRepository->shouldReceive('persistOrderData')->times(1);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(2);

		$ret = $this->order->addItem('test_prod', 1, array());

		$this->assertTrue($ret);

		$this->assertEquals(14.95, $this->order->getSubtotalAmount());
		$this->assertEquals(1, $this->order->getItemsCount());
	}

	function setupGetOrderItemsProductsExpectations(array $products)
	{
		$this->orderRepository->expects($this->atLeastOnce())
			->method('getOrderItemsProducts')
			->will($this->returnValue($products));
	}

	function test_addItem_when_product_not_found_returns_false()
	{
		$product = false;

		$this->productsRepository->shouldReceive('getProductByProductId')->times(1)->andReturn($product);

		$ret = $this->order->addItem('test_prod', 1, array());

		$this->assertFalse($ret);
		$this->assertEquals(0, $this->order->getSubtotalAmount());
		$this->assertEquals(0, $this->order->getItemsCount());
	}

	function test_addItem_when_existing_line_item_adds_quantity_to_existing_line()
	{
		$productValues = $this->getProductValues(1, 14.95, 'test_prod');

		$product = new Model_Product($productValues);

		$this->productsRepository->shouldReceive('getProductByProductId')->times(1)->andReturn($product);

		$this->order->items[] = $this->getLineItemValues(1, 14.95, 2, 1, 0, 0);
		$this->order->lineItems[1] = new Model_LineItem($this->order->items[0], 14.95, $this->settings);
		//TODO: Remove check, looks like this was converted to a collection lookup
		//$this->orderRepository->shouldReceive('findExistingLineItemForProduct')->times(1)->andReturn($this->order->lineItems[1]);

		$testCase = $this;
		$this->orderRepository->shouldReceive('persistLineItem')->times(1)->andReturnUsing(function($order, $lineItem, $updateAttributes) use ($testCase){
			//$lineItem->setId($testCase->lineItemId++);
		});

		$this->orderRepository->shouldReceive('getOrderItemsProducts')->times(1)->andReturn(array(1 => $product));

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->times(2);
		$this->orderRepository->shouldReceive('resetLineItemShippingPrices')->times(1);
		$this->discountRepository->shouldReceive('getActiveDiscount')->times(1);
		$this->orderRepository->shouldReceive('persistOrderData')->times(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(2);

		$ret = $this->order->addItem('test_prod', 1, array());
		$this->assertTrue($ret);
		$this->assertEquals(3, $this->order->lineItems[1]->getAdminQuantity());

		$this->assertEquals(44.85, $this->order->getSubtotalAmount());
		$this->assertEquals(3, $this->order->getItemsCount());
	}

	function test_addItem_multiple_lines()
	{
		$product1Values = $this->getProductValues(1, 14.95, 'test_prod1');
		$product2Values = $this->getProductValues(2, 14.95, 'test_prod2');

		$product1 = new Model_Product($product1Values);
		$product2 = new Model_Product($product2Values);

		$this->productsRepository->shouldReceive('getProductByProductId')->times(2)->andReturnValues(array($product1, $product2));

		$this->orderRepository->shouldReceive('getOrderItemsProducts')->times(2)->andReturnValues(array(1 => $product1Values), array(1 => $product1Values, 2 => $product2Values));

		//TODO: Remove check, looks like this was converted to a collection lookup
		//$this->orderRepository->shouldReceive('findExistingLineItemForProduct')->times(2)->andReturnValues(array(null, null));

		$testCase = $this;
		$this->orderRepository->shouldReceive('persistLineItem')->times(2)->andReturnUsing(function($order, $lineItem, $updateAttributes) use ($testCase){
			$lineItem->setId($testCase->lineItemId++);
		});

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(6);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->times(4);
		$this->orderRepository->shouldReceive('resetLineItemShippingPrices')->times(2);
		$this->discountRepository->shouldReceive('getActiveDiscount')->times(2);
		$this->orderRepository->shouldReceive('persistOrderData')->times(2);

		$ret = $this->order->addItem('test_prod1', 2, array());
		$this->assertTrue($ret);

		$this->assertEquals(29.90, $this->order->getSubtotalAmount());
		$this->assertEquals(2, $this->order->getItemsCount());

		$ret = $this->order->addItem('test_prod2', 1, array());

		$this->assertTrue($ret);

		$this->assertEquals(44.85, $this->order->getSubtotalAmount());
		$this->assertEquals(3, $this->order->getItemsCount());
	}

//	function test_addItem_with_global_discount_33_percent()
//	{
//		$this->settings['DiscountsActive'] = 'YES';
//
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$this->setupGetOrderItemsProductsExpectations(array(1 => new Model_Product($product1)));
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$this->discountRepository->expects($this->once())
//			->method('getActiveDiscount')
//			->with($this->equalTo(29.90))
//			->will($this->returnValue(array(
//				'type' => 'percent',
//				'discount' => 33,
//			)));
//
//		$ret = $this->order->addItem('test_prod1', 2, array());
//		$this->assertTrue($ret);
//
//		$this->assertEquals(29.90, $this->order->getSubtotalAmount());
//		$this->assertEquals(9.87, $this->order->getDiscountAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//	}
//
//	function test_addItem_multiple_lines_with_global_discount_33_percent()
//	{
//		$this->setupGetActiveDiscount(29.90, 0, array('type' => 'percent', 'discount' => 33,));
//		$this->setupGetActiveDiscount(44.89, 1, array('type' => 'percent', 'discount' => 33,));
//
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//		$product2 = $this->getProductValues(2, 14.99, 'test_prod2');
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//		$this->setupGetProductByProductIdExpectations('test_prod2', $product2, 1);
//
//		$this->setupGetOrderItemsProductsExpectations(array(1 => new Model_Product($product1), 2 => new Model_Product($product2)));
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod2', '', 7);
//
//		$ret = $this->order->addItem('test_prod1', 2, array());
//		$this->assertTrue($ret);
//
//		$this->assertEquals(29.90, $this->order->getSubtotalAmount());
//		$this->assertEquals(9.87, $this->order->getDiscountAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//
//		$ret = $this->order->addItem('test_prod2', 1, array());
//
//		$this->assertTrue($ret);
//
//		// Total discount will be lineDiscountAmount1 + lineDiscountAmount2
//		// Rounding is performed per line
//		// 9.87 + 4.95 = 14.82
//		$this->assertEquals(44.89, $this->order->getSubtotalAmount());
//		$this->assertEquals(14.82, $this->order->getDiscountAmount());
//		$this->assertEquals(3, $this->order->getItemsCount());
//	}
//
//	function test_addItem_with_wholesale_discount()
//	{
//		$this->settings['WholesaleDiscountType'] = 'GLOBAL';
//		$this->settings['WholesaleMaxLevels'] = 3;
//		$this->settings['WholesaleDiscount1'] = 5;
//
//		$this->setupDependencies($this->settings);
//
//		$this->user->level = 1;
//		$this->order->setUserLevel(1);
//
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$ret = $this->order->addItem('test_prod1', 2, array());
//		$this->assertTrue($ret);
//
//		$this->assertEquals(28.40, $this->order->getSubtotalAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//	}
//
//	function test_addItem_with_global_wholesale_discount_VAT_Tax()
//	{
//		$this->settings['WholesaleDiscountType'] = 'GLOBAL';
//		$this->settings['WholesaleMaxLevels'] = 3;
//		$this->settings['WholesaleDiscount1'] = 5;
//		$this->settings['DisplayPricesWithTax'] = 'YES';
//
//		$this->setupDependencies($this->settings);
//
//		$this->user->level = 1;
//		$this->order->setUserLevel(1);
//
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//		$product1['tax_rate'] = 8.375;
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$ret = $this->order->addItem('test_prod1', 2, array());
//		$this->assertTrue($ret);
//
//		$this->order->calculateTax();
//
//		$this->assertEquals(30.78, $this->order->getSubtotalAmount());
//		$this->assertEquals(30.78, $this->order->getSubtotalAmountWithTax());
//		$this->assertEquals(2.38, $this->order->getTaxAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//	}
//
//	function test_addItem_with_product_level_wholesale_discount_VAT_Tax()
//	{
//		$this->settings['WholesaleDiscountType'] = 'PRODUCT';
//		$this->settings['WholesaleMaxLevels'] = 3;
//		$this->settings['DisplayPricesWithTax'] = 'YES';
//
//		$this->setupDependencies($this->settings);
//
//		$this->user->level = 1;
//		$this->order->setUserLevel(1);
//
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//		$product1['tax_rate'] = 8.375;
//		$product1['price_level_1'] = 13.97;
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$ret = $this->order->addItem('test_prod1', 2, array());
//		$this->assertTrue($ret);
//
//		$this->order->calculateTax();
//
//		$this->assertEquals(30.28, $this->order->getSubtotalAmount());
//		$this->assertEquals(30.28, $this->order->getSubtotalAmountWithTax());
//		$this->assertEquals(2.34, $this->order->getTaxAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//	}
//
//	function test_addItem_with_global_promo_discount_33_percent()
//	{
//		$this->settings['DiscountsActive'] = 'YES';
//
//		$this->setupGetPromoCodeByPromoCampaignId(1, 29.90, 0, array(
//			'promo_type' => 'Global',
//			'min_amount' => 0,
//			'pid' => 1,
//			'discount_type' => 'percent',
//			'discount' => 33,
//		));
//
//		$this->order->setPromoCampaignId(1);
//
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$ret = $this->order->addItem('test_prod1', 2, array());
//		$this->assertTrue($ret);
//
//		$this->assertEquals(29.90, $this->order->getSubtotalAmount());
//		$this->assertEquals(9.87, $this->order->getPromoDiscountAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//	}
//
//	function test_addItem_with_vat_bug_172()
//	{
//		$this->settings['DisplayPricesWithTax'] = 'YES';
//
//		$this->setupDependencies($this->settings);
//
//		$product1 = $this->getProductValues(1, 9.97, 'test_prod1');
//		$product1['tax_rate'] = 8.375;
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$ret = $this->order->addItem('test_prod1', 3, array());
//		$this->assertTrue($ret);
//
//		$this->order->calculateTax();
//
//		$this->assertEquals(32.40, $this->order->getSubtotalAmount());
//		$this->assertEquals(32.40, $this->order->getSubtotalAmountWithTax());
//		$this->assertEquals(2.49, $this->order->getTaxAmount());
//		$this->assertEquals(3, $this->order->getItemsCount());
//	}
//
//	function test_addItem_multiple_lines_with_vat()
//	{
//		$this->settings['DisplayPricesWithTax'] = 'YES';
//
//		$this->setupDependencies($this->settings);
//
//		$product1 = $this->getProductValues(1, 9.97, 'test_prod1');
//		$product1['tax_rate'] = 8.375;
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$product2 = $this->getProductValues(1, 9.97, 'test_prod2');
//		$product2['tax_rate'] = 8.375;
//
//		$this->setupGetProductByProductIdExpectations('test_prod2', $product2, 1);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//		$this->setupAddLineItemNotFoundExpectations('test_prod2', '', 6);
//
//		$ret = $this->order->addItem('test_prod1', 3, array());
//		$this->assertTrue($ret);
//
//		$ret = $this->order->addItem('test_prod2', 7, array());
//		$this->assertTrue($ret);
//
//		$this->order->calculateTax();
//
//		$this->assertEquals(108, $this->order->getSubtotalAmount());
//		$this->assertEquals(108, $this->order->getSubtotalAmountWithTax());
//		$this->assertEquals(8.3, $this->order->getTaxAmount());
//		$this->assertEquals(10, $this->order->getItemsCount());
//	}
//
//	function test_addItem_multiple_lines_with_global_promo_discount_33_percent()
//	{
//		$this->setupGetPromoCodeByPromoCampaignId(1, 29.90, 0, array(
//			'promo_type' => 'Global',
//			'min_amount' => 0,
//			'pid' => 1,
//			'discount_type' => 'percent',
//			'discount' => 33,
//		));
//
//		$this->setupGetPromoCodeByPromoCampaignId(1, 44.89, 1, array(
//			'promo_type' => 'Global',
//			'min_amount' => 0,
//			'pid' => 1,
//			'discount_type' => 'percent',
//			'discount' => 33,
//		));
//
//		$this->order->setPromoCampaignId(1);
//
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//		$product2 = $this->getProductValues(2, 14.99, 'test_prod2');
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//		$this->setupGetProductByProductIdExpectations('test_prod2', $product2, 1);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', '', 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod2', '', 7);
//
//		$ret = $this->order->addItem('test_prod1', 2, array());
//		$this->assertTrue($ret);
//
//		$this->assertEquals(29.90, $this->order->getSubtotalAmount());
//		$this->assertEquals(9.87, $this->order->getPromoDiscountAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//
//		$ret = $this->order->addItem('test_prod2', 1, array());
//
//		$this->assertTrue($ret);
//
//		// Total discount will be lineDiscountAmount1 + lineDiscountAmount2
//		// Rounding is performed per line
//		// 9.87 + 4.95 = 14.82
//		$this->assertEquals(44.89, $this->order->getSubtotalAmount());
//		$this->assertEquals(14.82, $this->order->getPromoDiscountAmount());
//		$this->assertEquals(3, $this->order->getItemsCount());
//	}
//
//	function test_addItem_with_attribute_modifier()
//	{
//		$product1 = $this->getProductValues(1, 14.95, 'test_prod1');
//		$product1['attributes_count'] = 1;
//
//		$attribute = array(
//			array(
//				'paid' => 1,
//				'attribute_type' => 'select',
//				'pid' => 1,
//				'is_modifier' => 'Yes',
//				'is_active' => 'Yes',
//				'priority' => 5,
//				'track_inventory' => 1,
//				'name' => 'Size',
//				'caption' => 'Size',
//				'text_length' => 0,
//				'options' => "Small
//Medium (5%)
//Large (10%)",
//		));
//		$this->productsRepository->expects($this->at(1))
//			->method('getProductsAttributes')
//			->with($this->equalTo(1))
//			->will($this->returnValue($attribute));
//		$this->productsRepository->expects($this->at(2))
//			->method('getProductsAttributes')
//			->with($this->equalTo(1))
//			->will($this->returnValue($attribute));
//
//		$this->setupGetProductByProductIdExpectations('test_prod1', $product1, 0);
//
//		$this->setupAddLineItemNotFoundExpectations('test_prod1', 'Size : Large (+10%)', 0);
//
//		$products = array(
//			1 => new Model_Product($product1),
//		);
//		$this->orderRepository->expects($this->atLeastOnce())
//			->method('getOrderItemsProducts')
//			->with($this->equalTo(1), $this->anything())
//			->will($this->returnValue($products));
//
//		$ret = $this->order->addItem('test_prod1', 2, array(1 => 'Large'));
//		$this->assertTrue($ret);
//
//		$this->assertEquals(32.90, $this->order->getSubtotalAmount());
//		$this->assertEquals(2, $this->order->getItemsCount());
//
//		$this->order->lineItems[1]->setAttributes(array());
//
//		$this->order->updateItems(array(1 => 1));
//
//		$this->assertEquals(32.90, $this->order->getSubtotalAmount());
//		$this->assertEquals(16.45, $this->order->lineItems[1]->getFinalPrice());
//	}

	protected function setupGetPromoCodeByPromoCampaignId($promoCampaignId, $subtotal, $callIndex, $promoDiscountArray)
	{
		$this->promoDiscountRepository->expects($this->at($callIndex))
			->method('getPromoCodeByPromoCampaignId')
			->with($this->equalTo($promoCampaignId), $this->equalTo($subtotal))
			->will($this->returnValue($promoDiscountArray));
	}

	protected function setupGetActiveDiscount($subtotal, $callIndex, $discountArray)
	{
		$this->discountRepository->expects($this->at($callIndex))
			->method('getActiveDiscount')
			->with($this->equalTo($subtotal))
			->will($this->returnValue($discountArray));
	}

	protected function setupGetProductByProductIdExpectations($productId, &$product, $productsRepositoryCallIndex = 0)
	{
		$productResult = false;
		if ($product)
		{
			$productResult = new Model_Product($product);
		}

		$this->productsRepository->expects($this->at($productsRepositoryCallIndex))
			->method('getProductByProductId')
			->with($this->equalTo($productId))
			->will($this->returnValue($productResult));
	}

	protected function setupAddLineItemExpectations(&$lineItem, $productId, $attributes = '', $orderRepositoryCallIndex = 0)
	{
		//TODO: Remove check, looks like this was converted to a collection lookup
//		$this->orderRepository->expects($this->at($orderRepositoryCallIndex))
//			->method('findExistingLineItemForProduct')
//			->with($this->isInstanceOf('ORDER'), $this->equalTo($productId), $this->equalTo($attributes))
//			->will($this->returnValue($lineItem));

		$testCase = $this;
		$this->orderRepository->expects($this->at($orderRepositoryCallIndex + 1))
			->method('persistLineItem')
			->with($this->isInstanceOf('ORDER'), $this->isInstanceOf('Model_LineItem'), $this->equalTo(false))
			->will($this->returnCallback(function($order, $lineItem, $updateAttributes) use ($testCase) {
				$lineItem->setId($testCase->lineItemId++);
			}));
	}

	protected function setupAddLineItemNotFoundExpectations($productId, $attributes = '', $orderRepositoryCallIndex = 0)
	{
		//TODO: Remove check, looks like this was converted to a collection lookup
//		$this->orderRepository->expects($this->at($orderRepositoryCallIndex))
//			->method('findExistingLineItemForProduct')
//			->with($this->isInstanceOf('ORDER'), $this->equalTo($productId), $this->equalTo($attributes))
//			->will($this->returnValue(null));

		$testCase = $this;
		$this->orderRepository->expects($this->at($orderRepositoryCallIndex + 1))
			->method('persistLineItem')
			->with($this->isInstanceOf('ORDER'), $this->isInstanceOf('Model_LineItem'), $this->equalTo(true))
			->will($this->returnCallback(function($order, $lineItem, $updateAttributes) use ($testCase) {
				$lineItem->setId($testCase->lineItemId++);
			}));
	}

	protected function getProductValues($pid, $price, $productId, $additionalParams = array())
	{
		$ret = array(
			'pid' => $pid,
			'weight' => 0,
			'attributes_count' => 0,
			'cost' => 0,
			'price' => $price,
			'is_taxable' => 'Yes',
			'is_doba' => 'No',
			'free_shipping' => 'No',
			'product_type' => Model_Product::TANGIBLE,
			'digital_product_file' => '',
			'products_location_id' => 0,
			'product_id' => $productId,
			'product_sku' => 'sku',
			'product_gtin' => 'gtin',
			'product_mpn' => 'mpn',
			'product_upc' => 'upc',
			'title' => 'Test Product',
			'min_order' => 0,
			'max_order' => 0,
			'inventory_control' => 'No',
			'tax_rate' => -1,
			'tax_description' => '',
			'tax_class_id' => 0,
			'cid' => 1,
			'price_withtax' => $price,
			'stock' => 0,
		);

		return array_merge($ret, $additionalParams);
	}

	protected function getLineItemValues($pid, $price, $quantity, $ocid = 1, $discountAmount = 0, $promoDiscountAmount = 0, $additionalParams = array())
	{
		$ret = array(
			'pid' => $pid,
			'weight' => 0,
			'attributes_count' => 0,
			'cost' => 0,
			'price' => $price,
			'is_taxable' => 'Yes',
			'is_doba' => 'No',
			'free_shipping' => 'No',
			'product_type' => Model_Product::TANGIBLE,
			'digital_product_file' => '',
			'products_location_id' => 0,
			'product_id' => 'test_prod',
			'product_sku' => 'sku',
			'product_gtin' => 'gtin',
			'product_mpn' => 'mpn',
			'product_upc' => 'upc',
			'title' => 'Test Product',
			'min_order' => 0,
			'max_order' => 0,
			'inventory_control' => 'No',
			'tax_rate' => -1,
			'tax_description' => '',
			'tax_class_id' => 0,
			'product_sub_id' => '',
			'ocid' => $ocid,
			'product_price' => $price,
			'admin_price' => $price,
			'options' => '',
			'options_clean' => '',
			'admin_quantity' => $quantity,
			'quantity' => $quantity,
			'product_weight' => 0,
			'is_gift' => 'No',
			'price_before_quantity_discount' => $price,
			'digital_product_key' => '',
			'cat_name' => '',
			'cat_url' => '',
			'discount_amount' => $discountAmount,
			'promo_discount_amount' => $promoDiscountAmount,
			'final_price' => $price,
			'product_url' => '',
			'cid' => 1,
			'quantity_from_stock' => 0,
		);

		return array_merge($ret, $additionalParams);
	}
}
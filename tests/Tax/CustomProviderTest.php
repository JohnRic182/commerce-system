<?php

use \Mockery as m;

require_once dirname(dirname(__FILE__)).'/_init.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_user.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_order.php';

class CustomProviderTest extends PHPUnit_Framework_TestCase
{
	public function tearDown()
	{
		\Mockery::close();
	}

	/** @var \Mockery\Mock */
	private $orderRepository;
	/** @var \Mockery\Mock */
	private $taxRates;
	private $settings = array(
		'SecurityCookiesPrefix' => '',
		'enable_gift_cert' => 'No',
		'DisplayPricesWithTax' => 'NO',
	);

	function setUp()
	{
		$this->orderRepository = m::mock('DataAccess_OrderRepositoryInterface');
		$this->taxRates = m::mock('TaxRatesInterface');

		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->andReturn(false);
	}

	function test_can_createCustomProvider()
	{
		$customProvider = $this->getCustomProvider();

		$this->assertNotNull($customProvider);
	}

	function test_getDisplayPricesWithTax_Returns_Values_From_TaxRateProvider()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(true);

		$customProvider = $this->getCustomProvider();

		$this->assertTrue($customProvider->getDisplayPricesWithTax());
	}

	function test_getTaxRates_Calls_TaxRateProvider()
	{
		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1, 3, 0)->andReturn(array());

		$customProvider = $this->getCustomProvider();

		$taxRates = $customProvider->getTaxRates(1, 3, 0);

		$this->assertNotNull($taxRates);
	}

	function test_hasTaxRate_Calls_TaxRateProvider()
	{
		$this->taxRates->shouldReceive('hasTaxRate')->times(1)->with(1)->andReturn(true);

		$customProvider = $this->getCustomProvider();

		$hasTaxRate = $customProvider->hasTaxRate(1);

		$this->assertTrue($hasTaxRate);
	}

	function test_getTaxRate_Calls_TaxRateProvider()
	{
		$this->taxRates->shouldReceive('getTaxRate')->times(1)->with(1, false)->andReturn(10.5);

		$customProvider = $this->getCustomProvider();

		$taxRate = $customProvider->getTaxRate(1, false);

		$this->assertEquals(10.5, $taxRate);
	}

	function test_getTaxRateDescription_Calls_TaxRateProvider()
	{
		$this->taxRates->shouldReceive('getTaxRateDescription')->times(1)->with(1)->andReturn('Test Taxes');

		$customProvider = $this->getCustomProvider();

		$taxRateDesc = $customProvider->getTaxRateDescription(1);

		$this->assertEquals('Test Taxes', $taxRateDesc);
	}

	function test_calculateItemTax_Calls_TaxRateProvider()
	{
		$this->taxRates->shouldReceive('calculateTax')->times(1)->with(1, 25.5)->andReturn(2.55);

		$customProvider = $this->getCustomProvider();

		$taxAmt = $customProvider->calculateItemTax(1, 25.5);

		$this->assertEquals(2.55, $taxAmt);
	}

	function test_commitTax_Returns_True()
	{
		$customProvider = $this->getCustomProvider();

		$order = $this->getOrder();

		$this->assertTrue($customProvider->commitTax($order, null, null, null));
	}

	function test_refundTax_Returns_True()
	{
		$customProvider = $this->getCustomProvider();

		$order = $this->getOrder();

		$this->assertTrue($customProvider->refundTax($order, '', ''));
	}

	function test_voidTax_Returns_True()
	{
		$customProvider = $this->getCustomProvider();

		$order = $this->getOrder();

		$this->assertTrue($customProvider->voidTax($order, '', ''));
	}

	function test_deleteTax_Returns_True()
	{
		$customProvider = $this->getCustomProvider();

		$order = $this->getOrder();

		$this->assertTrue($customProvider->deleteTax($order));
	}

	function test_initTax_Sets_Country_State_To_Default_When_Not_Authed()
	{
		$order = $this->setupForDefaultCountryState();

		$customProvider = $this->getCustomProvider();

		$customProvider->initTax($order);

		$this->assertEquals(1, $order->getTaxCountryId());
		$this->assertEquals(3, $order->getTaxStateId());
		$this->assertEquals(0, $order->getUserLevel());
	}

	function test_initTax_Sets_Country_State_To_User_Values_When_Authed_And_No_Shipping_Address()
	{
		$order = $this->setupForUserCountryState();

		$customProvider = $this->getCustomProvider();

		$customProvider->initTax($order);

		$this->assertEquals(1, $order->getTaxCountryId());
		$this->assertEquals(3, $order->getTaxStateId());
		$this->assertEquals(0, $order->getUserLevel());
	}

	function test_initTax_Sets_Country_State_To_User_Address_When_Authed_And_Shipping_Address()
	{
		$order = $this->setupForUserAddressCountryState('Shipping');

		$order->setId(1);
		$order->setShippingRequired(true);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1);
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$customProvider = $this->getCustomProvider();

		$customProvider->initTax($order);

		$this->assertEquals(1, $order->getTaxCountryId());
		$this->assertEquals(3, $order->getTaxStateId());
		$this->assertEquals(0, $order->getUserLevel());
	}

	function test_initTax_Sets_Country_State_To_User_Values_When_Authed_And_Shipping_Address_Not_Found()
	{
		$order = $this->setupForUserAddressCountryState('Shipping', false);

		$order->setId(1);
		$order->setShippingRequired(true);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1);
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$customProvider = $this->getCustomProvider();

		$customProvider->initTax($order);

		$this->assertEquals(13, $order->getTaxCountryId());
		$this->assertEquals(33, $order->getTaxStateId());
		$this->assertEquals(0, $order->getUserLevel());
	}

	//TODO: Fully cover initTax / findTaxZone

	function test_can_Calculate_No_Tax()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 0, 0, array('is_taxable' => 'Yes', 'tax_rate' => 0));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(0, $taxAmount);
		$this->assertEquals(0, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(24.95, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Basic_Tax()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 0, 0, array('is_taxable' => 'Yes', 'tax_rate' => 10));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(2.5, $taxAmount);
		$this->assertEquals(2.5, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(27.45, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Basic_VAT_Tax()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(true);

		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 0, 0, array('is_taxable' => 'Yes', 'tax_rate' => 10, 'price_withtax' => 27.22, 'discount_amount_with_tax' => 0, 'promo_discount_amount_with_tax' => 0));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(2.27, $taxAmount);
		$this->assertEquals(2.27, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(24.95, $order->getSubtotalAmount());
		$this->assertEquals(27.22, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_VAT_Tax_When_Discount_Percent_Applied()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->once()->andReturn(true);

		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 2.5, 0, array('is_taxable' => 'Yes', 'tax_rate' => 8.3, 'price_withtax' => 27.02, 'discount_amount_with_tax' => 2.74, 'promo_discount_amount_with_tax' => 5.38));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->setDiscountType('percent');
		$order->setDiscountValue(10);

		$order->setDiscountAmount(2.5);
		$order->setPromoDiscountAmount(5);

		$lineItem->setDiscountAmount(2.5);
		$lineItem->setPromoDiscountAmount(5);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(1.45, $taxAmount);
		$this->assertEquals(1.45, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(24.95, $order->getSubtotalAmount());
		$this->assertEquals(27.02, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Tax_When_Discount_Percent_Applied()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 2.74, 0, array('is_taxable' => 'Yes', 'tax_rate' => 10));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$order->setDiscountType('percent');
		$order->setDiscountValue(11);

		$customProvider = $this->getCustomProvider();

		Tax_ProviderFactory::setTaxProvider($customProvider);

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(2.22, $taxAmount);
		$this->assertEquals(2.22, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(24.43, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Tax_When_Discount_Amount_Applied()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 5, 0, array('is_taxable' => 'Yes', 'tax_rate' => 10));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$order->setDiscountType('amount');
		$order->setDiscountAmount(5);

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(2.0, $taxAmount);
		$this->assertEquals(2.0, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(21.95, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Tax_When_PromoDiscount_Percent_Applied()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 0, 2.74, array('is_taxable' => 'Yes', 'tax_rate' => 10));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$order->setPromoType('Global');
		$order->setPromoDiscountType('percent');
		$order->setPromoDiscountValue(11);

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(2.22, $taxAmount);
		$this->assertEquals(2.22, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(24.43, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Tax_When_PromoDiscount_Amount_Applied()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 5, 0, array('is_taxable' => 'Yes', 'tax_rate' => 10));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$order->setPromoType('Global');
		$order->setPromoDiscountType('amount');
		$order->setPromoDiscountAmount(5);

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(2.0, $taxAmount);
		$this->assertEquals(2.0, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(21.95, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Tax_With_ShippingTax()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 0, 0, array('is_taxable' => 'Yes', 'tax_rate' => 10));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$order->setShippingTaxable(true);
		$order->setShippingTaxAmount(5);

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(7.5, $taxAmount);
		$this->assertEquals(7.5, $order->getTaxAmount());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals(5, $order->getShippingTaxAmount());
		$this->assertEquals(27.45, $order->getSubtotalAmountWithTax());
	}

	function test_can_Calculate_Tax_With_HandlingTax()
	{
		$order = $this->setupForDefaultCountryState();
		$order->setSubtotalAmount(24.95);

		$lineItemValues = $this->getLineItemValues(1, 24.95, 1, 1, 0, 0, array('is_taxable' => 'Yes', 'tax_rate' => 10));
		$lineItem = new Model_LineItem($lineItemValues, 24.95, $this->settings);

		$order->lineItems = array();
		$order->lineItems[1] = $lineItem;

		$order->setHandlingTaxable(true);
		$order->setHandlingTaxAmount(5);

		$customProvider = $this->getCustomProvider();

		$taxAmount = $customProvider->calculateTax($order);

		$this->assertEquals(7.5, $taxAmount);
		$this->assertEquals(7.5, $order->getTaxAmount());
		$this->assertEquals(5, $order->getHandlingTaxAmount());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals(27.45, $order->getSubtotalAmountWithTax());
	}

	protected function setupForDefaultCountryState()
	{
		$order = $this->getOrder();

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		return $order;
	}

	protected function setupForUserCountryState($taxAddress = 'Billing')
	{
		$order = $this->getOrder();

		$this->settings['TaxDefaultCountry'] = 10;
		$this->settings['TaxDefaultState'] = 30;
		$this->settings['TaxAddress'] = $taxAddress;

		$order->getUser()->auth_ok = true;
		$order->getUser()->data['country'] = 1;
		$order->getUser()->data['state'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		return $order;
	}

	protected function setupForUserAddressCountryState($taxAddress = 'Billing', $addressFound = true)
	{
		$order = $this->getOrder();

		$this->settings['TaxDefaultCountry'] = 10;
		$this->settings['TaxDefaultState'] = 30;
		$this->settings['TaxAddress'] = $taxAddress;

		$order->getUser()->auth_ok = true;
		$order->getUser()->data['country'] = 13;
		$order->getUser()->data['state'] = 33;

		$orderRepository = m::mock('DataAccess_OrderRepositoryInterface');
		$order->setOrderRepository($orderRepository);

		$address = array(
			'country_id' => 0,
		);
		if ($addressFound)
		{
			$address = array(
				'country_id' => 1,
				'state_id' => 3,
			);

			$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());
		}
		else
		{
			$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(13,33,0)->andReturn(array());
		}

		$orderRepository->shouldReceive('getShippingAddress')->atLeast(1)->andReturn($address);

		return $order;
	}

	protected function getOrder()
	{
		$db = null;
		$user = new USER($db, $this->settings, 0);
		$order = new ORDER($db, $this->settings, $user, 1);

		return $order;
	}

	protected function setupGetOrderItemsWithTaxInfo($orderItems = array())
	{
		$this->orderRepository->shouldReceive('getOrderItemsWithTaxInfo')->once()->with(1)->andReturn($orderItems);
	}

	protected function getCustomProvider()
	{
		return new Tax_CustomProvider($this->orderRepository, $this->settings, $this->taxRates);
	}

	protected function getLineItemValues($pid, $price, $quantity, $ocid = 1, $discountAmount = 0, $promoDiscountAmount = 0, $additionalParams = array())
	{
		$ret = array(
			'pid' => $pid,
			'weight' => 0,
			'attributes_count' => 0,
			'price' => $price,
			'is_taxable' => 'Yes',
			'is_doba' => 'No',
			'free_shipping' => 'No',
			'product_type' => Model_Product::TANGIBLE,
			'digital_product_file' => '',
			'products_location_id' => 0,
			'product_id' => 'test_prod',
			'product_sku' => 'sku',
			'product_gtin' => 'gtin',
			'product_mpn' => 'mpn',
			'product_upc' => 'upc',
			'title' => 'Test Product',
			'min_order' => 0,
			'max_order' => 0,
			'inventory_control' => 'No',
			'tax_rate' => -1,
			'tax_description' => '',
			'tax_class_id' => 0,
			'product_sub_id' => '',
			'ocid' => $ocid,
			'product_price' => $price,
			'admin_price' => $price,
			'options' => '',
			'options_clean' => '',
			'admin_quantity' => $quantity,
			'quantity' => $quantity,
			'product_weight' => 0,
			'is_gift' => 'No',
			'price_before_quantity_discount' => $price,
			'price_notax' => $price,
			'digital_product_key' => '',
			'cat_name' => '',
			'cat_url' => '',
			'discount_amount' => $discountAmount,
			'promo_discount_amount' => $promoDiscountAmount,
			'final_price' => $price,
			'product_url' => '',
			'cid' => 1,
		);

		return array_merge($ret, $additionalParams);
	}
}
<?php

class Mock_Helpers
{
	public static function getDdmViewFake($testCase)
	{
		$instance = $testCase->getMockBuilder('Ddm_View')
			->disableOriginalConstructor()
			->setMethods(array('assign', 'fetch'))
			->getMock();

		return $instance;
	}

	public static function getDbFake($testCase, $methods = null)
    {
    	$builder = $testCase->getMockBuilder('DB')
    		->setMethods(array('query', 'moveNext', 'update', 'selectOne', 'selectAll', 'reset', 'assign', 'assignStr', 'insert'))
        	->disableOriginalConstructor();

        if ($methods)
        	$builder = $builder->setMethods($methods);

        return $builder->getMock();
    }

	public static function getFileUtilsFake($testCase)
    {
    	$instance = $testCase->getMockBuilder('FileUtils')
    		->disableOriginalConstructor()
    		->getMock();

    	FileUtils::setInstance($instance);

    	return $instance;
    }

    public static function getSessionMock($testCase)
	{
		return $testCase->getMockBuilder('Session')
					->disableOriginalConstructor()
					->getMock();
	}

	public static function getOrderFake($testCase, $db)
    {
    	$order = $testCase->getMockBuilder('ORDER')
    		->disableOriginalConstructor()
    		->setMethods(array('db', 'reupdateItems'))
    		->getMock();

    	$order->expects($testCase->any())
			->method('db')
			->will($testCase->returnValue($db));

		return $order;
    }

	public static function setUnlinkImages($testCase, $FileUtils, $prefix, $startIndex = 0)
	{
		$FileUtils->expects($testCase->at($startIndex++))
			->method('unlink')
			->with($testCase->equalTo($prefix.'.jpg'));
		$FileUtils->expects($testCase->at($startIndex++))
			->method('unlink')
			->with($testCase->equalTo($prefix.'.gif'));
		$FileUtils->expects($testCase->at($startIndex++))
			->method('unlink')
			->with($testCase->equalTo($prefix.'.png'));

		return $startIndex;
	}
}
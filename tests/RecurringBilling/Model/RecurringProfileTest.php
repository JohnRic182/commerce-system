<?php

require_once dirname(dirname(dirname(__FILE__))).'/_init.php';
require_once dirname(dirname(dirname(dirname(__FILE__)))).'/content/engine/engine_functions.php';

class RecurringBilling_Model_RecurringProfileTest extends PHPUnit_Framework_TestCase
{
	function test_finishCurrentBillingCycle_Increments_BillingSequenceNumber_When_Profile_Has_No_Trials()
	{
		$profile = new RecurringBilling_Model_RecurringProfile();
		$profile->setTrialPeriodUnit(false);

		$profile->setStatus(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE);
		$profile->setBillingPeriodUnit(RecurringBilling_Model_RecurringProfile::UNIT_DAY);
		$profile->setBillingPeriodCycles(0);
		$profile->setBillingPeriodFrequency(1);

		$profile->finishCurrentBillingCycle();
		$this->assertEquals(1, $profile->getBillingSequenceNumber());
	}

	function test_finishCurrentBillingCycle_Increments_BillingSequenceNumber_When_Profile_Has_More_Billing_Cycles()
	{
		$profile = new RecurringBilling_Model_RecurringProfile();
		$profile->setTrialEnabled(false);
		$profile->setTrialPeriodUnit(false);

		$profile->setStatus(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE);
		$profile->setBillingPeriodUnit(RecurringBilling_Model_RecurringProfile::UNIT_DAY);
		$profile->setBillingPeriodCycles(5);
		$profile->setBillingPeriodFrequency(1);
		$profile->setBillingSequenceNumber(4);

		$profile->finishCurrentBillingCycle();
		$this->assertEquals(5, $profile->getBillingSequenceNumber());
		$this->assertEquals(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE, $profile->getStatus());
		$this->assertEquals(date('Y-m-d', strtotime("+1DAY")), $profile->getDateNextBilling()->format('Y-m-d'));
	}

	function test_finishCurrentBillingCycle_Increments_TrialSequenceNumber_When_Profile_Has_Trials()
	{
		$profile = new RecurringBilling_Model_RecurringProfile();
		$profile->setTrialEnabled(true);
		$profile->setTrialPeriodUnit(RecurringBilling_Model_RecurringProfile::UNIT_WEEK);
		$profile->setTrialPeriodFrequency(1);
		$profile->setTrialPeriodCycles(2);

		$profile->setStatus(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE);
		$profile->setBillingPeriodUnit(RecurringBilling_Model_RecurringProfile::UNIT_DAY);
		$profile->setBillingPeriodCycles(0);
		$profile->setBillingPeriodFrequency(1);

		$profile->finishCurrentBillingCycle();
		$this->assertEquals(1, $profile->getTrialSequenceNumber());
		$this->assertEquals(date('Y-m-d', strtotime("+1WEEK")), $profile->getDateNextBilling()->format('Y-m-d'));
		$this->assertEquals(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE, $profile->getStatus());
	}

	function test_finishCurrentBillingCycle_Increments_TrialSequenceNumber_Sets_Up_Regular_Recurring_When_Profile_Has_Trials_With_Max_2()
	{
		$profile = new RecurringBilling_Model_RecurringProfile();
		$profile->setTrialEnabled(true);
		$profile->setTrialPeriodUnit(RecurringBilling_Model_RecurringProfile::UNIT_WEEK);
		$profile->setTrialPeriodFrequency(1);
		$profile->setTrialPeriodCycles(2);
		$profile->setTrialSequenceNumber(1);

		$profile->setStatus(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE);
		$profile->setBillingPeriodUnit(RecurringBilling_Model_RecurringProfile::UNIT_MONTH);
		$profile->setBillingPeriodCycles(0);
		$profile->setBillingPeriodFrequency(1);
		$profile->setBillingSequenceNumber(0);

		$profile->finishCurrentBillingCycle();
		$this->assertEquals(2, $profile->getTrialSequenceNumber());
		$this->assertEquals(date('Y-m-d', strtotime("+1WEEK")), $profile->getDateNextBilling()->format('Y-m-d'));
		$this->assertEquals(0, $profile->getBillingSequenceNumber());

		$profile->finishCurrentBillingCycle();
		$this->assertEquals(2, $profile->getTrialSequenceNumber());
		$this->assertEquals(1, $profile->getBillingSequenceNumber());
		$this->assertEquals(date('Y-m-d', strtotime('+1MONTH')), $profile->getDateNextBilling()->format('Y-m-d'));
		$this->assertEquals(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE, $profile->getStatus());
	}

	function test_finishCurrentBillingCycle_Increments_BillingSequenceNumber_Expires_Profile_When_Profile_Has_Finished_All()
	{
		$profile = new RecurringBilling_Model_RecurringProfile();
		$profile->setTrialEnabled(false);

		$profile->setStatus(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE);
		$profile->setBillingPeriodUnit(RecurringBilling_Model_RecurringProfile::UNIT_DAY);
		$profile->setBillingPeriodCycles(2);
		$profile->setBillingPeriodFrequency(1);
		$profile->setBillingSequenceNumber(2);
		$profile->setDateNextBilling(new DateTime());

		$profile->finishCurrentBillingCycle();
		$this->assertEquals(2, $profile->getBillingSequenceNumber());
		$this->assertEquals(RecurringBilling_Model_RecurringProfile::STATUS_COMPLETED, $profile->getStatus());
		$this->assertEquals(date('Y-m-d'), $profile->getDateNextBilling()->format('Y-m-d'));
	}
}
<?php

require_once dirname(__FILE__).'/_init.php';

require_once CONTENT_DIR.'engine/engine_order.php';
require_once CONTENT_DIR.'engine/engine_user.php';

class EngineOrderTest extends PHPUnit_Framework_TestCase
{
	function test_isDobaProduct_Returns_True_When_Item_Is_Doba()
	{
		$item = array(
			'is_doba' => 'Yes',
		);

		$order = $this->getOrder();

		$this->assertTrue($order->isDobaProduct($item));
	}

	function test_isDobaProduct_Returns_False_When_Item_Is_Not_Doba()
	{
		$item = array(
			'is_doba' => 'No',
		);

		$order = $this->getOrder();

		$this->assertFalse($order->isDobaProduct($item));
	}

	function test_isProductLevelShipping_Returns_True_When_Item_Is_Shipping_Price()
	{
		$item = array(
			'product_is_shipping_price' => 'Yes',
		);

		$order = $this->getOrder();

		$this->assertTrue($order->isProductLevelShipping($item));
	}

	function test_isProductLevelShipping_Returns_False_When_Item_Is_Not_Shipping_Price()
	{
		$item = array(
			'product_is_shipping_price' => 'No',
		);

		$order = $this->getOrder();

		$this->assertFalse($order->isProductLevelShipping($item));
	}

	function test_isItemShippable_Returns_False_When_Item_Is_DigitalProduct()
	{
		$item = array(
			'free_shipping' => 'No',
			'product_type' => Model_Product::DIGITAL,
		);

		$order = $this->getOrder();

		$this->assertFalse($order->isItemShippable($item, true));
	}

//	function test_isItemShippable_Returns_True_When_Item_Is_Not_DigitalProduct_And_Is_Not_Free_Shipping()
//	{
//		$item = array(
//			'free_shipping' => 'No',
//			'product_type' => Model_Product::TANGIBLE,
//		);
//
//		$order = $this->getOrder();
//
//		$order->setShippingIgnoreFree(false);
//
//		$this->assertTrue($order->isItemShippable($item));
//	}

//	function test_isItemShippable_Returns_False_When_Item_Is_Not_DigitalProduct_And_Is_Free_Shipping()
//	{
//		$item = array(
//			'free_shipping' => 'Yes',
//			'product_type' => Model_Product::TANGIBLE,
//		);
//
//		$order = $this->getOrder();
//
//		$order->setShippingIgnoreFree(false);
//
//		$this->assertFalse($order->isItemShippable($item));
//	}

//	function test_isItemShippable_Returns_True_When_Item_Is_Not_DigitalProduct_And_Is_Free_Shipping_And_Order_Ignores_Free_Shipping()
//	{
//		$item = array(
//			'free_shipping' => 'Yes',
//			'product_type' => Model_Product::TANGIBLE,
//		);
//
//		$order = $this->getOrder();
//
//		$order->setShippingIgnoreFree(true);
//
//		$this->assertTrue($order->isItemShippable($item));
//	}

//	function test_Discount_Test()
//	{
//
//	}

	protected function getOrder()
	{
		$db = null;
		$settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
		);
		$user = new USER($db, $settings, 0);
		return new ORDER($db, $settings, $user, 1);
	}
}
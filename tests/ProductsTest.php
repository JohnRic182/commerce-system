<?php

class ProductsTest extends PHPUnit_Framework_TestCase
{
//	$product['attributes']['paid'] = array(
//		'paid' => {id},
//		'attribute_type' => 'select',
//		'pid' => {pid},
//		'gaid' => 0,
//		'is_modifier' => 'No',
//		'is_active' => 'Yes',
//		'priority' => 5,
//		'track_inventory' => 1,
//		'name' => {name},
//		'caption' => {caption},
//		'text_length' => 0,
//		'options' => array(
//			'Red' => array(
//				'full' => 'Red',
//				'name' => 'Red',
//				'price' => 5.0,
//				'price_difference' => 0,
//				'weight' => 0,
//				'modifier' => '',
//				'option_value' => 0,
//				'option_value_type' => 'amount',
//				'weight_option_value' => 0,
//				'weight_option_value_type' => 'amount',
//				'weight_difference' => 0,
//				'first' => true,
//			)
//		),
//	);

//	$product['inventory_hashes'] = array(
//		0 => array(
//			'pi_id' => {pi_id},
//			'pid' => {pid},
//			'is_active' => 1,
//			'stock' => 5,
//			'stock_warning' => 1,
//			'product_subid' => 'test_combos_r_s',
//			'attributes_hash' => 'dfadfaf',
//			'attributes_list' => 'Color: Red\nSize: Small',
//		),
//	);

	function test_cleanupSubProducts_Removes_Attributes_When_Set_To_Hide()
	{
		$product = $this->getProductSingleAttribute();

		$db = null;
		$settings = array();
		$products = new ShoppingCartProducts($db, $settings);

		$products->cleanupSubProducts($product);

		$this->assertArrayNotHasKey('Medium', $product['attributes'][1]['options']);
		$this->assertArrayHasKey('Small', $product['attributes'][1]['options']);
	}

	function test_cleanupSubProducts_Removes_Multiple_Attributes_When_Set_To_Hide()
	{
		$product = $this->getProductMultipleAttribute();

		$db = null;
		$settings = array();
		$products = new ShoppingCartProducts($db, $settings);

		$products->cleanupSubProducts($product);

		$this->assertArrayNotHasKey('Medium', $product['attributes'][1]['options']);
		$this->assertArrayHasKey('Small', $product['attributes'][1]['options']);
		$this->assertArrayHasKey('Red', $product['attributes'][2]['options']);
		$this->assertArrayHasKey('Green', $product['attributes'][2]['options']);
		$this->assertArrayNotHasKey('Blue', $product['attributes'][2]['options']);
	}

	function test_cleanupSubProducts_Removes_Multiple_Attributes_When_Set_To_Hide_But_Only_When_In_Inventory()
	{
		$product = $this->getProductMultipleAttributesWithSizeInInventory();

		$db = null;
		$settings = array();
		$products = new ShoppingCartProducts($db, $settings);

		$products->cleanupSubProducts($product);

		$this->assertArrayNotHasKey('Medium', $product['attributes'][1]['options']);
		$this->assertArrayHasKey('Small', $product['attributes'][1]['options']);
		$this->assertArrayHasKey('Red', $product['attributes'][2]['options']);
		$this->assertArrayHasKey('Green', $product['attributes'][2]['options']);
		$this->assertArrayHasKey('Blue', $product['attributes'][2]['options']);
	}

	function test_cleanupSubProducts_Removes_Attributes_When_Set_To_OutOfStock()
	{
		$product = $this->getProductSingleAttribute('OutOfStock');

		$db = null;
		$settings = array();
		$products = new ShoppingCartProducts($db, $settings);

		$products->cleanupSubProducts($product);

		$this->assertArrayHasKey('Medium', $product['attributes'][1]['options']);
		$this->assertArrayHasKey('Small', $product['attributes'][1]['options']);
	}

	function getProductSingleAttribute($inventory_rule = 'Hide')
	{
		return array(
			'inventory_control' => 'AttrRuleExc',
			'inventory_rule' => $inventory_rule,
			'attributes' => array(
				1 => array(
					'paid' => 1,
					'attribute_type' => 'select',
					'pid' => 1,
					'gaid' => 0,
					'is_modifier' => 'No',
					'is_active' => 'Yes',
					'priority' => 5,
					'track_inventory' => 1,
					'name' => 'Size',
					'caption' => 'Size',
					'text_length' => 0,
					'options' => array(
						'Small' => array(
							'full' => 'Small',
							'name' => 'Small',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => true,
						),
						'Medium' => array(
							'full' => 'Medium',
							'name' => 'Medium',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => false,
						),
					)
				)
			),
			'inventory_hashes' => array(
				0 => array(
					'pi_id' => 1,
					'pid' => 1,
					'is_active' => 1,
					'stock' => 5,
					'stock_warning' => 1,
					'product_subid' => 'test_combos_s',
					'attributes_hash' => 'dfadfaf',
					'attributes_list' => 'Size: Small',
				),
			)
		);
	}

	function getProductMultipleAttributesWithSizeInInventory($inventory_rule = 'Hide')
	{
		return array(
			'inventory_control' => 'AttrRuleExc',
			'inventory_rule' => $inventory_rule,
			'attributes' => array(
				1 => array(
					'paid' => 1,
					'attribute_type' => 'select',
					'pid' => 1,
					'gaid' => 0,
					'is_modifier' => 'No',
					'is_active' => 'Yes',
					'priority' => 5,
					'track_inventory' => 1,
					'name' => 'Size',
					'caption' => 'Size',
					'text_length' => 0,
					'options' => array(
						'Small' => array(
							'full' => 'Small',
							'name' => 'Small',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => true,
						),
						'Medium' => array(
							'full' => 'Medium',
							'name' => 'Medium',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => false,
						),
					)
				),
				2 => array(
					'paid' => 1,
					'attribute_type' => 'select',
					'pid' => 1,
					'gaid' => 0,
					'is_modifier' => 'No',
					'is_active' => 'Yes',
					'priority' => 5,
					'track_inventory' => 0,
					'name' => 'Color',
					'caption' => 'Color',
					'text_length' => 0,
					'options' => array(
						'Red' => array(
							'full' => 'Red',
							'name' => 'Red',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => true,
						),
						'Green' => array(
							'full' => 'Green',
							'name' => 'Green',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => false,
						),
						'Blue' => array(
							'full' => 'Blue',
							'name' => 'Blue',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => false,
						),
					)
				)
			),
			'inventory_hashes' => array(
				0 => array(
					'pi_id' => 1,
					'pid' => 1,
					'is_active' => 1,
					'stock' => 5,
					'stock_warning' => 1,
					'product_subid' => 'test_combos_s',
					'attributes_hash' => 'dfadfaf',
					'attributes_list' => "Size: Small",
				),
			)
		);
	}

	function getProductMultipleAttribute($inventory_rule = 'Hide')
	{
		return array(
			'inventory_control' => 'AttrRuleExc',
			'inventory_rule' => $inventory_rule,
			'attributes' => array(
				1 => array(
					'paid' => 1,
					'attribute_type' => 'select',
					'pid' => 1,
					'gaid' => 0,
					'is_modifier' => 'No',
					'is_active' => 'Yes',
					'priority' => 5,
					'track_inventory' => 1,
					'name' => 'Size',
					'caption' => 'Size',
					'text_length' => 0,
					'options' => array(
						'Small' => array(
							'full' => 'Small',
							'name' => 'Small',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => true,
						),
						'Medium' => array(
							'full' => 'Medium',
							'name' => 'Medium',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => false,
						),
					)
				),
				2 => array(
					'paid' => 1,
					'attribute_type' => 'select',
					'pid' => 1,
					'gaid' => 0,
					'is_modifier' => 'No',
					'is_active' => 'Yes',
					'priority' => 5,
					'track_inventory' => 1,
					'name' => 'Color',
					'caption' => 'Color',
					'text_length' => 0,
					'options' => array(
						'Red' => array(
							'full' => 'Red',
							'name' => 'Red',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => true,
						),
						'Green' => array(
							'full' => 'Green',
							'name' => 'Green',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => false,
						),
						'Blue' => array(
							'full' => 'Blue',
							'name' => 'Blue',
							'price' => 5,
							'price_difference' => 0,
							'weight' => 0,
							'modifier' => '',
							'option_value' => 0,
							'option_value_type' => 'amount',
							'weight_option_value' => 0,
							'weight_option_value_type' => 'amount',
							'weight_difference' => 0,
							'first' => false,
						),
					)
				)
			),
			'inventory_hashes' => array(
				0 => array(
					'pi_id' => 1,
					'pid' => 1,
					'is_active' => 1,
					'stock' => 5,
					'stock_warning' => 1,
					'product_subid' => 'test_combos_s_r',
					'attributes_hash' => 'dfadfaf',
					'attributes_list' => "Size: Small\nColor: Red",
				),
				1 => array(
					'pi_id' => 1,
					'pid' => 1,
					'is_active' => 1,
					'stock' => 5,
					'stock_warning' => 1,
					'product_subid' => 'test_combos_s_g',
					'attributes_hash' => 'dfadfaf',
					'attributes_list' => "Size: Small\nColor: Green",
				),
			)
		);
	}
}
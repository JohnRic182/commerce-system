<?php

/**
* 
*/
class ImageUtilityTest extends PHPUnit_Framework_TestCase
{
	protected function setUp()
	{
//		vfsStreamWrapper::register();
//		vfsStreamWrapper::setRoot(new vfsStreamDirectory('tmp'));
	}

	public function test_removeExistingImagesByName_Deletes_All_Matching_Similar_Filename_Extensions()
	{
		$this->markTestSkipped();

		vfsStreamWrapper::getRoot()->addChild(vfsStream::newFile('image.jpg'));
		vfsStreamWrapper::getRoot()->addChild(vfsStream::newFile('image.gif'));
		vfsStreamWrapper::getRoot()->addChild(vfsStream::newFile('image.png'));

		ImageUtility::removeExistingImagesByName(vfsStream::url('tmp/image.jpg'));

		$this->assertFalse(vfsStreamWrapper::getRoot()->hasChild('image.jpg'), 'JPG image should NOT exist');
		$this->assertFalse(vfsStreamWrapper::getRoot()->hasChild('image.gif'), 'GIF image should NOT exist');
		$this->assertFalse(vfsStreamWrapper::getRoot()->hasChild('image.png'), 'PNG image should NOT exist');
	}
}
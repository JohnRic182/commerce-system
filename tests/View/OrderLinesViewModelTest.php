<?php

require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_user.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_order.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/classes/View/Model.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/classes/View/OrderLinesViewModel.php';

class View_OrderLinesViewModelExposed extends View_OrderLinesViewModel
{
	public function resetLineItems()
	{
		parent::resetLineItems();
	}

	public function addLineItem($key, $title, $subtitle = "", $amount)
	{
		parent::addLineItem($key, $title, $subtitle, $amount);
	}

	public function formatPrice($amount)
	{
		return '$'.number_format($amount, 2, ".", ",");
	}

}

class View_OrderLinesViewModelTest extends PHPUnit_Framework_TestCase
{
	function setup_settings($settings = array())
	{
		return array_merge(
			array(
				'SecurityCookiesPrefix' => '',
				'enable_gift_cert' => 'No',
				'ShippingHandlingFee' => '1',
				'ShippingHandlingFeeType' => 'amount',
				'ShippingHandlingFeeWhenFreeShipping' => '0',
				'ShippingHandlingText' => 'Handling text',
				'ShippingHandlingSeparated' => '0',
				'ShippingHandlingTaxable' => '0',
				'ShippingHandlingTaxClassId' => '0',
				'RecurringBillingEnabled' => '0',
			),
			$settings
		);
	}

	function test_Line_Items_Manipulation()
	{
		$settings = $this->setup_settings();
		$msg = $this->getDefaultMsgs();

		$orderViewModel = new View_OrderLinesViewModelExposed($settings, $msg);

		$orderViewModel->addLineItem("fake1", "Fake 1", "fake 1", 3333.455);
		$orderViewModel->addLineItem("fake2", "Fake 2", "fake 2", 5555.634);
		$orderViewModel->addLineItem("fake3", "Fake 3", "fake 3", 7777.777);

		$lineItems = $orderViewModel->getLineItems();

		$this->assertArrayHasKey("fake1", $lineItems);
		$this->assertArrayHasKey("fake2", $lineItems);
		$this->assertArrayHasKey("fake3", $lineItems);

		$this->assertEquals(array("title"=>"Fake 1", "subtitle"=>"fake 1", "amount"=>"$3,333.46"), $lineItems["fake1"]);
		$this->assertEquals(array("title"=>"Fake 2", "subtitle"=>"fake 2", "amount"=>"$5,555.63"), $orderViewModel->getLineItem("fake2"));
		$this->assertEquals(array("title"=>"Fake 3", "subtitle"=>"fake 3", "amount"=>"$7,777.78"), $orderViewModel->getLineItem("fake3"));

		$orderViewModel->resetLineItems();

		$this->assertEmpty($orderViewModel->getLineItems());

	}

	function test_build_When_Display_Items_With_Tax()
	{
		$settings = $this->setup_settings(array('DisplayPricesWithTax' => 'NO'));

//		$taxProvider = $this->getMock('Tax_ProviderInterface');
//		Tax_ProviderFactory::setTaxProvider($taxProvider);
//
//		$taxProvider->expects($this->once())
//			->method('getDisplayPricesWithTax')
//			->will($this->returnValue(true));

		$msg = $this->getDefaultMsgs();

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setSubtotalAmount(1050.555);
		$order->setSubtotalAmountWithTax(1090.777);
		$order->setTotalAmount(1090.78);

		$orderViewModel = new View_OrderLinesViewModelExposed($settings, $msg);

		$orderViewModel->build($order);

		$this->assertEquals(array("title"=>"Subtotal", "subtitle"=>"", "amount"=>"$1,050.56"), $orderViewModel->getLineItem("subtotal"));
		$this->assertEquals(array("title"=>"Total", "subtitle"=>"", "amount"=>"$1,090.78"), $orderViewModel->getLineItem("total"));
	}

	function test_build_When_Display_Items_Without_Tax()
	{
		$settings = $this->setup_settings(array(
			"DisplayPricesWithTax" => "NO"
		));

		$msg = $this->getDefaultMsgs();

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setSubtotalAmount(1050.555);
		$order->setSubtotalAmountWithTax(1090.777);
		$order->setTotalAmount(1050.555);

		$orderViewModel = new View_OrderLinesViewModelExposed($settings, $msg);

		$orderViewModel->build($order);

		$this->assertEquals(array("title"=>"Subtotal", "subtitle"=>"", "amount"=>"$1,050.56"), $orderViewModel->getLineItem("subtotal"));
		$this->assertEquals(array("title"=>"Total", "subtitle"=>"", "amount"=>"$1,050.56"), $orderViewModel->getLineItem("total"));
	}

	function test_build_With_Empty_Values()
	{
		$settings = $this->setup_settings(array(
			"DisplayPricesWithTax" => "NO"
		));
		$msg = $this->getDefaultMsgs();

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setSubtotalAmount(1050.555);
		$order->setSubtotalAmountWithTax(1090.777);
		$order->setTaxAmount(1090.777 - 1050.555);
		$order->setTotalAmount(1090.777);

		$orderViewModel = new View_OrderLinesViewModelExposed($settings, $msg);

		$orderViewModel->build($order);

		$this->assertEquals(array("title"=>"Subtotal", "subtitle"=>"", "amount"=>"$1,050.56"), $orderViewModel->getLineItem("subtotal"));
		$this->assertEquals(null, $orderViewModel->getLineItem("discount"));
		$this->assertEquals(null, $orderViewModel->getLineItem("promo"));
		$this->assertEquals(null, $orderViewModel->getLineItem("shipping"));
		$this->assertEquals(null, $orderViewModel->getLineItem("handling"));
		$this->assertEquals(array("title"=>"Tax", "subtitle"=>"", "amount"=>"$40.22"), $orderViewModel->getLineItem("tax"));
		$this->assertEquals(array("title"=>"Total", "subtitle"=>"", "amount"=>"$1,090.78"), $orderViewModel->getLineItem("total"));
	}

	function test_build_With_Filled_Values()
	{
		$settings = $this->setup_settings(array(
			"DisplayPricesWithTax" => "NO"
		));
		$msg = $this->getDefaultMsgs();

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setSubtotalAmount(1050.555);
		$order->setSubtotalAmountWithTax(1090.777);

		$order->setDiscountAmount(190.739);
		$order->setPromoDiscountAmount(90.738);
		$order->setShippingAmount(10.123);
		$order->setHandlingSeparated(true);
		$order->setHandlingAmount(19.421);

		$order->setTaxAmount(1090.777 - 1050.555);
		$order->setTotalAmount(1090.777);

		$orderViewModel = new View_OrderLinesViewModelExposed($settings, $msg);

		$orderViewModel->build($order);

		$this->assertEquals(array("title"=>"Subtotal", "subtitle"=>"", "amount"=>"$1,050.56"), $orderViewModel->getLineItem("subtotal"));
		$this->assertEquals(array("title"=>"Discount", "subtitle"=>"", "amount"=>"$-190.74"), $orderViewModel->getLineItem("discount"));
		$this->assertEquals(array("title"=>"Promo Discount", "subtitle"=>"", "amount"=>"$-90.74"), $orderViewModel->getLineItem("promo"));
		$this->assertEquals(array("title"=>"Shipping", "subtitle"=>"", "amount"=>"$10.12"), $orderViewModel->getLineItem("shipping"));
		$this->assertEquals(array("title"=>"Handling", "subtitle"=>"", "amount"=>"$19.42"), $orderViewModel->getLineItem("handling"));
		$this->assertEquals(array("title"=>"Tax", "subtitle"=>"", "amount"=>"$40.22"), $orderViewModel->getLineItem("tax"));
		$this->assertEquals(array("title"=>"Total", "subtitle"=>"", "amount"=>"$1,090.78"), $orderViewModel->getLineItem("total"));
	}

	protected function getDefaultMsgs()
	{
		return array(
			'opc' => array(
				'invoice_subtotal' => 'Subtotal',
				'invoice_discount' => 'Discount',
				'invoice_promo_discount' => 'Promo Discount',
				'invoice_shipping' => 'Shipping',
				'invoice_handling' => 'Handling',
				'invoice_tax' => 'Tax',
				'invoice_total' => 'Total',
				'invoice_gift_certificate' => 'Gift Certificate',
				'invoice_remaining_amount' => 'Remaining Amount',
			),
		);
	}
}
<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_TaxTest extends PHPUnit_Framework_TestCase
{
	protected $db;
	protected $settings;
	protected $user;
	protected $order;
	protected $taxService;

	public function setUp()
	{
		$this->db = null;
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
		);

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);
		$this->order->setShippingAddress(array('address1' => '123 Test Dr'));

		$this->taxService = $this->getMock('Tax_TaxServiceInterface');
	}

	function test_can_reset_tax()
	{
		$this->order->setTaxAmount(20.25);

		$calculator = new Calculator_Tax($this->taxService);

		$calculator->reset($this->order);

		$this->assertEquals(0, $this->order->getTaxAmount());
	}

	function test_calculate_calls_taxProvider_calculateTax()
	{
		$this->order->setTaxAmount(20.25);

		$calculator = new Calculator_Tax($this->taxService);

		$this->taxService->expects($this->once())
			->method('calculateTax')
			->with($this->equalTo($this->order));

		$ret = $calculator->calculate($this->order);

		$this->assertEquals(20.25, $ret);
	}
}
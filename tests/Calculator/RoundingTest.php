<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_RoundingTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
	}

	function test_can_Round_Non_VAT_Taxes()
	{
		$productPrice = 2.87;
		$quantity = 3;
		$discount = 33;
		$discountType = 'percent';
		$taxRate = 10;

		$lineSubTotal = round($productPrice * $quantity, 2);
		$lineDiscountTotal = round($lineSubTotal * $discount / 100, 2);
		$lineTotal = $lineSubTotal - $lineDiscountTotal;

		$taxTotal = round($lineTotal * $taxRate / 100, 2);
		$subtotal = $lineTotal;
		$total = $subtotal + $taxTotal;

		$this->assertEquals(8.61, $lineSubTotal);
		$this->assertEquals(2.84, $lineDiscountTotal);
		$this->assertEquals(5.77, $lineTotal);
		$this->assertEquals(0.58, $taxTotal);
		$this->assertEquals(6.35, $total);
	}

	function test_can_Round_VAT_Taxes()
	{
		$productPrice = 2.87;
		$quantity = 3;
		$discount = 33;
		$discountType = 'percent';
		$taxRate = 10;

		$adjustedProductPrice = round($productPrice * (1 + $taxRate / 100), 2);
		$lineSubTotal = round($adjustedProductPrice * $quantity, 2);
		$lineDiscountTotal = round($lineSubTotal * $discount / 100, 2);
		$lineTotal = $lineSubTotal - $lineDiscountTotal;

		$taxTotal = round($adjustedProductPrice - $productPrice, 2) * $quantity;
		$subtotal = $lineTotal;
		$total = $subtotal;

		$this->assertEquals(3.16, $adjustedProductPrice);
		$this->assertEquals(9.48, $lineSubTotal);
		$this->assertEquals(3.13, $lineDiscountTotal);
		$this->assertEquals(6.35, $lineTotal);
		$this->assertEquals(0.87, $taxTotal);
		$this->assertEquals(6.35, $total);
	}

	function test_round_from_Victors_Spreadsheet_Non_VAT()
	{
		$productPrice1 = 12.38;
		$quantity1 = 17;
		$productPrice2 = 17.42;
		$quantity2 = 23;
		$productPrice3 = 12.15;
		$quantity3 = 12;
		$productPrice4 = 12.31;
		$quantity4 = 92;

		$discount = 11;
		$discountType = 'percent';
		$taxRate = 7.78;

		$line1SubTotal = round($productPrice1 * $quantity1, 2);
		$line1DiscountTotal = round($line1SubTotal * $discount / 100, 2);
		$line1Total = $line1SubTotal - $line1DiscountTotal;

		$line2SubTotal = round($productPrice2 * $quantity2, 2);
		$line2DiscountTotal = round($line2SubTotal * $discount / 100, 2);
		$line2Total = $line2SubTotal - $line2DiscountTotal;

		$line3SubTotal = round($productPrice3 * $quantity3, 2);
		$line3DiscountTotal = round($line3SubTotal * $discount / 100, 2);
		$line3Total = $line3SubTotal - $line3DiscountTotal;

		$line4SubTotal = round($productPrice4 * $quantity4, 2);
		$line4DiscountTotal = round($line4SubTotal * $discount / 100, 2);
		$line4Total = $line4SubTotal - $line4DiscountTotal;

		$subtotal = $line1Total + $line2Total + $line3Total + $line4Total;

		$taxTotal = round($subtotal * $taxRate / 100, 2);

		$total = $subtotal + $taxTotal;

		$this->assertEquals(210.46, $line1SubTotal);
		$this->assertEquals(23.15, $line1DiscountTotal);
		$this->assertEquals(187.31, $line1Total);

		$this->assertEquals(400.66, $line2SubTotal);
		$this->assertEquals(44.07, $line2DiscountTotal);
		$this->assertEquals(356.59, $line2Total);

		$this->assertEquals(145.80, $line3SubTotal);
		$this->assertEquals(16.04, $line3DiscountTotal);
		$this->assertEquals(129.76, $line3Total);

		$this->assertEquals(1132.52, $line4SubTotal);
		$this->assertEquals(124.58, $line4DiscountTotal);
		$this->assertEquals(1007.94, $line4Total);

		$this->assertEquals(1681.60, $subtotal);

		$this->assertEquals(130.83, $taxTotal);

		$this->assertEquals(1812.43, $total);
	}

	function test_round_from_Victors_Spreadsheet_VAT()
	{
		$productPrice1 = 12.38;
		$quantity1 = 17;
		$productPrice2 = 17.42;
		$quantity2 = 23;
		$productPrice3 = 12.15;
		$quantity3 = 12;
		$productPrice4 = 12.31;
		$quantity4 = 92;

		$discount = 11;
		$discountType = 'percent';
		$taxRate = 7.78;

		$adjustedProductPrice1 = round($productPrice1 * (1 + $taxRate / 100), 2);
		$adjustedProductPrice2 = round($productPrice2 * (1 + $taxRate / 100), 2);
		$adjustedProductPrice3 = round($productPrice3 * (1 + $taxRate / 100), 2);
		$adjustedProductPrice4 = round($productPrice4 * (1 + $taxRate / 100), 2);

		$line1SubTotal = round($adjustedProductPrice1 * $quantity1, 2);
		$line1DiscountTotal = round($line1SubTotal * $discount / 100, 2);
		$line1Total = $line1SubTotal - $line1DiscountTotal;

		$line2SubTotal = round($adjustedProductPrice2 * $quantity2, 2);
		$line2DiscountTotal = round($line2SubTotal * $discount / 100, 2);
		$line2Total = $line2SubTotal - $line2DiscountTotal;

		$line3SubTotal = round($adjustedProductPrice3 * $quantity3, 2);
		$line3DiscountTotal = round($line3SubTotal * $discount / 100, 2);
		$line3Total = $line3SubTotal - $line3DiscountTotal;

		$line4SubTotal = round($adjustedProductPrice4 * $quantity4, 2);
		$line4DiscountTotal = round($line4SubTotal * $discount / 100, 2);
		$line4Total = $line4SubTotal - $line4DiscountTotal;


		$subtotal = $line1Total + $line2Total + $line3Total + $line4Total;

		$taxTotal = $this->getProductVATTax($adjustedProductPrice1, $productPrice1) * $quantity1 + 
			$this->getProductVATTax($adjustedProductPrice2, $productPrice2) * $quantity2 +
			$this->getProductVATTax($adjustedProductPrice3, $productPrice3) * $quantity3  + 
			$this->getProductVATTax($adjustedProductPrice4, $productPrice4) * $quantity4;

		$total = $subtotal;

		$this->assertEquals(13.34, $adjustedProductPrice1);
		$this->assertEquals(18.78, $adjustedProductPrice2);
		$this->assertEquals(13.10, $adjustedProductPrice3);
		$this->assertEquals(13.27, $adjustedProductPrice4);

		$this->assertEquals(226.78, $line1SubTotal);
		$this->assertEquals(24.95, $line1DiscountTotal);
		$this->assertEquals(201.83, $line1Total);

		$this->assertEquals(431.94, $line2SubTotal);
		$this->assertEquals(47.51, $line2DiscountTotal);
		$this->assertEquals(384.43, $line2Total);

		$this->assertEquals(157.20, $line3SubTotal);
		$this->assertEquals(17.29, $line3DiscountTotal);
		$this->assertEquals(139.91, $line3Total);

		$this->assertEquals(1220.84, $line4SubTotal);
		$this->assertEquals(134.29, $line4DiscountTotal);
		$this->assertEquals(1086.55, $line4Total);

		$this->assertEquals(1812.72, $subtotal);

		$this->assertEquals(147.32, $taxTotal);

		$this->assertEquals(1812.72, $total);
	}

	protected function getProductVATTax($adjustedPrice, $price)
	{
		return round($adjustedPrice - $price, 2);
	}
}
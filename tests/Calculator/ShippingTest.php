<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';
require_once '../content/classes/Calculator/Shipping.php';

class Shipping_MethodFakeErrorTest implements Shipping_MethodInterface
{
	public function setShippingMethodId($shippingMethodId){}
	public function getShippingMethodId(){}
	public function calculate($db, $from, $to, $weight, $itemsCount, $itemsPriceBasedPrice, &$errorMessage, $defaultCurrencyCode = 'USD', $currencyExchangeRates = array())
	{
		$errorMessage = "Some error message";
	}
}

class Calculator_ShippingTest extends PHPUnit_Framework_TestCase
{
	protected $db;
	protected $settings;
	protected $user;
	protected $order;
	protected $shippingRepository;
	protected $orderRepository;
	protected $dobaOrderApi;
	protected $currencies;

	protected $addressOrigin = array(
		"country" => "United States", 
		"country_iso_a2" => "US", 
		"country_iso_a3" => "USA", 
		"country_iso_number" => "0840",
		"zip" => "91608"
	);

	protected $orderShippingAddress = array(
		"shipping_address_type" => "Business",
		"shipping_name" => "John Doe",
		"shipping_address1" => "4641 N 12th Srt.",
		"shipping_address2" => "STE 200",
		"shipping_city" => "Phoenix",
		"shipping_province" => "Arizona",
		"shipping_country" => "1",
		"shipping_zip" => "85014-1233",
		"country_name" => "United States",
		"iso_a2" => "US",
		"country_iso_a2" => "US",
		"iso_a3" => "USA",
		"country_iso_a3" => "USA", 
		"iso_number" => "840",
		"country_iso_number" => "840",
		"state_name" => "Arizona",
		"state_abbr" =>"AZ",
		"state_code" => "AZ"
	);

	protected $addressTo1 = array(
		"address_type" => "Business",
		"name" => "John Doe",
		"address1" => "4641 N 12th Srt.",
		"address2" => "STE 200",
		"city" => "Phoenix",
		"state" => "Arizona",
		"state_abbr" => "AZ",
		"country" => "United States",
		"country_iso_a2" => "US",
		"country_iso_a3" => "USA",
		"country_iso_number" => "840",
		"zip" => "85014",
		"state_code" => "AZ",
	);

	function setup_Settings($settings = array())
	{
		return array_merge(
			array(
				'SecurityCookiesPrefix' => '',
				'enable_gift_cert' => 'No',
				"ShippingInternational" => "NO",
				"ShippingOriginCountry" => 1,
				"ShippingCalcEnabled" => "YES",
				"ShippingHandlingSeparated" => "1",
				"ShippingOriginAddressLine1" => "100 Universal City Plaza",
				"ShippingOriginAddressLine2" => "",
				"ShippingOriginCity" => "Universal City",
				"ShippingOriginState" => "5",
				"ShippingOriginZip" => "91608",
			),
			$settings
		);
	}

	function setup_DB()
	{
		$this->db = $this->getMock('DB');

		$this->db->expects($this->any())
			->method('query')
			->will($this->returnValue(false));
	}

	function setup_OrderRepository()
	{
		$this->orderRepository = $this->getMock('DataAccess_OrderRepositoryInterface');


	}

	function setup_ShippingRepository()
	{
		$this->shippingRepository = $this->getMock('DataAccess_ShippingRepositoryInterface');

		$this->shippingRepository->expects($this->any())
			->method("getShippingOriginAddress")
			->with($this->greaterThan(0), $this->equalTo($this->settings))
			->will($this->returnValue($this->addressOrigin));

		$this->shippingRepository->expects($this->any())
			->method("getOrderShippingAddress")
			->with($this->greaterThan(0))
			->will($this->returnValue($this->orderShippingAddress));

		$this->shippingRepository->expects($this->any())
			->method("getOrderItems")
			->with($this->greaterThan(0))
			->will($this->returnCallback(array($this, "setup_ShippingRepositoryItems")));

		$this->shippingRepository->expects($this->any())
			->method("getOrderItemsWithShippingPrice")
			->with($this->equalTo(2222), $this->equalTo(77))
			->will($this->returnValue(array(
				array(
					"ocid" => 1979,
					"is_doba" => "No",
					"free_shipping" => "No",
					"product_type" => Model_Product::TANGIBLE,
					"product_is_shipping_price" => "No", 
					"product_shipping_price" => "1.47",
					"admin_price" => 22.77,
					"admin_quantity"=> 2,
					"product_id" => "pd-id-02", 
					"product_is_shipping_price" => "Yes",
					"weight" => 0.9
				)
			)));
	}

	/**
	 * Shipping repository items
	 * @return [type] [description]
	 */
	function setup_ShippingRepositoryItems()
	{
		$args = func_get_args();
		switch($args[0])
		{
			case 1111 : // plain
			{
				return array(array(
					"ocid" => 1978,
					"is_doba" => "No",
					"free_shipping" => "No",
					"product_type" => Model_Product::TANGIBLE,
					"product_is_shipping_price" => "No", 
					"product_shipping_price" => "0.00",
					"admin_price" => 11.37,
					"admin_quantity"=> 10,
					"product_id" => "pd-id-02", 
					"product_is_shipping_price" => "No",
					"weight" => 1.78
				));
			}
			case 5555 : // doba product
			{
				return array(
					array(
						"ocid" => 2005,
						"is_doba" => "Yes",
						"free_shipping" => "No",
						"product_type" => Model_Product::TANGIBLE,
						"product_is_shipping_price" => "No", 
						"product_shipping_price" => "0.00",
						"admin_price" => 2.44,
						"admin_quantity"=> 23,
						"product_id" => "pd-id-05-1", 
						"product_is_shipping_price" => "No",
						"weight" => 5.88
					),
					array(
						"ocid" => 2006,
						"is_doba" => "Yes",
						"free_shipping" => "No",
						"product_type" => Model_Product::TANGIBLE,
						"product_is_shipping_price" => "No", 
						"product_shipping_price" => "0.00",
						"admin_price" => 10.55,
						"admin_quantity"=> 11,
						"product_id" => "pd-id-05-2", 
						"product_is_shipping_price" => "No",
						"weight" => 2.45
					)
				);
			}
			case 7777 : // combined
			{
				return array(
					array(
						"ocid" => 1978,
						"is_doba" => "No",
						"free_shipping" => "No",
						"product_type" => Model_Product::TANGIBLE,
						"product_is_shipping_price" => "No", 
						"product_shipping_price" => "0.00",
						"admin_price" => 11.37,
						"admin_quantity"=> 10,
						"product_id" => "pd-id-02", 
						"product_is_shipping_price" => "No",
						"weight" => 1.78
					),
					array(
						"ocid" => 2007,
						"is_doba" => "No",
						"free_shipping" => "No",
						"product_type" => Model_Product::TANGIBLE,
						"product_is_shipping_price" => "No", 
						"product_shipping_price" => "0.00",
						"admin_price" => 1.37,
						"admin_quantity"=> 100,
						"product_id" => "pd-id-04", 
						"product_is_shipping_price" => "No",
						"weight" => 1.99
					),
					array(
						"ocid" => 2005,
						"is_doba" => "Yes",
						"free_shipping" => "No",
						"product_type" => Model_Product::TANGIBLE,
						"product_is_shipping_price" => "No", 
						"product_shipping_price" => "0.00",
						"admin_price" => 2.44,
						"admin_quantity"=> 23,
						"product_id" => "pd-id-05-1", 
						"product_is_shipping_price" => "No",
						"weight" => 5.88
					),
					array(
						"ocid" => 2006,
						"is_doba" => "Yes",
						"free_shipping" => "No",
						"product_type" => Model_Product::TANGIBLE,
						"product_is_shipping_price" => "No", 
						"product_shipping_price" => "0.00",
						"admin_price" => 10.55,
						"admin_quantity"=> 11,
						"product_id" => "pd-id-05-2", 
						"product_is_shipping_price" => "No",
						"weight" => 2.45
					)
				);
			}
		}
	}

	function setup_DobaOrderApi()
	{
		$this->dobaOrderApi = $this->getMock('Doba_OrderApiInterface');

		$this->dobaOrderApi->expects($this->any())
			->method('getShippingQuote')
			->with($this->equalTo($this->addressTo1), array(array("item_id"=>"pd-id-05-1", "quantity"=>23), array("item_id"=>"pd-id-05-2", "quantity"=>11)))
			->will($this->returnValue(77.88));
	}

	function setup_Currencies()
	{
		$this->currencies = $this->getMock('CurrenciesInterface');

		$this->currencies->expects($this->any())
			->method('getDefaultCurrency')
			->will($this->returnValue(array('code'=>'USD')));

		$this->currencies->expects($this->any())
			->method('getExchangeRates')
			->will($this->returnValue(array()));
	}

	public function setUp()
	{
		$this->setup_DB();
		
		$this->settings = $this->setup_settings();

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);

		$this->setup_OrderRepository();
		$this->setup_ShippingRepository();
		$this->setup_DobaOrderApi();
		$this->setup_Currencies();
	}

	/**
	 * Test reset 
	 */
	function test_can_reset_order_shipping()
	{
		$this->order->setShippingAmount(12.75);

		$calculator = new Calculator_Shipping(
			$this->settings, 
			$this->shippingRepository, 
			$this->orderRepository, 
			$this->currencies
		);

		$calculator->reset($this->order);

		$this->assertEquals(0, $this->order->getShippingAmount());
	}

	/**
	 * Test shipping
	 */
	function test_can_calculate_shipping_plain()
	{
		global $db;
		$db = $this->db;

		$this->order->setId(1111);

		/**
		 * Mock up shipping method
		 */
		$shippingMethod = $this->getMock("Shipping_MethodInterface");
		$shippingMethod->expects($this->any())
			->method('calculate')
			->with(
				$this->equalTo($db),
				$this->equalTo($this->addressOrigin),
				$this->equalTo($this->addressTo1),
				$this->equalTo(17.80), 
				$this->equalTo(10), 
				$this->equalTo(array(
					'before_discount'=>113.70,
					'after_discount'=>113.70
				)), 
				$this->equalTo(""), 
				$this->equalTo("USD"), 
				$this->equalTo(array())
			)
			->will($this->returnValue(10));

		/**
		 * Do calculations
		 * @var Calculator_Shipping
		 */
		$calculator = new Calculator_Shipping(
			$this->settings, 
			$this->shippingRepository, 
			$this->orderRepository, 
			$this->currencies
		);

		$calculator->calculate($this->db, $this->settings, $this->order, $shippingMethod, null, $error_message);

		$this->assertEquals(10, $this->order->getShippingAmount());
	}

	/**
	 * Test shipping with product level method
	 */
	function test_can_calculate_shipping_with_product_level_method()
	{
		global $db;
		$db = $this->db;

		$this->order->setId(2222);
		$this->order->setShippingCarrierId("custom");
		$this->order->setShippingMethodId("product_level");

		/**
		 * Setup shipping method
		 */
		$shippingMethod = $this->getMock("Shipping_MethodInterface");
		$shippingMethod->expects($this->any())
			->method('calculate')
			->with(
				$this->equalTo($db),
				$this->equalTo($this->addressOrigin),
				$this->equalTo($this->addressTo1),
				$this->equalTo(0),  // weight should be zero here because shipping price for these products will be calculated based on price
				$this->equalTo(0),  // as well as here
				$this->equalTo(array(
					'before_discount'=>0,
					'after_discount'=>0
				)), 
				$this->equalTo(""), 
				$this->equalTo("USD"), 
				$this->equalTo(array())
			)
			->will($this->returnValue(0));

		/**
		 * Do calculations
		 */
		$calculator = new Calculator_Shipping(
			$this->settings, 
			$this->shippingRepository, 
			$this->orderRepository, 
			$this->currencies
		);

		$product_level_method = array('ssid'=>77);

		$calculator->calculate($this->db, $this->settings, $this->order, $shippingMethod, $product_level_method, $error_message);

		$this->assertEquals(2.94, $this->order->getShippingAmount());
	}

	/**
	 * Test shipping with doba products
	 */
	function test_can_calculate_shipping_doba_amount()
	{
		global $db;
		$db = $this->db;

		$this->order->setId(5555);

		/**
		 * Mock for shipping method
		 */
		$shippingMethod = $this->getMock("Shipping_MethodInterface");
		$shippingMethod->expects($this->any())
			->method('calculate')
			->with(
				$this->equalTo($db),
				$this->equalTo($this->addressOrigin),
				$this->equalTo($this->addressTo1),
				$this->equalTo(0), 
				$this->equalTo(0), 
				$this->equalTo(array(
					'before_discount'=>0,
					'after_discount'=>0
				)), 
				$this->equalTo(""), 
				$this->equalTo("USD"), 
				$this->equalTo(array())
			)
			->will($this->returnValue(0));

		/**
		 * Do calculations
		 */
		$calculator = new Calculator_Shipping(
			$this->settings, 
			$this->shippingRepository, 
			$this->orderRepository, 
			$this->currencies
		);

		$calculator->calculate($this->db, $this->settings, $this->order, $shippingMethod, null, $error_message);

		$this->assertEquals(77.88, $this->order->getShippingAmount());
	}


	/**
	 * Test shipping with error message
	 */
	function test_can_calculate_shipping_error()
	{
		global $db;
		$db = $this->db;

		$this->order->setId(1111);

		/**
		 * Mock up shipping method
		 */
		$shippingMethod = new Shipping_MethodFakeErrorTest();
		/**
		 * Do calculations
		 * @var Calculator_Shipping
		 */
		$calculator = new Calculator_Shipping(
			$this->settings, 
			$this->shippingRepository, 
			$this->orderRepository, 
			$this->currencies
		);

		$calculator->calculate($this->db, $this->settings, $this->order, $shippingMethod, null, $error_message);

		$this->assertEquals(0, $this->order->getShippingAmount());
		$this->assertEquals("custom", $this->order->getShippingCarrierId());
		$this->assertEquals("disabled", $this->order->getShippingMethodId());
		$this->assertEquals("Some error message", $error_message);
	}

	/**
	 * Test shipping combined with doba
	 */
	function test_can_calculate_shipping_combined()
	{
		global $db;
		$db = $this->db;

		$this->order->setId(7777);

		/**
		 * Mock up shipping method
		 */
		$shippingMethod = $this->getMock("Shipping_MethodInterface");
		$shippingMethod->expects($this->any())
			->method('calculate')
			->with(
				$this->equalTo($db),
				$this->equalTo($this->addressOrigin),
				$this->equalTo($this->addressTo1),
				$this->equalTo(17.80), 
				$this->equalTo(10), 
				$this->equalTo(array(
					'before_discount'=>113.70,
					'after_discount'=>113.70
				)), 
				$this->equalTo(""), 
				$this->equalTo("USD"), 
				$this->equalTo(array())
			)
			->will($this->returnValue(10));

		/**
		 * Do calculations
		 * @var Calculator_Shipping
		 */
		$calculator = new Calculator_Shipping(
			$this->settings, 
			$this->shippingRepository, 
			$this->orderRepository, 
			$this->currencies
		);

		$calculator->calculate($this->db, $this->settings, $this->order, $shippingMethod, null, $error_message);

		$this->assertEquals(108.43, $this->order->getShippingAmount());
	}
}
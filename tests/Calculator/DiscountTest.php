<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_DiscountTest extends PHPUnit_Framework_TestCase
{
	protected $db;
	protected $settings;
	protected $user;
	protected $order;
	protected $orderRepository;
	protected $discountRepository;

	public function setUp()
	{
		$this->db = null;
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DiscountsActive' => 'YES',
			'DisplayPricesWithTax' => 'NO',
		);

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);

		$this->orderRepository = $this->getMock('DataAccess_OrderRepositoryInterface');
		$this->discountRepository = $this->getMock('DataAccess_DiscountRepositoryInterface');
	}

	function test_can_Reset_Discount_Properties()
	{
		$this->order->setDiscountAmount(5.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->reset($this->order);

		$this->assertEquals(0, $this->order->getDiscountAmount());
		$this->assertEquals('none', $this->order->getDiscountType());
		$this->assertEquals('0', $this->order->getDiscountValue());
	}

	function test_can_Compute_Five_Percent_Discount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$discount = array(
			'type' => 'percent',
			'discount' => '5',
		);
		$this->setGetActiveDiscountExpectation($this->discountRepository, 24.95, $discount);

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(1.25, $this->order->getDiscountAmount());
		$this->assertEquals('percent', $this->order->getDiscountType());
		$this->assertEquals('5', $this->order->getDiscountValue());
	}

	function test_can_Compute_Five_Amount_Discount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setDiscountAmount(1.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$discount = array(
			'type' => 'amount',
			'discount' => '5',
		);
		$this->setGetActiveDiscountExpectation($this->discountRepository, 24.95, $discount);

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(5, $this->order->getDiscountAmount());
		$this->assertEquals('amount', $this->order->getDiscountType());
		$this->assertEquals('5', $this->order->getDiscountValue());
	}

	function test_on_userLevel_change_will_reset_discount_values()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setDiscountAmount(1.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$this->order->setUserLevel(2);

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(0, $this->order->getDiscountAmount());
		$this->assertEquals('none', $this->order->getDiscountType());
		$this->assertEquals('0', $this->order->getDiscountValue());
	}

	function test_on_discount_deactivated_change_will_reset_discount_values()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setDiscountAmount(1.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$this->settings['DiscountsActive'] = 'NO';

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(0, $this->order->getDiscountAmount());
		$this->assertEquals('none', $this->order->getDiscountType());
		$this->assertEquals('0', $this->order->getDiscountValue());
	}

	function test_on_no_active_discount_will_reset_discount_values()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setDiscountAmount(1.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$this->setGetActiveDiscountExpectation($this->discountRepository, 24.95, null);

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(0, $this->order->getDiscountAmount());
		$this->assertEquals('none', $this->order->getDiscountType());
		$this->assertEquals('0', $this->order->getDiscountValue());
	}

	function test_on_discount_value_equal_to_0_and_type_percent_will_return_0_discount_amount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setDiscountAmount(1.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$discount = array(
			'type' => 'percent',
			'discount' => '0',
		);
		$this->setGetActiveDiscountExpectation($this->discountRepository, 24.95, $discount);

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(0, $this->order->getDiscountAmount());
		$this->assertEquals('percent', $this->order->getDiscountType());
		$this->assertEquals('0', $this->order->getDiscountValue());
	}

	function test_will_round_percent_discounts()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setDiscountAmount(1.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$discount = array(
			'type' => 'percent',
			'discount' => '33',
		);
		$this->setGetActiveDiscountExpectation($this->discountRepository, 24.95, $discount);

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(8.23, $this->order->getDiscountAmount());
		$this->assertEquals('percent', $this->order->getDiscountType());
		$this->assertEquals('33', $this->order->getDiscountValue());
	}

	function test_will_round_amount_discounts()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setDiscountAmount(1.25);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('percent');

		$discount = array(
			'type' => 'amount',
			'discount' => '5.7858',
		);
		$this->setGetActiveDiscountExpectation($this->discountRepository, 24.95, $discount);

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, true);

		$this->assertEquals(5.79, $this->order->getDiscountAmount());
		$this->assertEquals('amount', $this->order->getDiscountType());
		$this->assertEquals('5.7858', $this->order->getDiscountValue());
	}

	function test_can_Compute_Discount_Without_Reset()
	{
		$this->_addItem_To_Order(24.95, 1, 5);

		$this->order->setDiscountAmount(5);
		$this->order->setDiscountValue(5);
		$this->order->setDiscountType('amount');

		$discountCalculator = $this->getDiscountCalculator();

		$discountCalculator->calculate($this->order, false);

		$this->assertEquals(5, $this->order->getDiscountAmount());
		$this->assertEquals('amount', $this->order->getDiscountType());
		$this->assertEquals('5', $this->order->getDiscountValue());
	}

	protected function getDiscountCalculator()
	{
		return new Calculator_Discount($this->settings, $this->orderRepository, $this->discountRepository);
	}

	protected function _addItem_To_Order($productPrice, $quantity, $discountAmount = 0, $promoDiscountAmount = 0, $discountAmountWithTax = 0, $promoDiscountAmountWithTax = 0)
	{
		$values = array(
			'product_total' => $productPrice * $quantity,
			'product_id' => 'test_prod',
			'product_price' => $productPrice,
			'admin_quantity' => $quantity,
			'ocid' => count($this->order->lineItems) + 1,
			'discount_amount' => $discountAmount,
			'discount_amount_with_tax' => $discountAmountWithTax,
			'promo_discount_amount' => $promoDiscountAmount,
			'promo_discount_amount_with_tax' => $promoDiscountAmountWithTax,
			'is_gift' => 'No',
			'quantity' => $quantity,
			'product_sub_id' => '',
			'product_url' => '',
			'cat_url' => '',
			'cid' => 1,
			'pid' => 1,
			'inventory_control' => 'No',
			'options' => '',
			'options_clean' => '',
			'weight' => 0,
			'price' => $productPrice,
			'admin_price' => $productPrice,
			'is_taxable' => 'Yes',
			'tax_rate' => 0,
		);

		$this->order->items[] = $values;
		$this->order->lineItems[$values['ocid']] = new Model_LineItem($values, $productPrice, $this->settings);
	}

	protected function setGetActiveDiscountExpectation($discountRepositoryMock, $subtotalAmount, $discount = false)
	{
		$discountRepositoryMock->expects($this->once())
			->method('getActiveDiscount')
			->with($this->equalTo($subtotalAmount))
			->will($this->returnValue($discount));
	}
}

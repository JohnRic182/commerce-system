<?php

use \Mockery as m;

require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_user.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_order.php';
require_once dirname(dirname(__FILE__)).'/_init.php';

class Calculator_CalculatorsTest extends PHPUnit_Framework_TestCase
{
	public function tearDown()
	{
		\Mockery::close();

		Calculator_Discount::setInstance(null);
		Calculator_Handling::setInstance(null);
		Calculator_HandlingTax::setInstance(null);
		Calculator_PromoDiscount::setInstance(null);
		Calculator_Shipping::setInstance(null);
		Calculator_ShippingTax::setInstance(null);
		Calculator_Subtotal::setInstance(null);
		Calculator_Tax::setInstance(null);
		Calculator_Total::setInstance(null);
	}

	protected $db;
	protected $settings;
	protected $user;
	/** @var  ORDER $order */
	protected $order;
	/** @var  \Mockery\Mock */
	protected $orderRepository;
	/** @var  \Mockery\Mock */
	protected $promoCodeRepository;
	/** @var  \Mockery\Mock */
	protected $discountRepository;
	/** @var  \Mockery\Mock */
	protected $taxRates;
	/** @var  \Mockery\Mock */
	protected $shippingRepository;
	/** @var  \Mockery\Mock */
	protected $dobaApi;
	/** @var  \Mockery\Mock */
	protected $currencies;
	/** @var  \Mockery\Mock */
	protected $shippingMethod;

	public function setUp()
	{
		$this->db = m::mock('DB');
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DiscountsPromo' => 'YES',
			'DisplayPricesWithTax' => 'NO',
			'DiscountsActive' => 'YES',
			'TaxDefaultCountry' => '1',
			'TaxDefaultState' => '3',
			'ShippingHandlingFee' => '0',
			'ShippingHandlingFeeType' => '',
			'ShippingHandlingFeeWhenFreeShipping' => '',
			'ShippingHandlingSeparated' => '',
			'ShippingTaxable' => 'YES',
			'ShippingTaxClassId' => '1',
			'ShippingHandlingTaxable' => 'NO',
			'ShippingHandlingTaxClassId' => '1',
			'ShippingCalcEnabled' => 'NO',
		);

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);

		$this->orderRepository = m::mock('DataAccess_OrderRepositoryInterface');
		$this->promoCodeRepository = m::mock('DataAccess_PromoCodeRepositoryInterface');
		$this->discountRepository = m::mock('DataAccess_DiscountRepositoryInterface');
		$this->shippingRepository = m::mock('DataAccess_ShippingRepositoryInterface');
		$this->dobaApi = m::mock('Doba_OrderApiInterface');
		$this->currencies = m::mock('CurrenciesInterface');
		$this->shippingMethod = m::mock('Shipping_MethodInterface');
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->andReturn(false);
	}

	function test_discount_vat_rounding()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(true);

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$taxRate = new Model_TaxRate(1, .094);
		$this->taxRates->shouldReceive('hasTaxRate')->times(1)->with(1)->andReturn(true);
		$this->taxRates->shouldReceive('getTaxRate')->with(1, '')->andReturn(9.4);
		$this->taxRates->shouldReceive('getTaxRateDescription')->with(1)->andReturn('');
		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array(1 => $taxRate));

		$discount = array();
		$this->discountRepository->shouldReceive('getActiveDiscount')->once()->with(1299)->andReturn($discount);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->atLeast(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->twice()->with(1, 1, 0, 0, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(3)->with(1, 1, 0, 0, 649.5, 710.56);
		$this->orderRepository->shouldReceive('resetLineItemShippingPrices')->times(2)->with(1);

		$promoCode = array(
			'pid' => 1,
			'promo_type' => 'Global',
			'discount_type' => 'percent',
			'discount' => '50',
			'min_amount' => '0',
		);
		$this->promoCodeRepository->shouldReceive('getPromoCodeByPromoCampaignId')->twice()->with(1)->andReturn($promoCode);
		$this->order->setPromoCampaignId(1);

		$taxProvider = $this->getCustomProvider();
		$taxCalculator = new Calculator_Tax($taxProvider);

		Calculator_Tax::setInstance($taxCalculator);

		$this->setup_addItem_To_Order(1299, 1, 1421.11, 9.4);

		Shipping_MethodFactory::setMethod('', $this->shippingMethod);

		$this->order->setOrderRepository($this->orderRepository);
		$shippingAddress = array(
			'shipping_address_type' => 'Residential',
			'shipping_name' => 'Name',
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => '',
			'shipping_city' => 'Phoenix',
			'state_name' => 'Arizona',
			'state_abbr' => 'AZ',
			'shipping_country' => '1',
			'country_id' => '1',
			'country_name' => 'United States',
			'iso_a2' => 'US',
			'iso_a3' => 'USA',
			'iso_number' => '111',
			'shipping_zip' => '85020',
		);
		$this->order->setShippingAddress($shippingAddress);
		$this->shippingRepository->shouldReceive('getOrderItems')->once()->with(1)->andReturn($this->order->items);
		$this->shippingRepository->shouldReceive('getShippingOriginAddress')->once()->andReturn(array());
		$this->shippingRepository->shouldReceive('getOrderShippingAddress')->once()->andReturn($shippingAddress);

		$this->orderRepository->shouldReceive('persistOrderData')->twice();

		$this->currencies->shouldReceive('getDefaultCurrency')->once()->andReturn(array('code' => 'USD'));
		$this->currencies->shouldReceive('getExchangeRates')->once()->andReturn(array());

		$this->shippingMethod->shouldReceive('calculate')->once()->andReturn(1);

		$subTotalCalculator = new Calculator_Subtotal($this->orderRepository);
		$discountCalculator = $this->getDiscountCalculator();
		$promoCalculator = $this->getPromoDiscountCalculator();
		$totalCalculator = new Calculator_Total($this->settings);
		$shippingCalculator = new Calculator_Shipping($this->settings, $this->shippingRepository,
			$this->orderRepository, $this->dobaApi, $this->currencies);
		$shippingTaxCalculator = new Calculator_ShippingTax($this->settings, $taxProvider);

		Calculator_Subtotal::setInstance($subTotalCalculator);
		Calculator_Discount::setInstance($discountCalculator);
		Calculator_PromoDiscount::setInstance($promoCalculator);
		Calculator_Total::setInstance($totalCalculator);
		Calculator_Shipping::setInstance($shippingCalculator);
		Calculator_ShippingTax::setInstance($shippingTaxCalculator);

		$this->order->recalcSubtotals();

		$shippingErrorMessage = '';
		$this->order->recalcTotals($shippingErrorMessage, true);

		/** @var Model_LineItem $lineItem */
		$lineItem = $this->order->lineItems[1];
		$this->assertEquals(61.05, $lineItem->getTaxAmount());
		$this->assertEquals(1299, $this->order->getSubtotalAmount());
		$this->assertEquals(1421.11, $this->order->getSubtotalAmountWithTax());
		$this->assertEquals(649.5, $this->order->getPromoDiscountAmount());
		$this->assertEquals(710.56, $this->order->getPromoDiscountAmountWithTax());
		$this->assertEquals(1, $this->order->getShippingAmount());
		$this->assertEquals(0.09, $this->order->getShippingTaxAmount());
		$this->assertEquals(61.14, $this->order->getTaxAmount());
		$this->assertEquals(711.64, $this->order->getTotalAmount());
	}

	function test_applying_discount_and_promo_with_vat_taxes_enabled_calculates_correct_amounts()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(true);

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		$discount = array(
			'type' => 'percent',
			'discount' => '10',
		);
		$this->discountRepository->shouldReceive('getActiveDiscount')->once()->with(24.95)->andReturn($discount);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->atLeast(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 0, 0, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->twice()->with(1, 1, 2.5, 2.75, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(3)->with(1, 1, 2.5, 2.75, 2.5, 2.75);
		$this->orderRepository->shouldReceive('resetLineItemShippingPrices')->twice()->with(1);

		$promoCode = array(
			'pid' => 1,
			'promo_type' => 'Global',
			'discount_type' => 'percent',
			'discount' => '10',
			'min_amount' => '0',
		);
		$this->promoCodeRepository->shouldReceive('getPromoCodeByPromoCampaignId')->twice()->with(1)->andReturn($promoCode);
		$this->order->setPromoCampaignId(1);

		$taxCalculator = new Calculator_Tax($this->getCustomProvider());

		Calculator_Tax::setInstance($taxCalculator);

		$this->setup_addItem_To_Order(24.95, 1, 27.45, 10);

		Shipping_MethodFactory::setMethod('', $this->shippingMethod);

		$this->order->setOrderRepository($this->orderRepository);
		$shippingAddress = array(
			'shipping_address_type' => 'Residential',
			'shipping_name' => 'Name',
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => '',
			'shipping_city' => 'Phoenix',
			'state_name' => 'Arizona',
			'state_abbr' => 'AZ',
			'shipping_country' => '1',
			'country_id' => '1',
			'country_name' => 'United States',
			'iso_a2' => 'US',
			'iso_a3' => 'USA',
			'iso_number' => '111',
			'shipping_zip' => '85020',
		);
		$this->order->setShippingAddress($shippingAddress);
		$this->shippingRepository->shouldReceive('getOrderItems')->once()->with(1)->andReturn($this->order->items);
		$this->shippingRepository->shouldReceive('getShippingOriginAddress')->once()->andReturn(array());
		$this->shippingRepository->shouldReceive('getOrderShippingAddress')->once()->andReturn($shippingAddress);

		$this->orderRepository->shouldReceive('persistOrderData')->twice();

		$this->currencies->shouldReceive('getDefaultCurrency')->once()->andReturn(array('code' => 'USD'));
		$this->currencies->shouldReceive('getExchangeRates')->once()->andReturn(array());

		$this->shippingMethod->shouldReceive('calculate')->once()->andReturn(1);

		$subTotalCalculator = new Calculator_Subtotal($this->orderRepository);
		$discountCalculator = $this->getDiscountCalculator();
		$promoCalculator = $this->getPromoDiscountCalculator();
		$totalCalculator = new Calculator_Total($this->settings);
		$shippingCalculator = new Calculator_Shipping($this->settings, $this->shippingRepository,
		$this->orderRepository, $this->dobaApi, $this->currencies);

		Calculator_Subtotal::setInstance($subTotalCalculator);
		Calculator_Discount::setInstance($discountCalculator);
		Calculator_PromoDiscount::setInstance($promoCalculator);
		Calculator_Total::setInstance($totalCalculator);
		Calculator_Shipping::setInstance($shippingCalculator);

		$this->order->recalcSubtotals();

		$shippingErrorMessage = '';
		$this->order->recalcTotals($shippingErrorMessage, true);

		$this->assertEquals(2.0, $this->order->getTaxAmount());
		$this->assertEquals(22.95, $this->order->getTotalAmount());
	}

	function test_applying_shipping__promo_with_vat_taxes_enabled_calculates_correct_amounts()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(true);

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		$discount = false;
		$this->discountRepository->shouldReceive('getActiveDiscount')->once()->with(74.85)->andReturn($discount);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->atLeast(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 0, 0, 0, 0);

		$promoCode = array(
			'pid' => 1,
			'promo_type' => 'Shipping',
			'discount_type' => 'amount',
			'discount' => '1',
			'min_amount' => '0',
		);
		$this->promoCodeRepository->shouldReceive('getPromoCodeByPromoCampaignId')->once()->with(1)->andReturn($promoCode);
		$this->order->setPromoCampaignId(1);

		$taxCalculator = new Calculator_Tax($this->getCustomProvider());

		$this->setup_addItem_To_Order(24.95, 3, 27.45, 10);

		$subTotalCalculator = new Calculator_Subtotal($this->orderRepository);
		$discountCalculator = $this->getDiscountCalculator();
		$promoCalculator = $this->getPromoDiscountCalculator();
		$totalCalculator = new Calculator_Total($this->settings);

		$subTotalCalculator->reset($this->order);
		$discountCalculator->reset($this->order);
		$promoCalculator->reset($this->order, true);
		$taxCalculator->reset($this->order);
		$totalCalculator->reset($this->order);

		$subTotalCalculator->calculate($this->order, true, false);
		$discountCalculator->calculate($this->order);

		$this->order->setShippingAmount(3.00);
		$this->order->setShippingTaxRate(10);
		$this->order->setShippingTaxable(true);
		$this->order->setShippingTaxAmount(0.3);

		$this->order->setHandlingAmount(1);
		$this->order->setHandlingTaxable(true);
		$this->order->setHandlingFee(1);
		$this->order->setHandlingTaxRate(10);
		$this->order->setHandlingTaxAmount(.1);
		$this->order->setHandlingSeparated(true);

		$promoCalculator->calculate($this->order);
		$taxCalculator->calculate($this->order);
		$totalCalculator->calculate($this->order);

		$this->assertEquals(74.85, $this->order->getSubtotalAmount());
		$this->assertEquals(82.35, $this->order->getSubtotalAmountWithTax());
		$this->assertEquals(7.8, $this->order->getTaxAmount());
		$this->assertEquals(1, $this->order->getPromoDiscountAmount());
		$this->assertEquals(1.1, $this->order->getPromoDiscountAmountWithTax());

		$this->assertEquals(85.65, $this->order->getTotalAmount());
	}

	function test_applying_shipping_promo_with_vat_taxes_enabled_calculates_correct_amounts_with_promo_more_than_shipping()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(true);

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		$discount = false;
		$this->discountRepository->shouldReceive('getActiveDiscount')->once()->with(74.85)->andReturn($discount);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->atLeast(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 0, 0, 0, 0);

		$promoCode = array(
			'pid' => 1,
			'promo_type' => 'Shipping',
			'discount_type' => 'amount',
			'discount' => '10',
			'min_amount' => '0',
		);
		$this->promoCodeRepository->shouldReceive('getPromoCodeByPromoCampaignId')->once()->with(1)->andReturn($promoCode);
		$this->order->setPromoCampaignId(1);

		$taxCalculator = new Calculator_Tax($this->getCustomProvider());

		$this->setup_addItem_To_Order(24.95, 3, 27.45, 10);

		$subTotalCalculator = new Calculator_Subtotal($this->orderRepository);
		$discountCalculator = $this->getDiscountCalculator();
		$promoCalculator = $this->getPromoDiscountCalculator();
		$totalCalculator = new Calculator_Total($this->settings);

		$subTotalCalculator->reset($this->order);
		$discountCalculator->reset($this->order);
		$promoCalculator->reset($this->order, true);
		$taxCalculator->reset($this->order);
		$totalCalculator->reset($this->order);

		$subTotalCalculator->calculate($this->order, true, false);
		$discountCalculator->calculate($this->order);

		$this->order->setShippingAmount(3.00);
		$this->order->setShippingTaxRate(10);
		$this->order->setShippingTaxable(true);
		$this->order->setShippingTaxAmount(0.3);

		$this->order->setHandlingAmount(1);
		$this->order->setHandlingTaxable(true);
		$this->order->setHandlingFee(1);
		$this->order->setHandlingTaxRate(10);
		$this->order->setHandlingTaxAmount(.1);
		$this->order->setHandlingSeparated(true);

		$promoCalculator->calculate($this->order);
		$taxCalculator->calculate($this->order);
		$promoCalculator->calculate($this->order, false, true);
		$totalCalculator->calculate($this->order);

		$this->assertEquals(74.85, $this->order->getSubtotalAmount());
		$this->assertEquals(82.35, $this->order->getSubtotalAmountWithTax());
		$this->assertEquals(7.6, $this->order->getTaxAmount());
		$this->assertEquals(3, $this->order->getPromoDiscountAmount());
		$this->assertEquals(3.3, $this->order->getPromoDiscountAmountWithTax());

		$this->assertEquals(83.45, $this->order->getTotalAmount());
	}

	function test_applying_discount_and_promo_50_usd_with_taxes_enabled_calculates_correct_amounts()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(false);

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		$discount = array(
			'type' => 'percent',
			'discount' => '10',
		);
		$this->discountRepository->shouldReceive('getActiveDiscount')->once()->with(24.95)->andReturn($discount);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->atLeast(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 0, 0, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 2.5, 2.75, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 2.5, 2.75, 24.95, 27.45);

		$promoCode = array(
			'pid' => 1,
			'promo_type' => 'Global',
			'discount_type' => 'amount',
			'discount' => '50',
			'min_amount' => '0',
		);
		$this->promoCodeRepository->shouldReceive('getPromoCodeByPromoCampaignId')->once()->with(1)->andReturn($promoCode);
		$this->order->setPromoCampaignId(1);

		$taxCalculator = new Calculator_Tax($this->getCustomProvider());

		$this->setup_addItem_To_Order(24.95, 1, 27.45, 10);

		$subTotalCalculator = new Calculator_Subtotal($this->orderRepository);
		$discountCalculator = $this->getDiscountCalculator();
		$promoCalculator = $this->getPromoDiscountCalculator();
		$totalCalculator = new Calculator_Total($this->settings);

		$subTotalCalculator->reset($this->order);
		$discountCalculator->reset($this->order);
		$promoCalculator->reset($this->order, true);
		$taxCalculator->reset($this->order);
		$totalCalculator->reset($this->order);

		$subTotalCalculator->calculate($this->order, true, false);
		$discountCalculator->calculate($this->order);

		$this->order->setShippingAmount(1.00);

		$discountCalculator->calculate($this->order, false, true);
		$promoCalculator->calculate($this->order);
		$taxCalculator->calculate($this->order);
		$totalCalculator->calculate($this->order);

		$this->assertEquals(1, $this->order->getTotalAmount());
	}

	function test_applying_discount_50_usd_and_promo_50_usd_with_taxes_enabled_calculates_correct_amounts()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(false);

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		$discount = array(
			'type' => 'amount',
			'discount' => '50',
		);
		$this->discountRepository->shouldReceive('getActiveDiscount')->once()->with(24.95)->andReturn($discount);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->atLeast(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 0, 0, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 24.95, 27.45, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 24.95, 27.45, 24.95, 27.45);

		$promoCode = array(
			'pid' => 1,
			'promo_type' => 'Global',
			'discount_type' => 'amount',
			'discount' => '50',
			'min_amount' => '0',
		);
		$this->promoCodeRepository->shouldReceive('getPromoCodeByPromoCampaignId')->once()->with(1)->andReturn($promoCode);
		$this->order->setPromoCampaignId(1);

		$taxCalculator = new Calculator_Tax($this->getCustomProvider());

		$this->setup_addItem_To_Order(24.95, 1, 27.45, 10);

		$subTotalCalculator = new Calculator_Subtotal($this->orderRepository);
		$discountCalculator = $this->getDiscountCalculator();
		$promoCalculator = $this->getPromoDiscountCalculator();
		$totalCalculator = new Calculator_Total($this->settings);

		$subTotalCalculator->reset($this->order);
		$discountCalculator->reset($this->order);
		$promoCalculator->reset($this->order, true);
		$taxCalculator->reset($this->order);
		$totalCalculator->reset($this->order);

		$subTotalCalculator->calculate($this->order, true, false);
		$discountCalculator->calculate($this->order);

		$this->order->setShippingAmount(1.00);

		$discountCalculator->calculate($this->order, false, true);
		$promoCalculator->calculate($this->order);
		$taxCalculator->calculate($this->order);
		$totalCalculator->calculate($this->order);

		$this->assertEquals(1, $this->order->getTotalAmount());
	}

	function test_applying_discount_and_promo_10_usd_with_taxes_enabled_calculates_correct_amounts()
	{
		$this->taxRates = m::mock('TaxRatesInterface');
		$this->taxRates->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(false);

		$this->settings['TaxDefaultCountry'] = 1;
		$this->settings['TaxDefaultState'] = 3;

		$this->taxRates->shouldReceive('getTaxRates')->times(1)->with(1,3,0)->andReturn(array());

		$discount = array(
			'type' => 'percent',
			'discount' => '10',
		);
		$this->discountRepository->shouldReceive('getActiveDiscount')->once()->with(24.95)->andReturn($discount);

		$this->orderRepository->shouldReceive('resetLineItemDiscounts')->atLeast(1);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 0, 0, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 2.5, 2.75, 0, 0);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 2.5, 2.75, 10, 10);
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->once()->with(1, 1, 2.5, 2.75, 10, 11);

		$promoCode = array(
			'pid' => 1,
			'promo_type' => 'Global',
			'discount_type' => 'amount',
			'discount' => '10',
			'min_amount' => '0',
		);
		$this->promoCodeRepository->shouldReceive('getPromoCodeByPromoCampaignId')->once()->with(1)->andReturn($promoCode);
		$this->order->setPromoCampaignId(1);

		$taxCalculator = new Calculator_Tax($this->getCustomProvider());

		$this->setup_addItem_To_Order(24.95, 1, 27.45, 10);

		$subTotalCalculator = new Calculator_Subtotal($this->orderRepository);
		$discountCalculator = $this->getDiscountCalculator();
		$promoCalculator = $this->getPromoDiscountCalculator();
		$totalCalculator = new Calculator_Total($this->settings);

		$subTotalCalculator->reset($this->order);
		$discountCalculator->reset($this->order);
		$promoCalculator->reset($this->order, true);
		$taxCalculator->reset($this->order);
		$totalCalculator->reset($this->order);

		$subTotalCalculator->calculate($this->order, true, false);
		$discountCalculator->calculate($this->order);

		$this->order->setShippingAmount(1.00);

		$promoCalculator->calculate($this->order);
		$taxCalculator->calculate($this->order);

		$discountCalculator->calculate($this->order, false, true);
		$promoCalculator->calculate($this->order, false, true);
		$totalCalculator->calculate($this->order);

		$this->assertEquals(14.70, $this->order->getTotalAmount());
		$this->assertEquals(11, $this->order->getPromoDiscountAmountWithTax());
		$this->assertEquals(10, $this->order->getPromoDiscountAmount());
	}

	function setup_addItem_To_Order($productPrice, $quantity, $productPriceWithTax = false, $taxRate = 0, $discountAmount = 0, $promoDiscountAmount = 0, $discountAmountWithTax = 0, $promoDiscountAmountWithTax = 0)
	{
		$values = array(
			'product_total' => $productPrice * $quantity,
			'product_id' => 'test_prod',
			'product_price' => $productPrice,
			'price' => $productPrice,
			'admin_price' => $productPrice,
			'quantity' => $quantity,
			'admin_quantity' => $quantity,
			'ocid' => count($this->order->lineItems) + 1,
			'discount_amount' => $discountAmount,
			'discount_amount_with_tax' => $discountAmountWithTax,
			'promo_discount_amount' => $promoDiscountAmount,
			'promo_discount_amount_with_tax' => $promoDiscountAmountWithTax,
			'is_gift' => 'No',
			'product_sub_id' => '',
			'product_url' => '',
			'cat_url' => '',
			'cid' => 1,
			'pid' => 1,
			'inventory_control' => 'No',
			'options' => '',
			'options_clean' => '',
			'weight' => 0,
			'is_taxable' => 'Yes',
			'price_withtax' => $productPriceWithTax !== false ? $productPriceWithTax : $productPrice,
			'tax_rate' => $taxRate,
			'is_doba' => 'No',
			'product_type' => Model_Product::TANGIBLE,
			'free_shipping' => 'No',
			'product_is_shipping_price' => 'No',
		);

		$this->order->items[] = $values;
		$this->order->lineItems[$values['ocid']] = new Model_LineItem($values, $productPrice, $this->settings);
	}

	protected function getCustomProvider()
	{
		return new Tax_CustomProvider($this->orderRepository, $this->settings, $this->taxRates);
	}

	protected function getDiscountCalculator()
	{
		return new Calculator_Discount($this->settings, $this->orderRepository, $this->discountRepository);
	}

	protected function getPromoDiscountCalculator()
	{
		return new Calculator_PromoDiscount($this->orderRepository, $this->promoCodeRepository, $this->settings);
	}
}
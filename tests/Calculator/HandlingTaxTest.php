<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_HandlingTaxTest extends PHPUnit_Framework_TestCase
{
	function setup_settings($settings = array())
	{
		return array_merge(
			array(
				'SecurityCookiesPrefix' => '',
				'enable_gift_cert' => 'No',
				'ShippingHandlingFee' => '1',
				'ShippingHandlingFeeType' => 'amount',
				'ShippingHandlingFeeWhenFreeShipping' => '0',
				'ShippingHandlingText' => 'Handling text',
				'ShippingHandlingSeparated' => '0',
				'ShippingHandlingTaxable' => '0',
				'ShippingHandlingTaxClassId' => '0'
			),
			$settings
		);
	}

	function test_reset_For_Correct_Values()
	{
		$settings = $this->setup_settings();

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setHandlingTaxable(true);
		$order->setHandlingTaxClassId(1);
		$order->setHandlingTaxRate(12);
		$order->setHandlingTaxAmount(122);
		$order->setHandlingTaxDescription('some other text');

		$taxProvider = $this->getMock('Tax_ProviderInterface');

		$handlingTaxCalculator = new Calculator_HandlingTax($settings, $taxProvider);

		$handlingTaxCalculator->reset($order);

		$this->assertEquals(false, $order->getHandlingTaxable());
		$this->assertEquals(0, $order->getHandlingTaxClassId());
		$this->assertEquals(0, $order->getHandlingTaxRate());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals('', $order->getHandlingTaxDescription());
	}

	function test_can_Cancel_Taxable_When_Handling_Amount_Zero()
	{
		$settings = $this->setup_settings(array(
			'ShippingHandlingFee' => '10',
			'ShippingHandlingFeeType' => 'percent',
			'ShippingHandlingTaxable' => '1',
			'ShippingHandlingTaxClassId' => '1'
		));
		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$classId = $settings['ShippingHandlingTaxClassId'];
		$taxRate = 9.37;

		$order->setHandlingAmount(0);
		$order->setHandlingSeparated(true);

		$taxProvider = $this->getMock('Tax_ProviderInterface');

		$handlingTaxCalculator = new Calculator_HandlingTax($settings, $taxProvider);

		$handlingTaxCalculator->calculate($order);

		$this->assertEquals(true, $order->getHandlingTaxable());
		$this->assertEquals($classId, $order->getHandlingTaxClassId());
		$this->assertEquals(0, $order->getHandlingTaxRate());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals('', $order->getHandlingTaxDescription());
	}

	function test_can_Compute_Handling_Tax_When_Separated()
	{
		$settings = $this->setup_settings(array(
			'ShippingHandlingFee' => '10',
			'ShippingHandlingFeeType' => 'percent',
			'ShippingHandlingTaxable' => '1',
			'ShippingHandlingTaxClassId' => '1'
		));
		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$classId = $settings['ShippingHandlingTaxClassId'];
		$taxRate = 9.37;

		$order->setHandlingAmount(10);
		$order->setHandlingSeparated(true);

		$taxProvider = $this->getMock('Tax_ProviderInterface');
		$taxProvider
			->expects($this->once())
			->method('hasTaxRate')
			->with($this->equalTo($classId))
			->will($this->returnValue(true));

		$taxProvider
			->expects($this->once())
			->method('getTaxRate')
			->with($this->equalTo($classId))
			->will($this->returnValue($taxRate));

		$taxProvider
			->expects($this->once())
			->method('getTaxRateDescription')
			->with($this->equalTo($classId))
			->will($this->returnValue('Tax description'));

		$handlingTaxCalculator = new Calculator_HandlingTax($settings, $taxProvider);

		$handlingTaxCalculator->calculate($order);

		$this->assertEquals(true, $order->getHandlingTaxable());
		$this->assertEquals($classId, $order->getHandlingTaxClassId());
		$this->assertEquals($taxRate, $order->getHandlingTaxRate());
		$this->assertEquals(0.94, $order->getHandlingTaxAmount());
		$this->assertEquals('Tax description', $order->getHandlingTaxDescription());
	}

	function test_can_When_Handling_Included_In_Shipping_Should_Be_No_Handling_Tax()
	{
		$settings = $this->setup_settings(array(
			'ShippingHandlingFee' => '10',
			'ShippingHandlingFeeType' => 'percent',
			'ShippingHandlingTaxable' => '1',
			'ShippingHandlingTaxClassId' => '1',
			'ShippingTaxable' => 1,
			'ShippingTaxRateId' => 12
		));
		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new OrderFake($db, $settings, $user, 1); // note, use fake here to override shipping tax rates calculations

		$classId = $settings['ShippingHandlingTaxClassId'];
		$taxRate = 9.37;

		$order->setShippingTaxable(true);
		$order->setShippingTaxClassId(77);
		$order->setShippingTaxRate(15.87);
		$order->setShippingTaxDescription("Shipping tax description");

		$order->setHandlingAmount(10);
		$order->setHandlingSeparated(false);

		$taxProvider = $this->getMock('Tax_ProviderInterface');

		$handlingTaxCalculator = new Calculator_HandlingTax($settings, $taxProvider);

		$handlingTaxCalculator->calculate($order);

		$this->assertEquals(false, $order->getHandlingTaxable());
		$this->assertEquals(0, $order->getHandlingTaxClassId());
		$this->assertEquals(0, $order->getHandlingTaxRate());
		$this->assertEquals(0, $order->getHandlingTaxAmount());
		$this->assertEquals('', $order->getHandlingTaxDescription());
	}
}

class OrderFake extends ORDER
{
	public function setShippingTaxRate($shippingTaxRate)
	{
		$this->shippingTaxRate = $shippingTaxRate;
	}

	public function getShippingTaxRate()
	{
		return $this->shippingTaxRate;
	}
}
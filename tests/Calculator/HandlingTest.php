<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_HandlingTest extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		global $msg;

		$msg = array('opc' => array('invoice_handling' => 'Handling text'));
	}

	function setup_settings($settings = array())
	{
		return array_merge(
			array(
				'SecurityCookiesPrefix' => '',
				'enable_gift_cert' => 'No',
				'ShippingHandlingFee' => '1',
				'ShippingHandlingFeeType' => 'amount',
				'ShippingHandlingFeeWhenFreeShipping' => '0',
				'ShippingHandlingText' => 'Handling text',
				'ShippingHandlingSeparated' => '0',
			),
			$settings
		);
	}

	function test_reset_For_Correct_Values()
	{
		$settings = $this->setup_settings();

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setHandlingSeparated(true);
		$order->setHandlingFee(1);
		$order->setHandlingFeeType('amount');
		$order->setHandlingText('some random text');
		$order->setHandlingAmount(10);

		$handlingCalculator = new Calculator_Handling($settings, null);

		$handlingCalculator->reset($order);

		$this->assertEquals(false, $order->getHandlingSeparated());
		$this->assertEquals(0, $order->getHandlingFee());
		$this->assertEquals('percent', $order->getHandlingFeeType());
		$this->assertEquals('', $order->getHandlingText());
		$this->assertEquals(0, $order->getHandlingAmount());
		$this->assertEquals(false, $order->getHandlingTaxable());

	}

	function test_can_Compute_1_Dollar_Handling()
	{
		$settings = $this->setup_settings(); // set by default

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$handlingCalculator = new Calculator_Handling($settings, null);

		$handlingCalculator->calculate($order, false, true, true);

		$this->assertEquals(1, $order->getHandlingFee());
		$this->assertEquals('amount', $order->getHandlingFeeType());
		$this->assertEquals('Handling text', $order->getHandlingText());
		$this->assertEquals(1, $order->getHandlingAmount());
	}

	function test_can_Compute_10_Percent_Handling()
	{
		$settings = $this->setup_settings(array(
			'ShippingHandlingFee' => '10',
			'ShippingHandlingFeeType' => 'percent',
		)); 
		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setSubtotalAmount(39.97);

		$handlingCalculator = new Calculator_Handling($settings, null);

		$handlingCalculator->calculate($order, false, true, true);

		$this->assertEquals(10, $order->getHandlingFee());
		$this->assertEquals('percent', $order->getHandlingFeeType());
		$this->assertEquals('Handling text', $order->getHandlingText());
		$this->assertEquals(4, $order->getHandlingAmount());
	}

	function test_can_Compute_Handling_When_Shipping_Free()
	{
		$settings = $this->setup_settings(array(
			'ShippingHandlingFee' => '10',
			'ShippingHandlingFeeType' => 'percent',
			'ShippingHandlingFeeWhenFreeShipping' => '20',
		));
		
		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setSubtotalAmount(39.97);

		$handlingCalculator = new Calculator_Handling($settings, null);

		$handlingCalculator->calculate($order, true, true, true);

		$this->assertEquals(20, $order->getHandlingFee());
		$this->assertEquals('amount', $order->getHandlingFeeType());
		$this->assertEquals('Handling text', $order->getHandlingText());
		$this->assertEquals(20, $order->getHandlingAmount());
		$this->assertEquals(true, $order->getHandlingSeparated());
	}

	function test_can_Compute_Handling_When_Order_Has_Discounts()
	{
		$settings = $this->setup_settings(array(
			'ShippingHandlingFee' => '11.02',
			'ShippingHandlingFeeType' => 'percent'
		));

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setSubtotalAmount(39.97);
		$order->setDiscountAmount(10.11);
		$order->setPromoDiscountAmount(12.13);

		$handlingCalculator = new Calculator_Handling($settings, null);

		$handlingCalculator->calculate($order, false, true, true);

		$this->assertEquals(11.02, $order->getHandlingFee());
		$this->assertEquals('percent', $order->getHandlingFeeType());
		$this->assertEquals('Handling text', $order->getHandlingText());
		$this->assertEquals(1.95, $order->getHandlingAmount());
		$this->assertEquals(false, $order->getHandlingSeparated());
	}
}

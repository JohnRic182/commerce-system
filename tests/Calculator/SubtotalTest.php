<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_SubtotalTest extends PHPUnit_Framework_TestCase
{
	protected $db;
	protected $settings;
	protected $user;
	protected $order;
	protected $orderRepository;

	public function setUp()
	{
		$this->db = null;
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DisplayPricesWithTax' => 'NO',
		);

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);

		$this->orderRepository = $this->getMock('DataAccess_OrderRepositoryInterface');
	}

	function test_can_Reset_Subtotal_Properties()
	{
		$this->order->setSubtotalAmount(24.95);

		$subtotalCalculator = $this->getSubtotalCalculator();

		$subtotalCalculator->reset($this->order);

		$this->assertEquals(0, $this->order->getSubtotalAmount());
	}

	function test_can_Calculate_SubtotalAmount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$this->order->setSubtotalAmount(5.95);

		$subtotalCalculator = $this->getSubtotalCalculator();

		$subtotalCalculator->calculate($this->order);

		$this->assertEquals(24.95, $this->order->getSubtotalAmount());
	}

	protected function getSubtotalCalculator()
	{
		return new Calculator_Subtotal($this->orderRepository);
	}

	protected function _addItem_To_Order($productPrice, $quantity, $discountAmount = 0)
	{
		$values = array(
			'product_total' => $productPrice * $quantity,
			'product_id' => 'test_prod',
			'product_price' => $productPrice,
			'admin_quantity' => $quantity,
			'ocid' => count($this->order->lineItems) + 1,
			'discount_amount' => $discountAmount,
			'is_gift' => 'No',
			'quantity' => $quantity,
			'product_sub_id' => '',
			'product_url' => '',
			'cat_url' => '',
			'cid' => 1,
			'pid' => 1,
			'inventory_control' => 'No',
			'options' => '',
			'options_clean' => '',
			'weight' => 0,
			'price' => $productPrice,
			'admin_price' => $productPrice,
		);

		$this->order->items[] = $values;
		$this->order->lineItems[$values['ocid']] = new Model_LineItem($values, $productPrice, $this->settings);
	}
}
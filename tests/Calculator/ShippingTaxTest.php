<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_ShippingTaxTest extends PHPUnit_Framework_TestCase
{
	function setup_settings($settings = array())
	{
		return array_merge(
			array(
				'SecurityCookiesPrefix' => '',
				'enable_gift_cert' => 'No',
				'ShippingTaxable' => '0',
				'ShippingTaxClassId' => '0'
			),
			$settings
		);
	}

	function test_reset_For_Correct_Values()
	{
		$settings = $this->setup_settings();

		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$order->setShippingTaxable(true);
		$order->setShippingTaxClassId(1);
		$order->setShippingTaxRate(12);
		$order->setShippingTaxAmount(122);
		$order->setShippingTaxDescription('some other text');

		$taxProvider = $this->getMock('Tax_ProviderInterface');

		$shippingTaxCalculator = new Calculator_ShippingTax($settings, $taxProvider);

		$shippingTaxCalculator->reset($order);

		$this->assertEquals(false, $order->getShippingTaxable());
		$this->assertEquals(0, $order->getShippingTaxClassId());
		$this->assertEquals(0, $order->getShippingTaxRate());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals('', $order->getShippingTaxDescription());
	}

	function test_can_Cancel_Taxable_When_Shipping_Amount_Zero()
	{
		$settings = $this->setup_settings(array(
			"ShippingTaxable" => "1"
		));
		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$classId = $settings['ShippingTaxClassId'];
		$taxRate = 9.37;

		$order->setShippingAmount(0);

		$taxProvider = $this->getMock('Tax_ProviderInterface');

		$handlingTaxCalculator = new Calculator_ShippingTax($settings, $taxProvider);

		$handlingTaxCalculator->calculate($order);

		$this->assertEquals(false, $order->getShippingTaxable());
		$this->assertEquals($classId, $order->getShippingTaxClassId());
		$this->assertEquals(0, $order->getShippingTaxRate());
		$this->assertEquals(0, $order->getShippingTaxAmount());
		$this->assertEquals('', $order->getShippingTaxDescription());
	}

	function test_can_Compute_Shipping_Tax()
	{
		$settings = $this->setup_settings(array(
			"ShippingTaxable" => "1"
		));
		$db = null;

		$user = new USER($db, $settings, 0);
		$order = new ORDER($db, $settings, $user, 1);

		$classId = $settings['ShippingTaxClassId'];
		$taxRate = 9.37;

		$order->setShippingAmount(10);

		$taxProvider = $this->getMock('Tax_ProviderInterface');
		$taxProvider
			->expects($this->once())
			->method('hasTaxRate')
			->with($this->equalTo($classId))
			->will($this->returnValue(true));

		$taxProvider
			->expects($this->once())
			->method('getTaxRate')
			->with($this->equalTo($classId))
			->will($this->returnValue($taxRate));

		$taxProvider
			->expects($this->once())
			->method('getTaxRateDescription')
			->with($this->equalTo($classId))
			->will($this->returnValue('Tax description'));

		$shippingTaxCalculator = new Calculator_ShippingTax($settings, $taxProvider);

		$shippingTaxCalculator->calculate($order);

		$this->assertEquals(true, $order->getShippingTaxable());
		$this->assertEquals($classId, $order->getShippingTaxClassId());
		$this->assertEquals($taxRate, $order->getShippingTaxRate());
		$this->assertEquals(0.94, $order->getShippingTaxAmount());
		$this->assertEquals('Tax description', $order->getShippingTaxDescription());
	}
}
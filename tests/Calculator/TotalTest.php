<?php

require_once '../content/engine/engine_user.php';
require_once '../content/engine/engine_order.php';

class Calculator_TotalTest extends PHPUnit_Framework_TestCase
{
	protected $db;
	protected $settings;
	protected $user;
	protected $order;

	public function setUp()
	{
		$this->db = null;
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DisplayPricesWithTax' => 'NO',
		);

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);
	}

	function test_can_Reset_Total_Properties()
	{
		$this->order->setTotalAmount(24.95);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->reset($this->order);

		$this->assertEquals(0, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(0);
		$this->order->setHandlingAmount(0);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(24.95, $this->order->getTotalAmount());
	}

	function test_calculate_will_round_TotalAmount()
	{
		$this->order->setSubtotalAmount(24.95678);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(0);
		$this->order->setHandlingAmount(0);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(24.96, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_Discount()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(5.50);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(0);
		$this->order->setHandlingAmount(0);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(19.45, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_PromoDiscount()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(5.50);
		$this->order->setShippingAmount(0);
		$this->order->setHandlingAmount(0);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(19.45, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_Shipping()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(2.50);
		$this->order->setHandlingAmount(0);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(27.45, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_Handling()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(0);
		$this->order->setHandlingSeparated(true);
		$this->order->setHandlingAmount(2.50);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(27.45, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_Shipping_And_Handling()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(2.50);
		$this->order->setHandlingSeparated(true);
		$this->order->setHandlingAmount(2.50);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(29.95, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_Shipping_And_Handling_Included_In_Shipping()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(2.50);
		$this->order->setHandlingSeparated(false);
		$this->order->setHandlingAmount(1.50);
		$this->order->setTaxAmount(0);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(27.45, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_Tax()
	{
		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(0);
		$this->order->setHandlingAmount(0);
		$this->order->setTaxAmount(2.50);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(27.45, $this->order->getTotalAmount());
	}

	function test_can_Calculate_TotalAmount_With_Tax_VAT()
	{
		$taxProvider = $this->getMock('Tax_ProviderInterface');
		Tax_ProviderFactory::setTaxProvider($taxProvider);

		$this->order->setSubtotalAmount(24.95);
		$this->order->setDiscountAmount(0);
		$this->order->setPromoDiscountAmount(0);
		$this->order->setShippingAmount(0);
		$this->order->setHandlingAmount(0);
		$this->order->setTaxAmount(2.50);

		$totalCalculator = $this->getTotalCalculator();

		$totalCalculator->calculate($this->order);

		$this->assertEquals(27.45, $this->order->getTotalAmount());
	}

	protected function getTotalCalculator()
	{
		return new Calculator_Total($this->settings);
	}
}
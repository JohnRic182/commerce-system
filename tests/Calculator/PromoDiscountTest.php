<?php

use \Mockery as m;

require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_user.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_order.php';
require_once dirname(dirname(__FILE__)).'/_init.php';

class Calculator_PromoDiscountTest extends PHPUnit_Framework_TestCase
{
	public function tearDown()
	{
		\Mockery::close();
	}

	protected $db;
	protected $settings;
	protected $user;
	protected $order;
	protected $orderRepository;
	protected $promoCodeRepository;

	public function setUp()
	{
		$this->db = null;
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DiscountsPromo' => 'YES',
			'DisplayPricesWithTax' => 'NO',
		);

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);

		$this->orderRepository = m::mock('DataAccess_OrderRepositoryInterface');
		$this->promoCodeRepository = m::mock('DataAccess_PromoCodeRepositoryInterface');
	}

	function test_can_Reset_Discount_Properties_Just_PromoDiscountAmount()
	{
		$this->order->setPromoType('Product');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('amount');
		$this->order->setPromoDiscountValue(5.25);
		$this->order->setPromoDiscountAmount(5.25);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$promoDiscountCalculator->reset($this->order, true);

		$this->assertEquals('Product', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals(5.25, $this->order->getPromoDiscountValue());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
	}

	function test_can_Reset_Discount_Properties()
	{
		$this->order->setPromoType('Product');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('amount');
		$this->order->setPromoDiscountValue(5.25);
		$this->order->setPromoDiscountAmount(5.25);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator(false);

		$promoDiscountCalculator->reset($this->order);

		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals(0, $this->order->getPromoDiscountValue());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_when_reset_false_will_just_recalculate_with_existing_values()
	{
		$this->_addItem_To_Order(24.95, 1, 0, 1.25);

		$this->order->setPromoType('Global');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountAmount(5);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$ret = $promoDiscountCalculator->calculate($this->order, false);

		$this->assertEquals(1.25, $ret);

		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
		$this->assertEquals(5, $this->order->getPromoDiscountValue());
		$this->assertEquals(1.25, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_When_DiscountsPromo_NO_Resets()
	{
		$this->settings['DiscountsPromo'] = 'NO';
		
		$this->order->setPromoType('Global');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('percent');


		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountAmount(5);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals(0, $this->order->getPromoDiscountValue());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_when_promo_not_found_will_reset_values()
	{
		$this->_addItem_To_Order(24.95, 1);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, null);

		$this->order->setPromoType('Product');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountAmount(5);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals(0, $this->order->getPromoDiscountValue());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_when_promo_type_unknown_will_reset_values()
	{
		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'none',
			'min_amount' => 0,
		);
		$this->_addItem_To_Order(24.95, 1);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Product');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountAmount(5);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals(0, $this->order->getPromoDiscountValue());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_global_percent_discount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Global',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Global');
		$this->order->setPromoDiscountAmount(5);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(1.25, $ret);
		$this->assertEquals(1.25, $this->order->getPromoDiscountAmount());
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
		$this->assertEquals('5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_global_percent_discount_multiple_quantities()
	{
		$this->_addItem_To_Order(10, 3);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Global',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 30.00, $promo);

		$this->order->setPromoType('Global');
		$this->order->setPromoDiscountAmount(5);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(1.5, $ret);
		$this->assertEquals(1.5, $this->order->getPromoDiscountAmount());
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
		$this->assertEquals('5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_global_amount_discount_multiple_quantities()
	{
		$this->_addItem_To_Order(10, 3);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '2.5',
			'promo_type' => 'Global',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 30.00, $promo);

		$this->order->setPromoType('Global');
		$this->order->setPromoDiscountValue(2.5);
		$this->order->setPromoDiscountType('amount');
		$this->order->setPromoCampaignId(2);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(2.5, $ret);
		$this->assertEquals(2.5, $this->order->getPromoDiscountAmount());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals('2.5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_product_percent_discount_multiple_quantities()
	{
		$this->_addItem_To_Order(10, 3);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '10',
			'promo_type' => 'Product',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 30, $promo);

		$this->promoCodeRepository->shouldReceive('getPromoProducts')->once()->with(1, 2)->andReturn(array(
			array('pid' => 1)
		));

		$this->order->setPromoType('Product');
		$this->order->setPromoDiscountValue(10);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(3.00, $ret);
		$this->assertEquals(3.00, $this->order->getPromoDiscountAmount());
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
		$this->assertEquals('10', $this->order->getPromoDiscountValue());
		$this->assertEquals('Product', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_product_amount_discount_multiple_quantities()
	{
		$this->_addItem_To_Order(10, 3);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '2.5',
			'promo_type' => 'Product',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 30, $promo);

		$this->promoCodeRepository->shouldReceive('getPromoProducts')->once()->with(1, 2)->andReturn(array(
			array('pid' => 1)
		));

		$this->order->setPromoType('Product');
		$this->order->setPromoDiscountValue(10);
		$this->order->setPromoDiscountType('amount');
		$this->order->setPromoCampaignId(2);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(7.5, $ret);
		$this->assertEquals(7.5, $this->order->getPromoDiscountAmount());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals('2.5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Product', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	//TODO: We don't currently support this scenario
//	function test_calculate_product_amount_discount_multiple_quantities_with_max_quantity()
//	{
//		$this->_addItem_To_Order(10, 3);
//
//		$promo = array(
//			'pid' => 2,
//			'discount_type' => 'amount',
//			'discount' => '2.5',
//			'promo_type' => 'Product',
//			'min_amount' => 0,
//			'max_quantity' => 2,
//		);
//		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 30, $promo);
//
//		$this->promoCodeRepository->shouldReceive('getPromoProducts')->once()->with(1, 2)->andReturn(array(
//			array('pid' => 1)
//		));
//
//		$this->order->setPromoType('Product');
//		$this->order->setPromoDiscountValue(10);
//		$this->order->setPromoDiscountType('amount');
//		$this->order->setPromoCampaignId(2);
//
//		$promoDiscountCalculator = $this->getPromoDiscountCalculator();
//
//		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);
//
//		$ret = $promoDiscountCalculator->calculate($this->order);
//
//		$this->assertEquals(5, $ret);
//		$this->assertEquals(5, $this->order->getPromoDiscountAmount());
//		$this->assertEquals('amount', $this->order->getPromoDiscountType());
//		$this->assertEquals('2.5', $this->order->getPromoDiscountValue());
//		$this->assertEquals('Product', $this->order->getPromoType());
//		$this->assertEquals(2, $this->order->getPromoCampaignId());
//	}

	function test_calculate_global_amount_discount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '5',
			'promo_type' => 'Global',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Global');
		$this->order->setPromoDiscountAmount(5);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(5, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals(5, $this->order->getPromoDiscountValue());
		$this->assertEquals(5, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_When_calculatePromoDiscountAmount_Returns_False_Resets()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Global',
			'min_amount' => 1000,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Global');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountAmount(5);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
		$this->assertEquals(0, $this->order->getPromoDiscountValue());
	}

	function test_calculate_When_global_amount_greater_than_subtotal_sets_discountamount_to_subtotal()
	{
		$this->_addItem_To_Order(4.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '5',
			'promo_type' => 'Global',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 4.95, $promo);

		$this->order->setPromoType('Global');
		$this->order->setPromoDiscountAmount(5);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(4.95, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals(5, $this->order->getPromoDiscountValue());
		$this->assertEquals(4.95, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_can_apply_shipping_amount_discount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '5',
			'promo_type' => 'Shipping',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Shipping');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('amount');
		$this->order->setPromoCampaignId(2);

		$this->order->setShippingAmount(15);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(5, $ret);
		$this->assertEquals(5, $this->order->getPromoDiscountAmount());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals('5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Shipping', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_can_apply_shipping_amount_discount_with_tax()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '5',
			'promo_type' => 'Shipping',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setShippingTaxRate(10);
		$this->order->setPromoType('Shipping');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('amount');
		$this->order->setPromoCampaignId(2);

		$this->order->setShippingAmount(15);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(5, $ret);
		$this->assertEquals(5, $this->order->getPromoDiscountAmount());
		$this->assertEquals(5.5, $this->order->getPromoDiscountAmountWithTax());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals('5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Shipping', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_can_apply_shipping_percent_discount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Shipping',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Shipping');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->order->setShippingAmount(15);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0.75, $ret);
		$this->assertEquals(0.75, $this->order->getPromoDiscountAmount());
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
		$this->assertEquals('5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Shipping', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_can_apply_shipping_percent_discount_with_tax()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Shipping',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setShippingTaxRate(10);
		$this->order->setPromoType('Shipping');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->order->setShippingAmount(15);
		$this->order->setShippingTaxAmount(1.5);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0.75, $ret);
		$this->assertEquals(0.75, $this->order->getPromoDiscountAmount());
		$this->assertEquals(0.83, $this->order->getPromoDiscountAmountWithTax());
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
		$this->assertEquals('5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Shipping', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_can_shipping_discount_doesnt_apply_when_subtotal_less_than_min_amount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Shipping',
			'min_amount' => 1000,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Shipping');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->order->setShippingAmount(15);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
		$this->assertEquals(0, $this->order->getPromoDiscountValue());
	}

	function test_calculate_When_shipping_amount_greater_than_shippingamount_sets_discountamount_to_shippingamount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '5',
			'promo_type' => 'Shipping',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Shipping');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->order->setShippingAmount(1.5);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(1.5, $ret);
		$this->assertEquals(1.5, $this->order->getPromoDiscountAmount());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals('5', $this->order->getPromoDiscountValue());
		$this->assertEquals('Shipping', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_can_apply_product_amount_discount()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '5',
			'promo_type' => 'Product',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Product');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->promoCodeRepository->shouldReceive('getPromoProducts')->times(1)->with(1,2)->andReturn(array(array('pid' => 1, 'price' => 14.95, 'quantity' => 1)));
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(5, $ret);
		$this->assertEquals('Product', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals(5, $this->order->getPromoDiscountValue());
		$this->assertEquals(5, $this->order->getPromoDiscountAmount());

	}

	function test_calculate_can_apply_product_percent_discount()
	{
		$this->_addItem_To_Order(14.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Product',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 14.95, $promo);

		$this->order->setPromoType('Product');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountAmount(0);

		$this->promoCodeRepository->shouldReceive('getPromoProducts')->times(1)->with(1,2)->andReturn(array(array('pid' => 1, 'price' => 14.95, 'quantity' => 1)));
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0.75, $ret);

		$this->assertEquals('Product', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
		$this->assertEquals('percent', $this->order->getPromoDiscountType());
		$this->assertEquals(5, $this->order->getPromoDiscountValue());
		$this->assertEquals(0.75, $this->order->getPromoDiscountAmount());
	}

	function test_calculate_When_Product_subtotal_less_than_min_amount_will_reset()
	{
		$this->_addItem_To_Order(14.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'percent',
			'discount' => '5',
			'promo_type' => 'Product',
			'min_amount' => 20,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 14.95, $promo);

		$this->order->setPromoType('Product');
		$this->order->setPromoCampaignId(2);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountAmount(0);

		$this->promoCodeRepository->shouldReceive('getPromoProducts')->times(1)->with(1,2)->andReturn(array(array('pid' => 1, 'price' => 14.95, 'quantity' => 1)));

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
		$this->assertEquals(0, $this->order->getPromoDiscountValue());
	}

	function test_calculate_When_Product_totalPrice_less_than_discountamount_Sets_discountamount_to_totalPrice()
	{
		$this->_addItem_To_Order(14.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '15',
			'promo_type' => 'Product',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 14.95, $promo);

		$this->order->setPromoType('Product');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->promoCodeRepository->shouldReceive('getPromoProducts')->times(1)->with(1,2)->andReturn(array(array('pid' => 1, 'price' => 14.95, 'quantity' => 1)));
		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(14.95, $ret);
		$this->assertEquals(14.95, $this->order->getPromoDiscountAmount());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals('15', $this->order->getPromoDiscountValue());
		$this->assertEquals('Product', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
	}

	function test_calculate_When_Product_No_Products_Returned_Resets()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '15',
			'promo_type' => 'Product',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Product');
		$this->order->setPromoDiscountAmount(0);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$this->promoCodeRepository->shouldReceive('getPromoProducts')->times(1)->with(1,2)->andReturn(array());
//		$this->promoCodeRepository->expects($this->once())
//			->method('getPromoProducts')
//			->with($this->equalTo(1), $this->equalTo(2))
//			->will($this->returnValue(array()));

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals('0', $this->order->getPromoDiscountValue());
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
	}

	function test_bug1758_promo_code_still_applied_after_product_quantities_reduced_to_lower_than_minimum_requirement()
	{
		$this->_addItem_To_Order(24.95, 1);

		$promo = null;
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 24.95, $promo);

		$this->order->setPromoType('Product');
		$this->order->setPromoDiscountAmount(150);
		$this->order->setPromoDiscountValue(150);
		$this->order->setPromoDiscountType('amount');
		$this->order->setPromoCampaignId(2);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(0, $ret);
		$this->assertEquals(0, $this->order->getPromoDiscountAmount());
		$this->assertEquals('none', $this->order->getPromoDiscountType());
		$this->assertEquals('0', $this->order->getPromoDiscountValue());
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(0, $this->order->getPromoCampaignId());
	}

	function test_promo_code_calculates_correctly_with_global_discount_applied()
	{
		$this->_addItem_To_Order(64.99, 2);

		$promo = array(
			'pid' => 2,
			'discount_type' => 'amount',
			'discount' => '5',
			'promo_type' => 'Global',
			'min_amount' => 0,
		);
		$this->setGetPromoCodeByPromoCampaignIdExpectation($this->promoCodeRepository, 2, 4.95, $promo);

		$this->order->setPromoType('Global');
		$this->order->setPromoDiscountAmount(5);
		$this->order->setPromoDiscountValue(5);
		$this->order->setPromoDiscountType('percent');
		$this->order->setPromoCampaignId(2);

		$item = $this->order->lineItems[1];
		$item->setDiscountAmount(15.50);

		$this->orderRepository->shouldReceive('updateLineItemDiscount')->times(1);

		$promoDiscountCalculator = $this->getPromoDiscountCalculator();

		$ret = $promoDiscountCalculator->calculate($this->order);

		$this->assertEquals(5, $ret);
		$this->assertEquals('Global', $this->order->getPromoType());
		$this->assertEquals(2, $this->order->getPromoCampaignId());
		$this->assertEquals('amount', $this->order->getPromoDiscountType());
		$this->assertEquals(5, $this->order->getPromoDiscountValue());
		$this->assertEquals(5, $this->order->getPromoDiscountAmount());
	}

	protected function getPromoDiscountCalculator()
	{
		return new Calculator_PromoDiscount($this->orderRepository, $this->promoCodeRepository, $this->settings);
	}

	protected function _addItem_To_Order($productPrice, $quantity, $discountAmount = 0, $promoDiscountAmount = 0, $discountAmountWithTax = 0, $promoDiscountAmountWithTax = 0)
	{
		$values = array(
			'product_total' => $productPrice * $quantity,
			'product_id' => 'test_prod',
			'product_price' => $productPrice,
			'admin_quantity' => $quantity,
			'ocid' => count($this->order->lineItems) + 1,
			'discount_amount' => $discountAmount,
			'discount_amount_with_tax' => $discountAmountWithTax,
			'promo_discount_amount' => $promoDiscountAmount,
			'promo_discount_amount_with_tax' => $promoDiscountAmountWithTax,
			'pid' => 1,
			'is_gift' => 'No',
			'quantity' => $quantity,
			'product_sub_id' => '',
			'product_url' => '',
			'cat_url' => '',
			'cid' => 1,
			'inventory_control' => 'No',
			'options' => '',
			'options_clean' => '',
			'weight' => 0,
			'price' => $productPrice,
			'admin_price' => $productPrice,
			'is_taxable' => 'Yes',
			'tax_rate' => 0,
		);

		$this->order->items[] = $values;
		$this->order->lineItems[$values['ocid']] = new Model_LineItem($values, $productPrice, $this->settings);
	}

	protected function setGetPromoCodeByPromoCampaignIdExpectation($promoCodeRepositoryMock, $promoCampaignId, $subtotalAmount, $promoResult)
	{
		$promoCodeRepositoryMock->shouldReceive('getPromoCodeByPromoCampaignId')->times(1)->with($promoCampaignId)->andReturn($promoResult);
//		$promoCodeRepositoryMock->expects($this->once())
//			->method('getPromoCodeByPromoCampaignId')
//			->with($this->equalTo($promoCampaignId), $this->equalTo($subtotalAmount))
//			->will($this->returnValue($promoResult));
	}
}
<?php

require_once '_init.php';

require_once CONTENT_DIR.'engine/engine_functions.php';

class EngineFunctionsTest extends PHPUnit_Framework_TestCase
{
	protected static $_current_timezone = null;
	
	public static function setupBeforeClass()
	{
		self::$_current_timezone = date_default_timezone_get();
	}
	
	private function getSettings()
	{
		return array(
			'LocalizationUseSystemTimezone' => 'No',
			'LocalizationDefaultTimezone' => 'America/Argentina/Buenos_Aires'
		);
	}

	function test_normalizeProductLockFields_Returns_Unmapped_Values()
	{
		$lock_fields = array('name', 'category');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('name', $lock_fields);
		$this->assertContains('category', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Title()
	{
		$lock_fields = array('title');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('name', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Cid()
	{
		$lock_fields = array('cid');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('category', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Secondary_Categories()
	{
		$lock_fields = array('secondary_categories[]');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('secondary-categories', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Price2()
	{
		$lock_fields = array('price2');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('sale-price', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Meta_Title()
	{
		$lock_fields = array('meta_title');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('meta-title', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Meta_Description()
	{
		$lock_fields = array('meta_description');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('meta-description', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Image()
	{
		$lock_fields = array('image');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('images', $lock_fields);
	}

	function test_normalizeProductLockFields_Maps_Image_Url()
	{
		$lock_fields = array('image_url');

		$lock_fields = normalizeProductLockFields($lock_fields);

		$this->assertContains('images', $lock_fields);
	}

	public function test_setApplicationDefaultTimezone_Successfully_Sets_Given_Timezone()
	{
		$db = Mock_Helpers::getDbFake($this);
		$db->expects($this->once())
			->method('query')
			->with($this->equalTo('SET time_zone="-03:00"'));
		
		$settings = array(
			'LocalizationUseSystemTimezone' => 'No',
			'LocalizationDefaultTimezone' => 'America/Argentina/Buenos_Aires'
		);
		
		$timezone = Timezone::setApplicationDefaultTimezone($db, $settings);
		
		$this->assertEquals('America/Argentina/Buenos_Aires', $timezone);
		$this->assertEquals('America/Argentina/Buenos_Aires', date_default_timezone_get());
	}
	
	/**
	 * @expectedException PHPUnit_Framework_Error_Notice
	 */
	public function test_setApplicationDefaultTimezone_Throws_Exception_For_Invalid_Timezone_Identifier()
	{
		$db = Mock_Helpers::getDbFake($this);
		$settings = array(
			'LocalizationUseSystemTimezone' => 'No',
			'LocalizationDefaultTimezone' => 'An incorrect Timezone Identifier'
		);
		Timezone::setApplicationDefaultTimezone($db, $settings);
	}
	
	public function test_setApplicationDefaultTimezone_Does_Not_Throw_Exception_For_Blank_Timezone_Identifier()
	{
		self::_resetDefaultTimezone();
		$db = Mock_Helpers::getDbFake($this);
		try
		{
			$settings = array(
				'LocalizationUseSystemTimezone' => 'No',
				'LocalizationDefaultTimezone' => ''
			);
			
			$timezone = Timezone::setApplicationDefaultTimezone($db, $settings);
		}
		catch (Exception $e)
		{
			$this->fail('Exception thrown when it was not expected');
		}
		
		$this->assertEquals(self::$_current_timezone, $timezone);
	}
	
	public function test_setApplicationDefaultTimezone_Mysql_NOW_Method_Returns_Proper_Offset_After_Running_Change()
	{
		// Reset the timezone
		self::_resetDefaultTimezone();
		
		$db = Mock_Helpers::getDbFake($this);
		
		$db->expects($this->at(0))
			->method('query')
			->with($this->equalTo('SET time_zone="-03:00"'));
			
		$db->expects($this->at(1))
			->method('query')
			->with('SELECT @@session.time_zone');
			
		$db->expects($this->at(2))
			->method('moveNext')
			->will($this->returnValue(array('-03:00')));
		
		$settings = array(
			'LocalizationUseSystemTimezone' => 'No',
			'LocalizationDefaultTimezone' => 'America/Argentina/Buenos_Aires'
		);
		
		$timezone = Timezone::setApplicationDefaultTimezone($db, $this->getSettings());
		
		$db->query('SELECT @@session.time_zone');
		$value = $db->moveNext();
		
		$this->assertEquals('-03:00', $value[0], 'The timezone did not change in the mysql client session');
	}
	
	public function test_setApplicationDefaultTimezone_Will_Not_Set_New_Timezone_Because_Settings_Is_Set_To_Use_System_Timezone()
	{
		self::_resetDefaultTimezone();
		
		$settings = array(
			'LocalizationUseSystemTimezone' => 'Yes',
			'LocalizationDefaultTimezone' => 'America/Argentina/Buenos_Aires'
		);
		
		$db = Mock_Helpers::getDbFake($this);
		$timezone = Timezone::setApplicationDefaultTimezone($db, $settings);
		
		$this->assertEquals(self::$_current_timezone, $timezone);
	}
	
	protected static function _resetDefaultTimezone()
	{
		date_default_timezone_set(self::$_current_timezone);
	}
	
	public static function tearDownAfterClass()
	{
		self::_resetDefaultTimezone();
	}
}
<?php

require_once dirname(__FILE__).'/QBOSyncTestBase.php';

class QBOItemsSyncTest extends QBOSyncTestBase
{
	function test_syncItems_Without_IncomeAccount_Writes_Error()
	{
		$settings = array('intuit_income_account' => '', 'intuit_expense_account' => '');

		$qbsync = $this->getQBSync($settings);

		$qbsync->syncItems();

		$this->assertEquals('No income account defined', intuit_QBLogger::$lastMessage);
	}

	function test_canSyncItem()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => null,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Add',
			'<Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Add', '1', '1');
	}

	function test_canSyncItem_With_Colon_In_Name()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => null,
				'product_subid' => null,
				'product_id' => 'test:prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Add',
			'<Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Add', '1', '1');
	}

	function test_canSyncItem_With_Colon_In_Sub_Name()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => null,
				'product_subid' => 'test:prod:2',
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Add',
			'<Name>test_prod_2</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod_2', 'item', 'Add', '1', '1');
	}

	function test_canSyncItem_With_SubId()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => null,
				'product_subid' => 'test_prod_red',
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Add',
			'<Name>test_prod_red</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod_red', 'item', 'Add', '1', '1');
	}

	function test_canSyncItem_Taxable()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => null,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'Yes',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Add',
			'<Name>test_prod</Name><Desc>Test Product</Desc><Taxable>true</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Add', '1', '1');
	}

	function test_canSyncItem_ExpenseAccount()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => null,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'Yes',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account><Account><Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Expense</Name><Subtype>Inventory</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Add',
			'<Name>test_prod</Name><Desc>Test Product</Desc><Taxable>true</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">2</AccountId></ExpenseAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Add', '1', '1');
	}

	function test_canResyncItem_Doesnt_Update_When_No_Changes_Detected()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync->syncItems();

		$this->assertEquals(0, count($this->transport->requests));

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Updates_ProductId()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod_1',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod_1</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod_1', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Updates_Title()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Awesome Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Awesome Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Updates_Taxable()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'Yes',
				'price' => '5',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>true</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Updates_Price()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '15.95',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Failed_Item()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product2',
				'is_taxable' => 'Yes',
				'price' => '15.95',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');
		$failedItemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>2</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);
		$this->transport->addQueryResult('Item', array(), true, $failedItemsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>2</SyncToken><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>true</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Item_Not_In_Existing_Items()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '15.95',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"></qbo:CdmCollections></qbo:SearchResults>');

 		$itemXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Item xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><Id idDomain="QBO">17</Id><SyncToken>2</SyncToken><MetaData><CreateTime>2012-08-16T11:31:49-07:00</CreateTime><LastUpdatedTime>2012-08-16T11:31:49-07:00</LastUpdatedTime></MetaData><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>false</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef></Item>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$this->transport->addQueryByIdResult('Item', 17, $itemXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>2</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_syncItem_Handles_Duplicate_Name()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => null,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '15.95',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

 		$itemXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Item xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><Id idDomain="QBO">17</Id><SyncToken>2</SyncToken><MetaData><CreateTime>2012-08-16T11:31:49-07:00</CreateTime><LastUpdatedTime>2012-08-16T11:31:49-07:00</LastUpdatedTime></MetaData><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>false</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef></Item>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$this->transport->throwDuplicateException = true;

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Does_Not_Update_IncomeAccountRef()
	{
		$settings = array('intuit_income_account' => 3, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '15.95',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account><Account><Id idDomain="QBO">3</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Online Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef/></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}

	function test_canResyncItem_Does_Not_Update_ExpenseAccountRef()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => 17,
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '15.95',
			),
		);

		$qbsync = $this->getQBSync($settings);

		$this->syncRepository->itemsToSync = $itemsToSync;

 		$accountsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Accounts"><Account><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account><Account><Id idDomain="QBO">3</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Online Sales</Name><Subtype>SalesOfProductIncome</Subtype></Account><Account><Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Online Sales</Name><Subtype>Inventory</Subtype></Account><Account><Id idDomain="QBO">4</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Online Inventory</Name><Subtype>Inventory</Subtype></Account></qbo:CdmCollections></qbo:SearchResults>');

 		$itemsXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"><Item><Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><ItemParentId/><ItemParentName/><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><PurchaseDesc/><PurchaseCost/><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef></Item></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);
		$this->transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync->syncItems();

		$this->assertTransportRequest(0, 'Item', 'Mod',
			'<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>15.95</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>'
		);

		$this->assertSyncValues(0, 'test_prod', 'item', 'Mod', '17', '1');
	}
}
<?php

require_once dirname(__FILE__).'/../doubles/QBTransportTestDouble.php';
require_once dirname(__FILE__).'/../doubles/SyncRepositoryTestDouble.php';

class QBOSyncTestBase extends PHPUnit_Framework_TestCase
{
	protected $syncRepository;
	protected $transport;
	protected $qbConnector;

	protected function setUp()
	{
		$this->syncRepository = new SyncRepositoryTestDouble();
		$this->transport = new QBTransportTestDouble();
		$this->qbConnector = new intuit_Services_QBOConnector($this->transport, new intuit_Model_Mapping_ModelMapper('QBO'));

		$this->transport->addQueryResult('Item', array(), false, $this->getBlankItemResultsXml());
		$this->transport->addQueryResult('Item', array(), true, $this->getBlankItemResultsXml());
	}

	protected function assertTransportRequest($idx, $objectType, $operation, $xml)
	{
		$this->assertTrue(count($this->transport->requests) > $idx);

		$this->assertEquals($objectType, $this->transport->requests[$idx]['obj_type']);
		$this->assertEquals($xml, trim($this->transport->requests[$idx]['fields']));
		$this->assertEquals($operation, $this->transport->requests[$idx]['operation']);
	}

	protected function assertSyncValues($idx, $id, $type, $operation, $intuitId, $setDate)
	{
		$this->assertTrue(count($this->syncRepository->intuitSyncValues) > $idx);

		$this->assertEquals($id, $this->syncRepository->intuitSyncValues[$idx]['id']);
		$this->assertEquals($type, $this->syncRepository->intuitSyncValues[$idx]['type']);
		$this->assertEquals($operation, $this->syncRepository->intuitSyncValues[$idx]['operation']);
		$this->assertEquals($intuitId, $this->syncRepository->intuitSyncValues[$idx]['intuitId']);
		$this->assertEquals($setDate, $this->syncRepository->intuitSyncValues[$idx]['setDate']);		
	}

	protected function getQBSync(&$settings)
	{
		$sync = new intuit_Services_QBSync($this->syncRepository, $this->qbConnector, 'QBO', $settings);

		$sync->setCurrencyCode('USD');

		return $sync;
	}

	protected function getBlankItemResultsXml()
	{
		return $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Items"></qbo:CdmCollections></qbo:SearchResults>');
	}

	protected function getQueryResults($xml)
	{
		$xml_res = simplexml_load_string($xml);

		return (array)$xml_res->xpath('//qbo:SearchResults/qbo:CdmCollections/*');
	}
}
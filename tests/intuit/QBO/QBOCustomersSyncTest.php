<?php

require_once dirname(__FILE__).'/QBOSyncTestBase.php';

class QBOCustomersSyncTest extends QBOSyncTestBase
{
	function test_canSyncCustomer()
	{
		$settings = array();

		$customersToSync = array(
			array(
				'intuit_id' => null,
				'fname' => 'Test',
				'lname' => 'Tester',
				'uid' => 1,
				'address1' => '123 Test Dr',
				'address2' => 'Suite 2',
				'city' => 'Phoenix',
				'billing_state_name' => 'Arizona',
				'zip' => '85012',
				'company' => 'Test Company',
				'shipping_address1' => '123 Test Pl',
				'shipping_address2' => 'Suite3',
				'shipping_city' => 'Mesa',
				'shipping_state_name' => 'Arizona',
				'shipping_zip' => '85012',
				'shipping_company' => '',
				'email' => 'test@test.com',
				'phone' => '999-999-9999',
			),
		);

		$this->syncRepository->customersToSync = $customersToSync;

		$qbsync = $this->getQBSync($settings);

		$qbsync->syncCustomers();

		$this->assertTransportRequest(0, 'Customer', 'Add',
			'<Name>Test Tester [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Billing</Tag></Address><Address><Line1>123 Test Pl</Line1><Line2>Suite3</Line2><City>Mesa</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber></Phone><Email><Address>test@test.com</Address></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>'
		);

		$this->assertSyncValues(0, '1', 'user', 'Add', '1', '1');
	}

	function test_canResyncCustomer()
	{
		$settings = array();

		$customersToSync = array(
			array(
				'intuit_id' => 2,
				'fname' => 'Test',
				'lname' => 'Testing',
				'uid' => 1,
				'address1' => '123 Test Dr',
				'address2' => 'Suite 2',
				'city' => 'Phoenix',
				'billing_state_name' => 'Arizona',
				'zip' => '85012',
				'company' => 'Test Company',
				'shipping_address1' => '123 Test Pl',
				'shipping_address2' => 'Suite3',
				'shipping_city' => 'Mesa',
				'shipping_state_name' => 'Arizona',
				'shipping_zip' => '85012',
				'shipping_company' => '',
				'email' => 'test@test.com',
				'phone' => '999-999-9999',
			),
		);

		$this->syncRepository->customersToSync = $customersToSync;

		$customersXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Customer><Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><Name>Test Tester [1]</Name><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs></Customer>');

		$this->transport->addQueryByIdResult('Customer', 2, $customersXml);

		$qbsync = $this->getQBSync($settings);

		$qbsync->syncCustomers();

		$this->assertTransportRequest(0, 'Customer', 'Mod',
			'<Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><Name>Test Testing [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Billing</Tag></Address><Address><Line1>123 Test Pl</Line1><Line2>Suite3</Line2><City>Mesa</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber></Phone><Email><Address>test@test.com</Address></Email><GivenName>Test</GivenName><FamilyName>Testing</FamilyName><ShowAs>Test Testing</ShowAs>'
		);

		$this->assertSyncValues(0, '1', 'user', 'Mod', '2', '1');
	}

	function test_canResyncCustomer_Handles_Duplicate()
	{
		$settings = array();

		$customersToSync = array(
			array(
				'intuit_id' => null,
				'fname' => 'Test',
				'lname' => 'Testing',
				'uid' => 1,
				'address1' => '123 Test Dr',
				'address2' => 'Suite 2',
				'city' => 'Phoenix',
				'billing_state_name' => 'Arizona',
				'zip' => '85012',
				'company' => 'Test Company',
				'shipping_address1' => '123 Test Pl',
				'shipping_address2' => 'Suite3',
				'shipping_city' => 'Mesa',
				'shipping_state_name' => 'Arizona',
				'shipping_zip' => '85012',
				'shipping_company' => '',
				'email' => 'testing@test.com',
				'phone' => '999-999-9999',
			),
		);

		$this->syncRepository->customersToSync = $customersToSync;

		$this->transport->throwDuplicateException = true;

		$customersXml = $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:SearchResults xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:CdmCollections xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Customers"><Customer><Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><Name>Test Testing [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Billing</Tag></Address><Address><Line1>123 Test Pl</Line1><Line2>Suite3</Line2><City>Mesa</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber></Phone><Email><Address>test@test.com</Address></Email><GivenName>Test</GivenName><FamilyName>Testing</FamilyName><ShowAs>Test Testing</ShowAs></Customer></qbo:CdmCollections></qbo:SearchResults>');

		$this->transport->addQueryResult('Customer', array(), false, $customersXml);

		$qbsync = $this->getQBSync($settings);

		$qbsync->syncCustomers();

		$this->assertTransportRequest(0, 'Customer', 'Mod',
			'<Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><Name>Test Testing [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Billing</Tag></Address><Address><Line1>123 Test Pl</Line1><Line2>Suite3</Line2><City>Mesa</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85012</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber></Phone><Email><Address>testing@test.com</Address></Email><GivenName>Test</GivenName><FamilyName>Testing</FamilyName><ShowAs>Test Testing</ShowAs>'
		);

		$this->assertSyncValues(0, '1', 'user', 'Mod', '2', '1');
	}
}
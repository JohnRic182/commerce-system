<?php

require_once dirname(__FILE__).'/QBOSyncTestBase.php';

class QBOSalesReceiptsSyncTest extends QBOSyncTestBase
{
	function test_canSyncSalesReceipt()
	{
		$settings = array('intuit_nontaxable_tax_code' => 'Non', 'intuit_taxable_tax_code' => 'Tax');

		$ordersToSync = array(
			array(
				'intuit_id' => null,
				'order_num' => 1,
				'oid' => 1,
				'placed_date' => '2012-12-05 10:33:33',
				'intuit_user_id' => 2,
				'discount_amount' => 0,
				'promo_discount_amount' => 0,
				'shipping_amount' => 0,
				'handling_separated' => 1,
				'handling_amount' => 0,
				'total_amount' => 6.00,
				'tax_amount' => 0,
				'subtotal_amount' => 5.00,
			),
		);

		$orderLinesToSync = array(
			1 => array(
				array(
					'product_id' => 'test_prod',
					'product_sub_id' => '',
					'price' => 5.00,
					'quantity' => 1,
					'title' => 'Test Product',
					'options' => '',
					'is_taxable' => 'No',
				),
			),
		);

		$itemsMap = array(
			'test_prod' => 1,
		);

		$this->syncRepository->ordersToSync = $ordersToSync;
		$this->syncRepository->orderLinesToSync = $orderLinesToSync;
		$this->syncRepository->itemsMap = $itemsMap;

		$preferencesXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:QboCompanyPreferences xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:Preference><Name>TaxAccountId</Name><Value>null</Value></qbo:Preference><qbo:Preference><Name>TaxPercent</Name><Value></Value></qbo:Preference><qbo:Preference><Name>TaxEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>NewCustomersAreTaxable</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>DefaultDiscountAccountId</Name><Value>36</Value></qbo:Preference><qbo:Preference><Name>DiscountEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>DefaultCustomerMessage</Name></qbo:Preference><qbo:Preference><Name>TimeTrackingEnabled</Name><Value>true</Value></qbo:Preference><qbo:Preference><Name>EstimateEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>ClientTerminology</Name><Value>customers</Value></qbo:Preference><qbo:Preference><Name>ClassTrackingEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>ExpenseTrackingEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>CustomTransactionsEnabled</Name><Value>true</Value></qbo:Preference><qbo:Preference><Name>AccountNumbersEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>SalesEmailGreetingEnabled</Name><Value>true</Value></qbo:Preference><qbo:Preference><Name>SalesEmailGreeting</Name><Value>Dear</Value></qbo:Preference><qbo:Preference><Name>SalesEmailGreetingName</Name><Value>&lt;Full Name&gt;</Value></qbo:Preference><qbo:Preference><Name>SalesEmailInvoiceSubject</Name><Value>Invoice from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailCreditMemoSubject</Name><Value>Credit Memo from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailCashSaleSubject</Name><Value>Sales Receipt from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailPurchaseOrderSubject</Name><Value>Purchase Order from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailStatementSubject</Name><Value>Statement from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailInvoiceMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailCreditMemoMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailCashSaleMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailPurchaseOrderMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailStatementMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailCopyMeEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>SalesEmailPrintCopyEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>SalesEmailDeliveredByQboEnabled</Name><Value>false</Value></qbo:Preference></qbo:QboCompanyPreferences>');

		$preferencesXml = $preferencesXml->xpath('//qbo:Preference');

		$customerXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Customer xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-16T11:31:49-07:00</CreateTime><LastUpdatedTime>2012-08-16T11:31:49-07:00</LastUpdatedTime></MetaData><Name>Test Tester [1]</Name><Address><Line1>123 Test Dr</Line1><City>Phoenix</City><CountrySubDivisionCode>testing</CountrySubDivisionCode><PostalCode>85027</PostalCode><Tag>Billing</Tag></Address><Phone><DeviceType>Primary</DeviceType><FreeFormNumber>999-999-9999</FreeFormNumber></Phone><WebSite/><Email><Address>test@test.com</Address></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><CustomField xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="StringTypeCustomField"><DefinitionId>Preferred Delivery Method</DefinitionId><Value>EMAIL</Value></CustomField><CustomField xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="StringTypeCustomField"><DefinitionId>Resale Number</DefinitionId></CustomField><CustomField xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="BooleanTypeCustomField"><DefinitionId>Bill With Parent</DefinitionId><Value>false</Value></CustomField><ShowAs>Test Tester</ShowAs><OpenBalance><Amount>0</Amount></OpenBalance></Customer>');

		$itemXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Item xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-16T11:31:49-07:00</CreateTime><LastUpdatedTime>2012-08-16T11:31:49-07:00</LastUpdatedTime></MetaData><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>true</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef></Item>');

		$this->transport->addQueryResult('Preferences', array(), false, $preferencesXml);
		$this->transport->addQueryByIdResult('Customer', 2, $customerXml);
		$this->transport->addQueryByIdResult('Item', 1, $itemXml);

		$qbsync = $this->getQBSync($settings);

		$qbsync->syncSalesReceipts();

		$this->assertTransportRequest(0, 'SalesReceipt', 'Add',
			'<Header><DocNumber>1</DocNumber><TxnDate>2012-12-05</TxnDate><CustomerId idDomain="QBO">2</CustomerId><SubTotalAmt>5</SubTotalAmt><TaxAmt>0</TaxAmt><TotalAmt>6</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>false</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>'
		);

		$this->assertSyncValues(0, '1', 'order', 'Add', '1', '1');
	}


	function test_canSyncSalesReceipt_With_Tax()
	{
		$settings = array('intuit_nontaxable_tax_code' => 'Non', 'intuit_taxable_tax_code' => 'Tax');

		$ordersToSync = array(
			array(
				'intuit_id' => null,
				'order_num' => 1,
				'oid' => 1,
				'placed_date' => '2012-12-05 10:33:33',
				'intuit_user_id' => 2,
				'discount_amount' => 0,
				'promo_discount_amount' => 0,
				'shipping_amount' => 0,
				'handling_separated' => 1,
				'handling_amount' => 0,
				'total_amount' => 6.50,
				'tax_amount' => 0.5,
				'subtotal_amount' => 5.00,
			),
		);

		$orderLinesToSync = array(
			1 => array(
				array(
					'product_id' => 'test_prod',
					'product_sub_id' => '',
					'price' => 5.00,
					'quantity' => 1,
					'title' => 'Test Product',
					'options' => '',
					'is_taxable' => 'Yes',
				),
			),
		);

		$itemsMap = array(
			'test_prod' => 1,
		);

		$this->syncRepository->ordersToSync = $ordersToSync;
		$this->syncRepository->orderLinesToSync = $orderLinesToSync;
		$this->syncRepository->itemsMap = $itemsMap;

		$preferencesXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><qbo:QboCompanyPreferences xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><qbo:Preference><Name>TaxAccountId</Name><Value>null</Value></qbo:Preference><qbo:Preference><Name>TaxPercent</Name><Value></Value></qbo:Preference><qbo:Preference><Name>TaxEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>NewCustomersAreTaxable</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>DefaultDiscountAccountId</Name><Value>36</Value></qbo:Preference><qbo:Preference><Name>DiscountEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>DefaultCustomerMessage</Name></qbo:Preference><qbo:Preference><Name>TimeTrackingEnabled</Name><Value>true</Value></qbo:Preference><qbo:Preference><Name>EstimateEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>ClientTerminology</Name><Value>customers</Value></qbo:Preference><qbo:Preference><Name>ClassTrackingEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>ExpenseTrackingEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>CustomTransactionsEnabled</Name><Value>true</Value></qbo:Preference><qbo:Preference><Name>AccountNumbersEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>SalesEmailGreetingEnabled</Name><Value>true</Value></qbo:Preference><qbo:Preference><Name>SalesEmailGreeting</Name><Value>Dear</Value></qbo:Preference><qbo:Preference><Name>SalesEmailGreetingName</Name><Value>&lt;Full Name&gt;</Value></qbo:Preference><qbo:Preference><Name>SalesEmailInvoiceSubject</Name><Value>Invoice from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailCreditMemoSubject</Name><Value>Credit Memo from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailCashSaleSubject</Name><Value>Sales Receipt from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailPurchaseOrderSubject</Name><Value>Purchase Order from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailStatementSubject</Name><Value>Statement from Pinnacle Electronics</Value></qbo:Preference><qbo:Preference><Name>SalesEmailInvoiceMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailCreditMemoMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailCashSaleMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailPurchaseOrderMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailStatementMessage</Name><Value></Value></qbo:Preference><qbo:Preference><Name>SalesEmailCopyMeEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>SalesEmailPrintCopyEnabled</Name><Value>false</Value></qbo:Preference><qbo:Preference><Name>SalesEmailDeliveredByQboEnabled</Name><Value>false</Value></qbo:Preference></qbo:QboCompanyPreferences>');

		$preferencesXml = $preferencesXml->xpath('//qbo:Preference');

		$customerXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Customer xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><Id idDomain="QBO">2</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-16T11:31:49-07:00</CreateTime><LastUpdatedTime>2012-08-16T11:31:49-07:00</LastUpdatedTime></MetaData><Name>Test Tester [1]</Name><Address><Line1>123 Test Dr</Line1><City>Phoenix</City><CountrySubDivisionCode>testing</CountrySubDivisionCode><PostalCode>85027</PostalCode><Tag>Billing</Tag></Address><Phone><DeviceType>Primary</DeviceType><FreeFormNumber>999-999-9999</FreeFormNumber></Phone><WebSite/><Email><Address>test@test.com</Address></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><CustomField xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="StringTypeCustomField"><DefinitionId>Preferred Delivery Method</DefinitionId><Value>EMAIL</Value></CustomField><CustomField xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="StringTypeCustomField"><DefinitionId>Resale Number</DefinitionId></CustomField><CustomField xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="BooleanTypeCustomField"><DefinitionId>Bill With Parent</DefinitionId><Value>false</Value></CustomField><ShowAs>Test Tester</ShowAs><OpenBalance><Amount>0</Amount></OpenBalance></Customer>');

		$itemXml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Item xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo"><Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-16T11:31:49-07:00</CreateTime><LastUpdatedTime>2012-08-16T11:31:49-07:00</LastUpdatedTime></MetaData><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>true</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef></Item>');

		$this->transport->addQueryResult('Preferences', array(), false, $preferencesXml);
		$this->transport->addQueryByIdResult('Customer', 2, $customerXml);
		$this->transport->addQueryByIdResult('Item', 1, $itemXml);

		$qbsync = $this->getQBSync($settings);

		$qbsync->syncSalesReceipts();

		$this->assertTransportRequest(0, 'SalesReceipt', 'Add',
			'<Header><DocNumber>1</DocNumber><TxnDate>2012-12-05</TxnDate><CustomerId idDomain="QBO">2</CustomerId><SubTotalAmt>5</SubTotalAmt><TaxAmt>0.5</TaxAmt><TotalAmt>6.5</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>'
		);

		$this->assertSyncValues(0, '1', 'order', 'Add', '1', '1');
	}
}
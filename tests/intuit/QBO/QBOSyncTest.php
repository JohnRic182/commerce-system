<?php

require_once dirname(__FILE__).'/QBOSyncTestBase.php';

class QBOSyncTest extends QBOSyncTestBase
{
	function test_canCreateQBSync()
	{
		$settings = array();

		$qbsync = $this->getQBSync($settings);

		$this->assertNotNull($qbsync);
		$this->assertTrue($qbsync->isQBO());
	}

	function test_canDisconnect()
	{
		$settings = array();

		$qbsync = $this->getQBSync($settings);

		$qbsync->disconnect();

		$this->assertTrue($this->transport->disconnectCalled);
	}
}
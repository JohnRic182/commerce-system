<?php

class SyncRepositoryTestDouble implements intuit_DataAccess_SyncRepositoryInterface
{
	public $itemsToSync = array();
	public $customersToSync = array();
	public $ordersToSync = array();
	public $orderLinesToSync = array();
	public $itemsMap = array();

	public $intuitSyncValues = array();

	public function clearSyncRecords()
	{
		throw new Exception('Not implemented');
	}

	public function getItemsToSync()
	{
		return $this->itemsToSync;
	}

	public function getItemsMap()
	{
		return $this->itemsMap;
	}

	public function getCustomersToSync()
	{
		return $this->customersToSync;
	}

	public function getOrdersToSync($completed = true)
	{
		return $this->ordersToSync;
	}

	public function getOrderLines($oid)
	{
		if (isset($this->orderLinesToSync[$oid]))
			return $this->orderLinesToSync[$oid];

		return array();
	}

	public function getIntuitIdFor($id, $type)
	{
		throw new Exception('Not implemented');
	}

	public function saveIntuitSync($id, $type, $operation, $intuitId, $setDate = true)
	{
		$this->intuitSyncValues[] = array('id' => $id, 'type' => $type, 'operation' => $operation, 'intuitId' => $intuitId, 'setDate' => $setDate);
	}

	public function updateSetting($name, $value)
	{
		throw new Exception('Not implemented');
	}

	public function orderExists($orderNumber)
	{
		return true;
	}
}
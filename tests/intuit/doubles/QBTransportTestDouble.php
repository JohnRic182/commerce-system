<?php

class QBTransportTestDouble
{
	protected $queryResults = array();
	protected $queryByIdResults = array();

	public $requests = array();

	protected $intuitId = 1;

	public $disconnectCalled = false;

	public $throwDuplicateException = false;

	public function QBQueryById($obj_type, $id)
	{
		$key = $obj_type.'||'.$id;

		if (isset($this->queryByIdResults[$key]))
			return $this->queryByIdResults[$key];

		error_log('no results for query by id: '.$key);
		return null;
	}

	public function QBQuery($obj_type, $fields = array(), $deleted = false)
	{
		$key = $this->getQueryKey($obj_type, $fields, $deleted);

		if (isset($this->queryResults[$key]))
		{
			return $this->queryResults[$key];
		}

		error_log('no results for key: '.$key);
		return array();
	}

	public function QBRequest($obj_type, $fields, $operation)
	{
		if ($this->throwDuplicateException)
		{
			$this->throwDuplicateException = false;
			throw new intuit_DuplicateItemException('Duplicate Testing');
		}
		else
		{
			$this->requests[] = array('obj_type' => $obj_type, 'fields' => $fields, 'operation' => $operation);

			$id = $this->intuitId++;

			$xml = $this->getRequestSuccessXml($id);

			$ret = simplexml_load_string($xml);

			return $this->arrayify($ret);
		}
	}

	protected function getRequestSuccessXml($id)
	{
		return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Result><Id idDomain="QB">'.$id.'</Id></Result>';
	}

	public function addQueryResult($obj_type, array $fields, $deleted, $result)
	{
		$key = $this->getQueryKey($obj_type, $fields, $deleted);

		$this->queryResults[$key] = $result;
	}

	public function addQueryByIdResult($obj_type, $id, $result)
	{
		$key = $obj_type.'||'.$id;

		$this->queryByIdResults[$key] = $result;
	}

	public function disconnectIA()
	{
		$this->disconnectCalled = true;
	}

	protected function getQueryKey($obj_type, array $fields, $deleted)
	{
		return $obj_type.'||'.implode(',', $fields).($deleted ? 'deleted' : '');
	}

	public function arrayify ($xmlObject, $out = array())
	{
		foreach ((array)$xmlObject as $index => $node)
		{
			$out[$index] = (is_object( $node )) ? $this->arrayify ($node) : $node;
		}

		return $out;
	}
}
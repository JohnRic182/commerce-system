<?php

class QBDTransportTestDouble extends QBTransportTestDouble
{

	public function QBRequest($obj_type, $fields, $operation)
	{
		if ($this->throwDuplicateException)
		{
			$this->throwDuplicateException = false;
			throw new intuit_DuplicateItemException('Duplicate Testing');
		}
		else
		{
			$this->requests[] = array('obj_type' => $obj_type, 'fields' => $fields, 'operation' => $operation);

			$id = $this->intuitId++;

			$xml = $this->getRequestSuccessXml($id);

			return simplexml_load_string($xml);
		}
	}

	protected function getRequestSuccessXml($id)
	{
		return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Result><Success><ObjectRef><Id idDomain="QB">'.$id.'</Id></ObjectRef></Success></Result>';
	}
}
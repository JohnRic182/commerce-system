<?php

require_once dirname(__FILE__).'/../doubles/QBTransportTestDouble.php';
require_once dirname(__FILE__).'/../doubles/QBDTransportTestDouble.php';
require_once dirname(__FILE__).'/../doubles/SyncRepositoryTestDouble.php';

class QBDSyncTest extends PHPUnit_Framework_TestCase
{
	protected $syncRepository;
	protected $transport;
	protected $qbConnector;

	protected function setUp()
	{
		$this->syncRepository = new SyncRepositoryTestDouble();
		$this->transport = new QBDTransportTestDouble();
		$this->qbConnector = new intuit_Services_QBDConnector($this->transport, new intuit_Model_Mapping_ModelMapper('QBD'));
	}

	function test_canCreateQBSync()
	{
		$settings = array();

		$qbsync = $this->getQBSync($settings);

		$this->assertNotNull($qbsync);
		$this->assertFalse($qbsync->isQBO());
	}

	function test_canDisconnect()
	{
		$settings = array();

		$qbsync = $this->getQBSync($settings);

		$qbsync->disconnect();

		$this->assertTrue($this->transport->disconnectCalled);
	}

	protected function getQBSync(&$settings)
	{
		$sync = new intuit_Services_QBSync($this->syncRepository, $this->qbConnector, 'QBD', $settings);

		$sync->setCurrencyCode('USD');

		return $sync;
	}

	function test_resyncs_Failed_SalesReceipts()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2, 'intuit_nontaxable_tax_code' => 'Non', 'intuit_taxable_tax_code' => 'Tax');

		$ordersToSync = array();

		$this->syncRepository->ordersToSync = $ordersToSync;

		$xml_res = simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?>
<RestResponse>
	<CompanyPreferences>
<Preferences>
<UsingAccountNumbers>false</UsingAccountNumbers>
<RequiringAccounts>true</RequiringAccounts>
<UsingClasses>true</UsingClasses> 
<UsingMultiCurrency>false</UsingMultiCurrency>
<UsingInventory>true</UsingInventory>
<DaysBillsAreDue>10</DaysBillsAreDue>
<DefaultMarkup>0.00000</DefaultMarkup>
<TrackReimbursableExpenses>false</TrackReimbursableExpenses>
<AutoApplyPayments>true</AutoApplyPayments>
</Preferences>
</CompanyPreferences>
</RestResponse>
');

		$preferencesXml = $xml_res;

		$this->transport->addQueryResult('Preferences', array(), false, $preferencesXml);

 		$accountsXml = $this->getQueryResults(
 			'<?xml version="1.0" encoding="UTF-8" standalone="yes"?><RestResponse><Accounts><Account><Id idDomain="QB">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Type>Revenue</Type></Account><Account><Id idDomain="QB">2</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Expenses</Name><Type>Expense</Type></Account></Accounts></RestResponse>',
 			'Accounts'
 		);

		$this->transport->addQueryResult('Account', array(), false, $accountsXml);

		$salesReceiptsXml = $this->getQueryResults(
			'<?xml version="1.0" encoding="UTF-8" standalone="yes"?><RestResponse><SalesReceipts>
<SalesReceipt>
<Id idDomain="NG">144521</Id>
<SyncToken>1</SyncToken>
<MetaData>
<CreatedBy>app</CreatedBy>
<CreateTime>2013-01-09T23:07:01.0Z</CreateTime>
<LastModifiedBy>app</LastModifiedBy>
<LastUpdatedTime>2013-01-09T23:07:18.0Z</LastUpdatedTime>
</MetaData>
<Synchronized>false</Synchronized>
<Header>
<DocNumber>1</DocNumber>
<TxnDate>2013-01-09</TxnDate>
<CustomerId idDomain="NG">62116</CustomerId>
<SubTotalAmt>0</SubTotalAmt>
<TaxRate>0</TaxRate>
<TaxAmt>0</TaxAmt>
<TotalAmt>0</TotalAmt>
</Header>
<Line>
<Id idDomain="NG">66185</Id>
<Desc>Moshi ClearGuard Keyboard Protector for MacBook Air/Pro</Desc>
<Amount>24.95</Amount>
<Taxable>false</Taxable>
<ItemId idDomain="NG">28674</ItemId>
<Qty>1</Qty>
<SalesTaxCodeName>Non</SalesTaxCodeName>
</Line>
</SalesReceipt>
</SalesReceipts></RestResponse>',
			'SalesReceipts'
		);

		$this->transport->addQueryResult('SalesReceipt', array(), true, $salesReceiptsXml);

		$qbsync = new intuit_Services_QBSync($this->syncRepository, $this->qbConnector, 'QBD', $settings);

		$qbsync->setCurrencyCode('USD');

		$qbsync->syncSalesReceipts();

		$this->assertEquals(1, count($this->transport->requests));

		$this->assertEquals('SalesReceipt', $this->transport->requests[0]['obj_type']);
		$this->assertEquals(
			'<Id idDomain="NG">144521</Id><SyncToken>1</SyncToken><Header><DocNumber>1</DocNumber><TxnDate>2013-01-09</TxnDate><CustomerId idDomain="NG">62116</CustomerId><SubTotalAmt>0</SubTotalAmt><TaxRate>0</TaxRate><TaxAmt>0</TaxAmt><TotalAmt>0</TotalAmt></Header><Line><Id idDomain="NG">66185</Id><Desc>Moshi ClearGuard Keyboard Protector for MacBook Air/Pro</Desc><Amount>24.95</Amount><Taxable>false</Taxable><ItemId idDomain="NG">28674</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>',
			$this->transport->requests[0]['fields']
		);
		$this->assertEquals('Mod', $this->transport->requests[0]['operation']);

		$this->assertEquals(1, count($this->syncRepository->intuitSyncValues));
		$this->assertEquals('1', $this->syncRepository->intuitSyncValues[0]['id']);
		$this->assertEquals('order', $this->syncRepository->intuitSyncValues[0]['type']);
		$this->assertEquals('Mod', $this->syncRepository->intuitSyncValues[0]['operation']);
		$this->assertEquals('1', $this->syncRepository->intuitSyncValues[0]['intuitId']);
		$this->assertEquals('1', $this->syncRepository->intuitSyncValues[0]['setDate']);
	}

	function test_canResyncItem_Add_ExpenseAccount_When_COGSAccount_Exists()
	{
		$settings = array('intuit_income_account' => 1, 'intuit_expense_account' => 2);

		$itemsToSync = array(
			array(
				'intuit_id' => '4',
				'product_subid' => null,
				'product_id' => 'test_prod',
				'title' => 'Test Product',
				'is_taxable' => 'No',
				'price' => '5',
			),
		);

		$syncRepository = new SyncRepositoryTestDouble();
		$syncRepository->itemsToSync = $itemsToSync;

		$transport = new QBTransportTestDouble();
		$qbConnector = new intuit_Services_QBDConnector($transport, new intuit_Model_Mapping_ModelMapper('QBD'));

 		$accountsXml = $this->getQueryResults(
 			'<?xml version="1.0" encoding="UTF-8" standalone="yes"?><RestResponse><Accounts><Account><Id idDomain="QB">1</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Sales</Name><Type>Revenue</Type></Account><Account><Id idDomain="QB">2</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-15T10:29:11-07:00</CreateTime><LastUpdatedTime>2012-08-15T10:29:11-07:00</LastUpdatedTime></MetaData><Name>Expenses</Name><Type>Expense</Type></Account></Accounts></RestResponse>',
 			'Accounts'
 		);

		$transport->addQueryResult('Account', array(), false, $accountsXml);

		$itemsXml = $this->getQueryResults(
			'<?xml version="1.0" encoding="UTF-8" standalone="yes"?><RestResponse><Items><Item><Id idDomain="QB">4</Id><SyncToken>0</SyncToken><MetaData><CreateTime>2012-08-16T11:31:06-07:00</CreateTime><LastUpdatedTime>2012-08-16T11:31:06-07:00</LastUpdatedTime></MetaData><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><COGSAccountRef><AccountId idDomain="QB">3</AccountId></COGSAccountRef></Item></Items></RestResponse>',
			'Items'
		);

		$transport->addQueryResult('Item', array(), false, $itemsXml);

		$qbsync = new intuit_Services_QBSync($syncRepository, $qbConnector, 'QBD', $settings);

		$qbsync->setCurrencyCode('USD');

		$qbsync->syncItems();

		$this->assertEquals(1, count($transport->requests));

		$this->assertEquals('Item', $transport->requests[0]['obj_type']);
		$this->assertEquals('<Id idDomain="QB">4</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">2</AccountId></ExpenseAccountRef><COGSAccountRef><AccountId idDomain="QB">3</AccountId></COGSAccountRef>', trim($transport->requests[0]['fields']));
		$this->assertEquals('Mod', $transport->requests[0]['operation']);

		$this->assertEquals(1, count($syncRepository->intuitSyncValues));
		$this->assertEquals('test_prod', $syncRepository->intuitSyncValues[0]['id']);
		$this->assertEquals('item', $syncRepository->intuitSyncValues[0]['type']);
		$this->assertEquals('Mod', $syncRepository->intuitSyncValues[0]['operation']);
		$this->assertEquals('4', $syncRepository->intuitSyncValues[0]['intuitId']);
		$this->assertEquals('1', $syncRepository->intuitSyncValues[0]['setDate']);
	}

	protected function getBlankItemResultsXml()
	{
		return $this->getQueryResults('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><RestResponse xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:xdb="http://xmlns.oracle.com/xdb" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation ="http://www.intuit.com/sb/cdm/v2 ../common/RestDataFilter.xsd"><Items></Items></RestResponse>');
	}

	protected function getQueryResults($xml, $setName)
	{
		$xml_res = simplexml_load_string($xml);

		return $xml_res->$setName->children();
	}
}
<?php

abstract class ItemTest  extends PHPUnit_Framework_TestCase
{
	function test_canCreateItem()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = $this->createItem();

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 8;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 6;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->canCreateItem_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onUpdate_IncomeAccount_Does_Not_Change()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = $this->createItem($this->getExistingItemXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 8;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = null;

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->onUpdate_IncomeAccount_Does_Not_Change_Xml(),
			$o->toXml()
		);
		$this->assertFalse($o->isModified());
	}

	function test_onUpdate_ExpenseAccount_Does_Not_Change_When_Already_Set()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = $this->createItem($this->getExistingItemXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 8;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->onUpdate_ExpenseAccount_Does_Not_Change_When_Already_Set_Xml(),
			$o->toXml()
		);
		$this->assertFalse($o->isModified());
	}

	function test_onUpdate_ExpenseAccount_Set_When_Not_Set()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = $this->createItem($this->getExistingItemNoExpenseAccountXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 1;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->onUpdate_ExpenseAccount_Set_When_Not_Set_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onUpdate_ExpenseAccount_Set_When_Not_Set_No_IdDomain()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = $this->createItem($this->getExistingItemNoExpenseAccountXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 1;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = null;

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->onUpdate_ExpenseAccount_Set_When_Not_Set_No_IdDomain_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onUpdate_Updates_Name()
	{
		$productRecord = array(
			'product_id' => 'test_prod2',
			'title' => 'Test Product',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = $this->createItem($this->getExistingItemXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 1;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod2', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->onUpdate_Updates_Name_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onUpdate_Updates_Desc()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product2',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = $this->createItem($this->getExistingItemXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 1;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->onUpdate_Updates_Desc_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onUpdate_Updates_Taxable()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product2',
			'price' => '5.00',
			'is_taxable' => true,
		);

		$o = $this->createItem($this->getExistingItemXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 1;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			$this->onUpdate_Updates_Taxable_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onUpdate_Updates_UnitPrice()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product2',
			'price' => '55.50',
			'is_taxable' => false,
		);

		$o = $this->createItem($this->getExistingItemXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 1;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'GBP');

		$this->assertEquals(
			$this->onUpdate_Updates_UnitPrice_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	protected abstract function canCreateItem_Xml();

	protected abstract function onUpdate_IncomeAccount_Does_Not_Change_Xml();

	protected abstract function onUpdate_ExpenseAccount_Does_Not_Change_When_Already_Set_Xml();

	protected abstract function onUpdate_ExpenseAccount_Set_When_Not_Set_Xml();

	protected abstract function onUpdate_ExpenseAccount_Set_When_Not_Set_No_IdDomain_Xml();

	protected abstract function onUpdate_Updates_Name_Xml();

	protected abstract function onUpdate_Updates_Desc_Xml();

	protected abstract function onUpdate_Updates_Taxable_Xml();

	protected abstract function onUpdate_Updates_UnitPrice_Xml();

	protected abstract function createItem($xml = null);

	protected abstract function createAccount($xml = null);

	protected abstract function getDefaultIdDomain();

	protected abstract function getExistingItemXml();

	protected abstract function getExistingItemNoExpenseAccountXml();
}
<?php

abstract class CustomerTest extends PHPUnit_Framework_TestCase
{
	function test_canCreateCustomer()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => null,
			'phone' => null,
			'address1' => null,
			'shipping_address1' => null,
		);

		$customer = $this->createCustomer();

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->canCreateCustomer_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_canCreateCustomer_With_Email()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => 'test@test.com',
			'phone' => null,
			'address1' => null,
			'shipping_address1' => null,
		);

		$customer = $this->createCustomer();

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->canCreateCustomer_With_Email_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_canCreateCustomer_With_Phone()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => null,
			'phone' => '999-999-9999',
			'address1' => null,
			'shipping_address1' => null,
		);

		$customer = $this->createCustomer();

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->canCreateCustomer_With_Phone_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_canCreateCustomer_With_BillingAddress()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => null,
			'phone' => null,
			'address1' => '123 Test Dr',
			'address2' => 'Suite 1A',
			'city' => 'Phoenix',
			'billing_state_name' => 'Arizona',
			'zip' => '85020',
			'company' => 'Test Company',
			'shipping_address1' => null,
		);

		$customer = $this->createCustomer();

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->canCreateCustomer_With_BillingAddress_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_canCreateCustomer_With_ShippingAddress()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => null,
			'phone' => null,
			'address1' => null,
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => 'Suite 2B',
			'shipping_city' => 'Phoenix',
			'shipping_state_name' => 'Arizona',
			'shipping_zip' => '85020',
			'shipping_company' => 'Test Company',
		);

		$customer = $this->createCustomer();

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->canCreateCustomer_With_ShippingAddress_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_onUpdate_No_Update_If_Not_Changed()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => 'test@test.com',
			'phone' => '999-999-9999',
			'address1' => '123 Test Dr',
			'address2' => 'Suite 1A',
			'city' => 'Phoenix',
			'billing_state_name' => 'Arizona',
			'zip' => '85020',
			'company' => 'Test Company',
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => 'Suite 2B',
			'shipping_city' => 'Phoenix',
			'shipping_state_name' => 'Arizona',
			'shipping_zip' => '85020',
			'shipping_company' => 'Test Company',
		);

		$customer = $this->createCustomer($this->getExistingCustomerXml());

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->onUpdate_No_Update_If_Not_Changed_Xml(),
			$customer->toXml()
		);
		$this->assertFalse($customer->isModified());
	}

	function test_onUpdate_Updates_Name()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'John',
			'lname' => 'Doe',
			'email' => 'test@test.com',
			'phone' => '999-999-9999',
			'address1' => '123 Test Dr',
			'address2' => 'Suite 1A',
			'city' => 'Phoenix',
			'billing_state_name' => 'Arizona',
			'zip' => '85020',
			'company' => 'Test Company',
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => 'Suite 2B',
			'shipping_city' => 'Phoenix',
			'shipping_state_name' => 'Arizona',
			'shipping_zip' => '85020',
			'shipping_company' => 'Test Company',
		);

		$customer = $this->createCustomer($this->getExistingCustomerXml());

		$customer->setCustomerDetails($userRecord, 'John Doe [1]');

		$this->assertEquals(
			$this->onUpdate_Updates_Name_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_onUpdate_Updates_Email()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => 'test2@test.com',
			'phone' => '999-999-9999',
			'address1' => '123 Test Dr',
			'address2' => 'Suite 1A',
			'city' => 'Phoenix',
			'billing_state_name' => 'Arizona',
			'zip' => '85020',
			'company' => 'Test Company',
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => 'Suite 2B',
			'shipping_city' => 'Phoenix',
			'shipping_state_name' => 'Arizona',
			'shipping_zip' => '85020',
			'shipping_company' => 'Test Company',
		);

		$customer = $this->createCustomer($this->getExistingCustomerXml());

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->onUpdate_Updates_Email_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_onUpdate_Updates_Phone()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => 'test@test.com',
			'phone' => '555-555-5555',
			'address1' => '123 Test Dr',
			'address2' => 'Suite 1A',
			'city' => 'Phoenix',
			'billing_state_name' => 'Arizona',
			'zip' => '85020',
			'company' => 'Test Company',
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => 'Suite 2B',
			'shipping_city' => 'Phoenix',
			'shipping_state_name' => 'Arizona',
			'shipping_zip' => '85020',
			'shipping_company' => 'Test Company',
		);

		$customer = $this->createCustomer($this->getExistingCustomerXml());

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->onUpdate_Updates_Phone_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_onUpdate_Updates_BillingAddress()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => 'test@test.com',
			'phone' => '999-999-9999',
			'address1' => '456 Testing Ave',
			'address2' => 'Suite 292',
			'city' => 'Nowhere',
			'billing_state_name' => 'Kansas',
			'zip' => '67054',
			'company' => 'Awesome',
			'shipping_address1' => '123 Test Dr',
			'shipping_address2' => 'Suite 2B',
			'shipping_city' => 'Phoenix',
			'shipping_state_name' => 'Arizona',
			'shipping_zip' => '85020',
			'shipping_company' => 'Test Company',
		);

		$customer = $this->createCustomer($this->getExistingCustomerXml());

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->onUpdate_Updates_BillingAddress_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_onUpdate_Updates_ShippingAddress()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => 'test@test.com',
			'phone' => '999-999-9999',
			'address1' => '123 Test Dr',
			'address2' => 'Suite 1A',
			'city' => 'Phoenix',
			'billing_state_name' => 'Arizona',
			'zip' => '85020',
			'company' => 'Test Company',
			'shipping_address1' => '456 Testing Ave',
			'shipping_address2' => 'Suite 292',
			'shipping_city' => 'Nowhere',
			'shipping_state_name' => 'Kansas',
			'shipping_zip' => '67054',
			'shipping_company' => 'Awesome',
		);

		$customer = $this->createCustomer($this->getExistingCustomerXml());

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->onUpdate_Updates_ShippingAddress_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	function test_onUpdate_Adds_BillingAddress()
	{
		$userRecord = array(
			'uid' => 1,
			'fname' => 'Test',
			'lname' => 'Tester',
			'email' => 'test@test.com',
			'phone' => '999-999-9999',
			'address1' => '456 Testing Ave',
			'address2' => 'Suite 292',
			'city' => 'Nowhere',
			'billing_state_name' => 'Kansas',
			'zip' => '67054',
			'company' => 'Awesome',
			'shipping_address1' => null,
		);

		$customer = $this->createCustomer($this->getExistingCustomerNoAddressesXml());

		$customer->setCustomerDetails($userRecord, 'Test Tester [1]');

		$this->assertEquals(
			$this->onUpdate_Adds_BillingAddress_Xml(),
			$customer->toXml()
		);
		$this->assertTrue($customer->isModified());
	}

	protected abstract function createCustomer($xml = null);

	protected abstract function canCreateCustomer_Xml();

	protected abstract function canCreateCustomer_With_Email_Xml();

	protected abstract function canCreateCustomer_With_Phone_Xml();

	protected abstract function canCreateCustomer_With_BillingAddress_Xml();

	protected abstract function canCreateCustomer_With_ShippingAddress_Xml();

	protected abstract function onUpdate_No_Update_If_Not_Changed_Xml();

	protected abstract function onUpdate_Updates_Name_Xml();

	protected abstract function onUpdate_Updates_Email_Xml();

	protected abstract function onUpdate_Updates_Phone_Xml();

	protected abstract function onUpdate_Updates_BillingAddress_Xml();

	protected abstract function onUpdate_Updates_ShippingAddress_Xml();

	protected abstract function onUpdate_Adds_BillingAddress_Xml();

	protected abstract function getExistingCustomerXml();

	protected abstract function getExistingCustomerNoAddressesXml();

/*
$userRecord = array(
	'uid' => 1,
	'fname' => 'Test',
	'lname' => 'Tester',
	'email' => null, //'test@test.com',
	'phone' => null, //'999-999-9999',
	'address1' => null, //'123 Test Dr',
	'address2' => '',
	'city' => 'Phoenix',
	'billing_state_name' => 'Arizona',
	'zip' => '85020',
	'company' => 'Test Company',
	'shipping_address1' => null, //'123 Test Dr',
	'shipping_address2' => '',
	'shipping_city' => 'Phoenix',
	'shipping_state_name' => 'Arizona',
	'shipping_zip' => '85020',
	'shipping_company' => 'Test Company',
);
 */
}
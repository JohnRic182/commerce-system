<?php

require_once dirname(dirname(__FILE__)).'/SalesReceiptTest.php';

class QBO_SalesReceiptTest extends SalesReceiptTest
{
	function test_onCreate_Shipping_Item_Doesnt_Exist_Returns_False()
	{
		$this->markTestSkipped();
	}

	protected function canCreateSalesReceipt_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>0</TaxAmt><TotalAmt>5.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_TaxInformation_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>6.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Discount_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>5.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Discount</Desc><Amount>-1</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">2</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_PromoDiscount_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>5.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Promo Code</Desc><Amount>-1</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">3</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Handling_When_Handline_Separated_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>7.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Handling</Desc><Amount>1</Amount><Taxable>false</Taxable><ItemId idDomain="QBO">4</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Shipping_When_Handling_Not_Separated_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>7.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Shipping</Desc><Amount>1</Amount><Taxable>false</Taxable><ItemId>SHIPPING_LINE_ID</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>8.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Shipping</Desc><Amount>2</Amount><Taxable>false</Taxable><ItemId>SHIPPING_LINE_ID</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Shipping_Taxable_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.10</TaxAmt><TotalAmt>8.10</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Shipping</Desc><Amount>2</Amount><Taxable>true</Taxable><ItemId>SHIPPING_LINE_ID</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Handling</Desc><Amount>1</Amount><Taxable>false</Taxable><ItemId idDomain="QBO">4</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function canHave_2_products_when_1_promotion_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>6.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Test Product</Desc><Amount>0</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function can_have_line_with_sub_product_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SalesTaxCodeId>1</SalesTaxCodeId><SubTotalAmt>5.00</SubTotalAmt><TaxAmt>1.00</TaxAmt><TotalAmt>6.00</TotalAmt></Header><Line><Desc>Test Product
Options:

Size: Small
</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onUpdate_Updates_TotalAmount_Xml()
	{
		return '<Id idDomain="QBO">1</Id><SyncToken>0</SyncToken><Header><DocNumber>123</DocNumber><TxnDate>2012-08-16-07:00</TxnDate><CustomerId idDomain="QBO">17</CustomerId><SubTotalAmt>74.99</SubTotalAmt><TaxAmt>0</TaxAmt><TotalAmt>75.99</TotalAmt><ToBePrinted>false</ToBePrinted><ToBeEmailed>false</ToBeEmailed><ShipAddr><Line1>123 Test Dr</Line1><City>Phoenix</City><CountrySubDivisionCode>AZ</CountrySubDivisionCode><PostalCode>85027</PostalCode><Tag>CUSTOMER</Tag></ShipAddr><DepositToAccountId idDomain="QBO">4</DepositToAccountId><DiscountTaxable>true</DiscountTaxable></Header><Line><Id>1</Id><Desc>SAMSUNG SpinPoint 400GB  Hard Drive</Desc><Amount>74.99</Amount><Taxable>true</Taxable><ItemId idDomain="QBO">10</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Amount>1.00</Amount><ItemId idDomain="QBO">SHIPPING_LINE_ID</ItemId><Desc>Shipping</Desc><Qty>1</Qty><Taxable>false</Taxable><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function getExistingSalesReceiptXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<SalesReceipt xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:qbp="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:qbo="http://www.intuit.com/sb/cdm/qbo">
	<Id idDomain="QBO">1</Id>
	<SyncToken>0</SyncToken>
	<MetaData>
		<CreateTime>2012-09-13T09:54:30-07:00</CreateTime>
		<LastUpdatedTime>2012-09-13T09:54:30-07:00</LastUpdatedTime>
	</MetaData>
	<Header>
		<DocNumber>123</DocNumber>
		<TxnDate>2012-08-16-07:00</TxnDate>
		<CustomerId idDomain="QBO">17</CustomerId>
		<SubTotalAmt>84.99</SubTotalAmt>
		<TaxAmt>0</TaxAmt>
		<TotalAmt>85.99</TotalAmt>
		<ToBePrinted>false</ToBePrinted>
		<ToBeEmailed>false</ToBeEmailed>
		<ShipAddr>
			<Line1>123 Test Dr</Line1>
			<City>Phoenix</City>
			<CountrySubDivisionCode>AZ</CountrySubDivisionCode>
			<PostalCode>85027</PostalCode>
			<Tag>CUSTOMER</Tag>
		</ShipAddr>
		<ShipMethodId idDomain="QBO"/>
		<DepositToAccountId idDomain="QBO">4</DepositToAccountId>
		<DepositToAccountName>Undeposited Funds</DepositToAccountName>
		<DiscountTaxable>true</DiscountTaxable>
	</Header>
	<Line>
		<Id>1</Id>
		<Desc>SAMSUNG SpinPoint 400GB  Hard Drive</Desc>
		<Amount>84.99</Amount>
		<Taxable>true</Taxable>
		<ItemId idDomain="QBO">10</ItemId>
		<Qty>1</Qty>
		<SalesTaxCodeName>Tax</SalesTaxCodeName>
	</Line>
	<Line>
		<Amount>1.00</Amount>
		<ItemId idDomain="QBO">SHIPPING_LINE_ID</ItemId>
		<Desc>Shipping</Desc>
		<Qty>1</Qty>
		<Taxable>false</Taxable>
		<SalesTaxCodeName>Non</SalesTaxCodeName>
	</Line>
</SalesReceipt>'
);
	}

	protected function getCustomer($xml = null)
	{
		if (is_null($xml))
		{
			$xml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Customer>
	<Id idDomain="QBO">17</Id>
	<SyncToken>0</SyncToken>
	<Name>Test Tester [1]</Name>
	<Address>
		<Line1>Test Company</Line1>
		<Line2>123 Test Dr</Line2>
		<Line3>Suite 1A</Line3>
		<City>Phoenix</City>
		<CountrySubDivisionCode>Arizona</CountrySubDivisionCode>
		<PostalCode>85020</PostalCode>
		<Tag>Billing</Tag>
	</Address>
	<Address>
		<Line1>Test Company</Line1>
		<Line2>123 Test Dr</Line2>
		<Line3>Suite 2B</Line3>
		<City>Phoenix</City>
		<CountrySubDivisionCode>Arizona</CountrySubDivisionCode>
		<PostalCode>85020</PostalCode>
		<Tag>Shipping</Tag>
	</Address>
	<Phone><FreeFormNumber>999-999-9999</FreeFormNumber></Phone>
	<Email><Address>test@test.com</Address></Email>
	<GivenName>Test</GivenName>
	<FamilyName>Tester</FamilyName>
	<ShowAs>Test Tester</ShowAs>
</Customer>');
		}

		return new intuit_Model_QBO_Customer($xml);
	}

	protected function createSalesReceipt($xml = null)
	{
		return new intuit_Model_QBO_SalesReceipt($xml);
	}

	protected function getDefaultItemXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QBO">1</Id>
	<SyncToken>0</SyncToken>
	<Name>test_prod</Name>
	<Desc>Test Product</Desc>
	<Taxable>false</Taxable>
	<UnitPrice><Amount>5.00</Amount></UnitPrice>
</Item>
');
	}

	protected function getDefaultItem()
	{
		$item = new intuit_Model_QBO_Item();
		$item->Id = 1;
		$item->Id_idDomain = 'QBO';

		return $item;
	}

	protected function getDiscountItem()
	{
		$item = new intuit_Model_QBO_Item();
		$item->Id = 2;
		$item->Id_idDomain = 'QBO';
		$item->Name = 'Discount';

		return $item;
	}

	protected function getPromDiscountItem()
	{
		$item = new intuit_Model_QBO_Item();
		$item->Id = 3;
		$item->Id_idDomain = 'QBO';
		$item->Name = 'Promo Discount';

		return $item;
	}

	protected function getHandlingItem()
	{
		$item = new intuit_Model_QBO_Item();
		$item->Id = 4;
		$item->Id_idDomain = 'QBO';
		$item->Name = 'Handling';

		return $item;
	}

	protected function getShippingItem()
	{
		$item = new intuit_Model_QBO_Item();
		$item->Id = 5;
		$item->Id_idDomain = 'QBO';
		$item->Name = 'Shipping';

		return $item;
	}

	protected function getHardDriveItem()
	{
		return new intuit_Model_QBO_Item(simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QBO">10</Id>
	<SyncToken>0</SyncToken>
	<ItemParentId/>
	<ItemParentName/>
	<Name>test_samsung_spinpoint_400</Name>
	<Desc>SAMSUNG SpinPoint 400GB  Hard Drive</Desc>
	<Taxable>true</Taxable>
	<UnitPrice><Amount>84.99</Amount></UnitPrice>
	<IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>
	<PurchaseDesc/>
	<PurchaseCost/>
	<ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>
</Item>'));
	}
}
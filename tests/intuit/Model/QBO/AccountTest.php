<?php

class QBO_AccountModelTest extends PHPUnit_Framework_TestCase
{
	function test_can_create_QBO_Account()
	{
		$account = new intuit_Model_QBO_Account();

		$this->assertNotNull($account);
	}

	function test_can_load_QBO_Account_From_Xml()
	{
		$xml = $this->getQBOAccountXml();

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertEquals('SalesOfProductIncome', $account->getValue('Subtype'));
		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
		$this->assertEquals(1, $account->getValue('Id'));
		$this->assertEquals('QBO', $account->getIdDomain());
	}

	function test_Subtype_SuppliesMaterials_Is_ExpenseAccount()
	{
		$xml = $this->getQBOAccountXml('SuppliesMaterials');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertFalse($account->isIncomeAccount());
		$this->assertTrue($account->isExpenseAccount());
	}

	function test_Subtype_SuppliesMaterialsCogs_Is_ExpenseAccount()
	{
		$xml = $this->getQBOAccountXml('SuppliesMaterialsCogs');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertFalse($account->isIncomeAccount());
		$this->assertTrue($account->isExpenseAccount());
	}

	function test_Subtype_Inventory_Is_ExpenseAccount()
	{
		$xml = $this->getQBOAccountXml('Inventory');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertFalse($account->isIncomeAccount());
		$this->assertTrue($account->isExpenseAccount());
	}

	function test_Subtype_SalesOfProductIncome_Is_IncomeAccount()
	{
		$xml = $this->getQBOAccountXml('SalesOfProductIncome');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
	}

	function test_Subtype_ServiceFeeIncome_Is_IncomeAccount()
	{
		$xml = $this->getQBOAccountXml('ServiceFeeIncome');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
	}

	function test_Subtype_NonProfitIncome_Is_IncomeAccount()
	{
		$xml = $this->getQBOAccountXml('NonProfitIncome');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
	}

	function test_Subtype_OtherMiscellaneousIncome_Is_IncomeAccount()
	{
		$xml = $this->getQBOAccountXml('OtherMiscellaneousIncome');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
	}

	function test_Subtype_OtherPrimaryIncome_Is_IncomeAccount()
	{
		$xml = $this->getQBOAccountXml('OtherPrimaryIncome');

		$account = new intuit_Model_QBO_Account($xml);

		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
	}

	protected function getQBOAccountXml($subtype = 'SalesOfProductIncome')
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Account>
	<Id idDomain="QBO">1</Id>
	<SyncToken>0</SyncToken>
	<MetaData>
		<CreateTime>2012-12-05</CreateTime>
		<LastUpdatedTime>2012-12-05</LastUpdatedTime>
	</MetaData>
	<Name>Test Account</Name>
	<AccountParentId/>
	<Desc>Test Account</Desc>
	<Subtype>'.$subtype.'</Subtype>
	<AcctNum>1234</AcctNum>
	<OpeningBalance>1212.25</OpeningBalance>
	<OpeningBalanceDate>2012-12-05</OpeningBalanceDate>
	<CurrentBalance>1021.23</CurrentBalance>
</Account>');
	}
}
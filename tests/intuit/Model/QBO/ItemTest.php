<?php

require_once dirname(dirname(__FILE__)).'/ItemTest.php';

class QBO_ItemTest extends ItemTest
{
	protected function canCreateItem_Xml()
	{
		return '<Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">8</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">6</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_IncomeAccount_Does_Not_Change_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_ExpenseAccount_Does_Not_Change_When_Already_Set_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_ExpenseAccount_Set_When_Not_Set_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">5</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_ExpenseAccount_Set_When_Not_Set_No_IdDomain_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId>5</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_Name_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod2</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_Desc_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>false</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_Taxable_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>true</Taxable><UnitPrice><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_UnitPrice_Xml()
	{
		return '<Id idDomain="QBO">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>false</Taxable><UnitPrice><Amount>55.50</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>';
	}

	protected function createItem($xml = null)
	{
		return new intuit_Model_QBO_Item($xml);
	}

	protected function createAccount($xml = null)
	{
		return new intuit_Model_QBO_Account($xml);
	}

	protected function getDefaultIdDomain()
	{
		return 'QBO';
	}

	protected function getExistingItemXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QBO">17</Id>
	<SyncToken>0</SyncToken>
	<ItemParentId/>
	<ItemParentName/>
	<Name>test_prod</Name>
	<Desc>Test Product</Desc>
	<Taxable>false</Taxable>
	<UnitPrice><Amount>5</Amount></UnitPrice>
	<IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>
	<PurchaseDesc/>
	<PurchaseCost/>
	<ExpenseAccountRef><AccountId idDomain="QBO">4</AccountId></ExpenseAccountRef>
</Item>');
	}

	protected function getExistingItemNoExpenseAccountXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QBO">17</Id>
	<SyncToken>0</SyncToken>
	<ItemParentId/>
	<ItemParentName/>
	<Name>test_prod</Name>
	<Desc>Test Product</Desc>
	<Taxable>false</Taxable>
	<UnitPrice><Amount>5</Amount></UnitPrice>
	<IncomeAccountRef><AccountId idDomain="QBO">1</AccountId></IncomeAccountRef>
	<PurchaseDesc/>
	<PurchaseCost/>
</Item>');
	}
}
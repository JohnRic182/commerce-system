<?php

class QBD_AccountModelTest extends PHPUnit_Framework_TestCase
{
	function test_can_create_QBD_Account()
	{
		$account = new intuit_Model_QBD_Account();

		$this->assertNotNull($account);
	}

	function test_can_load_QBD_Account_From_Xml()
	{
		$xml = $this->getQBDAccountXml();

		$account = new intuit_Model_QBD_Account($xml);

		$this->assertEquals('Income', $account->getValue('Subtype'));
		$this->assertEquals('Revenue', $account->getValue('Type'));
		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
		$this->assertEquals(30, $account->getValue('Id'));
		$this->assertEquals('QB', $account->getIdDomain());
	}

	function test_QBD_Type_Expense_Is_ExpenseAccount()
	{
		$xml = $this->getQBDAccountXml('Expense', 'Expense');

		$account = new intuit_Model_QBD_Account($xml);

		$this->assertFalse($account->isIncomeAccount());
		$this->assertTrue($account->isExpenseAccount());
	}

	function test_QBD_Type_Revenue_Is_IncomeAccount()
	{
		$xml = $this->getQBDAccountXml('Revenue', 'OtherMiscellaneousIncome');

		$account = new intuit_Model_QBD_Account($xml);

		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
	}

	function test_QBD_Sbutype_Income_Is_IncomeAccount()
	{
		$xml = $this->getQBDAccountXml('Income', 'Income');

		$account = new intuit_Model_QBD_Account($xml);

		$this->assertTrue($account->isIncomeAccount());
		$this->assertFalse($account->isExpenseAccount());
	}

	protected function getQBDAccountXml($type = 'Revenue', $subtype = 'Income')
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Account>
	<Id idDomain="QB">30</Id>
	<SyncToken>1</SyncToken>
	<MetaData>
		<CreatedBy>app</CreatedBy>
		<CreatedById>1</CreatedById>
		<CreateTime>2012-12-21T18:05:09.0Z</CreateTime>
		<LastModifiedBy>app</LastModifiedBy>
		<LastModifiedById>1</LastModifiedById>
		<LastUpdatedTime>2012-12-21T18:05:09.0Z</LastUpdatedTime>
	</MetaData>
	<ExternalKey idDomain="QB">30</ExternalKey>
	<Synchronized>true</Synchronized>
	<AlternateId/>
	<CustomField/>
	<Draft/>
	<ObjectState/>
	<Name>Owners Equity</Name>
	<AccountParentId/>
	<AccountParentName/>
	<Desc>Monies invested in the business by the owner, and profits kept in company accounts</Desc>
	<Active>true</Active>
	<Type>'.$type.'</Type>
	<Subtype>'.$subtype.'</Subtype>
	<AcctNum>1234</AcctNum>
	<BankNum/>
	<RoutingNum/>
	<OpeningBalance>1212.25</OpeningBalance>
	<OpeningBalanceDate>2012-12-05</OpeningBalanceDate>
	<CurrentBalance>1021.23</CurrentBalance>
	<CurrentBalanceWithSubAccounts/>
</Account>');
	}
}
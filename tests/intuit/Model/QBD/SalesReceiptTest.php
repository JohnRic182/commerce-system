<?php

require_once dirname(dirname(__FILE__)).'/SalesReceiptTest.php';

class QBD_SalesReceiptTest extends SalesReceiptTest
{
	protected function canCreateSalesReceipt_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart NON TaxCode Ref</TaxName><TaxAmt>0</TaxAmt><TotalAmt>5.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_TaxInformation_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>6.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Discount_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>5.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Discount</Desc><Amount>-1</Amount><Taxable>true</Taxable><ItemId idDomain="QB">2</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_PromoDiscount_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>5.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Promo Code</Desc><Amount>-1</Amount><Taxable>true</Taxable><ItemId idDomain="QB">3</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Handling_When_Handline_Separated_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>7.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Handling</Desc><Amount>1</Amount><Taxable>false</Taxable><ItemId idDomain="QB">4</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Shipping_When_Handling_Not_Separated_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>7.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Shipping</Desc><Amount>1</Amount><Taxable>false</Taxable><ItemId idDomain="QB">5</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>8.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Shipping</Desc><Amount>2</Amount><Taxable>false</Taxable><ItemId idDomain="QB">5</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Shipping_Taxable_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.10</TaxAmt><TotalAmt>8.10</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Shipping</Desc><Amount>2</Amount><Taxable>true</Taxable><ItemId idDomain="QB">5</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Handling</Desc><Amount>1</Amount><Taxable>false</Taxable><ItemId idDomain="QB">4</ItemId><Qty>1</Qty><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function canHave_2_products_when_1_promotion_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>6.00</TotalAmt></Header><Line><Desc>Test Product</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Desc>Test Product</Desc><Amount>0</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function can_have_line_with_sub_product_Xml()
	{
		return '<Header><DocNumber>1</DocNumber><TxnDate>2012-12-25</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>5.00</SubTotalAmt><TaxName>Pinnacle Cart TAX TaxCode Ref</TaxName><TaxAmt>1.00</TaxAmt><TotalAmt>6.00</TotalAmt></Header><Line><Desc>Test Product
Options:

Size: Small
</Desc><Amount>5</Amount><Taxable>true</Taxable><ItemId idDomain="QB">1</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line>';
	}

	protected function onUpdate_Updates_TotalAmount_Xml()
	{
		return '<Id idDomain="QB">1</Id><SyncToken>0</SyncToken><Header><DocNumber>123</DocNumber><TxnDate>2012-08-16-07:00</TxnDate><CustomerId idDomain="QB">17</CustomerId><SubTotalAmt>74.99</SubTotalAmt><TaxName>Pinnacle Cart NON TaxCode Ref</TaxName><TaxAmt>0</TaxAmt><TotalAmt>75.99</TotalAmt><ToBePrinted>false</ToBePrinted><ToBeEmailed>false</ToBeEmailed><ShipAddr><Line1>123 Test Dr</Line1><City>Phoenix</City><CountrySubDivisionCode>AZ</CountrySubDivisionCode><PostalCode>85027</PostalCode><Tag>CUSTOMER</Tag></ShipAddr><DepositToAccountId idDomain="QB">4</DepositToAccountId><DepositToAccountName>Undeposited Funds</DepositToAccountName><DiscountTaxable>true</DiscountTaxable></Header><Line><Id>1</Id><Desc>SAMSUNG SpinPoint 400GB  Hard Drive</Desc><Amount>74.99</Amount><Taxable>true</Taxable><ItemId idDomain="QB">10</ItemId><Qty>1</Qty><SalesTaxCodeName>Tax</SalesTaxCodeName></Line><Line><Amount>1.00</Amount><ItemId idDomain="QB">5</ItemId><Desc>Shipping</Desc><Qty>1</Qty><Taxable>false</Taxable><SalesTaxCodeName>Non</SalesTaxCodeName></Line>';
	}

	protected function getExistingSalesReceiptXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<SalesReceipt>
	<Id idDomain="QB">1</Id>
	<SyncToken>0</SyncToken>
	<MetaData>
		<CreateTime>2012-09-13T09:54:30-07:00</CreateTime>
		<LastUpdatedTime>2012-09-13T09:54:30-07:00</LastUpdatedTime>
	</MetaData>
	<Header>
		<DocNumber>123</DocNumber>
		<TxnDate>2012-08-16-07:00</TxnDate>
		<CustomerId idDomain="QB">17</CustomerId>
		<SubTotalAmt>84.99</SubTotalAmt>
		<TaxName>Pinnacle Cart NON TaxCode Ref</TaxName>
		<TaxAmt>0</TaxAmt>
		<TotalAmt>85.99</TotalAmt>
		<ToBePrinted>false</ToBePrinted>
		<ToBeEmailed>false</ToBeEmailed>
		<ShipAddr>
			<Line1>123 Test Dr</Line1>
			<City>Phoenix</City>
			<CountrySubDivisionCode>AZ</CountrySubDivisionCode>
			<PostalCode>85027</PostalCode>
			<Tag>CUSTOMER</Tag>
		</ShipAddr>
		<ShipMethodId idDomain="QB"/>
		<DepositToAccountId idDomain="QB">4</DepositToAccountId>
		<DepositToAccountName>Undeposited Funds</DepositToAccountName>
		<DiscountTaxable>true</DiscountTaxable>
	</Header>
	<Line>
		<Id>1</Id>
		<Desc>SAMSUNG SpinPoint 400GB  Hard Drive</Desc>
		<Amount>84.99</Amount>
		<Taxable>true</Taxable>
		<ItemId idDomain="QB">10</ItemId>
		<Qty>1</Qty>
		<SalesTaxCodeName>Tax</SalesTaxCodeName>
	</Line>
	<Line>
		<Amount>1.00</Amount>
		<ItemId idDomain="QB">5</ItemId>
		<Desc>Shipping</Desc>
		<Qty>1</Qty>
		<Taxable>false</Taxable>
		<SalesTaxCodeName>Non</SalesTaxCodeName>
	</Line>
</SalesReceipt>'
);
	}

	protected function getCustomer($xml = null)
	{
		if (is_null($xml))
		{
			$xml = simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Customer>
	<Id idDomain="QB">17</Id>
	<SyncToken>0</SyncToken>
	<TypeOf>Person</TypeOf>
	<Name>Test Tester [1]</Name>
	<Address>
		<Line1>Test Company</Line1>
		<Line2>123 Test Dr</Line2>
		<Line3>Suite 1A</Line3>
		<City>Phoenix</City>
		<CountrySubDivisionCode>Arizona</CountrySubDivisionCode>
		<PostalCode>85020</PostalCode>
		<Tag>Billing</Tag>
	</Address>
	<Address>
		<Line1>Test Company</Line1>
		<Line2>123 Test Dr</Line2>
		<Line3>Suite 2B</Line3>
		<City>Phoenix</City>
		<CountrySubDivisionCode>Arizona</CountrySubDivisionCode>
		<PostalCode>85020</PostalCode>
		<Tag>Shipping</Tag>
	</Address>
	<Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone>
	<Email><Address>test@test.com</Address><Tag>Home</Tag></Email>
	<GivenName>Test</GivenName>
	<FamilyName>Tester</FamilyName>
	<ShowAs>Test Tester</ShowAs>
</Customer>');
		}

		return new intuit_Model_QBD_Customer($xml);
	}

	protected function createSalesReceipt($xml = null)
	{
		return new intuit_Model_QBD_SalesReceipt($xml);
	}

	protected function getDefaultItemXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QB">1</Id>
	<SyncToken>0</SyncToken>
	<Name>test_prod</Name>
	<Desc>Test Product</Desc>
	<Taxable>false</Taxable>
	<UnitPrice><Amount>5.00</Amount></UnitPrice>
</Item>
');
	}

	protected function getDefaultItem()
	{
		$item = new intuit_Model_QBD_Item();
		$item->Id = 1;
		$item->Id_idDomain = 'QB';

		return $item;
	}

	protected function getDiscountItem()
	{
		$item = new intuit_Model_QBD_Item();
		$item->Id = 2;
		$item->Id_idDomain = 'QB';
		$item->Name = 'Discount';

		return $item;
	}

	protected function getPromDiscountItem()
	{
		$item = new intuit_Model_QBD_Item();
		$item->Id = 3;
		$item->Id_idDomain = 'QB';
		$item->Name = 'Promo Discount';

		return $item;
	}

	protected function getHandlingItem()
	{
		$item = new intuit_Model_QBD_Item();
		$item->Id = 4;
		$item->Id_idDomain = 'QB';
		$item->Name = 'Handling';

		return $item;
	}

	protected function getShippingItem()
	{
		$item = new intuit_Model_QBD_Item();
		$item->Id = 5;
		$item->Id_idDomain = 'QB';
		$item->Name = 'Shipping';

		return $item;
	}

	protected function getHardDriveItem()
	{
		return new intuit_Model_QBD_Item(simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QB">10</Id>
	<SyncToken>0</SyncToken>
	<ItemParentId/>
	<ItemParentName/>
	<Name>test_samsung_spinpoint_400</Name>
	<Desc>SAMSUNG SpinPoint 400GB  Hard Drive</Desc>
	<Taxable>true</Taxable>
	<UnitPrice><Amount>84.99</Amount></UnitPrice>
	<IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef>
	<PurchaseDesc/>
	<PurchaseCost/>
	<ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>
</Item>'));
	}
}
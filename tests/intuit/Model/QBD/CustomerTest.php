<?php

require_once dirname(dirname(__FILE__)).'/CustomerTest.php';

class QBD_CustomerTest extends CustomerTest
{
	protected function createCustomer($xml = null)
	{
		return new intuit_Model_QBD_Customer($xml);
	}

	protected function canCreateCustomer_Xml()
	{
		return '<TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function canCreateCustomer_With_Email_Xml()
	{
		return '<TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Email><Address>test@test.com</Address><Tag>Home</Tag></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function canCreateCustomer_With_Phone_Xml()
	{
		return '<TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function canCreateCustomer_With_BillingAddress_Xml()
	{
		return '<TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 1A</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Billing</Tag></Address><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function canCreateCustomer_With_ShippingAddress_Xml()
	{
		return '<TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2B</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Shipping</Tag></Address><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function onUpdate_No_Update_If_Not_Changed_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 1A</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Billing</Tag></Address><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2B</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone><Email><Address>test@test.com</Address><Tag>Home</Tag></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function onUpdate_Updates_Name_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><TypeOf>Person</TypeOf><Name>John Doe [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 1A</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Billing</Tag></Address><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2B</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone><Email><Address>test@test.com</Address><Tag>Home</Tag></Email><GivenName>John</GivenName><FamilyName>Doe</FamilyName><ShowAs>John Doe</ShowAs>';
	}

	protected function onUpdate_Updates_Email_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 1A</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Billing</Tag></Address><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2B</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone><Email><Address>test2@test.com</Address><Tag>Home</Tag></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function onUpdate_Updates_Phone_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 1A</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Billing</Tag></Address><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2B</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>555-555-5555</FreeFormNumber><Tag>Home</Tag></Phone><Email><Address>test@test.com</Address><Tag>Home</Tag></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function onUpdate_Updates_BillingAddress_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Awesome</Line1><Line2>456 Testing Ave</Line2><Line3>Suite 292</Line3><City>Nowhere</City><CountrySubDivisionCode>Kansas</CountrySubDivisionCode><PostalCode>67054</PostalCode><Tag>Billing</Tag></Address><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 2B</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone><Email><Address>test@test.com</Address><Tag>Home</Tag></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function onUpdate_Updates_ShippingAddress_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Test Company</Line1><Line2>123 Test Dr</Line2><Line3>Suite 1A</Line3><City>Phoenix</City><CountrySubDivisionCode>Arizona</CountrySubDivisionCode><PostalCode>85020</PostalCode><Tag>Billing</Tag></Address><Address><Line1>Awesome</Line1><Line2>456 Testing Ave</Line2><Line3>Suite 292</Line3><City>Nowhere</City><CountrySubDivisionCode>Kansas</CountrySubDivisionCode><PostalCode>67054</PostalCode><Tag>Shipping</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone><Email><Address>test@test.com</Address><Tag>Home</Tag></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function onUpdate_Adds_BillingAddress_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><TypeOf>Person</TypeOf><Name>Test Tester [1]</Name><Address><Line1>Awesome</Line1><Line2>456 Testing Ave</Line2><Line3>Suite 292</Line3><City>Nowhere</City><CountrySubDivisionCode>Kansas</CountrySubDivisionCode><PostalCode>67054</PostalCode><Tag>Billing</Tag></Address><Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone><Email><Address>test@test.com</Address><Tag>Home</Tag></Email><GivenName>Test</GivenName><FamilyName>Tester</FamilyName><ShowAs>Test Tester</ShowAs>';
	}

	protected function getExistingCustomerXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Customer>
	<Id idDomain="QB">17</Id>
	<SyncToken>0</SyncToken>
	<TypeOf>Person</TypeOf>
	<Name>Test Tester [1]</Name>
	<Address>
		<Line1>Test Company</Line1>
		<Line2>123 Test Dr</Line2>
		<Line3>Suite 1A</Line3>
		<City>Phoenix</City>
		<CountrySubDivisionCode>Arizona</CountrySubDivisionCode>
		<PostalCode>85020</PostalCode>
		<Tag>Billing</Tag>
	</Address>
	<Address>
		<Line1>Test Company</Line1>
		<Line2>123 Test Dr</Line2>
		<Line3>Suite 2B</Line3>
		<City>Phoenix</City>
		<CountrySubDivisionCode>Arizona</CountrySubDivisionCode>
		<PostalCode>85020</PostalCode>
		<Tag>Shipping</Tag>
	</Address>
	<Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone>
	<Email><Address>test@test.com</Address><Tag>Home</Tag></Email>
	<GivenName>Test</GivenName>
	<FamilyName>Tester</FamilyName>
	<ShowAs>Test Tester</ShowAs>
</Customer>');
	}

	protected function getExistingCustomerNoAddressesXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Customer>
	<Id idDomain="QB">17</Id>
	<SyncToken>0</SyncToken>
	<TypeOf>Person</TypeOf>
	<Name>Test Tester [1]</Name>
	<Phone><FreeFormNumber>999-999-9999</FreeFormNumber><Tag>Home</Tag></Phone>
	<Email><Address>test@test.com</Address><Tag>Home</Tag></Email>
	<GivenName>Test</GivenName>
	<FamilyName>Tester</FamilyName>
	<ShowAs>Test Tester</ShowAs>
</Customer>');
	}
}
<?php

require_once dirname(dirname(__FILE__)).'/ItemTest.php';

class QBD_ItemTest extends ItemTest
{
	function test_onUpdate_ExpenseAccount_Set_When_Not_Set_And_Cogs_Account_Set()
	{
		$productRecord = array(
			'product_id' => 'test_prod',
			'title' => 'Test Product',
			'price' => '5.00',
			'is_taxable' => false,
		);

		$o = new intuit_Model_QBD_Item($this->getExistingItemNoExpenseAccountAndHasCogsAccountXml());

		$incomeAccount = $this->createAccount();
		$incomeAccount->Id = 1;
		$incomeAccount->Id_idDomain = $this->getDefaultIdDomain();

		$expenseAccount = $this->createAccount();
		$expenseAccount->Id = 5;
		$expenseAccount->Id_idDomain = $this->getDefaultIdDomain();

		$o->setItemDetails('test_prod', $productRecord, $incomeAccount, $expenseAccount, 'USD');

		$this->assertEquals(
			'<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">5</AccountId></ExpenseAccountRef><COGSAccountRef><AccountId idDomain="QB">24</AccountId><AccountName>Testing</AccountName><AccountType>Expense</AccountType></COGSAccountRef>',
			trim($o->toXml())
		);
	}

	protected function canCreateItem_Xml()
	{
		return '<Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">8</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">6</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_IncomeAccount_Does_Not_Change_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_ExpenseAccount_Does_Not_Change_When_Already_Set_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_ExpenseAccount_Set_When_Not_Set_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">5</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_ExpenseAccount_Set_When_Not_Set_No_IdDomain_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId>5</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_Name_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod2</Name><Desc>Test Product</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_Desc_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_Taxable_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>true</Taxable><UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5.00</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>';
	}

	protected function onUpdate_Updates_UnitPrice_Xml()
	{
		return '<Id idDomain="QB">17</Id><SyncToken>0</SyncToken><Name>test_prod</Name><Desc>Test Product2</Desc><Taxable>false</Taxable><UnitPrice><CurrencyCode>GBP</CurrencyCode><Amount>55.50</Amount></UnitPrice><IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef><ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>';
	}

	protected function createItem($xml = null)
	{
		return new intuit_Model_QBD_Item($xml);
	}

	protected function createAccount($xml = null)
	{
		return new intuit_Model_QBD_Account($xml);
	}

	protected function getDefaultIdDomain()
	{
		return 'QB';
	}

	protected function getExistingItemXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QB">17</Id>
	<SyncToken>0</SyncToken>
	<ItemParentId/>
	<ItemParentName/>
	<Name>test_prod</Name>
	<Desc>Test Product</Desc>
	<Taxable>false</Taxable>
	<UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5</Amount></UnitPrice>
	<IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef>
	<PurchaseDesc/>
	<PurchaseCost/>
	<ExpenseAccountRef><AccountId idDomain="QB">4</AccountId></ExpenseAccountRef>
</Item>');
	}

	protected function getExistingItemNoExpenseAccountXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QB">17</Id>
	<SyncToken>0</SyncToken>
	<ItemParentId/>
	<ItemParentName/>
	<Name>test_prod</Name>
	<Desc>Test Product</Desc>
	<Taxable>false</Taxable>
	<UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5</Amount></UnitPrice>
	<IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef>
	<PurchaseDesc/>
	<PurchaseCost/>
</Item>');
	}

	protected function getExistingItemNoExpenseAccountAndHasCogsAccountXml()
	{
		return simplexml_load_string('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Item>
	<Id idDomain="QB">17</Id>
	<SyncToken>0</SyncToken>
	<ItemParentId/>
	<ItemParentName/>
	<Name>test_prod</Name>
	<Desc>Test Product</Desc>
	<Taxable>false</Taxable>
	<UnitPrice><CurrencyCode>USD</CurrencyCode><Amount>5</Amount></UnitPrice>
	<IncomeAccountRef><AccountId idDomain="QB">1</AccountId></IncomeAccountRef>
	<PurchaseDesc/>
	<PurchaseCost/>
	<COGSAccountRef><AccountId idDomain="QB">24</AccountId><AccountName>Testing</AccountName><AccountType>Expense</AccountType></COGSAccountRef>
</Item>');
	}
}
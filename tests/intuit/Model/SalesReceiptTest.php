<?php

require_once dirname(__FILE__).'/../doubles/QBTransportTestDouble.php';
require_once dirname(__FILE__).'/../doubles/SyncRepositoryTestDouble.php';

abstract class SalesReceiptTest  extends PHPUnit_Framework_TestCase
{
	protected $syncRepository;
	protected $transport;
	protected $qbConnector;
	protected $nonTaxableTaxCodeName = 'Non';
	protected $taxableTaxCodeName = 'Tax';
	protected $taxesEnabled = true;


	protected function setUp()
	{
		$this->syncRepository = new SyncRepositoryTestDouble();
		$this->transport = new QBTransportTestDouble();
		$this->qbConnector = new intuit_Services_QBOConnector($this->transport, new intuit_Model_Mapping_ModelMapper('QBO'));

		intuit_QBLogger::$lastMessage = null;
	}

	function test_canCreateSalesReceipt()
	{
		$settings = array('intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => 0,
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->canCreateSalesReceipt_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_getItemById_Will_Set_existingItemsById_When_Item_Loaded_From_Connector()
	{
		$settings = array('intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => 0,
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
		);

		$this->transport->addQueryByIdResult('Item', 1, $this->getDefaultItemXml());

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->canCreateSalesReceipt_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());

		$this->assertTrue(array_key_exists(1, $existingItemsById));
	}

	function test_onCreate_When_Product_Not_Found_In_Items_SetOrderDetails_Returns_False()
	{
		$settings = array();

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => 0,
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array();
		$existingItemsById = array();

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertFalse($ret);
		$this->assertEquals(
			'No intuit item record found for order: 1, product id: test_prod',
			intuit_QBLogger::$lastMessage
		);
	}

	function test_onCreate_When_Customer_Null_SetOrderDetails_Returns_False()
	{
		$settings = array();

		$customer = null;

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => 0,
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array();
		$existingItemsById = array();

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertFalse($ret);
		$this->assertEquals(
			'',
			intuit_QBLogger::$lastMessage
		);
	}

	function test_onCreate_Sets_TaxInformation()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '6.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onCreate_Sets_TaxInformation_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onCreate_Discount_Item_Doesnt_Exist_Returns_False()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_discount_product_id' => 'discount');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => '1.00',
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertFalse($ret);
	}

	function test_onCreate_Sets_Discount()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_discount_product_id' => 'discount');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 1.00,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
			'discount' => 2,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
			2 => $this->getDiscountItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onCreate_Sets_Discount_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onCreate_PromoDiscount_Item_Doesnt_Exist_Returns_False()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_promo_code_product_id' => 'promo');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => '1.00',
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertFalse($ret);
	}

	function test_onCreate_Sets_PromoDiscount()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_promo_code_product_id' => 'promo');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 1.00,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
			'promo' => 3,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
			3 => $this->getPromDiscountItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onCreate_Sets_PromoDiscount_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onCreate_Handling_Item_Doesnt_Exist_Returns_False()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_handling_product_id' => 'handling');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '7.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 1,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertFalse($ret);
	}

	function test_onCreate_Shipping_Item_Doesnt_Exist_Returns_False()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_shipping_product_id' => 'shipping');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '8.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 0,
			'handling_amount' => 1,
			'shipping_amount' => 1,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertFalse($ret);
	}

	function test_onCreate_Sets_Handling_When_Handline_Separated()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_handling_product_id' => 'handling');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '7.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 1,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
			'handling' => 4,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
			4 => $this->getHandlingItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onCreate_Sets_Handling_When_Handline_Separated_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onCreate_Sets_Shipping_When_Handling_Not_Separated()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_shipping_product_id' => 'shipping', 'intuit_handling_product_id' => 'handling');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '7.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 0,
			'handling_amount' => 1,
			'shipping_amount' => 1,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
			'shipping' => 5,
			'handling' => 4,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
			5 => $this->getShippingItem(),
			4 => $this->getHandlingItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onCreate_Sets_Shipping_When_Handling_Not_Separated_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Shipping_Taxable()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_shipping_product_id' => 'shipping', 'intuit_handling_product_id' => 'handling');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '8.10',
			'subtotal_amount' => '5.00',
			'handling_separated' => 0,
			'handling_amount' => 1,
			'shipping_amount' => 3,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 1,
			'handling_taxable' => 0,
			'tax_amount' => '1.10',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
			'shipping' => 5,
			'handling' => 4,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
			5 => $this->getShippingItem(),
			4 => $this->getHandlingItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Shipping_Taxable_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_shipping_product_id' => 'shipping', 'intuit_handling_product_id' => 'handling');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '8.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 0,
			'handling_amount' => 1,
			'shipping_amount' => 2,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
			'shipping' => 5,
			'handling' => 4,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
			5 => $this->getShippingItem(),
			4 => $this->getHandlingItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_canHave_2_products_when_1_promotion()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '6.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '0.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->canHave_2_products_when_1_promotion_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_can_have_line_with_sub_product()
	{
		$settings = array('intuit_default_tax_rate_name' => 'Pinnacle Cart TAX TaxCode Ref', 'intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '6.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => '1.00',
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => 'test_prod_small',
				'title' => 'Test Product',
				'options' => "Size: Small\n",
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod_small' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->can_have_line_with_sub_product_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	function test_onUpdate_No_Update_If_Not_Changed()
	{
		$settings = array('intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_shipping_product_id' => 'shipping');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 123,
			'placed_date' => '2012-08-16 07:00',
			'total_amount' => '85.99',
			'subtotal_amount' => '84.99',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => '1.00',
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => 0,
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_samsung_spinpoint_400',
				'product_sub_id' => '',
				'title' => 'SAMSUNG SpinPoint 400GB  Hard Drive',
				'options' => '',
				'quantity' => 1,
				'price' => '84.99',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array('shipping' => 5);
		$existingItemsById = array(5 => $this->getShippingItem());

		$existingItemsMap['test_samsung_spinpoint_400'] = '10';
		$existingItemsById['10'] = $this->getHardDriveItem();

		$o = $this->createSalesReceipt($this->getExistingSalesReceiptXml());

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertFalse($o->isModified());
	}

	function test_getDocNumber_Returns_OrderNum()
	{
		$settings = array('intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 1101,
			'placed_date' => '2012-12-25 13:03:03',
			'total_amount' => '5.00',
			'subtotal_amount' => '5.00',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => 0,
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => 0,
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_prod',
				'product_sub_id' => '',
				'title' => 'Test Product',
				'options' => '',
				'quantity' => 1,
				'price' => '5.00',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array(
			'test_prod' => 1,
		);
		$existingItemsById = array(
			1 => $this->getDefaultItem(),
		);

		$o = $this->createSalesReceipt();

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertEquals(1101, $o->getDocNumber());
	}

	function test_onUpdate_Updates_TotalAmount()
	{
		$settings = array('intuit_zero_tax_rate_name' => 'Pinnacle Cart NON TaxCode Ref', 'intuit_shipping_product_id' => 'shipping');

		$customer = $this->getCustomer();

		$orderRecord = array(
			'order_num' => 123,
			'placed_date' => '2012-08-16 07:00',
			'total_amount' => '75.99',
			'subtotal_amount' => '74.99',
			'handling_separated' => 1,
			'handling_amount' => 0,
			'shipping_amount' => '1.00',
			'promo_discount_amount' => 0,
			'discount_amount' => 0,
			'shipping_taxable' => 0,
			'handling_taxable' => 0,
			'tax_amount' => 0,
		);

		$linesRecords = array(
			array(
				'product_id' => 'test_samsung_spinpoint_400',
				'product_sub_id' => '',
				'title' => 'SAMSUNG SpinPoint 400GB  Hard Drive',
				'options' => '',
				'quantity' => 1,
				'price' => '74.99',
				'is_taxable' => 'Yes',
			),
		);

		$existingItemsMap = array('shipping' => 5);
		$existingItemsById = array(5 => $this->getShippingItem());

		$existingItemsMap['test_samsung_spinpoint_400'] = '10';
		$existingItemsById['10'] = $this->getHardDriveItem();

		$o = $this->createSalesReceipt($this->getExistingSalesReceiptXml());

		$ret = $o->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $existingItemsById, $this->qbConnector,
									$settings, $this->nonTaxableTaxCodeName, $this->taxableTaxCodeName, $this->taxesEnabled, 'USD');

		$this->assertTrue($ret);

		$this->assertEquals(
			$this->onUpdate_Updates_TotalAmount_Xml(),
			$o->toXml()
		);
		$this->assertTrue($o->isModified());
	}

	protected abstract function canCreateSalesReceipt_Xml();

	protected abstract function onUpdate_Updates_TotalAmount_Xml();

	protected abstract function onCreate_Sets_TaxInformation_Xml();

	protected abstract function onCreate_Sets_Discount_Xml();

	protected abstract function onCreate_Sets_PromoDiscount_Xml();
	
	protected abstract function onCreate_Sets_Handling_When_Handline_Separated_Xml();

	protected abstract function onCreate_Sets_Shipping_When_Handling_Not_Separated_Xml();

	protected abstract function onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Xml();

	protected abstract function onCreate_Sets_Shipping_And_Handling_When_Handling_Not_Separated_Shipping_Taxable_Xml();

	protected abstract function canHave_2_products_when_1_promotion_Xml();

	protected abstract function can_have_line_with_sub_product_Xml();

	protected abstract function getCustomer();

	protected abstract function createSalesReceipt();

	protected abstract function getExistingSalesReceiptXml();

	protected abstract function getDefaultItemXml();

	protected abstract function getDefaultItem();

	protected abstract function getDiscountItem();

	protected abstract function getPromDiscountItem();

	protected abstract function getHandlingItem();

	protected abstract function getHardDriveItem();
}
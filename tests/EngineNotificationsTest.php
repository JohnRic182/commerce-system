<?php

require_once '_init.php';

class EngineNotificationsTest extends PHPUnit_Framework_TestCase
{
	function test_When_All_Params_False_Email_Not_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'user';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->never())
			->method('emailProductsLocationNotifyWrapper');

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, false, false, false);

		$this->assertTrue(is_a(EmailNotificationsHandler::getInstance(), 'EmailNotificationsHandler'));
	}

	function test_When_Location_Set_To_User_And_Payment_Set_To_Received_Then_Email_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'user';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->once())
			->method('emailProductsLocationNotifyWrapper')
			->with($this->isInstanceOf('DB'), $this->equalTo(1));

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, false, true, true);
	}

	function test_When_Location_Set_To_User_And_Payment_Set_To_Received_And_Order_Set_To_Completed_Then_Email_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'user';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->once())
			->method('emailProductsLocationNotifyWrapper')
			->with($this->isInstanceOf('DB'), $this->equalTo(1));

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, true, true, true);
	}

	function test_When_Location_Set_To_Admin_And_Payment_Set_To_Received_Then_Email_Not_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'admin';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->never())
			->method('emailProductsLocationNotifyWrapper');

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, false, true, true);
	}

	function test_When_Location_Set_To_Admin_And_Payment_Set_To_Received_And_Order_Set_To_Completed_Then_Email_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'admin';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->once())
			->method('emailProductsLocationNotifyWrapper')
			->with($this->isInstanceOf('DB'), $this->equalTo(1));

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, true, true, true);
	}

	function test_When_Location_Set_To_Admin_And_Payment_Not_Received_And_Order_Set_To_Completed_Then_Email_Not_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'admin';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->never())
			->method('emailProductsLocationNotifyWrapper');

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, true, false, false);
	}

	function test_When_Location_Set_To_Admin_And_Payment_Is_Received_And_Order_Set_To_Completed_Then_Email_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'admin';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->once())
			->method('emailProductsLocationNotifyWrapper')
			->with($this->isInstanceOf('DB'), $this->equalTo(1));

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, true, false, true);
	}

	function test_When_Location_Set_To_Off_Then_Email_Not_Sent()
	{
		$db = Mock_Helpers::getDbFake($this);

		$settings['ProductsLocationsSendOption'] = 'off';

		$orderId = 1;

		$emailNotificationsHelperMock = $this->_getEmailNotificationsHandlerMock();

		$emailNotificationsHelperMock->expects($this->never())
			->method('emailProductsLocationNotifyWrapper');

		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, true, true, true);
		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, true, false, true);
		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, false, true, true);
		EmailNotificationsHandler::emailProductsLocationsNotify($settings, $db, $orderId, false, false, true);
	}

	function _getEmailNotificationsHandlerMock()
	{
		$emailNotificationsHelperMock = $this->getMockBuilder('EmailNotificationsHandler')
			->setMethods(array('emailProductsLocationNotifyWrapper'))
			->getMock();

		EmailNotificationsHandler::setInstance($emailNotificationsHelperMock);

		return $emailNotificationsHelperMock;
	}
}
<?php

use \Mockery as m;

require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_user.php';
require_once dirname(dirname(dirname(__FILE__))).'/content/engine/engine_order.php';
require_once dirname(dirname(__FILE__)).'/_init.php';

class Listeners_QuantityDiscountsTest extends PHPUnit_Framework_TestCase
{
	protected $user;
	protected $order;

	/** @var m\Mock $orderRepository */
	protected $orderRepository;
	/** @var m\Mock $productsRepository */
	protected $productsRepository;

	protected $db;
	protected $settings;

	public function setUp()
	{
		$this->db = null;
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DiscountsPromo' => 'YES',
		);

		$this->user = new USER($this->db, $this->settings, 0);
		$this->order = new ORDER($this->db, $this->settings, $this->user, 1);

		$this->orderRepository = m::mock('DataAccess_OrderRepositoryInterface');
		$this->productsRepository = m::mock('DataAccess_ProductsRepositoryInterface');

		Listener_QuantityDiscounts::setOrderRepository($this->orderRepository);
		Listener_QuantityDiscounts::setProductsRepository($this->productsRepository);
	}

	function test_can_calculateQuantityDiscount_percent()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);

		$this->order->lineItems[1] = $item;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 1)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'percent', 'discount' => 5, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 4.75, 4.75, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(4.75, $item->getTotal());
	}

	function test_QuantityDiscount_when_discount_percentage_more_than_100_sets_to_1()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);

		$this->order->lineItems[1] = $item;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 1)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'percent', 'discount' => 200, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 0, 0, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(0, $item->getTotal());
	}

	function test_QuantityDiscount_when_discount_percentage_less_than_0_sets_to_0()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);

		$this->order->lineItems[1] = $item;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 1)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'percent', 'discount' => -5, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 5, 5, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(5, $item->getTotal());
	}

	function test_can_calculateQuantityDiscount_when_multiple_lineItems_for_same_product()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);
		$this->order->lineItems[1] = $item;

		$lineItemValues2 = $this->getLineItemValues(array('ocid' => 2));
		$item2 = new Model_LineItem($lineItemValues2);
		$this->order->lineItems[2] = $item2;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 2)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'percent', 'discount' => 5, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 4.75, 4.75, 'No');
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 2, 4.75, 4.75, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(4.75, $item->getTotal());
		$this->assertEquals(4.75, $item2->getTotal());
	}

	function test_can_calculateQuantityDiscount_amount()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);

		$this->order->lineItems[1] = $item;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 1)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'amount', 'discount' => 5, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 0, 0, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(0, $item->getTotal());
	}

	function test_can_calculateQuantityDiscount_amount_when_amount_greater_than_subtotal()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);

		$this->order->lineItems[1] = $item;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 1)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'amount', 'discount' => 15, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 0, 0, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(0, $item->getTotal());
	}

	function test_can_calculateQuantityDiscount_when_multiple_lineItems_for_same_product_and_discount_type_amount()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);
		$this->order->lineItems[1] = $item;

		$lineItemValues2 = $this->getLineItemValues(array('ocid' => 2));
		$item2 = new Model_LineItem($lineItemValues2);
		$this->order->lineItems[2] = $item2;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 2)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'amount', 'discount' => 2.5, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 2.5, 2.5, 'No');
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 2, 2.5, 2.5, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(2.5, $item->getTotal());
		$this->assertEquals(2.5, $item2->getTotal());
	}

	function test_QuantityDiscount_when_discount_amount_less_than_0_sets_to_0()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);

		$this->order->lineItems[1] = $item;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 1)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'amount', 'discount' => -5, 'free_shipping' => 'No'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 5, 5, 'No');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(5, $item->getTotal());
	}

	function test_can_calculateQuantityDiscount_when_free_shipping_amount_sets_free_shipping()
	{
		$lineItemValues = $this->getLineItemValues();

		$item = new Model_LineItem($lineItemValues);

		$this->order->lineItems[1] = $item;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);
		$event->setOrder($this->order);

		$this->productsRepository->shouldReceive('getProductsQuantityDiscounts')->times(1)->with(1, 1)->andReturn(array('apply_to_wholesale' => 'No', 'discount_type' => 'amount', 'discount' => 5, 'free_shipping' => 'Yes'));
		$this->orderRepository->shouldReceive('updateLineItemQuantityPricing')->times(1)->with(1, 1, 0, 0, 'Yes');

		Listener_QuantityDiscounts::checkQuantityDiscounts($event);

		$this->assertEquals(0, $item->getTotal());
		$this->assertEquals('Yes', $item->getFreeShipping());
	}

	protected function getLineItemValues(array $lineItemValues = array())
	{
		return array_merge(array(
			'pid' => 1,
			'cid' => 1,
			'product_id' => 'test_prod',
			'product_sub_id' => '',
			'product_url' => '',
			'cat_url' => '',
			'inventory_control' => 'No',
			'options' => '',
			'is_gift' => 'No',
			'admin_price' => 5,
			'price' => 5,
			'quantity' => 1,
			'admin_quantity' => 1,
			'price_before_quantity_discount' => 5,
			'ocid' => 1,
			'free_shipping' => 'No',
			'price_withtax' => 5,
			'discount_amount' => 0,
			'promo_discount_amount' => 0,
		), $lineItemValues);
	}
}
<?php

require_once '_init.php';

require_once '../content/classes/DataAccess/ProductsRepository.php';

class DataAccess_ProductsRepositoryTest extends PHPUnit_Framework_TestCase
{
	function test_Can_Create_ProductsRepository()
	{
		$db = Mock_Helpers::getDbFake($this);

		$repo = new DataAccess_ProductsRepository($db);

		$this->assertNotNull($repo);
	}

	function test_GetNextProductId_Returns_NextProductId()
	{
		$db = Mock_Helpers::getDbFake($this);
		$db->expects($this->once())
			->method('query')
			->with($this->equalTo("SELECT MIN(pid) AS c FROM ".DB_PREFIX."products WHERE pid > 1"));
		$db->expects($this->once())
			->method('moveNext')
			->will($this->returnValue(array('c' => 3)));

		$nextPid = DataAccess_ProductsRepository::getNextProductId($db, 1);

		$this->assertNotNull($nextPid);
		$this->assertEquals(3, $nextPid);
	}

	function test_GetPreviousProductId_Returns_PreviousProductId()
	{
		$db = Mock_Helpers::getDbFake($this);
		$db->expects($this->once())
			->method('query')
			->with($this->equalTo("SELECT MAX(pid) AS c FROM ".DB_PREFIX."products WHERE pid < 5"));
		$db->expects($this->once())
			->method('moveNext')
			->will($this->returnValue(array('c' => 4)));

		$prevPid = DataAccess_ProductsRepository::getPreviousProductId($db, 5);

		$this->assertNotNull($prevPid);
		$this->assertEquals(4, $prevPid);
	}
}
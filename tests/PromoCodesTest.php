// <?php

// require_once dirname(dirname(__FILE__)).'/content/classes/DataAccess/PromoCodeRepository.php';

// class PromoCodesTest extends PHPUnit_Framework_TestCase
// {
// 	function test_applyPromoCode_When_emptyPromoCode_Resets()
// 	{
// 		$settings = array('DiscountsPromo' => 'YES');
// 		$repository = $this->getMock('DataAccess_PromoCodeRepositoryInterface');

// 		$order = new OrderDouble();
// 		$order->oid = 1;

// 		$this->setupExpectsPromoReset($repository, 1);

// 		$promoCodes = new PromoCodes($repository, $settings);
// 		$ret = $promoCodes->applyPromoCode($order, '');

// 		$this->assertReset($order, $ret);
// 	}

// 	function assertReset($order, $returnValue)
// 	{
// 		$this->assertFalse($returnValue);

// 		$this->assertEquals(0, $order->promoDiscountAmount);
// 		$this->assertEquals(0, $order->promoDiscountValue);
// 		$this->assertEquals(0, $order->promoCampaignId);
// 		$this->assertEquals('Global', $order->promoType);
// 		$this->assertEquals('none', $order->promoDiscountType);
// 	}

// 	function setupExpectsPromoReset($repositoryMock, $oid, $at = false)
// 	{
// 		$repositoryMock->expects($at === false ? $this->once() : $this->at($at))
// 			->method('setPromoCodeValues')
// 			->with($this->equalTo($oid), $this->equalTo(0), $this->equalTo(0), $this->equalTo('Global'), $this->equalTo(0), $this->equalTo('none'));
// 	}

// 	function setupExpectsSetPromoData($repositoryMock, $oid, $promoDiscountAmount, $promoDiscountValue, $promoType, $promoCampaignId, $promoDiscountType, $at = false)
// 	{
// 		$repositoryMock->expects($at === false ? $this->once() : $this->at($at))
// 			->method('setPromoCodeValues')
// 			->with($this->equalTo($oid), $this->equalTo($promoDiscountAmount), $this->equalTo($promoDiscountValue),
// 					$this->equalTo($promoType), $this->equalTo($promoCampaignId), $this->equalTo($promoDiscountType));
// 	}
// }

// class OrderDouble
// {
// 	public $oid;

// 	public $promoDiscountAmount = 0;
// 	public $promoDiscountValue = 0;
// 	public $promoDiscountType = '';
// 	public $promoType = '';
// 	public $promoCampaignId = 0;

// 	public $shippingAmount = 0;

// 	public function getShippingAmount(&$message)
// 	{
// 		$message = 'test';

// 		return $this->shippingAmount;
// 	}
// }
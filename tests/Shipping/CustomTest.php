<?php

require_once dirname(dirname(__FILE__)).'/_init.php';

class Shipping_CustomTest extends PHPUnit_Framework_TestCase
{
	protected $shippingRepository;

	public function setUp()
	{
		$this->settings = array(
			'SecurityCookiesPrefix' => '',
			'enable_gift_cert' => 'No',
			'DiscountsPromo' => 'YES',
		);

		$this->shippingRepository = $this->getMock('DataAccess_ShippingRepositoryInterface');
	}

	public function test_setters_and_getters()
	{
		$shippingCustom = new Shipping_Custom($this->shippingRepository);

		$shippingCustom->setShippingMethodId(10);
		$shippingCustom->setShippingMethodType('price-based');
		$shippingCustom->setWeight(199.34);
		$shippingCustom->setItemsCount(10);
		$shippingCustom->setItemsPriceBasedPrice(99);

		$this->assertEquals(10, $shippingCustom->getShippingMethodId());
		$this->assertEquals('price-based', $shippingCustom->getShippingMethodType());
		$this->assertEquals(199.34, $shippingCustom->getWeight());
		$this->assertEquals(10, $shippingCustom->getItemsCount());
		$this->assertEquals(99, $shippingCustom->getItemsPriceBasedPrice());
	}

	public function test_base_weight()
	{
		$this->setGetCustomRateExpectations($this->shippingRepository, 1, 'base_weight', 7, 58.33, 17.33, array("base"=>10, "price"=>0.97));

		$shippingCustom = new Shipping_Custom($this->shippingRepository);

		$shippingCustom->setShippingMethodId(1);
		$shippingCustom->setShippingMethodType('base_weight');
		$shippingCustom->setItemsCount(7);
		$shippingCustom->setItemsPriceBasedPrice(58.33);
		$shippingCustom->setWeight(17.33);

		$result = $shippingCustom->getShippingRate();

		$this->assertEquals(26.8101, $result);
	}

	public function test_weight()
	{
		$this->setGetCustomRateExpectations($this->shippingRepository, 1, 'weight', 7, 58.33, 17.33, array("base"=>10, "price"=>0.97));

		$shippingCustom = new Shipping_Custom($this->shippingRepository);

		$shippingCustom->setShippingMethodId(1);
		$shippingCustom->setShippingMethodType('weight');
		$shippingCustom->setItemsCount(7);
		$shippingCustom->setItemsPriceBasedPrice(58.33);
		$shippingCustom->setWeight(17.33);

		$result = $shippingCustom->getShippingRate();

		$this->assertEquals(16.8101, $result);
	}

	public function test_flat()
	{
		$this->setGetCustomRateExpectations($this->shippingRepository, 1, 'flat', 7, 58.33, 17.33, array("base"=>10, "price"=>0.97));

		$shippingCustom = new Shipping_Custom($this->shippingRepository);


		$shippingCustom->setShippingMethodId(1);
		$shippingCustom->setShippingMethodType('flat');
		$shippingCustom->setItemsCount(7);
		$shippingCustom->setItemsPriceBasedPrice(58.33);
		$shippingCustom->setWeight(17.33);

		$result = $shippingCustom->getShippingRate();

		$this->assertEquals(6.79, $result);
	}

	public function test_price_amount()
	{
		$this->setGetCustomRateExpectations($this->shippingRepository, 1, 'price', 7, 58.33, 17.33, array("price"=>3.97, 'price_type' => 'amount'));

		$shippingCustom = new Shipping_Custom($this->shippingRepository);

		$shippingCustom->setShippingMethodId(1);
		$shippingCustom->setShippingMethodType('price');
		$shippingCustom->setItemsCount(7);
		$shippingCustom->setItemsPriceBasedPrice(58.33);
		$shippingCustom->setWeight(17.33);

		$result = $shippingCustom->getShippingRate();

		$this->assertEquals(3.97, $result);
	}

	public function test_price_percentage()
	{
		$this->setGetCustomRateExpectations($this->shippingRepository, 1, 'price', 7, 58.33, 17.33, array("price"=>12.37, 'price_type' => 'percentage'));

		$shippingCustom = new Shipping_Custom($this->shippingRepository);

		$shippingCustom->setShippingMethodId(1);
		$shippingCustom->setShippingMethodType('price');
		$shippingCustom->setItemsCount(7);
		$shippingCustom->setItemsPriceBasedPrice(58.33);
		$shippingCustom->setWeight(17.33);

		$result = $shippingCustom->getShippingRate();

		$this->assertEquals(7.215421, $result);
	}


	public function test_no_rate_available()
	{
		$this->setGetCustomRateExpectations($this->shippingRepository, 1, 'price', 7, 58.33, 17.33, false);

		$shippingCustom = new Shipping_Custom($this->shippingRepository);

		$shippingCustom->setShippingMethodId(1);
		$shippingCustom->setShippingMethodType('price');
		$shippingCustom->setItemsCount(7);
		$shippingCustom->setItemsPriceBasedPrice(58.33);
		$shippingCustom->setWeight(17.33);

		$result = $shippingCustom->getShippingRate();

		$this->assertEquals(false, $result);
	}

	protected function setGetCustomRateExpectations($shippingRepositoryMock, $shippingMethodId, $shippingMethodType,
		$itemsCount, $itemsPriceBasedPrice, $itemsWeight, $customRate)
	{
		$shippingRepositoryMock
			->expects($this->once())
			->method("getCustomRate")
			->with(
				$this->equalTo($shippingMethodId),
				$this->equalTo($shippingMethodType),
				$this->equalTo($itemsCount),
				$this->equalTo($itemsPriceBasedPrice),
				$this->equalTo($itemsWeight)
			)
			->will($this->returnValue($customRate));
	}
}
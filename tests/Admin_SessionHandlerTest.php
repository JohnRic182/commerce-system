<?php

require_once '_init.php';


class Admin_SessionHandlerTest extends PHPUnit_Framework_TestCase
{
	public function test_loginInit_Successful_Session_Start()
	{
		$this->setupFileUtilsMock(true, true);

		$settings = $this->getSettings();
		
		$_SERVER['SERVER_PORT'] = 443;

		$session = Mock_Helpers::getSessionMock($this);

		$session->expects($this->once())
			->method('setSavePath')
			->with($this->equalTo('/content/cache/sessiondata'));

		$session->expects($this->once())
			->method('setCacheExpire')
			->with($this->equalTo(1140));

		$session->expects($this->once())
			->method('setName')
			->with($this->equalTo('ShoppingCartSessionAdmin'));

		$session->expects($this->once())
			->method('setCookieParams')
			->with($this->equalTo(43200), $this->equalTo('/'), $this->equalTo(''), $this->isNull(), $this->isTrue());

		$session->expects($this->once())
			->method('start');

		Admin_SessionHandler::setSessionInstance($session);
		Admin_SessionHandler::loginInit($settings);
	}

	public function test_init_Successful_Session_Start()
	{
		$this->setupFileUtilsMock(true, true);

		$settings = $this->getSettings();
		
		$_SERVER['SERVER_PORT'] = 443;

		$session = Mock_Helpers::getSessionMock($this);

		$session->expects($this->once())
			->method('setSavePath')
			->with($this->equalTo('/content/cache/sessiondata'));

		$session->expects($this->once())
			->method('setCacheLimiter')
			->with($this->equalTo('must-revalidate'));

		$session->expects($this->once())
			->method('setName')
			->with($this->equalTo('ShoppingCartSessionAdmin'));

		$session->expects($this->once())
			->method('setCookieParams')
			->with($this->equalTo(43200), $this->equalTo('/'), $this->equalTo(''), $this->isNull(), $this->isTrue());

		$session->expects($this->once())
			->method('start');

		Admin_SessionHandler::setSessionInstance($session);
		Admin_SessionHandler::init($settings);
	}

	public function test_designModeInit_Successful_Session_Start()
	{
		$this->setupFileUtilsMock(true, true);

		$settings = $this->getSettings();
		
		$_SERVER['SERVER_PORT'] = 443;

		$session = Mock_Helpers::getSessionMock($this);

		$session->expects($this->once())
			->method('setSavePath')
			->with($this->equalTo('/content/cache/sessiondata'));

		$session->expects($this->once())
			->method('setName')
			->with($this->equalTo('ShoppingCartSessionAdmin'));

		$session->expects($this->once())
			->method('setCookieParams')
			->with($this->equalTo(43200), $this->equalTo('/'), $this->equalTo(''), $this->isNull(), $this->isTrue());

		$session->expects($this->once())
			->method('start');

		Admin_SessionHandler::setSessionInstance($session);
		Admin_SessionHandler::designModeInit($settings);

		// Implicit asserts
	}

	private function getSettings()
	{
		return array(
			'GlobalHttpUrl'    => 'http://www.domain.com',
			'GlobalHttpsUrl'   => 'https://www.domain.com',
			'GlobalServerPath' => null,
			'SecurityAdminTimeOut' => 20
		);
	}

	protected function setupFileUtilsMock($is_dir = true, $is_writable = true)
	{
		$FileUtils_fake = Mock_Helpers::getFileUtilsFake($this);

		$FileUtils_fake->expects($this->any())
			->method('is_dir')
			->will($this->returnValue($is_dir));

		$FileUtils_fake->expects($this->any())
			->method('is_writable')
			->will($this->returnValue($is_writable));

		FileUtils::setInstance($FileUtils_fake);
	}
}
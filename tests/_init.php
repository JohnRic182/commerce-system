<?php

define('CONTENT_DIR', dirname(dirname(__FILE__)).'/content/');
define('DS', DIRECTORY_SEPARATOR);

define('DB_PREFIX', 'test_');

require_once dirname(dirname(__FILE__)).'/vendor/autoload.php';

//require_once('vfsStream/vfsStream.php');

function unitTestClassAutoloader($class_name)
{
	static $classDirectories = null;

	if (is_null($classDirectories)) $classDirectories = array(
		CONTENT_DIR.'classes/',
		'fakes/',
		'helpers/',
		CONTENT_DIR.'vendors/',
	);
	
	foreach ($classDirectories as $class_dir)
	{
		$class_filename1 = strtolower($class_dir.'class.'.$class_name.'.php');
		$class_filename2 = strtolower($class_dir.str_replace('_','/', $class_name).'.php');
		$class_filename3 = $class_dir.str_replace('_','/', $class_name).'.php';

		if (file_exists($class_filename1)) require_once($class_filename1);
		elseif (file_exists($class_filename2)) require_once($class_filename2);
		elseif (file_exists($class_filename3)) require_once($class_filename3);
	}
}

spl_autoload_register('unitTestClassAutoloader');

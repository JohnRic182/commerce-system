<?php

require_once dirname(dirname(__FILE__)).'/_init.php';

class Model_LineItemTest extends PHPUnit_Framework_TestCase
{
	function test_can_create_LineItem()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$this->assertnotNull($item);
	}

	function test_setAdminPrice_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setAdminPrice(14.95);

		$this->assertEquals(14.95, $item->getAdminPrice());
		$this->assertEquals(14.95, $values['admin_price']);
		$this->assertEquals(14.95, $values['product_price']);
		$this->assertEquals(14.95, $values['product_total']);
	}

	function test_setPrice_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setPrice(17.99);

		$this->assertEquals(17.99, $item->getPrice());
		$this->assertEquals(17.99, $values['price']);
	}

	function test_setAdminQuantity_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setAdminPrice(14.33);
		$item->setAdminQuantity(3);

		$this->assertEquals(3, $item->getAdminQuantity());
		$this->assertEquals(3, $values['admin_quantity']);
		$this->assertEquals(42.99, $values['product_total']);
	}

	function test_setQuantity_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setQuantity(2);

		$this->assertEquals(2, $item->getQuantity());
		$this->assertEquals(2, $values['quantity']);
	}

	function test_setFinalPrice_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setFinalPrice(14.95);

		$this->assertEquals(14.95, $item->getAdminPrice());
		$this->assertEquals(14.95, $values['admin_price']);
		$this->assertEquals(14.95, $values['product_price']);
		$this->assertEquals(14.95, $values['product_total']);
		$this->assertEquals(14.95, $item->getPrice());
		$this->assertEquals(14.95, $values['price']);
	}

	function test_setFinalQuantity_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setAdminPrice(14.33);
		$item->setFinalQuantity(3);

		$this->assertEquals(3, $item->getAdminQuantity());
		$this->assertEquals(3, $values['admin_quantity']);
		$this->assertEquals(42.99, $values['product_total']);

		$this->assertEquals(3, $item->getQuantity());
		$this->assertEquals(3, $values['quantity']);
	}

	function test_setDiscountAmount_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setDiscountAmount(10.51);

		$this->assertEquals(10.51, $item->getDiscountAmount());
		$this->assertEquals(10.51, $values['discount_amount']);
	}

	function test_setPromoDiscountAmount_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setPromoDiscountAmount(10.52);

		$this->assertEquals(10.52, $item->getPromoDiscountAmount());
		$this->assertEquals(10.52, $values['promo_discount_amount']);
	}

	function test_setAttributeValue_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setAttributeValue(1.51);

		$this->assertEquals(1.51, $item->getAttributeValue());
		$this->assertEquals(1.51, $values['attribute_value']);
	}

	function test_setAttributeValueType_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setAttributeValueType('amount');

		$this->assertEquals('amount', $item->getAttributeValueType());
		$this->assertEquals('amount', $values['attribute_value_type']);
	}

	function test_setDigitalProduct_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setProductType(Model_Product::DIGITAL);

		$this->assertEquals(Model_Product::DIGITAL, $item->getProductType());
		$this->assertEquals(Model_Product::DIGITAL, $values['product_type']);
	}

	function test_setDigitalProductDownloads_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();

		$item = new Model_LineItem($values);

		$item->setDigitalProductDownloads(3);

		$this->assertEquals(3, $item->getDigitalProductDownloads());
		$this->assertEquals(3, $values['digital_product_downloads']);
	}

	function test_setDigitalProductFile_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setDigitalProductFile('test.txt');

		$this->assertEquals('test.txt', $item->getDigitalProductFile());
		$this->assertEquals('test.txt', $values['digital_product_file']);
	}

	function test_setDigitalProductKey_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setDigitalProductKey('test');

		$this->assertEquals('test', $item->getDigitalProductKey());
		$this->assertEquals('test', $values['digital_product_key']);
	}

	function test_setFreeShipping_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setFreeShipping('Yes');

		$this->assertEquals('Yes', $item->getFreeShipping());
		$this->assertEquals('Yes', $values['free_shipping']);
	}

	function test_setInventoryId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setInventoryId(21);

		$this->assertEquals(21, $item->getInventoryId());
		$this->assertEquals(21, $values['inventory_id']);
	}

	function test_setIsDoba_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setIsDoba('Yes');

		$this->assertEquals('Yes', $item->getIsDoba());
		$this->assertEquals('Yes', $values['is_doba']);
	}

	function test_setIsGift_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setIsGift('Yes');

		$this->assertEquals('Yes', $item->getIsGift());
		$this->assertEquals('Yes', $values['is_gift']);
	}

	function test_setIsShippingPrice_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setIsShippingPrice('Yes');

		$this->assertEquals('Yes', $item->getIsShippingPrice());
		$this->assertEquals('Yes', $values['is_shipping_price']);
	}

	function test_setIsStockChanged_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setIsStockChanged('Yes');

		$this->assertEquals('Yes', $item->getIsStockChanged());
		$this->assertEquals('Yes', $values['is_stock_changed']);
	}

	function test_setIsTaxable_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setIsTaxable('Yes');

		$this->assertEquals('Yes', $item->getIsTaxable());
		$this->assertEquals('Yes', $values['is_taxable']);
	}

	function test_setId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setId(32);

		$this->assertEquals(32, $item->getId());
		$this->assertEquals(32, $values['ocid']);
	}

	function test_setOptions_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setOptions('Test');

		$this->assertEquals('Test', $item->getOptions());
		$this->assertEquals('Test', $values['options']);
	}

	function test_setOptionsClean_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setOptionsClean('Test2');

		$this->assertEquals('Test2', $item->getOptionsClean());
		$this->assertEquals('Test2', $values['options_clean']);
	}

	function test_setOrderId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setOrderId(34);

		$this->assertEquals(34, $item->getOrderId());
		$this->assertEquals(34, $values['oid']);
	}

	function test_setPid_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setPid(35);

		$this->assertEquals(35, $item->getPid());
		$this->assertEquals(35, $values['pid']);
	}

	function test_setPriceBeforeQuantityDiscount_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setPriceBeforeQuantityDiscount(35.95);

		$this->assertEquals(35.95, $item->getPriceBeforeQuantityDiscount());
		$this->assertEquals(35.95, $values['price_before_quantity_discount']);
	}

	function test_setProductGtin_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductGtin('Test Gtin');

		$this->assertEquals('Test Gtin', $item->getProductGtin());
		$this->assertEquals('Test Gtin', $values['product_gtin']);
	}

	function test_setProductId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductId('Test_Prod');

		$this->assertEquals('Test_Prod', $item->getProductId());
		$this->assertEquals('Test_Prod', $values['product_id']);
	}

	function test_setProductMpn_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductMpn('Test Mpn');

		$this->assertEquals('Test Mpn', $item->getProductMpn());
		$this->assertEquals('Test Mpn', $values['product_mpn']);
	}

	function test_setProductRemoved_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductRemoved('Yes');

		$this->assertEquals('Yes', $item->getProductRemoved());
		$this->assertEquals('Yes', $values['product_removed']);
	}

	function test_setProductSku_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductSku('Test Sku');

		$this->assertEquals('Test Sku', $item->getProductSku());
		$this->assertEquals('Test Sku', $values['product_sku']);
	}

	function test_setProductSubId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductSubId('Test_Prod_Sub');

		$this->assertEquals('Test_Prod_Sub', $item->getProductSubId());
		$this->assertEquals('Test_Prod_Sub', $values['product_sub_id']);
	}

	function test_setProductUpc_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductUpc('Test Upc');

		$this->assertEquals('Test Upc', $item->getProductUpc());
		$this->assertEquals('Test Upc', $values['product_upc']);
	}

	function test_setProductsLocationId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setProductsLocationId(36);

		$this->assertEquals(36, $item->getProductsLocationId());
		$this->assertEquals(36, $values['products_location_id']);
	}

	function test_setShippingPrice_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setShippingPrice(36.45);

		$this->assertEquals(36.45, $item->getShippingPrice());
		$this->assertEquals(36.45, $values['shipping_price']);
	}

	function test_setTaxDescription_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setTaxDescription('AZ Tax');

		$this->assertEquals('AZ Tax', $item->getTaxDescription());
		$this->assertEquals('AZ Tax', $values['tax_description']);
	}

	function test_setTaxRate_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setTaxRate(6.95);

		$this->assertEquals(6.95, $item->getTaxRate());
		$this->assertEquals(6.95, $values['tax_rate']);
	}

	function test_setTaxExempt_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setTaxExempt('Yes');

		$this->assertEquals('Yes', $item->getTaxExempt());
		$this->assertEquals('Yes', $values['tax_exempt']);
	}

	function test_setTaxAmount_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setTaxAmount(2.95);

		$this->assertEquals(2.95, $item->getTaxAmount());
		$this->assertEquals(2.95, $values['tax_amount']);
	}

	function test_setTitle_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setTitle('Title');

		$this->assertEquals('Title', $item->getTitle());
		$this->assertEquals('Title', $values['title']);
	}

	function test_setWeight_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setWeight(4.95);

		$this->assertEquals(4.95, $item->getWeight());
		$this->assertEquals(4.95, $values['weight']);
	}

	function test_setAttributes_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);
		$item->setAttributes('Attributes');

		$this->assertEquals('Attributes', $item->getAttributes());
		$this->assertEquals('Attributes', $values['attributes']);
	}

	function test_setPriceWithTax_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$values['is_gift'] = 'No';
		$item = new Model_LineItem($values);
		$item->setPriceWithTax(19.95);

		$this->assertEquals(19.95, $item->getPRiceWithTax());
		$this->assertEquals(19.95, $values['price_withtax']);
	}

	function test_setPriceWithTax_When_IsGift_Yes_Sets_PriceWithTax_To_0()
	{
		$values = $this->getMinimumValues();
		$values['is_gift'] = 'Yes';
		$item = new Model_LineItem($values);
		$item->setPriceWithTax(19.95);

		$this->assertEquals(0, $item->getPRiceWithTax());
		$this->assertEquals(0, $values['price_withtax']);
	}

	function test_getSubtotal_Returns_Subtotal_Without_Tax()
	{
		$values = $this->getMinimumValues();
		$values['is_gift'] = 'No';
		$item = new Model_LineItem($values);
		$item->setPriceWithTax(19.95);
		$item->setFinalPrice(14.95);
		$item->setFinalQuantity(3);

		$this->assertEquals(44.85, $item->getSubtotal());
	}

	function test_getSubtotal_Returns_Subtotal_With_Tax()
	{
		$values = $this->getMinimumValues();
		$values['is_gift'] = 'No';
		$item = new Model_LineItem($values);
		$item->setPriceWithTax(19.95);
		$item->setFinalPrice(14.95);
		$item->setFinalQuantity(3);

		$this->assertEquals(59.85, $item->getSubtotal(true));
	}

	function test_getSubtotal_Returns_0_When_IsGift_Yes()
	{
		$values = $this->getMinimumValues();
		$values['is_gift'] = 'Yes';
		$item = new Model_LineItem($values);
		$item->setPriceWithTax(19.95);
		$item->setFinalPrice(14.95);
		$item->setFinalQuantity(3);

		$this->assertEquals(0, $item->getSubtotal(true));
	}

	function test_getTotal_Returns_Subtotal_Minus_DiscountTotal()
	{
		$values = $this->getMinimumValues();
		$values['is_gift'] = 'No';
		$item = new Model_LineItem($values);
		$item->setFinalPrice(14.95);
		$item->setFinalQuantity(3);
		$item->setPromoDiscountAmount(3.45);
		$item->setDiscountAmount(4.12);

		$this->assertEquals(37.28, $item->getTotal());
	}

	function test_getDiscountTotal_Returns_DiscountAmount_Plus_PromoDiscountAmount()
	{
		$values = $this->getMinimumValues(array('admin_price' => 15));
		$item = new Model_LineItem($values);
		$item->setPromoDiscountAmount(3.45);
		$item->setDiscountAmount(4.12);

		$this->assertEquals(7.57, $item->getDiscountTotal());
	}

	function test_parseOptions_Can_Parse_Single_Attribute()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);

		$item->setOptions('Color : Red');

		$options = $item->parseOptions();

		$this->assertArrayHasKey('Color', $options);
		$this->assertEquals('Red', $options['Color']);
	}

	function test_parseOptions_Can_Parse_Multiple_Attributes()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);

		$item->setOptions("Color : Red\nSize : Large\nEngraved : Yes");

		$options = $item->parseOptions();

		$this->assertArrayHasKey('Color', $options);
		$this->assertEquals('Red', $options['Color']);
		$this->assertArrayHasKey('Size', $options);
		$this->assertEquals('Large', $options['Size']);
		$this->assertArrayHasKey('Engraved', $options);
		$this->assertEquals('Yes', $options['Engraved']);
	}

	function test_parseOptions_Can_Parse_Single_Attribute_With_Modifier()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);

		$item->setOptions('Color : Red (+5,-3)');

		$options = $item->parseOptions();

		$this->assertArrayHasKey('Color', $options);
		$this->assertEquals('Red', $options['Color']);
	}

	function test_parseOptions_Can_Parse_Multiple_Attributes_With_Modifiers()
	{
		$values = $this->getMinimumValues();
		$item = new Model_LineItem($values);

		$item->setOptions("Color : Red (-1)\nSize : Large (+1)\nEngraved : Yes");

		$options = $item->parseOptions();

		$this->assertArrayHasKey('Color', $options);
		$this->assertEquals('Red', $options['Color']);
		$this->assertArrayHasKey('Size', $options);
		$this->assertEquals('Large', $options['Size']);
		$this->assertArrayHasKey('Engraved', $options);
		$this->assertEquals('Yes', $options['Engraved']);
	}

//	function test_parseValues_Sets_Values_When_SubProduct()
//	{
//		$values = $this->getMinimumValues();
//		$values['product_id'] = 'test_prod';
//		$values['product_sub_id'] = 'test_prod_sub';
//		$item = new Model_LineItem($values);
//
//		$this->assertEquals('test_prod', $values['product_parent_id']);
//		$this->assertEquals('test_prod / test_prod_sub', $values['product_id']);
//	}

	function test_parseValues_Sets_allowed_max_to_max_order_when_max_order_less_than_stock()
	{
		$values = $this->getMinimumValues();
		$values['inventory_control'] = 'Yes';
		$values['max_order'] = 3;
		$values['stock'] = 5;

		$item = new Model_LineItem($values);

		$this->assertEquals(3, $values['allowed_max']);
	}

	function test_parseValues_Sets_allowed_max_to_stock_when_max_order_more_than_stock()
	{
		$values = $this->getMinimumValues();
		$values['inventory_control'] = 'Yes';
		$values['max_order'] = 10;
		$values['stock'] = 5;

		$item = new Model_LineItem($values);

		$this->assertEquals(5, $values['allowed_max']);
	}

	function test_parseValues_Sets_allowed_max_to_stock_when_max_order_0()
	{
		$values = $this->getMinimumValues();
		$values['inventory_control'] = 'Yes';
		$values['max_order'] = 0;
		$values['stock'] = 50;

		$item = new Model_LineItem($values);

		$this->assertEquals(50, $values['allowed_max']);
	}

	// TODO: This seems wrong, shouldn't it set to max_order in this case?
	function test_parseValues_Sets_allowed_max_0_when_not_under_inventory_control()
	{
		$values = $this->getMinimumValues();
		$values['inventory_control'] = 'No';
		$values['max_order'] = 10;
		$values['stock'] = 5;

		$item = new Model_LineItem($values);

		$this->assertEquals(0, $values['allowed_max']);
	}

	function test_createNewLineItem_Initializes_Correct_Data()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
		);

		$product = $this->getProduct();

		$attributesData = array(
			'weight' => 0,
			'options' => 'Test : Test',
			'options_clean' => 'Test : Test2',
			'attributes' => array(1 => 1),
			'inventoryList' => 'Inventory',
		);

		$lineItem = Model_LineItem::createNewLineItem(100, $product, 50, $attributesData, 'No', $settings);

		$this->assertEquals('Test : Test', $lineItem->getOptions());
		$this->assertEquals('Test : Test2', $lineItem->getOptionsClean());
		$this->assertArrayHasKey(1, $lineItem->getAttributes());
		$this->assertEquals('Inventory', $lineItem->getInventoryList());
		$this->assertEquals(0, $lineItem->getId());
		$this->assertEquals(100, $lineItem->getOrderId());
		$this->assertEquals(1, $lineItem->getPid());
		$this->assertEquals(50, $lineItem->getAdminQuantity());
		$this->assertEquals(50, $lineItem->getQuantity());
		$this->assertEquals(3, $lineItem->getPriceBeforeQuantityDiscount());
		$this->assertEquals(3, $lineItem->getPrice());
		$this->assertEquals(3, $lineItem->getAdminPrice());
		$this->assertEquals(3, $lineItem->getFinalPrice());
		$this->assertEquals(3.5, $lineItem->getPriceWithTax());
		$this->assertEquals(0, $lineItem->getAttributeValue());
		$this->assertEquals('amount', $lineItem->getAttributeValueType());
		$this->assertEquals('Yes', $lineItem->getIsTaxable());
		$this->assertEquals('doba', $lineItem->getIsDoba());
		$this->assertEquals('No', $lineItem->getIsStockChanged());
		$this->assertEquals(1.5, $lineItem->getWeight());
		$this->assertEquals('Shipping', $lineItem->getFreeShipping());
		$this->assertEquals('No', $lineItem->getIsShippingPrice());
		$this->assertEquals(0, $lineItem->getShippingPrice());
		$this->assertEquals(16.67, $lineItem->getTaxRate());
		$this->assertEquals('', $lineItem->getTaxDescription());
		$this->assertEquals(Model_Product::DIGITAL, $lineItem->getProductType());
		$this->assertEquals('file.txt', $lineItem->getDigitalProductFile());
		$this->assertNotEquals('', $lineItem->getDigitalProductKey());
		$this->assertEquals(0, $lineItem->getDigitalProductDownloads());
		$this->assertEquals('No', $lineItem->getIsGift());
		$this->assertEquals('No', $lineItem->getProductRemoved());
		$this->assertEquals(0, $lineItem->getInventoryId());
		$this->assertEquals(4, $lineItem->getProductsLocationId());
		$this->assertEquals('test_prod2', $lineItem->getProductId());
		$this->assertEquals('sku', $lineItem->getProductSku());
		$this->assertEquals('upc', $lineItem->getProductUpc());
		$this->assertEquals('gitn', $lineItem->getProductGtin());
		$this->assertEquals('mpn', $lineItem->getProductMpn());
		$this->assertEquals('Test Product', $lineItem->getTitle());
		$this->assertEquals(0, $lineItem->getDiscountAmount());
		$this->assertEquals(0, $lineItem->getPromoDiscountAmount());
	}

	protected function getMinimumValues($values = array())
	{
		return array_merge(array(
			'product_sub_id' => '',
			'product_url' => '',
			'pid' => 1,
			'cid' => 1,
			'cat_url' => '',
			'inventory_control' => 'No',
			'allowed_max' => 0,
			'is_gift' => 'No',
			'options' => '',
			'admin_price' => 1,
			'admin_quantity' => 1,
			'is_taxable' => 'Yes',
			'quantity_from_stock' => 0,
		),
			$values
		);
	}

	protected function getProduct()
	{
		$productData = array(
			'pid' => 1,
			'cost' => 2,
			'price' => 3,
			'is_taxable' => 'Yes',
			'is_doba' => 'doba',
			'weight' => 1.5,
			'free_shipping' => 'Shipping',
			'product_type' => Model_Product::DIGITAL,
			'digital_product_file' => 'file.txt',
			'products_location_id' => 4,
			'product_id' => 'test_prod2',
			'product_sku' => 'sku',
			'product_upc' => 'upc',
			'product_gtin' => 'gitn',
			'product_mpn' => 'mpn',
			'title' => 'Test Product',
			'cid' => 1,
			'price_level_1' => 2.97,
			'price_level_2' => 2.5,
			'price_level_3' => 1.97,
			'price_withtax' => 3.5,
			'tax_rate' => 16.67,
			'inventory_control' => 'No',
			'stock' => 0,
		);

		return new Model_Product($productData);
	}
}
<?php

use \Mockery as m;

require_once dirname(dirname(__FILE__)).'/_init.php';

require_once dirname(dirname(dirname(__FILE__))).'/vendor/autoload.php';
require_once CONTENT_DIR.'engine/engine_order.php';

class Model_ProductTest extends PHPUnit_Framework_TestCase
{
	public function tearDown()
	{
		\Mockery::close();
	}

	function test_can_create_Product()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$this->assertNotNull($product);
	}

	function test_setPid_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setPid(2);

		$this->assertEquals(2, $product->getPid());
	}

	function test_setProductId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setProductId('test_prod');

		$this->assertEquals('test_prod', $product->getProductId());
	}

	function test_setAttributesCount_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setAttributesCount(3);

		$this->assertEquals(3, $product->getAttributesCount());
	}

	function test_setWeight_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setWeight(3.13);

		$this->assertEquals(3.13, $product->getWeight());
	}

	function test_setInventoryControl_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setInventoryControl('Yes');

		$this->assertEquals('Yes', $product->getInventoryControl());
	}

	function test_setPrice_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setPrice(13.14);

		$this->assertEquals(13.14, $product->getPrice());
	}

	function test_setFinalPrice_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setFinalPrice(13.15);

		$this->assertEquals(13.15, $product->getFinalPrice());
	}

	function test_setPriceWithTax_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setPriceWithTax(13.16);

		$this->assertEquals(13.16, $product->getPriceWithTax());
	}

	function test_getPriceWithTax_Returns_Final_Price_When_price_withtax_Not_Set()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setFinalPrice(13.15);

		$this->assertEquals(13.15, $product->getPriceWithTax());
	}

	function test_setMinOrder_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setMinOrder(11);

		$this->assertEquals(11, $product->getMinOrder());
	}

	function test_setMaxOrder_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setMaxOrder(13);

		$this->assertEquals(13, $product->getMaxOrder());
	}

	function test_setStock_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setStock(9);

		$this->assertEquals(9, $product->getStock());
	}

	function test_setPriceLevel1_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setPriceLevel1(9.91);

		$this->assertEquals(9.91, $product->getPriceLevel1());
	}

	function test_setPriceLevel2_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setPriceLevel2(9.92);

		$this->assertEquals(9.92, $product->getPriceLevel2());
	}

	function test_setPriceLevel3_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setPriceLevel3(9.93);

		$this->assertEquals(9.93, $product->getPriceLevel3());
	}

	function test_setIsDoba_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setIsDoba('Yes');

		$this->assertEquals('Yes', $product->getIsDoba());
	}

	function test_setFreeShipping_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setFreeShipping('Yes');

		$this->assertEquals('Yes', $product->getFreeShipping());
	}

	function test_setDigitalProduct_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setProductType(Model_Product::DIGITAL);

		$this->assertEquals(Model_Product::DIGITAL, $product->getProductType());
	}

	function test_setDigitalProductFile_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);

		$product->setDigitalProductFile('test.txt');

		$this->assertEquals('test.txt', $product->getDigitalProductFile());
	}

	function test_setProductsLocationId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setProductsLocationId(36);

		$this->assertEquals(36, $product->getProductsLocationId());
	}

	function test_setProductSku_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setProductSku('Test Sku');

		$this->assertEquals('Test Sku', $product->getProductSku());
	}

	function test_setProductUpc_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setProductUpc('Test Upc');

		$this->assertEquals('Test Upc', $product->getProductUpc());
	}

	function test_setProductMpn_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setProductMpn('Test Mpn');

		$this->assertEquals('Test Mpn', $product->getProductMpn());
	}

	function test_setProductGtin_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setProductGtin('Test Gtin');

		$this->assertEquals('Test Gtin', $product->getProductGtin());
	}

	function test_setTitle_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setTitle('Title');

		$this->assertEquals('Title', $product->getTitle());
	}

	function test_setCid_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setCid(3);

		$this->assertEquals(3, $product->getCid());
	}

	function test_setIsTaxable_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setIsTaxable('Yes');

		$this->assertEquals('Yes', $product->getIsTaxable());
	}

	function test_setTaxRate_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setTaxRate(6.95);

		$this->assertEquals(6.95, $product->getTaxRate());
	}

	function test_setTaxClassId_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setTaxClassId(5);

		$this->assertEquals(5, $product->getTaxClassId());
	}

	function test_setTaxDescription_Sets_Correct_Values()
	{
		$values = $this->getMinimumValues();
		$product = new Model_Product($values);
		$product->setTaxDescription('AZ Tax');

		$this->assertEquals('AZ Tax', $product->getTaxDescription());
	}

	function test_setTaxValues_should_setTaxRate_0_When_IsTaxable_No()
	{
		$values = $this->getMinimumValues();
		$values['is_taxable'] = 'No';
		$values['tax_rate'] = 2;
		$values['tax_description'] = 'Test Taxes';

		$product = new Model_Product($values);

		$product->setTaxValues();

		$this->assertEquals(0, $product->getTaxRate());
		$this->assertEquals('', $product->getTaxDescription());
	}

	function test_setTaxValues_should_setTaxRate_0_When_IsTaxable_Yes_And_TaxRate_0_And_TaxClassId_0()
	{
		$values = $this->getMinimumValues();
		$values['is_taxable'] = 'Yes';
		$values['tax_class_id'] = 0;
		$values['tax_rate'] = 0;
		$values['tax_description'] = 'Test Taxes';

		$product = new Model_Product($values);

		$product->setTaxValues();

		$this->assertEquals(0, $product->getTaxRate());
		$this->assertEquals('', $product->getTaxDescription());
	}

	function test_setTaxValues_should_setTaxRate_0_When_IsTaxable_Yes_And_Cannot_Find_TaxClass()
	{
		$values = $this->getMinimumValues();
		$values['is_taxable'] = 'Yes';
		$values['tax_class_id'] = 2;
		$values['tax_rate'] = 0;
		$values['tax_description'] = 'Test Taxes';

		$taxProvider = m::mock('Tax_ProviderInterface');

		Tax_ProviderFactory::setTaxProvider($taxProvider);

		$taxProvider->shouldReceive('hasTaxRate')->times(1)->with(2)->andReturn(false);

		$product = new Model_Product($values);

		$product->setTaxValues();

		$this->assertEquals(0, $product->getTaxRate());
		$this->assertEquals('', $product->getTaxDescription());
	}

	function test_setTaxValues_should_setTaxRate_When_IsTaxable_Yes_And_Can_Find_TaxClass()
	{
		$values = $this->getMinimumValues();
		$values['is_taxable'] = 'Yes';
		$values['tax_class_id'] = 2;
		$values['tax_rate'] = 0;
		$values['tax_description'] = 'Test Taxes';

		$taxProvider = m::mock('Tax_ProviderInterface');

		Tax_ProviderFactory::setTaxProvider($taxProvider);

		$taxProvider->shouldReceive('hasTaxRate')->times(1)->with(2)->andReturn(true);
		$taxProvider->shouldReceive('getTaxRate')->times(1)->with(2, false)->andReturn(6.5);
		$taxProvider->shouldReceive('getTaxRateDescription')->times(1)->with(2)->andReturn('Test Tax');
		$taxProvider->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(false);

		$product = new Model_Product($values);

		$product->setTaxValues();

		$this->assertEquals(6.5, $product->getTaxRate());
		$this->assertEquals('Test Tax', $product->getTaxDescription());
	}

	function test_setTaxValues_should_call_calculateVATTax()
	{
		$values = $this->getMinimumValues();
		$values['is_taxable'] = 'Yes';
		$values['tax_class_id'] = 2;
		$values['tax_rate'] = 0;
		$values['tax_description'] = 'Test Taxes';
		$values['price'] = 14.95;

		$taxProvider = m::mock('Tax_ProviderInterface');

		Tax_ProviderFactory::setTaxProvider($taxProvider);

		$taxProvider->shouldReceive('hasTaxRate')->times(1)->with(2)->andReturn(true);
		$taxProvider->shouldReceive('getTaxRate')->times(1)->with(2, false)->andReturn(6.5);
		$taxProvider->shouldReceive('getTaxRateDescription')->times(1)->with(2)->andReturn('Test Tax');
		$taxProvider->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(true);

		$product = new Model_Product($values);

		$product->setTaxValues();

		$this->assertEquals(6.5, $product->getTaxRate());
		$this->assertEquals('Test Tax', $product->getTaxDescription());
		$this->assertEquals(15.92, $product->getPriceWithTax());
	}

	function test_setTaxValues_should_setTaxRate_When_tax_rate_set_at_product_level()
	{
		$taxProvider = m::mock('Tax_ProviderInterface');

		Tax_ProviderFactory::setTaxProvider($taxProvider);
		$taxProvider->shouldReceive('getDisplayPricesWithTax')->times(1)->andReturn(false);

		$values = $this->getMinimumValues();
		$values['is_taxable'] = 'Yes';
		$values['tax_rate'] = 2;
		$values['tax_description'] = 'Test Taxes';

		$product = new Model_Product($values);

		$product->setTaxValues();

		$this->assertEquals(2, $product->getTaxRate());
		$this->assertEquals('', $product->getTaxDescription());
	}


	function test_when_tax_rate_set_to_0_does_not_calculate_vat_pricing()
	{
		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setTaxRate(0);

		$product->calculateVATTax();

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_when_tax_rate_set_calculates_vat_pricing()
	{
		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setTaxRate(10);

		$product->calculateVATTax();

		$this->assertEquals(3.3, $product->getPriceWithTax());
		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_when_attributes_does_not_have_price_modifier_does_not_affect_pricing()
	{
		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$attributes = array(
			array(
				'attribute_value' => '0',
				'attribute_value_type' => 'amount',
			),
		);

		$product->setAttributePricing($attributes);

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_set_when_attribute_modifier_amount_can_change_pricing()
	{
		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$attributes = array(
			array(
				'attribute_value' => '1',
				'attribute_value_type' => 'amount',
			),
		);

		$product->setAttributePricing($attributes);

		$this->assertEquals(4, $product->getFinalPrice());
	}

	function test_set_when_attribute_modifier_combinations_can_change_pricing()
	{
		$productData = $this->getProduct(array('price' => 4.67));
		$product = new Model_Product($productData);

		$attributes = array(
			array(
				'attribute_value' => '7',
				'attribute_value_type' => 'percent',
			),
			array(
				'attribute_value' => '1.37',
				'attribute_value_type' => 'amount',
			),
		);

		$product->setAttributePricing($attributes);

		$this->assertEquals(6.37, $product->getFinalPrice());
	}

	function test_set_when_attribute_modifier_percent_can_change_pricing()
	{
		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$attributes = array(
			array(
				'attribute_value' => '10',
				'attribute_value_type' => 'percent',
			),
		);

		$product->setAttributePricing($attributes);

		$this->assertEquals(3.3, $product->getFinalPrice());
	}

	function test_when_WholesaleDiscountType_Product_does_not_set_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'PRODUCT',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(1, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_when_user_level_0_Product_does_not_set_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'GLOBAL',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(0, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_when_user_level_1_Product_calculates_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'GLOBAL',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(1, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.85, $product->getFinalPrice());
	}

	function test_when_user_level_2_Product_calculates_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'GLOBAL',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(2, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.7, $product->getFinalPrice());
	}

	function test_when_user_level_3_Product_calculates_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'GLOBAL',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(3, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.55, $product->getFinalPrice());
	}

	function test_when_user_level_3_and_max_level_1_Product_calculates_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 1,
			'WholesaleDiscountType' => 'GLOBAL',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(3, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.85, $product->getFinalPrice());
	}

	function test_when_user_level_3_and_max_level_2_Product_calculates_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 2,
			'WholesaleDiscountType' => 'GLOBAL',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(3, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.7, $product->getFinalPrice());
	}

	function test_when_user_level_2_and_max_level_1_Product_calculates_wholesale_global_discount()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 1,
			'WholesaleDiscountType' => 'GLOBAL',
			'WholesaleDiscount1' => '5',
			'WholesaleDiscount2' => '10',
			'WholesaleDiscount3' => '15',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleGlobalDiscountPricing(2, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.85, $product->getFinalPrice());
	}

	function test_when_WholesaleDiscountType_Global_does_not_set_wholesale_pricing_at_product_level()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'GLOBAL',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleProductLevelPricing(1, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_when_WholesaleDiscountType_No_does_not_set_wholesale_pricing_at_product_level()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'No',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleProductLevelPricing(1, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_when_UserLevel_0_does_not_set_wholesale_pricing_at_product_level()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'No',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleProductLevelPricing(0, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_when_WholesaleMaxLevels_0_does_not_set_wholesale_pricing_at_product_level()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 0,
			'WholesaleDiscountType' => 'No',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleProductLevelPricing(1, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(3, $product->getFinalPrice());
	}

	function test_Product_sets_wholesale_pricing_at_product_level()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'PRODUCT',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleProductLevelPricing( 1, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.97, $product->getFinalPrice());
	}

	function test_when_price_level_2_is_0_will_set_pricing_to_level_1()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'PRODUCT',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);
		$product->setPriceLevel2(0);
		$product->setPriceLevel3(0);

		$product->setWholesaleProductLevelPricing(2, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.97, $product->getFinalPrice());
	}

	function test_when_price_level_2_and_3_are_0_will_set_pricing_to_level_1()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'PRODUCT',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);
		$product->setPriceLevel2(0);
		$product->setPriceLevel3(0);

		$product->setWholesaleProductLevelPricing(3, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.97, $product->getFinalPrice());
	}

	function test_when_price_level_3_is_0_will_set_pricing_to_level_2()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'PRODUCT',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);
		$product->setPriceLevel3(0);

		$product->setWholesaleProductLevelPricing(3, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.5, $product->getFinalPrice());
	}

	function test_when_userLevel_2_will_set_pricing_to_level_2()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'PRODUCT',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleProductLevelPricing(2, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(2.5, $product->getFinalPrice());
	}

	function test_when_userLevel_3_will_set_pricing_to_level_3()
	{
		$settings = array(
			'DisplayPricesWithTax' => 'NO',
			'WholesaleMaxLevels' => 3,
			'WholesaleDiscountType' => 'PRODUCT',
		);

		$productData = $this->getProduct();
		$product = new Model_Product($productData);

		$product->setWholesaleProductLevelPricing(3, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->assertEquals(1.97, $product->getFinalPrice());
	}

	protected function getMinimumValues()
	{
		return array(
			'price' => 1,
		);
	}

	protected function getProduct(array $productData = array())
	{
		return array_merge(array(
			'pid' => 1,
			'cost' => 2,
			'price' => 3,
			'is_taxable' => 'Yes',
			'is_doba' => 'doba',
			'weight' => 1.5,
			'free_shipping' => 'Shipping',
			'product_type' => Model_Product::DIGITAL,
			'digital_product_file' => 'file.txt',
			'products_location_id' => 4,
			'product_id' => 'test_prod2',
			'product_sku' => 'sku',
			'product_upc' => 'upc',
			'product_gtin' => 'gitn',
			'product_mpn' => 'mpn',
			'title' => 'Test Product',
			'cid' => 1,
			'price_level_1' => 2.97,
			'price_level_2' => 2.5,
			'price_level_3' => 1.97,
		), $productData);
	}
}
<?php

require_once '../content/engine/engine_functions.php';

class Model_TaxRateTest extends PHPUnit_Framework_TestCase
{
	function test_can_Compute_Simple_Tax_Rate()
	{
		$taxRate = new Model_TaxRate(1);

		$taxRate->addTaxRate(0, 5, 'Test');

		$taxAmount = $taxRate->calculateTax(5.25);

		$this->assertEquals(0.05, $taxRate->getTaxRate());
		$this->assertEquals(0.2625, $taxAmount);
		$this->assertEquals('Test (5.00%)', $taxRate->getDescription());
	}

	function test_can_Compute_Tax_Rate_With_Rates_At_Same_Priority()
	{
		$taxRate = new Model_TaxRate(1);

		$taxRate->addTaxRate(0, 5, 'Test');
		$taxRate->addTaxRate(0, 2.5, 'Test2');

		$taxAmount = $taxRate->calculateTax(5.25);

		$this->assertEquals(0.075, $taxRate->getTaxRate());
		$this->assertEquals(0.39375, $taxAmount);
		$this->assertEquals('Test (5.00%) + Test2 (2.50%)', $taxRate->getDescription());
	}

	function test_can_Compute_Tax_Rate_With_Rates_At_Different_Priorities()
	{
		$taxRate = new Model_TaxRate(1);

		$taxRate->addTaxRate(1, 2.5, 'Test2');
		$taxRate->addTaxRate(0, 5, 'Test');

		$taxAmount = $taxRate->calculateTax(5.25);

		$this->assertEquals(0.07625, $taxRate->getTaxRate());
		$this->assertEquals(0.4003125, $taxAmount);
		$this->assertEquals('Test (5.00%) + Test2 (2.50%)', $taxRate->getDescription());
	}

	function test_can_Calculate_Canada_Tax_Rate()
	{
		// See: http://community.freshbooks.com/support/converting-to-compound-taxes-from-effective-tax-rates/

		$taxRate = new Model_TaxRate(1);

		$taxRate->addTaxRate(1, 8.5, 'QST');
		$taxRate->addTaxRate(0, 5, 'GST');

		$taxAmount = $taxRate->calculateTax(300);

		$this->assertEquals(0.13925, $taxRate->getTaxRate());
		$this->assertEquals(41.775, $taxAmount);
		$this->assertEquals('GST (5.00%) + QST (8.50%)', $taxRate->getDescription());
	}
}

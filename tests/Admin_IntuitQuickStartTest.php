<?php

/**
 * Class Admin_IntuitQuickStartTest
 */
class Admin_IntuitQuickStartTest extends PHPUnit_Framework_TestCase
{
	public function test_action_render_Successfully_Renders_Html_With_Correct_Variables()
	{
		$settings = array(
			'intuit_quickbooks_version' => 'desktop',
			'intuit_sync_taxes' => 'No',
			'intuit_anywhere_connected' => 'No',
		);
		
		$view = Mock_Helpers::getDdmViewFake($this);


		$view->expects($this->at(6))
			->method('assign')
			->with($this->equalTo('intuit'), $this->equalTo(array('quickbooks_version' => 'desktop', 'sync_taxes' => 'No')));
		$view->expects($this->at(7))
			->method('fetch')
			->with($this->equalTo('pages/intuit-quickstart/index'));

		$quickstart = new Admin_Intuit_QuickStart(null, $settings, $view, null);
		
		$quickstart->action_render();
	}
	
	public function test_action_save_Successful_Save_Of_Settings()
	{
		$db = Mock_Helpers::getDbFake($this, array('reset', 'assign', 'assignStr', 'update'));
		$db->expects($this->atLeastOnce())
			->method('reset');

		$settings = array(
			'intuit_quickbooks_version' => 'online',
			'intuit_sync_taxes' => 'Yes'
		);
		
		$view = Mock_Helpers::getDdmViewFake($this); //Ddm_View_Fake::getInstance();
		
		$_REQUEST['quickbooks_version'] = 'desktop';
		$_REQUEST['sync_taxes'] = '0';
		
		$quickstart = new Admin_Intuit_QuickStart($db, $settings, $view, null);
		
		$quickstart->action_save();
		$quickbooks_version = $quickstart->getQuickbooksVersion();
		$sync_taxes = $quickstart->getSyncTaxes();
		
		$this->assertEquals('desktop', $quickbooks_version);
		$this->assertEquals('No', $sync_taxes);
	}
	
	public function test_action_save_Pass_Invalid_Value_For_Sync_Taxes_Returns_String_No()
	{
		$db = Mock_Helpers::getDbFake($this, array('reset', 'assign', 'assignStr', 'update'));
		$db->expects($this->atLeastOnce())
			->method('reset');

		$settings = array(
			'intuit_quickbooks_version' => 'online',
			'intuit_sync_taxes' => 'Yes'
		);
		
		$view = Mock_Helpers::getDdmViewFake($this);
		
		$_REQUEST['sync_taxes'] = 'Some Invalid Value';
		
		$quickstart = new Admin_Intuit_QuickStart($db, $settings, $view, null);
		
		$quickstart->action_save();
		$quickbooks_version = $quickstart->getQuickbooksVersion();
		$sync_taxes = $quickstart->getSyncTaxes();
		
		$this->assertEquals('online', $quickbooks_version);
		$this->assertEquals('No', $sync_taxes);
	}
	
	public function test_action_save_Pass_String_1_Returns_String_Yes()
	{
		$db = Mock_Helpers::getDbFake($this, array('reset', 'assign', 'assignStr', 'update'));
		$db->expects($this->atLeastOnce())
			->method('reset');
		
		$settings = array(
			'intuit_quickbooks_version' => 'desktop',
			'intuit_sync_taxes' => 'Yes'
		);
		
		$view = Mock_Helpers::getDdmViewFake($this);
		
		$_REQUEST['sync_taxes'] = '1';
		
		$quickstart = new Admin_Intuit_QuickStart($db, $settings, $view, null);
		
		$quickstart->action_save();
		$quickbooks_version = $quickstart->getQuickbooksVersion();
		$sync_taxes = $quickstart->getSyncTaxes();
		
		$this->assertEquals('desktop', $quickbooks_version);
		$this->assertEquals('Yes', $sync_taxes);
	}
}
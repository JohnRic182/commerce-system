<?php

class Admin_ChartTest extends PHPUnit_Framework_TestCase
{
	public function test_Will_Return_Chart_In_Flot_Format()
	{
		$chart = new Chart();
		$chart->setTitle('Some Title');
		$title = $chart->getTitle();
		
		$year  = isset($year)  ? intval($year)  : date('Y');
		$month = isset($month) ? intval($month) : date('m');
		
		$series = new ChartSeries();
		
		$series->setTitle('Monthly Sales Statistics for '.date("F Y", mktime(0,0,0, $month, 1, $year)).' (USD)');
		$series->setXAxisLabel('Date ('.date("F Y", mktime(0,0,0, $month, 1, $year)).')');
		$series->setYAxisLabel('Sales (January 2012)');
		$series->setYAxisFormat('$%d');
		
		for ($i = 1; $i <= 31; $i++)
		{
			$series->addData($i, number_format(rand(0,10000), 2, '.', ''), "Jan {$i}, 2011");
		}
		
		$chart->addSeries($series);
		
		$this->assertEquals('Some Title', $title);
	}
}
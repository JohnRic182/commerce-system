<?php

/**
* 
*/
class FileUtilityTest extends PHPUnit_Framework_TestCase
{
	protected function setUp()
	{
//		vfsStreamWrapper::register();
//		vfsStreamWrapper::setRoot(new vfsStreamDirectory('tmp'));
	}
	
	public function test_getFilePermission()
	{
		$this->markTestSkipped();

		mkdir(vfsStream::url('tmp/0755'), 0755, TRUE);
		mkdir(vfsStream::url('tmp/0777'), 0777, TRUE);

		$this->assertTrue(vfsStreamWrapper::getRoot()->hasChild('0755'));
		$this->assertTrue(vfsStreamWrapper::getRoot()->hasChild('0777'));
		$this->assertEquals(0755, octdec(FileUtils::getFilePermission(vfsStream::url('tmp/0755'))));
		$this->assertEquals(0777, octdec(FileUtils::getFilePermission(vfsStream::url('tmp/0777'))));
	}

	public function test_removeDirectory_Will_Remove_Without_Dry_Run()
	{
		$this->markTestSkipped();

		mkdir(vfsStream::url('tmp/remove/first'), 0755, TRUE);
		mkdir(vfsStream::url('tmp/remove/first/subdirectory'), 0755, TRUE);
		mkdir(vfsStream::url('tmp/remove/second'), 0755, TRUE);

		$errors = array();
		$result = FileUtils::removeDirectory(vfsStream::url('tmp/remove/first'), $errors, false);

		$this->assertFileNotExists(vfsStream::url('tmp/remove/first/subdirectory'));
		$this->assertFileNotExists(vfsStream::url('tmp/remove/first'));
		$this->assertFileExists(vfsStream::url('tmp/remove/second'));
	}

	public function test_removeDirectory_Will_Not_Remove_With_Dry_Run()
	{
		$this->markTestSkipped();

		mkdir(vfsStream::url('tmp/remove/first'), 0755, TRUE);
		mkdir(vfsStream::url('tmp/remove/first/subdirectory'), 0755, TRUE);
		mkdir(vfsStream::url('tmp/remove/second'), 0755, TRUE);

		$errors = array();
		$result = FileUtils::removeDirectory(vfsStream::url('tmp/remove/first'), $errors, true);

		$this->assertFileNotExists(vfsStream::url('tmp/remove/first/subdirectory'));
		$this->assertFileExists(vfsStream::url('tmp/remove/first'));
		$this->assertFileExists(vfsStream::url('tmp/remove/second'));
	}
}
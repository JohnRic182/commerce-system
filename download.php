<?php

	/**
	 * Load libraries
	 */
	require_once 'content/classes/_boot.php';

	if (!defined('DB_PREFIX')) define('DB_PREFIX', '');

	$db = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

	$a = isset($_GET['a'])?$_GET['a']:'';
	$b = isset($_GET['b'])?$_GET['b']:'';

	$db->query("SELECT value FROM ".DB_PREFIX."settings WHERE name='DigitalProductsDownloadLimit'");
	method_exists($db, 'moveNext') ? $db->moveNext() : $db->movenext();

	$limit = intval($db->col['value']);

	$db->query("SELECT * FROM ".DB_PREFIX."orders_content WHERE oid=".intval($a)." AND digital_product_key='".$db->escape($b)."'");

	if (method_exists($db, 'moveNext') ? $db->moveNext() : $db->movenext())
	{
		if ($db->col['digital_product_downloads'] < $limit)
		{
			$filename = './content/download/' . $db->col['digital_product_file'];
			if (is_file($filename))
			{
				header('Pragma: public');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Content-Type: application/force-download');
				header('Content-Type: application/octet-stream');
				header('Content-Type: application/download');
				header('Content-Disposition: attachment; filename='.basename($filename).';');
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: ' . filesize($filename));

				@set_time_limit(360000000);

				$f = fopen($filename, 'r');
				while ($s = fread($f, 1024)) echo $s;
				fclose($f);

				$db->query('UPDATE '.DB_PREFIX."orders_content SET digital_product_downloads = digital_product_downloads+1 WHERE ocid=".intval($db->col['ocid']));
			}
			else
			{
				print 'There was a problem processing your download. Please contact support';
			}
		}
		else
		{
			print 'Download limit exceeded. Please contact support if you get this message in error.';
		}
	}
	else
	{
		print "Can't download file: invalid params";
	}

	$db->done();
	exit();
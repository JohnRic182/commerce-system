<?php

	if (!(isset($_REQUEST['chart'])))
	{
		/**
		 * Do not cache output
		 */
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate, post-check=0, pre-check=0');
		header('Pragma: no-cache');
	}

	define('ENV', true);

	/**
	 * Load libraries
	 */
	require_once 'content/classes/_boot.php';

	$registry = new Framework_Registry();
	$registry->set('db', $db);
	$registry->setParameter('settings', $settings);

	define('IS_LABEL', (md5(str_replace(' ', '', strtolower($label_app_name))) != '944c32e5f7d6ab022c1101c963ac1370'));

	/**
	 * Unset some possibly posted vars/
	 */
	$unsetVars = array(
		'plesk_release', 'label_app_name',  'label_help_enabled',
		'label_home_site', 'label_terms', 'label_support_area', 'label_privacy_policy',
		'label_help_url', 'label_first_data', 'label_stamps', 'label_affiliate_tracking_url',
		'label_admin_tracking_script', 'settings', 'db'
	);

	foreach ($unsetVars as $unsetVar) unset($_POST[$unsetVar] , $_GET[$unsetVar], $_REQUEST[$unsetVar]);

	$request = Framework_Request::createFromGlobals();

	/**
	 * Check http-https url
	 */
	if (shouldBeSecure())
	{
		header('Location: ' . $settings['AdminHttpsUrl'] . getRequestUri());
		_session_write_close();
		exit();
	}

	Admin_SessionHandler::init($settings, $request->get('dmsid', null));

	/**
	 * Check current page
	 */
	$p = $request->get('p', 'home');

	/**
	 * Check iono
	 */
	$iono = new IonoKeys();

	if ($settings['ProxyAvailable'] == 'YES')
	{
		$iono->proxy_used = true;
		$iono->proxy_address = $settings['ProxyAddress'];
		$iono->proxy_port = $settings['ProxyPort'];
		
		if ($settings['ProxyRequiresAuthorization'] == 'YES')
		{
			$iono->proxy_requires_auth = true;
			$iono->proxy_username = $settings['ProxyUsername'];
			$iono->proxy_password = $settings['ProxyPassword'];
		}
	}

	$iono->iono_keys_call4520(trim(LICENSE_NUMBER), 'b575796b4295', 'content/cache/license/license.key', 604800); //172800 - 2 days

	if ($iono->status == '') $iono->get_status_1980();

	/**
	 * Check admin security
	 */
	unset($auth_ok);
	require('content/admin/admin_security.php');

	require_once('content/classes/Events/Register.php');

	if ($auth_ok && $iono->result == 1)
	{
		if (!file_exists('content/cache/languages/admin_english.php') || filemtime('content/admin/languages/english.php') > filemtime('content/cache/languages/admin_english.php'))
		{
			$admin_msg = include 'content/admin/languages/english.php';

			Framework_Translator::cache($admin_msg, 'content/cache/languages/admin_english.php');
		}

		$admin_msg = include 'content/cache/languages/admin_english.php';

		$translator = Framework_Translator::getInstance($admin_msg);

		/**
		 * Load other libraries
		 */
		require_once('content/admin/admin_zones.php');

		$currencies = new Currencies($db);

		if (!isset($_SESSION['admin_currency']))
		{
			$_SESSION['admin_currency'] = $admin_currency = $currencies->getDefaultCurrency();
		}
		else
		{
			$admin_currency = $_SESSION['admin_currency'];
		}

		$printing = isset($_REQUEST['printing']) ? true : false;
		$chart = isset($_REQUEST['chart']) ? true : false;
		$ajax = isset($_REQUEST['__ajax']) ? true : false;

		/**
		 * Check label things
		 */
		if (trim($label_support_area) == '')
		{
			$sn = array(
				'l' => LICENSE_NUMBER,
				'h' => $_SERVER['SERVER_NAME'],
				'i' => $_SERVER['SERVER_ADDR']
			);
			
			$label_support_area = 'http://www.cartmanual.com/support/get-support.php?a=' . urlencode(base64_encode(serialize($sn)));
		}

		$authstate = isset($_REQUEST['state']) ? $_REQUEST['state'] : '';
		if ($authstate == 'braintreeauth')
		{
			$controller = new Admin_Controller_Braintree($request, $db);
			$controller->authConnectAction();
		}
		else if ($auth_force_update_profile)
		{
			global $admin_auth_id;

			$controller = new Admin_Controller_AdminProfile($request, $db);
			$controller->setCurrentAdminId($admin_auth_id);

			if ($request->isPost())
			{
				$controller->updateSecurityProfileAction();
			}
			else
			{
				$controller->securityProfileAction();
			}
		}
		else
		{
			$router = new Admin_Router();

			$route = $router->getRoute($p, $request->get('mode', $request->get('action', 'index')));

			if ($route != null)
			{
				$controllerClassName = $route['controller'];
				$controllerActionName = $route['action'];

				/** @var Admin_Controller $controller */
				$controller = new $controllerClassName($request, $db);
				$controller->setCurrentAdminId($admin_auth_id);
				call_user_func(array($controller, $controllerActionName));
			}
			else
			{
				require_once('content/admin/' . $body);
			}
		}
	}
	else
	{
		header('Location: login.php');
	}

	$db->done();

	session_write_close();

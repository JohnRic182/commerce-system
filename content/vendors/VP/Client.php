<?php

class VP_Client {

    // @TODO version, url, authToken in config

    const URL_PRODUCTION = 'https://vipparcel.com/api/v1';
    const URL_TEST = 'https://vipparcel.com/api-test/v1';

	/**
	 * @var VP_Abstract_Request
	 */
	protected $_request;
    protected $_auth_token;
    protected $_is_test = FALSE;

    public function auth_token($value)
    {
        $this->_auth_token = $value;
        return $this;
    }

    public function is_test($value)
    {
        $this->_is_test = (bool) $value;
        return $this;
    }

    public function request(VP_Abstract_Request $request)
    {
        $this->_request = $request;
        return $this;
    }

    protected function _client_url()
    {
        if ($this->_is_test === TRUE) {
            return self::URL_TEST;
        }
        return self::URL_PRODUCTION;
    }

    protected function _default_params()
    {
        if ( ! $auth_token = $this->_auth_token) {
            throw new VP_Exception('Required parameter not passed: authToken');
        }
        return array('authToken' => $auth_token);
    }

    public function execute($settings = null)
    {
        if ($this->_request === NULL) {
            throw new VP_Exception('Request object does not exist');
        }

        $request_method = $this->_request->get_method();
        $request_url = $this->_request->get_url();

        $send_params = array_merge($this->_default_params(), $this->_request->get_params());

		if (defined('DEVMODE') && DEVMODE){
			@file_put_contents(
				dirname(dirname(dirname(__FILE__))).'/cache/log/shipping-vparcel.txt',
				date("Y/m/d-H:i:s")." - REQUEST:\n".http_build_query($send_params)."\n\n\n",
				FILE_APPEND
			);
		}
		$c = curl_init();
		if ($settings["ProxyAvailable"] == "YES"){
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5")){
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES"){
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		if (in_array($request_method, array('put', 'post')))
		{
			$full_url = $this->_client_url().$request_url;

			curl_setopt($c, CURLOPT_URL, $full_url);
			curl_setopt($c, CURLOPT_VERBOSE, 0);
			curl_setopt($c, CURLOPT_HEADER, 0);
			curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($send_params));
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
			set_time_limit(3000);
		}
		else
		{
			$full_url = $this->_client_url().$request_url.'?'.http_build_query($send_params);
			curl_setopt($c, CURLOPT_URL, $full_url);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$headers_array = array();
			$headers_array[] = 'CONTENT-TYPE: application/json';
			$headers_array[] = 'ACCEPT: application/json';

			curl_setopt($c, CURLOPT_HTTPHEADER, $headers_array);
			curl_setopt($c, CURLOPT_HEADER, false);
			
			set_time_limit(3000);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, FALSE);

			curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
		}

        /*
		$http_client = new Client();
        $http_client->setSslVerification(true);
        $http_client->getEventDispatcher()->addListener('request.error', function(\Guzzle\Common\Event $event) {
            if ($event['response']->getStatusCode() != 200) {
                $event->stopPropagation();
            }
        });

        $full_url = $this->_client_url().$request_url;

        if (in_array($request_method, array('put', 'post')))
        {
            $response = $http_client->$request_method($full_url, array('content-type' => 'application/json'), $send_params);
        }
        else
        {
            $response = $http_client->$request_method($full_url.'?'.http_build_query($send_params), array('content-type' => 'application/json'));
        }
        */

		$buffer = curl_exec($c);
		if (defined('DEVMODE') && DEVMODE){
			if (curl_errno($c) > 0){
				@file_put_contents(
					dirname(dirname(dirname(__FILE__))).'/cache/log/shipping-vparcel.txt',
					date("Y/m/d-H:i:s")." - CURLERROR:\n".curl_error($c)."\n\n\n",
					FILE_APPEND
				);
			}
		}

		if (defined('DEVMODE') && DEVMODE){
			@file_put_contents(
				dirname(dirname(dirname(__FILE__))).'/cache/log/shipping-vparcel.txt',
				date("Y/m/d-H:i:s")." - RESPONSE:\n".$buffer."\n\n\n",
				FILE_APPEND
			);
		}
		curl_close($c);
        return json_decode($buffer, TRUE);
    }

}
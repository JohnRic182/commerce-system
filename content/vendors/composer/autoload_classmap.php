<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname(dirname($vendorDir));

return array(
    'PAYMENT_2CHECKOUT' => $baseDir . '/content/engine/payment/payment_2checkout.php',
    'PAYMENT_AMAZON_SP' => $baseDir . '/content/engine/payment/payment_amazon_sp.php',
    'PAYMENT_AMERICAN_EXPRESS' => $baseDir . '/content/engine/payment/payment_american_express.php',
    'PAYMENT_AUTHORIZENET' => $baseDir . '/content/engine/payment/payment_authorizenet.php',
    'PAYMENT_AUTHORIZENET_AIM_3_1' => $baseDir . '/content/engine/payment/payment_authorizenet_aim_3_1.php',
    'PAYMENT_AUTHORIZENET_CIM' => $baseDir . '/content/engine/payment/payment_authorizenet_cim.php',
    'PAYMENT_AUTHORIZENET_CP_1_0' => $baseDir . '/content/engine/payment/payment_authorizenet_cp_1_0.php',
    'PAYMENT_AUTHORIZENET_ECHECK' => $baseDir . '/content/engine/payment/payment_authorizenet_echeck.php',
    'PAYMENT_BEANSTREAM' => $baseDir . '/content/engine/payment/payment_beanstream.php',
    'PAYMENT_BONGOCHECKOUT' => $baseDir . '/content/engine/payment/payment_bongocheckout.php',
    'PAYMENT_BRAINTREE' => $baseDir . '/content/engine/payment/payment_braintree.php',
    'PAYMENT_CARDINALCOMMERCE' => $baseDir . '/content/engine/payment/payment_cardinalcommerce.php',
    'PAYMENT_CART_CCS' => $baseDir . '/content/engine/payment/payment_cart_ccs.php',
    'PAYMENT_EPAY' => $baseDir . '/content/engine/payment/payment_epay.php',
    'PAYMENT_FIRSTDATA' => $baseDir . '/content/engine/payment/payment_firstdata.php',
    'PAYMENT_FUTUREPAY' => $baseDir . '/content/engine/payment/payment_futurepay.php',
    'PAYMENT_ITRANSACT' => $baseDir . '/content/engine/payment/payment_itransact.php',
    'PAYMENT_MONERIS' => $baseDir . '/content/engine/payment/payment_moneris.php',
    'PAYMENT_MONEYBOOKERS' => $baseDir . '/content/engine/payment/payment_moneybookers.php',
    'PAYMENT_NETBILLING' => $baseDir . '/content/engine/payment/payment_netbilling.php',
    'PAYMENT_ORBITAL' => $baseDir . '/content/engine/payment/payment_orbital.php',
    'PAYMENT_PAYJUNCTION' => $baseDir . '/content/engine/payment/payment_payjunction.php',
    'PAYMENT_PAYLEAP' => $baseDir . '/content/engine/payment/payment_payleap.php',
    'PAYMENT_PAYPALADV' => $baseDir . '/content/engine/payment/payment_paypaladv.php',
    'PAYMENT_PAYPALEC' => $baseDir . '/content/engine/payment/payment_paypalec.php',
    'PAYMENT_PAYPALHSUK' => $baseDir . '/content/engine/payment/payment_paypalhsuk.php',
    'PAYMENT_PAYPALIPN' => $baseDir . '/content/engine/payment/payment_paypalipn.php',
    'PAYMENT_PAYPALPFLINK' => $baseDir . '/content/engine/payment/payment_paypalpflink.php',
    'PAYMENT_PAYPALPFPRO' => $baseDir . '/content/engine/payment/payment_paypalpfpro.php',
    'PAYMENT_PAYPALPRO' => $baseDir . '/content/engine/payment/payment_paypalpro.php',
    'PAYMENT_PAYPALWPS' => $baseDir . '/content/engine/payment/payment_paypalwps.php',
    'PAYMENT_PSIGATE' => $baseDir . '/content/engine/payment/payment_psigate.php',
    'PAYMENT_QBMS' => $baseDir . '/content/engine/payment/payment_qbms.php',
    'PAYMENT_SHIFT4I4GO' => $baseDir . '/content/engine/payment/payment_shift4i4go.php',
    'PAYMENT_STRIPE' => $baseDir . '/content/engine/payment/payment_stripe.php',
    'PAYMENT_TRANSACTIONCENTRAL' => $baseDir . '/content/engine/payment/payment_transactioncentral.php',
    'PAYMENT_VIRTUAL_MERCHANT' => $baseDir . '/content/engine/payment/payment_virtual_merchant.php',
    'PAYMENT_VSPFORM' => $baseDir . '/content/engine/payment/payment_vspform.php',
    'PAYMENT_WORLDPAY' => $baseDir . '/content/engine/payment/payment_worldpay.php',
    'QbmsGateway' => $baseDir . '/content/engine/payment/qbms/qbms_gateway.php',
    'SecureNetGateway' => $baseDir . '/content/engine/payment/securenet/class_gateway.php',
    'SignatureException' => $baseDir . '/content/engine/payment/amazon/SignatureUtilsForOutbound.php',
    'SignatureUtilsForOutbound' => $baseDir . '/content/engine/payment/amazon/SignatureUtilsForOutbound.php',
    'mpgAvsInfo' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgCustInfo' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgCvdInfo' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgGlobals' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgHttpsPost' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgRecur' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgRequest' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgResponse' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'mpgTransaction' => $baseDir . '/content/engine/payment/moneris/mpgClasses.php',
    'orbital_client' => $baseDir . '/content/engine/payment/orbital/orbital_client.php',
    'securexml_transaction' => $baseDir . '/content/engine/payment/securepayau/securexml.php',
    'umTransaction' => $baseDir . '/content/engine/payment/epay/usaepay.php',
);

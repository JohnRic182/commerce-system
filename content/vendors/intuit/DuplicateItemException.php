<?php

class intuit_DuplicateItemException extends Exception
{
	public function __construct($message = '')
	{
		parent::__construct($message);
	}
}
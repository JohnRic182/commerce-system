<?php

abstract class intuit_Model_Item extends intuit_Model_BaseModel
{
	public function setItemDetails($intuitName, $productRecord, $incomeAccount, $expenseAccount, $currencyCode)
	{
		if ($this->isNew)
		{
			$this->setModified(true);

			$this->setIdRef('IncomeAccountRef', 'AccountId', $incomeAccount->Id, $incomeAccount->getIdDomain());
		}

		$this->Name = $intuitName;

		$expenseAccountId = $this->getExpenseAccountId();
		if (is_null($expenseAccountId) && !is_null($expenseAccount))
		{
			$this->setIdRef('ExpenseAccountRef', 'AccountId', $expenseAccount->Id, $expenseAccount->getIdDomain());
		}

		$this->Desc = $productRecord['title'];

		$this->Taxable = $productRecord['is_taxable'] == 'Yes' ? 'true' : 'false';
		$this->setUnitPrice($productRecord['price'], trim($currencyCode));
	}

	protected function setUnitPrice($amount, $currency)
	{
		$priorValue = isset($this->values['UnitPrice']['Amount']) ? $this->values['UnitPrice']['Amount'] : null;

		$unitPrice = array();

		if ($currency != '')
			$unitPrice['CurrencyCode'] = $currency;

		$unitPrice['Amount'] = $amount;

		$this->values['UnitPrice'] = $unitPrice;

		if ($priorValue != $amount) $this->setModified();
	}

	protected function getExpenseAccountId()
	{
		return $this->getValue('AccountId', 'ExpenseAccountRef');
	}

	protected function setIdRef($name, $idName, $id, $idDomain = null)
	{
		$ref = $this->getValue($name);

		if (is_null($ref)) $ref = array();

		$priorValue = isset($ref[$idName]) ? $ref[$idName] : null;

		$ref[$idName] = $id;

		if (trim($idDomain) != '')
		{
			$ref[$idName.'_idDomain'] = $idDomain;
		}
		else
		{
			unset($ref[$idName.'_idDomain']);
		}

		$this->values[$name] = $ref;

		if ($priorValue != $id) $this->setModified();
	}
}
<?php

class intuit_Model_Mapping_ModelMapper
{
	protected $qbType;

	public function __construct($qbType, $classMetadata = 'intuit_Model_Mapping_ClassMetadata')
	{
		$this->qbType = $qbType;
	}

	public function loadAccount($accountXml)
	{
		return $this->loadType($accountXml, 'intuit_Model_'.$this->qbType.'_Account');
	}

	public function loadItem($itemXml)
	{
		return $this->loadType($itemXml, 'intuit_Model_'.$this->qbType.'_Item');
	}

	public function loadCustomer($customerXml)
	{
		return $this->loadType($customerXml, 'intuit_Model_'.$this->qbType.'_Customer');
	}

	public function loadSalesReceipt($receiptXml)
	{
		return $this->loadType($receiptXml, 'intuit_Model_'.$this->qbType.'_SalesReceipt');
	}

	public function loadType($xml, $type)
	{
		return new $type($xml);
	}
}

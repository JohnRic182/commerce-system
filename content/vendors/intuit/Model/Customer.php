<?php

abstract class intuit_Model_Customer extends intuit_Model_BaseModel
{
	public function setCustomerDetails($userRecord, $intuitName)
	{
		$this->Name = $intuitName;
		$this->GivenName = $userRecord['fname'];
		$this->FamilyName = $userRecord['lname'];
		$this->ShowAs = trim($userRecord['fname'].' '.$userRecord['lname']);

		if (!is_null($userRecord['address1']) && trim($userRecord['address1']) != '')
		{
			$address = intuit_Model_Address::parse($userRecord['address1'], $userRecord['address2'], $userRecord['city'], 
							$userRecord['billing_state_name'], $userRecord['zip'], $userRecord['company'], 'Billing');

			$this->setAddress($address['Line1'], $address['Line2'], $address['Line3'], $address['Line4'], $address['Line5'],
					$address['City'], $address['CountrySubDivisionCode'], $address['PostalCode'], $address['Tag']);
		}

		if (!is_null($userRecord['shipping_address1']) && trim($userRecord['shipping_address1']) != '')
		{
			$address = intuit_Model_Address::parse($userRecord['shipping_address1'], $userRecord['shipping_address2'], $userRecord['shipping_city'], 
					$userRecord['shipping_state_name'], $userRecord['shipping_zip'], $userRecord['shipping_company'], 'Shipping');

			$this->setAddress($address['Line1'], $address['Line2'], $address['Line3'], $address['Line4'], $address['Line5'],
					$address['City'], $address['CountrySubDivisionCode'], $address['PostalCode'], $address['Tag']);
		}

		if (!is_null($userRecord['email']) && trim($userRecord['email']) != '')
		{
			$this->setEmail($userRecord['email']);
		}

		if (!is_null($userRecord['phone']) && trim($userRecord['phone']) != '')
		{
			$this->setPhone($userRecord['phone']);
		}
	}

	protected function setAddress($line1, $line2, $line3, $line4, $line5, $city, $state, $zip, $tag = 'Billing')
	{
		$address = array(
			'Line1' => $line1,
			'Line2' => $line2,
			'Line3' => $line3,
			'Line4' => $line4,
			'Line5' => $line5,
			'City' => $city,
			'CountrySubDivisionCode' => $state,
			'PostalCode' => $zip,
			'Tag' => $tag,
		);

		$this->replaceAddress($address);
	}

	protected function replaceAddress($address)
	{
		if (!isset($this->values['Address']))
		{
			$this->values['Address'] = array();
		}

		$addresses = array();
		$found = false;
		$prevAddress = null;
		foreach ($this->values['Address'] as $old)
		{
			if ($address['Tag'] == $old['Tag'])
			{
				$addresses[] = $address;
				$found = true;
				$prevAddress = $old;
			}
			else
			{
				$addresses[] = $old;
			}
		}

		if (!$found)
		{
			$addresses[] = $address;
		}

		$this->values['Address'] = $addresses;

		$modified = is_null($prevAddress);

		if (!$modified)
		{
			foreach ($prevAddress as $key => $value)
			{
				if ((!isset($address[$key]) && trim($value) != '') || $address[$key] != $value)
				{
					$modified = true;
					break;
				}
			}
			foreach ($address as $key => $value)
			{
				if (!isset($prevAddress[$key]) && trim($value) != '')
				{
					$modified = true;
					break;
				}
			}
		}

		if ($modified) $this->setModified();
	}

	protected function setEmail($email)
	{
		$priorValue = isset($this->values['Email']['Address']) ? $this->values['Email']['Address'] : null;

		if ($priorValue == $email) return;

		$emailValues = array();

		$emailValues['Address'] = $email;
		$emailValues['Tag'] = 'Home';

		$this->values['Email'] = $emailValues;

		$this->setModified();
	}

	protected function setPhone($phone)
	{
		$priorValue = isset($this->values['Phone']['FreeFormNumber']) ? $this->values['Phone']['FreeFormNumber'] : null;

		if ($priorValue == $phone) return;

		$phoneValues = array();

		$phoneValues['FreeFormNumber'] = $phone;
		$phoneValues['Tag'] = 'Home';

		$this->values['Phone'] = $phoneValues;

		$this->setModified();
	}
}
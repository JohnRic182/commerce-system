<?php

abstract class intuit_Model_Account extends intuit_Model_BaseModel
{
	public abstract function isExpenseAccount();
	public abstract function isIncomeAccount();

	public function getIdDomain()
	{
		return $this->getValue('Id_idDomain');
	}
}
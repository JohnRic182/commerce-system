<?php

abstract class intuit_Model_Preferences extends intuit_Model_BaseModel
{
	public abstract function taxesEnabled();

	public abstract function discountsEnabled();

	public abstract function multiCurrencyEnabled();

	public abstract function inventoryEnabled();
}
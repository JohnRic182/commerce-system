<?php

class intuit_Model_QBD_SalesTaxCode extends intuit_Model_BaseModel
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<SalesTaxCode>
	<Name/>
	<Active/>
	<Desc/>
	<Taxable/>
</SalesTaxCode>";

		return simplexml_load_string($xmlStr);
	}
}
<?php

class intuit_Model_QBD_Preferences extends intuit_Model_Preferences
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Preferences>
	<TaxId></TaxId>
	<DiscountAccountId></DiscountAccountId>
	<UsingMultiCurrency>false</UsingMultiCurrency>
	<UsingInventory>false</UsingInventory>
</Preferences>
";
		return simplexml_load_string($xmlStr);
	}

	public function taxesEnabled()
	{
		return !is_null($this->getValue('TaxId'));
	}

	public function discountsEnabled()
	{
		return !is_null($this->getValue('DiscountAccountId'));
	}

	public function multiCurrencyEnabled()
	{
		return $this->getValue('UsingMultiCurrency') == 'true';
	}

	public function inventoryEnabled()
	{
		return $this->getValue('UsingInventory') == 'true';
	}
}
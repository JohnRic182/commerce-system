<?php

class intuit_Model_QBD_SalesReceipt extends intuit_Model_SalesReceipt
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<SalesReceipt>
	<Id/>
	<SyncToken/>
	<ExternalKey/>
	<Synchornized/>
	<AlternateId/>
	<CustomField/>
	<Draft/>
	<ObjectState/>
	<Header/>
	<Line/>
</SalesReceipt>
";
		return simplexml_load_string($xmlStr);
	}

	public function setTaxInformation($orderRecord, $taxesEnabled, $settings)
	{
		if ($orderRecord['tax_amount'] > 0)
		{
			$this->header->setValue('TaxName', $settings['intuit_default_tax_rate_name']);
		}
		else
		{
			$this->header->setValue('TaxName', $settings['intuit_zero_tax_rate_name']);
		}

		$this->header->removeValue('TaxRate');
		$this->header->removeValue('TaxId');
		$this->header->removeValue('TaxId_idDomain');

		$this->header->setValue('TaxAmt', $orderRecord['tax_amount']);
	}

	protected function getHeader($headerValues = null)
	{
		$header = new intuit_Model_QBD_SalesReceiptHeader();

		if (is_array($headerValues) && count($headerValues) >0)
		{
			$header->setValues($headerValues);
			$header->clearModified();
		}

		return $header;
	}

	protected function isQBO()
	{
		return false;
	}

	protected function getLine($lineValues = null)
	{
		$line = new intuit_Model_QBD_SalesReceiptLine();

		if (is_array($lineValues) && count($lineValues) > 0)
		{
			$line->setValues($lineValues);
			$line->clearModified();
		}

		return $line;
	}
}
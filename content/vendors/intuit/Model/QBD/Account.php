<?php

class intuit_Model_QBD_Account extends intuit_Model_Account
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Account>
	<Id/>
	<SyncToken/>
	<ExternalKey/>
	<Synchronized/>
	<AlternateId/>
	<CustomField/>
	<Draft/>
	<ObjectState/>
	<Name/>
	<AccountParentId/>
	<AccountParentName/>
	<Desc/>
	<Active/>
	<Type/>
	<Subtype/>
	<AcctNum/>
	<BankNum/>
	<RoutingNum/>
	<OpeningBalance/>
	<OpeningBalanceDate/>
	<CurrentBalance/>
	<CurrentBalanceWithSubAccounts/>
</Account>
";
		return simplexml_load_string($xmlStr);
	}

	public function isExpenseAccount()
	{
		return $this->getValue('Type') == 'Expense';
	}

	public function isIncomeAccount()
	{
		return $this->getValue('Type') == 'Revenue' || $this->getValue('Subtype') == 'Income';
	}
}
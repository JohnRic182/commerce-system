<?php

class intuit_Model_QBD_SalesTax extends intuit_Model_BaseModel
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<SalesTax>
	<Name/>
	<Desc/>
	<TaxRate/>
</SalesTax>";

		return simplexml_load_string($xmlStr);
	}
}
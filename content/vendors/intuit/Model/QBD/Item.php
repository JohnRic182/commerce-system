<?php

class intuit_Model_QBD_Item extends intuit_Model_Item
{
	public function setItemDetails($intuitName, $productRecord, $incomeAccount, $expenseAccount, $currencyCode)
	{
		parent::setItemDetails($intuitName, $productRecord, $incomeAccount, $expenseAccount, $currencyCode);

		if ($expenseAccount !== null && $expenseAccount->getValue('Subtype') == 'Cost of Goods Sold')
		{
			$this->removeValue('ExpenseAccountRef');
			$this->setIdRef('COGSAccountRef', 'AccountId', $expenseAccount->Id, $expenseAccount->getIdDomain());
		}
	}

	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Item>
	<Id/>
	<SyncToken/>
	<ExternalKey/>
	<Synchronized/>
	<AlternateId/>
	<CustomField/>
	<Draft/>
	<ObjectState/>
	<ItemParentId/>
	<ItemParentName/>
	<Name/>
	<Desc/>
	<Taxable/>
	<Active/>
	<UnitPrice/>
	<RatePercent/>
	<Type/>
	<UOMId/>
	<UOMAbbrv/>
	<IncomeAccountRef/>
	<PurchaseDesc/>
	<PurchaseCost/>
	<ExpenseAccountRef/>
	<COGSAccountRef/>
	<AssetAccountRef/>
	<PrefVendorRef/>
	<AvgCost/>
	<QtyOnHand/>
	<QtyOnPurchaseOrder/>
	<QtyOnSalesOrder/>
	<ReorderPoint/>
	<ManPartNum/>
	<PrintGroupedItems/>
</Item>
";
		return simplexml_load_string($xmlStr);
	}

	protected function cleanupValues()
	{
		parent::cleanupValues();

		//Causes an error on QBD sync
		//Should only error when Expense Account is not set
		unset($this->values['PurchaseCost']);
	}
}
<?php

class intuit_Model_QBO_Preferences extends intuit_Model_Preferences
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Preferences>
	<TaxEnabled>false</TaxEnabled>
	<DiscountEnabled>false</DiscountEnabled>
</Preferences>
";
		return simplexml_load_string($xmlStr);
	}

	public function taxesEnabled()
	{
		return $this->getValue('TaxEnabled') == 'true';
	}

	public function discountsEnabled()
	{
		return $this->getValue('DiscountEnabled') == 'true';
	}

	public function multiCurrencyEnabled()
	{
		return false;
	}

	public function inventoryEnabled()
	{
		return false;
	}
}
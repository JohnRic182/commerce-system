<?php

class intuit_Model_QBO_SalesReceiptHeader extends intuit_Model_BaseModel
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Header>
	<DocNumber/>
	<TxnDate/>
	<Msg/>
	<Note/>
	<Status/>
	<CustomerId/>
	<CustomerName/>
	<ClassId/>
	<SalesTaxCodeId/>
	<SalesTaxCodeName/>
	<ShipDate/>
	<SubTotalAmt/>
	<TaxRate/>
	<TaxAmt/>
	<TotalAmt/>
	<ToBePrinted/>
	<ToBeEmailed/>
	<ShipAddr>
		<Line1/>
		<Line2/>
		<Line3/>
		<Line4/>
		<Line5/>
		<City/>
		<CountrySubDivisionCode/>
		<PostalCode/>
		<Tag/>
	</ShipAddr>
	<ShipMethodId/>
	<ShipMethodName/>
	<DepositToAccountId/>
	<PaymentMethodId/>
	<PaymentMethodName/>
	<DiscountAmt/>
	<DiscountRate/>
	<DiscountTaxable/>
</Header>";

		return simplexml_load_string($xmlStr);
	}
}
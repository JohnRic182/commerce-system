<?php

class intuit_Model_QBO_Customer extends intuit_Model_Customer
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Customer>
	<Id/>
	<SyncToken/>
	<Name></Name>
	<Address/>
	<Phone/>
	<WebSite/>
	<Email/>
	<ExternalId/>
	<GivenName></GivenName>
	<MiddleName/>
	<FamilyName></FamilyName>
	<Suffix/>
	<Gender/>
	<BirthDate/>
	<DBAName/>
	<TaxIdentifier/>
	<ShowAs/>
	<SalesTermId/>
	<SalesTaxCodeId/>
	<SalesTaxCodeName/>
	<PaymentMethodId/>
	<PaymentMethodName/>
	<OpenBalance/>
</Customer>
";
		return simplexml_load_string($xmlStr);
	}

	public function cleanupValues()
	{
		parent::cleanupValues();

		unset($this->values['Phone']['Tag']);
		unset($this->values['Email']['Tag']);
	}
}
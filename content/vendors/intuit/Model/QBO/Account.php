<?php

class intuit_Model_QBO_Account extends intuit_Model_Account
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Account>
	<Id/>
	<SyncToken/>
	<Name/>
	<AccountParentId/>
	<Desc/>
	<Subtype/>
	<AcctNum/>
	<OpeningBalance/>
	<OpeningBalanceDate/>
	<CurrentBalance/>
</Account>
";
		return simplexml_load_string($xmlStr);
	}

	public function isExpenseAccount()
	{
		$expenseSubTypes = array('SuppliesMaterials', 'SuppliesMaterialsCogs', 'Inventory');
		return in_array($this->getValue('Subtype'), $expenseSubTypes);
	}

	public function isIncomeAccount()
	{
		$incomeSubTypes = array('SalesOfProductIncome', 'ServiceFeeIncome', 'NonProfitIncome', 'OtherMiscellaneousIncome', 'OtherPrimaryIncome',);
		return in_array($this->getValue('Subtype'), $incomeSubTypes);
	}
}
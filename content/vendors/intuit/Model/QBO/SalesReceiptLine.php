<?php

class intuit_Model_QBO_SalesReceiptLine extends intuit_Model_BaseModel
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Line>
	<Id/>
	<Desc/>
	<Amount/>
	<ClassId/>
	<Taxable/>
	<ItemId/>
	<UnitPrice/>
	<Qty/>
	<OverrideItemAccountId/>
	<SalesTaxCodeId/>
	<SalesTaxCodeName/>
	<ServiceDate/>
</Line>";

		return simplexml_load_string($xmlStr);
	}

	public function getItemId()
	{
		return $this->getValue('ItemId');
	}

	public function getItemName()
	{
		return $this->getValue('ItemName');
	}

	public function getDesc()
	{
		return $this->getValue('Desc');
	}

	public function getAmount()
	{
		return $this->getValue('Amount');
	}

	public function setItemId($itemId, $idDomain = null)
	{
		$this->setValue('ItemId', $itemId);

		if (!is_null($idDomain))
		{
			$this->setValue('ItemId_idDomain', $idDomain);
		}
	}
}
<?php

class intuit_Model_QBO_Item extends intuit_Model_Item
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Item>
	<Id/>
	<SyncToken/>
	<ItemParentId/>
	<ItemParentName/>
	<Name/>
	<Desc/>
	<Taxable/>
	<UnitPrice/>
	<IncomeAccountRef/>
	<PurchaseDesc/>
	<PurchaseCost/>
	<ExpenseAccountRef/>
</Item>
";
		return simplexml_load_string($xmlStr);
	}

	public function cleanupValues()
	{
		parent::cleanupValues();

		unset($this->values['UnitPrice']['CurrencyCode']);
	}
}
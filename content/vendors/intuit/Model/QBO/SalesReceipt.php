<?php

class intuit_Model_QBO_SalesReceipt extends intuit_Model_SalesReceipt
{
	protected function getDefaultXml()
	{
		$xmlStr ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<SalesReceipt>
	<Id/>
	<SyncToken/>
	<Header/>
	<Line/>
</SalesReceipt>
";
		return simplexml_load_string($xmlStr);
	}

	public function setTaxInformation($orderRecord, $taxesEnabled, $settings)
	{
		if ($taxesEnabled)
		{
			if ($orderRecord['tax_amount'] > 0)
			{
				$this->header->setValue('SalesTaxCodeId', 1);
			}
		}

		$this->header->setValue('TaxAmt', $orderRecord['tax_amount']);
	}

	protected function getHeader($headerValues = null)
	{
		$header = new intuit_Model_QBO_SalesReceiptHeader();

		if (is_array($headerValues) && count($headerValues) >0)
		{
			$header->setValues($headerValues);
			$header->clearModified();
		}

		return $header;
	}

	protected function isQBO()
	{
		return true;
	}

	protected function getLine($lineValues = null)
	{
		$line = new intuit_Model_QBO_SalesReceiptLine();

		if (is_array($lineValues) && count($lineValues) > 0)
		{
			$line->setValues($lineValues);
			$line->clearModified();
		}

		return $line;
	}
}
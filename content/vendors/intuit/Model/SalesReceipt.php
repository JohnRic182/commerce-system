<?php

abstract class intuit_Model_SalesReceipt extends intuit_Model_BaseModel
{
	protected $header = null;
	protected $lines = array();

	public function __construct($xml = null)
	{
		parent::__construct($xml);

		$this->header = $this->getHeader($this->values['Header']);

		foreach ($this->values['Line'] as $lineValues)
		{
			$this->lines[] = $this->getLine($lineValues);
		}
	}

	protected function collectionElements()
	{
		return array('Line');
	}

	public function setOrderDetails($orderRecord, $customer, $linesRecords, &$existingItemsMap, &$existingItemsById, $qbConnector, &$settings, $nonTaxableTaxCodeName, $taxableTaxCodeName, $taxesEnabled, $currencyCode)
	{
		$lineError = false;

		if ($this->isNew)
		{
			$this->header->setValue('DocNumber', $orderRecord['order_num']);

			$date = $orderRecord['placed_date'];
			if ($date instanceof DateTime)
				$date = $date->format('Y-m-d');
			else
				$date = substr($date, 0, 10);

			$this->header->setValue('TxnDate', $date);

			if (!is_null($customer))
			{
				$this->setCustomerId($customer->Id, $customer->getIdDomain());
			}
			else
			{
				return false;
			}
		}

		foreach ($linesRecords as $lineRecord)
		{
			$intuitItemName = $lineRecord['product_id'];
			if (!is_null($lineRecord['product_sub_id']) && trim($lineRecord['product_sub_id']) != '')
			{
				$intuitItemName = $lineRecord['product_sub_id'];
			}

			$intuitItemKey = strtolower($intuitItemName);

			$item = null;
			if (isset($existingItemsMap[$intuitItemKey]))
			{
				$itemIntuitId = $existingItemsMap[$intuitItemKey];

				$item = $this->getItemById($itemIntuitId, $existingItemsMap, $existingItemsById, $qbConnector, $settings);
			}

			if (is_null($item))
			{
				intuit_QBLogger::error('No intuit item record found for order: '.$orderRecord['order_num'].', product id: '.$intuitItemKey);

				return false;
			}

			$description = $lineRecord['title'];
			if (!is_null($lineRecord['options']) && trim($lineRecord['options']) != '')
			{
				$description .= "\nOptions:\n\n". $lineRecord['options'];
			}
			$line = $this->getLineByItem($item->Name, $item->Id, $lineRecord['price'] == 0, $description);


			$line->Desc = $description;
			$line->Amount = $lineRecord['quantity'] * $lineRecord['price'];
			$line->Qty = $lineRecord['quantity'];
			$line->Taxable = $lineRecord['is_taxable'] == 'Yes' ? 'true' : 'false';
			$line->SalesTaxCodeName = $line->Taxable == 'true' ? $taxableTaxCodeName : $nonTaxableTaxCodeName;

			$line->setItemId($item->Id, $item->getIdDomain());
		}

		if ($orderRecord['discount_amount'] > 0)
		{
			$discountItem = $this->getItem('intuit_discount_product_id', $existingItemsMap, $existingItemsById, $qbConnector, $settings);

			if (!is_null($discountItem))
			{
				$line = $this->getLineByItem($discountItem->Name, $discountItem->Id);

				$line->Desc = 'Discount';
				$line->Amount = 0 - $orderRecord['discount_amount'];
				$line->Qty = 1;
				$line->Taxable = 'true';
				$line->SalesTaxCodeName = $line->Taxable == 'true' ? $taxableTaxCodeName : $nonTaxableTaxCodeName;

				$line->setItemId($discountItem->Id, $discountItem->getIdDomain());
			}
			else
			{
				intuit_QBLogger::error('No intuit discount item record found for order: '.$orderRecord['order_num'].', product id: '.$settings['intuit_discount_product_id']);

				return false;
			}
		}

		if ($orderRecord['promo_discount_amount'] > 0)
		{
			$discountItem = $this->getItem('intuit_promo_code_product_id', $existingItemsMap, $existingItemsById, $qbConnector, $settings);

			if (!is_null($discountItem))
			{
				$line = $this->getLineByItem($discountItem->Name, $discountItem->Id);

				$line->Desc = 'Promo Code';
				$line->Amount = 0 - $orderRecord['promo_discount_amount'];
				$line->Qty = 1;
				$line->Taxable = 'true';
				$line->SalesTaxCodeName = $line->Taxable == 'true' ? $taxableTaxCodeName : $nonTaxableTaxCodeName;

				$line->setItemId($discountItem->Id, $discountItem->getIdDomain());
			}
			else
			{
				intuit_QBLogger::error('No intuit promo discount item record found for order: '.$orderRecord['order_num'].', product id: '.$settings['intuit_promo_code_product_id']);

				return false;
			}
		}

		$shippingAmount = $orderRecord['shipping_amount'];
		$handlingAmount = 0;

		if ($orderRecord['handling_separated'] == 1)
		{
			$handlingAmount = $orderRecord['handling_amount'];
		}
		else
		{
			if ($orderRecord['shipping_taxable'] || $orderRecord['handling_taxable'])
			{
				$handlingAmount = $orderRecord['handling_amount'];

				$shippingAmount -= $handlingAmount;

				if ($shippingAmount < 0)
				{
					$shippingAmount = 0;
				}
			}
		}

		if ($shippingAmount > 0)
		{
			$itemName = null;
			$itemId = null;
			$itemIdDomain = null;
			if ($this->isQBO())
			{
				$itemId = 'SHIPPING_LINE_ID';
				$itemName = 'SHIPPING_LINE_ID';
			}
			else
			{
				$shippingItem = $this->getItem('intuit_shipping_product_id', $existingItemsMap, $existingItemsById, $qbConnector, $settings);

				if (!is_null($shippingItem))
				{
					$itemId = $shippingItem->Id;
					$itemIdDomain = $shippingItem->getIdDomain();
					$itemName = $shippingItem->Name;
				}
				else
				{
					intuit_QBLogger::error('No intuit shipping item record found for order: '.$orderRecord['order_num'].', product id: '.$settings['intuit_shipping_product_id']);

					return false;
				}
			}

			if (!is_null($itemId))
			{
				$line = $this->getLineByItem($itemName, $itemId);

				$line->Desc = 'Shipping';
				$line->Amount = $shippingAmount;
				$line->Qty = 1;
				$line->Taxable = $orderRecord['shipping_taxable'] == 1 ? 'true' : 'false';
				$line->SalesTaxCodeName = $line->Taxable == 'true' ? $taxableTaxCodeName : $nonTaxableTaxCodeName;
				$line->setItemId($itemId, $itemIdDomain);
			}
		}

		if ($handlingAmount > 0)
		{
			$shippingItem = $this->getItem('intuit_handling_product_id', $existingItemsMap, $existingItemsById, $qbConnector, $settings);

			if (!is_null($shippingItem))
			{
				$line = $this->getLineByItem($shippingItem->Name, $shippingItem->Id);

				$line->Desc = 'Handling';
				$line->Amount = $handlingAmount;
				$line->Qty = 1;
				$line->Taxable = $orderRecord['handling_taxable'] == 1 ? 'true' : 'false';
				$line->SalesTaxCodeName = $line->Taxable == 'true' ? $taxableTaxCodeName : $nonTaxableTaxCodeName;
				$line->setItemId($shippingItem->Id, $shippingItem->getIdDomain());
			}
			else
			{
				intuit_QBLogger::error('No intuit handling item record found for order: '.$orderRecord['order_num'].', product id: '.$settings['intuit_handling_product_id']);

				return false;
			}
		}

		$this->header->setValue('TotalAmt', $orderRecord['total_amount']);

		$this->setTaxInformation($orderRecord, $taxesEnabled, $settings);

		$this->header->setValue('SubTotalAmt', $orderRecord['subtotal_amount']);

		return !$lineError;
	}

	public function isModified()
	{
		if ($this->isNew || $this->modified) return true;

		if ($this->header->isModified()) return true;

		foreach ($this->lines as $line)
		{
			if ($line->isModified()) return true;
		}

		return false;
	}

	protected function getItem($settingsKey, &$existingItemsMap, &$existingItemsById, $qbConnector, &$settings)
	{
		if (isset($settings[$settingsKey]) && trim($settings[$settingsKey]) != '')
		{
			if (isset($existingItemsMap[$settings[$settingsKey]]))
			{
				return $this->getItemById($existingItemsMap[$settings[$settingsKey]], $existingItemsMap, $existingItemsById, $qbConnector, $settings);
			}
		}

		return null;
	}

	protected function getItemById($itemId, &$existingItemsMap, &$existingItemsById, $qbConnector, &$settings)
	{
		if (isset($existingItemsById[$itemId]))
		{
			return $existingItemsById[$itemId];
		}
		else
		{
			$item = $qbConnector->getItem($itemId, $existingItemsMap, $existingItemsById, $qbConnector, $settings);

			if ($item != null)
			{
				$existingItemsById[$itemId] = $item;
			}

			return $item;
		}
	}

	public function getDocNumber()
	{
		return $this->header->getValue('DocNumber');
	}

	public function setCustomerId($customerId, $idDomain = null)
	{
		$this->header->setValue('CustomerId', $customerId);

		if (!is_null($idDomain))
		{
			$this->header->setValue('CustomerId_idDomain', $idDomain);
		}
	}

	public function getLineByItem($itemName, $itemId, $isPromotion = false, $description = false)
	{
		foreach ($this->lines as $line)
		{
			if ($line->getItemId() == $itemId || $line->getItemName() == $itemName)
			{
				if ($isPromotion && floatval($line->getAmount()) != 0)
					continue;

				if ($description && $line->getDesc() != $description)
					continue;

				return $line;
			}
		}

		$lineObj = $this->getLine();
		$this->lines[] = $lineObj;

		return $lineObj;
	}

	public function makeCopy()
	{
		$clone = parent::makeCopy();

		$clone->header = $this->header;
		$clone->lines = $this->lines;

		return $clone;
	}

	protected function cleanupValues()
	{
		parent::cleanupValues();

		$this->values['Header'];

		$copy = $this->header->makeCopy();

		$this->values['Header'] = $copy->getValues();

		unset($this->values['Line']);

		$linesValues = array();

		foreach ($this->lines as $line)
		{
			$copy = $line->makeCopy();

			$linesValues[] = $line->getValues();
		}

		$this->values['Line'] = $linesValues;
	}

	public abstract function setTaxInformation($orderRecord, $taxesEnabled, $settings);

	protected abstract function getHeader($headerValues = null);

	protected abstract function isQBO();

	protected abstract function getLine($lineValues = null);
}
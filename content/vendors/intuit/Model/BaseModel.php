<?php

abstract class intuit_Model_BaseModel
{
	protected $isNew = false;
	protected $modified = false;

	protected $values;

	public function __construct($xml = null)
	{
		if (is_null($xml))
		{
			$xml = $this->getDefaultXml();
			$this->modified = true;
			$this->isNew = true;
		}
		else
		{
			$this->isNew = false;
			$this->modified = false;
		}

		$this->values = $this->arrayify($xml);
	}

	protected abstract function getDefaultXml();

	public function getIdDomain()
	{
		return $this->getValue('Id_idDomain');
	}

	protected function collectionElements()
	{
		return array();
	}

	protected function arrayify ($xmlObject, $out = array())
	{
		foreach ((array)$xmlObject as $index => $node)
		{
			if ($index == '@attributes') continue;

			if (is_object($node))
			{
				if (in_array($index, $this->collectionElements()))
				{
					$out[$index] = array($this->arrayify($node));
				}
				else
				{
					$out[$index] = $this->arrayify($node);
				}
			}
			else if (is_array($node))
			{
				$out[$index] = array();
				foreach ((array)$node as $node2)
				{
					$out[$index][] = $this->arrayify($node2);
				}
			}
			else
			{
				$out[$index] = $node;
			}

			if (isset($xmlObject->$index) && count($xmlObject->$index->attributes()) > 0)
			{
				$attributes = $xmlObject->$index->attributes();
				if (isset($attributes['idDomain']))
				{
					$out[$index.'_idDomain'] = (string)$attributes['idDomain'];
				}
			}
		}

		return $out;
	}

	public function __get($name)
	{
		$value = $this->getValue($name);

		if (is_array($value) && count($value) == 0) return null;

		return $value;
	}

	public function __set($name, $value)
	{
		$this->setValue($name, $value);
	}

	public function getValue($key, $parent = null)
	{
		if (!is_null($parent) && isset($this->values[$parent][$key]))
		{
			return $this->values[$parent][$key];
		}
		else if (isset($this->values[$key]))
		{
			return $this->values[$key];
		}

		return null;
	}

	public function setValue($key, $value, $parent = null)
	{
		$priorValue = $this->getValue($key, $parent);

		if (!is_null($parent) && isset($this->values[$parent]))
		{
			$this->values[$parent][$key] = $value;
		}
		else
		{
			$this->values[$key] = $value;
		}

		if (!$this->modified) $this->modified = ($priorValue != $value);
	}

	public function removeValue($key, $parent = null)
	{
		if (!is_null($parent) && isset($this->values[$parent]))
		{
			unset($this->values[$parent][$key]);
		}
		else
		{
			unset($this->values[$key]);
		}
	}

	public function clearModified()
	{
		$this->modified = false;
		$this->isNew = false;
	}

	public function setModified()
	{
		$this->modified = true;
	}

	public function isModified()
	{
		return $this->modified || $this->isNew;
	}

	public function getValues()
	{
		return $this->values;
	}

	public function setValues($values)
	{
		$this->values = $values;
	}

	protected function cleanupValues()
	{
		unset($this->values['MetaData']);
	}

	public function toXml()
	{
		$this->cleanupValues();

		$clone = $this->makeCopy();

		$clone->cleanupValues();

		return $clone->valuesToXml($clone->values);
	}

	public function makeCopy()
	{
		$class_name = get_class($this);
		$clone = new $class_name();

		foreach ($clone->values as $key => $value)
		{
			$clone->setValue($key, $this->getValue($key));
			if (isset($this->values[$key.'_idDomain']))
				$clone->setValue($key.'_idDomain', $this->values[$key.'_idDomain']);
		}

		$clone->modified = $this->modified;

		return $clone;
	}

	protected function valuesToXml($values)
	{
		$xml = '';
		foreach ($values as $name => $value)
		{
			if (is_array($value))
			{
				if (count($value) > 0)
				{
					if ($name === intval($name))
					{
						$elementXml = $this->valuesToXml($value);
					}
					else
					{
						$elementXml = '';

						if (in_array($name, array('Address', 'Line')))
						{
							foreach ($value as $subValues)
							{
								$subElementXml = '<'.$name.'>'.$this->valuesToXml($subValues).'</'.$name.'>';

								if ($subElementXml != '<'.$name.'></'.$name.'>')
								{
									$elementXml .= $subElementXml;
								}
							}
						}
						else
						{
							$elementXml = '<'.$name.'>'.$this->valuesToXml($value).'</'.$name.'>';
						}

						if ($elementXml == '<'.$name.'></'.$name.'>')
						{
							$elementXml = '';
						}
					}

					$xml .= $elementXml;
				}
			}
			else
			{
				if (substr(strrev($name), 0, strlen('niamoDdi_')) == 'niamoDdi_') continue;

				$attributes = '';
				if (isset($values[$name.'_idDomain']))
				{
					$attributes = ' idDomain="'.$values[$name.'_idDomain'].'"';
				}

				$elementXml = '<'.$name.$attributes.'>'.htmlspecialchars($value).'</'.$name.'>';

				if ($elementXml == '<'.$name.'></'.$name.'>')
				{
					$elementXml = '';
				}
				$xml .= $elementXml;
			}
		}

		return $xml;
	}
}
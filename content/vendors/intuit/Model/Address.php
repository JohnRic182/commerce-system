<?php

class intuit_Model_Address
{
	public static function parse($address1, $address2, $city, $state, $zip, $company, $type = 'Billing')
	{
		$ret = array();

		$nextLine = 'Line1';
		$lineNum = 1;

		if (!is_null($company) && trim($company) != '')
		{
			$ret[$nextLine] = $company;
			$lineNum++;
			$nextLine = 'Line'.$lineNum;
		}

		$ret[$nextLine] = $address1;
		$lineNum++;
		$nextLine = 'Line'.$lineNum;

		if (!is_null($address2) && trim($address2) != '')
		{
			$ret[$nextLine] = $address2;
			$lineNum++;
			$nextLine = 'Line'.$lineNum;
		}

		for ( ; $lineNum < 6; $lineNum++)
		{
			$ret['Line'.$lineNum] = '';
		}

		$ret['City'] = $city;

		$ret['CountrySubDivisionCode'] = $state;
		$ret['PostalCode'] = $zip;

		$ret['Tag'] = $type;

		return $ret;
	}
}
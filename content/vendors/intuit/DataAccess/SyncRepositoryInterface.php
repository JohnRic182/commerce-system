<?php

interface intuit_DataAccess_SyncRepositoryInterface
{
	function clearSyncRecords();

	function getItemsToSync();

	function getItemsMap();

	function getCustomersToSync();

	function getOrdersToSync($completed = true);

	function getOrderLines($oid);

	function orderExists($orderNumber);

	function getIntuitIdFor($id, $type);

	function saveIntuitSync($id, $type, $operation, $intuitId, $setDate = true);

	// function getCurrencyCode();

	function updateSetting($name, $value);
}
<?php

class intuit_DataAccess_SyncRepository implements intuit_DataAccess_SyncRepositoryInterface
{
	protected $db;
	protected $settings;

	public function __construct(&$db = null, &$settings = null)
	{
		if (is_null($db))
		{
			global $db;
		}
		$this->db = $db;

		if (is_null($settings))
		{
			global $settings;
		}
		$this->settings = $settings;
	}

	public function clearSyncRecords()
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."intuit_sync");
	}

	public function updateSetting($name, $value)
	{
		$this->db->query("UPDATE ".DB_PREFIX."settings SET value = '".$this->db->escape($value)."' WHERE name = '".$this->db->escape($name)."'");
	}

	public function getItemsMap()
	{
		$map = array();
		$sql = "SELECT id, intuit_id FROM ".DB_PREFIX."intuit_sync WHERE type='item'";
		$this->db->query($sql);
		while ($row = $this->db->moveNext())
		{
			$map[strtolower($row['id'])] = $row['intuit_id'];
		}

		return $map;
	}

	public function getItemsToSync()
	{
		require_once dirname(dirname(dirname(dirname(__FILE__)))).'/engine/engine_localization.php';

		$sql =	"SELECT p.pid, p.product_id, null as product_subid, p.title, p.price, p.inventory_control, 
	case p.inventory_control when 'Yes' THEN p.stock ELSE 0 END as stock,
	p.is_taxable, '' as attributes_list, 
	case p.is_visible when 'Yes' then 1 else 0 end as is_active
FROM ".DB_PREFIX."products p
UNION
SELECT p.pid, p.product_id, i.product_subid, p.title, p.price, p.inventory_control, i.stock, p.is_taxable, i.attributes_list, i.is_active
FROM ".DB_PREFIX."products p
	INNER JOIN ".DB_PREFIX."products_inventory i ON p.pid = i.pid";

		$this->db->query($sql);

		$products = array();

		while ($product = $this->db->moveNext())
		{
			if (!is_null($product['attributes_list']) && trim($product['attributes_list']) != '')
			{
				$product['title'] = $product['title'] . ' - ' . str_replace("\n", ", ", $product['attributes_list']);
			}
			$product_id = $product['product_id'];
			if (trim($product['product_subid']))
				$product_id = $product['product_subid'];

			$products[$product_id] = $product;
		}

		if (isset($this->settings['intuit_shipping_product_id']) && trim($this->settings['intuit_shipping_product_id']) != '' && trim($this->settings['intuit_shipping_product_id']) != 'SHIPPING_LINE_ID')
		{
			$products[$this->settings['intuit_shipping_product_id']] = array(
					'pid' => 0,
					'product_id' => $this->settings['intuit_shipping_product_id'],
					'product_subid' => null,
					'title' => 'Shipping',
					'price' => 0,
					'inventory_control' => 'No',
					'stock' => 0,
					'is_taxable' => $this->settings['ShippingTaxable'] == 1 ? 'Yes' : 'No',
					'is_active' => 'Yes',
				);
		}

		if (isset($this->settings['intuit_handling_product_id']) && trim($this->settings['intuit_handling_product_id']) != '')
		{
			$products[$this->settings['intuit_handling_product_id']] = array(
					'pid' => 0,
					'product_id' => $this->settings['intuit_handling_product_id'],
					'product_subid' => null,
					'title' => 'Handling',
					'price' => 0,
					'inventory_control' => 'No',
					'stock' => 0,
					'is_taxable' => $this->settings['ShippingHandlingTaxable'] == 1 ? 'Yes' : 'No',
					'is_active' => 'Yes',
				);
		}

		if (isset($this->settings['intuit_discount_product_id']) && trim($this->settings['intuit_discount_product_id']) != '')
		{
			$products[$this->settings['intuit_discount_product_id']] = array(
					'pid' => 0,
					'product_id' => $this->settings['intuit_discount_product_id'],
					'product_subid' => null,
					'title' => 'Discount',
					'price' => 0,
					'inventory_control' => 'No',
					'stock' => 0,
					'is_taxable' => 'Yes',
					'is_active' => 'Yes',
				);
		}

		if (isset($this->settings['intuit_promo_code_product_id']) && trim($this->settings['intuit_promo_code_product_id']) != '')
		{
			$products[$this->settings['intuit_promo_code_product_id']] = array(
					'pid' => 0,
					'product_id' => $this->settings['intuit_promo_code_product_id'],
					'product_subid' => null,
					'title' => 'Promo Code',
					'price' => 0,
					'inventory_control' => 'No',
					'stock' => 0,
					'is_taxable' => 'Yes',
					'is_active' => 'Yes',
				);
		}

		$this->db->query("SELECT *
FROM ".DB_PREFIX."intuit_sync
WHERE type = 'item'");
		while ($sync = $this->db->moveNext())
		{
			if (isset($products[$sync['id']]))
			{
				$products[$sync['id']]['intuit_id'] = $sync['intuit_id'];
			}
		}

		$this->db->query("SELECT *
FROM ".DB_PREFIX."products_attributes
WHERE is_active = 'Yes' AND is_modifier = 'Yes'");

		$attributesByProduct = array();
		while ($attribute = $this->db->moveNext())
		{
			if (!isset($attributesByProduct[$attribute['pid']]))
				$attributesByProduct[$attribute['pid']] = array();

			$options = array();
			ProductAttribute::parseOptions2($attribute['options'], 0, 0, $options, true);
			$attributesByProduct[$attribute['pid']][$attribute['name']] = $options;
		}

		$temp = array();
		foreach ($products as $product)
		{
			if (isset($attributesByProduct[$product['pid']]))
			{
				$productAttributeValues = $this->parseProductAttributes($product['attributes_list']);
				if (count($productAttributeValues) > 0)
				{
					$priceDiff = 0;
					foreach ($productAttributeValues as $key => $value)
					{
						if (isset($attributesByProduct[$product['pid']][$key]) && isset($attributesByProduct[$product['pid']][$key][$value]))
						{
							$priceDiff += $attributesByProduct[$product['pid']][$key][$value]['price_difference'];
						}
					}
					$product['price'] = floatval($product['price'] + $priceDiff);
				}
			}
			$temp[] = $product;
		}

		return $temp;
	}

	protected function parseProductAttributes($attributesList)
	{
		$ret = array();
		$attributes = explode("\n", $attributesList);
		foreach ($attributes as $attrLine)
		{
			$attrLineValues = explode(':', $attrLine);
			if (count($attrLineValues) > 1)
			{
				$key = $attrLineValues[0];
				$value = trim($attrLineValues[1]);
				$ret[$key] = $value;
			}
		}
		return $ret;
	}

	public function getCustomersToSync()
	{
		$sql =	"SELECT ".DB_PREFIX."users.*, ";
		$sql .= "IF(".DB_PREFIX."users.country=1 OR ".DB_PREFIX."users.country=2, bstate.short_name, ".DB_PREFIX."users.province) AS billing_state_name, 
			bcountry.iso_a2 AS billing_country_name, ";
		$sql .= "shipping.address1 as shipping_address1, shipping.address2 as shipping_address2, shipping.name as shipping_name, shipping.company as shipping_company, ";
		$sql .= "shipping.city as shipping_city, shipping.province as shipping_province, shipping.zip as shipping_zip, ";
		$sql .= "IF(shipping.country=1 OR shipping.country=2, sstate.short_name, shipping.province) AS shipping_state_name, 
			bcountry.iso_a2 AS billing_country_name ";
		$sql .= " , sync.intuit_id, sync.last_sync_date ";
		$sql .= "FROM ".DB_PREFIX."users ";
		$sql .= "LEFT JOIN ".DB_PREFIX."states AS bstate ON bstate.stid=".DB_PREFIX."users.state ";
		$sql .= "LEFT JOIN ".DB_PREFIX."countries AS bcountry ON bcountry.coid=".DB_PREFIX."users.country ";
		$sql .= "LEFT JOIN ".DB_PREFIX."intuit_sync AS sync ON sync.type = 'user' AND sync.id = ".DB_PREFIX."users.uid ";
		$sql .= "LEFT JOIN ".DB_PREFIX."users_shipping AS shipping ON shipping.uid=".DB_PREFIX."users.uid AND shipping.is_primary = 'Yes'";
		$sql .= "LEFT JOIN ".DB_PREFIX."states AS sstate ON sstate.stid=shipping.state ";
		$sql .= "LEFT JOIN ".DB_PREFIX."countries AS scountry ON scountry.coid=shipping.country ";
		$sql .= "WHERE ".DB_PREFIX."users.uid>0 and ".DB_PREFIX."users.fname <> '' and ".DB_PREFIX."users.lname <> '' AND (sync.last_sync_date IS NULL OR sync.last_sync_date < ".DB_PREFIX."users.last_update)";

		$this->db->query($sql);

		return $this->db->getRecords();
	}

	public function getOrdersToSync($completed = true)
	{
		$sql =	"SELECT ".DB_PREFIX."orders.*, ".DB_PREFIX."users.*, ";
		$sql .= "IF(".DB_PREFIX."users.country=1 OR ".DB_PREFIX."users.country=2, bstate.short_name, ".DB_PREFIX."users.province) AS billing_state_name, bcountry.iso_a2 AS billing_country_name, ";
		$sql .= "IF(".DB_PREFIX."orders.shipping_country=1 OR ".DB_PREFIX."orders.shipping_country=2, sstate.short_name, ".DB_PREFIX."orders.shipping_province) AS shipping_state_name, scountry.iso_a2 AS shipping_country_name ";
		$sql .= " , sync.intuit_id, sync.last_sync_date, intuit_user.intuit_id as intuit_user_id ";
		$sql .= "FROM ".DB_PREFIX."orders ";
		$sql .= "INNER JOIN ".DB_PREFIX."users ON ".DB_PREFIX."orders.uid=".DB_PREFIX."users.uid ";
		$sql .= "LEFT JOIN ".DB_PREFIX."states AS bstate ON bstate.stid=".DB_PREFIX."users.state ";
		$sql .= "INNER JOIN ".DB_PREFIX."countries AS bcountry ON bcountry.coid=".DB_PREFIX."users.country ";
		$sql .= "LEFT JOIN ".DB_PREFIX."states AS sstate ON sstate.stid=".DB_PREFIX."orders.shipping_state ";
		$sql .= "LEFT JOIN ".DB_PREFIX."intuit_sync AS sync ON sync.type = 'order' AND sync.id = ".DB_PREFIX."orders.oid ";
		$sql .= "LEFT JOIN ".DB_PREFIX."countries AS scountry ON scountry.coid=".DB_PREFIX."orders.shipping_country ";
		$sql .= "INNER JOIN ".DB_PREFIX."intuit_sync AS intuit_user ON intuit_user.type = 'user' AND intuit_user.id = ".DB_PREFIX."users.uid ";

		if ($completed)
			$sql .= " WHERE status = 'Completed' AND payment_status = 'Received' AND placed_date <> '0000-00-00 00:00:00' AND (sync.last_sync_date is null OR sync.last_sync_date < ".DB_PREFIX."orders.status_date)";

		$this->db->query($sql);
		return $this->db->getRecords();
	}

	public function getOrderLines($oid)
	{
		$sql = "SELECT * FROM ".DB_PREFIX."orders_content WHERE oid='". $oid ."'";
		$this->db->query($sql);

		return $this->db->getRecords();
	}

	public function orderExists($orderNumber)
	{
		$sql = "SELECT * FROM ".DB_PREFIX."orders WHERE order_num='".$orderNumber."'";

		$this->db->query($sql);

		return $this->db->moveNext() ? true : false;
	}

	public function getIntuitIdFor($id, $type)
	{
		$this->db->query("SELECT intuit_id FROM ".DB_PREFIX."intuit_sync WHERE id = '".$this->db->escape($id)."' AND type = '".$this->db->escape($type)."'");
		if ($row = $this->db->moveNext())
			return $row['intuit_id'];

		return null;
	}

	public function saveIntuitSync($id, $type, $operation, $intuitId, $setDate = true)
	{
		$result = $this->db->selectOne('SELECT * FROM '.DB_PREFIX."intuit_sync WHERE type = '".$type."' AND id = '".$this->db->escape($id)."'");

		$operation = ($result === false) ? 'Add' : 'Mod';

		if ($operation == 'Add')
		{
			$this->db->reset();
			$this->db->assignStr('id', $id);
			$this->db->assignStr('type', $type);
			$this->db->assignStr('intuit_id', $intuitId);
			if ($setDate)
			{
				$this->db->assign('last_sync_date', 'NOW()');
			}
			$this->db->insert(DB_PREFIX."intuit_sync");
		}
		else
		{
			if ($setDate)
			{
				$this->db->reset();
				$this->db->assign('last_sync_date', 'NOW()');
				$this->db->assignStr('intuit_id', $intuitId);
				$this->db->update(DB_PREFIX."intuit_sync", " WHERE type='".$type."' AND id = '".$this->db->escape($id)."'");
			}
		}
	}
}
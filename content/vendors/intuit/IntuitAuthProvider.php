<?php

/**
 * Class intuit_IntuitAuthProvider
 */
abstract class intuit_IntuitAuthProvider
{
	/**
	 * @param $url
	 * @param array $fields
	 * @return mixed
	 */
	public abstract function getAuthorizationHeader($url, $fields = array());
}
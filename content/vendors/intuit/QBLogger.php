<?php

/**
 * Class intuit_QBLogger
 */
class intuit_QBLogger
{
	protected static $fileHandle;

	public static $lastMessage = '';

	public static function debug($message)
	{
		self::log($message, 'DEBUG');
	}

	public static function info($message)
	{
		self::log($message, 'INFO');
	}

	public static function error($message)
	{
		self::log($message, 'ERROR');
	}

	public static function log($message, $level = 'INFO')
	{
		$levels = array('ERROR');
		if (defined('DEVMODE') && DEVMODE)
		{
			$levels[] = 'INFO';
		}
		if (defined('IDS_DEBUG') && IDS_DEBUG)
		{
			$levels[] = 'DEBUG';
			$levels[] = 'INFO';	
		}

		if (!in_array($level, $levels))
			return;

		$strToWrite = $message;
		if (is_array($message) || is_object($message))
		{
			$strToWrite = print_r($message,true);
		}

		if ($level == 'ERROR')
			error_log($strToWrite);

		@file_put_contents(
			"content/cache/log/intuit_ids.txt", 
			date("Y/m/d-H:i:s")." - ".$strToWrite."\n", 
			FILE_APPEND
		);

		self::$lastMessage = $strToWrite;
	}
}
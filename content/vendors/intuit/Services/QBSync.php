<?php

/**
 * Class intuit_Services_QBSync
 */
class intuit_Services_QBSync
{
	protected $repository;
	protected $qbConnector;
	protected $settings;
	protected $accounts = null;
	protected $preferences = null;
	protected $dataSource = null;
	protected $currencyCode = null;
	protected $existingItemsById;

	/**
	 * intuit_Services_QBSync constructor.
	 * @param intuit_DataAccess_SyncRepositoryInterface $repository
	 * @param intuit_Services_QBConnectorInterface $qbConnector
	 * @param $dataSource
	 * @param null $settings
	 */
	public function __construct(intuit_DataAccess_SyncRepositoryInterface $repository, intuit_Services_QBConnectorInterface $qbConnector, $dataSource, &$settings = null)
	{
		$this->repository = $repository;
		$this->qbConnector = $qbConnector;

		$this->dataSource = $dataSource;

		if (is_null($settings))
			global $settings;

		$this->settings = $settings;
	}

	/**
	 * @return bool
	 */
	public function isQBO()
	{
		return $this->dataSource == 'QBO';
	}

	/**
	 * @param bool $force_sync
	 */
	public function doSync($force_sync = false)
	{
		$shouldSync = $force_sync;

		if (!$shouldSync && (defined('DEVMODE') && DEVMODE && isset($_REQUEST['sync']) && $_REQUEST['sync'] == '1'))
		{
			$shouldSync = true;
		}

		if (!$shouldSync && (!isset($_SESSION['QB_SYNCD']) || $_SESSION['QB_SYNCD'] != true))
		{
			$shouldSync = true;
		}

		if (isset($_REQUEST['chart']))
		{
			$shouldSync = false;
		}

		if ($shouldSync)
		{
			if ($this->settings['intuit_income_account'] != '')
			{
				intuit_QBLogger::log('Beginning IDS sync...');

				$this->init();

				$this->syncItems();
				$this->syncCustomers();
				$this->syncSalesReceipts();

	 			$_SESSION['QB_SYNCD'] = true;

				intuit_QBLogger::log('Finished IDS sync.');
			}
		}
	}

	/**
	 * Init
	 */
	public function init()
	{
		$this->initAdminCurrency();

		$this->preferences = $this->qbConnector->getPreferences();
	}

	/**
	 * Disconnect
	 */
	public function disconnect()
	{
		$this->qbConnector->disconnect();
	}

	/**
	 * @param $currencyCode
	 */
	public function setCurrencyCode($currencyCode)
	{
		$this->currencyCode = $currencyCode;
	}

	/**
	 * @return null
	 */
	public function getCurrencyCode()
	{
		return $this->currencyCode;
	}

	/**
	 * 
	 */
	protected function initAdminCurrency()
	{
		if (is_null($this->currencyCode))
		{
			global $db;
			$currencies = new Currencies($db);
			if (!isset($_SESSION["admin_currency"]))
			{
				$_SESSION["admin_currency"] = $admin_currency = $currencies->getDefaultCurrency();
			}
			else
			{
				$admin_currency = $_SESSION["admin_currency"];
			}

			if (isset($admin_currency) && isset($admin_currency['code']))
			{
				$this->currencyCode = $admin_currency['code'];
			}
		}
	}

	public function syncItems()
	{
		intuit_QBLogger::log('Starting items sync');

		$productRecords = $this->repository->getItemsToSync();

		$incomeAccount = $this->getDefaultIncomeAccount();

		$expenseAccount = $this->getDefaultExpenseAccount();

		$expenseAccounts = $this->getExpenseAccounts();

		if (is_null($incomeAccount))
		{
			intuit_QBLogger::error('No income account defined');
			return;
		}

		$this->existingItemsById = $this->qbConnector->getExistingItemsById();

		$failedItems = $this->qbConnector->getFailedItems();

		$productsByIntuitName = array();
		foreach ($productRecords as $productRecord)
		{
			$intuitName = $this->getIntuitNameForItem($productRecord);

			$intuitId = isset($productRecord['intuit_id']) ? $productRecord['intuit_id'] : null;

			$item = $this->getItem2($intuitId, $intuitName, $failedItems);

			$operation = 'Mod';

			if (is_null($item))
			{
				$operation = 'Add';
				$item = $this->qbConnector->createItem($productRecord);
			}

			$item->setItemDetails($intuitName, $productRecord, $incomeAccount, $expenseAccount, $this->getCurrencyCode());

			$intuitId = $item->Id;

			if ($item->isModified())
			{
				try
				{
					$intuitIdFromPersist = $this->persistItem($item, $operation, $expenseAccounts);
				}
				catch (intuit_DuplicateItemException $e)
				{
					$item2 = $this->getItemByName($intuitName);

					if (is_null($item2))
					{
						$operation = 'Add';
						$item2 = $this->qbConnector->createItem($productRecord);
					}
					else
					{
						$operation = 'Mod';
					}

					$item2->setItemDetails($intuitName, $productRecord, $incomeAccount, $expenseAccount, $this->getCurrencyCode());

					try
					{
						$intuitIdFromPersist = $this->persistItem($item2, $operation, $expenseAccounts);
					}
					catch (intuit_DuplicateItemException $e)
					{
						if ($operation == 'Add')
						{
							intuit_QBLogger::error($e->getMessage());
							continue;
						}
						else
						{
							//Add and mod both failed, assume this is our item
						}
					}

					$intuitIdFromPersist = $item2->Id;
				}

				if (is_null($intuitId))
					$intuitId = $intuitIdFromPersist;

				if (is_object($intuitId))
				{
					$intuitId = (string)$intuitId;
				}

				$item->Id = $intuitId;

				if ($operation == 'Add')
					$this->existingItemsById[$intuitId] = $item;
			}

			intuit_QBLogger::debug('Item: product_id: '.$item->Name.' -> '.$intuitId);
			if (!is_null($intuitId))
			{
				$this->repository->saveIntuitSync($item->Name, 'item', $operation, $intuitId);
			}
		}

		intuit_QBLogger::log('Finished items sync');
	}

	protected function persistItem(&$item, $operation, &$expenseAccounts)
	{
		return $this->qbConnector->persistItem($item, $operation);
	}

	protected function getIntuitNameForItem($productRecord)
	{
		if (!is_null($productRecord['product_subid']) && trim($productRecord['product_subid']) != '')
		{
			return str_replace(':', '_', $productRecord['product_subid']);
		}

		return str_replace(':', '_', $productRecord['product_id']);
	}

	protected function getItem2($intuitId, $intuitName, $failedItems)
	{
		if (isset($failedItems[strtolower($intuitName)]))
		{
			return $failedItems[strtolower($intuitName)];
		}

		if (!is_null($intuitId) && trim($intuitId) != '')
		{
			if (isset($this->existingItemsById[$intuitId]))
			{
				return $this->existingItemsById[$intuitId];
			}
			else
			{
				$item = $this->qbConnector->getItem($intuitId);

				if (!is_null($item)) $item->setModified();

				return $item;
			}
		}

		return null;
	}

	protected function getItemByName($intuitName)
	{
		foreach ($this->existingItemsById as $id => $existingItem)
		{
			if (strcasecmp($existingItem->Name, $intuitName) == 0)
			{
				return $existingItem;
			}
		}

		return null;
	}

	public function syncCustomers()
	{
		intuit_QBLogger::log('Starting customers sync');

		$userRecords = $this->repository->getCustomersToSync();

		foreach ($userRecords as $userRecord)
		{
			$intuitId = $userRecord['intuit_id'];
			$intuitName = $this->getIntuitNameForCustomer($userRecord);

			$customer = null;

			if (!is_null($intuitId) && trim($intuitId) != '')
			{
				$customer = $this->qbConnector->getCustomer($intuitId);

				if (is_null($customer))
				{
					intuit_QBLogger::error('Cannot sync: '.$intuitName.' customer not found in IDS ('.$intuitId.')');
					continue;
				}
			}

			$operation = 'Mod';
			if (is_null($customer))
			{
				$customer = $this->qbConnector->createCustomer($userRecord);
				$operation = 'Add';
			}

			$customer->setCustomerDetails($userRecord, $intuitName);

			try
			{
				$intuitIdFromPersist = $this->qbConnector->persistCustomer($customer, $operation);
			}
			catch (intuit_DuplicateItemException $e)
			{
				intuit_QBLogger::error($e->getMessage());

				$customer2 = $this->qbConnector->getCustomerByName($customer->Name);

				if (is_null($customer2))
				{
					$customer2 = $this->qbConnector->createCustomer($userRecord);
					$intuitIdFromPersist = null;
					$operation = 'Add';
				}
				else
				{
					$intuitIdFromPersist = $customer2->Id;
					$operation = 'Mod';
				}

				$customer2->setCustomerDetails($userRecord, $intuitName);

				try
				{
					$this->qbConnector->persistCustomer($customer2, $operation);
				}
				catch (intuit_DuplicateItemException $e)
				{
					intuit_QBLogger::error($e->getMessage());
					continue;
				}

				$intuitIdFromPersist = $customer2->Id;
			}

			if (is_null($intuitId))
				$intuitId = $intuitIdFromPersist;

			intuit_QBLogger::debug('Customer: uid: '.$userRecord['uid'].' -> '.$intuitId);
			if (!is_null($intuitId))
			{
				$this->repository->saveIntuitSync($userRecord['uid'], 'user', $operation, $intuitId);
			}
		}

		intuit_QBLogger::log('Finished customers sync');
	}

	protected function getIntuitNameForCustomer($userRecord)
	{
		return trim($userRecord['fname']) . ' ' . trim($userRecord['lname']) . ' [' . $userRecord['uid'] . ']';
	}

	public function syncSalesReceipts()
	{
		intuit_QBLogger::log('Starting sales receipts sync');

		$orderRecords = $this->repository->getOrdersToSync();

		$existingItemsMap = $this->repository->getItemsMap();

		if (is_null($this->preferences)) $this->preferences = $this->qbConnector->getPreferences();

		$taxesEnabled = $this->preferences->taxesEnabled();

		$nonTaxableTaxCodeName = $this->settings['intuit_nontaxable_tax_code']; //Non';
		$taxableTaxCodeName = $this->settings['intuit_taxable_tax_code']; //'Tax';

		$failedSalesReceipts = $this->qbConnector->getFailedSalesReceipts();

		foreach ($orderRecords as $orderRecord)
		{
			$intuitId = $orderRecord['intuit_id'];
			$intuitName = $orderRecord['order_num'];

			$order = null;

			if (!is_null($intuitId) && trim($intuitId) != '')
			{
				$order = $this->qbConnector->getSalesReceipt($intuitId);

				if (is_null($order) && isset($failedSalesReceipts[$intuitName]))
				{
					$order = $failedSalesReceipts[$intuitName];
					$intuitId = $order->Id;
					unset($failedSalesReceipts[$intuitName]);
				}

				if (is_null($order))
				{
					intuit_QBLogger::error('Cannot sync: sales receipt for order: '.$intuitName.' not found in IDS ('.$intuitId.')');
					continue;
				}
			}

			$operation = is_null($order) || is_null($intuitId) ? 'Add' : 'Mod';

			$linesRecords = $this->repository->getOrderLines($orderRecord['oid']);

			$customer = null;
			if (is_null($order))
			{
				$order = $this->qbConnector->createSalesReceipt($orderRecord);

				$customer = $this->qbConnector->getCustomer($orderRecord['intuit_user_id']);
				if (is_null($customer))
				{
					intuit_QBLogger::error('No intuit customer record found for order: '.$orderRecord['order_num']);
					continue;
				}
			}

			if (!$order->setOrderDetails($orderRecord, $customer, $linesRecords, $existingItemsMap, $this->existingItemsById, $this->qbConnector,
											$this->settings, $nonTaxableTaxCodeName, $taxableTaxCodeName, $taxesEnabled, $this->getCurrencyCode()))
			{
				intuit_QBLogger::error('Sales Receipt: oid: '.$orderRecord['oid'].' not sync\'d, setOrderDetails returned false');
				continue;
			}

			$intuitIdFromPersist = $this->qbConnector->persistSalesReceipt($order, $operation);
			if (is_null($intuitId) || trim($intuitId) == '')
				$intuitId = $intuitIdFromPersist;

			intuit_QBLogger::debug('Sales Receipt: oid: '.$orderRecord['oid'].' -> '.$intuitId);
			if (!is_null($intuitId))
			{
				$this->repository->saveIntuitSync($orderRecord['oid'], 'order', $operation, $intuitId);
			}
		}

		foreach ($failedSalesReceipts as $orderNumber => $order)
		{
			if ($this->repository->orderExists($orderNumber))
			{
				$intuitId = $this->qbConnector->persistSalesReceipt($order, 'Mod');

				intuit_QBLogger::debug('failed Sales Receipt: oid: '.$order->getDocNumber().' -> '.$intuitId);
				if (!is_null($intuitId))
				{
					$this->repository->saveIntuitSync($order->getDocNumber(), 'order', 'Mod', $intuitId);
				}
			}
		}
		intuit_QBLogger::log('Finished sales receipts sync');
	}

	protected $salesTaxRates;

	public function getSalesTaxRates()
	{
		if (is_null($this->salesTaxRates))
			$this->salesTaxRates = $this->qbConnector->getSalesTaxes();

		return $this->salesTaxRates;
	}

	protected $taxCodes;

	public function getSalesTaxCodes()
	{
		if (is_null($this->taxCodes))
			$this->taxCodes = $this->qbConnector->getSalesTaxCodes();

		$active = array();
		foreach ($this->taxCodes as $taxCode)
		{
			if ($taxCode->Active)
				$active[] = $taxCode;
		}

		return $active;
	}

	public function getTaxableSalesTaxCodes()
	{
		$taxCodes = $this->getSalesTaxCodes();

		$ret = array();
		foreach ($taxCodes as $taxCode)
		{
			if ($taxCode->Taxable == 'true')
				$ret[] = $taxCode;
		}

		return $ret;
	}

	public function getNonTaxableSalesTaxCodes()
	{
		$taxCodes = $this->getSalesTaxCodes();

		$ret = array();
		foreach ($taxCodes as $taxCode)
		{
			if ($taxCode->Taxable != 'true')
				$ret[] = $taxCode;
		}

		return $ret;
	}

	/**
	 * @return array
	 */
	public function getExpenseAccounts()
	{
		if (is_null($this->accounts))
			$this->accounts = $this->qbConnector->getAccounts();

		$expenseAccounts = array();
		foreach ($this->accounts as $account)
		{
			if ($account->isExpenseAccount())
				$expenseAccounts[$account->Id] = $account;
		}

		return $expenseAccounts;
	}

	/**
	 * @return array
	 */
	public function getIncomeAccounts()
	{
		if (is_null($this->accounts))
			$this->accounts = $this->qbConnector->getAccounts();

		$incomeAccounts = array();
		foreach ($this->accounts as $account)
		{
			if ($account->isIncomeAccount())
				$incomeAccounts[] = $account;
		}

		return $incomeAccounts;
	}

	public function getDefaultIncomeAccount()
	{
		if (!is_null($this->settings['intuit_income_account']) && trim($this->settings['intuit_income_account']) != '')
		{
			$incomeAccounts = $this->getIncomeAccounts();

			foreach ($incomeAccounts as $ia)
			{
				if ($ia->Id == $this->settings['intuit_income_account'])
				{
					return $ia;
				}
			}
		}

		return null;
	}

	public function getDefaultExpenseAccount()
	{
		if (!is_null($this->settings['intuit_expense_account']) && trim($this->settings['intuit_expense_account']) != '')
		{
			$expenseAccounts = $this->getExpenseAccounts();

			foreach ($expenseAccounts as $ia)
			{
				if ($ia->Id == $this->settings['intuit_expense_account'])
				{
					return $ia;
				}
			}
		}

		return null;
	}

	public function ensureSalesTaxRateExists($name)
	{
		$rates = $this->getSalesTaxRates();

		foreach ($rates as $rate)
		{
			if ($rate->Name == $name)
				return $rate;
		}

		$rate = $this->qbConnector->createSalesTax(null);
		if ($rate != null)
		{
			$rate->Name = $name;
			$rate->TaxRate = 0;

			$this->qbConnector->persistSalesTax($rate, 'Add');
		}

		return $rate;
	}

	public function ensureIncomeAccountExists($name)
	{
		$accounts = $this->getIncomeAccounts();

		foreach ($accounts as $a)
		{
			if ($a->Name == $name)
				return $a->Id;
		}

		$a = $this->qbConnector->createAccount('income');
		if ($a != null)
		{
			$a->Name = $name;
			return $this->qbConnector->persistAccount($a, 'Add');
		}

		return '';
	}

	public function ensureExpenseAccountExists($name)
	{
		$accounts = $this->getExpenseAccounts();

		foreach ($accounts as $a)
		{
			if ($a->Name == $name)
				return $a->Id;
		}

		$a = $this->qbConnector->createAccount('expense');
		if ($a != null)
		{
			$a->Name = $name;
			return $this->qbConnector->persistAccount($a, 'Add');
		}

		return '';
	}

	public static function create()
	{
		global $db, $oauth_token, $oauth_token_secret;

		require_once dirname(__FILE__).'/../oauth/config.php';
		
		$db->query("
			SELECT *
			FROM ".DB_PREFIX."admin_oauth
			WHERE uid = 0"
		);

		$oauth_token = '';
		$oauth_token_secret = '';

		$intuitSync = null;

		if ($auth = $db->moveNext())
		{
			$oauth_token = $auth['oauth_token'];
			$oauth_token_secret = $auth['oauth_token_secret'];

			$intuitAuth = new intuit_IntuitOAuthProvider($oauth_token, $oauth_token_secret);

			$realmID = $auth['realm_id'];
			$intuitDataSource = $auth['data_source'];

			$intuitSync = intuit_Services_QBSync::get($intuitAuth, $realmID, $intuitDataSource);
		}

		return $intuitSync;
	}

	public static function get(intuit_IntuitAuthProvider $authProvider, $realmId, $dataSource = 'QBO')
	{
		$modelMapper = new intuit_Model_Mapping_ModelMapper($dataSource);
		$transport = null;
		$connector = null;
		if ($dataSource == 'QBD')
		{
			$transport = new intuit_QBDTransport($authProvider, $realmId);
			$connector = new intuit_Services_QBDConnector($transport, $modelMapper);
		}
		else
		{
			$transport = new intuit_QBOTransport($authProvider, $realmId);
			$connector = new intuit_Services_QBOConnector($transport, $modelMapper);
		}

		return new intuit_Services_QBSync(new intuit_DataAccess_SyncRepository(), $connector, $dataSource);
	}
}
<?php

class intuit_Services_QBDConnector extends intuit_Services_QBConnector
{
	protected function parsePreferences($preferencesXml)
	{
		if (isset($preferencesXml->CompanyPreferences) && isset($preferencesXml->CompanyPreferences->Preferences))
		{
			intuit_QBLogger::debug($preferencesXml->CompanyPreferences->Preferences);
			return $this->modelMapper->loadType($preferencesXml->CompanyPreferences->Preferences, 'intuit_Model_QBD_Preferences');
		}
		return null;
	}

	public function createAccount($type)
	{
		$a = new intuit_Model_QBD_Account();

		if ($type == 'income')
		{
			$a->Type = 'Revenue';
			$a->Subtype = 'Income';
		}
		else if ($type == 'expense')
		{
			$a->Type = 'Expense';
			$a->Subtype = 'Expense';
		}

		return $a;
	}

	public function createItem($productRecord)
	{
		$item = new intuit_Model_QBD_Item();

		$item->Type = 'Product';

		return $item;		
	}

	public function getSalesTaxCodes()
	{
		$results = $this->transport->QBQuery('SalesTaxCode');

		$ret = array();
		foreach ($results as $resultXml)
		{
			$result = $this->modelMapper->loadType($resultXml, 'intuit_Model_QBD_SalesTaxCode');
			$ret[$result->Name] = $result;
		}

		return $ret;
	}

	public function getSalesTaxes()
	{
		$results = $this->transport->QBQuery('SalesTax');

		$ret = array();
		foreach ($results as $resultXml)
		{
			$result = $this->modelMapper->loadType($resultXml, 'intuit_Model_QBD_SalesTax');
			$ret[$result->Name] = $result;
		}

		return $ret;
	}

	public function createCustomer($userRecord)
	{
		return new intuit_Model_QBD_Customer();
	}

	public function createSalesReceipt($orderRecord)
	{
		return new intuit_Model_QBD_SalesReceipt();
	}

	public function createSalesTax($record)
	{
		return new intuit_Model_QBD_SalesTax();
	}

	public function createSalesReceiptLine()
	{
		return new intuit_Model_QBD_SalesReceiptLine();
	}

	protected function getObjectRefId($qbResponse)
	{
		$intuitId = null;

		if (isset($qbResponse->Success) && isset($qbResponse->Success->ObjectRef) && isset($qbResponse->Success->ObjectRef->Id))
		{
			$intuitId = $qbResponse->Success->ObjectRef->Id;
		}
		else if (isset($qbResponse->Success) && isset($qbResponse->Success->PartyRoleRef) && isset($qbResponse->Success->PartyRoleRef->Id))
		{
			$intuitId = $qbResponse->Success->PartyRoleRef->Id;
		}

		return $intuitId;;
	}
}
<?php

/**
 * Class intuit_Services_QBConnector
 */
abstract class intuit_Services_QBConnector implements intuit_Services_QBConnectorInterface
{
	/** @var intuit_QBTransport $transport */
	protected $transport;

	protected $modelMapper;

	protected $existingCustomers = null;

	/**
	 * intuit_Services_QBConnector constructor.
	 *
	 * @param intuit_QBTransport $transport
	 * @param $modelMapper
	 */
	public function __construct(intuit_QBTransport $transport, $modelMapper)
	{
		$this->transport = $transport;

		$this->modelMapper = $modelMapper;
	}

	/**
	 * @return mixed
	 */
	public function getPreferences()
	{
		$preferencesXml = $this->transport->QBQuery('Preferences');

		return $this->parsePreferences($preferencesXml);
	}

	public function getSalesTaxCodes()
	{
		return array();
	}

	public function getSalesTaxes()
	{
		return array();
	}

	protected abstract function parsePreferences($preferencesXml);

	public function getAccounts()
	{
		$accounts = $this->transport->QBQuery('Account');

		$existingAccounts = array();
		foreach ($accounts as $accountXml)
		{
			$account = $this->modelMapper->loadAccount($accountXml);
			$existingAccounts[$account->Name] = $account;
		}

		return $existingAccounts;
	}

	public function getItem($id)
	{
		$itemXml = $this->transport->QBQueryById('Item', $id);

		if (is_null($itemXml))
			return null;

		return $this->modelMapper->loadItem($itemXml);
	}

	public function getFailedItems()
	{
		$items = $this->transport->QBQuery('Item', array(), true);

		$existingItems = array();
		foreach ($items as $itemXml)
		{
			$item = $this->modelMapper->loadItem($itemXml);
			$existingItems[strtolower($item->Name)] = $item;
		}

		return $existingItems;
	}

	public function getExistingItems()
	{
		$items = $this->transport->QBQuery('Item');

		$existingItems = array();
		foreach ($items as $itemXml)
		{
			$item = $this->modelMapper->loadItem($itemXml);
			$existingItems[$item->Name] = $item;
		}

		return $existingItems;
	}

	public function getExistingItemsById()
	{
		$items = $this->transport->QBQuery('Item');

		$existingItems = array();
		foreach ($items as $itemXml)
		{
			$item = $this->modelMapper->loadItem($itemXml);
			$existingItems[$item->Id] = $item;
		}

		return $existingItems;
	}

	public function persistAccount($account, $operation)
	{
		return $this->persist($account, 'Account', $operation);
	}

	public function persistItem(intuit_Model_Item $item, $operation)
	{
		return $this->persist($item, 'Item', $operation);
	}

	public function getCustomerByName($name)
	{
		if (is_null($this->existingCustomers))
		{
			$this->existingCustomers = $this->getExistingCustomers();
		}

		return isset($this->existingCustomers[$name]) ? $this->existingCustomers[$name] : null;
	}

	public function getExistingCustomers()
	{
		$customers = $this->transport->QBQuery('Customer');

		$existingCustomers = array();
		foreach ($customers as $customer)
		{
			$customer = $this->modelMapper->loadCustomer($customer);
			$existingCustomers[$customer->Name] = $customer;
		}

		return $existingCustomers;
	}

	public function getCustomer($id)
	{
		$customerXml = $this->transport->QBQueryById('Customer', $id);

		if (is_null($customerXml))
			return null;

		return $this->modelMapper->loadCustomer($customerXml);
	}

	public function persistCustomer(intuit_Model_Customer $customer, $operation)
	{
		return $this->persist($customer, 'Customer', $operation);
	}

	public function getSalesReceipt($id)
	{
		$salesReceiptXml = $this->transport->QBQueryById('SalesReceipt', $id);

		if (is_null($salesReceiptXml))
			return null;

		return $this->modelMapper->loadSalesReceipt($salesReceiptXml);
	}

	public function getExistingSalesReceipts()
	{
		$receipts = $this->transport->QBQuery('SalesReceipt');

		$existingReceipts = array();
		foreach ($receipts as $salesReceiptXml)
		{
			$receipt = $this->modelMapper->loadSalesReceipt($salesReceiptXml);

			$existingReceipts[$receipt->getDocNumber()] = $receipt;
		}

		return $existingReceipts;
	}

	public function getFailedSalesReceipts()
	{
		$receipts = $this->transport->QBQuery('SalesReceipt', array(), true);

		$existingReceipts = array();
		foreach ($receipts as $salesReceiptXml)
		{
			$receipt = $this->modelMapper->loadSalesReceipt($salesReceiptXml);

			$existingReceipts[$receipt->getDocNumber()] = $receipt;
		}

		return $existingReceipts;
	}

	public function persistSalesReceipt(intuit_Model_SalesReceipt $receipt, $operation)
	{
		return $this->persist($receipt, 'SalesReceipt', $operation);
	}

	public function persistSalesTax($salesTax, $operation)
	{
		return $this->persist($salesTax, 'SalesTax', $operation);
	}

	protected function persist(intuit_Model_BaseModel $model, $objType, $operation)
	{
		$xml = $model->toXml();

		$qbResponse = $this->transport->QBRequest($objType, $xml, $operation);

		return $this->getObjectRefId($qbResponse);
	}

	protected abstract function getObjectRefId($qbResponse);

	public function createSalesTax($record)
	{
		return null;
	}

	public function disconnect()
	{
		$this->transport->disconnectIA();
	}
}
<?php

class intuit_Services_QBOConnector extends intuit_Services_QBConnector
{
	protected $existingCustomers = null;

	protected function parsePreferences($preferencesXml)
	{
		$xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Preferences>';
		foreach ($preferencesXml as $nvPair)
		{
			$xml .= '<'.$nvPair->Name.'>'.htmlspecialchars($nvPair->Value).'</'.$nvPair->Name.'>';
		}
		$xml .= '</Preferences>';

		$xmlObj = simplexml_load_string($xml);

		return new intuit_Model_QBO_Preferences($xmlObj);
	}

	public function createAccount($type)
	{
		$a = new intuit_Model_QBO_Account();

		if ($type == 'income')
		{
			$a->Subtype = 'Income';
		}
		else if ($type == 'expense')
		{
			$a->Subtype = 'Expense';
		}

		return $a;
	}

	public function createItem($productRecord)
	{
		return new intuit_Model_QBO_Item();
	}

	public function createCustomer($userRecord)
	{
		return new intuit_Model_QBO_Customer();
	}

	public function createSalesReceipt($orderRecord)
	{
		return new intuit_Model_QBO_SalesReceipt();
	}

	public function createSalesReceiptLine()
	{
		return new intuit_Model_QBO_SalesReceiptLine();
	}

	protected function getObjectRefId($qbResponse)
	{
		if (isset($qbResponse['Id']))
		{
			return $qbResponse['Id'];
		}
		return null;
	}
}
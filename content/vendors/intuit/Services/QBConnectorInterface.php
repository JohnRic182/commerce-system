<?php

/**
 * Interface intuit_Services_QBConnectorInterface
 */
interface intuit_Services_QBConnectorInterface
{
	function createAccount($type);

	function persistAccount($account, $operation);

	function getAccounts();

	function getFailedItems();

	function getExistingItems();

	function getExistingItemsById();

	function getItem($id);

	function getSalesTaxCodes();

	function getSalesTaxes();

	function persistItem(intuit_Model_Item $item, $operation);

	function createItem($productRecord);

	function getCustomer($id);

	function getExistingCustomers();

	function persistCustomer(intuit_Model_Customer $customer, $operation);

	function createCustomer($userRecord);

	function getSalesReceipt($id);

	function getExistingSalesReceipts();

	function persistSalesReceipt(intuit_Model_SalesReceipt $receipt, $operation);

	function createSalesReceipt($orderRecord);

	function createSalesReceiptLine();

	function getPreferences();

	function createSalesTax($record);

	function persistSalesTax($salesTax, $operation);

	function disconnect();
}
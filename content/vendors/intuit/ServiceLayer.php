<?php

class intuit_ServiceLayer
{
	protected static function getBaseUrl()
	{
		if (defined('INTUIT_BASE_URL'))
		{
			return INTUIT_BASE_URL;
		}

		return 'https://account.'.getp().'.com/intuit/';
	}

	public static function getBaseApiUrl()
	{
		return self::getBaseUrl().'api/';
	}

	public static function getGrantUrl($popup = false)
	{
		global $settings;

		$url = self::getBaseUrl().'auth_grant.php?';
		$url .= 't='.base64_encode(LICENSE_NUMBER);
		$url .= '&u='.urlencode($settings['GlobalHttpsUrl']);
		$url .= '&'.time();
		if ($popup)
			$url .= '&popup=true';

		return $url;
	}

	public static function getConnectUrl($popup = false)
	{
		global $settings;

		$url = self::getBaseUrl().'openid_auth.php?';
		$url .= 't='.base64_encode(LICENSE_NUMBER);
		$url .= '&u='.urlencode($settings['GlobalHttpsUrl']);
		$url .= '&'.time();
		if ($popup)
			$url .= '&popup=true';

		return $url;
	}

	public static function menuProxy()
	{
		global $db;

		$db->query("SELECT * FROM ".DB_PREFIX."admin_oauth WHERE uid = 0");
		$row = $db->moveNext();
		if ($row)
		{
			$url = self::getBaseApiUrl().'menu_proxy.php?';
			$url .= 't='.base64_encode(LICENSE_NUMBER);
			$url .= '&'.time();
			$url .= '&h='.base64_encode(md5($row['oauth_token']));

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			$r = curl_exec($ch);

			print $r;	
		}
	}

	public static function disconnect()
	{
		global $db;

		$db->query("SELECT * FROM ".DB_PREFIX."admin_oauth WHERE uid=0");
		if ($row = $db->moveNext())
		{
			$url = self::getBaseApiUrl().'disconnect.php?';
			$url .= 't='.base64_encode(LICENSE_NUMBER);
			$url .= '&'.time();
			$url .= '&h='.base64_encode(md5($row['oauth_token']));

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			$r = curl_exec($ch);

			$db->query("
				DELETE
				FROM ".DB_PREFIX."admin_oauth
				WHERE uid = 0"
			);
		}
	}

	public static function verifyTokens($token, $tokenSecret, $realmId, $dataSource)
	{
		$hash = sha1($token.'|'.$tokenSecret.'|'.$realmId.'|'.$dataSource);

		$url = self::getBaseApiUrl().'sig_check.php?';
		$url .= 't='.base64_encode(LICENSE_NUMBER);
		$url .= '&'.time();
		$url .= '&h='.base64_encode($hash);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$r = curl_exec($ch);

		return ($r == 'verified');
	}	
}
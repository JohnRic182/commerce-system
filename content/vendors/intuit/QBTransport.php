<?php

/**
 * Class intuit_QBTransport
 */
abstract class intuit_QBTransport
{
	public static $ids_version = 'v3';

	protected $lastRequest;
	protected $lastResponse;

	protected $authProvider;
	protected $realmId;

	/**
	 * intuit_QBTransport constructor.
	 * @param intuit_IntuitAuthProvider $authProvider
	 * @param $realmId
	 */
	public function __construct(intuit_IntuitAuthProvider $authProvider, $realmId)
	{
		$this->authProvider = $authProvider;
		$this->realmId = $realmId;
	}

	/**
	 * @return mixed
	 */
	public function getLastRequest()
	{
		return $this->lastRequest;
	}

	/**
	 * @return mixed
	 */
	public function getLastResponse()
	{
		return $this->lastResponse;
	}

	/**
	 * @param $obj_type
	 * @param $id
	 * @return mixed
	 */
	public abstract function QBQueryById($obj_type, $id);

	/**
	 * @param $obj_type
	 * @param array $fields
	 * @param bool $deleted
	 * @return mixed
	 */
	public abstract function QBQuery($obj_type, $fields = array(), $deleted = false);

	/**
	 * @param $obj_type
	 * @param $fields
	 * @param $operation
	 * @return mixed
	 */
	public abstract function QBRequest($obj_type, $fields, $operation);

	/**
	 * @param $obj_type
	 * @param $fields
	 * @param $deleted
	 * @return mixed
	 */
	protected abstract function toQueryXml($obj_type, $fields, $deleted);

	/**
	 * @param $obj_type
	 * @param $fields
	 * @param string $operation
	 * @return mixed
	 */
	protected abstract function toRequestXml($obj_type, $fields, $operation = '');

	/**
	 * @throws intuit_AuthorizationFailureException
	 */
	public function disconnectIA()
	{
		$url = 'https://appcenter.intuit.com/api/v1/Connection/Disconnect';

		$headers = array();
		$headers['Content-Type'] = 'application/x-www-form-urlencoded';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, array(), 'GET');

		$response = $this->doGet($url, $headers);
	}

	/**
	 * @param $url
	 * @param $reqHeaders
	 * @param array $getParams
	 * @return bool|mixed
	 * @throws intuit_AuthorizationFailureException
	 */
	protected function doGet($url, $reqHeaders, $getParams = array())
	{
		$headers = array();
		foreach ($reqHeaders as $key => $value)
		{
			$headers[] = $key . ': ' . $value;
		}
		$headers[] = 'Content-Length: ' . strlen('');

		$query = '';
		if (count($getParams) > 0)
		{
			$query = '?'.http_build_query($getParams);
		}

		$params = array();
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url . $query;
		$params[CURLOPT_HTTPHEADER] = $headers;

		// debug
		$params[CURLOPT_VERBOSE] = false;

		// return headers
		$params[CURLOPT_HEADER] = false;

		$request = '';
		$request .= 'GET ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";
		foreach ($headers as $header)
		{
			$request .= $header . "\r\n";
		}

		intuit_QBLogger::log('Request to: '.$url, 'DEBUG');
		intuit_QBLogger::log('Raw request: '.$request, 'DEBUG');

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		$response = curl_exec($ch);

		intuit_QBLogger::log('Raw response: '.$response, 'DEBUG');

		if (strpos($response, 'Unauthorized OAuth Token') !== false || strpos($response, 'oauth_problem=') !== false)
		{
			intuit_QBLogger::debug('OAuth token expired');
			throw new intuit_AuthorizationFailureException();
		}

		if (curl_errno($ch))
		{
			$errnum = curl_errno($ch);
			$errmsg = curl_error($ch);

			intuit_QBLogger::error($errnum.': '.$errmsg);

			return false;
		}

		@curl_close($ch);

		return $response;
	}

	/**
	 * @param $url
	 * @param $rawBody
	 * @param $reqHeaders
	 * @param null $errnum
	 * @param null $errmsg
	 * @return bool|mixed|string
	 * @throws intuit_AuthorizationFailureException
	 */
	protected function requestCurl($url, $rawBody, $reqHeaders, &$errnum = null, &$errmsg = null)
	{
		foreach ($reqHeaders as $key => $value)
		{
			$headers[] = $key . ': ' . $value;
		}

		$headers[] = 'Content-Length: ' . strlen($rawBody);

		$params = array();
		$params[CURLOPT_POST] = true;
		$params[CURLOPT_POSTFIELDS] = $rawBody;
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url;
		$params[CURLOPT_HTTPHEADER] = $headers;

		$params[CURLOPT_FOLLOWLOCATION] = true;
		$params[CURLOPT_VERBOSE] = false;
		$params[CURLOPT_HEADER] = false;

		$request = 'POST ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";

		foreach ($headers as $header)
		{
			$request .= $header . "\r\n";
		}

		$request .= "\r\n";
		$request .= $rawBody;

		$this->lastRequest = $request;

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		intuit_QBLogger::log('Request to: ' . $url, 'DEBUG');
		intuit_QBLogger::log('Raw request: ' . $request, 'DEBUG');

		$response = curl_exec($ch);

		intuit_QBLogger::log('Raw response: ' . $response, 'DEBUG');

		if (strpos($response, 'Unauthorized OAuth Token') !== false || strpos($response, 'oauth_problem=') !== false)
		{
			intuit_QBLogger::debug('OAuth token expired');
			throw new intuit_AuthorizationFailureException();
		}

		if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404)
		{
			return '';
		}

		$this->lastResponse = $response;

		if (curl_errno($ch))
		{
			if ($errnum) $errnum = curl_errno($ch);
			if ($errmsg) $errmsg = curl_error($ch);
			intuit_QBLogger::error($errnum.': '.$errmsg);
			return false;
		} 

		@curl_close($ch);

		return $response;
	}

	/**
	 * @param $array_item
	 * @return string
	 */
	protected function xmlify($array_item)
	{
		$xml = '';
		foreach($array_item as $element => $value)
		{
			$close_element = $element;
			if(strpos($close_element, ' '))
			{
				$close_element = substr($close_element, 0, strpos($close_element, ' '));
			}

    		if (is_array($value))
    		{
				if (is_numeric($element))
					$xml .= $this->xmlify($value);
				else
        			$xml .= "<$element>".$this->xmlify($value)."</$close_element>";
    		}
    		elseif($value == '')
    		{
        		$xml .= "<$element />";
    		}
    		else
    		{
        		$xml .= "<$element>".htmlentities($value)."</$close_element>";
    		}
		}

		return $xml;
	}

	/**
	 * @param $xmlObject
	 * @param array $out
	 * @return array
	 */
	public function arrayify ($xmlObject, $out = array())
	{
		foreach ((array)$xmlObject as $index => $node)
		{
			$out[$index] = (is_object( $node )) ? $this->arrayify ($node) : $node;
		}

		return $out;
	}

	/**
	 * @param intuit_IntuitAuthProvider $authProvider
	 * @param $realmId
	 * @param string $dataSource
	 * @return intuit_QBDTransport|intuit_QBOTransport
	 */
	public static function get(intuit_IntuitAuthProvider $authProvider, $realmId, $dataSource = 'QBO')
	{
		if ($dataSource == 'QBO')
		{
			return new intuit_QBOTransport($authProvider, $realmId);
		}

		return new intuit_QBDTransport($authProvider, $realmId);
	}
}
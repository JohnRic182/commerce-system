<?php

/**
 * Class intuit_IntuitSamlProvider
 */
class intuit_IntuitSamlProvider extends intuit_IntuitAuthProvider
{
	protected $intuitAppToken;
	protected $ticket;

	/**
	 * intuit_IntuitSamlProvider constructor.
	 * @param $intuitAppToken
	 * @param $ticket
	 */
	public function __construct($intuitAppToken, $ticket)
	{
		$this->intuitAppToken = $intuitAppToken;
		$this->ticket = $ticket;
	}

	/**
	 * @param $url
	 * @param array $fields
	 * @return string
	 */
	public function getAuthorizationHeader($url, $fields = array())
	{
		return 'INTUITAUTH intuit-app-token="' . $this->intuitAppToken . '",intuit-token="' . $this->ticket . '"';
	}
}
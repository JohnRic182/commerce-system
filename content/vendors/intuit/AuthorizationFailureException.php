<?php

class intuit_AuthorizationFailureException extends Exception
{
	public function __construct($message = '')
	{
		parent::__construct($message);
	}
}
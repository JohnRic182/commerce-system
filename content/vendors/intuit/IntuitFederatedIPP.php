<?php

/**
 * Class intuit_IntuitFederatedIPP
 */
class intuit_IntuitFederatedIPP
{
	protected $realmId;
	protected $ticket;
	protected $token;

	/**
	 * intuit_IntuitFederatedIPP constructor.
	 * @param $realmId
	 * @param $ticket
	 * @param $token
	 */
	public function __construct($realmId, $ticket, $token)
	{
		$this->realmId = $realmId;
		$this->ticket = $ticket;
		$this->token = $token;
	}

	/**
	 * @return array
	 */
	public function getUserInfo()
	{
		$xml = '<qdbapi>
   				<ticket>' . $this->ticket . '</ticket>
   				<apptoken>' . $this->token . '</apptoken>';	
		
		$xml .= '
			</qdbapi>';

		$response = $this->requestCurl('API_GetUserInfo', $xml);

		$xml_res = simplexml_load_string($response);
		
		if (is_object($xml_res) && array_key_exists('errcode', $xml_res) && $xml_res->errcode <> 0)
		{
			error_log ("QB IPP Issue Request: API_GetUserInfo");
			error_log ("QB IPP Issue Response: " . $response);

			return array();
		}

		return $this->arrayify($xml_res->user);
	}

	/**
	 * @param $realmId
	 * @return array|SimpleXMLElement[]
	 */
	public function getIsQBORealm($realmId)
	{
		$xml = '<qdbapi>
   				<ticket>' . $this->ticket . '</ticket>
   				<apptoken>' . $this->token . '</apptoken>';	
		
		$xml .= '
			</qdbapi>';

		$response = $this->requestCurl('API_GetIsRealmQBO', $xml, $realmId);

		$xml_res = simplexml_load_string($response);
		
		if (is_object($xml_res) && array_key_exists('errcode', $xml_res) && $xml_res->errcode <> 0)
		{
		 	error_log ("QB IPP Issue Request: API_GetUserInfo");
		 	error_log ("QB IPP Issue Response: " . $response);

		 	return array();
		}

		return $xml_res->IsQBO;
	}

	/**
	 * @param $xmlObject
	 * @param array $out
	 * @return array
	 */
	protected function arrayify ($xmlObject, $out = array())
	{
		foreach ((array)$xmlObject as $index => $node)
		{
			$out[$index] = (is_object( $node )) ? $this->arrayify ($node) : $node;
		}

		return $out;
	}

	/**
	 * @param $action
	 * @param string $xml
	 * @param string $endpoint
	 * @return bool|mixed
	 */
	protected function requestCurl($action, $xml = '', $endpoint = 'main')
	{
		$url = 'https://workplace.intuit.com/db/'.$endpoint;

		$rawBody = $xml;

		$headers = array();
		$headers[] = 'QUICKBASE-ACTION:'.$action;
		$headers[] = 'Content-Type: application/xml';
		$headers[] = 'Content-Length: ' . strlen($rawBody);

		$params = array();
		$params[CURLOPT_POST] = true;
		$params[CURLOPT_POSTFIELDS] = $rawBody;
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url;
		$params[CURLOPT_HTTPHEADER] = $headers;

		// debug
		$params[CURLOPT_VERBOSE] = false;

		// return headers
		$params[CURLOPT_HEADER] = false;

		$request = 'POST ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";

		foreach ($headers as $header)
		{
			$request .= $header . "\r\n";
		}
		$request .= "\r\n";
		$request .= $rawBody;

		$ch = curl_init();
		curl_setopt_array($ch, $params);
	
		$response = curl_exec($ch);

		if (curl_errno($ch))
		{
			if ($errnum)
				$errnum = curl_errno($ch);
			if ($errmsg)
				$errmsg = curl_error($ch);

			return false;
		} 

		@curl_close($ch);

		return $response;
	}
}
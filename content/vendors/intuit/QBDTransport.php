<?php

/**
 * Class intuit_QBDTransport
 */
class intuit_QBDTransport extends intuit_QBTransport
{



	/**
	 * intuit_QBDTransport constructor.
	 * @param intuit_IntuitAuthProvider $authProvider
	 * @param $realmId
	 */
	public function __construct(intuit_IntuitAuthProvider $authProvider, $realmId)
	{
		parent::__construct($authProvider, $realmId);
	}


	/**
	 * @param $obj_type
	 * @param array $fields
	 * @param bool $failed
	 * @return array|SimpleXMLElement
	 * @throws intuit_AuthorizationFailureException
	 */
	public function QBQuery($obj_type, $fields = array(), $failed = false)
	{
		$resource = strtolower($obj_type);

		if ($obj_type == 'Preferences')
			$resource = 'preference';

		$url = 'https://quickbooks.api.intuit.com/' . self::$ids_version . '/company/' . $this->realmId;

		$headers = array();
		$headers['Content-Type'] = 'text/xml';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, array());

		$xmlType = $obj_type;
		if ($obj_type == 'Preferences')
			$xmlType = 'IntuitRequest';

		$xml = $this->toQueryXml($xmlType, $fields, $failed);

		$response = $this->requestCurl($url, $xml, $headers);
		$xml_res = simplexml_load_string($response);

		if (is_object($xml_res) && array_key_exists('Error', $xml_res))
		{
			if (isset($xml_res->Error) && isset($xml_res->Error->ErrorCode) && (string)$xml_res->Error->ErrorCode == '401')
			{
				throw new intuit_AuthorizationFailureException($xml_res->Message);
			}

			error_log ("QB Sync Issue Request: " . $xml);
			error_log ("QB Sync Issue Response: " . $response);
			return array();
		}

		if ($obj_type == 'Preferences')
			return $xml_res;

		if ($obj_type == 'SalesTax')
			$setName = $obj_type . 'es';
		else
			$setName = $obj_type . 's';

		$resultsPerPage = 500;

		if (!isset($xml_res->$setName))
		{

			error_log ("QB Sync Issue Request: " . $xml);
			error_log ("QB Sync Issue Response: " . $response);
			return array();
		}

		$results = array();
		$resultsTemp = $xml_res->$setName->children();
		foreach ($resultsTemp as $result)
		{
			$results[] = $result;
		}
		$resultCount = count($results);
		$resultPage = 1;

		while ($resultCount == $resultsPerPage)
		{
			$resultPage += 1;

			intuit_QBLogger::debug('Loading ResultsPage: '.$resultPage);

			$fields['StartPage'] = $resultPage;
			$fields['ChunkSize'] = $resultsPerPage;

			$xml = $this->toQueryXml($xmlType, $fields, $failed);

			$response = $this->requestCurl($url, $xml, $headers);
			$xml_res = simplexml_load_string($response);

			$new_set = $xml_res->$setName;
			$new_results = $new_set->children();
			foreach ($new_results as $new_result)
			{
				$results[] = $new_result;
			}
			$resultCount = count($new_results);
		}

		return $results;
	}

	/**
	 * @param $obj_type
	 * @param $id
	 * @param bool $failed
	 * @throws intuit_AuthorizationFailureException
	 */
	public function SyncStatus($obj_type, $id, $failed = false)
	{
		$url = 'https://services.intuit.com/sb/status/' . self::$ids_version . '/' . $this->realmId;

		$headers = array();
		$headers['Content-Type'] = 'text/xml';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, array());

		$xml = '';
		$xml .= '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<SyncStatusRequest'.($failed ? ' ErroredObjectsOnly="true"' : '').' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
		$xml .= 'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ';
		$xml .= 'xmlns="http://www.intuit.com/sb/cdm/v2">';

		$xml .= '<OfferingId>ipp</OfferingId>';
		$xml .= '<NgIdSet>';
		$xml .= '<NgId>'.$id.'</NgId>';
		$xml .= '<NgObjectType>'.$obj_type.'</NgObjectType>';
		$xml .= '</NgIdSet>';

		$xml .= '</SyncStatusRequest';

		$response = $this->requestCurl($url, $xml, $headers);

		$xml_res = simplexml_load_string($response);

		$xml = '';

		echo '<pre>'.print_r($xml_res,1).'</pre>';
	}

	/**
	 * @param $obj_type
	 * @param $id
	 * @return array|null
	 * @throws intuit_AuthorizationFailureException
	 */
	public function QBQueryById($obj_type, $id)
	{
		$url = 'https://services.intuit.com/sb/' . strtolower($obj_type) . '/' . self::$ids_version . '/' . $this->realmId . '/' . $id; // . '?idDomain=NG';

		$params = array();
		$params['idDomain'] = 'NG';

		$headers = array();
		$headers['Content-Type'] = 'text/xml';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, $params, 'GET');

		$response = $this->doGet($url, $headers, $params);

		$xml_res = simplexml_load_string($response);
		
		$xml = '';

		if (is_object($xml_res) && array_key_exists('Error', $xml_res))
		{
			error_log ("QB Sync Issue Request: " . $xml);
			error_log ("QB Sync Issue Response: " . $response);
			return array();
		}

		if (is_null($xml_res))
		{
			return null;
		}

		if ($obj_type == 'SalesTax')
			$setName = $obj_type . 'es';
		else
			$setName = $obj_type . 's';

		$set = $xml_res->$setName;

		$children = $set->children();
		if (count($children) > 0)
		{
			return $children[0];
		}
		return null;
	}

	/**
	 * @param $obj_type
	 * @param $itemXml
	 * @param $operation
	 * @return array|SimpleXMLElement
	 * @throws intuit_AuthorizationFailureException
	 * @throws intuit_DuplicateItemException
	 */
	public function QBRequest($obj_type, $itemXml, $operation)
	{
		$url = 'https://services.intuit.com/sb/' . strtolower($obj_type) . '/' . self::$ids_version . '/' . $this->realmId;

		$headers = array();
		$headers['Content-Type'] = 'text/xml';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url);

		$xml = $this->toRequestXml($obj_type, $itemXml, $operation);

		$response = $this->requestCurl($url, $xml, $headers);
		$xml_res = simplexml_load_string($response);

		if (is_object($xml_res) && array_key_exists('Error', $xml_res))
		{
			if (isset($xml_res->Error) && isset($xml_res->Error->ErrorCode) && (string)$xml_res->Error->ErrorCode == '-3002')
			{
				throw new intuit_DuplicateItemException($xml_res->Error->ErrorDesc);
			}
			else if (isset($xml_res->Error) && isset($xml_res->Error->ErrorCode) && (string)$xml_res->Error->ErrorCode == '401')
			{
				throw new intuit_AuthorizationFailureException($xml_res->Message);
			}

			error_log ("QB Sync Issue Request: " . $xml);
			error_log ("QB Sync Issue Response: " . $response);
			return array();
		}

		return $xml_res;
	}

	/**
	 * @param $obj_type
	 * @param $fields
	 * @param $failed
	 * @return string
	 */
	protected function toQueryXml($obj_type, $fields, $failed)
	{
		$xml = '';
		$xml .= '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<' . $obj_type . 'Query'.($failed ? ' ErroredObjectsOnly="true"' : '').' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
		$xml .= 'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ';
		$xml .= 'xmlns="http://www.intuit.com/sb/cdm/v2">';

		if (count($fields) > 0)
			$xml .= $this->xmlify($fields);
		else
			$xml .= '<!-- no filter -->';

		$xml .= '</' . $obj_type . 'Query>';

		return $xml;
	}

	/**
	 * @param $obj_type
	 * @param $itemXml
	 * @param string $operation
	 * @return string
	 */
	protected function toRequestXml($obj_type, $itemXml, $operation = '')
	{
		$xml = '';
		$xml .= '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<' . $operation . ' xmlns="http://www.intuit.com/sb/cdm/v2" ';
		$xml .= '	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
		$xml .= '	RequestId="' . md5(mt_rand() . microtime()) . '" ';
		$xml .= '	xsi:schemaLocation="http://www.intuit.com/sb/cdm/v2/ ../common/RestDataFilter.xsd">';
		$xml .= '	<OfferingId>ipp</OfferingId>';
		$xml .= '	<ExternalRealmId>' . $this->realmId . '</ExternalRealmId>';
		$xml .= '<Object xsi:type="' . $obj_type . '">';

		$xml .= $itemXml;
	      
		$xml .= '</Object>';
		$xml .= '</' . $operation . '>';

		return $xml;
	}
}
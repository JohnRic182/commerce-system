<?php

/**
 * Class intuit_QBOTransport
 */
class intuit_QBOTransport extends intuit_QBTransport
{
	protected $QBO_base_url;

	/**
	 * intuit_QBOTransport constructor.
	 * @param intuit_IntuitAuthProvider $authProvider
	 * @param $realmId
	 */
	public function __construct(intuit_IntuitAuthProvider $authProvider, $realmId)
	{
		parent::__construct($authProvider, $realmId);
	}

	/**
	 * @param $obj_type
	 * @param $id
	 * @return null|SimpleXMLElement
	 * @throws intuit_AuthorizationFailureException
	 */
	public function QBQueryById($obj_type, $id)
	{
		$url = $this->getQBOUrl($obj_type, $id);

		$params = array();

		$headers = array();
		$headers['Content-Type'] = 'application/x-www-form-urlencoded';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, array(), 'GET');

		$response = $this->doGet($url, $headers, $params);

		if ($response == '' || is_null($response))
			return null;

		$xml_res = simplexml_load_string($response);

		if (is_object($xml_res) && array_key_exists('ErrorCode', $xml_res))
		{
			error_log ("QBO Sync Issue Request: " . $url);
			error_log ("QBO Sync Issue Response: " . $response);
			return null;
		}

		return $xml_res;
	}

	/**
	 * @param $obj_type
	 * @param array $fields
	 * @param bool $failed
	 * @return array
	 * @throws intuit_AuthorizationFailureException
	 */
	public function QBQuery($obj_type, $fields = array(), $failed = false)
	{
		if ($failed)
		{
			return array();
		}

		$QBO_base_url = $this->getQBOBaseURL();

		if ($QBO_base_url == null)
		{
			error_log("Couldn't retrieve a QBO Base URL");
			return array();
		}

		$url = $this->getQBOUrl($obj_type);

		// Initial call is made with max allowed paging
		$resultsPerPage = 100;
		$fields['ResultsPerPage'] = $resultsPerPage;
		$fields['PageNum'] = 1;

		$headers = array();
		$headers['Content-Type'] = 'application/x-www-form-urlencoded';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, $fields);

		$payload = '';

		foreach($fields as $name => $value)
		{
		  	$payload .= '&'.urlencode($name).'='.urlencode($value);
		}

		$page_payload = $payload;
		$page_payload = ltrim($page_payload, '&');

		$response = $this->requestCurl($url, $page_payload, $headers);

		if ($response == '' || is_null($response))
			return null;

		$xml_res = simplexml_load_string($response);

		if (is_object($xml_res) && array_key_exists('ErrorCode', $xml_res))
		{
			if ($xml_res->ErrorCode == '401')
			{
				throw new intuit_AuthorizationFailureException($xml_res->Message);
			}

			error_log ("QBO Sync Issue Request: " . $url."\n".$page_payload);
			error_log ("QBO Sync Issue Response: " . $response);
			return array();
		}

		if ($obj_type == 'Preferences')
		{
			return $xml_res->xpath('//qbo:Preference');
		}

		$results = array();
		$resultsTemp = $xml_res->xpath('//qbo:SearchResults/qbo:CdmCollections/*');
		foreach ($resultsTemp as $result)
		{
			$results[] = $result;
		}
		$resultCount = $xml_res->xpath('//qbo:SearchResults/qbo:Count');
		$resultCount = $resultCount[0][0];
		$resultPage = 1;

		while ($resultCount == $resultsPerPage)
		{
			$resultPage += 1;

			intuit_QBLogger::debug('Loading ResultsPage: '.$resultPage);

			$fields['PageNum'] = $resultPage;
			$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, $fields);

			$payload = '';
	
			foreach($fields as $name => $value)
			{
			  	$payload .= '&'.urlencode($name).'='.urlencode($value);
			}

			$page_payload = $payload;
			$page_payload = ltrim($page_payload, '&');

			$response = $this->requestCurl($url, $page_payload, $headers);
			$xml_res = simplexml_load_string($response);

			$new_results = (array)$xml_res->xpath('//qbo:SearchResults/qbo:CdmCollections/*');
			foreach ($new_results as $new_result)
			{
				$results[] = $new_result;
			}
			$resultCount = $xml_res->xpath('//qbo:SearchResults/qbo:Count');
			$resultCount = $resultCount[0][0];
		}

		return $results;
	}

	/**
	 * @param $obj_type
	 * @param $id
	 * @param $syncToken
	 * @return array
	 * @throws intuit_AuthorizationFailureException
	 */
	public function QBDelete($obj_type, $id, $syncToken)
	{
		$url = $this->getQBOUrl($obj_type, $id);

		$headers = array();
		$headers['Content-Type'] = 'application/xml';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url, array('methodx' => 'delete'));

		$url .= '?methodx=delete';

		$fields = array(
			'Id' => $id,
			'SyncToken' => $syncToken,
		);

		$xml = $this->toRequestXml($obj_type, $fields);

		$response = $this->requestCurl($url, $xml, $headers);
		$xml_res = simplexml_load_string($response);

		if (is_object($xml_res) && array_key_exists('ErrorCode', $xml_res))
		{
			if ($xml_res->ErrorCode == '401')
			{
				throw new intuit_AuthorizationFailureException($xml_res->Message);
			}

			error_log ("QBO Sync Issue Request: " . $xml);
			error_log ("QBO Sync Issue Response: " . $response);

			return array();
		}

		return $this->arrayify($xml_res);
	}

	/**
	 * @param $obj_type
	 * @param $fieldsXml
	 * @param $operation
	 * @return array
	 * @throws intuit_AuthorizationFailureException
	 * @throws intuit_DuplicateItemException
	 */
	public function QBRequest($obj_type, $fieldsXml, $operation)
	{
		$QBO_base_url = $this->getQBOBaseURL();

		$resourceObj = strtolower($obj_type);
		if ($resourceObj == 'salesreceipt')
			$resourceObj = 'sales-receipt';
		else if ($resourceObj == 'paymentmethod')
			$resourceObj = 'payment-method';

		$url = $QBO_base_url . '/resource/' . $resourceObj . '/' . self::$ids_version . '/' . $this->realmId;

		$headers = array();
		$headers['Content-Type'] = 'application/xml';
		$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url);

		$xml = $this->toRequestXml($obj_type, $fieldsXml, $operation);

		$response = $this->requestCurl($url, $xml, $headers);
		$xml_res = simplexml_load_string($response);

		if (is_object($xml_res) && array_key_exists('ErrorCode', $xml_res))
		{
			if ($xml_res->ErrorCode == '401')
			{
				throw new intuit_AuthorizationFailureException($xml_res->Message);
			}
			else if ($xml_res->Cause == '-11202')
			{
				throw new intuit_DuplicateItemException($xml_res->Message);
			}

			error_log ("QBO Sync Issue Request: " . $xml);
			error_log ("QBO Sync Issue Response: " . $response);

			return array();
		}

		return $this->arrayify($xml_res);
	}

	/**
	 * @param $obj_type
	 * @param null $id
	 * @return string
	 */
	private function getQBOUrl($obj_type, $id = null)
	{
		$resourceObj = strtolower($obj_type);
		if ($resourceObj == 'salesreceipt')
			$resourceObj = 'sales-receipt';
		else if ($resourceObj == 'paymentmethod')
			$resourceObj = 'payment-method';
		else if ($resourceObj == 'preferences')
			$resourceObj = 'preference';

		return $this->getQBOBaseURL().'/resource/' . $resourceObj . ($id ? '' : 's') . '/' . self::$ids_version . '/' . $this->realmId . ($id ? '/'. $id : '');
	}

	/**
	 * @return null|SimpleXMLElement
	 * @throws intuit_AuthorizationFailureException
	 */
	private function getQBOBaseURL()
	{
		if (is_null($this->QBO_base_url))
		{
			$url = 'https://quickbooks.api.intuit.com/' . self::$ids_version . '/company/' . $this->realmId;
	
			$headers = array();
			$headers['Accept'] = 'application/json';
			$headers['Content-Type'] = 'application/json';
			$headers['Authorization'] = $this->authProvider->getAuthorizationHeader($url);
	
			$result = $this->requestCurl($url . '/preferences', '', $headers);

			$userJson = json_decode($result);

			//$userJson = simplexml_load_string($res);

			if (is_object($userJson) && array_key_exists('ErrorCode', $userJson))
			{
				if ($userJson->ErrorCode == '401')
				{
					//throw new intuit_AuthorizationFailureException($user_xml->Message);
				}

//				error_log ("QBO Sync Issue Request: " . $url);
//				error_log ("QBO Sync Issue Response: " . $res);
				return null;
			}

			$base_url_element = (is_object($userJson)) ? @$user_xml->xpath('//qbo:QboUser/qbo:CurrentCompany/qbo:BaseURI') : false;
	
			if (!$base_url_element)
				return null;

			$this->QBO_base_url = $base_url_element[0];
		}

		return $this->QBO_base_url;
	}

	/**
	 * @param $obj_type
	 * @param $fields
	 * @param $failed
	 * @return mixed|string
	 */
	protected function toQueryXml($obj_type, $fields, $failed)
	{
		return $this->toRequestXml($obj_type,  $this->xmlify($fields));
	}

	/**
	 * @param $obj_type
	 * @param $fieldsXml
	 * @param string $operation
	 * @return mixed|string
	 */
	protected function toRequestXml($obj_type, $fieldsXml, $operation = '')
	{
		$type = str_replace('-', '', $obj_type);

		$xml = '';
		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		if ($obj_type == 'SalesReceipt' || $obj_type == 'Customer' || $obj_type == 'Item')
		{
			$xml .= '<' . $obj_type . ' xmlns="http://www.intuit.com/sb/cdm/v2" xmlns:ns2="http://www.intuit.com/sb/cdm/qbopayroll/v1" xmlns:ns3="http://www.intuit.com/sb/cdm/qbo">';
		}
		else
		{
			$xml .= '<' . $obj_type . ' xmlns="http://www.intuit.com/sb/cdm/qbo" xmlns="http://www.intuit.com/sb/cdm/v2">';
		}

		$xml .= $fieldsXml;

		$xml .= '</' . $obj_type . '>';

		$xml = str_replace('<CustomerId>', '<CustomerId idDomain="QBO">', $xml);

		if ($obj_type == 'Item')
		{
			$xml = str_replace('<Id>', '<Id idDomain="QBO">', $xml);
			$xml = str_replace('<AccountId>', '<AccountId idDomain="QBO">', $xml);
		}
		return $xml;
	}
}
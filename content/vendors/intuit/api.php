<?php

chdir(dirname(dirname(dirname(dirname(__FILE__)))));

require_once 'content/classes/_boot.php';
require_once(dirname(__FILE__).'/ServiceLayer.php');

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

$pwd = 'intuit|X(X((f(D';

switch($action)
{
	case 'get_key':
	{
		if ($settings['intuit_certificate'] == '' || $settings['intuit_certificate'] == 'No')
		{
			$ds = DIRECTORY_SEPARATOR;
			$openSSL = new openSSL(4096, OPENSSL_KEYTYPE_RSA, 3650, $settings["GlobalServerPath"].$ds."content".$ds."ssl".$ds."openssl.cnf");

			$companyName = 'PC Cart';

			$dn = array(
					"countryName" => 'US',
					"stateOrProvinceName" => 'Arizona',
					"localityName" => 'Phoenix',
					"organizationName" => $companyName,
					"organizationalUnitName" => $companyName." Unit",
					"commonName" => $companyName,
					"emailAddress" => $settings["CompanyEmail"]
				);

			if (!$openSSL->generate($dn, $pwd))
			{
				//Error
			}
			else
			{
				$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape(base64_encode($openSSL->csr_pem))."' WHERE name='intuit_certificate'");
				$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape(base64_encode($openSSL->private_key_pem))."' WHERE name='intuit_private_key'");
				$settings['intuit_private_key'] = base64_encode($openSSL->private_key_pem);
				$settings['intuit_certificate'] = base64_encode($openSSL->csr_pem);
			}
		}

		echo $settings['intuit_certificate'];
		break;
	}
	case 'set_tokens':
	{
		if ($settings['intuit_certificate'] != '')
		{
			$ds = DIRECTORY_SEPARATOR;
			$openSSL = new openSSL(4096, OPENSSL_KEYTYPE_RSA, 3650, $settings["GlobalServerPath"].$ds."content".$ds."ssl".$ds."openssl.cnf");

			$token = $openSSL->decrypt(base64_decode($_REQUEST['token']), $pwd, base64_decode($settings['intuit_private_key']));
			$tokenSecret = $openSSL->decrypt(base64_decode($_REQUEST['tokenSecret']), $pwd, base64_decode($settings['intuit_private_key']));

			$custom1 = $_REQUEST['custom1'];
			$custom2 = $_REQUEST['custom2'];

			$realmId = $_REQUEST['realmId'];
			$dataSource = $_REQUEST['dataSource'];

			if (intuit_ServiceLayer::verifyTokens($token, $tokenSecret, $realmId, $dataSource))
			{
				if (!is_null($token) && $token != '' && $token)
				{
					//TODO: Validate token has access to realm (do get user call?)

					$db->query("DELETE FROM ".DB_PREFIX."admin_oauth");
					$db->query("INSERT INTO ".DB_PREFIX."admin_oauth (uid, oauth_token, oauth_token_secret, realm_id, data_source) VALUES (0, '".$db->escape($token)."', '".$db->escape($tokenSecret)."', '".$db->escape($realmId)."', '".$db->escape($dataSource)."')");
					$db->query("UPDATE ".DB_PREFIX."settings SET value = 'Yes' WHERE name='intuit_anywhere_enabled'");
					$db->query("UPDATE ".DB_PREFIX."settings SET value = 'Yes' WHERE name='intuit_anywhere_connected'");
					$db->query("UPDATE ".DB_PREFIX."settings SET value = '".$db->escape($custom1)."' WHERE name='intuit_anywhere_custom1'");
					$db->query("UPDATE ".DB_PREFIX."settings SET value = '".$db->escape($custom2)."' WHERE name='intuit_anywhere_custom2'");
					if ($dataSource == 'QBO')
					{
						$db->query("UPDATE ".DB_PREFIX."settings SET value = 'online' WHERE name='intuit_quickbooks_version'");
						$db->query("UPDATE ".DB_PREFIX."settings SET value = 'SHIPPING_LINE_ID' WHERE name='intuit_shipping_product_id'");
					}
					else
					{
						$db->query("UPDATE ".DB_PREFIX."settings SET value = 'desktop' WHERE name='intuit_quickbooks_version'");
					}
					$db->query('INSERT INTO '.DB_PREFIX.'enabled_apps (`key`) VALUES ("quickbooks")');
				}
			}
			
		}

		break;
	}
}
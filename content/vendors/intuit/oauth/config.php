<?php

global $settings, $menu_proxy, $grant_url;

$menu_proxy = $settings["GlobalHttpsUrl"].'/content/vendors/intuit/oauth/menu_proxy.php';
$grant_url = intuit_ServiceLayer::getGrantUrl(true);

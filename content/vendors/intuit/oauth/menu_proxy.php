<?php

	require_once '../../../engine/engine_config.php';
	require_once '../../../engine/engine_functions.php';
	
	$db = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

	$settingsRepository = new DataAccess_SettingsRepository($db);
	$settings = $settingsRepository->getAll();

	Timezone::setApplicationDefaultTimezone($db, $settings);

	require_once dirname(__FILE__).'/config.php';

	require_once dirname(__FILE__).'/../ServiceLayer.php';

	intuit_ServiceLayer::menuProxy();
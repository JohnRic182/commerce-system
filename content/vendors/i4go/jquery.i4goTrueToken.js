/*
	jquery.i4goTrueToken.js

	jQuery plug-in to (hopefully) simplify the i4Go integration process.
	
	DEPENDANCIES:
		jQuery.js version 2.0+

	Copyright (c) 2015 Shift4 Corporation - ALL RIGHTS RESERVED
*/

//	Ensures there will be no 'console is undefined' errors.
window.console = window.console || (function(){
	var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function(s){};
	return c;
})();
console = console || window.console;


/*!
 * jQuery postMessage - v0.5 - 9/11/2009
 * http://benalman.com/projects/jquery-postmessage-plugin/
 * 
 * Copyright (c) 2009 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery postMessage: Cross-domain scripting goodness
//
// *Version: 0.5, Last updated: 9/11/2009*
// 
// Project Home - http://benalman.com/projects/jquery-postmessage-plugin/
// GitHub       - http://github.com/cowboy/jquery-postmessage/
// Source       - http://github.com/cowboy/jquery-postmessage/raw/master/jquery.ba-postmessage.js
// (Minified)   - http://github.com/cowboy/jquery-postmessage/raw/master/jquery.ba-postmessage.min.js (0.9kb)
// 
// About: License
// 
// Copyright (c) 2009 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// http://benalman.com/about/license/
// 
// About: Examples
// 
// This working example, complete with fully commented code, illustrates one
// way in which this plugin can be used.
// 
// Iframe resizing - http://benalman.com/code/projects/jquery-postmessage/examples/iframe/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with and what browsers it has been tested in.
// 
// jQuery Versions - 1.3.2
// Browsers Tested - Internet Explorer 6-8, Firefox 3, Safari 3-4, Chrome, Opera 9.
// 
// About: Release History
// 
// 0.5 - (9/11/2009) Improved cache-busting
// 0.4 - (8/25/2009) Initial release

(function($){
  '$:nomunge'; // Used by YUI compressor.
  
  // A few vars used in non-awesome browsers.
  var interval_id,
    last_hash,
    cache_bust = 1,
    
    // A var used in awesome browsers.
    rm_callback,
    
    // A few convenient shortcuts.
    window = this,
    FALSE = !1,
    
    // Reused internal strings.
    postMessage = 'postMessage',
    addEventListener = 'addEventListener',
    
    p_receiveMessage,
    
    // I couldn't get window.postMessage to actually work in Opera 9.64!
    has_postMessage = window[postMessage];	// && !$.browser.opera;
  
  // Method: jQuery.postMessage
  // 
  // This method will call window.postMessage if available, setting the
  // targetOrigin parameter to the base of the target_url parameter for maximum
  // security in browsers that support it. If window.postMessage is not available,
  // the target window's location.hash will be used to pass the message. If an
  // object is passed as the message param, it will be serialized into a string
  // using the jQuery.param method.
  // 
  // Usage:
  // 
  // > jQuery.postMessage( message, target_url [, target ] );
  // 
  // Arguments:
  // 
  //  message - (String) A message to be passed to the other frame.
  //  message - (Object) An object to be serialized into a params string, using
  //    the jQuery.param method.
  //  target_url - (String) The URL of the other frame this window is
  //    attempting to communicate with. This must be the exact URL (including
  //    any query string) of the other window for this script to work in
  //    browsers that don't support window.postMessage.
  //  target - (Object) A reference to the other frame this window is
  //    attempting to communicate with. If omitted, defaults to `parent`.
  // 
  // Returns:
  // 
  //  Nothing.
  
  $[postMessage] = function( message, target_url, target ) {
    if ( !target_url ) { return; }
    
    // Serialize the message if not a string. Note that this is the only real
    // jQuery dependency for this script. If removed, this script could be
    // written as very basic JavaScript.
    message = typeof message === 'string' ? message : $.param( message );
    
    // Default to parent if unspecified.
    target = target || parent;
    
    if ( has_postMessage ) {
      // The browser supports window.postMessage, so call it with a targetOrigin
      // set appropriately, based on the target_url parameter.
      target[postMessage]( message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ) );
      
    } else if ( target_url ) {
      // The browser does not support window.postMessage, so set the location
      // of the target to target_url#message. A bit ugly, but it works! A cache
      // bust parameter is added to ensure that repeat messages trigger the
      // callback.
      target.location = target_url.replace( /#.*$/, '' ) + '#' + (+new Date) + (cache_bust++) + '&' + message;
    }
  };
  
  // Method: jQuery.receiveMessage
  // 
  // Register a single callback for either a window.postMessage call, if
  // supported, or if unsupported, for any change in the current window
  // location.hash. If window.postMessage is supported and source_origin is
  // specified, the source window will be checked against this for maximum
  // security. If window.postMessage is unsupported, a polling loop will be
  // started to watch for changes to the location.hash.
  // 
  // Note that for simplicity's sake, only a single callback can be registered
  // at one time. Passing no params will unbind this event (or stop the polling
  // loop), and calling this method a second time with another callback will
  // unbind the event (or stop the polling loop) first, before binding the new
  // callback.
  // 
  // Also note that if window.postMessage is available, the optional
  // source_origin param will be used to test the event.origin property. From
  // the MDC window.postMessage docs: This string is the concatenation of the
  // protocol and "://", the host name if one exists, and ":" followed by a port
  // number if a port is present and differs from the default port for the given
  // protocol. Examples of typical origins are https://example.org (implying
  // port 443), http://example.net (implying port 80), and http://example.com:8080.
  // 
  // Usage:
  // 
  // > jQuery.receiveMessage( callback [, source_origin ] [, delay ] );
  // 
  // Arguments:
  // 
  //  callback - (Function) This callback will execute whenever a <jQuery.postMessage>
  //    message is received, provided the source_origin matches. If callback is
  //    omitted, any existing receiveMessage event bind or polling loop will be
  //    canceled.
  //  source_origin - (String) If window.postMessage is available and this value
  //    is not equal to the event.origin property, the callback will not be
  //    called.
  //  source_origin - (Function) If window.postMessage is available and this
  //    function returns false when passed the event.origin property, the
  //    callback will not be called.
  //  delay - (Number) An optional zero-or-greater delay in milliseconds at
  //    which the polling loop will execute (for browser that don't support
  //    window.postMessage). If omitted, defaults to 100.
  // 
  // Returns:
  // 
  //  Nothing!
  
  $.receiveMessage = p_receiveMessage = function( callback, source_origin, delay ) {
    if ( has_postMessage ) {
      // Since the browser supports window.postMessage, the callback will be
      // bound to the actual event associated with window.postMessage.
      
      if ( callback ) {
        // Unbind an existing callback if it exists.
        rm_callback && p_receiveMessage();
        
        // Bind the callback. A reference to the callback is stored for ease of
        // unbinding.
        rm_callback = function(e) {
          if ( ( typeof source_origin === 'string' && e.origin !== source_origin )
            || ( $.isFunction( source_origin ) && source_origin( e.origin ) === FALSE ) ) {
            return FALSE;
          }
          callback( e );
        };
      }
      
      if ( window[addEventListener] ) {
        window[ callback ? addEventListener : 'removeEventListener' ]( 'message', rm_callback, FALSE );
      } else {
        window[ callback ? 'attachEvent' : 'detachEvent' ]( 'onmessage', rm_callback );
      }
      
    } else {
      // Since the browser sucks, a polling loop will be started, and the
      // callback will be called whenever the location.hash changes.
      
      interval_id && clearInterval( interval_id );
      interval_id = null;
      
      if ( callback ) {
        delay = typeof source_origin === 'number'
          ? source_origin
          : typeof delay === 'number'
            ? delay
            : 100;
        
        interval_id = setInterval(function(){
          var hash = document.location.hash,
            re = /^#?\d+&/;
          if ( hash !== last_hash && re.test( hash ) ) {
            last_hash = hash;
            callback({ data: hash.replace( re, '' ) });
          }
        }, delay );
      }
    }
  };
  
})(jQuery);

/*
 * END OF jQuery postMessage
*/


/*
 *	Shift4 i4Go stuff starts here.
*/
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

$.extend($.fn, {
	i4goTrueToken: function( options ) {

		// if nothing is selected, return nothing; can't chain anyway
		if ( !this.length ) {
			console.warn( "(i4goTrueToken) Nothing selected; can't create iframe and linkage; returning nothing." );
			return;
		}

		// check if a validator for this form was already created
		var i4goTrueTokenFramer = $.data( this[ 0 ], "i4goTrueTokenFramer" );
		if ( i4goTrueTokenFramer ) {
			return i4goTrueTokenFramer;
		}

		i4goTrueTokenFramer = new $.i4goTrueTokenFramer( options, this[ 0 ] );
		$.data( this[ 0 ], "i4goTrueTokenFramer", i4goTrueTokenFramer );

		return i4goTrueTokenFramer;
	}
});

// constructor for validator
$.i4goTrueTokenFramer = function( options, form ) {
	this.settings = $.extend( true, {}, $.i4goTrueTokenFramer.defaults, options );
	this.form = form;
	this.init();
};

// the beast
$.extend( $.i4goTrueTokenFramer, {

	defaults: {
		server:						"",							// REQUIRED from access.i4go.com call (or i4access.shift4test.com)
		accessBlock:				"",							// REQUIRED from access.i4go.com call (or i4access.shift4test.com)
		self:						"",							// REQUIRED - the full URL used to located the current page - MUST MATCH EXACTLY INCLUDING QUERY PARAMETERS - document.location should work
		template:					"bootstrap3-horizontal",

		url:						"",							// If not provided (the default), value will be calculated based on the server value provided

		frameContainer:				"i4goFrame",				// Only used if frameName does not exist
		frameName:					"",							// Auto-assigned if left empty
		frameAutoResize:			true,
		frameHeightPadding:			0,
		frameClasses:				"",

		// Auto populated form fields. Precedence: field name, field id
		formPaymentResponse:		"i4go_response",
		formPaymentResponseCode:	"i4go_responsecode",
		formPaymentResponseText:	"i4go_responsetext",
		formPaymentMaskedCard:		"i4go_maskedcard",
		formPaymentToken:			"i4go_uniqueid",
		formPaymentExpMonth:		"i4go_expirationmonth",
		formPaymentExpYear:			"i4go_expirationyear",
		formPaymentType:			"i4go_cardtype",
		formAutoSubmitOnSuccess:	false,
		formAutoSubmitOnFailure:	false,

		// Events
		onSuccess:					function( form,data ){
										console.log("(i4goTrueToken) onSuccess()",data);
									},
		onFailure:					function( form,data ){
										console.warn("(i4goTrueToken) onFailure()",data);
									},
		onComplete:					function( form,data ) {
										console.log("(i4goTrueToken) onComplete()",data);
									},

		acceptedPayments:			"AX,DC,GC,JC,MC,NS,VS",

		// card swipe capabilities
		encryptedOnlySwipe:			true,

		// Debug flags: Simply creates additional console log messages so a debugger needs to be running to view. In no event does CHD get logged.
		debug:						false,		// Parent side
		remoteDebug:				false		// Adds width indicators within the iframe contents to help with frame sizing
	},

	// http://jqueryvalidation.org/jQuery.validator.setDefaults/
	setDefaults: function( settings ) {
		$.extend( $.i4goTrueTokenFramer.defaults, settings );
		if ($.i4goTrueTokenFramer.defaults.debug) {
			console.log("(i4goTrueToken) setDefaults() called.");
		}
	},

	prototype: {

		init: function() {
			var myself=this,
				err=[];

			if (this.settings.debug) {
				console.log("(i4goTrueToken) init()",this.settings);
			}

			if(!this.settings.frameContainer.trim().length) {
				var msg="Usage Error: (i4goTrueToken) frameContainer name not provided.";
				console.error(msg);
				err.push(msg);
			}

			if(this.settings.frameContainer.trim().length) {
				this.container=$("#"+this.settings.frameContainer.trim());
				if (!this.container.length) {
					// container not provided so we'll prepend it to the form -- hopefully form is located in the correct position on the page!
					if (this.settings.debug) {
						console.log("(i4goTrueToken) iframe container div '"+this.settings.frameContainer+"' not provided -- inserting a default container into form.");
					}
					$(this.currentForm).prepend("<div id='"+this.settings.frameContainer+"'></div>");
					this.container=$("#"+this.settings.frameContainer.trim());
				}
				if (!this.container.length) {
					var msg="Usage Error: (i4goTrueToken) iframe container div '"+this.settings.frameContainer+"' not provided";
					console.error(msg);
					err.push(msg);
				}
			}
			if (!this.settings.server.trim().length) {
				var msg="Usage Error: (i4goTrueToken) server not provided";
				console.error(msg);
				err.push(msg);
			}
			if (!this.settings.accessBlock.trim().length) {
				var msg="Usage Error: (i4goTrueToken) accessBlock not provided";
				console.error(msg);
				err.push(msg);
			}
			if(this.settings.self.href && (typeof this.settings.self.href==='string')) {
				// set to string property (IE fix)
				this.settings.self = this.settings.self.href;
			}
			if (!this.settings.self.trim().length) {
				var msg="Usage Error: (i4goTrueToken) self not provided";
				console.error(msg);
				err.push(msg);
			}
			if (err.length) {
				// can't do anything more for this poor unfortunate sole -- simply return.
				alert(err.toString());
				return;
			}
			
			if (!this.settings.url.trim().length) {
				// Set url based on value of server
				this.settings.url = "https://i4m.i4go.com";					// default to production
				if (this.settings.server.search(/i4go\d\d\.shift4test\.com/i)>=0) {
					this.settings.url = "https://i4m.shift4test.com";		// testing/certify
				} else
				if (this.settings.server.search(/i4go\d\d\.s4-test\.com/i)>=0) {
					this.settings.url = "https://i4m.s4-test.com";			// internal s4-test
				} else
				if (this.settings.server.search(/wh-i4go\d\d\.shift4\.com/i)>=0) {
					this.settings.url = "https://wh-i4m.shift4.com";		// warthog
				}
				if (myself.settings.debug) {
					console.log("(i4goTrueToken) i4m calculation:",this.settings.url);
				}
			}
			
			// Setup interframe communication.
			this.src =  this.settings.url+"/index.cfm?fuseaction=get.token&i4go_server="+encodeURIComponent(this.settings.server)+"&i4go_accessBlock="+encodeURIComponent(this.settings.accessBlock)+"&i4go_template="+encodeURIComponent(this.settings.template)+"&i4go_self="+encodeURIComponent(this.settings.self);
			$.receiveMessage(
				function(e) {
					if (myself.settings.debug) {
						console.log("(i4goTrueToken) MESSAGE RECEIVED:",e);
					}
					myself.callback(e.data);
				},
				this.settings.url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' )
			);
			
			// setup frame
			var classes=" class='i4go-frame"+(this.settings.frameClasses===""?"":" "+this.settings.frameClasses)+"'";
			this.frameName = "i4goTT_"+this.generateUUID();
			this.container.html("<iframe id='"+this.frameName+"' name='"+this.frameName+"' src='"+this.src+"' frameBorder='0' width='100%' height='100%' scrolling='no' "+classes+"></iframe>");
		},
		
		callback: function(data) {
			if (this.settings.debug) {
				console.log("(i4goTrueToken) callback() BEGIN:",data);
			}
			try {
				data = JSON.parse(data);
				if(data.action) {
					switch(data.action) {
						case 'clear':
							this.clearFormValues();
							break;
						case 'get':
							this.sendFormValuesToFrame();
							break;
						case 'resize':
							this.resize(data);
							break;
						case 'set':
							this.setFormValues(data);
							break;
						case 'settings':
							this.sendSettingsToFrame();
							break;
						case 'test':
							this.test(data);
							break;
						default:
							console.warn("(i4goTrueToken) callback() called with unknown action value: "+data.action);
					}
				}
			} catch(err) {
				console.warn("(i4goTrueToken) callback() error:",err);
			}
			if (this.settings.debug) {
				console.log("(i4goTrueToken) callback() END:",data);
			}
		},
		
		getField: function(name) {
			var str="[name='"+name+"']",
				result=$(this.form).find(str);
			if(!result.length) {
				result=$("#"+name);
			}
			if (this.settings.debug) {
				console.log("(i4goTrueToken) getField('"+str+"')="+(result.length?"found":"NOT FOUND"));
			}
			return result;
		},

		clear: function() {
			var frame = $("#"+this.frameName)[0],
				data = JSON.stringify({
					action:	'clear'
				});
			if (this.settings.debug) {
				console.log("(i4goTrueToken) clear():",data);
			}
			$.postMessage(data,this.src,frame.contentWindow);
			this.clearFormValues();
		},

		clearFormValues: function() {
			if (this.settings.debug) {
				console.log("(i4goTrueToken) clearFormValues()");
			}
			this.getField(this.settings.formPaymentResponse).val("");
			this.getField(this.settings.formPaymentResponseCode).val("");
			this.getField(this.settings.formPaymentResponseText).val("");
			this.getField(this.settings.formPaymentToken).val("");
			this.getField(this.settings.formPaymentExpMonth).val("");
			this.getField(this.settings.formPaymentExpYear).val("");
			this.getField(this.settings.formPaymentType).val("");
		},

		generateUUID: function() {
			var d = new Date().getTime();
			var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = (d + Math.random()*16)%16 | 0;
				d = Math.floor(d/16);
				return (c=='x' ? r : (r&0x3|0x8)).toString(16);
			});
			return uuid;
		},

		propExists: function(obj, prop) {
			var parts = prop.split('.');
			for(var i = 0, l = parts.length; i < l; i++) {
				var part = parts[i];
				if(obj !== null && typeof obj === "object" && part in obj) {
					obj = obj[part];
				}
				else {
					return false;
				}
			}
			return true;
		},

		resize: function(data) {
			this.settings.debug && console.log("(i4goTrueToken) resize()",data);
			if(this.settings.frameAutoResize) {
				$("#"+this.frameName).height(data.height+this.settings.frameHeightPadding);
			}
		},

		sendFormValuesToFrame: function() {
			this.settings.debug && console.log("(i4goTrueToken) getFormValues()");
			return {
				i4go_uniqueid:			this.getFieldVal(this.settings.formToken,""),
				i4go_expirationmonth:	this.getFieldVal(this.settings.formExpMonth,""),
				i4go_expirationyear:	this.getFieldVal(this.settings.formExpYear,""),
				i4go_cardtype:			this.getFieldVal(this.settings.formPaymentType,"")
			};
		},

		sendSettingsToFrame: function() {
			var frame = $("#"+this.frameName)[0],
				data = JSON.stringify({
					action:	'settings',
					settings: this.settings
				});
			this.settings.debug && console.log("(i4goTrueToken) sendSettingsToFrame():",data,this.settings);
			$.postMessage(data,this.src,frame.contentWindow);
		},

		// this function expects a cardswipe result structure
		sendSwipeToFrame: function(data) {
			var frame = $("#"+this.frameName)[0],
				data = JSON.stringify({
					action:	'swipe',
					swipe: data
				});
			this.settings.debug && console.log("(i4goTrueToken) sendSwipeToFrame()");
			$.postMessage(data,this.src,frame.contentWindow);
			this.clearFormValues();
		},

		setFormValues: function(data) {
			this.settings.debug && console.log("(i4goTrueToken) setFormValues()",data);
			// Default all missing properties.
			var props = [ "i4go_response","i4go_responsecode","i4go_responsetext","i4go_uniqueid","i4go_expirationmonth","i4go_expirationyear","i4go_cardtype" ];
			for(var i = 0, l = props.length; i < l; i++) {
				var prop = props[i];
				if(!this.propExists(data.response,prop)) {
					data.response[prop] = "";
				}
			}
			// Now do the actual setting.
			this.clearFormValues();
			this.getField(this.settings.formPaymentResponse).val(data.response.i4go_response);
			this.getField(this.settings.formPaymentResponseCode).val(data.response.i4go_responsecode);
			this.getField(this.settings.formPaymentResponseText).val(data.response.i4go_responsetext);
			this.getField(this.settings.formPaymentToken).val(data.response.i4go_uniqueid);
			this.getField(this.settings.formPaymentExpMonth).val(data.response.i4go_expirationmonth);
			this.getField(this.settings.formPaymentExpYear).val(data.response.i4go_expirationyear);
			this.getField(this.settings.formPaymentType).val(data.response.i4go_cardtype);
			if(data.response.i4go_uniqueid.length>=4) {
				this.getField(this.settings.formPaymentMaskedCard).val("********"+data.response.i4go_uniqueid.substring(0,4));
			}

			if(data.response.i4go_response==="SUCCESS") {
				if(this.settings.onSuccess) {
					this.settings.onSuccess(this.form,data.response);
				}
				if(this.settings.onComplete) {
					this.settings.onComplete(this.form,data.response);
				}
				if(this.settings.formAutoSubmitOnSuccess) {
					this.form.submit();
				}
			}
			if(data.response.i4go_response==="FAILURE") {
				if(this.settings.onFailure) {
					this.settings.onFailure(this.form,data.response);
				}
				if(this.settings.onComplete) {
					this.settings.onComplete(this.form,data.response);
				}
				if(this.settings.formAutoSubmitOnFailure) {
					this.form.submit();
				}
			}
		},

		test: function() {
			console.log("(i4goTrueToken) test()");
			console.log("arguments.length="+arguments.length);
			for(var i=0; i<arguments.length; i++) {
				console.log("arguments["+i+"]="+arguments[i]);
			}
			this.clearFormValues();
			this.setFormValues({
				i4go_response:			"SUCCESS",
				i4go_responsecode:		"1",
				i4go_uniqueid:			"2222xxxxxxxxxxxx",
				i4go_expirationmonth:	"12",
				i4go_expirationyear:	"2020",
				i4go_cardtype:			"VS"
			});
		}
	}

});

}));

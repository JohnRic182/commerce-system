// A jQuery plugin to detect magnetic card swipes.  Requires a card reader that simulates a keyboard.
// This expects a card that encodes data on track 1, though it also reads tracks 2 and 3.  Most cards
// use track 1.  This won't recognize cards that don't use track 1, or work with a reader that
// doesn't read track 1.
//
// See http://en.wikipedia.org/wiki/Magnetic_card to understand the format of the data on a card.
//
// Uses pattern at https://github.com/umdjs/umd/blob/master/jqueryPlugin.js to declare
// the plugin so that it works with or without an AMD-compatible module loader, like RequireJS.
//
// NOTE: This version has been modified heavily to support track 1 and 2 (not just 1 only) as well as
// P2PE swipe readers.

/*
	IDTech M130 NOTE: To set the pre-amble and post-amble, use the following which is confusing in the 
	doc:
	
	Show all non-defaults
	32
	
	Get pre-amble
	52 D2
	
	Set pre-amble - %*
	53 D2 03 02 25 2A
	
	Clear pre-amble
	53 D2 01 00

	Get post-amble
	52 D3

	Set post-amble - \r
	53 D3 02 01 0D
	
	Clear post-amble
	53 D3 01 00
*/

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

	// State definitions:
	var states = { IDLE: 0, PENDING: 1, READING: 1, DISCARD: 2 };

	// State names used when debugging.
	var stateNames = { 0: 'IDLE', 1: 'READING', 2: 'DISCARD' };

	// Holds current state. Update only through state function.
	var currentState = states.IDLE;
	
	// Gets or sets the current state.
	var state = function() {

		if (arguments.length > 0) {
			// Set new state
			var newState = arguments[0];
			if (newState == state)
				return;

			settings.debug && console.log("%s -> %s", stateNames[currentState], stateNames[newState]);
			settings.debugElement && settings.debugElement.append(stateNames[currentState] + ' -> ' + stateNames[newState]);

			currentState = newState;
		}
		else {
			// Get current state
			return currentState;
		}

	};

	// Array holding scanned characters
	var scanbuffer;

	// Array holding scanned characters based on a timing block - P2PE support
	var p2pe_scanbuffer;
	var p2pe_holdobj;
	var p2pe_holdobjval;
	var p2pe_lasttime = 0;
	
	// Interdigit timer
	var timerHandle = 0;

	// Keypress listener
	var listener = function (e) {
		var processed = false;
		settings.debug && console.log(e.which + ': ' + String.fromCharCode(e.which));
		processP2PE(e.which);
		switch (state()) {

			// IDLE: Look for '%', and jump to PENDING.  Otherwise, pass the keypress through.
			case states.IDLE:
				// Look for '%'
				if ((e.which == 37) || (e.which == 59)) {
					state(states.READING);
					scanbuffer = new Array();
					processCode(e.which);
					e.preventDefault();
					e.stopPropagation();
					startTimer();
				}
				break;

			// READING: Copy characters to buffer until newline, then process the scanned characters
			case states.READING:
				processCode(e.which);
				startTimer();
				e.preventDefault();
				e.stopPropagation();

				// Carriage return indicates end of scan
				if (e.which == 13) {
					clearTimer();
					state(states.IDLE);
					processed = true;
					processScan();
				}

				if (settings.firstLineOnly && e.which == 63) {
					// End of line 1.  Return early, and eat remaining characters.
					state(states.DISCARD);
					processed = true;
					processScan();
				}
				break;

			// DISCARD: Eat up characters until newline, then jump to IDLE
			case states.DISCARD:
				e.preventDefault();
				e.stopPropagation();
				if (e.which == 13) {
					clearTimer();
					clearP2PE();
					state(states.IDLE);
					return;

				}
				startTimer();
				break;

		}
		if(!processed && (e.which == 13)) {
			settings.debug && console.log("P2PE check");
			if(isValidP2PE(p2pe_scanbuffer.join(''))) {
				clearTimer();
				state(states.IDLE);
				processed = true;
				processScan();
			}
		}

	};

	// Converts a scancode to a character and appends it to the buffer.
	var processP2PE = function (code) {
		var now = new Date().getTime();
		if((p2pe_lasttime + settings.interdigitTimeout) < now) {
			settings.debug && p2pe_scanbuffer.length && console.log("P2PE timeout");
			clearP2PE();
		}
		p2pe_scanbuffer.push(String.fromCharCode(code));
		p2pe_lasttime = now;
	};

	var clearP2PE = function () {
		p2pe_focusobj = $(':focus');
		p2pe_focusobjval = p2pe_focusobj.val();
		p2pe_scanbuffer = new Array();
	};
	
	var isValidP2PE = function(rawData) {
		return parseP2PE(rawData).valid;
	}

	var parseP2PE = function(rawData) {
		var p1 = /^%\*/;
		if(p1.exec(rawData)) {
			settings.debug && p2pe_scanbuffer.length && console.log("Preamble found - stripping");
			rawData = rawData.substr(2,rawData.length);
		}
		var result = createCardResult();
		settings.debug && console.log("P2PE parsing: %s", rawData);
		if(!result.valid) {
			parseP2PE_idtech(rawData,result);
		}
		return result;
	}
	
	var parseP2PE_idtech = function(rawData,result) {
		var idtech = /^02[0-9A-F]{4}/i,
			p1 = /^%\*/i,
			t1 = /%\*\d{4,6}\**\d{4}[\^][^\^]*[\^]\d{4}[^\?\%\;]*[\?]?/i,
			t2 = /;\d{4,6}\**\d{4}\=\d{4}[^\?\%\;]*[\?]?/i,
			found, len, a;
		found = idtech.exec(rawData);
		if(found) {
			len = parseInt(rawData.substr(4,2)+rawData.substr(2,2),16);
			if((rawData.length>=(len+12)) && rawData.substr(len+10,2)=='03') {
				rawData = rawData.substr(0,len+12);

				// track 1
				found = t1.exec(rawData);
				if(found) {
					result.track1 = found[0];
					a = result.track1.split('^');
					result.maskedCardNumber = a[0].substr(2,20);
					result.expirationYear = parseInt(a[2].substr(0,2));
					result.expirationMonth = parseInt(a[2].substr(2,2));
					result.cardholderName = a[1];
					result.valid = true;
				}

				// track 2
				found = t2.exec(rawData);
				if(found) {
					result.track2 = found[0];
					a = result.track2.split('=');
					result.maskedCardNumber = a[0].substr(1,20);
					result.expirationYear = parseInt(a[1].substr(0,2));
					result.expirationMonth = parseInt(a[1].substr(2,2));
					result.valid = true;
				}
				if((result.expirationYear>0) || (result.expirationMonth>0)) {
					if(((result.expirationMonth==0) || (result.expirationMonth>12)) && (result.expirationYear>=1) && (result.expirationYear<=12)) {
						// swap the two
						var m = result.expirationYear;
						result.expirationYear = result.expirationMonth;
						result.expirationMonth = m;
					}
					result.expirationYear += 2000;
				}

				// mask the card information consistently
				if(result.maskedCardNumber) {
					var re = /^(\d{4}).*(\d{4})$/;
					result.maskedCardNumber = result.maskedCardNumber.replace(re,"$1********$2");
				}

				if(result.valid) {
					result.device = 'idtech';
					result.encrypted = rawData;
					settings.debug && console.log("P2PE IDTech format found:", result);
				}
			}
		}
	}

	// Converts a scancode to a character and appends it to the buffer.
	var processCode = function (code) {
		scanbuffer.push(String.fromCharCode(code));
	}

	var startTimer = function () {
		clearTimeout(timerHandle);
		timerHandle = setTimeout(onTimeout, settings.interdigitTimeout);
	};

	var clearTimer = function () {
		clearTimeout(timerHandle);
		timerHandle = 0;
	};

	// Invoked when the timer lapses.
	var onTimeout = function () {
		settings.debug && console.log('Timeout!');
		if (state() == states.READING) {
			processScan();
		}
		clearP2PE();
		scanbuffer = null;
		state(states.IDLE);
	};

	// common result structure used by parsers
	var createCardResult = function() {
		return {
			valid: false,
			device: 'unknown',
			track1: null,
			track2: null,
			encrypted: null,
			maskedCardNumber: null,
			expirationMonth: 0,
			expirationYear: 0,
			cardholderName: null
		};
	}

	// Processes the scanned card
	var processScan = function () {
		var parsedData, rawData;

		clearTimer();
		state(states.IDLE);

		settings.debug && console.log('p2pe_scanbuffer: ', p2pe_scanbuffer);
		rawData = p2pe_scanbuffer.join('');
		parsedData = parseP2PE(rawData);
		if(parsedData.valid) {
			p2pe_focusobj.val(p2pe_focusobjval);
			clearP2PE();
		} else {
			// Invoke client parser and callbacks -- client parser only has access to non-P2PE data
			settings.debug && console.log('scanbuffer: ', scanbuffer);
			rawData = scanbuffer.join('');
			parsedData = settings.parser.call(this, rawData);
		}

		if (parsedData && parsedData.valid) {
			settings.success && settings.success.call(this, parsedData);
		}
		else {
			settings.error && settings.error.call(this, rawData);
		}
	};

	// Binds the event listener
	var bindListener = function () {
		$(document).bind("keypress.cardswipe", listener);
	};

	// Unbinds the event listener
	var unbindListener = function () {
		$(document).unbind(".cardswipe");
	};

	// Default parser. Separates raw data into up to three lines
	var defaultParser = function (rawData) {
		var t1 = /%B[\d\s]+[\^][^\^]*[\^]\d{4}[^\?\%\;]*[\?]?/i,
			t2 = /;\d+\=\d{4}\d+[\?]?/i,
			p1 = /%\*/,
			found, a,
			cardData = createCardResult();

		// p2pe first
		clearP2PE();
		if(p1.exec(rawData)) {
			settings.debug && console.log("Preamble found - parsing as P2PE");
			cardData = parseP2PE(rawData);
		}
		if(!cardData.valid) {
			// now the non-safe stuff -- non-p2pe
			found = t1.exec(rawData);
			if (found) {
				cardData.track1 = found[0];
				cardData.valid = true;
				a = cardData.track1.split('^');
				cardData.maskedCardNumber = a[0].substr(2,20);
				cardData.expirationYear = parseInt(a[2].substr(0,2));
				cardData.expirationMonth = parseInt(a[2].substr(2,2));
				cardData.cardholderName = a[1];
				settings.debug && console.log("Track 1 found");
			}
	
			found = t2.exec(rawData);
			if (found) {
				cardData.track2 = found[0];
				cardData.valid = true;
				a = cardData.track2.split('=');
				cardData.maskedCardNumber = a[0].substr(1,20);
				cardData.expirationYear = parseInt(a[1].substr(0,2));
				cardData.expirationMonth = parseInt(a[1].substr(2,2));
				settings.debug && console.log("Track 2 found");
			}

			if((cardData.expirationYear>0) || (cardData.expirationMonth>0)) {
				if(((cardData.expirationMonth==0) || (cardData.expirationMonth>12)) && (cardData.expirationYear>=1) && (cardData.expirationYear<=12)) {
					// swap the two
					var m = cardData.expirationYear;
					cardData.expirationYear = cardData.expirationMonth;
					cardData.expirationMonth = m;
				}
				cardData.expirationYear += 2000;
			}

			// mask the card information
			if(cardData.maskedCardNumber) {
				var re = /^(\d{4}).*(\d{4})$/;
				cardData.maskedCardNumber = cardData.maskedCardNumber.replace(re,"$1********$2");
			}
		}

		return cardData.valid ? cardData : null;
	};

	// Default callback used if no other specified. Works with default parser.
	var defaultSuccessCallback = function (cardData) {
		var text = ['Success!\Track 1: ', cardData.track1, '\nTrack 2: ', cardData.track2, '\nTrack 3: ', cardData.track3].join('');
		alert(text);
	};

	// Defaults for settings
	var defaults = {
		enabled: true,
		interdigitTimeout: 50,
		success: defaultSuccessCallback,
		error: null,
		parser: defaultParser,
		firstLineOnly: false,
		debug: false,
	};

	// Plugin actual settings
	var settings;


	// Callable plugin methods
	var methods = {
		init: function (options) {
			settings = $.extend(defaults, options || {});
			clearP2PE();
			if (settings.enabled)
				methods.enable();
		},

		disable: function (options) {
			unbindListener();
		},

		enable: function (options) {
			bindListener();
		}
	};


	// The extension proper.  Dispatches methods using the usual jQuery pattern.
	$.cardswipe = function (method) {
		// Method calling logic. If named method exists, execute it with passed arguments
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		// If no argument, or an object passed, invoke init method.
		else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			throw 'Method ' + method + ' does not exist on jQuery.cardswipe';
		}
	}

}));

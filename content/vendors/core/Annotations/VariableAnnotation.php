<?php

class core_Annotations_VariableAnnotation extends core_Annotations_Annotation
{
	private $type;
	private $variableName;

	public function getType()
	{
		return $this->type;
	}
	public function setType($type)
	{
		$this->type = rtrim($type, '*');
	}

	public function getVariableName()
	{
		return $this->variableName;
	}
	public function setVariableName($variableName)
	{
		$this->variableName = rtrim(ltrim($variableName, '$'), '*');
	}

	public function parse($line, $reflectionObj = null)
	{
		$lineParts = explode(' ', $line);

		$partsCount = count($lineParts);

		if (count($lineParts) > 0)
		{
			$this->setType(trim($lineParts[0]));
		}
		if (count($lineParts) > 1)
		{
			$this->setVariableName(trim($lineParts[1]));
		}

		if (is_null($this->type) || $this->type == '')
		{
			$this->type = 'string';
		}

		if ((is_null($this->variableName) || $this->variableName == '') && is_a($reflectionObj, 'ReflectionProperty'))
		{
			$this->setVariableName($reflectionObj->getName());
		}
	}

	public function getName()
	{
		return 'var';
	}
}
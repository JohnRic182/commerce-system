<?php

class core_Annotations_AnnotationReader
{
	protected static $attributesMap = array(
		'var' => 'core_Annotations_VariableAnnotation',
		'Permission' => 'core_Annotations_PermissionAnnotation',
		'Api' => 'core_Annotations_ApiAnnotation',
	);

	public static function registerAnnotation($name, $type)
	{
		if (!key_exists($name, self::$attributesMap))
		{
			self::$attributesMap[$name] = $type;
		}
	}

	public function getClassAnnotations(ReflectionClass $class)
	{
		return $this->parseDocComment($class->getDocComment(), $class);
	}

	public function getClassAnnotation(ReflectionClass $class, $name)
	{
		$annotations = $this->getClassAnnotations($class);

		foreach ($annotations as $annotationName => $annotation)
		{
			if ($annotationName === $name)
				return $annotation;
		}

		return null;
	}

	public function getPropertyAnnotations(ReflectionProperty $property)
	{
		return $this->parseDocComment($property->getDocComment(), $property);
	}

	public function getPropertyAnnotation(ReflectionProperty $property, $name)
	{
		$annotations = $this->getPropertyAnnotations($property);

		foreach ($annotations as $annotationName => $annotation)
		{
			if ($annotationName === $name)
				return $annotation;
		}

		return null;
	}

	public function parseDocComment($docComment, $reflectionObj)
	{
		$annotations = array();

		$index = 0;
		while (($index = strpos($docComment, '@', $index)) > 0)
		{
			$lineEnding = strpos($docComment, '*', $index);

			$line = substr($docComment, $index+1, ($lineEnding - $index));

			$parts = explode(' ', $line);

			$key = trim($parts[0]);
			unset($parts[0]);
			$line = rtrim(trim(implode(' ', $parts)), '*');

			if (isset(self::$attributesMap[$key]))
			{
				$annotation = new self::$attributesMap[$key]();
				$annotation->parse($line, $reflectionObj);
				$annotations[$annotation->getName()] = $annotation;
			}
			if ($lineEnding <= 0)
				break;

			$index = $lineEnding;
		}

		return $annotations;
	}
}
<?php

class core_Annotations_PermissionAnnotation extends core_Annotations_ParameterizedAnnotation
{
	protected $permissions;

	public function getPermissions()
	{
		if (!is_null($this->permissions) || trim($this->permissions) != '')
		{
			return explode(',', $this->permissions);
		}

		return array();
	}

	protected function _setParameter($name, $value)
	{
		$this->$name = $value;
	}

	public function getName()
	{
		return 'Permission';
	}
}
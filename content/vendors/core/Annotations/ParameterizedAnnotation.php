<?php

abstract class core_Annotations_ParameterizedAnnotation extends core_Annotations_Annotation
{
	public function parse($line, $reflectionObj = null)
	{
		$lineParts = $this->parseParameters($line, $reflectionObj);

		if (count($lineParts) > 0)
		{
			foreach ($lineParts as $name => $value)
			{
				$this->_setParameter($name, $value);
			}
		}
	}

	protected function parseParameters($line, $reflectionObj = null)
	{
		$result = array();

		$var_name = '';
		$value = '';
		$in_quotes = false;
		$in_varname = false;
		$in_value = false;
		$last_char = '';
		for ($i = 0; $i < strlen($line); $i++)
		{
			$cur_char = $line[$i];
			$saveValue = false;
			if (trim($cur_char) == '')
			{
				if (!$in_varname && !$in_value)
				{
					continue;
				}
				else if ($in_varname)
				{
					$saveValue = true;
					$value = null;
				}
				else if ($in_value)
				{
					if ($in_quotes)
					{
						$value .= $cur_char;
						continue;
					}
					else
					{
						$saveValue = true;
					}
				}
			}
			else if ($in_value)
			{
				if ($in_quotes === false && ($cur_char == '"' || $cur_char == "'") && $value == '')
				{
					$in_quotes = $cur_char;
				}
				else if ($in_quotes !== false && $cur_char == $in_quotes && $last_char != '\\')
				{
					$saveValue = true;
				}
				else
				{
					$value .= $cur_char;
				}
			}
			else
			{
				 if ($in_varname && $cur_char == '=')
				{
					$in_varname = false;
					$in_value = true;
				}
				else
				{
					$in_varname = true;
					$var_name .= $cur_char;
				}
			}

			if ($saveValue)
			{
				$result[$var_name] = $value;
				$in_quotes = false;
				$var_name = '';
				$value = '';
				$in_varname = false;
				$in_value = false;
				
			}
			$last_char = $cur_char;
		}

		if ($in_value)
		{
			if ($in_quotes)
			{
				throw new core_Annotations_ParametersException("Unmatched quotes in value on field: ".($reflectionObj->getName()));
			}
			else
			{
				$result[$var_name] = $value;
			}
		}
		else if ($in_varname)
		{
			$result[$var_name] = null;
		}

		return $result;
    }

	protected abstract function _setParameter($name, $value);
}
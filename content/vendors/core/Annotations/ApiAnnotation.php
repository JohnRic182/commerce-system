<?php

class core_Annotations_ApiAnnotation extends core_Annotations_ParameterizedAnnotation
{
	protected $name;
	protected $pluralName;
	protected $get;

	public function getEntityName() { return $this->name; }

	public function getEntityPluralName() { return is_null($this->pluralName) || trim($this->pluralName) == '' ? $this->name . 's' : $this->pluralName; }

	public function getGetMethod() { return $this->get; }

	protected function _setParameter($name, $value)
	{
		$this->$name = $value;
	}

	public function getName()
	{
		return 'Api';
	}
}
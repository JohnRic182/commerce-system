<?php

abstract class core_Annotations_Annotation
{
	public final function __construct()
	{
	}

	public abstract function getName();
	public abstract function parse($lineParts, $reflectionObj = null);

	// public function __get($name)
	// {
	// 	throw new BadMethodCallException('Unknown property \''.$name.'\' on annotation \''.get_class($this).'\'.');
	// }

	public function __set($name, $value)
	{
		throw new BadMethodCallException('Unknown property \''.$name.'\' on annotation \''.get_class($this).'\'.');
	}
}
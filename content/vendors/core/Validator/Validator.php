<?php

class core_Validator_Validator
{
	protected $classMetadataFactory;
	protected static $constraintsMap = array(
		'NotNull' => 'core_Validator_Constraint_NotNull',
		'NotBlank' => 'core_Validator_Constraint_NotBlank',
		'MaxLength' => 'core_Validator_Constraint_MaxLength',
	);

	public function __construct()
	{
		$this->classMetadataFactory = new core_MetadataMapping_ClassMetadataFactory();

		foreach (core_Validator_Validator::$constraintsMap as $name => $type)
			core_Annotations_AnnotationReader::registerAnnotation($name, $type);
	}

	public function validate($object, &$messages)
	{
		$class = get_class($object);

		$classMetadata = $this->classMetadataFactory->getClassMetadata($class);

		$messages = array();
		$isValid = true;

		foreach ($classMetadata->getProperties() as $fieldName => $property)
		{
			$fieldMessages = array();
			foreach (self::$constraintsMap as $name => $type)
			{
				$annotation = $property->getAnnotation($name);

				if (!is_null($annotation))
					$annotation->isValid($property->getValue($object), $fieldMessages);
			}

			if (count($fieldMessages) > 0)
			{
				$messages[$fieldName] = $fieldMessages;
				$isValid = false;
			}
		}
		return $isValid;
	}

	public static function registerConstraint($name, $type)
	{
		self::$constraintsMap[$name] = $type;
	}
}
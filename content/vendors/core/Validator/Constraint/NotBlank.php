<?php

class core_Validator_Constraint_NotBlank extends core_Validator_Constraint_Constraint
{
	protected $message = "This value should not be empty";

	public function getName()
	{
		return 'NotBlank';
	}

	public function isValid($value, array &$messages)
	{
		if ($value === false || (empty($value) && $value != '0'))
		{
			$messages[] = $this->message;
			return false;
		}

		return true;
	}
}
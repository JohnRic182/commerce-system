<?php

abstract class core_Validator_Constraint_Constraint extends core_Annotations_ParameterizedAnnotation
{
	protected $message;

	protected function _setParameter($name, $value)
	{
		$this->$name = $value;
	}

	public abstract function isValid($value, array &$messages);
}
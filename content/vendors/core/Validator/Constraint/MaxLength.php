<?php

class core_Validator_Constraint_MaxLength extends core_Validator_Constraint_Constraint
{
	protected $message = "This value is too long. It should have %d characters or less.";
	protected $limit = null;

	public function getName()
	{
		return 'MaxLength';
	}

	public function isValid($value, array &$messages)
	{
		if (!is_null($this->limit) && strlen($value) > $this->limit)
		{
			$messages[] = sprintf($this->message, $this->limit);
			return false;
		}

		return true;
	}
}
<?php

class core_Validator_Constraint_NotNull extends core_Validator_Constraint_Constraint
{
	protected $message = "This value should not be null";

	public function getName()
	{
		return 'NotNull';
	}

	public function isValid($value, array &$messages)
	{
		if ($value === null)
		{
			$messages[] = $this->message;
			return false;
		}

		return true;
	}
}
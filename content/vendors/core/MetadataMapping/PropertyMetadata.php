<?php

class core_MetadataMapping_PropertyMetadata
{
	protected $property;
	protected $name;
	protected $annotations;

	public function __construct($property, $annotations)
	{
		$this->property = $property;
		$this->name = $property->getName();
		$this->annotations = $annotations;
	}

	public function getProperty()
	{
		return $this->property;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getAnnotations()
	{
		return $this->annotations;
	}

	public function getAnnotation($name)
	{
		if (isset($this->annotations[$name]))
			return $this->annotations[$name];
	
		return null;
	}

	public function getValue($object)
	{
		//TODO: PHP 5.3 >:-(
		//$this->property->setAccessible(true);

		return $this->property->getValue($object);
	}

	public function setValue($object, $value)
	{
		//$this->property->setAccessible(true);

		return $this->property->setValue($object, $value);
	}
}
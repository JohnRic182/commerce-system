<?php

class core_MetadataMapping_ClassMetadata
{
	protected $reflectionClass;

	protected $name;

	protected $classAnnotations = array();

	protected  $properties = array();

	public function __construct($class)
	{
		$this->name = $class;

		$this->reflectFields();
	}

	public function getClassName()
	{
		return $this->name;
	}

	public function getAnnotations()
	{
		return $this->classAnnotations;
	}

	public function getAnnotation($name)
	{
		if (isset($this->classAnnotations[$name]))
		{
			return $this->classAnnotations[$name];
		}
		return null;
	}

	public function getProperties()
	{
		return $this->properties;
	}

	protected function reflectFields()
	{
		$reflectionClass = $this->getReflectionClass();

		$reader = new core_Annotations_AnnotationReader();

		$this->classAnnotations = $reader->getClassAnnotations($reflectionClass);

		$properties = $reflectionClass->getProperties();
		foreach ($properties as $property)
		{
			$annotations = $reader->getPropertyAnnotations($property);

			$this->properties[$property->getName()] = new core_MetadataMapping_PropertyMetadata($property, $annotations);
		}
	}

	public function getReflectionClass()
	{
		if (is_null($this->reflectionClass))
		{
			$this->reflectionClass = new ReflectionClass($this->getClassName());
		}

		return $this->reflectionClass;
	}
}
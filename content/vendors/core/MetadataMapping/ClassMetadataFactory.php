<?php

class core_MetadataMapping_ClassMetadataFactory
{
	protected $loadedClasses = array();

	protected $classMetadataType;

	public function __construct($classMetadataType = 'core_MetadataMapping_ClassMetadata')
	{
		$this->classMetadataType = $classMetadataType;
	}

	public function getClassMetadata($class)
	{
		if (!isset($this->loadedClasses[$class]))
		{
			$metadata = new $this->classMetadataType($class);

			//TODO: parent / child merging data

			//TODO: interface merging data

			$this->loadedClasses[$class] = $metadata;
		}

		return $this->loadedClasses[$class];
	}	
}
<?php

class core_Form_TextareaField extends core_Form_Field
{
	protected $wysiwyg = false;

	public function isWysiwyg()
	{
		return $this->wysiwyg;
	}
	
	public function setWysiwyg($wysiwyg)
	{
		$this->wysiwyg = $wysiwyg;
	}

	public function getType()
	{
		return 'textarea';
	}

	public function getCssClass()
	{
		$class = parent::getCssClass();

		if ($this->isWysiwyg())
		{
			$class .= ' wysiwyg';
		}
//		else
//		{
//			$class .= ' no-mce';
//		}

		return $class;
	}

	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'wysiwyg' => false,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_TextareaField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors']
		);
		
		$field->setWysiwyg($options['wysiwyg']);

		return $field;
	}
}
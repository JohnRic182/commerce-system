<?php

/**
 * Class core_Form_Field
 */
abstract class core_Form_Field
{
	protected $name;
	protected $label;
	protected $value;
	protected $required;
	protected $lockable;
	protected $lockName = null;
	protected $locked = false;
	protected $tooltip;
	protected $note;
	protected $readonly = false;
	protected $validators = null;
	protected $placeholder;

	protected $attr;
	protected $wrapperClass;
	protected $errors = null;

	protected $rendered = false;

	/**
	 * core_Form_Field constructor.
	 * @param $name
	 * @param $label
	 * @param null $value
	 * @param bool $required
	 * @param bool $lockable
	 * @param null $lockName
	 * @param null $tooltip
	 * @param null $note
	 * @param array $attr
	 * @param string $wrapperClass
	 * @param bool $readonly
	 * @param null $validators
	 * @param null $errors
	 * @param string $placeholder
	 */
	public function __construct($name, $label, $value = null, $required = false, $lockable = false, $lockName = null, $tooltip = null, $note = null, $attr = array(), $wrapperClass = '', $readonly = false, $validators = null, $errors = null, $placeholder = '')
	{
		$this->setName($name);
		$this->setLabel($label);
		$this->value = $value;
		$this->required = $required;
		$this->lockable = $lockable;
		$this->lockName = $lockName;
		$this->tooltip = $tooltip;
		$this->note = $note;
		$this->attr = $attr;
		$this->wrapperClass = $wrapperClass;
		$this->readonly = $readonly;
		$this->setValidators($validators);
		$this->setErrors($errors);
		$this->placeholder = $placeholder;
	}

	/**
	 * @return bool
	 */
	public function getRendered()
	{
		return $this->rendered;
	}

	/**
	 * @param bool $val
	 */
	public function setRendered($val = true)
	{
		$this->rendered = $val;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param $name
	 * @return $this
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public abstract function getType();

	/**
	 * @return mixed
	 */
	public function getLabel()
	{
		return $this->label;
	}

	/**
	 * @param $label
	 * @return $this
	 */
	public function setLabel($label)
	{
		$this->label = $label;

		return $this;
	}

	/**
	 * @return null
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param $value
	 * @return $this
	 */
	public function setValue($value)
	{
		$this->value = $value;

		return $this;
	}

	/**
	 * @return null
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @param $errors
	 */
	public function bindErrors($errors)
	{
		$this->setErrors($errors);
	}

	/**
	 * @param $errors
	 */
	public function setErrors($errors)
	{
		$this->errors = is_null($errors) ? null : (is_array($errors) ? $errors : array($errors));
	}

	/**
	 * @return bool
	 */
	public function isRequired()
	{
		return $this->required;
	}

	/**
	 * @param $required
	 * @return $this
	 */
	public function setRequired($required)
	{
		$this->required = $required;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isLockable()
	{
		return $this->lockable;
	}

	/**
	 * @param $lockable
	 * @return $this
	 */
	public function setLockable($lockable)
	{
		$this->lockable = $lockable;

		return $this;
	}

	/**
	 * @return null
	 */
	public function getLockName()
	{
		if ($this->lockName) return $this->lockName;

		return $this->name;
	}

	/**
	 * @param $lockName
	 * @return $this
	 */
	public function setLockName($lockName)
	{
		$this->lockName = $lockName;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isLocked()
	{
		return $this->locked;
	}

	/**
	 * @param $locked
	 */
	public function setLocked($locked)
	{
		$this->locked = $locked;
	}

	/**
	 * @return null
	 */
	public function getTooltip()
	{
		return $this->tooltip;
	}

	/**
	 * @param $tooltip
	 * @return $this
	 */
	public function setTooltip($tooltip)
	{
		$this->tooltip = $tooltip;

		return $this;
	}

	/**
	 * @return null
	 */
	public function getNote()
	{
		return $this->note;
	}

	/**
	 * @param $note
	 * @return $this
	 */
	public function setNote($note)
	{
		$this->note = $note;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isReadonly()
	{
		return $this->readonly;
	}

	/**
	 * @param $readonly
	 */
	public function setReadonly($readonly)
	{
		$this->readonly = $readonly;
	}

	/**
	 * @param $validators
	 */
	public function setValidators($validators)
	{
		$this->validators = is_null($validators) ? null : (is_array($validators) ? $validators : preg_split('/[\s,]+/', $validators));;
	}

	/**
	 * @return null
	 */
	public function getValidators()
	{
		return $this->validators;
	}

	/**
	 * @return null|string
	 */
	public function getElementId()
	{
		static $fieldIds = array();

		$id = $this->getAttribute('id');
		if (is_null($id))
		{
			return 'field-' . str_replace(']', '', str_replace('[', '-', $this->getName()));
		}

		return $id;
	}

	/**
	 * @return null|string
	 */
	public function getAttributes()
	{
		$attributes = array();
		
		if (is_array($this->attr))
		{
			foreach ($this->attr as $key => $value)
			{
				if ($key !== 'class' && $key !== 'id') $attributes[] = $key . '="' . $value . '"';
			}
		}
		
		if (!empty($attributes)) return implode(' ', $attributes);

		return null;
	}

	/**
	 * @param $name
	 * @return null
	 */
	public function getAttribute($name)
	{
		if (is_array($this->attr))
		{
			if (array_key_exists($name, $this->attr)) return $this->attr[$name];
		}
		return null;
	}

	/**
	 * @return null|string
	 */
	public function getCssClass()
	{
		$class = $this->getElementId();

		if ($this->getName() != $this->getLockName())$class .= ' field-' . $this->getLockName();
		if ($this->locked) $class .= ' readonly';
		if (!is_null($this->getAttribute('class')))	$class .= ' ' . $this->getAttribute('class');

		return $class;
	}

	/**
	 * @return string
	 */
	public function getWrapperClass()
	{
		return $this->wrapperClass;
	}

	/**
	 * @param $wrapperClass
	 * @return $this
	 */
	public function setWrapperClass($wrapperClass)
	{
		$this->wrapperClass = $wrapperClass;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPlaceholder()
	{
		return $this->placeholder;
	}

	/**
	 * @param $placeholder
	 * @return $this
	 */
	public function setPlaceholder($placeholder)
	{
		$this->placeholder = $placeholder;

		return $this;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$vals = array();

		$vals['value'] = $this->getValue();
		$vals['required'] = $this->isRequired();
		$vals['class'] = $this->getCssClass(); // What happens if they define a class argument in attr?
		$vals['id'] = $this->getElementId();
		$vals['read_only'] = $this->isLocked();
		$vals['attr'] = $this->getAttributes();
		$vals['wrapperClass'] = $this->getWrapperClass();
		$vals['errors'] = $this->getErrors();
		$vals['placeholder'] = $this->getPlaceholder();

		//TODO: Refactor missed named / extra field values
		$vals['name'] = $this->getName(); // => full_name?
		$vals['title'] = $this->getLabel(); // => label
		$vals['label'] = $this->getLabel();
		$vals['locked'] = $this->isLocked(); // Replace with read_only
		$vals['readonly'] = $this->isReadonly();

		//TODO: What to do with app specific values
		$vals['lockable'] = $this->isLockable();
		$vals['lockName'] = $this->getLockName();
		$vals['tooltip'] = $this->getTooltip();
		$vals['note'] = $this->getNote();

		//TODO: Implement missing field values
		$vals['max_length'] = null;
		$vals['pattern'] = null;

		$validators = $this->getValidators();

		$vals['validators'] = is_null($validators) ? '' : implode(' ', $validators);

		return $vals;
	}

	/**
	 * @return string
	 */
	public function getEditorName()
	{
		return 'field-'.$this->getType();
	}

	/**
	 * @return string
	 */
	public function getTemplate()
	{
		return 'templates/elements/form/'.$this->getEditorName().'.html';
	}

	/**
	 * @param $request
	 * @param $files
	 */
	public function bind($request, $files)
	{
		$value = $this->value;
		$fieldName = $this->getName();

		if (isset($request[$fieldName]))
		{
			$value = $request[$fieldName];
		}
		else if (isset($request[str_replace('[]', '', $fieldName)]))
		{
			$value = $request[str_replace('[]', '', $fieldName)];
		}

		$this->bindData($value);
	}

	/**
	 * @param $value
	 */
	public function bindData($value)
	{
		$this->value = $value;
	}

	/**
	 * @return null
	 */
	public function getData()
	{
		return $this->value;
	}

	/**
	 * @return null
	 */
	public function getDataMapper()
	{
		return null;
	}

	/**
	 * @param $options
	 * @return array
	 */
	public static function setDefaults(&$options)
	{
		$defaults = array(
			'name' => '',
			'label' => '',
			'value' => '',
			'required' => false,
			'lockable' => false,
			'lockName' => null,
			'tooltip' => null,
			'note' => null,
			'locked' => false,
			'attr' => array(),
			'wrapperClass' => '',
			'readonly' => false,
			'validators' => null,
			'errors' => null,
			'placeholder' => '',
		);

		$options = array_merge($defaults, $options);

		return $options;
	}
}
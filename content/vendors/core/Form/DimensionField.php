<?php

/**
 * Class core_Form_DimensionField
 */
class core_Form_DimensionField extends core_Form_Field
{
	protected $widthValue;
	protected $lengthValue;
	protected $heightValue;

	/**
	 * @param mixed $heightValue
	 */
	public function setHeightValue($heightValue)
	{
		$this->heightValue = $heightValue;
	}

	/**
	 * @return mixed
	 */
	public function getHeightValue()
	{
		return $this->heightValue;
	}

	/**
	 * @param mixed $lengthValue
	 */
	public function setLengthValue($lengthValue)
	{
		$this->lengthValue = $lengthValue;
	}

	/**
	 * @return mixed
	 */
	public function getLengthValue()
	{
		return $this->lengthValue;
	}

	/**
	 * @param mixed $widthValue
	 */
	public function setWidthValue($widthValue)
	{
		$this->widthValue = $widthValue;
	}

	/**
	 * @return mixed
	 */
	public function getWidthValue()
	{
		return $this->widthValue;
	}

	public function getElementId()
	{
		$id = parent::getElementId();

		return str_replace('field-', '', $id);
	}

	public function toArray()
	{
		$arr = parent::toArray();

		$arr['widthValue'] = $this->getWidthValue();
		$arr['lengthValue'] = $this->getLengthValue();
		$arr['heightValue'] = $this->getHeightValue();

		return $arr;
	}

	public function getType()
	{
		return 'dimension';
	}

	public function bind($request, $files)
	{
		$name = $this->getName();

		$width = isset($request[$name.'_width']) ? $request[$name.'_width'] : null;
		$length = isset($request[$name.'_length']) ? $request[$name.'_length'] : null;
		$height = isset($request[$name.'_height']) ? $request[$name.'_height'] : null;

		$this->setWidthValue($width);
		$this->setLengthValue($length);
		$this->setHeightValue($height);
	}

	public function getDataMapper()
	{
		return new core_Form_DimensionFieldDataMapper();
	}

	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'widthValue' => null,
			'lengthValue' => null,
			'heightValue' => null
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_DimensionField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setWidthValue($options['widthValue']);
		$field->setLengthValue($options['lengthValue']);
		$field->setHeightValue($options['heightValue']);

		return $field;
	}
}

// TODO: move into external file?
class core_Form_DimensionFieldDataMapper implements core_Form_Data_FieldMapper
{

	public function mapDataToForm($data, core_Form_Field $field)
	{
		/** @var core_Form_DimensionField $field */
		if (is_a($field, 'core_Form_DimensionField'))
		{
			$name = $field->getName();

			$methodName = 'get'.strtoupper(substr($name, 0, 1)).substr($name, 1).'Width';

			if (method_exists($data, $methodName))
			{
				$field->setWidthValue(call_user_func(array($data, $methodName)));
			}

			$methodName = 'get'.strtoupper(substr($name, 0, 1)).substr($name, 1).'Length';
			if (method_exists($data, $methodName))
			{
				$field->setLengthValue(call_user_func(array($data, $methodName)));
			}

			$methodName = 'get'.strtoupper(substr($name, 0, 1)).substr($name, 1).'Height';
			if (method_exists($data, $methodName))
			{
				$field->setHeightValue(call_user_func(array($data, $methodName)));
			}
		}
	}

	public function mapFormToData(core_Form_Field $field, &$data)
	{
		if (is_a($field, 'core_Form_DimensionField'))
		{
			/** @var core_Form_DimensionField $field */
			$name = $field->getName();

			$methodName = 'set'.strtoupper(substr($name, 0, 1)).substr($name, 1).'Width';
			if (method_exists($data, $methodName))
			{
				$value = $field->getWidthValue();
				call_user_func(array($data, $methodName), $value);
			}

			$methodName = 'set'.strtoupper(substr($name, 0, 1)).substr($name, 1).'Length';
			if (method_exists($data, $methodName))
			{
				$value = $field->getLengthValue();
				call_user_func(array($data, $methodName), $value);
			}

			$methodName = 'set'.strtoupper(substr($name, 0, 1)).substr($name, 1).'Height';
			if (method_exists($data, $methodName))
			{
				$value = $field->getHeightValue();
				call_user_func(array($data, $methodName), $value);
			}
		}
	}
}
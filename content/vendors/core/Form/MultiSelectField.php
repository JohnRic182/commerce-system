<?php

class core_Form_MultiSelectField extends core_Form_SelectField
{
	public function isMultiple()
	{
		return true;
	}

	public function getType()
	{
		if ($this->isExpanded())
		{
			return 'radio-list';
		}

		return 'multiselect';
	}

	public function getCssClass()
	{
		$class = parent::getCssClass();

		$class .= ' multi';

		return $class;
	}

	public function bindValue($value)
	{
		$this->setValue($value);
	}

	public function getData()
	{
		return $this->getValue();
	}

	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'options' => array(),
			'empty_value' => null,
			'expanded' => false,
			'grid' => false,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_MultiSelectField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors']
		);

		if ($options['empty_value'] != null)
		{
			$keys = array_keys($options['empty_value']);

			$value = $keys[0];
			$label = $options['empty_value'][$value];

			$field->addOption(new core_Form_Option($value, $label));
		}
		$field->setExpanded($options['expanded']);
		$field->addOptions($options['options']);
		$field->setGrid($options['grid']);

		return $field;
	}
}
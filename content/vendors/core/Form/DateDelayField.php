<?php
/**
 * Class core_Form_DateDelayField
 */
class core_Form_DateDelayField extends core_Form_Field
{
	protected $delayUnitOptions = array();

	/**
	 * Set delay unit options
	 *
	 * @param array $delayUnitOptions
	 */
	public function setDelayUnitOptions($delayUnitOptions)
	{
		$this->delayUnitOptions = $delayUnitOptions;
	}

	/**
	 * Get delay unit options
	 *
	 * @return array
	 */
	public function getDelayUnitOptions()
	{
		return $this->delayUnitOptions;
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$arr = parent::toArray();

		$arr['delayUnitOptions'] = $this->getDelayUnitOptions();

		return $arr;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'date-delay';
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_DateDelayField
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'delayUnitOptions' => array()
		);

		$options = array_merge($defaults, $options);

		/** @var core_Form_DateDelayField $field */
		$field = new core_Form_DateDelayField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setDelayUnitOptions($options['delayUnitOptions']);

		return $field;
	}
}
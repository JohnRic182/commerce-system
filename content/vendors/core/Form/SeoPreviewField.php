<?php

class core_Form_SeoPreviewField extends core_Form_Field
{
	/** @var string $seoOptions */
	protected $seoOptions = null;

	/**
	 * @param $name
	 * @param $title
	 * @param null $value
	 * @param bool $required
	 * @param bool $lockable
	 * @param null $lockName
	 * @param null $tooltip
	 * @param null $note
	 * @param array $attr
	 * @param string $wrapperClass
	 * @param bool $readonly
	 * @param null $validators
	 * @param null $errors
	 * @param null $seoOptions
	 */
	public function __construct($name, $title, $value = null, $required = false, $lockable = false, $lockName = null, $tooltip = null, $note = null, $attr = array(), $wrapperClass = '', $readonly = false, $validators = null, $errors = null, $seoOptions = null)
	{
		parent::__construct($name, $title, $value, $required, $lockable, $lockName, $tooltip, $note, $attr, $wrapperClass, $readonly, $validators, $errors);

		$this->setSeoOptions($seoOptions);
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'seopreview';
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$values = parent::toArray();
		$values['seoOptions'] = $this->getSeoOptions();
		return $values;
	}

	/**
	 * @return string
	 */
	public function getSeoOptions()
	{
		return $this->seoOptions;
	}

	/**
	 * @param $seoOptions
	 */
	public function setSeoOptions($seoOptions)
	{
		$this->seoOptions = $seoOptions;
	}

	/**
	 * @param $name
	 * @param array $options
	 * @return core_Form_SeoPreviewField
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		return new core_Form_SeoPreviewField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors'], $options['seoOptions']
		);
	}
}
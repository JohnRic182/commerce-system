<?php

class core_Form_Data_Validator
{
	public function validate($data, &$messages)
	{
		if (is_object($data))
		{
			$validator = new core_Validator_Validator();

			return $validator->validate($data, $messages);
		}

		return true;
	}
}
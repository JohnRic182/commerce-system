<?php

class core_Form_Data_DefaultMapper implements core_Form_Data_MapperInterface
{
	public function mapDataToForm($data, core_Form_Form $form)
	{
		$this->mapDataToFormItems($data, $form->getItems());
	}

	public function mapDataToField($data, core_Form_Field $field)
	{
		$name = $field->getName();

		$customDataMapper = $field->getDataMapper();
		if (!is_null($customDataMapper))
		{
			$customDataMapper->mapDataToForm($data, $field);
		}
		else
		{
			$methodName = 'get'.strtoupper(substr($name, 0, 1)).substr($name, 1);

			if (is_array($data))
			{
				if (isset($data[$name]))
					$field->bindData($data[$name]);
			}
			else if (method_exists($data, $methodName))
			{
				$field->bindData(call_user_func(array($data, $methodName)));
			}
			else
			{
				if (!is_null($data) && is_object($data) && property_exists($data, $name))
				{
					$field->bindData($data->$name);
				}
			}
		}
	}

	public function mapDataToFormItems($data, array $items)
	{
		foreach ($items as $item)
		{
			if (is_a($item, 'core_Form_Field'))
			{
				$this->mapDataToField($data, $item);
			}
			else if (is_a($item, 'core_Form_FieldGroup'))
			{
				$this->mapDataToFormItems($data, $item->getFields());
			}
		}		
	}

	public function mapFormToData(core_Form_Form $form, &$data)
	{
		$this->mapFormItemsToData($form->getItems(), $data);
	}

	public function mapFieldToData(core_Form_Field $field, &$data)
	{
		$name = $field->getName();

		$customDataMapper = $field->getDataMapper();
		if (!is_null($customDataMapper))
		{
			$customDataMapper->mapFormToData($field, $data);
		}
		else
		{
			if (is_object($data))
			{
				$methodName = 'set'.strtoupper(substr($name, 0, 1)).substr($name, 1);

				if (method_exists($data, $methodName))
				{
					$value = $field->getData();
					call_user_func(array($data, $methodName), $value);
				}
				else
				{
					if (property_exists($data, $name))
					{
						$value = $field->getData();
						$data->$name = $value;
					}
				}
			}
			else if (is_array($data))
			{
				$data[$name] = $field->getData();
			}
		}
	}

	public function mapFormItemsToData(array $items, &$data)
	{
		foreach ($items as $item)
		{
			if (is_a($item, 'core_Form_Field'))
			{
				$this->mapFieldToData($item, $data);
			}
			else if (is_a($item, 'core_Form_FieldGroup'))
			{
				$this->mapFormItemsToData($item->getFields(), $data);
			}
		}		
	}
}
<?php

interface core_Form_Data_MapperInterface
{
	function mapDataToForm($data, core_Form_Form $form);

	function mapFormToData(core_Form_Form $form, &$data);

	function mapDataToField($data, core_Form_Field $field);

	function mapFieldToData(core_Form_Field $field, &$data);

	function mapDataToFormItems($data, array $items);

	function mapFormItemsToData(array $items, &$data);
}
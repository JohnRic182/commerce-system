<?php

interface core_Form_Data_FieldMapper
{
	function mapDataToForm($data, core_Form_Field $field);

	function mapFormToData(core_Form_Field $field, &$data);
}
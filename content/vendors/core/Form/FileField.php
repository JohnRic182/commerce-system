<?php
/**
 * Class core_Form_FileField
 */
class core_Form_FileField extends core_Form_Field
{
	/**
	 * @return string
	 */
	public function getType()
	{
		return 'file';
	}

	/**
	 * @return null
	 */
	public function getData()
	{
		return null;
	}

	/**
	 * @param $name
	 * @param array $options
	 * @return core_Form_FileField
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		return new core_Form_FileField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'], 
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors']
		);
	}
}
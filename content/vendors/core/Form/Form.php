<?php

/**
 * Class core_Form_Form
 */
class core_Form_Form
{
	protected $items = array();
	protected $data = null;
	protected $dataMapper = null;
	protected $dataValidator = null;

	protected $bound = false;

	/**
	 * core_Form_Form constructor.
	 * @param null $dataMapper
	 * @param null $dataValidator
	 */
	public function __construct($dataMapper = null, $dataValidator = null)
	{
		$this->dataMapper = $dataMapper;
		$this->dataValidator = $dataValidator;
	}

	/**
	 * @param core_Form_Field $field
	 */
	public function addField(core_Form_Field $field)
	{
		$this->items[$field->getName()] = $field;

		if ($this->dataMapper)
		{
			$this->dataMapper->mapDataToField($this->data, $field);
		}
	}

	/**
	 * @param $name
	 * @return null
	 */
	public function getField($name)
	{
		foreach ($this->items as $item)
		{
			if (is_a($item, 'core_Form_Field') && $item->getName() == $name) return $item;
		}

		return null;
	}

	/**
	 * @param core_Form_FieldGroup $group
	 */
	public function addGroup(core_Form_FieldGroup $group)
	{
		$this->items[] = $group;

		if ($this->dataMapper)
		{
			$this->dataMapper->mapDataToFormItems($this->data, $group->getFields());
		}
	}

	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @return array
	 */
	public function getItemsToRender()
	{
		$temp = array();

		foreach ($this->items as $item)
		{
			if (!$item->getRendered()) $temp[] = $item;
		}
		return $temp;
	}

	/**
	 * @param $lock_fields
	 */
	public function lockForm($lock_fields)
	{
		foreach ($this->items as $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				$field->setLocked(in_array($field->getLockName(), $lock_fields));
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->lockFields($lock_fields);
			}
		}
	}

	/**
	 * @param $tooltips
	 */
	public function setTooltips($tooltips)
	{
		foreach ($this->items as $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				if (isset($tooltips[$field->getName()]))
				{
					$field->setTooltip($tooltips[$field->getName()]);
				}
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->setTooltips($tooltips);
			}
		}
	}

	/**
	 * @return core_Form_Data_Validator|null
	 */
	protected function getDataValidator()
	{
		if (is_null($this->dataValidator))
		{
			$this->dataValidator = new core_Form_Data_Validator();
		}

		return $this->dataValidator;
	}

	/**
	 * @param array $messages
	 * @return bool
	 */
	public function isValid(&$messages = array())
	{
		$isValid = true;

		if ($this->bound && !is_null($this->data))
		{
			$validator = $this->getDataValidator();
			$isValid = $validator->validate($this->data, $messages);

			if (!$isValid)
			{
				$this->setFieldErrors($messages);
			}
		}

		// isValid is not valid on a form that isn't bound
		return $isValid;
	}

	/**
	 * Set fields errors
	 *
	 * @param $messages
	 */
	public function setFieldErrors($messages)
	{
		foreach ($this->items as $fieldName => $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				if (isset($messages[$fieldName]))
				{
					$field->setErrors($messages[$fieldName]);
				}
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->setFieldErrors($messages);
			}
		}
	}

	/**
	 * Set form data
	 *
	 * @param $appData
	 */
	public function setData(&$appData)
	{
		$this->data = &$appData;

		if ($this->dataMapper)
		{
			$this->dataMapper->mapDataToForm($appData, $this);
		}
	}

	/**
	 * @param $data
	 */
	public function getData(&$data)
	{
		if ($this->dataMapper)
		{
			$this->dataMapper->mapFormToData($this, $data);
		}
	}

	/**
	 * @param $errors
	 */
	public function bindErrors($errors)
	{
		/** @var core_Form_Field|core_Form_FieldGroup $field */
		foreach ($this->items as $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				if (isset($errors[$field->getName()]))
				{
					$field->bindErrors($errors[$field->getName()]);
				}
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->bindErrors($errors);
			}
		}
	}

	/**
	 * Bind request
	 *
	 * @param null $request
	 * @param null $files
	 */
	public function bind($request = null, $files = null)
	{
		$post = $request ? $request : $_REQUEST;
		$postedFiles = $files ? $files : $_FILES;

		foreach ($this->items as $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{

				$field->bind($post, $postedFiles);
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->bind($post, $postedFiles);
			}
		}

		if (!is_null($this->data) && $this->dataMapper)
		{
			$this->dataMapper->mapFormToData($this, $this->data);
		}

		$this->bound = true;
	}
}

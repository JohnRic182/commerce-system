<?php

class core_Form_SelectField extends core_Form_Field
{
	protected $options = array();
	protected $expanded = false;
	protected $multiple = false;

	public function isExpanded()
	{
		return $this->expanded;
	}
	
	public function setExpanded($expanded)
	{
		$this->expanded = $expanded;
	}

	public function isMultiple()
	{
		return $this->multiple;
	}
	
	public function setMultiple($multiple)
	{
		$this->multiple = $multiple;
	}

	public function getOptions()
	{
		return $this->options;
	}
	
	public function addOption($option)
	{
		$this->options[] = $option;
	}
	
	public function addOptions($options)
	{
		foreach ($options as $value => $option)
		{
			if (is_a($option, 'core_Form_Option') || is_a($option, 'OptionNode'))
			{
				$this->addOption($option);
			}
			else
			{
				$this->addOption(new core_Form_Option($value, $option));
			}
		}
			
	}

	public function getValues()
	{
		if ($this->value !== null)
		{
			if ($this->isMultiple())
			{
				return $this->value;
			}
			else
			{
				return array($this->value);
			}
		}

		return array();
	}

	public function getType()
	{
		if ($this->isExpanded())
		{
			return 'radio-list';
		}

		return 'select';
	}

	protected $grid = false;
	public function isGrid()
	{
		return $this->grid;
	}
	public function setGrid($grid)
	{
		$this->grid = $grid;
	}

	public function toArray()
	{
		$ret = parent::toArray();

		$ret['field'] = $this;

		$ret['options'] = $this->getOptions();
		$ret['values'] = $this->getValues();
		$ret['multiple'] = $this->isMultiple();

		$ret['grid'] = $this->isGrid();

		return $ret;
	}

	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'options' => array(),
			'empty_value' => null,
			'expanded' => false,
			'multiple' => false,
			'attr' => array(),
			'grid' => false,
		);

		$options = array_merge($defaults, $options);
		
		$field = new core_Form_SelectField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors']
		);

		if ($options['empty_value'] != null)
		{
			$keys = array_keys($options['empty_value']);

			$value = $keys[0];
			$label = $options['empty_value'][$value];

			$field->addOption(new core_Form_Option($value, $label));
		}
		$field->addOptions($options['options']);

		$field->setExpanded($options['expanded']);
		$field->setMultiple($options['multiple']);
		$field->setGrid($options['grid']);

		return $field;
	}
}
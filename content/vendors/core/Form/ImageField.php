<?php
/**
 * Class core_Form_ImageField
 */
class core_Form_ImageField extends core_Form_Field
{
	/** @var string $imageDeleteUrl */
	protected $imageDeleteUrl = null;

	/**
	 * core_Form_ImageField constructor.
	 * @param $name
	 * @param $title
	 * @param null $value
	 * @param bool $required
	 * @param bool $lockable
	 * @param null $lockName
	 * @param null $tooltip
	 * @param null $note
	 * @param array $attr
	 * @param string $wrapperClass
	 * @param bool $readonly
	 * @param null $validators
	 * @param null $errors
	 * @param null $imageDeleteUrl
	 */
	public function __construct($name, $title, $value = null, $required = false, $lockable = false, $lockName = null, $tooltip = null, $note = null, $attr = array(), $wrapperClass = '', $readonly = false, $validators = null, $errors = null, $imageDeleteUrl = null)
	{
		parent::__construct($name, $title, $value, $required, $lockable, $lockName, $tooltip, $note, $attr, $wrapperClass, $readonly, $validators, $errors);

		$this->setImageDeleteUrl($imageDeleteUrl);
	}

	/**
	 * @param $imageDeleteUrl
	 */
	public function setImageDeleteUrl($imageDeleteUrl)
	{
		$this->imageDeleteUrl = $imageDeleteUrl;
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$values = parent::toArray();

		$values['imageDeleteUrl'] = $this->getImageDeleteUrl();

		return $values;
	}

	/**
	 * @return string
	 */
	public function getImageDeleteUrl()
	{
		return $this->imageDeleteUrl;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'image';
	}

	/**
	 * @return null
	 */
	public function getData()
	{
		return null;
	}

	/**
	 * @param $name
	 * @param array $options
	 * @return core_Form_ImageField
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		return new core_Form_ImageField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors'], $options['imageDeleteUrl']
		);
	}
}
<?php

class core_Form_Option
{
	public $value;
	public $label;
	public $selected = false;
	public $attr;

	public function __construct($value, $label = null, $selected = false, $attr = array())
	{
		$this->value = $value;
		$this->label = $label;
		$this->selected = $selected;
		$this->attr = $attr;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function getLabel()
	{
		return $this->label;
	}


	// public function toArray()
	// {
	// 	return array(
	// 			'value' => $this->value,
	// 			'label' => $this->label,
	// 			'selected' => $this->selected
	// 		);
	// }
}
<?php
/**
 * 
 */
class core_Form_FieldGroup
{
	protected $id;
	protected $title;
	protected $class;
	protected $collapsible = false;
	protected $hasWysiwyg = false;
	protected $fields = array();
	protected $rendered = false;
	protected $first = false;
	protected $wrapperClass = '';
	protected $note = '';

	/**
	 * Class constructor
	 *
	 * @param $id
	 * @param $title
	 * @param null $class
	 * @param bool $collapsible
	 * @param bool $hasWysiwyg
	 * @param bool $first
	 * @param string $wrapperClass
	 * @param string $note
	 */
	public function __construct($id, $title, $class = null, $collapsible = false, $hasWysiwyg = false, $first = false, $wrapperClass = '', $note = '')
	{
		$this->id = $id;
		$this->title = $title;
		$this->class = $class;
		$this->collapsible = $collapsible;
		$this->hasWysiwyg = $hasWysiwyg;
		$this->first = $first;
		$this->wrapperClass = $wrapperClass;
		$this->note = $note;
	}

	/**
	 * Get renderer
	 *
	 * @return type 
	 */
	public function getRendered()
	{
		return $this->rendered;
	}

	/**
	 * Set renderer
	 *
	 * @param bool $val
	 */
	public function setRendered($val = true)
	{
		$this->rendered = $val;
	}

	/**
	 * Get type
	 *
	 * @return type 
	 */
	public function getType()
	{
		return 'group';
	}

	/**
	 * Set type
	 *
	 * @return type 
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * Add field
	 *
	 * @param core_Form_Field $field 
	 */
	public function addField(core_Form_Field $field)
	{
		if ($field != null)
			$this->fields[] = $field;
	}
	
	/**
	 * Set fields
	 *
	 * @param array $fields
	 */
	public function setFields($fields)
	{
		$this->fields = $fields;
	}

	/**
	 * Get field
	 *
	 * @param string $name
	 * @return type 
	 */
	public function getField($name)
	{
		foreach ($this->fields as $item)
		{
			if (is_a($item, 'core_Form_Field') && $item->getName() == $name)
				return $item;
		}

		return null;
	}

	/**
	 * Lock fields
	 *
	 * @param type $lock_fields 
	 */
	public function lockFields($lock_fields)
	{
		foreach ($this->fields as $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				$field->setLocked(in_array($field->getLockName(), $lock_fields));
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->lockFields(in_array($field->getLockName(), $lock_fields));
			}
		}
	}

	/**
	 * Set tooltips
	 * @param $tooltips
	 */
	public function setTooltips($tooltips)
	{
		foreach ($this->fields as $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				if (isset($tooltips[$field->getName()]))
				{
					$field->setTooltip($tooltips[$field->getName()]);
				}
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->setTooltips($tooltips);
			}
		}
	}

	/**
	 * Bind
	 *
	 * @param type $request
	 * @param type $files 
	 */
	public function bind($request, $files)
	{
		foreach ($this->fields as $field)
			$field->bind($request, $files);
	}

	/**
	 * @param $errors
	 */
	public function bindErrors($errors)
	{
		/** @var core_Form_Field|core_Form_FieldGroup $field */
		foreach ($this->fields as $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				if (isset($errors[$field->getName()]))
				{
					$field->bindErrors($errors[$field->getName()]);
				}
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->bindErrors($errors);
			}
		}
	}

	/**
	 * Set fields errors
	 *
	 * @param $messages
	 */
	public function setFieldErrors($messages)
	{
		foreach ($this->fields as $fieldName => $field)
		{
			if (is_a($field, 'core_Form_Field'))
			{
				if (isset($messages[$fieldName]))
				{
					$field->setErrors($messages[$fieldName]);
				}
			}
			else if (is_a($field, 'core_Form_FieldGroup'))
			{
				$field->setFieldErrors($messages);
			}
		}
	}

	// /**
	//  *
	//  * @param type $messages
	//  * @return boolean 
	//  */
	// public function isValid(&$messages = array())
	// {
	// 	$isValid = true;

	// 	foreach ($this->fields as $field)
	// 	{
	// 		$ret = $field->isValid($messages);
	// 		if (!$ret) $isValid = false;
	// 	}

	// 	return $isValid;
	// }

	/**
	 * 
	 */
	public function toArray()
	{
		return array(
			'id' => $this->id,
			'title' => $this->title,
			'class' => $this->class,
			'collapsible' => $this->collapsible,
			'fields' => $this->fields,
			'hasWysiwyg' => $this->hasWysiwyg,
			'first' => $this->first,
			'wrapperClass' => $this->wrapperClass,
			'note' => $this->note,
		);
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 * @return core_Form_FieldGroup
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		$defaults = array(
			'label' => null,
			'class' => null,
			'collapsible' => false,
			'hasWysiwyg' => false,
			'first' => false,
			'wrapperClass' => '',
			'note' => '',
		);
		
		if (is_null($options))
		{
			$options = array();
		}
		
		$options = array_merge($defaults, $options);

		return new core_Form_FieldGroup($name, $options['label'], $options['class'], $options['collapsible'], $options['hasWysiwyg'], $options['first'], $options['wrapperClass'], $options['note']);
	}
}
<?php

class core_Form_WholesalePricingField extends core_Form_Field
{
	protected $levels = 1;
	protected $pricing = array();

	public function getLevels()
	{
		return $this->levels;
	}

	public function setLevels($levels)
	{
		$this->levels = $levels;
	}

	public function getPricing()
	{
		return $this->pricing;
	}
	
	public function setPricing($level, $price)
	{
		$this->pricing[$level] = $price;
	}

	public function toArray()
	{
		$arr = parent::toArray();

		$arr['levels'] = $this->getLevels();
		$arr['pricing'] = $this->getPricing();

		return $arr;
	}

	public function getType()
	{
		return 'wholesale-pricing';
	}

	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'levels' => 1,
			'price_level_1' => null,
			'price_level_2' => null,
			'price_level_3' => null,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_WholesalePricingField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'], 
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setLevels($options['levels']);
		$field->setPricing(1, $options['price_level_1']);
		$field->setPricing(2, $options['price_level_2']);
		$field->setPricing(3, $options['price_level_3']);

		return $field;
	}
}
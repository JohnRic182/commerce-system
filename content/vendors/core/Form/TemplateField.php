<?php

class core_Form_TemplateField extends core_Form_Field
{
	protected $file = false;

	public function getFile()
	{
		return $this->file;
	}

	public function setFile($file)
	{
		$this->file = $file;
	}

	public function getType()
	{
		return 'template';
	}

	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'file' => false,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_TemplateField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors'], $options['placeholder']
		);

		$field->setFile($options['file']);

		return $field;
	}
}
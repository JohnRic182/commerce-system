<?php

class core_Form_DateTimeField extends core_Form_DateField
{
	protected $hoursSet = array();
	protected $minutesSet = array();


	public function getType()
	{
		return 'datetime';
	}

	protected function initSets()
	{
		parent::initSets();

		$this->hoursSet = array_keys(array_fill(0, 24, 1));
		$this->minutesSet = array_keys(array_fill(0, 60, 1));
	}

	public function toArray()
	{
		$arr = parent::toArray();

		$arr['hoursSet'] = $this->hoursSet;
		$arr['minutesSet'] = $this->minutesSet;

		return $arr;
	}

	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'yearStart' => date('Y'),
			'yearFinish' => date('Y') + 25,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_DateTimeField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setDate($options['value']);

		$field->setYearStart($options['yearStart']);
		$field->setYearFinish($options['yearFinish']);

		$field->initSets();

		return $field;
	}
}
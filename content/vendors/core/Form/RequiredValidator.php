<?php

class core_Form_RequiredValidator implements core_Form_Validator
{
	public function isValid(core_Form_Field $field)
	{
		if (is_a($field, 'core_Form_CheckboxField'))
		{
			return $field->checked();
		}
		else
		{
			$val = $field->getValue();
			return $val !== null && trim($val) != '';
		}
	}

	public function getMessage(core_Form_Field $field)
	{
		return $field->getLabel().' is required';
	}
}
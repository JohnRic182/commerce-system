<?php

/**
 * Class core_Form_MinMaxNumericField
 */
class core_Form_MinMaxNumericField extends core_Form_Field
{
	protected $minValue;
	protected $maxValue;

	public function getMinValue()
	{
		return $this->minValue;
	}
	public function setMinValue($minValue)
	{
		$this->minValue = $minValue;
	}

	public function getMaxValue()
	{
		return $this->maxValue;
	}
	public function setMaxValue($maxValue)
	{
		$this->maxValue = $maxValue;
	}

	public function getElementId()
	{
		$id = parent::getElementId();

		return str_replace('field-', '', $id);
	}

	public function toArray()
	{
		$arr = parent::toArray();

		$arr['minValue'] = $this->getMinValue();
		$arr['maxValue'] = $this->getMaxValue();

		return $arr;
	}

	public function getType()
	{
		return 'min-max-numeric';
	}

	public function bind($request, $files)
	{
		$min = isset($request['min_'.$this->getName()]) ? $request['min_'.$this->getName()] : null;
		$max = isset($request['max_'.$this->getName()]) ? $request['max_'.$this->getName()] : null;

		$this->setMinValue($min);
		$this->setMaxValue($max);
	}

	public function getDataMapper()
	{
		return new core_Form_MinMaxFieldDataMapper();
	}

	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'minValue' => null,
			'maxValue' => null,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_MinMaxNumericField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setMinValue($options['minValue']);
		$field->setMaxValue($options['maxValue']);
	
		return $field;
	}
}

class core_Form_MinMaxFieldDataMapper implements core_Form_Data_FieldMapper
{
	public function mapDataToForm($data, core_Form_Field $field)
	{
		if (is_a($field, 'core_Form_MinMaxNumericField'))
		{
			$name = $field->getName();

			$methodName = 'getMin'.strtoupper(substr($name, 0, 1)).substr($name, 1);

			if (method_exists($data, $methodName))
			{
				$field->setMinValue(call_user_func(array($data, $methodName)));
			}

			$methodName = 'getMax'.strtoupper(substr($name, 0, 1)).substr($name, 1);

			if (method_exists($data, $methodName))
			{
				$field->setMaxValue(call_user_func(array($data, $methodName)));
			}
		}
	}

	public function mapFormToData(core_Form_Field $field, &$data)
	{
		if (is_a($field, 'core_Form_MinMaxNumericField'))
		{
			$name = $field->getName();

			$methodName = 'setMin'.strtoupper(substr($name, 0, 1)).substr($name, 1);

			if (method_exists($data, $methodName))
			{
				$value = $field->getMinValue();
				call_user_func(array($data, $methodName), $value);
			}

			$methodName = 'setMax'.strtoupper(substr($name, 0, 1)).substr($name, 1);

			if (method_exists($data, $methodName))
			{
				$value = $field->getMaxValue();
				call_user_func(array($data, $methodName), $value);
			}
		}
	}
}
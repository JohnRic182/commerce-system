<?php
/**
 * Class core_Form_MoneyTextField
 */
class core_Form_MoneyTextField extends core_Form_TextField
{

	protected $emptyValue;

	/**
	 * Get field type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'money';
	}

	/**
	 * Class constructor
	 *
	 * @param $name
	 * @param $label
	 * @param null $value
	 * @param bool $required
	 * @param bool $lockable
	 * @param null $lockName
	 * @param null $tooltip
	 * @param null $note
	 * @param array $attr
	 * @param string $wrapperClass
	 * @param null $emptyValue
	 * @param bool $readonly
	 * @param mixed $validators
	 * @param array|null $errors
	 */
	public function __construct($name, $label, $value = null, $required = false, $lockable = false, $lockName = null, $tooltip = null, $note = null, $attr = array(), $wrapperClass = '', $emptyValue = null, $readonly = false, $validators = null, $errors = null, $placeholder = '')
	{
		parent::__construct($name, $label, $value, $required, $lockable, $lockName, $tooltip, $note, $attr, $wrapperClass, $readonly, $validators, $errors, $placeholder);

		$this->emptyValue = $emptyValue;
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$ret = parent::toArray();

		$ret['emptyValue'] = $this->emptyValue;

		return $ret;
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_MoneyTextField|core_Form_TextField
	 */
	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		if (!isset($options['emptyValue'])) $options['emptyValue'] = null;

		return new core_Form_MoneyTextField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['emptyValue'], $options['readonly'],
			$options['validators'], $options['errors'], $options['placeholder']
		);
	}
}
<?php
/**
 * Class core_Form_LbsOzField
 */
class core_Form_LbsOzField extends core_Form_Field
{
	protected $lbsValue;
	protected $ozValue;

	/**
	 * @param mixed $lbsValue
	 */
	public function setLbsValue($lbsValue)
	{
		$this->lbsValue = $lbsValue;
	}

	/**
	 * @return mixed
	 */
	public function getLbsValue()
	{
		return $this->lbsValue;
	}

	/**
	 * @param mixed $ozValue
	 */
	public function setOzValue($ozValue)
	{
		$this->ozValue = $ozValue;
	}

	/**
	 * @return mixed
	 */
	public function getOzValue()
	{
		return $this->ozValue;
	}

	public function getElementId()
	{
		$id = parent::getElementId();

		return str_replace('field-', '', $id);
	}

	public function toArray()
	{
		$arr = parent::toArray();

		$arr['lbsValue'] = $this->getLbsValue();
		$arr['ozValue'] = $this->getOzValue();

		return $arr;
	}

	/**
	 * Return field type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'lbs_oz';
	}

	public function bind($request, $files)
	{
		$lbs = isset($request[$this->getName().'_lbs']) ? $request[$this->getName().'_lbs'] : null;
		$oz = isset($request[$this->getName().'_oz']) ? $request[$this->getName().'_oz'] : null;

		$this->setLbsValue($lbs);
		$this->setOzValue($oz);
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_TextField
	 */
	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'lbsValue' => null,
			'ozValue' => null,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_LbsOzField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors']
		);

		$field->setLbsValue($options['lbsValue']);
		$field->setOzValue($options['ozValue']);

		return $field;
	}
}
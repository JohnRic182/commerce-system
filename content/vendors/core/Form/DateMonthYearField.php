<?php
/**
 * Class core_Form_DateMonthYearField
 */
class core_Form_DateMonthYearField extends core_Form_DateField
{
	/**
	 * Get field type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'date-month-year';
	}

	/**
	 * Create from builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_DateMonthYearField
	 */
	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'yearStart' => date('Y'),
			'yearFinish' => date('Y') + 25,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_DateMonthYearField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setDate($options['value']);

		$field->setYearStart($options['yearStart']);
		$field->setYearFinish($options['yearFinish']);

		$field->initSets();

		return $field;
	}
}
<?php
/**
 * Class core_Form_DateField
 */
class core_Form_DateField extends core_Form_Field
{
	protected $date;

	protected $yearStart;
	protected $yearFinish;

	protected $yearsSet = array();
	protected $daysSet = array();
	protected $monthsSet = array();

	/**
	 * Return field type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'date';
	}

	/**
	 * Init arrays for selectors
	 */
	protected function initSets()
	{
		$this->yearsSet = array_keys(array_fill($this->yearStart, $this->yearFinish - $this->yearStart, 1));
		$this->monthsSet = array_keys(array_fill(1, 12, 1));
		$this->daysSet = array_keys(array_fill(1, 31, 1));
	}

	/**
	 * Set date
	 *
	 * @param $date
	 */
	protected function setDate($date)
	{
		if (is_object($date) && $date instanceof DateTime)
		{
			$date = date('Y-m-d H:i:s', $date->format('U'));
		}
		else if (trim($date) == '') $date = date('Y-m-d H:i:s');

		$this->date = date_parse($date);
	}

	/**
	 * Set start year
	 *
	 * @param $yearStart
	 */
	protected function setYearStart($yearStart)
	{
		$this->yearStart = $yearStart;
	}

	/**
	 * Set end year
	 *
	 * @param $yearFinish
	 */
	protected function setYearFinish($yearFinish)
	{
		$this->yearFinish = $yearFinish;
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$arr = parent::toArray();

		$arr['yearsSet'] = $this->yearsSet;
		$arr['monthsSet'] = $this->monthsSet;
		$arr['daysSet'] = $this->daysSet;

		$arr['date'] = $this->date;

		return $arr;
	}

	/**
	 * Create from builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_DateField
	 */
	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'yearStart' => date('Y'),
			'yearFinish' => date('Y') + 25,
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_DateField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setDate($options['value']);

		$field->setYearStart($options['yearStart']);
		$field->setYearFinish($options['yearFinish']);

		$field->initSets();

		return $field;
	}
}
<?php

/**
 * Class core_Form_PasswordField
 */
class core_Form_PasswordField extends core_Form_Field
{
	/** @var bool  */
	protected $meter = false;

	/** @var bool  */
	protected $confirmationFlag = false;

	/**
	 * Set confirmationFlag
	 *
	 * @param $meter
	 */
	public function setConfirmationFlag($confirmationFlag)
	{
		$this->confirmationFlag = $confirmationFlag;
	}

	/**
	 * Get confirmationFlag
	 *
	 * @return bool
	 */
	public function hasConfirmationFlag()
	{
		return $this->confirmationFlag;
	}

	/**
	 * Set meter
	 *
	 * @param $meter
	 */
	public function setMeter($meter)
	{
		$this->meter = $meter;
	}

	/**
	 * Get meter
	 *
	 * @return bool
	 */
	public function hasMeter()
	{
		return $this->meter;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'password';
	}

	/**
	 * Create field
	 *
	 * @param $name
	 * @param array $options
	 * @return core_Form_PasswordField
	 */
	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'meter' => false,
			'confirmationFlag' => false
		);

		$options = array_merge($defaults, $options);

		$field = new core_Form_PasswordField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'], 
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors'], $options['placeholder']
		);

		$field->setMeter($options['meter']);
		$field->setConfirmationFlag($options['confirmationFlag']);

		return $field;
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$values = parent::toArray();
		$values['meter'] = $this->hasMeter();
		$values['confirmationFlag'] = $this->hasConfirmationFlag();

		return $values;
	}
}
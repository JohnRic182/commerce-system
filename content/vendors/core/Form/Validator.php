<?php

interface core_Form_Validator
{
	public function isValid(core_Form_Field $field);

	public function getMessage(core_Form_Field $field);
}
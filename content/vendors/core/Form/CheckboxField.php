<?php
/**
 * Class core_Form_CheckboxField
 */
class core_Form_CheckboxField extends core_Form_Field
{
	protected $checked = false;

	/**
	 * Class constructor
	 * @param $name
	 * @param $title
	 * @param null $value
	 * @param bool $required
	 * @param bool $lockable
	 * @param null $lockName
	 * @param null $tooltip
	 * @param null $note
	 * @param array $attr
	 * @param string $wrapperClass
	 * @param null $current_value
	 * @param bool $readonly
	 * @param null $validators
	 * @param null $errors
	 */
	public function __construct($name, $title, $value = null, $required = false, $lockable = false, $lockName = null, $tooltip = null, $note = null, $attr = array(), $wrapperClass = '', $current_value = null, $readonly = false, $validators = null, $errors = null)
	{
		parent::__construct($name, $title, $value, $required, $lockable, $lockName, $tooltip, $note, $attr, $wrapperClass, $readonly, $validators, $errors);

		$this->setChecked($current_value);
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'checkbox';
	}

	/**
	 * Get is checked
	 *
	 * @return bool
	 */
	public function checked()
	{
		return $this->checked;
	}

	/**
	 * Set is checked
	 *
	 * @param $current_value
	 */
	public function setChecked($current_value)
	{
		if (!is_null($this->value) && trim($this->value) != '')
		{
			$this->checked = ($current_value == $this->value);
		}
		else
		{
			$this->checked = ($current_value ? true : false);
		}
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$vals = parent::toArray();

		$vals['checked'] = $this->checked;

		return $vals;
	}

	/**
	 * Bind from request
	 *
	 * @param $request
	 * @param $files
	 */
	public function bind($request, $files)
	{
		if (isset($request[$this->name]))
		{
			$val = $request[$this->name];
			$this->bindData($val);
		}
	}

	/**
	 * Bing data
	 *
	 * @param $value
	 */
	public function bindData($value)
	{
		if (trim($value) == 'on')
		{
			$this->setChecked(true);
		}
		else
		{
			$this->setChecked($value);
		}
	}

	/**
	 * Get data
	 *
	 * @return bool|null
	 */
	public function getData()
	{
		if (is_null($this->value) || trim($this->value) == '') return $this->checked;

		return ($this->checked) ? $this->value : null;
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_CheckboxField
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);
		$options['current_value'] = (isset($options['current_value']) ? $options['current_value'] : null);

		return new core_Form_CheckboxField(
			$name,
			$options['label'],
			$options['value'],
			$options['required'],
			$options['lockable'],
			$options['lockName'],
			$options['tooltip'],
			$options['note'],
			$options['attr'],
			$options['wrapperClass'],
			$options['current_value'],
			$options['readonly'],
			$options['validators'],
			$options['errors']
		);
	}
}
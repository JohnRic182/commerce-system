<?php

class core_Form_FormBuilder
{
	/**
	 * The children of the form
	 * @var array
	 */
	private $children = array();

	/**
	 * The name of the form
	 * @var string
	 */
	private $name;

	/**
	 * The data of the form
	 * @var mixed
	 */
	private $data;

	private $dataMapper;
	private $dataValidator;

	/**
	 * Class constructor
	 *
	 * @param $name
	 * @param null $data
	 */
	public function __construct($name, $data = null)
	{
		$this->name = $name;
		$this->data = $data;
	}

	/**
	 * Returns the form builder name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Returns the data
	 *
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Sets the data
	 *
	 * @param mixed $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}

	public function childCount()
	{
		return count($this->children);
	}

	/**
	 * Returns the children
	 *
	 * @return array
	 */
	protected function getChildren()
	{
		return $this->children;
	}

	public function get($name)
	{
		if (isset($this->children[$name]))
		{
			return $this->children[$name];
		}

		return null;
	}

	public function has($name)
	{
		return isset($this->children[$name]);
	}

	public function remove($name)
	{
		if (isset($this->children[$name]))
		{
			unset($this->children[$name]);
		}
	}

	/**
	 * Adds a new field to this group. A field must have a unique name within
	 * the group. Otherwise the existing field is overwritten.
	 *
	 * If you add a nested group, this group should also be represented in the
	 * object hierarchy.
	 *
	 * @param $child
	 * @param string $type
	 * @param array $options
	 * @return $this
	 * @throws Exception
	 */
	public function add($child, $type = 'string', array $options = array())
	{
		if ($child instanceof self)
		{
			$this->children[$child->getName()] = $child;

			return $this;
		}

		if (!is_string($child)) throw new Exception('string or core_Form_FormBuilder expected for parameter $child');
		if (!is_string($type)) throw new Exception('string expected for parameter $type');

		$this->children[$child] = array('type' => $type, 'options' => $options);

		return $this;
	}

	/**
	 * Return form object from array
	 * @param type $children
	 * @return core_Form_Form 
	 */
	public function getForm($children = null)
	{
		if ($children === null) $children = $this->children;

		$form = new core_Form_Form($this->getDataMapper(), $this->getDataValidator());

		foreach ($children as $name => $child)
		{
			if ($child instanceof self)
			{
				$group = core_Form_FieldGroup::createFromFormBuilder($name, $child->getData());

				$childForm = $this->getForm($child->getChildren());
				$fields = $childForm->getItems();
				$group->setFields($fields);

				$form->addGroup($group);
			}
			else
			{
				$options = $child['options'];

				if (!array_key_exists('label', $options))
				{
					$options['label'] = $name;
				}

				$field = null;

				switch ($child['type'])
				{
					case 'checkbox':
					{
						$field = core_Form_CheckboxField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'file':
					{
						$field = core_Form_FileField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'choice':
					{
//						if (isset($options['expanded']) && $options['expanded'])
//						{
//							$field = core_Form_SelectField::createFromFormBuilder($name, $options);
//						}
						//else
						if (isset($options['multiple']) && $options['multiple'])
						{
							$field = core_Form_MultiSelectField::createFromFormBuilder($name, $options);
						}
						else
						{
							$field = core_Form_SelectField::createFromFormBuilder($name, $options);
						}
						break;
					}
					
					case 'hidden':
					{
						$field = core_Form_HiddenField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'minmaxnumeric':
					{
						$field = core_Form_MinMaxNumericField::createFromFormBuilder($name, $options);
						break;
					}

					case 'dimension':
					{
						$field = core_Form_DimensionField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'money':
					{
						$field = core_Form_MoneyTextField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'numeric':
					{
						$field = core_Form_NumericTextField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'static':
					{
						$field = core_Form_StaticField::createFromFormBuilder($name, $options);
						break;
					}

					case 'description':
					{
						$field = core_Form_DescriptionField::createFromFormBuilder($name, $options);
						break;
					}

					case 'textarea':
					{
						$field = core_Form_TextareaField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'wholesalepricing':
					{
						$field = core_Form_WholesalePricingField::createFromFormBuilder($name, $options);
						break;
					}

					case 'recurring-settings':
					{
						$field = core_Form_RecurringSettingsField::createFromFormBuilder($name, $options);
						break;
					}

					case 'integer':
					{
						$field = core_Form_IntegerTextField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'password':
					{
						$field = core_Form_PasswordField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'imagepreview':
					{
						$field = core_Form_ImagePreviewField::createFromFormBuilder($name, $options);
						break;
					}

					case 'image':
					{
						$field = core_Form_ImageField::createFromFormBuilder($name, $options);
						break;
					}

					case 'image-multiple':
					{
						$field = core_Form_ImageMultipleField::createFromFormBuilder($name, $options);
						break;
					}

					case 'color' :
					{
						$field = core_Form_ColorField::createFromFormBuilder($name, $options);
						break;
					}
					
					case 'submit':
					{
						$field = core_Form_SubmitField::createFromFormBuilder($name, $options);
						break;
					}

					case 'date':
					{
						$field = core_Form_DateField::createFromFormBuilder($name, $options);
						break;
					}

					case 'datetime':
					{
						$field = core_Form_DateTimeField::createFromFormBuilder($name, $options);
						break;
					}

					case 'datemonthyear':
					{
						$field = core_Form_DateMonthYearField::createFromFormBuilder($name, $options);
						break;
					}

					case 'date-delay' :
					{
						$field = core_Form_DateDelayField::createFromFormBuilder($name, $options);
						break;
					}

					case 'template':
					{
						$field = core_Form_TemplateField::createFromFormBuilder($name, $options);
						break;
					}

					case 'lbs_oz':
					{
						$field = core_Form_LbsOzField::createFromFormBuilder($name, $options);
						break;
					}

					case 'seopreview':
					{
						$field = core_Form_SeoPreviewField::createFromFormBuilder($name, $options);
						break;
					}

					case 'text':
					default:
					{
						$field = core_Form_TextField::createFromFormBuilder($name, $options);
						break;
					}
				}

				if ($field)
				{
					$form->addField($field);
				}
			}
		}

		$form->setData($this->data);

		return $form;
	}

	public function getDataMapper()
	{
		if (is_null($this->dataMapper))
			$this->dataMapper = new core_Form_Data_DefaultMapper();

		return $this->dataMapper;
	}

	public function setDataMapper(core_Form_Data_MapperInterface $dataMapper)
	{
		$this->dataMapper = $dataMapper;
	}

	public function getDataValidator()
	{
		return $this->dataValidator;
	}

	public function setDataValidator(core_Form_Data_Validator $dataValidator)
	{
		$this->dataValidator = $dataValidator;
	}
}
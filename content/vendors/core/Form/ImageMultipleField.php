<?php
/**
 * Class core_Form_ImageField
 */
class core_Form_ImageMultipleField extends core_Form_Field
{
	/** @var string $imageUpdateUrl */
	protected $imageUpdateUrl = null;

	/** @var string $imageDeleteUrl */
	protected $imageDeleteUrl = null;

	/** @var string $allowExternalImage */
	protected $allowExternalImage = false;

	/**
	 * @param $name
	 * @param $title
	 * @param null $value
	 * @param bool $required
	 * @param bool $lockable
	 * @param null $lockName
	 * @param null $tooltip
	 * @param null $note
	 * @param array $attr
	 * @param string $wrapperClass
	 * @param bool $readonly
	 * @param null $validators
	 * @param null $errors
	 * @param null $imageUpdateUrl
	 * @param null $imageDeleteUrl
	 * @param bool $allowExternalImage
	 */
	public function __construct($name, $title, $value = null, $required = false, $lockable = false, $lockName = null, $tooltip = null, $note = null, $attr = array(), $wrapperClass = '', $readonly = false, $validators = null, $errors = null, $imageUpdateUrl = null, $imageDeleteUrl = null, $allowExternalImage = false)
	{
		parent::__construct($name, $title, $value, $required, $lockable, $lockName, $tooltip, $note, $attr, $wrapperClass, $readonly, $validators, $errors);

		$this->setImageUpdateUrl($imageUpdateUrl);
		$this->setImageDeleteUrl($imageDeleteUrl);
		$this->setAllowExternalImage($allowExternalImage);
	}

	/**
	 * @param $imageUpdateUrl
	 */
	public function setImageUpdateUrl($imageUpdateUrl)
	{
		$this->imageUpdateUrl = $imageUpdateUrl;
	}

	/**
	 * @param $imageDeleteUrl
	 */
	public function setImageDeleteUrl($imageDeleteUrl)
	{
		$this->imageDeleteUrl = $imageDeleteUrl;
	}

	/**
	 * @param $allowExternalImage
	 */
	public function setAllowExternalImage($allowExternalImage)
	{
		$this->allowExternalImage = $allowExternalImage;
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$values = parent::toArray();

		$values['imageUpdateUrl'] = $this->getImageUpdateUrl();
		$values['imageDeleteUrl'] = $this->getImageDeleteUrl();
		$values['allowExternalImage'] = $this->getAllowExternalImage();

		return $values;
	}

	/**
	 * @return string
	 */
	public function getImageUpdateUrl()
	{
		return $this->imageUpdateUrl;
	}

	/**
	 * @return string
	 */
	public function getImageDeleteUrl()
	{
		return $this->imageDeleteUrl;
	}

	/**
	 * @return string
	 */
	public function getAllowExternalImage()
	{
		return $this->allowExternalImage;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return 'image-multiple';
	}

	/**
	 * @return null
	 */
	public function getData()
	{
		return null;
	}

	/**
	 * @param $name
	 * @param array $options
	 * @return core_Form_ImageField
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$allowExternalImage = (isset($options['allowExternalImage']) ? $options['allowExternalImage'] : false);

		return new core_Form_ImageMultipleField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors'], $options['imageUpdateUrl'], $options['imageDeleteUrl'],
			$allowExternalImage
		);
	}
}
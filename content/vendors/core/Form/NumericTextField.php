<?php
/**
 * Class core_Form_NumericTextField
 */
class core_Form_NumericTextField extends core_Form_TextField
{
	/**
	 * Get field type
	 * @return string
	 */
	public function getType()
	{
		return 'numeric';
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_NumericTextField|core_Form_TextField
	 */
	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		return new core_Form_NumericTextField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors'], $options['placeholder']
		);
	}
}
<?php
/**
 * Class core_Form_RecurringSettingsField
 */
class core_Form_RecurringSettingsField extends core_Form_Field
{
	protected $isCustom = false;
	protected $isTrial = false;
	protected $periodUnitOptions = array();
	protected $periodFrequencyAndUnitOptions = array();
	protected $periodCyclesOptions = array();

	/**
	 * Set is custom
	 *
	 * @param boolean $isCustom
	 */
	public function setIsCustom($isCustom)
	{
		$this->isCustom = $isCustom;
	}

	/**
	 * Get is custom
	 *
	 * @return boolean
	 */
	public function getIsCustom()
	{
		return $this->isCustom;
	}

	/**
	 * Set is trial
	 *
	 * @param boolean $isTrial
	 */
	public function setIsTrial($isTrial)
	{
		$this->isTrial = $isTrial;
	}

	/**
	 * Get is trial
	 *
	 * @return boolean
	 */
	public function getIsTrial()
	{
		return $this->isTrial;
	}

	/**
	 * Set period unit options
	 *
	 * @param $periodUnitOptions
	 */
	public function setPeriodUnitOptions($periodUnitOptions)
	{
		$this->periodUnitOptions = $periodUnitOptions;
	}

	/**
	 * Get period unit options
	 *
	 * @return array
	 */
	public function getPeriodUnitOptions()
	{
		return $this->periodUnitOptions;
	}

	/**
	 * Set period frequency and unit options
	 *
	 * @param $periodFrequencyAndUnitOptions
	 */
	public function setPeriodFrequencyAndUnitOptions($periodFrequencyAndUnitOptions)
	{
		$this->periodFrequencyAndUnitOptions = $periodFrequencyAndUnitOptions;
	}

	/**
	 * Get period frequency and unit options
	 *
	 * @return array
	 */
	public function getPeriodFrequencyAndUnitOptions()
	{
		return $this->periodFrequencyAndUnitOptions;
	}

	/**
	 * Set period cycles options
	 *
	 * @param $periodCyclesOptions
	 */
	public function setPeriodCyclesOptions($periodCyclesOptions)
	{
		$this->periodCyclesOptions = $periodCyclesOptions;
	}

	/**
	 * Get period cycles options
	 *
	 * @return array
	 */
	public function getPeriodCyclesOptions()
	{
		return $this->periodCyclesOptions;
	}

	/**
	 * Return as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$arr = parent::toArray();

		$arr['custom'] = $this->getIsCustom();
		$arr['trial'] = $this->getIsTrial();
		$arr['periodFrequencyAndUnitOptions'] = $this->getPeriodFrequencyAndUnitOptions();
		$arr['periodCyclesOptions'] = $this->getPeriodCyclesOptions();
		$arr['periodUnitOptions'] = $this->getPeriodUnitOptions();

		return $arr;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'recurring-settings';
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_RecurringSettingsField
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);

		$defaults = array(
			'custom' => false,
			'trial' => false,
			'periodUnitOptions' => array(),
			'periodFrequencyAndUnitOptions' => array(),
			'periodCyclesOptions' => array(),
		);

		$options = array_merge($defaults, $options);
		$options['value']['period_frequency_and_unit'] = $options['value']['period_frequency'].'-'.$options['value']['period_unit'];

		/** @var core_Form_RecurringSettingsField $field */
		$field = new core_Form_RecurringSettingsField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'],
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);

		$field->setIsCustom($options['custom']);
		$field->setIsTrial($options['trial']);
		$field->setPeriodUnitOptions($options['periodUnitOptions']);
		$field->setPeriodFrequencyAndUnitOptions($options['periodFrequencyAndUnitOptions']);
		$field->setPeriodCyclesOptions($options['periodCyclesOptions']);

		return $field;
	}
}
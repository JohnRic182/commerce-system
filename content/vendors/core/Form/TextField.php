<?php
/**
 * Class core_Form_TextField
 */
class core_Form_TextField extends core_Form_Field
{
	/**
	 * Return field type
	 *
	 * @return string
	 */
	public function getType()
	{
		return 'text';
	}

	/**
	 * Create from form builder
	 *
	 * @param $name
	 * @param array $options
	 *
	 * @return core_Form_TextField
	 */
	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		return new core_Form_TextField(
			$name, $options['label'], $options['value'], $options['required'],
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'], 
			$options['attr'], $options['wrapperClass'], $options['readonly'],
			$options['validators'], $options['errors'], $options['placeholder']
		);
	}
}
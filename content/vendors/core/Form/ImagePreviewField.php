<?php
/**
 * 
 */
class core_Form_ImagePreviewField extends core_Form_Field
{
	protected $imageInfo = null;
	
	/**
	 * Class constructor
	 */
	public function __construct($name, $title, $value = null, $required = false, $lockable = false, $tooltip = null, $note = null, $attr = array(), $wrapperClass='', $imageInfo = null)
	{
		parent::__construct($name, $title, $value, $required, $lockable, $tooltip, $note, $attr, $wrapperClass);

		$this->setImageInfo($imageInfo);
		
		return $this;
	}
	
	/**
	 * Returns type
	 * @return type 
	 */
	public function getType()
	{
		return 'image-preview';
	}
	
	/**
	 * Set image info
	 * @param type $imageInfo 
	 */
	public function setImageInfo($imageInfo)
	{
		$this->imageInfo = $imageInfo;
		return $this;
	}

	/**
	 * Get image info
	 * @return type 
	 */
	public function getImageInfo()
	{
		return $this->imageInfo;
	}
	
	/**
	 *
	 * @return type 
	 */
	public function toArray()
	{
		$arr = parent::toArray();

		$arr['imageInfo'] = $this->getImageInfo();
		
		return $arr;
	}
	
	/**
	 *
	 * @param type $name
	 * @param array $options
	 * @return core_Form_ImagePreviewField 
	 */
	public static function createFromFormBuilder($name, $options = array())
	{
		core_Form_Field::setDefaults($options);
		
		$options['imageInfo'] = isset($options['imageInfo']) ? $options['imageInfo'] : null;

		return new core_Form_ImagePreviewField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'], 
			$options['attr'], $options['imageInfo']
		);
	}
}
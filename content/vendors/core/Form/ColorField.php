<?php

class core_Form_ColorField extends core_Form_Field
{
	public function getType()
	{
		return 'color';
	}

	public static function createFromFormBuilder($name, &$options = array())
	{
		core_Form_Field::setDefaults($options);

		return new core_Form_ColorField(
			$name, $options['label'], $options['value'], $options['required'], 
			$options['lockable'], $options['lockName'], $options['tooltip'], $options['note'], 
			$options['attr'], $options['wrapperClass'], $options['readonly']
		);
	}
}
<?php

	@session_destroy();

	$oldWorkingDir = getcwd();
	$_file_ = str_replace("\\", "/", __FILE__);
	$newWorkingDir = substr($_file_, 0, strpos($_file_, "content/vendors/tinymce"));

	chdir($newWorkingDir);

	/**
 	 * Init environment
 	 */
	define("ENV", true);

	require_once 'content/classes/_boot.php';

	$registry = new Framework_Registry();
	$registry->set('db', $db);
	$registry->setParameter('settings', $settings);
	Admin_SessionHandler::init($settings, null);

	/**
	 * Check admin security
	 */
	unset($auth_ok);
	require_once("content/admin/admin_security.php");

	define('CART_AUTH_OK', $iono->level != 99 ? $auth_ok : false);

	Admin_Log::log('TinyMCE - accessed moxie manager', Admin_Log::LEVEL_IMPORTANT);

	chdir($oldWorkingDir);

/**
 * This class handles MoxieManager SessionAuthenticator stuff.
 */
class MOXMAN_CartAuthenticator_Plugin implements MOXMAN_Auth_IAuthenticator
{
	public function authenticate(MOXMAN_Auth_User $user)
	{
		return CART_AUTH_OK;
	}
}

MOXMAN::getAuthManager()->add("CartAuthenticator", new MOXMAN_CartAuthenticator_Plugin());

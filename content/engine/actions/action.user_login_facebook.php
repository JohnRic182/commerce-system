<?php
/**
 * Process users login 
 */

	if (!defined("ENV")) exit();
	
	require_once('content/vendors/facebook/facebook.php');
	
	try
	{
		$facebook = new Facebook(array(
			'appId'  => $settings["facebookAppId"],
			'secret' => $settings["facebookAppSecret"]
		));

		$fbUserId = $facebook->getUser();

		if ($fbUserId)
		{
			$fbUserProfile = $facebook->api('/me');
			
			if ($fbUserProfile != null && is_array($fbUserProfile) && isset($fbUserProfile["id"]) && ($fbUserProfile["id"] == $fbUserId))
			{
				if ($user->facebookSync($fbUserId, $fbUserProfile))
				{
					if (isset($remove_user) && $remove_user)
					{
						$db->query("DELETE FROM ".DB_PREFIX."users WHERE uid='".intval($remove_user)."'");
					}

					$oa = ORDER_RE_UPDATE_ITEMS;
					if (isset($_SESSION["wl_pid"]))
					{
						$backurl = $url_https."ua=".USER_ADD_TO_WISHLIST."&wl_action=addto_wishlist&pid=".$_SESSION["wl_pid"];
						unset($_SESSION["wl_pid"]);
					}
					elseif (isset($_SESSION["user_started_checkout"]))
					{
						$backurl = $url_https."oa=".ORDER_OPC;
					}
					elseif ($_REQUEST["p"] == "one_page_checkout")
					{
						$backurl = $url_https."p=one_page_checkout";
					}
					elseif (isset($_SESSION["after_login_url"]))
					{
						$backurl = str_replace("pcsid=".session_id(), "", $_SESSION["after_login_url"]);
						unset($_SESSION["after_login_url"]);
					}
					else
					{
						$backurl = $url_https."p=account";
					}

					//set tracking cookie
					$user->setCookie();

					session_regenerate_id(true);

					// if http and https domain names are different, set cookie on http site too
					if ($user->isCrossDomainCookieRequired())
					{
						$backurl = $user->getCrossDomainCookieUrl($url_http, $backurl);
					}

					unset($_SESSION["user_started_checkout"]);
				}
			}
		}
		else
		{
			if (!isset($_REQUEST["retry"]))
			{
				$backurl = $url_https."ua=".USER_LOGIN_FACEBOOK."&p=login&retry=true";
			}
		}
	}
	catch(Exception $e)
	{
		
	}
	
<?php
/**
 * User's logout
 */

	if (!defined("ENV")) exit();

	$user->logOut();
	
	$_REQUEST["oa"] = $_POST["oa"] = $_GET["oa"] = ORDER_UNLINK_USER;

	// unset user's cookie
	
	$user->unsetCookie();
	
	$backurl = $url_http."ua=".USER_UNSET_COOKIE;

	/**
	 * Facebook logout
	 */
	if ($settings["facebookLogin"] == "Yes")
	{
		require_once('content/vendors/facebook/facebook.php');

		try
		{
			$facebook = new Facebook(array(
				'appId'  => $settings["facebookAppId"],
				'secret' => $settings["facebookAppSecret"]
			));

			if ($facebook->getUser())
			{
				$backurl = $facebookLogoutUrl = $facebook->getLogoutUrl(array("next" => $backurl));
			}
		}
		catch (Exception $e)
		{

		}
	}
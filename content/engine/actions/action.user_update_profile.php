<?php
/**
 * Show user profile
 */

if (!defined("ENV")) exit();

if ($user->auth_ok)
{
	// trying to update profile
	$form = isset($_POST["form"]) ? $_POST["form"] : array();
	$form = is_array($form) ? $form : array();
	
	$customFieldsData = isset($_POST["custom_field"]) ? $_POST["custom_field"] : array();

	if (!Nonce::verify(isset($_POST['nonce']) ? $_POST['nonce'] : '', 'user_update_profile'))
	{
		$user->setError($msg['common']['error_invalid_nonce']);
	}
	else
	{
		$user->updateProfile($form, $customFieldsData);
	}
}

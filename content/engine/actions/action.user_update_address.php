<?php
/**
 * Update users address
 */

if (!defined("ENV")) exit();

if ($user->auth_ok)
{
	// store shipping address data
	$address_id = isset($_REQUEST["address_id"]) ? $_REQUEST["address_id"] : 0;
	$form = isset($_REQUEST["form"]) ? (is_array($_REQUEST["form"]) ? $_REQUEST["form"] : array()) : array();
	
	// custom fields
	$custom_field = isset($_REQUEST["custom_field"]) ? (is_array($_REQUEST["custom_field"]) ? $_REQUEST["custom_field"] : array()) : array();

	$validationError = false;

	if (!Nonce::verify(isset($_POST['nonce']) ? $_POST['nonce'] : '', 'user_update_address'))
	{
		$validationError = true;
		$user->setError($msg['common']['error_invalid_nonce']);
	}

	/**
	if (!$validationError && $settings['EndiciaUspsAddressValidation'] == 1)
	{
		// check is address correct, and if it is not, just exit
		if (!AddressValidator::validate($db, $form, $msg))
		{
			$validationError = true;
			$user->is_error = true;
			$user->setError(AddressValidator::getLastError());

		}
	}
	**/

	if (!$validationError)
	{
		$user->updateShippingAddress($address_id, $form, $custom_field);
	}
	
	if (!$user->is_error)
	{
		$backurl = $url_https."p=address_book";
	}
}

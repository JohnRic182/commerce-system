<?php
/**
 * Complete order
 */

	if (!defined("ENV")) exit();
	
	if ($user->auth_ok && $order->itemsCount)
	{
		$form = isset($_POST["form"]) ? $_POST["form"] : array();

		$orderSource = isset($_SESSION["osc_source"]) ? $_SESSION["osc_source"] : null;
		$offsiteCampaignId = isset($_SESSION["osc_campaign_id"]) ? $_SESSION["osc_campaign_id"] : null;
		$ofid = $order->getOrderFormID();

		$valid = OrderProvider::completeOrder(
			$order, $user, $form, $customFields, 'Process', 'Pending',
			$orderSource, $offsiteCampaignId
		);

		if (!$valid)
		{
			$backurl = $url_https.'p=one_page_checkout';
		}
		else
		{
			// Complete order
			$backurl = $url_https."p=completed";
			if ($ofid)
			{
				$orderFormRepository = new DataAccess_OrderFormRepository($db);
				$form = $orderFormRepository->getById($ofid);
				if ($form){
					if (trim($form['thank_you_page_url']) != ''){
						$backurl = $form['thank_you_page_url'];
					}
				}
			}
		}
	}

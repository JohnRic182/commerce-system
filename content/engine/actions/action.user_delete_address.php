<?php
/**
 * Delete shipping address from address book
 */

if (!defined("ENV")) exit();

if ($user->auth_ok && Nonce::verify(isset($_REQUEST['nonce']) ? $_REQUEST['nonce'] : '', 'user_delete_address'))
{
	$address_id = isset($_REQUEST["address_id"]) ? intval($_REQUEST["address_id"]) : 0;
	$user->deleteShippingAddress($address_id);
	$backurl = UrlUtils::getBackUrl();
}


<?php 
/**
 * Save shipping address
 */

if (!defined("ENV")) exit();

if ($user->auth_ok && $order->totalAmount > 0)
{
	$db->query("SELECT * FROM ".DB_PREFIX."payment_methods WHERE id='paypalec' AND active='Yes'");
	if ($db->moveNext())
	{
		$paypal_payment_method = $db->col;

		$paypal_ec = new $db->col['class'];

		$paypal_ec_result = $paypal_ec->getExpressCheckoutDetails($_REQUEST['token']);

		view()->assign("paypal_ec_return_is_error", $paypal_ec->is_error);

		if ($paypal_ec->is_error)
		{
			view()->assign("paypal_ec_return_errors", array($paypal_ec->error_message));
			unset($_SESSION["paypal_express_checkout_details"]);
		}
		else
		{
			$user->payPalExpressUserInfo($paypal_ec_result);
			$shipping_address_id = $user->payPal2Shipping($paypal_ec_result);
			$user->getUserData();

			$_SESSION["paypal_express_checkout_details"] = $paypal_ec_result;
			$_SESSION["opc_shipping_address_id"] = $shipping_address_id;

			OrderProvider::updatePaymentMethod($order, $paypal_payment_method["pid"]);
			$db->query("SELECT * FROM ".DB_PREFIX."users_shipping WHERE usid='".$shipping_address_id."'");
			if ($db->moveNext())
			{
				$shipping_address_data = $db->col;
				OrderProvider::updateShippingAddress($order, $shipping_address_data);
			}
			$backurl = $url_https."p=one_page_checkout";
		}
	}
}

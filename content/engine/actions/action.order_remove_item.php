<?php
/**
 * Remove item from a cart
 */

	if (!defined("ENV")) exit();

	$ocid = isset($_REQUEST["ocid"]) ? intval($_REQUEST["ocid"]) : "";

	$promo_code = isset($_SESSION["order_promo_code"]) ? $_SESSION["order_promo_code"] : '';
	$promo_result = OrderProvider::removeItem($order, $ocid, $promo_code);

	if (!$promo_result)
	{ 
		$_SESSION["remove_item_promo_error"] = true;
	}

	$backurl = UrlUtils::getBackUrl();

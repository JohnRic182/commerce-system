<?php
/**
 * Process payment validation for cardinal commerce
 */	

	if (!defined("ENV")) exit();

	if (defined('DEVMODE') && DEVMODE)
		file_put_contents(
			'content/cache/log/payment-validation.txt', 
			date('r')."=============================================\r\n".
			"_REQUEST:\r\n".print_r($_REQUEST, 1)."\r\n\r\n".
			"_GET:\r\n".print_r($_GET, 1)."\r\n\r\n".
			"_POST:\r\n".print_r($_POST, 1)."\r\n\r\n".
			"_SESSION:\r\n".print_r(array_merge($_SESSION, array('CardinalPostForm' => array('cc_number' => str_repeat('X', 16), 'cc_cvv2' => str_repeat('X', 4)))), 1)."\r\n\r\n\r\n\r\n",
			FILE_APPEND
		);
	
	if (
		$user->auth_ok && 
		$order->itemsCount > 0 && 
		isset($_SESSION["CardinalRedirectUrl"]) && 
		isset($_SESSION["CardinalMD"]) && 
		isset($_REQUEST["MD"]) && 
		$_REQUEST["MD"] == $_SESSION["CardinalMD"]
	)
	{
		$redirectUrl = $_SESSION["CardinalRedirectUrl"];
		
		unset($_SESSION["CardinalMD"]);
		unset($_SESSION["CardinalRedirectUrl"]);
		
		$html = '<html><body style="font-family:Arial;background:white;">';
	
		//_GET vars
		$getParams = "";
		foreach($_GET as $key=>$value)
		{
			if ($key == "oa") $value = ORDER_PROCESS_PAYMENT;
			$getParams.= $key."=".urlencode($value)."&";
		}
	
		$html.= 
			'<form id="frm-validation-redirect" name="frm-validation-redirect" target="_top" method="post" action="'.$redirectUrl."?".$getParams.'">';
		
		foreach($_POST as $key=>$value)
		{
			$html.='<input type="hidden" name="'.$key.'" value="'.htmlspecialchars($value).'">';
		}
		
		$html.= 
			'</form>'.
			'Processing...'.
			'<script type="text/javascript">document.forms["frm-validation-redirect"].submit();</script>'.
			'</body></html>';	
	}
	else
	{
		$html = '<html><body style="font-family:Arial;background:white;">Processing error occured. Please do not refresh page or use browser\'s b"Back" button!</body></html>';
	}
	
	echo $html;
	
	_session_write_close();
	die();
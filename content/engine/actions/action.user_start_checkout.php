<?php
/**
 * Starts checkout
 */
	if (!defined("ENV")) exit();

	// check for paypal express checkout - in this case we have to go to cart page first
	$db->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."payment_methods WHERE id IN ('paypalec') AND active='Yes'");
	$db->moveNext();
	if ($db->col["c"] > 0 && $_REQUEST["checkout_start_page"] != "cart")
	{
		$backurl = $url_http."p=cart";
	}
	else
	{
		//if user already logged in
		if ($user->auth_ok || $userCookie)
		{
			//choose checkout mode and show first step
			$backurl = $url_https."p=one_page_checkout";
		}
		//or if user is now logged it
		else
		{
			//User MUST create account at this point
			if ($settings["AllowCreateAccount"] == "Yes")
			{
				//REMEMBER - on OPC we have to create environment to FORCE account creation and show Login option
				$user->expressLogin();
				if ($user->auth_ok)
				{
					$backurl = $url_https."p=one_page_checkout";
				}
				else
				{
					$_SESSION["user_started_checkout"] = true;
					$backurl = $url_https."p=login";
				}
			}
			//IF user is not forced to create account / may choose OR only use guest checkout
			else
			{
				//REMEMBER - on OPC we have to create environment to FORCE account creation and show Login option
				$user->expressLogin();
				if ($user->auth_ok)
				{
					$backurl = $url_https."p=one_page_checkout";
				}
				else
				{
					$backurl = $url_https."p=login";
				}
			}
		}
	}
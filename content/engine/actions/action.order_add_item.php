<?php
/**
 * Add item to a cart
 */

	if (!defined('ENV')) exit();

	$oa_id = isset($_REQUEST['oa_id']) ? $_REQUEST['oa_id'] : '';
	$oa_quantity = isset($_REQUEST['oa_quantity']) ? intval($_REQUEST['oa_quantity']) : 1;
	$oa_attributes = isset($_REQUEST['oa_attributes']) ? $_REQUEST['oa_attributes'] : array();
	$oa_attributes = is_array($oa_attributes) ? $oa_attributes : array();
	foreach ($oa_attributes as $key => $val)
	{
		$oa_attributes[$key] = urldecode($val);
	}
	$oa_custom_data = isset($_REQUEST['oa_custom_data']) ? is_array($_REQUEST['oa_custom_data']) ? $_REQUEST['oa_custom_data'] : array() : array();

	if ($settings['VisitorMayAddItem'] == 'YES' || ($user->auth_ok || $userCookie))
	{
		$itemAdded = OrderProvider::addItem($order, $oa_id, $oa_quantity, $oa_attributes, $oa_custom_data);

		if ($itemAdded)
		{
			if ($settings['AfterProductAddedGoTo'] == 'Cart Page' || $settings['InventoryStockUpdateAt'] == 'Add To Cart')
			{
				$backurl = $url_dynamic.'p=cart';
			}
			elseif ($settings['AfterProductAddedGoTo'] == 'Checkout Page')
			{
				if ($user->auth_ok)
				{
					$backurl = $url_dynamic . 'p=one_page_checkout';
				}
				else
				{
					$backurl = $url_dynamic . 'ua=user_expresscheckout';
				}
			}
			else
			{
				$_SESSION['HTTP_REFERER'] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
				$backurl = UrlUtils::getBackUrl();
			}
		}
		else
		{
			$flashMessages = new Model_FlashMessages($session);
			$flashMessages->setMessages($order->errors);
			
			$backurl = $url_dynamic."p=cart";
 		}
	}

<?php

/**
 * Add a new product review
 */

	if (!defined("ENV")) exit();

	if ($user->auth_ok || $userCookie)
	{
		// instantiate the reviews object
		$reviews = new Ddm_ProductReviews($db, $settings);

		print $reviews->addReview(array(
			'user_id' => $user->auth_ok ? $user->id : ($userCookie ? $userCookie["i"] : 0),
			'pid' => $_POST['pid'],
			'rating' => $_POST['rating'],
			'review_title' => $_POST['review_title'],
			'review_text' => $_POST['review_text']
		));
	}

	die();

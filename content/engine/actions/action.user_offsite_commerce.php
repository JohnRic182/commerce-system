<?php
/**
 * Process qr actions
 */

	if (!defined("ENV")) exit();
	
	if ($ua == USER_QR)
	{
		$_REQUEST["s"] = "qr";
	}
	
	$action = isset($_REQUEST["a"]) ? $_REQUEST["a"] : "h";
	$go = isset($_REQUEST["g"]) ? $_REQUEST["g"] : false;
    $id = isset($_REQUEST["i"]) ? base_convert($_REQUEST["i"], 36, 10) : false;
    $campaign_id = isset($_REQUEST["c"]) ? base_convert($_REQUEST["c"], 36, 10) : false;
    $order_source = isset($_REQUEST["s"]) ? $_REQUEST["s"] : false; 
	$backurl = $settings["GlobalHttpUrl"]."/";
	
	switch ($action)
	{
		// product page and add to cart
		case "a" :
		case "p" :
		{
			$r = $db->query("
				SELECT
					".DB_PREFIX."products.*,
					IF(".DB_PREFIX."products.url_custom='',".DB_PREFIX."products.url_default,".DB_PREFIX."products.url_custom) AS product_url
				FROM ".DB_PREFIX."products 
				WHERE 
					pid='".intval($id)."' AND 
					is_visible='Yes'
			");
			
			if (($product = $db->moveNext($r)) != false)
			{
				// add to cart
				if ($action == "a" && $product["attributes_count"] == 0)// && $product["inventory_control"] == "No")
				{
					$_GET["oa"] = $_POST["oa"] = $_REQUEST["oa"] = ORDER_ADD_ITEM;
                    $_GET["oa_quantity"] = $_POST["oa_quantity"] = $_REQUEST["oa_quantity"] = $product["min_order"];
					$_GET["oa_id"] = $_POST["oa_id"] = $_REQUEST["oa_id"] = $product["product_id"];

					if ($go !=='o')
					{
						$backurl = $settings['GlobalHttpsUrl'] . '/index.php?p=cart';
						$settings['AfterProductAddedGoTo'] = 'Cart Page';
					}
					else
					{
						$backurl = $settings['GlobalHttpsUrl'] . '/index.php?ua=user_expresscheckout';
						$settings['AfterProductAddedGoTo'] = 'Checkout Page';
					}
				}
				// go to product page
				else
				{
					$backurl = str_replace('&amp;', '&', UrlUtils::getProductUrl($product["product_url"], $product["pid"]));
				}
				
				$_SESSION["osc_source"] = $order_source;
				$_SESSION["osc_campaign_id"] = $campaign_id;
			}
			
			break;
		}
		
		// category
		case "c" :
		{
			$r = $db->query("
				SELECT
					".DB_PREFIX."catalog.*,
					IF(".DB_PREFIX."catalog.url_custom='',".DB_PREFIX."catalog.url_default,".DB_PREFIX."catalog.url_custom) AS category_url
				FROM ".DB_PREFIX."catalog 
				WHERE 
					cid='".intval($id)."' AND 
					is_visible='Yes'
			");
			
			if (($category = $db->moveNext($r)) != false)
			{
				$backurl = UrlUtils::getCatalogUrl($category["category_url"], $category["cid"]);
				
				$_SESSION["osc_referer"] = true;
				$_SESSION["osc_source"] = $order_source;
				$_SESSION["osc_campaign_id"] = $campaign_id;
			}
			break;
		}	
	}

	

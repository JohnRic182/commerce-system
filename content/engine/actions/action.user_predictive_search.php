<?php
/**
 * User predictive search
 */

if (!defined("ENV")) exit();

$searchService = new Search_ServicePredictive(
	new DataAccess_ProductsRepository($db),
	new DataAccess_CategoryRepository($db),
	new DataAccess_ManufacturerRepository($db),
	$settings
);

$searchStr = $_REQUEST['search_str'];
$results = $searchService->search($searchStr);

echo json_encode($results);

die();
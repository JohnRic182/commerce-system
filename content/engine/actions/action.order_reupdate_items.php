<?php
/**
 * Recalculte order amounts
 */

	if (!defined("ENV")) exit();
	
	if($user->auth_ok)
	{
		OrderProvider::reupdateOrder($order);
	}

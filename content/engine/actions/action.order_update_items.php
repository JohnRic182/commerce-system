<?php
/**
 * Update order items
 */

if (!defined("ENV")) exit();

if ($settings["VisitorMayAddItem"] == "YES" || ($user->auth_ok || $userCookie))
{
	/**
	 * Update quantities
	 */
	$oa_quantity = isset($_REQUEST["oa_quantity"]) ? $_REQUEST["oa_quantity"] : array();
	$oa_quantity = is_array($oa_quantity) ? $oa_quantity : array();

	$updateResult = OrderProvider::updateItems($order, $oa_quantity);

	if (!$updateResult)
	{
		$_REQUEST["oa_todo"] = 'update';

		//TODO: The variable $session should already be set in this context, why reset it?
		//$session = new Framework_Session(new Framework_Session_WrapperStorage());
		$flashMessages = new Model_FlashMessages($session);
		$flashMessages->setMessages($order->errors, Model_FlashMessages::TYPE_ERROR);
	}

	/**
	 * What are we going to do after item updated
	 */
	$oa_todo = isset($_REQUEST["oa_todo"]) ? $_REQUEST["oa_todo"] : "update";
	switch ($oa_todo)
	{
		case "continue" :
		{
			$backurl = trim($settings['AfterContinueShoppingClickGoTo']) != '' ? trim($settings['AfterContinueShoppingClickGoTo']) : $url_http;
			break;
		}
		case "checkout" :
		{
			$backurl = $url_checkout;
			break;
		}
		case "update" :
		default :
		{
			// do nothing
		}
	}

	/**
	 * Check min subtotal amount
	 */
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
	$min_subtotal_amount = normalizeNumber($settings["MinOrderSubtotalLevel".$userLevel]);

	if ($oa_todo != 'continue' && $min_subtotal_amount > $order->subtotalAmount)
	{
		$backurl = false;
	}

	/**
	 * Check promo code
	 */
	$promoResult = true;

	if ($settings["DiscountsPromo"] == "YES")
	{
		// Reset promo first
		$order->resetPromo(false);

		// Get form request
		$promoCode = isset($_REQUEST['promo_code']) ? trim($_REQUEST['promo_code']) : "";

		// Check promo code
		if (trim($promoCode) != '' && validate($promoCode, VALIDATE_STRING))
		{
			$promoResult = $order->checkPromoCode($promoCode);
		}

		// Save to session or return error
		if ($promoResult)
		{
			$_SESSION["order_promo_code"] = $promoCode;

			//have to recalculate subtotals here to show promo result
			$order->recalcSubtotals();
		}
		else
		{
			unset($_SESSION["order_promo_code"]);
			$backurl = false;
			$promo_error = true;
		}
	}
}

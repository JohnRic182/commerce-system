<?php
/**
 * User signup
 */

	if (!defined("ENV")) exit();

	// try captcha first of all
	$captchaError = false;
	if ($settings['captchaMethod'] != 'None')
	{
		$recaptcha = new \ReCaptcha\ReCaptcha($settings['captchaReCaptchaPrivateKey']);
		$response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER["REMOTE_ADDR"]);
		if (!$response->isSuccess())
		{
			$captchaErrors = $response->getErrorCodes();
			foreach ($captchaErrors as $error)
			{
				$user->setError("Captcha Error: $error");
			}
			$captchaError = true;
		}
	}
	
	// try to process form
	$form = isset($_POST["form"]) ? $_POST["form"] : array();
	$form = is_array($form) ? $form : array();

	$customFieldsData = isset($_POST["custom_field"]) ? $_POST["custom_field"] : array();
	
	$user_id = ($user->auth_ok && $user->express) ? $user->id : false;
	
	if (!$captchaError && $user->signUp($form, $customFieldsData, $user_id))
	{
		Notifications::emailNewUserRegistered($db, $user->id, $customFields);

		$user->login($form["login"], $form["password"]);
	
		if ($user->auth_ok)
		{
			if (isset($_SESSION["wl_pid"]))
			{	
				$backurl = $url_https."ua=".USER_ADD_TO_WISHLIST."&wl_action=addto_wishlist&pid=".$_SESSION["wl_pid"];

				unset($_SESSION["wl_pid"]);
			}
			elseif (isset($_SESSION["user_started_checkout"]))
			{
				$backurl = $url_https."oa=".ORDER_OPC;
			}
			else
			{				
				$backurl = $url_https."p=account";
			}
		
			//set tracking cookie
			$user->setCookie();
			
			session_regenerate_id(true);

			// if http and https domain names are different, set cookie on http site too
			if ($user->isCrossDomainCookieRequired())
			{
				$backurl = $user->getCrossDomainCookieUrl($url_http, $backurl);
			}

			unset($_SESSION["user_started_checkout"]);
		}
	}

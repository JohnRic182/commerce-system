<?php
/**
 * Process users login 
 */

	if (!defined("ENV")) exit();
	
	if ($user->auth_ok)
	{
		if ($user->express)
		{
			$remove_user = $user->id;
		}
		else
		{
			$user->logOut();
		}
	}

	$login_successful = $user->login(
		isset($_POST["login"]) ? trim($_POST["login"]) : "",
		isset($_POST["password"]) ? trim($_POST["password"]) : "",
		isset($_POST["remember_me"])
	);
	
	if ($login_successful)
	{
		if (isset($remove_user) && $remove_user)
		{
			$db->query("DELETE FROM ".DB_PREFIX."users WHERE uid='".intval($remove_user)."'");
		}
		
		$oa = ORDER_RE_UPDATE_ITEMS;

		if (isset($_SESSION["wl_pid"]))
		{
			$backurl = $url_https."ua=".USER_ADD_TO_WISHLIST."&wl_action=addto_wishlist&pid=".$_SESSION["wl_pid"];

			unset($_SESSION["wl_pid"]);
		}
		elseif (isset($_SESSION["user_started_checkout"]))
		{
			$backurl = $url_https."oa=".ORDER_OPC;
		}			
		elseif (isset($_SESSION["after_login_url"]))
		{
			$backurl = str_replace("pcsid=".session_id(), "", $_SESSION["after_login_url"]);
			unset($_SESSION["after_login_url"]);
		}
		else
		{
			$backurl = $url_https."p=account";
		}
		
		//set tracking cookie
		$user->setCookie();

		session_regenerate_id(true);
		
		// if http and https domain names are different, set cookie on http site too
		if ($user->isCrossDomainCookieRequired())
		{
			$backurl = $user->getCrossDomainCookieUrl($url_http, $backurl);
		}
		
		unset($_SESSION["user_started_checkout"]);
	}

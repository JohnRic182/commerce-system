<?php
/**
 * Process payment
 */

if (!defined('ENV')) exit();

if ($user->isAuthorized() && $order->getItemsCount() > 0)
{
	/**
	 * Gift Certificate
	 */
	if ($oa = ORDER_PROCESS_PAYMENT && isset($_SESSION['order_gift_certificate']))
	{
		$giftCertificateForm = $_SESSION['order_gift_certificate'];

		$gcFirstName = trim($giftCertificateForm['first_name']);
		$gcLastName = trim($giftCertificateForm['last_name']);
		$gcVoucher = trim($giftCertificateForm['voucher']);

		if ($gcFirstName != '' && $gcLastName != '' && $gcVoucher != '')
		{
			$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
			$giftCertData = $giftCertificateRepository->getGiftCertificate($gcFirstName, $gcLastName, $gcVoucher);

			if ($giftCertData)
			{
				$giftCertificateRepository->updateOrderGiftCertificate($order->getId(), $gcFirstName, $gcLastName, $gcVoucher);

				if ($order->getTotalAmount() > $giftCertData['balance'])
				{
					$giftCertificateRepository->updateOrderGiftCertAmountApplied($order->getId(), $giftCertData['balance']);
					$order->setGiftCertificateAmount($giftCertData['balance']);
				}
			}
		}
	}


	// TODO: get from registry
	$paymentProvider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings)));
	$paymentProcessor = $paymentProvider->getPaymentProcessor($order);

	if ($paymentProcessor)
	{
		// set to true by default
		$paymentError = true;

		if ($paymentProcessor->steps == 1)
		{
			//return entered values of background
			//orders processing
			$form = isset($_POST['form']) ? $_POST['form'] : array();
			$form = is_array($form) ? $form : array();
		}
		else
		{
			//return POST VARS passed by
			//payment gateway
			$form = $_REQUEST;
		}

		if (isset($form['cc_type']))
		{
			$form['cc_type'] = str_replace(array('Yes~', 'No~'), array('', ''), $form['cc_type']);
		}

		if (isset($form['payment_profile_id']))
		{
			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $registry->get('repository_payment_profile');

			/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
			$paymentProfile = $paymentProfileRepository->getById($form['payment_profile_id'], $user->getId());
			if (!is_null($paymentProfile))
			{
				$form['ext_profile_id'] = $paymentProfile->getExternalProfileId();
			}
		}

		if (isset($form['cc_number']))
		{
			$form['cc_number'] = preg_replace('/\s+/', '', $form['cc_number']);
		}

		$paymentResult = $paymentProcessor->process($db, $user, $order, $form);

		if ($paymentResult)
		{
			/**
			 * Payment was successful. Finish order now.
			 */
			$paymentError = false;

			$sendNotification = $paymentProcessor->sendEmailNotification($paymentResult);

			$orderStatus = ORDER::STATUS_PROCESS;

			if ($paymentResult === true)
			{
				$paymentResult = ORDER::PAYMENT_STATUS_RECEIVED;
			}

			if (!$order->getShippingRequired() && $paymentResult === ORDER::PAYMENT_STATUS_RECEIVED)
			{
				$orderStatus = ORDER::STATUS_COMPLETED;
			}

			$event = new Events_OrderStatusEvent(Events_OrderStatusEvent::ON_PROCESS_PAYMENT);
			$event->setOrder($order);
			$event->setOrderStatus($orderStatus);
			$event->setPaymentStatus($paymentResult);
			Events_EventHandler::handle($event);

			$orderStatus = $event->getOrderStatus();
			$paymentResult = $event->getPaymentStatus();

			$orderSource = isset($_SESSION["osc_source"]) ? $_SESSION["osc_source"] : null;
			$offsiteCampaignId = isset($_SESSION["osc_campaign_id"]) ? $_SESSION["osc_campaign_id"] : null;

			OrderProvider::closeOrder(
				$order, $form,
				$customFields, $orderStatus, $paymentResult,
				$orderSource, $offsiteCampaignId, $sendNotification
			);

			$backurl = $url_https."pcsid=".session_id()."&p=completed";
			$paymentProcessor->success();

			// TODO: looks like this code is not used any more
			if (isset($paymentProcessor->reload_parent_on_process) && $paymentProcessor->reload_parent_on_process)
			{
				echo
					'<!DOCTYPE HTML>'."\n".
					'<html>'."\n".
					'<body>'."\n".
					'	<script type="text/javascript">'."\n".
					'		parent.location = "'.$backurl.'";'."\n".
					'	</script>'."\n".
					'</body>'."\n".
					'</html>'."\n";

				$db->done();
				_session_write_close();
				exit();
			}
		}

		$paymentRedirect = $paymentProcessor->redirect;
		$paymentRedirectUrl = $paymentProcessor->redirectURL;

		if ($paymentError)
		{
			// TODO: use flash messages?
			$paymentErrorMessage = (isset($paymentProcessor) && $paymentProcessor) ? $paymentProcessor->error_message : 'Critical error: Could not create gateway class instance. Please contact to site administrator.';

			/**
			 * Check is engine set to block users accounts
			 * Possible cc hack protection
			 */
			if ($settings['SecurityAccountBlocking'] == 'YES')
			{
				$order->checksFailedPaymentAttempts(intval($settings['SecurityAccountBlockingAttempts']));

				if ($order->getStatus() == ORDER::STATUS_FAILED)
				{
					/**
					 * Close current order and log out current user
					 */
					OrderProvider::failOrder(
						$order, $form,
						false,
						isset($_SESSION['osc_source']) ? $_SESSION['osc_source'] : null,
						isset($_SESSION['osc_campaign_id']) ? $_SESSION['osc_campaign_id'] : null
					);

					$user->block($settings['SecurityAccountBlockingHours']);
					$user->logOut();

					$account_blocked = true;
					$backurl = $url_https.'p=blocked';
				}
			}
		}
	}
}
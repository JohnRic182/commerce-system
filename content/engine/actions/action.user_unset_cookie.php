<?php
/**
 * Unsets HTTP cookie after user's login out in HTTPS mode and domain names are different
 */
	
	if (!defined("ENV")) exit();
	
	$user->unsetCookie();
	
	$backurl = $url_http."p=login";
		
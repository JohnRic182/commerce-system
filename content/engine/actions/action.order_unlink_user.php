<?php

	$order->unlinkUser();

	if ($settings["ClearCartOnLogout"] == "Yes")
	{
		OrderProvider::clearItems($order);
	}

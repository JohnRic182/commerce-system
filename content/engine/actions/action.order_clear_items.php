<?php
/**
 * Clear cart items
 */

	if (!defined("ENV")) exit();
	
	OrderProvider::clearItems($order);
	$backurl = UrlUtils::getBackUrl();
	
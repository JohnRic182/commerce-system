<?php
/**
 * User password reset
 */

	if (!defined("ENV")) exit();
	
	// trying to reset password
	$resetPasswordSent = $user->passwordReset(
		isset($_POST["login"]) ? trim($_POST["login"]) : "",
		isset($_POST["email"]) ? trim($_POST["email"]) : ""
	);

	/* we set to return true always for security purposes as per PINNACLE-4140 */
	$resetPasswordSent = true;
	$resetPasswordProbe = true;

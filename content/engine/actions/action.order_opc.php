<?php
/**
 * Process one page checkout request
 */

	if (!defined("ENV")) exit();
	
	define('OPC_MSG_ERROR_DATA_MISSED', 1);

	header('Cache-Control: no-cache, no-store, must-revalidate');
	header('Pragma: no-cache');

	/**
	 * Init 
	 */
	$action = isset ($_REQUEST["action"]) ? $_REQUEST["action"] : false;
	$data = isset ($_POST["data"]) ? json_decode($_POST["data"]) : false;
	
	$opc = new OPC_Response();
	$getShippingMethods = false;
	$recheckGiftCertificate = true;
	$processOrderData = true; // should be set to false after successful payment completed

	$addressUpdated = false;

	$bongoActive = false;
	$bongoUsed = false;

	//check min subtotal amount
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
	$min_subtotal_amount = normalizeNumber($settings["MinOrderSubtotalLevel".$userLevel]);

	if ($min_subtotal_amount > $order->subtotalAmount)
	{
		$opc->setError($msg["cart"]["minimal_sustotal_not_reached"]."\n".$msg["cart"]["minimal_sustotal"].": ".getPrice($min_subtotal_amount));
	}
	elseif (!$action || !$data)
	{
		$opc->setError("Error processing form");
	}
	else
	{
		/**
	 	 * Process action
 	 	 */
		switch ($action)
		{
			case "doLogin" :
			{
				$_POST["login"] = $data->username;
				$_POST["password"] = $data->password;
				$opc_login = true;
				unset($_POST["remember_me"]); 
				require_once("content/engine/actions/action.user_login.php");
				if (!$login_successful)
				{
					$opc->setError("Please enter valid username and password");
				}
				break;
			}
			/**
			 * Save billing data
			 */
			case "saveBillingData" : 
			{
				//billing form
				$billingForm = $opc->unserialize($data->billingForm);

				$customFieldsData = isset($billingForm["custom_field"]) ? $billingForm["custom_field"] : array();
				unset($billingForm["custom_field"]);
				unset($billingForm["custom_field_required"]);
				unset($billingForm["custom_field_name"]);
				unset($billingForm["custom_field_place"]);

				// try to validate billing data with USPS
				if ($data->shippingAsBilling && $settings['EndiciaUspsAddressValidation'] == 1)
				{
					if (!isset($billingForm["payment-profile-id"]) || (isset($billingForm["payment-profile-id"]) && $billingForm["payment-profile-id"] == 'billing'))
					{
						// check is address correct, and if it is not, just exit
						if (!AddressValidator::validate($db, $billingForm, $msg))
						{
							$opc->setError(AddressValidator::getLastError());
							break;
						}
						else
						{
							$addressVariants = AddressValidator::getAddressVariants();
							if (!is_null($addressVariants) && $addressVariants)
							{
								$opc->setData("shippingAddressVariants", $addressVariants);
							}
						}
					}
				}

				//check data to convert current express checkout to normal user
				$positiveResult = false;
				if (isset($billingForm["create_account"]) && $billingForm["create_account"])
				{
					//try to create account
					if (!$user->signUp($billingForm, $customFieldsData, $user->id))
					{
						$opc->setError($user->errors);		
					}
					else
					{
						Notifications::emailNewUserRegistered($db, $user->id, $customFields);
						
						$user->login($billingForm["login"], $billingForm["password"]);
						
						if ($user->auth_ok)
						{
							//set tracking cookie
							$user->setCookie();

							// if http and https domain names are different, set cookie on http site too
							if ($user->isCrossDomainCookieRequired())
							{
								$opc->setData("backgroundURL", $user->getCrossDomainCookieUrl($url_http, false));
							}

							unset($_SESSION["user_started_checkout"]);
							
							$positiveResult = true;
						
							$opc->setData("accountCreated", true);
						}
						else
						{
							$opc->setError("Unknown error happened");	
						}
					}
				}
				else
				{
					//check is payment profile used
					if (isset($billingForm['payment-profile-id']) && $billingForm['payment-profile-id'] != 'billing')
					{
						$paymentProfileId = $billingForm['payment-profile-id'];

						/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
						$paymentProfileRepository = $registry->get('repository_payment_profile');
						$paymentProfile = $paymentProfileRepository->getById($paymentProfileId, $user->getId());

						if (!is_null($paymentProfile))
						{
							$user->updateBillingInfoFromPaymentProfile($paymentProfile);
							$positiveResult = true;
						}
					}
					else
					{
						//try to store billing data
						if (!$user->updateBillingInfo($billingForm, $customFieldsData))
						{
							$opc->setError($user->errors);
						}
						else
						{
							$positiveResult = true;
							$opc->setData('accountCreated', false);
						}
					}
				}

				if ($positiveResult)
				{
					$user->getUserData();
					if ($settings['TaxAddress'] == 'Billing')
					{
						$order->reupdateItems();
					}

					$billingData = $user->getBillingInfo();
					$opc->setData("billingData", $billingData);

					//update shipping address when needed
					if ($data->shippingAsBilling)
					{
						if (isset($billingForm['address_type']))
						{
							$billingData['address_type'] = $billingForm['address_type'];
						}
						else
						{
							$billingData['address_type'] = isset($billingData['company']) && trim($billingData['company']) != '' ? 'Business' : 'Residential';
						}

						OrderProvider::updateShippingAddress($order, $billingData);

						/**
						 * Set updated data
						 */
						$opc->setData("shippingAddress", $order->getShippingAddress());
						
						$getShippingMethods = true;
					}
				}

				$addressUpdated = true;
				break;
			}
			
			/**
			 * Check promo code
			 */
			case 'checkPromoCode' :
			{
				$promoResult = false;

				// Reset promo first
				$order->resetPromo(false);

				// Get form data
				$promoCode = $data->promo;
				
				// Check promo code
				if (trim($promoCode) != '' && validate($promoCode, VALIDATE_STRING))
				{
					$promoResult = $order->checkPromoCode($promoCode);
				}
				else
				{
					$order->resetPromo(false);
					$order->calculateTax();
				}

				// Save to session or return error
				if ($promoResult)
				{
					$_SESSION["order_promo_code"] = $promoCode;
				}
				else
				{
					unset($_SESSION["order_promo_code"]);
					if (trim($promoCode) != '')
					{
						$opc->setError("Entered promo code is expired, cannot be used with your order or is invalid");
					}
				}

				// Set response
				$opc->setData("promoResult", $promoResult);

				$getShippingMethods = true;
				
				break;
			}
			
			/**
			 * Save shipping data
			 */
			case 'saveShippingAddress' :
			{
				//update shipping address when needed
				$shippingForm = $opc->unserialize($data->shippingForm);
				
				//custom fields
				$customFieldsData = isset($shippingForm["custom_field"]) ? $shippingForm["custom_field"] : array();
				unset($shippingForm["custom_field"]);
				unset($shippingForm["custom_field_required"]);
				unset($shippingForm["custom_field_name"]);
				unset($shippingForm["custom_field_place"]);
				
				$shippingAddressChanged = false;

				/**
				 * Shipping the same as billing
				 */
				if ($shippingForm["shipping-address-id"] == "billing")
				{
					$billingData = $user->getBillingInfo();
					$billingData['address_type'] = isset($billingData['company']) && trim($billingData['company']) != '' ? 'Business' : 'Residential';
					$shippingForm = array_merge($shippingForm, $billingData);
				}
				/**
				 * Shipping address from address book
				 */
				elseif (is_numeric($shippingForm["shipping-address-id"]))
				{
					/** @var CustomFields $customFields */
					$shippingForm = array_merge($shippingForm, $user->getShippingAddressById(intval($shippingForm["shipping-address-id"])));
					$customFields = $order->customFields()->getCustomFieldsValues('shipping', $user->id, 0, $shippingForm["shipping-address-id"]);
					$customFieldsData = array();
					foreach ($customFields as $customField)
					{
						$customFieldsData[$customField['field_id']] = trim($customField['value']) == '' ? 'n/a' : $customField['value'];
					}
				}
				else
				{
					// we suppose that address is new
				}

				/**
				 * Validate address
				 */
				if (!$user->validateAddress($shippingForm, USER::ADDRESS_SHIPPING))
				{
					$opc->setError($user->errors);
				}

				if (is_array($customFieldsData))
				{
					$order->customFields()->resetErrors();
					$order->customFields()->validateCustomFields("shipping", $customFieldsData);

					if ($order->customFields()->is_error)
					{
						$opc->setError($order->customFields()->errors);
					} 
				}

				if ($opc->errors()) break;

				// try to validate shipping data with USPS
				if ($settings['EndiciaUspsAddressValidation'] == 1)
				{
					// check is address correct, and if it is not, just exit
					if (!AddressValidator::validate($db, $shippingForm, $msg))
					{
						$opc->setError(AddressValidator::getLastError());
						break;
					}
					else
					{
						$addressVariants = AddressValidator::getAddressVariants();
						if (!is_null($addressVariants) && $addressVariants)
						{
							$opc->setData("shippingAddressVariants", $addressVariants);
						}
					}
				}

				$_SESSION["opc-shipping-address-id"] = $shippingForm["shipping-address-id"];

				/**
				 * Save shipping address
				 */
				OrderProvider::updateShippingAddress($order, $shippingForm, $customFieldsData);
				
				/**
				 * Set updated data
				 */
				$opc->setData("shippingAddress", $order->getShippingAddress());
				
				/**
				 * Get shipping methods
				 */
				$getShippingMethods = true;

				$addressUpdated = true;

				break;
			}
			
			/**
			 * Get shipping methos
			 */
			case 'getShippingMethods' :
			{
				$getShippingMethods = true;
				break;
			}

			/**
			 * Set shipping method
			 */
			case 'setShippingMethod' :
			{
				// Check shipping method
				if (isset($data->shipmentsMethods))
				{
					$shipmentsMethods = json_decode($data->shipmentsMethods, true);
					if ($shipmentsMethods)
					{
						$order->updateShipmentsMethods($shipmentsMethods && is_array($shipmentsMethods) ? $shipmentsMethods : array());
						$order->persist();
					}
				}

				break;
			}
			
			/**
			 * Apply gift certificate
			 */
			case 'applyGiftCertificate' :
			{
				// Check and apply gift certificate
				$giftCertificateForm = $opc->unserialize($data->giftCertificateForm);
				
				$giftError = true;
				
				$gcFirstName = trim($giftCertificateForm["gift_cert_first_name"]);
				$gcLastName = trim($giftCertificateForm["gift_cert_last_name"]);
				$gcVoucher = trim($giftCertificateForm["gift_cert_voucher"]);
				
				if ($gcFirstName != '' && $gcLastName != '' && $gcVoucher != '')
				{
					$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
					$giftCertData = $giftCertificateRepository->getGiftCertificate($gcFirstName, $gcLastName, $gcVoucher);
					
					// Note: we only validating gift certificate without applying
					if ($giftCertData) 
					{
						// gift certificate is correct
						$giftError = false;
						$order->setGiftCertificateAmount($giftCertData["balance"]);

						$_SESSION["order_gift_certificate"] = array(
							"first_name" => $giftCertData['first_name'],
							"last_name" => $giftCertData['last_name'],
							"voucher" => $giftCertData['voucher']
						);
					}
					else
					{
						$order->setGiftCertificateAmount(0);

						unset($_SESSION["order_gift_certificate"]);
					}
				}

				if ($giftError)
				{
					$opc->setError("Provided gift certificate is not valid");
				}
			
				$recheckGiftCertificate = false;

				break;
			}

			/**
			 * Get payment methods
			 */
			case 'getPaymentMethods':
			{
				// TODO: move into registry
				$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings));

				$paymentMethods = $paymentMethodRepository->getPaymentMethods(true, $order);

				$paymentMethodsForOpc = array();

				if ($paymentMethods !== null)
				{
					/**@var Payment_Method $paymentMethod */
					foreach ($paymentMethods as $paymentMethod)
					{
						if ($paymentMethod->getCode() == 'paypalec' && !isset($_SESSION['paypal_express_checkout_details']))
						{
							$paymentMethod->setType(Payment_Method::TYPE_IPN);
							//$paymentMethod->steps = 2;
						}

						if ($paymentMethod->getCode() == 'shift4i4go')
						{
							$paymentMethod->setType(Payment_Method::TYPE_CC);
						}

						$paymentMethodsForOpc['p'.$paymentMethod->getId()] = $paymentMethod;
					}
				}

				$opc->setData('paymentMethods', Payment_View_PaymentMethodView::getPaymentMethods($paymentMethodsForOpc));
				$opc->setData('paymentMethodsCount', count($paymentMethods));
				break;
			}

			/**
			 * Process payment
			 */
			case 'processPayment' :
			{
				// TODO: move into event?
				$integrityErrors = array();
				if (!$order->checkDataIntegrity($db, $integrityErrors))
				{
					foreach ($integrityErrors as $integrityError) $opc->setError($integrityError);
					$opc->setData('integrityError', true);
				}

				$event = new Events_OrderEvent(Events_OrderEvent::ON_BEFORE_PAYMENT);
				$event->setOrder($order);
				Events_EventHandler::handle($event);

				if ($event->hasErrors())
				{
					foreach ($event->getErrors() as $eventError) $opc->setError($eventError);
				}

				// extract data from post
				$paymentMethodsForm = $opc->unserialize($data->paymentMethodsForm);
				$paymentMethodForm = $opc->unserialize($data->paymentMethodForm);

				OrderProvider::updatePaymentMethod($order, $paymentMethodsForm['payment-method-id']);

				// Check additional information
				if ($data->additionalInformation)
				{
					$additionalInformation = $opc->unserialize($data->additionalInformation);
					
					//gift message
					if ($settings["GiftCardActive"] == "YES")
					{
						if (isset($additionalInformation["add_gift"]) && $additionalInformation["add_gift"] == "yes")
						{
							$order->storeGiftMessage(isset($additionalInformation["gift_message"]) ? $additionalInformation["gift_message"] : "");
						}
					}
					
					//custom fields
					$customFieldsData = isset($additionalInformation["custom_field"]) ? $additionalInformation["custom_field"] : array();

					//store custom fields
					if (is_array($customFieldsData))
					{
						if (!$user->customFields()->validateCustomFields("invoice", $customFieldsData))
						{
							$opc->setError($user->customFields()->errors);
						}
						else
						{
							$customFields->storeCustomFields("invoice", $customFieldsData, $user->id, $order->oid, 0);
						}
					}
				}
				
				// Recheck promo code
				if (isset($_SESSION["order_promo_code"]) && $_SESSION["order_promo_code"] != '')
				{
					$promoResult = $order->checkPromoCode($_SESSION["order_promo_code"]);
					if (!$promoResult)
					{
						$_SESSION["order_promo_code"] = '';
						$opc->setError("Entered promo code is expired, cannot be used with your order or is invalid");
					}
				}

				$giftCertificateSet = false;

				// Recheck and apply gift certificate
				if ($data->giftCertificateForm)
				{
					$giftError = true;

					$giftCertificateForm = $opc->unserialize($data->giftCertificateForm);
					
					$gcFirstName = trim($giftCertificateForm['gift_cert_first_name']);
					$gcLastName = trim($giftCertificateForm['gift_cert_last_name']);
					$gcVoucher = trim($giftCertificateForm['gift_cert_voucher']);
				
					if ($gcFirstName != '' && $gcLastName != '' && $gcVoucher != '')
					{
						$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
						$giftCertData = $giftCertificateRepository->getGiftCertificate($gcFirstName, $gcLastName, $gcVoucher);
					
						if ($giftCertData) 
						{
							$_SESSION['order_gift_certificate'] = array(
								'first_name' => $giftCertData['first_name'],
								'last_name' => $giftCertData['last_name'],
								'voucher' => $giftCertData['voucher']
							);

							// gift certificate is correct
							$giftError = false;
							$giftCertificateRepository->updateOrderGiftCertificate($order->getId(), $gcFirstName, $gcLastName, $gcVoucher);
						
							if ($order->getTotalAmount() > $giftCertData['balance'])
							{
								$giftCertificateRepository->updateOrderGiftCertAmountApplied($order->getId(), $giftCertData['balance']);
								$order->setGiftCertificateAmount($giftCertData['balance']);
							}
							else
							{
								$giftCertificateRepository->updateOrderGiftCertAmountApplied($order->getId(), $order->getTotalAmount());
								$order->setGiftCertificateAmount($order->getTotalAmount());
								OrderProvider::updatePaymentMethod($order, 'free');
							}

							$giftCertificateSet = true;
						}
					}

					if ($giftError)
					{
						$opc->setError('Please check gift certificate');
					}
				}

				if (!$giftCertificateSet) unset($_SESSION['order_gift_certificate']);
				
				// define the backurl var
				$backurl = UrlUtils::getBackUrl();

				// get order data
				$order->getOrderData();
				
				// validate and update payment method
				$paymentMethodFreeError = false;
				
				if ((strtolower($paymentMethodsForm["payment-method-id"]) == "free" || $paymentMethodsForm["payment-method-id"] == 0) && ($order->getTotalAmount() > 0) && ($order->getGiftCertificateAmount() != $order->getTotalAmount()))
				{
					$paymentMethodFreeError = true;
				}
				
				if ($paymentMethodFreeError || !OrderProvider::updatePaymentMethod($order, $paymentMethodsForm["payment-method-id"]))
				{
					$opc->setError("Please select payment method");
				}

				// Process payment
				if (!$opc->errors())
				{
					if ($data->paymentMethodType != 'ipn')
					{
						foreach ($paymentMethodForm as $key=>$value)
						{
							$_REQUEST[$key] = $value;
							$_POST[$key] = $value;
						}

						if ($data->paymentMethodType != 'custom' && $data->paymentMethodType != 'free')
						{
							$paymentRedirect = false;
							$paymentRedirectUrl = null;
							$paymentError = false;
							$paymentErrorMessage = null;

							require_once("content/engine/actions/action.order_process_payment.php");

							if ($paymentRedirect && !is_null($paymentRedirectUrl))
							{
								$opc->setData("backURL", $paymentRedirectUrl);
								$opc->setData("verification", true);
							}
							else if ($paymentError)
							{
								$event = new Events_OrderEvent(Events_OrderEvent::ON_FAILED_PAYMENT);
								$event->setOrder($order);
								Events_EventHandler::handle($event);

								if (isset($account_blocked) && $account_blocked)
								{
									$opc->setData("backURL", $backurl);
									$processOrderData = false;
									$recheckGiftCertificate = false;
								}
								$opc->setData("paymentErrors", $paymentErrorMessage);
								$opc->setData("success", false);
							}
							else
							{
								$opc->setData("backURL", $backurl);
								$opc->setData("success", true);
								$processOrderData = false;
							}
						}
						else
						{
							require_once('content/engine/actions/action.order_complete_order.php');
							$opc->setData('backURL', $backurl);
							$opc->setData('success', true);
							$processOrderData = false;
						}

						break;
					}
				}
				else
				{
					$event = new Events_OrderEvent(Events_OrderEvent::ON_FAILED_PAYMENT);
					$event->setOrder($order);
					Events_EventHandler::handle($event);
				}

				// NOTE: do not break here. Under some conditions next step may be executed
			}
			/**
			 * Get payment form
			 */
			case 'getPaymentForm' :
			{
				// Get form data
				$paymentMethodsForm = $opc->unserialize($data->paymentMethodsForm);
				$paymentMethodId = isset($paymentMethodsForm['payment-method-id']) ? $paymentMethodsForm['payment-method-id'] : false;
				$paymentProfileId = isset($data->paymentProfileId) ? $data->paymentProfileId : 'billing';

				if ($paymentProfileId != 'billing')
				{
					//TODO: The variable $session should already be set in this context, why reset it?
					//$session = new Framework_Session(new Framework_Session_WrapperStorage());
					$session->set('opc-payment-profile-id', $paymentProfileId);
				}

				// Update payment method
				OrderProvider::updatePaymentMethod($order, $paymentMethodId);
				
				$override_payment_form = false;
				$payment_form_wide = false;
				$payment_form_override = false;
				$payment_in_popup = false;
				$payment_in_popup_width = 800;
				$payment_in_popup_height = 600;
				$payment_in_popup_title = 'Payment';
				
				$paymentForm = "";
				
				$_SESSION["opc-payment-method-id"] = $_SESSION["pc_method_id"] = $order->paymentMethodId;

				// Free order
				if ($paymentMethodId == 0)
				{
					$opc->setData('paymentForm', false);
					$opc->setData('paymentFormValidatorJS', false);
				}

				else
				{
					// TODO: move into registry
					$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings));

					$paymentMethod = $paymentMethodRepository->getById($order->getPaymentMethodId());

					if (!is_null($paymentMethod))
					{
						// PayPalEC
						if ($paymentMethod->getCode() == 'paypalec' && !isset($_SESSION["paypal_express_checkout_details"]))
						{
							$paymentMethod->setType(Payment_Method::TYPE_IPN);
						}

						// Get data required to render form
						$order->getOrderData();

						// ..and set template variables
						view()->assign('order_num', $order->getOrderNumber());
						view()->assign('order', $order);
						view()->assign('payment_method', new Payment_View_PaymentMethodView($paymentMethod));

						if ($paymentProfileId != 'billing')
						{
							/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
							$paymentProfileRepository = $registry->get('repository_payment_profile');
							$paymentProfile = $paymentProfileRepository->getById($paymentProfileId, $user->getId());

							if ($paymentProfile->getPaymentMethodId() == $order->getPaymentMethodId())
							{
								view()->assign('paymentProfile', $paymentProfile ? new PaymentProfiles_View_PaymentProfileView($paymentProfile) : false);
							}
						}

						if ($order->paymentIsRealtime == "Yes")
						{
							$order->getShippingAddress();

							$paymentProcessor = $paymentMethod->getPaymentProcessor();

							// Init gateway
							$paymentProcessor->assignUser($user);
							$paymentProcessor->assignOrder($order);

							$orderItems = $order->getOrderItems();

							$paymentProcessor->assignOrderItems($orderItems);
							$paymentProcessor->getCommonData($user, $order);

							// Check test/demo/sandbox mode
							$paymentTestMode = $paymentProcessor->isTestMode();

							// Get payment URL
							$paymentURL = $paymentProcessor->getPostUrl();

							// PayPalEC
							if ($paymentMethod->getCode() == 'paypalec' && !isset($_SESSION['paypal_express_checkout_details']))
							{
								$paymentProcessor->type = 'ipn';
								$paymentProcessor->steps = 2;

								$returnUrl = $url_dynamic.'p=one_page_checkout&oa='.ORDER_PROCESS_PAYMENT;
								$cancelUrl = $url_dynamic.'p=one_page_checkout';

								$cartItems = $order->getOrderItemsExtended($msg);

								$expressCheckoutUrl = $paymentProcessor->expressCheckout($order, $cartItems, false, $returnUrl, $cancelUrl);

								if ($paymentProcessor->is_error)
								{
									$opc->setError($paymentProcessor->error_message);
								}
								else
								{
									$paymentURL = $expressCheckoutUrl;
								}
							}

							if (trim($paymentURL) == '') $paymentURL = $url_https.'p=one_page_checkout'; // Force HTTPS here

							$validatorJS = $paymentProcessor->getValidatorJS();
							$paymentJS = $paymentProcessor->getJS();

							$payment_form_wide = $paymentProcessor->payment_form_wide;
							$payment_in_popup = $paymentProcessor->payment_in_popup;
							$payment_in_popup_width = $paymentProcessor->payment_in_popup_width;
							$payment_in_popup_height = $paymentProcessor->payment_in_popup_height;
							$payment_in_popup_title = $paymentProcessor->payment_in_popup_title;

							if (!$paymentProcessor->override_payment_form)
							{
								view()->assign('payment_test_mode', $paymentTestMode);
								view()->assign('auth_express', $user->express ? 'yes' : 'no');
								view()->assign('payment_in_popup', $payment_in_popup);
								view()->assign('payment_in_popup_width', $payment_in_popup_width);
								view()->assign('payment_in_popup_height', $payment_in_popup_height);
								view()->assign('payment_in_popup_title', $payment_in_popup_title);
								view()->assign('payment_form', $paymentProcessor->getPaymentForm($db));
								view()->assign('payment_profile_form', $paymentProcessor->getPaymentProfileForm($db));
								$cur_ip = $_SERVER['REMOTE_ADDR'];
								view()->assign('processor_template_data', $paymentProcessor->getTemplateData($cur_ip));
								view()->assign('processor_form_template', $paymentProcessor->getFormTemplate());
								view()->assign('show_cvv2_info', $paymentProcessor->show_cvv2);
								view()->assign('payment_url', $paymentURL);
								$paymentForm = view()->fetch('templates/pages/checkout/opc/opc-payment-form.html');
							}
							else
							{
								$paymentForm = $paymentProcessor->getPaymentForm($db);
								$paymentProfileForm = $paymentProcessor->getPaymentProfileForm($db);
								$payment_form_override = true;
							}

							if ($paymentProcessor->is_error)
							{
								$opc->setError($paymentProcessor->error_message);
							}
						}
						else
						{
							// Custom payment method
							$validatorJS = '';
							$paymentJS = '';
							$paymentURL = $url_https.'p=one_page_checkout';
							view()->assign('auth_express', $user->express ? 'yes' : 'no');
							view()->assign('payment_url', $paymentURL);
							$paymentForm = view()->fetch('templates/pages/checkout/opc/opc-payment-form.html');
						}

						// TODO: gift cerificate
						/**
						if ($order->gift_cert_amount > 0 && $order->totalAmount > $order->gift_cert_amount)
						{
						//$objResponse->addAssign("gift_cert_error", "style.display", "none");
						//$objResponse->addAssign("gift_cert_alt_payment", "style.display", "block");
						//$objResponse->addScript("altPaymentGiftCert('$order->totalAmount', '$order->gift_cert_amount')");
						}
						 **/

						$opc->setData('paymentMethodId', $paymentMethodId);
						$opc->setData('paymentForm', $paymentForm);
						$opc->setData('paymentFormWide', $payment_form_wide);
						$opc->setData('paymentFormPopUp', $payment_in_popup);
						$opc->setData('paymentFormPopUpWidth', $payment_in_popup_width);
						$opc->setData('paymentFormPopUpHeight', $payment_in_popup_height);
						$opc->setData('paymentFormPopUpTitle', $payment_in_popup_title);
						$opc->setData('paymentFormOverride', $payment_form_override);
						$opc->setData('paymentFormUrl', $paymentURL);
						$opc->setData('paymentFormValidatorJS', $validatorJS);
						$opc->setData('paymentFormJS', $paymentJS);
					}
					else
					{
						$opc->setError('Cannot find payment gateway by ID');
					}
				}

				/**
				 * Set session variables
				 */
				$_SESSION["pc_order_id"] = $order->oid;
				$_SESSION["pc_order_amount"] = $order->totalAmount - $order->shippingAmount - $order->taxAmount - ($order->handlingSeparated ? $order->handlingAmount : 0.00);
				$_SESSION["pc_method_id"] = $order->paymentMethodId;
				$_SESSION["pc_order_quantity"] = $order->itemsCount;
				$_SESSION["pc_order_num"] = $order->order_num;

				break;
			}
		}
	}

	if ($addressUpdated)
	{
		// Set new tax zone
		$taxProvider->initTax($order);
		$processOrderData = true;
	}

	//recheck gift certificate
	if ($recheckGiftCertificate)
	{
		$giftCertStoredData = isset($_SESSION["order_gift_certificate"]) ? $_SESSION["order_gift_certificate"] : false;

		if ($giftCertStoredData)
		{
			$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
			$giftCertData = $giftCertificateRepository->getGiftCertificate($giftCertStoredData["first_name"], $giftCertStoredData["last_name"], $giftCertStoredData["voucher"]);

			// Note: we only validating gift certificate without applying
			if ($giftCertData) 
			{
				// gift certificate is correct
				$giftError = false;
				$order->setGiftCertificateAmount($giftCertData["balance"]);
			}
			else
			{
				$order->setGiftCertificateAmount(0);
				unset($_SESSION["order_gift_certificate"]);
			}
		}
	}

	// get shipping methods
	if ($getShippingMethods)
	{
		unset($_SESSION['bongo_landed_costs']);

		if ($order->getShippingRequired())
		{
			// check for bongo shipping
			$bongoActive = ($bongoData = $db->selectOne('SELECT pid FROM '.DB_PREFIX.'payment_methods WHERE id="bongocheckout" AND active="Yes"')) ? true : false;
			$bongoUsed = false;
			$bongoShippingMethod = $settings['bongocheckout_Shipping_Method'];
		}

		// get rates
		$rates = false;
		$ratesAlternative = false;
		
		$currencies = new Currencies($db);
		$defaultCurrency = $currencies->getDefaultCurrency();
		$currencyExchangeRates = $currencies->getExchangeRates();

		$shippingQuotesViewModel = new View_ShippingQuotesViewModel($settings, $msg);
		$taxProvider = Tax_ProviderFactory::getTaxProvider();

		/**
		 * Prepare shipments
		 */
		$shipments = $order->getShipments();

		$orderHasFreeShippingProducts = false;
		$orderHasOnlyFreeShipments = true;
		$orderItems = $order->getOrderItems();

		foreach ($shipments as $shipmentId => $shipment)
		{
			$shipment['shipping_methods'] = false;
			$shipment['shipping_methods_alternative'] = false;
			$shipment['items'] = array();

			$itemsTotalCount = 0;
			$itemsFreeShippingCount = 0;

			foreach ($orderItems as $item)
			{
				if ($item['shipment_id'] == $shipment['id'])
				{
					$shipment['items'][] = array(
						'title' => $item['title'],
						'quantity' => $item['quantity'],
						'free_shipping' => $item['free_shipping'] == 'Yes'
					);

					$itemsTotalCount++;

					if ($item['product_type'] == Model_Product::TANGIBLE)
					{
						if ($item['free_shipping'] == 'Yes')
						{
							$itemsFreeShippingCount++;
						}
						else
						{
							$orderHasOnlyFreeShipments = false;
						}
					}
				}
			}

			if ($itemsTotalCount != $itemsFreeShippingCount) $orderHasOnlyFreeShipments = false;
			if ($itemsFreeShippingCount > 0) $orderHasFreeShippingProducts = true;

			$shipments[$shipmentId] = $shipment;
		}

		/**
		 * Get rates
		 */
		$rates = Shipping::getShippingQuotes(
			$db, $settings, $user, $order, false, false, false,
			$defaultCurrency['code'], $currencyExchangeRates,
			$bongoActive, $bongoUsed
		);

		if ($bongoUsed)
		{
			$rates = Payment_Bongo_BongoUtil::filterShippingMethods($rates, $bongoShippingMethod, $msg);
		}

		// show alternative shipping methods for free shipping products
		// TODO: recheck alternative shipping methods availability when Bongo used
		if (!$bongoUsed && $settings["ShippingShowAlternativeOnFree"] == "YES" && $orderHasFreeShippingProducts)
		{
			$ratesAlternative = Shipping::getShippingQuotes(
				$db, $settings, $user, $order, false, false, true,
				$defaultCurrency['code'], $currencyExchangeRates,
				$bongoActive, $bongoUsed
			);

			$ratesAlternative = Shipping::filterAlternativeShippingQuotes($rates, $ratesAlternative);
		}

		$order->persist();

		$shippingRatesExist = isset($rates) && !empty($rates) && $rates['shipping_methods_count'] > 0;
		$ratesView = $shippingRatesExist ? $shippingQuotesViewModel->getShippingQuotesView($order, $taxProvider, $rates) : false;

		$shippingRatesAlternativeExist = isset($ratesAlternative) && !empty($ratesAlternative) && $ratesAlternative['shipping_methods_count'] > 0;
		$ratesViewAlternative = $shippingRatesAlternativeExist ? $shippingQuotesViewModel->getShippingQuotesView($order, $taxProvider, $ratesAlternative) : false;

		/**
		 * Flip shipments and methods
		 * TODO: modify quito functions to ger valid data
		 */
		if ($ratesView)
		{
			foreach ($ratesView['shipments'] as $shipmentId => $shipment)
			{
				$shipments[$shipmentId]['shipping_methods'] = $shipment['shipping_methods'];
			}
		}

		if ($ratesViewAlternative)
		{
			foreach ($ratesViewAlternative['shipments'] as $shipmentId => $shipment)
			{
				$shipments[$shipmentId]['shipping_methods_alternative'] = $shipment['shipping_methods'];
			}
		}

		/**
		 * Check selected methods
		 */
		$shipmentsMethods = array();
		$shipmentsMethodsCookie = isset($_COOKIE['opc-shipments-methods']) && !empty($_COOKIE['opc-shipments-methods']) ? @json_decode($_COOKIE['opc-shipments-methods'], true) : false;

		foreach ($shipments as $shipmentId => &$shipment)
		{
			$shipmentsMethods[$shipmentId] = false;

			if (isset($shipmentsMethodsCookie[$shipmentId]))
			{
				$selectedMethodId = $shipmentsMethodsCookie[$shipmentId];
				if (($shipment['shipping_methods'] && isset($shipment['shipping_methods'][$selectedMethodId])) ||
					($shipment['shipping_methods_alternative'] && isset($shipment['shipping_methods_alternative'][$selectedMethodId])))
				{
					$shipmentsMethods[$shipmentId] = $selectedMethodId;
				}
			}

			if (!$shipmentsMethods[$shipmentId])
			{
				if ($shipment['shipping_methods'] && isset($shipment['shipping_methods'][0]))
				{
					$shipmentsMethods[$shipmentId] = $shipment['shipping_methods'][0]['ssid'];
				}
				else if ($shipment['shipping_methods_alternative'] && isset($shipment['shipping_methods_alternative'][0]))
				{
					$shipmentsMethods[$shipmentId] = $shipment['shipping_methods_alternative'][0]['ssid'];
				}
			}

			$shipments[$shipmentId]['shipping_method_id'] = $shipmentsMethods[$shipmentId];
		}

		/** @var ORDER $order */
		if ($shipmentsMethods)
		{
			$order->updateShipmentsMethods($shipmentsMethods);
			$order->persist();
		}


		setcookie('opc-shipments-methods', json_encode($shipmentsMethods));

		// TODO: use session class, do not access session directly
		$_SESSION['bongo_data'] = array('bongoActive' => $bongoActive, 'bongoUsed' => $bongoUsed);

		if ($bongoActive && $bongoUsed)
		{
			if ($order->getPaymentMethodId() != 'bongocheckout' && isset($bongoData))
			{
				OrderProvider::updatePaymentMethod($order, $bongoData['pid']);
			}

			// have to recalculate shipping here
			if ($action != 'setShippingMethod')
			{
				$errorMessage = '';
				$order->calculateShipping($errorMessage);
				if ($errorMessage != '') $opc->setError($errorMessage);
			}

			$bongoConnect = new Payment_Bongo_BongoConnect($settings['bongocheckout_Partner_Key']);
			// TODO: BONGO - check for zero shipping amount
			$bongoResult = $bongoConnect->getLandedCost($order->items, $order->shippingAddress['country_iso_a2'], $order->getShippingAmount() + $order->getHandlingAmount());

			if ($bongoResult)
			{
				$_SESSION['bongo_landed_costs'] = $bongoResult;
			}
			else
			{
				$opc->setError($bongoConnect->getLastError());
			}
		}

		$opc->setData('shipments', $shipments);
	}
	elseif (!$order->getShippingRequired())
	{
		unset($_SESSION['bongo_landed_costs']);
	}

	/**
 	 * Check do we have enough data to calculate everything
 	 */
	if ($processOrderData)
	{
		$user->getUserData();
		$order->reupdateItems();

		/**
		 * Check do we have enough data to calculate everything
		 */
		$shipping_error_message = '';
		$order->recalcTotals($shipping_error_message);

		if (trim($shipping_error_message) && $action == 'setShippingMethod')
		{
			$opc->setError($shipping_error_message);
		}

		$order->getOrderData();
		$order->getOrderItems();

		//tax-related for shipping
		$shippingTaxable = $settings["ShippingTaxable"];
		$shippingTaxRate = 0;
		$shippingTaxDescription = "";

		if ($shippingTaxable)
		{
			$shippingTaxClassId = $settings["ShippingTaxClassId"];

			$taxProvider = Tax_ProviderFactory::getTaxProvider();

			if ($taxProvider->hasTaxRate($shippingTaxClassId))
			{
				$shippingTaxRate = $taxProvider->getTaxRate($shippingTaxClassId, false);
				$shippingTaxDescription = $taxProvider->getTaxRateDescription($shippingTaxClassId);
			}
		}

		$orderData = array(
			"totalAmount" => $order->getTotalAmount(),
			"giftCertificateAmount" => $order->getGiftCertificateAmount(),
			"totalRemainingAmount" => $order->getRemainingAmountToBePaid(),
			'hasRecurringBillingItems' => $order->hasRecurringBillingItems(),
		);
		$opc->setData("orderData", $orderData);

		$orderViewModel = new View_OrderLinesViewModel($settings, $msg);
		$orderViewModel->build($order);

		if (isset($_SESSION['bongo_landed_costs']))
		{
			$bongoResponse = $_SESSION['bongo_landed_costs'];

			/**
			if ($bongoResponse->ddpAvailable == 1)
			{
				$orderViewModel->addLineItem('bongo-duty-tax', 'Duty / Tax', '', $bongoResponse->dutyCost + $bongoResponse->taxCost);
			}
			else
			{
				$orderViewModel->addLineItem('bongo-duty-tax', 'Duty / Tax', '', '<span style="font-size:10px;">pay courier on delivery</span>');
			}
			**/

			$orderViewModel->addLineItem('shipping', $msg["opc"]["invoice_shipping"], '', $bongoResponse->shippingCost);

			if ($bongoResponse->insuranceCost > 0)
			{
				$orderViewModel->addLineItem('bongo-insurance', 'Insurance', '', $bongoResponse->insuranceCost);
			}

			$total = $order->getTotalAmount();
			$total += $bongoResponse->dutyCost /** + $bongoResponse->taxCost **/- $order->getShippingAmount() + $bongoResponse->shippingCost + $bongoResponse->insuranceCost;

			$orderViewModel->addLineItem('total', $msg["opc"]["invoice_total"], '', $total);
		}

		$opc->setData('orderView', $orderViewModel->asArray());

		$giftCertificateViewModel = new View_GiftCertificateViewModel($settings, $msg);
		$giftCertificateViewModel->build($order);
		$opc->setData('giftCertificateView', $giftCertificateViewModel->asArray());
	}

	if (isset($_SESSION['bongo_data']))
	{
		$opc->setData("bongoActive", $_SESSION['bongo_data']['bongoActive']);
		$opc->setData("bongoUsed", $_SESSION['bongo_data']['bongoUsed']);
	}

	echo $opc->getResponse();
	
	_session_write_close();
	$db->done();
	die();
	

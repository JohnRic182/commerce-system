<?php
/**
 * Start express checkout
 */

	if (!defined("ENV")) exit();
	
	// Express checkout users should not be allowed to be created when the store owner has set the store to not display prices to visitors.
	if ($settings['VisitorSeePrice'] == 'NO')
	{
		header('Location: '.$url_dynamic.'p=login');
		exit;
	}
	
	if (!$user->auth_ok)
	{
		$user->expressLogin();
	}
	
	if ($user->auth_ok)
	{
		$backurl = $url_https."p=one_page_checkout";
		unset($_SESSION["user_started_checkout"]);
	}
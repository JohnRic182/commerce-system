<?php
/**
 * Add a new shipping address
 */

if (!defined('ENV')) exit();

if ($user->auth_ok)
{
	// add shipping address
	$form = isset($_REQUEST['form']) ? (is_array($_REQUEST['form']) ? $_REQUEST['form'] : array()) : array();
	
	$custom_field = isset($_REQUEST['custom_field']) ? (is_array($_REQUEST['custom_field']) ? $_REQUEST['custom_field'] : array()) : array();

	if (!Nonce::verify(isset($_POST['nonce']) ? $_POST['nonce'] : '', 'user_add_address'))
	{
		$validationError = true;
		$user->setError($msg['common']['error_invalid_nonce']);
	}
	else
	{
		$address_id = $user->addShippingAddress($form, $custom_field);
	}
}

<?php
/**
 * Sets HTTP cookie after user's login in HTTPS mode and domain names are different
 */
	
	if (!defined("ENV")) exit();

	if (isset($_GET["data"]))
	{
		$data = unserialize(base64_decode($_GET["data"]));
		if ($data && is_array($data) && isset($data["i"]) && isset($data["l"]) && isset($data["s"]) && isset($data["b"]))
		{
			// data array suppose to contain
			// i = user id in db
			// l = user level
			// s = session id
			// b = backurl 
			$user->setCookie($data["i"], $data["l"], $data["s"]);
			
			$backurl = $data["b"];
			
			/**
			 * No reasons to continue if backurl is empty
			 */
			if (!$backurl)
			{
				_session_write_close();
				
				$db->done();
				
				die();
			}
		}
	}

		
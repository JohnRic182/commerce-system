<?php
/**
 * Move wishlist to a cart
 */

if (!defined("ENV")) exit();

$wishlist = new Wishlist($db, $user->id);
$wlpid = isset($_REQUEST["wlpid"]) ? intval($_REQUEST["wlpid"]) : 0;
$wlid = isset($_REQUEST["wlid"]) ? intval($_REQUEST["wlid"]) : 0;
	
if ($user->auth_ok)
{
	if ($wlpid > 0)
	{
		$wl_product = $wishlist->getWishlistProduct($wlpid);

		OrderProvider::addItem($order, $wl_product["product_id"], "1", $wl_product["product_attributes"]);
	}
	
	if ($wlid > 0)
	{
		$wl_products = $wishlist->getWishlistData($wlid);
		
		foreach ($wl_products["wishlist_products"] as $product)
		{
			if ($product["out_of_stock"] == "No" || $product["out_of_stock"] == "")
			{
				$wl_product = $wishlist->getWishlistProduct($product["wlpid"]);

				OrderProvider::addItem($order, $wl_product["product_id"], "1", $wl_product["product_attributes"]);
			}
		}
	}
	
	if ($settings["AfterProductAddedGoTo"] == "Cart Page")
	{
		$backurl = $url_dynamic."p=cart";
	}
	elseif ($settings["AfterProductAddedGoTo"] == "Checkout Page")
	{
		$backurl = $url_dynamic."p=one_page_checkout";
	}
	else
	{
		$backurl = UrlUtils::getBackUrl();
	}
}

<?php
/**
 * Add new item to wishlist
 */

	if (!defined("ENV")) exit();

	$product_id = isset($_REQUEST["oa_id"]) ? $_REQUEST["oa_id"] : "";
	$pid = isset($_REQUEST["pid"]) ? intval($_REQUEST["pid"]) : 0;
	$wlid = isset($_REQUEST["wlid"]) ? intval($_REQUEST["wlid"]) : 0;
	$oa_attributes = isset($_POST["oa_attributes"]) ? $_POST["oa_attributes"] : array();
	$oa_attributes = is_array($oa_attributes) ? $oa_attributes : array();
	$wl_action = isset($_REQUEST["wl_action"]) ? $_REQUEST["wl_action"] : "";

	$wishlist = new Wishlist($db, $user->id);
	$wl_att = (isset($_SESSION["wl_att"]) ? $_SESSION["wl_att"] : array());

	if (count($oa_attributes)>0)
	{
		foreach ($oa_attributes as $key=>$val)
		{
			$db->query("SELECT name, options FROM ".DB_PREFIX."products_attributes WHERE pid= '".intval($pid)."' AND paid = '".$db->escape($key)."'");

			while ($db->moveNext())
			{
				if ($db->col["options"] != "")
				{
					$options = $db->col["options"];
					$options_array = explode("\n", $options);

					for ($i=0; $i<count($options_array); $i++)
					{
						$option = trim($options_array[$i]);

						if ($option != "")
						{
							$option_parts = array();
							$option_parts = explode("(", $option);
							$option_name = trim($option_parts[0]);

							if ($option_name == urldecode ($val))
							{
								$wl_att[$key]=$db->col["name"]." : ".$option_name;
							}
						}
					}
				}
				else
				{
					$wl_att[$key]=$db->col["name"]." : ".$val;
				}
			}
		}
	}

	$_SESSION["wl_att"] = $wl_att;

	if ($user->auth_ok)
	{
		switch ($wl_action)
		{
			case "update_wishlist" :
			{

				if ($wlid != 0)
				{
					$wishlist->updateWishlist($wlid);
					$wishlist->updateWishlistProducts($wlid, $pid);
					$act = "updated_wishlist";
				}
				else
				{
					$act = "wishlist_error";
				}

				break;
			}

			case "addnew_wishlist" :
			{
				$wishlist_name = isset($_REQUEST["wishlist_name"]) ? $_REQUEST["wishlist_name"] : "";

				if ($wishlist_name != "")
				{
					$wl_id = $wishlist->addNewWishlist($wishlist_name);

					if ($pid != 0)
					{
						$wishlist->updateWishlist($wl_id);
						$wishlist->updateWishlistProducts($wl_id, $pid);
						$act = "updated_wishlist";
					}
					else
					{
						$act = "added_wishlist";
					}
				}
				else
				{
					$act = "wishlist_error";
				}

				break;
			}

			case "addto_wishlist":
			{
				if ($wishlist->wishlistCount >1)
				{
					$act = "select_wishlist";
				}
				elseif ($wishlist->wishlistCount == 1)
				{
					$wishlist->updateWishlist($wishlist->wishlistList[0]["wlid"]);
					$wishlist->updateWishlistProducts($wishlist->wishlistList[0]["wlid"], $pid);
					$act = "updated_wishlist";
				}
				else
				{
					$act = "addnew_wishlist";
				}

				break;
			}
		}

		$backurl = $url_https."p=wishlist&wl_action=".$act."&pid=".$pid;
	}
	else
	{
		$_SESSION["wl_pid"] = $pid;
		$backurl = $url_https."p=login";
	}

<?php
/**
 * Add Gift Cert To Order
 * @subpackage EngineLogic
 * @copyright Copyright(c) Gaston Tavella, LLC. All Rights Reserved.
 * @author Gaston Tavella
 */

	if (!defined("ENV")) exit();
	
	/* Init Variabless */
	$first_name = isset($_REQUEST["first_name"]) ? $_REQUEST["first_name"] : '';
	$last_name = isset($_REQUEST["last_name"]) ? $_REQUEST["last_name"] : '';
	$phone = isset($_REQUEST["phone"]) ? $_REQUEST["phone"] : '';
	$rep_email = isset($_REQUEST["rep_email"]) ? $_REQUEST["rep_email"] : '';
	$from_name = isset($_REQUEST["from_name"]) ? $_REQUEST["from_name"] : '';
	$message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : '';
	$gift_amount = isset($_REQUEST["gift_amount"]) ? $_REQUEST["gift_amount"] : 0;
	$email_gift = (isset($_REQUEST["chk_email_gift"]) && $_REQUEST["chk_email_gift"]=='1' ? '1' : '0');
	
	if ($settings["VisitorMayAddItem"] == "YES" || ($user->auth_ok || $userCookie))
	{
		$errors = array();

		if (!validate(str_replace('.', '', $first_name), VALIDATE_NAME))
		{
			$errors[] = $msg['gift_certificate']['giftCertCreateErrors']['recipient_first_name'];
		}

		if (!validate(str_replace('.', '', $last_name), VALIDATE_NAME))
		{
			$errors[] = $msg['gift_certificate']['giftCertCreateErrors']['recipient_last_name'];
		}

		if (!validate(str_replace('.', '', $from_name), VALIDATE_NAME))
		{
			$errors[] = $msg['gift_certificate']['giftCertCreateErrors']['your_name'];
		}

		if (!validate($gift_amount, VALIDATE_FLOAT))
		{
			$errors[] = $msg['gift_certificate']['giftCertCreateErrors']['certificate_amount'];
		}

		if ($gift_amount < 1)
		{
			$errors[] = $msg['gift_certificate']['giftCertCreateErrors']['certificate_amount_gt_zero'];
		}

		if (!validate($rep_email, VALIDATE_EMAIL))
		{
			$errors[] = $msg['gift_certificate']['giftCertCreateErrors']['recipient_email_address'];
		}

		if (count($errors) > 0)
		{
			view()->assign("header_error_text", $msg['gift_certificate']['giftCertCreateError']);
			view()->assign("error_messages", $errors);
			view()->assign("first_name", $first_name);
			view()->assign("last_name", $last_name);
			view()->assign("phone", $phone);
			view()->assign("rep_email", $rep_email);
			view()->assign("from_name", $from_name);
			view()->assign("message", $message);
			view()->assign("gift_amount", $gift_amount);
			view()->assign("email_gift", $email_gift);
		}
		else 
		{
			$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));

			$values = array();
			$values['oid'] = $order->getId();
			$values['first_name'] = $first_name;
			$values['last_name'] = $last_name;
			$values['phone'] = $phone;
			$values['email'] = $rep_email;
			$values['gift_amount'] = $gift_amount;
			$values['from_name'] = $from_name;
			$values['message'] = $message;
			$values['email_gift'] = $email_gift == 1 ? 1 : 0;

			$lineItemId = $giftCertificateRepository->addGiftCertificate($order->getId(), $values);

			$orderRepository = new DataAccess_OrderRepository($db, $settings);
			$lineItemData = $orderRepository->getLineItemById($lineItemId);
			$lineItem = new Model_LineItem($lineItemData);
			$shipmentRepository = new DataAccess_OrderShipmentRepository($db, new DataAccess_SettingsRepository($db, $settings));
			$shipmentRepository->assignShipment($lineItem);

			$db->query('UPDATE '.DB_PREFIX.'orders_content SET shipment_id='.$lineItem->getShipmentId().' WHERE ocid='.$lineItem->getId());

			OrderProvider::refreshSubtotals($order);

			$backurl = $url_dynamic.'p=cart';
		}
	}

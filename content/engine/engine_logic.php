<?php 

	if (!defined("ENV")) exit();

	/**
	 * Get user from a session
	 * @var USER
	 */
	$user = USER::getFromSession($db, $settings);
	
	// ua - user action
	$ua = isset($_REQUEST["ua"]) ? $_REQUEST["ua"] : "";

	if ($ua != "")
	{
		switch($ua)
		{
			case USER_EXPRESS_CHECKOUT : require_once("content/engine/actions/action.user_express_checkout.php"); break;
			case USER_SIGNUP : require_once("content/engine/actions/action.user_signup.php"); break;
			case USER_PASSWORD_RESET : require_once("content/engine/actions/action.user_password_reset.php"); break;
			case USER_LOGIN : require_once("content/engine/actions/action.user_login.php"); break;
			case USER_LOGIN_FACEBOOK : require_once("content/engine/actions/action.user_login_facebook.php"); break;
			case USER_SET_COOKIE :  require_once("content/engine/actions/action.user_set_cookie.php"); break;
			case USER_UNSET_COOKIE :  require_once("content/engine/actions/action.user_unset_cookie.php"); break;
			case USER_UPDATE_PROFILE : require_once("content/engine/actions/action.user_update_profile.php"); break;
			case USER_ADD_ADDRESS : require_once("content/engine/actions/action.user_add_address.php"); break;
			case USER_UPDATE_ADDRESS : require_once("content/engine/actions/action.user_update_address.php"); break;
			case USER_DELETE_ADDRESS : require_once("content/engine/actions/action.user_delete_address.php"); break;
			case USER_LOGOUT : require_once("content/engine/actions/action.user_logout.php"); break;
			case USER_LOGOUT_FACEBOOK : require_once("content/engine/actions/action.user_logout_facebook.php"); break;
			case USER_FORM_MAIL : require_once("content/engine/actions/action.user_form_mail.php"); break;
			case USER_START_CHECKOUT : require_once("content/engine/actions/action.user_start_checkout.php"); break;
			case USER_ADD_TO_WISHLIST : require_once("content/engine/actions/action.user_addto_wishlist.php"); break;
			case USER_ADD_REVIEW : require_once("content/engine/actions/action.user_add_review.php"); break;
			case USER_PREDICTIVE_SEARCH : require_once("content/engine/actions/action.user_predictive_search.php"); break;
			case USER_QR:
			case USER_OFFSITE_COMMERCE: require_once("content/engine/actions/action.user_offsite_commerce.php"); break;
		}
	}
	
	// get user data second time after possible changes
	$user->getUserData();

	// create/restore, check & process order's action
	$oa = isset($_REQUEST["oa"]) ? $_REQUEST["oa"] : (isset($_REQUEST['OA']) ? $_REQUEST['OA'] : "");

	$order = ORDER::getFromSession($db, $settings, $user, true, true);

	// select order data first time before possible modification
	$order->getOrderData();
	$order->getOrderItems();

	if ($order->getForceRecalc())
	{
		$shippingErrorMessage = '';
		$order->recalcEverything($shippingErrorMessage, true);
	}

	// Backwards compatibility for DisplayPricesWithTax
	$taxProvider = Tax_ProviderFactory::getTaxProvider();
	$taxProvider->initTax($order);
	$settings['DisplayPricesWithTax'] = $taxProvider->getDisplayPricesWithTax() ? 'YES' : 'NO';
	view()->assign('DisplayPricesWithTax', $settings['DisplayPricesWithTax']);

	if ($oa != "")
	{	
		// oa - order action
		switch($oa)
		{
			case ORDER_ADD_ITEM : require_once("content/engine/actions/action.order_add_item.php"); break;
			case ORDER_REMOVE_ITEM : require_once("content/engine/actions/action.order_remove_item.php"); break;
			case ORDER_CLEAR_ITEMS : require_once("content/engine/actions/action.order_clear_items.php"); break;
			case ORDER_UNLINK_USER : require_once("content/engine/actions/action.order_unlink_user.php"); break;
			case ORDER_RE_UPDATE_ITEMS : require_once("content/engine/actions/action.order_reupdate_items.php"); break;
			case ORDER_UPDATE_ITEMS : require_once("content/engine/actions/action.order_update_items.php"); break;
			case ORDER_STORE_PAYPAL_EC_INFO : require_once("content/engine/actions/action.order_store_paypal_ec_info.php"); break;
			case ORDER_COMPLETE_ORDER : require_once("content/engine/actions/action.order_complete_order.php"); break;
			case ORDER_PROCESS_PAYMENT_VALIDATION : require_once("content/engine/actions/action.order_process_payment_validation.php"); break;
			case ORDER_PROCESS_PAYMENT : require_once("content/engine/actions/action.order_process_payment.php"); break;
			case ORDER_OPC : require_once("content/engine/actions/action.order_opc.php"); break;
			case ORDER_RESTORE_CART : require_once("content/engine/actions/action.order_restore_cart.php"); break;
			case ORDER_WISHLIST_TO_CART : require_once("content/engine/actions/action.order_wishlist_to_cart.php"); break;	
			case ORDER_ADD_GIFT_CERT : require_once("content/engine/actions/action.order_add_gift_cert.php"); break;
		}
		
		if ($oa != ORDER_PROCESS_PAYMENT && $oa != ORDER_COMPLETE_ORDER)
		{
			$user->getUserData();
			$order->getOrderData();
			$order->getOrderItems();
		}
	}

	if (isset($_GET['killsession'])) { session_destroy(); die(); }

	// Load cartSubtotalAmount here till we have a view model for cart/order
	if ($settings['DisplayPricesWithTax'] == 'YES')
	{
		foreach ($order->items as &$item)
		{
			$item['product_price'] = $item['price_withtax'];
		}
		$order->setCartSubtotalAmount($order->getSubtotalAmountWithTax());
	}
	else
	{
		$order->setCartSubtotalAmount($order->getSubtotalAmount());
	}
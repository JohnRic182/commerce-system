<?php

	if (!ini_get("date.timezone"))
	{
		date_default_timezone_set("America/Phoenix");
	}

	if (!defined('DS')) define("DS", DIRECTORY_SEPARATOR);
	if (!defined('APP_SYSTEM_TIMEZONE')) define('APP_SYSTEM_TIMEZONE', date_default_timezone_get());

	class tmp_object{};

	/**
	 * @param $str
	 * @param array|null $parameters
	 * @return string
	 */
	function trans($str, array $parameters = null)
	{
		$translator = Framework_Translator::getInstance();

		return $translator->trans($str, $parameters);
	}

	/**
	 * Additional service function
	 * @param $options
	 * @param $price
	 * @param $weight
	 * @param $result
	 * @param $admin_call
	 * @return mixed
	 */
	function parseOptions($options, $price, $weight, &$result, $admin_call = false)
	{
		return ProductAttribute::parseOptions2($options, $price, $weight, $result, $admin_call);
	}

	/**
	 * Get XML or SimpleXML Object and transform it to array
	 * @return array
	 */
	function xml2array($input, $callback = null, $recurse = false)
	{
	    // Get input, loading an xml string with simplexml if its the top level of recursion
	    $data = ((!$recurse) && is_string($input)) ? simplexml_load_string($input): $input;
	    // Convert SimpleXMLElements to array
	    if ($data instanceof SimpleXMLElement || $data instanceof stdClass)
	    {
	    	$data = (array) $data;
			if (count($data) == 0) $data = '';
	    } 

	    // Recurse into arrays
	    if (is_array($data)) foreach ($data as &$item) $item = xml2array($item, $callback, true);
	    // Run callback and return
	    return (!is_array($data) && is_callable($callback))? call_user_func($callback, $data): $data;
	}
	
	/**
	 * Transforms array to readable text
	 * @param array $array 
	 */
	function array2text($array, &$text = '', $level = 0)
	{
		foreach ($array as $key=>$value)
		{
			
			if (is_array($value))
			{
				$text.=str_repeat("  ", $level).$key."\n";
				array2text($value, $text, $level + 1);
			}
			else
			{
				if (is_bool($value))
				{
					$value = $value ? "true" : "false";
				}
				elseif (is_null($value))
				{
					$value = 'null';
				}
				$text.=str_repeat("  ", $level).$key." = ".$value."\n";
			}
		}
		if ($level == 0)
		{
			return $text;
		}
	}
	
	/**
	 * Resolve double encoding in smarty htmlspecialchars
	 * @param string $string
	 * @return string
	 */
	function smarty_modifier_htmlspecialchars($string)
	{
		return gs($string);
	}
	
	/**
	 * Return current URL
	 */




	function getSessionSavePath()
	{
		global $settings;

		$session_dir = $settings['GlobalServerPath'].DS.'content'.DS.'cache'.DS.'sessiondata';

		if (isset($settings['session_save_path']) && !is_null($settings['session_save_path']) && trim($settings['session_save_path']) != '')
		{
			if (is_dir($settings['session_save_path']) && is_writeable($settings['session_save_path']))
			{
				$session_dir = $settings['session_save_path'];
			}
			else
			{
				error_log('Save session path is not a directory or is not writable: '.$settings['session_save_path']);
			}
		}

		return $session_dir;
	}

	/**
	 *
	 */
	function setSessionSavePath()
	{
		global $settings;

		$session_dir = getSessionSavePath();
		ini_set('session.save_path', $session_dir);
		session_save_path($session_dir);
	}

	/**
	 * Write and close current session , but also copy vars changed in HTTPS mode to HTTP mode 
	 */
	function _session_write_close()
	{
		session_write_close();
	}
	
	/**
	 * Removes slashes from filename 
	 * @param $file_name
	 * @return unknown_type
	 */
	function escapeFileName($file_name)
	{
		return str_replace(array("/", "\\"), array("", ""), $file_name);
	}

	function normalizeProductLockFields(array &$lock_fields)
	{
		$map = array(
			'title' => 'name',
			'cid' => 'category',
			'secondary_categories[]' => 'secondary-categories',
			'price2' => 'sale-price',
			'meta_title' => 'meta-title',
			'meta_description' => 'meta-description',
			'image' => 'images',
			'image_url' => 'images',
		);

		foreach ($lock_fields as $key=>$lock_field)
		{
			if (isset($map[$lock_field]))
			{
				$lock_fields[$key] = $map[$lock_field];
			}
		}

		return $lock_fields;
	}

	/**
	 * Reads template file
	 * @param $templateFile
	 * @return text
	 */
	function readTemplateFile($templateFile)
	{
		global $settings;
		$pathBase = "content/engine/design/";
		$pathSkin = "content/skins/".escapeFileName($settings["DesignSkinName"])."/";
		$pathCustom = "content/skins/_custom/skin/";
		
		try
		{
			if (is_file($pathCustom.$templateFile)) return file_get_contents($pathCustom.$templateFile);
			if (is_file($pathSkin.$templateFile)) return file_get_contents($pathSkin.$templateFile);
			if (is_file($pathBase.$templateFile)) return file_get_contents($pathBase.$templateFile);
		}
		catch (Exception $e)
		{

		}
		return false;
	}
	
	/**
	 * Saves content
	 * @param $templateFile
	 * @param $content
	 * @return unknown_type
	 */
	function writeTemplateFile($templateFile, $content)
	{
		global $settings;
		try
		{
			$pathCustom = "content/skins/_custom/skin/";
			$pathCache = "content/cache/skins/".escapeFileName($settings["DesignSkinName"])."/";
		
			$templatePathCustom = dirname($pathCustom.$templateFile);
			$templatePathCache = dirname($pathCache.$templateFile);
		
			if (!is_dir($templatePathCustom)) mkdir($templatePathCustom, FileUtils::getDirectoryPermissionMode(), true);
			if (!is_dir($templatePathCache)) mkdir($templatePathCache, FileUtils::getDirectoryPermissionMode(), true);
		
			file_put_contents($pathCustom.$templateFile, $content);
			file_put_contents($pathCache.$templateFile, $content);
			return true;
		}
		catch (Exception $e)
		{
			return false;
		}
	}

	/**
	 * 
	 * @param $number
	 * @return unknown_type
	 */
	function normalizeNumber($number)
	{
		$s = "";
		$a = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
		$is_dot = false;
		$number = trim($number);
		if ($number != "")
		{
			if ($number[0] == "-")
			{
				$s = "-";
				$start = 1;	
			}
			else
			{
				$start = 0;	
			}
			for ($i = $start; $i < strlen($number); $i++)
			{
				$s.=in_array($number[$i], $a)?$number[$i]:"";
				if ($number[$i] == "." && ($is_dot == false))
				{
					$s.=".";
					$is_dot = true;
				}
			}
		}
		else
		{
			$s = 0;
		}
		return $s;
	}
	
	/**
	 * 
	 * @param $j
	 *
	 * @return string
	 */
	function myNum($j, $decimalPlaces = 5)
	{
		if ($j == "" || $j == 0) return "0";
		for ($i=2; $i<=5; $i++)
		{
			if(number_format($j, $i, ".", "")*1 == $j) return number_format($j, $i, ".", "");
		}
		return number_format($j, 5, ".", "");
	}

	/**
	 * 
	 */
	function gs($string)
	{
		return phpversion() > 5.2 ? htmlspecialchars($string, ENT_QUOTES, 'UTF-8', false) : htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
	}
	
	/**
	 * 
	 * @param $value
	 * @return unknown_type
	 */
	function isEmail($value)
	{
        $pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
        return preg_match($pattern, $value);
    }
    
    define("VALIDATE_NOT_EMPTY", 1);
    define("VALIDATE_ALPHA", 2); 	//only a-zA-Z
    define("VALIDATE_NUMERIC", 4); 	//1, 0.123, etc
    define("VALIDATE_INT", 8); 	//1,2,3,4
    define("VALIDATE_FLOAT", 16); 	//the same as numeric
    define("VALIDATE_STRING", 32); 	//anything not empty
    define("VALIDATE_NAME", 64); 	//a-zA-Z,-
    define("VALIDATE_EMAIL", 128); 	//
    define("VALIDATE_URL", 256); 	//
    define("VALIDATE_ZIP", 512);
	define("VALIDATE_ZIP_USA", 1024); //12345 or 12345-1234
	define("VALIDATE_ZIP_CANADA", 2048);
	define("VALIDATE_ZIP_UK", 4096);
	
	define("VALIDATE_USERNAME", 8192);
	define("VALIDATE_PASSWORD", 16384);
	define("VALIDATE_PASSWORD_LENGTH", 9);
	
	define("VALIDATE_PHONE", 32768);
	define("VALIDATE_CC", 65536);
	
    function validate($value, $type)
    {
    	$trimmed = trim($value);
    	if (($type & VALIDATE_NOT_EMPTY) && $trimmed == "") return false;
    	if (($type & VALIDATE_ALPHA) && !preg_match('/^([a-zA-Z])+$/', $value)) return false;
    	if (($type & VALIDATE_NUMERIC) && !preg_match('/^([0-9])+(\.([0-9])+)?$/', $value)) return false;
    	if (($type & VALIDATE_INT) && !preg_match('/^([0-9])+$/', $value)) return false;
    	if (($type & VALIDATE_FLOAT) && !preg_match('/^([0-9])+(\.([0-9])+)?$/', $value)) return false;
    	if (($type & VALIDATE_STRING) && $trimmed == "") return false;
		if (($type & VALIDATE_NAME) && ($trimmed == ""  || preg_match('/([0-9])+/', $value))) return false;
		//if (($type & VALIDATE_NAME) && !preg_match('/^(\pL)([\s\-\.\pL]{0,})/u', $value)) return false;
		//if (($type & VALIDATE_NAME) && !preg_match('/^([^\~\`\!\@\#\$\%\^\&\*\(\)\_\+\=\{\[\}\]\|\\\:\;\<\,\>\?\/0-9])+$/', $value)) return false;
		if (($type & VALIDATE_EMAIL) && !preg_match('/^([a-zA-Z0-9])+([\+\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/', $value)) return false;
    	if (($type & VALIDATE_URL) && !preg_match('/^((https?)\:\/\/)?([a-zA-Z0-9+!*(),;?&=\$_.-]+(\:[a-zA-Z0-9+!*(),;?&=\$_.-]+)?@)?([a-zA-Z0-9-.]*)\.([a-z]{2,3})(\:[0-9]{2,5})?(\/([a-zA-Z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-zA-Z0-9;:@&%=+\/\$_.-]*)?(#[a-z_.-][a-zA-Z0-9+\$_.-]*)?$/', $value)) return false;
    	if (($type & VALIDATE_ZIP) && !preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9 \-])+$/', $value)) return false;
    	if (($type & VALIDATE_ZIP_USA) && !preg_match('/^\d{5}$|^\d{5}-\d{4}$/', $value)) return false;
    	if (($type & VALIDATE_ZIP_CANADA) && !preg_match('/^([a-ceghj-npr-tv-z]){1}[0-9]{1}[a-ceghj-npr-tv-z]{1}[0-9]{1}[a-ceghj-npr-tv-z]{1}[0-9]{1}$/', strtolower(str_replace(" ", "", $value)))) return false;
    	if (($type & VALIDATE_ZIP_UK) && !preg_match('/^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/', $value)) return false;
    	if (($type & VALIDATE_USERNAME) && !preg_match('/^([a-zA-Z0-9-_@.\pL]){4,}$/u', $value)) return false;
    	if (($type & VALIDATE_PASSWORD) && ($trimmed == "" || strlen($trimmed) < VALIDATE_PASSWORD_LENGTH)) return false;
    	if (($type & VALIDATE_PHONE) && ($trimmed == "" || strlen($trimmed) < 1)) return false;

		// TODO: add normal cc validation (Luhn check, prefixes, length, etc)
		if (($type & VALIDATE_CC) && !preg_match('/^([0-9])+$/', $value)) return false;
    	return true;
    }

	/**
	 * @param $password
	 * @return string
	 */
  	function getPasswordHash($password)
  	{
		$passwordUtil = new PasswordUtil();

		return $passwordUtil->hashPassword($password, defined('PASSWORD_SALT') ? PASSWORD_SALT : false);
  	}

	/**
	 * @param $password
	 * @param $hash
	 * @return bool
	 */
	function checkPasswordHash($password, $hash)
	{
		$passwordUtil = new PasswordUtil();

		return $passwordUtil->checkPassword($password, $hash);
	}
	
	function paginate($pg, $pages_count, $pages_in_list, $url_prefix, $rewrite = false, $rewrite_first_page = false)
	{
		//global PAGES_IN_LIST;
		$pg = $pg - 1;
		$min = $pg - ($pg % $pages_in_list);
		$min ++;
		$max = $min + $pages_in_list - 1;
		$max = ($max > $pages_count)?$pages_count:$max;
		$t = array("pages"=>array(), "first"=>false, "range_left"=>false, "prev"=>false, "next"=>false, "range_right"=>false, "last"=>false);
		$pg++;
		
		if ($pg > $pages_in_list)
		{
			$t["first"]["url"] = (!$rewrite) ? $url_prefix.'&amp;pg=1' : ($rewrite_first_page ? $rewrite_first_page : str_replace("%n%", 1, $url_prefix));	
			$low_limit = $min - $pages_in_list;
			$t["range_left"] = 
				(!$rewrite) ?
				array("url" => $url_prefix.'&amp;pg='.($min-1), "text" => $low_limit.'-'.($low_limit + $pages_in_list - 1)) :
				array("url" => str_replace("%n%", ($min-1), $url_prefix), "text" => $low_limit.'-'.($low_limit + $pages_in_list - 1));
		}
		
		for ($i=$min; $i<=$max; $i++)
		{
			if ($i != $pg)
			{
				$t["pages"][] = 
					(!$rewrite) ? 
					array("url"=>$url_prefix.'&amp;pg='.$i, "text"=>$i) :
					array("url"=>(($i == 1 && $rewrite_first_page) ? $rewrite_first_page : str_replace("%n%", $i, $url_prefix)), "text"=>$i);
			}
			else
			{
				$t["pages"][] = array(
					"active"=>true,
					"text" => $i
				);
			}
		}
		
		if ($pages_count > $max)
		{
			$hi_limit = ($pages_count < ($max + $pages_in_list))?$pages_count:($max + $pages_in_list);
			$t["range_right"] = 
				(!$rewrite) ? 
				array("url"=>$url_prefix.'&amp;pg='.($max+1), "text"=>(($max+1==$hi_limit)?"":(($max+1).'-')).$hi_limit) :
				array("url"=>str_replace("%n%", ($max+1), $url_prefix), "text"=>(($max+1==$hi_limit)?"":(($max+1).'-')).$hi_limit);
			
			$t["last"]["url"] = (!$rewrite) ? $url_prefix.'&amp;pg='.($pages_count) : str_replace("%n%", $pages_count, $url_prefix);
		}
		
	
		$page_prev = $pg > 1 ? $pg - 1 : false;
		$page_next = $pg < $pages_count ? $pg + 1 : false;
		
		if ($page_prev)
		{
			$t["prev"] = 
				(!$rewrite) ?
				array("url"=>$url_prefix."&amp;pg=".$page_prev) :
				array("url"=>($page_prev == 1 && $rewrite_first_page) ? $rewrite_first_page : str_replace("%n%", $page_prev , $url_prefix));
		}
		
		if ($page_next)
		{
			$t["next"] = 
				(!$rewrite) ?
				array("url"=>$url_prefix."&amp;pg=".$page_next) :
				array("url"=>str_replace("%n%", $page_next , $url_prefix));
		}
		
		
		return $t;
	}
	
	/**
	 * Get state name by id
	 *
	 * @param DB $db
	 * @param int $stateId
	 *
	 * @return string|null
	 */
	function getState(DB $db, $stateId)
	{
		if ($stateId < 1) return null;

		$result = $db->selectOne('SELECT name FROM '.DB_PREFIX.'states WHERE stid='.intval($stateId));

		return $result ? $result['name'] : null;
	}

	/**
	 * Get state data
	 *
	 * @param DB $db
	 * @param int|bool $stateId
	 * @param string|bool $stateCode
	 *
	 * @return array|null
	 */
	function getStateData(DB $db, $stateId = false, $stateCode = false)
	{
		if ($stateId === false && $stateCode === false) return null;
		
		$result = $db->selectOne('
			SELECT *
			FROM '.DB_PREFIX.'states
			WHERE '.($stateId ? 'stid='.intval($stateId) : 'short_name="'.$db->escape($stateCode).'"')
		);

		return $result !== false ? $result : null ;
	}

	/**
	 * Get state id by state code
	 *
	 * @param DB $db
	 * @param $stateCode
	 *
	 * @return int|null
	 */
	function getStateId(DB $db, $stateCode)
	{
		$result = $db->selectOne('SELECT stid FROM '.DB_PREFIX.'states WHERE short_name ="'.$db->escape($stateCode).'"');
		return $result ? $result['stid'] : null;
	}

	/**
	 * Get country name by id
	 *
	 * @param DB $db
	 * @param int $countryId
	 *
	 * @return string|null
	 */
	function getCountry(DB $db, $countryId)
	{
		if ($countryId < 1) return null;

		$result = $db->selectOne('SELECT name FROM '.DB_PREFIX.'countries WHERE coid='.intval($countryId));

		return $result ? $result['name'] : null;
	}

	/**
	 * Get country data by id or code
	 *
	 * @param DB $db
	 * @param int|bool $countryId
	 * @param string|bool $countryCode
	 *
	 * @return bool|null
	 */
	function getCountryData(DB $db, $countryId = false, $countryCode = false)
	{
		if ($countryId === false && $countryCode === false) return null;

		$result = $db->selectOne('
			SELECT *
			FROM '.DB_PREFIX.'countries
			WHERE '.($countryId ? 'coid='.intval($countryId) : ('iso_a2="'.$db->escape($countryCode).'" OR iso_a3="'.$db->escape($countryCode).'"'))
		);

		return $result !== false ? $result : null;
	}
	
	function loadJS($filename, $path = null)
	{
		if (is_null($path)) $path = 'content/admin/javascript/';
		
		$filename .= (strstr($filename, '?'))
			? '&'.APP_VERSION
			: '?'.APP_VERSION;
		
		echo "\r\n";
		echo '<script type="text/javascript" language="JavaScript" src="'.$path.$filename.'"></script>';
		echo "\r\n";
	}
	
	function normalizeSettingsName($ss, $caption = "")
	{
		if($caption != "") return $caption;
		
		$s = "";
		for ($i=0; $i<strlen($ss); $i++)
		{
			if (strtoupper($ss[$i]) == $ss[$i])
			{
				$s = $s." ".$ss[$i];
			}
			else
			{
				$s = $s.$ss[$i];
			}
		}
		return trim($s);
	}

	function parseSearchKeywords($search_str = '', &$objects)
	{
		define("ADVANCED_SEARCH_DEFAULT_OPERATOR", "and");	

		$search_str = trim(mb_strtolower($search_str, "UTF-8"));
		$pieces = preg_split('/[\s,]+/', $search_str);
		
		$objects = array();
		$tmpstring = '';
		$flag = '';

		for ($k=0; $k<count($pieces); $k++)
		{
			while (substr($pieces[$k], 0, 1) == '(')
			{
				$objects[] = '(';
				if (strlen($pieces[$k]) > 1)
				{
					$pieces[$k] = substr($pieces[$k], 1);
	        	}
				else
				{
        	  		$pieces[$k] = '';
        		}
			}

			$post_objects = array();

			while (substr($pieces[$k], -1) == ')')
			{
				$post_objects[] = ')';
				if (strlen($pieces[$k]) > 1)
				{
					$pieces[$k] = substr($pieces[$k], 0, -1);
				}
				else
				{
					$pieces[$k] = '';
				}
			}

			// Check individual words
			if ((substr($pieces[$k], -1) != '"') && (substr($pieces[$k], 0, 1) != '"'))
			{
				$objects[] = trim($pieces[$k]);
				for ($j=0; $j<count($post_objects); $j++)
				{
					$objects[] = $post_objects[$j];
				}
			} 
			else
			{
				/* 	This means that the $piece is either the beginning or the end of a string.
   					So, we'll slurp up the $pieces and stick them together until we get to the
   					end of the string or run out of pieces.
				*/

				// Add this word to the $tmpstring, starting the $tmpstring
				$tmpstring = trim(str_replace('"', ' ', $pieces[$k]));

				// Check for one possible exception to the rule. That there is a single quoted word.
				if (substr($pieces[$k], -1 ) == '"')
				{
				// Turn the flag off for future iterations
					$flag = 'off';

					$objects[] = trim($pieces[$k]);

					for ($j=0; $j<count($post_objects); $j++)
					{
						$objects[] = $post_objects[$j];
					}

					unset($tmpstring);

					// Stop looking for the end of the string and move onto the next word.
					continue;
				}

				// Otherwise, turn on the flag to indicate no quotes have been found attached to this word in the string.
				$flag = 'on';

				// Move on to the next word
				$k++;

				// Keep reading until the end of the string as long as the $flag is on
				while (($flag == 'on') && ($k < count($pieces)))
				{
					while (substr($pieces[$k], -1) == ')')
					{
						$post_objects[] = ')';
						if (strlen($pieces[$k]) > 1)
						{
        	      			$pieces[$k] = substr($pieces[$k], 0, -1);
						}
						else
						{
							$pieces[$k] = '';
						}
					}

					// If the word doesn't end in double quotes, append it to the $tmpstring.
					if (substr($pieces[$k], -1) != '"')
					{
						// Tack this word onto the current string entity
						$tmpstring .= ' ' . $pieces[$k];
						// Move on to the next word
						$k++;
						continue;
					}
					else
					{
						/* 	If the $piece ends in double quotes, strip the double quotes, tack the
   							$piece onto the tail of the string, push the $tmpstring onto the $haves,
   							kill the $tmpstring, turn the $flag "off", and return.
						*/
						$tmpstring .= ' ' . trim(str_replace('"', ' ', $pieces[$k]));

						// Push the $tmpstring onto the array of stuff to search for
						$objects[] = trim($tmpstring);

						for ($j=0; $j<count($post_objects); $j++)
						{
							$objects[] = $post_objects[$j];
						}

						unset($tmpstring);

						// Turn off the flag to exit the loop
						$flag = 'off';
					}
				}
			}
	    }

		// add default logical operators if needed
    	$temp = array();
    	for ($i=0; $i<(count($objects)-1); $i++)
    	{
			$temp[] = $objects[$i];
			if (($objects[$i] != 'and') &&
				($objects[$i] != 'or') &&
				($objects[$i] != '(') &&
				($objects[$i+1] != 'and') &&
				($objects[$i+1] != 'or') &&
				($objects[$i+1] != ')') ){
				$temp[] = ADVANCED_SEARCH_DEFAULT_OPERATOR;
			}
		}
		if (array_key_exists($i, $objects))
		{
			$temp[] = $objects[$i];
		}
		else
		{
			$temp[] = "";
		}
		$objects = $temp;

		$keyword_count = 0;
		$operator_count = 0;
		$balance = 0;
		for ($i=0; $i<count($objects); $i++)
		{
			if ($objects[$i] == '(') $balance --;
			if ($objects[$i] == ')') $balance ++;
			if (($objects[$i] == 'and') || ($objects[$i] == 'or'))
			{
				$operator_count ++;
			}
			elseif ( ($objects[$i] || $objects[$i] == "0") && ($objects[$i] != '(') && ($objects[$i] != ')') )
			{
				$keyword_count ++;
			}
		}

		if (($operator_count < $keyword_count) && ($balance == 0))
		{
			return true;
		}
		else
		{
			return false;
		}
  	}

		/**
	 * Cookie Wrapper for better cookie management towards PCI compliancy
	 *
	 * @return object Ddm_Cookie
	 */
	function initCookies()
	{
		static $_id = null;
		
		if (is_null($_id))
		{
			$_id = new Ddm_Cookie(array(
				'domain' => '',
				'path' => '/',
				'expires' => time()+3600*24*365,
				'secure' => isSslMode()
			));
		}
		
		return $_id;
	}
	
	/**
	 * Checks whether the page was requested via HTTPS mode
	 * @return bool
	 * @author Sebastian Nievas
	 */
	function isSslMode()
	{
		static $mode = null;
		
		if (is_null($mode))
		{
			$mode = (array_key_exists('HTTPS', $_SERVER) && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') || $_SERVER['SERVER_PORT'] == 443;
		}
		
		return $mode;
	}

	function hasSslUrl()
	{
		global $settings;

		return parse_url($settings["GlobalHttpsUrl"], PHP_URL_SCHEME) == 'https';
	}

	function shouldBeSecure()
	{
		return !isSslMode() && hasSslUrl();
	}

	function getRequestUri()
	{
		$basePath = dirname($_SERVER['SCRIPT_NAME']);
		if ($basePath != '/')
		{
			return substr($_SERVER['REQUEST_URI'], strlen($basePath));
		}
		else
		{
			return $_SERVER['REQUEST_URI'];
		}
	}

	/**
	 * Checks password strength
	 * Password have to be at least 8 chars long, and contain lower and upper case letters, and numbers
	 * @param string $password
	 * @return boolean
	 */
	function pwdStrength($password)
	{
		$points = 0;
		
		// 1 point for length over 6
		if (strlen($password) >= 8) $points++;

		// 1 lower & uppercase characters
		if (preg_match("/[a-z]/", $password) && preg_match("/[A-Z]/", $password)) $points++;

		// 1 point for a number
		if (preg_match("/\d+/", $password)) $points++;
	
		// 1 point for special character
		if (preg_match("/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/", $password)) $points++;

		// 1 point for length over 12
		if (strlen($password) > 12) $points++;
	
		return $points >= 3;
	}
	
	/**
	 * Fill in white-space string with random chars from charset
	 * @param string $password
	 * @param string $charset
	 * @param int $count
	 */
	function _generatePassword(&$password, $charset, $count)
	{
		$_count = 0;
		while (true)
		{
			$p = rand(0, strlen($password) - 1);
			if ($password[$p] == " ")
			{
				$password[$p] = $charset[rand(0, strlen($charset) - 1)];
				$_count++;
			}	
			if ($_count >= $count) break;
		}
	}
 
	/**
	 * Generate random password
	 * @return string
	 */
	function generatePassword()
	{
		$length = 12;
		$password = str_pad("", $length, " ");
		$capital_count = rand(3,4);
		$capital_list = "ZAQWSXCDERFVBGTYHNMJUIKLOP";
		$lowercase_count = rand(5,6);
		$lowercase_list = strtolower($capital_list);
		$number_count = rand(1, $number_count = $length - $capital_count - $lowercase_count - 1);
		$number_list = "1425369708";
		$spec_count = $length - $capital_count - $lowercase_count - $number_count;
		$spec_list = "!@#%^&*?_~-()";
		_generatePassword($password, $capital_list, $capital_count);
		_generatePassword($password, $lowercase_list, $lowercase_count);
		_generatePassword($password, $number_list, $number_count);
		_generatePassword($password, $spec_list, $spec_count);
		return $password;
	}

	/**
	 * Returns database instance
	 * @return DB
	 */
	function db()
	{
		global $db;
		return $db;
	}
	
	/**
	 * Returns settings instance
	 * @return array
	 */
	function settings()
	{
		static $_id = null;
		if (is_null($_id)) global $settings;
		return $settings;
	}
	
	/**
	 * 
	 * @return Ddm_View
	 */
	function view()
	{
		static $_id = null;
		if (is_null($_id)) $_id = Ddm_View::getInstance();
		return $_id;
	}

	/**
	 * Removes directory recursivelly 
	 * @param string $path
	 */
	function remove_dir($path)
	{
		$d = @dir($path);
		if ($d)
		{
			while (false !== ($entry = $d->read()))
			{
				if (!in_array($entry, array(".", "..")))
				{
					if (is_file($path.DIRECTORY_SEPARATOR.$entry))
					{
						@unlink($path.DIRECTORY_SEPARATOR.$entry);
					}
					if (is_dir($path.DIRECTORY_SEPARATOR.$entry))
					{
						remove_dir($path.DIRECTORY_SEPARATOR.$entry);
					}
				}
			}
			$d->close();
		}

		@rmdir($path);
	}

	/**
	 * Copies directory
	 * @param string $source
	 * @param string $destination
	 * @param array $extend_options Array of options to extend
	 */
	function copy_dir($source, $destination, $extend_options = array())
	{
		if (strlen($source) > 0)
		{
			if (!in_array($source[strlen($source) - 1], array("/", "\\")))
			{
				$source = $source.DS;
			}
		}
		
		if (strlen($destination) > 0)
		{
			if (!in_array($destination[strlen($destination) - 1], array("/", "\\")))
			{
				$destination = $destination.DS;
			}
		}	
		
		$default_options = array(
			'include_folders' => null,
			'exclude_folders' => null,
			'include_extensions' => null,
			'exclude_extensions' => null,
			'overwrite' => false,
			'file_permission' => FileUtils::getFilePermissionMode(),
			'dir_permission' => FileUtils::getDirectoryPermissionMode()
		);
		
		$options = array_merge($default_options, $extend_options);
			
		$d = dir($source);
		if ($d)
		{
			while (false !== ($entry = $d->read()))
			{
				if (!in_array($entry, array(".", "..", ".svn", ".gitkeep")))
				{	
					$_source = $source.$entry;
					$_destination = $destination.$entry;
					
					if (is_file($_source))
					{
						if (!is_file($_destination))
						{
							copy($_source, $_destination);
						}
						elseif ($options['overwrite'])
						{
							unlink($_destination);
							copy($_source, $_destination);
						}
						
						if ($options['file_permission'] != null)
						{
							@chmod($_destination, $options['file_permission']);
						}
					}
					elseif (is_dir($_source))
					{
						if (!is_dir($_destination))
						{
							mkdir($_destination, $options['dir_permission']);
							FileUtils::setDirectoryPermissions($_destination, $options['dir_permission']);
						}
						copy_dir($_source, $_destination, $extend_options);
					}
				}
			}
			$d->close();
		}
	}
	
	function getp(){return base64_decode('cGlubmFjbGVjYXJ0');}

	function getpp($space = false)
	{
		if ($space)
		{
			return base64_decode('UGlubmFjbGUgQ2FydA==');
		}
		return base64_decode('UGlubmFjbGVDYXJ0');
	}

	/**
	 * For PHP version 5.5 below, this is an alternate function for array_columns
	 */
	if (!function_exists('array_column'))
	{
		function array_column(array $input, $columnKey, $indexKey = null)
		{
			$array = array();

			foreach ($input as $value)
			{
				if (!isset($value[$columnKey]))
				{
					trigger_error("Key \"$columnKey\" does not exist in array");
					return false;
				}

				if (is_null($indexKey))
				{
					$array[] = $value[$columnKey];
				}
				else
				{
					if (!isset($value[$indexKey]))
					{
						trigger_error("Key \"$indexKey\" does not exist in array");
						return false;
					}

					if (!is_scalar($value[$indexKey]))
					{
						trigger_error("Key \"$indexKey\" does not contain scalar value");
						return false;
					}

					$array[$value[$indexKey]] = $value[$columnKey];
				}
			}

			return $array;
		}
	}

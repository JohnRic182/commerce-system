<?php 
/**
 * Order actions definitions
 */
define('ORDER_ADD_ITEM', 'AddItem');
define('ORDER_REMOVE_ITEM', 'RemoveItem');
define('ORDER_CLEAR_ITEMS', 'ClearItems');
define('ORDER_UPDATE_ITEMS', 'UpdateItems');
define('ORDER_UNLINK_USER', 'UnlinkUser');
define('ORDER_RE_UPDATE_ITEMS', 'ReUpdateItems');
define('ORDER_STORE_PAYPAL_EC_INFO', 'StorePayPalECInfo');
define('ORDER_PROCESS_PAYMENT', 'ProcessPayment');
define('ORDER_PROCESS_PAYMENT_VALIDATION', 'ProcessPaymentValidation');
define('ORDER_COMPLETE_ORDER', 'CompleteOrder');
define('ORDER_WISHLIST_TO_CART', 'OrderWishlistToCart');
define('ORDER_OPC', 'OnePageCheckout');
define('ORDER_ADD_GIFT_CERT', 'AddGiftCert');
define('ORDER_RESTORE_CART', 'RestoreCart');

define('ORDER_ADD_RESULT_NOT_FOUND', 0);
define('ORDER_ADD_RESULT_ADDED', 1);
define('ORDER_ADD_RESULT_STOCK_ISSUE', -1);

/**
 * Order class
 */
class ORDER
{
	const STATUS_NEW = 'New';
	const STATUS_ABANDON = 'Abandon';
	const STATUS_PROCESS = 'Process';
	const STATUS_BACKORDER = 'Backorder';
	const STATUS_CANCELED = 'Canceled';
	const STATUS_COMPLETED = 'Completed';
	const STATUS_FAILED = 'Failed';

	const PAYMENT_STATUS_PENDING = 'Pending';
	const PAYMENT_STATUS_PARTIAL = 'Partial';
	const PAYMENT_STATUS_RECEIVED = 'Received';
	const PAYMENT_STATUS_REFUNDED = 'Refunded';
	const PAYMENT_STATUS_DECLINED = 'Declined';
	const PAYMENT_STATUS_CANCELED = 'Canceled';
	const PAYMENT_STATUS_AUTH_EXPIRED = 'Auth Expired';
	const PAYMENT_STATUS_REVERSED = 'Reversed';
	const PAYMENT_STATUS_REVIEW = 'Review';
	const PAYMENT_STATUS_ERROR = 'Error';

	const FULFILLMENT_STATUS_PENDING = 'pending';
	const FULFILLMENT_STATUS_PARTIAL = 'partial';
	const FULFILLMENT_STATUS_COMPLETED = 'completed';

	const TAX_EXEMPT_NO = 'No';
	const TAX_EXEMPT_YES = 'Yes';
	const TAX_EXEMPT_PARTIAL = 'Partial';

	const ORDER_TYPE_WEB = 'Web';
	const ORDER_TYPE_RECURRING = 'Recurring';

	// order properties
	private $_db = null;
	private $_settings = null;
	private $_user = null;
	
	private $_userLevel = 0;
	
	public $oid = 0, $order_num = 0;

	/** @var int */
	public $userId;

	public $orderType = self::ORDER_TYPE_WEB;

	public $errors;

	private $forceRecalc = false;

	public $securityId = '';

	public $status_date;
	public $status_date_formatted;

	//products in cart
	//var $productsCount = 0;
	public $itemsCount = 0;
	
	//subtotal amount
	public $subtotalAmount = 0;
	public $subtotalAmountWithTax = 0;
	public $cartSubtotalAmount = 0;

	//discount
	public $discountAmount = 0;
	public $discountValue = 0;
	public $discountType = '';
	
	//promo discount
	public $promoDiscountAmount = 0;
	public $promoDiscountValue = 0;
	public $promoDiscountType = '';
	public $promoType = '';
	public $promoCampaignId = 0;
	
	//tax
	public $taxCountryId = 0;
	public $taxStateId = 0;
	public $taxAmount = 0;
	public $taxExempt = ORDER::TAX_EXEMPT_NO;

	//handling - common
	public $handlingSeparated = false;
	public $handlingFee = 0.00;
	public $handlingFeeType = '';	
	public $handlingFeeWhenShippingFree = 0.00;
	public $handlingText = '';
	public $handlingAmount = 0.00; // when shipping IS NOT FREE
	//handling - tax-related
	public $handlingTaxable = false; // $settings['ShippingHandlingTaxable']
	public $handlingTaxClassId = 0; // $settings['ShippingHandlingTaxClassId']
	public $handlingTaxRate = 0.00;
	public $handlingTaxDescription = '';
	public $handlingTaxAmount = 0.00;
	
	//shipping
	public $shippingFreeProductsCount = 0;

	public $shippingRequired = false;
	public $shippingSSID = '0';
	public $shippingCarrierID = '';
	public $shippingMethodID = '';
	public $shippingCMName = '';
	public $shippingAddress = array();
	public $shippingAmount = 0.00;

	public $shippingTrackingNumber = '';
	public $shippingTrackingNumberType = '';
	public $shippingTracking = array();

	public $shippingTaxable = 0;
	public $shippingTaxClassId = 0;
	public $shippingTaxRate = 0.00;
	public $shippingTaxDescription = '';
	public $shippingTaxAmount = 0.00;

	public $giftMessage = '';

	public $order_source = '';
	public $offsite_campaign_id = 0;

	//payment
	public $paymentIsRealtime = '';
	public $paymentMethodId = '';
	public $paymentMethodName = '';
	public $paymentMethodDescription = '';
	public $paymentGatewayId = '';

	public $orderFormId = 0;

	/** @var int */
	public $paymentAttemptsCount = 0;

	public $userIpAddress = null;

	//order total amount
	public $totalAmount = 0;

	public $status;
	public $payment_status;

	public $fulfillmentStatus = ORDER::FULFILLMENT_STATUS_PENDING;

	/** @var DateTime */
	public $orderCreatedDate = null;

	/** @var DateTime */
	public $statusChangeDate = null;

	/** @var DateTime  */
	public $orderPlacedDate = null;

	// *** GIFT CERTIFICATE START
	public $gift_cert_amount = 0;
	// *** GIFT CERTIFICATE END

	//custom fields
	private $_customFields = null;

	protected $msg = null;

	public $items;
	/** @var array  */
	public $lineItems;

	/** @var array */
	public $fulfillments = array();

	/**
	 * Order class constructor
	 * 
	 * @param DB $db
	 * @param array $settings
	 * @param USER $user
	 * @param int $oid
	 * @param string $securityId
	 * 
	 * @return ORDER
	 */
	public function __construct(&$db, &$settings, &$user, $oid = 0, $securityId = '')
	{
		$this->_db = $db;
		$this->_settings = &$settings;
		$this->_user = $user;

		// Ensure that items / lineItems are valid arrays
		$this->lineItems = array();
		$this->items = array();

		$this->setId($oid);
		$this->errors = array();
		$this->setSecurityId($securityId);
		
		// *** GIFT CERTIFICATE START
		if ($settings['enable_gift_cert'] == 'Yes' && $this->getId() > 0)
		{
			$giftCertificateRepository = $this->getGiftCertificateRepository();
			$data = $giftCertificateRepository->getOrderGiftCert($this->getId());
			if ($data['gift_cert_valid'] == '1')
			{
				$this->setGiftCertificateAmount($data['gift_cert_amount']);
			} 
			else
			{
				$this->setGiftCertificateAmount(0);
			}
		}
		// *** GIFT CERTIFICATE END
		
		$this->setUserLevel($user->getLevel());
		//$this->setStatusChangeDate(new DateTime());
	}

	/**
	 * Generate order security id
	 *
	 * @return string
	 */
	public static function generateSecurityId()
	{
		return md5(uniqid(rand(), true)).time();
	}

	/**
	 * Get order data from session
	 * 
	 * @param DB $db
	 * @param array $settings
	 * @param USER $user
	 * @param bool $needDbRecord
	 * @param boolean $checkUserChange
	 * 
	 * @return ORDER
	 */
	public static function getFromSession(&$db, &$settings, &$user, $needDbRecord = false, $checkUserChange = true)
	{
		global $msg, $userCookie;

		$persistOrder = false;

		$order = new ORDER($db, $settings, $user);

		$order->setLanguageMessages($msg);
		$order->setUserLevel($user->auth_ok ? $user->getLevel() : ($userCookie ? $userCookie['l'] : 0));

		if (isset($_SESSION['osc_source'])) $order->setOrderSource($_SESSION['osc_source']);
		if (isset($_SESSION['osc_campaign_id'])) $order->setOffsiteCampaignId($_SESSION['osc_campaign_id']);

		/**
		 * Get order security ID from environment
		 */
		$securityId = isset($_COOKIE['_pcod']) ? $_COOKIE['_pcod'] : '';
		$securityId = $securityId == '' ? isset($_POST['_pcod']) ? $_POST['_pcod'] : $securityId : $securityId;
		$securityId = $securityId == '' ? isset($_GET['_pcod']) ? $_GET['_pcod'] : $securityId : $securityId;

		if ($securityId != '')
		{
			$orderData = $db->selectOne(
				'SELECT oid, uid FROM '.DB_PREFIX.'orders WHERE security_id="'.$db->escape($securityId).'" AND (status="Abandon" OR status="New") AND (payment_status = "Pending")'
			);

			if ($orderData)
			{
				$needDbRecord = false;

				$order->setId($orderData['oid']);

				$order->getOrderData();

				$order->setSecurityId($securityId);

				if ($checkUserChange && ($orderData['uid'] != $user->getId()))// && ($user->getId() > 0))
				{
					$order->setUserId($user->getId());
					$persistOrder = true;
				}
			}
		}

		if ($needDbRecord)
		{
			$securityId = ORDER::generateSecurityId();

			ORDER::setCookie($securityId);

			$order->setSecurityId($securityId);
			$order->setUserId($user->getId());
			$order->setOrderCreatedDate(new DateTime());
			$order->setStatusChangeDate(new DateTime());
			$order->setOrderType(ORDER::ORDER_TYPE_WEB);
			$order->setStatus(ORDER::STATUS_NEW);
			$order->setPaymentStatus(ORDER::PAYMENT_STATUS_PENDING);
			$order->setFulfillmentStatus(ORDER::FULFILLMENT_STATUS_PENDING);

			$persistOrder = true;
		}

		if ($persistOrder)
		{
			$order->persist();
		}

		return $order;
	}

	/**
	 * Get order by ID
	 * 
	 * @param DB $db
	 * @param array $settings
	 * @param USER $user
	 * @param int $oid
	 * 
	 * @return ORDER
	 */
	public static function getById(&$db, &$settings, &$user, $oid)
	{
		global $msg;

		$data = $db->selectOne('SELECT oid, security_id FROM '.DB_PREFIX.'orders WHERE oid='.intval($oid).' AND uid='.$user->getId());

		if ($data)
		{
			// User owns order
			$order = new ORDER($db, $settings, $user, $oid, $data['security_id']);

			$order->setLanguageMessages($msg);

			Tax_ProviderFactory::getTaxProvider()->initTax($order);
			return $order;
		}

		return null;
	}

	/**
	 * @param DB $db
	 * @param $errors
	 * @return bool
	 */
	public function checkDataIntegrity(DB $db, &$errors)
	{
		$errors = array();

		if ($this->checkShippingRequired())
		{
			$incompleteShipments = $db->selectOne('
				SELECT COUNT(*) AS c
				FROM ' . DB_PREFIX . 'orders_shipments
				WHERE oid=' . intval($this->getId()) . ' AND shipping_method_id=""
			');

			if ($incompleteShipments['c'] > 0)
			{
				$errors[] = 'One or more shipments do not have shipping method assigned. Checkout page will be refreshed.';
			}
		}

		return count($errors) == 0;
	}

	/**
	 * Set order's id
	 *
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->oid = $id;
	}

	/**
	 * Returns order id ($oid)
	 *
	 * @return int
	 */
	public function getId()
	{
		return intval($this->oid);
	}

	/**
	 * Sets current user id
	 *
	 * @param int $userId
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;
	}

	/**
	 * Returns current user id
	 * @return int
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * Returns assigned users's class instance
	 *
	 * @return USER
	 */
	public function getUser()
	{
		return $this->_user;
	}

	/**
	 * Returns user's level
	 *
	 * @return int
	 */
	public function getUserLevel()
	{
		return $this->getUser()->getLevel();
	}

	/**
	 * Sets order security id
	 *
	 * @param $securityId
	 */
	public function setSecurityId($securityId)
	{
		$this->securityId = $securityId;
	}

	/**
	 * Returns order security id
	 *
	 * @return string
	 */
	public function getSecurityId()
	{
		return $this->securityId;
	}

	/**
	 * Setter for order number
	 *
	 * @param string $orderNumber value to set
	 */
	public function setOrderNumber($orderNumber)
	{
		$this->order_num = $orderNumber;
	}

	/**
	 * Getter for order number
	 *
	 * @return string
	 */
	public function getOrderNumber()
	{
		return $this->order_num;
	}

	/**
	 * Set order type
	 *
	 * @param $orderType
	 */
	public function setOrderType($orderType)
	{
		$this->orderType = $orderType;
	}

	/**
	 * Get order type
	 *
	 * @return string
	 */
	public function getOrderType()
	{
		return $this->orderType;
	}

	/**
	 * @return bool
	 */
	public function getForceRecalc()
	{
		return $this->forceRecalc;
	}

	/**
	 * Setter for items count
	 *
	 * @param int $itemsCount value to set
	 */
	public function setItemsCount($itemsCount)
	{
		$this->itemsCount = $itemsCount;
	}

	/**
	 * Getter for items count
	 *
	 * @return int
	 */
	public function getItemsCount()
	{
		return $this->itemsCount;
	}

	/**
	 * Set tax country id
	 *
	 * @param int $countryId
	 */
	public function setTaxCountryId($countryId)
	{
		$this->taxCountryId = $countryId;
	}

	/**
	 * Returns tax country id
	 *
	 * @return int
	 */
	public function getTaxCountryId()
	{
		return $this->taxCountryId;
	}

	/**
	 * Set tax state id
	 *
	 * @param int $taxStateId
	 */
	public function setTaxStateId($taxStateId)
	{
		$this->taxStateId = $taxStateId;
	}

	/**
	 * Returns tax state id
	 *
	 * @return int
	 */
	public function getTaxStateId()
	{
		return $this->taxStateId;
	}

	/**
	 * Setter for subtotal amount
	 *
	 * @param float $subtotalAmount value to set
	 */
	public function setSubtotalAmount($subtotalAmount)
	{
		$this->subtotalAmount = $subtotalAmount;
	}

	/**
	 * Getter for subtotal amount
	 *
	 * @return float
	 */
	public function getSubtotalAmount()
	{
		return $this->subtotalAmount;
	}

	/**
	 * Set cart subtotal amount
	 *
	 * @param $cartSubtotalAmount
	 */
	public function setCartSubtotalAmount($cartSubtotalAmount)
	{
		$this->cartSubtotalAmount = $cartSubtotalAmount;
	}

	/**
	 * Get cart subtotal amount
	 *
	 * @return decimal
	 */
	public function getCartSubtotalAmount()
	{
		return $this->cartSubtotalAmount;
	}

	/**
	 * Set subtotal with tax
	 *
	 * @param float $subtotalAmountWithTax
	 */
	public function setSubtotalAmountWithTax($subtotalAmountWithTax)
	{
		$this->subtotalAmountWithTax = $subtotalAmountWithTax;
	}

	/**
	 * Returns subtotal with tax
	 *
	 * @return float
	 */
	public function getSubtotalAmountWithTax()
	{
		return $this->subtotalAmountWithTax;
	}

	/**
	 * Setter for discount amount
	 *
	 * @param float $discountAmount value to set
	 */
	public function setDiscountAmount($discountAmount)
	{
		$this->discountAmount = $discountAmount;
	}

	/**
	 * Getter for discount amount
	 *
	 * @return float
	 */
	public function getDiscountAmount()
	{
		return $this->discountAmount;
	}

	public $discountAmountWithTax;
	public function setDiscountAmountWithTax($discountAmountWithTax)
	{
		$this->discountAmountWithTax = $discountAmountWithTax;
	}

	public function getDiscountAmountWithTax()
	{
		return $this->discountAmountWithTax;
	}

	/**
	 * Setter for discount value
	 *
	 * @param float $discountValue value to set
	 */
	public function setDiscountValue($discountValue)
	{
		$this->discountValue = $discountValue;
	}

	/**
	 * Getter for discount value
	 *
	 * @return float
	 */
	public function getDiscountValue()
	{
		return $this->discountValue;
	}

	/**
	 * Setter for discount type
	 *
	 * @param string $discountType value to set
	 */
	public function setDiscountType($discountType)
	{
		$this->discountType = $discountType;
	}

	/**
	 * Getter for discount type
	 *
	 * @return string
	 */
	public function getDiscountType()
	{
		return $this->discountType;
	}

	/**
	 * Calculate discount percentage
	 *
	 * @return float|int
	 */
	public function getDiscountPercentage()
	{
		if ($this->getDiscountAmount() == 0)
		{
			return 0;
		}

		return $this->getDiscountType() == 'percent' ? $this->getDiscountValue() : $this->getDiscountValue() / ($this->getSubtotalAmount() / 100);
	}

	/**
	 * Setter for promo discount amount
	 *
	 * @param float $promoDiscountAmount value to set
	 */
	public function setPromoDiscountAmount($promoDiscountAmount)
	{
		$this->promoDiscountAmount = $promoDiscountAmount;
	}

	/**
	 * Getter for promo discount amount
	 *
	 * @return float
	 */
	public function getPromoDiscountAmount()
	{
		return $this->promoDiscountAmount;
	}

	public $promoDiscountAmountWithTax;
	public function setPromoDiscountAmountWithTax($promoDiscountAmountWithTax)
	{
		$this->promoDiscountAmountWithTax = $promoDiscountAmountWithTax;
	}

	public function getPromoDiscountAmountWithTax()
	{
		return $this->promoDiscountAmountWithTax;
	}

	/**
	 * Setter for promo discount type
	 *
	 * @param string $promoDiscountType value to set
	 */
	public function setPromoDiscountType($promoDiscountType)
	{
		$this->promoDiscountType = $promoDiscountType;
	}

	/**
	 * Getter for set promo discount type
	 *
	 * @return string
	 */
	public function getPromoDiscountType()
	{
		return $this->promoDiscountType;
	}

	/**
	 * Get promo discount percentage
	 *
	 * @return float|int
	 */
	public function getPromoDiscountPercentage()
	{
		if ($this->getPromoDiscountAmount() == 0)
		{
			return 0;
		}

		return $this->getPromoDiscountType() == 'percent' ? $this->getPromoDiscountValue() : $this->getPromoDiscountValue() / ($this->getSubtotalAmount() / 100);
	}

	/**
	 * Setter for promo discount value
	 *
	 * @param float $promoDiscountValue value to set
	 */
	public function setPromoDiscountValue($promoDiscountValue)
	{
		$this->promoDiscountValue = $promoDiscountValue;
	}

	/**
	 * Getter for promo discount value
	 *
	 * @return float
	 */
	public function getPromoDiscountValue()
	{
		return $this->promoDiscountValue;
	}

	/**
	 * Setter for promo discount campaign id
	 *
	 * @param int $promoCampaignId value to set
	 */
	public function setPromoCampaignId($promoCampaignId)
	{
		$this->promoCampaignId = $promoCampaignId;
	}

	/**
	 * Getter for promo campaign id
	 *
	 * @return int
	 */
	public function getPromoCampaignId()
	{
		return $this->promoCampaignId;
	}

	/**
	 * Setter for promo type
	 *
	 * @param string $promoType value to set
	 */
	public function setPromoType($promoType)
	{
		$this->promoType = $promoType;
	}

	/**
	 * Getter for promo type
	 *
	 * @return string
	 */
	public function getPromoType()
	{
		return $this->promoType;
	}

	/**
	 * Setter for handling separated
	 *
	 * @param bool $handlingSeparated value to set
	 */
	public function setHandlingSeparated($handlingSeparated)
	{
		$this->handlingSeparated = $handlingSeparated;
	}

	/**
	 * Getter for handling separated
	 *
	 * @return bool
	 */
	public function getHandlingSeparated()
	{
		return $this->handlingSeparated;
	}

	/**
	 * Setter for handling fee
	 *
	 * @param float $handlingFee value to set
	 */
	public function setHandlingFee($handlingFee)
	{
		$this->handlingFee = $handlingFee;
	}

	/**
	 * Getter for handling fee
	 *
	 * @return float
	 */
	public function getHandlingFee()
	{
		return $this->handlingFee;
	}

	/**
	 * Setter for handling fee type
	 *
	 * @param float $handlingFeeType value to set
	 */
	public function setHandlingFeeType($handlingFeeType)
	{
		$this->handlingFeeType = $handlingFeeType;
	}

	/**
	 * Getter for handling fee type
	 *
	 * @return string
	 */
	public function getHandlingFeeType()
	{
		return $this->handlingFeeType;
	}

	/**
	 * Setter for handlingFeeWhenShippingFree
	 *
	 * @param float $handlingFeeWhenShippingFree value to set
	 */
	public function setHandlingFeeWhenShippingFree($handlingFeeWhenShippingFree)
	{
		$this->handlingFeeWhenShippingFree = $handlingFeeWhenShippingFree;
	}

	/**
	 * Getter for handlingFeeWhenShippingFree
	 *
	 * @return float
	 */
	public function getHandlingFeeWhenShippingFree()
	{
		return $this->handlingFeeWhenShippingFree;
	}

	/**
	 * Setter for handling text
	 *
	 * @param string $handlingText value to set
	 */
	public function setHandlingText($handlingText)
	{
		$this->handlingText = $handlingText;
	}

	/**
	 * Getter for handling text
	 *
	 * @return string
	 */
	public function getHandlingText()
	{
		return $this->handlingText;
	}

	/**
	 * Setter for handling amount
	 *
	 * @param float $handlingAmount value to set
	 */
	public function setHandlingAmount($handlingAmount)
	{
		$this->handlingAmount = $handlingAmount;
	}

	/**
	 * Getter for handling amount
	 *
	 * @return float
	 */
	public function getHandlingAmount()
	{
		return $this->handlingAmount;
	}

	/**
	 * Setter for handling taxable
	 *
	 * @param bool $handlingTaxable value to set
	 */
	public function setHandlingTaxable($handlingTaxable)
	{
		$this->handlingTaxable = $handlingTaxable;
	}

	/**
	 * Getter for handling taxable
	 *
	 * @return bool
	 */
	public function getHandlingTaxable()
	{
		return $this->handlingTaxable;
	}

	/**
	 * Setter for handlingTaxRate
	 *
	 * @param float $handlingTaxRate value to set
	 */
	public function setHandlingTaxRate($handlingTaxRate)
	{
		$this->handlingTaxRate = $handlingTaxRate;
	}

	/**
	 * Getter for handlingTaxRate
	 *
	 * @return float
	 */
	public function getHandlingTaxRate()
	{
		return $this->handlingTaxRate;
	}

	/**
	 * Setter for handling tax amount
	 *
	 * @param float $handlingTaxAmount value to set
	 */
	public function setHandlingTaxAmount($handlingTaxAmount)
	{
		$this->handlingTaxAmount = $handlingTaxAmount;
	}

	/**
	 * Getter for handling tax amount
	 *
	 * @return float
	 */
	public function getHandlingTaxAmount()
	{
		return $this->handlingTaxAmount;
	}

	/**
	 * Setter for handling tax description
	 *
	 * @param string $handlingTaxDescription value to set
	 */
	public function setHandlingTaxDescription($handlingTaxDescription)
	{
		$this->handlingTaxDescription = $handlingTaxDescription;
	}

	/**
	 * Getter for handling tax description
	 *
	 * @return string
	 */
	public function getHandlingTaxDescription()
	{
		return $this->handlingTaxDescription;
	}

	/**
	 * Setter for handling tax class id
	 *
	 * @param int $handlingTaxClassId value to set
	 */
	public function setHandlingTaxClassId($handlingTaxClassId)
	{
		$this->handlingTaxClassId = $handlingTaxClassId;
	}

	/**
	 * Getter for handling tax class id
	 *
	 * @return int
	 */
	public function getHandlingTaxClassId()
	{
		return $this->handlingTaxClassId;
	}

	/**
	 * Set update ip address
	 *
	 * @param $userIpAddress
	 */
	public function setUserIpAddress($userIpAddress)
	{
		$this->userIpAddress = $userIpAddress;
	}

	/**
	 * Get user ip address
	 *
	 * @return null
	 */
	public function getUserIpAddress()
	{
		return $this->userIpAddress;
	}

	/**
	 * Check if we have enough information to calculate totals and order is not just started
	 *
	 * @return bool
	 */
	public function getBillingCompleted()
	{
		$user = $this->getUser();
		return
			$user && !is_null($user) && $user->auth_ok
			&& $user->data['fname'] != '' && $user->data['lname'] != ''
			&& $user->data['address1'] != '' && $user->data['city'] != ''
			&& $user->data['country'] != '0' && $user->data['zip'] != ''
			&& $user->data['email'] != '';
	}

	/**
	 * Check if we have enough information to calc shipping
	 * In this case we my run all calculations,
	 * otherwise only subtotals
	 *
	 * @return bool
	 */
	public function getShippingComplete()
	{
		return
			$this->_settings['ShippingCalcEnabled'] == 'NO' ||
			(!$this->checkShippingRequired()) ||
			(
				(is_array($this->shippingAddress) && $this->shippingAddress['country_id'] > 0)
			);
	}

	/**
	 * Getter for shipping is free
	 *
	 * @return bool
	 */
	public function getShippingIsFree()
	{
		return false;
	}

	/**
	 * Setter for shipping free products count
	 *
	 * @param int $shippingFreeProductsCount value to set
	 */
	public function setShippingFreeProductsCount($shippingFreeProductsCount)
	{
		$this->shippingFreeProductsCount = $shippingFreeProductsCount;
	}

	/**
	 * Getter for shipping free products count
	 *
	 * @return int
	 */
	public function getShippingFreeProductsCount()
	{
		return $this->shippingFreeProductsCount;
	}

	/**
	 * Setter for shipping virtual
	 *
	 * @param bool $shippingRequired value to set
	 */
	public function setShippingRequired($shippingRequired)
	{
		$this->shippingRequired = $shippingRequired;
	}

	/**
	 * Getter for shipping virtual
	 *
	 * @return bool
	 */
	public function getShippingRequired()
	{
		return $this->shippingRequired;
	}

	/**
	 * Setter for shippingAddress
	 *
	 * @param array $shippingAddress value to set
	 */
	public function setShippingAddress($shippingAddress)
	{
		$this->shippingAddress = $shippingAddress;
	}

	/**
	 * Getter for shippingAddress
	 *
	 * @return array
	 */
	public function getShippingAddress()
	{
		if ((!$this->shippingAddress || empty($this->shippingAddress)) && $this->getId() > 0)
		{
			$this->getShippingAddressFromDb();
		}

		return $this->shippingAddress;
	}
		
	/**
	 * Setter for shippingAmount
	 *
	 * @param float $shippingAmount value to set
	 */
	public function setShippingAmount($shippingAmount)
	{
		$this->shippingAmount = $shippingAmount;
	}

	/**
	 * Getter for shippingAmount
	 *
	 * @return float
	 */
	public function getShippingAmount()
	{
		return $this->shippingAmount;
	}

	/**
	 * Setter for shippingTrackingNumber
	 *
	 * @param string $shippingTrackingNumber value to set
	 */
	public function setShippingTrackingNumber($shippingTrackingNumber)
	{
		$this->shippingTrackingNumber = $shippingTrackingNumber;
	}

	/**
	 * Getter for shippingTrackingNumber
	 *
	 * @return string
	 */
	public function getShippingTrackingNumber()
	{
		return $this->shippingTrackingNumber;
	}

	/**
	 * Setter for shippingTrackingNumberType
	 *
	 * @param string $shippingTrackingNumberType value to set
	 */
	public function setShippingTrackingNumberType($shippingTrackingNumberType)
	{
		$this->shippingTrackingNumberType = $shippingTrackingNumberType;
	}

	/**
	 * Getter for shippingTrackingNumberType
	 *
	 * @return string
	 */
	public function getShippingTrackingNumberType()
	{
		return $this->shippingTrackingNumberType;
	}

	/**
	 * Setter for shippingTracking
	 *
	 * @param array $shippingTracking
	 */
	public function setShippingTracking(array $shippingTracking)
	{
		$this->shippingTracking = $shippingTracking;
	}

	/**
	 * Getter for shippingTracking
	 *
	 * @return array
	 */
	public function getShippingTracking()
	{
		return $this->shippingTracking;
	}

	/**
	 * Setter for shippingTaxable
	 *
	 * @param bool $shippingTaxable value to set
	 */
	public function setShippingTaxable($shippingTaxable)
	{
		$this->shippingTaxable = $shippingTaxable;
	}

	/**
	 * Getter for shippingTaxable
	 *
	 * @return bool
	 */
	public function getShippingTaxable()
	{
		return $this->shippingTaxable;
	}

	/**
	 * Setter for shippingTaxClassId
	 *
	 * @param int $shippingTaxClassId value to set
	 */
	public function setShippingTaxClassId($shippingTaxClassId)
	{
		$this->shippingTaxClassId = $shippingTaxClassId;
	}

	/**
	 * Getter for shippingTaxClassId
	 *
	 * @return int
	 */
	public function getShippingTaxClassId()
	{
		return $this->shippingTaxClassId;
	}

	/**
	 * Setter for shippingTaxRate
	 *
	 * @param float $shippingTaxRate value to set
	 */
	public function setShippingTaxRate($shippingTaxRate)
	{
		$this->shippingTaxRate = $shippingTaxRate;
	}

	/**
	 * Getter for shippingTaxRate
	 *
	 * @return float
	 */
	public function getShippingTaxRate()
	{
		return $this->shippingTaxRate;
	}

	/**
	 * Setter for shippingTaxDescription
	 *
	 * @param string $shippingTaxDescription value to set
	 *
	 */
	public function setShippingTaxDescription($shippingTaxDescription)
	{
		$this->shippingTaxDescription = $shippingTaxDescription;
	}

	/**
	 * Getter for shippingTaxDescription
	 *
	 * @return string
	 */
	public function getShippingTaxDescription()
	{
		return $this->shippingTaxDescription;
	}

	/**
	 * Setter for shippingTaxAmount
	 *
	 * @param float $shippingTaxAmount value to set
	 */
	public function setShippingTaxAmount($shippingTaxAmount)
	{
		$this->shippingTaxAmount = $shippingTaxAmount;
	}

	/**
	 * Getter for shippingTaxAmount
	 *
	 * @return float
	 */
	public function getShippingTaxAmount()
	{
		return $this->shippingTaxAmount;
	}

	/**
	 * Check is it a doba product
	 *
	 * @param $item
	 *
	 * @return bool
	 */
	public function isDobaProduct(&$item)
	{
		return $item['is_doba'] == 'Yes';
	}

	/**
	 * Check is this product level shipping - do we have shipping price on product level?
	 *
	 * @param $item
	 *
	 * @return bool
	 */
	public function isProductLevelShipping(&$item)
	{
		return $item['product_is_shipping_price'] == 'Yes';
	}

	/**
	 * Check is item shippable
	 *
	 * @param $item
	 *
	 * @return bool
	 */
	public function isItemShippable(&$item, $ignoreFreeShipping)
	{
		if ($item['product_type'] != Model_Product::TANGIBLE) return false;

		return ($item['free_shipping'] == 'Yes' && $ignoreFreeShipping) || $item['free_shipping'] == 'No';
	}

	/**
	 * Setter for gift message
	 *
	 * @param string $giftMessage value to set
	 */
	public function setGiftMessage($giftMessage)
	{
		$this->giftMessage = $giftMessage;
	}

	/**
	 * Getter for gift message
	 *
	 * @return string
	 */
	public function getGiftMessage()
	{
		return $this->giftMessage;
	}

	/**
	 * Setter for tax amount
	 *
	 * @param float $taxAmount value to set
	 */
	public function setTaxAmount($taxAmount)
	{
		$this->taxAmount = $taxAmount;
	}

	/**
	 * Getter for tax amount
	 *
	 * @return float
	 */
	public function getTaxAmount()
	{
		return PriceHelper::round($this->taxAmount);
	}

	/**
	 * Set tax exemption status
	 *
	 * @param $exempt
	 */
	public function setTaxExempt($exempt)
	{
		$this->taxExempt = $exempt;
	}

	/**
	 * Return tax exemption status
	 *
	 * @return string
	 */
	public function getTaxExempt()
	{
		return $this->taxExempt;
	}

	/**
	 * Setter for total amount
	 *
	 * @param float $totalAmount value to set
	 */
	public function setTotalAmount($totalAmount)
	{
		$this->totalAmount = $totalAmount;
	}

	/**
	 * Getter for total amount
	 *
	 * @return float
	 */
	public function getTotalAmount()
	{
		return PriceHelper::round($this->totalAmount);
	}

	/**
	 * Sets gift certificate amount
	 * 
	 * @param float $amount
	 */
	public function setGiftCertificateAmount($amount)
	{
		$this->gift_cert_amount = $amount;
	}

	/**
	 * Return gift certificate amount (total value that remains on voucher's 'account')
	 * 
	 * @return float
	 */
	public function getGiftCertificateAmount()
	{
		return $this->gift_cert_amount;
	}

	/**
	 * Getter for amount that should be paid after gift certificate / or partial payment applied
	 *
	 * @return float
	 */
	public function getRemainingAmountToBePaid()
	{
		$totalAmount = $this->getTotalAmount();
		$giftCertUsedAmount = $this->getGiftCertificateAmountUsedBeforePayment();

	    return $totalAmount - $giftCertUsedAmount;
	}
	
	/**
	 * Amount that will be used from gift certificate 'voucher account'
	 * 
	 * @return float
	 */
	public function getGiftCertificateAmountUsedBeforePayment()
	{
		$totalAmount = $this->getTotalAmount();
		$giftCertAmount = $this->getGiftCertificateAmount();

		return $totalAmount > $giftCertAmount ? $giftCertAmount : $totalAmount;
	}

	/**
	 * Set order status
	 *
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * Get order status
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set payment status
	 *
	 * @param string $payment_status
	 */
	public function setPaymentStatus($payment_status)
	{
		$this->payment_status = $payment_status;
	}

	/**
	 * Get payment status
	 *
	 * @return string
	 */
	public function getPaymentStatus()
	{
		return $this->payment_status;
	}

	/**
	 * @param mixed $fulfillmentStatus
	 */
	public function setFulfillmentStatus($fulfillmentStatus)
	{
		$this->fulfillmentStatus = $fulfillmentStatus;
	}

	/**
	 * @return mixed
	 */
	public function getFulfillmentStatus()
	{
		return $this->fulfillmentStatus;
	}

	/**
	 * Set order created date
	 *
	 * @param DateTime $date
	 */
	public function setOrderCreatedDate(DateTime $date)
	{
		$this->orderCreatedDate = $date;
	}

	/**
	 * Get order created date
	 *
	 * @return DateTime
	 */
	public function getOrderCreatedDate()
	{
		return $this->orderCreatedDate;
	}

	/**
	 * Set order placed date
	 *
	 * @param DateTime $date
	 */
	public function setOrderPlacedDate(DateTime $date)
	{
		$this->orderPlacedDate = $date;
	}

	/**
	 * Get order placed date
	 *
	 * @return DateTime
	 */
	public function getOrderPlacedDate()
	{
		return $this->orderPlacedDate;
	}

	/**
	 * Set status change date
	 *
	 * @param DateTime $date
	 */
	public function setStatusChangeDate(DateTime $date)
	{
		$this->statusChangeDate = $date;
	}

	/**
	 * Get status change date
	 *
	 * @return DateTime
	 */
	public function getStatusChangeDate()
	{
		return $this->statusChangeDate;
	}

	/**
	 * @param string $paymentIsRealtime
	 */
	public function setPaymentIsRealtime($paymentIsRealtime)
	{
		$this->paymentIsRealtime = $paymentIsRealtime;
	}

	/**
	 * @return string
	 */
	public function getPaymentIsRealtime()
	{
		return $this->paymentIsRealtime;
	}

	/**
	 * @param string $paymentGatewayId
	 */
	public function setPaymentGatewayId($paymentGatewayId)
	{
		$this->paymentGatewayId = $paymentGatewayId;
	}

	/**
	 * @return string
	 */
	public function getPaymentGatewayId()
	{
		return $this->paymentGatewayId;
	}

	public function setOrderFormID($id)
	{
		$this->orderFormId = $id;
	}

	public function getOrderFormID()
	{
		return $this->orderFormId;
	}

	/**
	 * @param string $paymentMethodDescription
	 */
	public function setPaymentMethodDescription($paymentMethodDescription)
	{
		$this->paymentMethodDescription = $paymentMethodDescription;
	}

	/**
	 * @return string
	 */
	public function getPaymentMethodDescription()
	{
		return $this->paymentMethodDescription;
	}

	/**
	 * @param string $paymentMethodId
	 */
	public function setPaymentMethodId($paymentMethodId)
	{
		$this->paymentMethodId = $paymentMethodId;
	}

	/**
	 * @return string
	 */
	public function getPaymentMethodId()
	{
		return $this->paymentMethodId;
	}

	/**
	 * @param string $paymentMethodName
	 */
	public function setPaymentMethodName($paymentMethodName)
	{
		$this->paymentMethodName = $paymentMethodName;
	}

	/**
	 * @return string
	 */
	public function getPaymentMethodName()
	{
		return $this->paymentMethodName;
	}

	/**
	 * Set payment attempts count
	 *
	 * @param int $count
	 */
	public function setPaymentAttemptsCount($count)
	{
		$this->paymentAttemptsCount = $count;
	}

	/**
	 * Get payment attempts count
	 *
	 * @return int
	 */
	public function getPaymentAttemptsCount()
	{
		return $this->paymentAttemptsCount;
	}

	/**
	 * Set order source
	 *
	 * @param string $order_source
	 */
	public function setOrderSource($order_source)
	{
		$this->order_source = $order_source;
	}

	/**
	 * Get order source
	 *
	 * @return string
	 */
	public function getOrderSource()
	{
		return $this->order_source;
	}

	/**
	 * Set off-site campaign id
	 *
	 * @param int $offsite_campaign_id
	 */
	public function setOffsiteCampaignId($offsite_campaign_id)
	{
		$this->offsite_campaign_id = $offsite_campaign_id;
	}

	/**
	 * Get offsite campaign id
	 *
	 * @return int
	 */
	public function getOffsiteCampaignId()
	{
		return $this->offsite_campaign_id;
	}

	/**
	 * Unlink user from order
	 */
	public function unlinkUser()
	{
		$this->db()->reset();
		$this->db()->assign('uid', 0);
		$this->db()->update(DB_PREFIX.'orders', 'WHERE oid='.$this->getId());

		$this->resetValues();

		$this->persist();
	}

	/**
	 * Set messages from current language
	 *
	 * @param array $msg
	 */
	public function setLanguageMessages(&$msg)
	{
		$this->msg = $msg;
	}

	/**
	 * Sets user level from current active user or users cookie
	 */
	public function setUserLevel($userLevel)
	{
		$this->_userLevel = $userLevel;
	}

	/**
	 * Assign custom fields class
	 *
	 * @param bool $customFields
	 *
	 * @return mixed
	 */
	public function customFields($customFields = false)
	{
		if ($customFields) $this->_customFields = $customFields;
		return $this->_customFields = $this->_customFields != null ? $this->_customFields : new CustomFields($this->_db);
	}
	
	/**
	 * Sets order cookie
	 *
	 * @param string $securityId
	 */
	public static function setCookie($securityId)
	{
		global $settings;

		setcookie(
			'_pcod', 
			$securityId, 
			time() + 60*60*24*30*6, 
			'/',
			isSslMode() ? $settings['GlobalHttpsCookieDomain'] : $settings['GlobalHttpCookieDomain']
		);
	}
				
	/**
	 * Set order error message
	 *
	 * @param string $message
	 */
	public function setError($message)
	{
		$this->errors[] = $message;
		$this->is_error = true;
	}
	
	/**
	 * Database class instance
	 *
	 * @return DB
	 */
	public function db()
	{
		return $this->_db;
	}
	
	/**
	 * Returns class instance
	 *
	 * @return USER
	 */
	public function user()
	{
		return $this->_user;
	}
	
	/**
	 * Set abandon status
	 */
	public function setAbandonStatus()
	{
		$this->status = ORDER::STATUS_ABANDON;
		$this->db()->reset();
		$this->db()->assignStr('status', $this->status);
		$this->db()->update(DB_PREFIX.'orders', 'WHERE oid='.$this->getId());
	}

	/**
	 * Resets promo code
	 *
	 * @param bool $justPromoDiscountAmount
	 */
	public function resetPromo($justPromoDiscountAmount = true)
	{
		Calculator_PromoDiscount::getInstance($this->_settings)->reset($this, $justPromoDiscountAmount);
	}

	protected $promoCodeRepository;
	protected $productsRepository;
	protected $shipmentRepository;
	protected $orderRepository;
	protected $settingsRepository;
	protected $giftCertificateRepository;

	/**
	 * @return DataAccess_PromoCodeRepository
	 */
	protected function getPromoCodeRepository()
	{
		if (is_null($this->promoCodeRepository))
		{
			$this->promoCodeRepository = new DataAccess_PromoCodeRepository($this->db(), $this->_settings);
		}

		return $this->promoCodeRepository;
	}

	/**
	 * @param DataAccess_PromoCodeRepositoryInterface $promoCodeRepository
	 */
	public function setPromoCodeRepository(DataAccess_PromoCodeRepositoryInterface $promoCodeRepository)
	{
		$this->promoCodeRepository = $promoCodeRepository;
	}

	/**
	 * @return DataAccess_GiftCertificateRepository
	 */
	protected function getGiftCertificateRepository()
	{
		if ($this->giftCertificateRepository === null)
		{
			$this->giftCertificateRepository = new DataAccess_GiftCertificateRepository($this->db(), $this->getSettingsRepository());
		}

		return $this->giftCertificateRepository;
	}

	/**
	 * @param DataAccess_GiftCertificateRepository $repository
	 */
	public function setGiftCertificateRepository(DataAccess_GiftCertificateRepository $repository)
	{
		$this->giftCertificateRepository = $repository;
	}

	/**
	 * @return DataAccess_ProductsRepository
	 */
	protected function getProductsRepository()
	{
		if ($this->productsRepository == null)
		{
			$this->productsRepository = new DataAccess_ProductsRepository($this->db());
		}

		return $this->productsRepository;
	}

	/**
	 * @return DataAccess_OrderShipmentRepository
	 */
	protected function getShipmentRepository()
	{
		if ($this->shipmentRepository == null)
		{
			$this->shipmentRepository = new DataAccess_OrderShipmentRepository($this->db(), $this->getSettingsRepository());
		}

		return $this->shipmentRepository;
	}

	/**
	 * @return mixed
	 */
	public function getShipments()
	{
		return $this->getShipmentRepository()->getShipmentsByOrderId($this->getId());
	}

	/**
	 * @param DataAccess_ProductsRepositoryInterface $productsRepository
	 */
	public function setProductsRepository(DataAccess_ProductsRepositoryInterface $productsRepository)
	{
		$this->productsRepository = $productsRepository;
	}

	/**
	 * @return DataAccess_SettingsRepository
	 */
	public function getSettingsRepository()
	{
		if ($this->settingsRepository === null)
		{
			$this->settingsRepository = new DataAccess_SettingsRepository($this->db(), $this->_settings);
		}

		return $this->settingsRepository;
	}

	/**
	 * @param DataAccess_SettingsRepository $settingsRepository
	 */
	public function setSettingsRepository(DataAccess_SettingsRepository $settingsRepository)
	{
		$this->settingsRepository = $settingsRepository;
	}

	/**
	 * @return DataAccess_OrderRepository
	 */
	protected function getOrderRepository()
	{
		if (is_null($this->orderRepository))
		{
			$this->orderRepository = DataAccess_OrderRepository::getInstance($this->db());
		}

		return $this->orderRepository;
	}

	/**
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 */
	public function setOrderRepository(DataAccess_OrderRepositoryInterface $orderRepository)
	{
		$this->orderRepository = $orderRepository;
	}

	/**
	 * Checks is promo code correct.
	 *
	 * @param string $promoCode
	 *
	 * @return boolean
	 */
	public function checkPromoCode($promoCode)
	{
		if (strtolower($this->_settings['DiscountsPromo']) == 'yes' && trim($promoCode) != '')
		{
			$promoDiscountCalculator = Calculator_PromoDiscount::getInstance($this->_settings, $this->getOrderRepository(), $this->getPromoCodeRepository());

			$promoData = $promoDiscountCalculator->checkPromoDiscount($this, $promoCode);
			if (!is_null($promoData))
			{
				// Save promo data and exit
				$this->setPromoType($promoData['promo_type']);
				$this->setPromoDiscountType($promoData['discount_type']);
				$this->setPromoDiscountValue($promoData['discount']);
				$this->setPromoCampaignId($promoData['pid']);
				$this->persist();

				return true;
			}
		}

		// Clear promo code data
		$this->resetPromo(false);
		$this->persist();

		return false;
	}

	/**
	 * @param $productId
	 * @param $attributesData
	 * @param $customData
	 * @return Model_LineItem|null
	 */
	public function findExistingLineItemForProduct($productId, $attributesData, $customData)
	{
		$options = $attributesData['options'];

		/** @var Model_LineItem $lineItem */
		foreach ($this->lineItems as $lineItem)
		{
			if ($lineItem->getProductId() == $productId && $lineItem->getOptionsClean() == $options)
			{
				$event = new Events_OrderEvent(Events_OrderEvent::ON_FIND_EXISTING_LINE_ITEM);
				$event->setOrder($this);
				$event->setData('item', $lineItem);
				$event->setData('attributesData', $attributesData);
				$event->setData('customData', $customData);
				Events_EventHandler::handle($event);

				$lineItem = $event->getData('item');

				if ($lineItem) return $lineItem;
			}
		}

		return null;
	}

	/**
	 * Add a product to a cart
	 *
	 * @param Model_Product $product
	 * @param $quantity
	 * @param $attributes
	 * @param array $customData
	 * @return bool
	 */
	public function _addItem(Model_Product $product, $quantity, $attributes, array $customData = array())
	{
		if (!$product)
		{
			// TODO: Check cart for the productId, and if exists then it should be deleted

			$this->setError($this->msg['cart']['error_wrong_product_id']);
			return false;
		}

		// get attributes data
		$productAttributes = $this->getProductsRepository()->getProductsAttributes($product->getPid());
		$attributesData = $product->parseAttributes($attributes, $productAttributes);

		// get repositories
		$productsRepository = $this->getProductsRepository();

		/**
		 * Process on find existing item event
		 */

		/* @var Model_LineItem $lineItem */
		$lineItem = $this->findExistingLineItemForProduct($product->getProductId(), $attributesData, $customData);

		if ($lineItem)
		{
			$isNew = false;
			$quantity += $lineItem->getAdminQuantity();
			$product->calculateProductPricing($this->getUserLevel(), $attributesData, $this->_settings);
			$lineItem->setProductFreeShipping($product->getFreeShipping());
		}
		else
		{
			$isNew = true;
			$product->calculateProductPricing($this->getUserLevel(), $attributesData, $this->_settings);

			$lineItem = Model_LineItem::createNewLineItem($this->getId(), $product, $quantity, $attributesData, 'No');

			$inventoryList = $attributesData['inventoryList'];

			if ($inventoryList != '')
			{
				if (($inventory = $productsRepository->getProductsInventory($product->getPid(), $inventoryList)) != false)
				{
					$lineItem->setInventoryId($inventory['pi_id']);
					$lineItem->setProductSubId($inventory['product_subid']);
					if (trim($inventory['product_sku']) != '') $lineItem->setProductSku($inventory['product_sku']);
				}
				else if (in_array($product->getInventoryControl(), array('AttrRuleInc', 'AttrRuleExc')))
				{
					$this->setError($this->msg['cart']['error_out_of_stock']);
					return false;
				}
			}

			/**
			 * Assign shipment
			 */
			$this->getShipmentRepository()->assignShipment($lineItem);
		}

		if ($this->doLineItemUpdate($lineItem, $product, $quantity, $attributesData))
		{
			if ($isNew)
			{
				$this->items[] = $lineItem->toArray();
				$this->lineItems[$lineItem->getId()] = $lineItem;
			}

			$this->resetItemProductPricing();

			/**
			 * Process on add item event
			 */
			$event = new Events_OrderEvent(Events_OrderEvent::ON_ADD_ITEM);
			$event->setOrder($this);
			$event->setData('item', $lineItem);
			$event->setData('attributesData', $attributesData);
			$event->setData('customData', $customData);
			Events_EventHandler::handle($event);

			$this->checkShippingRequired();
			$this->recalcSubtotals();

			return true;
		}

		return false;
	}

	/**
	 * @param $productId
	 * @param $quantity
	 * @param $attributes
	 * @param array $customData
	 * @return bool
	 */
	public function addItem($productId, $quantity, $attributes, array $customData = array())
	{
		if (!$this->canEdit())
		{
			// TODO: Ensure that order can be modified
		}

		$productsRepository = $this->getProductsRepository();

		/* @var Model_Product $product */
		$product = $productsRepository->getProductByProductId($productId);

		if ($product !== null && $product)
		{
			return $this->_addItem($product, $quantity, $attributes, $customData);
		}
		else
		{
			$this->setError($this->msg['cart']['error_wrong_product_id']);
			return false;
		}
	}

	/**
	 *
	 */
	protected function resetItemProductPricing()
	{
		$lineItemIds = array_keys($this->lineItems);
		$products = $this->getOrderRepository()->getOrderItemsProducts($this->getId(), $lineItemIds);

		/* @var Model_LineItem $item */
		foreach ($this->lineItems as $item)
		{
			/* @var Model_Product $product */
			$product = isset($products[$item->getPid()]) ? $products[$item->getPid()] : null;

			if ($product)
			{
				$attributes = $item->parseOptions();

				$productAttributes = $this->getProductsRepository()->getProductsAttributes($product->getPid());
				$attributesData = $product->parseAttributes($attributes, $productAttributes, false);

				$product->calculateProductPricing($this->getUserLevel(), $attributesData, $this->_settings);

				//TODO: check why we need settings here. Looks like there only one param now
				$item->setDataFromProduct($product); //, $this->_settings);
			}
		}
	}

	protected function canEdit()
	{
		return $this->getStatus() == ORDER::STATUS_ABANDON || $this->getStatus() == ORDER::STATUS_ABANDON || ($this->getStatus() == ORDER::STATUS_PROCESS && $this->getPaymentStatus() == ORDER::PAYMENT_STATUS_PENDING);
	}
	/**
	 * Bulk updating of items in the cart
	 *
	 * @param array $quantities
	 *
	 * @return bool
	 */
	public function updateItems($quantities)
	{
		if (!$this->canEdit())
		{
			// TODO: Ensure that order can be modified
		}

		Tax_ProviderFactory::getTaxProvider()->initTax($this);

		$lineItemIds = array_map('intval', array_keys($quantities));

		if (count($lineItemIds) == 0) return false;

		$orderRepository = $this->getOrderRepository();

		$products = $orderRepository->getOrderItemsProducts($this->getId(), $lineItemIds);

		if (is_array($this->lineItems) && count($this->lineItems) > 0)
		{
			$lineItemIdsForDeletion = array();

			/* @var $item Model_LineItem */
			foreach ($this->lineItems as $item)
			{
				$needsDeleted = false;

				/* @var $product Model_Product */
				$product = isset($products[$item->getPid()]) ? $products[$item->getPid()] : null;

				if (!$product)
				{
					$needsDeleted = true;
				}
				else
				{
					if ($product->getProductId() == 'gift_certificate' || $item->getIsGift() == 'Yes') continue;

					$attributes = $item->parseOptions();

					$productAttributes = $this->getProductsRepository()->getProductsAttributes($product->getPid());
					$attributesData = $product->parseAttributes($attributes, $productAttributes, false);

					$this->doLineItemUpdate($item, $product, $quantities[$item->getId()], $attributesData);
				}

				if ($needsDeleted)
				{
					// item is not available
					$lineItemIdsForDeletion[] = $item->getId();
				}
			}

			$lineItemsForDeletion = array();
			foreach ($lineItemIdsForDeletion as $lineItemId)
			{
				$lineItemsForDeletion = $this->lineItems[$lineItemId];
				unset($this->lineItems[$lineItemId]);
			}

			$this->items = array();
			/* @var Model_LineItem $lineItem */
			foreach ($this->lineItems as $lineItem)
			{
				$this->items[] = $lineItem->toArray();
			}
			if (count($lineItemsForDeletion) > 0)
			{
				$event = new Events_OrderEvent(Events_OrderEvent::ON_REMOVE_ITEM);
				$event->setOrder($this);
				$event->setData('removedItems', $lineItemsForDeletion);
				Events_EventHandler::handle($event);
			}

			$orderRepository->deleteLineItems($this->getId(), $lineItemIdsForDeletion);
		}

		if (count($this->errors) == 0)
		{
			$event = new Events_OrderEvent(Events_OrderEvent::ON_UPDATE_ITEMS);
			$event->setOrder($this);
			Events_EventHandler::handle($event);
		}

		/**
		 * Clean up shipments
		 */
		$this->getShipmentRepository()->cleanupShipments($this->getId());

		return count($this->errors) > 0 ? false : true;
	}

	/**
	 * @param $product
	 * @param $quantity
	 * @return bool
	 */
	protected function lineItemCheckMinMaxOrderQuantity(Model_Product $product, $quantity)
	{
		// check min / max order
		if ($product->getMinOrder() > 0 && $quantity < $product->getMinOrder())
		{
			$this->setError($this->msg['javascript']['number_of_items_exceeded_min']);
			return false;
		}
		else if ($product->getMaxOrder() > 0 && $quantity > $product->getMaxOrder())
		{
			$this->setError($this->msg['javascript']['number_of_items_exceeded_max']);
			return false;
		}

		return true;
	}

	/**
	 * Check inventory
	 *
	 * @param $lineItem
	 * @param $product
	 * @param $quantity
	 * @param $attributesData
	 *
	 * @return bool
	 */
	protected function lineItemCheckInventory(Model_LineItem $lineItem, Model_Product $product, $quantity, &$attributesData)
	{
		$stock = INF;

		$quantity -= $lineItem->getQuantityFromStock();

		if ($quantity < 0)
		{
			$quantity = 0;
		}

		// TODO: Make this into a select statement (couple of select statements) that do the inventory check in one call,
		// having the checks in php lead to a 'race' condition where it might have been in stock, but by the time it
		// renders it will have gone out of stock
		// check current inventory
		switch ($product->getInventoryControl())
		{
			case 'Yes' :  // on product level
			{
				$stock = $product->getStock();

//				$inventoryId = intval($lineItem->getInventoryId());
//				if ($inventoryId > 0)
//				{
//					if ($attributeLevelStock = $this->getProductsRepository()->getInventoryStock($inventoryId))
//					{
//						$stock = $attributeLevelStock['stock'];
//					}
//				}

				break;
			}

			case 'AttrRuleInc' : // on attribute level
			case 'AttrRuleExc' :
			{
				$inventoryId = intval($lineItem->getInventoryId());
				if ($inventoryId > 0)
				{
					if ($attributeLevelStock = $this->getProductsRepository()->getInventoryStock($inventoryId))
					{
						$stock = $attributeLevelStock['stock'];
					}
				}
				break;
			}
		}

		if ($quantity > $stock && $this->_settings['AllowOutOfInventoryOrders'] != 'Yes')
		{
			$this->setError(sprintf($this->msg['cart']['error_stock_limited'], $lineItem->getTitle()));
			return false;
		}

		return true;
	}

	/**
	 * Add/update line item
	 *
	 * @param Model_LineItem $lineItem
	 * @param Model_Product $product
	 * @param $quantity
	 * @param $attributesData
	 *
	 * @return bool
	 */
	protected function doLineItemUpdate(Model_LineItem $lineItem, Model_Product $product, $quantity, &$attributesData)
	{
		if (isset($attributesData['weight']) && $attributesData['weight'] > 0)
		{
			$lineItem->setWeight($attributesData['weight']);
		}

		$product->calculateProductPricing($this->getUserLevel(), $attributesData, $this->_settings);

		// check min/max order
		if (!$this->lineItemCheckMinMaxOrderQuantity($product, $quantity)) return false;

		// check inventory
		if (!$this->lineItemCheckInventory($lineItem, $product, $quantity, $attributesData)) return false;

		$event = new Events_OrderEvent(Events_OrderEvent::ON_PRE_UPDATE_LINE_ITEM);
		$event->setOrder($this);
		$event->setData('item', $lineItem);
		Events_EventHandler::handle($event);

		if (count($this->errors) > 0) return false;

		$lineItem->setFinalQuantity($quantity);

		$lineItem->setFinalPrice($product->getFinalPrice());
		$lineItem->setPriceBeforeQuantityDiscount($product->getFinalPrice());
		$lineItem->setPriceWithTax($product->getPriceWithTax());

		$lineItem->setTaxDescription($product->getTaxDescription());
		$lineItem->setTaxRate($product->getTaxRate());
		$lineItem->setIsTaxable($product->getIsTaxable());

		$orderRepository = $this->getOrderRepository();
		$orderRepository->persistLineItem($this, $lineItem, $lineItem->getId() > 0 ? false : true);

		return true;
	}

	/**
	 * Remove item from a cart
	 *
	 * @param int $lineItemId
	 *
	 * @return boolean
	 */
	public function removeItem($lineItemId)
	{
		$foundAt = -1;
		if (is_array($this->items))
		{
			foreach ($this->items as $pos => $item)
			{
				if ($item['ocid'] == $lineItemId)
				{
					$foundAt = $pos;
					break;
				}
			}
		}

		if ($foundAt >= 0)
		{
			$item = $this->lineItems[$lineItemId];

			unset($this->items[$foundAt]);
			unset($this->lineItems[$lineItemId]);

			$lineItemIds = array(intval($lineItemId));
			$this->getOrderRepository()->deleteLineItems($this->getId(), $lineItemIds);

			$this->resetItemProductPricing();

			$event = new Events_OrderEvent(Events_OrderEvent::ON_REMOVE_ITEM);
			$event->setOrder($this);
			$event->setData('item', $item);

			Events_EventHandler::handle($event);

			/**
			 * Clean up shipments
			 */
			$this->getShipmentRepository()->cleanupShipments($this->getId());

			return true;
		}
		else
		{
			$this->setError('Incorrect line item id');
			return false;
		}
	}

	/**
	 * Remove all items from a cart
	 */
	public function clearItems()
	{
		$oldItems = $this->lineItems;

		$this->getOrderRepository()->deleteAllLineItems($this->getId());

		$this->lineItems = array();
		$this->items = array();

		if (count($oldItems) > 0)
		{
			$event = new Events_OrderEvent(Events_OrderEvent::ON_REMOVE_ITEM);
			$event->setOrder($this);
			$event->setData('removedItems', $oldItems);
			Events_EventHandler::handle($event);
		}
	}

	/**
	 * Check payment failed attempts
	 *
	 * @param int $failureThreshold
	 *
	 * @return bool
	 */
	public function checksFailedPaymentAttempts($failureThreshold = INF)
	{
		if ($this->getPaymentAttemptsCount() >= $failureThreshold)
		{
			$oldOrderStatus = $this->getStatus();
			$oldPaymentStatus = $this->getPaymentStatus();

			$this->setStatus(ORDER::STATUS_FAILED);
			$this->setPaymentStatus(ORDER::PAYMENT_STATUS_DECLINED);
			$this->setStatusChangeDate(new DateTime());
			$this->persist();

			/**
			 * Handle on status change event
			 */
			$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);

			$event
				->setOrder($this)
				->setData('oldOrderStatus', $oldOrderStatus)
				->setData('oldPaymentStatus', $oldPaymentStatus)
				->setData('newOrderStatus', $this->getStatus())
				->setData('newPaymentStatus', $this->getPaymentStatus());

			Events_EventHandler::handle($event);
		}
		else
		{
			$this->setPaymentAttemptsCount($this->getPaymentAttemptsCount() + 1);
			$this->persist();
		}
	}

	/**
	 * Sets order number
	 */
	public function setOrderNum()
	{
		if ($this->getOrderNumber() < 1)
		{
			$this->setOrderNumber($this->getOrderRepository()->getNextOrderNumber($this->getId(), $this->_settings['MinOrderNumber']));
		}
	}

	/**
	 * @param $form
	 * @param null $isMobile
	 * @param null $source
	 * @param null $oscCampaignId
	 * @return int
	 */
	public function failOrder($form, $isMobile = null, $source = null, $oscCampaignId = null)
	{
		global $_SESSION;
		unset($_SESSION['payment_session_delay_timeout']);
		unset($_SESSION['order_promo_code']);
		unset($_SESSION['order_gift_certificate']);
		unset($_SESSION['facebook_new_account']);

		$this->setOrderNum();

		/**
		 * Set the payment_method_description on the order
		 */

		$this->setPaymentMethodDescription(array_key_exists('cc_type', $form) && isset($form['cc_type']) && trim($form['cc_type']) != '' ? $form['cc_type'] : $this->paymentMethodName);
		$this->setUserIpAddress(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'unknown');
		$this->setStatus(ORDER::STATUS_FAILED);
		$this->setPaymentStatus(ORDER::PAYMENT_STATUS_DECLINED);
		$this->setOrderPlacedDate(new DateTime());
		$this->setStatusChangeDate(new DateTime());

		if (!is_null($source) && $source) $this->setOrderSource($source);
		if (!is_null($oscCampaignId) && $oscCampaignId) $this->setOffsiteCampaignId($oscCampaignId);

		$this->persist();

		$_SESSION['LastOrderID'] = $this->getId();

		/**
		 * Handle on status change event
		 */
		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);

		$event
			->setOrder($this)
			->setData('oldOrderStatus', ORDER::STATUS_ABANDON)
			->setData('oldPaymentStatus', ORDER::PAYMENT_STATUS_PENDING)
			->setData('newOrderStatus', ORDER::STATUS_FAILED)
			->setData('newPaymentStatus', ORDER::PAYMENT_STATUS_DECLINED);

		Events_EventHandler::handle($event);

		return $this->order_num;
	}

	/**
	 * Closes order on user side
	 *
	 * @param string $order_status
	 * @param mixed $result payment processing result
	 * @param array $form
	 * @param mixed $customFields
	 * @param null $source
	 * @param null $osc_campaign_id
	 * @param bool $send_notification
	 *
	 * @return int
	 */
	public function closeOrder($order_status, $result, $form, $customFields, $source = null, $osc_campaign_id = null, $send_notification = true)
	{
		global $_SESSION;
		unset($_SESSION['payment_session_delay_timeout']);
		unset($_SESSION['order_promo_code']);
		unset($_SESSION['order_gift_certificate']);
		unset($_SESSION['facebook_new_account']);

		/**
		 * Set order number (if unset yet)
		 */
		$this->setOrderNum();

		$this->getShippingAddress();

		/**
		 * Set payment status based on payment gateways result
		 */
		$payment_status =
			is_bool($result) ?
				($result ? ORDER::PAYMENT_STATUS_RECEIVED : ORDER::PAYMENT_STATUS_PENDING) :
				(in_array($result, array(ORDER::PAYMENT_STATUS_PENDING, ORDER::PAYMENT_STATUS_RECEIVED)) ? $result : ORDER::PAYMENT_STATUS_RECEIVED);

		if ($this->totalAmount == 0) $payment_status = ORDER::PAYMENT_STATUS_RECEIVED;

		// check is gift certificate used to place (fund) an order
		$giftCertificateAmountUsed = 0;
		if ($this->totalAmount > 0)
		{
			$giftCertificateRepository = $this->getGiftCertificateRepository();
			$giftCertificateOrderData = $giftCertificateRepository->getOrderGiftCert($this->getId());
			
			if ($giftCertificateOrderData['gift_cert_valid'] == '1')
			{
				$giftCertificateData = $giftCertificateRepository->getGiftCertificate(
					$giftCertificateOrderData['gift_cert_first_name'],
					$giftCertificateOrderData['gift_cert_last_name'],
					$giftCertificateOrderData['gift_cert_voucher']
				);

				if ($giftCertificateData !== false)
				{
					$giftCertificateAmountUsed = $this->totalAmount > $giftCertificateData['balance'] ? $giftCertificateData['balance'] : $this->totalAmount;
					$giftCertificateRepository->deductAmount($giftCertificateData['id'], $giftCertificateAmountUsed);

					//when everything was paid by Gift Certificate, update payment method
					if ($giftCertificateAmountUsed >= $this->getTotalAmount())
					{
						$this->db()->reset();
						$this->db()->assignStr('payment_method_name', 'Gift Certificate');
						$this->db()->assignStr('payment_method_description', 'Gift Certificate');
						$this->db()->assignStr('payment_gateway_id', '');
						$this->db()->update(DB_PREFIX.'orders', 'WHERE oid='.intval($this->getId()));
						view()->assign('payment_method_name', 'Gift Certificate');
						$payment_status = 'Received';
					}
				}
			}
		}

		view()->assign('gift_cert_amount', $giftCertificateAmountUsed);

		/**
		 * Set the payment_method_description on the order
		 */
		if (array_key_exists('cc_type', $form) && isset($form['cc_type']) && trim($form['cc_type']) != '')
		{
			$this->paymentMethodDescription = $form['cc_type'];
		}
		else
		{
			$this->paymentMethodDescription = $this->paymentMethodName;
		}

		/**
		 * Store the buyers IP address upon completing order
		 */
		$ipAddress = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : 'unknown';
		$this->db()->reset();
		$this->db()->assignStr('ipaddress', $ipAddress);
		$this->db()->assignStr('payment_method_description', $this->paymentMethodDescription);
		$this->db()->update(DB_PREFIX.'orders', 'WHERE oid='.$this->getId());

		/**
		 * Set order dates / status
		 */

		// TODO: replace with order object / data repository
		$this->db()->reset();
		$this->db()->assignStr('status', $order_status);
		$this->db()->assignStr('payment_status', $payment_status);
		$this->db()->assign('placed_date', 'NOW()');
		$this->db()->assign('status_date', 'NOW()');
		$this->db()->assignStr('removed', 'No');

		if (!is_null($source) && $source) $this->db()->assignStr('order_source', $source);
		if (!is_null($osc_campaign_id) && $osc_campaign_id) $this->db()->assignStr('offsite_campaign_id', $osc_campaign_id);

		$this->db()->update(DB_PREFIX.'orders', 'WHERE oid='.$this->getId());
		$_SESSION['LastOrderID'] = $this->getId();

		/**
		 * DOBA
		 */
		if ($this->_settings['doba_autosend_orders'] == 'Yes')
		{
			$doba_items = array();
			foreach ($this->lineItems as $item)
			{
				if ($item->getIsDoba() === true || $item->getIsDoba() == 'Yes')
				{
					$doba_items[] = array('item_id'=>$item->getProductId(), 'quantity'=>$item->getFinalQuantity());
				}
			}
			if (count($doba_items) > 0)
			{
				$dobaOrder = new Doba_OrderApi($this->_settings, $this->_db);

				$doba_order_response = $dobaOrder->createOrder($this->shippingAddress, $doba_items, $this->order_num);

				if ($doba_order_response)
				{
					$this->db()->query('UPDATE '.DB_PREFIX.'orders SET doba_order_sent=1, doba_order_id="'.$this->db()->escape($doba_order_response['response']['order_id']).'" WHERE oid='.$this->getId());
				}
			}
		}

		/**
		 * Send notifications
		 */
		if ($send_notification)
		{
			Notifications::emailOrderReceived($this->_db, $this, $this->_user, $customFields, $payment_status);
		}

		if ($payment_status == ORDER::PAYMENT_STATUS_RECEIVED)
		{
			$fulfillment = Model_Fulfillment::createFulfillmentFromOrder($this, false);
			if ($fulfillment)
			{
				$fulfillment->setStatus(Model_Fulfillment::STATUS_COMPLETED);

				foreach ($fulfillment->getItems() as $lineItemId => $fulfillmentItem)
				{
					/** @var Model_FulfillmentItem $fulfillmentItem */

					if (isset($this->lineItems[$lineItemId]))
					{
						/** @var Model_LineItem $lineItem */
						$lineItem = $this->lineItems[$lineItemId];

						$lineItem->setFulfilledQuantity($lineItem->getFinalQuantity());
						$lineItem->setFulfillmentStatus(ORDER::FULFILLMENT_STATUS_COMPLETED);

						$fulfillmentItem->setQuantity($lineItem->getFinalQuantity());
					}
				}

				Notifications::emailOrderShipped($this->_db, $this, $fulfillment);

				$repository = new DataAccess_FulfillmentRepository($this->_db, $this->_settings);
				$repository->save($fulfillment);

				$event = new Events_FulfillmentEvent(Events_FulfillmentEvent::ON_COMPLETED);
				$event->setOrder($this);
				$event->setFulfillment($fulfillment);
				Events_EventHandler::handle($event);

				$fulfilledStatus = ORDER::FULFILLMENT_STATUS_COMPLETED;
				foreach ($this->lineItems as $lineItem)
				{
					if ($lineItem->getFulfillmentStatus() != ORDER::FULFILLMENT_STATUS_COMPLETED)
					{
						$fulfilledStatus = ORDER::FULFILLMENT_STATUS_PARTIAL;
						break;
					}
				}
				$this->setFulfillmentStatus($fulfilledStatus);

				$this->persist(true);

				if ($fulfilledStatus == ORDER::FULFILLMENT_STATUS_COMPLETED)
				{
					$order_status = ORDER::STATUS_COMPLETED;
				}
			}
		}

		/**
		 * Handle on status change event
		 */
		$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);

		$event
			->setOrder($this)
			->setData('oldOrderStatus', ORDER::STATUS_ABANDON)
			->setData('oldPaymentStatus', ORDER::PAYMENT_STATUS_PENDING)
			->setData('newOrderStatus', $order_status)
			->setData('newPaymentStatus', $payment_status);

		Events_EventHandler::handle($event);

		return $this->order_num;
	}

	/**
	 * Checks if shipping is virtual
	 *
	 * @return boolean
	 */
	public function checkShippingRequired()
	{
		if (!is_array($this->lineItems))
		{
			$this->getOrderItems();
		}

		$hasTangibleProducts = false;
		$hasDigitalDownloads = false;

		/* @var $item Model_LineItem */
		foreach ($this->lineItems as $item)
		{
			if ($item->getProductType() == Model_Product::TANGIBLE && $item->getProductId() != 'gift_certificate')
			{
				$hasTangibleProducts = true;
				break;
			}

			if ($item->getProductType() == Model_Product::DIGITAL)
			{
				$hasDigitalDownloads = true;
			}
		}

		$this->setShippingRequired($hasTangibleProducts);

		if (!$this->getShippingRequired())
		{
//			$this->setShippingCarrierID('custom');
//			$this->setShippingMethodId($hasDigitalDownloads ? 'digital' : 'virtual');
//			$this->setShippingCMName($hasDigitalDownloads ? 'Digital download' : 'N/A');
			$this->setShippingTaxable(0);
			$this->setShippingTaxRate(0);
			$this->setShippingTaxAmount(0);
			$this->setShippingTaxDescription('');
		}

		return $this->getShippingRequired();
	}


	/**
	 * @param $shipmentsMethods
	 */
	public function updateShipmentsMethods($shipmentsMethods)
	{
		$db = $this->db();

		// Reset shipments
		$db->query('UPDATE '.DB_PREFIX.'orders_shipments SET shipping_ssid="", shipping_carrier_id="", shipping_method_id="", shipping_cm_name="", shipping_amount=0 WHERE oid='.intval($this->getId()));
		if ($shipmentsMethods && is_array($shipmentsMethods) && count($shipmentsMethods) > 0)
		{
			/**
			 * Select shipping methods
			 */
			$shipmentsMethodsSSIDs = array();

			foreach ($shipmentsMethods as $shipmentId => $value)
			{
				$shipmentsMethodsSSIDs[$shipmentId] = intval(str_replace('a', '', $value));
			}

			$shippingMethodsData = $db->selectAll('SELECT * FROM ' . DB_PREFIX . 'shipping_selected  WHERE ssid IN (' . implode(',', $shipmentsMethodsSSIDs) . ')');

			$shippingMethods = array();

			foreach ($shippingMethodsData as $shippingMethodData)
			{
				$shippingMethods[$shippingMethodData['ssid']] = $shippingMethodData;
			}

			/**
			 * Store shipping methods data
			 */
			foreach ($shipmentsMethods as $shipmentId => $methodId)
			{
				$ignoreFreeShipping = strstr($methodId, 'a') !== false;
				$methodId = intval(str_replace('a', '', $methodId));

				if ($methodId == 0 && $this->_settings['ShippingWithoutMethod'] == 'YES')
				{
					$shippingMethodData = array(
						'ssid' => '0', 'carrier_id' => 'custom', 'method_id' => 'future_charge',
						'carrier_name' => 'Undefined', 'method_name' => ''
					);
				}
				else
				{
					$shippingMethodData = isset($shippingMethods[intval($methodId)]) ? $shippingMethods[intval($methodId)] : false;

					if (!$shippingMethodData)
					{
						$shippingMethodData = array(
							'ssid' => '0', 'carrier_id' => 'custom', 'method_id' => 'disabled',
							'carrier_name' => 'Shipping is disabled', 'method_name' => ''
						);
					}
				}

				$db->reset();
				$db->assignStr('shipping_ssid', $shippingMethodData['ssid']);
				$db->assignStr('shipping_carrier_id', $shippingMethodData['carrier_id']);
				$db->assignStr('shipping_method_id', $shippingMethodData['method_id']);
				$db->assignStr('shipping_cm_name', $shippingMethodData['carrier_name'].($shippingMethodData['carrier_id'] != 'custom' ? (' - '.$shippingMethodData['method_name']) : ''));
				$db->assignStr('shipping_ignore_free', $ignoreFreeShipping ? '1' : '0');
				$db->assignStr('shipping_amount', 0);
				$db->update(DB_PREFIX . 'orders_shipments', 'WHERE id=' . intval($shipmentId) . ' AND oid=' . intval($this->getId()));
			}
		}

		$this->setShippingAmount(0);
		$this->setShippingTaxAmount(0);
		$this->setShippingTaxRate(0);
	}

	/**
	 * Update shipping address information
	 *
	 * @param $address
	 * @param bool $customFieldsData
	 * @param array $shipmentsMethods
	 */
	public function updateShippingAddress($address, $customFieldsData = false, array $shipmentsMethods = array())
	{
		/**
		 * Update shipments methods
		 */
		$this->updateShipmentsMethods($shipmentsMethods);

		$address['address_type'] = isset($address['address_type']) ? $address['address_type'] : 'Residential';

		/**
		 * Get country and state date to fill in gaps
		 */
		$countryId = intval(isset($address['country_id']) ? $address['country_id'] : $address['country']);
		$stateId = intval(isset($address['state_id']) ? $address['state_id'] : (isset($address['state']) ? $address['state'] : 0));

		$countryStateData = $this->db()->selectOne('
			SELECT
				c.coid AS country_id,
				c.name AS country,
				c.name AS country_name,
				c.iso_a2 AS country_iso_a2,
				c.iso_a3 AS country_iso_a3,
				c.iso_number AS country_iso_number,
				s.stid AS state_id,
				s.short_name AS state_abbr,
				s.name AS state
			FROM '.DB_PREFIX.'countries AS c
			LEFT JOIN '.DB_PREFIX.'states AS s ON c.coid = s.coid AND s.stid='.$stateId.'
			WHERE c.coid='.$countryId
		);

		if ($countryStateData)
		{
			$address = array_merge($address, $countryStateData);
		}

		if ($address['state_id'] == '' || $address['state'] == '')
		{
			$address['state_id'] = 0;
			$address['state'] = isset($address['province']) ? $address['province'] : '';
			$address['state_abbr'] = isset($address['province']) ? $address['province'] : '';
		}
		else
		{
			$address['province'] = $address['state'];
		}

		$this->shippingAddress = $address;

		if (is_array($customFieldsData))
		{
			$this->customFields()->storeCustomFields('shipping', $customFieldsData, $this->user()->getId(), $this->getId(), 0);
		}
	}

	/**
	 * Update payment method
	 *
	 * @param $payment_method_id
	 *
	 * @return boolean
	 */
	public function updatePaymentMethod($payment_method_id)
	{
		if ($payment_method_id == 0 || $payment_method_id == 'free')
		{
			$this->setPaymentIsRealtime('No');
			$this->setPaymentMethodId(0);
			$this->setPaymentMethodName('Free');
			$this->setPaymentMethodDescription('Free');
			$this->setPaymentGatewayId('Free');

//			$this->db()->reset();
//			$this->db()->assignStr('payment_is_realtime', 'No');
//			$this->db()->assignStr('payment_method_id', 0);
//			$this->db()->assignStr('payment_method_name', 'Free');
//			$this->db()->assignStr('payment_method_description', 'Free');
//			$this->db()->assignStr('payment_gateway_id', 'Free');
//			$this->db()->update(DB_PREFIX.'orders', 'WHERE oid='.$this->getId());
//			$this->paymentMethodId = 0;
			return true;
		}
		else
		{
			$this->db()->query('SELECT * FROM '.DB_PREFIX.'payment_methods WHERE pid='.intval($payment_method_id).' AND active="Yes"');
			if ($this->db()->moveNext())
			{
				$this->setPaymentIsRealtime($this->db()->col['type']!='custom' ? 'Yes' : 'No');
				$this->setPaymentMethodId($payment_method_id);
				$this->setPaymentMethodName($this->db()->col['name']);
				$this->setPaymentMethodDescription('');
				$this->setPaymentGatewayId($this->db()->col['id']);

//				$this->db()->reset();
//				$this->db()->assignStr('payment_is_realtime', $this->db()->col['type']!='custom'?'Yes':'No');
//				$this->db()->assignStr('payment_method_id', $payment_method_id);
//				$this->db()->assignStr('payment_method_name', $this->db()->col['name']);
//				$this->db()->assignStr('payment_gateway_id', $this->db()->col['id']);
//				$this->db()->update(DB_PREFIX.'orders', 'WHERE oid='.$this->getId());
//				$this->paymentMethodId = $payment_method_id;
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	public function updateOrderFormID($id)
	{
		if ($id)
		{
			$this->setOrderFormID($id);
			return true;
		}
		return false;
	}

	/**
	 * Update order gift message
	 *
	 * @param $gift_message
	 */
	public function storeGiftMessage($gift_message)
	{
		$this->giftMessage = $gift_message;
		$this->db()->query('UPDATE '.DB_PREFIX.'orders SET gift_message="'.$this->db()->escape($gift_message).'" WHERE oid='.intval($this->getId()));
	}

	/**
	 * Run items update again to reset ordered items info
	 */
	public function reupdateItems()
	{
		$db = $this->db();

		$orderItems = $db->selectAll('
			SELECT oc.ocid, oc.admin_quantity as quantity
			FROM '.DB_PREFIX.'orders_content oc
			INNER JOIN '.DB_PREFIX.'orders o ON oc.oid=o.oid AND o.payment_status<>"Received"
			WHERE oc.oid='.intval($this->getId())
		);

		if (count($orderItems))
		{
			$q = array();
			foreach ($orderItems as $orderItem) $q[$orderItem['ocid']] = $orderItem['quantity'];
			return $this->updateItems($q);
		}

		return true;
	}

	/**
	 * Calculate items count
	 */
	public function calculateItemsCount()
	{
		$itemsCount = 0;
		if (is_array($this->lineItems))
		{
			foreach ($this->lineItems as $item)
			{
				/* @var $item Model_LineItem */
				$itemsCount += $item->getAdminQuantity();
			}
		}

		$this->setItemsCount($itemsCount);
	}
	
	/**
	 * Check is shipping is free
	 * 
	 * @return boolean
	 */
	public function isShippingFree()
	{
		if (is_array($this->lineItems))
		{
			foreach ($this->lineItems as $item)
			{
				/* @var $item Model_LineItem */
				if ($item->getFreeShipping() == 'No')
				{
					return false;
				}
			}
		}

		return true;
	}
	
	/**
	 * Get free shipping products count
	 *
	 * @return int
	 */
	public function freeShippingProductsCount()
	{
		$freeShippingCount = 0;
		if (is_array($this->lineItems))
		{
			foreach ($this->lineItems as $item)
			{
				/* @var $item Model_LineItem */
				if ($item->getFreeShipping() == 'Yes')
				{
					$freeShippingCount++;
				}
			}
		}

		return $freeShippingCount;
	}

	/**
	 * Calculate subtotal amount
	 *
	 * @param bool $persist
	 * @param bool $excludeGiftCertificates
	 *
	 * @return int
	 */
	public function calculateSubtotal($persist = true, $excludeGiftCertificates = false)
	{
		return Calculator_Subtotal::getInstance()->calculate($this, $persist, $excludeGiftCertificates);
	}

	/**
	 * Calculate promo discount amount
	 *
	 * @param bool $reset
	 * @param bool $taxesCalculated
	 *
	 * @return mixed
	 */
	protected function calculatePromoDiscount($reset = true, $taxesCalculated = false)
	{
		return Calculator_PromoDiscount::getInstance($this->_settings)->calculate($this, $reset, $taxesCalculated);
	}

	/**
	 * Calculate discount amount
	 *
	 * @param bool $reset
	 * @param bool $taxesCalculated
	 *
	 * @return string
	 */
	protected function calculateDiscount($reset = true, $taxesCalculated = false)
	{
		return Calculator_Discount::getInstance($this->_settings)->calculate($this, $reset, $taxesCalculated);
	}

	/**
	 * Calculate shipping amount
	 *
	 * @param $error_message
	 *
	 * @return float
	 */
	public function calculateShipping(&$error_message)
	{
		$shipments = $this->getShipmentRepository()->getShipmentsByOrderId($this->getId());

		$shipmentsMethods = array();

		foreach ($shipments as $shipmentId => $shipment)
		{
			$shipmentsMethods[$shipmentId] = Shipping_MethodFactory::getMethod($shipment['shipping_ssid']);
		}

		return Calculator_Shipping::getInstance($this->_settings)
			->calculate($this->db(), $this->_settings, $this, $shipmentsMethods, $error_message);
	}

	/**
	 * Calculate shipping tax
	 * 
	 * @return float
	 */
	public function calculateShippingTax()
	{
		return Calculator_ShippingTax::getInstance($this->_settings)->calculate($this);
	}

	/**
	 * Calculate handling amount
	 *
	 * @param bool $shippingIsFree
	 * @param bool $addToShippingWhenNeeded
	 * @param bool $ignoreZeroShippingPrice
	 *
	 * @return float
	 */
	public function calculateHandling($shippingIsFree = false, $addToShippingWhenNeeded = true, $ignoreZeroShippingPrice = false)
	{
		return Calculator_Handling::getInstance($this->_settings)->calculate($this, $shippingIsFree, $addToShippingWhenNeeded, $ignoreZeroShippingPrice);
	}

	/**
	 * Calculate handling tax
	 * 
	 * @return float
	 */
	public function calculateHandlingTax()
	{
		return Calculator_HandlingTax::getInstance($this->_settings)->calculate($this);
	}

	/**
	 * Calculate taxes
	 * 
	 * @return float
	 */
	public function calculateTax()
	{
		if (!$this->getShippingRequired() || ($this->getShippingComplete()))// && $this->getShippingMethodId() != 'disabled'))
		{
			return Calculator_Tax::getInstance()->calculate($this);
		}
		else
		{
			// TODO: HACK: Refactor this, because getTaxProvider() is returning CustomProvider, we need a service locator
			// for configuring TaxProvider/TaxServices, here we need the cart calculation service
			$provider = Tax_ProviderFactory::getTaxProvider();

			if ($provider)
			{
				return $provider->calculateTax($this);
			}
		}

		return 0;
	}

	/**
	 * Calculate total amount
	 * 
	 * @return float
	 */
	public function calculateTotal()
	{
		return Calculator_Total::getInstance()->calculate($this);
	}
	
	/**
	 * Get Shipping Address
	 *
	 * @return mixed
	 */
	public function getShippingAddressFromDb()
	{
		$addressData = $this->getOrderRepository()->getShippingAddress($this->getId());

		$this->setShippingAddress($addressData);

		return $this->getShippingAddress();
	}

	/**
	 * Check are there any recurring billing items in current order
	 *
	 * @return bool
	 */
	public function hasRecurringBillingItems()
	{
		/** @var Model_LineItem $item */
		foreach ($this->lineItems as $item)
		{
			if ($item->getEnableRecurringBilling()) return true;
		}

		return false;
	}

	/**
	 * Check are there only recurring billing items in current order
	 *
	 * @return bool
	 */
	public function hasOnlyRecurringBillingItems()
	{
		/** @var Model_LineItem $item */
		foreach ($this->lineItems as $item)
		{
			if (!$item->getEnableRecurringBilling()) return false;
		}

		return true;
	}

	/**
	 * Get Order Items
	 *
	 * @param bool $includeRemovedProducts
	 *
	 * @return bool|mixed
	 */
	public function getOrderItems($includeRemovedProducts = false)
	{
		if ($this->getId())
		{
			$a = $this->getOrderRepository()->getLineItems($this->getId(), $includeRemovedProducts);

			if (count($a) > 0)
			{
				$subtotal = 0;
				$subtotalWithTax = 0;

				for ($i=0; $i < count($a); $i++)
				{
					$lineItem = new Model_LineItem($a[$i]);

					$this->lineItems[$a[$i]['ocid']] = $lineItem;

					$a[$i] = $lineItem->toArray();

					$subtotal += $lineItem->getFinalPrice() * $lineItem->getFinalQuantity();
					$subtotalWithTax += $lineItem->getPriceWithTax() * $lineItem->getFinalQuantity();
				}

				$this->items = $a;

				$this->setSubtotalAmountWithTax($subtotalWithTax);
				$this->setSubtotalAmount($subtotal);

				$this->calculateItemsCount();

				$this->checkShippingRequired();
				$this->setShippingFreeProductsCount($this->freeShippingProductsCount());

				return $a;
			}
			else
			{
				$this->calculateItemsCount();
				$this->setShippingFreeProductsCount($this->freeShippingProductsCount());
			}
		}

		return false;
	}

	/**
	 * Get order items with additional info
	 *
	 * @param array $msg
	 *
	 * @return mixed
	 */
	public function getOrderItemsExtended(&$msg)
	{
		$pr = new ShoppingCartProducts($this->_db, $this->_settings);

		$FileUtils = FileUtils::getInstance();

		return self::_getOrderItemsExtended($this->items, $this->_settings['CatalogThumbSize'], $this->_settings['CatalogThumbType'], $pr, $FileUtils, $msg);
	}

	/**
	 * Helper function for get order items extended
	 *
	 * @param $orderItems
	 * @param $catalogThumbSize
	 * @param $catalogThumbType
	 * @param $pr
	 * @param $FileUtils
	 * @param $msg
	 *
	 * @return mixed
	 */
	public static function _getOrderItemsExtended(&$orderItems, $catalogThumbSize, $catalogThumbType, $pr, $FileUtils, &$msg)
	{
		if ($orderItems)
		{
			foreach ($orderItems as $key => $orderItem)
			{
				$orderItem['product_price'] = $orderItem['price_withtax'];

				$product_id = $orderItem['product_id'];

				if ($orderItem['product_sub_id'] != '')
				{
					$orderItem['product_id'] = $orderItem['product_id'].' / '.$orderItem['product_sub_id'];
				}
				
				$orderItem['image'] = $pr->getProductImage($product_id, $orderItem['image_location'], $orderItem['image_url']);

				if ($orderItem['image'])
				{
					$orderItem['thumb'] = $thumb_path = $pr->getProductThumb($product_id, $orderItem['image_location'], $orderItem['image_url']);
					$orderItem['thumb'] = $orderItem['thumb']?$orderItem['thumb']:$orderItem['image'];
					
					if ($orderItem['image_location'] == 'Web' || $catalogThumbType == 'Square')
					{
						$orderItem['thumb_size'] = ' width="'.$catalogThumbSize.'" height="'.$catalogThumbSize.'" ';
					}
					else
					{
						$imageSize = FileUtils::getInstance()->getImageSize($thumb_path);

						if ($imageSize)
						{
							if ($imageSize[0] > $imageSize[1])
							{
								$orderItem['thumb_size'] = ' width="'.$catalogThumbSize.'" ';
							}
							else
							{
								$orderItem['thumb_size'] = ' height="'.$catalogThumbSize.'" ';
							}
						}
						else
						{
							$orderItem['thumb_size'] = ' height="'.$catalogThumbSize.'" ';
						}
					}
				}
				else
				{
					$orderItem['thumb'] = false;
				}

				if (isset($orderItem['enable_recurring_billing']) && $orderItem['enable_recurring_billing'] && isset($orderItem['recurring_billing_data']) && !is_null($orderItem['recurring_billing_data']))
				{
					/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
					$recurringBillingData = $orderItem['recurring_billing_data'];
					$itemsQuantity = $orderItem['admin_quantity'];

					$orderItem['recurring_trial_description'] = View_OrderLineItemsViewModel::getRecurringTrialDescription($recurringBillingData, $itemsQuantity, $msg);
					$orderItem['recurring_billing_description'] = View_OrderLineItemsViewModel::getRecurringBillingDescription($recurringBillingData, $itemsQuantity, $msg);
				}

				$orderItems[$key] = $orderItem;
			}
		}

		return $orderItems;
	}

	/**
	 * Reset order values
	 */
	protected function resetValues()
	{
		$this->resetSubtotal();
		$this->resetDiscount();
		$this->resetPromo(); // resetting only promo amount
		$this->resetShipping();
		$this->resetHandling();
		$this->resetShippingTax();
		$this->resetHandlingTax();
		$this->resetTax();
		$this->resetTotal();
	}

	/**
	 * Reset subtotal'
	 */
	protected function resetSubtotal()
	{
		Calculator_Subtotal::getInstance()->reset($this);
	}

	/**
	 * Reset discounts
	 */
	protected function resetDiscount()
	{
		Calculator_Discount::getInstance($this->_settings)->reset($this);
	}

	/**
	 * Reset handling
	 */
	protected function resetHandling()
	{
		Calculator_Handling::getInstance($this->_settings)->reset($this);
	}

	/**
	 * Reset handling tax
	 */
	protected function resetHandlingTax()
	{
		Calculator_HandlingTax::getInstance($this->_settings)->reset($this);
	}

	/**
	 * Reset shipping
	 */
	protected function resetShipping()
	{
        Calculator_Shipping::getInstance($this->_settings)->reset($this);
	}

	/**
	 * Reset shipping taxes
	 */
	protected function resetShippingTax()
	{
		Calculator_ShippingTax::getInstance($this->_settings)->reset($this);
	}

	/**
	 * Reset taxes
	 */
	protected function resetTax()
	{
		Calculator_Tax::getInstance()->reset($this);
	}

	/**
	 * Reset totals
	 */
	protected function resetTotal()
	{
		Calculator_Total::getInstance()->reset($this);
	}

	/**
	 * Get order data
	 *
	 * @return mixed
	 */
	public function getOrderData()
	{
		if ($this->getId() > 0)
		{
			$this->hydrate();
		}
	}

	/**
	 * Recalculates order subtotal amount and discounts only
	 * 
	 * @param boolean $resetDiscounts
	 */
	public function recalcSubtotals($resetDiscounts = true)
	{
		/**
		 * Do full reset
		 */
		$this->resetValues(); // reset everything

		$this->calculateItemsCount();

		/**
		 * Recalculate amount
		 */
		$this->calculateSubtotal(); // calculate subtotal
		$this->calculateDiscount($resetDiscounts); // discount
		$this->calculatePromoDiscount($resetDiscounts); // promo discounts - at this moment we don't care about shipping
		$this->calculateTotal(); // total at this moment will contain running total only without taxes and shipping

		$this->persist();
	}

	/**
	 * Recalculate totals
	 * Suppose to be called when shipping information available
	 *
	 * @param $shippingErrorMessage
	 * @param bool $resetDiscounts
	 */
	public function recalcTotals(&$shippingErrorMessage, $resetDiscounts = true)
	{
		/**
		 * Do partial reset first 
		 */
		$this->resetShipping(); // note - we do not reset shipping method here
		$this->resetHandling();
		$this->resetPromo(); // note - resetting it because it may be changed by shipping
		$this->resetShippingTax();
		$this->resetHandlingTax();
		$this->resetTax();
		$this->resetTotal();

		$this->calculateItemsCount();

		$this->recalc($shippingErrorMessage, $resetDiscounts);

		$this->forceRecalc = false;

		$this->persist();
	}

	/**
	 * Recalculate order amounts
	 *
	 * @param string $shippingErrorMessage
	 * @param bool $resetDiscounts
	 */
	public function recalcEverything(&$shippingErrorMessage = '', $resetDiscounts = true)
	{
		$shippingErrorMessage = '';

		$this->resetValues();

		$this->calculateItemsCount();

		$this->calculateSubtotal();
		$this->recalc($shippingErrorMessage, $resetDiscounts, true);

		$this->forceRecalc = false;

		$this->persist();
	}

	/**
	 * Recalculate order amounts
	 *
	 * @param string $shippingErrorMessage
	 * @param bool $resetDiscounts
	 * @param bool $includeDiscounts
	 */
	protected function recalc(&$shippingErrorMessage = '', $resetDiscounts = true, $includeDiscounts = false)
	{
		/**
		 * Recalculate amount
		 */
		$shippingErrorMessage = '';

		if ($includeDiscounts)
		{
			$this->calculateDiscount($resetDiscounts);
		}

		if ($this->getPromoType() != 'Shipping')
		{
			// if promo is product or global based it must be calculated before
			// shipping because discount may affect shipping amount
			$this->calculatePromoDiscount($resetDiscounts);
		}

		$orderHasOnlyRecurringItems = $this->hasOnlyRecurringBillingItems();

		if ($this->getShippingComplete() && $this->getOrderType() != ORDER::ORDER_TYPE_RECURRING && !$orderHasOnlyRecurringItems)
		{
			$this->calculateShipping($shippingErrorMessage);
			$this->calculateHandling($this->isShippingFree(), true, false);
			$this->calculateShippingTax();
		}

		if ($this->getPromoType() == 'Shipping')
		{
			// for shipping promo codes we calculate promo after shipping
			// because we have to know shipping amount
			$this->calculatePromoDiscount($resetDiscounts);
		}

		if ($this->getShippingComplete() && $this->getOrderType() != ORDER::ORDER_TYPE_RECURRING && !$orderHasOnlyRecurringItems)
		{
			$this->calculateHandlingTax();
		}

		$this->calculateTax();

		//Re-calculate discounts with taxes
		if ($includeDiscounts)
		{
			$this->calculateDiscount(false, true);
		}
		$this->calculatePromoDiscount(false, true);

		$this->calculateTotal();
	}

	/**
	 * Save order data into database
	 */
	public function persist($updateLineItems = false)
	{
		$repository = $this->getOrderRepository();
		$repository->persistOrderData($this);

		if ($updateLineItems)
		{
			foreach ($this->lineItems as $lineItem)
			{
				$repository->persistLineItem($this, $lineItem, false);
			}
		}

		return $this;
	}

	/**
	 * Hydrate order object from database
	 */
	public function hydrate()
	{
		$data = $this->getOrderRepository()->getOrderData($this->getId());

		if ($data)
		{
			$this->forceRecalc = $data['force_recalc'] == 1;

			$this->setUserId($data['uid']);

			//ORDER NUMBER
			$this->setOrderNumber($data['order_num']);
			$this->setStatus($data['status']);
			$this->setPaymentStatus($data['payment_status']);
			$this->setOrderType($data['order_type']);

			$this->setFulfillmentStatus($data['fulfillment_status']);

			$this->setOrderCreatedDate(new DateTime($data['create_date']));
			$this->setOrderPlacedDate(new DateTime($data['create_date']));
			$this->setStatusChangeDate(new DateTime($data['status_date']));

			// TODO: fix duplication
			$this->status_date = $data['status_date'];
			$this->status_date_formatted = $data['status_date_formatted'];

			//SUBTOTAL
			$this->setSubtotalAmount($data['subtotal_amount']);
			
			//DISCOUNT
			$this->setDiscountAmount($data['discount_amount']);
			$this->setDiscountAmountWithTax($data['discount_amount_with_tax']);
			$this->setDiscountValue($data['discount_value']);
			$this->setDiscountType($data['discount_type']);
			
			//PROMO DISCOUNT
			$this->setPromoDiscountAmount($data['promo_discount_amount']);
			$this->setPromoDiscountAmountWithTax($data['promo_discount_amount_with_tax']);
			$this->setPromoDiscountValue($data['promo_discount_value']);
			$this->setPromoDiscountType($data['promo_discount_type']);
			$this->setPromoType($data['promo_type']);
			$this->setPromoCampaignId($data['promo_campaign_id']);
			
			//HANDLING
			$this->setHandlingSeparated($data['handling_separated']);
			$this->setHandlingFee($data['handling_fee']);
			$this->setHandlingFeeType($data['handling_fee_type']);
			$this->setHandlingText($data['handling_text']);
			$this->setHandlingAmount($data['handling_amount']);

			$this->setHandlingTaxable($data['handling_taxable'] == '1');
			$this->setHandlingTaxRate($data['handling_tax_rate']);
			$this->setHandlingTaxAmount($data['handling_tax_amount']);
			$this->setHandlingTaxDescription($data['handling_tax_description']);
			
			//SHIPPING

			$this->getShippingAddressFromDb();

			$this->setShippingRequired($data['shipping_required'] == '1');
			$this->setShippingAmount($data['shipping_amount']);
			$this->setShippingTaxable($data['shipping_taxable']);
			$this->setShippingTaxRate($data['shipping_tax_rate']);
			$this->setShippingTaxAmount($data['shipping_tax_amount']);
			$this->setShippingTaxDescription($data['shipping_tax_description']);

			$this->setShippingTrackingNumber($data['shipping_tracking_number']);
			$this->setShippingTrackingNumberType($data['shipping_tracking_number_type']);
			$this->setShippingTracking(trim($data['shipping_tracking']) != '' ? unserialize($data['shipping_tracking']) : array());

			//TAXES
			$this->setTaxAmount($data['tax_amount']);
			$this->setTaxExempt($data['tax_exempt']);
			
			//PAYMENT
			$this->paymentIsRealtime = $data['payment_is_realtime'];
			$this->paymentMethodId = $data['payment_method_id'];
			$this->paymentMethodName = $data['payment_method_name'];
			$this->paymentGatewayId = $data['payment_gateway_id'];
			$this->paymentMethodDescription = $data['payment_method_description'];

			$this->setPaymentAttemptsCount(intval($data['attempts']));
			
			//OTHER STUFF
			$this->setGiftMessage($data['gift_message']);
			$this->setOffsiteCampaignId(intval($data['offsite_campaign_id']));
			$this->setOrderSource($data['order_source']);

			$this->setTotalAmount($data['total_amount']);

			$this->setOrderFormID($data['ofid']);
		}
	}
}

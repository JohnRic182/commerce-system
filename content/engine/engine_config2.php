<?php
	define('DB_HOST', 'localhost');
	define('DB_USER', 'uroot');
	define('DB_PASSWORD', 'uroot');
	define('DB_NAME', 'dev_tiptontest');
	define('DB_PREFIX', '');
	define('LICENSE_NUMBER', '5845-50882-3-1375309624-f457c545');
	define('PASSWORD_SALT', '$2a$08$Zz5J7aP7twST6FKvYrz61u');

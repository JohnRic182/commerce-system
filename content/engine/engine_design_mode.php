<?php
	/**
	 * Check Cart Designer
	 */

	if ($isDesignMode && isset($_SESSION['admin_auth_id']) && $_SESSION['admin_auth_id'] && isset($_SESSION['admin_design_mode']) && $_SESSION['admin_design_mode'])
	{
		$designMode = new Ddm_DesignMode($settings);

		view()->assign('isAdmin', true);
		view()->assign('isDesignMode', true);
		
		/**
		 * Get current themes 
		 */
		$themesFolderPath = 'content'.DS.'skins';
		$themes = DataAccess_Repository::get('SiteThemeRepository')->getInstalledThemes($themesFolderPath);
		$currentThemes = array();
		foreach ($themes as $theme)
		{
			$currentThemes[$theme->getName()] = array(
				'name' => $theme->getName(), // actual folder name
				'title' => $theme->getTitle(), // skin title
				'author' => $theme->getAuthor(), // who is the author
				'thumbnailImage' => $theme->getThumbnailImage(),
				'fullsizeImage' => $theme->getFullsizeImage(),
				'current' => $theme->getName() == $settings['DesignSkinName']
			);
		}
	
		view()->assign('currentThemes', json_encode($currentThemes));
		view()->assign('themesInstallable', is_writable($themesFolderPath) ? 'true' : 'false');
		
		// Read the contents of the styles and themes directory for display
		// FIXME: Save this into cache folder the way we do languages
		$designStylesheets = array(
			'styles' => array(),
			'themes' => array()
		);
		
		$template_directory = 'content/skins/'.$settings['DesignSkinName'].'/';
		
		if (($fh = opendir($template_directory)) !== false)
		{
			while (($file = readdir($fh)) !== false)
			{
				if (is_dir($template_directory.$file) && in_array($file, array('styles')))//, 'themes')))
				{
					if (($folder = opendir($template_directory.$file)) !== false)
					{
						while (($stylesheet = readdir($folder)) !== false)
						{
							if (!in_array($stylesheet, array('.', '..', '.svn')) && substr($stylesheet, -4) == '.css')
								$designStylesheets[$file][] = $stylesheet;
						}
						closedir($folder);
					}
				}
			}
			closedir($fh);
		}
		
		view()->assign('designStylesheets', $designStylesheets);
		
		$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "";
		
		switch ($action)
		{
			case "loadElement" : 
			{
				$doRender = false;
				if (isset($_REQUEST['class']))
				{
					$templateFile = "templates/" . str_replace('_', '/', substr($_REQUEST['class'], 8)) . '.html';

					if (is_file(view()->templateDir().$templateFile))
					{
						$html = view()->fetch($templateFile);
						$response = array("status" => 1, "html" => $html);
					}
					else
					{
						$response = array("status" => 0, "message" => "Cannot load element.\n".view()->templateDir().$templateFile." file does not exist.\nPlease check permissions");
					}
				}
				else
				{
					$response = array("status" => 0, "message" => 'Error Loading Data');
				}
				echo json_encode($response);
				break;		
			}
		}
	}
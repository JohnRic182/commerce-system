<?php
	$label_app_name = "PinnacleCart";
	$label_home_site = 'http://www.pinnaclecart.com';
	$label_support_area = "http://www.pinnaclecart.com/support/";
	$label_terms = "http://www.pinnaclecart.com/terms/";
	$label_privacy_policy = "http://www.pinnaclecart.com/privacy-policy/";
	$label_help_enabled = true;
	$label_help_url = "http://helpwith.mypinnaclecart.com/";
	$label_first_data = "http://www.pinnaclecart.com/preferred-payment-gateway/";
	$label_stamps = 'pinnaclecart'; //'welcome';

	$label_affiliate_tracking_url = 'https://'.(defined('BILLING_TEST_MODE') && BILLING_TEST_MODE ? 'staging' : 'www' ) .
		'.pinnaclecart.com/wp-content/themes/Pinnacle/affiliate-tracking.html?' .
		(defined('TRIAL_TOKEN') ? TRIAL_TOKEN : '');

	define('QBMS_APP_LOGIN', '');
	define('QBMS_APP_ID', '');

	$label_admin_tracking_script = '';

	global $settings;
	if (strpos($settings['AdminHttpsUrl'], '.mypinnaclecart.com') !== false || (defined('BILLING_TEST_MODE') && BILLING_TEST_MODE))
	{
		$label_admin_tracking_script = '
<script type="text/javascript">
	var dataLayer = [
			{
				"adminMode": "'.( defined('TRIAL_MODE') && TRIAL_MODE ? 'trial' : 'live' ).'"
			}
		];
</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KD9S8W"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
			\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'GTM-KD9S8W\');</script>
<!-- End Google Tag Manager -->';
	}

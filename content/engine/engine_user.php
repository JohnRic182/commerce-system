<?php 
/**
 * Users actions definitions
 */
define("USER_SIGNUP", "user_signup");
define("USER_LOGIN", "user_login");
define("USER_LOGIN_FACEBOOK", "user_login_fb");
define("USER_LOGOUT", "user_logout");
define("USER_LOGOUT_FACEBOOK", "user_logout_fb");
define("USER_PASSWORD_RESET", "password_reset");
define("USER_UPDATE_PROFILE", "user_updateprofile");
define("USER_EXPRESS_CHECKOUT", "user_expresscheckout");
define("USER_START_CHECKOUT", "user_start_checkout");
define("USER_ADD_ADDRESS", "AddShippingAddress");
define("USER_DELETE_ADDRESS", "DeleteShippingAddress");
define("USER_UPDATE_ADDRESS", "UpdateShippingAddress");
define("USER_ADD_TO_WISHLIST", "UserAddToWishlist");
define("USER_FORM_MAIL", "FormMail");
define("USER_SET_COOKIE", "set_c");
define("USER_UNSET_COOKIE", "unset_c");
define("USER_ADD_REVIEW", "AddReview");
define("USER_ADD_PAYMENT_PROFILE", "AddPaymentProfile");
define("USER_DELETE_PAYMENT_PROFILE", "DeletePaymentProfile");
define("USER_UPDATE_PAYMENT_PROFILE", "UpdatePaymentProfile");
define("USER_QR", "qr");
define("USER_OFFSITE_COMMERCE", "osc");
define("EXPRESS_CHECKOUT_USER", "ExpressCheckoutUser");
define("USER_PREDICTIVE_SEARCH", "user_predictive_search");

/**
 * User class
 */
class USER
{
	const ADDRESS_SHIPPING = 1;
	const ADDRESS_BILLING = 2;

	public $id = 0;
	public $sessionId = "";
	public $level = 0;
	public $fname = "";
	public $lname = "";
	public $login = "";
	public $auth_ok = false;
	public $data = array();
	public $messages = array();
	public $is_error = false;
	public $errors = array();
	public $blocked = false;
	public $cookie = "CartUserCookie";
	public $back_user = false;
	public $back_user_login = "";
	public $express = false;
	protected $isTaxExempt = false;
	private $_db;
	private $_settings;
	private $_customFields = null;

	/**
	 * Class constructor
	 * @param $db
	 * @param $settings
	 * @param int $id
	 * @param string $sessionId
	 * @param bool $authenticated
	 */
	public function __construct(&$db, &$settings, $id = 0, $sessionId = '', $authenticated = false)
	{
		$this->_db = $db;
		$this->_settings = $settings;

		//set cookie prefix
		$this->cookie = $this->_settings["SecurityCookiesPrefix"];

		$this->setId($id);
		$this->setSessionId($sessionId);
		$this->auth_ok = $authenticated;

		if ($this->getId() > 0)
		{
			$this->getUserData();
		}
	}

	/**
	 * Get user by id
	 *
	 * @param $db
	 * @param $settings
	 * @param $id
	 *
	 * @return USER
	 */
	public static function getById(&$db, &$settings, $id)
	{
		return new USER($db, $settings, $id);
	}

	/**
	 * Get user from current session
	 * 
	 * @param DB $db
	 * @param array $settings
	 * 
	 * @return USER
	 */
	public static function getFromSession(&$db, &$settings)
	{
		// $cookiePrefix = $settings["SecurityCookiesPrefix"];

		// check returning customers
		// $back_user = in_array($cookiePrefix."LoginUserName", array_keys($_COOKIE));
		// $back_user_login = isset($_COOKIE[$cookiePrefix."LoginUserName"])?$_COOKIE[$cookiePrefix."LoginUserName"]:"";
			
		// check user session
		$id = isset($_SESSION["UserID"]) ? intval($_SESSION["UserID"]) : 0;
		$sessionId = isset($_SESSION["SessionID"]) ? $_SESSION["SessionID"] : "";

		$auth_ok = false;

		// check is user registered (by user id)
		if ($id)
		{
			$db->query("
				SELECT uid FROM ".DB_PREFIX."users 
				WHERE 
					uid='".intval($id)."' AND removed = 'No' AND 
					DATE_ADD(session_date, INTERVAL ".intval($settings["SecurityUserTimeout"])." SECOND)>NOW() AND
					".($settings["SecurityAccountBlocking"] == "YES" ? "block_date < NOW() AND" : "")."
					session_id='".$db->escape($sessionId)."'
			");
			
			if ($db->moveNext())
			{
				// user is registered - user_id, session_id correct and not expired
				$auth_ok = true;
				
				// update session_timeout in db
				$db->query("UPDATE ".DB_PREFIX."users SET session_date=NOW() WHERE uid=".intval($id));
			}
			else
			{
				// user is not registered - can't use existing user session
				$id = 0;
				$sessionId = self::generateSessionId();
			}
			
			// store user data to session
			$_SESSION["SessionID"] = $sessionId;
			$_SESSION["UserID"] = $id;
		}

		return new USER($db, $settings, $id, $sessionId, $auth_ok);
	}

	/**
	 * Set user ID
	 *
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Get current user's ID
	 * 
	 * @return int
	 */
	public function getId()
	{
		return (int) $this->id;
	}

	/**
	 * Set is user tax exempt
	 *
	 * @param $isTaxExempt
	 */
	public function setIsTaxExempt($isTaxExempt)
	{
		$this->isTaxExempt = $isTaxExempt;
	}

	/**
	 * Check is user tax exempt
	 *
	 * @return bool
	 */
	public function getTaxExempt()
	{
		return $this->isTaxExempt;
	}

	/**
	 * Setter for user's level
	 *
	 * @param string $level value to set
	 *
	 * @return self
	 */
	public function setLevel($level)
	{
		$this->level = $level;
	}

	/**
	 * Getter for user's level
	 *
	 * @return int
	 */
	public function getLevel()
	{
		return (int) $this->level;
	}

	/**
	 * Setter for sessionId
	 *
	 * @param string $sessionId value to set
	 *
	 * @return self
	 */
	public function setSessionId($sessionId)
	{
		$this->sessionId = $sessionId;
		return $this;
	}

	/**
	 * Getter for sessionId
	 *
	 * @return string
	 */
	public function getSessionId()
	{
		return $this->sessionId;
	}
	
	/**
	 * Generates random session id
	 * 
	 * @return string
	 */
	public static function generateSessionId()
	{
		return uniqid(md5(rand(1, 1000000)));
	}

	/**
	 * Check is user authorized
	 *
	 * @return bool
	 */
	public function isAuthorized()
	{
		return $this->auth_ok;
	}

	/**
	 * Check is user authenticated
	 *
	 * @return bool
	 */
	public function isAuthenticated()
	{
		return $this->auth_ok && !$this->express;
	}

	/**
	 * Sets error
	 * 
	 * @param string $message
	 */
	public function setError($message)
	{
		if (is_array($message))
		{
			foreach($message as $m) $this->errors[] = $m;
		}
		else
		{
			$this->errors[] = $message;
		}

		$this->is_error = true;
	}
	
	/**
	 * Return database instance
	 * 
	 * @return DB
	 */
	public function db()
	{
		return $this->_db;
	}

	/**
	 * Assign custom fields class
	 * 
	 * @param mixed $customFields
	 *
	 * @return mixed
	 */
	public function customFields($customFields = false)
	{
		if ($customFields) $this->_customFields = $customFields;
		return $this->_customFields = $this->_customFields != null ? $this->_customFields : new CustomFields($this->_db);
	}
	
	/**
	 * Update / edit billing address on express checkout
	 * 
	 * @param array $address
	 */
	public function payPalExpressUserInfo($address)
	{
		//preselect country
		$this->db()->query("SELECT * FROM ".DB_PREFIX."countries WHERE iso_a2 LIKE '".$this->db()->escape($address["COUNTRYCODE"])."'");
		
		$country = $this->db()->moveNext() ? $this->db()->col["coid"] : 1;
		
		//preselect state
		$payPalState = isset($address["STATE"]) ? $address["STATE"] : (isset($address['SHIPTOSTATE']) ? $address['SHIPTOSTATE'] : '');
		$this->db()->query("SELECT * FROM ".DB_PREFIX."states WHERE coid='".$country."' AND (short_name LIKE '".$this->db()->escape($payPalState)."' OR name LIKE '".$this->db()->escape($payPalState)."')");
		
		if ($this->db()->moveNext())
		{
			$state = $this->db()->col["stid"];
			$province = $this->db()->col["name"];
		}
		else
		{
			$state = 0;
			$province = $payPalState;
		}
		
		$this->db()->reset();
		$this->db()->assignStr("fname", 
			trim(
				$address["FIRSTNAME"]
			)
		);
		$this->db()->assignStr("lname", $address["LASTNAME"]);
		
		$this->db()->assignStr("address1", isset($address["STREET"]) ? $address["STREET"] : (isset($address['SHIPTOSTREET']) ? $address['SHIPTOSTREET'] : ''));
		$this->db()->assignStr("address2", isset($address["STREET2"]) ?$address["STREET2"] : (isset($address['SHIPTOSTREET2']) ? $address['SHIPTOSTREET2'] : ''));
		$this->db()->assignStr("city", isset($address["CITY"]) ? $address["CITY"] : (isset($address['SHIPTOCITY']) ? $address['SHIPTOCITY'] : ''));
		$this->db()->assignStr("state", $state);
		$this->db()->assignStr("province", $province);
		$this->db()->assignStr("country", $country);
		$this->db()->assignStr("phone", isset($address["PHONENUM"]) ? $address["PHONENUM"] : '');
		$this->db()->assignStr("zip", isset($address["ZIP"]) ? $address["ZIP"] : isset($address['SHIPTOZIP']) ? $address['SHIPTOZIP'] : '');
		
		$this->db()->assignStr("email", $address["EMAIL"]);
		$this->db()->assign('last_update', 'NOW()');
		$this->db()->update(DB_PREFIX."users", "WHERE uid='".intval($this->getId())."'");
	}
		
	/**
	 * Add address from PayPal to User's Address Book
	 * 
	 * @param array $address
	 * 
	 * @return boolean
	 */
	public function payPal2Shipping($address)
	{
		$this->db()->query("SELECT * FROM ".DB_PREFIX."users WHERE uid='".intval($this->getId())."' AND removed='No'");
		if ($this->db()->moveNext())
		{
			//check primary
			$this->db()->query("SELECT * FROM ".DB_PREFIX."users_shipping WHERE uid='".intval($this->getId())."' AND is_primary='Yes'");
			$is_primary = $this->db()->moveNext()?"No":"Yes";
			//get country id
			$paypalCountry = isset($address['SHIPTOCOUNTRY']) ? $address['SHIPTOCOUNTRY'] : (isset($address['SHIPTOCOUNTRYCODE']) ? $address['SHIPTOCOUNTRYCODE'] : '');

			$this->db()->query("SELECT * FROM ".DB_PREFIX."countries WHERE iso_a2 = '".$this->db()->escape($paypalCountry)."'");
			if ($this->db()->moveNext())
			{
				$country = $this->db()->col["coid"];

				$payPalState = $address["SHIPTOSTATE"];
				$this->db()->query("SELECT * FROM ".DB_PREFIX."states WHERE coid='".intval($country)."' AND (short_name LIKE '".$this->db()->escape($payPalState)."' OR name LIKE '".$this->db()->escape($payPalState)."')");
				if ($this->db()->moveNext())
				{
					$state = $this->db()->col["stid"];
					$province = $this->db()->col["name"];
				}
				else
				{
					$state = 0;
					$province = $address["SHIPTOSTATE"];
				}
			}
			else
			{
				$country = 1;
				$state = 0;
				$province = $address["SHIPTOSTATE"];
			}
			
			//check for duplicate
			$this->db()->query("
				SELECT usid
				FROM ".DB_PREFIX."users_shipping 
				WHERE
					uid = '".intval($this->getId())."' AND
					country = '".intval($country)."' AND
					state = '".intval($state)."' AND
					name = '".$this->db()->escape($address['SHIPTONAME'])."' AND
					company = '".$this->db()->escape(isset($address["SHIPTOBUSINESS"]) ? $address["SHIPTOBUSINESS"] : '')."' AND
					address1 = '".$this->db()->escape($address['SHIPTOSTREET'])."' AND
					address2 = '".$this->db()->escape(isset($address["SHIPTOSTREET2"]) ? $address["SHIPTOSTREET2"] : '')."' AND
					city = '".$this->db()->escape($address['SHIPTOCITY'])."' AND
					province = '".$this->db()->escape($province)."' AND
					zip = '".$this->db()->escape($address["SHIPTOZIP"])."'
				ORDER BY usid DESC 
				LIMIT 1
			");
			
			if ($this->db()->moveNext())
			{
				$address_id = $this->db()->col['usid'];
			}
			else
			{
				$this->db()->reset();
				$this->db()->assign("uid", $this->getId());
				$this->db()->assign("country", intval($country));
				$this->db()->assign("state", intval($state));
				$this->db()->assignStr("is_primary", $is_primary);
				$this->db()->assignStr("name", $address["SHIPTONAME"]);
				$this->db()->assignStr("company", isset($address["SHIPTOBUSINESS"]) ? $address["SHIPTOBUSINESS"] : '');
				$this->db()->assignStr("address1", $address["SHIPTOSTREET"]);
				$this->db()->assignStr("address2", isset($address["SHIPTOSTREET2"]) ? $address["SHIPTOSTREET2"] : '');
				$this->db()->assignStr("city", $address["SHIPTOCITY"]);
				$this->db()->assignStr("province", $province);
				$this->db()->assignStr("zip", $address["SHIPTOZIP"]);
				$address_id = $this->db()->insert(DB_PREFIX."users_shipping");
			}
			return $address_id;
		}
		return false;
	}

	/**
	 * Add shipping address to users account
	 *
	 * @param $address
	 * @param bool $customFieldsData
	 * @param bool $checkForDuplicate
	 * @param bool $validate
	 *
	 * @return bool|int
	 */
	public function addShippingAddress($address, $customFieldsData = false, $checkForDuplicate = false, $validate = true)
	{
		global $msg;
		$this->is_error = false;
		$this->errors = array();

		if ($validate)
		{
			$this->validateAddress($address, self::ADDRESS_SHIPPING);

			if ($this->_settings['EndiciaUspsAddressValidation'] == 1)
			{
				// check is address correct, and if it is not, just exit
				if (!AddressValidator::validate($this->_db, $address, $msg))
				{
					if (AddressValidator::getLastError() == null)
					{
						$this->setAddressVariantError($msg);
					}
					else
					{
						$this->setError(AddressValidator::getLastError());
					}
				}
				else
				{
					$this->setAddressVariantError($msg);
				}
			}
		}
		//validate custom fields for shipping
		if (is_array($customFieldsData))
		{
			$this->customFields()->resetErrors();
			$this->customFields()->validateCustomFields("shipping", $customFieldsData);
			if ($this->customFields()->is_error) $this->setError($this->customFields()->errors);
		}
		
		if (!$this->is_error)
		{
			//check is there already shipping addresses for user
			$this->db()->query("SELECT * FROM ".DB_PREFIX."users_shipping WHERE uid='".intval($this->getId())."'");
			if (!$this->db()->moveNext())
			{
				//this shipping address is first one
				//set it as primary
				$address["set_is_primary"] = "Yes";
			}
			else
			{
				//check if there attempt to set primary address
				if (isset($address["set_is_primary"]) && ($address["set_is_primary"] == "Yes"))
				{
					//reset all existing addresses to "not primary"
					$this->db()->reset();
					$this->db()->assignStr("is_primary", "No");
					$this->db()->update(DB_PREFIX."users_shipping", "WHERE uid='".intval($this->getId())."'");
					//set current to primary
					$address["set_is_primary"] = "Yes";
				}
				else
				{
					//unset primary (this stuff for check bof control which does not pass data if not checked)
					$address["set_is_primary"] = "No";
				}
			}
			
			$duplicate = false;
			
			if ($checkForDuplicate)
			{
				//check for duplicate
				$this->db()->query("
					SELECT * 
					FROM ".DB_PREFIX."users_shipping 
					WHERE
						uid = '".intval($this->getId())."' AND
						address_type = '".intval(isset($address["address_type"]) ? $address["address_type"] : 'Residential')."' AND
						country = '".intval($address["country"])."' AND
						state = '".intval($address["state"])."' AND
						name = '".$this->db()->escape($address['name'])."' AND
						company = '".$this->db()->escape($address['company'])."' AND
						address1 = '".$this->db()->escape($address['address1'])."' AND
						address2 = '".$this->db()->escape($address['address2'])."' AND
						city = '".$this->db()->escape($address['city'])."' AND
						province = '".$this->db()->escape($address['province'])."' AND
						zip = '".$this->db()->escape($address['zip'])."'
					ORDER BY usid DESC 
					LIMIT 1
				");
			
				if ($this->db()->moveNext())
				{
					$address_id = $this->db()->col['usid'];

					$duplicate = true;

					$currentCustomFieldsData = $this->customFields()->getCustomFieldsValues("shipping", $this->id, 0, $address_id);

					if (is_array($currentCustomFieldsData) && is_array($customFieldsData))
					{
						if (count($currentCustomFieldsData) == count($customFieldsData))
						{
							foreach ($currentCustomFieldsData as $cf)
							{
								if ($cf['value'] != $customFieldsData[$cf['field_id']]) $duplicate = false;
							}
						}
						else
						{
							$duplicate = false;
						}
					}
					else
					{
						if (is_array($currentCustomFieldsData) || (is_array($customFieldsData) && count($customFieldsData) > 0)) $duplicate = false;
					}
				}
			}
			
			if (!$duplicate)
			{
				//insert address values
				$this->db()->reset();
				$this->db()->assign("uid", intval($this->getId()));
				$this->db()->assignStr("address_type", isset($address["address_type"]) ? $address["address_type"] : 'Residential');
				$this->db()->assign("country", intval($address["country"]));
				$this->db()->assign("state", intval($address["state"]));
				$this->db()->assignStr("is_primary", $address["set_is_primary"]);
				$this->db()->assignStr("name", $address["name"]);
				$this->db()->assignStr("company", $address["company"]);
				$this->db()->assignStr("address1", $address["address1"]);
				$this->db()->assignStr("address2", $address["address2"]);
				$this->db()->assignStr("city", $address["city"]);
				$this->db()->assignStr("province", $address["province"]);
				$this->db()->assignStr("zip", $address["zip"]);
				$address_id = $this->db()->insert(DB_PREFIX."users_shipping");

				//store custom fields data
				if (is_array($customFieldsData))
				{
					$this->customFields()->storeCustomFields("shipping", $customFieldsData, $this->getId(), 0, $address_id);
				}
			}
			
			return $address_id;
		}

		return false;
	}
	
	public function setAddressVariantError($msg)
	{
		$addressVariants = AddressValidator::getAddressVariants();
		if (!is_null($addressVariants) && $addressVariants)
		{
			$this->setError($msg['shipping']['address_variants_error']);
		}
	}
	
	/**
	 * Update shipping address
	 * 
	 * @param int $address_id
	 * @param array $address
	 * @param mixed $customFieldsData
	 * 
	 * @return boolean
	 */
	public function updateShippingAddress($address_id, $address, $customFieldsData = false)
	{
		global $msg;
		$this->is_error = false;
		$this->errors = array();

		$this->validateAddress($address, self::ADDRESS_SHIPPING);

		if ($this->_settings['EndiciaUspsAddressValidation'] == 1)
		{
			// check is address correct, and if it is not, just exit
			if (!AddressValidator::validate($this->_db, $address, $msg))
			{
				$this->setError(AddressValidator::getLastError());
			}
			else
			{
				$addressVariants = AddressValidator::getAddressVariants();
				if (!is_null($addressVariants) && $addressVariants)
				{
					$this->setError($msg['shipping']['address_variants_error']);
				}
			}
		}

		//validate custom fields for shipping
		if (is_array($customFieldsData))
		{
			$this->customFields()->resetErrors();
			$this->customFields()->validateCustomFields("shipping", $customFieldsData);
			if ($this->customFields()->is_error) $this->setError($this->customFields()->errors);
		}
		
		if (!$this->is_error)
		{
			//check is there already shipping addresses for user
			$this->db()->query("SELECT * FROM ".DB_PREFIX."users_shipping WHERE uid='".intval($this->getId())."'");
			if (!$this->db()->moveNext())
			{
				//this shipping address is first one
				//set it as primary
				$address["set_is_primary"] = "Yes";
			}
			else
			{
				//check if there attempt to set primary address
				if (isset($address["set_is_primary"]) && ($address["set_is_primary"] == "Yes"))
				{
					//reset all existing addresses to "not primary"
					$this->db()->reset();
					$this->db()->assignStr("is_primary", "No");
					$this->db()->update(DB_PREFIX."users_shipping", "WHERE uid='".intval($this->getId())."'");
					//set current to primary
					$address["set_is_primary"] = "Yes";
				}
				else
				{
					//unset primary (this stuff for check bof control which does not pass data if not checked)
					$address["set_is_primary"] = "No";
				}
			}
			
			//insert address values
			$this->db()->reset();
			$this->db()->assign("uid", intval($this->getId()));
			$this->db()->assignStr("address_type", isset($address["address_type"]) ? $address["address_type"] : 'Residential');
			$this->db()->assign("country", intval($address["country"]));
			$this->db()->assign("state", intval($address["state"]));
			$this->db()->assignStr("is_primary", $address["set_is_primary"]);
			$this->db()->assignStr("name", $address["name"]);
			$this->db()->assignStr("company", $address["company"]);
			$this->db()->assignStr("address1", $address["address1"]);
			$this->db()->assignStr("address2", $address["address2"]);
			$this->db()->assignStr("city", $address["city"]);
			$this->db()->assignStr("province", $address["province"]);
			$this->db()->assignStr("zip", $address["zip"]);
			$this->db()->update(DB_PREFIX."users_shipping", "WHERE uid='".intval($this->getId())."' AND usid='".$address_id."'");
			
			//update custom shipping data
			if (is_array($customFieldsData))
			{
				$this->customFields()->storeCustomFields("shipping", $customFieldsData, $this->getId(), 0, $address_id);	
			}
			
			return true;
		}

		return false;
	}
		
	/**
	 * Remove shipping address from user's profile
	 * 
	 * @param $id
	 * 
	 * @return boolean
	 */
	public function deleteShippingAddress($address_id)
	{
		$this->db()->query("SELECT * FROM ".DB_PREFIX."users_shipping WHERE uid='".intval($this->getId())."' AND usid='".intval($address_id)."'");
		
		if ($this->db()->moveNext())
		{
			$this->db()->query("DELETE FROM ".DB_PREFIX."users_shipping WHERE uid='".intval($this->getId())."' AND usid='".intval($address_id)."'");
			$this->db()->query("DELETE FROM ".DB_PREFIX."custom_fields_values WHERE profile_id='".intval($this->getId())."' AND address_id='".intval($address_id)."'");
			return true;
		}
		
		return false;
	}
		
	/**
	 * Get Shipping Addresses
	 * 
	 * @return mixed
	 */
	public function getShippingAddresses()
	{
		$this->db()->query("
			SELECT 
				".DB_PREFIX."users_shipping.*,
				".DB_PREFIX."countries.name AS country_name,
				".DB_PREFIX."states.name AS state_name
			FROM ".DB_PREFIX."users_shipping 
			LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users_shipping.country
			LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."users_shipping.state
			WHERE uid='".intval($this->getId())."'
			ORDER BY is_primary
		");
		return $this->db()->numRows() > 0 ? $this->db()->getRecords() : false; 
	}
	
	/**
	 * Get primary shipping address
	 *
	 * @return mixed
	 */
	public function getShippingAdressPrimary()
	{
		$r = $this->db()->query("
			SELECT 
				".DB_PREFIX."users_shipping.*,
				".DB_PREFIX."countries.name AS country_name,
				".DB_PREFIX."states.name AS state_name
			FROM ".DB_PREFIX."users_shipping 
			LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users_shipping.country
			LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."users_shipping.state
			WHERE uid='".intval($this->getId())."' AND is_primary = 'Yes'
			ORDER BY is_primary
		");
		return $this->db()->numRows($r) > 0 ? $this->db()->moveNext($r) : false;
	}
		
	/**
	 * Get shipping address by ID
	 * 
	 * @param int $address_id
	 * 
	 * @return mixed
	 */
	public function getShippingAddressById($address_id)
	{
		$this->db()->query("SELECT * FROM ".DB_PREFIX."users_shipping WHERE uid='".intval($this->getId())."' AND usid='".intval($address_id)."'");
		return $this->db()->moveNext() ? $this->db()->col : false;
	}

	/**
	 * Validate address
	 *
	 * @param $form
	 * @param int $type
	 *
	 * @return bool
	 */
	public function validateAddress(&$form, $type =  self::ADDRESS_SHIPPING)
	{
		$errCount = count($this->errors);
		$trans = trans($type == self::ADDRESS_SHIPPING ? 'shipping' : 'billing');

		if (isset($form["address_type"]))
		{
			if (trim($form["address_type"]) == '') $this->setError($trans['error_select_adderss_type']);
		}
		
		if (isset($form["name"]))
		{
			if (!validate($form["name"], VALIDATE_NAME)) $this->setError($trans['error_enter_name']);
		}
		else
		{
			if (!isset($form["fname"]) || !validate($form["fname"], VALIDATE_NAME)) $this->setError($trans['error_enter_first_name']);
			if (!isset($form["lname"]) || !validate($form["lname"], VALIDATE_NAME)) $this->setError($trans['error_enter_last_name']);
		}
		
		if (!isset($form["address1"]) || !validate($form["address1"], VALIDATE_STRING)) $this->setError($trans['error_enter_address_line1']);
		if (!isset($form["address2"])) $form["address2"] = "";

		if ($type == self::ADDRESS_SHIPPING)
		{
			if ($this->_settings["FormsShippingCompany"] == 'Required' && (!isset($form["company"]) || !validate($form["company"], VALIDATE_STRING))) $this->setError($trans['error_enter_company_name']);
			if ($this->_settings["FormsShippingAddressLine2"] == 'Required' && (!isset($form["address2"]) || !validate($form["address2"], VALIDATE_STRING))) $this->setError($trans['error_enter_address_line2']);
		}
		else if ($type == self::ADDRESS_BILLING)
		{
			if ($this->_settings["FormsBillingCompany"] == 'Required' && (!isset($form["company"]) || !validate($form["company"], VALIDATE_STRING))) $this->setError($trans['error_enter_company_name']);
			if ($this->_settings["FormsBillingAddressLine2"] == 'Required' && (!isset($form["address2"]) || !validate($form["address2"], VALIDATE_STRING))) $this->setError($trans['error_enter_address_line2']);
		}

		if (!isset($form["city"]) || !validate($form["city"], VALIDATE_STRING)) $this->setError($trans['error_enter_city_name']);
			
		//validate country, state and province & zip
		if (!isset($form["country"]) || !validate($form["country"], VALIDATE_INT))
		{
			$this->setError($trans['error_select_country']);
		}
		else
		{
			$this->db()->query("SELECT * FROM ".DB_PREFIX."countries WHERE coid='".intval($form["country"])."'");
			if (($country = $this->db()->moveNext()) != false)
			{
				$this->db()->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."states WHERE coid='".intval($form["country"])."'");

				$hasProvinces = (int)$country['has_provinces'];
				$c = $this->db()->moveNext();
				if ($c["c"] > 0)
				{
					if ($hasProvinces && (!isset($form["state"]) || !validate($form["state"], VALIDATE_INT)))
					{
						$this->setError($trans['error_enter_state_province_name']);
					}
					else
					{
						$this->db()->query("SELECT * FROM ".DB_PREFIX."states WHERE coid='".intval($form["country"])."' AND stid='".intval($form["state"])."'");
						if (($c = $this->db()->moveNext()) != false)
						{
							$form["province"] = $c["name"];
						}
					}
				}
				else
				{
					if ($hasProvinces && (!isset($form["province"]) || !validate($form["province"], VALIDATE_STRING)))
						$this->setError($trans['error_enter_state_province_name']);
					else
						$form["state"] = 0;
				}
				
				if (isset($form["zip"]))
				{
					$zipRequired = (int)$country['zip_required'];
					if ($country["iso_a3"] == "USA" && !validate($form["zip"], VALIDATE_ZIP_USA)) $this->setError($trans['error_enter_zip_code']);
					elseif ($country["iso_a3"] == "CAN" && !validate($form["zip"], VALIDATE_ZIP_CANADA)) $this->setError($trans['error_enter_postal_code']);
					elseif ($country["iso_a3"] == "GBP" && !validate($form["zip"], VALIDATE_ZIP_UK)) $this->setError($trans['error_enter_postal_code']);
					elseif ($zipRequired && !validate($form["zip"], VALIDATE_ZIP)) $this->setError($trans['error_enter_zip_postal_code']);
				}
				else
				{
					$this->setError($trans['error_enter_zip_postal_code']);
				}
			}
			else
			{
				$this->setError($trans['error_select_country']);
			}
		}

		return $errCount == count($this->errors);
	}

	/**
	 * Sign-up procedure
	 *
	 * @param $form
	 * @param bool $customFieldsData
	 * @param bool $guestUserId
	 *
	 * @return bool
	 */
	public function signUp(&$form, &$customFieldsData = false, $guestUserId = false)
	{
		global $msg;
		$this->is_error = false;
		$this->errors = array();
		$trans = trans('account');

		//check form variable
		if (is_array($form))
		{
			//trim form
			foreach ($form as $key=>$value) $form[$key] = trim($value);

			$this->validateAddress($form, self::ADDRESS_BILLING);

			if ($this->_settings["FormsBillingPhone"] == 'Required' && (!isset($form["phone"]) || !validate($form["phone"], VALIDATE_PHONE))) $this->setError($trans['error_enter_phone_number']);

			//validate custom fields for billing
			if (is_array($customFieldsData))
			{
				$this->customFields()->resetErrors();
				$this->customFields()->validateCustomFields("billing", $customFieldsData);
				if ($this->customFields()->is_error) $this->setError($this->customFields()->errors);
			}
			
			if (!isset($form["login"]) || !validate($form["login"], VALIDATE_USERNAME))
			{
				$this->setError($trans['error_enter_username']);
			}
			else
			{
				$this->db()->query("
					SELECT * 
					FROM ".DB_PREFIX."users 
					WHERE 
						login LIKE '".$this->db()->escape(trim($form["login"]))."' 
						AND removed='No'
				");
				
				if ($this->db()->moveNext()) $this->setError($trans['error_login_exist']);
			}
			
			if (!isset($form["password"]) || !validate($form["password"], VALIDATE_PASSWORD))
			{
				$this->setError($trans['error_check_password']);
			}
			else
			{
				if (!isset($form["password2"]) || (isset($form["password2"]) && $form["password"] != $form["password2"]))
				{
					$this->setError($trans['error_check_password_confirmation']);
				}
			}
			
			if (!isset($form["email"]) || !validate($form["email"], VALIDATE_EMAIL))
			{
				$this->setError($trans['error_enter_valid_email']);
			}
			else
			{
				$this->db()->query("
					SELECT uid
					FROM ".DB_PREFIX."users 
					WHERE 
						LOWER(email) = '".$this->db()->escape(strtolower(trim($form["email"])))."'
						AND login <> '".EXPRESS_CHECKOUT_USER."' AND removed='No'
				");
				
				if ($this->db()->moveNext()) $this->setError($trans['error_email_exist']);
			}
			
			// validate custom fields for account and signup
			if (is_array($customFieldsData))
			{
				$this->customFields()->resetErrors();
				$this->customFields()->validateCustomFields("account", $customFieldsData);
				$this->customFields()->validateCustomFields("signup", $customFieldsData);
				if ($this->customFields()->is_error) $this->setError($this->customFields()->errors);
			}

			$shippingTheSame = isset($form['thesame']);

			if (!$this->is_error && $shippingTheSame && $this->_settings['EndiciaUspsAddressValidation'] == 1)
			{
				// check is address correct, and if it is not, just exit
				if (!AddressValidator::validate($this->_db, $form, $msg))
				{
					$this->setError(AddressValidator::getLastError());
				}
				else
				{
					$addressVariants = AddressValidator::getAddressVariants();
					if (!is_null($addressVariants) && $addressVariants)
					{
						$this->setError($msg['shipping']['address_variants_error']);
					}
				}
			}
			
			$form["receives_marketing"] = isset($form["receives_marketing"]) ? "Yes" : "No";

			if (!$this->is_error)
			{
				$this->db()->reset();
				//account settings
				$this->db()->assign("created_date", "NOW()");
				$this->db()->assign("last_update", "NOW()");
				$this->db()->assignStr("receives_marketing", $form["receives_marketing"]);
				$this->db()->assignStr("email", $form["email"]);
				$this->db()->assignStr("login", $form["login"]);
				$this->db()->assignStr("password", getPasswordHash($form["password"]));
				//billing info
				$this->db()->assignStr("fname", $form["fname"]);
				$this->db()->assignStr("lname", $form["lname"]);
				$this->db()->assignStr("company", $form["company"]);
				//address
				$this->db()->assignStr("address1", $form["address1"]);
				$this->db()->assignStr("address2", $form["address2"]);
				$this->db()->assignStr("city", $form["city"]);
				$this->db()->assignStr("state", intval($form["state"]));
				$this->db()->assignStr("province", $form["province"]);
				$this->db()->assignStr("zip", $form["zip"]);
				$this->db()->assignStr("country", intval($form["country"]));
				//phone
				$this->db()->assignStr("phone", $form["phone"]);
				
				//check is it really new user
				if (!$guestUserId)
				{
					//create account
					$this->setId($this->db()->insert(DB_PREFIX."users"));
				}
				else
				{
					//conver existing user
					$this->setId(intval($guestUserId));
					$this->db()->update(DB_PREFIX."users", "WHERE uid='".$this->getId()."'");
				}
				
				if (is_array($customFieldsData))
				{
					$this->customFields()->storeCustomFields("billing", $customFieldsData, $this->getId(), 0, 0);
					$this->customFields()->storeCustomFields("account", $customFieldsData, $this->getId(), 0, 0);
					$this->customFields()->storeCustomFields("signup", $customFieldsData, $this->getId(), 0, 0);
				}
				
				//store shipping address if checked "the same"
				if ($shippingTheSame)
				{
					$shippingAddress = $form;
					$shippingAddress['is_primary'] = 'Yes';
					$shippingAddress['name'] = $form['fname'].' '.$form['lname'];
					$this->addShippingAddress($shippingAddress, false, false, false);
				}
				
				if ($form['receives_marketing'] == 'Yes')
				{
					NewsletterConnector::subscribe($form['email']);
				}
				
				$this->getUserData();
			}
		}
		else
		{
			$this->is_error = true;
			$this->setError("Incorrect form params");
		}

		return !$this->is_error;
	}
	
	/**
	 * Update profile procedure
	 * 
	 * @param array $form
	 * @param mixed $customFieldsData
	 * 
	 * @return boolean
	 */
	public function updateProfile(&$form, &$customFieldsData = false)
	{
		$this->is_error = false;
		$this->errors = array();
		$trans = trans('account');

		//check form variable
		if (is_array($form))
		{
			//trim form
			foreach ($form as $key=>$value) $form[$key] = trim($value);

			$this->validateAddress($form, self::ADDRESS_BILLING);

			if ($this->_settings["FormsBillingPhone"] == 'Required' && (!isset($form["phone"]) || !validate($form["phone"], VALIDATE_PHONE))) $this->setError($trans['error_enter_phone_number']);

			//validate custom fields for billing
			if (is_array($customFieldsData))
			{
				$this->customFields()->resetErrors();
				$this->customFields()->validateCustomFields("billing", $customFieldsData);
				if ($this->customFields()->is_error) $this->setError($this->customFields()->errors);
			}

			if (isset($form["password"]) && trim($form["password"]) != "")
			{
				if (!validate($form["password"], VALIDATE_PASSWORD))
				{
					$this->setError($trans['error_check_password']);
				}
				else
				{
					if (!isset($form["password2"]) || (isset($form["password2"]) && $form["password"] != $form["password2"]))
					{
						$this->setError($trans['error_check_password_confirmation']);
					}
					else
					{
						if (isset($form["old_password"]) && trim($form["old_password"]) != "")
						{
							$user_data = $this->getUserData();
							$validated = checkPasswordHash($form["old_password"], $user_data['password']);
							if (!$validated)
							{
								$this->setError($trans['error_current_password']);
							}
						}
						else
						{
							$this->setError($trans['error_current_password']);
						}
					}
				}
			}
			
			if (!isset($form["email"]) || !validate($form["email"], VALIDATE_EMAIL))
			{
				$this->setError($trans['error_enter_valid_email']);
			}
			else
			{
				$this->db()->query("
					SELECT uid
					FROM ".DB_PREFIX."users 
					WHERE 
						LOWER(email) = '".$this->db()->escape(strtolower(trim($form["email"])))."'
						AND login <> '".EXPRESS_CHECKOUT_USER."'
						AND uid <> '".intval($this->getId())."'
						AND removed = 'No'
				");
				if ($this->db()->moveNext()) $this->setError($trans['error_email_exist']);
			}
		
			//validate custom fields for account
			if (is_array($customFieldsData))
			{
				$this->customFields()->resetErrors();
				$this->customFields()->validateCustomFields("account", $customFieldsData);
				if ($this->customFields()->is_error) $this->setError($this->customFields()->errors);
			}
			
			$form["receives_marketing"] = isset($form["receives_marketing"]) ? "Yes" : "No";

			if (!$this->is_error)
			{
				$previousNewsletterSetting = 'No';
				$this->db()->query("SELECT email, receives_marketing FROM ".DB_PREFIX."users WHERE uid = '".intval($this->id)."' AND removed='No'");
				$tempRow = $this->db()->moveNext();
				if ($tempRow)
				{
					$previousNewsletterSetting = $tempRow['receives_marketing'];
				}
				
				$this->db()->reset();

				//account settings
				$this->db()->assign("last_update", "NOW()");
				$this->db()->assignStr("receives_marketing", $form["receives_marketing"]);
				$this->db()->assignStr("email", $form["email"]);

				if (isset($form["password"]) && $form["password"] != "")
				{
					$this->db()->assignStr("password", getPasswordHash($form["password"]));
				}
				
				//billing info
				$this->db()->assignStr("fname", $form["fname"]);
				$this->db()->assignStr("lname", $form["lname"]);
				$this->db()->assignStr("company", $form["company"]);
				//address
				$this->db()->assignStr("address1", $form["address1"]);
				$this->db()->assignStr("address2", $form["address2"]);
				$this->db()->assignStr("city", $form["city"]);
				$this->db()->assignStr("state", intval($form["state"]));
				$this->db()->assignStr("province", $form["province"]);
				$this->db()->assignStr("zip", $form["zip"]);
				$this->db()->assignStr("country", intval($form["country"]));
				//phone
				$this->db()->assignStr("phone", $form["phone"]);
				
				$this->db()->update(DB_PREFIX."users", "WHERE ".DB_PREFIX."users.uid='".intval($this->getId())."'");
				
				if (is_array($customFieldsData))
				{
					$this->customFields()->storeCustomFields("billing", $customFieldsData, intval($this->getId()), 0, 0);
					$this->customFields()->storeCustomFields("account", $customFieldsData, intval($this->getId()), 0, 0);
				}
				
				if ($previousNewsletterSetting != $form['receives_marketing'])
				{
					if ($form['receives_marketing'] == 'Yes')
					{
						NewsletterConnector::subscribe($form['email']);
					}
					else
					{
						NewsletterConnector::unsubscribe($form['email']);
					}
				}
			}
		}
		else
		{
			$this->setError("Incorrect form params");
		}

		return !$this->is_error;
	}
	
	/**
	 * Update billing info from payment profile
	 * 
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 */
	public function updateBillingInfoFromPaymentProfile(PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var Model_Address $address */
		$address = $paymentProfile->getBillingData()->getAddress();

		/** @var DB $db */
		$db = $this->db();
		
		$db->reset();
		$db->assignStr('fname', $address->getFirstName());
		$db->assignStr('lname', $address->getLastName());
		$db->assignStr('company', $address->getCompany());
		$db->assignStr('address1', $address->getAddressLine1());
		$db->assignStr('address2', $address->getAddressLine2());
		$db->assignStr('city', $address->getCity());
		$db->assignStr('state', $address->getStateId());
		$db->assignStr('province', $address->getProvince());
		$db->assignStr('zip', $address->getZip());
		$db->assignStr('country', $address->getCountryId());
		$phone = trim($paymentProfile->getBillingData()->getPhone());
		if ($phone != '') $db->assignStr('phone', $phone); // do not empty phone
		
		$db->update(DB_PREFIX.'users', 'WHERE '.DB_PREFIX.'users.uid='.intval($this->getId()));
	}
	
	/**
	 * Update billing info
	 * 
	 * @param array $form
	 * @param mixed $customFieldsData
	 * 
	 * @return boolean
	 */
	public function updateBillingInfo(&$form, &$customFieldsData = false)
	{
		$this->is_error = false;
		$this->errors = array();
		$trans = trans('billing');

		//check form variable
		if (is_array($form))
		{
			//trim form
			foreach ($form as $key=>$value) $form[$key] = trim($value);
			
			$this->validateAddress($form, self::ADDRESS_BILLING);
			
			if ($this->_settings["FormsBillingPhone"] == 'Required' && (!isset($form["phone"]) || !validate($form["phone"], VALIDATE_PHONE))) $this->setError($trans['error_enter_phone_number']);
			
			if (!isset($form["email"]) || !validate($form["email"], VALIDATE_EMAIL))
			{
				$this->setError($trans['error_enter_valid_email']);
			}
			else
			{
				//validate email only if it is not express checkout user
				if (!$this->express)
				{
					$this->db()->query("
						SELECT uid
						FROM ".DB_PREFIX."users 
						WHERE 
							LOWER(email) = '".$this->db()->escape(strtolower(trim($form["email"])))."'
							AND login <> '".EXPRESS_CHECKOUT_USER."'
							AND uid <> '".intval($this->getId())."'
							AND removed='No'
					");
					if ($this->db()->moveNext()) $this->setError($trans['error_email_exist']);
				}
			}
			
			if (is_array($customFieldsData))
			{
				$this->customFields()->resetErrors();
				$this->customFields()->validateCustomFields("billing", $customFieldsData);
				if ($this->customFields()->is_error) $this->setError($this->customFields()->errors);
			}
			
			$this->is_error = count($this->errors) > 0;
			
			if (!$this->is_error)
			{
				$this->db()->reset();
				//profile settings
				$this->db()->assignStr("email", $form["email"]);
				
				if (isset($form["password"]) && $form["password"] != "")
				{
					$this->db()->assignStr("password", getPasswordHash($form["password"]));
				}
				
				//billing info
				$this->db()->assign("last_update", "NOW()");
				$this->db()->assignStr("fname", $form["fname"]);
				$this->db()->assignStr("lname", $form["lname"]);
				$this->db()->assignStr("company", $form["company"]);
				//address
				$this->db()->assignStr("address1", $form["address1"]);
				$this->db()->assignStr("address2", $form["address2"]);
				$this->db()->assignStr("city", $form["city"]);
				$this->db()->assignStr("state", intval($form["state"]));
				$this->db()->assignStr("province", $form["province"]);
				$this->db()->assignStr("zip", $form["zip"]);
				$this->db()->assignStr("country", intval($form["country"]));
				//phone
				$this->db()->assignStr("phone", $form["phone"]);
				$this->db()->update(DB_PREFIX."users", "WHERE ".DB_PREFIX."users.uid='".intval($this->getId())."'");
				
				if (is_array($customFieldsData))
				{
					$this->customFields()->storeCustomFields("billing", $customFieldsData, $this->getId(), 0, 0);
				}
			}
		}
		else
		{
			$this->is_error = true;
			$this->setError("Incorrect form params");
		}

		return !$this->is_error;
	}
	
	/**
	 * Login procedure
	 * 
	 * @param $login
	 * @param $password
	 * @param $remember_me
	 * 
	 * @return boolean
	 */
	public function login($login, $password, $remember_me = false)
	{
		$this->db()->query("
			SELECT uid, IF(block_date > NOW(), 1, 0) AS blocked, password
			FROM ".DB_PREFIX."users 
			WHERE LCASE(login) = '".$this->db()->escape(strtolower($login))."' AND removed='No'
		");

		if ($user_data = $this->db()->moveNext())
		{
			$validated = checkPasswordHash($password, $user_data['password']);

			if (!$validated)
			{
				$this->db()->query("
					SELECT uid
					FROM ".DB_PREFIX."users
					WHERE uid = ".$user_data['uid']." AND (password = SHA1('".$this->db()->escape($password)."') OR password = MD5('".$this->db()->escape($password)."') OR password = PASSWORD('".$this->db()->escape($password)."'))
				");

				$validated = ($this->db()->moveNext() != false);
			}

			if (!$validated)
			{
				return false;
			}

			// check is account blocked
			if ($this->_settings["SecurityAccountBlocking"] == "YES" && $user_data["blocked"] == "1")
			{
				$this->blocked = true;
				$this->auth_ok = false;
				return false;
			}

			$this->setId($user_data["uid"]);

			$this->auth_ok = true;
			
			$this->getUserData();
			
			$this->setSessionId(self::generateSessionId());
			
			$this->db()->query("UPDATE ".DB_PREFIX."users SET session_id='".$this->db()->escape($this->getSessionId())."', session_date=NOW() WHERE uid=".intval($this->id));
			
			$cookies = initCookies();
			
			if ($remember_me)
			{
				$cookies->set($this->cookie."LoginUserName", $login);
			}
			else
			{
				$cookies->remove($this->cookie."LoginUserName");
			}

			$_SESSION["SessionID"] = $this->getSessionId();
			$_SESSION["UserID"] = $this->getId();
			
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Check do we need cross domain cookie setter
	 *
	 * @return bool
	 */
	public function isCrossDomainCookieRequired()
	{
		return parse_url($this->_settings["GlobalHttpUrl"], PHP_URL_HOST) != parse_url($this->_settings["GlobalHttpsUrl"], PHP_URL_HOST);
	}

	/**
	 * Returns URL for cross domain cookie setter
	 *
	 * @param string $httpUrl
	 * @param string $backUrl
	 *
	 * @return string
	 */
	public function getCrossDomainCookieUrl($httpUrl, $backUrl)
	{
		return
			$httpUrl."ua=".USER_SET_COOKIE."&pcsid=".session_id().
			"&data=".base64_encode(serialize(array("b"=>$backUrl, "i"=>$this->getId(), "l"=>$this->getLevel(), "s"=>md5($this->getSessionId()))));
	}

	/**
	 * Facebook synk
	 *
	 * @param string $fbUserId
	 * @param array $fbUserProfile
	 *
	 * @return bool
	 */
	public function facebookSync($fbUserId, $fbUserProfile)
	{
		$this->blocked = false;
		$this->auth_ok = true;
		$this->setSessionId(self::generateSessionId());
		
		$result = $this->db()->query("SELECT *, IF(block_date > NOW(), 1, 0) AS blocked FROM ".DB_PREFIX."users WHERE facebook_user=1 AND facebook_id='".$this->db()->escape($fbUserId)."' AND removed='No'");
		
		/**
		 * User exists
		 */
		if (($data = $this->db()->moveNext($result)) != false)
		{
			$this->setId($data["uid"]);
			
			// check is account blocked
			if ($this->_settings["SecurityAccountBlocking"] == "YES" && $data["blocked"] == "1")
			{
				$this->blocked = true;
				$this->auth_ok = false;
				return false;
			}
			
			// update userdata
			$this->db()->query("UPDATE ".DB_PREFIX."users SET session_id='".$this->db()->escape($this->getSessionId())."', session_date=NOW() WHERE uid=".intval($this->getId()));
			unset($_SESSION["facebook_new_account"]);
		}
		/**
		 * Need to create a new user account
		 */
		else
		{
			$this->auth_ok = true;
			$this->setSessionId(self::generateSessionId());
			
			$this->db()->reset();
			$this->db()->assignStr("facebook_user", 1);
			$this->db()->assignStr("facebook_id", $fbUserId);
			$this->db()->assignStr("login", "fb-".$fbUserId);
			$this->db()->assignStr("password", md5(uniqid(rand())));
			$this->db()->assignStr("fname", $fbUserProfile["first_name"]);
			$this->db()->assignStr("lname", $fbUserProfile["last_name"]);
			$this->db()->assignStr('email', $fbUserProfile['email']);
			$this->db()->assignStr("country", 0);
			$this->db()->assignStr("state", 0);
			$this->db()->assignStr("receives_marketing", "No");
			$this->db()->assign("last_update", "NOW()");
			$this->db()->assign("created_date", "NOW()");
			$this->db()->assignStr("session_id", $this->getSessionId());
			$this->db()->assign("session_date", "NOW()");
			$this->db()->insert(DB_PREFIX."users");
			$this->setId($this->db()->insertId());
			
			$_SESSION["facebook_new_account"] = true;
		}
		
		$this->getUserData();
		
		$_SESSION["SessionID"] = $this->getSessionId();
		$_SESSION["UserID"] = $this->getId();
		
		return true;
	}
	
	/**
	 * Express checkout login procedure
	 * 
	 * @return boolean
	 */
	public function expressLogin()
	{
		$this->db()->query("INSERT INTO ".DB_PREFIX."users (login, password, country, state, receives_marketing, last_update, created_date) VALUES ('".EXPRESS_CHECKOUT_USER."', '".(md5(uniqid(rand())))."', '0', '0', 'No', NOW(), NOW())");
		$this->id = $this->db()->insertId();
		$this->auth_ok = true;
		$this->setSessionId(self::generateSessionId());
		$this->db()->query("UPDATE ".DB_PREFIX."users SET session_id='".$this->getSessionId()."', session_date=NOW() WHERE uid=".intval($this->getId()));

		$_SESSION["SessionID"] = $this->getSessionId();
		$_SESSION["UserID"] = $this->getId();

		$this->getUserData();
		
		return true;
	}
	
	/**
	 * Password reset
	 * 
	 * @param string $login
	 * @param string $email
	 * 
	 * @return boolean
	 */
	public function passwordReset($login, $email)
	{
		$this->db()->query("
			SELECT * 
			FROM ".DB_PREFIX."users 
			WHERE 
				login = '".$this->db()->escape($login)."' AND 
				LOWER(email) = '".strtolower($this->db()->escape($email))."' AND 
				facebook_user = 0 AND 
				login <> '".$this->db()->escape(EXPRESS_CHECKOUT_USER)."' AND 
				removed='No'
		");
		if (($user_data = $this->db()->moveNext()) != false)
		{
			$new_password = generatePassword();
			$this->db()->query("UPDATE ".DB_PREFIX."users SET password='".$this->db()->escape(getPasswordHash($new_password))."', last_update = NOW() WHERE uid='".intval($user_data["uid"])."'");
			Notifications::emailUserResetPassword($user_data, $this->db()->col["uid"], $new_password);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Account blocking on payment failures
	 * 
	 * @param $hours
	 */
	public function block($hours)
	{
		$this->db()->query("UPDATE ".DB_PREFIX."users SET block_date=DATE_ADD(NOW(), INTERVAL ".intval($hours)." HOUR), last_update = NOW() WHERE uid='".intval($this->getId())."'");
	}
	
	/**
	 * Get orders history list
	 * 
	 * @return mixed
	 */
	public function getOrders()
	{
		if ($this->auth_ok)
		{
			$this->db()->query("SELECT *, DATE_FORMAT(status_date, '".$this->_settings["LocalizationDateTimeFormat"]."') AS status_date FROM ".DB_PREFIX."orders WHERE uid='".intval($this->getId())."' AND status <> 'Abandon' AND status <> 'New' ORDER BY placed_date DESC");

			if ($this->db()->numRows() > 0)
			{
				return $this->db()->getRecords();
			}
		}

		return false;
	}
	
	/**
	 * Get historical order data
	 * 
	 * @param int $order_id
	 * 
	 * @return mixed
	 */
	public function getOrderData($order_id)
	{
		if ($this->auth_ok)
		{
			$r = $this->db()->query("
				SELECT ".DB_PREFIX."orders.*,
					DATE_FORMAT(".DB_PREFIX."orders.status_date, '".$this->_settings["LocalizationDateTimeFormat"]."') AS status_date,
					s_countries.name AS shipping_country_name,
					s_states.name AS shipping_state_name
				FROM ".DB_PREFIX."orders 
				LEFT JOIN ".DB_PREFIX."countries AS s_countries ON s_countries.coid = ".DB_PREFIX."orders.shipping_country
				LEFT JOIN ".DB_PREFIX."states AS s_states ON s_states.stid = ".DB_PREFIX."orders.shipping_state
				WHERE oid='".intval($order_id)."' 
					AND uid='".intval($this->getId())."' 
					AND status<>'Abandon' 
					AND status<>'New'
			");

			return $this->db()->moveNext($r) ? $this->db()->col : false;
		}

		return false;
	}

	/**
	 * Get order items for historical order
	 * 
	 * @param int $order_id
	 * 
	 * @return mixed
	 */
	public function getOrderItems($order_id)
	{
		if ($this->auth_ok)
		{
			$this->db()->query("SELECT * FROM ".DB_PREFIX."orders_content WHERE oid='".intval($order_id)."' ORDER BY product_id");
			
			if ($this->db()->numRows()>0)
			{
				$products = $this->db()->getRecords();
				
				for ($i=0; $i<count($products); $i++)
				{
					if ($products[$i]["product_sub_id"] != "")
					{
						$products[$i]["product_id"] = $products[$i]["product_id"]." / ".$products[$i]["product_sub_id"];
					}
				}
			}

			return $products;
		}

		return false;
	}
	
	/**
	 * Logout procedure
	 */
	public function logOut()
	{
		if ($this->auth_ok)
		{
			// generate protect-on-logout session id
			$s = self::generateSessionId();
			$this->db()->query("UPDATE ".DB_PREFIX."users SET session_id='".$this->db()->escape($s)."' WHERE uid=".intval($this->getId()));
		}
		
		unset($_SESSION["SessionID"]);
		unset($_SESSION["UserID"]);
		unset($_SESSION["order_promo_code"]);
		unset($_SESSION["order_gift_certificate"]);

		$this->setId(0);
		$this->setLevel(0);
		$this->auth_ok = false;
		$this->setSessionId("");
	}
	
	/**
	 * Return user data
	 * 
	 * @return mixed
	 */
	public function getUserData()
	{
		if ($this->getId())
		{
			$result = $this->db()->selectOne("
				SELECT 
					".DB_PREFIX."users.*,
					CONCAT(".DB_PREFIX."users.fname, ' ', ".DB_PREFIX."users.lname) AS name,
					".DB_PREFIX."countries.name AS country_name,
					".DB_PREFIX."countries.iso_a2 AS country_iso_a2,
					".DB_PREFIX."countries.iso_a3 AS country_iso_a3,
					".DB_PREFIX."countries.iso_number AS country_iso_number,
					".DB_PREFIX."states.name AS state_name,
					IF(".DB_PREFIX."states.name IS NULL, ".DB_PREFIX."users.province, ".DB_PREFIX."states.name) AS province,
					".DB_PREFIX."states.short_name AS state_abbr
				FROM ".DB_PREFIX."users
				LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users.country
				LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."users.state
				WHERE ".DB_PREFIX."users.uid='".intval($this->getId())."' AND removed='No'
			");
			
			if ($result)
			{
				$this->data = $result;
				$this->setLevel($result["level"]);
				$this->data["usid"] = "billing";
				$this->express = $this->data["login"] == EXPRESS_CHECKOUT_USER;
				return $this->data;
			}
		}

		return false;
	}
	
	/**
	 * Returns current user billing address, phone and email
	 *
	 * @return mixed
	 */
	public function getBillingInfo()
	{
		if ($this->getId())
		{
			return $this->db()->selectOne("
				SELECT 
					".DB_PREFIX."users.fname,
					".DB_PREFIX."users.lname,
					CONCAT(".DB_PREFIX."users.fname, ' ', ".DB_PREFIX."users.lname) AS name,
					".DB_PREFIX."users.company,
					".DB_PREFIX."users.address1,
					".DB_PREFIX."users.address2,
					".DB_PREFIX."users.city,
					".DB_PREFIX."users.state,
					".DB_PREFIX."users.state AS state_id,
					".DB_PREFIX."users.province,
					".DB_PREFIX."users.country,
					".DB_PREFIX."users.country AS country_id,
					".DB_PREFIX."users.zip,
					".DB_PREFIX."users.phone,
					".DB_PREFIX."users.email,
					".DB_PREFIX."countries.name AS country_name,
					".DB_PREFIX."countries.iso_a2 AS country_iso_a2,
					".DB_PREFIX."countries.iso_a3 AS country_iso_a3,
					".DB_PREFIX."countries.iso_number AS country_iso_number,
					".DB_PREFIX."states.name AS state_name,
					".DB_PREFIX."states.short_name AS state_abbr
				FROM ".DB_PREFIX."users 
				LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users.country
				LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."users.state
				WHERE ".DB_PREFIX."users.uid='".intval($this->getId())."' AND removed='No'
			");
		}

		return false;
	}
	
	/**
	 * Set user cookie
	 * 
	 * @param int $uid
	 * @param int $level
	 * @param string $sessionId
	 */
	public function setCookie($uid = null, $level = null, $sessionId = null)
	{
		setcookie(
			"_pcud", 
			base64_encode(
				serialize(
					array(
						"i"=>!is_null($uid) ? $uid : $this->getId(),
						"l"=>!is_null($level) ? $level : $this->getLevel(),
						"s"=>!is_null($sessionId) ? $sessionId : md5($this->getSessionId())
					)
				)
			), 
			time() + 31536000, 
			"/",
			isSslMode() ? $this->_settings["GlobalHttpsCookieDomain"] : $this->_settings["GlobalHttpCookieDomain"]
		);
	}
	
	/**
	 * Unset user cookie
	 */
	public function unsetCookie()
	{
		setcookie(
			"_pcud", 
			"", 
			time() - 31536000, 
			"/",
			isSslMode() ? $this->_settings["GlobalHttpsCookieDomain"] : $this->_settings["GlobalHttpCookieDomain"]
		);
	}
}


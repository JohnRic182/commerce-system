<?php 
	function getPrice($amount)
	{
		global $settings, $_SESSION;
		if (isset($_SESSION['current_currency']))
		{
			$cc = $_SESSION["current_currency"];
		}
		else
		{
			global $currencies;

			$cc = $currencies->getDefaultCurrency();
		}

		return ($amount < 0 ? '-' : '').$cc["symbol_left"].number_format(abs($amount) * (float) $cc["exchange_rate"], $cc["decimal_places"], $settings["LocalizationCurrencyDecimalSymbol"], $settings["LocalizationCurrencySeparatingSymbol"]).$cc["symbol_right"];
	}

	function getAdminPrice($amount)
	{
		global $settings, $_SESSION, $currencies;
		$cc = getCurrentAdminCurrency();

		return ($amount < 0 ? '-' : '').$cc["symbol_left"].number_format(abs($amount) * $cc["exchange_rate"], $cc["decimal_places"], $settings["LocalizationCurrencyDecimalSymbol"], $settings["LocalizationCurrencySeparatingSymbol"]).$cc["symbol_right"];
	}

	function getCurrentAdminCurrency()
	{
		global $currencies, $db;

		if (isset($_SESSION['admin_currency']))
		{
			return $_SESSION['admin_currency'];
		}
		else
		{
			if (is_null($currencies))
			{
				$currencies = new Currencies($db);
			}

			$_SESSION["admin_currency"] = $admin_currency = $currencies->getDefaultCurrency();

			return $admin_currency;
		}
	}

	function getWeight($weight)
	{
		global $settings;
		return number_format($weight, 2, ".", ",")." ".$settings["LocalizationWeightUnits"];
	}

	function weightToLbs($weight){
		global $settings;
		if($settings["LocalizationWeightUnits"] == "kg"){
			return $weight / 0.45359;
		}
		else{
			return $weight;
		}
	}
	
	function weightToKg($weight){
		global $settings;
		if($settings["LocalizationWeightUnits"] == "kg"){
			return $weight;
		}
		else{
			return $weight * 0.45359;
		}
	}

	/**
	 * @param $mysqlDateFormat
	 * @return mixed
	 */
	function mysqlDateFormatToPhp($mysqlDateFormat)
	{
		return str_replace(
			array(
				'%a', '%b', '%c', '%D', '%d', '%e', '%f', '%H', '%h', '%I',
				'%i', '%j', '%k', '%l', '%M', '%m', '%p', '%r', '%S', '%s',
				'%T', '%U', '%u', '%V', '%v', '%W', '%w', '%X', '%x', '%Y',
				'%y', '%%'),
			array(
				'D', 'M', 'n', 'jS', 'd', 'j', 'u', 'H', 'h', 'h',
				'i', 'z', 'G', 'g', 'F', 'm', 'a', 'h:i:sa', 's', 's',
				'H:i:s', 'W', 'W', 'W', 'W', 'l', 'W', 'w', 'Y', 'Y', 'Y',
				'y', '%'
			),
			$mysqlDateFormat
		);
	}
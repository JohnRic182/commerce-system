<?php

	define("PAYMENT_TRANSACTION_ERROR_TEXT", "We were unable to complete your transaction.  Please check your information to ensure it is correct, or try another card. ");
	define("PAYMENT_SERVER_ERROR_TEXT", "We are sorry, but an error occurred while processing your transaction. Please try again or contact the site administrator. ");

	/**
	 * Base class for all payment processors (gateways)
	 */
	class PAYMENT_PROCESSOR
	{
		public $id;
		public $name;
		public $version;
		public $class_name;
		public $type;
		public $description;
		public $testMode                 = false;
		public $allow_change_gateway_url = true;

		public $post_url    = "";
		public $gateway_url = "";

		public $common_vars;
		public $settings_vars;
		public $custom_vars;

		public $is_error      = false;
		public $is_fatal      = false;
		public $error_message = "";

		/** @var bool|USER */
		public $user = false;
		/** @var bool|ORDER */
		public $order       = false;
		public $order_items = false;

		public $show_cvv2            = true;
		public $is_certificate       = false;
		public $certificate_path     = "";
		public $certificate_optional = false;

		public $support_accepted_cc    = true;
		public $requires_cc_start_date = false;
		public $requires_issue_number  = false;
		public $has_issue_number       = false;
		public $need_cc_codes          = false;
		public $payment_page           = "one_page_checkout";

		public $possible_payment_delay         = false;
		public $possible_payment_delay_timeout = 0; // seconds

		public $override_payment_form = false;
		public $payment_form_wide     = false;

		public $payment_in_popup        = false;
		public $payment_in_popup_width  = 800;
		public $payment_in_popup_height = 600;
		public $payment_in_popup_title  = 'Payment';

		public $db_id = 0;

		public $logFile     = false;
		public $logFileName = "";
		public $logFilePath = "content/cache/log";
		public $enableLog   = false;

		public $url_to_gateway = '';

		public $support_ccs = false;
		public $enable_ccs  = false;

		public $currency = false;

		public $redirect    = false;
		public $redirectURL = false;

		public $formtemplate = false;
		public $templatedata = false;

		/**
		 * Indicates do we want to send notification when order placed
		 */
		public function sendEmailNotification($orderStatus)
		{
			return true;
		}

		public function getTemplateData($param = false)
		{
			return $this->templatedata;
		}

		public function getFormTemplate()
		{
			return $this->formtemplate;
		}

		public function getTransactionDetails(ORDER $order)
		{
			return false;
		}

		public function getCcType($ccNum)
		{
			$ccTypeMap = array('AX'  => array('34', '37'),
							   'DI'  => array('300', '301', '302', '303', '304', '305', '36', '54', '55'),
							   'DS'  => array('6011',
								   '622126',
								   '622127',
								   '622128',
								   '622129',
								   '62213',
								   '62214',
								   '62215',
								   '62216',
								   '62217',
								   '62218',
								   '62219',
								   '6222',
								   '6223',
								   '6224',
								   '6225',
								   '6226',
								   '6227',
								   '6228',
								   '62290',
								   '62291',
								   '622920',
								   '622921',
								   '622922',
								   '622923',
								   '622924',
								   '622925',
								   '644',
								   '645',
								   '646',
								   '647',
								   '648',
								   '649',
								   '65'),
							   'MC'  => array('51', '52', '53', '54', '55'),
							   'JCB' => array('3528', '3529', '353', '354', '355', '356', '357', '358'),
							   'SO'  => array('6334', '6767'),
							   'SW'  => array('5018',
								   '5020',
								   '5038',
								   '6304',
								   '6759',
								   '6761',
								   '6762',
								   '6763',
								   '6764',
								   '6765',
								   '6766'),
							   'VI'  => array('4'),);

			foreach ($ccTypeMap as $ccType => $ccPrefixes)
			{
				foreach ($ccPrefixes as $ccPrefix)
				{
					if (substr($ccNum, 0, strlen($ccPrefix)) == $ccPrefix)
					{
						return $ccType;
					}
				}
			}

			return 'OT';
		}

		public function isCardinalCommerceEnabled($ccNum = '')
		{
			global $settings;

			if (trim($ccNum) != '')
			{
				$ccType = $this->getCcType($ccNum);

				if (!in_array($ccType, array('VI', 'MC', 'JCB')))
				{
					return false;
				}
			}

			return strtolower($settings['cardinalcommerce_Enabled']) == 'yes' && strtolower($settings[$this->id . '_Enable_Cardinal_Centinel']) == 'yes';
		}

		/**
		 * return bool|null
		 */
		public function testConnection()
		{
			return null;
		}

		/**
		 *
		 */
		public function clearErrors()
		{
			$this->is_fatal = false;
			$this->is_error = false;
			$this->error_message = '';
		}

		/**
		 * Assign user
		 *
		 * @param USER $user
		 */
		public function assignUser(&$user)
		{
			$this->user = $user;
		}

		/**
		 * Assign order
		 *
		 * @param ORDER $order
		 */
		public function assignOrder(&$order)
		{
			$this->order = $order;
		}

		/**
		 * Assign order items
		 *
		 * @param array $order_items
		 */
		public function assignOrderItems(&$order_items)
		{
			$this->order_items = $order_items;
		}

		/**
		 * @param $db
		 * @param null $urlToGateway
		 * @param null $ccsEnabled
		 */
		public function getSettingsData(&$db, $urlToGateway = null, $ccsEnabled = null)
		{
			global $settings;

			if (is_null($urlToGateway) && is_null($ccsEnabled))
			{
				$result = $db->selectOne('SELECT url_to_gateway, enable_ccs FROM ' . DB_PREFIX . 'payment_methods WHERE pid="' . $db->escape($this->db_id) . '" OR id="' . $db->escape($this->id) . '"');
				if ($result)
				{
					$this->url_to_gateway = $result['url_to_gateway'];

					$this->enable_ccs = $result['enable_ccs'] == '1' && $settings['SecurityCCSActive'] == '1' && $settings['SecurityCCSCertificate'] != '' && $settings['SecurityCCSPrivateKey'] != '';
				}
			}
			else
			{
				$this->url_to_gateway = $urlToGateway;
				$this->enable_ccs = $ccsEnabled && $settings['SecurityCCSActive'] == '1' && $settings['SecurityCCSCertificate'] != '' && $settings['SecurityCCSPrivateKey'] != '';
			}

			$db->query('SELECT name, value FROM ' . DB_PREFIX . 'settings WHERE group_name LIKE "payment_' . $db->escape($this->id) . '"');
			while ($db->moveNext())
			{
				$this->settings_vars[$db->col['name']] = array('name'        => $db->col['name'],
															   'value'       => $db->col['value'],
															   'description' => $db->col["name"],
															   'input_type'  => 'hidden');
			}
		}

		/**
		 * Gt credit card options
		 * @param DB $db
		 * @return mixed
		 */
		public function getCreditCardOptions(&$db)
		{
			$db->query("SELECT * FROM " . DB_PREFIX . "payment_accepted_cc WHERE pid='" . $db->escape($this->db_id) . "' AND enable='Yes'");

			if ($db->numRows() > 0)
			{
				$options = '';

				while ($db->moveNext())
				{
					$options .= '<option value="' . ($db->col["enable_cvv2"]) . '">' . gs($db->col["name"]) . '</option>';
				}

				return $options;
			}

			return false;
		}

		/**
		 * Credit card options codes
		 *
		 * @param DB $db
		 *
		 * @return mixed
		 */
		public function getCreditCardOptionsCodes(&$db)
		{
			$db->query("SELECT * FROM " . DB_PREFIX . "payment_accepted_cc WHERE pid='" . $db->escape($this->db_id) . "' AND enable='Yes'");
			if ($db->numRows() > 0)
			{
				$options = '';

				while ($db->moveNext())
				{
					$options .= '<option value="' . ($db->col["code"]) . '">' . gs($db->col["name"]) . '</option>';
				}

				return $options;
			}

			return false;
		}

		/**
		 *
		 */
		public function addCommon($name)
		{
			$this->common_vars[$name] = array('name'        => '',
											  'value'       => '',
											  'description' => '',
											  'input_type'  => 'hidden');
		}

		/**
		 * Set common value
		 *
		 * @param string $name
		 * @param mixed $value
		 */
		public function setCommonValue($name, $value)
		{
			$this->common_vars[$name]['value'] = $value;
		}

		/**
		 * @param $name
		 * @return null
		 */
		public function getCommonValue($name)
		{
			return isset($this->common_vars[$name]['value']) ? $this->common_vars[$name]['value'] : null;
		}

		/**
		 *
		 * @param type $name
		 * @param type $rename
		 * @param type $value
		 * @param type $description
		 * @param type $input_type
		 */
		public function assignCommon($name, $rename, $value, $description, $input_type)
		{
			$this->common_vars[$name] = array("name"        => $rename,
											  "value"       => $value,
											  "description" => $description,
											  "input_type"  => $input_type);
		}

		/**
		 *
		 * @param type $name
		 * @param type $value
		 * @param type $description
		 * @param type $input_type
		 */
		public function assignCustom($name, $value, $description, $input_type)
		{
			$this->custom_vars[$name] = array("name"        => $name,
											  "value"       => $value,
											  "description" => $description,
											  "input_type"  => $input_type);
		}

		/**
		 * Class constructor
		 * @global type $_SERVER
		 * @global type $_SESSION
		 * @return PAYMENT_PROCESSOR
		 */
		public function PAYMENT_PROCESSOR()
		{
			global $_SERVER, $_SESSION;
			$this->common_vars = array();
			$this->custom_vars = array();
			$this->settings_vars = array();
			$this->post_url = "";
			$this->protocol = "";

			// register common vars
			$this->addCommon("customer_id");

			$this->addCommon("billing_first_name");
			$this->addCommon("billing_last_name");
			$this->addCommon("billing_name");
			$this->addCommon("billing_company");
			$this->addCommon("billing_address");
			$this->addCommon("billing_address1");
			$this->addCommon("billing_address2");
			$this->addCommon("billing_city");
			$this->addCommon("billing_state");
			// wmw 03/30/05 added for paypal ipn
			$this->addCommon("billing_state_abbr");

			$this->addCommon("billing_country");
			$this->addCommon("billing_country_iso_a2");
			$this->addCommon("billing_country_iso_a3");
			$this->addCommon("billing_country_iso_number");
			$this->addCommon("billing_zip");
			$this->addCommon("billing_phone");
			$this->addCommon("billing_email");

			$this->addCommon("shipping_required");
			$this->addCommon("shipping_address_type");
			$this->addCommon("shipping_name");
			$this->addCommon("shipping_company");
			$this->addCommon("shipping_address");
			$this->addCommon("shipping_address1");
			$this->addCommon("shipping_address2");
			$this->addCommon("shipping_city");
			$this->addCommon("shipping_state");
			// wmw 03/30/05 added for paypal ipn
			$this->addCommon("shipping_state_abbr");
			$this->addCommon("shipping_country");
			$this->addCommon("shipping_country_iso_a2");
			$this->addCommon("shipping_country_iso_a3");
			$this->addCommon("shipping_country_iso_number");
			$this->addCommon("shipping_zip");

			$this->addCommon("order_id");
			$this->addCommon("order_total_amount");
			$this->addCommon("order_security_id");
			$this->addCommon("order_tax_amount");
			$this->addCommon("order_shipping_amount");

			$this->addCommon('billingAddress');
			$this->addCommon('shippingAddress');

			$this->currency = $_SESSION["default_currency"];

			$this->logFileName = "content/cache/log/payment-" . $this->id . ".txt";

			return $this;
		}

		/**
		 * Get common data
		 * @param USER $user
		 * @param ORDER $order
		 */
		public function getCommonData(&$user, &$order)
		{
			$this->setCommonValue('billingAddress', new Model_Address($user->getBillingInfo()));
			$this->setCommonValue('shippingAddress', new Model_Address($order->getShippingAddress()));

			$this->setCommonValue("customer_id", $user->id);
			$this->setCommonValue("billing_name", $user->data["fname"] . " " . $user->data["lname"]);
			$this->setCommonValue("billing_last_name", $user->data["lname"]);
			$this->setCommonValue("billing_first_name", $user->data["fname"]);
			$this->setCommonValue("billing_company", $user->data["company"]);
			$this->setCommonValue("billing_address", trim($user->data["address1"] . (trim($user->data["address2"]) != "" ? ("; " . $user->data["address2"]) : "")));
			$this->setCommonValue("billing_address1", $user->data["address1"]);
			$this->setCommonValue("billing_address2", $user->data["address2"]);
			$this->setCommonValue("billing_city", $user->data["city"]);
			$this->setCommonValue("billing_state", $user->data["province"]);
			$this->setCommonValue("billing_state_abbr", $user->data["state_abbr"]);
			$this->setCommonValue("billing_country", $user->data["country_name"]);
			$this->setCommonValue("billing_country_iso_a2", $user->data["country_iso_a2"]);
			$this->setCommonValue("billing_country_iso_a3", $user->data["country_iso_a3"]);
			$this->setCommonValue("billing_country_iso_number", $user->data["country_iso_number"]);
			$this->setCommonValue("billing_zip", $user->data["zip"]);
			$this->setCommonValue("billing_phone", $user->data["phone"]);
			$this->setCommonValue("billing_email", $user->data["email"]);
			$this->setCommonValue("shipping_required", $order->getShippingRequired());
			$this->setCommonvalue("shipping_address_type", $order->shippingAddress["address_type"]);
			$this->setCommonvalue("shipping_name", $order->shippingAddress["name"]);

			$shippingName = $order->shippingAddress['name'];
			$shippingNameParts = explode(' ', trim($shippingName));

			$this->setCommonValue('shipping_first_name', $shippingNameParts[0]);
			unset($shippingNameParts[0]);

			if (count($shippingNameParts) > 0)
			{
				$this->setCommonValue('shipping_last_name', implode(' ', $shippingNameParts));
			}
			else
			{
				$this->setCommonValue('shipping_last_name', '');
			}

			$this->setCommonvalue("shipping_company", $order->shippingAddress["company"]);
			$this->setCommonvalue("shipping_address", trim($order->shippingAddress["address1"] . (trim($order->shippingAddress["address2"]) != "" ? ("; " . $order->shippingAddress["address2"]) : "")));
			$this->setCommonvalue("shipping_address1", $order->shippingAddress["address1"]);
			$this->setCommonvalue("shipping_address2", $order->shippingAddress["address2"]);
			$this->setCommonvalue("shipping_city", $order->shippingAddress["city"]);
			$this->setCommonvalue("shipping_state", $order->shippingAddress["province"]);
			$this->setCommonvalue("shipping_state_abbr", $order->shippingAddress["state_abbr"]);
			$this->setCommonvalue("shipping_zip", $order->shippingAddress["zip"]);
			$this->setCommonvalue("shipping_country", $order->shippingAddress["country"]);
			$this->setCommonValue("shipping_country_iso_a2", $order->shippingAddress["country_iso_a2"]);
			$this->setCommonValue("shipping_country_iso_a3", $order->shippingAddress["country_iso_a3"]);
			$this->setCommonValue("shipping_country_iso_number", $order->shippingAddress["country_iso_number"]);

			$this->setCommonValue("order_id", $order->order_num);
			$this->setCommonValue("order_security_id", $order->getSecurityId());
			$this->setCommonValue("order_oid", $order->oid);
			// *** GIFT CERTIFICATE START
			$this->setCommonValue("order_total_amount", $order->totalAmount - $order->gift_cert_amount);
			// *** GIFT CERTIFICATE END
			//$this->setCommonValue("order_security_id", md5($order->oid."-".$order->order_num));
			$this->setCommonValue("order_tax_amount", $order->taxAmount);
			$this->setCommonValue("order_subtotal_amount", $order->subtotalAmount);
			$this->setCommonValue("order_shipping_amount", $order->shippingAmount);
			$this->setCommonValue("order_handling_amount", $order->handlingAmount);
			$this->setCommonValue("order_handling_separated", $order->handlingSeparated);
			$this->setCommonValue("order_discount_amount", $order->discountAmount);
		}

		/**
		 * Returns payment profile form
		 * @param DB $db
		 * @return array
		 */
		public function getPaymentProfileForm($db)
		{
			$fields = array();

			return $fields;
		}

		/**
		 * Set payment form data
		 *
		 * @param DB $db
		 *
		 * @return array
		 */
		public function getPaymentForm($db)
		{
			global $msg;
			global $_SESSION;
			$this->cc_options = $this->getCreditCardOptions($db);
			$this->cc_options_codes = $this->getCreditCardOptionsCodes($db);

			if ($this->possible_payment_delay)
			{
				$_t_o = isset($_SESSION["payment_session_delay_timeout"]) ? $_SESSION["payment_session_delay_timeout"] : 0;
				$_SESSION["payment_session_delay_timeout"] = $_t_o > $this->possible_payment_delay_timeout ? $_t_o : $this->possible_payment_delay_timeout;
			}

			$fields = array();

			if ($this->type == "cc")
			{
				$accepted_cc = array();

				$db->query("SELECT * FROM " . DB_PREFIX . "payment_accepted_cc WHERE pid='" . $db->escape($this->db_id) . "' AND enable='Yes'");

				if ($db->numRows() > 0)
				{
					while ($db->moveNext())
					{
						$accepted_cc[] = array(//"value" => $this->need_cc_codes ? $db->col["code"] : $db->col["enable_cvv2"], "caption"=>$db->col["name"]
							"value"   => $db->col["enable_cvv2"] . "~" . $db->col["code"],
							"caption" => $db->col["name"]);
					}
				}

				$cc_start_years = array();

				for ($i = date("Y") - 10; $i <= date("Y") + 10; $i++)
				{
					$cc_start_years[] = array("value" => $i, "caption" => $i);
				}

				$cc_exp_years = array();

				for ($i = date("Y"); $i <= date("Y") + 10; $i++)
				{
					$cc_exp_years[] = array("value" => $i, "caption" => $i);
				}

				$cc_start_months = array();

				for ($i = 1; $i <= 12; $i++)
				{
					$cc_start_months[] = array("value" => ($i < 10 ? "0" : "") . $i, "caption" => $i);
				}

				$cc_exp_months = array();

				for ($i = 1; $i <= 12; $i++)
				{
					$cc_exp_months[] = array("value" => ($i < 10 ? "0" : "") . $i, "caption" => $i);
				}

				$fields["oa"] = array("type" => "hidden", "name" => "oa", "value" => ORDER_PROCESS_PAYMENT);

				if (count($accepted_cc) > 0)
				{
					$fields["cc_type"] = array("type"    => "select",
											   "name"    => "form[cc_type]",
											   "id"      => "payment_form_field_cc_type",
											   "value"   => "",
											   "options" => $accepted_cc,
											   "caption" => $msg["billing"]["cc_type"]);
				}

				$fields["cc_first_name"] = array("type"    => "text",
												 "name"    => "form[cc_first_name]",
												 "id"      => "payment_form_field_cc_first_name",
												 "value"   => $this->common_vars["billing_first_name"]["value"],
												 "caption" => $msg["billing"]["cc_first_name"]);
				$fields["cc_last_name"] = array("type"    => "text",
												"name"    => "form[cc_last_name]",
												"id"      => "payment_form_field_cc_last_name",
												"value"   => $this->common_vars["billing_last_name"]["value"],
												"caption" => $msg["billing"]["cc_last_name"]);

				$fields["cc_number"] = array("type"    => "text",
											 "name"    => "form[cc_number]",
											 "id"      => "payment_form_field_cc_number",
											 "value"   => "",
											 "caption" => $msg["billing"]["cc_number"]);

				if (isset($this->requires_cc_start_date))
				{
					if ($this->requires_cc_start_date)
					{
						$fields["cc_start_year"] = array("type"    => "select",
														 "name"    => "form[cc_start_year]",
														 "id"      => "payment_form_field_cc_start_year",
														 "class"   => "select-start-year",
														 "value"   => date("Y"),
														 "options" => $cc_start_years,
														 "caption" => (isset($msg["billing"]["cc_start_date"]) ? $msg["billing"]["cc_start_date"] : "Credit Card Start Date"),
														 "hint"    => (isset($msg["billing"]["cc_start_date_hint"]) ? $msg["billing"]["cc_start_date_hint"] : "Required for Switch and Solo cards"));
						$fields["cc_start_month"] = array("type"    => "select",
														  "name"    => "form[cc_start_month]",
														  "id"      => "payment_form_field_cc_start_month",
														  "class"   => "select-start-month",
														  "value"   => date("n"),
														  "options" => $cc_start_months,
														  "caption" => (isset($msg["billing"]["cc_start_date"]) ? $msg["billing"]["cc_start_date"] : "Credit Card Start Date"),
														  "hint"    => (isset($msg["billing"]["cc_start_date_hint"]) ? $msg["billing"]["cc_start_date_hint"] : "Required for Switch and Solo cards"));
					}
				}

				if ($this->has_issue_number || isset($this->requires_issue_number))
				{
					if ($this->has_issue_number || $this->requires_issue_number)
					{
						$fields["cc_issue_number"] = array("type"    => "text",
														   "name"    => "form[cc_issue_number]",
														   "id"      => "payment_form_field_cc_issue_number",
														   "value"   => "",
														   "caption" => $msg["billing"]["card_issue_number"]);
					}
				}

				$fields["cc_expiration_year"] = array("type"    => "select",
													  "name"    => "form[cc_expiration_year]",
													  "id"      => "payment_form_field_cc_expiration_year",
													  "class"   => "select-expiration-year",
													  "value"   => date("Y"),
													  "options" => $cc_exp_years,
													  "caption" => $msg["billing"]["cc_expiration_date"]);
				$fields["cc_expiration_month"] = array("type"    => "select",
													   "name"    => "form[cc_expiration_month]",
													   "id"      => "payment_form_field_cc_expiration_month",
													   "class"   => "select-expiration-month",
													   "value"   => date("n"),
													   "options" => $cc_exp_months,
													   "caption" => $msg["billing"]["cc_expiration_date"]);

				if ($this->show_cvv2)
				{
					$fields["cc_cvv2"] = array("type"      => "text",
											   "name"      => "form[cc_cvv2]",
											   "id"        => "payment_form_field_cc_cvv2",
											   "maxlength" => "4",
											   "class"     => "input-cvv2",
											   "value"     => "",
											   "caption"   => $msg["billing"]["cc_cvv2"],
											   "hint"      => $msg["billing"]["cc_cvv2_hint"]);
				}
			}

			if ($this->type == "check")
			{
				$fields["oa"] = array("type" => "hidden", "name" => "oa", "value" => ORDER_PROCESS_PAYMENT);
				$fields["check_bank_aba_code"] = array("type"    => "text",
													   "name"    => "form[check_bank_aba_code]",
													   "id"      => "payment_form_field_check_bank_aba_code",
													   "value"   => "",
													   "caption" => $msg["billing"]["check_bank_aba_code"]);
				$fields["check_bank_account_number"] = array("type"    => "text",
															 "name"    => "form[check_bank_account_number]",
															 "id"      => "payment_form_field_check_bank_account_number",
															 "value"   => "",
															 "caption" => $msg["billing"]["check_bank_account_number"]);
				$fields["check_bank_account_type"] = array("type"    => "select",
														   "name"    => "form[check_bank_account_type]",
														   "id"      => "payment_form_field_check_bank_account_type",
														   "value"   => "",
														   "options" => array(array("value"   => "checking",
																					"caption" => $msg["billing"]["check_bank_account_type_checking"]),
															   array("value"   => "business_checking",
																	 "caption" => $msg["billing"]["check_bank_account_type_business_checking"]),
															   array("value"   => "savings",
																	 "caption" => $msg["billing"]["check_bank_account_type_savings"]),

														   ),
														   "caption" => $msg["billing"]["check_bank_account_type"]);
				$fields["check_bank_name"] = array("type"    => "text",
												   "name"    => "form[check_bank_name]",
												   "id"      => "payment_form_field_check_bank_name",
												   "value"   => "",
												   "caption" => $msg["billing"]["check_bank_name"]);
				$fields["check_bank_account_name"] = array("type"    => "text",
														   "name"    => "form[check_bank_account_name]",
														   "id"      => "payment_form_field_check_bank_account_name",
														   "value"   => $this->common_vars["billing_name"]["value"],
														   "caption" => $msg["billing"]["check_bank_account_name"]);
			}

			return $fields;
		}

		/**
		 * Checks does gateway work in a test mode
		 * @return boolean
		 */
		public function isTestMode()
		{
			return $this->testMode;
		}

		/**
		 * @return string
		 */
		public function getJS()
		{
			return '';
		}

		/**
		 * Get javascript script tags to include in checkout html
		 *
		 * @return string
		 */
		public function getCheckoutPageJS()
		{
			return '';
		}

		/**
		 * Get validation javascript
		 */
		public function getValidatorJS()
		{
			global $msg, $settings;

			if ($this->type == "cc")
			{
				$JSmsg = "\n" . "if(!validateName(frm.elements['form[cc_first_name]'].value)){\n" . "	alert('" . $msg["billing"]["enter_valid_cc_first_name"] . "');\n" . "	frm.elements['form[cc_first_name]'].focus();\n" . "	return false;\n" . "}\n" . "if(!validateName(frm.elements['form[cc_last_name]'].value)){\n" . "	alert('" . $msg["billing"]["enter_valid_cc_last_name"] . "');\n" . "	frm.elements['form[cc_last_name]'].focus();\n" . "	return false;\n" . "}\n" . ((isset($settings['CardTypeMustMatchNumber']) && $settings['CardTypeMustMatchNumber'] == 'Yes') ? "if($(frm).find('select[name=\"form[cc_type]\"]').length > 0){\n" . "	var cc_number = $(frm).find('input[name=\"form[cc_number]\"]:first').val();\n" . "	var cc_type_value = $(frm).find('select[name=\"form[cc_type]\"]:first').val();\n" . "	var cc_type_caption = $(frm).find('select[name=\"form[cc_type]\"] option:selected').text();\n" . "	var cc_type_correct = validateCCType(cc_type_value, cc_type_caption, cc_number);\n" . "	if(!cc_type_correct){\n" . "		alert('" . $msg["billing"]["select_valid_cc_type"] . "');\n" . "		$(frm).find('select[name=\"form[cc_type]\"]').focus();\n" . "		return false;\n" . "	}\n" . "}\n" : "") . "if(!LuhnCheck(frm.elements['form[cc_number]'].value) || !validateCCNum(frm.elements['form[cc_number]'].value)){\n" . "	alert('" . $msg["billing"]["enter_valid_cc_number"] . "');\n" . "	frm.elements['form[cc_number]'].focus();\n" . "	return false;\n" . "}\n" . "if(isCardExpired(frm.elements['form[cc_expiration_month]'].value, frm.elements['form[cc_expiration_year]'].value))
					{
						alert('" . addslashes($msg["billing"]["enter_valid_cc_expiration"]) . "');
						return false;
					}\n" . "if(frm.elements['form[cc_cvv2]'] != undefined && !validateCVC2(frm.elements['form[cc_cvv2]'].value)){\n" . "	alert('" . $msg["billing"]["enter_valid_cc_cvv2"] . "');\n" . "	frm.elements['form[cc_cvv2]'].focus();\n" . "	return false;\n" . "}\n" . "if(frm.elements['form[cc_type]']){\n" . "	var a = frm.elements['form[cc_type]'].value.substring(0, 4);\n" . "	if (a == 'Yes~' && frm.elements['form[cc_cvv2]'] != undefined && frm.elements['form[cc_cvv2]'].value == ''){\n" . "		alert('" . $msg["billing"]["enter_valid_cc_cvv2_when_required"] . "');\n" . "		frm.elements['form[cc_cvv2]'].focus();\n" . "		return false;\n" . "	}\n" . "}\n";
				if ($this->requires_issue_number)
				{
					$JSmsg .= "if(!validateText(frm.elements['form[cc_issue_number]'].value)){\n" . "	alert('" . $msg["billing"]["enter_issue_number"] . "');\n" . "	frm.elements['form[cc_issue_number]'].focus();\n" . "	return false;\n" . "}\n";
				}

				return $JSmsg;
			}

			if ($this->type == "check")
			{
				return "\n" . "if(!validateText(frm.elements['form[check_bank_aba_code]'].value)){\n" . "	alert('" . $msg["billing"]["enter_check_bank_aba_code"] . "');\n" . "	frm.elements['form[check_bank_aba_code]'].focus();\n" . "	return false;\n" . "}\n" . "if(!validateText(frm.elements['form[check_bank_account_number]'].value)){\n" . "	alert('" . $msg["billing"]["enter_check_bank_account_number"] . "');\n" . "	frm.elements['form[check_bank_account_number]'].focus();\n" . "	return false;\n" . "}\n" . "if(!validateText(frm.elements['form[check_bank_name]'].value)){\n" . "	alert('" . $msg["billing"]["enter_check_bank_name"] . "');\n" . "	frm.elements['form[check_bank_name]'].focus();\n" . "	return false;\n" . "}\n" . "if(!validateText(frm.elements['form[check_bank_account_name]'].value)){\n" . "	alert('" . $msg["billing"]["enter_check_bank_account_name"] . "');\n" . "	frm.elements['form[check_bank_account_name]'].focus();\n" . "	return false;\n" . "}\n";
			}
		}

		/**
		 * Process payment
		 * @param DB $db
		 * @param USER $user
		 * @param ORDER $order
		 * @param  $post_form
		 * @return bool
		 */
		public function process($db, $user, $order, $post_form)
		{
			return true;
		}

		/**
		 * @return bool
		 */
		public function success()
		{
			return true;
		}

		/**
		 * @param  $db
		 * @param  $custom_id
		 * @param  $order_id
		 * @return bool
		 */
		public function processCustom($db, $custom_id, $order_id)
		{
			return false;
		}

		protected function fireOrderEvent($order, $status, $payment_status)
		{
			Events_OrderEvent::dispatchStatusChangeEvent($order, $status, $payment_status);
		}

		/**
		 * @return string
		 */
		public function getPostUrl()
		{
			return $this->post_url;
		}

		/**
		 * @param string $custom1
		 * @param string $custom2
		 * @param string $custom3
		 * @param string $order_status
		 * @param string $payment_status
		 * @return array
		 */
		public function getVarStatus($custom1 = "", $custom2 = "", $custom3 = "", $order_status = "", $payment_status = "")
		{
			return array("1" => false, "2" => false, "3" => false);
		}

		/**
		 * @param  $db
		 * @param  $order
		 * @param  $card_data
		 * @param  $csr_pem
		 * @return void
		 */
		public function saveCCdata($db, $order, $card_data, $csr_pem)
		{
			global $settings;

			if ($csr_pem != "")
			{
				$openSSL = new openSSL(4096, OPENSSL_KEYTYPE_RSA, 3650, $settings["GlobalServerPath"] . "/content/ssl/openssl.cnf");
				$crypted = $openSSL->crypt(serialize($card_data), $csr_pem);
				$data = urlencode(serialize(array("l4" => substr($card_data["cc"], strlen($card_data["cc"]) - 4, 4),
												  "cr" => $crypted)));
				$db->reset();
				$db->assignStr("card_data", $data);
				$db->update(DB_PREFIX . "orders", "WHERE oid='" . $order->oid . "'");
			}
		}

		/**
		 * Save transaction
		 * @param $db
		 * @param $user
		 * @param $order
		 * @param $response
		 * @param $extra
		 * @param $custom1
		 * @param $custom2
		 * @param $custom3
		 * @param $security_id
		 */
		public function createTransaction(&$db, &$user, &$order, $response = "", $extra = "", $custom1 = "", $custom2 = "", $custom3 = "", $security_id = "")
		{
			$update = false;
			$db->reset();
			if (trim($security_id) != '')
			{
				$update = true;
				$db->assignStr("security_id", $security_id);
			}
			if (!is_null($custom1))
			{
				$update = true;
				$db->assignStr("custom1", $custom1);
			}
			if (!is_null($custom2))
			{
				$update = true;
				$db->assignStr("custom2", $custom2);
			}
			if (!is_null($custom3))
			{
				$update = true;
				$db->assignStr("custom3", $custom3);
			}
			if ($update)
			{
				$db->update(DB_PREFIX . "orders", "WHERE oid='" . $order->oid . "'");
			}

			// Do not change this to use the order methods, the data passed in here for $user/$order is sometimes a tmp_object
			$this->storeTransaction($db, $user, $order, $response, true, $extra, $order->subtotalAmount, $order->totalAmount - $order->gift_cert_amount, $order->shippingAmount, $order->taxAmount);
		}

		public function storeTransaction(&$db, &$user, &$order, $response = "", $success = false, $extra = "", $subtotalAmount = 0, $totalAmount = 0, $shippingAmount = 0, $taxAmount = 0)
		{
			$db->reset();
			$db->assign("oid", $order->oid);
			$db->assign("uid", $user->id);

			$db->assign("completed", 'NOW()');
			$db->assign('is_success', $success ? '1' : '0');

			if (!is_null($extra))
			{
				$db->assignStr("extra", $extra);
			}

			$db->assignStr("payment_type", $this->type);
			$db->assignStr("payment_gateway", $this->id . " (" . $this->name . ")");
			if (is_array($response))
			{
				$this->cleanData($response);
			}
			$db->assignStr("payment_response", is_array($response) ? array2text($response) : $response);

			$db->assignStr("order_subtotal_amount", $subtotalAmount);
			$db->assignStr("order_total_amount", $totalAmount);

			$db->assignStr("shipping_method", "");
			$db->assignStr("shipping_submethod", "");
			$db->assignStr("shipping_amount", $shippingAmount);

			$db->assignStr("tax_amount", $taxAmount);

			$db->insert(DB_PREFIX . "payment_transactions");
		}

		/**
		 * Starts log process by opening file for writing
		 *
		 * @deprecated
		 *
		 * @return bool
		 */
		public function logStart()
		{
			return true;
		}

		/**
		 * Closes log file
		 *
		 * @deprecated
		 *
		 * @return bool
		 */
		public function logFinish()
		{
			return false;
		}

		/**
		 * @param array $values
		 */
		protected function cleanData(array &$values)
		{
			foreach ($this->replaceDataKeys() as $key)
			{
				$parts = explode('.', $key);

				$s_path = &$values;
				for ($i = 0; $i < count($parts); $i++)
				{
					$part = $parts[$i];
					if (isset($s_path[$part]))
					{
						if (is_array($s_path[$part]))
						{
							$s_path = &$s_path[$part];
						}
						else
						{
							if ($i + 1 >= count($parts))
							{
								$s_path[$part] = str_repeat('X', strlen($s_path[$part]));
							}
						}
					}
				}
			}
		}

		/**
		 * Write message into log
		 *
		 * @param $s
		 * @param array $values
		 */
		public function log($s, $values = array())
		{
			if ($this->enableLog)
			{
				if (!is_array($values))
				{
					$values = array($values);
				}

				if ($values)
				{
					$this->cleanData($values);
				}

				file_put_contents($this->logFilePath . "/payment-" . $this->id . ".txt", date("Y/m/d-H:i:s") . " - " . $this->id . "\n" . (is_array($s) ? array2text($s) : $s) . (count($values) > 0 ? "\n" . array2text($values) : '') . "\n\n\n", FILE_APPEND);
			}
		}

		protected function getPaymentMethodId()
		{
			global $db;

			$pid = $db->selectOne("SELECT pid FROM " . DB_PREFIX . "payment_methods WHERE id = '" . $db->escape($this->id) . "'");

			if (!isset($pid['pid']))
			{
				return;
			}

			return $pid['pid'];
		}

		/**
		 * @return array
		 */
		public function replaceDataKeys()
		{
			return array();
		}

		/**
		 * Checks does payment gateway supports refund for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsRefund($order_data = false)
		{
			return false;
		}

		/**
		 * Checks does payment gateway supports partial refund for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsPartialRefund($order_data = false)
		{
			return false;
		}

		/**
		 * Returns refundable amount
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function refundableAmount($order_data = false)
		{
			return 0;
		}

		/**
		 * Refunds order
		 *
		 * @param bool $order_data
		 * @param bool $refund_amount
		 *
		 * @return bool
		 */
		public function refund($order_data = false, $refund_amount = false)
		{
			return false;
		}

		/**
		 * Checks does payment gateway support void for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsVoid($order_data = false)
		{
			return false;
		}

		/**
		 * Void order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function void($order_data = false)
		{
			return false;
		}

		/**
		 * Cancel order, deprecated, use void instead
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function cancel($order_data = false)
		{
			return $this->void($order_data);
		}

		/**
		 * Checks does payment gateway support capture for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsCapture($order_data = false)
		{
			return false;
		}

		/**
		 * Checks does payment gateway support partial captures for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return bool
		 */
		function supportsPartialCapture($order_data = false)
		{
			return false;
		}

		/**
		 * Returns capturable amount
		 * @param mixed $order_data
		 * @return boolean
		 */
		public function capturableAmount($order_data = false)
		{
			return 0;
		}

		/**
		 * Captures funds
		 *
		 * @param bool $order_data
		 * @param bool $capture_amount
		 *
		 * @return bool
		 */
		public function capture($order_data = false, $capture_amount = false)
		{
			return false;
		}

		/**
		 * Returns amount captured
		 *
		 * @param $order_data
		 */
		public function capturedAmount($order_data = false)
		{
			if (in_array($order_data["custom1"], array("order",
					"authorization",
					"fulfill")) && is_numeric($order_data["custom3"])
			)
			{
				return $order_data['custom3'];
			}

			return false;
		}

		/**
		 * Returns current order payment status
		 * @param type $order_data
		 * @return type
		 */
		public function currentOrderPaymentStatus($order_data = false)
		{
			return '';
		}

		/**
		 * Returns whether the payment gateway supports recurring billing profiles
		 * @return bool
		 */
		public function supportsRecurringBillingProfiles()
		{
			return false;
		}

		/**
		 * Returns whether the payment gateway uses the cart's scheduler for recurring billing,
		 *   a value of false means that if the gateway supports recurring billing, it will be
		 *   handled at the payment gateway
		 * @return bool
		 */
		public function useSchedulerForRecurringBilling()
		{
			return false;
		}

		/**
		 * Generate an updated token to use with the recurring profile, returns true if generated successfully
		 *
		 * @param RecurringBilling_Model_RecurringProfile $profile
		 * @param array $post_form
		 *
		 * @return bool
		 */
		public function updateRecurringBillingProfile(RecurringBilling_Model_RecurringProfile $profile, array $post_form)
		{
			return false;
		}

		/**
		 * Generate an payment transaction based on the RecurringProfile and ORDER
		 *
		 * @param RecurringBilling_Model_RecurringProfile $profile
		 * @param ORDER $order
		 *
		 * @return bool
		 */
		public function processRecurringBillingPayment(RecurringBilling_Model_RecurringProfile $profile, ORDER $order)
		{
			return false;
		}

		/**
		 * Checks does payment gateway supports payment profiles
		 *
		 * @return bool
		 */
		public function supportsPaymentProfiles()
		{
			return false;
		}

		//		function getPaymentProfiles($db, $user)
		//		{
		//			return array();
		//		}

		/**
		 * Returns payment profile details
		 *
		 * @param DB $db
		 * @param mixed $ext_profile_id
		 * @param boolean $active
		 *
		 * @return mixed
		 */
		public function getPaymentProfileDetails($db, $ext_profile_id, $active = true)
		{
			return null;
		}

		/**
		 * Returns payment profile array
		 *
		 * @param DB $db
		 * @param $profileId
		 *
		 * @return array
		 */
		public function getPaymentProfile($db, $profileId)
		{
			return $db->selectOne("SELECT * FROM " . DB_PREFIX . "users_payment_profiles WHERE usid = " . intval($profileId));
		}

		/**
		 * Create a new payment profile
		 *
		 * @param DB $db
		 * @param USER $user
		 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
		 *
		 * @return bool
		 */
		public function addPaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			return false;
		}

		/**
		 * Update payment profile
		 *
		 * @param DB $db
		 * @param USER $user
		 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
		 *
		 * @return bool
		 */
		public function updatePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			return false;
		}

		/**
		 * Delete payment profile
		 *
		 * @param DB $db
		 * @param USER $user
		 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
		 *
		 * @return bool
		 */
		public function deletePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			return false;
		}

		/**
		 * Get repository
		 *
		 * @return PaymentProfiles_DataAccess_PaymentProfileRepository
		 */
		protected function getPaymentProfileRepository()
		{
			global $registry;

			return $registry->get('repository_payment_profile');
		}
	}


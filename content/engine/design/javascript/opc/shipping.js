var opc_shipping = (function($) {
	var initModule,
		setNewShippingAddressForm,
		setShippingAddressView,
		setShippingAddressBook,
		setShippingAddressVariants,
		saveShippingAddress,
		editShippingAddress,
		getShippingMethods,
		setShippingMethodsForm,
		setShippingMethod,
		showShippingSection,
		hideShippingSection,
		isCompleted,
		setShippingAddressSet,
		isShippingAddressSet,
		ensureShippingSet
	;

	var opc_shippingAddressSet = false,
		opc_shippingMethodSet = false,
		opc_loadedShippingMethods = false
	;

	initModule = function() {
		$('#opc-form-shipping').submit(function() {
			return false;
		});

		/**
		 * Init shipping form and view
		 */
		if (!opcData.shippingRequired) {
			$("#opc-shipping").hide();
			hideShippingSection();

			opc_shippingAddressSet = true;

			opc_payment.initPayment();
		} else {
			// Get previously selected shipping method id
			if ($.cookie('opc-shipments-methods') != null && $.cookie('opc-shipments-methods') != undefined) {
				opcData.shipmentsMethods = JSON.parse($.cookie('opc-shipments-methods'));
			}

			// Set shipping address book
			setShippingAddressBook(opcData.shippingAddressBook);

			if (opcData.shippingAsBillingDisabled) {
				opcData.shippingAddressId = 'new';
				$('#opc-shipping-as-billing-radio').hide();

				if (opcData.shippingAddressBook != undefined && opcData.shippingAddressBook != null && opcData.shippingAddressBook && opcData.shippingAddressBook.length > 0) {
					$('#opc-shipping-new-radio').show();
				} else {
					$('#opc-shipping-new-radio').hide();
				}
			} else {
				$('#opc-shipping-as-billing-radio').show();
				$('#opc-shipping-new-radio').show();
			}

			// Set current shipping selection
			$("#opc-shipping-address-id-" + opcData.shippingAddressId).attr("checked", true);

			var shippingAddressFormEle = $("#opc-shipping-address-form");
			// If billing is empty - show options
			if (opcData.shippingIsEmpty) {
				$("#opc-shipping-address-view").hide();
				$("#opc-shipping-address-form-new").hide();
				shippingAddressFormEle
					.show()
					.find(".buttons").show();

				opc_payment.hidePaymentSection();
			} else {
				// Else preset address
				shippingAddressFormEle.hide();
				$("#opc-shipping-address-form-new").hide();
				$("#opc-shipping-address-view").show();

				opc_shippingAddressSet = true;
			}

			if (shippingAddressFormEle.find("input[type='radio'][name='shipping-address-id']:checked").length < 1) {
				shippingAddressFormEle.find("input[type='radio'][name='shipping-address-id']:first").attr('checked', 'checked');
			} else if (opcData.shippingAddressId == "new") {
				$("#opc-shipping-address-form-new").show();
			}

			// Update shipping addresses preview
			setShippingAddressView(opcData.shippingAddress);

			// Handle address change
			shippingAddressFormEle.find("input[name='shipping-address-id']").each(function() {
				$(this).change(function() {
					opcData.shippingAddressId = $(this).val();

					var shippingAddressFormEle = $("#opc-shipping-address-form");
					if ($(this).val() == "billing" && $("#opc-billing-form").is(":visible")) {
						shippingAddressFormEle.find(".buttons").hide();
					} else {
						shippingAddressFormEle.find(".buttons").show();
					}

					if ($(this).val() == "new") {
						setNewShippingAddressForm(opcData.shippingAddress);
						$("#opc-shipping-address-form-new").show();
					} else {
						$("#opc-shipping-address-form-new").hide();
					}

					shippingAddressFormEle.data("changed", true);

					opc_shippingAddressSet = false;
					opc_payment.setPaymentMethodsSet(false);
				});
			});

			if (opcData.shippingAddressId == 'new') {
				setNewShippingAddressForm(opcData.shippingAddress);
			}

			// Handle new shipping address save button
			$("#opc-shipping-address-form-button-save").click(function(){
				saveShippingAddress();
				return false;
			});

			// Handle new shipping address edit button
			$("#opc-shipping-address-form-button-edit").click(function(){
				editShippingAddress();
				return false;
			});

			// If country is known, trying to get shipping methods
			if (opc_billing.isCompleted() && opcData.billingData.country > 0 && opcData.shippingAddress.country_id != null && opcData.shippingAddress.country_id > 0) {
				$("#opc-payment-methods,#opc-payment-methods-free").hide();
				getShippingMethods(function(){
					opc_payment.initPayment();
				});
			} else {
				opc_payment.initPayment();
			}
		}
	};

	/**
	 * Sets values into shipping form
	 * @param shippingAddress
	 */
	setNewShippingAddressForm = function(shippingAddress) {
		try {
			setCountriesStates(
				"#field-shipping-country", shippingAddress.country_id,
				"#field-shipping-state", shippingAddress.state_id,
				"#field-shipping-province", shippingAddress.province,
				countriesStates
			);

			$.each(shippingAddress, function(fieldName, fieldValue){
				$("#opc-shipping-address-form-" + fieldName)
					.val(fieldValue)
					.change(function(){
						$("#opc-shipping-address-form").data("changed", true);
						$('#opc-shipping-address-form-use_user_address').val('0');
						opc_shippingAddressSet = false;
						opc_payment.setPaymentMethodsSet(false);

						onePageCheckout.setCompleteButton();
					});
			});

			// Check custom fields changes
			$("#opc-shipping-address-form")
				.data("changed", false)
				.find(".custom-field").each(function(){
					$(this).change(function(){
						$("#opc-shipping-address-form").data("changed", true);
						opc_shippingAddressSet = false;
						opc_payment.setPaymentMethodsSet(false);

						onePageCheckout.setCompleteButton();
					});
				});
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Generates shipping address preview
	 * @param shippingAddress
	 */
	setShippingAddressView = function(shippingAddress) {
		try {
			var html = "";

			if (shippingAddress.country_id != null && shippingAddress.country_id != '0') {
				html = html +
					escapeHtml(shippingAddress.name) + ' <i>(' + (shippingAddress.address_type == 'Business' ? msg.opc_shipping_address_type_business : msg.opc_shipping_address_type_residential) + ')</i><br/>' +
					(shippingAddress.company != '' ? escapeHtml(shippingAddress.company) + '<br>' : '') +
					escapeHtml(shippingAddress.address1) + '<br/>' +
					($.trim(shippingAddress.address2) != '' ? escapeHtml(shippingAddress.address2) + '<br/>' : '') +
					escapeHtml(shippingAddress.city) + ', ' + (shippingAddress.state > 0 ? escapeHtml(shippingAddress.state_name) + ', ' : (shippingAddress.province != '') ? escapeHtml(shippingAddress.province) + ', ' : '')  + escapeHtml(shippingAddress.zip) + '<br/>' +
					escapeHtml(shippingAddress.country_name);
			}

			if (typeof(ShippingAllowSeparateAddress) == 'undefined' || ShippingAllowSeparateAddress != 0) {
				html = html +
					'<div class="gap-top gap-bottom"><a href="#" id="opc-shipping-address-edit-link">' + msg.opc_edit_shipping_address + '</a></div>';
			}

			$("#opc-shipping-address-view").find("div:first").html(html);
			$("#opc-shipping-address-edit-link").click(function() {
				editShippingAddress();
				return false;
			});
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Set shipping address book
	 * @param addressBook
	 * @return
	 */
	setShippingAddressBook = function(addressBook) {
		try {
			var html = '';

			if (addressBook != undefined && addressBook != null && addressBook) {
				html = html + '<table class="fieldset"><tbody>';

				$.each(addressBook, function(key, shippingAddress) {
					html = html +
						'<tr class="field checkbox"><td><div>' +
						'<div class="input"><input type="radio" id="opc-shipping-address-id-' + shippingAddress.usid + '" name="shipping-address-id" value="' + shippingAddress.usid + '"></div>' +
						'<label for="opc-shipping-address-id-' + shippingAddress.usid + '">' +
						escapeHtml(shippingAddress.name) +  ' <i>(' + (shippingAddress.address_type == 'Business' ? msg.opc_shipping_address_type_business : msg.opc_shipping_address_type_residential) + ')</i><br/>' +
						(shippingAddress.company != '' ? escapeHtml(shippingAddress.company) + '<br>' : '') +
						escapeHtml(shippingAddress.address1) + '<br/>' +
						(jQuery.trim(shippingAddress.address2) != '' ? escapeHtml(shippingAddress.address2) + '<br/>' : '') +
						escapeHtml(shippingAddress.city) + ', ' + (shippingAddress.state > 0 ? escapeHtml(shippingAddress.state_name) : escapeHtml(shippingAddress.province)) + ', ' + escapeHtml(shippingAddress.zip) + '<br/>' +
						escapeHtml(shippingAddress.country_name) +
						'</label>' +
						'</div></td></tr>';
				});

				html = html + '</tbody></table>'
			}

			if (html != '') {
				$("#opc-shipping-address-book")
					.html(html)
					.show();
			} else {
				$("#opc-shipping-address-book").hide();
			}
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Set shipping address variant
	 */
	setShippingAddressVariants = function(shippingAddressVariants, useBillingAddress) {
		try {
			var html = '';

			// Show address variants
			$.each(shippingAddressVariants, function (i, address) {
				html = html +
					'<li class="address-variants-li">' +
					'<input name="opc-address-variant-index" class="address-variants-radio" id="address-variant-' + i + '" type="radio" value="' + i +'">' +
					'<div class="address-variants-option"><label for="address-variant-' + i + '">' +
					escapeHtml(address.address1) + '<br/>' +
					(address.address2 != '' ? (escapeHtml(address.address2) + '<br/>') : '') +
					escapeHtml(address.city) + ', ' + escapeHtml(address.state_name) + ' ' + escapeHtml(address.zip) +
					'</label></div>' +
					'</li>';
			});

			// Add current address
			var formPrefix = useBillingAddress ? "#opc-billing-form-" : "#opc-shipping-address-form-";
			var stateElementId = useBillingAddress ? "#opc-billing-form-state" : "#opc-shipping-address-form-state_id";

			html = html +
				'<li class="address-variants-li">' +
				'<input name="opc-address-variant-index" class="address-variants-radio" id="address-variant-user" type="radio" value="user">' +
				'<div class="address-variants-option"><label for="address-variant-user">' +
				'<strong>' + msg.opc_shipping_address_variants_use_my_address + '</strong><br/>' +
				escapeHtml($(formPrefix + 'address1').val()) + '<br/>' +
				($(formPrefix + 'address2').val() != '' ? (escapeHtml($(formPrefix + 'address2').val()) + '<br/>') : '') +
				escapeHtml($(formPrefix + 'city').val()) + ', ' +
				escapeHtml($(stateElementId + ' option[value="' + $(stateElementId).val() + '"]').text()) + ' ' +
				escapeHtml($(formPrefix + 'zip').val()) +
				'</label></div>' +
			'</li>';

			// Build and show dialog
			var addressVariantsDialogEle = $('#opc-dialog-address-variants');
			addressVariantsDialogEle.find('ul').html(html);
			addressVariantsDialogEle.find('input[type=radio]:first').attr('checked', 'checked');

			addressVariantsDialogEle.dialog({
				'modal': true,
				'title': msg.opc_shipping_address_variants_title,
				'minWidth': 700,
				'resizable' : false,
				'buttons': [
					{
						text: msg.opc_shipping_address_variants_use_selected_address,
						click: function() {

							var addressVariantsDialogEle = $('#opc-dialog-address-variants');
							addressVariantsDialogEle.dialog("close");

							var selectedAddressVariant = addressVariantsDialogEle.find('input[type=radio]:checked').val();

							if (selectedAddressVariant != 'user') {
								var selectedAddress = opcData.shippingAddressVariants[selectedAddressVariant];

								$(formPrefix + 'address1').val(selectedAddress.address1);
								$(formPrefix + 'address2').val(selectedAddress.address2);
								$(formPrefix + 'city').val(selectedAddress.city);
								$(stateElementId).val(selectedAddress.state_id);
								$(formPrefix + 'zip').val(selectedAddress.zip);

								if (useBillingAddress) {
									$("#opc-billing-form").data("changed", true);
									$('#opc-billing-form-button-save').click();
								} else {
									$("#opc-shipping-address-form").data("changed", true);
									$('#opc-shipping-address-form-button-save').click();
								}
							} else {
								$('#opc-billing-form-use_user_address').val('1');
								$('#opc-shipping-address-form-use_user_address').val('1');
								$('#opc-billing-form-button-save').click();
							}
						}
					},
					{
						text: msg.opc_shipping_address_variants_edit_address,
						click: function() {
							$('#opc-dialog-address-variants').dialog("close");
						}
					}
				]
			});
		} catch (err) {
			alert(err);
			onePageCheckout.setCompleteButton();
		}
	};

	/**
	 * Save shipping address
	 */
	saveShippingAddress = function() {
		try {
			var shippingAddressFormContainerEle = $("#opc-shipping-address-form");
			if (shippingAddressFormContainerEle.data("changed") || opcData.shippingIsEmpty) {
				opc_shippingAddressSet = false;
				opc_payment.setPaymentMethodsSet(false);

				onePageCheckout.sendRequest(
					"saveShippingAddress",
					{
						'shippingForm': shippingAddressFormContainerEle.find("form").serialize()
					},
					function (response) {
						if (response.result) {
							// Set Bongo Status
							if (response.data.bongoActive != undefined && response.data.bongoActive != null) opcData.bongoActive = response.data.bongoActive;
							if (response.data.bongoUsed != undefined && response.data.bongoUsed != null) opcData.bongoUsed = response.data.bongoUsed;

							if (response.data.shippingAddressVariants != undefined && response.data.shippingAddressVariants != null) {
								var shippingAsBilling = $("#opc-shipping-address-id-billing").is(":checked");
								opcData.shippingAddressVariants = response.data.shippingAddressVariants;
								setShippingAddressVariants(opcData.shippingAddressVariants, shippingAsBilling);
							} else {
								// Process response
								opcData.shippingAddress = response.data.shippingAddress;

								opc_shippingAddressSet = true;
								onePageCheckout.setBongoOnly(false);

								$('#opc-shipping-address-form-address1').val(opcData.shippingAddress.address1);
								$('#opc-shipping-address-form-address2').val(opcData.shippingAddress.address2);
								$('#opc-shipping-address-form-city').val(opcData.shippingAddress.city);
								$('#opc-shipping-address-form-state_id').val(opcData.shippingAddress.state_id);
								$('#opc-shipping-address-form-zip').val(opcData.shippingAddress.zip);

								setShippingAddressView(opcData.shippingAddress);

								$("#opc-shipping-address-form").data("changed", false).slideUp('fast');
								$("#opc-shipping-address-view").slideDown('fast');

								var needToReloadShipping = false;
								// Check are shipping methods returned
								if (response.data.shipments != undefined && response.data.shipments != null) {
									opcData.shipments = response.data.shipments;
									setShippingMethodsForm(opcData.shipments);
									needToReloadShipping = true;
								}

								var finish = function() {
									// Check bongo status
									onePageCheckout.checkBongoStatus();
									opc_payment.showPaymentSection();
								};

								if (needToReloadShipping) {
									// Set current method as active
									setShippingMethod(finish);
								} else {
									finish();
								}
							}

							onePageCheckout.setCompleteButton();
						}
					},
					msg.opc_saving_shipping_address
				);
			} else {
				shippingAddressFormContainerEle.slideUp('fast');
				$("#opc-shipping-address-view").slideDown('fast');

				opc_payment.showPaymentSection();
				onePageCheckout.setCompleteButton();
			}
		} catch (err) {
			alert(err);
			onePageCheckout.setCompleteButton();
		}
	};

	/**
	 * Edit shipping information
	 */
	editShippingAddress = function()
	{
		try
		{
			opc_payment.hidePaymentSection();

			var shippingAddressFormEle = $("#opc-shipping-address-form");
			shippingAddressFormEle.find(".buttons").show();

			$("#opc-shipping-address-view").slideUp('fast');
			shippingAddressFormEle.slideDown('fast');

			if (opcData.shippingAddressId == "new") {
				$("#opc-shipping-address-form-new").show();
			} else {
				$("#opc-shipping-address-form-new").hide();
			}
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Get shipping method
	 */
	getShippingMethods = function(callback) {
		try {
			onePageCheckout.sendRequest(
				'getShippingMethods',
				{
					'data': true
				},
				function (response) {
					if (response.result) {
						opc_loadedShippingMethods = true;

						// Set Bongo status
						if (response.data.bongoActive != undefined && response.data.bongoActive != null) opcData.bongoActive = response.data.bongoActive;
						if (response.data.bongoUsed != undefined && response.data.bongoUsed != null) opcData.bongoUsed = response.data.bongoUsed;

						onePageCheckout.setBongoOnly(false);

						var needsToReloadShipping = !opc_shippingMethodSet;
						// Check are shipping methods returned
						if (response.data.shipments != undefined && response.data.shipments != null) {
							opcData.shipments = response.data.shipments;
							setShippingMethodsForm(opcData.shipments);
							needsToReloadShipping = true;
						} else {
							alert(msg.opc_error_no_shipping_methods);
						}

						var finish = function() {
							// Check bongo status
							onePageCheckout.checkBongoStatus();
							if (callback != undefined && callback != null) {
								callback();
							}
						};

						if (needsToReloadShipping) {
							// Set current method as active
							setShippingMethod(finish);
						} else {
							finish();
						}
					}
				},
				msg.opc_getting_shipping_quotes
			);
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Set shipping methods form
	 */
	setShippingMethodsForm = function(shipments) {
		try {
			var shippingMethodsFormEle = $("#opc-shipping-methods-form");

			opc_loadedShippingMethods = true;

			/**
			 * Check is shipping disabled
			 */
			if (!opcData.shippingEnabled) {
				shippingMethodsFormEle.html('');
				opcData.shipmentsMethods = {};
				$.cookie('opc-shipments-methods', JSON.stringify(opcData.shipmentsMethods));
				onePageCheckout.setCompleteButton();
				onePageCheckout.setInvoicePreview();
				opc_shippingMethodSet = true;
				return;
			}

			var html = '<strong>' + msg.opc_shipping_methods_title + '</strong><div class="gap-top gap-bottom">';

			if (!opc_shippingAddressSet) {
				html = html + msg.opc_select_shipping_address;
			} else {

				opc_shippingMethodSet = opcData.shippingWithoutMethod;

				if (opcData.bongoActive && opcData.bongoUsed) {
					html = html + msg.opc_shipping_bongo_used + '<br/><br/>';
				}

				var n = 1;

				$.each(shipments, function(shipmentId, shipment){
					html +=
						'<div class="opc-shipment"><table class="fieldset"><tbody>' +
							'<tr><td class="opc-shipment-title"><strong>Shipment #' + (n++) + (shipment.shipping_type == 'local' ? ' - Local delivery' : '' ) + '</strong></td></tr>' +
							'<tr class="field radio"><td><div><ul class="opc-shipment-items">';

					$.each(shipment.items, function(ocid, shippingItem) {
						html += '<li>' +shippingItem.quantity + ' x ' + shippingItem.title + (shippingItem.free_shipping ? ' <i>(free shipping)</i>' : '') + '</li>';
					});
					html += '</ul></div></td></tr>';

					if (shipment.shipping_methods != undefined && shipment.shipping_methods) {
						var shipping_methods_arr = new Array();

						$.each(shipment.shipping_methods, function (ssid, shippingMethod) {
							shipping_methods_arr.push(shippingMethod);
						});

						shipping_methods_arr.sort(function (obj1, obj2) {
							if (parseInt(obj1.priority) < parseInt(obj2.priority)) return -1;
							if (parseInt(obj1.priority) > parseInt(obj2.priority)) return 1;
							return 0;
						});

						$.each(shipping_methods_arr, function (key, shippingMethod) {
							ssid = shippingMethod.ssid;
							html +=
								'<tr class="field radio"><td><div>' +
								'<div class="input">' +
								'<input type="radio" class="shipment-shipping-method" id="opc-shipping-method-' + shipmentId + '-' + shippingMethod.ssid + '" data-shipment-id="' + shipmentId + '" name="shipping-method-id[' + shipmentId + ']" value="' + shippingMethod.ssid + '">' +
								'</div>' +
								'<label for="opc-shipping-method-' + shipmentId + '-' + shippingMethod.ssid + '">' +
								(shippingMethod.orderByTime && shippingMethod.deliveryDay != 'Today' ? 'Local Delivery' : shippingMethod.method_name) + (opcData.bongoActive && opcData.bongoUsed ? '' : ' - ' + shippingMethod.shipping_price) +
								(shippingMethod.orderByTime ?
									(shippingMethod.deliveryDay == 'Today' ? '<br>order by <i>' + shippingMethod.orderByTime + '</i> to be delivered <i>' + shippingMethod.deliveryDay + '</i> by <i>' + shippingMethod.deliverByTime + '</i>'
										: '<br> Get it <i>' + shippingMethod.deliveryDay + '</i> by <i>' + shippingMethod.deliverByTime + '</i>')
									: '') +
								'</label>' +
								'</div></td></tr>';
						});

						opc_shippingMethodSet = true;
					}

					if (shipment.shipping_methods_alternative != undefined && shipment.shipping_methods_alternative) {
						var shipping_methods_arr = new Array();

						html += '<tr><td>' + msg.opc_alternative_shipping + '</td></tr>';

						$.each(shipment.shipping_methods_alternative, function (ssid, shippingMethod) {
							shipping_methods_arr.push(shippingMethod);
						});

						shipping_methods_arr.sort(function (obj1, obj2) {
							if (parseInt(obj1.priority) < parseInt(obj2.priority)) return -1;
							if (parseInt(obj1.priority) > parseInt(obj2.priority)) return 1;
							return 0;
						});

						$.each(shipping_methods_arr, function (key, shippingMethod) {
							ssid = shippingMethod.ssid;
							html +=
								'<tr class="field radio"><td><div>' +
								'<div class="input">' +
								'<input type="radio" class="shipment-shipping-method" id="opc-shipping-method-' + shipmentId + '-a' + shippingMethod.ssid + '" data-shipment-id="' + shipmentId + '" name="shipping-method-id[' + shipmentId + ']" value="a' + shippingMethod.ssid + '">' +
								'</div>' +
								'<label for="opc-shipping-method-' + shipmentId + '-a' + shippingMethod.ssid + '">' +
								(shippingMethod.orderByTime && shippingMethod.deliveryDay != 'Today' ? 'Local Delivery' : shippingMethod.method_name) + (opcData.bongoActive && opcData.bongoUsed ? '' : ' - ' + shippingMethod.shipping_price) +
								(shippingMethod.orderByTime ?
									(shippingMethod.deliveryDay == 'Today' ? '<br>order by <i>' + shippingMethod.orderByTime + '</i> to be delivered <i>' + shippingMethod.deliveryDay + '</i> by <i>' + shippingMethod.deliverByTime + '</i>'
										: '<br> Get it <i>' + shippingMethod.deliveryDay + '</i> by <i>' + shippingMethod.deliverByTime + '</i>')
									: '') +
								'</label>' +
								'</div></td></tr>';
						});

						opc_shippingMethodSet = true;
					}

					html += '</tbody></table></div>';
				});
			}

			if (!opc_shippingMethodSet) {
				html = '<div>' + msg.opc_no_shipping_methods_for_order + '</div>';
			}

			html = html + '</div>';


			//		if (shippingMethods.shipping_methods_count > 0 && shippingMethods.shipments) {
			//
			//			html += '<div class="opc-shipments">';
			//
			//			$.each(shippingMethods.shipments, function(shipmentId, shipment){
			//				if (shipment.shipping_methods_count > 0) {
			//					html +=
			//						'<div class="opc-shipment"><table class="fieldset"><tbody>' +
			//						'<tr><td class="opc-shipment-title"><strong>Shipment #' + shipment.num + (shipment.shipping_type == 'local' ? ' - Local delivery' : '' ) + '</strong></td></tr>' +
			//						'<tr class="field radio"><td><div><ul class="opc-shipment-items">';
			//
			//					$.each(shipment.items, function(ocid, shippingItem) {
			//						html += '<li>' +shippingItem.quantity + ' x ' + shippingItem.title + (shippingItem.free_shipping ? ' <i>(free shipping)</i>' : '') + '</li>';
			//					});
			//					html += '</ul></div></td></tr>';
			//
			//					var shipping_methods_arr = new Array();
			//
			//					$.each(shipment.shipping_methods,function(ssid, shippingMethod){
			//						shipping_methods_arr.push(shippingMethod);
			//					});
			//
			//					shipping_methods_arr.sort(function(obj1, obj2) {
			//						if (parseInt(obj1.priority) < parseInt(obj2.priority)) return -1;
			//						if (parseInt(obj1.priority) > parseInt(obj2.priority)) return 1;
			//						return 0;
			//					});
			//
			//					$.each(shipping_methods_arr, function (key, shippingMethod) {
			//						ssid  = shippingMethod.ssid;
			//						html +=html = html + '</div>';

			//						'<tr class="field radio"><td><div>' +
			//							'<div class="input">' +
			//								'<input type="radio" class="shipment-shipping-method" id="opc-shipping-method-' + shipmentId + '-' + shippingMethod.ssid + '" data-shipment-id="' + shipmentId + '" name="shipping-method-id[' + shipmentId + ']" value="' + shippingMethod.ssid + '">' +
			//							'</div>' +
			//							'<label for="opc-shipping-method-' + shipmentId + '-' + shippingMethod.ssid + '">' +
			//								(shippingMethod.orderByTime && shippingMethod.deliveryDay != 'Today' ? 'Local Delivery' : shippingMethod.method_name) + (opcData.bongoActive && opcData.bongoUsed ? '' : ' - ' + shippingMethod.shipping_price) +
			//								(shippingMethod.orderByTime ?
			//									(shippingMethod.deliveryDay == 'Today' ? '<br>order by <i>' + shippingMethod.orderByTime + '</i> to be delivered <i>' + shippingMethod.deliveryDay + '</i> by <i>' + shippingMethod.deliverByTime + '</i>'
			//									: '<br> Get it <i>' + shippingMethod.deliveryDay + '</i> by <i>' + shippingMethod.deliverByTime + '</i>')
			//							: '') +
			//							'</label>' +
			//						'</div></td></tr>';
			//					});
			//
			//					if (shippingMethodsAlternative && shippingMethodsAlternative.shipping_methods_count > 0 && shippingMethodsAlternative.shipments) {
			//						if (shippingMethodsAlternative.shipments[shipmentId] != undefined) {
			//							html += '<tr><td>' + msg.opc_alternative_shipping + '</td></tr>';
			//							$.each(shippingMethodsAlternative.shipments[shipmentId].shipping_methods, function(ssid, shippingMethod) {
			//								html = html +
			//									'<tr class="field radio"><td><div>' +
			//										'<div class="input"><input type="radio" class="shipment-shipping-method" id="opc-shipping-method-a-' + shipmentId + '-' + shippingMethod.ssid + '" data-shipment-id="' + shipmentId + '" name="shipping-method-id[' + shipmentId + ']" value="a' + shippingMethod.ssid + '"/></div>' +
			//										'<label for="opc-shipping-method-a-' + shipmentId + '-' + shippingMethod.ssid + '">' +
			//											shippingMethod.method_name + (opcData.bongoActive && opcData.bongoUsed ? '' : ' - ' + shippingMethod.shipping_price) +
			//										'</label>' +
			//									'</div></td></tr>';
			//							});
			//						}
			//					}
			//
			//					html += '</tbody></table></div>';
			//				}
			//			});var html = '<strong>' + msg.opc_shipping_methods_title + '</strong><div class="gap-top gap-bottom">';
			//			html += '</div>';
			//
			//			opc_shippingMethodSet = true;
			//		}
			//	}
			//

			//}


			/**
			 * Set HTML
			 */
			shippingMethodsFormEle.html(html);

			/**
			 * Set initial shipping method
			 */
			if ($.cookie('opc-shipments-methods') != null && opcData.shipmentsMethods.length == 0) {
				opcData.shipmentsMethods = JSON.parse($.cookie('opc-shipments-methods'));
			}

			$.each(opcData.shipmentsMethods, function(shipmentId, ssid){
				$('#opc-shipping-method-' + shipmentId + '-' + ssid).attr('checked', 'checked');
			});

			/**
			 * Check is current method was selected (are there active radios?)
			 */
			opcData.shipmentsMethods = {};

			if (shippingMethodsFormEle.find("input.shipment-shipping-method").length > 0) {
				shippingMethodsFormEle.find("input.shipment-shipping-method").each(function(){
					var shipmentId = $(this).attr('data-shipment-id');

					if (shippingMethodsFormEle.find("input[name='shipping-method-id[" + shipmentId + "]']:checked").length == 0) {
						shippingMethodsFormEle.find("input[name='shipping-method-id[" + shipmentId + "]']:first").attr("checked", "checked");
					}
				});

				shippingMethodsFormEle.find("input.shipment-shipping-method:checked").each(function(){
					var shipmentId = $(this).attr('data-shipment-id');
					var shippingMethodId = $(this).val();
					opcData.shipmentsMethods[shipmentId.toString()] = shippingMethodId;
				});
			}

			$.cookie('opc-shipments-methods', JSON.stringify(opcData.shipmentsMethods));

			/**
			 * Handle new shipping method selection
			 */
			shippingMethodsFormEle.find("input.shipment-shipping-method").each(function(){
				$(this).change(function() {
					opcData.shipmentsMethods[$(this).attr('data-shipment-id')] = $(this).val();
					setShippingMethod();
				});
			});

			$.cookie('opc-shipments-methods', JSON.stringify(opcData.shipmentsMethods));
		} catch (err) {
			alert(err);
		}
	};

	ensureShippingSet = function(callback) {
		if (!opc_loadedShippingMethods) {
			opc_shippingMethodSet = false;
			if (isShippingAddressSet()) {
				getShippingMethods(callback);
			} else {
				if (callback != undefined && callback != null) {
					callback();
				}
			}
		} else {
			setShippingMethod(callback);
		}
	};

	setShippingMethod = function(callback) {
		try {
			$.cookie('opc-shipments-methods', JSON.stringify(opcData.shipmentsMethods));
			onePageCheckout.setCompleteButton();
			onePageCheckout.sendRequest(
				"setShippingMethod",
				{
					'shipmentsMethods' : JSON.stringify(opcData.shipmentsMethods)
				},
				function (response) {
					if (response.result) {
						onePageCheckout.setCompleteButton();

						if (callback != undefined && callback != null) {
							callback();
						}
					}
				}
			);
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Show shipping section
	 */
	showShippingSection = function() {
		if (opcData.shippingRequired) {
			$("#opc-shipping-inner").slideDown('fast');
			$("#opc-shipping").find("h3:first").removeClass('light');
		}
	};

	/**
	 * Hide shipping section
	 */
	hideShippingSection = function() {
		$("#opc-shipping-inner").slideUp('fast');
		$("#opc-shipping").find("h3:first").addClass('light');
	};

	isCompleted = function() {
		return !opcData.shippingRequired || (opc_shippingAddressSet && opc_shippingMethodSet && opc_loadedShippingMethods);
	};

	setShippingAddressSet = function(value) {
		opc_shippingAddressSet = value;
	};

	isShippingAddressSet = function() {
		return opc_shippingAddressSet;
	};

	return {
		initModule: initModule,
		setShippingAddressView: setShippingAddressView,
		setShippingAddressVariants: setShippingAddressVariants,
		editShippingAddress: editShippingAddress,
		showShippingSection: showShippingSection,
		hideShippingSection: hideShippingSection,
		setShippingMethodsForm: setShippingMethodsForm,
		isCompleted: isCompleted,
		setShippingAddressSet: setShippingAddressSet,
		isShippingAddressSet: isShippingAddressSet,
		ensureShippingSet: ensureShippingSet
	};
}(jQuery));
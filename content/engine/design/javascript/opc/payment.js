var
	opc_paymentFormValidationAsync = false,
	opc_paymentFormValidationAsyncFinished = false,
	opc_paymentFormValidationAsyncResult = false
;

var opc_payment = (function($) {
	var initModule,
		initPayment,
		showPaymentError,
		hidePaymentError,
		setPaymentMethods,
		getPaymentForm,
		setGiftCertificate,
		applyGiftCertificate,
		processPayment,
		sendPaymentRequest,
		setPromoCodeForm,
		showPaymentSection,
		hidePaymentSection,
		isCompleted,
		setPaymentFormSet,
		setGiftCertificateSet,
		setPaymentMethodsSet
	;

	var opc_paymentFormSet = false,
		opc_giftCertificateSet = true,
		opc_auto_show_popup = false,
		opc_paymentMethodsSet = false
	;

	initModule = function() {
	};

	initPayment = function() {
		setPromoCodeForm();

		if (opc_billing.isCompleted() && opc_shipping.isCompleted()) {
			setPaymentMethods();
			/**
			 * Check "stoppers"
			 */
			if (opcData.paymentMethodsCount < 1) {
				showPaymentError(msg.opc_error_no_payment_methods);
			}
		}

		onePageCheckout.setCompleteButton();

		onePageCheckout.setInvoicePreview(); // or wait until shipping methods returned
		onePageCheckout.hideSpinner();
	};

	/**
	 * Show payment error
	 * @param errorMessage
	 */
	showPaymentError = function(errorMessage) {
		try {
			$('#opc-payment-methods-error').html(errorMessage).show();
			$('#opc-payment-inner').show();
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Hide payment error
	 */
	hidePaymentError = function() {
		$('#opc-payment-methods-error').hide();
	};

	/**
	 *
	 * @return
	 */
	setPaymentMethods = function() {
		if (!opc_billing.isCompleted() || !opc_shipping.isCompleted()) return;

		try {
			hidePaymentError();

			onePageCheckout.sendRequest(
				"getPaymentMethods",
				{},
				function (response) {
					if (response.result) {
						if (response.data.paymentMethods != undefined && response.data.paymentMethods != null && response.data.paymentMethods) {
							opc_paymentMethodsSet = true;
							opcData.paymentMethods = response.data.paymentMethods;
							opcData.paymentMethodsCount = response.data.paymentMethodsCount;

							if (opcData.paymentMethodsCount > 0) {
								/**
								 * Add payment methods
								 */
								var html = '<div id="opc-payment-methods-list"><table class="fieldset"><tbody>';
								$.each(opcData.paymentMethods, function(key, paymentMethod) {
									if ((!onePageCheckout.isBongoOnly() && paymentMethod.code != 'bongocheckout') || (onePageCheckout.isBongoOnly() && paymentMethod.code == 'bongocheckout'))
									{
										html = html +
											'<tr class="field radio"><td><div>' +
											'<div class="input"><input class="payment-method-' + paymentMethod.code + '" type="radio" id="opc-payment-method-' + paymentMethod.id + '" name="payment-method-id" value="' + paymentMethod.id + '"/></div>' +
											'<label for="opc-payment-method-' + paymentMethod.id + '">' + paymentMethod.title + '</label>' +
											'</div></td></tr>';
									}
								});

								/**
								 * Add gift certificate radio
								 */
								if (opcData.giftCertificateEnabled && !opcData.orderData.hasRecurringBillingItems && !opcData.OPCLight) {
									/**
									 * Standard site
									 */
									html = html +
										'<tr class="field radio"><td><div>' +
										'<div class="input"><input type="radio" id="opc-payment-method-gift-certificate" value="gift-certificate" name="payment-method-id"/></div>' +
										'<label for="opc-payment-method-gift-certificate">' + msg.opc_use_gift_certificate + '</label>' +
										'</div></td></tr>';

									$("#opc-gift-certificate-form").on('submit', function() {
										return false;
									});
								}

								html = html + '</tbody></table></div>';

								$("#opc-payment-inner").slideDown('fast');
								$("#opc-payment").find("h3:first").removeClass('light');

								$("#opc-payment-methods-options").html(html);

								var paymentMethodsFormContainerEle = $("#opc-payment-methods-form");
								paymentMethodsFormContainerEle.show();

								/**
								 * Preselect payment method
								 */
								if ($.cookie('opc-payment-method-id') != null && opcData.paymentMethodId == '0') opcData.paymentMethodId = $.cookie('opc-payment-method-id');

								if (opcData.paymentMethodId != '0') {
									var paymentMethodInputEle = $("#opc-payment-method-" + opcData.paymentMethodId);
									if (paymentMethodInputEle.length > 0) {
										paymentMethodInputEle.attr("checked", "checked");
										getPaymentForm();
									} else {
										paymentMethodsFormContainerEle.find("input[name='payment-method-id']:first").attr("checked", "checked");
										opcData.paymentMethodId = paymentMethodsFormContainerEle.find("input[name='payment-method-id']:first").val();
										$.cookie('opc-payment-method-id', opcData.paymentMethodId);
										getPaymentForm();
									}
								} else {
									paymentMethodsFormContainerEle.find("input[name='payment-method-id']:first").attr("checked", "checked");
									opcData.paymentMethodId = paymentMethodsFormContainerEle.find("input[name='payment-method-id']:first").val();
									$.cookie('opc-payment-method-id', opcData.paymentMethodId);
									getPaymentForm();
								}

								/**
								 * Handle payment methods radio buttons click
								 */
								paymentMethodsFormContainerEle.find("input[name='payment-method-id']").each(function() {
									$(this).change(function(){
										opc_auto_show_popup = $(this).is('.payment-method-paypaladv');

										$("#opc-payment-method-error").hide();

										if ($(this).val() == 'gift-certificate') {
											opc_giftCertificateSet = false;
											$("#opc-payment-method-form").hide();
											$("#opc-gift-certificate-form").show();
											$("#opc-gift-certificate-view").hide();
											$("#opc-button-complete").attr('disabled', 'disabled');
										} else {
											$("#opc-gift-certificate-form").hide();
											$("#opc-gift-certificate-view").hide();

											opc_giftCertificateSet = true;

											if ($(this).val() == opcData.paymentMethodId) {
												opc_paymentFormSet = true;
												$("#opc-payment-method-form").show();
												onePageCheckout.setCompleteButton();
											} else {
												opcData.paymentMethodId = $(this).val();
												$.cookie('opc-payment-method-id', opcData.paymentMethodId);

												// TODO: ensure that there are no conditions when it may fail here
												opc_billing.setIsCompleted(true);
												getPaymentForm();
											}
										}
									});
								});

								/**
								 * Gift certificate apply button handler
								 */
								if (opcData.giftCertificateEnabled) {
									$("#opc-gift-certificate-apply").unbind('click').click(function() {
										applyGiftCertificate();
									});
								}
							} else {
								$("#opc-payment-methods").hide();
								$("#opc-payment-methods-options").html('');

								showPaymentError(msg.opc_error_no_payment_methods);
							}
						} else {
							$("#opc-payment-methods").hide();
							$("#opc-payment-methods-options").html('');

							showPaymentError(msg.opc_error_no_payment_methods);
						}
					} else {
						$("#opc-payment-methods").hide();
						$("#opc-payment-methods-options").html('');

						showPaymentError(msg.opc_error_no_payment_methods);
					}
				}
			);
		}
		catch (err)
		{
			alert(err);
		}
	};

	/**
	 * Get payment form
	 *
	 * @return
	 */
	getPaymentForm = function() {
		if (!(opc_billing.isCompleted() && opc_shipping.isCompleted())) {
			opcData.paymentError = false;
			$("#opc-payment-method-form").html('');
			return;
		}

		if (!opc_paymentMethodsSet) {
			setPaymentMethods();
		}
		try {
			if ($('#opc-payment-methods-form').find('input[name="payment-method-id"]').length < 1) {
				return;
			}

			var paymentMethodsFormData = $.trim($("#opc-payment-methods-form").serialize());

			if (paymentMethodsFormData == '') return;

			opc_paymentFormSet = false;

			onePageCheckout.setCompleteButton();

			onePageCheckout.sendRequest(
				"getPaymentForm",
				{
					'paymentMethodsForm' : paymentMethodsFormData,
					'paymentProfileId' : opcData.paymentProfileId
				},
				function (response) {
					if (response.result) {
						opcData.paymentFormPopUp = response.data.paymentFormPopUp;
						opcData.paymentFormPopUpWidth = response.data.paymentFormPopUpWidth;
						opcData.paymentFormPopUpHeight = response.data.paymentFormPopUpHeight;
						opcData.paymentFormPopUpTitle = response.data.paymentFormPopUpTitle;

						if ($('#opc-payment-method-form').length == 0) {
							$('<form id="opc-payment-method-form" method="POST" class="clearfix invisible"></form>').insertAfter('#opc-gift-certificate-view');
						}

						var paymentMethodFormEle = $("#opc-payment-method-form");
						paymentMethodFormEle.attr('action', response.data.paymentFormUrl);

						if (opcData.paymentFormPopUp) {
							paymentMethodFormEle.attr('target', 'opc-payment-dialog-iframe');
						} else {
							paymentMethodFormEle.attr('target', '');
						}

						if (response.data.paymentForm != undefined && response.data.paymentForm != null && response.data.paymentForm) {
							paymentMethodFormEle
								.html(response.data.paymentForm)
								.show();

							if (response.data.paymentFormWide != undefined && response.data.paymentFormWide != null && response.data.paymentFormWide) {
								$("#opc-payment-methods").find("div.col-50").removeClass('col-50').addClass('col-100');
							} else {
								$("#opc-payment-methods").find("div.col-100").removeClass('col-100').addClass('col-50');
							}

							if (response.data.paymentFormOverride != undefined && response.data.paymentFormOverride != null && response.data.paymentFormOverride) {
								$("#opc-button-complete").hide();
							} else {
								$("#opc-button-complete").show();
							}
						} else {
							paymentMethodFormEle.html('');
						}

						if (response.data.paymentFormValidatorJS != undefined && response.data.paymentFormValidatorJS != null && response.data.paymentFormValidatorJS) {
							opcData.paymentFormValidatorJS = response.data.paymentFormValidatorJS;
						} else {
							opcData.paymentFormValidatorJS = '';
						}

						if (response.data.paymentFormJS != undefined && response.data.paymentFormJS != null && response.data.paymentFormJS) {
							opcData.paymentFormJS = response.data.paymentFormJS;
						} else {
							opcData.paymentFormJS = '';
						}

						opc_paymentFormSet = true;

						/**
						 * Show payment error (if exists)
						 */
						if (opcData.paymentError) {
							$("#opc-payment-method-error-message").html(opcData.paymentError);
							$("#opc-payment-method-error").slideDown('fast');
							opcData.paymentError = false;
						}

						$("#opc-payment-use-profile").click();

						/**
						 * Handle profile change
						 */
						if (paymentMethodFormEle.find("input[name='form[payment_method_way]']").length > 0) {
							paymentMethodFormEle.find("input[name='form[payment_method_way]']").each(function() {
								$(this).change(function(){
									opcData.paymentMethodWay = $(this).val();
									if (opcData.paymentMethodWay == "card") {
										$("#opc-payment-method-form-profile").hide();
										$("#opc-payment-method-form-card").show();
									} else {
										$("#opc-payment-method-form-card").hide();
										$("#opc-payment-method-form-profile").show();
									}
								});
							});
						} else {
							opcData.paymentMethodWay = 'card';
							$("#opc-payment-method-form-profile").hide();
							$("#opc-payment-method-form-card").show();
						}

						/**
						 * Set gift certificates
						 */
						setGiftCertificate();

						onePageCheckout.setCompleteButton();

						if (opc_auto_show_popup) {
							$('#opc-button-complete:not(:disabled)').click();
						}

						if (opcData.paymentFormJS != '') {
							$("#opc-payment-form-validator").html(opcData.paymentFormJS);
						}
					}
				},
				msg.opc_getting_payment_form
			);
			onePageCheckout.setCompleteButton();
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Presets session-stored gift certificate
	 */
	setGiftCertificate = function() {
		if (opcData.giftCertificate != undefined && opcData.giftCertificate != null && opcData.giftCertificate) {
			$("#gift_cert_first_name").val(opcData.giftCertificate.first_name);
			$("#gift_cert_last_name").val(opcData.giftCertificate.last_name);
			$("#gift_cert_voucher").val(opcData.giftCertificate.voucher);
		}
	};

	/**
	 * Apply gift certificate
	 */
	applyGiftCertificate = function() {
		try {
			onePageCheckout.sendRequest(
				'applyGiftCertificate',
				{
					'giftCertificateForm' : $("#opc-gift-certificate-form").serialize()
				},
				function (response) {
					if (response.result) {
						$("#opc-gift-certificate-form").hide();
						$("#opc-gift-certificate-view").show();
						opc_giftCertificateSet = true;
					} else {
						opc_giftCertificateSet = false;
					}

					onePageCheckout.setInvoicePreview();
					onePageCheckout.setCompleteButton();
				},
				msg.opc_checking_gift_certificate
			);
		} catch (err) {
			alert(err);
		}
	};

	BrainTreeCardTokenization = function(_paymentMethodFormEle, paymentMethodType, formUsed){
		var cur_nonce = $(_paymentMethodFormEle).find("input[name='payment_method_nonce']").val();
		//console.log('BrainTreeCardTokenization  cur_nonce   '+cur_nonce);
		if (cur_nonce == ''){
			braintreeclient = new braintree.api.Client({
				clientToken: bt_token
			});
			if (braintreeclient){
					var _number =  $(_paymentMethodFormEle).find("#payment_form_field_cc_number").val();
					var _cardholderName =  $(_paymentMethodFormEle).find("#payment_form_field_cc_first_name").val()+' '+$(_paymentMethodFormEle).find("#payment_form_field_cc_last_name").val();
					var _expirationMonth =  $(_paymentMethodFormEle).find("#payment_form_field_cc_expiration_month").val();
					var _expirationYear =  $(_paymentMethodFormEle).find("#payment_form_field_cc_expiration_year").val();
				//console.log('BrainTreeCardTokenization  number   '+_number);
				//console.log('BrainTreeCardTokenization  cardholderName   '+_cardholderName);
				//console.log('BrainTreeCardTokenization  _expirationMonth   '+_expirationMonth);
				//console.log('BrainTreeCardTokenization  _expirationYear   '+_expirationYear);
				braintreeclient.tokenizeCard({
					number: _number,
					cardholderName: _cardholderName,
					expirationMonth: _expirationMonth,
					expirationYear: _expirationYear
				}, function (err, nonce) {
					if (err){
						//console.log('BrainTreeCardTokenization  err   '+err);
						onePageCheckout.hideSpinner();
						alert(err);
						return false;
					}
					else{
						//console.log('BrainTreeCardTokenization  nonce   '+nonce);
						$(_paymentMethodFormEle).find("input[name='payment_method_nonce']").val(nonce);
						cur_nonce = $(_paymentMethodFormEle).find("input[name='payment_method_nonce']").val();
						//console.log('BrainTreeCardTokenization  nonce   '+cur_nonce);
						sendPaymentRequest(paymentMethodType, formUsed);
					}
				});
			}
			else{
				onePageCheckout.hideSpinner();
				alert(' Braintree client init failed');
				return false;
			}
		}
		else{
			sendPaymentRequest(paymentMethodType, formUsed);
		}
	};

	/**
	 *  Process payment
	 */
	processPayment = function() {
		try {
			$("#opc-button-complete").attr('disabled', 'disabled').addClass('button-disabled');
			onePageCheckout.showSpinner(msg.opc_validating_data);

			$("#opc-payment-method-error").slideUp('fast');

			var paymentMethodType = 'free';
			var formUsed = '-free';

			/**
			 * Check is this a free order
			 */
			if (opcData.orderData.totalRemainingAmount > 0 || opcData.orderData.hasRecurringBillingItems) {
				var paymentMethod = opcData.paymentMethods['p' + opcData.paymentMethodId];
				paymentMethodType = paymentMethod.type;
				formUsed = '';

				var paymentMethodFormEle = $("#opc-payment-method-form");
				if (paymentMethodFormEle.find("input[name='form[payment_method_way]']").length > 0 &&
					paymentMethodFormEle.find("input[name='form[payment_method_way]']").val() == 'profile') {
					sendPaymentRequest(paymentMethodType, formUsed);
				}
				else if (paymentMethodFormEle.find("input[name='form[payment_method_pp]']").length > 0 &&
					paymentMethodFormEle.find("input[name='form[payment_method_pp]']").val() == 'profile') {
					sendPaymentRequest(paymentMethodType, formUsed);
				}
				else {
					/**
					 * Validate payment form
					 */
					var html =
						'<script type="text/javascript">\nfunction _checkPaymentForm(){\n' +
							'var frm=document.getElementById("opc-payment-method-form");\n' +
							(opcData.paymentFormValidatorJS != '' ? opcData.paymentFormValidatorJS : '') + '\n' +
							'//	additionally check is there term & conditions checkbox \n' +
							'if ($("#cb_agree").length && !$("#cb_agree").is(":checked")) \n' +
							'{ alert(msg.opc_agree_to_terms_and_conditions); return false; } \n' +
							'return true;\n' +
							'}\n' +
							'</script>';

					$("#opc-payment-form-validator").html(html);

					opc_paymentFormValidationAsync = false;
					opc_paymentFormValidationAsyncFinished = false;
					opc_paymentFormValidationAsyncResult = false;

					if (_checkPaymentForm()) {
						if (opc_paymentFormValidationAsync) {
							return false;
						} else {
							if(paymentMethod.code == 'braintree'){
								BrainTreeCardTokenization(paymentMethodFormEle, paymentMethodType, formUsed);
							}
							else{
								sendPaymentRequest(paymentMethodType, formUsed);
							}
						}
					} else {
						onePageCheckout.setCompleteButton();
						onePageCheckout.hideSpinner();
					}
				}
			} else {
				paymentMethodType ='free';
				sendPaymentRequest(paymentMethodType, formUsed);
			}
		} catch (err) {
			onePageCheckout.hideSpinner();
			alert(err);
		}
	};

	/**
	 * Send payment request
	 */
	sendPaymentRequest = function(paymentMethodType, formUsed) {
		var additionalEle = $('#opc-additional');
		onePageCheckout.sendRequest(
			'processPayment',
			{
				'paymentMethodsForm' : $("#opc-payment-methods-form" + formUsed).serialize(),
				'paymentMethodForm' : $("#opc-payment-method-form" + formUsed).serialize(),
				'paymentMethodType' : paymentMethodType,
				'shippingMethodId' : opcData.shippingMethodId,
				'giftCertificateForm' : opcData.orderData.giftCertificateAmount > 0 ? $("#opc-gift-certificate-form").serialize() : false,
				'additionalInformation' : additionalEle.find("form").length > 0 ? additionalEle.find("form").serialize() : false
			},
			function (response)
			{
				if (response.result) {
					// Handling IPN forms
					if (response.data.paymentForm != undefined && response.data.paymentForm != null && response.data.paymentForm) {
						// Save user input (if any)
						var formData = [];

						var paymentMethodFormSelector = "#opc-payment-method-form input[type!='hidden'],#opc-payment-method-form select,#opc-payment-method-form textarea";
						$(paymentMethodFormSelector).each(function() {
							formData[$(this).attr('name')] = $(this).attr('type') == 'checkbox' ? $(this).is(':checked') : $(this).val();
						});

						var paymentMethodFormEle = $("#opc-payment-method-form");

						paymentMethodFormEle.attr('action', response.data.paymentFormUrl)
							.html(response.data.paymentForm);

						$(paymentMethodFormSelector).each(function() {
							if ($(this).attr('type') == 'checkbox') {
								if (formData[$(this).attr('name')]) $(this).attr('checked', 'checked'); else $(this).removeAttr('checked');
							} else {
								$(this).val(formData[$(this).attr('name')]);
							}
						});

						if (opcData.paymentFormPopUp) {
							onePageCheckout.hideSpinner();
							$('#opc-payment-dialog').dialog({
								width: opcData.paymentFormPopUpWidth,
								height: opcData.paymentFormPopUpHeight,
								title: opcData.paymentFormPopUpTitle,
								modal: true,
								open: function() {
									var paymentDialogEle = $("#opc-payment-dialog");
									paymentDialogEle.html(
										'<iframe style="' +
											$('#opc-payment-dialog-iframe').attr('style') +
											'" id="opc-payment-dialog-iframe" src="about:blank" name="opc-payment-dialog-iframe"></iframe>'
									);
									paymentDialogEle.contents().find('body').html(msg.opc_redirect_to_payment_gateway + '...');
								},
								close: function() {
									onePageCheckout.setCompleteButton();
								}
							});
						} else {
							onePageCheckout.showSpinnerDelayed(msg.opc_redirect_to_payment_gateway)
						}
						paymentMethodFormEle.submit();
					} else {
						// Processing real time, free and custom
						if (response.data.verification != undefined && response.data.verification != null && response.data.verification) {
							onePageCheckout.showSpinnerDelayed(msg.opc_verification);
							document.location = response.data.backURL;
						} else if (response.data.success != undefined && response.data.success != null) {
							// Check for success
							if (response.data.success) {
								onePageCheckout.showSpinnerDelayed(msg.opc_finishing_order);
								if (response.data.backURL != undefined && response.data.backURL != null && response.data.backURL) {
									document.location = response.data.backURL;
								}
							} else {
								var paymentMethodFormEle = $("#opc-payment-method-form");
								if (paymentMethodFormEle.find("input[name='form[payment_method_pp]']").length > 0) {
									paymentMethodFormEle.find("input[name='payment_method_nonce']").val('');
								}
								// Show error message
								if (response.data.backURL != undefined && response.data.backURL != null && response.data.backURL) {
									document.location = response.data.backURL;
									return;
								} else if (response.data.paymentErrors != undefined && response.data.paymentErrors != null && response.data.paymentErrors) {
									$("#opc-payment-method-error-message").html(response.data.paymentErrors);
									$("#opc-payment-method-error").slideDown('fast');
								}

								onePageCheckout.setCompleteButton();
							}
						}
					}
				}
			},
			msg.opc_processing_payment
		);
	};

	/**
	 * Set promo code form
	 */
	setPromoCodeForm = function() {
		try {
			if (opcData.promoAvailable) {
				$("#opc-promo-code").val(opcData.promoCode)
					.unbind('keypress').keypress(function(e) {
						if (e.which == 13) {
							$("#opc-promo-code-apply").click();
						}
					})
				;
				$("#opc-promo-code-apply").unbind('click').click(function(e) {
					opcData.promoCode = $("#opc-promo-code").val();
					e.stopPropagation();
					$("#opc-button-complete").attr("disabled", "disabled");
					onePageCheckout.sendRequest(
						'checkPromoCode',
						{
							'promo' : opcData.promoCode
						},
						function (response) {
							if (!response.result) $("#opc-promo-code").focus();

							if (opcData.shippingRequired) {
								opc_shipping.setShippingMethodsForm(opcData.shipments);
								// Set current method as active
								opc_shipping.ensureShippingSet(function() {
									setPaymentMethods();
									onePageCheckout.setInvoicePreview();
									onePageCheckout.setCompleteButton();
								});
							} else {
								setPaymentMethods();
								onePageCheckout.setInvoicePreview();
								onePageCheckout.setCompleteButton();
							}
						},
						msg.opc_checking_promo_code
					);
				});
			}
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Show payment section
	 */
	showPaymentSection = function()
	{
		if (!opc_paymentMethodsSet) {
			setPaymentMethods();
		}

		$("#opc-payment-inner").slideDown('fast');
		$("#opc-payment").find("h3:first").removeClass('light');

		onePageCheckout.setCompleteButton();
	};

	/**
	 * Hide payment section
	 */
	hidePaymentSection = function() {
		$("#opc-payment-inner").slideUp('fast');
		$("#opc-payment").find("h3:first").addClass('light');
	};

	isCompleted = function() {
		return opc_paymentFormSet && opc_giftCertificateSet;
	};

	setPaymentFormSet = function(value) {
		opc_paymentFormSet = value;
	};

	setGiftCertificateSet = function(value) {
		opc_giftCertificateSet = value;
	};

	setPaymentMethodsSet = function(value) {
		opc_paymentMethodsSet = value;
	};

	return {
		initModule: initModule,
		initPayment: initPayment,
		showPaymentError: showPaymentError,
		hidePaymentError: hidePaymentError,
		setPaymentMethods: setPaymentMethods,
		getPaymentForm: getPaymentForm,
		processPayment: processPayment,
		showPaymentSection: showPaymentSection,
		hidePaymentSection: hidePaymentSection,
		isCompleted: isCompleted,
		setPaymentFormSet: setPaymentFormSet,
		setGiftCertificateSet: setGiftCertificateSet,
		setPaymentMethodsSet: setPaymentMethodsSet,
		sendPaymentRequest: sendPaymentRequest
	};
}(jQuery));
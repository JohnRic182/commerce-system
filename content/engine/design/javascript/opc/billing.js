var opc_billing = (function($) {
	var initModule,
		initAccount,
		initLogin,
		initPaymentProfiles,
		doLogin,
		setBillingForm,
		setBillingView,
		editBillingData,
		saveBillingData,
		setPaymentProfiles,
		copyBillingToShipping,
		isCompleted
	;

	var opc_billingDataSet = false;

	initModule = function() {
		initAccount();
		initLogin();

		/**
		 * Init billing form & view
		 */
		setBillingForm(opcData.billingData);
		setBillingView(opcData.billingData);

		$('#opc-additional-inner').find('form').submit(function(){
			return false;
		});

		initPaymentProfiles();

		$("#opc-billing-form-button-save").click(function(){
			saveBillingData();
			return false;
		});

		if (opcData.paymentProfiles != undefined && opcData.paymentProfiles) {
			editBillingData();
		} else if (!opcData.billingIsEmpty) {
			$("#opc-additional-inner").show();
			$("#opc-additional").find("h3:first").removeClass('light');

			opc_shipping.showShippingSection();
		} else {
			opc_payment.hidePaymentSection();
		}

		$('#opc-link-copy-billing-to-shipping').click(function(e) {
			e.preventDefault();
			copyBillingToShipping();
			return false;
		});

		var addGiftMessageEle = $('#opc-add-gift-message');
		addGiftMessageEle.change(function(){
			if ($('#opc-add-gift-message').is(':checked'))
			{
				$("#opc-gift-message").slideDown('fast');
			}
			else
			{
				$("#opc-gift-message").slideUp('fast');
			}
		});

		$('#opc-gift-message')
			.toggle(addGiftMessageEle.is(':checked'))
			.find('textarea')
			.bind('keyup change', function(event){
				event.stopPropagation();
				var s = $(this).val();
				if (s.length > opcData.giftMessageLength)
				{
					$(this).val(s.substr(0, opcData.giftMessageLength));
				}
			})
			.autoResize({
				// On resize:
				onResize : function() {
					$(this).css({opacity:0.8});
				},
				// After resize:
				animateCallback : function() {
					$(this).css({opacity:1});
				},
				// Quite slow animation:
				animateDuration : 300,
				// More extra space:
				extraSpace : 40
			});
	};

	initAccount = function() {
		var createAccountEle = $('#cb-create-account');
		if (createAccountEle.length > 0) {
			// correction for slide down - works better this way
			var billingFormContainerEle = $('#opc-billing-form');
			billingFormContainerEle.show();
			var accountInnerContainerEle = $('#opc-account-inner');
			accountInnerContainerEle
				.show()
				.css('height', accountInnerContainerEle.height() + 10);
			billingFormContainerEle.hide();
			accountInnerContainerEle.hide();

			createAccountEle.change(function() {
				if ($(this).is(":checked")) {
					$("#opc-account-inner").slideDown('fast');
				} else {
					$("#opc-account-inner").slideUp('fast');
				}
			});
		}
	};

	initLogin = function() {
		var loginDialogEle = $("#opc-dialog-login");
		/**
		 * Init login dialog
		 */
		loginDialogEle.data('counter', 0).dialog({
			width:370,
			height:220,
			modal:true,
			autoOpen:false,
			resizable:false,
			closeOnEscape:true,
			draggable:false,
			open: function() {
				$("#opc-dialog-login").find(".button-login").attr('default', 'default');
			}
		});

		loginDialogEle.parent().find('.ui-dialog-titlebar').remove();

		$("#opc-form-login").submit(function(){
			doLogin();
			return false;
		});

		$("#opc-link-login").click(function() {
			$("#opc-dialog-login").dialog('open');
			return false;
		});

		loginDialogEle.find(".button-close").click(function() {
			$("#opc-dialog-login").dialog('close');
		});
	};

	initPaymentProfiles = function() {
		if (opcData.paymentProfiles != undefined && opcData.paymentProfiles && opcData.paymentProfileMethodId != 'billing') {
			opcData.paymentMethodId = opcData.paymentProfileMethodId;
			$.cookie('opc-payment-method-id', opcData.paymentMethodId);
		}

		if (opcData.billingIsEmpty || (opcData.paymentProfiles != undefined && opcData.paymentProfiles)) {
			// In normal conditions will be empty only for user checkout without account
			$("#opc-billing-view").hide();
			$("#opc-billing-form")
				.show()
				.data("changed", true)
			;
			$("#opc-shipping-address-form").find(".buttons").hide();

			// In this case we also have to hide everything under billing form to do not confuse user
			$("#opc-additional-inner").hide();
			$("#opc-additional").find("h3:first").addClass('light');

			opc_shipping.hideShippingSection();
			opc_payment.hidePaymentSection();
		}  else {
			$("#opc-billing-form").hide();
			$("#opc-billing-view").show();

			opc_billingDataSet = true;
		}

		/**
		 * Payment Profiles
		 */
		setPaymentProfiles();
	};

	/**
	 * Show login form and process login action
	 * @return
	 */
	doLogin = function() {
		try {
			var loginDialogEle = $("#opc-dialog-login");
			onePageCheckout.sendRequest(
				"doLogin",
				{
					'username' : loginDialogEle.find(".login-username").val(),
					'password' : loginDialogEle.find(".login-password").val()
				},
				function (response) {
					if (response.result) {
						$("#opc-dialog-login").dialog('close');
						onePageCheckout.showSpinnerDelayed(msg.opc_login_successfull);
						document.location = document.location;
					} else {
						$(".login-username").focus();
					}
				},
				msg.opc_checking_credentials
			);
		} catch (err) {
			alert(err);
		}

		return false;
	};

	/**
	 * Sets billing address from array into form
	 * @param billingData
	 */
	setBillingForm = function(billingData) {
		try {
			// Set form
			$.each(billingData, function(fieldName, fieldValue){
				$("#opc-billing-form-" + fieldName)
					.val(fieldValue)
					.change(function(){
						$("#opc-billing-form").data("changed", true);
						opc_billingDataSet = false;
						opc_payment.setPaymentMethodsSet(false);
						onePageCheckout.setCompleteButton();
					});
			});

			// Check custom fields changes
			$("#opc-billing-form .custom-field, #cb-create-account").each(function(){
				$(this).change(function(){
					$("#opc-billing-form").data("changed", true);
					opc_billingDataSet = false;
					opc_payment.setPaymentMethodsSet(false);
					onePageCheckout.setCompleteButton();
				});
			});

			// Set countries and states
			setCountriesStates(
				"#field-billing-country", billingData.country,
				"#field-billing-state", billingData.state,
				"#field-billing-province", billingData.province,
				countriesStates
			);

			$("#opc-billing-form").data("changed", false);
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Sets billing address from array to view
	 * @param billingData
	 */
	setBillingView = function(billingData) {
		try {
			var htmlAddress =
				escapeHtml(billingData.fname) + ' ' + escapeHtml(billingData.lname) + '<br/>' +
					($.trim(billingData.company) != '' ? escapeHtml(billingData.company) + '<br/>' : '') +
					escapeHtml(billingData.address1) + '<br/>' +
					($.trim(billingData.address2) != '' ? escapeHtml(billingData.address2) + '<br/>' : '') +
					escapeHtml(billingData.city) + ', ' + (billingData.state > 0 ? escapeHtml(billingData.state_name) + ', '  : (billingData.province != '') ? escapeHtml(billingData.province) + ', ' : '')  + escapeHtml(billingData.zip) + '<br/>' +
					escapeHtml(billingData.country_name);

			var editLink = '<div class="gap-top gap-bottom"><a href="#" id="opc-billing-edit-link">' + msg.opc_edit_billing_address + '</a></div>';

			var htmlContacts =
				(billingData.phone != "" ? msg.opc_phone + ': ' + escapeHtml(billingData.phone) + '<br/>' : '') +
					escapeHtml(billingData.email);

			$("#opc-billing-view-address").html(htmlAddress + editLink);
			$("#opc-billing-view-contacts").html(htmlContacts);
			$("#opc-billing-edit-link").click(function() {
				editBillingData();
				return false;
			});
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Edit billing information
	 */
	editBillingData = function() {
		try {
			if (!opcData.paymentProfiles) {
				$("#opc-billing-form-inner").show();
			}

			$("#opc-billing-view").slideUp('fast');
			$("#opc-billing-form").slideDown('fast');

			opc_shipping.hideShippingSection();
			opc_payment.hidePaymentSection();
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Validates / saves billing information
	 */
	saveBillingData = function() {
		try
		{
			var billingFormContainerEle = $("#opc-billing-form");
			var revalidateForm = billingFormContainerEle.find(".required .custom-field").length > 0;

			// Check are there changes or form must be revalidated
			if (billingFormContainerEle.data("changed") || revalidateForm)
			{
				opc_billingDataSet = false;
				opc_payment.setPaymentMethodsSet(false);

				// Check is shipping address the same as billing
				var shippingAsBilling = $("#opc-shipping-address-id-billing").is(":checked");

				// Save billing data
				onePageCheckout.sendRequest(
					"saveBillingData",
					{
						'billingForm' : billingFormContainerEle.find("form").serialize(),
						'shippingAsBilling' : shippingAsBilling
					},
					function (response) {
						if (response.result) {
							// Set Bongo status
							if (response.data.bongoActive != undefined && response.data.bongoActive != null) opcData.bongoActive = response.data.bongoActive;
							if (response.data.bongoUsed != undefined && response.data.bongoUsed != null) opcData.bongoUsed = response.data.bongoUsed;

							// Check are there address variants
							if (response.data.shippingAddressVariants != null && response.data.shippingAddressVariants) {
								opcData.shippingAddressVariants = response.data.shippingAddressVariants;
								opc_shipping.setShippingAddressVariants(opcData.shippingAddressVariants, true);
								return;
							}

							// Check if we got back possibly corrected billing data
							if (response.data.billingData != undefined && response.data.billingData != null && response.data.billingData) {
								opcData.billingData = response.data.billingData;
								setBillingForm(opcData.billingData);
								setBillingView(opcData.billingData);
							}

							// Process response - billing
							billingFormContainerEle.slideUp('fast');
							$("#opc-billing-view").slideDown('fast');

							billingFormContainerEle.data("changed", false);
							opc_billingDataSet = true;
							onePageCheckout.setBongoOnly(false);

							// Process response - shipping
							$("#opc-additional-inner").slideDown('fast');
							$("#opc-additional").find("h3:first").removeClass('light');

							var needToReloadShipping = !opc_shipping.isCompleted();
							// Update shipping address as well
							if (shippingAsBilling && response.data.shippingAddress != undefined && response.data.shippingAddress != null) {
								opcData.shippingAddress = response.data.shippingAddress;
								opc_shipping.setShippingAddressView(opcData.shippingAddress);

								opc_shipping.setShippingAddressSet(true);

								// Check are shipping methods returned
								if (response.data.shipments != undefined && response.data.shipments != null) {
									opcData.shipments = response.data.shipments;
									opc_shipping.setShippingMethodsForm(opcData.shipments, opcData.shippingMethodsAlternative);
									needToReloadShipping = true;
								}

								// Check Bongo status
								onePageCheckout.checkBongoStatus();

								$("#opc-shipping-address-form").slideUp('fast');
								$("#opc-shipping-address-view").slideDown('fast');
							} else {
								if (!shippingAsBilling && $('#opc-shipping-address-id-billing').is(':checked')) {
									opcData.shippingAddressId = "new";
									opc_shipping.setShippingAddressSet(false);
									opc_payment.setPaymentMethodsSet(false);

									$('#opc-shipping-address-id-new').click();
									opc_shipping.editShippingAddress();
								}
							}

							if (response.data.accountCreated != null && response.data.accountCreated) {
								$("#opc-account").remove();
								$("#opc-login,#opc-link-login").remove();
							}

							if (response.data.backgroundURL != null && response.data.backgroundURL) {
								$('<iframe style="display:none;" src="' + response.data.backgroundURL + '"></iframe>').appendTo('body');
							}

							var finish = function() {
								opc_shipping.showShippingSection();

								if (opc_shipping.isCompleted()) {
									opc_payment.showPaymentSection();
								}
								onePageCheckout.setCompleteButton();
							};

							if (needToReloadShipping) {
								// Set current method as active
								opc_shipping.ensureShippingSet(finish);
							} else {
								finish();
							}
						}
					},
					msg.opc_saving_billing_data
				);
			} else {
				billingFormContainerEle.slideUp('fast');
				$("#opc-billing-view").slideDown('fast');

				opc_shipping.showShippingSection();

				if (opc_shipping.isCompleted()) {
					opc_payment.showPaymentSection();
				} else {
					opc_payment.hidePaymentSection();
				}

				onePageCheckout.setCompleteButton();
			}
		} catch (err) {
			onePageCheckout.setCompleteButton();
			alert(err);
		}
	};

	/**
	 * Set payment profiles addresses
	 */
	setPaymentProfiles = function() {
		if (opcData.paymentProfiles == undefined || !opcData.paymentProfiles || opcData.paymentProfiles.length < 1) return;

		var paymentProfilesHTML =
			'<div class="spacer no-space-top no-gap-top">' +
				'<strong>Select Billing Address</strong>' +
				'<div class="gap-top col-wrap">';

		$(opcData.paymentProfiles).each(function(index, paymentProfile) {
			var billingData = paymentProfile.billingData;
			paymentProfilesHTML +=
				'<div class="col-50 opc-payment-profile-view"><table class="fieldset"><tbody><tr class="field checkbox"><td><div>' +
					'<div class="input"><input type="radio" id="opc-payment-profile-id-' + paymentProfile.id + '" name="payment-profile-id" value="' + paymentProfile.id + '"></div>' +
					'<label for="opc-payment-profile-id-' + paymentProfile.id + '">' +
					'<strong>' +
					escapeHtml(billingData.firstName) + ' ' + escapeHtml(billingData.lastName) + '<br/>' +
					escapeHtml(billingData.ccNumber) + ' - ' + escapeHtml(billingData.ccExpirationDate) +
					'</strong>' +
					'<br/>' +
					escapeHtml(billingData.addressLine1) + '<br/>' +
					((billingData.addressLine2 != '') ? escapeHtml(billingData.addressLine2) + '<br/>' : '') +
					escapeHtml(billingData.city) + ', ' + escapeHtml(billingData.stateName) + ' ' +
					escapeHtml(billingData.zip) + '<br/>' + escapeHtml(billingData.countryName) +
					'</label>' +
					'</div></td></tr></tbody></table></div>';
		});
		paymentProfilesHTML +=
			'</div><div class="gap-top"><table class="fieldset"><tbody><tr class="field checkbox"><td><div>' +
				'<div class="input"><input type="radio" id="opc-payment-profile-id-billing" name="payment-profile-id" value="billing"></div>' +
				'<label for="opc-payment-profile-id-billing"><strong>Use your billing address</strong></label>' +
				'</div></td></tr>' +
				'</tbody></table></div></div>';

		$("#opc-billing-form-profiles").html(paymentProfilesHTML).show();

		if (opcData.paymentProfileId) {
			$('#opc-payment-profile-id-' + opcData.paymentProfileId).attr('checked', 'checked');
		}

		if (opcData.paymentProfileId != 'billing') {
			$("#opc-billing-form-inner").hide();
		}

		$("input[name='payment-profile-id']").change(function() {
			$("#opc-billing-form").data("changed", true);
			opcData.paymentProfileId = $(this).val();

			if (opcData.paymentProfileId == 'billing') {
				$("#opc-billing-form-inner").show();
				opcData.paymentProfileMethodId = 0;
			} else {
				$.each(opcData.paymentProfiles, function(index, paymentProfile){
					if (opcData.paymentProfileId == paymentProfile['usid']) {
						opcData.paymentProfileMethodId = paymentProfile['pid'];
					}
				});
				$("#opc-billing-form-inner").hide();
			}
		});
	};

	/**
	 * Copy billing address
	 */
	copyBillingToShipping = function() {
		$('#opc-shipping-address-form-name').val($('#opc-billing-form-fname').val() + ' ' + $('#opc-billing-form-lname').val());
		$('#opc-shipping-address-form-company').val($('#opc-billing-form-company').val());
		$('#opc-shipping-address-form-address1').val($('#opc-billing-form-address1').val());
		$('#opc-shipping-address-form-address2').val($('#opc-billing-form-address2').val());
		$('#opc-shipping-address-form-city').val($('#opc-billing-form-city').val());
		$('#opc-shipping-address-form-country_id').val($('#opc-billing-form-country').val()).change();
		$('#opc-shipping-address-form-state_id').val($('#opc-billing-form-state').val()).change();
		$('#opc-shipping-address-form-province').val($('#opc-billing-form-province').val());
		$('#opc-shipping-address-form-zip').val($('#opc-billing-form-zip').val());
	};

	isCompleted = function() {
		return opc_billingDataSet;
	};

	setIsCompleted = function(val) {
		opc_billingDataSet = val;
	};

	return {
		initModule: initModule,
		isCompleted: isCompleted,
		setIsCompleted: setIsCompleted
	};
}(jQuery));
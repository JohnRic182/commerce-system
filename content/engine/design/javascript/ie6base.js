$(document).ready(function(){
	// Fixes select width
	$('div.panel-catalog-manufacturers div.content select').css('width', '88%');
	
	// fixes border-left on menu elements
	$('ul.menu li:first-child, #footer ul li:first-child').css('border-left', 'none');
	
	$('.catalog-product-image img, .product-image img').each(function(){
		if ($(this).width() >= $('.catalog-product-image').width())
		{
			$(this).css('width', '100%');
		}
	});
	$(document).pngFix();
});
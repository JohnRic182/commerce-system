var onePageCheckout = (function($) {
	var init,
		setLayout,
		showSpinner,
		hideSpinner,
		setInvoicePreview,
		setCompleteButton,
		checkBongoStatus,
		sendRequest,
		setBongoOnly,
		isBongoOnly
	;

	var opc_spinnerTimeout = null,
		opc_bongoOnly = false
	;

	init = function() {
		$(document).ready(function() {
			window.onbeforeunload = function() {
				$("#opc-button-complete").attr('disabled', 'disabled').addClass('button-disabled');
			};

			$(window).resize(function(){ setLayout(); });
			$(window).scroll(function(){ setLayout(); });

			setLayout();

			if (top.location != self.location) {
				top.location = self.location;
			}

			try {
				$("#opc-error").hide();

				/**
				 * Init cart items view toggle - this works all the time..
				 */
				var cartItemsToggleEle = $("#cart-items-toggle");
				cartItemsToggleEle.click(function() {
					$("#cart-items").toggle("fast", function() {
						var visible = $("#cart-items").is(':visible');
						$("#cart-items-toggle").html(visible ? msg.opc_invoice_click_to_hide : msg.opc_invoice_click_to_view);
						$.cookie('opc-cart-items-visible', visible ? '1' : '0');
					});
					return false;
				});

				/**
				 * Init progress dialog
				 */
				$("#opc-dialog-spinner").data('counter', 0).dialog({
					width:400,
					height:124,
					modal:true,
					autoOpen:false,
					resizable:false,
					closeOnEscape:false,
					draggable:false
				})
					.parent().find('.ui-dialog-titlebar').remove();

				showSpinner();

				opc_billing.initModule();
				opc_shipping.initModule();
				opc_payment.initModule();

				if ($.cookie('opc-cart-items-visible') != null && $.cookie('opc-cart-items-visible') == "1") {
					cartItemsToggleEle.html(msg.opc_invoice_click_to_hide);
					$("#cart-items").show();
				} else {
					cartItemsToggleEle.html(msg.opc_invoice_click_to_view);
					$("#cart-items").hide();
				}

				if(opcData.OPCLight){
					$("#opc-invoice-cart").hide();
				}
			} catch (err) {
				alert(err);
				hideSpinner();
			}
		});
	};

	/**
	 * Set window element layout
	 */
	setLayout = function() {
		function viewport() {
			var e = window, a = 'inner';
			if (!('innerWidth' in window )) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
		}

		if (!$.mobile) {
			var invoiceTotalRowEle = $('#opc-invoice-total-row');
			var elementOffset = invoiceTotalRowEle.offset();
			var elementHeight = invoiceTotalRowEle.height();
			var vp = viewport();

			if (vp.width <= 991 && $(window).scrollTop() > elementOffset.top - elementHeight) {
				$('#opc-invoice-fixed').show();
			} else {
				$('#opc-invoice-fixed').hide();
			}
		}
	};

	/**
	 * Shows spinner
	 * @param _msg
	 */
	showSpinner = function(_msg) {
		try {
			var spinnerDialogEle = $("#opc-dialog-spinner");
			var counter = spinnerDialogEle.data('counter');
			if (_msg == null) _msg = 'Updating data';
			spinnerDialogEle.find(".spinner-message").html(_msg).show();
			spinnerDialogEle.dialog('open');
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Hides spinner
	 */
	hideSpinner = function() {
		try {
			if (opc_spinnerTimeout != null) {
				clearTimeout(opc_spinnerTimeout);
				opc_spinnerTimeout = null;
			}
			var spinnerDialogEle = $("#opc-dialog-spinner");
			var counter = spinnerDialogEle.data('counter');
			counter = counter - 1;
			if (counter <= 0) {
				counter = 0;
				spinnerDialogEle.dialog('close');
			}
			spinnerDialogEle.data('counter', counter);
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Recalculates invoice amounts and shows totals
	 */
	setInvoicePreview = function() {
		try {
			var invoiceTotalsEle = $("#opc-invoice-totals");
			invoiceTotalsEle.children("table").find('tr').hide();
			invoiceTotalsEle.find(".invoice-row").hide();

			$.each(opcData.orderView.lineItems, function(key, lineItem){
				if (lineItem.subtitle != '') {
					$("#opc-invoice-" + key + "-row .invoice-title").html(lineItem.title + '<br/><span class="invoice-subtitle">' + lineItem.subtitle + '</span>');
				} else {
					$("#opc-invoice-" + key + "-row .invoice-title").html(lineItem.title);
				}
				$("#opc-invoice-" + key + "-row .invoice-amount").html(lineItem.amount);
				$("#opc-invoice-" + key + "-row").show();

				if (key == 'subtotal') {
					$('.order-total-amount').html(lineItem.amount);
				}

				if (key == 'total') {
					$('.opc-order-total-amount').html(lineItem.amount);
				}
			});

			//TODO: Refactor to payments
			// Manage payment methods view
			if ($('#opc-payment-methods-error:visible').length > 0) {
				$("#opc-payment-methods-free").hide();
				$("#opc-payment-methods").hide();
			} else {
				if (opcData.orderData.totalAmount > 0 || opcData.orderData.hasRecurringBillingItems) {
					$("#opc-payment-methods-free").hide();
					$("#opc-payment-methods").show();

					// Gift certificate amount - gift certificate was used
					if (opcData.giftCertificateView != undefined && opcData.giftCertificateView != null) {
						gsView = opcData.giftCertificateView;

						if (gsView.giftCertificateUsed) {
							var giftCertificateViewEle = $("#opc-gift-certificate-view");
							giftCertificateViewEle.html(
								'<strong>' + msg.opc_gift_certificate + '</strong>' +
									'<div class="spacer">' + gsView.paymentFormText + '</div>'
							);

							// Show / hide payment methods depending on remaining amount.
							if (gsView.orderHasRemainingAmount) {
								opc_payment.setPaymentFormSet(opcData.paymentMethodId != 'gift-certificate');
								$("#opc-payment-methods-list").show();
							} else {
								$("#opc-payment-method-gift-certificate").attr("checked", "checked");
								$("#opc-payment-methods-list").hide();
								$("#opc-payment-method-form").hide();

								$("#opc-gift-certificate-form").hide();
								giftCertificateViewEle.show();
								opc_payment.setPaymentFormSet(true);
							}

							opc_payment.setGiftCertificateSet(true);
						}
					}
				} else {
					$("#opc-payment-methods").hide();
					$("#opc-payment-methods-free").show();
					opcData.paymentMethodId = 'free';
				}
			}
		} catch (err) {
			alert(err);
		}
	};

	/**
	 * Check is it time to enable / disable button
	 * @return
	 */
	setCompleteButton = function() {
		var completeButtonEle = $("#opc-button-complete");
		try {
			//condition to checks
			if (opc_billing.isCompleted() && opc_shipping.isCompleted() && opc_payment.isCompleted()) {
				completeButtonEle.removeAttr('disabled').removeClass('button-disabled');
			} else {
				completeButtonEle.attr('disabled', 'disabled').addClass('button-disabled');
			}
		} catch (err) {
			alert(err);
		}

		completeButtonEle.unbind('click').click(function(e) {
			e.stopPropagation();
			opc_payment.processPayment();
			return false;
		});
	};

	/**
	 * Check Bongo status
	 *
	 */
	checkBongoStatus = function() {
		opc_bongoOnly = opcData.bongoActive && opcData.bongoUsed;
		if (opcData.bongoActive) opc_payment.initPayment();
	};

	/**
	 * Send request to a server
	 * @param _action
	 * @param _data
	 * @param _handler
	 * @param _message
	 * @return
	 */
	sendRequest = function(_action, _data, _handler, _message) {
		try {
			showSpinner(_message);
			var requestTime = new Date();

			$.ajax(
				{
					url: site_https_url + 'oa=OnePageCheckout&action=' + _action + '&requestTime=' + requestTime.getTime(),
					type: 'POST',
					cache: false,
					headers: {
						"cache-control": "no-cache"
					},
					//async: false,
					data: {
						data: JSON.stringify(_data)
					},
					success:
						/**
						 * Process response here
						 */
						function (response) {
							try {
								hideSpinner();

								hasResult = true;
								if (response && response != null && $.trim(response).length >= 0 && $.trim(response).charAt(0) == '{') {
									response = JSON.parse(response);
									if (response.result == null) {
										hasResult = false;
									} else {
										if (response.result != null && !response.result) {
											var _msg = "";
											jQuery.each(response.errors, function(errKey, errMsg){
												_msg = _msg + (_msg == "" ? "" : "\n") + '- ' + errMsg;
											});
											alert(_msg);

											if (response.data != undefined && response.data.integrityError != undefined && response.data.integrityError)
											{
												document.location.reload(true);
											}
										}

										if (response.data.orderData != undefined && response.data.orderData != null) {
											opcData.orderData = response.data.orderData;
										}

										if (response.data.shipments != undefined && response.data.shipments != null) {
											opcData.shipments = response.data.shipments;
										}

										if (response.data.orderView != undefined && response.data.orderView != null) {
											opcData.orderView = response.data.orderView;
										}

										if (response.data.giftCertificateView != undefined && response.data.giftCertificateView != null) {
											opcData.giftCertificateView = response.data.giftCertificateView;
										}

										if (_handler != undefined && _handler != null) {
											_handler(response);
										}

										setInvoicePreview();

									}
								} else {
									hasResult = false;
								}

								if (!hasResult) {
									alert(msg.opc_error_cannot_process_response);
								}
								setCompleteButton();
							} catch (err) {
								alert(err);
								hideSpinner();
							}
						}
				}
			);
		} catch (err) {
			alert(err);
			hideSpinner();
		}
	};

	setBongoOnly = function(value) {
		opc_bongoOnly = value;
	};

	isBongoOnly = function() {
		return opc_bongoOnly;
	};

	init();

	return {
		showSpinner: showSpinner,
		showSpinnerDelayed: showSpinner,
		hideSpinner: hideSpinner,
		setInvoicePreview: setInvoicePreview,
		setCompleteButton: setCompleteButton,
		checkBongoStatus: checkBongoStatus,
		sendRequest: sendRequest,
		setBongoOnly: setBongoOnly,
		isBongoOnly: isBongoOnly
	};
}(jQuery));

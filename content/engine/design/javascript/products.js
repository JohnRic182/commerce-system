$(document).ready(function(){
	// enable lightbox
	if ($('a[rel="lightbox"]').size() > 0)
	{
		$('a[rel="lightbox"]').lightBox();
	}

	$('input[name="oa_custom_data[recurring_start_date]"]').each(function(){
		var maxStartDateDelay = $(this).attr('data-max-start-date-delay');
		if (maxStartDateDelay == undefined || maxStartDateDelay == '') maxStartDateDelay = '1';

		$(this)
			.attr('placeholder', 'mm/dd/yyyy')
			.datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: new Date(),
				maxDate: '+' + maxStartDateDelay + 'm',
				showOtherMonths: true
			})
			.datepicker('setDate', new Date())
			.on('change', function(e) {
				var maxStartDateDelay = $(this).attr('data-max-start-date-delay');
				if (maxStartDateDelay == undefined || maxStartDateDelay == '') maxStartDateDelay = '1';
				maxStartDateDelay = parseInt(maxStartDateDelay)

				// fix data if it's wrong
				$(this).datepicker('setDate', $(this).val());
				var selectedDate = $(this).datepicker('getDate');

				// get current and max dates
				var nowDate = new Date();
				var maxDate = new Date();
				maxDate.setMonth(maxDate.getMonth() + maxStartDateDelay); // allow to set only 3 month ahead
				maxDate.setHours(0, 0, 0, 0);

				// check is current date in range
				if (selectedDate < nowDate) $(this).datepicker('setDate', nowDate);
				if (selectedDate > maxDate) $(this).datepicker('setDate', maxDate);
			});
	});

	calcAttrPrice();
	// responsive lightbox initialize options
	$('.responsive-lightbox').magnificPopup({
        delegate:'a',
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
            verticalFit: true
        },
        gallery:{
			enabled:true
		}  
    });
});

/**
 * Calc attribute price based on data available, update stock as well
 * @return
 */
function calcAttrPrice()
{
	var price_diff = 0;
	var weight_diff = 0;
	var attributes_list = "";
	var attributes_hash = "";
	var hasMissingAttribute = false;
	var val;
	$.each(product_attributes, function(i, attribute){
		var t =  $("input[name='oa_attributes[" + attribute.paid + "]'],select[name='oa_attributes[" + attribute.paid + "]']").prop('type');
		if (t == 'select-one' || t == 'radio')
		{
			var ctrl = (t == 'select-one') ? $("#attribute_input_" + attribute.paid) : $("input[name='oa_attributes[" + attribute.paid + "]']:checked");

			val = urldecode($(ctrl).val());

			if (typeof(attribute.options[val]) != 'undefined')
			{
				price_diff = price_diff + attribute.options[val].price_difference;
				weight_diff = weight_diff + attribute.options[val].weight_difference;

				if (attribute.track_inventory == 1)
				{
					if (attributes_hash != '') attributes_hash = attributes_hash + '\n';
					attributes_hash += attribute.paid + ': ' + val;

					if (attributes_list != '') attributes_list = attributes_list + '\n';
					attributes_list += attribute.name + ': ' + val;
				}
			}
			else
			{
				if (ctrl.parents('.product-attribute').attr('attribute_required') == 'No')
				{
					hasMissingAttribute = true;
					ctrl.parents('.product-attribute').hide();
				}
			}
		}
	});

	// modify visually
	if (displayPricesWithTax && tax_rate > 0)
	{
		price_diff = price_diff + ( price_diff * tax_rate / 100);
	}

	setHTMLPrice("product_price", product_price + price_diff);
	if (product_price2 != null) setHTMLPrice("product_price2", product_price2 + price_diff);

	if (userLevel > 0)
	{
		if (WholesaleDiscountType == "PRODUCT")
		{
			setHTMLPrice("product_price_wholesale", product_price_wholesale + price_diff);
		}
		else
		{
			setHTMLPrice("product_price_wholesale", (product_price_wholesale + price_diff) / 100 * (100 - product_price_wholesale_discount));
		}
	}

	setHTMLWeight("product_weight", number_format(product_weight + weight_diff, 2, ".", ","));

	inventory_hashes = inventory_hashes == undefined ? false : inventory_hashes;

	if ((inventory_hashes && inventory_hashes.length > 0) || inventory_control == "AttrRuleExc" || inventory_control == "AttrRuleInc")
	{
		var variantStock = inventory_control == "AttrRuleExc" || inventory_control == "AttrRuleInc";

		var current_hash = hex_md5(attributes_hash);
		var product_may_be_added = false;
		inventory_hash_found = false;
		$.each(inventory_hashes, function(i, inventory_hash)
		{
			if (inventory_hash.attributes_hash == current_hash)
			{
				inventory_hash_found = true;
				if (variantStock)
				{
					product_stock = inventory_hash.stock;
				}

				if (!variantStock || (product_stock > 0))
				{
					$('#div-out-of-stock').hide();
					$('#div-add-button').show();
					$('.product-quantity').show();

					if (product_stock < DisplayStockOnProductPageThreshold) {
						$("#product_stock").parent().show();
						$("#product_stock").html(product_stock);
					} else {
						$("#product_stock").parent().hide();
						$("#product_stock").html(0);
					}

					product_may_be_added = true;
				}
				else
				{
					$('#div-add-button').hide();
					$('.product-quantity').hide();
					$('#div-out-of-stock').show();
					$("#product_stock").parent().hide();
					$("#product_stock").html(0);
					product_may_be_added = false;
				}
			}
		});

		if (!inventory_hash_found)
		{
			if (variantStock) product_stock = 0;
			if (inventory_control == 'AttrRuleInc' && !hasMissingAttribute)
			{
				$('#div-out-of-stock').hide();
				$('#div-add-button').show();
				$('.product-quantity').show();
				$("#product_stock").parent().show();
				$("#product_stock").html(product_stock);
				product_may_be_added = true;
			}
			else
			{
				$('#div-add-button').hide();
				$('.product-quantity').hide();
				$('#div-out-of-stock').show();
				$("#product_stock").parent().hide();
				$("#product_stock").html(0);
				product_may_be_added = false;
			}
		}

		if (val == '-0-') // default "Select" option value
		{
			$('#div-out-of-stock').hide();
			$('#div-add-button').show();
			$('.product-quantity').show();
			product_may_be_added = false;
		}
	}
}

$(document).ready(function(){
	$('#form-wishlist').submit(addToWishlist);
});

function addToWishlist()
{
	attributes = $('form[name="frmAddItem"] :input[name^="oa_attributes"]');

	$(attributes).each(function()
	{
		hasVal = true;
		val = $(this).val();
		if ($(this).is(':radio') && !$(this).is(':checked'))
		{
			hasVal = false;
		}

		if (hasVal)
		{
			input = $('<input>').attr({
				type: 'hidden',
				name: $(this).attr('name'),
				value: val
			});
			$('#form-wishlist').append(input);
		}
	});
}

/**
 * Set price
 * @param el_id
 * @param amount
 * @return
 */
function setHTMLPrice(el_id, amount)
{
	$('#' + el_id).html(getPrice(amount));
}

/**
 * Set weight
 * @param el_id
 * @param weight
 * @return
 */
function setHTMLWeight(el_id, weight)
{
	$('#' + el_id).html(weight + ' ' + weight_unit);
}


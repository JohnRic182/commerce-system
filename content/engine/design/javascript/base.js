$(document).ready(function(){
	removeEmptyZones();
	setContentWidth();
	setTimeout('setContentWidth();', 1000);
	$(window).resize(function(){
		setContentWidth();
		setTimeout('setContentWidth();', 1000);
	});

	init();

	if ($('#design-mode').size() < 1)
	{
		$('a[href^="#"]').each(function(idx, anchor){
			var a = $(anchor);
			a.attr('href', document.location.pathname + a.attr('href'));
		});
	}

	/**
	 * Facebook
	 */
	if ((typeof(facebookAccountLogin) != undefined && facebookAccountLogin != null && facebookAccountLogin) && (typeof(facebookAppId) != undefined && facebookAppId != null && facebookAppId))
	{
		window.fbAsyncInit = function() {
			FB.init({
				appId: facebookAppId,
				status: true,
				cookie: true,
				xfbml: true,
				oauth: true
			});

			var page = $('body').attr('id').substr(5);

			FB.Event.subscribe('auth.authResponseChange', function(response){
				if (response.status == 'connected')
				{
					if (!login_ok && (page == 'login' || page == 'one_page_checkout'))
					{
						FB.api('/me', function(response){
							//logged in and connected user, someone you know
							window.location = site_http_url + 'ua=user_login_fb&p=' + page;
						});
					}
				}
				else if (response.status == 'not_authorized')
				{
					FB.login();
				}
				else
				{
					if (!login_ok && (page == 'login' || page == 'one_page_checkout'))
					{
						FB.login();
					}
				}
			});
		};
		(function(d){
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		}(document));
	}
});


/**
 * Init user site
 */
function init()
{
	$("body").css("display", "block");

	// init top menu
	if (topMenu == "YES" && $("#menu ul").length > 0)
	{
		$("#menu ul").dropDownMenu({});
	}

	// initialize categories tree structure
	if (sideMenuStyle == "tree" && $(".tree").length > 0)
	{
		$(".tree").treeview({collapsed: true, animated: "medium", persist: "location"});
	}

	// or, alternativelly, initialize categories drop-down structure
	if (sideMenuStyle == "drop-down" || $(".drop-down-menu-vertical").length > 0)
	{
		$(".drop-down-menu-vertical").parent(".panel .content").each(function(){$(this).css("padding", "0px").css("margin", "0px");});
		$(".drop-down-menu-vertical .current").parents('li').each(function(){$(this).addClass('current');});
		$(".drop-down-menu-vertical").dropDownMenu({
			layout : 'vertical',
			onBeforeShow: function()
			{
				var w = $(this).parent().parent().width();
				$(this).css("left", w + "px").css("width", w + "px");
			}
		});
	}

	//ini cataog navigation
	if ($("body").hasClass("page-catalog"))
	{
		var currentPageSizeElement = $("#catalog-navigation-menu-pagesize ul li a.current");
		if (currentPageSizeElement.length > 0)
		{
			var a = $("#catalog-navigation-menu-pagesize a:first").click(function(e){e.preventDefault();});
			$(a).html($(a).html() + ": " + $(currentPageSizeElement).html());
		}
		var currentSortItem = $("#catalog-navigation-menu-sort ul li a.current");
		if (currentSortItem.length > 0)
		{
			var a = $("#catalog-navigation-menu-sort a:first");
			a.click(function(e){e.preventDefault();});
			$(a).html($(a).html() + ': ' + $(currentSortItem).html());
		}
		var currentViewItem = $("#catalog-navigation-menu-view ul li a.current");
		if (currentViewItem.length > 0)
		{
			var a = $("#catalog-navigation-menu-view a:first");
			a.click(function(e){e.preventDefault();});
			$(a).html($(a).html() + ': ' + $(currentViewItem).html());
		}
		
		$("#catalog-navigation-menu").dropDownMenu({subMenuWidth:'100%', menuMinWidth:'100px', arrowCode:'&nbsp; &darr;'});
	}
	
	if ($(".catalog-view-flexible").length > 0)
	{
		setCatalogViewFlexible();
		setTimeout('setCatalogViewFlexible', 500);
		var winWidth = $(window).width(), winHeight = $(window).height();
		
		$(window).resize(function(){
			var newWinWidth = $(window).width(), newWinHeight = $(window).height();
			
			if (winWidth != newWinWidth || winHeight != newWinHeight)
			{
				setCatalogViewFlexible();
				setTimeout('setCatalogViewFlexible', 500);
				
				winWidth = newWinWidth;
				winHeight = newWinHeight;
			}
		});
	}
	
	//add product rate
	$(".product-rating-small").each(function(){
		$(this).rater({
			'curvalue': this.title,
			'style': 'small',
			'disable': true
		});
	});

	//init product tabs
	if ($("body").hasClass("page-product"))
	{
		if ($(".product-page-blocks").hasClass("product-page-blocks-tabs"))
		{
           	var html = '';
			var blockCounter = 0;
			$(".product-page-blocks .product-page-block").each(function(index, block){
				//check is there header
				if ($(block).find(".product-page-block-header").length > 0)
				{
					var blockId = $(block).attr("id") != undefined ? $(block).attr("id") : 'product-page-block-' + blockCounter;
					$(block).attr("id", blockId);
					var title = $(block).find(".product-page-block-header:first").html();

                    //TODO: review this - related to jquery ui issue http://bugs.jqueryui.com/ticket/7822 - for now have to pass full URL as it does not resolve it correctly
					html = html + '<li><a href="' + document.location + '#' + blockId + '">' + title + '</a></li>';
				}
				blockCounter++;
			});

            $(".product-page-blocks .product-page-block .product-page-block-header").remove();

			if (html != '')
            {
                $(".product-page-blocks-tabs").prepend('<ul>' + html + '</ul>');
                $('.product-page-blocks-tabs').tabs();

				$(".product-rating a").click(function(e){
					e.preventDefault();

					$('.product-page-blocks-tabs .ui-tabs-nav a').each(function(){
						var s = $(this).attr('href');
						if (s.indexOf('#product-page-reviews') > 0)
						{
							$(this).click();
						}
					});
					return false;
				});
            }
		}
	}

	$('.panel-catalog-products-filters .filter-options-toggle-link').click(function() {
		$($(this).attr('data-target')).slideToggle('fast');
		return false;
	});

	$('.link-reset-products-filters')
		.on('click', function(){
			$('.panel-catalog-products-filters form').trigger('reset');
		});

	$('.panel-catalog-products-filters form')
		.on('submit', function(){
			var filtersChecked = $('.filter-input:checked');
			var filtersArray = {};

			$(filtersChecked).each(function(index, filter) {
				filtersArray[$(filter).attr('name')] = [];
			});

			$(filtersChecked).each(function(index, filter) {
				filtersArray[$(filter).attr('name')].push($(filter).val());
			});

			var filtersQueryString = [];
			$.each(filtersArray, function(index, data) {
				filtersQueryString.push(index + '=' + data.join(','));
			});

			filtersQueryString = filtersQueryString.join('&');

			var baseUrl = $(this).attr('action');
			window.location.href =  baseUrl + (baseUrl.indexOf('?') >= 0 ? '&' : '?') + filtersQueryString;

			return false;
		})
		.on('reset', function(){
			window.location.href =  $(this).attr('action');
			return false;
		});
}
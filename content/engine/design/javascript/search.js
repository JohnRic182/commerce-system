(function($) {
	$.fn.predictiveSearch = function ( options ) {

		var settings = $.extend({
			hasKeyboardNavigation : true,
			timeOut : 500
		}, options);

		$(this).attr('autocomplete', 'off');

		return this.each(function(index, el) {
			var predictiveSearchTimer = null;
			var searchInput = this;
			var searchResultContainer = $('.predictive-search-results');

			if ($(searchResultContainer).length == 0) {
				searchResultContainer = $('<div>', {class: 'predictive-search-results', style: 'display:none'});
				$(searchResultContainer).appendTo('body');
			}

			$(searchInput).closest('form').on('submit', function(){
				if ($(searchInput).is(':focus') && $(searchResultContainer).is(':visible')) {
					return false;
				}
			});

			$(searchInput).on('blur', function(){
				$(searchResultContainer).hide();
			});

			$(searchInput).keyup(function(e) {
				var code = e.keyCode || e.which;
				switch (code) {
					case 13: { // enter
						var href = searchResultContainer.find('li.selected a').attr('href');
						if (href != null) document.location = href;
						break;
					}
					case 38: { // up
						if (!settings.hasKeyboardNavigation) return;
						var prev = searchResultContainer.find('.selected').prevAll('.link:first');
						if (prev.length > 0) {
							searchResultContainer.find('.selected').removeClass('selected');
							$(prev).addClass('selected');
						}
						break;
					}
					case 40: { // down
						if (!settings.hasKeyboardNavigation) return;
						var next = searchResultContainer.find('.selected').nextAll('.link:first');
						if (next.length > 0) {
							searchResultContainer.find('.selected').removeClass('selected');
							$(next).addClass('selected');
						}
						break;
					}
					case 27: {
						$(searchResultContainer).hide();
						break;
					}
					case 37: // left
					case 39: { // right
						// ignore
						break;
					}
					default: {
						var str = $.trim($(this).val());

						if (str.length > 0) {
							var html = '';

							/**
							 * Server search
							 */
							if (predictiveSearchTimer) clearTimeout(predictiveSearchTimer);

							predictiveSearchTimer = setTimeout(function() {
								$.ajax({
									url: 'index.php?p=catalog&ua=user_predictive_search&mode=search',
									method: 'POST',
									dataType: 'json',
									data : {search_str : str},
									success: function (response, status) {
										if (response && response.status && response.results && response.count > 0 && $(searchInput).is(':focus')) {
											$.each(response.results, function(index, item) {
												if (item.type = 'group') {
													if (item.url === undefined) {
														html += '<li class="group">' + (item.title) + ' (' + item.count + ')</li>';
													} else {
														html += '<li class="group link"><a href="' + item.url + '">' + (item.title) + ' (' + item.count + ')</a></li>';
													}
													$.each(item.items, function(subIndex, subItem) {
														html += '<li class="item link"><a href="' + subItem.url + '">' + (subItem.title) + '</a></li>';
													});
												} else {
													html += '<li class="item link"><a href="' + item.url + '">' + (item.title) + '</a></li>';
												}
											});
											searchResultContainer.html('<ul>' + html + '</ul>');
											searchResultContainer.find('ul li:first-child').addClass('selected');

											var searchInputOffset = $(searchInput).offset();

											searchResultContainer.css({
												top: searchInputOffset.top + $(searchInput).outerHeight(),
												left: searchInputOffset.left,
												position: 'absolute'
											}).show();
										} else {
											$(searchResultContainer).hide();
										}

									}
								});
							}, 500);
						} else {
							$(searchResultContainer).hide();
						}
					}
				}
			});

			searchResultContainer.on('mouseover', 'ul li.link', function () {
				$(this).focus();
				$(searchResultContainer).find('.selected').removeClass('selected');
				$(this).addClass('selected');
			});

			// Handle click event on search results
			// Since the blur hides those elements, you end up clicking nothing
			// So using mousedown event instead of click will fix the issue
			searchResultContainer.on('mousedown', function(){
				var href = searchResultContainer.find('li.selected a').attr('href');
				if (href != null) document.location = href;
			});
		});
	}

}(jQuery));

$(document).ready(function() {
	$('input[name="search_str"]').each( function () {
		$(this).predictiveSearch({});
	});
});
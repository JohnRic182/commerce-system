$(document).ready(function()
{
	var recaptchaEnabled = $('#new-email-to-friend .captcha').length > 0;

	$("#new-email-to-friend").dialog({
		'modal'			: true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : false,
		'width'         : recaptchaEnabled ? 600 : 500,
		'height'        : recaptchaEnabled ? 500 : 320,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Email to friend',
		'closeOnEscape'	: false,
		'open': function(ev, ui)
		{
			$("#email-to-friend-submit").unbind("click");
			$('#email-to-friend-submit').removeAttr("disabled");
			$('#email-to-friend-submit').click(function(){
				$(this).attr("disabled", true);
				submitEmailToFriend();
				return false;
			});
		}
	});
});


/**
 * Show email 2 friend dialog
 * @return
 */
function showEmail2FriendDialog()
{
	$('#new-email-to-friend').dialog("open");
	$('#e2f-fname').val('');
	$('#e2f-femail').val('');

	return false;
}

/**
 * Submit email to friend
 * @return
 */
function submitEmailToFriend()
{
	var error_message = '';
	var focus = false;
	
	if ($('#e2f-yname').val().length < 3)
	{
		error_message = error_message + ' - ' + msg.your_name + "\n";
		if (!focus) focus = '#e2f-yname';
	}
	
	if (!isEmail($('#e2f-yemail').val()))
	{
		error_message = error_message + ' - ' + msg.your_email_address + "\n";
		if (!focus) focus = '#e2f-yemail';
	}
	
	if ($('#e2f-fname').val().length < 3)
	{
		error_message = error_message + ' - ' + msg.your_friend_name + "\n";
		if (!focus) focus = '#e2f-fname';
	}
	
	if (!isEmail($('#e2f-femail').val()))
	{
		error_message = error_message + ' - ' + msg.your_friend_email_address + "\n";
		if (!focus) focus = '#e2f-femail';
	}
	
	if (parseInt(error_message.length))
	{
		alert(msg.to_continue_please_check + '\n' + error_message);
		if (focus) $(focus).focus();
		$('#email-to-friend-submit').removeAttr("disabled");
		return false;
	}
	
	var pid =  $("input[name='pid']").val();

	$.post(site_ajax_url + '&p=email2friend', {
		'pid': pid,
		'yname': $('#e2f-yname').val(),
		'yemail': $('#e2f-yemail').val(),
		'fname': $('#e2f-fname').val(),
		'femail': $('#e2f-femail').val(),
		'nonce': $('#e2f-nonce').val(),
		'recaptcha_challenge_field': $('#new-email-to-friend  input[name="recaptcha_challenge_field"]').val(),
		'recaptcha_response_field': $('#new-email-to-friend  input[name="recaptcha_response_field"]').val(),
		'action': 'send'
	}, function(data) {
		$('#e2f-nonce').val(data.nonce);
		if (data.status == 1) {
			$('#email-to-friend-submit').removeAttr("disabled");
			$("#new-email-to-friend").dialog("close");
		} else {
			alert(data.captcha_error ? msg.enter_valid_recaptcha : msg.please_refresh_page);
			$('#email-to-friend-submit').removeAttr("disabled");
		}
	});
}

//copy billing address to shipping
function tcBilling2Shipping(){
	frm = document.getElementById("frmTCBillingAndShipping");
	frm.elements["shipping_address[name]"].value = frm.elements["billing_address[fname]"].value + " " + frm.elements["billing_address[lname]"].value;
	frm.elements["shipping_address[company]"].value = frm.elements["billing_address[company]"].value;
	frm.elements["shipping_address[address1]"].value = frm.elements["billing_address[address1]"].value;
	frm.elements["shipping_address[address2]"].value = frm.elements["billing_address[address2]"].value;
	frm.elements["shipping_address[city]"].value = frm.elements["billing_address[city]"].value;
	frm.elements["shipping_address[country]"].value = frm.elements["billing_address[country]"].value;
	setShippingState();
	frm.elements["shipping_address[state]"].value = frm.elements["billing_address[state]"].value;
	frm.elements["shipping_address[zip]"].value = frm.elements["billing_address[zip]"].value;
	document.getElementById('sa_new').click();

}

//check billing form
function tcCheckBillingForm(){
	frm = document.getElementById("frmTCBillingAndShipping");
	if(!CheckField(frm, "billing_address[fname]", msg.first_name)) return false;
	if(!CheckField(frm, "billing_address[lname]", msg.last_name)) return false;
	if(billing_address_company == "Required" && (!CheckField(frm, "billing_address[company]", msg.company_name))) return false;
	if(!CheckField(frm, "billing_address[address1]", msg.address_line1)) return false;
	if(billing_address_line2 == "Required" && (!CheckField(frm, "billing_address[address2]", msg.address_line2))) return false;
	if(!CheckField(frm, "billing_address[city]", msg.city_name)) return false;
	
	//check is state/province select OR custom state/province
	if($("#billing_state:visible").length > 0){
		//check province/state from select
		if(frm.elements["billing_address[state]"].value == "" || frm.elements["billing_address[state]"].value == "0"){
			alert(msg.select_province_state);
			frm.elements["billing_address[state]"].focus();
			return false;
		}
	}
	else{
		//check custom province/state
		if(!CheckField(frm, "billing_address[province]", msg.custom_province_state)) return false;
	}
	
	if(!CheckField(frm, "billing_address[zip]", msg.zip_postal_code)) return false;
	if(billing_address_phone == "Required" && (!CheckField(frm, "billing_address[phone]", msg.phone_number))) return false;
	if(!CheckField(frm, "billing_address[email]", msg.email_address)) return false;
	if(!isEmail(frm.elements["billing_address[email]"].value)){
		alert(msg.enter_valid_email);
		frm.elements["billing_address[email]"].focus();
		return false;
	}
	if(!CheckCustomFields(frm, 'billing')){
		return false;	
	}
	return true;
}

//check shipping form
function tcCheckShippingForm(){
	frm = document.getElementById("frmTCBillingAndShipping");
	shipping_address_id = "";
	
	for(i=0; i<frm.elements.length; i++){
		if(frm.elements[i].name == "shipping_address_id" && frm.elements[i].type == "radio"){
			if(frm.elements[i].checked){
				shipping_address_id = frm.elements[i].value;
			}
		}
	}
	
	if(shipping_address_id != "new") return true;
	
	if(!CheckField(frm, "shipping_address[name]", msg.name)) return false;
	if(shipping_address_company == "Required" && (!CheckField(frm, "shipping_address[company]", msg.company_name))) return false;
	if(!CheckField(frm, "shipping_address[address1]", msg.address_line1)) return false;
	if(shipping_address_line2 == "Required" && (!CheckField(frm, "shipping_address[address2]", msg.address_line2))) return false;
	if(!CheckField(frm, "shipping_address[city]", msg.city_name)) return false;
	
	
	//check is state/province select OR custom state/province
	if($("#shipping_state:visible").length > 0){
		//check province/state from select
		if(frm.elements["shipping_address[state]"].value == "" || frm.elements["shipping_address[state]"].value == "0"){
			alert(msg.select_province_state);
			frm.elements["shipping_address[state]"].focus();
			return false;
		}
	}
	else{
		//check custom province/state
		if(!CheckField(frm, "shipping_address[province]", msg.custom_province_state)) return false;
	}
	
	if(!CheckField(frm, "shipping_address[zip]", msg.zip_postal_code)) return false;
	
	if(!CheckCustomFields(frm, 'shipping')){
		return false;	
	}
	
	return true;
}

//set shipping address
function tcSetShippingAddress()
{
	if ($("#frmTCBillingAndShipping input[name='shipping_address_id']:checked").val() == "new")
	{
		$("#div_new_shipping_address").slideDown("fast");
	}
	else
	{
		$("#div_new_shipping_address").slideUp("fast");
	}
}

//get shipping rates
function tcGetShippingRates(){
	if(!tcCheckBillingForm()){
		return false;
	}
	if(!tcCheckShippingForm()){
		return false;
	}
	frm = document.getElementById("frmTCBillingAndShipping");
	if(!CheckCustomFields(frm, 'invoice')){
		return false;	
	}
	return true;	
}
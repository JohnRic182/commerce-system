<?php

	$url = $_GET["url"];

	// Fix for issue 1244
	if ($url == $settings["GlobalServerPath"] || (isset($_SERVER["SCRIPT_FILENAME"]) && $url == dirname($_SERVER["SCRIPT_FILENAME"])))
	{
		header("Location: ".$url_dynamic);
		_session_write_close();
		die();
	}

	$matchFound = false;

	/**
	 * Check is URL empty
	 * and patch for http://site.com/cart ==> http://site.com/cart/
	 */
	if (trim($url) == "")
	{
		$_REQUEST["p"] = "home";
		$matchFound = true;
	}
	else if ($url == $settings["GlobalServerPath"] ||  $url == dirname($_SERVER["SCRIPT_FILENAME"]))
	{
		header("Location: ".$settings["GlobalHttp".(isSslMode() ? "s" : "")."Url"]."/");
		_session_write_close();
	}

	/**
	 * Try to match category pagination
	 */
	if (!$matchFound)
	{
		$pageNumberRegExp =
			str_replace(
				$a = array("\\", ".", "(", ")", "[", "]", "%PageNumber%", "/"),
				$b = array("\\\\", "\\.", "\\(", "\\)", "\\[", "\\]", "(\d+)", "\/"),
				$settings["SearchURLCategoryPaginatorTemplate"]
			);
		$pageNumberPattern =
			"/^(.+)".
			$pageNumberRegExp.
			"((\/|(\.([a-zA-Z0-9])+))?)$/";

		if (preg_match($pageNumberPattern, $url, $matches))
		{
			$_url = (isset($matches[1]) ? $matches[1] : '').(isset($matches[3]) ? $matches[3] : '');

			$db->query("SELECT cid FROM ".DB_PREFIX."catalog WHERE url_hash='".$db->escape(md5($_url))."'");
			if ($db->movenext())
			{
				$_REQUEST["p"] = "catalog";
				$_REQUEST["parent"] = $db->col["cid"];
				$_REQUEST["pg"] = $matches[2];
				$_REQUEST["mode"] = "catalog";
				$matchFound = true;
			}
		}
	}

	/**
	 * Categories
	 */
	if (!$matchFound)
	{
		$urlMd5 = md5($url);
		$db->query("SELECT cid FROM ".DB_PREFIX."catalog WHERE url_hash='".$db->escape($urlMd5)."'");
		if ($db->movenext())
		{
			$_REQUEST["p"] = "catalog";
			$_REQUEST["parent"] = $db->col["cid"];
			$_REQUEST["pg"] = 1;
			$_REQUEST["mode"] = "catalog";
			$matchFound = true;
		}
	}

	/**
	 * Products
	 */
	if (!$matchFound)
	{
		$db->query("SELECT pid FROM ".DB_PREFIX."products WHERE url_hash='".$db->escape($urlMd5)."'");
		if ($db->movenext())
		{
			$_REQUEST["p"] = "product";
			$_REQUEST["id"] = $db->col["pid"];
			$matchFound = true;
		}
	}

	/**
	 * Try to match manufacturer pagination
	 */
	if (!$matchFound)
	{
		$pageNumberRegExp =
			str_replace(
				$a = array("\\", ".", "(", ")", "[", "]", "%PageNumber%", "/"),
				$b = array("\\\\", "\\.", "\\(", "\\)", "\\[", "\\]", "(\d+)", "\/"),
				$settings["SearchURLManufacturerPaginatorTemplate"]
			);
		$pageNumberPattern =
			"/^(.+)".
			$pageNumberRegExp.
			"((\/|(\.([a-zA-Z0-9])+))?)$/";

		if (preg_match($pageNumberPattern, $url, $matches))
		{
			$_url = (isset($matches[1]) ? $matches[1] : '').(isset($matches[3]) ? $matches[3] : '');

			$db->query("SELECT manufacturer_id AS mid FROM ".DB_PREFIX."manufacturers WHERE url_hash='".$db->escape(md5($_url))."'");
			if ($db->movenext())
			{
				$_REQUEST["p"] = "catalog";
				$_REQUEST["mode"] = "manufacturer";
				$_REQUEST["mid"] = $db->col["mid"];
				$_REQUEST["pg"] = $matches[2];
				$matchFound = true;
			}
		}
	}

	/**
	 * Manufacturer
	 */
	if (!$matchFound)
	{
		$urlMd5 = md5($url);
		$db->query("SELECT manufacturer_id AS mid FROM ".DB_PREFIX."manufacturers WHERE url_hash='".$db->escape($urlMd5)."'");
		if ($db->movenext())
		{
			$_REQUEST["p"] = "catalog";
			$_REQUEST["mid"] = $db->col["mid"];
			$_REQUEST["pg"] = 1;
			$_REQUEST["mode"] = "manufacturer";
			$matchFound = true;
		}
	}

	/**
	 * Pages
	 */
	if (!$matchFound)
	{
		$db->query("SELECT name FROM ".DB_PREFIX."pages WHERE url_hash='".$db->escape($urlMd5)."'");
		if ($db->movenext())
		{
			$_REQUEST["p"] = "page";
			$_REQUEST["page_id"] = $db->col["name"];
			$matchFound = true;
		}
	}

	/**
	 * Other
	 */
	if (!$matchFound)
	{
		$activePage =  str_replace($settings["SearchURLJoiner"], "", $url);
		$allowedPages = array(
			'sitemap' => 'site_map',
			'international' => 'international',
			'testimonials' => 'testimonials'
		);

		if (array_key_exists($activePage, $allowedPages))
		{
			$_REQUEST["p"] = $allowedPages[$activePage];
			$matchFound = true;
		}
	}

	/**
	 * Slash issue fix: http://site.com/cart --> http://site.com/cart/
	 */
	if (!$matchFound)
	{
		if ($url == $settings["GlobalServerPath"])
		{
			header("Location: ".$settings["GlobalHttpUrl".(isSslMode() ? "s" : "")]."/");
			_session_write_close();
			die();

		}
	}

	// Order Forms
	if (!$matchFound)
	{
		$urlMd5 = md5($url);
		$db->query("SELECT * FROM ".DB_PREFIX."order_forms WHERE url_hash='".$db->escape($urlMd5)."'");
		if ($db->movenext())
		{
			$_REQUEST["p"] = "order_form";
			$_REQUEST["ofid"] = $db->col["ofid"];
			$matchFound = true;
		}
	}

	if (!$matchFound)
	{
		$_REQUEST["p"] = "404";
		$matchFound = true;
	}

<?php 
$payment_processor_id = "authorizenet_echeck";
$payment_processor_name = "Authorize.Net eCheck.Net";
$payment_processor_class = "PAYMENT_AUTHORIZENET_ECHECK";
$payment_processor_type = "check";

class PAYMENT_AUTHORIZENET_ECHECK extends PAYMENT_PROCESSOR
{
	function PAYMENT_AUTHORIZENET_ECHECK()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "authorizenet_echeck";
		$this->name = "Authorize.Net eCheck.Net";
		$this->class_name = "PAYMENT_AUTHORIZENET_ECHECK";
		$this->type = "check";
		$this->description = "";
		$this->steps = 1;
		$this->testMode = false;
		$this->support_ccs = false;
		return $this;
	}

	function getPaymentForm($db)
	{
		$fields = parent::getPaymentForm($db);

		if ($this->settings_vars[$this->id."_Secure_Source_eCheck"]["value"] == "YES")
		{
			$fields["x_customer_organization_type"] = array(
				"type"=>"select", "name"=>"form[x_customer_organization_type]", "value"=>"",
				"options"=>array(
					array("value"=>"I", "caption"=>"Individual"),
					array("value"=>"B", "caption"=>"Business")
				),
				"caption"=>"Organization type"
			);
			$fields["x_customer_tax_id_title"] = array(
				"type"=>"separator", "name"=>"", "value"=>"",
				"caption"=>"<b>Please enter your Tax ID / SSN number</b>"
			);
			$fields["x_customer_tax_id"] = array(
				"type"=>"text", "name"=>"form[x_customer_tax_id]", "value"=>"",
				"caption"=>"Your Tax ID or SSN",
				"hint"=>"Numbers only. No spaces or additional characters"
			);
			$fields["x_drivers_license_num_title"] = array(
				"type"=>"separator", "name"=>"", "value"=>"",
				"caption"=>"<b>OR Driver license information</b>"
			);
			$fields["x_drivers_license_num"] = array(
				"type"=>"text", "name"=>"form[x_drivers_license_num]", "value"=>"",
				"caption"=>"Drivers license number"
			);
			$fields["x_drivers_license_state"] = array(
				"type"=>"text", "name"=>"form[x_drivers_license_state]", "value"=>"",
				"caption"=>"Drivers license state",
				"hint" =>"Please enter 2 charactes state code"
			);
			$fields["x_drivers_license_dob"] = array(
				"type"=>"text", "name"=>"form[x_drivers_license_dob]", "value"=>"",
				"caption"=>"DOB on drivers license",
				"hint" => "Please use MM-DD-YYYY format"
			);
		}
		return $fields;
	}

	function getValidatorJS()
	{
		$validators = parent::getValidatorJS();
		if ($this->settings_vars[$this->id."_Secure_Source_eCheck"]["value"] == "YES")
		{
			$validators = $validators. "\n".
				"tax_id_entered = validateText(frm.elements['form[x_customer_tax_id]'].value);\n".
				"if(!tax_id_entered && (!validateText(frm.elements['form[x_drivers_license_num]'].value) || !validateText(frm.elements['form[x_drivers_license_state]'].value) || !validateText(frm.elements['form[x_drivers_license_dob]'].value))){\n".
				"	alert('Please enter your tax id OR fill in drivers license data');\n".
				"	frm.elements['form[x_customer_tax_id]'].focus();\n".
				"	return false;\n".
				"}\n";
		}
		return $validators;
	}

	function isTestMode()
	{
		if ($this->settings_vars[$this->id."_Test_Request"]["value"] == "TRUE")
		{
			$this->testMode = true;
		}
		else
		{
			$this->testMode = false;
		}
		return $this->testMode;
	}

	function process_curl($post_data, $post_url)
	{
		global $settings;
		$c = curl_init($post_url);

		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_VERBOSE, 0);
		curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		@set_time_limit(3000);

		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
		$buffer = curl_exec($c);

		if (curl_errno($c) > 0)
		{
			$this->is_error = true;
			$this->error_message = curl_error($c);
			return false;
		}

		if (!$buffer)
		{
			return false;
		}
		return $buffer;
	}

	public function replaceDataKeys()
	{
		return array('x_bank_aba_code', 'x_bank_acct_num', 'x_login', 'x_tran_key', 'x_drivers_license_num', 'x_drivers_license_dob', 'x_drivers_license_state');
	}

	function process($db, $user, $order, $post_form)
	{
		global $settings;

		if (isset($post_form) && is_array($post_form))
		{
			//build post data for query
			$post_data = "";
			// get post_form, common, settings and custom vars
			reset($this->common_vars);
			//echeck datas

			$bank_account_types = array(
				"checking" => "CHECKING",
				"business_checking" => "BUSINESSCHECKING",
				"savings" => "SAVINGS",
			);
			// = $post_form["check_bank_account_type"];

			$post_data = "".
				"x_bank_aba_code=".urlencode($post_form["check_bank_aba_code"]).
				"&x_bank_acct_num=".urlencode($post_form["check_bank_account_number"]).
				"&x_bank_acct_type=".urlencode($bank_account_types[$post_form["check_bank_account_type"]]).
				"&x_bank_name=".urlencode($post_form["check_bank_name"]).
				"&x_bank_acct_name=".urlencode($post_form["check_bank_account_name"]).
				"&x_type=".urlencode("AUTH_CAPTURE").
				"&x_echeck_type=".urlencode("WEB");

			if ($this->settings_vars[$this->id."_Secure_Source_eCheck"]["value"] == "YES")
			{
				$post_data = $post_data.
					"&x_customer_organization_type=".urlencode($post_form["x_customer_organization_type"]);

				if (trim($post_form["x_customer_tax_id"]) != "")
				{
					$post_data = $post_data.
						"&x_customer_tax_id=".urlencode($post_form["x_customer_tax_id"]);
				}
				else
				{
					$post_data = $post_data.
						"&x_drivers_license_num=".urlencode($post_form["x_drivers_license_num"]).
						"&x_drivers_license_state=".urlencode($post_form["x_drivers_license_state"]).
						"&x_drivers_license_dob=".urlencode($post_form["x_drivers_license_dob"]);
				}
			}

			//billing data
			$post_data = $post_data.
				"&x_first_name=".urlencode($this->common_vars["billing_first_name"]["value"]).
				"&x_last_name=".urlencode($this->common_vars["billing_last_name"]["value"]).
				"&x_company=".urlencode($this->common_vars["billing_company"]["value"]).
				"&x_address=".urlencode($this->common_vars["billing_address"]["value"]).
				"&x_city=".urlencode($this->common_vars["billing_city"]["value"]).
				"&x_state=".urlencode($this->common_vars["billing_state"]["value"]).
				"&x_zip=".urlencode($this->common_vars["billing_zip"]["value"]).
				"&x_country=".urlencode($this->common_vars["billing_country"]["value"]).
				"&x_phone=".urlencode($this->common_vars["billing_phone"]["value"]).
				"&x_email=".urlencode($this->common_vars["billing_email"]["value"]).
				"&x_recurring_billing=NO";

			//shipping data
			if ($this->common_vars['shipping_required']['value'])
			{
				$post_data = $post_data.
					"&x_ship_to_last_name=".urlencode($this->common_vars["shipping_name"]["value"]).
					"&x_ship_to_address=".urlencode($this->common_vars["shipping_address"]["value"]).
					"&x_ship_to_city=".urlencode($this->common_vars["shipping_city"]["value"]).
					"&x_ship_to_state=".urlencode($this->common_vars["shipping_state"]["value"]).
					"&x_ship_to_zip=".urlencode($this->common_vars["shipping_zip"]["value"]).
					"&x_ship_to_country=".urlencode($this->common_vars["shipping_country"]["value"]);
			}

			$post_data = $post_data.
				//order data
				"&x_invoice_num=".urlencode($this->common_vars["order_id"]["value"]).
				"&x_po_num=".urlencode($this->common_vars["order_id"]["value"]).
				"&x_amount=".urlencode(number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")).
				//"&x_trans_id=".($this->common_vars["order_security_id"]["value"]).
				"&x_tax=".urlencode(number_format($this->common_vars["order_tax_amount"]["value"], 2, ".", "")).

				//setting from admin area
				"&x_login=".urlencode($this->settings_vars[$this->id."_Login"]["value"]).
				"&x_tran_key=".urlencode($this->settings_vars[$this->id."_Transaction_Key"]["value"]).
				"&x_test_request=".urlencode($this->settings_vars[$this->id."_Test_Request"]["value"]).

				//advanced gateway data
				"&x_delim_char=".urlencode("~").
				"&x_delim_data=".urlencode("True").
				"&x_version=".urlencode("3.1").
				"&x_relay_response=".urlencode("False").
				"&x_method=".urlencode("ECHECK").
				"&x_freight=".urlencode(number_format($this->common_vars["order_shipping_amount"]["value"], 2, ".", "")).
				"&x_customer_ip=".urlencode($_SERVER["REMOTE_ADDR"]).
				"&x_duplicate_window=0".
				"&x_solution_id=".urlencode("AAA170440");

			//post data to authorize.net
			$_post_url = trim($this->url_to_gateway)==""?"https://secure.authorize.net/gateway/transact.dll":trim($this->url_to_gateway);

			$buffer = $this->process_curl($post_data, $_post_url);

			$requestData = array();
			parse_str($post_data, $requestData);

			$this->log("process Request:\n".$_post_url, $requestData);
			$this->log("process Response:\n".$buffer);

			if ($buffer)
			{
				$data = explode("~", $buffer);
				// Approved
				if ($data[0] == "1")
				{
					$this->createTransaction($db, $user, $order, explode("~", $buffer), "", "", "", "", $data[6]);
					$this->is_error = false;
					$this->error_message = "";

					return "Received";
				}
				else
				{
					$this->is_error = true;

					switch($data[0])
					{
						case "2" :
						{
							$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: ".$data[3];
							break;
						}
						case "3" :
						{
							$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: ".$data[3];
							break;
						}
						default :
						{
							$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
							break;
						}
					}
				}
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = "Incorrect form params. Please contact site administartor.";
			//return false;
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $buffer, '', false);
		}

		return !$this->is_error;
	}
}
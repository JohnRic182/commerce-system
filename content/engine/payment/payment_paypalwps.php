<?php 

	require_once(dirname(__FILE__).'/payment_paypalipn.php');

	$payment_processor_id = "paypalwps";
	$payment_processor_name = "PayPal Website Payments Standard";
	$payment_processor_class = "PAYMENT_PAYPALWPS";
	$payment_processor_type = "ipn";
	
	class PAYMENT_PAYPALWPS extends PAYMENT_PAYPALIPN
	{
		function PAYMENT_PAYPALWPS()
		{
			parent::PAYMENT_PAYPALIPN();
			
			$this->id = "paypalwps";
			$this->name = "PayPal Website Payments Standard";
			$this->class_name = "PAYMENT_PAYPALWPS";
			$this->type = "ipn";
			$this->description = "";
			$this->post_url = "https://www.paypal.com/cgi-bin/webscr";
			$this->steps = 2;
			$this->possible_payment_delay = true;
			$this->possible_payment_delay_timeout = 30; // seconds
			$this->allow_change_gateway_url = false;

			$this->buttonSource = 'PCart_cart_IPN_US';

			return $this;
		}
	}

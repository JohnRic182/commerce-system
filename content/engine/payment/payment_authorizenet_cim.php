<?php 
	$payment_processor_id = "authorizenet_cim";
	$payment_processor_name = "Authorize.Net CIM";
	$payment_processor_class = "PAYMENT_AUTHORIZENET_CIM";
	$payment_processor_type = "cc";

	require_once dirname(dirname(dirname(__FILE__))).'/vendors/anet_php_sdk/AuthorizeNet.php';

	class PAYMENT_AUTHORIZENET_CIM extends PAYMENT_PROCESSOR
	{
		function PAYMENT_AUTHORIZENET_CIM()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "authorizenet_cim";
			$this->name = "Authorize.Net CIM";
			$this->class_name = "PAYMENT_AUTHORIZENET_CIM";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->support_ccs = true;
			$this->need_cc_codes = true;
			return $this;
		}

		public function replaceDataKeys()
		{
			return array('x_card_num', 'x_card_code', 'x_login', 'x_tran_key', 'x_exp_date',
				'card_num', 'card_code', 'login', 'tran_key', 'exp_date'
			);
		}

		function getPaymentForm($db)
		{
			$fields = parent::getPaymentForm($db);

			$saveCreditCardAdded = false;

			/** @var ORDER $order */
			$order = isset($this->order) ? $this->order : null;
			if ($order && $order instanceof ORDER && $order->hasRecurringBillingItems())
			{
				$fields['save_credit_card'] = array('type' => 'static', 'caption' => 'This card will be securely stored for future recurring payments', 'wrapperClass'=>'payment-form-cc-will-be-stored');
				$saveCreditCardAdded = true;
			}

			if ($this->isCardinalCommerceEnabled())
			{
				$fields["do_cmpi_lookup"] = array("type" => "hidden", "name"=> "do_cmpi_lookup", "value" => 1);
			}

			if (!$saveCreditCardAdded && $this->supportsPaymentProfiles() && isset($this->user) && !is_null($this->user) && isset($this->user->auth_ok) && $this->user->auth_ok && (!isset($this->user->express) || !$this->user->express))
			{
				$fields['save_credit_card'] = array('type' => 'checkbox', 'name' => 'form[save_credit_card]', 'value' => '1', 'caption' => 'Save this card for later use');
			}

			return $fields;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Test_Request"]["value"] == "TRUE")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		function normalize_phone($phone)
		{
			$result = "";
			for ($i=0; $i<strlen($phone); $i++)
			{
				$result = $result.(is_numeric($phone[$i])?$phone[$i]:"");
			}

			return  $result;
		}

		protected function processPaymentWithProfile($db, $user, $order, $postForm, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			$transaction = new AuthorizeNetTransaction();
			$transaction->amount = number_format($postForm['amount'], 2, ".", "");
			$transaction->tax->amount = number_format($postForm['tax_amount'], 2, ".", "");
			$transaction->order->invoiceNumber = $postForm['order_id'];
			$transaction->order->purchaseOrderNumber = $postForm['order_id'];
			$transaction->shipping->amount = number_format($postForm['shipping_amount'], 2, ".", "");

			$idParts = explode('||', $paymentProfile->getExternalProfileId());
			if (count($idParts) != 2)
			{
				$this->is_error = true;
				$this->error_message = 'Invalid payment profile';
				return false;
			}

			$customerProfileId = $idParts[0];
			$profileId = $idParts[1];

			$authNetCIM = $this->getAuthNetCIM();
			$transaction->customerProfileId = $customerProfileId;
			$transaction->customerPaymentProfileId = $profileId;

			$this->log('request: ', json_encode($transaction));

			// Both customerProfileId and customerPaymentProfileId should be numeric based from http://www.authorize.net/content/dam/authorize/documents/CIM_XML_guide.pdf(page 40)
			if (!is_numeric($transaction->customerProfileId))
			{
				$this->is_error = true;
				$this->error_message = 'Error: Invalid Customer Profile';
				return false;
			}

			if (!is_numeric($transaction->customerPaymentProfileId))
			{
				$this->is_error = true;
				$this->error_message = 'Error: Invalid Customer Payment Profile';
				return false;
			}

			$response = $authNetCIM->createCustomerProfileTransaction(
				$this->settings_vars[$this->id . '_Auth_Type']['value'] == 'Auth-Capture' ? 'AuthCapture' : 'AuthOnly',
				$transaction,
				//'x_duplicate_window='.$this->settings_vars[$this->id . "_Duplicate_Window"]["value"].
				'&x_solution_id=AAA170440' .
				($this->isTestMode() ? '&x_test_request=TRUE' : '')
			);

			$this->log('response: ', $response->response);

			if ($response->isOk())
			{
				$transaction = $response->getTransactionResponse();

				if ($transaction->approved || $transaction->held)
				{
					$this->createTransaction($db, $user, $order, $transaction->response, "",
						//custom 1-transaction type, 2-amount, 3-trans_id
						$this->settings_vars[$this->id . "_Auth_Type"]["value"] == "Auth-Capture" ? "AUTH_CAPTURE" : "AUTH_ONLY",
						($this->settings_vars[$this->id . "_Auth_Type"]["value"] != "Auth-Capture" ?
							number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")
							:
							""
						),
						$transaction->transaction_id,
						$transaction->transaction_id
					);

					$this->is_error = false;
					$this->error_message = "";

					return $this->settings_vars[$this->id . '_Auth_Type']['value'] == 'Auth-Capture' && $transaction->approved ? ORDER::PAYMENT_STATUS_RECEIVED : ORDER::PAYMENT_STATUS_PENDING;
				}
				else
				{
					$this->is_error = true;

					if ($transaction->declined)
					{
						$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: " . $transaction->response_reason_text;
					} else if ($transaction->error)
					{
						$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: " . $transaction->response_reason_text;
					} else
					{
						$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
					}
				}
			}
			else
			{
				$em = $response->getErrorMessage();
				$this->is_error = true;
				$this->error_message = $em == '' ? PAYMENT_TRANSACTION_ERROR_TEXT : $em;
			}

			{
			if ($this->is_error)
				$this->storeTransaction($db, $user, $order, $response->response, '', false);
			}

			return false;
		}

		/**
		 * @param DB $db
		 * @param USER $user
		 * @param ORDER $order
		 * @param $post_form
		 * @param string $x_authentication_indicator
		 * @param string $x_cardholder_authentication_value
		 *
		 * @return bool
		 */
		protected function processPaymentForm($db, $user, $order, $post_form, $x_authentication_indicator="", $x_cardholder_authentication_value="")
		{
			global $settings;

			$paymentProfileId = isset($post_form['payment_profile_id']) ? $post_form['payment_profile_id'] : false;
			$paymentProfileId = isset($post_form['payment_method_way']) && $post_form['payment_method_way'] != 'profile' ? false : $paymentProfileId;

			// get post_form, common, settings and custom vars
			reset($this->common_vars);

			if ($paymentProfileId)
			{
				$postForm = array(
					'amount' => $this->common_vars["order_total_amount"]["value"],
					'tax_amount' => $this->common_vars["order_tax_amount"]["value"],
					'order_id' => $this->common_vars["order_id"]["value"],
					'shipping_amount' => $this->common_vars["order_shipping_amount"]["value"],
				);

				$paymentProfile = $this->getPaymentProfileRepository()->getById($paymentProfileId, $user->getId());

				if (!$paymentProfile)
				{
					$this->is_error = true;
					$this->error_message = 'Invalid payment profile';
					return false;
				}

				if ($postForm['amount'] > 0)
				{
					$ret = $this->processPaymentWithProfile($db, $user, $order, $postForm, $paymentProfile);
				}
				else
				{
					$ret = true;
				}

				if ($ret && $order->hasRecurringBillingItems())
				{
					$this->createRecurringProfile($order, $paymentProfile);
				}

				return $ret;
			}
			else
			{
				if ($this->common_vars['order_total_amount']['value'] > 0)
				{
					$fields = array();
					$transaction = new AuthorizeNetAIM($settings['authorizenet_cim_Login'], $settings['authorizenet_cim_Transaction_Key']);
					$transaction->setSandbox($this->isTestMode());
					$fields = array_merge($fields, array(
						'first_name' => $post_form["cc_first_name"],
						'last_name' => $post_form["cc_last_name"],
						'card_num' => $post_form["cc_number"],
						'card_code' => $post_form["cc_cvv2"],
						'exp_date' => $post_form["cc_expiration_month"] . "/" . $post_form["cc_expiration_year"], //"MM/YYYY" format used
						'company' => $this->common_vars["billing_company"]["value"],
						'address' => $this->common_vars["billing_address"]["value"],
						'city' => $this->common_vars["billing_city"]["value"],
						'state' => $this->common_vars["billing_state"]["value"],
						'zip' => $this->common_vars["billing_zip"]["value"],
						'country' => $this->common_vars["billing_country_iso_a3"]["value"],
						'phone' => $this->normalize_phone($this->common_vars["billing_phone"]["value"]),
						'email' => $this->common_vars["billing_email"]["value"],
						'invoice_num' => $this->common_vars["order_id"]["value"],
						'po_num' => $this->common_vars["order_id"]["value"],
						'amount' => number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""),
						'tax' => number_format($this->common_vars["order_tax_amount"]["value"], 2, ".", ""),
						//'type' => '',
						'method' => 'CC',
						'freight' => number_format($this->common_vars["order_shipping_amount"]["value"], 2, ".", ""),
						'customer_ip' => $_SERVER["REMOTE_ADDR"],
						'duplicate_window' => $this->settings_vars[$this->id . "_Duplicate_Window"]["value"],
					));
					if ($this->common_vars['shipping_required']['value'])
					{
						$shipping_name_parts = explode(' ', trim($this->common_vars["shipping_name"]["value"]));
						$shipping_name_first = trim($shipping_name_parts[0]);
						unset($shipping_name_parts[0]);
						$shipping_name_last = trim(implode(' ', $shipping_name_parts));

						$fields = array_merge($fields, array(
							'ship_to_first_name' => $shipping_name_first,
							'ship_to_last_name' => $shipping_name_last,
							'ship_to_address' => $this->common_vars["shipping_address"]["value"],
							'ship_to_city' => $this->common_vars["shipping_city"]["value"],
							'ship_to_state' => $this->common_vars["shipping_state"]["value"],
							'ship_to_zip' => $this->common_vars["shipping_zip"]["value"],
							'ship_to_country' => $this->common_vars["shipping_country_iso_a3"]["value"],
						));
					}

					if ($this->isCardinalCommerceEnabled($post_form["cc_number"]))
					{
						if ($x_authentication_indicator != "" && $x_cardholder_authentication_value != "")
						{
							$fields = array_merge($fields, array(
								'authentication_indicator' => $x_authentication_indicator,
								'cardholder_authentication_value' => $x_cardholder_authentication_value,
							));
						}
					}

					$transaction->setFields($fields);

					$this->log('request: ', $fields);

					$response = $this->settings_vars[$this->id . '_Auth_Type']['value'] == 'Auth-Capture' ? $transaction->authorizeAndCapture() : $transaction->authorizeOnly();

					$this->log('response: ', $response->response);

					// Approved
					if ($response->approved || $response->held)
					{
						//$transactionId = $response->transaction_id;
						$this->createTransaction($db, $user, $order, $response->response, "",
							//custom 1-transaction type, 2-amount, 3-trans_id
							$this->settings_vars[$this->id . "_Auth_Type"]["value"] == "Auth-Capture" ? "AUTH_CAPTURE" : "AUTH_ONLY",
							($this->settings_vars[$this->id . "_Auth_Type"]["value"] != "Auth-Capture" ?
								number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")
								:
								""
							),
							$response->transaction_id,
							$response->transaction_id
						);

						if ($this->support_ccs && $this->enable_ccs)
						{
							$card_data = array(
								"fn" => $post_form["cc_first_name"],
								"ln" => $post_form["cc_last_name"],
								"ct" => $post_form["cc_type"],
								"cc" => $post_form["cc_number"],
								"em" => $post_form["cc_expiration_month"],
								"ey" => $post_form["cc_expiration_year"]
							);
							$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]));
						}

						if (isset($post_form['save_credit_card']) || $order->hasRecurringBillingItems())
						{
							$paymentProfile = $this->createPaymentProfileFromTransaction($db, $user, $post_form);

							if($order->hasRecurringBillingItems())
							{
								if (!$paymentProfile)
								{
									//TODO: Need to void the order
//									$transaction = new AuthorizeNetAIM($settings['authorizenet_cim_Login'], $settings['authorizenet_cim_Transaction_Key']);
//									$transaction->setSandbox($this->isTestMode());
//									$response = $transaction->void($transactionId);
//
//									$this->storeTransaction($db, $user, $order, $response->response, '', false);
//									return false;
								}
								else
								{
									$this->createRecurringProfile($order, $paymentProfile);
								}
							}
						}

						$this->is_error = false;
						$this->error_message = "";

						return $this->settings_vars[$this->id . '_Auth_Type']['value'] == 'Auth-Capture' && $response->approved ? ORDER::PAYMENT_STATUS_RECEIVED : ORDER::PAYMENT_STATUS_PENDING;
					}
					else
					{
						$this->is_error = true;

						if ($response->declined)
						{
							$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: " . $response->response_reason_text;
						}
						else if ($response->error)
						{
							$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: " . $response->response_reason_text;
						}
						else
						{
							$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
						}
					}

					if ($this->is_error)
					{
						$this->storeTransaction($db, $user, $order, $response->response, '', false);
					}
				}
				else
				{
					if ($order->hasRecurringBillingItems())
					{
						$paymentProfile = $this->createPaymentProfileFromTransaction($db, $user, $post_form);

						if($order->hasRecurringBillingItems())
						{
							if (!$paymentProfile)
							{
								$this->is_error = true;
								return false;
							}
							else
							{
								$this->createRecurringProfile($order, $paymentProfile);
								$this->createTransaction($db, $user, $order, '$0.00 order', "",
									//custom 1-transaction type, 2-amount, 3-trans_id
									"AUTH_CAPTURE",
									'',
									'',
									''
								);
								return ORDER::PAYMENT_STATUS_RECEIVED;
							}
						}
					}
				}

				return false;
			}
		}

		/**
		 * @param ORDER $order
		 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
		 */
		protected function createRecurringProfile(ORDER $order, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			$orderRepository = DataAccess_OrderRepository::getInstance();

			/** @var Model_LineItem $item */
			foreach ($order->lineItems as $item)
			{
				if ($item->getEnableRecurringBilling() && !is_null($item->getRecurringBillingData()))
				{
					/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
					$recurringBillingData = $item->getRecurringBillingData();

					$recurringBillingData->setPaymentProfileId($paymentProfile->getId());

					$item->setRecurringBillingData($recurringBillingData);

					$orderRepository->persistLineItem($order, $item);
				}
			}
		}

		/**
		 * @param DB $db
		 * @param USER $user
		 * @param $post_form
		 *
		 * @return null|PaymentProfiles_Model_PaymentProfile
		 */
		protected function createPaymentProfileFromTransaction(DB $db, USER $user, $post_form)
		{
			$paymentProfile = new PaymentProfiles_Model_PaymentProfile();

			$billingData = $paymentProfile->getBillingData();

			/** @var Model_Address $address */
			$address = $this->common_vars['billingAddress']['value'];

			// override names
			$address->setFirstName($post_form['cc_first_name']);
			$address->setLastName($post_form['cc_last_name']);

			$billingData->setAddress($address);
			$billingData->setName(trim(trim($post_form['cc_first_name']).' '.trim($post_form['cc_last_name'])));
			$billingData->setPhone($this->common_vars['billing_phone']['value']);
			//$billingData->setCcType($ccType);
			$billingData->setCcNumber($post_form['cc_number']);
			$billingData->setCcExpirationMonth($post_form['cc_expiration_month']);
			$billingData->setCcExpirationYear($post_form['cc_expiration_year']);
			$billingData->setCcExpirationDate($post_form['cc_expiration_month'].'/'.$post_form['cc_expiration_year']);

			$paymentProfile->setBillingData($billingData);
			$paymentProfile->setUserId($user->getId());
			//$paymentProfile->setExternalProfileId($token);
			$paymentProfile->setPaymentMethodId($this->db_id);

			$ret = $this->addPaymentProfile($db, $user, $paymentProfile);

			if ($ret)
			{
				$this->getPaymentProfileRepository()->persist($paymentProfile);
				return $paymentProfile;
			}

			return null;
		}

		function process($db, $user, $order, $post_form)
		{
			if ($this->isCardinalCommerceEnabled())
			{
				$authorizeResult = $this->processCardinalCommerce($db, $user, $order, $post_form);
			}
			else
			{
				$authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form);
			}

			return $this->is_error ? false : $authorizeResult;
		}

		protected function processCardinalCommerce($db, $user, $order, &$post_form)
		{
			global $settings;

			$authorizeResult = false;

			$cardinal = new CardinalCommerce();
			$cardinal->common_vars = $this->common_vars;

			if (isset($_REQUEST["do_cmpi_lookup"]))
			{
				$cardinal_lookup_xml = $cardinal->getLookupXML(
					$post_form["cc_first_name"],
					$post_form["cc_last_name"],
					$post_form["cc_number"],
					$post_form["cc_expiration_month"],
					$post_form["cc_expiration_year"],
					$this->currency["code"]
				);

				$cardinal_lookup_result = $cardinal->processRequest($cardinal_lookup_xml);

				if ($cardinal_lookup_result)
				{
					if ($cardinal_lookup_result["CardinalMPI"][0]["Enrolled"][0] == "Y")
					{
						$this->redirect = true;
						$this->redirectURL = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=payment_validation";

						$_SESSION["CardinalPostForm"] = $post_form;
						$_SESSION["CardinalTransactionId"] = $cardinal_lookup_result["CardinalMPI"][0]["TransactionId"][0];
						$_SESSION["CardinalACSUrl"] = $cardinal_lookup_result["CardinalMPI"][0]["ACSUrl"][0];
						$_SESSION["CardinalPayload"] = $cardinal_lookup_result["CardinalMPI"][0]["Payload"][0];
						//validator will replace ORDER_PROCESS_PAYMENT_VALIDATION with ORDER_PROCESS_PAYMENT
						$_SESSION["CardinalNotifyUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT_VALIDATION."&do_cmpi_authenticate=true";
						$_SESSION["CardinalRedirectUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"];

						return false;
					}
					else
					{
						//do not redirect, process simple authorize transaction
						$authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form);
					}
				}
				else
				{
					if ($cardinal->is_fatal_error)
					{
						$this->is_error = true;
						$this->error_message = $cardinal->error_message;
					}
					else
					{
						$authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form);
					}
				}
			}

			if (isset($_REQUEST["do_cmpi_authenticate"]) && isset($_SESSION["CardinalPostForm"]) && isset($_REQUEST["PaRes"]))
			{
				$post_form = $_SESSION["CardinalPostForm"];

				unset($_SESSION["CardinalPostForm"]);

				$cardinal_auth_xml = $cardinal->getAuthenticateXML($_REQUEST["PaRes"]);
				$cardinal_auth_result = $cardinal->processRequest($cardinal_auth_xml);

				if ($cardinal_auth_result)
				{
					$EciFlag = $cardinal_auth_result["CardinalMPI"][0]["EciFlag"][0];
					$PAResStatus = $cardinal_auth_result["CardinalMPI"][0]["PAResStatus"][0];
					$Cavv = $cardinal_auth_result["CardinalMPI"][0]["Cavv"][0];
					$SignatureVerification = $cardinal_auth_result["CardinalMPI"][0]["SignatureVerification"][0];

					if ($SignatureVerification == "Y" && $PAResStatus != "N")
					{
						switch ($PAResStatus)
						{
							case "Y" :
							case "A" : $authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form, $EciFlag, $Cavv); break;
							case "U" : $authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form); break;
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = "Your card can not be validated. Please try other card or other payment method";
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $cardinal->error_message;
				}
			}

			return $authorizeResult;
		}

		public function capture($order_data = false, $capture_amount = false)
		{
			global $settings, $order, $db;

			$this->is_error = false;

			$oldPaymentStatus = $order->getPaymentStatus();


			//check is transaction completed
			if ($order_data["custom1"] != "AUTH_ONLY")
			{
				$this->is_error = true;
				$this->error_message = "Transaction already completed";
				return false;
			}

			$total_amount = $order_data['total_amount'];

			if (floatval($order_data['gift_cert_amount']) > 0)
			{
				$total_amount -= $order_data['gift_cert_amount'];

				if ($total_amount < 0) $total_amount = 0;
			}

			//get captured total amount & x_trans_id
			$transaction_id = $order_data["custom3"];

			if ($total_amount > $order_data["custom2"])
			{
				$this->is_error = true;
				$this->error_message = "New total amount can't be greater than authorized amount";
				return false;
			}

			$fields = array(
				'amount' => number_format($total_amount, 2, ".", ""),
				'tax' => number_format($order_data["tax_amount"], 2, ".", ""),
				'freight' => number_format($order_data["shipping_amount"], 2, ".", ""),
				'invoice_num' => $order_data["order_num"],
				'po_num' => $order_data["order_num"],
			);

			$transaction = new AuthorizeNetAIM($settings['authorizenet_cim_Login'], $settings['authorizenet_cim_Transaction_Key']);
			$transaction->setSandbox($this->isTestMode());
			$transaction->setFields($fields);

			$this->log('request: ', $fields);

			$response = $transaction->priorAuthCapture($transaction_id);

			$this->log('response: ', $response->response);

			$userTmp = new stdClass();
			$userTmp->id = $order_data["uid"];

			$orderTmp = new stdClass();
			$orderTmp->oid = $order_data["oid"];
			$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
			$orderTmp->totalAmount = $order_data["total_amount"];
			$orderTmp->shippingAmount = $order_data["shipping_amount"];
			$orderTmp->taxAmount = $order_data["tax_amount"];
			$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

			if ($response->approved)
			{
				$order->setPaymentStatus('Received');

				$db->reset();
				$db->assignStr("custom1", "");
				$db->assignStr("custom2", "");
				$db->assignStr("custom3", "");
				$db->assignStr("payment_status", $order->getPaymentStatus());
				$db->update(DB_PREFIX."orders", "WHERE oid= ".$order_data['oid']);

				$this->createTransaction($db, $userTmp, $orderTmp,
					$response->response,
					"", "PRIOR_AUTH_CAPTURE", "", ""
				);
			}
			else
			{
				$this->is_error = true;

				if ($response->declined)
				{
					$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: ".$response->response_reason_text;
				}
				else if ($response->error)
				{
					$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: ".$response->response_reason_text;
				}
				else
				{
					$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
				}

				$this->storeTransaction($db, $userTmp, $orderTmp, $response->response, '', false);
			}

			$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

			return !$this->is_error;
		}

		public function supportsCapture($order_data = false)
		{
			return $this->capturableAmount($order_data) > 0;
		}

		function supportsPartialCapture($order_data = false)
		{
			return false;
		}

		public function capturableAmount($order_data = false)
		{
			if ($order_data['custom1'] == 'AUTH_ONLY' && $order_data['payment_status'] == 'Pending')
			{
				return $order_data['custom2'];
			}

			return 0;
		}

		public function capturedAmount($order_data = false)
		{
			if ($order_data["custom1"] == "AUTH_ONLY")
			{
				return $order_data['custom2'];
			}
			else if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_RECEIVED)
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'];
			}

			return false;
		}

		public function supportsPaymentProfiles()
		{
			return true;
		}

		/**
		 * @param DB $db
		 * @param mixed $ext_profile_id
		 * @param bool $active
		 * @return mixed
		 */
		function getPaymentProfileDetails($db, $ext_profile_id, $active = false)
		{
			$db->selectOne("SELECT * FROM ".DB_PREFIX."users_payment_profiles WHERE ext_profile_id = '".$db->escape($ext_profile_id)."'");

			$data = unserialize($db->col['billing_data']);
			$data['usid'] = $db->col['usid'];

			$country = getCountryData($db, false, $data['country']);
			$data['country_id'] = $country['coid'];

			$state = getStateData($db, false, $data['state']);
			$data['state_id'] = $state['stid'];

			$data['name'] = $data['first_name'].' '.$data['last_name'];


			return $data;
		}

		/**
		 * Add a new payment profile
		 *
		 * @param DB $db
		 * @param USER $user
		 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
		 *
		 * @return bool|void
		 */
		public function addPaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			/** @var PaymentProfiles_Model_BillingData $billingData */
			$billingData = $paymentProfile->getBillingData();

			/** @var Model_Address $billingAddress */
			$billingAddress = $billingData->getAddress();

			$authNetCIM = $this->getAuthNetCIM();
			$customerProfile = new AuthorizeNetCustomer();
			$customerProfile->description = $billingAddress->getFirstName().' '.$billingAddress->getLastName().' Profile';
			$customerProfile->merchantCustomerId = $user->getId();
			$customerProfile->email = $user->data['email'];

			$response = $authNetCIM->createCustomerProfile($customerProfile);
			if ($response->isOk())
			{
				$customerProfileId = $response->getCustomerProfileId();
			}
			else
			{
				if ($response->getMessageCode() == 'E00039')
				{
					$customerProfileId = trim(str_replace(' already exists.', '', str_replace('A duplicate record with ID ', '', $response->getMessageText())));
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $response->getErrorMessage();
					return false;
				}
			}

			$profile = new AuthorizeNetPaymentProfile();
			$profile->customerType = 'business';
			$profile->billTo->firstName = $billingAddress->getFirstName();
			$profile->billTo->lastName = $billingAddress->getLastName();
			$profile->billTo->company = $billingAddress->getCompany();
			$profile->billTo->address = $billingAddress->getAddressLine1().(trim($billingAddress->getAddressLine2()) != '' ? (', '.$billingAddress->getAddressLine2()) : '');
			$profile->billTo->city = $billingAddress->getCity();
			$profile->billTo->country = $billingAddress->getCountryIso2();
			$profile->billTo->state = trim($billingAddress->getStateCode()) != '' ? $billingAddress->getStateCode() : $billingAddress->getStateName();
			$profile->billTo->zip = $billingAddress->getZip();
			$profile->billTo->phoneNumber = $user->data['phone'];

			$profile->payment->creditCard->cardNumber = $billingData->getCcNumber();
			$profile->payment->creditCard->expirationDate = $billingData->getCcExpirationMonth().substr($billingData->getCcExpirationYear(), -2);
			if (trim($billingData->getCcCode()) != '') $profile->payment->creditCard->cardCode = $billingData->getCcCode();

			$response = $authNetCIM->createCustomerPaymentProfile($customerProfileId, $profile, $this->isTestMode() ? 'none' : 'liveMode');
			if ($response->isOk())
			{
				$paymentProfile->setExternalProfileId($customerProfileId.'||'.$response->getPaymentProfileId());
				return true;
			}
			else
			{
				$errorMessage = $response->getErrorMessage();

				if ($response->getMessageCode() == 'E00039')
				{ //$this->error_message == 'A duplicate customer payment profile already exists. E00039')
					$response = $authNetCIM->getCustomerProfile($customerProfileId);
					$paymentProfiles = $response->xpath('//profile/paymentProfiles');

					foreach ($paymentProfiles as $p)
					{
						$firstName = isset($p->billTo->firstName) ? trim((string)$p->billTo->firstName) : '';
						$lastName = isset($p->billTo->lastName) ? trim((string)$p->billTo->lastName) : '';
						$address = isset($p->billTo->address) ? trim((string)$p->billTo->address) : '';
						$city = isset($p->billTo->city) ? trim((string)$p->billTo->city) : '';
						$state = isset($p->billTo->state) ? trim((string)$p->billTo->state) : '';
						$zip = isset($p->billTo->zip) ? trim((string)$p->billTo->zip) : '';
						$country = isset($p->billTo->country) ? trim((string)$p->billTo->country) : '';
						$phoneNumber = isset($p->billTo->phoneNumber) ? trim((string)$p->billTo->phoneNumber) : '';

						$ccNumber = isset($p->payment->creditCard->cardNumber) ? trim((string)$p->payment->creditCard->cardNumber) : '';
						//Can't do expiration

						if (
							$firstName == $profile->billTo->firstName &&
							$lastName == $profile->billTo->lastName &&
							$address == $profile->billTo->address &&
							$city == $profile->billTo->city &&
							$state == $profile->billTo->state &&
							$zip == $profile->billTo->zip &&
							$country == $profile->billTo->country &&
							$phoneNumber == $profile->billTo->phoneNumber &&
							$ccNumber == 'XXXX'.substr($profile->payment->creditCard->cardNumber, -4)
						)
						{
							$profileId = (string)$p->customerPaymentProfileId;
							$paymentProfile->setExternalProfileId($customerProfileId.'||'.$profileId);
							return true;
						}
					}
				}

				$this->is_error = true;
				$this->error_message = $errorMessage;

				return false;
			}
		}

		/**
		 * Update payment profile
		 *
		 * @param DB $db
		 * @param USER $user
		 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
		 *
		 * @return bool
		 */
		public function updatePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			$idParts = explode('||', $paymentProfile->getExternalProfileId());
			if (count($idParts) != 2)
			{
				$this->is_error = true;
				$this->error_message = 'Invalid external profile id';
				return false;
			}

			$customerProfileId = $idParts[0];
			$profileId = $idParts[1];

			/** @var PaymentProfiles_Model_BillingData $billingData */
			$billingData = $paymentProfile->getBillingData();

			/** @var Model_Address $billingAddress */
			$billingAddress = $billingData->getAddress();

			$externalProfileData = explode('||', $paymentProfile->getExternalProfileId());
			if (count($externalProfileData) > 0 && count($externalProfileData) == 2)
			{
				// Both customerProfileId and customerPaymentProfileId should be numeric based from http://www.authorize.net/content/dam/authorize/documents/CIM_XML_guide.pdf(page 40)
				if (!is_numeric($externalProfileData[0]))
				{
					$this->is_error = true;
					$this->error_message = 'Error: Invalid Customer Profile';
					return false;
				}

				if (!is_numeric($externalProfileData[1]))
				{
					$this->is_error = true;
					$this->error_message = 'Error: Invalid Customer Payment Profile';
					return false;
				}
			}

			$authNetCIM = $this->getAuthNetCIM();

			$profile = new AuthorizeNetPaymentProfile();
			$profile->customerType = 'business';
			$profile->billTo->firstName = $billingAddress->getFirstName();
			$profile->billTo->lastName = $billingAddress->getLastName();
			$profile->billTo->company = $billingAddress->getCompany();
			$profile->billTo->address = $billingAddress->getAddressLine1().(trim($billingAddress->getAddressLine2()) != '' ? (', '.$billingAddress->getAddressLine2()) : '');
			$profile->billTo->city = $billingAddress->getCity();
			$profile->billTo->country = $billingAddress->getCountryIso2();
			$profile->billTo->state = trim($billingAddress->getStateCode()) != '' ? $billingAddress->getStateCode() : $billingAddress->getStateName();
			$profile->billTo->zip = $billingAddress->getZip();
			$profile->billTo->phoneNumber = $user->data['phone'];

			$profile->payment->creditCard->cardNumber = $billingData->getCcNumber();
			$profile->payment->creditCard->expirationDate = $billingData->getCcExpirationMonth().substr($billingData->getCcExpirationYear(), -2);
			if (trim($billingData->getCcCode()) != '') $profile->payment->creditCard->cardCode = $billingData->getCcCode();

			$response = $authNetCIM->updateCustomerPaymentProfile($customerProfileId, $profileId, $profile, $this->isTestMode() ? 'none' : 'liveMode');

			if ($response->isOk())
			{
				return true;
			}
			else
			{
				$this->is_error = true;
				$this->error_message = $response->getErrorMessage();
				return false;
			}
		}

		/**
		 * Remove payment profile
		 *
		 * @param $db
		 * @param $user
		 * @param $paymentProfile
		 *
		 * @return bool|void
		 */
		public function deletePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
		{
			$idParts = explode('||', $paymentProfile->getExternalProfileId());
			if (count($idParts) != 2)
			{
				$this->is_error = true;
				$this->error_message = 'Invalid external profile id';
				return false;
			}

			$customerProfileId = $idParts[0];
			$profileId = $idParts[1];

			$authNetCIM = $this->getAuthNetCIM();

			$response = $authNetCIM->deleteCustomerPaymentProfile($customerProfileId, $profileId);

			if ($response->isOk())
			{
				return true;
			}
			else
			{
				$this->is_error = true;
				$this->error_message = $response->getErrorMessage();
				//TODO: Do we return true or false
				return true;
			}
		}

		/**
		 * {@inheritdoc}
		 */
		public function supportsRecurringBillingProfiles()
		{
			global $settings;

			return $settings['firstdata_Payment_Profiles'] == 'Yes';
		}

		/**
		 * {@inheritdoc}
		 */
		public function useSchedulerForRecurringBilling()
		{
			return true;
		}

		public function processRecurringBillingPayment(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order)
		{
			global $db;

			$user = $order->getUser();

			$postForm = array(
				'amount' => $order->getTotalAmount(),
				'tax_amount' => $order->getTaxAmount(),
				'order_id' => $order->getOrderNumber(),
				'shipping_amount' => $order->getShippingAmount(),
			);

			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $this->getPaymentProfileRepository();

			/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
			$paymentProfile = $paymentProfileRepository->getById($recurringProfile->getPaymentProfileId());

			if (is_null($paymentProfile)) return 'fatal';

			$ret = $this->processPaymentWithProfile($db, $user, $order, $postForm, $paymentProfile);

			if ($ret !== false)
			{
				$order->setPaymentStatus($ret === true ? 'Received' : $ret);

				return 'success';
			}
			else
			{
				return $this->is_fatal ? 'fatal' : 'error';
			}
		}

		protected function getAuthNetCIM()
		{
			global $settings;

			$authNetCIM = new AuthorizeNetCIM($settings['authorizenet_cim_Login'], $settings['authorizenet_cim_Transaction_Key']);
			$authNetCIM->setSandbox($this->isTestMode());
			return $authNetCIM;
		}
	}

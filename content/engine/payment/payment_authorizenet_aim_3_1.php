<?php 
	$payment_processor_id = "authorizenet_aim_3_1";
	$payment_processor_name = "Authorize.Net AIM 3.1";
	$payment_processor_class = "PAYMENT_AUTHORIZENET_AIM_3_1";
	$payment_processor_type = "cc";

	class PAYMENT_AUTHORIZENET_AIM_3_1 extends PAYMENT_PROCESSOR
	{
		function PAYMENT_AUTHORIZENET_AIM_3_1()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "authorizenet_aim_3_1";
			$this->name = "Authorize.Net AIM 3.1";
			$this->class_name = "PAYMENT_AUTHORIZENET_AIM_3_1";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->support_ccs = true;
			$this->need_cc_codes = true;
			return $this;
		}

		public function replaceDataKeys()
		{
			return array('x_card_num', 'x_card_code', 'x_login', 'x_tran_key', 'x_exp_date');
		}

		function getPaymentForm($db)
		{
			$fields = parent::getPaymentForm($db);
			if ($this->isCardinalCommerceEnabled())
			{
				$fields["do_cmpi_lookup"] = array("type" => "hidden", "name"=> "do_cmpi_lookup", "value" => 1);
			}
			return $fields;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Test_Request"]["value"] == "TRUE")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		function process_curl($post_data, $post_url)
		{
			global $settings;
			$c = curl_init($post_url);

			if ($settings["ProxyAvailable"] == "YES")
			{
				//curl_setopt($c, CURLOPT_VERBOSE, 1);
				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}
				curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}

				curl_setopt($c, CURLOPT_TIMEOUT, 120);
			}

			curl_setopt($c, CURLOPT_VERBOSE, 0);
			curl_setopt($c, CURLOPT_HEADER, 0);
			curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/');
			curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

			@set_time_limit(3000);

			$buffer = curl_exec($c);
			if (curl_errno($c) > 0)
			{
				$this->is_error = true;
				$this->error_message = curl_error($c);
				return false;
			}

			return !$buffer ? false : $buffer;
		}

		function normalize_phone($phone)
		{
			$result = "";
			for ($i=0; $i<strlen($phone); $i++)
			{
				$result = $result.(is_numeric($phone[$i])?$phone[$i]:"");
			}

			return  $result;
		}

		function processAuthorize($db, $user, $order, $post_form, $x_authentication_indicator="", $x_cardholder_authentication_value="")
		{
			global $settings;

			//build post data for query
			$post_data = "";
			// get post_form, common, settings and custom vars
			reset($this->common_vars);
			$post_data = "".
				//form data
				"x_first_name=".urlencode($post_form["cc_first_name"]).
				"&x_last_name=".urlencode($post_form["cc_last_name"]).
				"&x_card_num=".urlencode($post_form["cc_number"]).
				"&x_card_code=".urlencode($post_form["cc_cvv2"]).
				"&x_exp_date=".urlencode($post_form["cc_expiration_month"]."/".$post_form["cc_expiration_year"]). //"MM/YYYY" format used

				//billing data
				"&x_company=".urlencode($this->common_vars["billing_company"]["value"]).
				"&x_address=".urlencode($this->common_vars["billing_address"]["value"]).
				"&x_city=".urlencode($this->common_vars["billing_city"]["value"]).
				"&x_state=".urlencode($this->common_vars["billing_state"]["value"]).
				"&x_zip=".urlencode($this->common_vars["billing_zip"]["value"]).
				"&x_country=".urlencode($this->common_vars["billing_country"]["value"]).
				"&x_phone=".urlencode($this->normalize_phone($this->common_vars["billing_phone"]["value"])).
				"&x_email=".urlencode($this->common_vars["billing_email"]["value"]);

			//shipping data
			if ($this->common_vars['shipping_required']['value'])
			{
				$shipping_name_parts = explode(' ', trim($this->common_vars["shipping_name"]["value"]));
				$shipping_name_first = trim($shipping_name_parts[0]);
				unset($shipping_name_parts[0]);
				$shipping_name_last = trim(implode(' ', $shipping_name_parts));

				$post_data .=
					"&x_ship_to_first_name=".urlencode($shipping_name_first).
					"&x_ship_to_last_name=".urlencode($shipping_name_last).
					"&x_ship_to_address=".urlencode($this->common_vars["shipping_address"]["value"]).
					"&x_ship_to_city=".urlencode($this->common_vars["shipping_city"]["value"]).
					"&x_ship_to_state=".urlencode($this->common_vars["shipping_state"]["value"]).
					"&x_ship_to_zip=".urlencode($this->common_vars["shipping_zip"]["value"]).
					"&x_ship_to_country=".urlencode($this->common_vars["shipping_country"]["value"]);
			}

			$post_data .=
				//order data
				"&x_invoice_num=".urlencode($this->common_vars["order_id"]["value"]).
				"&x_po_num=".urlencode($this->common_vars["order_id"]["value"]).
				"&x_amount=".urlencode(number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")).
				//"&x_trans_id=".($this->common_vars["order_security_id"]["value"]).
				"&x_tax=".urlencode(number_format($this->common_vars["order_tax_amount"]["value"], 2, ".", "")).

				//setting from admin area
				"&x_login=".urlencode($this->settings_vars[$this->id."_Login"]["value"]).
				"&x_tran_key=".urlencode($this->settings_vars[$this->id."_Transaction_Key"]["value"]).
				"&x_test_request=".urlencode($this->settings_vars[$this->id."_Test_Request"]["value"]).

				//advanced gateway data
				"&x_type=".urlencode($this->settings_vars[$this->id."_Auth_Type"]["value"] == "Auth-Capture" ? "AUTH_CAPTURE" : "AUTH_ONLY").
				"&x_delim_char=".urlencode("~").
				"&x_delim_data=".urlencode("True").
				"&x_encap_char=\"".
				"&x_version=".urlencode("3.1").
				"&x_relay_response=".urlencode("False").
				"&x_method=".urlencode("CC").
				"&x_freight=".urlencode(number_format($this->common_vars["order_shipping_amount"]["value"], 2, ".", "")).
				"&x_customer_ip=".urlencode($_SERVER["REMOTE_ADDR"]).
				"&x_duplicate_window=".urlencode($this->settings_vars[$this->id."_Duplicate_Window"]["value"]).
				"&x_solution_id=".urlencode("AAA170440");

			if ($this->isCardinalCommerceEnabled($post_form["cc_number"]))
			{
				if ($x_authentication_indicator != "" && $x_cardholder_authentication_value != "")
				{
					$post_data.=
						"&x_authentication_indicator=".urlencode($x_authentication_indicator).
						"&x_cardholder_authentication_value=".urlencode($x_cardholder_authentication_value);
				}
			}

			//post data to authorize.net
			$_post_url = trim($this->url_to_gateway)=="" ? "https://secure.authorize.net/gateway/transact.dll" : trim($this->url_to_gateway);

			$post_values = array();
			parse_str($post_data, $post_values);

			$this->log("processAuthorize Request:\n".$_post_url, $post_values);

			$buffer = $this->process_curl($post_data, $_post_url);

			if ($buffer)
			{
				$data = explode("~", $buffer);

				$temp = array();
				foreach ($data as $value)
				{
					$temp[] = trim(rtrim(ltrim($value, '"'), '"'));
				}
				$data = $temp;

				$this->cleanData($data);
				$this->log("processAuthorize Response:", $data);

				// Approved
				if ($data[0] == "1" || $data[0] == "4")
				{
					$avsResult = isset($data[5]) ? $data[5]: '';
					$cvvResult = isset($data[38]) ? $data[38]: '';

					$event = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_PAYMENT_METHOD_DATA_SET);
					$event->setOrder($order);
					$paymentParams = array();
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_AVS] =  $avsResult;
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CVV]  = $cvvResult;
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDFNAME]  = $post_form["cc_first_name"];
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDLNAME]  = $post_form["cc_last_name"];
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM]  = $post_form["cc_number"];
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDCVV2]  = $post_form["cc_cvv2"];
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPMONTH]  = $post_form["cc_expiration_month"];
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPYEAR]  = $post_form["cc_expiration_year"];
					$event->setPaymentMethodData($paymentParams);
					Events_EventHandler::handle($event);

					$this->createTransaction($db, $user, $order, $data, "", 
						//custom 1-transaction type, 2-amount, 3-trans_id
						$this->settings_vars[$this->id."_Auth_Type"]["value"] == "Auth-Capture" ? "AUTH_CAPTURE" : "AUTH_ONLY",
						($this->settings_vars[$this->id."_Auth_Type"]["value"] != "Auth-Capture" ? 
							number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "") 
							: 
							""
						),
						$data[6],
						$data[6]
					);

					if ($this->support_ccs && $this->enable_ccs)
					{
						$card_data = array(
							"fn"=>$post_form["cc_first_name"],
							"ln"=>$post_form["cc_last_name"],
							"ct"=>$post_form["cc_type"],
							"cc"=>$post_form["cc_number"],
							"em"=>$post_form["cc_expiration_month"], 
							"ey"=>$post_form["cc_expiration_year"]
						);
						$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]));
					}

					$this->is_error = false;
					$this->error_message = "";

					return $this->settings_vars[$this->id.'_Auth_Type']['value'] == 'Auth-Capture' && $data[0] == '1' ? ORDER::PAYMENT_STATUS_RECEIVED : ORDER::PAYMENT_STATUS_PENDING;
				}
				else
				{
					$this->is_error = true;

					switch ($data[0])
					{
						case "2" :
						{
							$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: ".$data[3];
							break;
						}
						case "3" :
						{
							$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: ".$data[3];
							break;
						}
						default :
						{
							$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
							break;
						}
					}
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Incorrect form params. Please contact site administrator.";
				//return false;
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $buffer, '', false);
			}

			return !$this->is_error;
		}

		function process($db, $user, $order, $post_form)
		{
			global $settings;
			global $backurl;
			$authorizeResult = false;

			//check cardinal centinel is enabled
			if ($this->isCardinalCommerceEnabled())
			{
				$authorizeResult = $this->processCardinalCommerce($db, $user, $order, $post_form);
			}
			else
			{
				$authorizeResult = $this->processAuthorize($db, $user, $order, $post_form);
			}

			return $this->is_error ? false : $authorizeResult;
		}

		protected function processCardinalCommerce($db, $user, $order, &$post_form)
		{
			global $settings;

			$authorizeResult = false;

			$cardinal = new CardinalCommerce();
			$cardinal->common_vars = $this->common_vars;

			if (isset($_REQUEST["do_cmpi_lookup"]))
			{
				$cardinal_lookup_xml = $cardinal->getLookupXML(
					$post_form["cc_first_name"],
					$post_form["cc_last_name"],
					$post_form["cc_number"],
					$post_form["cc_expiration_month"],
					$post_form["cc_expiration_year"],
					$this->currency["code"]
				);

				$cardinal_lookup_result = $cardinal->processRequest($cardinal_lookup_xml);

				if ($cardinal_lookup_result)
				{
					if ($cardinal_lookup_result["CardinalMPI"][0]["Enrolled"][0] == "Y")
					{
						$this->redirect = true;
						$this->redirectURL = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=payment_validation";

						$_SESSION["CardinalPostForm"] = $post_form;
						$_SESSION["CardinalTransactionId"] = $cardinal_lookup_result["CardinalMPI"][0]["TransactionId"][0];
						$_SESSION["CardinalACSUrl"] = $cardinal_lookup_result["CardinalMPI"][0]["ACSUrl"][0];
						$_SESSION["CardinalPayload"] = $cardinal_lookup_result["CardinalMPI"][0]["Payload"][0];
						//validator will replace ORDER_PROCESS_PAYMENT_VALIDATION with ORDER_PROCESS_PAYMENT
						$_SESSION["CardinalNotifyUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT_VALIDATION."&do_cmpi_authenticate=true";
						$_SESSION["CardinalRedirectUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"];

						return false;
					}
					else
					{
						//do not redirect, process simple authorize transaction
						$authorizeResult = $this->processAuthorize($db, $user, $order, $post_form);
					}
				}
				else
				{
					if ($cardinal->is_fatal_error)
					{
						$this->is_error = true;
						$this->error_message = $cardinal->error_message;
					}
					else
					{
						$authorizeResult = $this->processAuthorize($db, $user, $order, $post_form);
					}
				}
			}

			if (isset($_REQUEST["do_cmpi_authenticate"]) && isset($_SESSION["CardinalPostForm"]) && isset($_REQUEST["PaRes"]))
			{
				$post_form = $_SESSION["CardinalPostForm"];

				unset($_SESSION["CardinalPostForm"]);

				$cardinal_auth_xml = $cardinal->getAuthenticateXML($_REQUEST["PaRes"]);
				$cardinal_auth_result = $cardinal->processRequest($cardinal_auth_xml);

				if ($cardinal_auth_result)
				{
					$EciFlag = $cardinal_auth_result["CardinalMPI"][0]["EciFlag"][0];
					$PAResStatus = $cardinal_auth_result["CardinalMPI"][0]["PAResStatus"][0];
					$Cavv = $cardinal_auth_result["CardinalMPI"][0]["Cavv"][0];
					$SignatureVerification = $cardinal_auth_result["CardinalMPI"][0]["SignatureVerification"][0];
					$Xid = $cardinal_auth_result["CardinalMPI"][0]["Xid"][0];
					$ErrorNo = $cardinal_auth_result["CardinalMPI"][0]["ErrorNo"][0];

					if ($SignatureVerification == "Y" && $PAResStatus != "N")
					{
						switch ($PAResStatus)
						{
							case "Y" :
							case "A" : $authorizeResult = $this->processAuthorize($db, $user, $order, $post_form, $EciFlag, $Cavv); break;
							case "U" : $authorizeResult = $this->processAuthorize($db, $user, $order, $post_form); break;
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = "Your card can not be validated. Please try other card or other payment method";
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $cardinal->error_message;
				}
			}

			return $authorizeResult;
		}

		public function capture($order_data = false, $capture_amount = false)
		{
			global $settings, $order, $db;

			$this->is_error = false;

			$oldPaymentStatus = $order->getPaymentStatus();


			//check is transaction completed
			if ($order_data["custom1"] != "AUTH_ONLY")
			{
				$this->is_error = true;
				$this->error_message = "Transaction already completed";
				return false;
			}

			$total_amount = $order_data['total_amount'];

			if (floatval($order_data['gift_cert_amount']) > 0)
			{
				$total_amount -= $order_data['gift_cert_amount'];

				if ($total_amount < 0) $total_amount = 0;
			}

			//get captured total amount & x_trans_id
			$captured_total_amount = $total_amount - $order_data["custom2"];
			$transaction_id = $order_data["custom3"];

			if ($total_amount > $order_data["custom2"])
			{
				$this->is_error = true;
				$this->error_message = "New total amount can't be greater than authorized amount";
				return false;
			}

			//process transaction
			$post_data =
				"x_version=".urlencode("3.1")."&".
				"x_delim_data=".urlencode("True")."&".
				"x_delim_char=".urlencode("~")."&".
				"x_encap_char=\"&".
				"x_relay_response=".urlencode("False")."&".
				"x_login=".urlencode($settings[$this->id."_Login"])."&".
				"x_tran_key=".urlencode($settings[$this->id."_Transaction_Key"])."&".
				"x_amount=".urlencode(number_format($total_amount, 2, ".", ""))."&".
				"x_tax=".urlencode(number_format($order_data["tax_amount"], 2, ".", ""))."&".
				"x_freight=".urlencode(number_format($order_data["shipping_amount"], 2, ".", ""))."&".
				"x_type=".urlencode("PRIOR_AUTH_CAPTURE")."&".
				"x_trans_id=".urldecode($transaction_id)."&".
				"x_invoice_num=".urlencode($order_data["order_num"])."&".
				"x_po_num=".urlencode($order_data["order_num"])."&".
				"x_test_request=".urlencode($settings[$this->id."_Test_Request"]);

			$post_values = array();
			parse_str($post_data, $post_values);

			$_post_url = trim($this->url_to_gateway)==""?"https://secure.authorize.net/gateway/transact.dll":trim($this->url_to_gateway);

			$this->log("processCustom Request:\n".$_post_url, $post_values);

			$buffer = $this->process_curl($post_data, $_post_url);


			if ($buffer)
			{
				$data = explode("~", $buffer);

				$temp = array();
				foreach ($data as $value)
				{
					$temp[] = trim(rtrim(ltrim($value, '"'), '"'));
				}
				$data = $temp;

				$this->cleanData($data);
				$this->log("processCustom Response:", $data);

				$userTmp = new stdClass();
				$userTmp->id = $order_data["uid"];

				$orderTmp = new stdClass();
				$orderTmp->oid = $order_data["oid"];
				$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
				$orderTmp->totalAmount = $order_data["total_amount"];
				$orderTmp->shippingAmount = $order_data["shipping_amount"];
				$orderTmp->taxAmount = $order_data["tax_amount"];
				$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

				// Approved
				if ($data[0] == "1")
				{
					$order->setPaymentStatus('Received');

					$db->reset();
					$db->assignStr("custom1", "");
					$db->assignStr("custom2", "");
					$db->assignStr("custom3", "");
					$db->assignStr("payment_status", $order->getPaymentStatus());
					$db->update(DB_PREFIX."orders", "WHERE oid= ".$order_data['oid']);

					$this->createTransaction($db, $userTmp, $orderTmp,
						"ORDER TYPE: PRIOR_AUTH_CAPTURE\n\n".str_replace("~", "\n", $buffer),
						"", "PRIOR_AUTH_CAPTURE", "", ""
					);
				}
				else
				{
					$this->is_error = true;

					switch ($data[0])
					{
						case "2" :
						{
							$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: ".$data[3];
							break;
						}
						case "3" :
						{
							$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: ".$data[3];
							break;
						}
						default :
							{
							$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
							break;
							}
					}

					$this->storeTransaction($db, $userTmp, $orderTmp, $buffer, '', false);
				}
			}

			$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

			return !$this->is_error;
		}

		public function supportsCapture($order_data = false)
		{
			return $this->capturableAmount($order_data) > 0;
		}

		function supportsPartialCapture($order_data = false)
		{
			return false;
		}

		public function capturableAmount($order_data = false)
		{
			if ($order_data['custom1'] == 'AUTH_ONLY' && $order_data['payment_status'] == 'Pending')
			{
				return $order_data['custom2'];
			}

			return 0;
		}

		public function capturedAmount($order_data = false)
		{
			if ($order_data["custom1"] == "AUTH_ONLY")
			{
				return $order_data['custom2'];
			}
			else if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_RECEIVED)
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'];
			}

			return false;
		}
	}

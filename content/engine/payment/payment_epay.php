<?php
	$payment_processor_id = "epay";
	$payment_processor_name = "USAepay";
	$payment_processor_class = "PAYMENT_EPAY";
	$payment_processor_type = "cc";

	class PAYMENT_EPAY extends PAYMENT_PROCESSOR
	{
		function PAYMENT_EPAY()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "epay";
			$this->name = "USAepay";
			$this->class_name = "PAYMENT_EPAY";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			return $this;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Test_Request"]["value"] == "TRUE")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		public function replaceDataKeys()
		{
			return array('card', 'cvv2', 'pin', 'cavv');
		}

		//main payment module entry point
		function process($db, $user, $order, $post_form)
		{
			global $settings;

			require dirname(__FILE__)."/epay/usaepay.php";

			$tran = new umTransaction();
			$tran->transport = 'curl';
			$tran->ignoresslcerterrors = true;

			$tran->key=$this->settings_vars[$this->id."_Key"]["value"];
			
			if ($this->settings_vars[$this->id."_pin"]["value"]!="")
			{
				$tran->pin = $this->settings_vars[$this->id."_pin"]["value"];
			}

			$tran->gatewayurl = $this->url_to_gateway;
			$tran->testmode = $this->testMode;
			$tran->command = $this->settings_vars[$this->id."_Auth_Type"]["value"];
			// card number, no dashes, no spaces
			$tran->card = $post_form["cc_number"];
			//Exp Date
			$tran->exp = $post_form["cc_expiration_month"].substr($post_form["cc_expiration_year"], 2, 2);
			// charge amount in dollars (no international support yet)
			$tran->amount = number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "");
			// invoice number and Order ID.  must be unique.
			$tran->invoice = $this->common_vars["order_id"]["value"];
			$tran->orderid = $this->common_vars["order_id"]["value"];
			// name of card holder
			$tran->cardholder = $post_form["cc_first_name"]." ".$post_form["cc_last_name"];
			// street address
			$tran->street = $this->common_vars["billing_address"]["value"];
			// zip code
			$tran->zip = $this->common_vars["billing_zip"]["value"];
			// description of charge
			$tran->description = "Online Order";
			// cvv2 code
			$tran->cvv2 = $post_form["cc_cvv2"];

			//Billing Details
			$tran->billfname = $this->common_vars["billing_first_name"]["value"];
			$tran->billlname = $this->common_vars["billing_last_name"]["value"];
			$tran->billstreet = $this->common_vars["billing_address"]["value"];
			$tran->billcity = $this->common_vars["billing_city"]["value"];
			$tran->billstate = $this->common_vars["billing_state"]["value"];
			$tran->billzip = $this->common_vars["billing_zip"]["value"];
			$tran->billcountry = $this->common_vars["billing_country"]["value"];
			$tran->billphone = $this->common_vars["billing_phone"]["value"];
			$tran->email = $this->common_vars["billing_email"]["value"];

			//Shipping Details

			$tran->shipfname = $this->common_vars["shipping_name"]["value"];
			//$tran->shiplname = $this->common_vars["shipping_name"]["value"];
			$tran->shipstreet = $this->common_vars["shipping_address"]["value"];
			//$tran->shipstreet2;
			$tran->shipcity = $this->common_vars["shipping_city"]["value"];
			$tran->shipstate = $this->common_vars["shipping_state"]["value"];
			$tran->shipzip = $this->common_vars["shipping_zip"]["value"];
			$tran->shipcountry = $this->common_vars["shipping_country"]["value"];
			//$tran->shipphone;

			// Curl Proxy Setting
			$tran->proxyavailable = $settings["ProxyAvailable"];
			$tran->proxyurl = $settings["ProxyAddress"];
			$tran->proxytype = $settings["ProxyType"];
			$tran->proxyport = $settings["ProxyPort"];
			$tran->proxyauthorization = $settings["ProxyRequiresAuthorization"];
			$tran->proxyusername = $settings["ProxyUsername"];
			$tran->proxypassword = $settings["ProxyPassword"];

			$requestData = get_object_vars($tran);
			$this->log("process Request:", $requestData);

			$isValid = $tran->Process();

			$responseData = explode("\n", $tran->rawresult);
			$resultData = $responseData[count($responseData)-1];
			$responseData = array();
			parse_str($resultData, $responseData);

			$this->log("process Response:", $responseData);

			if ($isValid)
			{
				// Add for Auth_Capture
				$tType = $this->settings_vars[$this->id."_Auth_Type"]["value"] == "Sale" ? "Sale" : "AUTH_ONLY";
				$tCapturedAmmount = ($tType == "Sale")?number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""):number_format(0, 2, ".", "");
				$this->createTransaction($db, $user, $order, $responseData, "",
					//custom 1-transaction type,2-cc data,3-amount,
					$tType,
					$tCapturedAmmount,
					$tran->refnum,
					$tran->refnum
				);

				$this->is_error = false;
				$this->error_message = "";

				return $this->settings_vars[$this->id."_Auth_Type"]["value"] == "Sale" ? "Received" : "Pending";
			}
			else 
			{
				$this->error_message=$tran->error;
				$this->is_error = true; 
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $responseData, '', false);
			}

			return !$this->is_error;
		}

		public function supportsCapture($order_data = false)
		{
			return $this->capturableAmount($order_data) > 0;
		}

		function supportsPartialCapture($order_data = false)
		{
			return false;
		}

		public function capturableAmount($order_data = false)
		{
			if ($order_data['custom1'] == 'AUTH_ONLY' && $order_data['payment_status'] == 'Pending')
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'] - $order_data['custom2'];
			}

			return 0;
		}

		public function capturedAmount($order_data = false)
		{
			if ($order_data["custom1"] == "AUTH_ONLY")
			{
				return $order_data['custom2'];
			}
			else if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_RECEIVED)
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'];
			}

			return false;
		}

		public function capture($order_data = false, $capture_amount = false)
		{
			global $order, $db;

			$this->is_error = false;

			$user = $order->user();
			$oldPaymentStatus = $order->getPaymentStatus();

			$this->is_error = false;

			$db->query("SELECT * FROM ".DB_PREFIX."orders WHERE oid=".$order_data['oid']);

			//check is transaction completed
			if ($order_data["custom1"] != "AUTH_ONLY")
			{
				$this->is_error = true;
				$this->error_message = "Transaction already completed";
				return false;
			}

			$capt_amount = $order_data['total_amount'] - $order_data['gift_cert_amount'] - $order_data['custom2'];
			$refnum = $order_data["custom3"];

			if ($capt_amount <= 0 )
			{
				$this->is_error = true;
				$this->error_message = "Capture amount must be greater than 0";
				return false;
			}


			// Custom Call Transaction Process
			require dirname(__FILE__)."/epay/usaepay.php";

			$tran = new umTransaction();
			$tran->transport = 'curl';
			$tran->ignoresslcerterrors = true;

			$tran->key = $this->settings_vars[$this->id."_Key"]["value"];
			if ($this->settings_vars[$this->id."_pin"]["value"]!="")
			{
				$tran->pin = $this->settings_vars[$this->id."_pin"]["value"];
			}

			$tran->gatewayurl = $this->url_to_gateway;
			$tran->testmode = $this->testMode;
			$tran->amount = $capt_amount;

			$tran->command = "capture";

			$tran->refnum = $refnum;

			$requestData = get_object_vars($tran);
			$this->log("process Request:", $requestData);

			$isValid = $tran->Process();

			$responseData = explode("\n", $tran->rawresult);
			$resultData = $responseData[count($responseData)-1];
			$responseData = array();
			parse_str($resultData, $responseData);

			$this->log("process Response:", $responseData);

			if ($isValid)
			{
				$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);

				$db->reset();
				$db->assignStr("payment_status", $order->getPaymentStatus());
				$db->update(DB_PREFIX."orders", "WHERE oid=".$order_data['oid']);

				$this->createTransaction($db, $user, $order,
					$responseData,
					"",
					 "Sale", $capt_amount, $tran->refnum
				);
			}
			else
			{
				$this->error_message=$tran->error;
				$this->is_error = true;

				$this->storeTransaction($db, $user, $order, $responseData, '', false);
			}

			$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

			return !$this->is_error;
		}

		public function supportsRefund($order_data = false)
		{
			return $order_data && $order_data['payment_status'] == 'Received' && $order_data['custom1'] == 'Sale';
		}

		public function supportsPartialRefund($order_data = false)
		{
			return false;
		}

		public function refundableAmount($order_data = false)
		{
			return $order_data["custom2"];
		}

		public function refund($order_data = false, $refund_amount = false)
		{
			global $order, $db;

			$this->is_error = false;

			$user = $order->user();
			$oldPaymentStatus = $order->getPaymentStatus();

			$this->is_error = false;

			$db->query("SELECT * FROM ".DB_PREFIX."orders WHERE oid = ".$order_data['oid']);
			if ($db->moveNext())
			{
				$order_data = $db->col;

				//check is transaction completed
				if ($order_data["custom1"] != "Sale")
				{
					$this->is_error = true;
					$this->error_message = "Transaction already completed";
					return false;
				}

				//get captured total amount & x_trans_id
				$captured_total_amount = $order_data["custom2"];
				$refnum = $order_data["custom3"];

				if ($order_data["total_amount"] > $captured_total_amount)
				{
					$this->is_error = true;
					$this->error_message = "New total amount can't be bigger as captured total amount";
					return false;
				}

				// Custom Call Transaction Process
				require dirname(__FILE__)."/epay/usaepay.php";

				$tran = new umTransaction();
				$tran->transport = 'curl';
				$tran->ignoresslcerterrors = true;

				$tran->key=$this->settings_vars[$this->id."_Key"]["value"];
				if ($this->settings_vars[$this->id."_pin"]["value"]!="")
				{
					$tran->pin=$this->settings_vars[$this->id."_pin"]["value"];
				}

				$tran->gatewayurl = $this->url_to_gateway;
				$tran->testmode = $this->testMode;

				$tran->command = "creditvoid";

				$tran->refnum = $refnum;

				$requestData = get_object_vars($tran);
				$this->log("process Request:", $requestData);

				$isValid = $tran->Process();

				$responseData = explode("\n", $tran->rawresult);
				$resultData = $responseData[count($responseData)-1];
				$responseData = array();
				parse_str($resultData, $responseData);

				$this->log("process Response:", $responseData);

				if ($isValid)
				{
					$order->setPaymentStatus(ORDER::PAYMENT_STATUS_REFUNDED);
					$db->reset();
					$db->assignStr("payment_status", $order->getPaymentStatus());
					$db->update(DB_PREFIX."orders", "WHERE oid=".$order_data['oid']);

					$this->createTransaction($db, $user, $order,
						$responseData,
						"",
						"REFUNDED", $captured_total_amount, $tran->refnum
					);
				}
				else
				{
					$this->error_message=$tran->error;
					$this->is_error = true;

					$this->storeTransaction($db, $user, $order, $responseData, '', false);
				}
			}

			$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

			return !$this->is_error;
		}
	}

<?php 

	$payment_processor_id = "psigate";
	$payment_processor_name = "PsiGate.Com";
	$payment_processor_class = "PAYMENT_PSIGATE";
	$payment_processor_type = "cc";

	class PAYMENT_PSIGATE extends PAYMENT_PROCESSOR
	{
		function PAYMENT_PSIGATE()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "psigate";
			$this->name = "PsiGate.Com";
			$this->class_name = "PAYMENT_PSIGATE";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->show_cvv2 = false;
			return $this;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Test_Result"]["value"] != "Live")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			
			return $this->testMode;
		}

		public function replaceDataKeys()
		{
			return array('Passphrase', 'CardNumber');
		}

		function process($db, $user, $order, $post_form)
		{
			global $settings;

			//global $form;
			if (isset($post_form) && is_array($post_form))
			{
				//prepare amounts
				$amount_shipping = 
					$this->common_vars["order_shipping_amount"]["value"] + 
					($this->common_vars["order_handling_separated"]["value"]=="yes"?$this->common_vars["order_handling_amount"]["value"]:0.00);
				$amount_tax = $this->common_vars["order_tax_amount"]["value"];

				// Fix for Gift Certificates - See Bug 769 for more information
				if ($this->common_vars["order_total_amount"]["value"] > 0)
				{
					$amount_subtotal = $this->common_vars["order_total_amount"]["value"] - $amount_shipping - $amount_tax;
				}
				else
				{
					$amount_subtotal = $this->common_vars["order_total_amount"]["value"] + ($amount_shipping + $amount_tax);
				}

				//build XML
				$xml = 
					"<Order>".
						"<StoreID>".utf8_encode($this->settings_vars[$this->id."_Store_Id"]["value"])."</StoreID>".
						"<Passphrase>".utf8_encode($this->settings_vars[$this->id."_Pass_Phrase"]["value"])."</Passphrase>".
						"<PaymentType>CC</PaymentType>".
						"<CardAction>".($this->settings_vars[$this->id."_Mode"]["value"]=="Sale"?"0":"1")."</CardAction>". // sale / preauth
						"<Subtotal>".utf8_encode(number_format($amount_subtotal, 2, ".", ""))."</Subtotal>".
						"<CardNumber>".utf8_encode($post_form["cc_number"])."</CardNumber>".
						"<CardExpMonth>".utf8_encode($post_form["cc_expiration_month"])."</CardExpMonth>".
						"<CardExpYear>".utf8_encode(substr($post_form["cc_expiration_year"], 2, 2))."</CardExpYear>".
						"<OrderID>".utf8_encode($this->common_vars["order_id"]["value"])."</OrderID>".
						"<Userid>".utf8_encode($this->common_vars["customer_id"]["value"])."</Userid>".
						"<Bname>".utf8_encode($post_form["cc_first_name"]." ".$post_form["cc_last_name"])."</Bname>".
						"<Bcompany>".utf8_encode($this->common_vars["billing_company"]["value"])."</Bcompany>".
						"<Baddress1>".utf8_encode($this->common_vars["billing_address1"]["value"])."</Baddress1>".
						"<Baddress2>".utf8_encode($this->common_vars["billing_address2"]["value"])."</Baddress2>".
						"<Bcity>".utf8_encode($this->common_vars["billing_city"]["value"])."</Bcity>".
						"<Bprovince>".utf8_encode($this->common_vars["billing_state_abbr"]["value"])."</Bprovince>".
						"<Bpostalcode>".utf8_encode($this->common_vars["billing_zip"]["value"])."</Bpostalcode>".
						"<Bcountry>".utf8_encode($this->common_vars["billing_country_iso_a2"]["value"])."</Bcountry>".
						"<Phone>".utf8_encode($this->common_vars["billing_phone"]["value"])."</Phone>".
						"<Fax>".utf8_encode($this->common_vars["billing_phone"]["value"])."</Fax>".
						"<Email>".utf8_encode($this->common_vars["billing_email"]["value"])."</Email>".
						"<Comments>".utf8_encode("Order #".$this->common_vars["order_id"]["value"])."</Comments>".
						"<Tax1>".utf8_encode(number_format($amount_tax, 2, ".", ""))."</Tax1>".
						"<ShippingTotal>".utf8_encode(number_format($amount_shipping, 2, ".", ""))."</ShippingTotal>".
						"<CustomerIP>".utf8_encode($_SERVER["REMOTE_ADDR"])."</CustomerIP>";

				//check/add test mode
				if ($this->settings_vars[$this->id."_Test_Result"]["value"] != "Live")
				{
					switch ($this->settings_vars[$this->id."_Test_Result"]["value"])
					{
						case "Approved" : $xml.="<TestResult>A</TestResult>"; break;
						case "Declined" : $xml.="<TestResult>D</TestResult>"; break;
						case "Randomly" : $xml.="<TestResult>R</TestResult>"; break;
						case "Fraud" : $xml.="<TestResult>F</TestResult>"; break;
					}
				}

				//check/add shipping address
				if ($order->getShippingRequired())
				{
					$xml.=
						"<Sname>".utf8_encode($this->common_vars["shipping_name"]["value"])."</Sname>".
						"<Scompany>".utf8_encode($this->common_vars["shipping_company"]["value"])."</Scompany>".
						"<Saddress1>".utf8_encode($this->common_vars["shipping_address1"]["value"])."</Saddress1>".
						"<Saddress2>".utf8_encode($this->common_vars["shipping_address2"]["value"])."</Saddress2>".
						"<Scity>".utf8_encode($this->common_vars["shipping_city"]["value"])."</Scity>".
						"<Sprovince>".utf8_encode($this->common_vars["shipping_state"]["value"])."</Sprovince>".
						"<Spostalcode>".utf8_encode($this->common_vars["shipping_zip"]["value"])."</Spostalcode>".
						"<Scountry>".utf8_encode($this->common_vars["shipping_country_iso_a2"]["value"])."</Scountry>";
				}
				$xml.=
					"</Order>";

				//send data to server
				@set_time_limit(300);

				$_post_url = trim($this->url_to_gateway)==""?"https://dev.psigate.com:7989/Messenger/XMLMessenger":trim($this->url_to_gateway);
				$c = curl_init();

				if ($settings["ProxyAvailable"] == "YES")
				{
					//curl_setopt($c, CURLOPT_VERBOSE, 1);
					if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
					{
						curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
					}

					curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
					
					if ($settings["ProxyRequiresAuthorization"] == "YES")
					{
						curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
					}

					curl_setopt($c, CURLOPT_TIMEOUT, 120);
				}
				$this->log($_post_url);
				curl_setopt($c, CURLOPT_URL, $_post_url);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($c, CURLOPT_TIMEOUT, 10);
				curl_setopt($c, CURLOPT_POSTFIELDS, $xml);

				curl_setopt($c, CURLOPT_SSLVERSION, 1);

				curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
				curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
				$buffer = curl_exec($c);

				$requestData = xml2array($xml);
				$this->log("process Request:", $requestData);

				//check CURL error
				if (curl_errno($c) > 0)
				{
					$this->log("process Response:\n".curl_error($c));
					$this->is_error = true;
					$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT." ".curl_error($c);
					return false;
				}

				//check is there data returned
				if (strlen($buffer) < 10)
				{
					$buffer = "<Result><Approved>ERROR</Approved><ErrMsg>Could not connect to the PsiGate.com server. Please try again.</ErrMsg></Result>";
				}

				$responseData = xml2array($buffer);
				$this->log("process Response:", $responseData);

				if ($buffer)
				{
					//parse XML

					$xmlParser = new XMLParser();

					$data = $xmlParser->parse($buffer);

					//check is there "Approved" tag 
					if (isset($data["Result"][0]["Approved"][0]))
					{
						//check "Approved" tag value
						switch ($data["Result"][0]["Approved"][0])
						{
							//transaction completed
							case "APPROVED" :  
							{
								//build response string
								$response = "";

								$security_id = '';
								foreach ($data["Result"][0] as $var=>$value)
								{
									$response.= $var."  =  ".$value[0]."\n";
									if ($var == 'TransRefNumber')
									{
										$security_id = $value[0];
									}
								}

								$extra = '';
								$custom1 = $this->settings_vars[$this->id."_Mode"]["value"];
								$custom2 = '';
								$custom3 = '';

								//create transaction
								$this->createTransaction($db, $user, $order,
									$responseData,
									$extra,
									$custom1,
									$custom2,
									$custom3,
									$security_id
								);
								//return positive result value
								$this->is_error = false;
								$this->error_message = "";
								return $this->settings_vars[$this->id."_Mode"]["value"] == "Sale" ? "Received" : "Pending";
								break;
							}
							//transaction error
							default :
							case "ERROR" :
							case "DECLINED" : 
							{
								$this->is_error = true;
								
								if (isset($data["Result"][0]["ErrMsg"][0]))
								{
									//return gateway error message
									$this->error_message = $data["Result"][0]["ErrMsg"][0];
								}
								else
								{
									//return standard error message
									$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
								}
								break;
							}
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
				}

				if ($this->is_error)
				{
					$this->storeTransaction($db, $user, $order, $responseData, '', false);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Incorrect form params. Please contact site administrator.";
			}
			return !$this->is_error;
		}
		public function supportsCapture($order_data = false)
		{
			return $this->capturableAmount($order_data) > 0;
		}

		function supportsPartialCapture($order_data = false)
		{
			return false;
		}

		public function capturableAmount($order_data = false)
		{
			if ($order_data['custom1'] == 'PreAuth' && $order_data['payment_status'] == 'Pending')
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'] - $this->capturedAmount($order_data);
			}

			return 0;
		}

		public function capturedAmount($order_data = false)
		{
			if ($order_data["custom1"] == "PreAuth")
			{
				return 0;
			}
			else if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_RECEIVED)
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'];
			}

			return false;
		}

		/* @var $db DB */
		public function capture($order_data = false, $capture_amount = false)
		{
			global $order, $db, $settings;

			$user = $order->getUser();
			$oldPaymentStatus = $order->getPaymentStatus();

			$this->is_error = false;

			//check order ID
			$db->query("SELECT * FROM ".DB_PREFIX."orders WHERE oid=".$order_data['oid']);

			if ($db->moveNext())
			{
				//get order data
				$order_data = $db->col;

				//check is transaction already completed or it is not PreAuth
				if ($order_data["custom1"] != "PreAuth")
				{
					$this->is_error = true;
					$this->error_message = "Transaction already completed";
					return false;
				}

				//build XML
				$xml =
					"<Order>".
						"<StoreID>".utf8_encode($this->settings_vars[$this->id."_Store_Id"]["value"])."</StoreID>".
						"<Passphrase>".utf8_encode($this->settings_vars[$this->id."_Pass_Phrase"]["value"])."</Passphrase>".
						"<PaymentType>CC</PaymentType>".
						"<CardAction>2</CardAction>". // post auth
						"<OrderID>".utf8_encode($order_data["order_num"])."</OrderID>"; // order ID
						//"<SubTotal>".utf8_encode(number_format($order_data["total_amount"], 2, ".", ""))."</SubTotal>". // sale
				//check/add test mode
				if ($this->settings_vars[$this->id."_Test_Result"]["value"] != "Live")
				{
					switch ($this->settings_vars[$this->id."_Test_Result"]["value"])
					{
						case "Approved" : $xml.="<TestResult>A</TestResult>"; break;
						case "Declined" : $xml.="<TestResult>D</TestResult>"; break;
						case "Randomly" : $xml.="<TestResult>R</TestResult>"; break;
						case "Fraud" : $xml.="<TestResult>F</TestResult>"; break;
					}
				}
				$xml.=
						"<CustomerIP>".utf8_encode($_SERVER["REMOTE_ADDR"])."</CustomerIP>".
					"</Order>";

				//send data to a server
				$_post_url = trim($this->url_to_gateway)==""?"https://dev.psigate.com:7989/Messenger/XMLMessenger":trim($this->url_to_gateway);

				@set_time_limit(300);

				$c = curl_init();

				if ($settings["ProxyAvailable"] == "YES")
				{
					//curl_setopt($c, CURLOPT_VERBOSE, 1);
					if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
					{
						curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
					}

					curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

					if ($settings["ProxyRequiresAuthorization"] == "YES")
					{
						curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
					}

					curl_setopt($c, CURLOPT_TIMEOUT, 120);
				}

				curl_setopt($c, CURLOPT_URL, $_post_url);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($c, CURLOPT_TIMEOUT, 10);
				curl_setopt($c, CURLOPT_POSTFIELDS, $xml);

				curl_setopt($c, CURLOPT_SSLVERSION, 1);

				curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
				curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
				$buffer = curl_exec($c);

				$requestData = xml2array($xml);
				$this->log("processCustom Request:", $requestData);

				//check CURL error
				if (curl_errno($c) > 0)
				{
					$this->is_error = true;
					$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT." ".curl_error($c);
					return false;
				}

				//check is there data returned
				if (strlen($buffer) < 10)
				{
					$buffer = "<Result><Approved>ERROR</Approved><ErrMsg>Could not connect to the PsiGate.com server. Please try again.</ErrMsg></Result>";
				}

				$responseData = xml2array($buffer);
				$this->log("processCustom Response:", $responseData);

				if ($buffer)
				{
					//parse XML
					$xmlParser = new XMLParser();
					$data = $xmlParser->parse($buffer);

					if (isset($data["Result"][0]["Approved"][0]))
					{
						switch ($data["Result"][0]["Approved"][0])
						{
							case "APPROVED" :
							{
								$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);

								$db->reset();
								$db->assignStr("payment_status", $order->getPaymentStatus());
								$db->update(DB_PREFIX."orders", "WHERE oid=".$order_data['oid']);

								$extra = '';
								$custom1 = 'PostAuth';
								$custom2 = '';
								$custom3 = '';
								$security_id = $data['Result'][0]['TransRefNumber'][0];

								$this->createTransaction($db, $user, $order,
									$responseData,
									$extra, $custom1, $custom2, $custom3, $security_id
								);
								break;
							}
							default :
							case "ERROR" :
							case "DECLINED" :
							{
								$this->is_error = true;

								if (isset($data["Result"][0]["ErrMsg"][0]))
								{
									$this->error_message = $data["Result"][0]["ErrMsg"][0];
								}
								else
								{
									$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
								}

								break;
							}
						}//end of switch
					}
					else
					{
						$this->is_error = true;
						$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
				}

				if ($this->is_error)
				{
					$this->storeTransaction($db, $userTmp, $orderTmp, $responseData, '', false);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Incorrect form params. Please contact site administrator.";
			}

			$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

			return !$this->is_error;
		}
	}

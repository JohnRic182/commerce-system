<?php

	$payment_processor_id = "Bongo Checkout";
	$payment_processor_name = "Bongo.com";
	$payment_processor_class = "PAYMENT_BONGOCHECKOUT";
	$payment_processor_type = "ipn";
	
	class PAYMENT_BONGOCHECKOUT extends PAYMENT_PROCESSOR
	{
		protected $realCallback = false;

		/**
		 * Class constructor
		 */
		public function PAYMENT_BONGOCHECKOUT()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "bongocheckout";
			$this->name = "Bongo Checkout";
			$this->class_name = "PAYMENT_BONGOCHECKOUT";
			$this->type = "ipn";
			$this->description = "";
			$this->post_url = "";
			$this->steps = 2;
			$this->testMode = false;
			$this->allow_change_gateway_url = true;
			
			return $this;
		}

		/**
		 * Sent email notification
		 */
		public function sentEmailNotification($orderStatus)
		{
			return false;//$this->realCallback;
		}
		
		/**
		 * Get post URL
		 */
		public function getPostUrl()
		{
			return trim($this->url_to_gateway);
		}

		protected function getHash($hashKey, $oid, $securityId)
		{
			return md5($hashKey.$oid.$securityId);
		}
		
		/**
		 * Get payment form
		 * @param $db
		 */
		public function getPaymentForm($db)
		{
			$_pf = parent::getPaymentForm($db);
			global $settings, $msg;

			if (!isset($_SESSION['bongo_hash_key'])) $_SESSION['bongo_hash_key'] = array();
			$hashKey = md5(uniqid('', true).uniqid('', false));
			$hash = $this->getHash($hashKey, $this->common_vars["order_oid"]["value"], $this->common_vars["order_security_id"]["value"]);

			$db->query('UPDATE '.DB_PREFIX.'orders SET custom1="'.$db->escape($hash).'" WHERE oid="'.intval($this->common_vars["order_oid"]["value"]).'"');

			$db->query('SELECT * FROM '.DB_PREFIX.'orders WHERE oid="'.intval($this->common_vars["order_oid"]["value"]).'"');
			$orderData = $db->moveNext();
			$totalDiscount = $orderData['discount_amount'] + $orderData['promo_discount_amount'];
			$discountCoefficient = 1 - $totalDiscount/$orderData['subtotal_amount'];

			$fields = array();
			
			$fields[] = array("type"=>"hidden", "name"=>"PARTNER_KEY", "value"=>$this->settings_vars[$this->id."_Partner_Key"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUSTOM_ORDER_1", "value"=>$this->common_vars["order_oid"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUSTOM_ORDER_2", "value"=>$hash);
			$fields[] = array("type"=>"hidden", "name"=>"CUSTOM_ORDER_3", "value"=>serialize(
				array('s'=>session_id(), 'i'=>$this->common_vars["order_security_id"]["value"])
			));

			// add products
			$db->query("SELECT * FROM ".DB_PREFIX."orders_content WHERE oid='".$this->common_vars["order_oid"]["value"]."'");
			$i = 1;
			
			while ($db->moveNext())
			{
				$fields[] = array(
					"type"=>"hidden", "name"=>"PRODUCT_ID_".$i,
					"value"=>$db->col["product_id"].(trim($db->col["product_sub_id"]) == '' ? '' : '-'.$db->col["product_sub_id"])
				);
				$fields[] = array("type"=>"hidden", "name"=>"PRODUCT_NAME_".$i, "value"=>$db->col["title"]);
				$fields[] = array("type"=>"hidden", "name"=>"PRODUCT_PRICE_".$i, "value"=>$db->col["price"] * $discountCoefficient);
				$fields[] = array("type"=>"hidden", "name"=>"PRODUCT_Q_".$i, "value"=>$db->col["quantity"]);
				//$fields[] = array("type"=>"hidden", "name"=>"PRODUCT_SHIPPING_".$i, "value"=>0.00);

				if (trim($db->col["options_clean"]) != '')
				{
					$chunks = str_split(trim($db->col["options_clean"]), 100);
					$customNum = 1;
					foreach ($chunks as $chunk)
					{
						if ($customNum <= 3)
						{
							$fields[] = array("type"=>"hidden", "name"=>"PRODUCT_CUSTOM_".$customNum."_".$i, "value"=>$chunk);
						}
						$customNum++;
					}
				}

				$i++;
			}

			$fields[] = array("type"=>"hidden", "name"=>"CUST_COUNTRY", "value"=>$this->common_vars["billing_country_iso_a2"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_FIRST_NAME", "value"=>$this->common_vars["billing_first_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_LAST_NAME", "value"=>$this->common_vars["billing_last_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_COMPANY", "value"=>$this->common_vars["billing_company"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_ADDRESS_LINE_1", "value"=>$this->common_vars["billing_address1"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_ADDRESS_LINE_2", "value"=>$this->common_vars["billing_address2"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"CUST_CITY", "value"=>$this->common_vars["billing_city"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_STATE", "value"=>$this->common_vars["billing_state"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_ZIP", "value"=>$this->common_vars["billing_zip"]["value"]);
			
			$fields[] = array("type"=>"hidden", "name"=>"CUST_EMAIL", "value"=>$this->common_vars["billing_email"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"CUST_PHONE", "value"=>$this->common_vars["billing_phone"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"SHIP_EMAIL", "value"=>$this->common_vars["billing_email"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"SHIP_PHONE", "value"=>$this->common_vars["billing_phone"]["value"]);

			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_FIRST_NAME', 'value'=>$this->common_vars['shipping_first_name']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_LAST_NAME', 'value'=>$this->common_vars['shipping_last_name']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_COMPANY', 'value'=>$this->common_vars['shipping_company']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_ADDRESS_LINE_1', 'value'=>$this->common_vars['shipping_address1']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_ADDRESS_LINE_2', 'value'=>$this->common_vars['shipping_address2']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_CITY', 'value'=>$this->common_vars['shipping_city']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_STATE', 'value'=>$this->common_vars['shipping_state']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_ZIP', 'value'=>$this->common_vars['shipping_zip']['value']);
			$fields[] = array('type'=>'hidden', 'name'=>'SHIP_COUNTRY', 'value'=>$this->common_vars['shipping_country_iso_a2']['value']);

			$shippingAmount = $this->common_vars["order_shipping_amount"]["value"];

			if ($this->common_vars["order_handling_separated"]["value"])
			{
				$shippingAmount += $this->common_vars["order_handling_amount"]["value"];
			}

			$fields[] = array("type"=>"hidden", "name"=>"TOTAL_DOMESTIC_SHIPPING_CHARGE", "value"=>round($shippingAmount, 2));
			
			$_SESSION['BongoCheckoutInProgress'] = true;

			$this->log('Payment FORM:'.print_r($fields, 1));

			return $fields;
		}
		
		/**
		 * Get JS
		 */
		public function getValidatorJS()
		{
			return "";
		}
		
		/**
		 * Is it a test mode?
		 */
		public function isTestMode()
		{
			return false;
		}
		
		/**
		 * Process payment 
		 */
		public function process($db, $user, $order, $post_form)
		{
			global $_REQUEST, $_SESSION;

			$this->log('Process/request: '.print_r($_REQUEST, 1));
			$this->log('Session: '.print_r($_SESSION, 1));

			unset($_SESSION['BongoCheckoutInProgress']);
			
			/**
			 * Process order
			 */
			$status = $_REQUEST['status'];

			if (isset($_REQUEST['complete']) && $_REQUEST['complete'] == '1' && $status == 'P')
			{
				$this->realCallback = false;
				$this->createTransaction($db, $user, $order, 'Pending before notification', null, null, null, null);
				return 'Pending';
			}
			else
			{
				$this->realCallback = true;
				$partnerKey = $_REQUEST['partner_key'];
				$requestXml = base64_decode($_REQUEST['order']);
				$orderData = simplexml_load_string($requestXml);

				$this->log('Request XML: '.$requestXml);

				$oid = $orderData->channel->item->custom_order1;
				$externalHash = $orderData->channel->item->custom_order2;

				$orderQueryResult = $db->query('SELECT * FROM '.DB_PREFIX.'orders WHERE oid="'.intval($oid).'"');
				
				if ($db->moveNext($orderQueryResult))
				{
					$internalHash = $db->col['custom1'];

					$this->log('db order data: '.print_r($db->col, 1));
					$this->log('externalHash: '.$externalHash);
					$this->log('internalHash: '.$internalHash);
					$this->log('_Partner_Key: '.$this->settings_vars[$this->id."_Partner_Key"]["value"]);
					$this->log('partnerKey: '.$partnerKey);

					if ($externalHash == $internalHash && $this->settings_vars[$this->id."_Partner_Key"]["value"] == $partnerKey)
					{
						$this->log('Match:'.$status);
						$this->createTransaction($db, $user, $order, $requestXml, null, null, null, $orderData->channel->item->idorder);
						switch ($status)
						{
							case 'N' : return 'Received';
							case 'P' : return 'Pending';
						}
					}
				}
			}

			return false;
		}
	}

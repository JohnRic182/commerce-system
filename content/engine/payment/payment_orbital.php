<?php
$payment_processor_id = "orbital";
$payment_processor_name = "Orbital Gateway";
$payment_processor_class = "PAYMENT_ORBITAL";
$payment_processor_type = "cc";

class PAYMENT_ORBITAL extends PAYMENT_PROCESSOR
{
	protected $client = false;

	/**
	 * Class constructor
	 */
	public function PAYMENT_ORBITAL()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "orbital";
		$this->name = "Orbital Gateway";
		$this->class_name = "PAYMENT_ORBITAL";
		$this->type = "cc";
		$this->description = "";
		$this->steps = 1;
		$this->testMode = false;
		$this->need_cc_codes = true;
		$this->support_ccs = false;

		return $this;
	}

	public function replaceDataKeys()
	{
		return array('AccountNum', 'MerchantID');
	}

	/**
	 *
	 * @param DB $db
	 * @return type
	 */
	public function getPaymentProfileForm($db)
	{
		global $msg;
		$fields = parent::getPaymentProfileForm($db);
		$fields["pp_cc_cvv2"] = array("type"=>"text", "name"=>"form[pp_cc_cvv2]", "maxlength"=>"4", "class"=>"input-cvv2", "value"=>"", "caption"=>$msg["billing"]["cc_cvv2"], "hint"=>$msg["billing"]["cc_cvv2_hint"]);
		return $fields;
	}

	/**
	 * Check does is it test mode
	 */
	public function isTestMode()
	{
		$this->testMode =$this->settings_vars[$this->id."_Test_Mode"]["value"] == "On";
		return $this->testMode;
	}

	/**
	 * @return bool|orbital_client
	 */
	protected function getClient()
	{
		if ($this->client)
			return $this->client;

		global $db;
		$this->getSettingsData($db);

		$connectionUsername = $this->settings_vars[$this->id."_Connection_Username"]["value"];
		$connectionPassword = $this->settings_vars[$this->id."_Connection_Password"]["value"];

		$BIN = $this->settings_vars[$this->id."_BIN"]["value"];
		$merchantID = $this->settings_vars[$this->id."_Merchant_ID"]["value"];
		$terminalID = $this->settings_vars[$this->id."_Terminal_ID"]["value"];

		if ($connectionPassword == '') $connectionPassword = false;
		if ($connectionUsername == '') $connectionUsername = false;

		$this->client = new orbital_client($merchantID, $BIN, $terminalID, $connectionUsername, $connectionPassword, $this->isTestMode());

		return $this->client;
	}

	/**
	 * Handle new order calls
	 *
	 * @param $db
	 * @param $user
	 * @param $order
	 * @param $post_form
	 *
	 * @return bool|string
	 */
	function newOrder($db, $user, $order, $post_form)
	{
		global $settings;

		$client = $this->getClient();

		$messageTypes = array('Authorize-Capture'=>'AC', 'Authorize'=>'A');

		$messageType = $messageTypes[$this->settings_vars[$this->id."_Message_Type"]["value"]];
		$currencyCode = $this->settings_vars[$this->id."_Currency_Code"]["value"];

		$industryType = 'EC';
		$orderId = $this->common_vars["order_id"]["value"];
		$totalAmount = round($this->common_vars["order_total_amount"]["value"]*100);
		$name = $this->common_vars["billing_name"]["value"];
		$address1 = $this->common_vars["billing_address1"]["value"];
		$address2 = $this->common_vars["billing_address2"]["value"];
		$city = $this->common_vars["billing_city"]["value"];
		$state = $this->common_vars["billing_state_abbr"]["value"];
		$country = $this->common_vars["billing_country_iso_a2"]["value"];
		$zip = $this->common_vars["billing_zip"]["value"];
		$phone = preg_replace('/[^0-9]/', '', $this->common_vars["billing_phone"]["value"]);
		$ccNumber = $post_form["cc_number"];
		$ccExpirationDate = $post_form['cc_expiration_month'].substr($post_form['cc_expiration_year'], 2, 2);

		$ext_profile_id = isset($post_form['ext_profile_id']) ? $post_form['ext_profile_id'] : false;
		$cvv2 = $ext_profile_id ? $post_form["pp_cc_cvv2"] : $post_form["cc_cvv2"];

		$response = $client->newOrder(
			$orderId, $totalAmount, $name, $address1, $address2,
			$city, $state, $country, $zip, $phone, $ccNumber, $ccExpirationDate,
			$cvv2, $ext_profile_id, $currencyCode, $messageType, $industryType
		);

		$this->cleanData($response);

		$this->log("newOrder Response:", $response);

		if (isset($response['ProcStatus']) && $response['ProcStatus'] == '0' &&
			isset($response['ApprovalStatus']) && $response['ApprovalStatus'] == '1')
		{
			$extra = '';
			$custom1 = $messageType == 'AC' ? 'AUTH_CAPTURE' : 'AUTH_ONLY';
			//If Auth only, then store auth'd amount to custom2
			$custom2 = $messageType != "AC" ? number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "") : "";
			//If Auth Capture, then store captured amount to custom3
			$custom3 = $messageType == "AC" ? number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "") : 0;
			$security_id = $response['TxRefNum'];

			$this->createTransaction($db, $user, $order, $response, $extra, $custom1, $custom2, $custom3, $security_id);

			return $messageType == 'AC' ? 'Received' : 'Pending';
		}
		else
		{
			$this->is_error = true;

			if (isset($response['ProcStatusMsg']))
			{
				$this->error_message = $response['ProcStatusMsg'];
			}
			else
			{
				$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
			}
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $response, '', false);
		}

		return !$this->is_error;
	}

	/**
	 * (non-PHPdoc)
	 * @see content/engine/PAYMENT_PROCESSOR#process()
	 */
	function process($db, $user, $order, $post_form)
	{
		return $this->newOrder($db, $user, $order, $post_form);
	}

	function currentOrderPaymentStatus($order_data = false)
	{
		if ($order_data)
		{
			return $order_data['custom1'];
		}
		return false;
	}

	function supportsCapture($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == 'Pending' || $order_data['payment_status'] == 'Partial')
			{
				if ($this->isTotalAmountCaptured($order_data))
					return false;

				return true;
			}
		}
		return false;
	}

	function isTotalAmountCaptured($order_data)
	{
		return $order_data['custom3'] == $order_data['total_amount'];
	}

	function supportsRefund($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Partial')
			{
				return true;
			}
			else
			if ($order_data['payment_status'] == 'Refunded')
			{
				return $this->_refundableAmount($order_data) > 0.0;
			}
		}
		return false;
	}

	public function supportsPartialRefund($order_data = false)
	{
		return true;
	}

	function supportsVoid($order_data = false)
	{
		return $order_data && ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Pending');
	}

	public function capturedAmount($order_data = false)
	{
		return $order_data ? $order_data['custom3'] : false;
	}

	function capturableAmount($order_data=false)
	{
		if ($order_data)
		{
			if ($this->supportsCapture($order_data))
			{
				$capturedAmount = $order_data['custom3'];

				$total_amount = $order_data['total_amount'];
				$captured_amount =  $order_data['custom3'];

				$capturableAmount = $order_data['total_amount'] - $order_data['custom3'];
				if ($capturableAmount < 0)
					$capturableAmount = 0.0;

				return number_format($capturableAmount, 2, '.', '');
			}
		}
		return 0;
	}

	function refundableAmount($order_data=false)
	{
		if ($order_data)
		{
			if ($this->supportsRefund($order_data))
			{
				return $this->_refundableAmount($order_data);
			}
		}
		return 0;
	}

	function _refundableAmount($order_data)
	{
		$refundableAmount = $this->getTotalCaptured($order_data);
		return number_format($refundableAmount, 2, '.', '');
	}

	function getTotalCaptured($order_data)
	{
		return 0.00 + $order_data['custom3'];
	}

	function capture($order_data=false,$capture_amount=false)
	{
		global $settings, $db, $order;

		$oldPaymentStatus = $order->getPaymentStatus();

		$this->is_error = false;

		if ($order_data)
		{
			$order_id = $order_data['order_num'];
			$client = $this->getClient();

			//check is transaction completed
			if ($this->isTotalAmountCaptured($order_data))
			{
				$this->is_error = true;
				$this->error_message = "Transaction already completed";
				return false;
			}

			//get captured total amount & x_trans_id
			$captured_total_amount = $capture_amount ? $capture_amount : $order_data["custom2"];
			$transaction_id = $order_data["security_id"];

			if ($transaction_id == '')
			{
				$this->is_error = true;
				$this->error_message = 'This order does not have a valid transaction id';
				return false;
			}
			else
			{
				if ($captured_total_amount <= 0)
				{
					$this->is_error = true;
					$this->error_message = "Capture amount must be greater than 0";
					return false;
				}
				else if ($captured_total_amount > $order_data['total_amount'])
				{
					$this->is_error = true;
					$this->error_message = "Capture amount cannot be more than total amount";
					return false;
				}
				else if ($order_data['custom3'] + $captured_total_amount > $order_data['total_amount'])
				{
					$this->is_error = true;
					$this->error_message = 'Captured amount cannot be more than total amount';
					return false;
				}

				$response = $client->markForCapture($order_id, round($captured_total_amount * 100), $transaction_id);

				$this->cleanData($response);

				$this->log("capture Response:", $response);

				$userTmp = new tmp_object();
				$userTmp->id = $order_data["uid"];

				$orderTmp = new tmp_object();
				$orderTmp->oid = $order_data["oid"];
				$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
				$orderTmp->totalAmount = $order_data["total_amount"];
				$orderTmp->shippingAmount = $order_data["shipping_amount"];
				$orderTmp->taxAmount = $order_data["tax_amount"];
				$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

				if (isset($response['ProcStatus']) && $response['ProcStatus'] == '0')
				{
					$totalCaptured = $this->getTotalCaptured($order_data) + $captured_total_amount;
					$custom3 = number_format($totalCaptured, 2, '.', '');

					$db->reset();
					$db->assignStr("custom1", "");
					$db->assignStr("custom2", "");
					$db->assignStr("custom3", $custom3);

					$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);

					if ($order_data['total_amount'] > $totalCaptured)
					{
						$order->setPaymentStatus(ORDER::PAYMENT_STATUS_PARTIAL);
					}

					$db->assignStr("payment_status", $order->getPaymentStatus());

					$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

					$order_data['custom1'] = 'PRIOR_AUTH_CAPTURE';
					$order_data['custom2'] = '';
					$order_data['custom3'] = $custom3;
					$order_data['payment_status'] = $order->getPaymentStatus();

					$custom1 = $order_data['custom1'];
					$custom2 = $order_data['custom2'];
					$security_id = $transaction_id;

					$this->createTransaction($db, $userTmp, $orderTmp,
						"ORDER TYPE: PRIOR_AUTH_CAPTURE\n\n".array2text($response),
						"",
						$custom1,
						$custom2,
						$custom3,
						$security_id
					);

					$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

					return true;
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $response['StatusMsg'];

					$this->storeTransaction($db, $userTmp, $orderTmp, $response, '', false);

					return false;
				}
			}
		}
		return !$this->is_error;
	}

	function refund($order_data=false,$refund_amount=false)
	{
		global $settings, $db, $order;

		$oldPaymentStatus = $order->getPaymentStatus();

		$this->is_error = false;

		if ($order_data)
		{
			$order_id = $order_data['order_num'];

			if (!$this->supportsRefund($order_data))
			{
				$this->is_error = true;
				$this->error_message = "Order cannot be refunded";
				return false;
			}

			//get captured total amount & x_trans_id
			$refund_total_amount = $refund_amount;

			if (!$refund_total_amount)
			{
				$refund_total_amount = $order_data['custom3'];
			}

			if ($refund_total_amount < 0)
			{
				$this->is_error = true;
				$this->error_message = "Refund amount must be greater than 0";
				return false;
			}
			else if ($refund_total_amount > $order_data['total_amount'])
			{
				$this->is_error = true;
				$this->error_message = "Refund amount cannot be more than total amount";
				return false;
			}
			else if ($refund_total_amount > $order_data['custom3'])
			{
				$this->is_error = true;
				$this->error_message = 'Refund amount cannot be more than total captured amount';
				return false;
			}

			$transaction_id = $order_data["security_id"];

			if ($transaction_id == '')
			{
				$this->is_error = true;
				$this->error_message = 'This order does not have a valid transaction id';
				return false;
			}
			else
			{
				$currencyCode = $this->settings_vars[$this->id."_Currency_Code"]["value"];
				$client = $this->getClient();

				$response = $client->refund($order_id, $transaction_id, round($refund_total_amount*100), $currencyCode, 'EC');

				$this->cleanData($response);

				$this->log("refund Response:", $response);

				$userTmp = new tmp_object();
				$userTmp->id = $order_data["uid"];

				$orderTmp = new tmp_object();
				$orderTmp->oid = $order_data["oid"];
				$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
				$orderTmp->totalAmount = $order_data["total_amount"];
				$orderTmp->shippingAmount = $order_data["shipping_amount"];
				$orderTmp->taxAmount = $order_data["tax_amount"];
				$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

				if (isset($response['ProcStatus']) && $response['ProcStatus'] == '0')
				{
					$totalCaptured = $this->getTotalCaptured($order_data) - $refund_total_amount;
					$custom3 = number_format($totalCaptured, 2, '.', '');

					$db->reset();
					$db->assignStr("custom1", "");
					$db->assignStr("custom2", "");
					$db->assignStr("custom3", $custom3);

					$order->setPaymentStatus(ORDER::PAYMENT_STATUS_REFUNDED);

					$db->assignStr("payment_status", $order->getPaymentStatus());
					$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

					$order_data['custom1'] = 'REFUND';
					$order_data['custom2'] = '';
					$order_data['custom3'] = $custom3;
					$order_data['payment_status'] = $order->getPaymentStatus();

					$custom1 = $order_data['custom1'];
					$custom2 = $order_data['custom2'];

					$security_id = $transaction_id;

					$this->createTransaction($db, $userTmp, $orderTmp,
						"ORDER TYPE: REFUND\n\n".array2text($response),
						"",
						$custom1,
						$custom2,
						$custom3,
						$security_id
					);

					$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

					return true;
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $response['StatusMsg'];

					$this->storeTransaction($db, $userTmp, $orderTmp, $response, '', false);

					return false;
				}
			}
		}
		return !$this->is_error;
	}

	function void($order_data=false)
	{
		global $settings, $db, $order;

		$oldPaymentStatus = $order->getPaymentStatus();
		$oldOrderStatus = $order->getStatus();

		$this->is_error = false;

		if ($order_data)
		{
			$order_id = $order_data['order_num'];
			$client = $this->getClient();

			$total_amount = $order_data['total_amount'];
			$transaction_id = $order_data["security_id"];

			$response = $client->reversal($order_id, $transaction_id, round($total_amount * 100));

			$this->cleanData($response);
			$this->log("void Response:", $response);

			$userTmp = new tmp_object();
			$userTmp->id = $order_data["uid"];

			$orderTmp = new tmp_object();
			$orderTmp->oid = $order_data["oid"];
			$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
			$orderTmp->totalAmount = $order_data["total_amount"];
			$orderTmp->shippingAmount = $order_data["shipping_amount"];
			$orderTmp->taxAmount = $order_data["tax_amount"];
			$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

			if (isset($response['ProcStatus']) && $response['ProcStatus'] == '0')
			{
				$custom1 = '';
				$custom2 = '';
				$custom3 = '';

				$db->reset();
				$db->assignStr("custom1", "");
				$db->assignStr("custom2", "");
				$db->assignStr("custom3", '');

				$order->setPaymentStatus(ORDER::PAYMENT_STATUS_CANCELED);
				$order->setStatus(ORDER::STATUS_CANCELED);

				$db->assignStr("payment_status", $order->getPaymentStatus());
				$db->assignStr('status', $order->getStatus());
				$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

				$custom1 = 'CANCELED';
				$order_data['custom1'] = $custom1;
				$order_data['custom2'] = $custom2;
				$order_data['custom3'] = $custom3;
				$order_data['payment_status'] = $order->getPaymentStatus();
				$order_data['status'] = $order->getStatus();

				$this->createTransaction($db, $userTmp, $orderTmp,
					"ORDER TYPE: REVERSAL\n\n".array2text($response),
					"",
					$custom1,
					$custom2,
					$custom3,
					$transaction_id
				);

				$this->fireOrderEvent($order, $oldOrderStatus, $oldPaymentStatus);

				return true;
			}
			else
			{
				$this->is_error = true;
				$this->error_message = $response['StatusMsg'];

				$this->storeTransaction($db, $userTmp, $orderTmp, $response, '', false);

				return false;
			}
		}
		return !$this->is_error;
	}

	function supportsPaymentProfiles()
	{
		global $settings;
		return $settings[$this->id."_Cust_Profiles"] == "On";
	}

	/**
	 * Get payment profile details
	 *
	 * @param DB $db
	 * @param mixed $ext_profile_id
	 * @param bool $active
	 *
	 * @return array|mixed|null
	 */
	function getPaymentProfileDetails($db, $ext_profile_id, $active = true)
	{
		$client = $this->getClient();

		$response = $client->getPaymentProfile($ext_profile_id);

		if (isset($response['ProfileProcStatus']) && $response['ProfileProcStatus'] == '0')
		{
			if ($active && isset($response['Status']) && $response['Status'] != 'A')
				return null;

			$data = array();

			$this->mapValues('CustomerName', 'name', $response, $data);
			$this->mapValues('CustomerAddress1', 'address1', $response, $data);
			$this->mapValues('CustomerAddress2', 'address2', $response, $data);
			$this->mapValues('CustomerCity', 'city', $response, $data);
			$this->mapValues('CustomerState', 'state', $response, $data);
			$this->mapValues('CustomerZIP', 'zip', $response, $data);
			$this->mapValues('CustomerCountryCode', 'country', $response, $data);
			$this->mapValues('CCAccountNum', 'cc_number', $response, $data);
			$this->mapValues('CustomerPhone', 'phone', $response, $data);

			if (isset($response['CCExpireDate']))
			{
				$data['cc_expiration_month'] = substr($response['CCExpireDate'], 0, 2);
				$data['cc_expiration_year'] = substr($response['CCExpireDate'], 2, 2);
				$data['cc_expiration_date'] = $data['cc_expiration_month'].'/'.$data['cc_expiration_year'];
			}

			$data['first_name'] = trim(substr($data['name'], 0, strpos($data['name'], ' ')));
			$data['last_name'] = trim(substr($data['name'], strpos($data['name'], ' ')));

			$country = getCountryData($db, false, $data['country']);
			$data['country_id'] = $country['coid'];
			$state = getStateData($db, false, $data['state']);
			$data['state_id'] = $state['stid'];

			return $data;
		}
		return null;
	}

	/**
	 * Map values
	 * @param $sourceKey
	 * @param $destKey
	 * @param $source
	 * @param $dest
	 */
	protected function mapValues($sourceKey, $destKey, $source, &$dest)
	{
		if (isset($source[$sourceKey])) $dest[$destKey] = $source[$sourceKey];
	}

	/**
	 * Add a new payment profile
	 *
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool|void
	 */
	public function addPaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		/** @var Model_Address $billingAddress */
		$billingAddress = $billingData->getAddress();

		/** @var array $userData */
		$userData = $user->getUserData();

		$postForm = array(
			'first_name' => $billingAddress->getFirstName(),
			'last_name' => $billingAddress->getLastName(),
			'address1' => $billingAddress->getAddressLine1(),
			'address2' => $billingAddress->getAddressLIne2(),
			'city' => $billingAddress->getCity(),
			'state' => $billingAddress->getStateCode(),
			'zip' => $billingAddress->getZip(),
			'country' => $billingAddress->getCountryIso2(),
			'email' => isset($userData['email']) ? $userData['email'] : '',
			'phone' => $billingData->getPhone(),
			'cc_number' => $billingData->getCcNumber(),
			'cc_expiration_month' => $billingData->getCcExpirationMonth(),
			'cc_expiration_year' => $billingData->getCcExpirationYear()
		);

		$response = $this->updateProfile($db, $user, $postForm);

		if ($response && isset($response['ProfileProcStatus']) && $response['ProfileProcStatus'] == '0' && isset($response['CustomerRefNum']))
		{
			$paymentProfile->setExternalProfileId($response['CustomerRefNum']);

			return true;
		}

		$this->error_message = 'There was an error adding your credit card';
		$this->is_error = true;

		return false;
	}

	/**
	 * Update payment profile
	 *
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool
	 */
	public function updatePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		/** @var Model_Address $billingAddress */
		$billingAddress = $billingData->getAddress();

		/** @var array $userData */
		$userData = $user->getUserData();

		$postForm = array(
			'first_name' => $billingAddress->getFirstName(),
			'last_name' => $billingAddress->getLastName(),
			'address1' => $billingAddress->getAddressLine1(),
			'address2' => $billingAddress->getAddressLIne2(),
			'city' => $billingAddress->getCity(),
			'state' => $billingAddress->getStateCode(),
			'zip' => $billingAddress->getZip(),
			'country' => $billingAddress->getCountryIso2(),
			'email' => isset($userData['email']) ? $userData['email'] : '',
			'phone' => $billingData->getPhone(),
			'cc_number' => $billingData->getCcNumber(),
			'cc_expiration_month' => $billingData->getCcExpirationMonth(),
			'cc_expiration_year' => $billingData->getCcExpirationYear(),
			'ext_profile_id' => $paymentProfile->getExternalProfileId(),
			'profile_id' => $paymentProfile->getId()
		);

		$response = $this->updateProfile($db, $user, $postForm, 'U');

		if ($response && isset($response['ProfileProcStatus']) && $response['ProfileProcStatus'] == '0')
		{
			return true;
		}

		$this->error_message = 'There was an error updating your credit card';
		$this->is_error = false;

		return false;
	}

	/**
	 * Remove payment profile
	 *
	 * @param $db
	 * @param $user
	 * @param $paymentProfile
	 *
	 * @return bool|void
	 */
	public function deletePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$client = $this->getClient();
		$response = $client->deletePaymentProfile($paymentProfile->getExternalProfileId());

		if (($response && isset($response['CustomerProfileMessage']) && strstr($response['CustomerProfileMessage'], 'Profile does not exist')) ||
			(isset($response['ProfileProcStatus']) && $response['ProfileProcStatus'] == '0'))
		{
			return true;
		}

		$this->error_message = 'There was an error deleting your credit card';
		$this->is_error = true;

		return false;
	}

	/**
	 * Update payment profile
	 *
	 * @param $db
	 * @param $user
	 * @param $post_form
	 * @param string $customerProfileAction
	 *
	 * @return bool
	 */
	protected function updateProfile($db, $user, $post_form, $customerProfileAction = 'C')
	{
		$customer_ref_num = isset($post_form['ext_profile_id']) ? $post_form['ext_profile_id'] : '';
		$customer_name = $post_form['first_name']." ".$post_form['last_name'];
		$address1 = $post_form['address1'];
		$address2 = isset($post_form['address2']) ? $post_form['address2'] : '';
		$city = $post_form['city'];
		$state = getStateData($db, $post_form['state']);

		if ($state)
		{
			$state = $state['short_name'];
		}

		$zip = $post_form['zip'];
		$country = getCountryData($db, $post_form['country']);

		if ($country)
		{
			$country = $country['iso_a2'];
		}

		$email = isset($post_form['email']) ? $post_form['email'] : $user->data['email'];
		$phone = str_replace('-', '', $post_form['phone']);

		$customerAccountType = 'CC';
		$ccAccountNum = '';

		if (isset($post_form['cc_number']))
		{
			$ccAccountNum = $post_form['cc_number'];
		}

		$ccExpireDate = $post_form['cc_expiration_month'].substr($post_form['cc_expiration_year'], 2, 2);

		$customerProfileFromOrderInd = 'A';

		if ($customerProfileAction == 'U')
		{
			$customerProfileFromOrderInd = 'S';
		}

		$client = $this->getClient();

		return $client->updatePaymentProfile(
			$customer_name, $customer_ref_num, $address1, $address2, $city,
			$state, $zip, $email, $phone, $country, $ccAccountNum, $ccExpireDate, $customerProfileAction,
			$customerProfileFromOrderInd, $customerAccountType
		);
	}
}

<?php 
	$payment_processor_id = "paypalpflink";
	$payment_processor_name = "paypal.com";
	$payment_processor_class = "PAYMENT_PAYPALPFLINK";
	$payment_processor_type = "ipn";

	require_once dirname(__FILE__).'/payment_paypalpfpro.php';

	class PAYMENT_PAYPALPFLINK extends PAYMENT_PAYPALPFPRO
	{
		public function PAYMENT_PAYPALPFLINK()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "paypalpflink";
			$this->name = "PayPal Payflow Link";
			$this->class_name = "PAYMENT_PAYPALPFLINK";
			$this->type = "ipn";
			$this->description = "";
			$this->post_url = "https://payflowlink.paypal.com";
			$this->allow_change_gateway_url = false;
			$this->steps = 2;
			$this->override_payment_form = false;

			$this->reload_parent_on_process = true;

			$this->payment_in_popup = true;
			$this->payment_in_popup_width = 540;
			$this->payment_in_popup_height = 680;

			$this->buttonSource = getpp().'_Cart_PFL';

			return $this;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Mode"]["value"] == "Test")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		//redeclare getPaymentForm
		public function getPaymentForm($db)
		{
			global $settings, $order;

			$_pf = parent::getPaymentForm($db);

			$liveMode = (strtolower(trim($this->settings_vars[$this->id."_Mode"]["value"])) != 'test');

			$api = $this->getClient();

			$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?securityId=".$this->common_vars["order_security_id"]["value"]."&oid=".$order->oid."&p=paypal_callback";

			$cancel_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;

			$paypalLines = array();
			foreach ($this->order_items as $cartItem)
			{
				$paypalLines[] = array(
					'name' => $cartItem['title'],
					'desc' => $cartItem['product_id'] . "\n" . $cartItem['options_clean'],
					'amt' => $cartItem['product_price'],
					'qty' => $cartItem['quantity'],
					'itemcategory' => $cartItem['product_type'] == 'Tangible' ? 'Physical' : 'Digital',
					'tax' => $cartItem['product_price'] * $cartItem['quantity'] * $cartItem['tax_rate'] / 100,
				);
			}

			$taxAmt = $this->common_vars["order_tax_amount"]["value"];
			$shippingAmt = $this->common_vars["order_shipping_amount"]["value"];
			$handlingAmt = ($this->common_vars['order_handling_separated'] == 0) ? $this->common_vars["order_handling_amount"]["value"] : 0;
			$discountAmt = $this->common_vars["order_discount_amount"]["value"];

			$orderTotal = $this->common_vars["order_total_amount"]["value"];

			if ($discountAmt <= 0)
			{
				if ($order->discountAmount > 0)
				{
					$discountAmt += $order->discountAmount;
				}

				if ($settings['DiscountsPromo'] == 'YES' && $order->promoDiscountAmount > 0)
				{
					$discountAmt += $order->promoDiscountAmount;
				}
			}
			$giftCertificateData = isset($_SESSION["order_gift_certificate"]) ? $_SESSION["order_gift_certificate"] : false;

			if ($giftCertificateData)
			{
				$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
				$giftCertData = $giftCertificateRepository->getGiftCertificate($giftCertificateData['first_name'], $giftCertificateData['last_name'], $giftCertificateData['voucher']);

				if ($giftCertData)
				{
					$giftCertAmount = $giftCertData['balance'];

					$discountAmt += $giftCertAmount;
				}
			}

			$response = $api->getSecureToken(
				$this->settings_vars[$this->id."_Transaction_Type"]["value"] == "Sale" ? "S" : "A",
				$this->common_vars["order_id"]["value"],
				number_format($orderTotal, 2, ".", ""),
				$this->settings_vars[$this->id.'_Currency_Code']['value'],
				$this->common_vars["billing_first_name"]["value"],
				$this->common_vars["billing_last_name"]["value"],
				$this->common_vars["billing_address"]["value"],
				$this->common_vars["billing_city"]["value"],
				$this->common_vars["billing_state_abbr"]["value"],
				$this->common_vars["billing_zip"]["value"],
				$this->common_vars["billing_country_iso_a3"]["value"],
				$this->common_vars["billing_email"]["value"],
				$this->common_vars["billing_phone"]["value"],
				$notify_url,
				$notify_url,
				$cancel_url,
				ltrim($this->settings_vars[$this->id."_Color_Header_Text"]["value"], '#'), // Header text color
				ltrim($this->settings_vars[$this->id."_Color_Border"]["value"], '#'), // Border Color
				ltrim($this->settings_vars[$this->id."_Color_Field_Label"]["value"], '#'), // Field color
				ltrim($this->settings_vars[$this->id."_Color_Button_Text"]["value"], '#'), // Button text color
				ltrim($this->settings_vars[$this->id."_Color_Button_Background"]["value"], '#'), //Buton Bg Color
				$paypalLines,
				$taxAmt,
				$shippingAmt,
				$handlingAmt,
				$discountAmt,
				$this->common_vars["shipping_first_name"]["value"],
				$this->common_vars["shipping_last_name"]["value"],
				$this->common_vars["shipping_address"]["value"],
				$this->common_vars["shipping_city"]["value"],
				$this->common_vars["shipping_state_abbr"]["value"],
				$this->common_vars["shipping_zip"]["value"],
				$this->common_vars["shipping_country_iso_a3"]["value"]
			);

			$fields = array();

			$parsedResponse = $api->parseResponse($response);

			$fields[] = array("type"=>"hidden", "name"=>'SECURETOKEN', "value"=> $parsedResponse['SECURETOKEN']);
			$fields[] = array("type"=>"hidden", "name"=>'SECURETOKENID', "value"=> $parsedResponse['SECURETOKENID']);

			if (!$liveMode) $fields[] = array("type"=>"hidden", "name"=>'MODE', "value"=> 'test');

			return $fields;
		}

		public function getValidatorJS()
		{
			return "";
		}

		public function getPostUrl()
		{
			if (trim($this->url_to_gateway) != "")
			{
				return trim($this->url_to_gateway);
			}
			else
			{
				return $this->post_url;
			}
		}

		public function process($db, $user, $order, $post_form)
		{
			$api = $this->getClient();

			$this->log("Form Response", $post_form);
			if (isset($post_form['PNREF']))
			{
				$security_id = $post_form['PNREF'];

				$post_form = $api->doInquiry($security_id);
			}

			$this->log("Process", $post_form);

			$_result = isset($post_form["RESULT"])?intval($post_form["RESULT"]):-1000;
			$_respmsg = isset($post_form["RESPMSG"])?"Gateway Response: ".$post_form["RESPMSG"]:PAYMENT_SERVER_ERROR_TEXT;

			if ($_result == 0 && $_respmsg == "Gateway Response: Approved")
			{
				$messageType = $this->settings_vars[$this->id."_Transaction_Type"]["value"] == "Sale" ? "S" : "A";

				$extra = '';

				$custom1 = $messageType == 'S' ? 'AUTH_CAPTURE' : 'AUTH_ONLY';
				//If Auth only, then store auth'd amount to custom2
				$custom2 = $messageType != "S" ? number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "") : "";
				//If Auth Capture, then store captured amount to custom3
				$custom3 = $messageType == "S" ? number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "") : 0;
				$security_id = isset($post_form['ORIGPNREF']) ? $post_form['ORIGPNREF'] : $post_form['PNREF'];

				$this->createTransaction($db, $user, $order, $post_form, $extra, $custom1, $custom2, $custom3, $security_id);

				$this->is_error = false;
				$this->error_message = "";

				$transstate = isset($post_form['TRANSSTATE']) ? $post_form['TRANSSTATE'] : false;

				if ($transstate)
				{
					if (in_array($transstate, array(0,6,7,8,9)))
					{
						return 'Received';
					}

					return 'Pending';
				}

				if (isset($post_form['PENDINGREASON']) && !in_array($post_form['PENDINGREASON'], array('none', 'completed')))
				{
					return 'Pending';
				}

				if (isset($post_form['TYPE']))
				{
					return $post_form['TYPE'] == 'S' ? 'Received' : 'Pending';
				}
				else if (isset($post_form['TRXTYPE']))
				{
					return $post_form['TRXTYPE'] == 'S' ? 'Received' : 'Pending';	
				}
			}
			else
			{
				$this->is_error = true;

				$this->storeTransaction($db, $user, $order, $post_form, '', false);

				$this->error_message = $_respmsg;
			}
			return !$this->is_error;
		}
	}

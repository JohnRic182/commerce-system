var stripeTokenCreated = false;
/**
 * Validate payment form and get token
 *
 * @returns {boolean}
 */
function stripeValidatePaymentForm()
{
	opc_paymentFormValidationAsync = true;

	var form = $('#opc-payment-method-form');

	// add stripe attributes
	$(form).find('input[name="form[cc_number]"]').attr('data-stripe', 'number');
	$(form).find('input[name="form[cc_cvv2]"]').attr('data-stripe', 'cvc');
	$(form).find('select[name="form[cc_expiration_year]"]').attr('data-stripe', 'exp-year');
	$(form).find('select[name="form[cc_expiration_month]"]').attr('data-stripe', 'exp-month');

	var cardholderName = $(form).find('input[name="form[cc_first_name]"]').val() + ' ' + $(form).find('input[name="form[cc_last_name]"]').val();
	$(form).find('input[name="form[cc_cardholder_name]"]').attr('data-stripe', 'name').val(cardholderName);
	$(form).find('input[name="form[cc_address_line1]"]').attr('data-stripe', 'address_line1');
	$(form).find('input[name="form[cc_address_line2]"]').attr('data-stripe', 'address_line2');
	$(form).find('input[name="form[cc_address_city]"]').attr('data-stripe', 'address_city');
	$(form).find('input[name="form[cc_address_state]"]').attr('data-stripe', 'address_state');
	$(form).find('input[name="form[cc_address_zip]"]').attr('data-stripe', 'address_zip');
	$(form).find('input[name="form[cc_address_country]"]').attr('data-stripe', 'address_country');

	Stripe.setPublishableKey(stripePublishableKey);
	Stripe.card.createToken(form, stripeResponseHandler);
}

/**
 * Stripe response callback
 * @param status
 * @param response
 */
function stripeResponseHandler(status, response)
{
	var form = $('#opc-payment-method-form');
	if (response.error)
	{
		alert(response.error.message);
		opc_paymentFormValidationAsyncResult = false;
		onePageCheckout.setCompleteButton();
		onePageCheckout.hideSpinner();
	}
	else
	{
		$(form).append($('<input type="hidden" name="form[stripeToken]" />').val(response.id));
		opc_paymentFormValidationAsyncResult = true;
		opc_payment.sendPaymentRequest('cc', '');
	}

	opc_paymentFormValidationAsyncFinished = true;
}
<?php

// load main config file
require_once("../../engine_config.php");

// load independed libraries
require_once("../../engine_functions.php");

$db = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$settingsRepository = new DataAccess_SettingsRepository($db);
$settings = $settingsRepository->getAll();

Timezone::setApplicationDefaultTimezone($db, $settings);

require_once("../../engine_label.php");

// create general MySQL connection

/**
 * Handles Refunds/Captures
 *
 * @package default
 * @author Sebastian Nievas
 */

class QbmsGateway
{
	/**
	 * Content to output to the screen
	 *
	 * @var string
	 */
	var $content;
	
	/**
	 * Sets the caption to display for the form
	 *
	 * @var string
	 */
	var $caption;
	
	/**
	 * Database handle
	 *
	 * @var string
	 */
	var $db;
	
	/**
	 * Order ID this transaction belongs to
	 *
	 * @var string
	 */
	var $order_id;
	
	/**
	 * The current payment status of the order
	 *
	 * @var string
	 */
	var $payment_status;
	
	/**
	 * The transaction type to perform based on the current payment status
	 *
	 * @var string
	 */
	var $transaction_type;
	/**
	 * Order data
	 *
	 * @var string
	 */
	var $data;
	
	/**
	 * Constructor
	 *
	 * @param string $db 
	 * @return void
	 * @author Sebastian Nievas
	 */
	function QbmsGateway(&$db)
	{
		// store the 
		$this->db = $db;
		
		// set up the variables used for this transaction in a sanitized environment
		$this->setEnvironment();
		
		// display the form
		$this->displayForm();
	}
	
	/**
	 * Prepares the environment to run the transaction in a safe manner
	 *
	 * @return void
	 * @author Sebastian Nievas
	 */
	function setEnvironment()
	{
		
		$this->order_id = $_GET['oid'];
		
		switch($_GET['payment_status'])
		{
			case 'Pending': $this->transaction_type = '1'; break;
			case 'Received': break;
		}
		
	}
	
	/**
	 * Displays the form to perform the transactions
	 *
	 * @return void
	 * @author Sebastian Nievas
	 */
	function displayForm()
	{
		$fields = '';
		switch ($this->transaction_type)
		{
			// Pending: Show void/cancel and capture payment
			case '1':
				$this->caption = '<p>To receive the funds for this transaction, you must Capture Payment.</p><p>You can also void this transaction before it has settled.</p>';
				$fields = '
					<input id="process_capture" name="process_capture" type="button" value="Capture Payment" />
					<input id="process_void" name="process_void" type="button" value="Void Transaction" />
				';
				
				$this->postVars = '';
				break;
				
			// Refund
			// case '2':
			// 	$this->caption = 'To receive the funds for this transaction, you must Complete the Capture. You can also void this transaction from being captured. You cannot void a capture that has settled.';
			// 	$fields = '
			// 		<label for="refund_amount">Refund Amount:</label> <input id="refund_amount" name="refund_amount" type="text" value="" /><br />
			// 		<input id="process_refund" name="process_refund" type="submit" value="Refund" />
			// 	';
			// 	$this->postVars = 'post_data[refund_amount]="+frm.elements["refund_amount"].value+"&post_data[refund_reason]="+frm.elements["refund_reason"].value';
			// 	break;
		}
		
		$html = '
				<fieldset>
					<legend><strong>QuickBooks Merchant Services Options</strong></legend>
					<p>'.$this->caption.'</p>
					<input id="type" name="type" type="hidden" value="'.$this->transaction_type.'" />
					<input id="oid" name="oid" type="hidden" value="'.$this->order_id.'" />
					'.$fields.'
					<input id="cancel" name="cancel" onclick="window.close();" type="button" value="Close Window" />
				</fieldset>
		';
		
		$this->setContent($html);
	}
	
	/**
	 * Displays the content to the screen
	 *
	 * @return void
	 * @author Sebastian Nievas
	 */
	function display()
	{
		print $this->getContent();
	}
	
	/**
	 * Returns $content
	 *
	 * @return void
	 * @author Sebastian Nievas
	 */
	function getContent()
	{
		return $this->content;
	}
	
	/**
	 * Sets the contents of $content
	 *
	 * @param string $content 
	 * @return void
	 * @author Sebastian Nievas
	 */
	function setContent($content)
	{
		$this->content = $content;
	}
}

$qbms = new QbmsGateway($db);

?>
<html>
	<head>
		<title>QuickBooks Merchant Services</title>
		<script type="text/javascript" language="JavaScript" src="../../../vendors/jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" language="JavaScript">
			$(document).ready(function(){
				$('#process_capture').click(function(){
					dispatchTransaction(1);
				});
				
				// $('#process_refund').click(function(){
				// 	dispatchTransaction(2);
				// });
				
				$('#process_void').click(function(){
					dispatchTransaction(3);
				});
			});
			
			function dispatchTransaction(custom)
			{
				window.opener.location.href=$('#post_url').val()+"/admin.php?p=order_process&oid="+$('#oid').val()+"&mode=process_custom&custom_id="+custom+"<?php echo $qbms->postVars; ?>";
				window.close();
				return false;
			}
		</script>
		<style type="text/css">
			body,html { font-family: Verdana; font-size: 12px; text-align: center; }
			input { margin: 5px; }
		</style>
	</head>
	
	<body>
		<form action="" method="POST">
			<input type="hidden" id="post_url" name="post_url" value="<?php echo $settings["GlobalHttpsUrl"]; ?>" />
			<?php $qbms->display(); ?>
		</form>
	</body>
</html>
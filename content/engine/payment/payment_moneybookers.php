<?php 
	$payment_processor_id = "moneybookers";
	$payment_processor_name = "moneybookers.com";
	$payment_processor_class = "PAYMENT_MONEYBOOKERS";
	$payment_processor_type = "ipn";

	class PAYMENT_MONEYBOOKERS extends PAYMENT_PROCESSOR
	{
		function PAYMENT_MONEYBOOKERS()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "moneybookers";
			$this->name = "moneybookers.com";
			$this->class_name = "PAYMENT_MONEYBOOKERS";
			$this->type = "ipn";
			$this->description = "";
			$this->post_url = "https://www.moneybookers.com/app/payment.pl";
			$this->steps = 2;
			$this->possible_payment_delay = true;
			$this->possible_payment_delay_timeout = 10; // seconds
			$this->override_payment_form = false;
			$this->payment_in_popup = true;
			return $this;
		}

		public function replaceDataKeys()
		{
			return array('customer_id');
		}

		function getSessionID($db)
		{
			global $settings;

			$status_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;
			$return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=completed";
			$cancel_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;

			$db->query("SELECT content FROM ".DB_PREFIX."design_elements WHERE element_id='image-logo'");

			if ($db->moveNext())
			{
				$logo_url = $settings["GlobalHttpsUrl"]."/".$db->col["content"];
			}
			else
			{
				$logo_url = '';
			}

			$data = array();

			$data['pay_to_email'] = $this->settings_vars[$this->id."_email_id"]["value"];
			$data['customer_id'] = $this->settings_vars[$this->id."_customer_id"]["value"];
			$data["language"] =$this->settings_vars[$this->id."_language"]["value"];
			$data["recipient_description"] = $settings["CompanyName"];
			$data["item_name"] = $settings["CompanyName"].' Order #'.$this->common_vars["order_id"]["value"]; 
			$data["transaction_id"] = $this->common_vars["order_id"]["value"];
			$data["firstname"] =$this->common_vars["billing_first_name"]["value"];
			$data["lastname"] = $this->common_vars["billing_last_name"]["value"];
			$data["address"] = $this->common_vars["billing_address1"]["value"];
			$data["address2"] = $this->common_vars["billing_address2"]["value"];
			$data["phone_number"] = preg_replace('/[^\d\s]/', '', $this->common_vars["billing_phone"]["value"]);
			$data["postal_code"] = $this->common_vars["billing_zip"]["value"];
			$data["city"] = $this->common_vars["billing_city"]["value"];
			$data["state"] = $this->common_vars["billing_state_abbr"]["value"] != "" ? $this->common_vars["billing_state_abbr"]["value"] : $this->common_vars["billing_state"]["value"];
			$data["country"] = $this->common_vars["billing_country_iso_a3"]["value"];
			$data["amount"] = round($this->common_vars["order_total_amount"]["value"], 2);
			$data["currency"] = $this->settings_vars[$this->id."_currency_code"]["value"];
			$data["quantity"] = "1";
			$data["logo_url"] = $logo_url;
			$data["status_url2"] = 'mailto:'.$this->settings_vars[$this->id."_status_url2"]["value"];
			$data["invoice"] = $this->common_vars["order_id"]["value"];
			$data["pay_from_email"] = $this->common_vars["billing_email"]["value"];
			$data["notify_version"] = "2.0";
			$data["status_url"] = $status_url;
			$data["return_url"] = $return_url;
			$data["cancel_url"] = $cancel_url;
			$data["hide_login"] = "1";
			$data["prepare_only"] = "1";
			$data["merchant_fields"] = "platform";
			$data["platform"] = "21477251";

			$data["payment_methods"] = "ACC";

			$c = curl_init($this->post_url);

			if ($settings["ProxyAvailable"] == "YES")
			{
				//curl_setopt($c, CURLOPT_VERBOSE, 1);
				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}

				curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 20);
			curl_setopt($c, CURLOPT_USERAGENT, 'Pinnacle Moneybookers');
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $data);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);

			$buffer = curl_exec($c);

			$this->log("getSessionID Request:", $data);
			$this->log("getSessionID Response:\n".$buffer);

			if (curl_errno($c) || $buffer == "")
			{
				$this->isError = true;
				$this->errorMessage = curl_error($c);
				return false;
			}
			
			return $buffer;
		}

		//redeclare getPaymentForm
		function getPaymentForm($db)
		{
			global $_SESSION;
			global $settings, $order, $url_https;
			global $msg;
			
			$_pf = parent::getPaymentForm($db);
			
			$fields = array();
			$fields[] = array("type"=>"hidden", "name"=>"sid", "value"=>$this->getSessionID($db));
			return $fields;
		}

		function getValidatorJS()
		{
			return '';
		}

		function getPostUrl()
		{
			if (trim($this->url_to_gateway) != "")
			{
				return trim($this->url_to_gateway);
			}
			else
			{
				return $this->post_url;
			}
		}

		function process($db, $user, $order, $post_form)
		{
			global $_POST;
			global $settings;

			$this->log("process post_form:\n".print_r($post_form, 1));

			//check payment status
			if ((trim(strtolower($post_form["status"])) == "2") || (trim(strtolower($post_form["status"])) == "0"))
			{
				//reinsure all is OK - verify request
				$post_data = 'cmd=_notify-validate';
				reset($post_form);

				foreach ($_POST as $var_name => $var_value)
				{
					$post_data .= "&".$var_name."=".urlencode($var_value);
				}

				//https://www.moneybookers.com
				$this->post_url = $this->getPostUrl();
				//check verification value

				if ((trim(strtolower($post_form["status"])) == "2") || (trim(strtolower($post_form["status"])) == "0") )
				{
					reset($post_form);
					$response = '';
					foreach ($post_form as $var_name => $var_value)
					{
						$response .=$var_name." = ".$var_value."\n";
						if ($var_name == "transaction_id") $transaction_id = $var_value;
						if ($var_name == "status") $payment_status = $var_value;	
						if ((trim(strtolower($post_form["status"])) == "0")) $PendingReason = 'This status is sent when the customers pays via the pending bank transfer option - Moneybookers.';
					}
					$this->createTransaction($db, $user, $order, $response, "", $PendingReason, "", 0, $transaction_id);
					$this->is_error = false;
					$this->error_message = "";
					return $payment_status;
				}
				else
				{
					$this->is_error = true;
					$this->error_message = "Can't complete your order. MoneyBookers payment is not completed yet.";
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "An error occurred while processing this transaction.";
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $post_form, '', false);
			}
			
			return !$this->is_error;
		}
	}
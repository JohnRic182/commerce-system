<?php 
	$payment_processor_id = "virtual_merchant";
	$payment_processor_name = "Virtual Merchant";
	$payment_processor_class = "PAYMENT_VIRTUAL_MERCHANT";
	$payment_processor_type = "ipn";
	
	class PAYMENT_VIRTUAL_MERCHANT extends PAYMENT_PROCESSOR
	{
		/**
		 * Class constructor
		 */
		function PAYMENT_VIRTUAL_MERCHANT()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "virtual_merchant";
			$this->name = "Virtual Merchant";
			$this->class_name = "PAYMENT_VIRTUAL_MERCHANT";
			$this->type = "ipn";
			$this->description = "";
			$this->post_url = "https://www.myvirtualmerchant.com/VirtualMerchant/process.do";
			$this->steps = 2;
			$this->possible_payment_delay = true;
			$this->possible_payment_delay_timeout = 10; // seconds
			return $this;
		}
		
		/**
		 * Get payment form
		 * @param $db
		 */
		function getPaymentForm($db)
		{
			global $_SESSION;
			global $settings, $order;
			global $msg;
			
			$_pf = parent::getPaymentForm($db);

			$notify_url = $settings["GlobalHttpsUrl"]."/content/engine/payment/virtualpayment/callback.php";
			$return_url = $settings["GlobalHttpsUrl"]."/content/engine/payment/virtualpayment/return.php";

			$billing_country = $country = Countries::getCountryByFullName($this->common_vars["billing_country"]["value"]);
			$billing_country = (is_null($billing_country)) ? $this->common_vars["billing_country"]["value"] : $billing_country['iso_a2'];
			
			$fields = array();
			
			$fields[] = array("type"=>"hidden", "name"=>"ssl_merchant_id", "value"=>$this->settings_vars[$this->id."_merchant_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_user_id", "value"=>$this->settings_vars[$this->id."_user_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_pin", "value"=>$this->settings_vars[$this->id."_pin"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_amount", "value"=>round($this->common_vars["order_total_amount"]["value"], 2));
			$fields[] = array("type"=>"hidden", "name"=>"ssl_test_mode", "value"=>$this->isTestMode() ? "true" : "false");
			$fields[] = array("type"=>"hidden", "name"=>"ssl_invoice_number", "value"=>$this->common_vars["order_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_transaction_type", "value"=>"ccsale");
			$fields[] = array("type"=>"hidden", "name"=>"ssl_customer_code", "value"=>$this->common_vars["customer_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_email", "value"=>$this->common_vars["billing_email"]["value"]);
			
			$fields[] = array("type"=>"hidden", "name"=>"ssl_first_name", "value"=>$this->common_vars["billing_first_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_last_name", "value"=>$this->common_vars["billing_last_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_avs_address", "value"=>$this->common_vars["billing_address"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_address2", "value"=>"");
			$fields[] = array("type"=>"hidden", "name"=>"ssl_city", "value"=>$this->common_vars["billing_city"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_state", "value"=>$this->common_vars["billing_state"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_avs_zip", "value"=>$this->common_vars["billing_zip"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_country", "value"=>$billing_country);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_phone", "value"=>$this->common_vars["billing_phone"]["value"]);
			
			if ($this->common_vars['shipping_required']['value'])
			{
				$shipping_country = $country = Countries::getCountryByFullName($this->common_vars["shipping_country"]["value"]);
				$shipping_country = (is_null($shipping_country)) ? $this->common_vars["shipping_country"]["value"] : $shipping_country['iso_a2'];
				
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_first_name", "value"=>$this->common_vars["shipping_name"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_last_name", "value"=>$this->common_vars["shipping_name"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_address1", "value"=>$this->common_vars["shipping_address1"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_address2", "value"=>$this->common_vars["shipping_address2"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_city", "value"=>$this->common_vars["shipping_city"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_state", "value"=>$this->common_vars["shipping_state"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_zip", "value"=>$this->common_vars["shipping_zip"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_ship_to_country", "value"=>$shipping_country);
			}
			
			$fields[] = array("type"=>"hidden", "name"=>"ssl_result_format", "value"=>"HTML");
			$fields[] = array("type"=>"hidden", "name"=>"ssl_receipt_decl_method", "value"=>"REDG");
			$fields[] = array("type"=>"hidden", "name"=>"ssl_receipt_decl_get_url", "value"=>$return_url);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_receipt_apprvl_method", "value"=>"REDG");
			$fields[] = array("type"=>"hidden", "name"=>"ssl_receipt_apprvl_get_url", "value"=>$notify_url);
			$fields[] = array("type"=>"hidden", "name"=>"ssl_receipt_link_text", "value"=>"Continue");

			if ($this->common_vars['order_tax_amount']['value'] > 0)
			{
				$fields[] = array('type' => 'hidden', 'name' => 'ssl_salestax', 'value' => $this->common_vars['order_tax_amount']['value']);
			}

			if ($this->settings_vars[$this->id."_site_form"]["value"] == "Yes")
			{
				$fields[] = array("type"=>"hidden", "name"=>"ssl_show_form", "value"=>"false");
				$fields[] = array("type"=>"text", "name"=>"ssl_first_name", "value"=>$this->common_vars["billing_first_name"]["value"], "caption"=>$msg["billing"]["cc_first_name"]);
				$fields[] = array("type"=>"text", "name"=>"ssl_last_name", "value"=>$this->common_vars["billing_last_name"]["value"], "caption"=>$msg["billing"]["cc_last_name"]);
				$fields[] = array("type"=>"text", "name"=>"ssl_card_number", "value"=>"", "caption"=>$msg["billing"]["cc_number"]);
				$fields[] = array("type"=>"text", "name"=>"ssl_exp_date", "value"=>"", "caption"=>"Expiration date (MMYY)");
				$fields[] = array("type"=>"hidden", "name"=>"ssl_cvv2cvc2_indicator", "value"=>"1");
				$fields[] = array("type"=>"text", "name"=>"ssl_cvv2cvc2", "maxlength"=>"4", "size"=>"4", "value"=>"", "caption"=>$msg["billing"]["cc_cvv2"], "hint"=>$msg["billing"]["cc_cvv2_hint"]);
			}
			else
			{
				$fields[] = array("type"=>"hidden", "name"=>"ssl_show_form", "value"=>"true");
			}
			
			if ($this->settings_vars[$this->id."_avs"]["value"] == "Yes")
			{
				$fields[] = array("type"=>"hidden", "name"=>"ssl_avs_address", "value"=>$this->common_vars["billing_address1"]["value"]);
				$fields[] = array("type"=>"hidden", "name"=>"ssl_avs_zip", "value"=>$this->common_vars["billing_zip"]["value"]);
			}

			return $fields;
		}
		
		/**
		 * Get validator javascript
		 */
		function getValidatorJS()
		{
			return "";
		}
		
		/**
		 * Is test mode?
		 */
		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_test_mode"]["value"] == "Yes")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}
		
		/**
		 * Get post URL
		 */
		function getPostUrl()
		{
			if (trim($this->url_to_gateway) != "")
			{
				return trim($this->url_to_gateway);
			}
			else
			{
				return $this->post_url;
			}
		}
		
		/**
		 * Process payment
		 * @param $db
		 * @param $user
		 * @param $order
		 * @param $post_form
		 */
		function process($db, $user, $order, $post_form)
		{
			global $settings;
			
			$this->log("Response:\n".array2text($post_form));
			
			//check payment status				
			if ((trim(strtolower($post_form["ssl_result"])) == "0") || (trim(strtolower($post_form["ssl_result_message"])) == "APPROVAL"))
			{
				$this->createTransaction($db, $user, $order, $_REQUEST, "", "", "", "", $post_form["ssl_txn_id"]);
				$this->is_error = false;
				$this->error_message = "";
				return "Received";;
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "An error occurred while PayPal was processing your order. It will be investigated by a human at the earliest opportunity. We apologise for any inconvenience.";
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $post_form, '', false);
			}

			return !$this->is_error;
		}
	}

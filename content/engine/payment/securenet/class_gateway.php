<?php
class SecureNetGateway
{
	private $host = "gateway.securenet.com";
	private $base_url = "https://gateway.securenet.com/API/Gateway.svc/webHttp";
	
	public $is_error = false;
	public $error_message = null;
	
	// Sets base URL
	public function set_base_url($url)
	{
		$this->base_url = $url;
	}
	
	// Lists the valid methods you can use in this class, not very useful but can speed up dev time.
	public function methods()
	{
		echo "->ProcessTransaction(\$data[]);<br>\n";
		echo "->ProcessVaultTransaction(\$data[]);<br>\n";
		echo "->UpdateTransaction(\$data[]);<br>\n";
		echo "->ProcessAccount(\$data[]);<br>\n";
		echo "->ProcessCustomer(\$data[]);<br>\n";
		echo "->ProcessCustomerAndAccount(\$data[]);<br>\n";
		echo "->AddASPAccount(\$data[]);<br>\n";
		echo "->UpdateASPAccount(\$data[]);<br>\n";
		echo "->UpdateASPSchedule(\$data[]);<br>\n";
	}
	
	// This magic method allows for the selection of the above methods in the format: $response = $Gateway_object->method_name($array_of_data);
	public function __call($name, $request)
	{
		switch ($name)
		{
			case "ProcessTransaction" :
			case "AddASPAccount" :
			case "UpdateASPAccount" :
			case "UpdateASPSchedule" :
			{
				$xml = '<TRANSACTION xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://gateway.securenet.com/API/Contracts">'.$this->xmlize($request).'</TRANSACTION>';
				break;
			}

			case "ProcessVaultTransaction":
			case "UpdateTransaction":
			case "ProcessAccount":
			case "ProcessCustomer":
			case "ProcessCustomerAndAccount":
			{
				$xml  = '<TRANSACTION_VAULT xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://gateway.securenet.com/API/Contracts">'.$this->xmlize($request).'</TRANSACTION_VAULT>';
				break;
			}

			default:
			{
				throw new Exception('Method Choice Error - '.$name.' is not a valid Gateway method name.');
				return false;
			}
			break;
		}
		
		return $this->send($xml, $name);
	}
	
	
	/**
	 * Format Request as XML (recursive)
	 * @param mixed $request
	 * @return string 
	 */
	private function xmlize($request)
	{
		$xml = '';
		
		ksort($request); // At one time, perhaps still, the items in the XML needed to be alphabetized. This does that.
		
		foreach ($request as $k => $v) // For each root key of the array structure,
		{
			if (is_int($k))
			{
				$xml .= $this->xmlize($v);
			}
			elseif (is_array($v)) // if the value is an array,
			{
				$xml .= '<'.$k.'>';
				$xml .= $this->xmlize($v); // walk that new array like the first;
				$xml .= '</'.$k.'>';
			} 
			elseif (is_bool($v) OR $v === "" OR is_null($v)) // however, if it is a bool value or NULL,
			{
				$xml .= '<'.$k.' i:nil="true" />'; // write out the funny bool i:nil value.
			} 
			else // Otherwise, and these should all be strings, but it will accept ints or floats as well,
			{
				$xml .= '<'.$k.'>'.$v.'</'.$k.'>'; // just write the value into a standard tag structure.
			}
		}
		return $xml;	
	}
	
	/**
	 * Sends a request to the desired method
	 * @param type $request
	 * @param type $method
	 * @return type 
	 */
	private function send($request, $method)
	{
		$this->isError = false;
		
		$header[] = "Content-type: text/xml";
		$header[] = "Content-length: ".strlen($request) . "\r\n";
		$header[] = $request;
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $this->base_url.'/'.$method);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	
		$result = curl_exec($ch);

		if (curl_errno($ch))
		{
			$this->is_error = true;
			$this->error_message = curl_error($ch);
		}
		
		curl_close($ch); 
		
		return $this->isError ? false : $result;
	}
}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
 <HEAD>
  <TITLE> USAePay CreditVoid </TITLE>  
  <META NAME="Author" CONTENT="">
  <META NAME="Keywords" CONTENT="">
  <META NAME="Description" CONTENT="">  
  <?php
  // load main config file
	require_once("../../engine_config.php");

	// load independed libraries
	require_once("../../engine_functions.php");

	// create general MySQL connection
	$db = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  	$settingsRepository = new DataAccess_SettingsRepository($db);
  	$settings = $settingsRepository->getAll();

  	Timezone::setApplicationDefaultTimezone($db, $settings);

	require_once("../../engine_label.php");
	echo '<script language="javascript">
	function Confrom()
	{
		window.opener.location.href="'.$settings["GlobalHttpsUrl"].'/admin.php?p=order_process&oid='.$_GET['oid'].'&mode=process_custom&custom_id=2";
		window.close();
	}
	</script>';
	?>
 </HEAD>

 <BODY>
  <div valing="center" >
  <table width="100%" border="0">
  <tr>
    <td><b>USAePay :Credit/Void Transaction</b></td>
  </tr>
  <tr>
    <td align="justify"></br><p>Clicking the button below will credit / void transaction. If this transaction has already settled, the entire order amount will be refunded back to the customer. If you need to do a partial refund, please login to the gateway.</p></td>
  </tr>
  <tr>
    <td align="center"></br></br><input name="Confirm" type="button" value="Confirm" OnClick="Confrom();return false;" /></td>
  </tr>
</table>
  	
  </div>
 </BODY>
</HTML>

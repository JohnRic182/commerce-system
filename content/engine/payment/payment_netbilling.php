<?php 
	$payment_processor_id = "netbilling";
	$payment_processor_name = "NetBilling.Com";
	$payment_processor_class = "PAYMENT_NETBILLING";
	$payment_processor_type = "cc";

	class PAYMENT_NETBILLING extends PAYMENT_PROCESSOR
	{
		function PAYMENT_NETBILLING()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "netbilling";
			$this->name = "NetBilling.Com";
			$this->class_name = "PAYMENT_NETBILLING";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->support_ccs = true;
			$this->need_cc_codes = true;

			return $this;
		}

		public function replaceDataKeys()
		{
			return array('account_id', 'card_number', 'card_cvv2');
		}

		function process($db, $user, $order, $post_form)
		{
			global $settings;

			if (isset($post_form) && is_array($post_form))
			{
				//build post data for query
				$post_data = "";
				// get post_form, common, settings and custom vars
				reset($this->common_vars);
				$post_data = "".
					"pay_type=C". // credit card
					"&tran_type=S". // sale
					"&account_id=".urlencode($this->settings_vars[$this->id."_account_id"]["value"]).
					"&card_number=".urlencode($post_form["cc_number"]).
					"&card_expire=".urlencode($post_form["cc_expiration_month"].substr($post_form["cc_expiration_year"], 2, 2)).
					"&amount=".urlencode(number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")).
					"&tax_amount=".urlencode(number_format($this->common_vars["order_tax_amount"]["value"], 5, ".", "")).
					"&ship_amount=".urlencode(number_format($this->common_vars["order_shipping_amount"]["value"], 2, ".", "")).
					"&purch_order=".urlencode($this->common_vars["order_id"]["value"]).
					"&bill_name1=".urlencode($post_form["cc_first_name"]).
					"&bill_name2=".urlencode($post_form["cc_last_name"]).
					"&bill_street=".urlencode($this->common_vars["billing_address"]["value"]).
					"&bill_city=".urlencode($this->common_vars["billing_city"]["value"]).
					"&bill_state=".urlencode($this->common_vars["billing_state_abbr"]["value"]).
					"&bill_zip=".urlencode($this->common_vars["billing_zip"]["value"]).
					"&bill_country=".urlencode($this->common_vars["billing_country_iso_a2"]["value"]).
					"&ship_name1=".urlencode($this->common_vars["shipping_name"]["value"]).
					"&ship_name2=".urlencode($this->common_vars["shipping_name"]["value"]).
					"&ship_street=".urlencode($this->common_vars["shipping_address"]["value"]).
					"&ship_city=".urlencode($this->common_vars["shipping_city"]["value"]).
					"&ship_state=".urlencode($this->common_vars["shipping_state_abbr"]["value"]).
					"&ship_zip=".urlencode($this->common_vars["shipping_zip"]["value"]).
					"&ship_country=".urlencode($this->common_vars["shipping_country_iso_a2"]["value"]).
					"&cust_phone=".urlencode($this->common_vars["billing_phone"]["value"]).
					"&cust_email=".urlencode($this->common_vars["billing_email"]["value"]).
					"&cust_ip=".urlencode($_SERVER["REMOTE_ADDR"]).
					"&cust_host=".urlencode(gethostbyaddr($_SERVER["REMOTE_ADDR"])).
					"&description=".urlencode($settings["CompanyName"]." Order #".$order->order_num);

				if (trim($post_form["cc_cvv2"]) == "")
				{
					$post_data.="&disable_cvv2=1";
				}
				else
				{
					$post_data.="&card_cvv2=".urlencode($post_form["cc_cvv2"]);
				}

				if (trim($_SERVER["HTTP_USER_AGENT"]) != "")
				{
					$post_data.="&cust_browser=".urlencode($_SERVER["HTTP_USER_AGENT"]);
				}

				if (trim($this->settings_vars[$this->id."_site_tag"]["value"]) != "")
				{
					$post_data.="&site_tag=".urlencode(trim($this->settings_vars[$this->id."_site_tag"]["value"]));
				}

				$_post_url = trim($this->url_to_gateway)==""?"https://secure.netbilling.com:1402/gw/sas/direct3.0":trim($this->url_to_gateway);

				$c = curl_init($_post_url);

				if ($settings["ProxyAvailable"] == "YES")
				{
					//curl_setopt($c, CURLOPT_VERBOSE, 1);
					if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
					{
						curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
					}
					curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
					if ($settings["ProxyRequiresAuthorization"] == "YES")
					{
						curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
					}

					curl_setopt($c, CURLOPT_TIMEOUT, 120);
				}

				curl_setopt($c, CURLOPT_VERBOSE, 0);
				curl_setopt($c, CURLOPT_HEADER, 0);
				curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
				curl_setopt($c, CURLOPT_POST, 1);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

				@set_time_limit(3000);

				curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
				curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
				$buffer = curl_exec($c);

				$requestData = array();
				parse_str($post_data, $requestData);

				$this->log("process request:", $requestData);
				$this->log("process response:\n".$buffer);

				if (curl_errno($c) == 0)
				{
					if ($buffer)
					{
						$_data = explode("&", $buffer);
						$data = array();
						for ($i=0; $i<count($_data); $i++)
						{
							$a = explode("=", $_data[$i]);
							$data[$a[0]] = urldecode($a[1]);
						}
						// Approved
						// 1 - Successful monetary transaction
						// I - Pending transaction, such as an unfunded ACH payment
						if (isset($data["status_code"]) && ($data["status_code"] == "1" || $data["status_code"] == "I"))
						{
							$this->createTransaction($db, $user, $order, $data);

							if ($this->support_ccs && $this->enable_ccs)
							{
								$card_data = array(
									"fn"=>$post_form["cc_first_name"],
									"ln"=>$post_form["cc_last_name"],
									"ct"=>$post_form["cc_type"],
									"cc"=>$post_form["cc_number"],
									"em"=>$post_form["cc_expiration_month"], 
									"ey"=>$post_form["cc_expiration_year"]
								);
								$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]));
							}
							$this->is_error = false;
							$this->error_message = "";
						}
						// Error
						else
						{
							$this->is_error = true;
							if (isset($data["auth_msg"]))
							{
								$this->is_error = true;
								$this->error_message = $data["auth_msg"];
							}
							else
							{
								$this->is_error = true;
								$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
							}
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
					}

					if ($this->is_error)
					{
						$this->storeTransaction($db, $user, $order, $buffer, '', false);
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = curl_error($c);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Incorrect form params. Please contact site administartor.";
			}
			return !$this->is_error;
		}
	}

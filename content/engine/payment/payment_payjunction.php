<?php
	$payment_processor_id = "payjunction";
	$payment_processor_name = "PayJunction";
	$payment_processor_class = "PAYMENT_PAYJUNCTION";
	$payment_processor_type = "cc";

	class PAYMENT_PAYJUNCTION extends PAYMENT_PROCESSOR
	{
		function PAYMENT_PAYJUNCTION()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "payjunction";
			$this->name = "PayJunction";
			$this->class_name = "PAYMENT_PAYJUNCTION";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			//$this->url_to_gateway = "https://payjunction.com/quick_link";
			return $this;
		}

		public function replaceDataKeys()
		{
			return array('dc_logon', 'dc_password', 'dc_number', 'dc_verification_number');
		}

		function isTestMode()
		{
			$this->testMode = $this->settings_vars[$this->id."_dc_test_request"]["value"] == "TRUE";
			return $this->testMode;
		}

		function process($db, $user, $order, $post_form)
		{
			global $settings;
			$post_array = array(
				"dc_logon" => $this->settings_vars[$this->id."_dc_logon"]["value"],
				"dc_password" => $this->settings_vars[$this->id."_dc_password"]["value"],
				"dc_transaction_amount" => $this->common_vars["order_total_amount"]["value"] - $this->common_vars["order_shipping_amount"]["value"],
				"dc_first_name" => $post_form["cc_first_name"],
				"dc_last_name" => $post_form["cc_last_name"],
				"dc_number" => $post_form["cc_number"],
				"dc_expiration_month" => $post_form["cc_expiration_month"],
				"dc_expiration_year" => $post_form["cc_expiration_year"],
				"dc_verification_number" => $post_form["cc_cvv2"],
				"dc_address" => $this->common_vars["billing_address1"]["value"]." ".$this->common_vars["billing_address2"]["value"],
				"dc_city" => $this->common_vars["billing_city"]["value"],
				"dc_state" => $this->common_vars["billing_state"]["value"],
				"dc_zipcode" => $this->common_vars["billing_zip"]["value"],
				"dc_country" => "US",
				"dc_transaction_type" => $this->settings_vars[$this->id."_dc_transaction_type"]["value"],
				"dc_test" => "No",
				"dc_version" => "1.2"
			);

			if ($this->common_vars["order_shipping_amount"]["value"] > 0)
			{
				$post_array["dc_shipping_amount"] = round($this->common_vars["order_shipping_amount"]["value"], 2);
			}

			$request = "";

			foreach ($post_array as $key => $val)
			{
				$request .= $key . "=" . urlencode($val) . "&";
			}

			$request = substr($request, 0, -1);
			$_post_url = trim($this->url_to_gateway)==""?"https://payjunction.com/quick_link":trim($this->url_to_gateway);
			$ch = curl_init($_post_url);

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

			curl_setopt($ch, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($ch, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

			$content = curl_exec($ch);


			$this->log("process Request:", $post_array);
			$this->log("process Response:\n".$content);

			$curl_errno = curl_errno($ch);
			$curl_error = curl_error($ch);
			curl_close($ch);

			if ($curl_errno > 0)
			{
				$this->error_message = $curl_errno.":-".$curl_error;
				$this->is_error = true;
			}
			else
			{
				$res=$content;
				$response = array();

				$content = explode(chr(28), $content); // The ASCII field separator character is the delimiter

				foreach ($content as $key_value) 
				{
					$a = explode("=", $key_value);
					$response[$a[0]] = $a[1];
				}

				if (isset($response['dc_response_code']))
				{
					if (strcmp($response['dc_response_code'], "00") == 0 || strcmp ($response['dc_response_code'], "85") == 0)
					{
						$this->createTransaction($db, $user, $order, $response);
						$this->is_error = false;
						$this->error_message = "";
					}
					else
					{
						$this->error_message.= 
							$response['dc_response_message']." - ".
							$response['dc_response_code'];
						
						$this->is_error = true;
					}
				}
				else
				{
					$this->is_error = false;
					$this->error_message = "Cannot process gateway response";

					$this->storeTransaction($db, $user, $order, $content, '', false);
				}
			}
			return !$this->is_error;
		}
	}

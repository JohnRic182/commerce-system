<?php 
	$payment_processor_id = "paypaladv";
	$payment_processor_name = "PayPal Payments Advanced";
	$payment_processor_class = "PAYMENT_PAYPALADV";
	$payment_processor_type = "ipn";

	require_once dirname(__FILE__).'/payment_paypalpflink.php';

	class PAYMENT_PAYPALADV extends PAYMENT_PAYPALPFLINK
	{
		public function PAYMENT_PAYPALADV()
		{
			parent::PAYMENT_PAYPALPFLINK();
			$this->id = "paypaladv";
			$this->name = "PayPal Payments Advanced";
			$this->class_name = "PAYMENT_PAYPALADV";
			$this->type = "ipn";
			$this->description = "";
			$this->allow_change_gateway_url = false;
			$this->post_url = "https://payflowlink.paypal.com";
			$this->steps = 2;
			$this->override_payment_form = false;
			
			$this->reload_parent_on_process = true;
			
			$this->payment_in_popup = true;
			$this->payment_in_popup_width = 540;
			$this->payment_in_popup_height = 680;
			
			$this->buttonSource = getpp().'_Cart_PPA';

			return $this;
		}
	}

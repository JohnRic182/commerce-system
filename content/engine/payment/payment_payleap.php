<?php
	$payment_processor_id = "payleap";
	$payment_processor_name = "PayLeap";
	$payment_processor_class = "PAYMENT_PAYLEAP";
	$payment_processor_type = "cc";

	class PAYMENT_PAYLEAP extends PAYMENT_PROCESSOR
	{
		function PAYMENT_PAYLEAP()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "payleap";
			$this->name = "PayLeap";
			$this->class_name = "PAYMENT_PAYLEAP";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->support_ccs = false;
			$this->need_cc_codes = true;
			return $this;
		}

		function isTestMode()
		{
			$this->testMode = $this->settings_vars[$this->id."_Test_Mode"]["value"] == "True";

			if ($this->testMode)
			{
				$this->url_to_gateway = 'https://uat.payleap.com/TransactServices.svc';
			}

			return $this->testMode;
		}

		function call($end_point, $post_data)
		{
			@set_time_limit(3000);

			global $settings, $db;

			$_post_url = (trim($this->url_to_gateway) == "" ? "https://secure1.payleap.com/TransactServices.svc" : trim($this->url_to_gateway))."/".$end_point;

			$c = curl_init($_post_url);

			if ($settings["ProxyAvailable"] == "YES")
			{
				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}

				curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}

				curl_setopt($c, CURLOPT_TIMEOUT, 120);
			}

			curl_setopt($c, CURLOPT_VERBOSE, 0);
			curl_setopt($c, CURLOPT_HEADER, 0);
			curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

			$buffer = curl_exec($c);

			if (curl_errno($c))
			{
				$this->error_message = curl_error($c);
				$this->is_error = true;
			}
			else
			{
				if ($buffer && trim($buffer) != "")
				{
					$result = simplexml_load_string($buffer);
					if (!$result)
					{
						$this->error_message = "Cannot parse server response. Please try again";

						$this->is_error = true;
					}
					return $result;
				}
			}
			return false;
		}

		//main payment module entry point
		function process($db, $user, $order, $post_form)
		{
			global $settings, $backurl, $order;

			$post_data =
				"UserName=".htmlspecialchars($this->settings_vars[$this->id."_Api_Login_Id"]["value"]).
				"&Password=".htmlspecialchars($this->settings_vars[$this->id."_Transaction_Key"]["value"]).
				"&TransType=".htmlspecialchars($this->settings_vars[$this->id."_Transaction_Type"]["value"]).
				"&CardNum=".htmlspecialchars($post_form["cc_number"]).
				"&ExpDate=".htmlspecialchars($post_form["cc_expiration_month"].substr($post_form["cc_expiration_year"], 2, 2)).
				"&MagData=".
				"&PNRef=".
				"&NameOnCard=".htmlspecialchars($post_form["cc_first_name"]." ".$post_form["cc_last_name"]).
				"&Street=".htmlspecialchars($this->common_vars["billing_address"]["value"]).
				"&City=".htmlspecialchars($this->common_vars["billing_city"]["value"]).
				"&State=".htmlspecialchars($this->common_vars["billing_state_abbr"]["value"]).
				"&Zip=".htmlspecialchars($this->common_vars["billing_zip"]["value"]).
				"&Country=".htmlspecialchars($this->common_vars["billing_country_iso_a2"]["value"]).
				"&Amount=".htmlspecialchars(number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")).
				"&InvNum=".htmlspecialchars($this->common_vars["order_id"]["value"]).
				"&CVNum=".htmlspecialchars($post_form["cc_cvv2"]).
				"&PONum=".
				"&ExtData=".
					"<CustCode>".htmlspecialchars($this->common_vars["customer_id"]["value"])."</CustCode>".
					"<TrainingMode>".($this->testMode?"T":"F")."</TrainingMode>".
					"<City>".htmlspecialchars($this->common_vars["billing_city"]["value"])."</City>".
					"<BillToState>".htmlspecialchars($this->common_vars["billing_state_abbr"]["value"])."</BillToState>".
					"<CustomerID>".htmlspecialchars($this->common_vars["customer_id"]["value"])."</CustomerID>".
					"<CVPresence>".($post_form["cc_cvv2"]==""?"NotPresent":"Submitted Illegible")."</CVPresence>".
					"<EntryMode>MANUAL</EntryMode>".
					"<Invoice>".
						"<InvNum>".htmlspecialchars($this->common_vars["order_id"]["value"])."</InvNum>".
						//"<Date>".date("ymd")."</Date>".
						"<BillTo>".
							"<CustomerId>".htmlspecialchars($this->common_vars["customer_id"]["value"])."</CustomerId>".
							"<Name>".htmlspecialchars($this->common_vars["billing_name"]["value"])."</Name>".
							"<Address>".
								"<Street>".htmlspecialchars($this->common_vars["billing_address"]["value"])."</Street>".
								"<City>".htmlspecialchars($this->common_vars["billing_city"]["value"])."</City>".
								"<State>".htmlspecialchars($this->common_vars["billing_state_abbr"]["value"])."</State>".
								"<Zip>".htmlspecialchars($this->common_vars["billing_zip"]["value"])."</Zip>".
								"<Country>".htmlspecialchars($this->common_vars["billing_country_iso_a2"]["value"])."</Country>".
							"</Address>".
							"<Email>".htmlspecialchars($this->common_vars["billing_email"]["value"])."</Email>".
							"<Phone>".htmlspecialchars($this->common_vars["billing_phone"]["value"])."</Phone>".
							"<CustCode>".htmlspecialchars($this->common_vars["customer_id"]["value"])."</CustCode>".
							//"<PONum>".htmlspecialchars($this->common_vars["order_id"]["value"])."</PONum>".
							"<PONum></PONum>".
						"</BillTo>".
						"<TotalAmt>".htmlspecialchars(number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""))."</TotalAmt>".
					"</Invoice>";

			$result = $this->call("ProcessCreditCard", $post_data);

			$this->log("process Request:\n".$post_data);
			$this->log("process Response:\n".array2text(xml2array($result)));

			if ($result)
			{
				if ($result->Result == 0 && $result->RespMSG == "Approved")
				{
					$response = "";
					foreach ($result as $key=>$value)
					{
						$response .= $key." = ".$value."\n";
					}
					$this->createTransaction($db, $user, $order, $response, "",
						$this->settings_vars[$this->id."_Transaction_Type"]["value"], //transaction type
						$this->settings_vars[$this->id."_Transaction_Type"]["value"] != "Sale" ?
							base64_encode(serialize(array(
								"Amount" => number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""),
								"PNRef" => (string)$result->PNRef,
								"AuthCode" => (string)$result->AuthCode,
								"C4" => substr($post_form["cc_number"], -4),
								"ExpDate" => $post_form["cc_expiration_month"].substr($post_form["cc_expiration_year"], 2, 2)
							))) : ""
					);
					$this->is_error = false;
					$this->error_message = "";
					return $this->settings_vars[$this->id."_Transaction_Type"]["value"] == "Sale" ? "Received" : "Pending";
				}
				else
				{
					$this->error_message = $result->RespMSG." (".$result->Result.") ";
					$this->is_error = true;

					$this->storeTransaction($db, $user, $order, $result, '', false);
				}
			}

			return false;
		}

		public function supportsCapture($order_data = false)
		{
			return $this->capturableAmount($order_data) > 0;
		}

		function supportsPartialCapture($order_data = false)
		{
			return false;
		}

		public function capturableAmount($order_data = false)
		{
			if ($order_data['custom1'] == 'Auth' && $order_data['payment_status'] == 'Pending')
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'] - $this->capturedAmount($order_data);
			}

			return 0;
		}

		public function capturedAmount($order_data = false)
		{
			if ($order_data["custom1"] == "Auth")
			{
				$data = unserialize(base64_decode($order_data["custom2"]));

				return $data["Amount"];
			}
			else if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_RECEIVED)
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'];
			}

			return false;
		}

		public function capture($order_data = false, $capture_amount = false)
		{
			global $order, $db;

			$this->is_error = false;

			$oldPaymentStatus = $order->getPaymentStatus();

			$db->query("SELECT * FROM ".DB_PREFIX."orders WHERE oid=".$order_data['oid']);
			if ($db->moveNext())
			{
				$order_data = $db->col;

				//check is transaction completed
				if ($order_data["custom1"] != "Auth")
				{
					$this->is_error = true;
					$this->error_message = "Transaction already completed";
					return false;
				}

				$data = unserialize(base64_decode($order_data["custom2"]));

				$post_data =
					"UserName=".htmlspecialchars($this->settings_vars[$this->id."_Api_Login_Id"]["value"]).
					"&Password=".htmlspecialchars($this->settings_vars[$this->id."_Transaction_Key"]["value"]).
					"&TransType=".htmlspecialchars("Force").
					"&CardNum=".htmlspecialchars($data["C4"]).
					"&ExpDate=".htmlspecialchars($data["ExpDate"]).
					"&MagData=0".
					"&Amount=".htmlspecialchars($data["Amount"]).
					"&PNRef=".htmlspecialchars($data["PNRef"]).
					"&ExtData=<AuthCode>".htmlspecialchars($data["AuthCode"])."</AuthCode>";

				$result = $this->call("ProcessCreditCard", $post_data);

				$this->log("processCustom Request:\n".$post_data);
				$this->log("processCustom Response:\n".array2text(xml2array($result)));

				if ($result)
				{
					if ($result->Result == 0 && $result->RespMSG == "Approved")
					{
						$response = "";
						foreach ($result as $key=>$value)
						{
							$response .= $key." = ".$value."\n";
						}

						$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);

						$db->reset();
						$db->assignStr("custom1", "");
						$db->assignStr("custom2", "");
						$db->assignStr("custom3", "");
						$db->assignStr("payment_status", $order->getPaymentStatus());
						$db->update(DB_PREFIX."orders", "WHERE oid=".$order_data['oid']);

						$userTmp = new tmp_object();
						$userTmp->id = $order_data["uid"];

						$orderTmp = new tmp_object();
						$orderTmp->oid = $order_data["oid"];
						$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
						$orderTmp->totalAmount = $order_data["total_amount"];
						$orderTmp->shippingAmount = $order_data["shipping_amount"];
						$orderTmp->taxAmount = $order_data["tax_amount"];
						$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

						$this->createTransaction($db, $userTmp, $orderTmp, $response, "",
							"Force"
						);
						$this->is_error = false;
						$this->error_message = "";
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $result->RespMSG." (".$result->Result.") ";
					}
				}
			}

			$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

			return !$this->is_error;
		}
	}


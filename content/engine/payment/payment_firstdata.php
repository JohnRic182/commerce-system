<?php

$payment_processor_id = 'firstdata';
$payment_processor_name = 'FirstData';
$payment_processor_class = 'PAYMENT_FIRSTDATA';
$payment_processor_type = 'cc';

class PAYMENT_FIRSTDATA extends PAYMENT_PROCESSOR
{
	function PAYMENT_FIRSTDATA()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = 'firstdata';
		$this->name = 'First Data Payeezy Gateway';
		$this->class_name = 'PAYMENT_FIRSTDATA';
		$this->type = 'cc';
		$this->description = '';
		$this->steps = 1;
		$this->testMode = false;
		$this->support_ccs = false;
		$this->need_cc_codes = true;
		return $this;
	}

	/**
	 * Get payment form
	 *
	 * @param $db
	 *
	 * @return array
	 */
	public function getPaymentForm($db)
	{
		$fields = array();

		$saveCreditCardAdded = false;

		if (isset($this->order) && $this->order && $this->order instanceof ORDER && $this->order->hasRecurringBillingItems())
		{
			$fields['save_credit_card'] = array('type' => 'static', 'caption' => 'This card will be securely stored for future recurring payments', 'wrapperClass'=>'payment-form-cc-will-be-stored');
			$saveCreditCardAdded = true;
		}

		$standardFields = parent::getPaymentForm($db);

		$fields = array_merge($fields, $standardFields);

		if ($this->isCardinalCommerceEnabled())
		{
			$fields['do_cmpi_lookup'] = array('type' => 'hidden', 'name'=> 'do_cmpi_lookup', 'value' => 1);
		}

		if (!$saveCreditCardAdded && $this->supportsPaymentProfiles() && isset($this->user) && !is_null($this->user) && isset($this->user->auth_ok) && $this->user->auth_ok && (!isset($this->user->express) || !$this->user->express))
		{
			$fields['save_credit_card'] = array('type' => 'checkbox', 'name' => 'form[save_credit_card]', 'value' => '1', 'caption' => 'Save this card for later use');
		}

		return $fields;
	}

	/**
	 * Replace data keys
	 *
	 * @return array
	 */
	public function replaceDataKeys()
	{
		return array('password', 'cc_number', 'cc_verification_str2');
	}

	/**
	 * Check is gateway is in a test mode
	 * @return bool
	 */
	public function isTestMode()
	{
		global $settings;

		if (strtolower($settings['firstdata_Test_Request']) == 'true')
		{
			$this->testMode = true;
		}
		else
		{
			$this->testMode = false;
		}
		return $this->testMode;
	}

	/**
	 * Get gateway URL
	 *
	 * @return string
	 */
	protected function getGatewayUrl()
	{
		if ($this->isTestMode())
		{
			return 'https://api.demo.globalgatewaye4.firstdata.com/transaction/v12';
		}

		return 'https://api.globalgatewaye4.firstdata.com/transaction/v12';
	}

	/**
	 * Process payment entry point
	 *
	 * @param $db
	 * @param $user
	 * @param $order
	 * @param $post_form
	 *
	 * @return bool|string
	 */
	public function process($db, $user, $order, $post_form)
	{
		if ($this->isCardinalCommerceEnabled())
		{
			$result = $this->processCardinalCommerce($db, $user, $order, $post_form);
		}
		else
		{
			$result = $this->processPaymentForm($db, $user, $order, $post_form);
		}

		return $this->is_error ? false : $result;
	}

	/**
	 * Process cardinal commerce
	 *
	 * @param $db
	 * @param $user
	 * @param $order
	 * @param $post_form
	 * @return bool|string
	 */
	protected function processCardinalCommerce($db, $user, $order, &$post_form)
	{
		global $settings;

		$authorizeResult = false;

		if (isset($post_form['payment_method_way']) && $post_form['payment_method_way'] == 'profile')
		{
			return $this->processPaymentForm($db, $user, $order, $post_form);
		}

		//create object and set it up
		$cardinal = new CardinalCommerce();
		$cardinal->common_vars = $this->common_vars;

		if (isset($_REQUEST['do_cmpi_lookup']))
		{
			$cardinal_lookup_xml = $cardinal->getLookupXML(
				$post_form['cc_first_name'],
				$post_form['cc_last_name'],
				$post_form['cc_number'],
				$post_form['cc_expiration_month'],
				$post_form['cc_expiration_year'],
				$this->currency['code']
			);

			$cardinal_lookup_result = $cardinal->processRequest($cardinal_lookup_xml);

			if ($cardinal_lookup_result)
			{
				if ($cardinal_lookup_result['CardinalMPI'][0]['Enrolled'][0] == 'Y')
				{
					$this->redirect = true;
					$this->redirectURL = $settings['GlobalHttpsUrl'].'/'.$settings['INDEX'].'?pcsid='.session_id().'&p=payment_validation';

					$_SESSION['CardinalPostForm'] = $post_form;
					$_SESSION['CardinalTransactionId'] = $cardinal_lookup_result['CardinalMPI'][0]['TransactionId'][0];
					$_SESSION['CardinalACSUrl'] = $cardinal_lookup_result['CardinalMPI'][0]['ACSUrl'][0];
					$_SESSION['CardinalPayload'] = $cardinal_lookup_result['CardinalMPI'][0]['Payload'][0];
					//validator will replace ORDER_PROCESS_PAYMENT_VALIDATION with ORDER_PROCESS_PAYMENT
					$_SESSION['CardinalNotifyUrl'] = $settings['GlobalHttpsUrl'].'/'.$settings['INDEX'].'?pcsid='.session_id().'&p='.$this->payment_page.'&oa='.ORDER_PROCESS_PAYMENT_VALIDATION.'&do_cmpi_authenticate=true';
					$_SESSION['CardinalRedirectUrl'] = $settings['GlobalHttpsUrl'].'/'.$settings['INDEX'];

					return false;
				}
				else
				{
					//do not redirect, process simple authorize transaction
					$authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form);
				}
			}
			else
			{
				if ($cardinal->is_fatal_error)
				{
					$this->is_error = true;
					$this->error_message = $cardinal->error_message;
				}
				else
				{
					$authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form);
				}
			}
		}

		if (isset($_REQUEST['do_cmpi_authenticate']) && isset($_SESSION['CardinalPostForm']) && isset($_REQUEST['PaRes']))
		{
			$post_form = $_SESSION['CardinalPostForm'];

			unset($_SESSION['CardinalPostForm']);

			$cardinal_auth_xml = $cardinal->getAuthenticateXML($_REQUEST['PaRes']);
			$cardinal_auth_result = $cardinal->processRequest($cardinal_auth_xml);

			if ($cardinal_auth_result)
			{
				$EciFlag = $cardinal_auth_result['CardinalMPI'][0]['EciFlag'][0];
				$PAResStatus = $cardinal_auth_result['CardinalMPI'][0]['PAResStatus'][0];
				$Cavv = $cardinal_auth_result['CardinalMPI'][0]['Cavv'][0];
				$SignatureVerification = $cardinal_auth_result['CardinalMPI'][0]['SignatureVerification'][0];
				$Xid = $cardinal_auth_result['CardinalMPI'][0]['Xid'][0];

				if ($SignatureVerification == 'Y' && $PAResStatus != 'N')
				{
					switch ($PAResStatus)
					{
						case 'Y' :
						case 'A' : $authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form, $EciFlag, $Cavv, $Xid); break;
						case 'U' : $authorizeResult = $this->processPaymentForm($db, $user, $order, $post_form); break;
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = 'Your card can not be validated. Please try other card or other payment method';
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = $cardinal->error_message;
			}
		}

		return $authorizeResult;
	}

	/**
	 * Process payment form
	 *
	 * @param $db
	 * @param USER $user
	 * @param ORDER $order
	 * @param $postForm
	 * @param bool $EciFlag
	 * @param bool $Cavv
	 * @param bool $Xid
	 *
	 * @return bool|string
	 */
	function processPaymentForm($db, USER $user, ORDER $order, $postForm, $EciFlag = false, $Cavv = false, $Xid = false)
	{
		$paymentProfileId = isset($postForm['payment_profile_id']) ? $postForm['payment_profile_id'] : false;
		$paymentProfileId = isset($postForm['payment_method_way']) && $postForm['payment_method_way'] != 'profile' ? false : $paymentProfileId;

		/**
		 * Prepare transaction data
		 */
		$transactionData = array(
			'amount' => $this->common_vars['order_total_amount']['value'],
			'billing_address1' => $this->common_vars['billing_address1']['value'],
			'billing_zip' => $this->common_vars['billing_zip']['value'],
			'billing_city' => $this->common_vars['billing_city']['value'],
			'billing_state_abbr' => $this->common_vars['billing_state_abbr']['value'],
			'billing_country_iso_a3' => $this->common_vars['billing_country_iso_a3']['value'],
			'tax_amount' => $this->common_vars['order_tax_amount']['value'],
			'client_ip' => $_SERVER['REMOTE_ADDR'],
			'client_email' => $this->common_vars['billing_email']['value'],
			'ecommerce_flag' => 7
		);

		$paymentProfile = null;

		/**
		 * Pay using payment profile
		 */
		if ($paymentProfileId)
		{
			$paymentProfile = $this->getPaymentProfileRepository()->getById($paymentProfileId, $user->getId());

			$billingData = $paymentProfile->getBillingData();

			$transactionData['cardholder_name'] = $billingData->getName();
			$transactionData['transarmor_token'] = $paymentProfile->getExternalProfileId();
			$transactionData['cc_expiration_month'] = $billingData->getCcExpirationMonth();
			$transactionData['cc_expiration_year'] = $billingData->getCcExpirationYear();
			$transactionData['credit_card_type'] = $billingData->getCcType();
			$transactionData['payment_profile_id'] = $paymentProfile->getId();
		}
		/**
		 * Pay using cc card
		 */
		else
		{
			$transactionData['cardholder_name'] = $postForm['cc_first_name'].' '.$postForm['cc_last_name'];
			$transactionData['cc_number'] = $postForm['cc_number'];
			$transactionData['cc_expiration_month'] = $postForm['cc_expiration_month'];
			$transactionData['cc_expiration_year'] = $postForm['cc_expiration_year'];
			$transactionData['cc_verification_str2'] = $postForm['cc_cvv2'];
		}

		/**
		 * Extra payment validation
		 */
		if ($EciFlag !== false)
		{
			$transactionData['cavv'] = $Cavv;
			$transactionData['xid'] = $Xid;
			$transactionData['ecommerce_flag'] = intval($EciFlag);
		}

		return $this->processPayment($transactionData, $db, $order, false, $postForm, $paymentProfile);
	}

	/**
	 * Process payment
	 *
	 * @param array $transactionData
	 * @param DB $db
	 * @param ORDER $order
	 * @param bool $isChild
	 * @param bool $postForm
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool|string
	 */
	function processPayment(array $transactionData, DB $db, ORDER $order, $isChild = false, $postForm = false, PaymentProfiles_Model_PaymentProfile $paymentProfile = null)
	{
		$user = $order->getUser();

		$paymentRequestData = array(
			'gateway_id' => $this->settings_vars[$this->id.'_GatewayId']['value'],
			'password' => $this->settings_vars[$this->id.'_Password']['value'],
			'transaction_type' => $this->settings_vars[$this->id.'_Auth_Type']['value'] == 'Auth-Capture' ? '00' : '01',
			'amount' => number_format($transactionData['amount'], 2, '.', ''),
			'cc_verification_str1' => $transactionData['billing_address1'].'|'.$transactionData['billing_zip'].'|'.
				$transactionData['billing_city'].'|'.$transactionData['billing_state_abbr'].'|'.$transactionData['billing_country_iso_a3'],
			'cvd_presence_ind' => 1,
			'zip_code' => $transactionData['billing_zip'],
			'tax1_amount' => number_format($transactionData['tax_amount'], 2, '.', ''),
			'customer_ref' => $order->getId(),
			'client_ip' => isset($transactionData['client_ip']) ? $transactionData['client_ip'] : '',
			'client_email' => $transactionData['client_email'],
			'currency_code' => $this->settings_vars[$this->id.'_Currency_Code']['value'],
			'ecommerce_flag' => 7,
			'reference_no' => $order->getOrderNumber(),
		);

		if ($transactionData['amount'] == 0)
		{
			//Pre-Authorization Only when processing 0 amount order
			$paymentRequestData['transaction_type'] = '05';
		}

		$paymentRequestData['cardholder_name'] = $transactionData['cardholder_name'];

		if (isset($transactionData['transarmor_token']))
		{
			$paymentRequestData['transarmor_token'] = $transactionData['transarmor_token'];
			$paymentRequestData['credit_card_type'] = $transactionData['credit_card_type'];
			$paymentProfileUsed = true;
		}
		else
		{
			$paymentRequestData['cc_number'] = $transactionData['cc_number'];
			$paymentRequestData['cc_verification_str2'] = $transactionData['cc_verification_str2'];
			$paymentProfileUsed = false;
		}

		$paymentRequestData['cc_expiry'] = $transactionData['cc_expiration_month'].substr($transactionData['cc_expiration_year'], -2);

		if (isset($transactionData['cavv']))
		{
			$paymentRequestData['cavv'] = $transactionData['cavv'];
			$paymentRequestData['xid'] = $transactionData['xid'];
			$paymentRequestData['ecommerce_flag'] = $transactionData['ecommerce_flag'];
		}

		$this->log('Request: ', $paymentRequestData);

		$result = $this->process_curl($paymentRequestData);

		if ($result)
		{
			$paymentResponseData = @json_decode($result);

			if ($paymentResponseData)
			{
				$this->log('Response: ', (array)$paymentResponseData);

				/**
				 * Transaction approved
				 */
				if ($paymentResponseData->transaction_approved)
				{
					$extra = $paymentResponseData->transaction_tag;

					$custom1 = $this->settings_vars[$this->id.'_Auth_Type']['value'] == 'Auth-Capture' ? 'AUTH_CAPTURE' : 'AUTH_ONLY';

					//If Auth only, then store auth'd amount to custom2
					$custom2 = $this->settings_vars[$this->id.'_Auth_Type']['value'] != 'Auth-Capture' ? number_format($order->getTotalAmount(), 2, '.', '') : '';

					//If Auth Capture, then store captured amount to custom3
					$custom3 = $this->settings_vars[$this->id.'_Auth_Type']['value'] == 'Auth-Capture' ? number_format($order->getTotalAmount(), 2, '.', '') : 0;

					$securityId = $paymentResponseData->authorization_num;

					/**
					 * Store transaction
					 */
					$this->createTransaction($db, $user, $order, (array)$paymentResponseData, $extra, $custom1, $custom2, $custom3, $securityId);

					/**
					 * Store payment profile when needed
					 */
					if (!$isChild && $postForm)
					{
						$orderHasRecurringItems = $order->hasRecurringBillingItems();

						if (!$paymentProfileUsed && is_null($paymentProfile) && (isset($postForm['save_credit_card']) || $orderHasRecurringItems))
						{
							$token = $paymentResponseData->transarmor_token;
							$ccType = $paymentResponseData->credit_card_type;

							if ($token && $token != '')
							{
								$paymentProfile = new PaymentProfiles_Model_PaymentProfile();

								$billingData = $paymentProfile->getBillingData();

								$address = $this->common_vars['billingAddress']['value'];

								// override names
								$address->setFirstName($postForm['cc_first_name']);
								$address->setLastName($postForm['cc_last_name']);

								$billingData->setAddress($address);
								$billingData->setName(trim(trim($postForm['cc_first_name']).' '.trim($postForm['cc_last_name'])));
								$billingData->setPhone($this->common_vars['billing_phone']['value']);
								$billingData->setCcType($ccType);
								$billingData->setCcNumber($postForm['cc_number']);
								$billingData->setCcExpirationMonth($postForm['cc_expiration_month']);
								$billingData->setCcExpirationYear($postForm['cc_expiration_year']);
								$billingData->setCcExpirationDate($postForm['cc_expiration_month'].'/'.$postForm['cc_expiration_year']);

								$paymentProfile->setBillingData($billingData);
								$paymentProfile->setUserId($user->getId());
								$paymentProfile->setExternalProfileId($token);
								$paymentProfile->setPaymentMethodId($this->db_id);


								$this->getPaymentProfileRepository()->persist($paymentProfile);
							}
						}

						/**
						 * Do some extra stuff for recurring payment
						 */
						if ($orderHasRecurringItems)
						{
							/**
							 * At this point payment profile is REQUIRED
							 */
							if (is_null($paymentProfile))
							{
								/**
								 * Cancel transaction when failed to create payment profile
								 */
								$orderData = array(
									'total_amount' => $order->getTotalAmount(),
									'security_id' => $securityId,
									'oid' => $order->getId(),
								);
								$this->void($orderData);
								$this->is_error = true;
								$this->error_message = 'We are sorry, but there has been an error processing this transaction.<br>The transaction response did not return a TransArmor token.';
							}
							else
							{
								$orderRepository = DataAccess_OrderRepository::getInstance();

								/** @var Model_LineItem $item */
								foreach ($order->lineItems as $item)
								{
									if ($item->getEnableRecurringBilling() && !is_null($item->getRecurringBillingData()))
									{
										/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
										$recurringBillingData = $item->getRecurringBillingData();

										$recurringBillingData->setPaymentProfileId($paymentProfile->getId());

										$item->setRecurringBillingData($recurringBillingData);

										$orderRepository->persistLineItem($order, $item);
									}
								}
							}
						}
					}

					if (!$this->is_error)
					{
						$this->is_error = false;
						$this->error_message = '';
						return $this->settings_vars[$this->id.'_Auth_Type']['value'] == 'Auth-Capture' ? ORDER::PAYMENT_STATUS_RECEIVED : ORDER::PAYMENT_STATUS_PENDING;
					}
				}
				else
				{
					$this->storeTransaction($db, $user, $order, (array)$paymentResponseData);

					//TODO: Fatal?
					if (in_array($paymentResponseData->exact_resp_code, array('08', '22', '25', '26', '27', '28', '31', '32', '57', '58', '60', '63', '64', '68', '72', '93')))
					{
						$this->is_error = true;
						$this->error_message = 'We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: '.$paymentResponseData->exact_message;
					}
					else
					{
						$this->is_error = true;
						$this->is_fatal = true;
						$this->error_message = 'We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: '.$paymentResponseData->bank_message;
					}
				}
			}
			else
			{
				$this->log('Response: '. $result);
				$this->storeTransaction($db, $user, $order, $paymentResponseData ? $paymentResponseData : $result);

				$this->is_error = true;
				$this->is_fatal = true;
				$this->error_message = 'We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: '.$result;
			}
		}
		else
		{
			$this->is_error = true;
			$this->is_fatal = true;
			$this->error_message = 'We are sorry, but there has been an error processing this transaction.';
		}

		return !$this->is_error;
	}

	/**
	 * Send data to gateway
	 *
	 * @param $post_data
	 *
	 * @return bool|mixed
	 */
	function process_curl($post_data)
	{
		global $settings;

		$post_url = $this->getGatewayUrl();

		$c = curl_init($post_url);

		if ($settings['ProxyAvailable'] == 'YES')
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined('CURLOPT_PROXYTYPE') && defined('CURLPROXY_HTTP') && defined('CURLPROXY_SOCKS5'))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings['ProxyType'] == 'HTTP' ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($c, CURLOPT_PROXY, $settings['ProxyAddress'].':'.$settings['ProxyPort']);
			if ($settings['ProxyRequiresAuthorization'] == 'YES')
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings['ProxyUsername'].':'.$settings['ProxyPassword']);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}


		$keyId = $settings['firstdata_KeyId'];
		$hmacKey = $settings['firstdata_HmacKey'];

		$content = json_encode($post_data);

		$contentType = 'application/json; charset=UTF-8';
		$contentDigest = sha1($content);
		$gge4Date = gmdate('c');
		$uri = parse_url($post_url,  PHP_URL_PATH);

		$hmacContent = "POST\n".$contentType."\n".$contentDigest."\n".$gge4Date."\n".$uri;

		$hmacHash = base64_encode(hash_hmac('sha1', $hmacContent, $hmacKey, true));

		$headers = array(
			'Content-Type: '.$contentType,
			'Accept: application/json',
			'X-GGe4-Content-SHA1: '.$contentDigest,
			'X-GGe4-Date: '.$gge4Date,
			'Authorization: GGE4_API '.$keyId.':'.$hmacHash
		);

		curl_setopt($c, CURLOPT_VERBOSE, 0);
		curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($c, CURLOPT_POSTFIELDS, $content);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

		@set_time_limit(3000);

		$buffer = curl_exec($c);

		if (curl_errno($c) > 0)
		{
			$this->is_error = true;
			$this->error_message = curl_error($c);
			return false;
		}
		else if (!$buffer || $buffer == '')
		{
			$this->is_error = true;
			$this->error_message = 'Got empty response from the server';
			return false;
		}

		return $buffer;
	}

	function supportsCapture($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == 'Pending' || $order_data['payment_status'] == 'Partial')
			{
				if ($this->isTotalAmountCaptured($order_data))
					return false;

				return true;
			}
		}
		return false;
	}

	function supportsPartialCapture($order_data = false)
	{
		return false;
	}

	function isTotalAmountCaptured($order_data)
	{
		return $order_data['custom3'] == $order_data['total_amount'];
	}

	public function capturedAmount($order_data = false)
	{
		return $order_data ? $order_data['custom3'] : false;
	}

	function capturableAmount($order_data=false)
	{
		if ($order_data)
		{
			if ($this->supportsCapture($order_data))
			{
				$capturableAmount = $order_data['total_amount'] - $order_data['custom3'];
				if ($capturableAmount < 0)
					$capturableAmount = 0.0;

				return number_format($capturableAmount, 2, '.', '');
			}
		}
		return 0;
	}

	function getTotalCaptured($order_data)
	{
		return 0.00 + $order_data['custom3'];
	}

	function getLastTransactionId($oid)
	{
		global $db;

		$transactionId = $db->selectOne('SELECT extra FROM '.DB_PREFIX.'payment_transactions WHERE oid='.$oid.' AND is_success = 1 ORDER BY tid DESC LIMIT 1');

		if ($transactionId)
		{
			$transactionId = array_shift($transactionId);
		}

		return $transactionId;
	}

	function capture($order_data=false, $capture_amount=false)
	{
		global $db, $order;

		$oldPaymentStatus = $order->getPaymentStatus();
		$user = $order->getUser();

		$this->is_error = false;

		if ($order_data)
		{
			//check is transaction completed
			if ($this->isTotalAmountCaptured($order_data))
			{
				$this->is_error = true;
				$this->error_message = 'Transaction already completed';
				return false;
			}

			//get captured total amount & x_trans_id
			$captured_total_amount = $capture_amount ? $capture_amount : $order_data['custom2'];
			$transaction_id = $this->getLastTransactionId($order->getId());
			$authorization_num = $order_data['security_id'];

			if ($transaction_id == '')
			{
				$this->is_error = true;
				$this->error_message = 'This order does not have a valid transaction id';
				return false;
			}
			else
			{
				if ($captured_total_amount <= 0)
				{
					$this->is_error = true;
					$this->error_message = 'Amount to capture must be greater than 0';
					return false;
				}
				else if ($captured_total_amount > $order_data['total_amount'])
				{
					$this->is_error = true;
					$this->error_message = 'Amount to capture cannot be more than total amount';
					return false;
				}
				else if ($order_data['custom3'] + $captured_total_amount > $order_data['total_amount'])
				{
					$this->is_error = true;
					$this->error_message = 'Amount to capture cannot be more than total amount';
					return false;
				}

				$post_data = array(
					'gateway_id' => $this->settings_vars[$this->id.'_GatewayId']['value'],
					'password' => $this->settings_vars[$this->id.'_Password']['value'],
					'transaction_type' => '32',
					'amount' => number_format($captured_total_amount, 2, '.', ''),
					'transaction_tag' => $transaction_id,
					'authorization_num' => $authorization_num,
				);

				$this->log('Request: ', $post_data);

				$buffer = $this->process_curl($post_data);

				if ($buffer)
				{
					$response = @json_decode($buffer);

					if ($response)
					{
						$this->log('capture Response:', (array)$response);

						if ($response->transaction_approved)
						{
							$totalCaptured = $this->getTotalCaptured($order_data) + $captured_total_amount;
							$custom3 = number_format($totalCaptured, 2, '.', '');

							$db->reset();
							$db->assignStr('custom1', '');
							$db->assignStr('custom2', '');
							$db->assignStr('custom3', $custom3);

							$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);

							if ($order_data['total_amount'] > $totalCaptured)
							{
								$order->setPaymentStatus(ORDER::PAYMENT_STATUS_PARTIAL);
								$extra = $transaction_id;
								$security_id = $authorization_num;
							}
							else
							{
								$extra = $response->transaction_tag;
								$security_id = $response->authorization_num;
							}

							$db->assignStr('payment_status', $order->getPaymentStatus());

							$db->update(DB_PREFIX.'orders', "WHERE oid='".$order_data['oid']."'");

							$order_data['custom1'] = 'PRIOR_AUTH_CAPTURE';
							$order_data['custom2'] = '';
							$order_data['custom3'] = $custom3;
							$order_data['payment_status'] = $order->getPaymentStatus();

							$custom1 = $order_data['custom1'];
							$custom2 = $order_data['custom2'];

							$this->createTransaction($db, $user, $order,
								(array)$response,
								$extra,
								$custom1,
								$custom2,
								$custom3,
								$security_id
							);

							$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

							return true;
						}
						else
						{
							$this->is_error = true;
							$this->error_message = $response->exact_message;

							$this->storeTransaction($db, $userTmp, $orderTmp, $response, '', false);

							return false;
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $buffer;

						$this->log('capture Response:'.$buffer);

						$this->storeTransaction($db, $user, $order, $buffer, '', false);

						return false;
					}
				}
				else
				{
					$this->is_error = true;

					return false;
				}
			}
		}
		return !$this->is_error;
	}

	function supportsVoid($order_data = false)
	{
		return $order_data && ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Pending');
	}

	function void($order_data=false)
	{
		/** @var DB $db */
		/** @var ORDER $order */
		global $db, $order;

		$oldPaymentStatus = $order->getPaymentStatus();
		$oldOrderStatus = $order->getStatus();

		$user = $order->getUser();

		$this->is_error = false;

		if ($order_data)
		{
			$total_amount = $order_data['total_amount'];
			$transaction_id = $this->getLastTransactionId($order->getId());
			$authorization_num = $order_data['security_id'];

			$post_data = array(
				'gateway_id' => $this->settings_vars[$this->id.'_GatewayId']['value'],
				'password' => $this->settings_vars[$this->id.'_Password']['value'],
				'transaction_type' => '33',
				'amount' => number_format($total_amount, 2, '.', ''),
				'transaction_tag' => $transaction_id,
				'authorization_num' => $authorization_num,
			);

			$this->log('Request: ', $post_data);

			$buffer = $this->process_curl($post_data);

			if ($buffer)
			{
				$response = @json_decode($buffer);

				if ($response)
				{
					$this->log('void Response:', (array)$response);

					if ($response->transaction_approved)
					{
						$extra = $response->transaction_tag;
						$security_id = $response->authorization_num;
						$custom1 = '';
						$custom2 = '';
						$custom3 = '';

						$db->reset();
						$db->assignStr('custom1', $custom1);
						$db->assignStr('custom2', $custom2);
						$db->assignStr('custom3', $custom3);

						$order->setPaymentStatus(ORDER::PAYMENT_STATUS_CANCELED);
						$order->setStatus(ORDER::STATUS_CANCELED);

						$db->assignStr('payment_status', $order->getPaymentStatus());
						$db->assignStr('status', $order->getStatus());
						$db->update(DB_PREFIX.'orders', "WHERE oid='".$order_data['oid']."'");

						$custom1 = 'CANCELED';
						$order_data['custom1'] = $custom1;
						$order_data['custom2'] = $custom2;
						$order_data['custom3'] = $custom3;
						$order_data['payment_status'] = $order->getPaymentStatus();
						$order_data['status'] = $order->getStatus();

						$this->createTransaction($db, $user, $order,
							(array)$response,
							$extra,
							$custom1,
							$custom2,
							$custom3,
							$security_id
						);

						$this->fireOrderEvent($order, $oldOrderStatus, $oldPaymentStatus);

						return true;
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $response->exact_message;

						$this->storeTransaction($db, $user, $order, (array)$response, '', false);

						return false;
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $buffer;

					$this->log('void Response:'.$buffer);

					$this->storeTransaction($db, $user, $order, $buffer, '', false);

					return false;
				}
			}
			else
			{
				$this->is_error = true;

				return false;
			}
		}

		return !$this->is_error;
	}

	function supportsRefund($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Partial')
			{
				return true;
			}
			else if ($order_data['payment_status'] == 'Refunded')
			{
				return $this->_refundableAmount($order_data) > 0.0;
			}
		}
		return false;
	}

	public function supportsPartialRefund($order_data = false)
	{
		return true;
	}

	function refundableAmount($order_data=false)
	{
		if ($order_data)
		{
			if ($this->supportsRefund($order_data))
			{
				return $this->_refundableAmount($order_data);
			}
		}
		return 0;
	}

	function _refundableAmount($order_data)
	{
		$refundableAmount = $this->getTotalCaptured($order_data);
		return number_format($refundableAmount, 2, '.', '');
	}

	function refund($order_data=false,$refund_amount=false)
	{
		global $db, $order;

		$user = $order->getUser();

		$oldPaymentStatus = $order->getPaymentStatus();

		$this->is_error = false;

		if ($order_data)
		{
			if (!$this->supportsRefund($order_data))
			{
				$this->is_error = true;
				$this->error_message = 'Order cannot be refunded';
				return false;
			}

			//get captured total amount & x_trans_id
			$refund_total_amount = $refund_amount;

			if (!$refund_total_amount)
			{
				$refund_total_amount = $order_data['custom3'];
			}

			if ($refund_total_amount < 0)
			{
				$this->is_error = true;
				$this->error_message = 'Amount to refund must be greater than 0';
				return false;
			}
			else if ($refund_total_amount > $order_data['custom3'] || $refund_total_amount > $order_data['total_amount'])
			{
				$this->is_error = true;
				$this->error_message = 'Amount to refund cannot be more than total captured amount';
				return false;
			}

			$transaction_id = $this->getLastTransactionId($order->getId());
			$authorization_num = $order_data['security_id'];

			if ($transaction_id == '')
			{
				$this->is_error = true;
				$this->error_message = 'This order does not have a valid transaction id';
				return false;
			}
			else
			{
				$post_data = array(
					'gateway_id' => $this->settings_vars[$this->id.'_GatewayId']['value'],
					'password' => $this->settings_vars[$this->id.'_Password']['value'],
					'transaction_type' => '34',
					'amount' => number_format($refund_total_amount, 2, '.', ''),
					'transaction_tag' => $transaction_id,
					'authorization_num' => $authorization_num,
				);

				$this->log('Refund Request: ', $post_data);

				$buffer = $this->process_curl($post_data);

				if ($buffer)
				{
					$response = @json_decode($buffer);

					if ($response)
					{
						$this->log('Refund Response:', (array)$response);

						$user = $order->getUser();

						if ($response->transaction_approved)
						{
							$totalCaptured = $this->getTotalCaptured($order_data) - $refund_total_amount;
							$custom3 = number_format($totalCaptured, 2, '.', '');

							$db->reset();
							$db->assignStr('custom1', '');
							$db->assignStr('custom2', '');
							$db->assignStr('custom3', $custom3);

							if ($order_data['total_amount'] > $totalCaptured)
							{
								$extra = $transaction_id;
								$security_id = $authorization_num;
							}
							else
							{
								$extra = $response->transaction_tag;
								$security_id = $response->authorization_num;
							}

							$order->setPaymentStatus(ORDER::PAYMENT_STATUS_REFUNDED);

							$db->assignStr('payment_status', $order->getPaymentStatus());
							$db->update(DB_PREFIX.'orders', "WHERE oid='".$order_data['oid']."'");

							$order_data['custom1'] = 'REFUND';
							$order_data['custom2'] = '';
							$order_data['custom3'] = $custom3;
							$order_data['payment_status'] = $order->getPaymentStatus();

							$custom1 = $order_data['custom1'];
							$custom2 = $order_data['custom2'];

							$this->createTransaction($db, $user, $order,
								(array)$response,
								$extra,
								$custom1,
								$custom2,
								$custom3,
								$security_id
							);

							$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

							return true;
						}
						else
						{
							$this->is_error = true;
							$this->error_message = $response->exact_message;

							$this->storeTransaction($db, $user, $order, (array)$response, '', false);

							return false;
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $buffer;

						$this->log('refund Response:'.$buffer);

						$this->storeTransaction($db, $user, $order, $buffer, '', false);

						return false;
					}
				}
				else
				{
					$this->is_error = true;

					return false;
				}
			}
		}

		return !$this->is_error;
	}

	public function getTransactionDetails(ORDER $order)
	{
		// ctr response value must be mapped to
		global $db;

		$row = $db->selectOne('
			SELECT *
			FROM '.DB_PREFIX.'payment_transactions
			WHERE oid = '.$order->oid.' AND is_success = 1
			ORDER BY completed DESC
			LIMIT 1
		');
		$ret = false;

		if ($row)
		{
			$ret = array();

			$response_values = explode("\n", $row['payment_response']);
			foreach ($response_values as  $response_value)
			{
				$kvPair = explode(' = ', $response_value);

				if ($kvPair[0] == 'authorization_num')
				{
					$ret['authorization_code'] = urldecode($kvPair[1]);
				}
				else if ($kvPair[0] == 'credit_card_type')
				{
					$ret['card_type'] = urldecode($kvPair[1]);
				}
				else if ($kvPair[0] == 'cc_number')
				{
					// TODO: This value is fully masked on the response
					//$ret['card_number'] = urldecode($kvPair[1]);
				}
				else if ($kvPair[0] == 'ctr')
				{
					$ret['notes'] = urldecode($kvPair[1]);
				}
			}
		}

		return $ret;
	}

	public function supportsPaymentProfiles()
	{
		global $settings;

		return $settings['firstdata_Payment_Profiles'] == 'Yes';
	}

	/**
	 * Generate payment profile token
	 *
	 * @param $post_form
	 * @return array|bool|mixed
	 */
	protected function generateToken($post_form)
	{
		global $settings;

		$post_data = array(
			'gateway_id' => $settings['firstdata_GatewayId'],
			'password' => $settings['firstdata_Password'],
			'transaction_type' => '05',
			'amount' => 0.00,
			'cc_number' => $post_form['cc_number'],
			'cc_expiry' => isset($post_form['cc_expiry']) ? $post_form['cc_expiry'] : ($post_form['cc_expiration_month'].substr($post_form['cc_expiration_year'], -2)),
			'cardholder_name' => $post_form['first_name'].' '.$post_form['last_name'],
			'zip_code' => $post_form['zip'],
			'client_ip' => $_SERVER['REMOTE_ADDR']
		);

		$this->log('Token Request: ', $post_data);
		$result = $this->process_curl($post_data);

		if ($result)
		{
			$response = @json_decode($result);

			if ($response && is_object($response))
			{
				$this->log('Token Response:', (array)$response);

				if ($response->transaction_approved)
				{
					if (isset($response->transarmor_token) && trim($response->transarmor_token) != '')
					{
						return $response;
					}
					else
					{
						$this->is_error = true;
						$this->error_message = 'There was an error storing your credit card.';
						$this->log('transarmor token no returned. Merchant account is possibly not configured with transarmor.');
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $response->bank_message;
				}
			}
			else
			{
				$this->log('Token Response: '.$result);
				$this->is_error = true;
				$this->error_message = $result;
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = 'There was an error storing your credit card.';
		}

		return false;
	}

	/**
	 * @param DB $db
	 * @param mixed $ext_profile_id
	 * @param bool $active
	 * @return mixed
	 */
	function getPaymentProfileDetails($db, $ext_profile_id, $active = false)
	{
		$db->selectOne("SELECT * FROM ".DB_PREFIX."users_payment_profiles WHERE ext_profile_id = '".$db->escape($ext_profile_id)."'");

		$data = unserialize($db->col['billing_data']);
		$data['usid'] = $db->col['usid'];

		$country = getCountryData($db, false, $data['country']);
		$data['country_id'] = $country['coid'];

		$state = getStateData($db, false, $data['state']);
		$data['state_id'] = $state['stid'];

		$data['name'] = $data['first_name'].' '.$data['last_name'];


		return $data;
	}

	/**
	 * Add a new payment profile
	 *
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool|void
	 */
	public function addPaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		/** @var Model_Address $billingAddress */
		$billingAddress = $billingData->getAddress();

		$tokenRequest = array(
			'first_name' => $billingAddress->getFirstName(),
			'last_name' => $billingAddress->getLastName(),
			'cc_number' => $billingData->getCcNumber(),
			'cc_expiry' => $billingData->getCcExpirationMonth().substr($billingData->getCcExpirationYear(), -2),
			'cardholder_name' => $billingAddress->getFirstName().' '.$billingAddress->getLastName(),
			'zip' => $billingAddress->getZip()
		);

		$tokenResponse = $this->generateToken($tokenRequest);

		// TODO: add more advanced error handling

		if ($tokenResponse && !$this->is_error)
		{
			$paymentProfile->setExternalProfileId($tokenResponse->transarmor_token);
			$paymentProfile->getBillingData()->setCcType($tokenResponse->credit_card_type);

			return true;
		}

		return false;
	}

	/**
	 * Update payment profile
	 *
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool
	 */
	public function updatePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		/** @var Model_Address $billingAddress */
		$billingAddress = $billingData->getAddress();

		$tokenRequest = array(
			'first_name' => $billingAddress->getFirstName(),
			'last_name' => $billingAddress->getLastName(),
			'cc_number' => $billingData->getCcNumber(),
			'cc_expiry' => $billingData->getCcExpirationMonth().substr($billingData->getCcExpirationYear(), -2),
			'cardholder_name' => $billingAddress->getFirstName().' '.$billingAddress->getLastName(),
			'zip' => $billingAddress->getZip()
		);

		$tokenResponse = $this->generateToken($tokenRequest);

		// TODO: add more advanced error handling

		if ($tokenResponse && !$this->is_error)
		{
			$paymentProfile->setExternalProfileId($tokenResponse->transarmor_token);
			$billingData->getCcType($tokenResponse->credit_card_type);

			return true;
		}

		return false;
	}

	/**
	 * Remove payment profile
	 *
	 * @param $db
	 * @param $user
	 * @param $paymentProfile
	 *
	 * @return bool|void
	 */
	public function deletePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function supportsRecurringBillingProfiles()
	{
		global $settings;

		return $settings['firstdata_Payment_Profiles'] == 'Yes';
	}

	/**
	 * {@inheritdoc}
	 */
	public function useSchedulerForRecurringBilling()
	{
		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function updateRecurringBillingProfile(RecurringBilling_Model_RecurringProfile $profile, array $post_form)
	{
		global $registry;

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $repository */
		$repository = $registry->get('repository_payment_profile');

		$paymentProfile = $repository->getById($profile->getPaymentProfileId());

		if (is_null($paymentProfile))
		{
			return false;
		}

		$billingData = $paymentProfile->getBillingData();

		$post_form = array_merge($billingData, $post_form);

		$tokenResponse = $this->generateToken($post_form);

		if(!$tokenResponse)
		{
			$errors = array();
			$errors[] = 'There was an error updating your credit card.';

			view()->assign('user_errors', $errors);

			return false;
		}

		$token = $tokenResponse->transarmor_token;

		$billingData['cc_number'] = str_repeat('X', strlen($post_form['cc_number']) - 4).substr($post_form['cc_number'], -4);
		$billingData['cc_expiration_date'] = $post_form['cc_expiration_month'].'/'.$post_form['cc_expiration_year'];

		$paymentProfile->setExternalProfileId($token);
		$isPrimary = false;
		$paymentProfile->setIsPrimary($isPrimary);
		$paymentProfile->setBillingData($billingData);

		$repository->persist($paymentProfile);

		$cc_num = str_repeat('X', strlen($post_form['cc_number']) - 4);
		$cc_num .= substr($post_form['cc_number'], -4);

		$profile->setCardType($this->getCcType($post_form['cc_number']));
		$profile->setCardNumberMasked($cc_num);
		$profile->setDateCardExpires($post_form['cc_expiration_year'].'-'.$post_form['cc_expiration_month'].'-01');

		return true;
	}

	/**
	 * {@inheritdoc}
	 */
	public function processRecurringBillingPayment(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order)
	{
		global $db;

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->getPaymentProfileRepository();

		/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
		$paymentProfile = $paymentProfileRepository->getById($recurringProfile->getPaymentProfileId());

		if (is_null($paymentProfile)) return 'fatal';

		/** @var PaymentProfiles_Model_BillingData $paymentProfileData */
		$billingData = $paymentProfile->getBillingData();

		/** @var Model_Address $billingAddress */
		$billingAddress = $billingData->getAddress();

		$userDetails = $order->getUser()->getUserData();
		$transactionData = array(
			'amount' => $order->getTotalAmount(),
			'billing_address1' => $billingAddress->getAddressLine1(),
			'billing_zip' => $billingAddress->getZip(),
			'billing_city' => $billingAddress->getCity(),
			'billing_state_abbr' => $billingAddress->getStateCode(),
			'billing_country_iso_a3' => $billingAddress->getCountryIso3(),
			'tax_amount' => $order->getTaxAmount(),
			'client_email' => $userDetails['email'],
			'ecommerce_flag' => 7,
		);

		//$expiresDate = strtotime($profile->getDateCardExpires());
		$transactionData['cardholder_name'] = $billingAddress->getName();
		$transactionData['transarmor_token'] = $paymentProfile->getExternalProfileId();
		$transactionData['cc_expiration_month'] = $paymentProfile->getBillingData()->getCcExpirationMonth();
		$transactionData['cc_expiration_year'] = $paymentProfile->getBillingData()->getCcExpirationYear();
		$transactionData['credit_card_type'] = $paymentProfile->getBillingData()->getCcType();

		$ret = $this->processPayment($transactionData, $db, $order, true);

		if ($ret !== false)
		{
			$order->setPaymentStatus($ret === true ? 'Received' : $ret);

			return 'success';
		}
		else
		{
			return $this->is_fatal ? 'fatal' : 'error';
		}
	}

	/**
	 * Returns current order payment status
	 *
	 * @param bool $order_data
	 * @return type
	 */
	public function currentOrderPaymentStatus($order_data = false)
	{
		return $order_data['custom1'];
	}
}
<?php

$payment_processor_id = 'braintree';
$payment_processor_name = 'Braintree Payments';
$payment_processor_class = 'PAYMENT_BRAINTREE';
$payment_processor_type = 'cc';

class PAYMENT_BRAINTREE extends PAYMENT_PROCESSOR
{
	const BT_ENVIRONMENT_SANDBOX = 'sandbox';
	const BT_ENVIRONMENT_PRODUCTION = 'production';

	const BT_TRANSACTION_SETTLEMENT_SALE = 'sale';
	const BT_TRANSACTION_SETTLEMENT_AUTH = 'auth';

	const TRANSACTION_BN_CODE = 'PinnacleCart_BT';

	const CT_CHECKOUT_TYPE_PAYPAL = 'PayPalAccount';
	const CT_CHECKOUT_TYPE_FORM = 'customform';

	const BT_SETTLEMENT_APPROVAL = '4000';
	const BT_SETTLEMENT_PENDING = '4002';

	const ERROR_COMMON = 'Braintree API error. Please contact site administrator';

	protected $braintreeSDK = null;
	protected $_settings    = array();

	protected $merchant_id    = '';
	protected $bt_environment = '';
	protected $public_key     = '';
	protected $private_key    = '';
	protected $clientToken    = '';

	protected $oauth_accessToken = '';

	/** @var Braintree\Gateway $oauth_gateway */
	protected $oauth_gateway = null;

	protected $transaction_type = '';
	protected $storeInVault = 'No';
	protected $recurringPlanId = '';
	protected $transact_data = array();

	protected $transact_data_default = array(
		'amount' => 0,
		'channel' => self::TRANSACTION_BN_CODE,
		'orderId' => '',
		'paymentMethodNonce' => '',
		'paymentMethodToken' => '',
		'submitForSettlement' => '',
		'storeInVault' => ''
	);

	protected $cc_data = array();

	protected $cc_data_default = array(
		'customerId' => '',
		'cardholderName' => '',
		'expirationMonth' => '',
		'expirationYear' => '',
		'number' => ''
	);

	protected $billing_data = array();

	protected $billing_data_default = array(
		'company' => '',
		'countryCodeAlpha2' => '',
		'countryCodeAlpha3' => '',
		'countryCodeNumeric' => '',
		'countryName' => '',
		'streetAddress' => '',
		'extendedAddress' => '',
		'firstName' => '',
		'lastName' => '',
		'locality' => '',
		'postalCode' => '',
		'region' => ''
	);

	protected $customer_data_default = array(
		'company' => '',
		'email' => '',
		'fax' => '',
		'firstName' => '',
		'id' => '',
		'lastName' => '',
		'phone' => '',
		'website' => '',
		'paymentMethodNonce' => ''
	);

	protected $customer_data = array();

	protected $shipping_data = array();

	protected $shipping_data_default = array(
		'company' => '',
		'countryCodeAlpha2' => '',
		'countryCodeAlpha3' => '',
		'countryCodeNumeric' => '',
		'countryName' => '',
		'streetAddress' => '',
		'extendedAddress' => '',
		'firstName' => '',
		'lastName' => '',
		'locality' => '',
		'postalCode' => '',
		'region' => ''
	);

	/**
	 * PAYMENT_BRAINTREE constructor.
	 */
	function PAYMENT_BRAINTREE()
	{
		global $settings;

		parent::PAYMENT_PROCESSOR();
		$this->id = 'braintree';
		$this->name = 'Braintree Payments';
		$this->class_name = 'PAYMENT_BRAINTREE';
		$this->type = 'cc';
		$this->description = '';
		$this->steps = 1;
		$this->testMode = false;
		$this->support_ccs = true;
		$this->need_cc_codes = true;
		$this->allow_change_gateway_url = false;
		$this->formtemplate = 'templates/pages/checkout/opc/braintree_payform.html';
		$this->_settings = $settings;
		$this->merchant_id = $settings['braintree_merchantId'];
		$this->public_key = $settings['braintree_publicKey'];
		$this->private_key = $settings['braintree_privateKey'];
		$this->bt_environment = $settings['braintree_environment'];
		$this->transaction_type = $settings['braintree_transaction_type'];
		$this->storeInVault = 'Yes';
		$this->oauth_accessToken = $settings['braintree_oauth_accessToken'];
		$this->supportsRecurringBilling = true;

		try
		{
			$this->setupBraintreeSDK($this->_settings);
		}
		catch (Braintree\Exception\Authorization $e)
		{
			throw new Exception('API key is not authorized to perform the attempted action');
		}
		catch (Braintree\Exception\Configuration $e)
		{
			throw new Exception("Braintree library isn't configured");
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			throw new Exception('Braintree request timed out');
		}
		catch (Braintree\Exception\ServerError $e)
		{
			throw new Exception('Braintree Server Error');
		}
		catch (Exception $e)
		{
			throw new Exception($this->error_message);
		}

		return $this;
	}

	/**
	 * @param $settings
	 * @return bool
	 */
	private function setupBraintreeSDK($settings)
	{
		try
		{
			require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/content/vendors/braintree/autoload.php';

			if ($this->oauth_accessToken)
			{
				$this->oauth_gateway = new Braintree\Gateway(array('accessToken' => $this->oauth_accessToken));

				$this->clientToken = $this->oauth_gateway->clientToken()->generate();
			}
			else
			{
				Braintree\Configuration::environment($this->bt_environment);
				Braintree\Configuration::merchantId($this->merchant_id);
				Braintree\Configuration::publicKey($this->public_key);
				Braintree\Configuration::privateKey($this->private_key);
				$this->clientToken = Braintree\ClientToken::generate();
			}

			return ($this->clientToken) ? true : false;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e, 'API key is not authorized to perform the attempted action');

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", "Braintree library isn't configured");
			$this->setExceptionError($e, "Braintree library isn't configured");

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}
	}

	/**
	 * @param Exception $e
	 * @param string $msg
	 */
	protected function setExceptionError(Exception $e, $msg = '')
	{
		$this->is_error = true;
		$this->error_message = (trim($msg) == '') ? $e->getMessage() : $msg;
	}

	/**
	 * @return bool
	 */
	function isTestMode()
	{
		$this->testMode = false;
		if ($this->bt_environment == self::BT_ENVIRONMENT_SANDBOX)
		{
			$this->testMode = true;
		}

		return $this->testMode;
	}

	/**
	 * @param DB $db
	 * @return array
	 */
	function getPaymentForm($db)
	{
		global $settings;

		$fields = parent::getPaymentForm($db);
		$fields['postal_code'] = array('type' => 'input', 'name' => 'postal_code', 'value' => '');

		$saveCreditCardAdded = false;

		/** @var ORDER $order */
		$order = isset($this->order) ? $this->order : null;
		if ($order && $order instanceof ORDER && $order->hasRecurringBillingItems())
		{
			$fields['save_credit_card'] = array(
				'type' => 'static',
				'caption' => 'This card will be securely stored for future recurring payments',
				'wrapperClass' => 'payment-form-cc-will-be-stored'
			);

			$saveCreditCardAdded = true;
		}

		if (!$saveCreditCardAdded && $this->supportsPaymentProfiles() && isset($this->user) && !is_null($this->user) && isset($this->user->auth_ok) && $this->user->auth_ok && (!isset($this->user->express) || !$this->user->express))
		{
			$fields['save_credit_card'] = array(
				'type' => 'checkbox',
				'name' => 'form[save_credit_card]',
				'value' => '1',
				'caption' => 'Save this card for later use'
			);
		}

		return $fields;
	}

	/**
	 * @param bool $param
	 * @return array
	 */
	public function getTemplateData($param = false)
	{
		$auth_result = array();
		$auth_result['merchant_id'] = $this->merchant_id;
		$auth_result['public_key'] = $this->public_key;
		$auth_result['client_token'] = $this->clientToken;
		$auth_result['tarnsaction_amount'] = $this->common_vars['order_total_amount']['value'];
		$auth_result['storeInVault'] = $this->storeInVault;
		$auth_result['accessToken'] = $this->oauth_accessToken;
		$auth_result['sandbox'] = $this->testMode;

		return $auth_result;
	}

	/**
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 * @param ORDER $order
	 * @return string
	 */
	private function processPaymentProfileProcess(PaymentProfiles_Model_PaymentProfile $paymentProfile, ORDER $order)
	{
		if (is_null($paymentProfile))
		{
			return 'fatal';
		}

		if (is_null($order))
		{
			return 'fatal';
		}
		global $db;

		$user = $order->getUser();

		$idParts = explode('||', $paymentProfile->getExternalProfileId());

		if (count($idParts) != 2)
		{
			$this->is_error = true;
			$this->error_message = 'Invalid payment profile';

			return false;
		}
		else
		{
			$customerProfileId = $idParts[0];
			$cc_token = $idParts[1];
		}

		$params_arr = array_merge(
			$this->transact_data_default,
			array(
				'amount' => $order->getTotalAmount(),
				'orderId' => $order->getOrderNumber(),
				'paymentMethodToken' => $cc_token,
				'submitForSettlement' => ($this->transaction_type == self::BT_TRANSACTION_SETTLEMENT_SALE ? true : false),
				'storeInVault' => false
			)
		);

		$this->setTransactionParams($params_arr);

		$params_arr = array_merge(
			$this->customer_data_default,
			array(
				'firstName' => $user->fname,
				'lastName'  => $user->lname,
				'id' => $customerProfileId
			)
		);

		$this->setCustomerData($params_arr);

		$trans_data = $this->getTransactData();
		$customer_data = $this->getCustomerData();
		$billing_data = $this->getBillingData();
		$shipping_data = $this->getShippingData();
		$cc_data = $this->getCCData();

		/** @var Braintree\Result\Successful $result */
		$result = $this->doSale($trans_data, $customer_data, $billing_data, $shipping_data);
		$this->log('Sale Transaction response:', $result);

		if ($result->success)
		{
			/** @var Braintree\Transaction $transaction */
			$transaction = $result->transaction;
			$security_id = $transaction->id;

			$custom1 = ($trans_data['submitForSettlement'] == self::BT_TRANSACTION_SETTLEMENT_SALE) ? self::BT_TRANSACTION_SETTLEMENT_SALE : self::BT_TRANSACTION_SETTLEMENT_AUTH;
			$custom2 = '';
			$custom3 = '';
			if ($trans_data['submitForSettlement'] == self::BT_TRANSACTION_SETTLEMENT_SALE)
			{
				$custom3 = number_format($trans_data['amount'], 2, '.', '');
			}

			$_response = var_export($result, true);
			$this->createTransaction($db, $user, $order, $_response,
				$security_id, $custom1, $custom2, $custom3, $security_id);
			$this->is_error = false;
			$this->error_message = '';
			$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);

			return 'success';
		}
		else
		{
			/** @var Braintree\Result\Error $result */
			$this->is_error = true;
			if ($result)
			{
				foreach ($result->errors->deepAll() AS $error)
				{
					$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
				}
				$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: " . "\n\n" . $this->error_message;
			}
			else
			{
				$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: " . "\n\n" . $this->error_message;
			}

			return 'error';
		}
	}

	/**
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param ORDER $order
	 * @return string
	 */
	public function processRecurringBillingPayment(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order)
	{
		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->getPaymentProfileRepository();

		/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
		$paymentProfile = $paymentProfileRepository->getById($recurringProfile->getPaymentProfileId());

		return $this->processPaymentProfileProcess($paymentProfile, $order);
	}

	/**
	 * @param $db
	 * @param $user
	 * @param $order
	 * @param $post_form
	 * @return bool|string
	 */
	protected function _procss($db, $user, $order, $post_form)
	{
		if (isset($post_form) && is_array($post_form))
		{

			$params_arr = array_merge($this->transact_data_default, array('amount'              => number_format($this->common_vars['order_total_amount']['value'], 2, '.', ''),
																		  'orderId'             => $this->common_vars['order_id']['value'],
																		  'paymentMethodNonce'  => $_REQUEST['payment_method_nonce'],
																		  'submitForSettlement' => ($this->transaction_type == self::BT_TRANSACTION_SETTLEMENT_SALE ? true : false),
																		  'storeInVault'        => ($this->storeInVault == 'Yes' ? true : false)));
			$this->setTransactionParams($params_arr);

			$params_arr = array_merge($this->customer_data_default, array('firstName' => $post_form['cc_first_name'],
																		  'lastName'  => $post_form['cc_last_name'],
																		  'company'   => $this->common_vars['billing_company']['value'],
																		  'phone'     => $this->common_vars['billing_phone']['value'],
																		  'email'     => $this->common_vars['billing_email']['value']));
			$this->setCustomerData($params_arr);
			$this->setBillingData($post_form, $_REQUEST);
			$shippingAddress = $this->extractShippingAddressData();
			$this->setShippingData($shippingAddress);

			$trans_data = $this->getTransactData();
			$customer_data = $this->getCustomerData();
			$billing_data = $this->getBillingData();
			$shipping_data = $this->getShippingData();

			/** @var Braintree\Result\Successful $result */
			$result = $this->doSale($trans_data, $customer_data, $billing_data, $shipping_data);
			$this->log('Sale Transaction response:', $result);
			if ($result->success)
			{
				/** @var Braintree\Transaction $transaction */
				$transaction = $result->transaction;
				$security_id = $transaction->id;

				if ($this->storeInVault == 'Yes')
				{
					if ($order->hasRecurringBillingItems() || isset($post_form['save_credit_card']))
					{
						/** @var Braintree\Transaction $transaction */
						$_paymentProfile = $this->createPaymentProfileFromTransaction($user, $transaction);
						if ($order->hasRecurringBillingItems())
						{
							$this->createRecurringProfile($order, $_paymentProfile);
						}
					}
				}

				$custom1 = ($trans_data['submitForSettlement'] == self::BT_TRANSACTION_SETTLEMENT_SALE) ? self::BT_TRANSACTION_SETTLEMENT_SALE : self::BT_TRANSACTION_SETTLEMENT_AUTH;
				$custom2 = '';
				$custom3 = '';
				if ($trans_data['submitForSettlement'] == self::BT_TRANSACTION_SETTLEMENT_SALE)
				{
					$custom3 = number_format($trans_data['amount'], 2, '.', '');
				}

				$_response = var_export($result, true);
				$this->createTransaction($db, $user, $order, $_response, $security_id, $custom1, $custom2, $custom3, $security_id);
				$this->is_error = false;
				$this->error_message = '';
			}
			else
			{
				/** @var Braintree\Result\Error $result */
				$this->is_error = true;
				if ($result)
				{
					foreach ($result->errors->deepAll() AS $error)
					{
						$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
					}
				}
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = 'Incorrect form params. Please contact site administartor.';
		}
		if (!$this->is_error)
		{
			$transaction_status = ($this->transaction_type == self::BT_TRANSACTION_SETTLEMENT_AUTH ? ORDER::PAYMENT_STATUS_PENDING : true);
		}
		else
		{
			$transaction_status = false;
		}

		return $transaction_status;
	}

	function process($db, $user, $order, $post_form)
	{
		if (isset($post_form['ext_profile_id']))
		{
			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $this->getPaymentProfileRepository();

			/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
			$paymentProfile = $paymentProfileRepository->getByExtProfileId($post_form['ext_profile_id']);

			$profileprocessresult = $this->processPaymentProfileProcess($paymentProfile, $order);
			$transaction_status = false;
			if ($profileprocessresult == 'success')
			{
				if ($order->hasRecurringBillingItems())
				{
					/** @var Braintree\Transaction $transaction */
					$this->createRecurringProfile($order, $paymentProfile);
				}
			}
			if (!$this->is_error)
			{
				$transaction_status = ($this->transaction_type == self::BT_TRANSACTION_SETTLEMENT_AUTH ? ORDER::PAYMENT_STATUS_PENDING : true);
			}
			else
			{
				$transaction_status = false;
			}

			return $transaction_status;
		}
		else
		{
			return $this->_procss($db, $user, $order, $post_form);
		}
	}

	private function setTransactionParams($form)
	{
		$this->transact_data = array_merge($this->transact_data_default, $form);

		return $this->transact_data;
	}

	private function setCreditCardParams($form)
	{
		$this->cc_data = array_merge($this->cc_data_default, $form);

		return $this->cc_data;
	}

	private function setCustomerData($form)
	{
		if (!isset($form['company']))
		{
			$form['company'] = $this->common_vars['billing_company']['value'];
		}

		if (!isset($form['phone']))
		{
			$form['phone'] = $this->common_vars['billing_phone']['value'];
		}

		if (!isset($form['email']))
		{
			$form['email'] = $this->common_vars['billing_email']['value'];
		}
		$this->customer_data = array_merge($this->customer_data_default, array('firstName' => $form['firstName'],
																			   'lastName'  => $form['lastName'],
																			   'company'   => $form['company'],
																			   'phone'     => $form['phone'],
																			   'email'     => $form['email']));

		return $this->customer_data;
	}

	private function setBillingData($form, $request)
	{
		$this->billing_data = array_merge($this->billing_data_default, array('firstName'         => $form['cc_first_name'],
																			 'lastName'          => $form['cc_last_name'],
																			 'company'           => $this->common_vars['billing_company']['value'],
																			 'streetAddress'     => $this->common_vars['billing_address']['value'],
																			 'extendedAddress'   => '',
																			 'locality'          => $this->common_vars['billing_city']['value'],
																			 'region'            => $this->common_vars['billing_state_abbr']['value'],
																			 'postalCode'        => $this->common_vars['billing_zip']['value'],
																			 'countryCodeAlpha2' => $this->common_vars['billing_country_iso_a2']['value'],
																			 'countryCodeAlpha3' => $this->common_vars['billing_country_iso_a3']['value'],));

		return $this->billing_data;
	}

	protected function extractShippingAddressData()
	{
		$result = array();
		$shipping_name_parts = explode(' ', trim($this->common_vars['shipping_name']['value']));
		$shipping_name_first = trim($shipping_name_parts[0]);
		unset($shipping_name_parts[0]);
		$shipping_name_last = trim(implode(' ', $shipping_name_parts));

		$result = array_merge($this->shipping_data_default, array('firstName'         => $shipping_name_first,
																  'lastName'          => $shipping_name_last,
																  'streetAddress'     => $this->common_vars['shipping_address']['value'],
																  'locality'          => $this->common_vars['shipping_city']['value'],
																  'region'            => $this->common_vars['shipping_state_abbr']['value'],
																  'postalCode'        => $this->common_vars['shipping_zip']['value'],
																  'countryCodeAlpha2' => $this->common_vars['shipping_country_iso_a2']['value'],
																  'countryCodeAlpha3' => $this->common_vars['shipping_country_iso_a3']['value'],
																  'company'           => $this->common_vars['shipping_company']['value']));
	}

	private function setShippingData($form)
	{

		$this->shipping_data = array_merge($this->shipping_data_default, array('firstName'         => $form['firstName'],
																			   'lastName'          => $form['lastName'],
																			   'streetAddress'     => $form['streetAddress'],
																			   'locality'          => $form['locality'],
																			   'region'            => $form['region'],
																			   'postalCode'        => $form['postalCode'],
																			   'countryCodeAlpha2' => $form['countryCodeAlpha2'],
																			   'countryCodeAlpha3' => $form['countryCodeAlpha3'],
																			   'company'           => $form['company']));

		return $this->shipping_data;
	}

	/**
	 * @return array
	 */
	public function getTransactData()
	{
		return $this->transact_data;
	}

	/**
	 * @return array
	 */
	public function getCustomerData()
	{
		return $this->customer_data;
	}

	/**
	 * @return array
	 */
	public function getBillingData()
	{
		return $this->billing_data;
	}

	/**
	 * @return array
	 */
	public function getShippingData()
	{
		return $this->shipping_data;
	}

	public function getCCData()
	{
		return $this->cc_data;
	}

	private function doSale($transact_params, $customer_data, $billing_data, $shipping_data)
	{

		$this->log("Sale Transaction params: common: \n", $transact_params);
		$this->log("\n customer: \n", $customer_data);
		$this->log("\n billing: \n", $billing_data);
		$this->log("\n shipping: \n", $shipping_data);
		$this->log("\n", array());
		try
		{
			if ((trim($customer_data['id']) == '') && (trim($transact_params['paymentMethodToken']) == ''))
			{
				$t_params = array(
					'amount' => $transact_params['amount'],
					'orderId' => $transact_params['orderId'],
					'channel' => $transact_params['channel'],
					'paymentMethodNonce' => $transact_params['paymentMethodNonce'],
					'customer' => array(
						'firstName' => $customer_data['firstName'],
						'lastName' => $customer_data['lastName'],
						'company' => $customer_data['company'],
						'phone' => $customer_data['phone'],
						'fax' => $customer_data['fax'],
						'website' => $customer_data['website'],
						'email' => $customer_data['email']
					),
					'billing' => array(
						'firstName' => $billing_data['firstName'],
						'lastName' => $billing_data['lastName'],
						'company' => $billing_data['company'],
						'streetAddress' => $billing_data['streetAddress'],
						'extendedAddress' => $billing_data['extendedAddress'],
						'locality' => $billing_data['locality'],
						'region' => $billing_data['region'],
						'postalCode' => $billing_data['postalCode'],
						'countryCodeAlpha2' => $billing_data['countryCodeAlpha2']
					),
					'shipping'=> array(
						'firstName' => $shipping_data['firstName'],
						'lastName' => $shipping_data['lastName'],
						'company' => $shipping_data['company'],
						'streetAddress' => $shipping_data['streetAddress'],
						'extendedAddress' => $shipping_data['extendedAddress'],
						'locality' => $shipping_data['locality'],
						'region' => $shipping_data['region'],
						'postalCode' => $shipping_data['postalCode'],
						'countryCodeAlpha2' => 'US'
					),
					'options' => array(
						'submitForSettlement'=> $transact_params['submitForSettlement'],
						'storeInVaultOnSuccess' => $transact_params['storeInVault']
					)
				);
			}
			else
			{
				/* transaction use params stored in Vault */
				$t_params = array(
					'amount' => $transact_params['amount'],
					'orderId' => $transact_params['orderId'],
					'channel' => $transact_params['channel'],
					'paymentMethodToken' => $transact_params['paymentMethodToken'],
					'customer' => array('id' => $customer_data['id']),
					'options' => array(
						'submitForSettlement' => $transact_params['submitForSettlement'],
						'storeInVaultOnSuccess' => $transact_params['storeInVault']
					)
				);
			}

			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->transaction()->sale($t_params);
			}
			else
			{
				$result = Braintree\Transaction::sale($t_params);
			}
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e, 'API key is not authorized to perform the attempted action');

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", "Braintree library isn't configured");
			$this->setExceptionError($e, "Braintree library isn't configured");

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	/**
	 * @param USER $user
	 * @param $transaction
	 * @return bool|null|PaymentProfiles_Model_PaymentProfile
	 */
	protected function createPaymentProfileFromTransaction(USER $user, $transaction)
	{
		$result = false;
		$paymentProfile = new PaymentProfiles_Model_PaymentProfile();

		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = new PaymentProfiles_Model_BillingData($user->getBillingInfo());

		/** @var Model_Address $address */
		$address = new Model_Address($user->getBillingInfo());

		/** @var Braintree\Transaction $transaction */
		$address->setAddressLine1($transaction->billingDetails->streetAddress);
		$address->setFirstName($transaction->billingDetails->firstName);
		$address->setLastName($transaction->billingDetails->lastName);

		if ($transaction->paymentInstrumentType !== Braintree\PaymentInstrumentType::PAYPAL_ACCOUNT)
		{
			$billingData->setCcNumber($transaction->creditCardDetails->maskedNumber);
			$billingData->setCcExpirationMonth($transaction->creditCardDetails->expirationMonth);
			$billingData->setCcExpirationYear($transaction->creditCardDetails->expirationYear);
			$billingData->setCcExpirationDate($transaction->creditCardDetails->expirationMonth . '/' . $transaction->creditCardDetails->expirationYear);
			$billingData->setCcType($transaction->creditCardDetails->cardType);
		}
		else
		{
			$billingData->setCcNumber('XXXXXXXXXXXppal');
			$paymentProfile->setName(Braintree\PaymentInstrumentType::PAYPAL_ACCOUNT . '  : ' . $transaction->paypalDetails->payerEmail);
			$billingData->setCcExpirationMonth('-');
			$billingData->setCcExpirationYear('-');
			$billingData->setCcExpirationDate('-');
			$address->setAddressLine1('-');
			$address->setFirstName($transaction->paypal['payerFirstName']);
			$address->setLastName($transaction->paypal['payerLastName']);
			$billingData->setCcType(Braintree\PaymentInstrumentType::PAYPAL_ACCOUNT);
		}

		$billingData->setAddress($address);

		$billingData->setName(trim(trim($transaction->billingDetails->firstName) . ' ' . trim($transaction->billingDetails->lastName)));

		$paymentProfile->setBillingData($billingData);

		$paymentProfile->setUserId($user->getId());
		$paymentProfile->setPaymentMethodId($this->db_id);

		$extProfiletokens = $transaction->customerDetails->id . '||' . $transaction->creditCardDetails->token;
		$paymentProfile->setExternalProfileId($extProfiletokens);
		//global $db;
		//$paymentProfileRepository = new PaymentProfiles_DataAccess_PaymentProfileRepository($db);
		$result = $this->getPaymentProfileRepository()->persist($paymentProfile);
		if ($result)
		{
			return $result;
		}

		return null;
	}

	/**
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 * @return bool
	 */
	public function deletePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$idParts = explode('||', $paymentProfile->getExternalProfileId());
		if (count($idParts) != 2)
		{
			$this->is_error = true;
			$this->error_message = 'Invalid external profile id';

			return false;
		}

		$customerProfileId = $idParts[0];
		$cc_token = $idParts[1];

		/** @var Braintree\Result\Successful $result */
		$result = $this->doDeletePaymentMethod($cc_token);
		if ($result->success)
		{
			$result2 = $this->doDeleteCustomer($customerProfileId);

			if ($result2->success)
			{

				$this->is_error = false;
				$this->error_message = '';

				return true;
			}
			else
			{
				$this->is_error = true;
				if ($result)
				{
					foreach ($result->errors->deepAll() AS $error)
					{
						$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
					}
				}
				else
				{
					$this->error_message = $this::ERROR_COMMON;
				}

				return false;
			}
		}
		else
		{
			$this->is_error = true;
			if ($result)
			{
				foreach ($result->errors->deepAll() AS $error)
				{
					$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
				}
			}
			else
			{
				$this->error_message = 'Wrong payment profile id!';
			}

			return false;
		}
	}

	/**
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 * @return bool
	 */
	public function addPaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		/** @var Model_Address $billingAddress */
		$billingAddress = $billingData->getAddress();
		$additionalData = $paymentProfile->getAdditionalParams();

		if (!(isset($additionalData['payment_method_nonce']) && $additionalData['payment_method_nonce']))
		{
			return false;
		}
		else
		{
			$card_nonce = $additionalData['payment_method_nonce'];
		}

		$customer = array_merge($this->customer_data_default, array('firstName' => $billingAddress->getFirstName(),
																	'lastName'  => $billingAddress->getLastName(),
																	'company'   => $billingAddress->getCompany(),
																	'phone'     => $user->data['phone'],
																	'email'     => $user->data['email']));
		if ($card_nonce)
		{
			$customer['paymentMethodNonce'] = $card_nonce;
		}
		$this->setCustomerData($customer);

		$result = $this->doAddCustomer($customer);
		/** @var Braintree\Result\Successful $result */
		if ($result->success)
		{
			/** @var Braintree\Customer $_customer */
			$_customer = $result->customer;
			$customerProfileId = $_customer->id;
			$paymentProfileId = $_customer->paymentMethods[0]->token;

			if ($paymentProfileId)
			{
				/** @var Braintree\PaymentMethod $pMethodInfo */

				$pMethodInfo = $this->doFindPaymentMethodByToken($paymentProfileId);
				if (get_class($pMethodInfo) == 'Braintree\PayPalAccount')
				{
					$billingData->setCcNumber('XXXXXXXXXXXppal');
					$billingData->setCcExpirationMonth('-');
					$billingData->setCcExpirationYear('-');
					$billingData->setCcExpirationDate('-');
					$billingData->setCcType(Braintree\PaymentInstrumentType::PAYPAL_ACCOUNT);
					$paymentProfile->setBillingData($billingData);
				}
			}

			if ($customerProfileId && $paymentProfileId)
			{
				$this->is_error = false;
				$this->error_message = '';
				$paymentProfile->setExternalProfileId($customerProfileId . '||' . $paymentProfileId);

				return true;
			}
			else
			{
				$p_method = array_merge($this->cc_data_default, array('customerId'         => $customerProfileId,
																	  'paymentMethodNonce' => $card_nonce,
																	  'options'            => array('verifyCard' => true)));

				/** @var Braintree\Result\Successful $result2 */

				$result2 = false;

				if ($result2->success)
				{
					/** @var Braintree\CreditCard $_card */
					$_card = $result2->creditCard;
					$paymentProfileId = $_card->token;
					$this->is_error = false;
					$this->error_message = '';
					$paymentProfile->setExternalProfileId($customerProfileId . '||' . $paymentProfileId);

					return true;
				}
				else
				{
					/** @var Braintree\Result\CreditCardVerification $verification */
					$verification = $result2->creditCardVerification;
					$this->is_error = true;
					$this->error_message = $verification->status . '  ' . $verification->processorResponseText . "\n\n";

					return false;
				}
			}
		}
		else
		{
			$this->is_error = true;
			if ($result)
			{
				foreach ($result->errors->deepAll() AS $error)
				{
					$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
				}
			}
			else
			{
				$this->error_message = $this::ERROR_COMMON;
			}

			return false;
		}
	}

	/**
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 * @return bool
	 */
	public function updatePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		$idParts = explode('||', $paymentProfile->getExternalProfileId());
		if (count($idParts) != 2)
		{
			$this->is_error = true;
			$this->error_message = 'Invalid external profile id';

			return false;
		}

		$customerProfileId = $idParts[0];
		$profileId = $idParts[1];

		$additionalData = $paymentProfile->getAdditionalParams();

		if ((isset($additionalData['payment_method_nonce']) && $additionalData['payment_method_nonce']))
		{
			$card_nonce = $additionalData['payment_method_nonce'];
			/** @var Braintree\PaymentMethodNonce $pMethodInfo */
			$pMethodInfo = $this->doFindPaymentMethodByNonce($card_nonce);
			//var_dump($pMethodInfo);
			if ($pMethodInfo)
			{
				$result = $this->doDeletePaymentMethod($profileId);
				if ($result->success)
				{
					$p_method = array('customerId'         => $customerProfileId,
									  'paymentMethodNonce' => $card_nonce,
									  'options'            => array('verifyCard' => true));

					/** @var Braintree\Result\Successful $result2 */
					$result2 = $this->doAddPaymentMethod($p_method);
					if ($result2->success)
					{
						$profileId = $result2->paymentMethod->token;
						if ($pMethodInfo->type == 'PayPalAccount')
						{
							$billingData->setCcNumber('XXXXXXXXXXXppal');
							$billingData->setCcExpirationMonth('-');
							$billingData->setCcExpirationYear('-');
							$billingData->setCcExpirationDate('-');
							$billingData->setCcType(Braintree\PaymentInstrumentType::PAYPAL_ACCOUNT);
							$paymentProfile->setBillingData($billingData);
						}
						$billingAddress = $billingData->getAddress();

						$customer = array_merge($this->customer_data_default, array('firstName' => $billingAddress->getFirstName(),
																					'id'        => $customerProfileId,
																					'lastName'  => $billingAddress->getLastName(),
																					'company'   => $billingAddress->getCompany(),
																					'phone'     => $user->data['phone'],
																					'email'     => $user->data['email']));
						$this->setCustomerData($customer);
						$result = $this->doUpdateCustomer($customer);
						if ($result->success)
						{
							$paymentProfile->setExternalProfileId($customerProfileId . '||' . $profileId);
							$this->is_error = false;
							$this->error_message = '';

							return true;
						}

					}
					else
					{
						$this->is_error = true;
						if ($result)
						{
							//var_dump($result);
							foreach ($result->errors->deepAll() AS $error)
							{
								$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
							}
						}
						else
						{
							$this->error_message = $this::ERROR_COMMON;
						}

						return false;
					}
				}
				else
				{
					if ($result)
					{
						foreach ($result->errors->deepAll() AS $error)
						{
							$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
						}
					}
					else
					{
						if (!$this->is_error)
						{
							$this->is_error = true;
							$this->error_message = $this::ERROR_COMMON;
						}
					}

					return false;
				}
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = ('Wrong payment method ' . "\n\n");

			return false;
		}
	}

	/**
	 * @param ORDER $order
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 */
	protected function createRecurringProfile(ORDER $order, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$orderRepository = DataAccess_OrderRepository::getInstance();

		/** @var Model_LineItem $item */
		foreach ($order->lineItems as $item)
		{
			if ($item->getEnableRecurringBilling() && !is_null($item->getRecurringBillingData()))
			{
				/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
				$recurringBillingData = $item->getRecurringBillingData();

				$recurringBillingData->setPaymentProfileId($paymentProfile->getId());

				$item->setRecurringBillingData($recurringBillingData);

				$orderRepository->persistLineItem($order, $item);
			}
		}
	}

	public function supportsPaymentProfiles()
	{
		return true;
	}

	public function supportsPartialRefund($order_data = false)
	{
		return true;
	}

	function supportsVoid($order_data = false)
	{
		return $order_data && ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_PENDING);
	}

	public function capturedAmount($order_data = false)
	{
		return $order_data ? $order_data['custom3'] : false;
	}

	function capture($order_data = false, $capture_amount = false)
	{
		global $order, $db;

		$this->is_error = false;
		$transaction_order = $order;
		$transaction_db = $db;

		if ($order_data)
		{
			//check is transaction completed
			if ($this->isTotalAmountCaptured($order_data))
			{
				$this->is_error = true;
				$this->error_message = 'Transaction already completed';

				return false;
			}

			//get captured total
			$captured_total_amount = $capture_amount ? $capture_amount : $this->capturableAmount($order_data);
			$transaction_id = $order_data['security_id'];

			if ($transaction_id == '')
			{
				$this->is_error = true;
				$this->error_message = 'This order does not have a valid transaction id';

				return false;
			}
			else
			{
				if ($captured_total_amount <= 0)
				{
					$this->is_error = true;
					$this->error_message = 'Capture amount must be greater than 0';

					return false;
				}
				else
				{
					if ($captured_total_amount > $order_data['total_amount'])
					{
						$this->is_error = true;
						$this->error_message = 'Capture amount cannot be more than total amount';

						return false;
					}
					else
					{
						if ($order_data['custom3'] + $captured_total_amount > $order_data['total_amount'])
						{
							$this->is_error = true;
							$this->error_message = 'Captured amount cannot be more than total amount';

							return false;
						}
					}
				}

				$result = $this->doCapture($transaction_id, $captured_total_amount);

				$this->log('capture Transaction result: ', $result);

				$userTmp = new tmp_object();
				$userTmp->id = $order_data['uid'];

				$orderTmp = new tmp_object();
				$orderTmp->oid = $order_data['oid'];
				$orderTmp->subtotalAmount = $order_data['subtotal_amount'];
				$orderTmp->totalAmount = $order_data['total_amount'];
				$orderTmp->shippingAmount = $order_data['shipping_amount'];
				$orderTmp->taxAmount = $order_data['tax_amount'];
				$orderTmp->gift_cert_amount = $order_data['gift_cert_amount'];
				if ($result->success)
				{

					/** @var Braintree\Transaction $transaction */
					$transaction = $result->transaction;
					$security_id = $transaction_id;

					$totalCaptured = $this->getTotalCaptured($order_data) + $captured_total_amount;
					$custom2 = '';
					$custom3 = number_format($totalCaptured, 2, '.', '');

					$transaction_db->reset();
					$transaction_db->assignStr('custom2', $custom2);
					$transaction_db->assignStr('custom3', $custom3);
					$transaction_db->assignStr('security_id', $security_id);

					$oldPaymentStatus = $transaction_order->getPaymentStatus();
					if ($order_data['total_amount'] > $totalCaptured)
					{
						$transaction_order->setPaymentStatus(ORDER::PAYMENT_STATUS_PARTIAL);
					}
					else
					{
						$transaction_order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);
					}
					$transaction_db->assignStr('payment_status', $transaction_order->getPaymentStatus());
					$transaction_db->update(DB_PREFIX . 'orders', "WHERE oid='" . $order_data['oid'] . "'");

					$_response = var_export($result, true);
					$this->createTransaction($transaction_db,
											$userTmp,
											$transaction_order,
											$_response,
											$security_id, 'capture', $custom2, $custom3, $security_id);

					$this->fireOrderEvent($transaction_order, $transaction_order->getStatus(), $oldPaymentStatus);

					$this->is_error = false;
					$this->error_message = '';
				}
				else
				{
					/** @var Braintree\Result\Error $result */
					$this->is_error = true;
					if ($result)
					{
						foreach ($result->errors->deepAll() AS $error)
						{
							$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
						}
					}
					else
					{
						$this->error_message = $this::ERROR_COMMON;
					}
				}
			}
		}

		return !$this->is_error;
	}

	function isTotalAmountCaptured($order_data)
	{
		return $order_data['custom3'] != '' && $order_data['custom3'] == $order_data['total_amount'];
	}

	function capturableAmount($order_data = false)
	{
		if ($order_data)
		{
			if ($this->supportsCapture($order_data))
			{
				$capturableAmount = $order_data['total_amount'] - $order_data['custom3'];
				if ($capturableAmount < 0)
				{
					$capturableAmount = 0.0;
				}

				return number_format($capturableAmount, 2, '.', '');
			}
		}

		return 0;
	}

	function supportsCapture($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_PENDING || $order_data['payment_status'] == ORDER::PAYMENT_STATUS_PARTIAL)
			{
				$statusResult = true;

				if ($this->isTotalAmountCaptured($order_data))
				{
					return false;
				}

				return $statusResult;
			}
		}

		return false;
	}

	private function doCapture($transaction_id, $amount)
	{

		$this->log("Capture Transaction params: common: \n", array('transaction_id' => $transaction_id,
																   'amount'         => $amount));
		$this->log("\n", array());

		if ($this->oauth_accessToken && $this->oauth_gateway)
		{
			$result = $this->oauth_gateway->transaction()->submitForSettlement($transaction_id);
		}
		else
		{
			$result = Braintree\Transaction::submitForSettlement($transaction_id);
		}

		//submitForSettlement($transaction_id, $amount);
		return $result;
	}

	function getTotalCaptured($order_data)
	{
		return 0.00 + $order_data['custom3'];
	}

	public function refundableAmount($order_data = false)
	{
		if ($order_data)
		{
			if ($this->supportsRefund($order_data))
			{
				return $this->_refundableAmount($order_data);
			}
		}

		return 0;
	}

	function supportsRefund($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_RECEIVED || $order_data['payment_status'] == ORDER::PAYMENT_STATUS_PARTIAL)
			{
				$tData = $this->getTransactionData($order_data['security_id']);
				$statusResult = false;
				if ($tData)
				{
					if (($tData->status == \Braintree\Transaction::SETTLED) || ($tData->status == \Braintree\Transaction::SETTLING))
					{
						$statusResult = true;
					}
				}

				return ($statusResult);
			}
			else
			{
				if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_REFUNDED)
				{
					$amountResult = $this->_refundableAmount($order_data) > 0.0;
					$tData = $this->getTransactionData($order_data['security_id']);
					$statusResult = false;
					if ($tData)
					{
						if (($tData->status == \Braintree\Transaction::SETTLED) || ($tData->status == \Braintree\Transaction::SETTLING))
						{
							$statusResult = true;
						}
					}

					return ($amountResult && $statusResult);
				}
			}
		}

		return false;
	}

	private function getTransactionData($transaction_id)
	{

		$this->log("Get Transaction Data \n", array('transact_id' => $transaction_id));
		$this->log("\n", array());

		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->transaction()->find($transaction_id);
			}
			else
			{
				$result = Braintree\Transaction::find($transaction_id);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Transaction  not found (' . $transaction_id . ')');
			$this->setExceptionError($e, 'Payment method not found!(' . $transaction_id . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function _refundableAmount($order_data)
	{
		$refundableAmount = $this->getTotalCaptured($order_data);

		return number_format($refundableAmount, 2, '.', '');
	}

	function refund($order_data = false, $refund_amount = false)
	{
		global $order, $db;

		$this->is_error = false;
		$transaction_order = $order;
		$transaction_db = $db;

		if ($order_data)
		{
			if (!$this->supportsRefund($order_data))
			{
				$this->is_error = true;
				$this->error_message = 'Order cannot be refunded';

				return false;
			}

			$refund_total_amount = $refund_amount;

			if (!$refund_total_amount)
			{
				$refund_total_amount = $order_data['custom3'];
			}

			if ($refund_total_amount < 0)
			{
				$this->is_error = true;
				$this->error_message = 'Refund amount must be greater than 0';

				return false;
			}
			else
			{
				if ($refund_total_amount > $order_data['total_amount'])
				{
					$this->is_error = true;
					$this->error_message = 'Refund amount cannot be more than total amount';

					return false;
				}
				else
				{
					if ($refund_total_amount > $order_data['custom3'])
					{
						$this->is_error = true;
						$this->error_message = 'Refund amount cannot be more than total captured amount';

						return false;
					}
				}
			}

			$transaction_id = $order_data["security_id"];
			$result = $this->doRefund($transaction_id, $refund_total_amount);
			$this->log('refund Transaction result:', $result);

			if ($result->success)
			{

				/** @var Braintree\Transaction $transaction */
				$transaction = $result->transaction;
				$security_id = $transaction_id;

				$payment_status = ORDER::PAYMENT_STATUS_REFUNDED;
				$db->reset();
				$db->assignStr('payment_status', $payment_status);
				$db->update(DB_PREFIX . 'orders', "WHERE oid='" . $order_data['oid'] . "'");
				$userTmp = new tmp_object();
				$userTmp->id = $order_data['uid'];

				$_ra = floatval($order_data['oid']) + $refund_total_amount;
				$custom2 = number_format($_ra, 2, '.', '');
				$totalCaptured = $this->getTotalCaptured($order_data) - $refund_total_amount;
				$custom3 = number_format($totalCaptured, 2, '.', '');

				$_response = var_export($result, true);
				$this->createTransaction($db,
					$userTmp,
					$order,
					$_response,
					$security_id, 'refund', $custom2, $custom3, $security_id);

				$this->is_error = false;
				$this->error_message = '';
			}
			else
			{
				/** @var Braintree\Result\Error $result */
				$this->is_error = true;
				if ($result)
				{
					foreach ($result->errors->deepAll() AS $error)
					{
						$this->error_message .= ($error->attribute . ': ' . $error->code . ' ' . $error->message . "\n\n");
					}
				}
				else
				{
					$this->error_message = $this::ERROR_COMMON;
				}
				$custom1 = $order_data['custom1'];
				$custom2 = $order_data['custom2'];
				$custom3 = $order_data['custom3'];
				$_response = var_export($result, true);
				$this->createTransaction($db,
					$userTmp,
					$order,
					$_response,
					$transaction_id, 'refund:error', $custom2, $custom3, $transaction_id);
			}
		}

		return (!$this->is_error);
	}

	private function doRefund($transaction_id, $amount)
	{

		$this->log("Refund Transaction  \n", array('transact_id' => $transaction_id, 'amount' => $amount));
		$this->log("\n", array());

		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->transaction()->refund($transaction_id);
			}
			else
			{
				$result = Braintree\Transaction::refund($transaction_id, $amount);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Transaction  not found (' . $transaction_id . ')');
			$this->setExceptionError($e, 'Payment method not found!(' . $transaction_id . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doFindPaymentMethodByNonce($nonce)
	{

		$this->log("FindPaymentMethodByNonce Transaction  \n", array('nonce' => $nonce));
		$this->log("\n", array());

		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->paymentMethodNonce()->find($nonce);
			}
			else
			{
				$result = Braintree\PaymentMethodNonce::find($nonce);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Payment Method  not found (' . $nonce . ')');
			$this->setExceptionError($e, 'Payment method not found!(' . $nonce . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doFindPaymentMethodByToken($token)
	{

		$this->log("doFindPaymentMethodByToken Transaction  \n", array('token' => $token));
		$this->log("\n", array());

		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->paymentMethod()->find($token);
			}
			else
			{
				$result = Braintree\PaymentMethod::find($token);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Payment Method  not found (' . $token . ')');
			$this->setExceptionError($e, 'Payment method not found!(' . $token . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doDeletePaymentMethod($token)
	{

		$this->log("Delete PaymentMethod Transaction  \n", array('method token' => $token));
		$this->log("\n", array());

		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->paymentMethod()->delete($token);
			}
			else
			{
				$result = Braintree\PaymentMethod::delete($token);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Payment method not found (' . $token . ')');
			$this->setExceptionError($e, 'Payment method not found!(' . $token . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doDeleteCustomer($id)
	{

		$this->log("Delete Customer Transaction  \n", array('customer id' => $id));
		$this->log("\n", array());

		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->customer()->delete($id);
			}
			else
			{
				$result = Braintree\Customer::delete($id);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Customer  not found (' . $id . ')');
			$this->setExceptionError($e, 'Customer not found!(' . $id . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doUpdateCustomer($customer)
	{
		try
		{
			$t_params = array('firstName' => $customer['firstName'],
							  'lastName'  => $customer['lastName'],
							  'company'   => $customer['company'],
							  'phone'     => $customer['phone'],
							  'fax'       => $customer['fax'],
							  'website'   => $customer['website'],
							  'email'     => $customer['email']);

			$this->log("Update Customer Transaction  \n", array('data' => $t_params, 'id' => $customer['id']));
			$this->log("\n", array());

			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->customer()->update($customer['id'], $t_params);
			}
			else
			{
				$result = Braintree\Customer::update($customer['id'], $t_params);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Customer  not found (' . $customer['id'] . ')');
			$this->setExceptionError($e, 'Customer not found!(' . $customer['id'] . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doAddCustomer($customer)
	{

		$this->log("Add Customer Transaction  \n", array('data' => $customer));
		$this->log("\n", array());

		try
		{
			$t_params = array('firstName' => $customer['firstName'],
							  'lastName'  => $customer['lastName'],
							  'company'   => $customer['company'],
							  'phone'     => $customer['phone'],
							  'fax'       => $customer['fax'],
							  'website'   => $customer['website'],
							  'email'     => $customer['email']);

			if ($customer['paymentMethodNonce'])
			{
				$t_params['paymentMethodNonce'] = $customer['paymentMethodNonce'];
			}
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->customer()->create($t_params);
			}
			else
			{
				$result = Braintree\Customer::create($t_params);
			}
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doUpdatePaymentMethod($token, $params)
	{
		try
		{
			if (isset($params['paymentMethodNonce']) && $params['paymentMethodNonce'])
			{
				$t_params = array(
					'paymentMethodNonce' => $params['paymentMethodNonce'],
					'options' => array('verifyCard' => true)
				);
			}
			else
			{
				$t_params = array(
					'cardholderName' => $params['cardholderName'],
					'expirationMonth' => $params['expirationMonth'],
					'expirationYear' => $params['expirationYear'],
					'number' => $params['number'],
					'options' => array('verifyCard' => true)
				);
			}

			$this->log("Update Payment Method Transaction  \n", array('data' => $t_params, 'token' => $token));
			$this->log("\n", array());

			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->paymentMethod()->update($token, $t_params);
			}
			else
			{
				$result = Braintree\PaymentMethod::update($token, $t_params);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Payment Method  not found (' . $token . ')');
			$this->setExceptionError($e, 'Payment Method not found!(' . $token . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	private function doAddPaymentMethod($params)
	{

		$this->log("Add Payment Method Transaction  \n", array('data' => $params));
		$this->log("\n", array());

		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->paymentMethod()->create($params);
			}
			else
			{
				$result = Braintree\PaymentMethod::create($params);
			}
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	function void($order_data = false)
	{
		global $order, $db;
		$this->is_error = false;

		if ($order_data)
		{
			$result = $this->doVoid($order_data);

			$this->log('Void Transaction response:', $result);
			if ($result->success)
			{

				$payment_status = ORDER::PAYMENT_STATUS_CANCELED;
				$db->reset();
				$db->assignStr('payment_status', $payment_status);
				$db->update(DB_PREFIX . 'orders', "WHERE oid='" . $order_data['oid'] . "'");
				$userTmp = new tmp_object();
				$userTmp->id = $order_data['uid'];

				$_response = var_export($result, true);
				$this->createTransaction($db, $userTmp, $order, $_response,
					$order_data['security_id'], 'void', '', '', $order_data['security_id']);

				$this->is_error = false;
				$this->error_message = '';
			}
			else
			{
				/** @var Braintree\Result\Error $result */
				$this->is_error = true;
				$this->error_message = $result->transaction->processorSettlementResponseCode . ' ' . $result->transaction->processorSettlementResponseText;
			}
		}

		return !$this->is_error;
	}

	private function doVoid($order_data)
	{

		$this->log("Void Transaction  \n", array('order'       => $order_data['order_num'],
												 'transact_id' => $order_data['security_id']));
		$this->log("\n", array());

		$transaction_id = $order_data['security_id'];
		try
		{
			if ($this->oauth_accessToken && $this->oauth_gateway)
			{
				$result = $this->oauth_gateway->transaction()->void($transaction_id);
			}
			else
			{
				$result = Braintree\Transaction::void($transaction_id);
			}
		}
		catch (Braintree\Exception\NotFound $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_NotFound :\n", 'Transaction  not found (' . $order_data['security_id'] . ')');
			$this->setExceptionError($e, 'Transaction  not found (' . $order_data['security_id'] . ')');

			return $result;
		}
		catch (Braintree\Exception\Authorization $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Authorization :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\Configuration $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_Configuration :\n", 'API key is not authorized to perform the attempted action');
			$this->setExceptionError($e);

			return $result;
		}
		catch (Braintree\Exception\DownForMaintenance $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_DownForMaintenance :\n", 'Braintree request timed out');
			$this->setExceptionError($e, 'Braintree request timed out');

			return $result;
		}
		catch (Braintree\Exception\ServerError $e)
		{
			$result = false;
			$this->log("process Braintree_Exception_ServerError :\n", 'Braintree Server Error');
			$this->setExceptionError($e, 'Braintree Server Error');

			return $result;
		}
		catch (Exception $e)
		{
			$result = false;
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);

			return $result;
		}

		return $result;
	}

	public function getRecurringPlans()
	{
		if ($this->oauth_accessToken && $this->oauth_gateway)
		{
			$result = $this->oauth_gateway->plan()->all();
		}
		else
		{
			$result = Braintree\Plan::all();
		}

		return $result;
	}

	public function supportsRecurringBillingProfiles()
	{
		$result = true;
		return $result;
	}
}

<?php

	chdir("./../../../../");
	define('ENV', true);

	require_once 'content/classes/_boot.php';

	/**
	 * Force handling to be included in shipping price as google does not support handling
	 */
	$settings['ShippingHandlingSeparated'] = '0';

	// load localization procedures
	require_once("content/engine/engine_localization.php");
	require_once("content/engine/engine_user.php");
	require_once("content/engine/engine_order.php");
	require_once("content/engine/engine_payment.php");

	/**
	 * Check session variable and start session
	 */
	$pcsid = isset($_REQUEST["pcsid"]) ? $_REQUEST["pcsid"] : "";

	if (preg_match("/[0-9a-zA-Z]{1,}/", $pcsid))
	{
		session_id($pcsid);
	}

	setSessionSavePath();

	session_set_cookie_params($settings["SecurityUserTimeout"]);
	session_cache_expire(round($settings["SecurityUserTimeout"]/60)); // 24 hours
	session_name("ShoppingCartSession");
	session_start();

	// custom fields
	$customFields = new CustomFields($db);

	/**
	 * Languages
	 */
	$languages = new Language($db);

	view()->assign("languages", $active_languages = $languages->getActiveLanguages());
	view()->assign("languages_count", count($active_languages));

	// check / set language for a first time
	if (!isset($_SESSION["current_language_id"]))
	{
		$def_lang = $languages->getDefaultLanguage();
		$_SESSION["current_language_id"] = $def_lang["language_id"];
	}

	//check / set current active language
	$current_language = $languages->getActiveLanguageById($_SESSION["current_language_id"]);
	view()->assign("current_language", $current_language);
	view()->assign("msg", $msg = $languages->getDictionary($current_language["code"]));
	view()->prependJavascript($languages->getJavascriptCache($current_language["code"]));

	// pass settings to smarty
	foreach ($settings as $key=>$value) view()->assign($key, $value);

	$oid = $_REQUEST['oid'];
	$securityId = $_REQUEST['_pcod'];

	$retObj = new stdClass();
	$retObj->status = 404;
	$retObj->message = 'ORDER_NOT_FOUND';

	$orderDb = $db->selectOne('SELECT * FROM '.DB_PREFIX.'orders WHERE oid="'.intval($oid).'" AND security_id="'.$securityId.'"');

	if ($orderDb)
	{
		$user = USER::getById($db, $settings, $orderDb["uid"]);
		$order = ORDER::getById($db, $settings, $user, $orderDb["oid"]);
		$order->getOrderData();
		$order->getOrderItems();

		// Backwards compatibility for DisplayPricesWithTax
		$taxProvider = Tax_ProviderFactory::getTaxProvider();
		$taxProvider->initTax($order);
		$settings['DisplayPricesWithTax'] = $taxProvider->getDisplayPricesWithTax() ? 'YES' : 'NO';
		view()->assign('DisplayPricesWithTax', $settings['DisplayPricesWithTax']);

		// TODO: get from registry
		$paymentProvider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings)));
		$pp = $paymentProvider->getPaymentProcessor($order);

		if ($pp)
		{
			$form = $_REQUEST;
			$result = $pp->process($db, $user, $order, $form);

			if ($result == 'Received' || $result === true)
			{
				$sendNotification = $pp->sendEmailNotification($result);

				$orderStatus = 'Process';

				if ($result === true)
				{
					$result = 'Received';
				}

				if (!$order->getShippingRequired() && $result === 'Received')
				{
					$orderStatus = 'Completed';
				}

				$orderSource = isset($_SESSION["osc_source"]) ? $_SESSION["osc_source"] : null;
				$offsiteCampaignId = isset($_SESSION["osc_campaign_id"]) ? $_SESSION["osc_campaign_id"] : null;

				OrderProvider::closeOrder($order, $form, $customFields, $orderStatus, $result,
					$orderSource, $offsiteCampaignId, $sendNotification);

				$retObj->status = 200;
				$retObj->message = 'OK';
			}
			else
			{
				$retObj->status = 500;
				$retObj->message = $pp->error_message;
			}
		}
		else
		{
			$retObj->status = 500;
			$retObj->message = 'CONFIG_ERROR';
		}
	}

	header('Content-Type: application/json');
	header("Cache-Control: private");
	echo json_encode($retObj);
<?php 
	$payment_processor_id = "paypalhsuk";
	$payment_processor_name = "PayPal Website Payments Pro Hosted Solution";
	$payment_processor_class = "PAYMENT_PAYPALHSUK";
	$payment_processor_type = "ipn";
	
	class PAYMENT_PAYPALHSUK extends PAYMENT_PROCESSOR
	{
		protected $buttonSource = '';

		function PAYMENT_PAYPALHSUK()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "paypalhsuk";
			$this->name = "PayPal Website Payments Pro Hosted Solution";
			$this->class_name = "PAYMENT_PAYPALHSUK";
			$this->type = "ipn";
			$this->description = "";
			$this->post_url = "https://securepayments.paypal.com/cgi-bin/acquiringweb";
			$this->steps = 2;
			$this->possible_payment_delay = true;
			$this->possible_payment_delay_timeout = 30; // seconds
			$this->allow_change_gateway_url = false;

			$this->buttonSource = getpp().'_Cart_HSS';

			return $this;
		}
		
		//redeclare getPaymentForm 
		function getPaymentForm($db)
		{
			global $_SESSION;
			global $settings, $order, $url_https;
			global $msg;

			$this->isTestMode();

			$_pf = parent::getPaymentForm($db);
			
			$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?securityId=".$this->common_vars["order_security_id"]["value"]."&oid=".$this->common_vars['order_oid']['value']."&p=paypal_callback";
			$return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=completed";
			$cancel_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;
			
			// wmw 04/05/05 Added to get phone number fields for PayPal
			if ($this->common_vars["billing_country_iso_a2"]["value"] == "US")
			{
				$pp_phone = preg_replace("[-\(\)\. ]","",$this->common_vars["billing_phone"]["value"]);
				$pp_phone_a = substr($pp_phone,0,3);
				$pp_phone_b = substr($pp_phone,3,3);
				$pp_phone_c = substr($pp_phone,6,4);
			} 
			else 
			{
				$pp_phone_a = "";
				$pp_phone_b = "";
				$pp_phone_c = "";
			}
			// END wmw 04/05/05
				
			$fields = array();
			
			$fields[] = array("type"=>"hidden", "name"=>"cmd", "value"=>"_hosted-payment");
			$fields[] = array("type"=>"hidden", "name"=>"business", "value"=>$this->settings_vars[$this->id."_business"]["value"]);
			
			$fields[] = array("type"=>"hidden", "name"=>"billing_first_name", "value"=>$this->common_vars["billing_first_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"billing_last_name", "value"=>$this->common_vars["billing_last_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"billing_address1", "value"=>$this->common_vars["billing_address1"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"billing_address2", "value"=>$this->common_vars["billing_address2"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"billing_city", "value"=>$this->common_vars["billing_city"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"billing_country", "value"=>$this->common_vars["billing_country_iso_a2"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"billing_state", "value"=>$this->common_vars["billing_state_abbr"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"billing_zip", "value"=>$this->common_vars["billing_zip"]["value"]);
			
			$fields[] = array("type"=>"hidden", "name"=>"paymentaction", "value"=>strtolower($this->settings_vars[$this->id."_Transaction_Type"]["value"]));
			$fields[] = array("type"=>"hidden", "name"=>"currency_code", "value"=>$this->settings_vars[$this->id."_Currency_Code"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"bn", "value"=> $this->buttonSource);
			
			$fields[] = array("type"=>"hidden", "name"=>"invoice", "value"=>$this->common_vars["order_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"custom", "value"=>$this->common_vars["order_id"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"subtotal", "value"=>round($this->common_vars["order_total_amount"]["value"], 2));
			
			$fields[] = array("type"=>"hidden", "name"=>"address_override", "value"=>"false");
			$fields[] = array("type"=>"hidden", "name"=>"first_name", "value"=>$this->common_vars["shipping_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address1", "value"=>trim($this->common_vars["shipping_address1"]["value"]));
			$fields[] = array("type"=>"hidden", "name"=>"address2", "value"=>trim($this->common_vars["shipping_address2"]["value"]));

			$fields[] = array("type"=>"hidden", "name"=>"city", "value"=>$this->common_vars["shipping_city"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"state", "value"=>$this->common_vars["shipping_state_abbr"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"zip", "value"=>$this->common_vars["shipping_zip"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"country", "value"=>$this->common_vars["shipping_country_iso_a2"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"buyer_email", "value"=>$this->common_vars["billing_email"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"notify_url", "value"=>$notify_url);
			$fields[] = array("type"=>"hidden", "name"=>"return", "value"=>$return_url);
			$fields[] = array("type"=>"hidden", "name"=>"cancel_return", "value"=>$cancel_url);

			$certId = $this->settings_vars[$this->id."_Cert_ID"]["value"];
			if (trim($certId) != '')
			{
				// Use encryption
				$temp = $fields;
				$fields = array();

				$random_string = rand(1000000, 9999999).'-'.$order->oid.'-';

				$data = 'cert_id='.$certId."\n";
				foreach ($temp as $field)
				{
					$data .= $field['name'] .'='. $field['value'] ."\n";
				}

				$dataFile = $settings['GlobalServerPath'].'/content/cache/tmp/paypal-'.$random_string.'data.txt';
				$signedFile = $settings['GlobalServerPath'].'/content/cache/tmp/paypal-'.$random_string.'signed.txt';
				$encryptedFile = $settings['GlobalServerPath'].'/content/cache/tmp/paypal-'.$random_string.'encrypted.txt';

				$certFile = $settings['GlobalServerPath'].'/content/engine/payment/paypal/paypal.cer';
				$permFile = $settings['GlobalServerPath'].'/content/engine/payment/paypal/paypal.pem';
				$paypalKey = $settings['GlobalServerPath'].'/content/engine/payment/paypal/'.($this->isTestMode() ? 'paypal_sandbox_public.pem' : 'paypal_public.pem');

				$fp = fopen($dataFile, 'w');
				fwrite($fp, $data);
				fclose($fp);

				unset($data);

				$data = '';
				if (function_exists('openssl_pkcs7_sign') && function_exists('openssl_pkcs7_encrypt'))
				{
					if (@openssl_pkcs7_sign($dataFile, $signedFile, @file_get_contents($certFile), @file_get_contents($permFile), array('From' => $this->settings_vars[$this->id."_business"]["value"]), PKCS7_BINARY))
					{
						unlink($dataFile);
	 
						$signed = @file_get_contents($signedFile);
						$signed = explode("\n\n", $signed);
						$signed = base64_decode($signed[1]);

						$fp = fopen($signedFile, 'w');
						fwrite($fp, $signed);
						fclose($fp);

						unset($signed);

						openssl_pkcs7_encrypt($signedFile, $encryptedFile, @file_get_contents($paypalKey), array('From' => $this->settings_vars[$this->id."_business"]["value"]), PKCS7_BINARY);

						unlink($signedFile);

						$data = @file_get_contents($encryptedFile);
						$data = explode("\n\n", $data);
						$data = '-----BEGIN PKCS7-----' . "\n" . $data[1] . "\n" . '-----END PKCS7-----';

						unlink($encryptedFile);

						$fields[] = array('type' => 'hidden', 'name' => 'cmd', 'value' => '_s-xclick');
						$fields[] = array('type' => 'hidden', 'name' => 'encrypted', 'value' => $data);
					}
				}

				if (trim($data) == '')
				{
					//Reset the fields if encryption failed

					$this->log('Encryption failed, returning non-encrypted form');
					error_log('PayPal HSUK Encryption failed, returning non-encrypted form');

					$fields = $temp;
				}
			}

			return $fields;
		}
		
		function getValidatorJS()
		{
			return "";
		}
		
		function isTestMode()
		{
			if (strtolower($this->settings_vars[$this->id."_Mode"]["value"]) == "test")
			{
				$this->testMode = true;
				$this->post_url = "https://securepayments.sandbox.paypal.com/acquiringweb";
			}
			else
			{
				$this->testMode = false;
				$this->post_url = "https://securepayments.paypal.com/cgi-bin/acquiringweb";
			}
			return $this->testMode;
		}
		
		function getPostUrl()
		{
			return $this->post_url;
		}
		
		function process($db, $user, $order, $post_form)
		{
			global $_POST;
			global $settings;

			$this->log('Process Request:', $_POST);

			if (trim(strtolower($post_form["payment_status"])) == "completed")
			{
				if ((isset($post_form['mc_gross']) && $order->totalAmount != $post_form['mc_gross']) ||
					(isset($post_form['payment_gross']) && $order->totalAmount != $post_form['payment_gross']))
				{
					$post_form['payment_status'] = 'pending';
				}
			}

			//check payment status				
			if ((trim(strtolower($post_form["payment_status"])) == "completed") || (trim(strtolower($post_form["payment_status"])) == "pending"))
			{
				//reinsure all is OK - verify request
				$post_data = 'cmd=_notify-validate';
				reset($post_form);
				
				foreach ($_POST as $var_name=>$var_value)
				{
					$post_data .= "&".$var_name."=".urlencode($var_value);
				}

				//https://www.sandbox.paypal.com/cgi-bin/webscr
				$this->post_url = $this->getPostUrl();
				
				//send data back to server
				$c = curl_init();
				
				if ($settings["ProxyAvailable"] == "YES")
				{
					//curl_setopt($c, CURLOPT_VERBOSE, 1);
					if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
					{
						curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
					}
					curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
					if ($settings["ProxyRequiresAuthorization"] == "YES")
					{
						curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
					}

					curl_setopt($c, CURLOPT_TIMEOUT, 120);
				}
			
				curl_setopt($c, CURLOPT_URL, trim($this->post_url));
				curl_setopt($c, CURLOPT_FAILONERROR, 1);
				curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($c, CURLOPT_POST, 1);
				curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
			
				curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
				curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

				$buffer = curl_exec($c);

				$this->log("process Response:\n".$buffer);
				
				curl_close($c);
				
				//check verification value
				if (trim(strtolower($buffer)) == "verified")
				{
					reset($post_form);
					$response = "Verification Status: ".$buffer."\n";

					$transaction_id = '';
					$PendingReason = '';

					foreach ($post_form as $var_name=>$var_value)
					{
						$response.=$var_name." = ".$var_value."\n";
						
						if ($var_name == "txn_id") $transaction_id = $var_value;
						
						if ($var_name == "payment_status") $payment_status = $var_value;	
						
						if ($var_name == "pending_reason") $PendingReason = $var_value;
					}
					
					$this->createTransaction($db, $user, $order, $response, "", $PendingReason, "", 0, $transaction_id);
					$this->is_error = false;
					$this->error_message = "";
					
					return strtolower($payment_status) == 'completed' ? 'Received' : 'Pending';
				}
				else
				{
					$this->is_error = true;

					$this->storeTransaction($db, $user, $order, $post_form, '', false);

					$this->error_message = "Can't complete your order. PayPal payment is not completed yet.";
				}
			}
			else
			{
				//(trim(strtolower($post_form["payment_status"])) == "refunded")
				$this->is_error = true;
				$this->error_message = "An error occurred while PayPal was processing your order. It will be investigated by a human at the earliest opportunity. We apologise for any inconvenience.";
			}

			return !$this->is_error;
		}

		function success()
		{
			die();
		}
	}

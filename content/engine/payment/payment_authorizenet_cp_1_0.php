<?php 
	$payment_processor_id = "authorizenet_cp_1_0";
	$payment_processor_name = "Authorize.Net CP 1.0";
	$payment_processor_class = "PAYMENT_AUTHORIZENET_CP_1_0";
	$payment_processor_type = "cc";

	class PAYMENT_AUTHORIZENET_CP_1_0 extends PAYMENT_PROCESSOR
	{
		function PAYMENT_AUTHORIZENET_CP_1_0()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "authorizenet_cp_1_0";
			$this->name = "Authorize.Net CP 1.0";
			$this->class_name = "PAYMENT_AUTHORIZENET_CP_1_0";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->support_ccs = true;
			$this->need_cc_codes = true;
			return $this;
		}

		function getPaymentForm($db)
		{
			global $settings;

			$fields = parent::getPaymentForm($db);

			if ($this->isCardinalCommerceEnabled())
			{
				$fields["do_cmpi_lookup"] = array("type" => "hidden", "name"=> "do_cmpi_lookup", "value" => 1);
			}

			return $fields;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Test_Request"]["value"] == "TRUE")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}

			return $this->testMode;
		}

		function processAuthorize($db, $user, $order, $post_form, $x_authentication_indicator="", $x_cardholder_authentication_value="")
		{
			global $settings;

			//build post data for query
			$post_data = "";
			// get post_form, common, settings and custom vars
			reset($this->common_vars);
			$post_data = "".
				//Merchant Account Information
				"x_cpversion=1.0".
				"&x_login=".urlencode($this->settings_vars[$this->id."_Login"]["value"]).
				"&x_tran_key=".urlencode($this->settings_vars[$this->id."_Transaction_Key"]["value"]).
				"&x_market_type=2". // Retail
				"&x_device_type=8". // Website
				"&x_test_request=".urlencode($this->settings_vars[$this->id."_Test_Request"]["value"]).
				"&x_response_format=1".
				"&x_duplicate_window=".urlencode($this->settings_vars[$this->id."_Duplicate_Window"]["value"]).
				//Gateway Response Configuration
				"&x_delim_char=".urlencode("~").
				//Customer Name and Billing Address
				"&x_first_name=".urlencode($post_form["cc_first_name"]).
				"&x_last_name=".urlencode($post_form["cc_last_name"]).
				"&x_company=".urlencode($this->common_vars["billing_company"]["value"]).
				"&x_address=".urlencode($this->common_vars["billing_address"]["value"]).
				"&x_city=".urlencode($this->common_vars["billing_city"]["value"]).
				"&x_state=".urlencode($this->common_vars["billing_state"]["value"]).
				"&x_zip=".urlencode($this->common_vars["billing_zip"]["value"]).
				"&x_country=".urlencode($this->common_vars["billing_country"]["value"]).
				"&x_phone=".urlencode($this->common_vars["billing_phone"]["value"]).
				//Additional Customer Data
				"&x_cust_id=".urlencode($user->id).
				"&x_email=".urlencode($this->common_vars["billing_email"]["value"]).
				//Invoice Information
				"&x_invoice_num=".urlencode($this->common_vars["order_id"]["value"]);

			//shipping data
			if ($this->common_vars['shipping_required']['value'])
			{
				$shipping_name_parts = explode(' ', trim($this->common_vars["shipping_name"]["value"]));
				$shipping_name_first = trim($shipping_name_parts[0]);
				unset($shipping_name_parts[0]);
				$shipping_name_last = trim(implode(' ', $shipping_name_parts));

				$post_data = $post_data.
					//Customer Shipping Address
					"&x_ship_to_first_name=".urlencode($shipping_name_first).
					"&x_ship_to_last_name=".urlencode($shipping_name_last).
					"&x_ship_to_address=".urlencode($this->common_vars["shipping_address"]["value"]).
					"&x_ship_to_city=".urlencode($this->common_vars["shipping_city"]["value"]).
					"&x_ship_to_state=".urlencode($this->common_vars["shipping_state"]["value"]).
					"&x_ship_to_zip=".urlencode($this->common_vars["shipping_zip"]["value"]).
					"&x_ship_to_country=".urlencode($this->common_vars["shipping_country"]["value"]);
			}

			$post_data = $post_data.
				//Transaction Data
				"&x_amount=".urlencode(number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")).
				//"&x_currency_code=USD".
				"&x_method=".urlencode("CC").
				"&x_type=".urlencode($this->settings_vars[$this->id."_Auth_Type"]["value"] == "Auth-Capture" ? "AUTH_CAPTURE" : "AUTH_ONLY").
				"&x_card_num=".urlencode($post_form["cc_number"]).
				"&x_card_code=".urlencode($post_form["cc_cvv2"]).
				"&x_exp_date=".urlencode($post_form["cc_expiration_month"].substr($post_form["cc_expiration_year"], 2, 2)).
				//Level 2 Data
				"&x_po_num=".urlencode($this->common_vars["order_id"]["value"]).
				"&x_tax=".urlencode(number_format($this->common_vars["order_tax_amount"]["value"], 2, ".", "")).
				"&x_freight=".urlencode(number_format($this->common_vars["order_shipping_amount"]["value"], 2, ".", "")).
				"&x_solution_id=".urlencode("AAA170440");

			if ($this->isCardinalCommerceEnabled($post_form['cc_number']))
			{
				if ($x_authentication_indicator != "" && $x_cardholder_authentication_value != "")
				{
					$post_data.=
						"&x_authentication_indicator=".urlencode($x_authentication_indicator).
						"&x_cardholder_authentication_value=".urlencode($x_cardholder_authentication_value);
				}
			}

			// post data to authorize.net
			$_post_url = trim($this->url_to_gateway)==""?"https://cardpresent.authorize.net/gateway/transact.dll":trim($this->url_to_gateway);

			$c = curl_init($_post_url);

			if ($settings["ProxyAvailable"] == "YES")
			{
				//curl_setopt($c, CURLOPT_VERBOSE, 1);
				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}

				curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
				
				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}

				curl_setopt($c, CURLOPT_TIMEOUT, 120);
			}

			curl_setopt($c, CURLOPT_VERBOSE, 0);
			curl_setopt($c, CURLOPT_HEADER, 0);
			curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

			@set_time_limit(3000);

			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

			$buffer = curl_exec($c);

			$this->log("processAuthorize Request:\n".$_post_url."\n".preg_replace(array('/x\_card\_num\=(\d+)/','/x\_card\_code\=(\d+)/'), array('x_card_num=XXXXXXXXXXXXXXXX', 'x_card_code=XXX'), $post_data));
			$this->log("processAuthorize Response:\n".$buffer);

			if ($buffer)
			{
				$data = explode("~", $buffer);
				// Approved
				if ($data[1] == "1" || $data[1] == "4")
				{
					$this->createTransaction($db, $user, $order, $data, '', '', '', '', $data[9]);

					if ($this->support_ccs && $this->enable_ccs)
					{
						$card_data = array(
							"fn"=>$post_form["cc_first_name"],
							"ln"=>$post_form["cc_last_name"],
							"ct"=>$post_form["cc_type"],
							"cc"=>$post_form["cc_number"],
							"em"=>$post_form["cc_expiration_month"], 
							"ey"=>$post_form["cc_expiration_year"]
						);
						$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]), "", "", "", "", $data[6]);
					}
					$this->is_error = false;
					$this->error_message = "";

					return $data[0] == "1" ? "Received" : "Pending";
				}
				else
				{
					$this->is_error = true;

					switch ($data[1])
					{
						case "2" : 
						{
							$this->error_message = "We are sorry, but this transaction has been declined.<br>Payment gateway response: ".$data[3];
							break;
						}
						case "3" : 
						{
							$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: ".$data[3];
							break;
						}
						default : 
						{
							$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
							break;
						}
					}
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $buffer, '', false);
			}

			return !$this->is_error;
		}

		//main payment module entry point
		function process($db, $user, $order, $post_form)
		{
			global $settings, $backurl;

			//check cardinal centinel is enabled
			if ($this->isCardinalCommerceEnabled())
			{
				//create object and set it up
				$cardinal = new CardinalCommerce();
				$cardinal->common_vars = $this->common_vars;

				if (isset($_REQUEST["do_cmpi_lookup"]))
				{
					$cardinal_lookup_xml = $cardinal->getLookupXML(
						$post_form["cc_first_name"],
						$post_form["cc_last_name"],	
						$post_form["cc_number"], 
						$post_form["cc_expiration_month"], 
						$post_form["cc_expiration_year"],
						$this->currency["code"]
					);

					$cardinal_lookup_result = $cardinal->processRequest($cardinal_lookup_xml);
					if ($cardinal_lookup_result)
					{
						if ($cardinal_lookup_result["CardinalMPI"][0]["Enrolled"][0] == "Y")
						{
							$this->redirect = true;
							$this->redirectURL = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=payment_validation";

							$_SESSION["CardinalPostForm"] = $post_form;
							$_SESSION["CardinalTransactionId"] = $cardinal_lookup_result["CardinalMPI"][0]["TransactionId"][0];
							$_SESSION["CardinalACSUrl"] = $cardinal_lookup_result["CardinalMPI"][0]["ACSUrl"][0];
							$_SESSION["CardinalPayload"] = $cardinal_lookup_result["CardinalMPI"][0]["Payload"][0];
							//validator will replace ORDER_PROCESS_PAYMENT_VALIDATION with ORDER_PROCESS_PAYMENT
							$_SESSION["CardinalNotifyUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT_VALIDATION."&do_cmpi_authenticate=true";
							$_SESSION["CardinalRedirectUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"];
							return false;
						}
						else
						{
							//do not redirect, process simple authorize transaction
							$this->processAuthorize($db, $user, $order, $post_form);
						}
					}
					else
					{
						if ($cardinal->is_fatal_error)
						{
							$this->is_error = true;
							$this->error_message = $cardinal->error_message;
						}
						else
						{
							$this->processAuthorize($db, $user, $order, $post_form);
						}
					}
				}

				if (isset($_REQUEST["do_cmpi_authenticate"]) && isset($_SESSION["CardinalPostForm"]) && isset($_REQUEST["PaRes"]))
				{
					$post_form = $_SESSION["CardinalPostForm"];

					unset($_SESSION["CardinalPostForm"]);

					$cardinal_auth_xml = $cardinal->getAuthenticateXML($_REQUEST["PaRes"]);
					$cardinal_auth_result = $cardinal->processRequest($cardinal_auth_xml);

					if ($cardinal_auth_result)
					{
						$EciFlag = $cardinal_auth_result["CardinalMPI"][0]["EciFlag"][0];
						$PAResStatus = $cardinal_auth_result["CardinalMPI"][0]["PAResStatus"][0];
						$Cavv = $cardinal_auth_result["CardinalMPI"][0]["Cavv"][0];
						$SignatureVerification = $cardinal_auth_result["CardinalMPI"][0]["SignatureVerification"][0];
						$Xid = $cardinal_auth_result["CardinalMPI"][0]["Xid"][0];
						$ErrorNo = $cardinal_auth_result["CardinalMPI"][0]["ErrorNo"][0];

						if ($SignatureVerification == "Y" && $PAResStatus != "N")
						{
							switch ($PAResStatus)
							{
								case "Y" : 
								case "A" : $this->processAuthorize($db, $user, $order, $post_form, $EciFlag, $Cavv); break;
								case "U" : $this->processAuthorize($db, $user, $order, $post_form); break;
							}
						}
						else 
						{
							$this->is_error = true;
							$this->error_message = "Your card can not be validated. Please try other card or other payment method";
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $cardinal->error_message;
					}
				}
			}
			else
			{
				$this->processAuthorize($db, $user, $order, $post_form);
			}
			
			return !$this->is_error;
		}
	}


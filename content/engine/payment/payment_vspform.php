<?php 

$payment_processor_id = "vspform";
$payment_processor_name = "VSP Form";
$payment_processor_class = "PAYMENT_VSPFORM";
$payment_processor_type = "ipn";

class PAYMENT_VSPFORM extends PAYMENT_PROCESSOR
{
	function PAYMENT_VSPFORM()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "vspform";
		$this->name = "VSP Form";
		$this->class_name = "PAYMENT_VSPFORM";
		$this->type = "ipn";
		$this->description = "";
		$this->post_url = "https://test.sagepay.com/Simulator/VSPFormGateway.asp";
		$this->steps = 2;

		return $this;
	}

	function getToken($thisString)
	{
		// List the possible tokens
		$Tokens = array(
			"Status",
			"StatusDetail",
			"VendorTxCode",
			"VPSTxId",
			"TxAuthNo",
			"Amount",
			"AVSCV2",
			"AddressResult",
			"PostCodeResult",
			"CV2Result",
			"GiftAid",
			"3DSecureStatus",
			"CAVV"
		);
		// Initialise arrays
		$output = array();
		$resultArray = array();

		// Get the next token in the sequence
		for ($i = count($Tokens)-1; $i >= 0 ; $i--)
		{
			// Find the position in the string
			$start = strpos($thisString, $Tokens[$i]);
			// If it's present
			if ($start !== false)
			{
				// Record position and token name
				$resultArray[$i]->start = $start;
				$resultArray[$i]->token = $Tokens[$i];
			}
		}

		// Sort in order of position
		sort($resultArray);

		// Go through the result array, getting the token values
		for ($i = 0; $i<count($resultArray); $i++)
		{
			// Get the start point of the value
			$valueStart = $resultArray[$i]->start + strlen($resultArray[$i]->token) + 1;
			// Get the length of the value
			if ($i==(count($resultArray)-1))
			{
				$output[$resultArray[$i]->token] = substr($thisString, $valueStart);
			}
			else
			{
				$valueLength = $resultArray[$i+1]->start - $resultArray[$i]->start - strlen($resultArray[$i]->token) - 2;
				$output[$resultArray[$i]->token] = substr($thisString, $valueStart, $valueLength);
			}
		}
		// Return the ouput array
		return $output;
	}

	//redeclare getPaymentForm
	function getPaymentForm($db)
	{
		global $settings, $order;

		$_pf = parent::getPaymentForm($db);

		$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;
		$return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;

		$crypt =
			"VendorTxCode=".$this->common_vars["order_id"]["value"]."&".
			"Amount=".number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "")."&".
			"Currency=".$this->settings_vars[$this->id."_Currency"]["value"]."&".
			"Description=Order ".$this->common_vars["order_id"]["value"]."&".
			"SuccessURL=".$notify_url."&".
			"FailureURL=".$return_url."&".
			"CustomerName=".$this->common_vars["billing_first_name"]["value"]."&".
			"CustomerEMail=".$this->common_vars["billing_email"]["value"]."&".
			"VendorEMail=".$settings["GlobalNotificationEmail"]."&".
			//"eMailMessage=".$this->common_vars["order_id"]["value"]."&".
			"BillingSurname=".($this->common_vars["billing_last_name"]["value"])."&".
			"BillingFirstnames=".($this->common_vars["billing_first_name"]["value"])."&".
			"BillingAddress1=".($this->common_vars["billing_address1"]["value"])."&".
			"BillingAddress2=".($this->common_vars["billing_address2"]["value"])."&".
			"BillingCity=".($this->common_vars["billing_city"]["value"])."&".
			"BillingPostCode=".($this->common_vars["billing_zip"]["value"])."&".
			"BillingCountry=".$this->common_vars["billing_country_iso_a2"]["value"]."&".
			(($this->common_vars["billing_country_iso_a2"]["value"] != "US")?"":("BillingState=".$this->common_vars["billing_state_abbr"]["value"]."&")).
			"BillingPhone=".($this->common_vars["billing_phone"]["value"])."&".

			"DeliverySurname=".($this->common_vars["shipping_last_name"]["value"])."&".
			"DeliveryFirstnames=".($this->common_vars["shipping_first_name"]["value"])."&".
			"DeliveryAddress1=".($this->common_vars["shipping_address1"]["value"])."&".
			"DeliveryAddress2=".($this->common_vars["shipping_address2"]["value"])."&".
			"DeliveryCity=".($this->common_vars["shipping_city"]["value"])."&".
			"DeliveryPostCode=".($this->common_vars["shipping_zip"]["value"])."&".
			(($this->common_vars["shipping_country_iso_a2"]["value"] != "US")?"":("DeliveryState=".$this->common_vars["shipping_state_abbr"]["value"]."&")).
			"DeliveryCountry=".$this->common_vars["shipping_country_iso_a2"]["value"]."&".
			"ContactNumber=".($this->common_vars["billing_phone"]["value"])."&".
			"ApplyAVSCV2=".$this->settings_vars[$this->id."_ApplyAVSCV2"]["value"]."&".
			"Apply3DSecure=0";

		$fields = array();
		$fields[] = array("type"=>"hidden", "name"=>"VPSProtocol", "value"=>"3.00");
		$fields[] = array("type"=>"hidden", "name"=>"TxType", "value"=>$this->settings_vars[$this->id."_Transaction_Type"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"Vendor", "value"=>$this->settings_vars[$this->id."_Vendor"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"Crypt", "value"=>  $this->encryptAes($crypt, $this->settings_vars[$this->id."_Password"]["value"]));

		return $fields;
	}

	function getValidatorJS()
	{
		return "";
	}

	function getPostUrl()
	{
		if (trim($this->url_to_gateway) != "")
		{
			return trim($this->url_to_gateway);
		}
		else
		{
			return $this->post_url;
		}
	}

	function process($db, $user, $order, $post_form)
	{
		if (isset($_REQUEST["crypt"]))
		{
			$decoded = $this->decryptAes(str_replace(' ', '+', $_REQUEST["crypt"]), $this->settings_vars[$this->id."_Password"]["value"]);
			$values = $this->getToken($decoded);

			$this->log("Response:", $values);

			if (isset($values['Status']))
			{
				if ($values["Status"] == "OK")
				{
					$this->createTransaction($db, $user, $order, $values, '', '', '', '', isset($values['VPSTxId']) ? $values['VPSTxId'] : '');
					$this->is_error = false;
					$this->error_message = "";
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $values["StatusDetail"];
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $values, '', false);
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
		}

		return !$this->is_error;
	}

	private function encryptAes($string, $key)
	{
		// AES encryption, CBC blocking with PKCS5 padding then HEX encoding.
		// Add PKCS5 padding to the text to be encypted.
		$string = $this->addPKCS5Padding($string);

		// Perform encryption with PHP's MCRYPT module.
		$crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $string, MCRYPT_MODE_CBC, $key);

		// Perform hex encoding and return.
		return "@" . strtoupper(bin2hex($crypt));
	}

	private function addPKCS5Padding($input)
	{
		$blockSize = 16;
		$padd = "";

		// Pad input to an even block size boundary.
		$length = $blockSize - (strlen($input) % $blockSize);
		for ($i = 1; $i <= $length; $i++)
		{
			$padd .= chr($length);
		}

		return $input . $padd;
	}

	private function decryptAes($strIn, $password)
	{
		// HEX decoding then AES decryption, CBC blocking with PKCS5 padding.
		// Use initialization vector (IV) set from $str_encryption_password.
		$strInitVector = $password;

		// Remove the first char which is @ to flag this is AES encrypted and HEX decoding.
		$hex = substr($strIn, 1);

		// Throw exception if string is malformed
		if (!preg_match('/^[0-9a-fA-F]+$/', $hex))
		{
			throw new Exception('Invalid encryption string');
		}
		$strIn = pack('H*', $hex);

		// Perform decryption with PHP's MCRYPT module.
		$string = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $password, $strIn, MCRYPT_MODE_CBC, $strInitVector);
		return $this->removePKCS5Padding($string);
	}

	private function removePKCS5Padding($input)
	{
		$blockSize = 16;
		$padChar = ord($input[strlen($input) - 1]);

		/* Check for PadChar is less then Block size */
		if ($padChar > $blockSize)
		{
			throw new Exception('Invalid encryption string');
		}
		/* Check by padding by character mask */
		if (strspn($input, chr($padChar), strlen($input) - $padChar) != $padChar)
		{
			throw new Exception('Invalid encryption string');
		}

		$unpadded = substr($input, 0, (-1) * $padChar);
		/* Chech result for printable characters */
		if (preg_match('/[[:^print:]]/', $unpadded))
		{
			throw new Exception('Invalid encryption string');
		}
		return $unpadded;
	}
}

<?php 
	$payment_processor_id = "cardinalcommerce";
	$payment_processor_name = "CardinalCommerce.com";
	$payment_processor_class = "PAYMENT_CARDINALCOMMERCE";
	$payment_processor_type = "cc";
	
	class PAYMENT_CARDINALCOMMERCE extends PAYMENT_PROCESSOR
	{
		function PAYMENT_CARDINALCOMMERCE()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "cardinalcommerce";
			$this->name = "CardinalCommerce.com";
			$this->class_name = "PAYMENT_CARDINALCOMMERCE";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->is_certificate = false;
			return $this;
		}
		
		//////////////////////////////////////////////////////
		// send data to gateway, get xml response and parse it 
		function processRequest($_post_url, $xml)
		{
			global $settings;
			$ch = curl_init($_post_url);
			
			if ($settings["ProxyAvailable"] == "YES")
			{
				//curl_setopt($ch, CURLOPT_VERBOSE, 1);
				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($ch, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}
				
				curl_setopt($ch, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
				
				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($ch, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}

				curl_setopt($ch, CURLOPT_TIMEOUT, 120);
			}
			
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "cmpi_msg=".$xml);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

			curl_setopt($ch, CURLOPT_TIMEOUT, 300);
			
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);

			curl_setopt($ch, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($ch, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
			
			$result = curl_exec($ch);
			
			$this->log("processRequest Request:\n".$_post_url."\n".$xml);
			$this->log("processRequest Response:\n".$result);
						
			if (curl_errno($ch) > 0)
			{
				$this->is_error = true;
				$this->error_message = curl_error($ch);
			}
			elseif ($result == "")
			{
				$this->is_error = true;
				$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
			}
			else
			{
				$xmlParser = new XMLParser();
				$parsed = $xmlParser->parse($result);
				
				if ($parsed["CardinalMPI"][0]["ErrorNo"][0] != "0")
				{
					$this->is_error = true;
					$this->error_message = "Paymen gateway response: ".$parsed["CardinalMPI"][0]["ErrorDesc"][0];
				}
				else
				{
					return $parsed;
				}
			}
			return false;
		}
		
		////////////////////////////////////////////////////////
		// process cmpi_lookup 
		function getLookupXML($post_form)
		{
			$xml = 
				"<CardinalMPI>".
					"<MsgType>cmpi_lookup</MsgType>".
					"<Version>1.6</Version>".
					"<ProcessorId>".utf8_encode($this->settings_vars[$this->id."_Processor_Id"]["value"])."</ProcessorId>".
					"<MerchantId>".utf8_encode($this->settings_vars[$this->id."_Merchant_Id"]["value"])."</MerchantId>".
					"<TransactionPwd>".utf8_encode($this->settings_vars[$this->id."_Password"]["value"])."</TransactionPwd>".
					"<TransactionType>C</TransactionType>".
					"<Amount>".utf8_encode(number_format($this->common_vars["order_total_amount"]["value"], 2, "", ""))."</Amount>".						
					"<CurrencyCode>840</CurrencyCode>".
					"<CardNumber>".utf8_encode($post_form["cc_number"])."</CardNumber>".
					"<CardExpMonth>".utf8_encode($post_form["cc_expiration_month"])."</CardExpMonth>".
					"<CardExpYear>".utf8_encode($post_form["cc_expiration_year"])."</CardExpYear>".
					"<OrderNumber>".utf8_encode($this->common_vars["order_id"]["value"])."</OrderNumber>".
					"<OrderDesc>".utf8_encode("Order ".$this->common_vars["order_id"]["value"])."</OrderDesc>".
					"<UserAgent>".utf8_encode($_SERVER["HTTP_USER_AGENT"])."</UserAgent>".
					"<BrowserHeader>".utf8_encode($_SERVER["HTTP_ACCEPT"])."</BrowserHeader>".
					"<Recurring>N</Recurring>".
					"<EMail>".utf8_encode($this->common_vars["billing_email"]["value"])."</EMail>".
					"<IPAddress>".utf8_encode($_SERVER["REMOTE_ADDR"])."</IPAddress>".
				"</CardinalMPI>";
			return $xml;
		}
		
		////////////////////////////////////////////////////////
		// process cmpi_lookup 
		function getAuthenticateXML($PAResPayload)
		{
			$xml = 
				"<CardinalMPI>".
					"<MsgType>cmpi_authenticate</MsgType>".
					"<Version>1.6</Version>".
					"<ProcessorId>".utf8_encode($this->settings_vars[$this->id."_Processor_Id"]["value"])."</ProcessorId>".
					"<MerchantId>".utf8_encode($this->settings_vars[$this->id."_Merchant_Id"]["value"])."</MerchantId>".
					"<TransactionPwd>".utf8_encode($this->settings_vars[$this->id."_Password"]["value"])."</TransactionPwd>".
					"<TransactionType>C</TransactionType>".
					"<TransactionId>".utf8_encode($_SESSION["CardinalTransactionId"])."</TransactionId>".
					"<PAResPayload>".urlencode($PAResPayload)."</PAResPayload>".
				"</CardinalMPI>";
			return $xml;
		}
			
		/////////////////////////////////////////////////
		// process payment transaction
		function process($db, $user, $order, $post_form)
		{
			global $settings;
			$_post_url = trim($this->url_to_gateway)==""?"https://centineltest.cardinalcommerce.com/maps/txns.asp":trim($this->url_to_gateway);
			
			//get data from payment form
			if (isset($post_form["do_cmpi_lookup"]))
			{
				$lookup_xml = $this->getLookupXML($post_form);
				$lookup_result = $this->processRequest($_post_url, $lookup_xml);
				
				if ($lookup_result)
				{
					//check enrolled value 
					if (in_array($lookup_result["CardinalMPI"][0]["Enrolled"][0], array("Y", "N")))
					{
						//do redirect
						$_SESSION["CardinalTransactionId"] = $lookup_result["CardinalMPI"][0]["TransactionId"][0];
						$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=".$this->payment_page."&oa=ProcessPayment&do_cmpi_authenticate=true";
						echo 
							'<HTML>'."\n".
							'<BODY onload="document.frmLaunch.submit();">'."\n".
							'<FORM name="frmLaunch" method="POST" action="'.$lookup_result["CardinalMPI"][0]["ACSUrl"][0].'">'."\n".
							'<input type=hidden name="PaReq" value="'.$lookup_result["CardinalMPI"][0]["Payload"][0].'">'."\n".
							'<input type=hidden name="TermUrl" value="'.$notify_url.'">'."\n".
							'<input type=hidden name="MD" value="">'."\n".
							'</FORM>'."\n".
							'</BODY>'."\n".
							'</HTML>';
						die();
					}
					else
					{
						$_SESSION["CardinalTransactionId"] = $lookup_result["CardinalMPI"][0]["TransactionId"][0];
						$auth_xml = $this->getAuthenticateXML($lookup_result["CardinalMPI"][0]["Payload"][0]);
						$auth_result = $this->processRequest($_post_url, $auth_xml);
						
						if ($auth_result)
						{
							$EciFlag = $auth_result["CardinalMPI"][0]["EciFlag"][0];
							$PAResStatus = $auth_result["CardinalMPI"][0]["PAResStatus"][0];
							$SignatureVerification = $auth_result["CardinalMPI"][0]["SignatureVerification"][0];
							$Xid = $auth_result["CardinalMPI"][0]["Xid"][0];
							
							if (($PAResStatus == "Y" || $PAResStatus == "U") && $SignatureVerification == "Y")
							{
								$this->createTransaction($db, $user, $order, "", "");
								$this->is_error = false;
								$this->error_message = "";
							}
						}
					}

					if ($this->is_error)
					{
						$this->storeTransaction($db, $user, $order, $lookup_result, '', false);
					}
				}
			}
			elseif (isset($_REQUEST["do_cmpi_authenticate"]))
			{
				$auth_xml = $this->getAuthenticateXML($_REQUEST["PaRes"]);
				$auth_result = $this->processRequest($_post_url, $auth_xml);
				
				if ($auth_result)
				{
					$EciFlag = $auth_result["CardinalMPI"][0]["EciFlag"][0];
					$PAResStatus = $auth_result["CardinalMPI"][0]["PAResStatus"][0];
					$SignatureVerification = $auth_result["CardinalMPI"][0]["SignatureVerification"][0];
					$Xid = $auth_result["CardinalMPI"][0]["Xid"][0];
					
					if (($PAResStatus == "Y" || $PAResStatus == "U") && $SignatureVerification == "Y")
					{
						$this->createTransaction($db, $user, $order);
						$this->is_error = false;
						$this->error_message = "";
					}

					if ($this->is_error)
					{
						$this->storeTransaction($db, $user, $order, $auth_result, '', false);
					}
				}
			}

			return !$this->is_error;
		}
	}

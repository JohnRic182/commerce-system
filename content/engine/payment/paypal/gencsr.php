<?php

if (!defined("ENV")) exit();

function createIdentityCertificate($countryName, $stateOrProvinceName, $localityName, $organizationName, $organizationalUnitName, $commonName, $emailAddress, $foafLocation = null)
{
    global $settings;

    $serverPath = $settings['GlobalServerPath'];

	// Create the DN array for the openssl function calls
    if ($countryName)
        $dn = array("countryName" => $countryName);

    if ($stateOrProvinceName)
    {   
        if ($dn)
            $dn = array_merge($dn, 
                array("stateOrProvinceName" => $stateOrProvinceName));
        else
            $dn = array("stateOrProvinceName" => $stateOrProvinceName);
    }

    if ($localityName)
    {
        if ($dn)
            $dn = array_merge($dn, array("localityName" => $localityName));
        else
            $dn = array("localityName" => $localityName);
    }

    if ($organizationName)
    {
        if ($dn)
            $dn = array_merge($dn, array("organizationName" => $organizationName));
        else
            $dn = array("organizationName" => $organizationName);
    }

    if ($organizationalUnitName)
    {
        if ($dn)
            $dn = array_merge($dn, 
                array("organizationalUnitName" => $organizationalUnitName));
        else
            $dn = array("organizationalUnitName" => $organizationalUnitName);
    }

    if ($commonName)
    {
        if ($dn)
            $dn = array_merge($dn, array("commonName" => $commonName));
        else
            $dn = array("commonName" => $commonName);
    }

    if ($emailAddress) 
    {
        if ($dn)
            $dn = array_merge($dn, array("emailAddress" => $emailAddress));
        else
            $dn = array("emailAddress" => $emailAddress);
    }

    // if the $dn array is NULL at this point set country name to the default of GB
    if (!isset($dn))
        $dn = array("countryName" => "GB");

    // Setup the contents of the subjectAltName
    if ($foafLocation)
        $SAN="URI:$foafLocation";

    if ($emailAddress) 
    {
        if ($SAN)
            $SAN.=",email:$emailAddress";
        else
            $SAN="email:$emailAddress";
    }

    // Export the subjectAltName to be picked up by the openssl.cnf file
    if ($SAN)
    {
        putenv("SAN=$SAN");
    }

    // Create the array to hold the configuration options for the openssl function calls
    $config = array('config'=>$serverPath . '/content/ssl/openssl.cnf');
    
    if ($SAN)
    {
        // TODO - This should be more easily configured
        //$config = array_merge($config, array('x509_extensions' => 'usr_cert'));
    }

    // Generate a new private (and public) key pair
    $privkey = openssl_pkey_new($config);

    if ($privkey==FALSE) 
    {
        // Show any errors that occurred here
        while (($e = openssl_error_string()) !== false)
        {
            echo $e . "n";
            print "<br><br>";
        }
    }

    // Generate a certificate signing request
    $csr = openssl_csr_new($dn, $privkey, $config);

    if (!$csr)
    {
        // Show any errors that occurred here
        while (($e = openssl_error_string()) !== false) 
        {
            echo $e . "n";
            print "<br><br>";
        }
    }

    // You will usually want to create a self-signed certificate at this
    // point until your CA fulfills your request.
    // This creates a self-signed cert that is valid for 365 days
    $sscert = openssl_csr_sign($csr, null, $privkey, 365, $config);
    
    if ($sscert==FALSE) 
    {
        // Show any errors that occurred here
        while (($e = openssl_error_string()) !== false)
        {
            echo $e . "n";
            print "<br><br>";
        }
    }

    if (openssl_x509_export($sscert, $certout)==FALSE)
    {
        // Show any errors that occurred here
        while (($e = openssl_error_string()) !== false) 
        {
            echo $e . "n";
            print "<br><br>";
        }
    }
    $pkout = "";
    if (openssl_pkey_export($privkey, $pkout,null, $config)==FALSE)
    {
        // Show any errors that occurred here
        while (($e = openssl_error_string()) !== false) 
        {
            echo $e . "n";
            print "<br><br>";
        }
    }
    openssl_sign($certout, $signature, $privkey);
    return array("cer" => $certout, "pem" => $pkout, "signature" => $signature);
}
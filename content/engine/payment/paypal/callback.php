<?php

	if (!defined("ENV")) exit();

	$pcod = isset($_REQUEST['securityId']) ? trim($_REQUEST['securityId']) : '';

	$user = null;
	$order = null;

	$row = null;
	if ($pcod != '')
	{
		$db->query("SELECT * FROM ".DB_PREFIX."orders WHERE security_id = '".$db->escape($pcod)."' AND (status = 'New' OR status = 'Abandon') AND payment_status <> 'Received' AND payment_gateway_id in ('paypalipn', 'paypalhsau', 'paypalhsuk', 'paypalpflink', 'paypaladv')");

		if ($row = $db->moveNext())
		{
			$user = USER::getFromSession($db, $settings);
			$user->id = $row['uid'];
			$user->auth_ok = true;
			$user->getUserData();

			$order = ORDER::getFromSession($db, $settings, $user, false);
			$order->oid = $row['oid'];
		
			$order->setUserLevel($user->auth_ok ? $user->level : ($userCookie ? $userCookie["l"] : 0));

			// select order data first time before possible modification
			$order->getOrderData();
			$order->getOrderItems();

			require_once("content/engine/actions/action.order_process_payment.php");
		}
	}

	if (!is_array($row) && isset($_REQUEST['oid']))
	{
		$testMode = false;
		if (isset($_REQUEST['test_ipn']))
		{
			$testMode = $_REQUEST['test_ipn'] == '1';
		}

		if (Payment_PayPal_PayPalApi::verifyIPN($testMode))
		{
			$oid = intval($_REQUEST['oid']);
			if ($oid > 0)
			{
				$securityId = '';
				$paymentStatus = '';
				$reasonCode = '';
				$paypal_partial_capture = 0;
				if (isset($_REQUEST['parent_txn_id']))
				{
					$securityId = $_REQUEST['parent_txn_id'];
					if (isset($_REQUEST['payment_status']))
					{
						$paymentStatus = $_REQUEST['payment_status'];
					}
					if (isset($_REQUEST['reason_code']))
					{
						$reasonCode = $_REQUEST['reason_code'];
					}
					if (isset($_REQUEST['mc_gross']))
					{
						$paypal_partial_capture = $_REQUEST['mc_gross'];
						if ($paypal_partial_capture < 0)
						{
							$paypal_partial_capture = 0 - $paypal_partial_capture;
						}
					}

					$db->query("SELECT * FROM ".DB_PREFIX."orders WHERE oid = '".$oid."' AND security_id = '".$db->escape($securityId)."'");
					if ($row = $db->moveNext())
					{
						$uid = $row['uid'];
						$user = USER::getById($db, $settings, $uid);
						$order = ORDER::getById($db, $settings, $user, $oid);
						$order->getOrderData();
						$order->getOrderItems();

						$oldOrderStatus = $order->getStatus();
						$oldPaymentStatus = $order->getPaymentStatus();

						$payment_gateway = $row['payment_gateway_id'];
						
						$gateway_details = array();
						$gateway_details[0] = "cc";
						$gateway_details[1] = $payment_processor_name." : ".$paymentStatus;

						switch ($paymentStatus)
						{
							case 'Refunded':
								$refund_type = 'Partial';

								$total_charged_amount = $row['custom3'] - $paypal_partial_capture;

								$db->reset();
								if ($total_charged_amount <= 0)
								{
									$total_charged_amount = 0;

									$db->assignStr("payment_status", "Refunded");
									$db->assignStr("custom1", "refund");
								}
												
								$db->assign("custom3", $total_charged_amount);
								$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");

								$order->setPaymentStatus('Refunded');

								$response = '';
								foreach ($_REQUEST as $key=>$value)
								{
									$response.=$key." = ".$value."\n";
								}

								Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $reasonCode, $response, $paypal_partial_capture, $gateway_details);

								Events_OrderEvent::dispatchStatusChangeEvent($order, $order->getStatus(), $oldPaymentStatus);

								break;
							case 'Canceled_Reversal':						
								$db->reset();
								$db->assignStr("payment_status", "Received");
								$db->assign("custom3", $paypal_partial_capture);
								$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
								
								$response = '';
								foreach ($_REQUEST as $key=>$value)
								{
									$response.=$key." = ".$value."\n";
								}

								Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $reasonCode, $response, $paypal_partial_capture, $gateway_details);

								$order->setPaymentStatus('Received');
								Events_OrderEvent::dispatchStatusChangeEvent($order, $order->getStatus(), $oldPaymentStatus);

								break;
							case 'Completed':
								if ($row['payment_status'] == 'Pending' || $row['payment_status'] == 'Partial')
								{
									$total_charged_amount = $row['custom3'] + $paypal_partial_capture;

									$payment_status = 'Partial';
									if ((isset($_REQUEST['auth_status']) && $_REQUEST['auth_status'] == 'Completed') || $total_charged_amount >= $row['total_amount'])
									{
										$payment_status = 'Received';
									}

									$db->reset();

									$db->assignStr('payment_status', $payment_status);
									
									$db->assign("custom3", $total_charged_amount);
									$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
									
									$response = '';
									foreach ($_REQUEST as $key=>$value)
									{
										$response.=$key." = ".$value."\n";
									}

									Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $reasonCode, $response, $paypal_partial_capture, $gateway_details);

									$order->setPaymentStatus($payment_status);
									Events_OrderEvent::dispatchStatusChangeEvent($order, $order->getStatus(), $oldPaymentStatus);

								}
								break;
							case 'Expired':
								$db->reset();
								$db->assignStr("payment_status", "Auth Expired");
								$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
								
								$response = '';
								foreach ($_REQUEST as $key=>$value)
								{
									$response.=$key." = ".$value."\n";
								}

								Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $reasonCode, $response, $paypal_partial_capture, $gateway_details);

								$order->setPaymentStatus('Auth Expired');
								Events_OrderEvent::dispatchStatusChangeEvent($order, $order->getStatus(), $oldPaymentStatus);

								break;
							case 'Failed':
								$db->reset();
								$db->assignStr("payment_status", "Error");
								$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
								
								$response = '';
								foreach ($_REQUEST as $key=>$value)
								{
									$response.=$key." = ".$value."\n";
								}

								Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $reasonCode, $response, $paypal_partial_capture, $gateway_details);

								$order->setPaymentStatus('Error');
								Events_OrderEvent::dispatchStatusChangeEvent($order, $order->getStatus(), $oldPaymentStatus);

								break;
							case 'Reversed':
								$db->reset();
								$db->assignStr("payment_status", "Reversed");
								$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
								
								$response = '';
								foreach ($_REQUEST as $key=>$value)
								{
									$response.=$key." = ".$value."\n";
								}

								Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $reasonCode, $response, $paypal_partial_capture, $gateway_details);

								$order->setPaymentStatus('Reversed');
								Events_OrderEvent::dispatchStatusChangeEvent($order, $order->getStatus(), $oldPaymentStatus);

								break;
							case 'Voided':
								$db->reset();
								$db->assignStr('status', 'Canceled');
								$db->assignStr("payment_status", "Canceled");
								$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
								
								$response = '';
								foreach ($_REQUEST as $key=>$value)
								{
									$response.=$key." = ".$value."\n";
								}

								Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $reasonCode, $response, $paypal_partial_capture, $gateway_details);

								$order->setStatus('Canceled');
								$order->setPaymentStatus('Canceled');
								Events_OrderEvent::dispatchStatusChangeEvent($order, $oldOrderStatus, $oldPaymentStatus);

								break;
							case 'Pending':
								//When does this occur?
								break;
							case 'Processed':
								//When does this occur?
								break;
							case 'Created':
								//Shouldn't apply?
								break;
						}
					}
				}
			}
		}
	}

	if (is_null($row))
	{
		header("HTTP/1.1 500 Internal server error");
		echo 'No order found';
	}

	die(); // Die here, otherwise Smarty error is generated in error logs

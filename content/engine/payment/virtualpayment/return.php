<?php


	/**
	 * Change dir to cart root
	 */
	chdir("./../../../../");

	/**
	 * Load libraries used
	 */
	require_once("content/engine/engine_config.php");
	require_once("content/engine/engine_functions.php");

	/**
	 * Create database connection 
	 */
	$db = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

	$settingsRepository = new DataAccess_SettingsRepository($db);
	$settings = $settingsRepository->getAll();

	Timezone::setApplicationDefaultTimezone($db, $settings);

	require_once("content/engine/engine_label.php");

	header('Location: '.$settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?p=one_page_checkout");
	exit();
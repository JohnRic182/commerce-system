<?php
require_once("content/engine/payment/amazon/SignatureUtilsForOutbound.php");

// Details for the payment method loader.
$payment_processor_id = "amazon_sp";
$payment_processor_name = "Amazon Simple Pay";
$payment_processor_class = "PAYMENT_AMAZON_SP";
$payment_processor_type = "ipn";

// Implementation Class
class PAYMENT_AMAZON_SP extends PAYMENT_PROCESSOR
{
	// Constructor
	function PAYMENT_AMAZON_SP()
	{
		// Initialise base class
		parent::PAYMENT_PROCESSOR();

		// Set up integration details
		$this->id = "amazon_sp";
		$this->name = "Amazon Simple Pay";
		$this->class_name = "PAYMENT_AMAZON_SP";
		$this->type = "ipn";
		$this->description = "";
		$this->steps = 2;
		$this->testMode = false;
	}

	/**
	 * Turn an error code into a string
	 * @param $code
	 */
	function getErrorString($code)
	{
		$lookup = array(
			"PS"=>"Payment succeeded",
			"PF"=>"Payment failed",
			"RF"=>"Refund failed",
			"RS"=>"Refund succeeded",
			"ME"=>"Server error",
			"A"=>"Payment aborted",
			"SE"=>"Service error",
		);
		if(array_key_exists($code, $lookup))
			return $lookup[$code];

		return "Unknown error";
	}

	/**
	 * Generate form
	 * @param $db
	 */
	function getPaymentForm($db)
	{
		global $settings;

		// Generate the default payment form. -- Ricky26
		$_pf = parent::getPaymentForm($db);

		// Work out URLs here, keeps code clean. -- Ricky26
		$abandon_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;
		$ipn_url = $return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;

		// Generate fields for amazon IN NATURAL ABC ORDER
		// NOTE: SignatureMethod and SignatureVersion must be the first 2 params, and in this order
		$data = array();
		$data["SignatureMethod"] = "HmacSHA1";
		$data["SignatureVersion"] = 2;
		$data["abandonUrl"] = $abandon_url;
		$data["accessKey"] = $this->settings_vars[$this->id."_access_key"]["value"];
		$data["amount"] = "USD " . number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "");
		$data["description"] = $settings["GlobalSiteName"]." Order ".$this->common_vars["order_id"]["value"];
		$data["immediateReturn"] = "1";
		$data["processImmediate"] = "1";
		$data["referenceId"] = $this->common_vars["order_id"]["value"];
		$data["returnUrl"] = $return_url;

		$CanonicalizedQueryString = "";
		foreach ($data as $key=>$value)
		{
			$CanonicalizedQueryString .= ($CanonicalizedQueryString==""?"":"&").urlencode($key)."=".urlencode($value);
		}

		$StringToSign =
			"POST\n".
			($this->isTestMode() ? "authorize.payments-sandbox.amazon.com" : "authorize.payments.amazon.com")
			."\n".
			"/pba/paypipeline\n".
			$CanonicalizedQueryString;

		$data["signature"] = base64_encode($this->hashHMAC($StringToSign, $this->settings_vars[$this->id."_secret_key"]["value"]));

		$fields = array();

		foreach ($data as $key => $value)
		{
			$fields[] = array("type"=>"hidden", "name"=>$key, "value"=>$value);
		}

		return $fields;
	}

	/**
	 * Check is it a test mode
	 */
	function isTestMode()
	{
		$this->testMode = $this->settings_vars[$this->id."_test_mode"]['value'] != "No";
		return $this->testMode;
	}

	/**
	 * Get JS code for form validation
	 */
	function getValidatorJS()
	{
		return "return true;";
	}

	/**
	 * Get the url to forward to.
	 */
	function getPostUrl()
	{
		if ($this->url_to_gateway != "")
		{
			$this->post_url = trim($this->url_to_gateway);
		}
		elseif ($this->isTestMode())
		{
			$this->post_url = "https://authorize.payments-sandbox.amazon.com/pba/paypipeline";
		}
		else
		{
			$this->post_url = "https://authorize.payments.amazon.com/pba/paypipeline";
		}

		return $this->post_url;
	}

	// Process the result from the AWS.
	function process($db, $user, $order, $post_form)
	{
		global $settings;
		$_DATA = $_GET;

		$signatureVersion = isset($_DATA["signatureVersion"]) ? $_DATA["signatureVersion"] : false;
		$signatureMethod = isset($_DATA["signatureMethod"]) ? $_DATA["signatureMethod"] : false;
		$certificateUrl = isset($_DATA["certificateUrl"]) ? $_DATA["certificateUrl"] : false;
		$signature = isset($_DATA["signature"]) ? $_DATA["signature"] : false;
		$transactionId = isset($_DATA['transactionId']) ? $_DATA['transactionId'] : '';

		if ($signatureVersion == 2 && $signatureMethod == "RSA-SHA1" && $certificateUrl && $signature)
		{
			$utils = new SignatureUtilsForOutbound();

			$urlEndPoint = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;

			$this->log("DATA Sent to gateyway:", $_DATA);

			$verificationResult = $utils->validateRequest($_DATA, $urlEndPoint, "GET");

			$this->log("DATA Returned back:", $_DATA);

			if ($verificationResult)
			{
				if ($_DATA["status"] == "PS")
				{
					$this->createTransaction($db, $user, $order, $_DATA, '', '', '', '', $transactionId);
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $this->getErrorString($_DATA["status"]);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Cannot verificate request";
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = "One or more parameters missing";
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $_DATA, '', false);
		}

		return !$this->is_error;
	}

	// Generate a HMAC hash.
	function hashHMAC($data, $key)
	{
		// Convert + to %20 for the correct signature
		$sig = hash_hmac("sha1", str_replace('+', '%20', $data), $key, TRUE);
		return $sig;
	}
}

<?php 
$payment_processor_id = "2checkout";
$payment_processor_name = "2CheckOut.com";
$payment_processor_class = "PAYMENT_2CHECKOUT";
$payment_processor_type = "ipn";

class PAYMENT_2CHECKOUT extends PAYMENT_PROCESSOR
{
	/**
	 * Class constructor
	 */
	public function PAYMENT_2CHECKOUT()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "2checkout";
		$this->name = "2CheckOut.com";
		$this->class_name = "PAYMENT_2CHECKOUT";
		$this->type = "ipn";
		$this->description = "";
		$this->post_url = "https://www.2checkout.com/2co/buyer/purchase";
		$this->steps = 2;
		$this->testMode = false;

		return $this;
	}

	/**
	 * Get post URL
	 */
	public function getPostUrl()
	{
		if (trim($this->url_to_gateway) != "")
		{
			return trim($this->url_to_gateway);
		}
		else
		{
			return $this->post_url;
		}
	}

	/**
	 * Get payment form
	 *
	 * @param DB $db
	 * @return array
	 */
	public function getPaymentForm($db)
	{
		$_pf = parent::getPaymentForm($db);
		global $settings, $msg;

		$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;
		$return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;

		$fields = array();

		$fields[] = array("type"=>"hidden", "name"=>"x_login", "value"=>$this->settings_vars[$this->id."_sid"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_amount", "value"=>number_format($this->common_vars["order_total_amount"]["value"], 2, '.', ''));
		$fields[] = array("type"=>"hidden", "name"=>"x_invoice_num", "value"=>$this->common_vars["order_id"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"merchant_order_id", "value"=>$this->common_vars["order_id"]["value"]);

		if ($this->settings_vars[$this->id."_demo"]["value"] == "Y")
		{
			$fields[] = array("type"=>"hidden", "name"=>"demo", "value"=>$this->settings_vars[$this->id."_demo"]["value"]);
		}

		$fields[] = array("type"=>"hidden", "name"=>"return_url", "value"=>$return_url);
		$fields[] = array("type"=>"hidden", "name"=>"pay_method", "value"=>"CC");
		$fields[] = array("type"=>"hidden", "name"=>"x_first_name", "value"=>$this->common_vars["billing_first_name"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_last_name", "value"=>$this->common_vars["billing_last_name"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_address", "value"=>$this->common_vars["billing_address"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_city", "value"=>$this->common_vars["billing_city"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_state", "value"=>$this->common_vars["billing_state"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_zip", "value"=>$this->common_vars["billing_zip"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_country", "value"=>$this->common_vars["billing_country"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_email", "value"=>$this->common_vars["billing_email"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_phone", "value"=>$this->common_vars["billing_phone"]["value"]);

		$fields[] = array("type"=>"hidden", "name"=>"x_ship_to_first_name", "value"=>$this->common_vars["shipping_name"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_ship_to_last_name", "value"=>$this->common_vars["shipping_name"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_ship_to_address", "value"=>$this->common_vars["shipping_address"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_ship_to_city", "value"=>$this->common_vars["shipping_city"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_ship_to_state", "value"=>$this->common_vars["shipping_state"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_ship_to_zip", "value"=>$this->common_vars["shipping_zip"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_ship_to_country", "value"=>$this->common_vars["shipping_country"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"x_receipt_link_url", "value"=>$notify_url);
		$fields[] = array("type"=>"hidden", "name"=>"skip_landing", "value"=>"1");

		//add products by new requirements
		//https://support.2co.com/deskpro/faq.php?do=article&articleid=357
		$db->query("SELECT * FROM ".DB_PREFIX."orders_content WHERE oid='".$this->common_vars["order_oid"]["value"]."'");
		$i = 1;

		while ($db->moveNext())
		{
			$fields[] = array("type"=>"hidden", "name"=>"c_prod_".$i, "value"=>$db->col["product_id"].','.$db->col["quantity"]);
			$fields[] = array("type"=>"hidden", "name"=>"c_name_".$i, "value"=>$db->col["title"]);
			$fields[] = array("type"=>"hidden", "name"=>"c_description_".$i, "value"=>$db->col["title"]);
			$fields[] = array("type"=>"hidden", "name"=>"c_price_".$i, "value"=>round($db->col["price"],2));
			$fields[] = array("type"=>"hidden", "name"=>"c_tangible_".$i, "value"=>($db->col['product_type'] == 'Tangible'?"Y":"N"));
			$i++;
		}
		$fields[] = array("type"=>"hidden", "name"=>"id_type", "value"=>"2");

		return $fields;
	}

	/**
	 * Get JS
	 */
	public function getValidatorJS()
	{
		return "";
	}

	/**
	 * Is it a test mode?
	 */
	public function isTestMode()
	{
		if ($this->settings_vars[$this->id."_demo"]["value"] == "Y")
		{
			$this->testMode = true;
		}
		else
		{
			$this->testMode = false;
		}

		return $this->testMode;
	}

	/**
	 * Process payment
	 */
	public function process($db, $user, $order, $post_form)
	{
		global $_REQUEST;

		$this->log("Processing Transaction:", $_REQUEST);

		if (array_key_exists("x_2checked", $_REQUEST) && array_key_exists("x_response_code", $_REQUEST))
		{
			//check other stuff
			if ($_REQUEST["x_2checked"] == "Y" && $_REQUEST["x_response_code"] == "1")
			{
				//(&$db, &$user, &$order, $response="", $extra="", $custom1="", $custom2="", $custom3="", $security_id="")
				$this->createTransaction($db, $user, $order, $_REQUEST, "", "", "", "", isset($_REQUEST["x_trans_id"]) ? $_REQUEST["x_trans_id"] : '');
				$this->is_error = false;
				$this->error_message = "";
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Transaction error. Please try again.";
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = "Transaction error. Please try again.";
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $_REQUEST, '', false);
		}

		return !$this->is_error;
	}
}

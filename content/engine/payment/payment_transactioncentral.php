<?php 

$payment_processor_id = "transactioncentral";
$payment_processor_name = "Credit Card (Transaction Central)";
$payment_processor_class = "PAYMENT_TRANSACTIONCENTRAL";
$payment_processor_type = "ipn";

class PAYMENT_TRANSACTIONCENTRAL extends PAYMENT_PROCESSOR
{
	function PAYMENT_TRANSACTIONCENTRAL()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "transactioncentral";
		$this->name = "Credit Card (Transaction Central)";
		$this->class_name = "PAYMENT_TRANSACTIONCENTRAL";
		$this->type = "ipn";
		$this->description = "";
		$this->testMode = false;
		$this->post_url = "https://webservices.primerchants.com/billing/TransactionCentral/processCC.asp";
		$this->steps = 2;

		return $this;
	}

	function getPaymentForm($db)
	{
		global $settings, $msg;

		$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;

		$fields = array();

		$fields[] = array("type"=>"hidden", "name"=>"MerchantID", "value"=>$this->settings_vars[$this->id."_MerchantID"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"Regkey", "value"=>$this->settings_vars[$this->id."_RegKey"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"Amount", "value"=>round($this->common_vars["order_total_amount"]["value"], 2));
		$fields[] = array("type"=>"hidden", "name"=>"REFID", "value"=>$this->common_vars["order_id"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"AVSADDR", "value"=>$this->common_vars["billing_address"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"AVSZIP", "value"=>$this->common_vars["billing_zip"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"CCRURL", "value"=>$notify_url);

		$fields[] = array("type"=>"hidden", "name"=>"oa", "value"=>ORDER_PROCESS_PAYMENT);

		$cc_exp_years = array();
		for ($i=date("Y"); $i<=date("Y")+10; $i++)
		{
			$cc_exp_years[] = array("value"=>$i, "caption"=>$i);
		}

		$cc_exp_months = array();

		for ($i=1; $i<=12; $i++)
		{
			$cc_exp_months[] = array("value"=>($i<10?"0":"").$i, "caption"=>$i);
		}

		$fields[] = array("type"=>"text", "name"=>"NameinAccount", "value"=>$this->common_vars["billing_name"]["value"], "caption"=>$msg["billing"]["cc_name"]);
		$fields[] = array("type"=>"text", "name"=>"AccountNo", "value"=>"", "caption"=>$msg["billing"]["cc_number"]);
		$fields[] = array("type"=>"select", "name"=>"CCMonth", "value"=>date("n"), "options"=>$cc_exp_months, "caption"=>$msg["billing"]["cc_expiration_month"]);
		$fields[] = array("type"=>"select", "name"=>"CCYear", "value"=>date("Y"), "options"=>$cc_exp_years, "caption"=>$msg["billing"]["cc_expiration_year"]);
		$fields[] = array("type"=>"text", "name"=>"CVV2", "value"=>"", "caption"=>$msg["billing"]["cc_cvv2"]);

		return $fields;
	}

	function getValidatorJS()
	{
		global $msg;
		return
			"\r\n".
			"if(!validateName(frm.elements['NameinAccount'].value)){\r\n".
			"	alert('".$msg["billing"]["enter_valid_cc_name"]."');\r\n".
			"	frm.elements['NameinAccount'].focus();\r\n".
			"	return false;\r\n".
			"}\r\n".
			"if(!LuhnCheck(frm.elements['AccountNo'].value) || !validateCCNum(frm.elements['AccountNo'].value)){\r\n".
			"	alert('".$msg["billing"]["enter_valid_cc_number"]."');\r\n".
			"	frm.elements['AccountNo'].focus();\r\n".
			"	return false;\r\n".
			"}\r\n".
			"if(!validateCVC2(frm.elements['CVV2'].value)){\r\n".
			"	alert('".$msg["billing"]["enter_valid_cc_cvv2"]."');\r\n".
			"	frm.elements['CVV2'].focus();\r\n".
			"	return false;\r\n".
			"}\r\n";
	}

	function process($db, $user, $order, $post_form)
	{
		$this->log("Process:", $post_form);

		if (count($post_form)>0)
		{
			if (array_key_exists("Auth", $post_form))
			{
				if ($post_form["Auth"] == "" || $post_form["Auth"] == "Declined" || $post_form["RefNo"] != $order->order_num)
				{
					$this->is_error = true;
					$this->error_message = !empty($post_form["Notes"])?$post_form["Notes"]:"Transaction Declined";
				}
				else
				{
					$temp = explode("/",getenv("HTTP_REFERER"));
					$referer = $temp[2];

					if ($referer=="")
					{
						$referer = $_SERVER['HTTP_REFERER'];
						list($remove, $stuff) = explode('//',$referer,2);
						list($home, $stuff) = explode('/',$stuff,2);
						$referer = $home;
					}

					$response = "Referer:".$referer." (webservices.primerchants.com?)\r\n";

					foreach ($post_form as $var_name=>$var_value)
					{
						$response.=$var_name." = ".$var_value."\r\n";
					}

					$this->createTransaction($db, $user, $order, $response, "");
					$this->is_error = false;
					$this->error_message = "";
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Server does not return required data";
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = "Error processing transaction";
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $post_form, '', false);
		}

		return !$this->is_error;
	}
}

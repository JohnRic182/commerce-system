<?php

class orbital_client
{
	private $urls = array(
			'production' => array(
				'https://orbital1.paymentech.net/authorize',
				'https://orbital2.paymentech.net/authorize'
			),
			'certification' => array(
				'https://orbitalvar1.paymentech.net/authorize',
				'https://orbitalvar2.paymentech.net/authorize'
			)
		);

	private $currenciesExponents = array(
			'012'=>2, '422'=>2, '032'=>2, '440'=>2, '051'=>2, '446'=>2, '533'=>2, '450'=>0, '036'=>2, '454'=>2,
			'031'=>2, '458'=>2, '044'=>2, '462'=>2, '050'=>2, '478'=>2, '052'=>2, '480'=>2, '242'=>2, '410'=>0,
			'270'=>2, '144'=>2, '981'=>2, '748'=>2, '288'=>2, '752'=>2, '292'=>2, '756'=>2, '320'=>2, '901'=>2,
			'324'=>2, '834'=>2, '624'=>2, '764'=>2, '328'=>2, '776'=>2, '332'=>2, '780'=>2, '340'=>2, '949'=>2,
			'344'=>2, '800'=>0, '348'=>2, '980'=>2, '352'=>2, '784'=>2, '356'=>2, '858'=>2, '360'=>2, '840'=>2,
			'376'=>2, '860'=>2, '388'=>2, '548'=>0, '392'=>0, '862'=>2, '398'=>2, '704'=>2, '404'=>2, '886'=>2,
			'417'=>2, '894'=>2, '418'=>0, '716'=>2, '428'=>2, '124'=>2, '978'=>2, '826'=>2
		);

	protected $test_mode;
	protected $connectionUsername;
	protected $connectionPassword;
	protected $merchantId;
	protected $bin;
	protected $terminalId;

	public function __construct($merchantId, $bin, $terminalId, $connectionUsername = false, $connectionPassword = false, $test_mode = false)
	{
		$this->merchantId = $merchantId;
		$this->bin = $bin;
		$this->terminalId = $terminalId;
		$this->connectionUsername = $connectionUsername;
		$this->connectionPassword = $connectionPassword;

		$this->test_mode = $test_mode;
	}

	public function newOrder($orderId, $totalAmount, $name,
		$address1, $address2, $city, $state, $country, $zip, $phone, $ccNumber,
		$ccExpirationDate, $cvv2, $ext_profile_id, $currencyCode, $messageType = 'A', $industryType = 'EC')
	{
		$data = array(
			'MessageType' => $messageType,
			'IndustryType' => $industryType,
			'AccountNum' => $ccNumber,
			'Exp' => $ccExpirationDate,
			'CurrencyCode' => $currencyCode,
			'CurrencyExponent' => $this->currenciesExponents[$currencyCode],
			'CardSecValInd' => $this->getCardSecValInd($ccNumber, $cvv2),
			'CardSecVal' => $cvv2,
			'AVSzip' => $zip,
			'AVSaddress1' => $address1,
			'AVSaddress2' => $address2,
			'AVScity' => $city,
			'AVSstate' => $state,
			'AVSphoneNum' => $this->cleanupPhone($phone),
			'AVSname' => $name,
			'AVScountryCode' => $country,
			'OrderID' => $orderId,
			'Amount' => $totalAmount,
			'Comments' => 'Order - '.$orderId,
		);

		if ($ext_profile_id)
		{
			$data['CustomerRefNum'] = $ext_profile_id;

			//Ensure that CC AccountNum and Exp are nulled
			$data['AccountNum'] = '';
			$data['Exp'] = '';
		}

		$values = $this->getNewOrderArray($data);

		$response = $this->call($values);

		return $response;
	}

	private function cleanupPhone($phone)
	{
		if ($phone === null)
			return '';

		return str_replace('-', '', $phone);
	}

	private function getCardSecValInd($ccNumber, $cvv2)
	{
		if ($ccNumber == '' || $ccNumber == null)
			return '';

		$ind = ($cvv2 != '' ? '1' : '');
		if ($ind == '1' && ($ccNumber[0] == '4' || $ccNumber[0] == '6'))
			return $ind;

		return '';
	}

	protected function getNewOrderArray($data = array())
	{
		$values = array(
			'OrbitalConnectionUsername' => '',
			'OrbitalConnectionPassword' => '',
			'IndustryType' => '',
			'MessageType' => '',
			'BIN' => '',
			'MerchantID' => '',
			'TerminalID' => '',
			'AccountNum' => '',
			'Exp' => '',
			'CurrencyCode' => '',
			'CurrencyExponent' => '',
			'CardSecValInd' => '',
			'CardSecVal' => '',
			'AVSzip' => '',
			'AVSaddress1' => '',
			'AVSaddress2' => '',
			'AVScity' => '',
			'AVSstate' =>'',
			'AVSphoneNum' => '',
			'AVSname' => '',
			'AVScountryCode' => '',
			'CustomerProfileFromOrderInd' => '',
			'CustomerRefNum' => '',
			'CustomerProfileOrderOverrideInd' => '',
			'Status' => '',
			'OrderID' => '',
			'Amount' => '',
			'Comments' => '',
		);

		return $this->getRequestArray($data, $values, 'NewOrder');
	}

	public function markForCapture($orderId, $capture_amount, $transaction_id)
	{
		$data = array(
			'OrderID' => $orderId,
			'Amount' => $capture_amount,
			'TxRefNum' => $transaction_id
		);
		$values = $this->getMarkForCaptureArray($data);

		$response = $this->call($values);

		return $response;
	}

	protected function getMarkForCaptureArray($data)
	{
		$values = array(
			'OrbitalConnectionUsername' => '',
			'OrbitalConnectionPassword' => '',
			'OrderID' => '',
			'Amount' => '',
			'BIN' => '',
			'MerchantID' => '',
			'TerminalID' => '',
			'TxRefNum' => ''
		);

		return $this->getRequestArray($data, $values, 'MarkForCapture');
	}

	public function reversal($orderId, $transaction_id, $amount = '')
	{
		$data = array(
			'OrderID' => $orderId,
			'AdjustedAmt' => $amount,
			'TxRefNum' => $transaction_id
		);
		$values = $this->getReversalArray($data);
		
		return $this->call($values);
	}

	protected function getReversalArray($data)
	{
		$values = array(
			'OrbitalConnectionUsername' => '',
			'OrbitalConnectionPassword' => '',
			'TxRefNum' => '',
			'AdjustedAmt' => '',
			'OrderID' => '',
			'BIN' => '',
			'MerchantID' => '',
			'TerminalID' => ''
		);
		
		return $this->getRequestArray($data, $values, 'Reversal');
	}

	public function refund($orderId, $transaction_id, $amount = '', $currencyCode, $industryType = 'EC')
	{
		$data = array(
			'MessageType' => 'R',
			'IndustryType' => $industryType,
			'CurrencyCode' => $currencyCode,
			'CurrencyExponent' => $this->currenciesExponents[$currencyCode],
			'OrderID' => $orderId,
			'Amount' => $amount,
			'AccountNum' => '',
			'TxRefNum' => $transaction_id,
		);

		$values = $this->getNewOrderArray($data);

		$response = $this->call($values);

		return $response;
	}

	public function updatePaymentProfile($customer_name, $customer_ref_num, $address1, $address2, $city,
		$state, $zip, $email, $phone, $country, $ccAccountNum, $ccExpireDate, $customerProfileAction = 'C', 
		$customerProfileFromOrderInd = 'A', $customerAccountType = 'CC')
	{
		$data = array(
			'CustomerName' => $customer_name,
			'CustomerRefNum' => $customer_ref_num,
			'CustomerAddress1' => $address1,
			'CustomerAddress2' => $address2,
			'CustomerCity' => $city,
			'CustomerState' => $state,
			'CustomerZIP' => $zip,
			'CustomerEmail' => $email,
			'CustomerPhone' => $this->cleanupPhone($phone),
			'CustomerCountryCode' => $country,
			'CustomerProfileAction' => $customerProfileAction,
			'CustomerProfileOrderOverrideInd' => 'NO',
			'CustomerProfileFromOrderInd' => $customerProfileFromOrderInd,
			'CustomerAccountType' => $customerAccountType,
			'Status' => 'A',
			'CCAccountNum' => $ccAccountNum,
			'CCExpireDate' => $ccExpireDate,
		);

		$request = $this->getPaymentProfileArray($data);

		return $this->call($request);
	}

	public function deletePaymentProfile($cust_ref_num)
	{
		$data = array(
			'CustomerRefNum' => $cust_ref_num,
			'CustomerProfileAction' => 'D'
		);

		$request = $this->getPaymentProfileArray($data);

		return $this->call($request);
	}

	public function getPaymentProfile($cust_ref_num)
	{
		$data = array(
			'CustomerRefNum' => $cust_ref_num,
			'CustomerProfileAction' => 'R',
		);

		$request = $this->getPaymentProfileArray($data);

		return $this->call($request);
	}

	protected function getPaymentProfileArray($data)
	{
		$values = array(
			'OrbitalConnectionUsername' => '',
			'OrbitalConnectionPassword' => '',
			'CustomerBin' => '',
			'CustomerMerchantID' => '',
			'CustomerName' => '',
			'CustomerRefNum' => '',
			'CustomerAddress1' => '',
			'CustomerAddress2' => '',
			'CustomerCity' => '',
			'CustomerState' => '',
			'CustomerZIP' => '',
			'CustomerEmail' => '',
			'CustomerPhone' => '',
			'CustomerCountryCode' => '',
			'CustomerProfileAction' => '',
			'CustomerProfileOrderOverrideInd' => '',
			'CustomerProfileFromOrderInd' => '',
			'CustomerAccountType' => '',
			'Status' => '',
			'CCAccountNum' => '',
			'CCExpireDate' => '',
		);

		return $this->getRequestArray($data, $values, 'Profile');
	}

	protected function getRequestArray($data, &$values, $requestType)
	{
		$this->setConnectionInformation($values);
		$this->merge($data, $values);

		$request = array('Request' => array(
			$requestType => $values
		));

		return $request;
	}

	protected function merge($data, &$values)
	{
		foreach ($data as $key => $value)
		{
			$values[$key] = $value;
		}
	}

	protected function setConnectionInformation(&$values)
	{
		if ($this->connectionUsername)
		{
			$values['OrbitalConnectionUsername'] = $this->connectionUsername;
			$values['OrbitalConnectionPassword'] = $this->connectionPassword;
		}
		if (isset($values['BIN']))
			$values['BIN'] = $this->bin;
		else if (isset($values['CustomerBin']))
			$values['CustomerBin'] = $this->bin;

		if (isset($values['MerchantID']))
			$values['MerchantID'] = $this->merchantId;
		else if (isset($values['CustomerMerchantID']))
			$values['CustomerMerchantID'] = $this->merchantId;

		if (isset($values['TerminalID']))
			$values['TerminalID'] = $this->terminalId;
	}

	protected function generateXml($values, &$xml)
	{
		foreach ($values as $key => $value)
		{
			if (is_array($value))
			{
				$xml .= '<'.$key.'>';
				$this->generateXml($value, $xml);
				$xml .= '</'.$key.'>';
			}
			else if (($key == 'Amount' && $value === '0') || $key == 'CCAccountNum' || $key == 'AccountNum' || $value != '')
			{
				if ($value == '')
				{
					$xml .= '<'.$key.'/>';
				}
				else
				{
					$xml .= '<'.$key.'>';
					$xml .= utf8_encode($value);
					$xml .= '</'.$key.'>';
				}
			}
		}
	}

	protected function call($values)
	{
		$xml = '<?xml version="1.0" encoding="UTF-8"?>';
		$this->generateXml($values, $xml);

		$this->log('Request:', $values);

		$url = $this->urls['production'][0];
		if ($this->test_mode)
			$url = $this->urls['certification'][0];

		$c = curl_init($url);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_HTTPHEADER, array(
			'MIME-Header: POST/AUTHORIZE HTTP/1.1',
			'MIME-Version: 1.1',
			'Content-type: application/PTI51',
			'Content-length: '.strlen($xml),
			'Content-transfer-encoding: text',
			'Request-number: 1',
			'Document-type: Request',
			//'Trace-number: '.$traceNumber,
			'Interface-Version: 1.0'
		));
		curl_setopt($c, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

		$response = curl_exec($c);

		if (curl_errno($c))
		{
			$this->is_error = true;
			$this->error_message = curl_error($c);
		}
		else
		{
			curl_close($c);

			$respArray = $this->xml2array($response);

			//Return inner response array
			$respArray = array_values($respArray);

			return $respArray[0];
		}
		return false;
	}

	protected function replaceDataKeys()
	{
		return array('Request.NewOrder.AccountNum', 'Request.NewOrder.OrbitalConnectionUsername',
			'Request.NewOrder.OrbitalConnectionPassword', 'Request.NewOrder.BIN', 'Request.NewOrder.MerchantID',
			'Request.NewOrder.CardSecValInd', 'Request.NewOrder.CardSecVal', 'Request.MarkForCapture.OrbitalConnectionUsername',
			'Request.MarkForCapture.OrbitalConnectionPassword', 'Request.MarkForCapture.BIN', 'Request.MarkForCapture.MerchantID',
			'Request.Reversal.OrbitalConnectionUsername', 'Request.Reversal.OrbitalConnectionPassword', 'Request.Reversal.BIN',
			'Request.Reversal.MerchantID', 'Request.Profile.CCAccountNum', 'Request.Profile.OrbitalConnectionUsername',
			'Request.Profile.OrbitalConnectionPassword', 'Request.Profile.CustomerBin', 'Request.Profile.CustomerMerchantID',
		);
	}

	protected function cleanData(array &$values)
	{
		foreach ($this->replaceDataKeys() as $key)
		{
			$parts = explode('.', $key);

			$s_path = &$values;
			for ($i = 0; $i < count($parts); $i++)
			{
				$part = $parts[$i];
				if (isset($s_path[$part]))
				{
					if (is_array($s_path[$part]))
					{
						$s_path = &$s_path[$part];
					}
					else if ($i + 1 >= count($parts))
					{
						$s_path[$part] = str_repeat('X', strlen($s_path[$part]));
					}
				}
			}
		}
	}

	/**
	 * Writer message into log
	 * @param string $s
	 */
	protected function log($s, array $values = array())
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			if ($values)
			{
				$this->cleanData($values);
			}

			$logFilePath = 'content/cache/log';

			file_put_contents(
				$logFilePath."/payment-orbital.txt",
				date("Y/m/d-H:i:s")." - orbital\n".(is_array($s) ? array2text($s) : $s).(count($values) > 0 ? "\n".array2text($values) : '')."\n\n\n",
				FILE_APPEND
			);
		}
	}

	private function xml2array($input, $callback = null, $recurse = false)
	{
		// Get input, loading an xml string with simplexml if its the top level of recursion
		$data = ((!$recurse) && is_string($input))? simplexml_load_string($input): $input;
		// Convert SimpleXMLElements to array
		if ($data instanceof SimpleXMLElement || $data instanceof stdClass)
		{
			$data = (array) $data;
			if (count($data) == 0) $data = '';
		}

		// Recurse into arrays
		if (is_array($data)) foreach ($data as &$item) $item = $this->xml2array($item, $callback, true);

		// Run callback and return
		return (!is_array($data) && is_callable($callback))? call_user_func($callback, $data): $data;
	}
}
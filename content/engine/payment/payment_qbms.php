<?php 
	$payment_processor_id = "qbms";
	$payment_processor_name = "QuickBooks Merchant Services";
	$payment_processor_class = "PAYMENT_QBMS";
	$payment_processor_type = "cc";

	class PAYMENT_QBMS extends PAYMENT_PROCESSOR
	{
		function PAYMENT_QBMS()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "qbms";
			$this->name = "QuickBooks Merchant Services";
			$this->class_name = "PAYMENT_QBMS";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->need_cc_codes = false;
			$this->support_ccs = false;
			return $this;
		}

		public function replaceDataKeys()
		{
			return array('QBMSXMLMsgsRq.CustomerCreditCardAuthRq.CreditCardNumber', 'QBMSXMLMsgsRq.CustomerCreditCardAuthRq.CardSecurityCode', 'QBMSXMLMsgsRq.CustomerCreditCardChargeRq.CreditCardNumber', 'QBMSXMLMsgsRq.CustomerCreditCardChargeRq.CardSecurityCode');
		}

		function isTestMode()
		{
			static $testMode = null;

			if (!is_null($testMode)) return $testMode;

			$testMode = ($this->settings_vars[$this->id."_test_mode"]["value"] == "On");
			$this->testMode = $testMode;

			return $this->testMode;
		}

		// process transaction

		function dispatch($transactionType, $postVars)
		{
			global $settings;

			// production vars

			$applicationLogin = $this->settings_vars[$this->id."_app_login"]["value"];
			$appId = $this->settings_vars[$this->id."_app_id"]["value"];

			// If not set from admin, allow defined settings to override
			if (trim($applicationLogin) == '' && trim($appId) == '' &&
			    defined('QBMS_APP_LOGIN') && trim(QBMS_APP_LOGIN) != '' && 
			    defined('QBMS_APP_ID') && trim(QBMS_APP_ID) != '')
			{
				$applicationLogin = QBMS_APP_LOGIN;
				$appId = QBMS_APP_ID;
			}

			$connectionTicket = $this->settings_vars[$this->id."_connection_ticket"]["value"];

			$post_url = $this->isTestMode() ? 'https://merchantaccount.ptc.quickbooks.com/j/AppGateway' : 'https://merchantaccount.quickbooks.com/j/AppGateway';

			$this->log('Connection Url: '.$post_url);

			// create unique transaction request id
			$transactionRequestId = md5(rand(1,9999).mktime());

			$input_array = array();
			switch ($transactionType)
			{
				case 'CustomerCreditCardAuth':
				case 'CustomerCreditCardCharge':

					if (mktime(0,0,0,date('m'),1,date('y')) > mktime(0,0,0,$postVars['cc_expiration_month'],1,$postVars['cc_expiration_year']))
					{
						$this->error_message = 'This credit card has expired. Please check the expiration date or use a different credit card.';
						return false;
					}

					$transactionRequestXml = '
						<CreditCardNumber>'.substr($postVars["cc_number"], 0, 19).'</CreditCardNumber>
						<ExpirationMonth>'.substr($postVars['cc_expiration_month'], 0, 12).'</ExpirationMonth>
						<ExpirationYear>'.$postVars['cc_expiration_year'].'</ExpirationYear>
						<IsECommerce>1</IsECommerce>
						<Amount>'.$this->common_vars["order_total_amount"]["value"].'</Amount>
						<NameOnCard>'.substr($postVars["cc_first_name"].' '.$postVars["cc_last_name"], 0, 30).'</NameOnCard>
						<CreditCardAddress>'.substr($this->common_vars["billing_address1"]["value"].' '.$this->common_vars["billing_address2"]["value"], 0, 30).'</CreditCardAddress>
						<CreditCardPostalCode>'.substr(str_replace('-', '', $this->common_vars["billing_zip"]["value"]), 0, 9).'</CreditCardPostalCode>
						<SalesTaxAmount>'.$this->common_vars["order_tax_amount"]["value"].'</SalesTaxAmount>
						<CardSecurityCode>'.substr($postVars['cc_cvv2'], 0, 4).'</CardSecurityCode>';
					break;

				case 'CustomerCreditCardTxnVoid':
				case 'CustomerCreditCardCapture':
					$transactionRequestXml = '
						<CreditCardTransID>'.$postVars['transaction_id'].'</CreditCardTransID>
					';
					break;
				// not passing valid transactionType
				default: 
					return false;
			}

			$xml = '<?xml version="1.0"?>
				<?qbmsxml version="2.0"?>
				<QBMSXML>
					<SignonMsgsRq>
						<SignonDesktopRq>
							<ClientDateTime>'.date('Y-m-d\TH:i:s').'</ClientDateTime>
							<ApplicationLogin>'.$applicationLogin.'</ApplicationLogin>
							<ConnectionTicket>'.$connectionTicket.'</ConnectionTicket>
							<Language>English</Language>
							<AppID>'.$appId.'</AppID>
							<AppVer>1.0</AppVer>
						</SignonDesktopRq>
					</SignonMsgsRq>
					<QBMSXMLMsgsRq>
						<'.$transactionType.'Rq>
							<TransRequestID>'.$transactionRequestId.'</TransRequestID>
							'.$transactionRequestXml.'
						</'.$transactionType.'Rq>
					</QBMSXMLMsgsRq>
				</QBMSXML>
			';

			// initialize curl
			$ch = curl_init();

			$headers = array();
			$headers[] = "Content-type: application/x-qbmsxml";
			$headers[] = "Content-length: ".strlen($xml);

			curl_setopt($ch, CURLOPT_TIMEOUT, 500);
			curl_setopt($ch, CURLOPT_URL, $post_url);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			@set_time_limit(3000);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($ch, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
			$buffer = curl_exec($ch);

			$requestData = xml2array($xml);
			$this->cleanData($requestData);
			$this->log($transactionType." Request:", $requestData);

			if ($buffer)
			{
				$response = xml2array($buffer);
				$responseTemp = $response;
				$this->cleanData($requestData);

				$this->log($transactionType." Response:", $responseTemp);

				return $response;
			}

			return false;
		}

		/**
		 * Process user's payment
		 * @param $db
		 * @param $user
		 * @param $order
		 * @param $post_form
		 * @return unknown_type
		 */
		function process($db, $user, $order, $post_form)
		{
			global $settings;

			if (isset($post_form) && is_array($post_form))
			{
				// get the correct transaction request type
				switch ($this->settings_vars[$this->id.'_transaction_type']['value'])
				{
					case 'Authorize Only': $transactionType = 'CustomerCreditCardAuth'; break;
					case 'Authorize and Capture': $transactionType = 'CustomerCreditCardCharge'; break;
				}

				$data = $this->dispatch($transactionType, $post_form);

				/**
				 * Data received, parse it
				 */
				if ($data)
				{
					$rsSignonMsgs = $data['SignonMsgsRs']['SignonDesktopRs']['@attributes'];

					if ($rsSignonMsgs['statusCode'] > 0)
					{
						if ($this->isTestMode())
						{
							switch($rsSignonMsgs['statusCode'])
							{
								case '2000': $this->error_message = 'Authentication failed -- Invalid Connection Ticket'; break;
								case '2010': $this->error_message = 'Session Authentication Required'; break;
								case '2030': $this->error_message = 'Unsupported Signon Version'; break;
								case '2040': $this->error_message = 'Internal Error'; break;
							}
						}
						else 
						{
							$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
						}

						$this->is_error = true;
						return false;
					}

					// we have some data
					$data = $data['QBMSXMLMsgsRs'];

					// preset some variables
					$rsStatusCode = $data[$transactionType.'Rs']['@attributes']['statusCode'];
					$rsStatusMessage = $data[$transactionType.'Rs']['@attributes']['statusMessage'];

					// process the order based on status code (0 = successful)
					switch ($rsStatusCode)
					{
						case '0':
						{
							$rsClientTransID = $data[$transactionType.'Rs']['ClientTransID'];
							$rsCreditCardTransId = $data[$transactionType.'Rs']['CreditCardTransID'];
							$charged_amount = $this->common_vars["order_total_amount"]["value"];

							// Create the response string
							$responseData = $data;
							$this->cleanData($responseData);

							// log the transaction
							$this->createTransaction($db, $user, $order, $responseData, $rsClientTransID, $transactionType, '', $charged_amount, $rsCreditCardTransId);

							// transaction successful, return to not go past this point
							return ($transactionType == 'CustomerCreditCardAuth') ? 'Pending' : 'Received';
						}
						// unknown error from gateway, print general text
						default: 
						{
							if ($this->isTestMode())
							{
								$this->error_message = "Code {$rsStatusCode}: {$rsStatusMessage}<br />" . PAYMENT_TRANSACTION_ERROR_TEXT;
							}
							else 
							{
								$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
							}
							break;
						}
					} // end switch

					// if script reaches this point, there was an error from the gateway
					$this->is_error = true;

					if ($this->is_error)
					{
						$this->storeTransaction($db, $user, $order, $data, '', false);
					}

					return false;
				}
				else 
				{
					// no $data set, return error
					$this->is_error = true;
					$this->error_message = ($this->error_message) ? $this->error_message : PAYMENT_SERVER_ERROR_TEXT;
				}

				if ($this->is_error)
				{
					$this->storeTransaction($db, $user, $order, $data, '', false);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Incorrect form params. Please contact site administrator.";
			}
			return !$this->is_error;
		}

		public function supportsCapture($order_data = false)
		{
			return $this->capturableAmount($order_data) > 0;
		}

		function supportsPartialCapture($order_data = false)
		{
			return false;
		}

		public function capturableAmount($order_data = false)
		{
			if ($order_data['custom1'] == 'CustomerCreditCardAuth' && $order_data['payment_status'] == 'Pending')
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'] - $this->capturedAmount($order_data);
			}

			return 0;
		}

		public function capturedAmount($order_data = false)
		{
			if ($order_data["custom1"] == "CustomerCreditCardAuth")
			{
				return $order_data["custom3"];
			}
			else if ($order_data['payment_status'] == ORDER::PAYMENT_STATUS_RECEIVED)
			{
				return $order_data['total_amount'] - $order_data['gift_cert_amount'];
			}

			return false;
		}

		public function supportsVoid($order_data = false)
		{
			return $this->supportsCapture($order_data);
		}

		public function capture($order_data = false, $capture_amount = false)
		{
			global $order, $db;

			$this->is_error = false;

			$db->query("
				SELECT ".DB_PREFIX."orders.*,
					".DB_PREFIX."countries.name AS country_name,
					".DB_PREFIX."countries.iso_a2 AS country_iso_a2,
					".DB_PREFIX."countries.iso_a3 AS country_iso_a3,
					".DB_PREFIX."countries.iso_number AS country_iso_number
				FROM ".DB_PREFIX."orders
				LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."orders.shipping_country
				WHERE
					".DB_PREFIX."orders.oid=".$order_data['oid']
			);

			$db->moveNext();
			$data = $db->col;

			// set some environment variables
			$oldPaymentStatus = $order->getPaymentStatus();

			$custom = $data['custom1'];
			$transaction_id = $data['security_id'];

			$this->is_error = false;

			$transactionType = 'CustomerCreditCardCapture';
			$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);

			$data = $this->dispatch(
				$transactionType,
				array('transaction_id' => $transaction_id)
			);

			if ($data)
			{
				$data = $data['QBMSXMLMsgsRs'];

				// preset some variables
				$rsStatusCode = $data[$transactionType.'Rs']['@attributes']['statusCode'];

				switch ($rsStatusCode)
				{
					case '0':
					{
						if ($transactionType == 'CustomerCreditCardCapture')
						{
							$rsAuthorizationCode = $data[$transactionType.'Rs']['AuthorizationCode'];
							$rsPaymentStatus = $data[$transactionType.'Rs']['PaymentStatus'];

							if ($rsPaymentStatus != 'Completed')
							{
								$this->is_error = true;
								$this->error_message = 'The transaction may have been successfully or unsuccessfully processed at the card issuer: there simply was no response (possibly due to network errors).';
							}
						}

						$rsCreditCardTransId = $data[$transactionType.'Rs']['CreditCardTransID'];

						$this->createTransaction($db, $user, $order, $data, '', $transactionType, '', '', $rsCreditCardTransId);

						// Update the orders table status
						$db->reset();
						$db->assignStr('payment_status', $order->getPaymentStatus());
						$db->update(DB_PREFIX."orders", "WHERE oid=".$order_data['oid']);

						$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

						return true;
					}
					// an error has occured
					default:
					{
						$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
						$this->storeTransaction($db, $user, $order, $data, false);
						return false;
					}
				}
			}
			else
			{
				$this->is_error = true;
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $data, '', false);
			}

			$this->error_message = 'The gateway could not process this request';

			// will return false
			return false;
		}

		public function void($order_data = false)
		{
			global $order, $db;

			$this->is_error = false;

			$db->query("
				SELECT ".DB_PREFIX."orders.*,
					".DB_PREFIX."countries.name AS country_name,
					".DB_PREFIX."countries.iso_a2 AS country_iso_a2,
					".DB_PREFIX."countries.iso_a3 AS country_iso_a3,
					".DB_PREFIX."countries.iso_number AS country_iso_number
				FROM ".DB_PREFIX."orders
				LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."orders.shipping_country
				WHERE
					".DB_PREFIX."orders.oid=".$order_data['oid']
			);

			$db->moveNext();
			$data = $db->col;

			// set some environment variables
			$oldPaymentStatus = $order->getPaymentStatus();

			$custom = $data['custom1'];
			$transaction_id = $data['security_id'];

			$this->is_error = false;

			$transactionType = 'CustomerCreditCardTxnVoid';
			$order->setPaymentStatus(ORDER::PAYMENT_STATUS_CANCELED);

			$data = $this->dispatch(
				$transactionType,
				array('transaction_id' => $transaction_id)
			);

			if ($data)
			{
				$data = $data['QBMSXMLMsgsRs'];

				// preset some variables
				$rsStatusCode = $data[$transactionType.'Rs']['@attributes']['statusCode'];

				switch ($rsStatusCode)
				{
					case '0':
					{
						if ($transactionType == 'CustomerCreditCardCapture')
						{
							$rsAuthorizationCode = $data[$transactionType.'Rs']['AuthorizationCode'];
							$rsPaymentStatus = $data[$transactionType.'Rs']['PaymentStatus'];

							if ($rsPaymentStatus != 'Completed')
							{
								$this->is_error = true;
								$this->error_message = 'The transaction may have been successfully or unsuccessfully processed at the card issuer: there simply was no response (possibly due to network errors).';
							}
						}

						$rsCreditCardTransId = $data[$transactionType.'Rs']['CreditCardTransID'];

						$this->createTransaction($db, $user, $order, $data, '', $transactionType, '', '', $rsCreditCardTransId);

						// Update the orders table status
						$db->reset();
						$db->assignStr('payment_status', $order->getPaymentStatus());
						$db->update(DB_PREFIX."orders", "WHERE oid=".$order_data['oid']);

						$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

						return true;
					}
					// an error has occured
					default:
					{
						$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
						$this->storeTransaction($db, $user, $order, $data, false);
						return false;
					}
				}
			}
			else
			{
				$this->is_error = true;
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $data, '', false);
			}

			$this->error_message = 'The gateway could not process this request';

			// will return false
			return false;
		}
	}
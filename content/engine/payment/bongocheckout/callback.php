<?php
	
	chdir("./../../../../");

	require_once 'content/classes/_boot.php';

	/**
	 * Check for return to cart (not real callback), return in browser
	 */
	if (!isset($_REQUEST['status']) && isset($_REQUEST['complete']) && $_REQUEST['complete'] == '1')
	{
		/**
		 * Restore session
		 */
		setSessionSavePath();

		if (isSslMode())
		{
			$host = $settings['GlobalHttpsCookieDomain'] != '' ? $settings['GlobalHttpsCookieDomain'] : parse_url($settings["GlobalHttpsUrl"], PHP_URL_HOST);
		}
		else
		{
			$host = $settings['GlobalHttpCookieDomain'] != '' ? $settings['GlobalHttpCookieDomain'] : parse_url($settings["GlobalHttpUrl"], PHP_URL_HOST);
		}

		if ($host != "localhost")
		{
			session_set_cookie_params($settings["SecurityUserTimeout"], "/", $host);
		}
		else
		{
			session_set_cookie_params($settings["SecurityUserTimeout"], "/");
		}

		session_cache_expire(round($settings["SecurityUserTimeout"]/60));
		session_name("ShoppingCartSession");
		session_start();

		/**
		 * Check session variable
		 */
		
		$params = array();

		if (isset($_SESSION['BongoCheckoutInProgress']))
		{
			// process payment was not called yet
			// do process payment and mark payment as pending
			_session_write_close();
			
			$_GET['oa'] = 'ProcessPayment';
			$_GET['status'] = 'P';
			$_GET['p'] = 'one_page_checkout';

			header('Location: '.$settings["GlobalHttpsUrl"].'/index.php?'.http_build_query($_GET));
		}
		else
		{
			// process payment already executed, just go to completed page
			header('Location: '.$settings["GlobalHttpsUrl"].'/index.php?p=completed');
		}
	}
	/**
	 * Handle real callback
	 */
	else
	{
		$_GET['norender'] = 'true';

		if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- Server call back\n======================\n\n", FILE_APPEND);

		// callback notification
		if (!isset($_REQUEST['status']) || !isset($_REQUEST['partner_key']) || !isset($_REQUEST['order'])) exit(0);

		$status = $_REQUEST['status'];
		$partnerKey = $_REQUEST['partner_key'];
		$order = base64_decode($_REQUEST['order']);
		$orderData = simplexml_load_string($order);

		$oid = $orderData->channel->item->custom_order1;
		$a = unserialize(str_replace('\"', '"', $orderData->channel->item->custom_order3));

		if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- a\n".print_r($a, 1)."\n\n", FILE_APPEND);

		/**
		 * Check current order status
		 */
		$queryResult = $db->query('SELECT * FROM '.DB_PREFIX.'orders WHERE oid="'.intval($oid).'" AND security_id="'.$db->escape($a['i']).'"');

		if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- q\n".$db->query."\n\n", FILE_APPEND);

		if (($orderDb = $db->moveNext($queryResult)) != false)
		{
			$oldOrderStatus = $newOrderStatus = $orderDb['status'];
			$oldPaymentStatus = $newPaymentStatus = $orderDb['payment_status'];

			/**
			 * Update bongo order data
			 * Change payment status and bongo order id
			 */
			$bongoOrderData = array(
				"bongoOrderId" => (string) $orderData->channel->item->idorder, 
				"trackingLink" => (string) $orderData->channel->item->trackinglink,
				"shippingAddress" =>
					$orderData->channel->item->shipaddress1."\n".
					(trim($orderData->channel->item->shipaddress2) != '' ? $orderData->channel->item->shipaddress2."\n" : '').
					$orderData->channel->item->shipcity.", ".$orderData->channel->item->shipstate.", ".$orderData->channel->item->shipzip."\n".
					$orderData->channel->item->shipcountry."\n".
					$orderData->channel->item->shipphone,
				"orderSubtotalAmount" => (string) $orderData->channel->item->ordersubtotal, 
				"orderDutyAmount" => (string) $orderData->channel->item->orderdutycost, 
				"orderTaxAmount" => (string) $orderData->channel->item->ordertaxcost, 
				"orderShippingAmount"  => (string) $orderData->channel->item->ordershippingcost, 
				"orderShippingDomesticAmount"=> (string) $orderData->channel->item->ordershippingcostdomestic, 
				"orderInsuranceAmount" => (string) $orderData->channel->item->orderinsurancecost, 
				"orderTotalAmount" => (string) $orderData->channel->item->ordertotal,
				"orderCurrency" => (string) $orderData->channel->item->ordercurrencycode
			);

			$db->query('SELECT name FROM '.DB_PREFIX.'states WHERE stid="'.intval($settings['bongocheckout_State']).'"');
			$stateName = $db->moveNext() ? $db->col['name'] : false;
			
			$db->reset();
			
			$db->assignStr('custom2', serialize($bongoOrderData));
			$db->assignStr('custom3', $orderData->channel->item->idorder);

			$db->update(DB_PREFIX.'orders', 'WHERE oid="'.intval($oid).'"');

			if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- Got order\n".print_r($orderDb, 1)."\n\n", FILE_APPEND);
			
			/**
			 * If orders status id new or abandoned, try to process payment
			 */
			if ($orderDb['status'] == 'Abandon' || $orderDb['status'] == 'New')
			{
				if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- ORDER STILL ".$orderDb['status']."\n\n", FILE_APPEND);
				// Try to restore session
				$_REQUEST['pcsid'] = $_GET['pcsid'] = $_POST['pcsid'] = $a['s'];
				$_REQUEST['_pcod'] = $_GET['_pcod'] = $_POST['_pcod'] = $a['i'];
				$_REQUEST['oa'] = 'ProcessPayment';
				
				$db->done();

				if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- Try to process payment\n\n", FILE_APPEND);

				require_once('index.php');
			}
			/**
			 *  Or just change payment status and update order details
			 */
			else
			{
				if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- ORDER STATUS IS ".$orderDb['status']."\n\n", FILE_APPEND);
				// check hash
				$externalHash = $orderData->channel->item->custom_order2;
				$internalHash = $orderDb['custom1'];

				if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- update status ".$externalHash." vs ".$internalHash."\n\n", FILE_APPEND);

				/**
				 * Check for hashes and change order status / data
				 */
				if ($externalHash == $internalHash && $settings["bongocheckout_Partner_Key"] == $partnerKey)
				{
					$paymentStatuses = array(
						'N' => 'Received',
	    				'P' => 'Pending',
						'B' => 'Declined',
						'C' => 'Canceled',
						'V' => 'Received'
					);

					/**
 					 * Update order data
 					 * Change payment status and bongo order id
 					 */

					$newPaymentStatus = isset($paymentStatuses[$status]) ? $paymentStatuses[$status] : 'Pending';
					$db->reset();
					$db->assignStr('payment_status', $newPaymentStatus);
					$db->update(DB_PREFIX.'orders', 'WHERE oid="'.intval($oid).'"');

					if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- q\n".$db->query."\n\n", FILE_APPEND);

					// Store Bongo callback transaction
					$db->reset();
					$db->assign("oid", $oid);
					$db->assign("uid", $orderDb['uid']);

					$db->assign("completed", 'NOW()');
					$db->assign('is_success', $status == "N" || $status == "P" || $status == "V" ? '1' : '0');
					
					$db->assignStr("payment_type", "ipn");
					$db->assignStr("payment_gateway", "bongocheckout (Bongo Checkout)");
					$db->assignStr("payment_response", array2text(xml2array($orderData)));
					
					$db->assignStr("order_subtotal_amount", $orderData->channel->item->ordersubtotal);
					$db->assignStr("order_total_amount", $orderData->channel->item->ordertotal);
					
					$db->assignStr("shipping_method", "Bongo");
					$db->assignStr("shipping_submethod", "");
					$db->assignStr("shipping_amount", 
						$orderData->channel->item->ordershippingcost + $orderData->channel->item->ordershippingcostdomestic + $orderData->channel->item->orderinsurancecost
					);
					
					$db->assignStr("tax_amount", $orderData->channel->item->orderdutycost + $orderData->channel->item->ordertaxcost);
					
					$db->insert(DB_PREFIX."payment_transactions");

					if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- q\n".$db->query."\n\n", FILE_APPEND);
				}
				/**
				 * Log unsuccessful payment transaction
				 */
				else
				{
					$db->reset();
					$db->assign("oid", $oid);
					$db->assign("uid", $orderDb['uid']);

					$db->assign("completed", 'NOW()');
					$db->assign('is_success', '0');
					$db->assignStr("payment_response", $order);
					$db->insert(DB_PREFIX."payment_transactions");

					if (defined('DEVMODE') && DEVMODE) @file_put_contents('content/cache/log/bongo-callback.txt', date('r')."- q\n".$db->query."\n\n", FILE_APPEND);
				}
			}

			/**
			 * Handle on status change event
			 */
			if ($newOrderStatus != $oldOrderStatus || $newPaymentStatus != $oldPaymentStatus)
			{
				require_once('content/engine/engine_user.php');
				require_once('content/engine/engine_order.php');

				$user = USER::getById($db, $settings, $orderDb["uid"]);
				$order = ORDER::getById($db, $settings, $user, $orderDb["oid"]);
				Events_OrderEvent::dispatchStatusChangeEvent($order, $oldOrderStatus, $oldPaymentStatus);
			}
		}
	}

	$db->done();
<?php
require "moneris/mpgClasses.php";

$payment_processor_id = "Moneris";
$payment_processor_name = "moneris.com";
$payment_processor_class = "PAYMENT_MONERIS";
$payment_processor_type = "cc";

class PAYMENT_MONERIS extends PAYMENT_PROCESSOR
{
	function PAYMENT_MONERIS()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "moneris";
		$this->name = "moneris.com";
		$this->class_name = "PAYMENT_MONERIS";
		$this->type = "cc";
		$this->description = "";
		$this->steps = 1;
		$this->testMode = true;
		$this->show_cvv2 = true;
		return $this;
	}

	//***************************************************************
	//	function isTestMode()
	//		returns true or false
	//		sets $store_id and $api_token to either Live Mode values
	//		or Test Mode Values
	//		Live Mode values are taken from the data base
	//***************************************************************
	function isTestMode()
	{
		global $settings;
		$this->testMode = ($settings[$this->id."_Transaction_Mode"] != "Live");

		return $this->testMode;
	}

	public function replaceDataKeys()
	{
		return array('pan');
	}

	//***************************************************************
	//	function process()
	//		returns type boolean
	//		Talks to the moneris gateway to process a transaction
	//***************************************************************
	function process($db, $user, $order, $post_form)
	{
		global $_SERVER;
		global $settings;

		define('MONERIS_CANADA_TESTMODE', $this->isTestMode());

		if (isset($post_form) && is_array($post_form))
		{
			// get post_form, common, settings and custom vars
			reset($this->common_vars);
		}
		else
		{
			$this->is_error = true;
			$this->error_message = "Incorrect form params. Please contact site administrator.";

			return $this->is_error;
		}

		$store_id="".urlencode($settings[$this->id."_Store_Id"]);
		$api_token="".urlencode($settings[$this->id."_Api_Token"]);

		$type="purchase";
		$cust_id="CUSTOMER_ID_".$this->common_vars["customer_id"]["value"];

		//Due to the order id not changing when a card fails we must provied a uniqe method of identifying
		//transactions, we shall use the date and time down to seconds to do this
		$order_id="ORDER_ID_".$this->common_vars["order_id"]["value"]."_".date('dmYHis');
		$amount="".number_format($this->common_vars["order_total_amount"]["value"], 2, '.', '');
		$pan="".$post_form["cc_number"];
		$expiry_date="".substr($post_form["cc_expiration_year"], 2, 2).$post_form["cc_expiration_month"];
		$crypt="7";

		/************************** CVD Variables *****************************/
		$cvd_indicator = 1;
		$cvd_value = $post_form['cc_cvv2'];

		/********************** CVD Associative Array *************************/

		$cvdTemplate = array(
				'cvd_indicator' => $cvd_indicator,
				'cvd_value' => $cvd_value
		);

		$mpgCvdInfo = new mpgCvdInfo ($cvdTemplate);

		/*********************** Transactional Associative Array **********************/
		$txnArray = array(
			'type'=>$type,
			'order_id'=>$order_id,
			'cust_id'=>$cust_id,
			'amount'=>$amount,
			'pan'=>$pan,
			'expdate'=>$expiry_date,
			'crypt_type'=>$crypt
		);

		/**************************** Transaction Object *****************************/
		@$mpgTxn = new mpgTransaction($txnArray);

		/************************ Set CVD *****************************/
		$mpgTxn->setCvdInfo($mpgCvdInfo);

		/****************************** Request Object *******************************/
		@$mpgRequest = new mpgRequest($mpgTxn);

		/***************************** HTTPS Post Object *****************************/
		@$mpgHttpPost = new mpgHttpsPost($store_id, $api_token, $mpgRequest);

		/******************************* Response ************************************/
		@$mpgResponse = $mpgHttpPost->getMpgResponse();

		// ------ step 6) retrieve data using get methods. Using these methods you can retrieve the
		// ------ appropriate variables (getResponseCode) to check if the transactions is approved
		// ------ (=>0 or <50) or declined (>49) or incomplete (NULL)

		$responseData = array(
			'Type' => $type,
			'Customer Id' => $cust_id,
			'Order Id' => $order_id,
			'Amount' => $amount,
			'Crypt' => $crypt,
			'CardType' => $mpgResponse->getCardType(),
			'TransAmount' => $mpgResponse->getTransAmount(),
			'TxnNumber' => $mpgResponse->getTxnNumber(),
			'ReceiptId' => $mpgResponse->getReceiptId(),
			'TransType' => $mpgResponse->getTransType(),
			'ReferenceNum' => $mpgResponse->getReferenceNum(),
			'ResponseCode' => $mpgResponse->getResponseCode(),
			'ISO' => $mpgResponse->getISO(),
			'Message' => $mpgResponse->getMessage(),
			'AuthCode' => $mpgResponse->getAuthCode(),
			'Complete' => $mpgResponse->getComplete(),
			'TransDate' => $mpgResponse->getTransDate(),
			'TransTime' => $mpgResponse->getTransTime(),
			'Ticket' => $mpgResponse->getTicket(),
			'TimedOut' => $mpgResponse->getTimedOut(),
			'CVDResponse' => $mpgResponse->getCvdResultCode()
		);

		$requestData = $txnArray;
		$requestData['cvd_indicator'] = $cvd_indicator;
		$requestData['cvd_value'] = $cvd_value;
		$this->log("process Request:", $requestData);
		$this->log("process Response:", $responseData);

		//response code = null
		//we handle this case seperatly.  When moneris returns null in the response code it means that some
		//data wasn't sent that needed to be.  Look what $transactionMsg says for clues

		if ($mpgResponse->getResponseCode() == "null")
		{
			$this->is_error = true;
			$this->error_message = "An error occurred while processing this transaction";
		}
		//response code = 0-49
		//This means that the card was accepted in some form or another different response codes mean different things
		//but the money will be transfered.
		else if($mpgResponse->getResponseCode()>=0 and $mpgResponse->getResponseCode()<50)
		{
			$this->createTransaction($db, $user, $order, $responseData);
			$this->is_error = false;
			$this->error_message = "".$mpgResponse->getMessage();
		}
		//response code < 0 or > 50
		//These codes are given when all the data given was corect but the transaction was declined
		//idealy we want to save these messages as failed transactions
		else
		{
			$this->is_error = true;
			$this->error_message = "An error occurred while processing this transaction: Declined ".$mpgResponse->getResponseCode();;

			$this->storeTransaction($db, $user, $order, $responseData, '', false);
		}
		return !$this->is_error;
	}
}
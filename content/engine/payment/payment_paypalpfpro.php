<?php 
	$payment_processor_id = "paypalpfpro";
	$payment_processor_name = "PayPal Payflow Pro";
	$payment_processor_class = "PAYMENT_PAYPALPFPRO";
	$payment_processor_type = "cc";

	class PAYMENT_PAYPALPFPRO extends PAYMENT_PROCESSOR
	{
		protected $buttonSource = '';

		function PAYMENT_PAYPALPFPRO()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "paypalpfpro";
			$this->name = "PayPal Payflow Pro";
			$this->class_name = "PAYMENT_PAYPALPFPRO";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->support_ccs = true;
			$this->need_cc_codes = true;
			$this->allow_change_gateway_url = false;

			$this->buttonSource = getpp().'_Cart_PFP';

			return $this;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Mode"]["value"] == "Test")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		function getClient()
		{
			global $settings;

			$liveMode = !$this->isTestMode();

			$api = new Payment_PayPal_PayflowApi($settings, $liveMode);

			$api->setApiPartner($this->settings_vars[$this->id."_Partner"]["value"]);
			$api->setApiVendor($this->settings_vars[$this->id."_Vendor"]["value"]);
			$api->setApiUsername($this->settings_vars[$this->id."_User"]["value"]);
			$api->setApiPassword($this->settings_vars[$this->id."_Password"]["value"]);

			$api->setButtonSource($this->buttonSource);

			return $api;
		}

		/**
		 * @param DB $db
		 * @param USER $user
		 * @param ORDER $order
		 * @param $post_form
		 * @return bool
		 */
		function process($db, $user, $order, $post_form)
		{ 
			global $settings, $order;

			if (isset($post_form) && is_array($post_form))
			{
				$transactionType = $this->settings_vars[$this->id . '_Transaction_Type']['value'] == 'Sale' ? 'S' : 'A';

				$api = $this->getClient();

				if (preg_match('/^(.+) (.+)$/', $this->common_vars['shipping_name']['value'], $m))
				{
					$shipToFirstName = $m[1];
					$shipToLastName = $m[2];
				}
				else
				{
					$shipToFirstName = $this->common_vars['shipping_name']['value'];
					$shipToLastName = '';
				}

				$shipTo = array(
					'firstName' => $shipToFirstName,
					'lastName' => $shipToLastName,
					'street' => $this->common_vars['shipping_address']['value'],
					'city' => $this->common_vars['shipping_city']['value'],
					'state' => $this->common_vars['shipping_state_abbr']['value'],
					'zip' => $this->common_vars['shipping_zip']['value'],
					'countryIsoA3' => $this->common_vars['shipping_country_iso_a3']['value'],
				);

				$subtotalAmount = 0;
				$items = array();
				$index = 1;

				foreach ($order->items as $line)
				{
					$items['L_NAME' . $index] = substr($line['title'], 0, 127);
					$items['L_DESC' . $index] = substr($line['description'], 0, 127);
					$items['L_COST' . $index] = number_format($line['price'], 2, '.', '');
					$items['L_QTY' . $index] = $line['quantity'];
					$items['L_SKU' . $index] = substr($line['product_id'], 0, 17);

					if ($line['tax_amount'] > 0)
					{
						$items['L_TAXAMT' . $index]= number_format($line['tax_amount'], 2, '.', '');
					}
					else
					{
						$items['L_TAXAMT' . $index]= number_format($line['price'] * $line['quantity'] * $line['tax_rate'] / 100, 2, '.', '');
					}

					$subtotalAmount +=  $items['L_COST' . $index] * $items['L_QTY' . $index];

					$index++;
				}

				/**
				 * Charge card
				 */
				$response = $api->doTransaction(
					$transactionType,
					$this->common_vars['order_id']['value'],
					$this->common_vars['order_total_amount']['value'],
					$post_form['cc_number'],
					$post_form['cc_expiration_month'].substr($post_form['cc_expiration_year'], 2, 2),
					$post_form['cc_cvv2'],
					$post_form['cc_first_name'],
					$post_form['cc_last_name'],
					$this->common_vars['billing_address1']['value'],
					$this->common_vars['billing_address2']['value'],
					$this->common_vars['billing_city']['value'],
					$this->common_vars['billing_state_abbr']['value'],
					$this->common_vars['billing_zip']['value'],
					$this->common_vars['billing_country_iso_a3']['value'],
					false,
					$shipTo,
					$subtotalAmount,
					$order->getTaxAmount(),
					$order->getShippingAmount(),
					$order->getHandlingSeparated() ? $order->getHandlingAmount() : 0,
					$order->getDiscountAmount() + $order->getPromoDiscountAmount(),
					$items
				);

				if ($response && isset($response['RESULT']))
				{
					if ($response['RESULT'] == '0' || $response['RESULT'] == '126')
					{
						$extra = '';

						$custom1 = $transactionType == 'S' ? 'AUTH_CAPTURE' : 'AUTH_ONLY';

						// If Auth only, then store auth'd amount to custom2
						$custom2 = $transactionType != 'S' ? number_format($this->common_vars['order_total_amount']['value'], 2, '.', '') : '';

						// If Auth Capture, then store captured amount to custom3
						$custom3 = $transactionType == 'S' ? number_format($this->common_vars['order_total_amount']['value'], 2, '.', '') : 0;

						$securityId = $response['PNREF'];

						$this->createTransaction($db, $user, $order, $response, $extra, $custom1, $custom2, $custom3, $securityId);

						if ($this->support_ccs && $this->enable_ccs)
						{
							$card_data = array(
								'fn'=>$post_form['cc_first_name'],
								'ln'=>$post_form['cc_last_name'],
								'ct'=>$post_form['cc_type'],
								'cc'=>$post_form['cc_number'],
								'em'=>$post_form['cc_expiration_month'], 
								'ey'=>$post_form['cc_expiration_year']
							);

							$this->saveCCdata($db, $order, $card_data, base64_decode($settings['SecurityCCSCertificate']));
						}
						
						$this->is_error = false;
						$this->error_message = '';

						return ($transactionType == 'S') ? 'Received' : 'Pending';
					}
					else
					{
						$this->is_error = true;
						$this->error_message = 'Error code: '.$response['RESULT'] . ' ' . $response['RESPMSG'];
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
				}

				if ($this->is_error)
				{
					$this->storeTransaction($db, $user, $order, $response, '', false);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Incorrect form params. Please contact site administartor.";
			}
			return !$this->is_error;
		}

		/**
		 * @param bool $order_data
		 * @return bool
		 */
		function supportsCapture($order_data = false)
		{
			if ($order_data)
			{
				if ($order_data['payment_status'] == 'Pending' || $order_data['payment_status'] == 'Partial')
				{
					if ($this->isTotalAmountCaptured($order_data))
						return false;

					return true;
				}
			}
			return false;
		}

		/**
		 * @param bool $order_data
		 * @return bool
		 */
		function supportsRefund($order_data = false)
		{
			if ($order_data)
			{
				if ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Partial')
				{
					return true;
				}
				else
				if ($order_data['payment_status'] == 'Refunded')
				{
					return $this->_refundableAmount($order_data) > 0.0;
				}
			}
			return false;
		}

		public function supportsPartialRefund($order_data = false)
		{
			return true;
		}

		function supportsVoid($order_data = false)
		{
			return $order_data && $order_data['custom1'] != 'AUTH_CAPTURE' && ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Pending');
		}

		function isTotalAmountCaptured($order_data)
		{
			return $order_data['custom3'] != '' && $order_data['custom3'] == $order_data['total_amount'];
		}

		public function capturedAmount($order_data = false)
		{
			return $order_data ? $order_data['custom3'] : false;
		}

		function capturableAmount($order_data=false)
		{
			if ($order_data)
			{
				if ($this->supportsCapture($order_data))
				{
					$capturableAmount = $order_data['total_amount'] - $order_data['custom3'];
					if ($capturableAmount < 0)
						$capturableAmount = 0.0;
					return number_format($capturableAmount, 2, '.', '');
				}
			}
			return 0;
		}

		function getTotalCaptured($order_data)
		{
			return 0.00 + $order_data['custom3'];
		}

		function capture($order_data=false,$capture_amount=false)
		{
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$client = $this->getClient();

				//check is transaction completed
				if ($this->isTotalAmountCaptured($order_data))
				{
					$this->is_error = true;
					$this->error_message = "Transaction already completed";
					return false;
				}

				//get captured total amount & x_trans_id
				$captured_total_amount = $capture_amount ? $capture_amount : $order_data["custom2"];
				$transaction_id = $order_data["security_id"];

				if ($transaction_id == '')
				{
					$this->is_error = true;
					$this->error_message = 'This order does not have a valid transaction id';
					return false;
				}
				else
				{
					if ($captured_total_amount <= 0)
					{
						$this->is_error = true;
						$this->error_message = "Capture amount must be greater than 0";
						return false;
					}
					else if ($captured_total_amount > $order_data['total_amount'])
					{
						$this->is_error = true;
						$this->error_message = "Capture amount cannot be more than total amount";
						return false;
					}
					else if ($order_data['custom3'] + $captured_total_amount > $order_data['total_amount'])
					{
						$this->is_error = true;
						$this->error_message = 'Captured amount cannot be more than total amount';
						return false;
					}

					$response = $client->doCapture($transaction_id);

					$this->log("capture Response:", $response);

					$userTmp = new tmp_object();
					$userTmp->id = $order_data["uid"];

					$orderTmp = new tmp_object();
					$orderTmp->oid = $order_data["oid"];
					$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
					$orderTmp->totalAmount = $order_data["total_amount"];
					$orderTmp->shippingAmount = $order_data["shipping_amount"];
					$orderTmp->taxAmount = $order_data["tax_amount"];
					$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

					if (isset($response['RESULT']) && $response['RESULT'] == '0')
					{
						$transaction_id = $response['PNREF'];

						$totalCaptured = $this->getTotalCaptured($order_data) + $captured_total_amount;
						$custom1 = 'PRIOR_AUTH_CAPTURE';
						$custom2 = '';
						$custom3 = number_format($totalCaptured, 2, '.', '');

						$db->reset();
						$db->assignStr("custom1", $custom1);
						$db->assignStr("custom2", $custom2);
						$db->assignStr("custom3", $custom3);
						$db->assignStr('security_id', $transaction_id);

						$oldPaymentStatus = $order->getPaymentStatus();
						$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);
						
						if ($order_data['total_amount'] > $totalCaptured)
						{
							$order->setPaymentStatus(ORDER::PAYMENT_STATUS_PARTIAL);
						}
						
						$db->assignStr("payment_status", $order->getPaymentStatus());
						$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

						$order_data['custom1'] = $custom1;
						$order_data['custom2'] = $custom2;
						$order_data['custom3'] = $custom3;
						$order_data['payment_status'] = $order->getPaymentStatus();

						$security_id = $transaction_id;

						$this->createTransaction($db, $userTmp, $orderTmp, 
							"ORDER TYPE: PRIOR_AUTH_CAPTURE\n\n".array2text($response),
							"",
							$custom1,
							$custom2,
							$custom3,
							$security_id
						);

						$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

						return true;
					}
					else
					{
						$this->is_error = true;

						$this->storeTransaction($db, $userTmp, $orderTmp, $response, '', false);

						$this->error_message = $response['RESPMSG'];
						return false;
					}
				}
			}
			return !$this->is_error;
		}

		function refundableAmount($order_data=false)
		{
			if ($order_data)
			{
				if ($this->supportsRefund($order_data))
				{
					return $this->_refundableAmount($order_data);
				}
			}
			return 0;
		}

		function _refundableAmount($order_data)
		{
			$refundableAmount = $this->getTotalCaptured($order_data);
			return number_format($refundableAmount, 2, '.', '');
		}

		function refund($order_data=false,$refund_amount=false)
		{
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				if (!$this->supportsRefund($order_data))
				{
					$this->is_error = true;
					$this->error_message = "Order cannot be refunded";
					return false;
				}

				//get captured total amount & x_trans_id
				$refund_total_amount = $refund_amount;

				if (!$refund_total_amount)
				{
					$refund_total_amount = $order_data['custom3'];
				}

				if ($refund_total_amount < 0)
				{
					$this->is_error = true;
					$this->error_message = "Refund amount must be greater than 0";
					return false;
				}
				else if ($refund_total_amount > $order_data['total_amount'])
				{
					$this->is_error = true;
					$this->error_message = "Refund amount cannot be more than total amount";
					return false;
				}
				else if ($refund_total_amount > $order_data['custom3'])
				{
					$this->is_error = true;
					$this->error_message = 'Refund amount cannot be more than total captured amount';
					return false;
				}

				$transaction_id = $order_data["security_id"];

				if ($transaction_id == '')
				{
					$this->is_error = true;
					$this->error_message = 'This order does not have a valid transaction id';
					return false;
				}
				else
				{
					$client = $this->getClient();

					$response = $client->doCredit($transaction_id, $refund_total_amount);
					
					$this->log("refund Response:", $response);

					$userTmp = new tmp_object();
					$userTmp->id = $order_data["uid"];

					$orderTmp = new tmp_object();
					$orderTmp->oid = $order_data["oid"];
					$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
					$orderTmp->totalAmount = $order_data["total_amount"];
					$orderTmp->shippingAmount = $order_data["shipping_amount"];
					$orderTmp->taxAmount = $order_data["tax_amount"];
					$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

					if (isset($response['RESULT']) && $response['RESULT'] == '0')
					{
						$transaction_id = $response['PNREF'];

						$totalCaptured = $this->getTotalCaptured($order_data) - $refund_total_amount;
						$custom3 = number_format($totalCaptured, 2, '.', '');

						$db->reset();
						$db->assignStr("custom1", "");
						$db->assignStr("custom2", "");
						$db->assignStr("custom3", $custom3);
						$db->assignStr('security_id', $transaction_id);

						$oldPaymentStatus = $order->getPaymentStatus();
						$order->setPaymentStatus(ORDER::PAYMENT_STATUS_REFUNDED);

						$db->assignStr("payment_status", $order->getPaymentStatus());
						$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

						$order_data['custom1'] = 'REFUND';
						$order_data['custom2'] = '';
						$order_data['custom3'] = $custom3;
						$order_data['payment_status'] = $order->getPaymentStatus();
						
						$custom1 = $order_data['custom1'];
						$custom2 = $order_data['custom2'];
						$security_id = $transaction_id;

						$this->createTransaction($db, $userTmp, $orderTmp, 
							"ORDER TYPE: REFUND\n\n".array2text($response),
							"",
							$custom1,
							$custom2,
							$custom3,
							$security_id
						);

						$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

						return true;
					}
					else
					{
						$this->is_error = true;

						$this->storeTransaction($db, $userTmp, $orderTmp, $response, '', false);

						$this->error_message = $response['RESPMSG'];
						return false;
					}
				}
			}
			return !$this->is_error;
		}

		function void($order_data=false)
		{
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$client = $this->getClient();

				$transaction_id = $order_data["security_id"];

				$response = $client->doVoid($transaction_id);

				$this->log("void Response:", $response);

				$userTmp = new tmp_object();
				$userTmp->id = $order_data["uid"];

				$orderTmp = new tmp_object();
				$orderTmp->oid = $order_data["oid"];
				$orderTmp->subtotalAmount = $order_data["subtotal_amount"];
				$orderTmp->totalAmount = $order_data["total_amount"];
				$orderTmp->shippingAmount = $order_data["shipping_amount"];
				$orderTmp->taxAmount = $order_data["tax_amount"];
				$orderTmp->gift_cert_amount = $order_data["gift_cert_amount"];

				if (isset($response['RESULT']) && $response['RESULT'] == '0')
				{
					$transaction_id = $response['PNREF'];

					$custom1 = 'CANCELED';
					$custom2 = '';
					$custom3 = '';

					$db->reset();
					$db->assignStr("custom1", $custom1);
					$db->assignStr("custom2", $custom2);
					$db->assignStr("custom3", $custom3);
					$db->assignStr('security_id', $transaction_id);

					$oldPaymentStatus = $order->getPaymentStatus();
					$oldOrderStatus = $order->getStatus();

					$order->setPaymentStatus(ORDER::PAYMENT_STATUS_CANCELED);
					$order->setStatus(ORDER::STATUS_CANCELED);

					$db->assignStr("payment_status", $order->getPaymentStatus());
					$db->assignStr('status', $order->getStatus());
					$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

					$order_data['custom1'] = $custom1;
					$order_data['custom2'] = $custom2;
					$order_data['custom3'] = $custom3;
					$order_data['payment_status'] = $order->getPaymentStatus();
					$order_data['status'] = $order->getStatus();

					$this->createTransaction($db, $userTmp, $orderTmp, 
						"ORDER TYPE: REVERSAL\n\n".array2text($response),
						"",
						$custom1,
						$custom2,
						$custom3,
						$transaction_id
					);

					$this->fireOrderEvent($order, $oldOrderStatus, $oldPaymentStatus);

					return true;
				}
				else
				{
					$this->is_error = true;

					$this->storeTransaction($db, $userTmp, $orderTmp, $response, '', false);

					$this->error_message = $response['RESPMSG'];
					return false;
				}
			}
			return !$this->is_error;
		}
	}

<?php
	$payment_processor_id = "paypalpro";
	$payment_processor_name = "PayPal Website Payments Pro";
	$payment_processor_class = "PAYMENT_PAYPALPRO";
	$payment_processor_type = "cc";
	
	class PAYMENT_PAYPALPRO extends PAYMENT_PROCESSOR
	{
		protected $buttonSource = '';

		function PAYMENT_PAYPALPRO()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "paypalpro";
			$this->name = "PayPal Payments Pro";
			$this->class_name = "PAYMENT_PAYPALPRO";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->is_certificate = true;
			$this->certificate_optional = true;
			$this->certificate_path = "paypal/certificate.pem";
			$this->need_cc_codes = true;
			$this->support_ccs = true;
			$this->need_cc_codes = true;
			$this->allow_change_gateway_url = false;

			$this->buttonSource = getpp().'_Cart_PPP';

			return $this;
		}
		
		/**
		 * Returns payment form fields
		 */
		function getPaymentForm($db)
		{
			global $settings;
			$fields = parent::getPaymentForm($db);
			if ($this->isCardinalCommerceEnabled())
			{
				$fields["do_cmpi_lookup"] = array("type" => "hidden", "name"=> "do_cmpi_lookup", "value" => 1);
			}
			return $fields;
		}

		function isTestMode()
		{
			if ($this->settings_vars[$this->id."_Mode"]["value"] == "Test")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		function getClient()
		{
			global $settings;

			$liveMode = !$this->isTestMode();

			$api = new Payment_PayPal_PayPalApi($settings, $liveMode);

			$api->setButtonSource($this->buttonSource);

			$api->setApiUsername($this->settings_vars[$this->id."_Username"]["value"]);
			$api->setApiPassword($this->settings_vars[$this->id."_Password"]["value"]);

			if (!is_null($this->settings_vars[$this->id."_Signature"]["value"]) && !empty($this->settings_vars[$this->id."_Signature"]["value"]))
			{
				$api->setApiSignature($this->settings_vars[$this->id."_Signature"]["value"]);
			}
			else
			{
				$api->setUseCertificate(true);
			}

			return $api;
		}

		/**
		 * Process paypal transaction
		 * @param $db
		 * @param $user
		 * @param $order
		 * @param $post_form
		 * @param $EciFlag
		 * @param $Cavv
		 * @return unknown_type
		 */
		function processPayPal($db, $user, $order, $post_form, $cardinalData = false)
		{
			global $settings;
			
			$this->testMode = $this->settings_vars[$this->id."_Mode"] != 'Live';

			$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?securityId=".$this->common_vars["order_security_id"]["value"].'&oid='.$order->oid."&p=paypal_callback";

			/**
			 * Process paypal
			 */
			if (isset($post_form) && is_array($post_form))
			{
				$api = $this->getClient();

				if ($cardinalData && !$this->isCardinalCommerceEnabled($post_form['cc_number']))
				{
					$cardinalData = false;
				}

				$response = $api->doDirectPayment($this->settings_vars[$this->id."_Transaction_Type"]["value"], $this->id, $post_form, $this->settings_vars, $this->common_vars, $cardinalData, $notify_url);

				if (!$api->wasLastRequestError())
				{
					if (isset($response['ACK']) && ($response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarnings' || $response['ACK'] == 'SuccessWithWarning'))
					{
						$responseStr = '';

						$charged_amount = 0;

						$transaction_id = '';
						foreach ($response as $var => $value)
						{
							$responseStr.= $var."  =  ".$value."\n";
							if($var == "TRANSACTIONID")
								$transaction_id = $value;
						}

						if ($this->settings_vars[$this->id."_Transaction_Type"]["value"] == "Sale")
						{
							$charged_amount = $this->common_vars["order_total_amount"]["value"];
						}

						$custom1 = strtolower($this->settings_vars[$this->id."_Transaction_Type"]["value"]);

						$avsResult = isset($response['AVSCODE']) ? $response['AVSCODE'] : '';
						$cvvResult = isset($response['CVV2MATCH']) ? $response['CVV2MATCH'] : '';

						$event = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_PAYMENT_METHOD_DATA_SET);
						$event->setOrder($order);
						$paymentParams = array();
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_AVS] =  $avsResult;
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CVV]  = $cvvResult;
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDFNAME]  = $post_form["cc_first_name"];
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDLNAME]  = $post_form["cc_last_name"];
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM]  = $post_form['cc_number'];
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDCVV2]  = $post_form["cc_cvv2"];
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPMONTH]  = $post_form["cc_expiration_month"];
						$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPYEAR]  = $post_form["cc_expiration_year"];
						$event->setPaymentMethodData($paymentParams);
						Events_EventHandler::handle($event);

						$this->createTransaction($db, $user, $order, $responseStr, "", $custom1, "", $charged_amount, $transaction_id);
						
						if ($this->support_ccs && $this->enable_ccs)
						{
							$card_data = array(
								"fn"=>$post_form["cc_first_name"],
								"ln"=>$post_form["cc_last_name"],
								"ct"=>$post_form["cc_type"],
								"cc"=>$post_form["cc_number"],
								"em"=>$post_form["cc_expiration_month"], 
								"ey"=>$post_form["cc_expiration_year"]
							);
							$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]));
						}
						$this->is_error = false;
						if ($this->settings_vars[$this->id."_Transaction_Type"]["value"] == "Authorization")
						{
							return "Pending";
						}
						return "Received";
					}
					elseif (isset($response['ACK']) && $response['ACK'] == 'Failure')
					{
						$this->is_error = true;
						$this->error_message = isset($response['L_SHORTMESSAGE0']) ? $response['L_SHORTMESSAGE0'].': '.$response['L_LONGMESSAGE0'] : PAYMENT_TRANSACTION_ERROR_TEXT;
					}
					else
					{
						$this->is_error = true;
						$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
					}
				}
				else
				{
					$this->is_error = true;
				 	$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
				}
				
				if ($this->is_error)
				{
					$this->storeTransaction($db, $user, $order, $response);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Incorrect form params. Please contact site administartor.";
			}
			return !$this->is_error;
		}
		
		/**
		 * Process transactoin
		 * @param $db
		 * @param $user
		 * @param $order
		 * @param $post_form
		 * @return boolean
		 */
		function process($db, $user, $order, $post_form)
		{
			global $settings;
			global $backurl;

			$payment_status = false;

			//check cardinal centinel is enabled
			if ($this->isCardinalCommerceEnabled())
			{
				//create object and set it up
				$cardinal = new CardinalCommerce();
				$cardinal->version ="1.7";
				$cardinal->common_vars = $this->common_vars;
				
				if (isset($_REQUEST["do_cmpi_lookup"]))
				{
					$cardinal_lookup_xml = $cardinal->getLookupXML(
						$post_form["cc_first_name"],
						$post_form["cc_last_name"],	
						$post_form["cc_number"], 
						$post_form["cc_expiration_month"], 
						$post_form["cc_expiration_year"],
						$this->currency["code"]
					);
					
					$cardinal_lookup_result = $cardinal->processRequest($cardinal_lookup_xml);
					
					if ($cardinal_lookup_result)
					{
						if ($cardinal_lookup_result["CardinalMPI"][0]["Enrolled"][0] == "Y")
						{
							$this->redirect = true;
							$this->redirectURL = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=payment_validation";
							
							$_SESSION["CardinalPostForm"] = $post_form;
							$_SESSION["CardinalTransactionId"] = $cardinal_lookup_result["CardinalMPI"][0]["TransactionId"][0];
							$_SESSION["CardinalACSUrl"] = $cardinal_lookup_result["CardinalMPI"][0]["ACSUrl"][0];
							$_SESSION["CardinalPayload"] = $cardinal_lookup_result["CardinalMPI"][0]["Payload"][0];
							//validator will replace ORDER_PROCESS_PAYMENT_VALIDATION with ORDER_PROCESS_PAYMENT
							$_SESSION["CardinalNotifyUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT_VALIDATION."&do_cmpi_authenticate=true";
							$_SESSION["CardinalRedirectUrl"] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"];

							return false;
						}
						else
						{
							//do not redirect, process simple paypal transaction
							$payment_status = $this->processPayPal($db, $user, $order, $post_form);
						}
					}
					else
					{
						if ($cardinal->is_fatal_error)
						{
							$payment_status = false;
							$this->is_error = true;
							$this->error_message = $cardinal->error_message;
						}
						else
						{
							$payment_status = $this->processPayPal($db, $user, $order, $post_form);
						}
					}
				}
				
				if (isset($_REQUEST["do_cmpi_authenticate"]) && isset($_SESSION["CardinalPostForm"]) && isset($_REQUEST["PaRes"]))
				{
					$post_form = $_SESSION["CardinalPostForm"];
					unset($_SESSION["CardinalPostForm"]);
					$cardinal_auth_xml = $cardinal->getAuthenticateXML($_REQUEST["PaRes"]);
					$cardinal_auth_result = $cardinal->processRequest($cardinal_auth_xml);
					
					if ($cardinal_auth_result)
					{
						$EciFlag = $cardinal_auth_result["CardinalMPI"][0]["EciFlag"][0];
						$PAResStatus = $cardinal_auth_result["CardinalMPI"][0]["PAResStatus"][0];
						$Cavv = $cardinal_auth_result["CardinalMPI"][0]["Cavv"][0];
						$SignatureVerification = $cardinal_auth_result["CardinalMPI"][0]["SignatureVerification"][0];
						$Xid = $cardinal_auth_result["CardinalMPI"][0]["Xid"][0];
						$ErrorNo = $cardinal_auth_result["CardinalMPI"][0]["ErrorNo"][0];
						
						if ($SignatureVerification == "Y" && $PAResStatus != "N")
						{
							switch ($PAResStatus)
							{
								case "Y" : 
								case "A" : 
								{
									$cardinalData = array(
										"AuthStatus3ds" => $PAResStatus,
										"MpiVendor3ds" => "Y",
										"Cavv" => $Cavv,
										"Eci3ds" => $EciFlag,
										"Xid" => $Xid
									);
									$payment_status = $this->processPayPal($db, $user, $order, $post_form, $cardinalData);
									break;
								}
								case "U" : 
								{
									$payment_status = $this->processPayPal($db, $user, $order, $post_form);
									break;
								}
							}
						}
						else 
						{
							$payment_status = false;
							$this->is_error = true;
							$this->error_message = "Your card can not be validated. Please try other card or other payment method";
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $cardinal->error_message;
					}
				}
			}
			else
			{
				$payment_status = $this->processPayPal($db, $user, $order, $post_form);
			}

			return $this->is_error ? false : $payment_status;
		}


		/**
		 * Checks does payment gateway support capture for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsCapture($order_data = false)
		{
			return $order_data['custom1'] == 'authorization';
		}


		/**
		 * Checks does payment gateway support partial captures for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return bool
		 */
		function supportsPartialCapture($order_data = false)
		{
			return true;
		}

		/**
		 * Returns capturable amount
		 * @param mixed $order_data
		 * @return boolean
		 */
		public function capturableAmount($order_data = false)
		{
			return $order_data['custom3'] > 0 ? $order_data["total_amount"] - $order_data["custom3"] : $order_data['total_amount'];
		}

		/**
		 * Captures funds
		 *
		 * @param bool $order_data
		 * @param bool $capture_amount
		 *
		 * @return bool
		 */
		public function capture($order_data = false, $capture_amount = false)
		{
			$apiClient = $this->getClient();
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$capturable_amount = $this->capturableAmount($order_data);

				//check is transaction completed
				if ($capturable_amount == 0)
				{
					$this->is_error = true;
					$this->error_message = 'Transaction already completed';
					return false;
				}

				//get captured total amount & x_trans_id
				$captured_total_amount = $capture_amount ? $capture_amount : $capturable_amount;

				$totalCaptured = $order_data['custom3'] + $captured_total_amount;

				if ($order_data['total_amount'] == $totalCaptured)
				{
					$complete_type = "Complete";
				}
				else
				{
					$complete_type = "NotComplete";
				}

				$result = $apiClient->handleCapture($db, $order, $order_data['security_id'], $captured_total_amount, $totalCaptured, $this->currency["code"], '', $complete_type, '');
				if ($result !== false)
				{
					$this->is_error = true;
					$this->error_message = $result;
				}
				else
				{
					$this->is_error = false;
					$this->error_message = '';
				}
			}

			return !$this->is_error;
		}

		/**
		 * Returns amount captured
		 *
		 * @param $order_data
		 */
		public function capturedAmount($order_data = false)
		{
			if (in_array($order_data["custom1"], array("order", "authorization", "fulfill")) && is_numeric($order_data["custom3"]))
			{
				return $order_data['custom3'];
			}

			return false;
		}

		/**
		 * Checks does payment gateway supports refund for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsRefund($order_data = false)
		{
			return $order_data['custom1'] == 'sale'; // || $order_data['custom1'] == 'none';
		}

		/**
		 * Checks does payment gateway supports partial refund for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsPartialRefund($order_data = false)
		{
			return false;
		}

		/**
		 * Returns refundable amount
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function refundableAmount($order_data = false)
		{
			return $order_data['custom3'];
		}

		/**
		 * Refunds order
		 *
		 * @param bool $order_data
		 * @param bool $refund_amount
		 *
		 * @return bool
		 */
		public function refund($order_data = false, $refund_amount = false)
		{
			$apiClient = $this->getClient();
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$refundable_amount = $this->refundableAmount($order_data);

				//check is transaction completed
				if ($refundable_amount == 0)
				{
					$this->is_error = true;
					$this->error_message = 'Transaction already refunded';
					return false;
				}

				$refund_total_amount = $refund_amount ? $refund_amount : $refundable_amount;

				if ($order_data['custom3'] - $refund_total_amount < 0) $refund_total_amount = $order_data['custom3'];

				$result = $apiClient->handleRefund($db, $order, $order_data['security_id'], $refund_total_amount, $this->currency["code"], '', '', $refund_total_amount, $order_data["total_amount"]);

				if ($result !== false)
				{
					$this->is_error = true;
					$this->error_message = $result;
				}
				else
				{
					$this->is_error = false;
					$this->error_message = '';
				}
			}

			return !$this->is_error;
		}

		/**
		 * Checks does payment gateway support void for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsVoid($order_data = false)
		{
			return $order_data['custom1'] == 'sale' || $order_data['custom1'] == 'authorization';
		}

		/**
		 * Void order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function void($order_data = false)
		{
			$apiClient = $this->getClient();
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$result = $apiClient->handleVoid($db, $order, $order_data['security_id'], '', 0, 0, '');

				if ($result !== false)
				{
					$this->is_error = true;
					$this->error_message = $result;
				}
				else
				{
					$this->is_error = false;
					$this->error_message = '';
				}
			}

			return !$this->is_error;
		}

	}

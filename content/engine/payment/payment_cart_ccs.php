<?php

	$payment_processor_id = "cart_ccs";
	$payment_processor_name = "Cart Credit Card Storage";
	$payment_processor_class = "PAYMENT_CART_CCS";
	$payment_processor_type = "cc";
	
	class PAYMENT_CART_CCS extends PAYMENT_PROCESSOR
	{
		public $allow_change_gateway_url = false;

		public function PAYMENT_CART_CCS()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "cart_ccs";
			$this->name = "Cart Credit Card Storage";
			$this->class_name = "PAYMENT_CART_CCS";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->need_cc_codes = true;
			return $this;
		}

        public function supportsPaymentProfiles()
        {
            return true;
        }

		public function getPaymentForm($db)
		{
			$form = parent::getPaymentForm($db);
			unset($form["cc_cvv2"]); // no cvv2 to be PCI complient
			return $form;
		}
		
		public function process($db, $user, $order, $post_form)
		{
			global $settings;
			
			// check expiration date on card
			if (intval($post_form["cc_expiration_year"].$post_form["cc_expiration_month"]) < intval(date('Ym')))
			{
				$this->is_error = true;
				$this->error_message = 'This credit card has expired. Please check the expiration date or use a different credit card.';
			}
			
			if ($settings["SecurityCCSActive"] == "0")
			{
				$this->is_error = true;
				$this->error_message = "CCS is not activated";
			}
			elseif($settings["SecurityCCSCertificate"] == "" || $settings["SecurityCCSPrivateKey"] == "")
			{
				$this->is_error = true;
				$this->error_message = "CCS Certificate is empty";
			}
			
			if (!$this->is_error)
			{
				$card_data = array(
					"fn"=>$post_form["cc_first_name"],
					"ln"=>$post_form["cc_last_name"],
					"ct"=>isset($post_form["cc_type"]) ? $post_form["cc_type"] : '',
					"cc"=>$post_form["cc_number"],
					"em"=>$post_form["cc_expiration_month"], 
					"ey"=>$post_form["cc_expiration_year"]
				);

				$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]));
			}
			
			return !$this->is_error;
		}
	}
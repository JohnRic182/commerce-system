<?php
	$payment_processor_id = "futurepay";
	$payment_processor_name = "FuturePay";
	$payment_processor_class = "PAYMENT_FUTUREPAY";
	$payment_processor_type = "ipn";

	class PAYMENT_FUTUREPAY extends PAYMENT_PROCESSOR
	{
		/**
		 * Class constructor
		 */
		public function PAYMENT_FUTUREPAY()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "futurepay";
			$this->name = "FuturePay";
			$this->class_name = "PAYMENT_FUTUREPAY";
			$this->type = "ipn";
			$this->description = "";
			$this->steps = 2;
			//$this->possible_payment_delay = true;
			//$this->possible_payment_delay_timeout = 10; // seconds
			$this->override_payment_form = false;
			$this->payment_in_popup = false;
			$this->reload_parent_on_process = false;

			return $this;
		}

		public function replaceDataKeys()
		{
			return array('gmid', 'pmid');
		}

		function isTestMode()
		{
			global $settings;

			if (strtolower($settings['futurepay_Test_Request']) == 'true')
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		protected function getGatewayUrl()
		{
			if ($this->isTestMode())
			{
				return 'https://sandbox.futurepay.com/remote/';
			}

			return 'https://api.futurepay.com/remote/';
		}

		public function testConnection()
		{
			$postDataArray = array();
			$postDataArray['reference'] = 'ORDER0';

			// Billing Info, must be US only, no p.o. boxes
			$postDataArray['email'] = 'test123@somenameserver.com';
			$postDataArray['first_name'] = 'Test';
			$postDataArray['last_name'] = 'Tester';
			$postDataArray['company'] = '';
			$postDataArray['address_line_1'] = '123 Test Dr';
			$postDataArray['address_line_2'] = '';
			$postDataArray['city'] = 'Nowhere';
			$postDataArray['state'] = 'KS';
			$postDataArray['country'] = 'US';
			$postDataArray['zip'] = '67054';
			$postDataArray['phone'] = '1231231234';

			$postDataArray['sku'][] = 'order';
			$postDataArray['price'][] = 1.00;
			$postDataArray['tax_amount'][] = 0;
			$postDataArray['description'][] = 'Test Invoice Subtotal';
			$postDataArray['quantity'][] = 1;

			$orderToken = trim($this->doCall('merchant-request-order-token', $postDataArray));

			return substr($orderToken, 0, 4) == 'FPTK';
		}

		private function doCall($method, &$postDataArray)
		{
			global $settings;

			$GMID = $settings['futurepay_MerchantId'];
			$PMID = '4807befacabe6f7d43e0d80f382494181afc1d57FPM888250775';

			$postDataArray['pmid'] = $PMID;
			$postDataArray['gmid'] = $GMID;
			$postDataArray['api_version'] = '01_00_00';

			$postData = http_build_query($postDataArray);

			$ch = curl_init();

			if ($settings["ProxyAvailable"] == "YES")
			{
				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($ch, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}

				curl_setopt($ch, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($ch, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}
			}

			curl_setopt($ch, CURLOPT_URL, $this->getGatewayUrl().$method);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$server_output = curl_exec ($ch);

			$this->log("Request:", $postDataArray);
			$this->log("Response:\n".$server_output);

			curl_close($ch);

			return $server_output;
		}

		function getOrderToken()
		{
			global $settings, $order;

			$postDataArray = array();
			$postDataArray['reference'] = 'ORDER'.$this->common_vars["order_id"]["value"];

			// Billing Info, must be US only, no p.o. boxes
			$postDataArray['email'] = $this->common_vars["billing_email"]["value"];
			$postDataArray['first_name'] = $this->common_vars["billing_first_name"]["value"];
			$postDataArray['last_name'] = $this->common_vars["billing_last_name"]["value"];
			$postDataArray['company'] = $this->common_vars["billing_company"]["value"];
			$postDataArray['address_line_1'] = $this->common_vars["billing_address1"]["value"];
			$postDataArray['address_line_2'] = $this->common_vars["billing_address2"]["value"];
			$postDataArray['city'] = $this->common_vars["billing_city"]["value"];
			$postDataArray['state'] = $this->common_vars["billing_state_abbr"]["value"] != "" ? $this->common_vars["billing_state_abbr"]["value"] : $this->common_vars["billing_state"]["value"];
			$postDataArray['country'] = $this->common_vars["billing_country_iso_a2"]["value"];
			$postDataArray['zip'] = $this->common_vars["billing_zip"]["value"];
			$postDataArray['phone'] = preg_replace('/[^\d\s]/', '', $this->common_vars["billing_phone"]["value"]);

			if ($order->getDiscountAmount() > 0 || $order->getPromoDiscountAmount() > 0)
			{
				//FuturePay doesn't support discounts, just put it all together on one line
				$totalDiscount = $order->getDiscountAmount() + $order->getPromoDiscountAmount();

				$postDataArray['sku'][] = 'order';
				$postDataArray['price'][] = $order->getSubtotalAmount() - $totalDiscount;
				$postDataArray['tax_amount'][] = $order->getTaxAmount();
				$postDataArray['description'][] = 'Invoice Subtotal';
				$postDataArray['quantity'][] = 1;
			}
			else
			{
				/** @var Model_LineItem $item */
				foreach ($order->lineItems as $item)
				{
					$postDataArray['sku'][] = trim($item->getProductSku()) != '' ? $item->getProductSku() : $item->getProductId();
					$postDataArray['price'][] = $item->getFinalPrice();
					$postDataArray['tax_amount'][] = $item->getTaxAmount();
					$postDataArray['description'][] = $item->getTitle();
					$postDataArray['quantity'][] = $item->getFinalQuantity();
				}
			}

			if ($order->getShippingAmount() > 0)
			{
				$shippingMethod = 'Undefined';
				$orderShipments = $order->getShipments();

				foreach ($orderShipments as $orderShipment)
				{
					$shippingMethod = $orderShipment['shipping_cm_name'];
				}

				$postDataArray['sku'][] = 'shipping';
				$postDataArray['price'][] = $order->getShippingAmount();
				$postDataArray['tax_amount'][] = '0.00';
				$postDataArray['description'][] = $shippingMethod;
				$postDataArray['quantity'][] = '1';
			}

			// Shipping info, must be US only, no p.o.boxes
			if ($order->getShippingRequired())
			{
				$postDataArray['shipping_address_1'] = $this->common_vars['shipping_address1']['value'];
				$postDataArray['shipping_address_2'] = $this->common_vars['shipping_address2']['value'];
				$postDataArray['shipping_city'] = $this->common_vars['shipping_city']['value'];
				$postDataArray['shipping_state'] = $this->common_vars['shipping_state_abbr']['value'];
				$postDataArray['shipping_country'] = $this->common_vars['shipping_country_iso_a2']['value'];
				$postDataArray['shipping_zip'] = $this->common_vars['shipping_zip']['value'];
			}

			return $this->doCall('merchant-request-order-token', $postDataArray);
		}

		function getPaymentForm($db)
		{
			$fields = array();

			return $fields;
		}

		/** @var ORDER $order */
		public function process($db, $user, $order, $post_form)
		{
			$this->log('Process Request:', $post_form);

			if (isset($post_form['status']) && ($post_form['status'] == 'OK' || $post_form['status'] == 'SIGNUP_OK_AND_PURCHASED'))
			{
				$postDataArray = array(
					'otxnid' => $post_form['transaction_id'],
				);

				$response = $this->doCall('merchant-order-verification', $postDataArray);

				$response = @json_decode($response);

				if (isset($response->OrderStatusCode) && $response->OrderStatusCode == 'ACCEPTED')
				{
					$extra = '';

					$custom1 = 'AUTH_CAPTURE';
					$custom2 = '';
					$custom3 = $order->getTotalAmount();

					$this->createTransaction($db, $user, $order, $post_form, $extra, $custom1, $custom2, $custom3, $post_form['transaction_id']);
					$this->is_error = false;
					$this->error_message = "";

					return 'Received';
				}
				else
				{
					$this->is_error = true;
					$this->error_message = 'We are sorry, but the payment authorization request has invalid data.';

					$this->storeTransaction($db, $user, $order, $post_form, '', false);
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = $post_form['message'];

				$this->storeTransaction($db, $user, $order, $post_form, '', false);
			}

			return false;
		}

		public function getJS()
		{
			$orderToken = trim($this->getOrderToken());

			if (substr($orderToken, 0, 4) == 'FPTK')
			{
				global $settings, $order;
				$processUrl = $settings["GlobalHttpsUrl"]."/content/engine/payment/futurepay/callback.php?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&oid=".$order->getId();
				$completedUrl = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=completed";

				return '<script type="text/javascript">
				$("#opc-button-complete").attr("disabled", "disabled").addClass("button-disabled");
				$.getScript("'.$this->getGatewayUrl().'cart-integration/'.$orderToken.'", function() {
					pc_initFuturePay();
				});
				var pc_fp_loaded;
				function pc_initFuturePay(){
					clearTimeout(pc_fp_loaded);

					if (typeof(FP.CartIntegration) == "undefined")
					{
						pc_fp_loaded = setTimeout(function() { pc_initFuturePay(); }, 500);
						return;
					}

					FP.CartIntegration();
					$("#opc-payment-method-form-card").html(FP.CartIntegration.getFormContent());
					FP.CartIntegration.displayFuturePay();

					$("#opc-button-complete").unbind("click").click(function() {
						$("#opc-button-complete").removeAttr("disabled").removeClass("button-disabled");
						FP.CartIntegration();
						FP.CartIntegration.placeOrder();
					});
				}
				function FuturePayResponseHandler(response){
					if (response.error){
						$.post("'.$processUrl.'", response, function(){
							$("#opc-payment-method-error-message").html(response.message);
							$("#opc-payment-method-error").slideDown("fast");
							$("#opc-button-complete").attr("disabled", "disabled").addClass("button-disabled");
						})
						.fail(function(data){
							$("#opc-payment-method-error-message").html(data.message);
							$("#opc-payment-method-error").slideDown("fast");
							$("#opc-button-complete").attr("disabled", "disabled").addClass("button-disabled");
						});
					} else {
						onePageCheckout.showSpinner("Saving data...");
						$.post("'.$processUrl.'", response, function(){
							document.location = "'.$completedUrl.'";
						});
					}
				}
				</script>
				';
			}
			else
			{
				$message = '';

				switch ($orderToken)
				{
					case 'FP_NO_ZIP_FOUND':
					case 'FP_ZIP_EXCEPTION':
						$message = 'We are sorry, but a valid residential US zip code is required.';
						break;
					case 'FP_MISSING_REQUIRED_PHONE':
						$message = 'We are sorry, but a phone number is required.';
						break;
					case 'FP_INVALID_EMAIL_FORMAT':
						$message = 'We are sorry, but your email address appears to be invalid.';
						break;
					case 'FP_COUNTRY_US_ONLY':
						$message = 'We are sorry, but FuturePay only accepts payments from US based customers.  Please choose another payment method.';
						break;
					case 'FP_INVALID_ID_REQUEST':
					case 'FP_MISSING_ORDER_ITEM_FIELDS':
					case 'FP_INVALID_PRICE':
					case 'FP_INVALID_TAX':
					case 'FP_INVALID_QUANTITY':
					case 'FP_INVALID_TOTAL_TRANSACTION_AMOUNT':
						$message = 'We are sorry, but the payment authorization request has invalid data.';
						break;
					case 'FP_PRE_ORDER_EXCEEDS_MAXIMUM':
					case 'FP_PRE_ORDER_EXCEEDS_CREDIT_AVAILABLE':
						$message = 'This order exceeds the maximum available amount on your FuturePay account.  Please adjust your order, or choose another form of payment.';
						break;
					default:
						$message = 'There was a problem creating your order.  No charges have been applied to your FuturePay tab.  Please contact FuturePay at support@futurepay.com.';
						break;
				}

				return '<script type="text/javascript">
				$("#opc-button-complete").attr("disabled", "disabled").addClass("button-disabled");
				$("#opc-payment-method-error-message").html("'.str_replace('"', '\\"', $message).'");
				$("#opc-payment-method-error").slideDown("fast");
</script>';
			}
		}

		public function currentOrderPaymentStatus($order_data = false)
		{
			if ($order_data)
			{
				return $order_data['custom1'];
			}
			return false;
		}

		public function supportsRefund($order_data = false)
		{
			if ($order_data)
			{
				if ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Partial')
				{
					return true;
				}
				else if ($order_data['payment_status'] == 'Refunded')
				{
					return $this->_refundableAmount($order_data) > 0.0;
				}
			}
			return false;
		}

		public function supportsPartialRefund($order_data = false)
		{
			return true;
		}

		public function refundableAmount($order_data = false)
		{
			if ($order_data)
			{
				if ($this->supportsRefund($order_data))
				{
					return $this->_refundableAmount($order_data);
				}
			}
			return 0;
		}

		function getTotalCaptured($order_data)
		{
			return 0.00 + $order_data['custom3'];
		}

		function _refundableAmount($order_data)
		{
			$refundableAmount = $this->getTotalCaptured($order_data);
			return number_format($refundableAmount, 2, '.', '');
		}

		public function refund($order_data = false, $refund_amount = false)
		{
			global $db, $order;

			$user = $order->getUser();

			$oldPaymentStatus = $order->getPaymentStatus();

			$this->is_error = false;

			if ($order_data)
			{
				if (!$this->supportsRefund($order_data))
				{
					$this->is_error = true;
					$this->error_message = 'Order cannot be refunded';
					return false;
				}

				//get captured total amount & x_trans_id
				$refund_total_amount = $refund_amount;

				if (!$refund_total_amount)
				{
					$refund_total_amount = $order_data['custom3'];
				}

				if ($refund_total_amount < 0)
				{
					$this->is_error = true;
					$this->error_message = 'Amount to refund must be greater than 0';
					return false;
				}
				else if ($refund_total_amount > $order_data['custom3'] || $refund_total_amount > $order_data['total_amount'])
				{
					$this->is_error = true;
					$this->error_message = 'Amount to refund cannot be more than total captured amount';
					return false;
				}

				$transaction_id = $order_data['security_id'];

				if ($transaction_id == '')
				{
					$this->is_error = true;
					$this->error_message = 'This order does not have a valid transaction id';
					return false;
				}
				else
				{
					$postDataArray = array(
						'reference' => 'ORDER'.$order_data['order_num'],
						'total_price' => $refund_amount,
					);

					$response = $this->doCall('merchant-returns', $postDataArray);

					$response = @json_decode($response);

					if ($response)
					{
						$user = $order->getUser();

						if (isset($response->STATUS) && $response->STATUS == 'FP_REFUND_SUCCESSFUL')
						{
							$extra = '';
							$totalCaptured = $this->getTotalCaptured($order_data) - $refund_total_amount;
							$custom3 = number_format($totalCaptured, 2, '.', '');

							$db->reset();
							$db->assignStr('custom1', '');
							$db->assignStr('custom2', '');
							$db->assignStr('custom3', $custom3);

							$security_id = $transaction_id;

							$order->setPaymentStatus(ORDER::PAYMENT_STATUS_REFUNDED);

							$db->assignStr('payment_status', $order->getPaymentStatus());
							$db->update(DB_PREFIX.'orders', "WHERE oid='".$order_data['oid']."'");

							$order_data['custom1'] = 'REFUND';
							$order_data['custom2'] = '';
							$order_data['custom3'] = $custom3;
							$order_data['payment_status'] = $order->getPaymentStatus();

							$custom1 = $order_data['custom1'];
							$custom2 = $order_data['custom2'];

							$this->createTransaction($db, $user, $order,
								(array)$response,
								$extra,
								$custom1,
								$custom2,
								$custom3,
								$security_id
							);

							$this->fireOrderEvent($order, $order->getStatus(), $oldPaymentStatus);

							return true;
						}
						else
						{
							$this->is_error = true;
							$this->error_message = 'We are sorry, but there has been an error processing this transaction.'.(isset($response->STATUS) ? '<br>'.$response->STATUS : '');

							$this->storeTransaction($db, $user, $order, (array)$response, '', false);

							return false;
						}
					}
				}
			}

			return !$this->is_error;
		}
	}

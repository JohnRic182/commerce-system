<?php

	require_once('content/vendors/stripe/Stripe.php');

	$payment_processor_id = "stripe";
	$payment_processor_name = "Stripe";
	$payment_processor_class = "PAYMENT_STRIPE";
	$payment_processor_type = "cc";

class PAYMENT_STRIPE extends PAYMENT_PROCESSOR
{
	const MODE_AUTH_ONLY = 'Auth only';
	const MODE_AUTH_CAPTURE = 'Capture';

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->id = 'stripe';
		$this->name = 'Stripe';
		$this->class_name = 'PAYMENT_STRIPE';
		$this->type = 'cc';
		$this->description = '';
		$this->steps = 1;
		$this->testMode = false;
		$this->support_ccs = false;
		$this->need_cc_codes = false;
		$this->support_accepted_cc = false;
	}

	public function replaceDataKeys()
	{
		return array('cc_number', 'cc_cvv2',);
	}

	/**
	 * Get custom js
	 *
	 * @return string
	 */
	public function getCheckoutPageJS()
	{
		$js = parent::getCheckoutPageJS();

		$js .=
			'<script type="text/javascript" src="https://js.stripe.com/v2/"></script>'."\n".
			'<script type="text/javascript">'."\n".
				'var stripePublishableKey="'.htmlspecialchars($this->settings_vars[$this->id.'_ApiPublishableKey']['value']).'"'."\n".
			'</script>'."\n".
			'<script type="text/javascript" src="content/engine/payment/stripe/opc.js"></script>'."\n";

		return $js;
	}

	/**
	 * Get payment form fields
	 *
	 * @param $db
	 *
	 * @return array
	 */
	public function getPaymentForm($db)
	{
		$fields = parent::getPaymentForm($db);

		$fields['cc_cardholder_name'] = array('type' => 'hidden', 'name' => 'form[cc_cardholder_name]', 'value' => $this->common_vars['billing_name']['value']);
		$fields['cc_address_line1'] = array('type' => 'hidden', 'name' => 'form[cc_address_line1]', 'value' => $this->common_vars['billing_address1']['value']);
		$fields['cc_address_line2'] = array('type' => 'hidden', 'name' => 'form[cc_address_line2]', 'value' => $this->common_vars['billing_address2']['value']);
		$fields['cc_address_city'] = array('type' => 'hidden', 'name' => 'form[cc_address_city]', 'value' => $this->common_vars['billing_city']['value']);
		$fields['cc_address_state'] = array('type' => 'hidden', 'name' => 'form[cc_address_state]', 'value' => $this->common_vars['billing_state_abbr']['value']);
		$fields['cc_address_zip'] = array('type' => 'hidden', 'name' => 'form[cc_address_zip]', 'value' => $this->common_vars['billing_zip']['value']);
		$fields['cc_address_country'] = array('type' => 'hidden', 'name' => 'form[cc_address_country]', 'value' => $this->common_vars['billing_country_iso_a2']['value']);

		return $fields;
	}

	/**
	 * Get validator js
	 *
	 * @return string
	 */
	public function getValidatorJS()
	{
		$validatorJS = parent::getValidatorJS();
		return $validatorJS."\n	stripeValidatePaymentForm();\n";
	}

	/**
	 * Convert response to text
	 *
	 * @param $charge
	 *
	 * @return string
	 */
	protected function chargeToText($charge = null)
	{
		return print_r($charge, 1);
	}

	/**
	 * Set exception error
	 *
	 * @param Exception $e
	 */
	protected function setExceptionError(Exception $e)
	{
		$this->is_error = true;
		$this->error_message = $e->getMessage();
	}

	/**
	 * Checks does payment gateway support capture for current order
	 * @param mixed $order_data
	 * @return boolean
	 */
	public function supportsCapture($order_data = false)
	{
		return true;
	}

	/**
	 * @param $order_data
	 * @return bool
	 */
	protected  function isTotalAmountCaptured($order_data)
	{
		return $order_data['custom3'] == $order_data['total_amount'];
	}


	/**
	 * Captures funds
	 *
	 * @param bool $order_data
	 * @param bool $capture_amount
	 *
	 * @return bool
	 */
	public function capture($order_data = false, $capture_amount = false)
	{
		if ($order_data)
		{
			//check is transaction completed
			if ($this->isTotalAmountCaptured($order_data))
			{
				$this->is_error = true;
				$this->error_message = 'Transaction already completed';
				return false;
			}

			$this->processAdminAction('capture', $order_data = false, $capture_amount = false);
		}
	}

	/**
	 * Checks does payment gateway supports refund for current order
	 *
	 * @param mixed $order_data
	 *
	 * @return boolean
	 */
	public function supportsRefund($order_data = false)
	{
		return true;
	}

	/**
	 * Refunds order
	 *
	 * @param bool $order_data
	 * @param bool $refund_amount
	 *
	 * @return bool
	 */
	public function refund($order_data = false, $refund_amount = false)
	{
		$this->processAdminAction('refund', $order_data = false, $capture_amount = false);
	}


	/**
	 * Process admin action
	 * @param bool|mixed $action
	 * @param bool $order_data
	 * @param bool $capture_amount
	 * @return bool|string
	 */
	protected function processAdminAction($action, $order_data = false, $capture_amount = false)
	{
		$this->log("process Admin Request:\n", array('action' => $action, 'capture_amount' => $capture_amount));

		Stripe::setApiKey(
			$this->settings_vars[$this->id.'_ApiSecretKey']['value']
		);

		Stripe::setVerifySslCerts(false);

		$token = $order_data['custom2'];

		// Create the capture on Stripe's servers - this will charge the user's card
		try
		{
			/** @var $capture */
			$charge = Stripe_Charge::retrieve($token);
			if ($action == 'capture')
			{
				$charge->capture();
			}
			else if ($action == 'refund')
			{
				$charge->capture();
			}

			$responseText = $this->chargeToText($charge);

			$this->log("process Admin Response:\n", $responseText);

			$user = new tmp_object();
			$user->id = $order_data['uid'];

			$order = new tmp_object();
			$order->oid = $order_data['oid'];
			$order->subtotalAmount = $order_data['subtotal_amount'];
			$order->totalAmount = $order_data['total_amount'];
			$order->gift_cert_amount = $order_data['gift_cert_amount'];
			$order->shippingAmount = $order_data['shipping_amount'];
			$order->taxAmount = $order_data['tax_amount'];

			$this->createTransaction($db, $user, $order, $responseText, '', '', $token);

			return 'Received';
		}
		/**
		 * Handle errors
		 */
		catch (Stripe_ApiConnectionError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_ApiError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_AuthenticationError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_CardError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_InvalidRequestError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_Error $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Exception $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
	}


	/**
	 * Process payment
	 *
	 * @param $db
	 * @param $user
	 * @param $order
	 * @param $post_form
	 *
	 * @return bool|void
	 */
	public function process($db, $user, $order, $post_form)
	{
		$post_values = $post_form;
		$this->log("process Request:\n", $post_values);

		Stripe::setApiKey(
			$this->settings_vars['stripe_ApiSecretKey']['value']
		);

		Stripe::setVerifySslCerts(false);

		$token = isset($post_form['stripeToken']) ? $post_form['stripeToken'] : '';

		$authMode = self::MODE_AUTH_CAPTURE;

		// Create the charge on Stripe's servers - this will charge the user's card
		try
		{
			/** @var $charge */
			$charge = Stripe_Charge::create(array(
				'amount' => number_format($this->common_vars['order_total_amount']['value'], intval($this->currency['decimal_places']), '', ''),
				'currency' => $this->currency['code'],
				'card' => $token,
				'description' => 'Order #'.$this->common_vars['order_id']['value'],
				'capture' => $authMode == self::MODE_AUTH_CAPTURE,
				'metadata' => array('order_id' => $this->common_vars['order_id']['value'])
			));

			$responseText = $this->chargeToText($charge);

			$this->log("process Response:\n", $responseText);

			$transactionId = $charge['id'];

			$this->createTransaction($db, $user, $order, $responseText, '', $authMode, $token, $authMode == self::MODE_AUTH_CAPTURE ? $this->common_vars['order_total_amount']['value'] : '0.00', $transactionId);

			return 'Received';
		}
		/**
		 * Handle errors
		 */
		catch (Stripe_ApiConnectionError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_ApiError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_AuthenticationError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_CardError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_InvalidRequestError $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Stripe_Error $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
		catch (Exception $e)
		{
			$this->log("process Exception:\n", $e->getMessage());
			$this->setExceptionError($e);
			return false;
		}
	}
}
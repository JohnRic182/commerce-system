<?php 

$payment_processor_id = "worldpay";
$payment_processor_name = "WorldPay.com";
$payment_processor_class = "PAYMENT_WORLDPAY";
$payment_processor_type = "ipn";

class PAYMENT_WORLDPAY extends PAYMENT_PROCESSOR
{
	public function PAYMENT_WORLDPAY()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "worldpay";
		$this->name = "WorldPay.com";
		$this->class_name = "PAYMENT_WORLDPAY";
		$this->type = "ipn";
		$this->description = "";
		$this->post_url = "https://select.worldpay.com/wcc/purchase";
		$this->steps = 2;
		$this->testMode = false;
		return $this;
	}

	public function getPaymentForm($db)
	{
		global $settings;

		$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;
		$return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;

		$fields = array();
		$fields[] = array("type"=>"hidden", "name"=>"instId", "value"=>$this->settings_vars[$this->id."_instId"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"currency", "value"=>$this->settings_vars[$this->id."_currency"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"desc", "value"=>"Order #".$this->common_vars["order_id"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"cartId", "value"=>$this->common_vars["order_id"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"amount", "value"=>$this->common_vars["order_total_amount"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"name", "value"=>$this->common_vars["billing_name"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"address", "value"=>trim($this->common_vars["billing_address"]["value"]));
		$fields[] = array("type"=>"hidden", "name"=>"postcode", "value"=>$this->common_vars["billing_zip"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"country", "value"=>$this->common_vars["billing_country_iso_a2"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"tel", "value"=>$this->common_vars["billing_phone"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"email", "value"=>$this->common_vars["billing_email"]["value"]);
		$fields[] = array("type"=>"hidden", "name"=>"MC_callback", "value"=>$notify_url);

		if ($this->settings_vars[$this->id."_testMode"]["value"] != "Live")
		{
			$fields[] = array("type"=>"hidden", "name"=>"testMode", "value"=>($this->settings_vars[$this->id."_testMode"]["value"] == "Succeed"?"100":"101"));
		}

		return $fields;
	}

	public function getValidatorJS()
	{
		return "";
	}

	public function getPostUrl()
	{
		if (trim($this->url_to_gateway) != "")
		{
			return trim($this->url_to_gateway);
		}
		else
		{
			return $this->post_url;
		}
	}

	public function isTestMode()
	{
		if ($this->settings_vars[$this->id."_testMode"]["value"] != "Live")
		{
			$this->testMode = true;
			$this->post_url = "https://select-test.wp3.rbsworldpay.com/wcc/purchase";
		}
		else{
			$this->testMode = false;
			$this->post_url = "https://select.worldpay.com/wcc/purchase";
		}
		return $this->testMode;
	}

	public function process($db, $user, $order, $post_form)
	{
		$this->log("Response:", $post_form);

		if (array_key_exists("transStatus", $post_form))
		{
			//check other stuff
			if (($post_form["transStatus"] == "Y") && ($post_form["amount"] == $order->totalAmount) && ($post_form["cartId"] == $order->order_num))
			{
				$this->createTransaction($db, $user, $order, $post_form);
				$this->is_error = false;
				$this->error_message = "";
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "Transaction cancelled. Please try again.";
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = "Transaction cancelled. Please try again.";
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $post_form, '', false);
		}
		return !$this->is_error;
	}

	function success()
	{
		global $settings;
		$return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=completed";
		echo '<meta http-equiv="refresh" content="0;url='.$return_url.'"/>';
		_session_write_close();
		die();
	}
}

<?php

$payment_processor_id = "american_express";
$payment_processor_name = "American Express";
$payment_processor_class = "PAYMENT_AMERICAN_EXPRESS";
$payment_processor_type = "cc";

class PAYMENT_AMERICAN_EXPRESS extends PAYMENT_PROCESSOR
{
	function PAYMENT_AMERICAN_EXPRESS()
	{
		global $settings,$db;

		parent::PAYMENT_PROCESSOR();
		$this->id = "american_express";
		$this->name = "American Express";
		$this->class_name = "PAYMENT_AMERICAN_EXPRESS";
		$this->type = "cc";
		$this->description = "";
		$this->steps = 1;
		$this->testMode = false;
		$this->rest_path = '/api/rest/version/9';
		$this->process_success = false;
		$this->curl_handle = false;
		$this->gateway_sessionid = false;
		$this->hosted_form = false;

		$this->setting_var_names = array(
			'auth_type' => '_Auth_Type',
			'auth_type_select' => array(
					'auth' => 'Auth-Only',
					'pay' => 'Auth-Capture',
				),
			'api_password' => '_API_Password',
			'merchand_id' => '_Merchant_ID',
			'payment_profiles' => '_Payment_Profiles',
			'payment_profiles_select' => array(
					'yes' => 'Yes',
					'No'  => 'No',
				),
			'token_generation' => '_Token_Generation',
			'token_generation_select' =>array(
					'american_express' => 'American Express',
					'local'  => 'Local',
				),
		);

		$col_backup = $db->col;

		$db->selectOne("select * from ".DB_PREFIX."payment_methods WHERE id='".($this->id)."'");
		$this->pid = $db->col['pid'];

		$this->getSettingsData($db);

		$db->col = $col_backup;

		return $this;
	}

	public function getPostUrl()
	{
		return parent::getPostUrl();
	}

	public function createSession()
	{
		$this->getSettingsData($db);

		$response = $this->sendRequest(array(),'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/session','POST');

		$this->log('Create Session Response: '."\n\n".array2text($response));

		$this->process_success = !empty($response) && $response['result'] == 'SUCCESS';

		if($this->process_success)
		{
			$this->gateway_sessionid = $response['session'];

			return $this->gateway_sessionid;
		}
		else
		{
			$this->gateway_sessionid = false;

			return false;
		}
	}

	public function currentOrderPaymentStatus($order_data = false)
	{
		if(!$order_data) return '';

		$order_action_totals = unserialize($order_data['custom2']);

		if($order_data['custom1'] == 'VOID')
			return 'Voided';

		if($order_action_totals['refunded_amount'] == $order_data['total_amount'])
			return 'Refunded';

		if($order_action_totals['captured_amount'] == $order_data['total_amount'])
			return 'Captured';

		if($order_data['custom1'] == 'AUTHORIZE')
			return 'Authorized';

		return '';
	}

	public function getPaymentForm($db)
	{
		global $settings, $user;

		$fields = parent::getPaymentForm($db);

		if (
			$this->settings_vars[$this->id.$this->setting_var_names['payment_profiles']]['value'] == $this->setting_var_names['payment_profiles_select']['yes'] &&
			isset($this->user) && !is_null($this->user) &&
			isset($this->user->auth_ok) && $this->user->auth_ok &&
			(!isset($this->user->express) || !$this->user->express)
		)
		{
			$fields['save_credit_card'] = array("type"=>"checkbox", "name"=>"form[save_credit_card]", "value"=>"Y","caption" => "Save credit card for later use:");
		}

		return $fields;
	}

	public function getValidatorJS()
	{
		return parent::getValidatorJS();
	}

	function mapValues($destKey, $sourceKey,&$dest,$source)
	{
		if (isset($source[$sourceKey]) && !empty($source[$sourceKey]))
			$dest[$destKey] = $source[$sourceKey];
	}

	public function replaceDataKeys()
	{
		return array('sourceOfFunds.provided.card.number', 'sourceOfFunds.provided.card.securityCode');
	}

	public function process($db, $user, $order, $post_form)
	{
		$this->getSettingsData($db);

		if(isset($post_form['form']) && is_array($post_form['form']))
			$post_form = array_merge($post_form,$post_form['form']);

		if($this->settings_vars[$this->id.$this->setting_var_names['auth_type']]['value'] == $this->setting_var_names['auth_type_select']['auth'])
		{
			$process_type = 'AUTHORIZE';
		}
		else
		{
			$process_type = 'PAY';
		}


		$request_obj = array();

		$request_obj['apiOperation'] = $process_type;

		$billing = array();
		$billing['address'] = array();

		$this->mapValues('city','city',$billing['address'],$user->data);
		$this->mapValues('country','country_iso_a3',$billing['address'],$user->data);
		$this->mapValues('postcodeZip','zip',$billing['address'],$user->data);
		$this->mapValues('stateProvince','state_name',$billing['address'], $user->data);
		$this->mapValues('street','address1',$billing['address'], $user->data);

		if(empty($billing['address'])) unset($billing['address']);

		if(!empty($billing))
			$request_obj['billing'] = $billing;

		$sourceOfFunds = array();

		if(isset($post_form['payment_method_way']) && $post_form['payment_method_way'] == 'profile' && isset($post_form['ext_profile_id']))
		{
			$sourceOfFunds['token'] = $post_form['ext_profile_id'];
		}
		else
		{
			$provided = array();
			$provided['card'] = array();

			$provided['card']['number'] = strval($post_form['cc_number']);
			$provided['card']['securityCode'] = strval($post_form['cc_cvv2']);

			$provided['card']['expiry'] = array();
			$provided['card']['expiry']['month'] = strval(intval($post_form['cc_expiration_month']));
			$provided['card']['expiry']['year'] = substr($post_form['cc_expiration_year'],-2);

			$provided['card']['holder'] = array();
			$provided['card']['holder']['firstName'] = $post_form['cc_first_name'];
			$provided['card']['holder']['lastName'] = $post_form['cc_last_name'];

			$sourceOfFunds['provided'] = $provided;
		}

		$sourceOfFunds['type'] = 'CARD';

		$request_obj['sourceOfFunds'] = $sourceOfFunds;

		$transaction = array();
		$transaction['amount']    = number_format($order->totalAmount,2,'.','');
		$transaction['currency']  = $this->currency['code'];
		$transaction['frequency']  = 'SINGLE';
		$transaction['source']    = 'INTERNET';

		$request_obj['transaction'] = $transaction;

		$order_id = mt_rand(10000000000,9999999999999999);
		$transaction_id = uniqid();

		$response = $this->sendRequest($request_obj,'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/order/'.$order_id.'/transaction/'.$transaction_id);
		$this->process_success = $this->_processTransactionResponse($response);

		if($this->process_success)
		{
			$orderids = array(
				'order_id' => $order_id,
				'transactions' => array(
					array(
						'transaction_id' => $transaction_id,
						'action' => $process_type,
					),
				),
			);

			$orderids = serialize($orderids);

			if($process_type == 'PAY')
			{
				$order_action_totals = array(
					'captured_amount' => $order->totalAmount,
					'refunded_amount' => 0,
				);
			}
			else
			{
				$order_action_totals = array(
					'captured_amount' => 0,
					'refunded_amount' => 0,
				);
			}

			$this->createTransaction($db, $user, $order, $response, '', $process_type, serialize($order_action_totals), $orderids, $transaction_id);

			if(
				(!isset($post_form['payment_method_way']) || $post_form['payment_method_way'] != 'profile') &&
				isset($post_form['save_credit_card']) &&
				$post_form['save_credit_card'] == 'Y' &&
				$this->settings_vars[$this->id.$this->setting_var_names['payment_profiles']]['value'] == $this->setting_var_names['payment_profiles_select']['yes']
			) {
				$values = array(
					'cc_number' => $post_form['cc_number'],
					'cc_expiration_month' => $post_form['cc_expiration_month'],
					'cc_expiration_year' => $post_form['cc_expiration_year'],
				);
				$values = array_merge($values, $user->data);

				$billingData = new PaymentProfiles_Model_BillingData($values);
				$profile = new PaymentProfiles_Model_PaymentProfile();
				$profile->setBillingData($billingData);

				$this->addPaymentProfile($db, $user, $profile);

				$this->getPaymentProfileRepository()->persist($profile);
			}
		}

		return  $this->process_success ? ($process_type == 'PAY' ? 'Received' : 'Pending') : false;
	}

	public function success()
	{
		return $this->process_success;
	}

	public function supportsVoid($order_data = false)
	{
		if(!$order_data) return false;

		$orderids = unserialize($order_data['custom3']);

		if(count($orderids['transactions']) == 1 && ($orderids['transactions'][0]['action'] == 'PAY' || $orderids['transactions'][0]['action'] == 'AUTHORIZE'))
		{
			return true;
		}

		if($orderids['transactions'][count($orderids['transactions'])-1]['action'] == 'CAPTURE')
		{
			return true;
		}

		return false;
	}

	public function void($order_data = false)
	{
		global $db;

		if(!$order_data) return false;

		$this->getSettingsData($db);

		$orderids = unserialize($order_data['custom3']);
		$order_id = $orderids['order_id'];
		$target_transaction_id = $orderids['transactions'][count($orderids['transactions']) - 1]['transaction_id'];

		$request_obj = array();
		$request_obj['apiOperation'] = 'VOID';

		$transaction = array();
		$transaction['targetTransactionId'] = $target_transaction_id;

		$request_obj['transaction'] = $transaction;

		$transaction_id = uniqid();

		$response = $this->sendRequest($request_obj,'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/order/'.$order_id.'/transaction/'.$transaction_id);
		$this->process_success = $this->_processTransactionResponse($response);

		if($this->process_success)
		{
			$orderids['transactions'][] = array(
			'transaction_id' => $transaction_id,
			'action' 	 => 'VOID',
			);

			$user = new tmp_object();
			$user->id = $order_data["uid"];

			$order = new tmp_object();
			$order->oid = $order_data["oid"];
			$order->subtotalAmount = $order_data["subtotal_amount"];
			$order->totalAmount = $order_data["total_amount"];
			$order->shippingAmount = $order_data["shipping_amount"];
			$order->taxAmount = $order_data["tax_amount"];
			$order->gift_cert_amount = $order_data["gift_cert_amount"];

			$this->createTransaction($db, $user, $order, $response, '', 'VOID', $order_data['custom2'], serialize($orderids), $transaction_id);
		}

		return $this->process_success;
	}

	public function supportsRefund($order_data = false)
	{
		if(!$order_data) return false;

		$order_action_totals = unserialize($order_data['custom2']);

		return ('PAY' == $order_data['custom1'] || 'AUTHORIZE' == $order_data['custom1']) && $order_action_totals['captured_amount'] > $order_action_totals['refunded_amount'];
	}

	public function supportsPartialRefund($order_data = false)
	{
		return true;
	}

	public function refundableAmount($order_data = false)
	{
		$order_action_totals = unserialize($order_data['custom2']);

		return $order_action_totals['captured_amount'] - $order_action_totals['refunded_amount'];
	}

	public function refund($order_data = false, $amount = false)
	{
		global $db;

		if(!$order_data || !is_numeric($amount)) return false;

		$order_action_totals = unserialize($order_data['custom2']);

		$this->getSettingsData($db);

		$amount = $order_action_totals['captured_amount'];

		if($order_action_totals['captured_amount'] - $order_action_totals['refunded_amount'] <  $amount)
		{
			$this->is_error = true;
			$this->error_message = 'The refundable amount left is '.$this->currency['symbol_left'].number_format($order_action_totals['captured_amount'] - $order_action_totals['refunded_amount'],'2','.','').$this->currency['symbol_right'].'.  You attempted to capture '.$this->currency['symbol_left'].number_format($amount,'2','.','').$this->currency['symbol_right'];

			return false;
		}

		if($amount <= 0)
		{
			$this->is_error = true;
			$this->error_message = 'Refund amount must be larger then zero.';

			return false;
		}

		$orderids = unserialize($order_data['custom3']);

		$request_obj = array();

		$request_obj['apiOperation'] = 'REFUND';

		$transaction = array();
		$transaction['amount']    = number_format($amount,2,'.','');
		$transaction['currency']  = $this->currency['code'];
		$transaction['source']    = 'INTERNET';

		$request_obj['transaction'] = $transaction;

		$transaction_id = uniqid();

		$response = $this->sendRequest($request_obj,'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/order/'.$orderids['order_id'].'/transaction/'.$transaction_id);
		$this->process_success = $this->_processTransactionResponse($response);

		if($this->process_success)
		{
			$order_action_totals['refunded_amount'] += $amount;

			$orderids['transactions'][] = array(
			'transaction_id' => $transaction_id,
			'action' 	 => 'REFUND',
			);

			$user = new tmp_object();
			$user->id = $order_data["uid"];

			$order = new tmp_object();
			$order->oid = $order_data["oid"];
			$order->subtotalAmount = $order_data["subtotal_amount"];
			$order->totalAmount = $order_data["total_amount"];
			$order->shippingAmount = $order_data["shipping_amount"];
			$order->taxAmount = $order_data["tax_amount"];
			$order->gift_cert_amount = $order_data["gift_cert_amount"];

			$this->createTransaction($db, $user, $order, $response, '', $order_data['custom1'], serialize($order_action_totals), serialize($orderids), $transaction_id);
		}

		return $this->process_success;
	}

	public function supportsCapture($order_data = false)
	{
		if(!$order_data) return false;

		$order_action_totals = unserialize($order_data['custom2']);

		return $order_data['custom1'] == 'AUTHORIZE' && $order_action_totals['captured_amount'] < $order_data['total_amount'];
	}

	public function capturableAmount($order_data = false)
	{
		if(!$order_data) return false;

		$order_action_totals = unserialize($order_data['custom2']);

		if($order_data['custom1'] == 'AUTHORIZE')
		{
			return $order_data['total_amount'] - $order_action_totals['captured_amount'];
		}

		return 0;
	}

	public function capturedAmount($order_data = false)
	{
		if (!$order_data) return false;

		$order_action_totals = unserialize($order_data['custom2']);

		return $order_action_totals['captured_amount'];
	}

	public function capture($order_data = false,$amount = false)
	{
		global $db;

		if(!$order_data || !is_numeric($amount)) return false;

		$order_action_totals = unserialize($order_data['custom2']);

		$amount = $order_data['total_amount'];

		if($order_data['total_amount'] < $order_action_totals['captured_amount'] + $amount)
		{
			$this->is_error = true;
			$this->error_message = 'The capturable amount left is '.$this->currency['symbol_left'].number_format($order_data['total_amount'] - $order_action_totals['captured_amount'],'2','.','').$this->currency['symbol_right'].'.  You attempted to capture '.$this->currency['symbol_left'].number_format($amount,'2','.','').$this->currency['symbol_right'];

			return false;
		}

		if($amount <= 0)
		{
			$this->is_error = true;
			$this->error_message = 'Capture amount must be larger then zero.';

			return false;
		}

		$this->getSettingsData($db);


		$orderids = unserialize($order_data['custom3']);

		$request_obj = array();

		$request_obj['apiOperation'] = 'CAPTURE';


		$transaction = array();
		$transaction['amount']    = number_format($amount,2,'.','');
		$transaction['currency']  = $this->currency['code'];
		$transaction['source']    = 'INTERNET';

		$request_obj['transaction'] = $transaction;

		$transaction_id = uniqid();

		$response = $this->sendRequest($request_obj,'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/order/'.$orderids['order_id'].'/transaction/'.$transaction_id);
		$this->process_success = $this->_processTransactionResponse($response);

		if($this->process_success)
		{
			$db->reset();

			$order_action_totals['captured_amount'] += $amount;


			$orderids['transactions'][] = array(
			'transaction_id' => $transaction_id,
			'action' 	 => 'CAPTURE',
			);

			$user = new tmp_object();
			$user->id = $order_data["uid"];

			$order = new tmp_object();
			$order->oid = $order_data["oid"];
			$order->subtotalAmount = $order_data["subtotal_amount"];
			$order->totalAmount = $order_data["total_amount"];
			$order->shippingAmount = $order_data["shipping_amount"];
			$order->taxAmount = $order_data["tax_amount"];
			$order->gift_cert_amount = $order_data["gift_cert_amount"];

			$this->createTransaction($db, $user, $order, $response, '', 'AUTHORIZE', serialize($order_action_totals), serialize($orderids), $transaction_id);
		}

		return $this->process_success;
	}

	public function supportsPaymentProfiles()
	{
		return $this->settings_vars[$this->id.$this->setting_var_names['payment_profiles']]['value'] == $this->setting_var_names['payment_profiles_select']['yes'];
	}

	function getPaymentProfileDetails($db, $ext_profile_id, $active = false)
	{
		$db->selectOne("select * from  ".DB_PREFIX."users_payment_profiles where ext_profile_id = '$ext_profile_id'");

		$response = $this->sendRequest('','/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/token/'.$ext_profile_id,'GET');

		if(!$response)
		{
			$this->is_error = true;
			$this->error_message = 'There was an error retreiving your credit card.';

			return null;
		}

		$data = unserialize($db->col['billing_data']);

		$country = getCountryData($db, false, $data['country']);
		$data['country_id'] = $country['coid'];

		$state = getStateData($db, false, $data['state']);
		$data['state_id'] = $state['stid'];

		$data['name'] = $data['first_name'].' '.$data['last_name'];

		$data['cc_number'] = $response['sourceOfFunds']['provided']['card']['number'];
		$data['cc_expiration_month'] = $response['sourceOfFunds']['provided']['card']['expiry']['month'];
		$data['cc_expiration_year'] = $response['sourceOfFunds']['provided']['card']['expiry']['year'];
		$data['cc_expiration_date'] = $response['sourceOfFunds']['provided']['card']['expiry']['month'].'/'.$response['sourceOfFunds']['provided']['card']['expiry']['year'];

		return $data;
	}

	private function issetReturnVal($array,$key)
	{
		if(is_array($key))
		{
			foreach($key as $k)
			{
				if(!is_array($array) || !isset($array[$k]))
					return '';

				$array = $array[$k];
			}

			return $array;
		}

		return isset($array[$key])?$array[$key]:'';
	}

	/**
	 * @param $post_form
	 * @return bool|string
	 */
	function generateToken($post_form)
	{
		$token = '';

		$request_obj = array();

		$provided = array();
		$provided['card'] = array();

		$provided['card']['number'] = strval($post_form['cc_number']);


		if(isset($post_form['cc_cvv2']))
			$provided['card']['securityCode'] = strval($post_form['cc_cvv2']);

		$provided['card']['expiry'] = array();
		$provided['card']['expiry']['month'] = strval(intval($post_form['cc_expiration_month']));
		$provided['card']['expiry']['year'] = substr($post_form['cc_expiration_year'],-2);

		$sourceOfFunds['provided'] = $provided;

		$sourceOfFunds['type'] = 'CARD';

		$request_obj['sourceOfFunds'] = $sourceOfFunds;


		if($this->settings_vars[$this->id.$this->setting_var_names['token_generation']]['value'] == $this->setting_var_names['token_generation_select']['local'])
		{
			$token = uniqid();
			$response = $this->sendRequest($request_obj,'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/token/'.$token);
		}
		else
			$response = $this->sendRequest($request_obj,'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/token','POST');

		if(!$response)
		{
			$this->is_error = true;
			$this->error_message = 'There was an error storing your credit card.';

			return false;
		}

		if($token != $response['token'])
			$token = $response['token'];

		return $token;
	}

	/**
	 * Create a new payment profile
	 *
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool
	 */
	public function addPaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$paymentProfile->setUserId($user->getId());
		$paymentProfile->setPaymentMethodId($this->pid);

		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		$tokenPostForm = array(
			'cc_number' => $billingData->getCcNumber(),
			'cc_expiration_month' => $billingData->getCcExpirationMonth(),
			'cc_expiration_year' => $billingData->getCcExpirationYear()
		);

		if (($ccCode = $billingData->getCcCode()) != '') $post_form['cc_cvv2'] = $ccCode;

		$token = $this->generateToken($tokenPostForm);

		if (!$token)
		{
			$this->error_message = 'There was an error storing your credit card.';
			$this->is_error = true;

			return false;
		}

		$paymentProfile->setExternalProfileId($token);

		return true;
	}

	/**
	 * Update payment profile
	 *
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool
	 */
	public function updatePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		/** @var PaymentProfiles_Model_BillingData $billingData */
		$billingData = $paymentProfile->getBillingData();

		$tokenPostForm = array(
			'cc_number' => $billingData->getCcNumber(),
			'cc_expiration_month' => $billingData->getCcExpirationMonth(),
			'cc_expiration_year' => $billingData->getCcExpirationYear()
		);

		if (($ccCode = $billingData->getCcCode()) != '') $post_form['cc_cvv2'] = $ccCode;

		$token = $this->generateToken($tokenPostForm);

		if (!$token)
		{
			$this->error_message = 'There was an error updating your credit card.';
			$this->is_error = true;

			return false;
		}

		/**
		 * Delete "remote" payment profile assigned to current "local" payment profile
		 */
		$response = $this->sendRequest(
			'',
			'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/token/'.$paymentProfile->getExternalProfileId(),
			'DELETE'
		);

		// TODO: probably should have warning message here. not an error
		if (!$response)
		{
			$this->error_message = 'There was an error updating your credit card.';
			$this->is_error = true;

			return false;
		}

		$paymentProfile->setExternalProfileId($token);

		return true;
	}

	/**
	 * Remove payment profile
	 *
	 * @param DB $db
	 * @param USER $user
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return bool
	 */
	public function deletePaymentProfile(DB $db, USER $user, PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$response = $this->sendRequest(
			'',
			'/merchant/'.$this->settings_vars[$this->id.$this->setting_var_names['merchand_id']]['value'].'/token/'.$paymentProfile->getExternalProfileId(),
			'DELETE'
		);

		if (!$response)
		{
			$this->error_message = 'There was an error removing your credit card.';
			$this->is_error = true;

			return false;
		}

		return true;
	}

	private function _processTransactionResponse($response)
	{
		if($response['result'] == 'SUCCESS' || $response['result'] == 'PENDING')
		{
			$this->is_error = false;
			$this->error_message = '';

			return true;
		}

		if($response['result'] == 'FAILURE')
		{
			$this->log("Gateway Error: \n".$response['result']."\n".array2text($response['response']));

			if(strpos($response['response']['gatewayCode'],'DECLINED') !== false || $response['response']['gatewayCode'] == 'INVALID_CSC')
			{
				$this->is_error = true;
				$this->error_message = 'Your card was declined.';
			}
			else if($response['response']['gatewayCode'] == 'EXPIRED_CARD')
			{
				$this->is_error = true;
				$this->error_message = 'Your card has expired.';
			}
			else if($response['response']['gatewayCode'] == 'NOT_SUPPORTED')
			{
				$this->is_error = true;
				$this->error_message = 'Your card is not supported by this gateway.';
			}
			else if($response['response']['gatewayCode'] == 'SERVICE_NOT_SUPPORTED')
			{
				$this->is_error = true;
				$this->error_message = 'This gateway is not support in your country.';
			}
			else if($response['response']['gatewayCode'] == 'INSUFFICIENT_FUNDS')
			{
				$this->is_error = true;
				$this->error_message = 'There is insufficent funds are on your card..';
			}
			else
			{
				$this->is_error = true;
				$this->error_message = 'Your order has failed.';
			}
		}

		return false;
	}

	private function sendRequest($data, $request_url_path,$request_method = 'PUT')
	{

		$request_url = $this->url_to_gateway.$this->rest_path.$request_url_path;

		if(!empty($data))
			$json_data = json_encode($data);
		else
			$json_data = '{}';

		$this->log('request url path: '.$request_url_path);
		$this->log('request type: '.$request_method);
		$this->log('json: ', $data);

		$this->curl_handle = curl_init($request_url);

		curl_setopt($this->curl_handle,CURLOPT_CUSTOMREQUEST, $request_method);
		curl_setopt($this->curl_handle,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl_handle,
			CURLOPT_HTTPHEADER,
			array (
				'Content-type: Application/json; charset=UTF-8',
				'Content-Length: '.strlen($json_data),
				"Authorization: Basic ".base64_encode(':'.$this->settings_vars[$this->id.$this->setting_var_names['api_password']]['value']),
			)
		);

		curl_setopt($this->curl_handle,CURLOPT_POSTFIELDS,$json_data);

		$response = curl_exec($this->curl_handle);

		$this->log('response: '.$response);

		$response = json_decode($response,true);

		if(curl_errno($this->curl_handle)) {
			$this->log("Curl Error: \n".curl_error($this->curl_handle));
			$this->is_error = true;
			$this->error_message = "There was an internal issue.  Please contact the Administrator.";

			return false;
		}

		if(isset($response['error']) && $response['error']) {
			$this->log("Gateway Error: \n".array2text($response).' '.$request_url);
			$this->is_error = true;
			$this->error_message = "There was an error: ".$response['error']['explanation'];

			return false;
		}

		return $response;
	}
}

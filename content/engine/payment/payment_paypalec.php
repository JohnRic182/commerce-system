<?php 
	$payment_processor_id = "paypalec";
	$payment_processor_name = "PayPal Exress Checkout";
	$payment_processor_class = "PAYMENT_PAYPALEC";
	$payment_processor_type = "cc";

	class PAYMENT_PAYPALEC extends PAYMENT_PROCESSOR
	{
		protected $api;
		protected $buttonSource = '';

		/**
		 * @return PAYMENT_PAYPALEC
		 */
		public function PAYMENT_PAYPALEC()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "paypalec";
			$this->name = "PayPal Express Checkout";
			$this->class_name = "PAYMENT_PAYPALEC";
			$this->type = "cc";
			$this->description = "";
			$this->steps = 1;
			$this->testMode = false;
			$this->is_certificate = true;
			$this->certificate_optional = true;
			$this->certificate_path = "paypal/certificate.pem";
			$this->support_accepted_cc = false;
			$this->show_cvv2 = false;
			$this->allow_change_gateway_url = false;

			$this->buttonSource = getpp().'_Cart_EC';

			return $this;
		}

		/**
		 * Get payment form
		 * @param type $db
		 * @return string 
		 */
		public function getPaymentForm($db)
		{
			parent::getPaymentForm($db);

			$cc_options_codes = $this->cc_options_codes;
			$fields = array();
			$fields[] = array("type"=>"hidden", "name"=>"oa", "value"=>ORDER_PROCESS_PAYMENT);
			return $fields;
		}

		/**
		 * Get validation javascript
		 * @return string 
		 */
		public function getValidatorJS()
		{
			return "";
		}

		/**
		 * Test mode
		 * @return type 
		 */
		public function isTestMode()
		{
			if (strtolower($this->settings_vars[$this->id."_Mode"]["value"]) == "test")
			{
				$this->testMode = true;
			}
			else
			{
				$this->testMode = false;
			}
			return $this->testMode;
		}

		/**
		 * @param ORDER $order
		 * @param array $cartItems
		 * @param bool $fromCart
		 *
		 * @return array
		 */
		protected function getLineItems($order, $cartItems, $fromCart)
		{
			global $settings, $db;

			$paypalLines = array();

			foreach ($cartItems as $cartItem)
			{
				$paypalLines[] = array(
					'name' => $cartItem['title'],
					'desc' => $cartItem['product_id'] . "\n" . $cartItem['options_clean'],
					'amt' => PriceHelper::round($cartItem['admin_price']),
					'qty' => $cartItem['quantity'],
					'tax' => $fromCart ? 0 : PriceHelper::round($cartItem['tax_amount']),
					'itemcategory' => $cartItem['product_type'] == 'Tangible' ? 'Physical' : 'Digital',
				);
			}

			if ($order->discountAmount > 0)
			{
				$paypalLines[] = array(
					'name' => 'Discount amount',
					'desc' => '',
					'amt' => '-'.number_format($order->discountAmount, 2, '.', ''),
					'qty' => 1,
					'tax' => 0,
					'itemcategory' => 'Physical'
				);
			}

			if ($settings['DiscountsPromo'] == 'YES' && $order->promoDiscountAmount > 0)
			{
				$paypalLines[] = array(
					'name' => 'Promo discount',
					'desc' => '',
					'amt' => '-'.number_format($order->promoDiscountAmount, 2, '.', ''),
					'qty' => 1,
					'tax' => 0,
					'itemcategory' => 'Physical'
				);
			}

			$giftCertificateData = isset($_SESSION["order_gift_certificate"]) ? $_SESSION["order_gift_certificate"] : false;
			if (!$fromCart && $giftCertificateData)
			{
				$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
				$giftCertData = $giftCertificateRepository->getGiftCertificate($giftCertificateData['first_name'], $giftCertificateData['last_name'], $giftCertificateData['voucher']);

				if ($giftCertData)
				{
					$giftCertAmount = $giftCertData['balance'];
					$paypalLines[] = array(
						'name' => 'Gift Certificate',
						'desc' => '',
						'amt' => '-'.number_format($giftCertAmount, 2, '.', ''),
						'qty' => 1,
						'tax' => 0,
						'itemcategory' => 'Physical'
					);
				}
			}

			return $paypalLines;
		}

		/**
		 * @param ORDER $order
		 * @param array $cartItems
		 * @param bool $fromCart
		 * @param string $returnUrl
		 * @param string $cancelUrl
		 * @param bool $billMeLater
		 * @return bool|mixed
		 */
		public function expressCheckout($order, $cartItems, $fromCart, $returnUrl, $cancelUrl, $billMeLater = false)
		{
			global $settings, $db;

			$paypalLines = $this->getLineItems($order, $cartItems, $fromCart);

			if ($fromCart)
			{
				$paypalTotal = $order->subtotalAmount;
			}
			else
			{
				$paypalTotal = $order->totalAmount;
			}

			if ($order->discountAmount > 0)
			{
				if ($fromCart)
				{
					$paypalTotal -= $order->discountAmount;
				}
			}

			if ($settings['DiscountsPromo'] == 'YES' && $order->promoDiscountAmount > 0)
			{
				if ($fromCart)
				{
					$paypalTotal -= $order->promoDiscountAmount;
				}
			}

			if ($fromCart && $settings['DisplayPricesWithTax'] == 'YES')
			{
				$order->taxAmount = $order->subtotalAmountWithTax - $order->subtotalAmount;
			}

			if (!$fromCart)
			{
				$amountLines = array(
					"shipping_amount" => $order->shippingAmount,
					"tax_amount" => $order->taxAmount,
					"handling_amount" => $order->handlingSeparated ? $order->handlingAmount : 0,
					"discount_amount" => $order->discountAmount,
					"promo_discount_amount" => $order->promoDiscountAmount,
				);
			}
			else
			{
				$amountLines = array(
					"shipping_amount" => 0,
					"tax_amount" => 0, //$order->taxAmount,
					"handling_amount" => 0,
					"discount_amount" => 0,
					"promo_discount_amount" => 0,
				);
			}

			$giftCertificateData = isset($_SESSION["order_gift_certificate"]) ? $_SESSION["order_gift_certificate"] : false;
			if (!$fromCart && $giftCertificateData)
			{
				$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
				$giftCertData = $giftCertificateRepository->getGiftCertificate($giftCertificateData['first_name'], $giftCertificateData['last_name'], $giftCertificateData['voucher']);

				if ($giftCertData)
				{
					$paypalTotal -= $giftCertData['balance'];
				}
			}

			if ($paypalTotal <= 0)
			{
				return false;
			}

			$paypal_ec_token = $this->setExpressCheckout(
				$paypalTotal, 
				$returnUrl, 
				$cancelUrl, 
				!$order->getShippingRequired(),
				$paypalLines,
				$amountLines,
				$fromCart ? false : true,
				$billMeLater
			);

			if (!$fromCart)
			{
				$key = $this->getExpressCheckoutTokenKey($billMeLater);
				$_SESSION[$key] = $paypal_ec_token;
			}

			if (trim($paypal_ec_token) == '')
			{
				return false;
			}

			return $this->getExpressCheckoutUrl($paypal_ec_token, $fromCart);
		}

		/**
		 * Get Express Checkout details
		 * @param type $token
		 * @return string 
		 */
		public function getExpressCheckoutDetails($token)
		{
			$api = $this->getClient();

			$response = $api->getExpressCheckoutDetails($this->settings_vars[$this->id.'_Transaction_Type']['value'], $token);

			if ((isset($response['RESULT']) && $response['RESULT'] == '0') || (isset($response['ACK']) && $response['ACK'] == 'Success'))
			{
				return $response;
			}
			else
			{
				$this->is_error = true;

				if (isset($response['RESPMSG']))
				{
					$this->error_message = $response['RESPMSG'];
				}
				else if (isset($response['L_SHORTMESSAGE0']))
				{
					$this->error_message = $response['L_SHORTMESSAGE0'] .': '.$response['L_LONGMESSAGE0'];
				}
			}

			return '';
		}

		protected function getExpressCheckoutTokenKey($billMeLater)
		{
			return $billMeLater ? 'paypal_express_checkout_bml_token_opc' : 'paypal_express_checkout_token_opc';
		}

		/**
		 * Set express checkout
		 *
		 * @param $orderTotal
		 * @param $returnUrl
		 * @param $cancelUrl
		 * @param bool $noShipping
		 * @param bool $lines
		 * @param bool $orderAmounts
		 * @param bool $noShippingAddress
		 * @param bool $billMeLater
		 * @return string
		 */
		public function setExpressCheckout(
			$orderTotal, $returnUrl, $cancelUrl, $noShipping = false, $lines = false, 
			$orderAmounts = false, $noShippingAddress = false, $billMeLater = false
		)
		{
			global $db, $settings, $order;

			$sessionKey = $this->getExpressCheckoutTokenKey($billMeLater);
			unset($_SESSION[$sessionKey]);

			$api = $this->getClient();

			$uxParams = array();

			if (substr($settings['GlobalHttpsUrl'], 0, strlen('https://')) == 'https://' || 1)
			{
				if (isset($this->settings_vars[$this->id.'_Logo']) && $this->settings_vars[$this->id.'_Logo']['value'] != '')
				{
					$uxParams['HDRIMG'] = $settings['GlobalHttpsUrl'].'/'.$this->settings_vars[$this->id.'_Logo']['value'];
				}
				else
				{
					$db->query('SELECT * FROM '.DB_PREFIX."design_elements WHERE name = 'image-logo'");
					
					if ($row = $db->moveNext())
					{
						$uxParams['HDRIMG'] = $settings['GlobalHttpsUrl'].'/'.$row['content'];
					}
				}
			}

			if (isset($this->settings_vars[$this->id.'_Border']))
				$uxParams['HDRBORDERCOLOR'] = $this->settings_vars[$this->id.'_Border']['value'];

			if (isset($this->settings_vars[$this->id.'_Background']))
				$uxParams['HDRBACKCOLOR'] = $this->settings_vars[$this->id.'_Background']['value'];

			$response = $api->setExpressCheckout(
				$this->settings_vars[$this->id.'_Transaction_Type']['value'],
				$orderTotal, 
				$this->settings_vars[$this->id."_Currency_Code"]["value"], 
				$returnUrl, 
				$cancelUrl, 
				$noShipping, 
				$lines, 
				$uxParams,
				$orderAmounts,
				$noShippingAddress,
				$order,
				$billMeLater
			);

			if (
				(isset($response['RESULT']) && $response['RESULT'] == '0') || 
				(isset($response['ACK']) && in_array($response['ACK'], array('Success', 'SuccessWithWarning', 'SuccessWithWarnings')))
			)
			{
				if (isset($response['TOKEN']))
				{
					$_SESSION[$sessionKey] = $response['TOKEN'];
					return $response['TOKEN'];
				}
			}
			else
			{
				$this->is_error = true;
				if (isset($response['RESPMSG']))
				{
					$this->error_message = $response['RESPMSG'];
				}
				else if (isset($response['L_SHORTMESSAGE0']))
				{
					$this->error_message = $response['L_SHORTMESSAGE0'] .': '.$response['L_LONGMESSAGE0'];
				}
			}

			return '';
		}

		/**
		 * Get Express Checkout URL
		 *
		 * @param $token
		 * @param bool $fromCart
		 *
		 * @return mixed
		 */
		public function getExpressCheckoutUrl($token, $fromCart = false)
		{
			$api = $this->getClient();

			return $api->getExpressCheckoutUrl($token, $fromCart);
		}

		/**
		 * Process transaction
		 * @global array $settings
		 * @param DB $db
		 * @param USER $user
		 * @param ORDER $order
		 * @param array $post_form
		 * @return boolean|string 
		 */
		public function process($db, $user, $order, $post_form)
		{
			global $settings;
			if (isset($_SESSION['paypal_express_checkout_details']) || isset($_SESSION['paypal_express_checkout_token_opc']))
			{
				$api = $this->getClient();

				/**
				 * Get token
				 */
				$token = '';

				if (isset($_SESSION['paypal_express_checkout_token_opc']) && $_SESSION['paypal_express_checkout_token_opc'] != '')
				{
					$token = $_SESSION['paypal_express_checkout_token_opc'];
				}
				else if (isset($_SESSION['paypal_express_checkout_token']) && $_SESSION['paypal_express_checkout_token'] != '')
				{
					$token = $_SESSION["paypal_express_checkout_token"];
				}
				else if (isset($_SESSION['paypal_express_checkout_details']['TOKEN']) && $_SESSION['paypal_express_checkout_details']['TOKEN'] != '')
				{
					$token = $_SESSION['paypal_express_checkout_details']['TOKEN'];
				}

				if ($token == '')
				{
					$this->is_error = true;
					$this->error_message = 'PayPal Express Checkout Token is blank';
					return false;
				}

				/**
				 * Validate token
				 */
				$expressCheckoutDetails = $this->getExpressCheckoutDetails($token);
				if (!$expressCheckoutDetails || $expressCheckoutDetails == '' || $this->is_error)
				{
					return false;
				}

				/**
				 * Get payer id
				 */
				$payerId = isset($expressCheckoutDetails['PAYERID']) ? $expressCheckoutDetails['PAYERID'] : '';

				if ($payerId == '')
				{
					$this->is_error = true;
					$this->error_message = 'PayPal Express Checkout Payer ID is blank';
					return false;
				}

				$charged_amt = 0;

				global $msg;

				$paypalLines = $this->getLineItems($order, $order->getOrderItemsExtended($msg), false);
				$orderAmounts = array(
					"shipping_amount" => $order->shippingAmount,
					"tax_amount" => $order->taxAmount,
					"handling_amount" => $order->handlingSeparated ? $order->handlingAmount : 0,
					"discount_amount" => $order->discountAmount,
					"promo_discount_amount" => $order->promoDiscountAmount,
				);

				$result = $api->doExpressCheckoutPayment(
					$this->settings_vars[$this->id."_Transaction_Type"]["value"], $token, $payerId,
					$this->common_vars["order_total_amount"]["value"], $this->settings_vars[$this->id."_Currency_Code"]["value"],
					null,
					$paypalLines,
					$orderAmounts,
					$order->getOrderNumber()
				);

				if ((isset($result['RESULT']) && $result['RESULT'] == '0') || (isset($result['ACK']) && in_array($result['ACK'], array('Success', 'SuccessWithWarning', 'SuccessWithWarnings'))))
				{
					$response = "";
					foreach ($result as $var=>$val)
					{
						$response .= $var." = ".$val."\n";
					}

					$transaction_id = "";
					$payment_status = "Received";

					unset($_SESSION["paypal_express_checkout_token_opc"]);

					if (isset($result['ACK']))
					{
						//PayPal API
						$transaction_id = $result['PAYMENTINFO_0_TRANSACTIONID'];
						$PendingReason = $result['PAYMENTINFO_0_PENDINGREASON'];
						if ($result['PAYMENTINFO_0_PAYMENTSTATUS'] == 'Completed')
						{
							$payment_status = $this->settings_vars[$this->id."_Transaction_Type"]["value"] == 'Sale' ? ORDER::PAYMENT_STATUS_RECEIVED : ORDER::PAYMENT_STATUS_PENDING;
						}
						else
						{
							$payment_status = ORDER::PAYMENT_STATUS_PENDING;
						}
					}
					else
					{
						//Payflow API
						$transaction_id = $result['PNREF'];
						$PendingReason = $result['PENDINGREASON'];

						if ($PendingReason == 'completed')
						{
							$payment_status = $this->settings_vars[$this->id."_Transaction_Type"]["value"] == 'Sale' ?  ORDER::PAYMENT_STATUS_RECEIVED : ORDER::PAYMENT_STATUS_PENDING;
						}
						else
						{
							$payment_status = ORDER::PAYMENT_STATUS_PENDING;
						}
					}
					
					if ($payment_status == ORDER::PAYMENT_STATUS_RECEIVED)
					{
						$charged_amt = $order->totalAmount;
					}

					$this->createTransaction($db, $user, $order, $response, "", strtolower($PendingReason), "", $charged_amt, $transaction_id);
					$this->is_error = false;

					return $payment_status;
				}
				else
				{
					$this->is_error = true;
					if (isset($result['RESPMSG']))
					{
						$this->error_message = $result['RESPMSG'];
					}
					else if (isset($result['L_SHORTMESSAGE0']))
					{
						$this->error_message = $result['L_SHORTMESSAGE0'] .': '.$result['L_LONGMESSAGE0'];
					}
					else
					{
						$this->error_message = "Unknown error occurred while processing your transaction";
					}

					$this->storeTransaction($db, $user, $order, $result, '', false);
				}
			}
			else
			{
				$this->error_message = "PayPal Express Checkout session expired or does not exist";
				$this->is_error = true;
			}

			return false;
		}
		/**
		 * Checks does payment gateway support capture for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsCapture($order_data = false)
		{
			return $order_data['custom1'] == 'authorization';
		}


		/**
		 * Checks does payment gateway support partial captures for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return bool
		 */
		function supportsPartialCapture($order_data = false)
		{
			return true;
		}

		/**
		 * Returns capturable amount
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function capturableAmount($order_data = false)
		{
			return $order_data['custom3'] > 0 ? $order_data["total_amount"] - $order_data["custom3"] : $order_data['total_amount'];
		}

		/**
		 * Captures funds
		 *
		 * @param bool $order_data
		 * @param bool $capture_amount
		 *
		 * @return bool
		 */
		public function capture($order_data = false, $capture_amount = false)
		{
			$apiClient = $this->getClient();
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$capturable_amount = $this->capturableAmount($order_data);

				//check is transaction completed
				if ($capturable_amount == 0)
				{
					$this->is_error = true;
					$this->error_message = 'Transaction already completed';
					return false;
				}

				//get captured total amount & x_trans_id
				$captured_total_amount = $capture_amount ? $capture_amount : $capturable_amount;

				$totalCaptured = $order_data['custom3'] + $captured_total_amount;

				if ($order_data['total_amount'] == $totalCaptured)
				{
					$complete_type = "Complete";
				}
				else
				{
					$complete_type = "NotComplete";
				}

				$result = $apiClient->handleCapture($db, $order, $order_data['security_id'], $captured_total_amount, $totalCaptured, $this->currency["code"], '', $complete_type, '');
				if ($result !== false)
				{
					$this->is_error = true;
					$this->error_message = $result;
				}
				else
				{
					$this->is_error = false;
					$this->error_message = '';
				}
			}

			return !$this->is_error;
		}

		/**
		 * Returns amount captured
		 *
		 * @param bool $order_data
		 *
		 * @return bool
		 */
		public function capturedAmount($order_data = false)
		{
			if (in_array($order_data["custom1"], array("order", "authorization", "fulfill")) && is_numeric($order_data["custom3"]))
			{
				return $order_data['custom3'];
			}

			return false;
		}

		/**
		 * Checks does payment gateway supports refund for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsRefund($order_data = false)
		{
			return $order_data['custom1'] == 'sale' || $order_data['custom1'] == 'none';
		}

		/**
		 * Checks does payment gateway supports partial refund for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsPartialRefund($order_data = false)
		{
			return false;
		}

		/**
		 * Returns refundable amount
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function refundableAmount($order_data = false)
		{
			return $order_data['custom3'];
		}

		/**
		 * Refunds order
		 *
		 * @param bool $order_data
		 * @param bool $refund_amount
		 *
		 * @return bool
		 */
		public function refund($order_data = false, $refund_amount = false)
		{
			$apiClient = $this->getClient();
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$refundable_amount = $this->refundableAmount($order_data);

				//check is transaction completed
				if ($refundable_amount == 0)
				{
					$this->is_error = true;
					$this->error_message = 'Transaction already refunded';
					return false;
				}

				$refund_total_amount = $refund_amount ? $refund_amount : $refundable_amount;

				if ($order_data['custom3'] - $refund_total_amount < 0) $refund_total_amount = $order_data['custom3'];

				$result = $apiClient->handleRefund($db, $order, $order_data['security_id'], $order_data['custom3'], $this->currency["code"], '', '', $refund_total_amount, $order_data["total_amount"]);

				if ($result !== false)
				{
					$this->is_error = true;
					$this->error_message = $result;
				}
				else
				{
					$this->is_error = false;
					$this->error_message = '';
				}
			}

			return !$this->is_error;
		}

		/**
		 * Checks does payment gateway support void for current order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function supportsVoid($order_data = false)
		{
			return $order_data['custom1'] == 'sale' || $order_data['custom1'] == 'authorization'  || $order_data['custom1'] == 'none';
		}

		/**
		 * Void order
		 *
		 * @param mixed $order_data
		 *
		 * @return boolean
		 */
		public function void($order_data = false)
		{
			$apiClient = $this->getClient();
			global $db, $order;

			$this->is_error = false;

			if ($order_data)
			{
				$result = $apiClient->handleVoid($db, $order, $order_data['security_id'], '', 0, 0, '');

				if ($result !== false)
				{
					$this->is_error = true;
					$this->error_message = $result;
				}
				else
				{
					$this->is_error = false;
					$this->error_message = '';
				}
			}

			return !$this->is_error;
		}

		/**
		 * Get client to be used to communicate EC
		 * @global type $settings
		 * @return type 
		 */
		public function getClient()
		{
			if (is_null($this->api))
			{
				global $settings;

				$this->api = Payment_PayPal_PayPalApi::getExpressCheckoutApi($settings, $this->buttonSource);

				$this->settings_vars['paypalec_Mode']['value'] = $settings['paypalec_Mode'];
				$this->settings_vars['paypalec_Currency_Code']['value'] = $settings['paypalec_Currency_Code'];
				$this->settings_vars['paypalec_Transaction_Type']['value'] = $settings['paypalec_Transaction_Type'];
			}

			return $this->api;
		}
	}

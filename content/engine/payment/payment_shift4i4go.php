<?php 
$payment_processor_id = "shift4i4go";
$payment_processor_name = "Shift4.com";
$payment_processor_class = "PAYMENT_SHIFT4I4GO";
$payment_processor_type = "ipn";

class PAYMENT_SHIFT4I4GO extends PAYMENT_PROCESSOR
{
	const   RESULT_OK       = 1;
	private $auth_url       = "https://access.i4go.com";
	private $auth_url_test  = "https://access.shift4test.com";

	public function PAYMENT_SHIFT4I4GO()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "shift4i4go";
		$this->name = "Shift4.com";
		$this->class_name = "PAYMENT_SHIFT4I4GO";
		$this->type = "ipn";
		$this->description = "";
		$this->post_url = "https://certify.i4go.com/index.cfm";
		$this->steps = 2;
		$this->formtemplate = "templates/pages/checkout/opc/shift4i4gov2_paymentform.html";
		$this->override_payment_form = false;

		//$this->reload_parent_on_process = true;

		//$this->payment_in_popup = true;
		//$this->payment_in_popup_width = 540;
		//$this->payment_in_popup_height = 680;

		return $this;
	}

	public function getTemplateData($param = false)
	{
		global $settings;
		$auth_result = $this->doAuthRequest($param);
		if ($auth_result){
			if (isset($auth_result['i4go_responsecode'])){
				if ($auth_result['i4go_responsecode'] == self::RESULT_OK){
					$auth_result['successUrl'] = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page.'&oa='.ORDER_PROCESS_PAYMENT;
					$this->cur_accsess_token = $auth_result;
					return $auth_result;
				}
			}
		}

		$this->is_error = true;
		$this->error_message = "We are sorry, but there has been an error processing this transaction.";
		return false;
	}

	function doAuthRequest($ip)
	{
		global $settings;

		$request_data = array(
			'i4go_clientIP' => $ip,
			'i4go_accessToken' => $settings['shift4i4go_access_Token']
		);

		$post_data = http_build_query($request_data);

		if ($this->_isTestMode()){
			$access_url = $this->auth_url_test;
		}
		else{
			$access_url = $this->auth_url;
		}

		$c = curl_init($access_url);
		if ($settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}
		else
		{
			curl_setopt($c, CURLOPT_TIMEOUT, 65);
		}

		curl_setopt($c, CURLOPT_VERBOSE, 0);
		curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		set_time_limit(3000);

		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);

		$this->log("Request:", $request_data);

		$buffer = curl_exec($c);

		if (curl_errno($c))
		{
			if (curl_errno($c) == 28)
			{
				$buffer = 'timeout';
			}
			else
			{
				$buffer = curl_error($c);
			}
		}

		$response = json_decode($buffer, true);
		$this->log("Response:", $response);

		return $response;
	}

	private function _isTestMode(){
		global $settings;
		if (strpos($settings['shift4i4go_UTG_Server'], 'shift4test') === false){
			return false;
		}
		else{
			return true;
		}
	}

	protected function getDotnUrl()
	{
		global $settings;

		if (!empty($settings['shift4i4go_UTG_Server']))
		{
			return $settings['shift4i4go_UTG_Server'];
		}

		$testMode = false;
		if ($testMode)
		{
			return 'https://dotn.shift4test.com/api/s4tran_action.cfm';
		}

		return 'https://server' . mt_rand(1,12) . '.dollarsonthenet.net/api/s4tran_action.cfm';
	}

	protected function isTestDotnUrl($url)
	{
		return in_array(strtolower($url), array('https://dotn.shift4test.com/api/s4tran_action.cfm', 'https://certify.dollarsonthenet.net/api/s4tran_action.cfm'));
	}

	public function replaceDataKeys()
	{
		return array('apipassword');
	}

	protected function getRequestData()
	{
		global $settings;

		return array(
			'username' => $settings['shift4i4go_Login'],
			'apipassword' => $settings['shift4i4go_Password'],
			'apiserialnumber' => $settings['shift4i4go_SN'],
			'merchantid' => $settings['shift4i4go_Merchant_Id'],
			'apioptions' => 'ALLDATA',
			'apiformat' => 0,
			'apisignature' => '$',
			'contenttype' => 'xml',
			'verbose' => 'YES',
			'vendor' => getpp().':'.getpp().':'.APP_VERSION,
			'date' => date('mdy'),
			'time' => date('His'),
		);
	}

	public function process($db, $user, $order, $post_form)
	{
		$this->log("Response", $post_form);

		$this->storeTransaction($db, $user, $order, $post_form, true, '');

		if (isset($post_form['i4go_responsecode']) && $post_form['i4go_responsecode'] == 1)
		{
			$security_id = $_REQUEST['i4go_uniqueid'];

			$refNum = uniqid();

			$functionRequestCode = '1D';

			if ($this->settings_vars[$this->id.'_Auth_Type']['value'] == 'Auth-Only')
			{
				$functionRequestCode = '1B';
			}

			$invoice = $this->getInvoiceNumber($this->common_vars["order_id"]["value"]);

			$request_data = array_merge($this->getRequestData(), array(
//				'cardpreset' => 'N',
				'cardtype' => 'CC',
				'cardentrymode' => 'M',
				'cvv2indicator' => '0',
				'requestorreference' => $refNum,
				'functionrequestcode' => $functionRequestCode,
				'saleflag' => 'S',
				'invoice' => $invoice,
				'primaryamount' => number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""),
				'customername' => $this->common_vars['billing_name']['value'],
				'addressline1' => $this->common_vars['billing_address1']['value'],
				'addressline2' => $this->common_vars['billing_address2']['value'],
				'city' => $this->common_vars['billing_city']['value'],
				'state' => $this->common_vars['billing_zip']['value'],
				'postalcode' => $this->common_vars['billing_zip']['value'],
				'country' => $this->common_vars['billing_country_iso_number']['value'],
				'phonenumber' => $this->common_vars['billing_phone']['value'],
				'destinationzipcode' => $this->common_vars['shipping_zip']['value'],
				'customerreference' => $this->common_vars['order_id']['value'],
				'notes' => '',
				'uniqueid' => $security_id,
				'receipttextcolumns' => '080',
			));

			if ($this->common_vars['order_tax_amount']['value'] > 0)
			{
				$request_data['taxindicator'] = 'Y';
				$request_data['taxamount'] = number_format($this->common_vars["order_tax_amount"]["value"], 2, ".", "");
			}

			$count = 1;
			$notes = '';
			foreach ($order->items as $item)
			{
				$msg = $item['title'] .' x ' . $item['quantity'];
				if ($count < 5)
				{
					$request_data['productdescriptor'.$count] = $msg;
				}

				$notes .= $msg . '<br/>';
			}

			$request_data['notes'] = $notes;

			$buffer = $this->doRequest($request_data);

			$response = null;
			$isValid = false;
			$errorMessage = '';
			if ($buffer == 'timeout')
			{
				sleep(5);

				$errorMessage = 'There was a problem communicating with the payment gateway. Please try again.';

				$invoiceDetails = $this->getInvoice($invoice, $security_id);
				if ($invoiceDetails['status'] == 1)
				{
					$response = $invoiceDetails['response'];
				}
				else
				{
					$isValid = false;

					$errorMessage = $invoiceDetails['error'];

					$this->logTimeout($order, $functionRequestCode, number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""), $security_id, $invoice);
				}
			}
			else
			{
				$response = xml2array($buffer);

				if (!$response || empty($response))
				{
					$isValid = false;

					$errorMessage = '';
				}
			}

			$shouldVoid = true;
			if ($response)
			{
				$isValid = $this->checkValidResponse($response, $errorMessage);
			}

			if (!$isValid && $response)
			{
				if (isset($response['errorindicator']))
				{
					$errorIndicator = strtolower($response['errorindicator']);
					if ($errorIndicator == 'y')
					{
						$errorCode = intval($response['primaryerrorcode']);

						if (in_array($errorCode, array(1001,9012,9018,9020,9023,9033,9489,9901,9902,9951,
							9957,9960,9961,9962,9964,9978,4003)) ||
							($errorCode >= 2000 && $errorCode <= 2999)
						)
						{
							// Do not void when UTG timed out communicating with dotn, the request may have gone
							// through
							$shouldVoid = false;

							// utg to dotn timeout
							sleep(5);

							$invoiceDetails = $this->getInvoice($invoice, $security_id);
							if ($invoiceDetails['status'] == 1)
							{
								$response = $invoiceDetails['response'];

								$errorMessageTemp = '';
								$isValid = $this->checkValidResponse($response, $errorMessageTemp);
								if ($errorMessageTemp != '')
								{
									$errorMessage = $errorMessageTemp;
								}
							}
							else
							{
								$isValid = false;

								$errorMessage = $invoiceDetails['error'];

								$this->logTimeout($order, $functionRequestCode, number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""), $security_id, $invoice);
							}
						}
					}
				}
			}

			if ($isValid && $response)
			{
				$security_id = isset($response['uniqueid']) ? $response['uniqueid'] : $security_id;

				$custom1 = $functionRequestCode == '1B' ? 'AUTH_ONLY' : 'AUTH_CAPTURE';
				//If Auth only, then store auth'd amount to custom2
				$custom2 = $functionRequestCode == "1B" ? number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "") : "";
				//If Auth Capture, then store captured amount to custom3
				$custom3 = $functionRequestCode != "1B" ? number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "") : 0;

				$extra = $invoice;

				$avsResult = isset($response['avsresult']) ? $response['avsresult'] : '';
				$cvvResult = isset($response['cvv2result']) ? $response['cvv2result'] : '';
				$event = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_PAYMENT_METHOD_DATA_SET);
				$event->setOrder($order);
				$paymentParams = array();
				$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_AVS] =  $avsResult;
				$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CVV]  = $cvvResult;
				if (isset($response['cardnumber']))
				{
					$paymentParams[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM] = $response['cardnumber'];
				}
				$event->setPaymentMethodData($paymentParams);
				Events_EventHandler::handle($event);
				$this->createTransaction($db, $user, $order, $response, $extra, $custom1, $custom2, $custom3, $security_id);

				$this->is_error = false;
				$this->error_message = '';
			}
			else
			{
				if ($response)
				{
					if ($shouldVoid)
					{
						$security_id = isset($response['uniqueid']) ? $response['uniqueid'] : $security_id;

						$invoice = $this->getLastInvoice($order->oid);

						if (!$invoice)
						{
							$invoice = $this->getInvoiceNumber($order->order_num);
						}

						$buffer = $this->_doVoid($security_id, $invoice);
						$response = xml2array($buffer);
					}
				}

				$errorMessage = urldecode($errorMessage);

				$this->is_error = true;
				$this->error_message = "We are sorry, but there has been an error processing this transaction.<br>Payment gateway response: ".$errorMessage;
			}

			if ($this->is_error)
			{
				$this->storeTransaction($db, $user, $order, $response, false, '');
			}

			return $this->is_error ? false : ($functionRequestCode == '1B' ? 'Pending' : 'Received');
		}
		else
		{
			$this->is_error = true;

			$this->error_message = "We are sorry, but there has been an error processing this transaction.";

			return false;
		}
	}

	function checkValidResponse($response, &$errorMessage)
	{
		$responseCode = isset($response['response']) ? $response['response'] : (isset($response['responsecode']) ? $response['responsecode'] : '');

		$avsResult = strtolower(isset($response['validavs']) ? $response['validavs'] : '');
		$cvvResult = strtolower(isset($response['cvv2valid']) ? $response['cvv2valid'] : '');

		$isValid = in_array($responseCode, array('A', 'C')) && $avsResult != 'n' && $cvvResult != 'n';

		if (!$isValid)
		{
			if ($avsResult == 'n' || $cvvResult == 'n' || in_array($responseCode, array('D', 'X', 'f')) || $responseCode == 'R')
			{
				$errorMessage = 'Payment declined.  Please try again.';
			}
			else
			{
				if (isset($response['longerror']))
				{
					$errorMessage =  $response['longerror'];
				}
			}
		}

		return $isValid;
	}

	function getInvoiceNumber($oid, $transactionCount = 0)
	{
		return ''.$oid.($transactionCount < 10 ? '0' : '').$transactionCount;
	}

	function doRequest($request_data)
	{
		global $settings;

		$dotnUrl = $this->getDotnUrl();

		$request_data = array_merge(array('STX' => 'YES'), $request_data, array('ETX' => 'YES'));

		$post_data = http_build_query($request_data);

		$this->log('API Endpoint: '.$dotnUrl);

		$c = curl_init($dotnUrl);

		if ($settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}
		else
		{
			curl_setopt($c, CURLOPT_TIMEOUT, 65);
		}

		curl_setopt($c, CURLOPT_VERBOSE, 0);
		curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		@set_time_limit(3000);

		if ($this->isTestDotnUrl($dotnUrl))
		{
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		}
		else
		{
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
			curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
		}

		$this->log("Request:", $request_data);

		$buffer = curl_exec($c);

		if (curl_errno($c))
		{
			if (curl_errno($c) == 28)
			{
				$buffer = 'timeout';
			}
			else
			{
				$buffer = curl_error($c);
			}
		}

		$response = xml2array($buffer);
		$this->log("Response:", $response);

		return $buffer;
	}

	function supportsCapture($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == 'Pending' || $order_data['payment_status'] == 'Partial')
			{
				if ($this->isTotalAmountCaptured($order_data))
					return false;

				return true;
			}
		}
		return false;
	}

	function isTotalAmountCaptured($order_data)
	{
		return $order_data['custom3'] == $order_data['total_amount'];
	}

	function supportsRefund($order_data = false)
	{
		if ($order_data)
		{
			if ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Partial')
			{
				return true;
			}
			else
			if ($order_data['payment_status'] == 'Refunded')
			{
				return $this->_refundableAmount($order_data) > 0.0;
			}
		}
		return false;
	}

	public function supportsPartialRefund($order_data = false)
	{
		return true;
	}

	function supportsVoid($order_data = false)
	{
		return $order_data && ($order_data['payment_status'] == 'Received' || $order_data['payment_status'] == 'Pending');
	}

	public function capturedAmount($order_data = false)
	{
		return $order_data ? $order_data['custom3'] : false;
	}

	function capturableAmount($order_data=false)
	{
		if ($order_data)
		{
			if ($this->supportsCapture($order_data))
			{
				$capturedAmount = $order_data['custom3'];

				$total_amount = $order_data['total_amount'];
				$captured_amount =  $order_data['custom3'];

				$capturableAmount = $order_data['total_amount'] - $order_data['custom3'];
				if ($capturableAmount < 0)
					$capturableAmount = 0.0;
				return number_format($capturableAmount, 2, '.', '');
			}
		}
		return 0;
	}

	function refundableAmount($order_data=false)
	{
		if ($order_data)
		{
			if ($this->supportsRefund($order_data))
			{
				return $this->_refundableAmount($order_data);
			}
		}
		return 0;
	}

	function _refundableAmount($order_data)
	{
		$refundableAmount = $this->getTotalCaptured($order_data);
		return number_format($refundableAmount, 2, '.', '');
	}

	function getTotalCaptured($order_data)
	{
		return 0.00 + $order_data['custom3'];
	}

	function getTransactionsCount($oid)
	{
		global $db;

		$count = $db->selectOne('SELECT COUNT(*) as `trans_count` FROM '.DB_PREFIX.'payment_transactions WHERE oid='.$oid);

		return $count['trans_count'];
	}

	function getLastInvoice($oid)
	{
		global $db;

		$invoice = $db->selectOne('SELECT extra FROM '.DB_PREFIX.'payment_transactions WHERE oid='.$oid.' AND is_success = 1 ORDER BY tid DESC LIMIT 1');

		if ($invoice)
		{
			$invoice = array_shift($invoice);
		}

		return $invoice;
	}

	function capture($order_data=false,$capture_amount=false)
	{
		global $settings, $db;

		$this->is_error = false;

		if ($order_data)
		{
			//check is transaction completed
			if ($this->isTotalAmountCaptured($order_data))
			{
				$this->is_error = true;
				$this->error_message = "Transaction already completed";
				return false;
			}

			$captured_total_amount = $capture_amount ? $capture_amount : $order_data["custom2"];

			if ($captured_total_amount <= 0)
			{
				$this->is_error = true;
				$this->error_message = "Capture amount must be greater than 0";
				return false;
			}
			else if ($captured_total_amount > $order_data['total_amount'])
			{
				$this->is_error = true;
				$this->error_message = "Capture amount cannot be more than total amount";
				return false;
			}
			else if ($order_data['custom3'] + $captured_total_amount > $order_data['total_amount'])
			{
				$this->is_error = true;
				$this->error_message = 'Captured amount cannot be more than total amount';
				return false;
			}

			$refNum = uniqid();
			$security_id = $order_data['security_id'];

			$trans_count = $this->getTransactionsCount($order_data['oid']);
			$invoice = $this->getInvoiceNumber($order_data['order_num'], $trans_count);

			$request_data = array_merge($this->getRequestData(), array(
				'requestorreference' => $refNum,
				'functionrequestcode' => '1D',
				'invoice' => $invoice,
				'cardtype' => 'CC',
				'cardpresent' => 'N',
				'cardentrymode' => 'M',
				'saleflag' => 'S',
				'primaryamount' => number_format($captured_total_amount, 2, ".", ""),
				'customerreference' => $order_data["order_num"],
				'taxindicator' => 'Y',
				'taxamount' => number_format($order_data['tax_amount'], 2, ".", ""),
				'notes' => '',
				'uniqueid' => $security_id,
			));

			$buffer = $this->doRequest($request_data);

			if ($buffer)
			{
				$response = xml2array($buffer);

				$responseCode = isset($response['response']) ? $response['response'] : (isset($response['responsecode']) ? $response['responsecode'] : '');
				if (in_array($responseCode, array('A', 'C')))
				{
					$totalCaptured = $this->getTotalCaptured($order_data) + $captured_total_amount;
					$custom3 = number_format($totalCaptured, 2, '.', '');

					$security_id = isset($response['uniqueid']) ? $response['uniqueid'] : $security_id;

					$payment_status = 'Received';

					if ($order_data['total_amount'] > $totalCaptured)
					{
						$payment_status = 'Partial';
					}

					$db->reset();
					$db->assignStr("payment_status", $payment_status);
					$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

					$order_data['custom1'] = 'PRIOR_AUTH_CAPTURE';
					$order_data['custom2'] = '';
					$order_data['custom3'] = $custom3;
					$order_data['payment_status'] = $payment_status;

					$user = new tmp_object();
					$user->id = $order_data["uid"];

					$order = new tmp_object();
					$order->oid = $order_data["oid"];
					$order->subtotalAmount = $order_data["subtotal_amount"];
					$order->totalAmount = $order_data["total_amount"];
					$order->shippingAmount = $order_data["shipping_amount"];
					$order->taxAmount = $order_data["tax_amount"];
					$order->gift_cert_amount = $order_data["gift_cert_amount"];

					$custom1 = $order_data['custom1'];
					$custom2 = $order_data['custom2'];

					$this->createTransaction($db, $user, $order,
						$response,
						$invoice,
						$custom1,
						$custom2,
						$custom3,
						$security_id
					);

					return true;
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $response['longerror'];
					return false;
				}
			}
		}

		return !$this->is_error;
	}

	function refund($order_data=false,$refund_amount=false)
	{
		global $settings, $db;

		$this->is_error = false;

		if ($order_data)
		{
			if (!$this->supportsRefund($order_data))
			{
				$this->is_error = true;
				$this->error_message = "Order cannot be refunded";
				return false;
			}

			//get captured total amount & x_trans_id
			$refund_total_amount = $refund_amount;

			if (!$refund_total_amount)
			{
				$refund_total_amount = $order_data['custom3'];
			}

			if ($refund_total_amount < 0)
			{
				$this->is_error = true;
				$this->error_message = "Refund amount must be greater than 0";
				return false;
			}
			else if ($refund_total_amount > $order_data['total_amount'])
			{
				$this->is_error = true;
				$this->error_message = "Refund amount cannot be more than total amount";
				return false;
			}
			else if ($refund_total_amount > $order_data['custom3'])
			{
				$this->is_error = true;
				$this->error_message = 'Refund amount cannot be more than total captured amount';
				return false;
			}

			$security_id = $order_data["security_id"];

			if ($security_id == '')
			{
				$this->is_error = true;
				$this->error_message = 'This order does not have a valid transaction id';
				return false;
			}
			else
			{
				$refNum = uniqid();
				$security_id = $order_data['security_id'];

				$trans_count = $this->getTransactionsCount($order_data['oid']);
				$invoice = $this->getInvoiceNumber($order_data['order_num'], $trans_count);

				$request_data = array_merge($this->getRequestData(), array(
					'requestorreference' => $refNum,
					'functionrequestcode' => '1D',
					'invoice' => $invoice,
					'cardtype' => 'CC',
					'cardpresent' => 'N',
					'cardentrymode' => 'M',
					'saleflag' => 'C',
					'primaryamount' => number_format($refund_total_amount, 2, ".", ""),
					'customerreference' => $order_data["order_num"],
					'taxindicator' => 'Y',
					'taxamount' => number_format($order_data['tax_amount'], 2, ".", ""),
					'notes' => '',
					'uniqueid' => $security_id,
				));

				$buffer = $this->doRequest($request_data);

				if ($buffer)
				{
					$response = xml2array($buffer);

					if (isset($response['errorindicator']))
					{
						if (strtolower($response['errorindicator']) == 'n')
						{
							$totalCaptured = $this->getTotalCaptured($order_data) - $refund_total_amount;
							$custom3 = number_format($totalCaptured, 2, '.', '');

							$security_id = isset($response['uniqueid']) ? $response['uniqueid'] : $security_id;

							$payment_status = 'Refunded';

							$db->reset();
							$db->assignStr("payment_status", $payment_status);
							$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

							$order_data['custom1'] = 'REFUND';
							$order_data['custom2'] = '';
							$order_data['custom3'] = $custom3;
							$order_data['payment_status'] = $payment_status;

							$user = new tmp_object();
							$user->id = $order_data["uid"];

							$order = new tmp_object();
							$order->oid = $order_data["oid"];
							$order->subtotalAmount = $order_data["subtotal_amount"];
							$order->totalAmount = $order_data["total_amount"];
							$order->shippingAmount = $order_data["shipping_amount"];
							$order->taxAmount = $order_data["tax_amount"];
							$order->gift_cert_amount = $order_data["gift_cert_amount"];

							$custom1 = $order_data['custom1'];
							$custom2 = $order_data['custom2'];

							$this->createTransaction($db, $user, $order,
								$response,
								$invoice,
								$custom1,
								$custom2,
								$custom3,
								$security_id
							);

							return true;
						}
						else
						{
							$this->is_error = true;
							$this->error_message = $response['longerror'];
							return false;
						}
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $response['longerror'];
						return false;
					}
				}
			}
		}

		return !$this->is_error;
	}

	function getInvoice($invoice, $security_id)
	{
		$refNum = uniqid();

		$request_data = array_merge($this->getRequestData(), array(
			'requestorreference' => $refNum,
			'functionrequestcode' => '07',
			'invoice' => $invoice,
			'uniqueid' => $security_id,
		));

		$buffer = $this->doRequest($request_data);

		if ($buffer)
		{
			$response = xml2array($buffer);

			if (isset($response['errorindicator']) && strtolower($response['errorindicator']) == 'n')
			{
				return array(
					'status' => 1,
					'response' => $response,
					'error' => '',
				);
			}
			else
			{
				return array(
					'status' => 0,
					'response' => $response,
					'error' => $response['longerror'],
				);
			}
		}

		return array(
			'status' => 0,
			'error' => 'There was a problem communicating with the payment gateway. Please try again.',
		);
	}

	function _doVoid($security_id, $invoice)
	{
		$refNum = uniqid();

		$request_data = array_merge($this->getRequestData(), array(
			'requestorreference' => $refNum,
			'functionrequestcode' => '08',
			'invoice' => $invoice,
			'uniqueid' => $security_id,
		));

		return $this->doRequest($request_data);
	}

	function void($order_data=false)
	{
		global $db;

		$this->is_error = false;

		if ($order_data)
		{
			$invoice = $this->getLastInvoice($order_data['oid']);

			if (!$invoice)
			{
				$invoice = $this->getInvoiceNumber($order_data['order_num']);
			}

			$buffer = $this->_doVoid($order_data['security_id'], $invoice);

			if ($buffer)
			{
				$response = xml2array($buffer);

				if (isset($response['errorindicator']))
				{
					if (strtolower($response['errorindicator']) == 'n')
					{
						$security_id = isset($response['uniqueid']) ? $response['uniqueid'] : $order_data['security_id'];

						$payment_status = 'Canceled';
						$status = 'Canceled';

						$db->assignStr("payment_status", $payment_status);
						$db->assignStr('status', $status);
						$db->update(DB_PREFIX."orders", "WHERE oid='".$order_data['oid']."'");

						$custom1 = 'CANCELED';
						$custom2 = '';
						$custom3 = '';

						$order_data['custom1'] = 'CANCELED';
						$order_data['custom2'] = '';
						$order_data['custom3'] = '';
						$order_data['payment_status'] = $payment_status;
						$order_data['status'] = $status;

						$user = new tmp_object();
						$user->id = $order_data["uid"];

						$order = new tmp_object();
						$order->oid = $order_data["oid"];
						$order->subtotalAmount = $order_data["subtotal_amount"];
						$order->totalAmount = $order_data["total_amount"];
						$order->shippingAmount = $order_data["shipping_amount"];
						$order->taxAmount = $order_data["tax_amount"];
						$order->gift_cert_amount = $order_data["gift_cert_amount"];

						$this->createTransaction($db, $user, $order,
							$response,
							$invoice,
							$custom1,
							$custom2,
							$custom3,
							$security_id
						);

						return true;
					}
					else
					{
						$this->is_error = true;
						$this->error_message = $response['longerror'];
						return false;
					}
				}
				else
				{
					$this->is_error = true;
					$this->error_message = $response['longerror'];
					return false;
				}
			}
		}

		return !$this->is_error;
	}

	public function getPostUrl()
	{
		return $this->url_to_gateway;
	}

	protected function logTimeout($order, $request_code, $amount, $unique_id, $invoice)
	{
		global $db;

		$oid = $order->oid;
		$uid = $order->user()->id;

		$db->query('INSERT INTO '.DB_PREFIX.'shift4_timeout_log (oid, uid, request_code, amount, unique_id, timestamp, invoice) VALUES ('.$oid.','.$uid.',\''.$request_code.'\','.$amount.',\''.$unique_id.'\', NOW(), \''.$invoice.'\')');
	}

	public function getTransactionDetails(ORDER $order)
	{
		global $db;

		$row = $db->selectOne('SELECT *
FROM '.DB_PREFIX.'payment_transactions
WHERE oid = '.$order->oid.' AND is_success = 1
ORDER BY completed DESC
LIMIT 1');
		$ret = false;

		if ($row)
		{
			$ret = array();

			$brandsMap = array(
				'AX' => 'American Express',
				'DC' => 'Diners Club',
				'JC' => 'JCB',
				'JCB' => 'JCB',
				'MC' => 'MasterCard',
				'NS' => 'Discover',
				'VS' => 'Visa',
				'DB' => 'Debit',
				'SR' => 'Sears',
				'SC' => 'Sears Canada',
			);

			$response_values = explode("\n", $row['payment_response']);
			foreach ($response_values as  $response_value)
			{
				$kvPair = explode(' = ', $response_value);

				if ($kvPair[0] == 'cardnumber')
				{
					$ret['card_number'] = urldecode($kvPair[1]);
				}
				else if ($kvPair[0] == 'cardtype')
				{
					$cardType = urldecode($kvPair[1]);

					$ret['card_type'] = array_key_exists($cardType, $brandsMap) ? $brandsMap[$cardType] : $cardType;
				}
				else if ($kvPair[0] == 'authorization')
				{
					$ret['authorization_code'] = urldecode($kvPair[1]);
				}
				else if ($kvPair[0] == 'receipttext')
				{
					$ret['notes'] = urldecode(str_replace('%0D%0A', ' ', $kvPair[1]));
				}
			}
		}

		return $ret;
	}
}

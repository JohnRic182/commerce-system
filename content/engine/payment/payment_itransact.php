<?php 
$payment_processor_id = "itransact";
$payment_processor_name = "Itransact.com";
$payment_processor_class = "PAYMENT_ITRANSACT";
$payment_processor_type = "cc";

class PAYMENT_ITRANSACT extends PAYMENT_PROCESSOR
{
	function PAYMENT_ITRANSACT()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "itransact";
		$this->name = "Itransact.com";
		$this->class_name = "PAYMENT_ITRANSACT";
		$this->type = "cc";
		$this->description = "";
		$this->steps = 1;
		$this->testMode = false;
		$this->support_ccs = true;
		$this->need_cc_codes = true;
		return $this;
	}

	function process($db, $user, $order, $post_form)
	{
		global $settings;

		//build XML request
		$xml =
			'xml=<?xml version="1.0"?>'.
			'<SaleRequest>'.
				'<CustomerData>'.
					'<Email>'.$this->common_vars["billing_email"]["value"].'</Email>'.
					'<BillingAddress>'.
						'<Address1>'.$this->common_vars["billing_address1"]["value"].'</Address1>'.
						(trim($this->common_vars["billing_address2"]["value"])!=""?('<Address2>'.$this->common_vars["billing_address2"]["value"].'</Address2>'): "").
						'<FirstName>'.$post_form["cc_first_name"].'</FirstName>'.
						'<LastName>'.$post_form["cc_last_name"].'</LastName>'.
						'<City>'.$this->common_vars["billing_city"]["value"].'</City>'.
						'<State>'.$this->common_vars["billing_state"]["value"].'</State>'.
						'<Zip>'.$this->common_vars["billing_zip"]["value"].'</Zip>'.
						'<Country>'.$this->common_vars["billing_country_iso_a2"]["value"].'</Country>'.
						'<Phone>'.$this->common_vars["billing_phone"]["value"].'</Phone>'.
					'</BillingAddress>'.
					'<ShippingAddress>'.
						'<FirstName>'.$this->common_vars["shipping_name"]["value"].'</FirstName>'.
						'<LastName>'.$this->common_vars["shipping_name"]["value"].'</LastName>'.
						'<Address1>'.$this->common_vars["shipping_address1"]["value"].'</Address1>'.
						(trim($this->common_vars["shipping_address2"]["value"])!=""?('<Address2>'.$this->common_vars["shipping_address2"]["value"].'</Address2>'): "").
						'<City>'.$this->common_vars["shipping_city"]["value"].'</City>'.
						'<State>'.$this->common_vars["shipping_state"]["value"].'</State>'.
						'<Zip>'.$this->common_vars["shipping_zip"]["value"].'</Zip>'.
						'<Country>'.$this->common_vars["shipping_country_iso_a2"]["value"].'</Country>'.
						'<Phone>111-111-1111</Phone>'.
					'</ShippingAddress>'.
					'<AccountInfo>'.
						'<CardInfo>'.
							'<CCNum>'.$post_form["cc_number"].'</CCNum>'.
							'<CCMo>'.$post_form["cc_expiration_month"].'</CCMo>'.
							'<CCYr>'.$post_form["cc_expiration_year"].'</CCYr>'.
							($post_form["cc_cvv2"] != ""?('<CVV2Number>'.$post_form["cc_cvv2"].'</CVV2Number><CVV2Illegible>1</CVV2Illegible>'):"").
						'</CardInfo>'.
					'</AccountInfo>'.
				'</CustomerData>'.
				'<TransactionData>'.
					'<TestMode>1</TestMode>'.
					//'<AuthCode>'.$this->common_vars["order_security_id"]["value"].'</AuthCode>'.
					'<VendorId>'.$this->settings_vars[$this->id."_CustomerCode"]["value"].'</VendorId>'.
					'<VendorPassword>'.$this->settings_vars[$this->id."_Password"]["value"].'</VendorPassword>'.
					'<HomePage>'.$settings["GlobalHttpUrl"].'</HomePage>'.
					'<OrderItems>'.
						'<Item>'.
							'<Description>Order '.$this->common_vars["order_id"]["value"].'</Description>'.
							'<Cost>'.number_format($this->common_vars["order_total_amount"]["value"], 2, ".", "").'</Cost>'.
							'<Qty>1</Qty>'.
						'</Item>'.
					'</OrderItems>'.
				'</TransactionData>'.
			'</SaleRequest>';

		//prepare curl
		$_post_url = trim($this->url_to_gateway) == "" ? "https://secure.paymentclearing.com/cgi-bin/rc/xmltrans.cgi" : trim($this->url_to_gateway);
		$c = curl_init($_post_url);

		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($c, CURLOPT_TIMEOUT, 360);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
		$buffer = curl_exec($c);

		$this->log("process Request:\n".$xml);
		$this->log("process Response:\n".$buffer);

		if (strlen($buffer) < 2)
		{
			$buffer = "<SaleResponse><TransactionData><Status>FAILED</Status><ErrorMessage>Could not connect to the LinkPoint server. Please try again.</ErrorMessage></SaleResponse>";
		}

		$xml_parser = new XMLParser();
		$xml_data = $xml_parser->parse($buffer);

		if (isset($xml_data["SaleResponse"][0]["TransactionData"][0]["Status"][0]))
		{
			$status = $xml_data["SaleResponse"][0]["TransactionData"][0]["Status"][0];
		}
		else
		{
			$status = "FAILURE";
		}

		switch ($status)
		{
			//transaction completed
			case "OK" :
			{
				$this->createTransaction($db, $user, $order, $buffer);
				if ($this->support_ccs && $this->enable_ccs)
				{
					$card_data = array(
						"fn"=>$post_form["cc_first_name"],
						"ln"=>$post_form["cc_last_name"],
						"ct"=>$post_form["cc_type"],
						"cc"=>$post_form["cc_number"],
						"em"=>$post_form["cc_expiration_month"],
						"ey"=>$post_form["cc_expiration_year"]
					);
					$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]));
				}
				$this->is_error = false;
				break;
			}
			//transaction error
			default :
			{
				$this->is_error = true;
				if (isset($xml_data["SaleResponse"][0]["TransactionData"][0]["ErrorMessage"][0]))
				{
					$this->error_message = "Gateway Response: ".$xml_data["SaleResponse"][0]["TransactionData"][0]["ErrorMessage"][0];
				}
				else
				{
					$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
				}
				break;
			}
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $buffer, '', false);
		}
		//complete process
		return !$this->is_error;
	}
}

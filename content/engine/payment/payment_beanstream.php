<?php
$payment_processor_id = "beanstream";
$payment_processor_name = "Beanstream";
$payment_processor_class = "PAYMENT_BEANSTREAM";
$payment_processor_type = "cc";

class PAYMENT_BEANSTREAM extends PAYMENT_PROCESSOR
{
	/**
	 * Class constructor
	 */
	public function PAYMENT_BEANSTREAM()
	{
		parent::PAYMENT_PROCESSOR();
		$this->id = "beanstream";
		$this->name = "Beanstream";
		$this->class_name = "PAYMENT_BEANSTREAM";
		$this->type = "cc";
		$this->description = "";
		$this->steps = 1;
		$this->testMode = false;
		$this->support_ccs = true;
		$this->need_cc_codes = true;
		return $this;
	}

	/**
	 * Get data by var name
	 * @param array $valuepairs
	 * @param string $key
	 * @return string
	 */
	protected function getValueByKey($valuepairs, $key)
	{
		$retval = "";

		if (is_array($valuepairs))
		{
			foreach ($valuepairs as $valuepair)
			{
				$parts = explode("=", $valuepair);

				if ($parts[0] == $key)
				{
					$retval = $parts[1];
					break;
				}
			}
		}
		return $retval;
	}

	/**
	 * Processing Auth-Captures
	 * @global array $settings
	 * @param DB $db
	 * @param USER $user
	 * @param ORDER $order
	 * @param array $post_form
	 * @return mixed
	 */
	protected function processAuthCapture($db, $user, $order, $post_form)
	{
		global $settings;

		// set post fields
		reset($this->common_vars);
		$postdata =
			"requestType=BACKEND&".
			"merchant_id=" . urlencode($this->settings_vars[$this->id . '_Merchant_Id']['value'])."&".
			"trnCardOwner=" . urlencode($post_form["cc_first_name"] . " " . $post_form["cc_last_name"])."&".
			"trnCardNumber=" . urlencode($post_form["cc_number"])."&".
			"trnExpMonth=" . urlencode($post_form["cc_expiration_month"])."&".
			"trnExpYear=" . urlencode(substr($post_form["cc_expiration_year"], 2, 2))."&".
			"trnOrderNumber=" . urlencode($this->common_vars["order_id"]["value"])."&".
			"trnAmount=" . urlencode(number_format($this->common_vars["order_total_amount"]["value"], 2, ".", ""))."&".
			"ordEmailAddress=" . urlencode($this->common_vars["billing_email"]["value"])."&".
			"ordName=" . urlencode($this->common_vars["billing_first_name"]["value"] . " " . $this->common_vars["billing_last_name"]["value"])."&".
			"ordPhoneNumber=" . urlencode($this->common_vars["billing_phone"]["value"])."&".
			"ordAddress1=" . urlencode($this->common_vars["billing_address1"]["value"])."&".
			"ordAddress2=" . urlencode($this->common_vars["billing_address2"]["value"])."&".
			"ordCity=" . urlencode($this->common_vars["billing_city"]["value"])."&".
			"ordProvince=" . urlencode($this->common_vars["billing_state_abbr"]["value"])."&".
			"ordPostalCode=" . urlencode($this->common_vars["billing_zip"]["value"])."&".
			"ordCountry=" . urlencode($this->common_vars["billing_country_iso_a2"]["value"])."&".
			"shipName=" . urlencode($this->common_vars['shipping_name']['value']).'&'.
			'shipAddress1=' . urlencode($this->common_vars['shipping_address1']['value']).'&'.
			'shipAddress2='.urlencode($this->common_vars['shipping_address2']['value']).'&'.
			'shipCity='.urlencode($this->common_vars['shipping_city']['value']).'&'.
			'shipProvince='.urlencode($this->common_vars['shipping_state_abbr']['value']).'&'.
			'shipPostalCode='.urlencode($this->common_vars['shipping_zip']['value']).'&'.
			'shipCountry='.urlencode($this->common_vars['shipping_country_iso_a2']['value']).'&'.
			'shipPhoneNumber='.urlencode($this->common_vars['billing_phone']['value']).'&'.
			'shippingMethod='.urlencode(substr('', 0, 64)).'&'.
			'shippingRequired='.($order->getShippingRequired() ? '1' : '')
		;

		// set gateway url
		$posturl = trim($this->url_to_gateway);

		// setup curl
		$c = curl_init($posturl);

		if ($settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_VERBOSE, 0);
		curl_setopt($c, CURLOPT_HEADER, 0);
		curl_setopt($c, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		@set_time_limit(3000);

		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
		$response = curl_exec($c);

		$requestData = array();
		parse_str($postdata, $requestData);

		$this->log("processAuthCapture Request:\n".$posturl, $requestData);
		$responseData = array();
		parse_str($response, $responseData);
		$this->log("processAuthCapture Response:", $responseData);

		// process response
		if ($response)
		{
			$valuepairs = explode("&", $response);
			$approved = intval($this->getValueByKey($valuepairs, "trnApproved"));

			if ($approved)
			{
				// Transaction succeeded
				$this->createTransaction($db, $user, $order, $valuepairs);

				if ($this->support_ccs && $this->enable_ccs)
				{
					$card_data = array(
						"fn" => $post_form['cc_first_name'],
						"ln" => $post_form['cc_last_name'],
						"ct" => $post_form['cc_type'],
						"cc" => $post_form['cc_number'],
						"em" => $post_form['cc_expiration_month'],
						"ey" => $post_form['cc_expiration_year']
					);
					$this->saveCCdata($db, $order, $card_data, base64_decode($settings["SecurityCCSCertificate"]));
				}

				$this->is_error = false;
				$this->error_message = "";
			}
			else
			{
				// Transaction failed
				$this->is_error = true;
				$this->error_message = urldecode($this->getValueByKey($valuepairs, "messageText"));
			}
		}
		else
		{
			$this->is_error = true;
			$this->error_message = PAYMENT_SERVER_ERROR_TEXT;
		}

		if ($this->is_error)
		{
			$this->storeTransaction($db, $user, $order, $response, '', false);
		}

		return !$this->is_error;
	}

	/**
	 * Main payment module entry point
	 * @param DB $db
	 * @param USER $user
	 * @param ORDER $order
	 * @param array $post_form
	 * @return mixed
	 */
	public function process($db, $user, $order, $post_form)
	{
		return $this->processAuthCapture($db, $user, $order, $post_form);
	}
}
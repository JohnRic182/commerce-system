<?php 
	$payment_processor_id = "paypalipn";
	$payment_processor_name = "PayPal  Payments Standard";
	$payment_processor_class = "PAYMENT_PAYPALIPN";
	$payment_processor_type = "ipn";

	class PAYMENT_PAYPALIPN extends PAYMENT_PROCESSOR
	{
		protected $buttonSource = '';

		function PAYMENT_PAYPALIPN()
		{
			parent::PAYMENT_PROCESSOR();
			$this->id = "paypalipn";
			$this->name = "PayPal Payments Standard";
			$this->class_name = "PAYMENT_PAYPALIPN";
			$this->type = "ipn";
			$this->description = "";
			$this->post_url = "https://www.paypal.com/cgi-bin/webscr";
			$this->steps = 2;
			$this->possible_payment_delay = true;
			$this->possible_payment_delay_timeout = 30; // seconds
			$this->allow_change_gateway_url = false;

			$this->buttonSource = 'PCart_cart_IPN_US';

			return $this;
		}

		//redeclare getPaymentForm 
		function getPaymentForm($db)
		{
			global $_SESSION;
			global $settings, $order, $url_https;
			global $msg;

			$_pf = parent::getPaymentForm($db);

			//$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page."&oa=".ORDER_PROCESS_PAYMENT;
			$notify_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?securityId=".$this->common_vars["order_security_id"]["value"]."&oid=".$this->common_vars['order_oid']['value']."&p=paypal_callback";
			$return_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=completed";
			$cancel_url = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id()."&_pcod=".$this->common_vars["order_security_id"]["value"]."&p=".$this->payment_page;

			// wmw 04/05/05 Added to get phone number fields for PayPal
			if ($this->common_vars["billing_country_iso_a2"]["value"] == "US")
			{
				$pp_phone = preg_replace("[-\(\)\. ]","",$this->common_vars["billing_phone"]["value"]);
				$pp_phone_a = substr($pp_phone,0,3);
				$pp_phone_b = substr($pp_phone,3,3);
				$pp_phone_c = substr($pp_phone,6,4);
			} 
			else 
			{
				$pp_phone_a = "";
				$pp_phone_b = "";
				$pp_phone_c = "";
			}
			// END wmw 04/05/05

			$fields = array();

			$fields[] = array("type"=>"hidden", "name"=>"business", "value"=>$this->settings_vars[$this->id."_business"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"receiver_email", "value"=>$this->settings_vars[$this->id."_business"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"item_name", "value"=>$settings["CompanyName"].' Order #'.$this->common_vars["order_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"item_number", "value"=>$this->common_vars["order_id"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"first_name", "value"=>$this->common_vars["billing_first_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"last_name", "value"=>$this->common_vars["billing_last_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address1", "value"=>$this->common_vars["billing_address1"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address2", "value"=>$this->common_vars["billing_address2"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"city", "value"=>$this->common_vars["billing_city"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"state", "value"=>$this->common_vars["billing_state_abbr"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"zip", "value"=>$this->common_vars["billing_zip"]["value"]);

			$fields[] = array("type"=>"hidden", "name"=>"paymentaction", "value"=>strtolower($this->settings_vars[$this->id."_Transaction_Type"]["value"]));
			$fields[] = array("type"=>"hidden", "name"=>"currency_code", "value"=>$this->settings_vars[$this->id."_Currency_Code"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"cmd", "value"=>"_xclick");
			$fields[] = array("type"=>"hidden", "name"=>"bn", "value"=> $this->buttonSource);

			$fields[] = array("type"=>"hidden", "name"=>"quantity", "value"=>"1");
			$fields[] = array("type"=>"hidden", "name"=>"invoice", "value"=>$this->common_vars["order_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"custom", "value"=>$this->common_vars["order_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"shipping", "value"=>"0.00");
			$fields[] = array("type"=>"hidden", "name"=>"amount", "value"=>round($this->common_vars["order_total_amount"]["value"], 2));

			$fields[] = array("type"=>"hidden", "name"=>"address_name", "value"=>$this->common_vars["shipping_name"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address_street", "value"=>trim($this->common_vars["shipping_address"]["value"]));
			$fields[] = array("type"=>"hidden", "name"=>"address_city", "value"=>$this->common_vars["shipping_city"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address_state", "value"=>$this->common_vars["shipping_state_abbr"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address_zip", "value"=>$this->common_vars["shipping_zip"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address_country", "value"=>$this->common_vars["shipping_country"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"address_status", "value"=>"unconfirmed");

			$fields[] = array("type"=>"hidden", "name"=>"country", "value"=>$this->common_vars["billing_country_iso_a2"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"email", "value"=>$this->common_vars["billing_email"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"no_shipping", "value"=>"2");

			$fields[] = array("type"=>"hidden", "name"=>"payer_id", "value"=>$this->common_vars["customer_id"]["value"]);
			$fields[] = array("type"=>"hidden", "name"=>"notify_version", "value"=>"1.6");
			$fields[] = array("type"=>"hidden", "name"=>"notify_url", "value"=>$notify_url);
			$fields[] = array("type"=>"hidden", "name"=>"return", "value"=>$return_url);
			$fields[] = array("type"=>"hidden", "name"=>"cancel_return", "value"=>$cancel_url);

			$certId = $this->settings_vars[$this->id."_Cert_ID"]["value"];
			if (trim($certId) != '')
			{
				// Use encryption
				$temp = $fields;
				$fields = array();

				$random_string = rand(1000000, 9999999).'-'.$order->oid.'-';

				$data = '';
				foreach ($temp as $field)
				{
					$data .= $field['name'] .'='. $field['value'] ."\n";
				}
				$data .= 'cert_id='.$certId."\n";

				$dataFile = $settings['GlobalServerPath'].'/content/cache/tmp/paypal-'.$random_string.'data.txt';
				$signedFile = $settings['GlobalServerPath'].'/content/cache/tmp/paypal-'.$random_string.'signed.txt';
				$encryptedFile = $settings['GlobalServerPath'].'/content/cache/tmp/paypal-'.$random_string.'encrypted.txt';

				$certFile = $settings['GlobalServerPath'].'/content/engine/payment/paypal/paypal.cer';
				$permFile = $settings['GlobalServerPath'].'/content/engine/payment/paypal/paypal.pem';
				$paypalKey = $settings['GlobalServerPath'].'/content/engine/payment/paypal/'.($this->isTestMode() ? 'paypal_sandbox_public.pem' : 'paypal_public.pem');

				$fp = fopen($dataFile, 'w');
				fwrite($fp, $data);
				fclose($fp);

				unset($data);

				$data = '';
				if (function_exists('openssl_pkcs7_sign') && function_exists('openssl_pkcs7_encrypt'))
				{
					if (@openssl_pkcs7_sign($dataFile, $signedFile, @file_get_contents($certFile), @file_get_contents($permFile), array('From' => $this->settings_vars[$this->id."_business"]["value"]), PKCS7_BINARY))
					{
						unlink($dataFile);
	 
						$signed = @file_get_contents($signedFile);
						$signed = explode("\n\n", $signed);
						$signed = base64_decode($signed[1]);

						$fp = fopen($signedFile, 'w');
						fwrite($fp, $signed);
						fclose($fp);

						unset($signed);

						openssl_pkcs7_encrypt($signedFile, $encryptedFile, @file_get_contents($paypalKey), array('From' => $this->settings_vars[$this->id."_business"]["value"]), PKCS7_BINARY);

						unlink($signedFile);

						$data = @file_get_contents($encryptedFile);
						$data = explode("\n\n", $data);
						$data = '-----BEGIN PKCS7-----' . "\n" . $data[1] . "\n" . '-----END PKCS7-----';

						unlink($encryptedFile);

						$fields[] = array('type' => 'hidden', 'name' => 'cmd', 'value' => '_s-xclick');
						$fields[] = array('type' => 'hidden', 'name' => 'encrypted', 'value' => $data);
					}
				}

				if (trim($data) == '')
				{
					//Reset the fields if encryption failed

					$this->log('Encryption failed, returning non-encrypted form');
					error_log('PayPal IPN Encryption failed, returning non-encrypted form');

					$fields = $temp;
				}
			}

			return $fields;
		}

		function getValidatorJS()
		{
			return "";
		}

		function isTestMode()
		{
			if (strtolower($this->settings_vars[$this->id."_Mode"]["value"]) == "test")
			{
				$this->testMode = true;
				$this->post_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
			}
			else
			{
				$this->testMode = false;
				$this->post_url = "https://www.paypal.com/cgi-bin/webscr";
			}
			return $this->testMode;
		}

		function getPostUrl()
		{
			return $this->post_url;
		}

		function process($db, $user, $order, $post_form)
		{
			global $_POST;
			global $settings;

			$this->log('Process Request:', $_POST);

			if (trim(strtolower($post_form["payment_status"])) == "completed")
			{
				if (isset($post_form['mc_gross']) && $order->totalAmount != $post_form['mc_gross'])
				{
					$post_form['payment_status'] = 'pending';
					$post_form['pending_reason'] = 'MC_GROSS does not match order total amount';
				}
			}

			//check payment status
			if ((trim(strtolower($post_form["payment_status"])) == "completed") || (trim(strtolower($post_form["payment_status"])) == "pending"))
			{
				//check verification value
				if (Payment_PayPal_PayPalApi::verifyIPN($this->isTestMode()))
				{
					reset($post_form);
					$response = "Verification Status: verified\n";

					$payment_status = '';
					$PendingReason = '';
					$transaction_id = '';
					foreach ($post_form as $var_name=>$var_value)
					{
						$response.=$var_name." = ".$var_value."\n";
						
						if ($var_name == 'txn_id') $transaction_id = $var_value;
						
						if ($var_name == "payment_status") $payment_status = $var_value;	
						
						if ($var_name == "pending_reason") $PendingReason = $var_value;
					}

					$this->createTransaction($db, $user, $order, $response, "", $PendingReason, "", 0, $transaction_id);
					$this->is_error = false;
					$this->error_message = "";

					return strtolower($payment_status) == 'completed' ? 'Received' : 'Pending';
				}
				else
				{
					$this->is_error = true;

					$this->storeTransaction($db, $user, $order, $post_form, '', false);

					$this->error_message = "Can't complete your order. PayPal payment is not completed yet.";
				}
			}
			else
			{
				$this->is_error = true;
				$this->error_message = "An error occurred while PayPal was processing your order. It will be investigated by a human at the earliest opportunity. We apologise for any inconvenience.";
			}

			return !$this->is_error;
		}

		function success()
		{
			die();
		}
	}

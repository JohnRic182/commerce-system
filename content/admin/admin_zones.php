<?php
	if (!defined('ENV')) exit();

	//groups and privileges
	$adminPages = array(
		//common privileges
		'any'=>array('home', 'profile', 'profile_openid', 'profile_security', 'api', 'gift_cert', 'search', 'file_validate', 'speed_test', 'paypal_payments_standard'),
		//manage products
		'products'=>array(
			'products', 'product',
			'bulk_products',
			'products_attributes', 'products_attribute', 'product_page_attributes', 'product_page_variants',
			'product_page_promotions', 'product_page_qd',
			'manufacturers', 'manufacturer',
			'doba', 'doba_json',
			'bulk_manufacturers',
			'categories', 'category', 'bulk_categories', 'bulk_global_attributes_products',
			'order_form', 'order_forms'
		),
		//users
		'users'=>array('customers', 'customer', 'user_payment_profiles'),
		'customers'=>array('customers', 'customer','user_payment_profiles'),
		//orders
		'orders'=>array(
			'orders', 'endicia_generatepostagelabel', 'order', 'order_invoice',
			'vparcel',
			'order_endicia', 'intuit_quickstart',
			'order_stamps',
			'order_stampscom_ajax'
		),
		'recurring'=>array('recurring_profiles'),
		//globaldddd
		'global'=>array(
			'settings',
			'settings_intuit',
			'quickbooks',
			'store_settings',
			'store_info',
			'settings_invoice',
			'endicia',
			'countries','states',
			'settings_wholesale',
			'order_notifications',
			'languages', 'language',
			'update',
			'currency',
			'currencies',
			'apps', 'app',
			'facebook_login',
			'checkout',
			'stamps',
			'backup_restore',
			'admins', 'admin', 'admins_log',
			'email_notifications', 'email_notification',
			'taxes', 'taxes_zones', 'taxes_zone', 'taxes_classes', 'taxes_class', 'taxes_rates', 'taxes_rate', 'taxes_settings',
			'payment_methods', 'payment_method', 'payment_cardinal',
			'fraud_methods', 'fraud_method',
			'shipping', 'shipping_realtime_settings', 'shipping_custom_rates',
			'pikfly',
			'paypalipn_gencsr', 'bongo', 'bongo_extend', 'paypal_bml',
			'settings_social',
			'widgets',
			'digital_download',
			'quick_start_guide',
		),
		'ccs'=>array('settings_ccs'),
		//business data
		'business'=>array(),
		//content
		'content'=>array(
			'pages', 'page',
			'design_settings', 'design_images',
			'design_catalog',
			'filemanager',
			'forms_control', 'custom_forms', 'custom_form', 'custom_form_field', 'custom_form_data',
			'design_mode',
			'testimonial',
			'testimonials',
			'products_reviews', 'products_review',
			'thumbs_generator',
			'snippet', 'snippets',
		),
		//statistics
		'stats'=>array('reports', 'report', 'dashboard_reports', 'order_reports', 'revenue_reports'),
		'marketing'=>array(
			'google_marketing',
			'shopzilla',
			'norton_seal',
			'wholesale_central',
			'affiliate', 'drift_marketing',
			'mailchimp','braintree',
			'marketing_qr_ajax',
			'avalara', 'avalara_tax', 'avalara_tree', 'exactor', 'exactor_euc_ajax', 'exactor_euc_tree', 'promotions',
			'products_families', 'products_family',
			'settings_qr',
			'settings_search',
			'settings_bestsellers',
		),
		'billing' => array('billing'),
	);

	if (_ACCESS_PRODUCTS_FILTERS && _CAN_ENABLE_PRODUCTS_FILTERS)
	{
		$adminPages['products'][] = 'product_feature';
		$adminPages['products'][] = 'features_groups';
		$adminPages['products'][] = 'feature_groups';
		$adminPages['products'][] = 'products_features';
		$adminPages['products'][] = 'products_feature';
	}

	$body = 'admin_home.php';
	$pageFound = false;
	reset($adminPages);
	foreach ($adminPages as $zone => $zonePages)
	{
		if (in_array($p, $zonePages))
		{
			//check privileges
			if (in_array($zone, $auth_rights) || ($zone == 'any') || in_array('all', $auth_rights))
			{
				$body = 'admin_' . $p . '.php';
				$pageFound = true;
			}
			else
			{
				$p = 'security_error';
				$body = 'admin_security_error.php';
			}

			break;
		}
	}

	if (!$pageFound) $p = 'home';


<?php

	/**
	 * Prepare cron environment
	 */
	define('ENV', true);
	define('CRON', true);

	define('BASE_PATH', dirname(dirname(dirname(dirname(__FILE__)))) . '/');
	chdir(BASE_PATH);
	
	echo date('c').' - Cron system init, base path set to: ' . BASE_PATH . "\n";

	require_once 'content/classes/_boot.php';

	view()->initNotificationsSmarty();
	$currencies = new Currencies($db);

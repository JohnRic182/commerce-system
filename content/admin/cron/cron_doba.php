<?php

	echo date("c")." - Doba products update\n";
	
	$doba = new Doba_ProductApi($settings, $db);
	
	$watchLists = $doba->getWatchlists();
	
	$effectedProducts = "";
	
	if ($watchLists && $watchLists['watchlist'])
	{
		echo "Watchlists found: ".count($watchLists['watchlist'])."\n";
		
		foreach ($watchLists['watchlist'] as $watchList)
		{
			echo "\nUpdating watchlist ".$watchList['name']."\n";

			$watchlist_done = false;
			$watchlist_page = 1;

			while (!$watchlist_done)
			{
				$watchlistId = $watchList['watchlist_id'];
				$watchlist_products = $doba->getWatchlistProducts($watchlistId, $watchlist_page);
				$logFile = 'content/cache/log/doba-skipped-products-'.escapeFileName($watchlistId).'-'.date('Ymd').'.txt';

				if ($watchlist_products && count($watchlist_products))
				{
					if (!array_key_exists(0, $watchlist_products)) $watchlist_products = array($watchlist_products);
					foreach ($watchlist_products as $product)
					{
						$items = isset($product['items']['item'][0]) ? $product['items']['item'] : array($product['items']['item']);
						foreach ($items as $item)
						{
							$effectedProducts .= ($effectedProducts == "" ? "" : ",")."'".$db->escape($item['item_id'])."'";

							$doba->itemToDb($product, $item, false, $settings["doba_images"] == "No", $logFile);

							if (!in_array("images", $doba->lastProductLockedFields))
							{
								if ($settings["doba_images"] != "No")
								{
									$doba->getItemDefaultImage($doba->getItemDefaultImageUrl($product), $item['item_id'], $settings["doba_images"] == "All");

									if ($settings["doba_images_secondary"] == "Yes")
									{
										$doba->getItemSecondaryImages($doba->getItemSecondaryImagesUrl($product), $item['item_id'], $settings["doba_images"] == "All");
									}
								}
								else
								{
									$imageFileName = ImageUtility::imageFileName($item['item_id']);
									
									$imageFile = 'images/products/' . $imageFileName . '.jpg';
									$previewFile = 'images/products/preview/' . $imageFileName . '.jpg';
									$thumbFile = 'images/products/thumbs/' . $imageFileName . '.jpg';
									
									if (is_file($imageFile)) @unlink($imageFile);
									if (is_file($previewFile)) @unlink($previewFile);
									if (is_file($thumbFile)) @unlink($thumbFile);
								}
							}
							echo ".";
						}
					}
					$watchlist_page++;
				}
				else
				{
					$watchlist_done = true;
				}
			}
		}
		Seo::updateSeoURLs($db, $settings, true, true, false, true);
		echo "\n";
	}
	
	if ($effectedProducts != "")
	{	
		$productsToRemove = "";
		$db->query("SELECT pid FROM ".DB_PREFIX."products WHERE is_doba='Yes' AND product_id NOT IN (".$effectedProducts.")");
		while ($db->moveNext()) $productsToRemove .= ($productsToRemove==""?"":",").$db->col["pid"];
		if ($productsToRemove != "")
		{
			$db->query("DELETE FROM ".DB_PREFIX."products WHERE pid IN (".$productsToRemove.")");
			$db->query("DELETE FROM ".DB_PREFIX."products_categories WHERE pid IN (".$productsToRemove.")");
			
			echo "Removed products: ".count($productsToRemove)." - ".$productsToRemove."\n";
		}
	}
	
	//echo "Synchronizing database products with doba watchlists: Removed ".count($products_removed)." items"."\n";
	
	//updateSeoURLs($db, $settings, true, true, false, true);
	

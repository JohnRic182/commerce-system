<?php

	echo date("c")." - CCS cleanup cron job started\n";
	
	$super_admins = array();
	$inactive_accounts = array();
	
	$db->query("SELECT *, NOW() AS now FROM ".DB_PREFIX."orders WHERE card_data <> '' AND DATE_ADD(placed_date, INTERVAL ".$settings["SecurityCCSStoredFor"]." DAY) < NOW()");
	if (($c = $db->numRows()) > 0)
	{
		$db->query("UPDATE ".DB_PREFIX."orders SET card_data='' WHERE DATE_ADD(placed_date, INTERVAL ".$settings["SecurityCCSStoredFor"]." DAY) < NOW()");
		echo date("c")." - CC data cleaned up in ".$c."orders\n";
	}
	
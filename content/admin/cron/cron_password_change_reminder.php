<?php

	echo date("c")." - Password change reminder cron job started\n";
	
	$super_admins = array();
	$admins_with_expired_passwords = array();
	
	$db->query("SELECT *, CONCAT(fname, ' ', lname) AS name, NOW() AS now FROM ".DB_PREFIX."admins");
	while (($admin = $db->moveNext()) != false)
	{	
		//get admin rights
		$admin_rights = explode(",", $admin["rights"]);

		//save somewhere super admins
		if (in_array("all", $admin_rights) || in_array("admins", $admin_rights))
		{
			$super_admins[] = $admin;
		}
		
		//see what this admin wants to receive
		$receive_notifications = explode(",", $admin["receive_notifications"]);
		$password_age = (strtotime($admin["now"]) - strtotime($admin["password_change_date"])) / 86400; // in days
		if (
			(in_array("pwdchange30", $receive_notifications) && $password_age >= 30) ||
			(in_array("pwdchange60", $receive_notifications) && $password_age >= 60) ||
			(in_array("pwdchange90", $receive_notifications) && $password_age >= 90)
		)
		{
			view()->assign("admin", $admin);
			view()->assign("password_age", round($password_age));
			$email = view()->fetch("templates/emails/admin_password_change_reminder_html.html");
			
			Notifications::sendMail($admin["email"], "Password change reminder", $email, "");
			echo date("c")." - Reminder sent to '".$admin["username"]."'\n";
		}
		
		//check for compliant passwords
		if ($settings["SecurityForceCompliantAdminPasswords"] == "YES" && $password_age >= 90)
		{
			$admins_with_expired_passwords[] = $admin;
			echo date("c")." - '".$admin["username"]."' password older as 90 days\n";
		}
	}
	
	//if there are some expired password, send emails to admin with rights to change them
	if (count($super_admins) > 0 && count($admins_with_expired_passwords) > 0)
	{
		view()->assign("admins_with_expired_passwords", $admins_with_expired_passwords);
		
		foreach ($super_admins as $admin)
		{
			view()->assign("admin", $admin);
			$email = view()->fetch("templates/emails/admin_password_expired_html.html");
			Notifications::sendMail($admin["email"], "Accounts with expired passwords", $email, "");
			echo date("c")." - Accounts with expired passwords area sent to '".$admin["username"]."'\n";
		}
	}
<?php

if ($settings['intuit_anywhere_enabled'] == 'Yes' && $settings['intuit_anywhere_cron_enabled'] == 'Yes')
{
	define('_ACCESS_INTUIT_ANYWHERE', true);

	require_once BASE_PATH.'content/vendors/intuit/oauth/config.php';

	echo date("c")." - Intuit Anywhere cron job started\n";

	$intuitSync = intuit_Services_QBSync::create();

	if (!is_null($intuitSync))
	{
		try
		{
			$intuitSync->doSync();
		}
		catch (intuit_AuthorizationFailureException $e)
		{
			if (defined('DEVMODE')) error_log($e->getMessage());

			intuit_ServiceLayer::disconnect();

			$oauth_token = '';
			$oauth_token_secret = '';
		}
	}

	echo date("c")." - Intuit Anywhere cron job finished\n";
}
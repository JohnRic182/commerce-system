<?php
/**
 * Cron
 */
	require_once('cron_init.php');

	$jobGroupId = isset($argv[1]) ? $argv[1] : (isset($_REQUEST['job_group_id']) ? $_REQUEST['job_group_id'] : '12hour');

	if ($jobGroupId == '5min')
	{
    	include(BASE_PATH.'content/admin/cron/cron_drift_marketing_emails.php');
	}
	else if ($jobGroupId == '12hour')
	{
    	if ($settings['doba_cron_update'] == 'Yes')
		{
			include(BASE_PATH.'content/admin/cron/cron_doba.php');
		}

    	if ($settings['SecurityCCSActive'])
		{
			include(BASE_PATH.'content/admin/cron/cron_ccs.php');
		}

    	include(BASE_PATH.'content/admin/cron/cron_password_change_reminder.php');
    	include(BASE_PATH.'content/admin/cron/cron_inactive_accounts_notification.php');

		if ($settings['intuit_anywhere_enabled'] == 'Yes')
		{
			include(BASE_PATH.'content/admin/cron/cron_intuit_anywhere.php');
		}

		if ($settings['WholesaleCentralEnableCron'] == 1)
		{
			include(BASE_PATH.'content/admin/cron/cron_wholesalecentral.php');
		}
	}

	//TODO: We need to register more events than these, include Register.php?
	$registry = new Framework_Registry();
	$registry->set('db', $db);
	$registry->setParameter('settings', $settings);

	if (isset($settings['RecurringBillingEnabled']) && $settings['RecurringBillingEnabled'])
	{
		RecurringBilling_Plugin::init($registry);
	}
	PaymentProfiles_Plugin::init($registry);

	/**
	 * Cron events
	 */
	if ($settings['RecurringBillingEnabled'] == '1')
	{
		Events_EventHandler::registerListener(Events_CronEvent::ON_EVERY_12HOURS, array('RecurringBilling_Listener_RecurringBillingCron', 'onProcessRecurringBilling'));
		Events_EventHandler::registerListener(Events_CronEvent::ON_EVERY_12HOURS, array('RecurringBilling_Listener_RecurringBillingCron', 'onCheckCardsExpiration'));
	}

	Events_EventHandler::registerListener(Events_CronEvent::ON_EVERY_5MIN, array('Listener_StockCleanup', 'onCron'));

	$eventNames = array(
		'5min' => Events_CronEvent::ON_EVERY_5MIN,
		'hour' => Events_CronEvent::ON_EVERY_HOUR,
		'12hour' => Events_CronEvent::ON_EVERY_12HOURS,
		'day' => Events_CronEvent::ON_EVERY_DAY,
		'week' => Events_CronEvent::ON_EVERY_WEEK,
		'2weeks' => Events_CronEvent::ON_EVERY_2WEEKS,
		'month' => Events_CronEvent::ON_EVERY_MONTH
	);

	$eventName = isset($eventNames[$jobGroupId]) ? $eventNames[$jobGroupId] : Events_CronEvent::ON_EVERY_12HOURS;

	Events_EventHandler::handle(new Events_CronEvent($eventName));

<?php
    require_once("cron_init.php");
    echo date("c")." - Drift marketing emails cron job started\n";

    view()->initNotificationsSmarty();
    $productUtil = new ShoppingCartProducts($db, $settings);

    $allCampaigns = DriftEmailCampaign::LoadAll($db);
    foreach ($allCampaigns as $campaign)
    {
        $timeframe = $campaign->GetTimeframe()*60;

        $orderRows = DriftEmailCampaign::GetOrders($db, $campaign->GetCampaignId());

        foreach ($orderRows as $orderRow)
        {
            $orderStatusDate = strtotime($orderRow["status_date"]);

            if ((time() - $orderStatusDate) > $timeframe)
            {
                $oid = $orderRow["oid"];

                $sql = "select a.*, b.image_url, b.image_location from ".DB_PREFIX."orders_content a, ".DB_PREFIX."products b where
                        a.oid = $oid
                        and
                        b.pid = a.pid";
                $db->query($sql);
                $orderItemRows = $db->getRecords();

                foreach ($orderItemRows as &$orderItemRow)
                {
                    $thumbPath = $productUtil->getProductThumb($orderItemRow["product_id"], $orderItemRow["image_location"], $orderItemRow["image_url"]);
                    if ($thumbPath)
                    {
                        $thumbImage = $settings["GlobalHttpUrl"] . "/" . $thumbPath;
                        $orderItemRow["thumb_image_url"] = $thumbImage;
                    }
                }
                //send mail to the customer
				Notifications::emailCartAbandoned($db, $orderRow, $orderItemRows, $orderRow, $campaign->GetHtmlEmail(), $campaign->GetEmailSubject());

                //save it into the history that we sent this email
                $db->reset();
                $db->assign("oid",$oid);
                $db->assign("campaign_id",$campaign->GetCampaignId());
                $db->insert(DB_PREFIX."drift_email_history");
            }
        }
    }
    echo date("c")." - Drift marketing emails cron job completed\n";

?>
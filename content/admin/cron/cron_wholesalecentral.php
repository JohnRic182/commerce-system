<?php

if ($settings['WholesaleCentralEnableCron'] == '1')
{
	echo date("c")." - WholesaleCentral.com cron job started\n";

	try
	{
		if (!isset($_SERVER["SERVER_PORT"]))
		{
			$_SERVER['SERVER_PORT'] = 80;
		}
		
		$wcClient = new WholesaleCentral($settings['WholesaleCentralUsername'], $settings['WholesaleCentralPassword'], $db);
		$controller = new Controller_WholesaleCentral($wcClient, $db, $settings);
		
		$response = @$controller->sendFeedAction(BASE_PATH.'content/cache/tmp/wholesalecentral.txt');
		
		if ($response['status'] == 'success')
		{
			echo date("c")." - WholesaleCentral.com - EZFeed successfully uploaded\n";
		}
		else
		{
			echo date("c")." - WholesaleCentral.com - EZFeed upload error: ".$response['message']."\n";
		}
	}
	catch (Exception $e)
	{
		if (defined('DEVMODE')) error_log('WholesaleCentral.com cron error: '.$e->getMessage());
	}

	echo date("c")." - WholesaleCentral.com cron job finished\n";
}
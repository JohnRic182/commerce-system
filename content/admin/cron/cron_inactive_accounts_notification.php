<?php

	echo date("c")." - Inactive accounts notification cron job started\n";
	$super_admins = array();
	$inactive_accounts = array();
	
	$db->query("SELECT *, CONCAT(fname, ' ', lname) AS name, NOW() AS now FROM ".DB_PREFIX."admins WHERE active <> 'No' AND (expires = 0 OR expiration_date > NOW())");
	while (($admin = $db->moveNext()) != false)
	{		

		//check for inactive accounts
		$last_login = (strtotime($admin["now"]) - strtotime($admin["last_access"])) / 86400; // days ago

		if ($settings["SecurityForceCompliantAdminPasswords"] == "YES" && $last_login >= 90)
		{
			$inactive_accounts[] = $admin;
			echo date("c")." - '".$admin["username"]."' account is not active for more as 90 days\n";
		}

		//get admin rights
		$admin_rights = explode(",", $admin["rights"]);

		$receive_notifications = explode(",", $admin["receive_notifications"]);
		//save somewhere super admins
		if ((in_array("all", $admin_rights) || in_array("admins", $admin_rights)) && in_array("notactive", $receive_notifications))
		{
			$super_admins[] = $admin;
		}
	}

	//if there are some inactive accounts, send emails to admin with rights to change them
	if (count($super_admins) > 0 && count($inactive_accounts) > 0)
	{
		view()->assign("inactive_accounts", $inactive_accounts);
		
		foreach ($super_admins as $admin)
		{
			view()->assign("admin", $admin);
			$email = view()->fetch("templates/emails/admin_inactive_accounts_html.html");
			Notifications::sendMail($admin["email"], "Inactive accounts notification", $email, "");
			echo date("c")." - Inactive accounts notification is sent to '".$admin["username"]."'\n";
		}
	}
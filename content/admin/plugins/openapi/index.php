<?php
/**
 * API Entry Point.
 */

	require_once 'initialize.php';
	
	//ApiCalls.php is where the actual api method implementation is, regardless of apiType
	require_once PLUGIN_PATH . 'ApiCalls.php';
	
	//if a php error occurs, we do not want to output html.  We use a custom handler to output api appropriate errors.
	set_error_handler(array("ApiCalls","ErrorHandler"));
	
	//This is the data that will be returned to the caller
	$processorOutput = "";
	
	$processorKey = isset($_REQUEST["apiType"]) ? $_REQUEST["apiType"] : null;
	$downloadFile = isset($_REQUEST["download"]) ? $_REQUEST["download"] : null;
	$template = isset($_REQUEST["template"]) ? str_replace(chr(0), '', ltrim($_REQUEST["template"], '/\\')) : null;
	$call = isset($_REQUEST["call"]) ? $_REQUEST["call"] : null;
	
	//some processors (namely soap) require the raw http data
	$rawData = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : null;
	
	//some api calls are public and do not require authentication,
	//check if the requested call is one of them.
	$unauthAccessAllowed = false;
	if ($call && isset(ApiCalls::$CallRequiresAuth[$call]) && ApiCalls::$CallRequiresAuth[$call] == false)
	{
	    $unauthAccessAllowed = true;
	}

	if ($processorKey) //$processorKey is required
	{
		$processor = $processors[$processorKey];
	}
	else
	{
		echo '';
		exit();
	}

	if ($processor)
	{
		require_once PLUGIN_PATH . 'processors/' . $processor . '.php';
	}
	else
	{
		throw new ErrorException("Invalid api type specified", 0, E_ERROR, __FILE__, __LINE__);
	}
	
	//if the user passed valid credentials, or if this call is public, go ahead and process the request,
	//otherwise send a notAuthenticated error
	if ($plugin_auth || $unauthAccessAllowed)
	{
	    $processorOutput = call_user_func_array(array($processor, "Process"), array($rawData, $_POST, $_GET, isset($_FILE) ? $_FILE : null, $_REQUEST, $template));
	}
	else
	{
	    $processorOutput = call_user_func_array(array($processor, "ProcessNotAuthenticated"), array($rawData, $_POST, $_GET, isset($_FILE) ? $_FILE : null, $_REQUEST, $template));
	}
	
	//each api processor can return data of any mime-type it chooses.
	//ask the processor what mime-type it will be outputting
	$mimeType = call_user_func_array(array($processor, "Get_FileMimeType"), array());
	$filesize = strlen($processorOutput);
	header("Content-type: $mimeType");
	header("Content-Length: $filesize");

	if ($downloadFile) //send force-download headers
	{
	    $call = isset($_REQUEST["call"])?$_REQUEST["call"]:"unknown_call";
	    $outputFileName = isset($_REQUEST["outfile"])?$_REQUEST["outfile"]:"ProductDataFeed_".$call."_".$processorKey."_".$template;
	    $fileext = call_user_func(array($processor,"Get_FileExtension"));
	    header("Content-Disposition: attachment; filename=\"$outputFileName.$fileext\";");
	}
	
	//output the api processor result
	print $processorOutput;
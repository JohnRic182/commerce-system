<?php

class PinnacleApiWsdl
{
	public static function getEndpointUrl($ApiUsername, $ApiPassword, $ApiToken)
	{
		global $settings;

		return $settings['AdminHttpsUrl'].'/content/admin/plugins/openapi/index.php?apiType=soap&username='.$ApiUsername.'&password='.$ApiPassword.'&token='.$ApiToken;
	}
	public static function generate($ApiUsername, $ApiPassword, $ApiToken)
	{
		return '<?xml version="1.0" encoding="ISO-8859-1"?>
<definitions xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="urn:PinnacleApi" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="urn:PinnacleApi">
	<types>
		<xsd:schema targetNamespace="urn:PinnacleApi">
			<xsd:import namespace="http://schemas.xmlsoap.org/soap/encoding/"/>
			<xsd:import namespace="http://schemas.xmlsoap.org/wsdl/"/>
			<xsd:complexType name="ApiAddress">
				<xsd:all>
					<xsd:element name="Name" type="xsd:string"/>
					<xsd:element name="Street1" type="xsd:string"/>
					<xsd:element name="Street2" type="xsd:string"/>
					<xsd:element name="City" type="xsd:string"/>
					<xsd:element name="State" type="xsd:string"/>
					<xsd:element name="Zip" type="xsd:string"/>
					<xsd:element name="Country" type="xsd:string"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiAddressArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiAddress[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiBillingInfo">
				<xsd:all>
					<xsd:element name="UserId" type="xsd:integer"/>
					<xsd:element name="FullName" type="xsd:string"/>
					<xsd:element name="FirstName" type="xsd:string"/>
					<xsd:element name="LastName" type="xsd:string"/>
					<xsd:element name="Email" type="xsd:string"/>
					<xsd:element name="Company" type="xsd:string"/>
					<xsd:element name="Phone" type="xsd:string"/>
					<xsd:element name="Address" type="tns:ApiAddress"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiCustomer">
				<xsd:all>
					<xsd:element name="UserId" type="xsd:integer"/>
					<xsd:element name="UserName" type="xsd:string"/>
					<xsd:element name="Billing" type="tns:ApiBillingInfo"/>
					<xsd:element name="AddressBook" type="tns:ApiAddressArray"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiCustomerArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiCustomer[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiShippingInfo">
				<xsd:all>
					<xsd:element name="FullName" type="xsd:string"/>
					<xsd:element name="Email" type="xsd:string"/>
					<xsd:element name="Company" type="xsd:string"/>
					<xsd:element name="Phone" type="xsd:string"/>
					<xsd:element name="Address" type="tns:ApiAddress"/>
					<xsd:element name="ShippingRequired" type="xsd:string"/>
					<xsd:element name="ShippingDigital" type="xsd:string"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiProductOption">
				<xsd:all>
					<xsd:element name="Title" type="xsd:string"/>
					<xsd:element name="Caption" type="xsd:string"/>
					<xsd:element name="OptionId" type="xsd:string"/>
					<xsd:element name="Price" type="xsd:float"/>
					<xsd:element name="Type" type="xsd:string"/>
					<xsd:element name="Weight" type="xsd:float"/>
					<xsd:element name="Priority" type="xsd:integer"/>
					<xsd:element name="Options" type="tns:ApiProductOptionArray"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiProductOptionArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiProductOption[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiProductQoh">
				<xsd:all>
					<xsd:element name="PID" type="xsd:integer"/>
					<xsd:element name="Qoh" type="xsd:integer"/>
					<xsd:element name="ProductId" type="xsd:string"/>
					<xsd:element name="ProductSubId" type="xsd:string"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiProductQohArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiProductQoh[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiProduct">
				<xsd:all>
					<xsd:element name="PID" type="xsd:integer"/>
					<xsd:element name="ProductId" type="xsd:string"/>
					<xsd:element name="Qoh" type="xsd:integer"/>
					<xsd:element name="Price" type="xsd:float"/>
					<xsd:element name="DisplayPrice" type="xsd:string"/>
					<xsd:element name="Taxable" type="xsd:string"/>
					<xsd:element name="Weight" type="xsd:float"/>
					<xsd:element name="UPC" type="xsd:string"/>
					<xsd:element name="Title" type="xsd:string"/>
					<xsd:element name="URL" type="xsd:string"/>
					<xsd:element name="ThumbnailImageUrl" type="xsd:string"/>
					<xsd:element name="ImageUrl" type="xsd:string"/>
					<xsd:element name="Discontinued" type="xsd:string"/>
					<xsd:element name="Options" type="tns:ApiProductOptionArray"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiProductArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiProduct[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiProductCategory">
				<xsd:all>
					<xsd:element name="Title" type="xsd:string"/>
					<xsd:element name="CategoryId" type="xsd:integer"/>
					<xsd:element name="Children" type="tns:ApiProductCategoryArray"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiProductCategoryArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiProductCategory[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiOrderItemOption">
				<xsd:all>
					<xsd:element name="Title" type="xsd:string"/>
					<xsd:element name="SelectedOption" type="xsd:string"/>
					<xsd:element name="Price" type="xsd:float"/>
					<xsd:element name="Weight" type="xsd:float"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiOrderItemOptionArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiOrderItemOption[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiOrderItem">
				<xsd:all>
					<xsd:element name="PID" type="xsd:integer"/>
					<xsd:element name="ProductType" type="xsd:string"/>
					<xsd:element name="Quantity" type="xsd:integer"/>
					<xsd:element name="Price" type="xsd:float"/>
					<xsd:element name="Taxable" type="xsd:string"/>
					<xsd:element name="Weight" type="xsd:float"/>
					<xsd:element name="FreeShipping" type="xsd:string"/>
					<xsd:element name="ShippingPrice" type="xsd:float"/>
					<xsd:element name="TaxRate" type="xsd:float"/>
					<xsd:element name="TaxDescription" type="xsd:string"/>
					<xsd:element name="DigitalProduct" type="xsd:string"/>
					<xsd:element name="UpdatedQuantity" type="xsd:integer"/>
					<xsd:element name="UpdatedPrice" type="xsd:float"/>
					<xsd:element name="ProductId" type="xsd:string"/>
					<xsd:element name="ProductSubId" type="xsd:string"/>
					<xsd:element name="SKU" type="xsd:string"/>
					<xsd:element name="UPC" type="xsd:string"/>
					<xsd:element name="Title" type="xsd:string"/>
					<xsd:element name="LineItemWeight" type="xsd:float"/>
					<xsd:element name="ProductWeight" type="xsd:float"/>
					<xsd:element name="Options" type="tns:ApiOrderItemOptionArray"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiOrderItemArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiOrderItem[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ApiOrder">
				<xsd:all>
					<xsd:element name="OrderId" type="xsd:integer"/>
					<xsd:element name="OrderNumber" type="xsd:integer"/>
					<xsd:element name="OrderDate" type="xsd:string"/>
					<xsd:element name="Status" type="xsd:string"/>
					<xsd:element name="StatusDate" type="xsd:string"/>
					<xsd:element name="PaymentMethod" type="xsd:string"/>
					<xsd:element name="PaymentStatus" type="xsd:string"/>
					<xsd:element name="ShippingCarrierId" type="xsd:string"/>
					<xsd:element name="ShippingMethodId" type="xsd:string"/>
					<xsd:element name="ShippingMethodName" type="xsd:string"/>
					<xsd:element name="ShippingAmount" type="xsd:float"/>
					<xsd:element name="ShippingTaxable" type="xsd:string"/>
					<xsd:element name="ShippingTaxRate" type="xsd:float"/>
					<xsd:element name="ShippingTaxAmount" type="xsd:float"/>
					<xsd:element name="ShippingTaxDescription" type="xsd:string"/>
					<xsd:element name="HandlingSeparated" type="xsd:string"/>
					<xsd:element name="HandlingFee" type="xsd:float"/>
					<xsd:element name="HandlingFeeType" type="xsd:string"/>
					<xsd:element name="HandlingAmount" type="xsd:float"/>
					<xsd:element name="HandlingTaxable" type="xsd:string"/>
					<xsd:element name="HandlingTaxRate" type="xsd:float"/>
					<xsd:element name="HandlingTaxAmount" type="xsd:float"/>
					<xsd:element name="HandlingTaxDescription" type="xsd:string"/>
					<xsd:element name="HandlingText" type="xsd:string"/>
					<xsd:element name="SubtotalAmount" type="xsd:float"/>
					<xsd:element name="DiscountAmount" type="xsd:float"/>
					<xsd:element name="DiscountValue" type="xsd:float"/>
					<xsd:element name="DiscountType" type="xsd:string"/>
					<xsd:element name="PromoDiscountAmount" type="xsd:float"/>
					<xsd:element name="PromoDiscountValue" type="xsd:float"/>
					<xsd:element name="PromoDiscountType" type="xsd:string"/>
					<xsd:element name="TaxAmount" type="xsd:float"/>
					<xsd:element name="TotalAmount" type="xsd:float"/>
					<xsd:element name="GiftMessage" type="xsd:string"/>
					<xsd:element name="Billing" type="tns:ApiBillingInfo"/>
					<xsd:element name="Shipping" type="tns:ApiShippingInfo"/>
					<xsd:element name="OrderItems" type="tns:ApiOrderItemArray"/>
				</xsd:all>
			</xsd:complexType>
			<xsd:complexType name="ApiOrderArray">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:ApiOrder[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
			<xsd:complexType name="ArrayOfString">
				<xsd:complexContent>
					<xsd:restriction base="SOAP-ENC:Array">
						<xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="xsd:string[]"/>
					</xsd:restriction>
				</xsd:complexContent>
			</xsd:complexType>
		</xsd:schema>
	</types>
	<message name="GetVersionRequest"></message>
	<message name="GetVersionResponse">
		<part name="return" type="xsd:string"/>
	</message>
	<message name="GetOrderCountRequest">
		<part name="Status" type="xsd:string"/>
		<part name="CustomerId" type="xsd:integer"/>
		<part name="LastOrder" type="xsd:integer"/>
		<part name="LastDate" type="xsd:string"/>
	</message>
	<message name="GetOrderCountResponse">
		<part name="return" type="xsd:integer"/>
	</message>
	<message name="GetOrdersRequest">
		<part name="Status" type="xsd:string"/>
		<part name="CustomerId" type="xsd:integer"/>
		<part name="LastOrder" type="xsd:integer"/>
		<part name="LastDate" type="xsd:string"/>
		<part name="StartNumber" type="xsd:integer"/>
		<part name="BatchSize" type="xsd:integer"/>
	</message>
	<message name="GetOrdersResponse">
		<part name="return" type="tns:ApiOrderArray"/>
	</message>
	<message name="GetCustomerCountRequest"></message>
	<message name="GetCustomerCountResponse">
		<part name="return" type="xsd:integer"/>
	</message>
	<message name="GetCustomersRequest">
		<part name="StartNumber" type="xsd:integer"/>
		<part name="BatchSize" type="xsd:integer"/>
	</message>
	<message name="GetCustomersResponse">
		<part name="return" type="tns:ApiCustomerArray"/>
	</message>
	<message name="GetProductCountRequest"></message>
	<message name="GetProductCountResponse">
		<part name="return" type="xsd:integer"/>
	</message>
	<message name="GetProductsRequest">
		<part name="StartNumber" type="xsd:integer"/>
		<part name="BatchSize" type="xsd:integer"/>
		<part name="BatchPage" type="xsd:integer"/>
		<part name="PrimaryCategory" type="xsd:integer"/>
		<part name="ExtraArgs" type="xsd:string"/>
		<part name="ProductGroup" type="xsd:integer"/>
		<part name="IncludeVatTaxes" type="xsd:string"/>
	</message>
	<message name="GetProductsResponse">
		<part name="return" type="tns:ApiProductArray"/>
	</message>
	<message name="GetCategoriesRequest"></message>
	<message name="GetCategoriesResponse">
		<part name="return" type="tns:ApiProductCategoryArray"/>
	</message>
	<message name="GetInventoryRequest">
		<part name="StartNumber" type="xsd:integer"/>
		<part name="BatchSize" type="xsd:integer"/>
	</message>
	<message name="GetInventoryResponse">
		<part name="return" type="tns:ApiProductQohArray"/>
	</message>
	<message name="UpdateInventoryRequest">
		<part name="Update" type="xsd:string"/>
	</message>
	<message name="UpdateInventoryResponse">
		<part name="return" type="xsd:string"/>
	</message>
	<message name="UpdateInventoryBulkRequest">
		<part name="Update" type="xsd:string"/>
	</message>
	<message name="UpdateInventoryBulkResponse">
		<part name="return" type="tns:ArrayOfString"/>
	</message>
	<portType name="PinnacleApiPortType">
		<operation name="GetVersion">
			<documentation>Get the version number</documentation>
			<input message="tns:GetVersionRequest"/>
			<output message="tns:GetVersionResponse"/>
		</operation>
		<operation name="GetOrderCount">
			<documentation>Get the total number of order that match the passed criteria</documentation>
			<input message="tns:GetOrderCountRequest"/>
			<output message="tns:GetOrderCountResponse"/>
		</operation>
		<operation name="GetOrders">
			<documentation>Get all of your orders based on the passed criteria</documentation>
			<input message="tns:GetOrdersRequest"/>
			<output message="tns:GetOrdersResponse"/>
		</operation>
		<operation name="GetCustomerCount">
			<documentation>Get the total number of customers</documentation>
			<input message="tns:GetCustomerCountRequest"/>
			<output message="tns:GetCustomerCountResponse"/>
		</operation>
		<operation name="GetCustomers">
			<documentation>Get all of your customers</documentation>
			<input message="tns:GetCustomersRequest"/>
			<output message="tns:GetCustomersResponse"/>
		</operation>
		<operation name="GetProductCount">
			<documentation>Get the total number of products</documentation>
			<input message="tns:GetProductCountRequest"/>
			<output message="tns:GetProductCountResponse"/>
		</operation>
		<operation name="GetProducts">
			<documentation>Get your products based on the passed criteria.  The &lt;a href=&quot;https://www.YourSiteName.com/content/admin/plugins/openapi/docs/index.php#GetProducts&quot;&gt;ExtraArgs&lt;/a&gt; parameter provides a mechanism for passing a wide assortment of criteria for getting only the products you wish</documentation>
			<input message="tns:GetProductsRequest"/>
			<output message="tns:GetProductsResponse"/>
		</operation>
		<operation name="GetCategories">
			<documentation>Get all of the product categories</documentation>
			<input message="tns:GetCategoriesRequest"/>
			<output message="tns:GetCategoriesResponse"/>
		</operation>
		<operation name="GetInventory">
			<documentation>Get your store inventory</documentation>
			<input message="tns:GetInventoryRequest"/>
			<output message="tns:GetInventoryResponse"/>
		</operation>
		<operation name="UpdateInventory">
			<documentation>Update single item inventory</documentation>
			<input message="tns:UpdateInventoryRequest"/>
			<output message="tns:UpdateInventoryResponse"/>
		</operation>
		<operation name="UpdateInventoryBulk">
			<documentation>Bulk update inventory</documentation>
			<input message="tns:UpdateInventoryBulkRequest"/>
			<output message="tns:UpdateInventoryBulkResponse"/>
		</operation>
	</portType>
	<binding name="PinnacleApiBinding" type="tns:PinnacleApiPortType">
		<soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
		<operation name="GetVersion">
			<soap:operation soapAction="PinnacleApi#GetVersion" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetOrderCount">
			<soap:operation soapAction="PinnacleApi#GetOrderCount" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetOrders">
			<soap:operation soapAction="PinnacleApi#GetOrders" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetCustomerCount">
			<soap:operation soapAction="PinnacleApi#GetCustomerCount" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetCustomers">
			<soap:operation soapAction="PinnacleApi#GetCustomers" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetProductCount">
			<soap:operation soapAction="PinnacleApi#GetProductCount" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetProducts">
			<soap:operation soapAction="PinnacleApi#GetProducts" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetCategories">
			<soap:operation soapAction="PinnacleApi#GetCategories" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="GetInventory">
			<soap:operation soapAction="PinnacleApi#GetInventory" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="UpdateInventory">
			<soap:operation soapAction="PinnacleApi#UpdateInventory" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
		<operation name="UpdateInventoryBulk">
			<soap:operation soapAction="PinnacleApi#UpdateInventoryBulk" style="rpc"/>
			<input><soap:body use="literal" namespace="PinnacleApi"/></input>
			<output><soap:body use="literal" namespace="PinnacleApi"/></output>
		</operation>
	</binding>
	<service name="PinnacleApi">
		<port name="PinnacleApiPort" binding="tns:PinnacleApiBinding">
			<soap:address location="'.str_replace('&', '&amp;', self::getEndpointUrl($ApiUsername, $ApiPassword, $ApiToken)).'"/>
		</port>
	</service>
</definitions>';
	}
}

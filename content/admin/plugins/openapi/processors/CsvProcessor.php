<?php

require_once(PLUGIN_PATH . "processors/TextProcessor.php");

/**
 * Class CsvProcessor
 */
class CsvProcessor extends TextProcessor
{
    public static function Get_FileExtension()
    {
        return "csv";
    }
    public static function Get_FileMimeType()
    {
        return "text/csv";
    }
}
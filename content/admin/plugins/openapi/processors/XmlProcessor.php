<?php

require_once PLUGIN_PATH . 'processors/ProcessorBase.php';

/**
 * Class XmlProcessor
 */
class XmlProcessor extends ProcessorBase
{
	/**
	 * @param $httpRaw
	 * @param $httpPost
	 * @param $httpGet
	 * @param $httpFile
	 * @param $httpRequest
	 * @param $template
	 * @return mixed|string|void
	 */
	public static function Process($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest, $template)
	{
		global $smarty;
		
		$output = parent::Process($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);

		$smarty->assign("XmlData", $output);
		
		$call = isset($httpRequest["call"]) ? $httpRequest["call"] : null;
		
		$smarty->assign("CdataFields", ApiCalls::getCdataFields($call));

		return $template ? $smarty->fetch("$template.tpl") : $smarty->fetch("xml.tpl");
	}

	/**
	 * @param $httpRaw
	 * @param $httpPost
	 * @param $httpGet
	 * @param $httpFile
	 * @param $httpRequest
	 * @param $template
	 * @return mixed|string|void
	 */
	public static function ProcessNotAuthenticated($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		global $smarty;
		$output = parent::ProcessNotAuthenticated($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);
		$smarty->assign("XmlData", $output);
		return $smarty->fetch("xml.tpl");
	}

	/**
	 * @return string
	 */
	public static function Get_FileExtension()
	{
		return "xml";
	}

	/**
	 * @return string
	 */
	public static function Get_FileMimeType()
	{
		return "text/xml";
	}
}
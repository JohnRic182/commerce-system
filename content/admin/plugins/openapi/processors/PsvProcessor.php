<?php

require_once(PLUGIN_PATH . "processors/TextProcessor.php");

/**
 * Processor for Pipe separated values
 */
class PsvProcessor extends TextProcessor
{
    public static function Get_FileExtension()
    {
        return "txt";
    }

    public static function Get_FileMimeType()
    {
        return "text/plain";
    }
}
<?php

require_once PLUGIN_PATH . "processors/TextProcessor.php";

/**
 * Class TsvProcessor
 */
class TsvProcessor extends TextProcessor
{
    /**
     * @return string
     */
    public static function Get_FileExtension()
    {
        return "txt";
    }

    /**
     * @return string
     */
    public static function Get_FileMimeType()
    {
        return "text/tab-separated-values";
    }
}
<?php

require_once PLUGIN_PATH . 'processors/ProcessorBase.php';

/**
 * Class TextProcessor
 */
class TextProcessor extends ProcessorBase
{
    /**
     * @param $httpRaw
     * @param $httpPost
     * @param $httpGet
     * @param $httpFile
     * @param $httpRequest
     * @param $template
     * @return mixed|string|void
     */
    public static function Process($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
    {
        global $smarty;
        $output = parent::Process($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);
        $smarty->assign("CsvData",$output);
        return $template ? $smarty->fetch("$template.tpl") : $smarty->fetch("csv.tpl");
    }

    /**
     * @param $httpRaw
     * @param $httpPost
     * @param $httpGet
     * @param $httpFile
     * @param $httpRequest
     * @param $template
     * @return mixed|string|void
     */
    public static function ProcessNotAuthenticated($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
    {
        global $smarty;
        $output = parent::ProcessNotAuthenticated($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);
        $smarty->assign("CsvData",$output);
        return $smarty->fetch("csv.tpl");
    }
}
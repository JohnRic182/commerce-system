<?php

/**
 * Interface IProcessor
 */
interface IProcessor
{
    /**
     * @param $httpRaw
     * @param $httpPost
     * @param $httpGet
     * @param $httpFile
     * @param $httpRequest
     * @param $template
     * @return mixed
     */
    static function Process($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest, $template);

    /**
     * @param $httpRaw
     * @param $httpPost
     * @param $httpGet
     * @param $httpFile
     * @param $httpRequest
     * @param $template
     * @return mixed
     */
    static function ProcessNotAuthenticated($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest, $template);

    /**
     * @return mixed
     */
    static function Get_FileExtension();

    /**
     * @return mixed
     */
    static function Get_FileMimeType();
}

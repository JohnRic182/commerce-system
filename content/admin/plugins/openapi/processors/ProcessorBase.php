<?php

require_once(PLUGIN_PATH . "processors/IProcessor.php");

/**
 * Class ProcessorBase
 */
abstract class ProcessorBase implements IProcessor
{
	/**
	 * @param $httpRaw
	 * @param $httpPost
	 * @param $httpGet
	 * @param $httpFile
	 * @param $httpRequest
	 * @param $template
	 * @return mixed|string
	 */
	static function Process($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		try
		{
			$call = isset($httpRequest["call"])?$httpRequest["call"]:null;
			if ($call)
			{
				if (method_exists("ApiCalls",$call))
				{
					$apiOutput = call_user_func(array("ApiCalls",$call), $_REQUEST);
				}
				else
				{
					throw new InvalidArgumentException("Invalid call");
				}
			}
			else
			{
				throw new InvalidArgumentException("Missing argument: call");
			}
		}
		catch (Exception $e)
		{
			$apiOutput = "error: " . $e->getMessage();
		}

		return $apiOutput;
	}

	/**
	 * @param $httpRaw
	 * @param $httpPost
	 * @param $httpGet
	 * @param $httpFile
	 * @param $httpRequest
	 * @param $template
	 * @return string
	 */
	static function ProcessNotAuthenticated($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		return "Plugin is not active or authentication failed";
	}

	/**
	 * @return string
	 */
	static function Get_FileExtension()
	{
		return "api";
	}

	/**
	 * @return string
	 */
	static function Get_FileMimeType()
	{
		return "text/plain";
	}
}
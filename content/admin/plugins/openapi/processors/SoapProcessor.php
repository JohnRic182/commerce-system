<?php

require_once PLUGIN_PATH . 'processors/ProcessorBase.php';
require_once PLUGIN_PATH . 'processors/PinnacleApiWsdl.php';

/**
 * Class SoapProcessor
 */
class SoapProcessor extends ProcessorBase
{
	/**
	 * @param $httpRaw
	 * @param $httpPost
	 * @param $httpGet
	 * @param $httpFile
	 * @param $httpRequest
	 * @param $template
	 * @return string
	 */
	static function Process($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		ApiCalls::$callType = 'soap';

		if (isset($httpGet['wsdl']) && $httpGet['wsdl'] == 1)
		{
			//TODO: Content-Type, headers
			header('Content-Type: text/xml');
			echo PinnacleApiWsdl::generate($httpGet['username'], $httpGet['password'], $httpGet['token']);
			die();
		}

		$request = isSet($httpRaw) ? $httpRaw : '';
		$server = new SoapServer(PinnacleApiWsdl::getEndpointUrl($httpGet['username'], $httpGet['password'], $httpGet['token']).'&wsdl=1', array('cache_wsdl' =>  WSDL_CACHE_NONE));
		$server->setClass('ApiCalls');

		ob_start();
		$server->handle($request);
		$output = ob_get_contents();
		ob_end_clean();

		return $output;
	}

	/**
	 * @param $httpRaw
	 * @param $httpPost
	 * @param $httpGet
	 * @param $httpFile
	 * @param $httpRequest
	 * @param $template
	 */
	static function ProcessNotAuthenticated($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		$output = parent::ProcessNotAuthenticated($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);
		$request = isSet($httpRaw) ? $httpRaw : '';
		$server = new SoapServer(PinnacleApiWsdl::getEndpointUrl($httpGet['username'], $httpGet['password'], $httpGet['token']).'&wsdl=1', array('cache_wsdl' =>  WSDL_CACHE_NONE));

		$server->fault(0, $output);
	}
}

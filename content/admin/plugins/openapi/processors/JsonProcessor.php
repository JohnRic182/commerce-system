<?php

require_once(PLUGIN_PATH . "processors/ProcessorBase.php");

/**
 * Class JsonProcessor
 */
class JsonProcessor extends ProcessorBase
{
	static function Process($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		global $smarty;
		$output = parent::Process($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);

		$smarty->assign("ApiData",$output);
		if ($template)
		{
			$xml = $smarty->fetch("$template.tpl");
		}
		else
		{
			if (!is_object($output) && !is_array($output))
			{
				$json = json_encode($output);
			}
			else
			{
				$json_array = array();
				foreach ($output as $nodeName => $node)
				{
					if (is_object($node))
					{
						$json_array[] = array($node->GetItemType() => $node);
					}
					elseif (is_array($node))
					{
						$json_array[] = array($nodeName => $node);
					}
				}
				$json = json_encode($json_array);
			}
			
			return $json;
		}
		
		return $xml;
	}

	static function ProcessNotAuthenticated($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		global $smarty;
		$output = parent::ProcessNotAuthenticated($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);
		$xml = "";
		$smarty->assign("ApiData",$output);
		$xml = $smarty->fetch("json.tpl");
		return $xml;
	}

	public static function Get_FileExtension()
	{
		return "json";
	}

	public static function Get_FileMimeType()
	{
		return "application/json";
	}
}
<?php

require_once PLUGIN_PATH . 'processors/ProcessorBase.php';

/**
 * Class ZipProcessor
 */
class ZipProcessor extends ProcessorBase
{
    /**
     * @param $httpRaw
     * @param $httpPost
     * @param $httpGet
     * @param $httpFile
     * @param $httpRequest
     * @param $template
     * @return null
     */
    public static function ProcessNotAuthenticated($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
    {
        return null;
    }

    /**
     * @return string
     */
    public static function Get_FileExtension()
    {
        return "zip";
    }

    /**
     * @return string
     */
    public static function Get_FileMimeType()
    {
        return "application/zip";
    }
}
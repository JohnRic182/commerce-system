<?php

require_once(PLUGIN_PATH . "processors/ProcessorBase.php");

/**
 * Class RssProcessor
 */
class RssProcessor extends ProcessorBase
{
	static function Process($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		global $smarty, $settings;
		$call = isset($httpRequest["call"])?$httpRequest["call"]:null;
		if ($call)
		{
			switch ($call)
			{
				case "GetProducts" :
				{
					$output = parent::Process($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest, $template);
					$rss = "";
					$smarty->assign("ApiData", $output);
					$smarty->assign("ItemTitleField","Title");
					$smarty->assign("ItemLinkField","URL");
					$smarty->assign("ItemImageField","ThumbnailImageUrl");
					$smarty->assign("PubDateField","Added");
					$calledUrl = (($_SERVER["HTTPS"]=="on")?"https://":"http://"). $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					$smarty->assign(
						"Channel",
						array(
							"Title"=>"Products",
							"Description"=>"Products",
							"Url"=>$calledUrl,
							"ImageUrl"=>$settings['GlobalHttpUrl']."/images/admin/logo.png",
							"LastBuildDate"=>time(),
							"LastPubDate"=>time()
						)
					);
					if ($template)
					{
						$rss = $smarty->fetch("{$template}.tpl");
					}
					else
					{
						$rss = $smarty->fetch("rss.tpl");
					}
					break;
				}
				default :
				{
					$rss = "Invalid Call";
				}
			}
		}
		return $rss;
	}

	static function ProcessNotAuthenticated($httpRaw,$httpPost,$httpGet,$httpFile,$httpRequest,$template)
	{
		global $smarty;
		$output = parent::ProcessNotAuthenticated($httpRaw, $httpPost, $httpGet, $httpFile, $httpRequest,$template);

		$xml = "";
		$smarty->assign("ApiData",$output);
		$xml = $smarty->fetch("rss.tpl");
		return $xml;
	}

	public static function Get_FileExtension()
	{
		return "rss";
	}
	
	public static function Get_FileMimeType()
	{
		return "application/rss+xml";
	}
}
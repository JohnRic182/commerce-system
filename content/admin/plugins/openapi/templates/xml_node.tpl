{if is_array($XmlData) || is_object($XmlData)}
{assign var="XmlArray" value=$XmlData}
	{foreach from=$XmlArray item="XmlItem" key="XmlKey"}
		{if is_object($XmlItem)}
			{assign var="NodeName" value=$XmlItem->GetItemType()}
		{else}
			{assign var="NodeName" value=$XmlKey}
		{/if}
		{assign var="XmlData" value=$XmlItem}
		{if in_array(strtolower($NodeName), $CdataFields)}
			<{$NodeName}><![CDATA[{include file="xml_node.tpl"}]]></{$NodeName}>
		{else}
			<{$NodeName}>{include file="xml_node.tpl"}</{$NodeName}>
		{/if}
	{/foreach}
{else}{$XmlData|htmlspecialchars}{/if}
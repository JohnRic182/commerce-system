{if is_array($CsvData) || is_object($CsvData)}
path,code,price,name,caption
{foreach name="rowLoop" from=$CsvData item="CsvRow" key="CsvRowKey"}
"{$CsvRow->YahooPath}","{$CsvRow->ProductId}","{$CsvRow->Price}","{$CsvRow->Title}","{$CsvRow->Description|regex_replace:"/[\r\t\n]/":""|strip_tags:false|escape}"
{/foreach}
{/if}
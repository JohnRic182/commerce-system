<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
<title>{$Channel.Title}</title>
<description>{$Channel.Description}</description>
<link>{$Channel.Url|escape}</link>
<lastBuildDate>{$Channel.LastBuildDate|date_format:"%a, %d %b %Y %H:%M:%S %z"}</lastBuildDate>
<pubDate>{$Channel.LastPubDate|date_format:"%a, %d %b %Y %H:%M:%S %z"}</pubDate>
<image>
  <url>{$Channel.ImageUrl}</url>
  <title>{$Channel.Title}</title>
  <link>{$Channel.Url|escape}</link>
</image>
<atom:link href="{$Channel.Url|escape}" rel="self" type="application/rss+xml" />
{if is_array($ApiData) || is_object($ApiData)}
    {foreach from=$ApiData item="ApiItem" key="ApiItemKey"}
        <item>
        <title>{$ApiItem->$ItemTitleField}</title>
        <description>{$ApiItem->$ItemTitleField}</description>
        <link>{$ApiItem->$ItemLinkField}</link>
        <guid isPermaLink="false">{$ApiItem->$ItemLinkField|escape}</guid>
        <pubDate>{$ApiItem->$PubDateField|date_format:"%a, %d %b %Y %H:%M:%S %z"}</pubDate>
        </item>
    {/foreach}
{/if}
</channel>
</rss>
<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0"
xmlns:g="http://base.google.com/ns/1.0"
xmlns:c="http://base.google.com/cns/1.0">
<channel>
<title>{$Channel.Title}</title>
<description>{$Channel.Description}</description>
<link>{$Channel.Url|escape}</link>
<image>
  <url>{$Channel.ImageUrl}</url>
  <title>{$Channel.Title}</title>
  <link>{$Channel.Url|escape}</link>
</image>
{if is_array($ApiData) || is_object($ApiData)}
    {foreach from=$ApiData item="ApiItem" key="ApiItemKey"}
        <item>
        <title><![CDATA[{$ApiItem->Title|trim|htmlspecialchars}]]></title>
        <description><![CDATA[{$ApiItem->Description|trim|htmlspecialchars}]]></description>
        <link>{$ApiItem->URL}</link>
        <g:image_link>{$ApiItem->ImageUrl}</g:image_link>
        <g:id>{$ApiItem->ProductId}</g:id>
        <g:upc>{$ApiItem->UPC}</g:upc>
        <g:price>{$ApiItem->Price}</g:price>
        <g:quantity>{$ApiItem->Qoh}</g:quantity>
        <g:condition>{$ApiItem->GoogleItemCondition}</g:condition>
		<g:google_product_category>{$ApiItem->GoogleProductType|htmlspecialchars}</g:google_product_category>
        </item>
    {/foreach}
{/if}
</channel>
</rss>
{if is_array($CsvData) || is_object($CsvData)}
Product Name,Manufacturer,Manufacturer Part Number (MPN),UPC,Retsku,Categorization,Detailed Description,Selling Price,Availability,Product URL,Image URL,Product Condition,Weight
{foreach name="rowLoop" from=$CsvData item="CsvRow" key="CsvRowKey"}
{$CsvRow->Title},{$CsvRow->ManufaturerName},{$CsvRow->PricegrabberPartNumber},{$CsvRow->UPC},{$CsvRow->Sku},"{$CsvRow->PricegrabberCategory}","{$CsvRow->Description|regex_replace:"/[\r\t\n]/":""|strip_tags:false|escape}",{$CsvRow->Price},{if $CsvRow->Qoh > 0}yes{else}no{/if},{$CsvRow->URL},{$CsvRow->ImageUrl},{$CsvRow->PricegrabberItemCondition},{$CsvRow->Weight}
{/foreach}
{/if}
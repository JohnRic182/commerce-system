{if is_array($CsvData) || is_object($CsvData)}
Product Name,Manufacturer,Manufacturer Part Number (MPN),UPC,Category,Description,Price,Stock Status,Click-out URL,Image URL,Product Condition,Weight
{foreach name="rowLoop" from=$CsvData item="CsvRow" key="CsvRowKey"}
{$CsvRow->Title},{$CsvRow->ManufacturerName},{$CsvRow->NextagPartNumber},{$CsvRow->UPC},{$CsvRow->NextagCategory},"{$CsvRow->Description|regex_replace:"/[\r\t\n]/":""|strip_tags:false|escape}",{$CsvRow->Price},{if $CsvRow->InventoryControl=="yes"}{if $CsvRow->Qoh > 0}In Stock{else}Out of Stock{/if}{/if},{$CsvRow->URL},{$CsvRow->ImageUrl},{$CsvRow->NextagItemCondition},{$CsvRow->Weight}
{/foreach}
{/if}
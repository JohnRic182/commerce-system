<?php

	set_time_limit(300000);

	define('ENV', true);

	if (!defined('__DIR__')) define('__DIR__', dirname(__FILE__));

	define('BASE_PATH', dirname(dirname(dirname(dirname(__DIR__)))) . '/');
	define('PLUGIN_PATH', __DIR__ . '/');
	chdir(BASE_PATH);

	require_once 'content/classes/_boot.php';

	require_once("content/vendors/smarty/Smarty.class.php");
	require_once("content/vendors/nusoap/nusoap.php");

	$smarty = new Smarty();
	$smarty->template_dir = PLUGIN_PATH . '/templates';
	$smarty->compile_dir = BASE_PATH . 'content/cache/tmp';

	if (!is_dir(BASE_PATH . 'content/cache/tmp'))
	{
		mkdir(BASE_PATH . 'content/cache/tmp', FileUtils::getDirectoryPermissionMode());
	}

	session_cache_limiter("nocache");

	$username = isset($_REQUEST["username"])?trim($_REQUEST["username"]):"";
	$password = isset($_REQUEST["password"])?trim($_REQUEST["password"]):"";
	$token = isset($_REQUEST["token"])?trim($_REQUEST["token"]):"";

	// authorization should always be false
	$plugin_auth = false;

	$plugin_active = $settings["plugin_openapi_active"];
	$plugin_username = $settings["plugin_openapi_username"];
	$plugin_password = $settings["plugin_openapi_password"];
	$plugin_token = $settings["plugin_openapi_token"];
	$global_http_url = $settings["GlobalHttpUrl"];

	// check credentials
	if ($plugin_username == $username && checkPasswordHash($password, $plugin_password) && $plugin_token == $token)
	{
		$plugin_auth = true;
	}

	// ensure that the call is created over HTTPS connection and is not currently in DEVMODE
	if ($plugin_auth)
	{
		if ($_SERVER["SERVER_PORT"] != 443 && (!defined("DEVMODE") || !DEVMODE))
		{
			$plugin_auth = false;
		}
	}

	/**
	 * @var $processors array
	 * This holds a list of all available API processors
	 */
	$processors = array(
		"soap"=>"SoapProcessor", "json"=>"JsonProcessor", "xml"=>"XmlProcessor", "csv"=>"CsvProcessor", "tsv"=>"TsvProcessor",
		"psv"=>"PsvProcessor", "zip"=>"ZipProcessor", "rss"=>"RssProcessor");

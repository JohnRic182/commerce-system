$(document).ready(function(){
	$("#button-token").click(function(){
		var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		var token = "";
		var length = 64;
		for (x=0; x<length; x++) token += chars.charAt(Math.floor(Math.random() * 62));
		$("#plugin_openapi_token").val(token);
	});
});
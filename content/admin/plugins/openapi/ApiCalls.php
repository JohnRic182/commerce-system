<?php
/**
 * Static class for processing api requests.
 * The methods of this class will return the raw response; it is up to the individual api processor to format the response to their liking
 */
class ApiCalls
{
	public static $callType;

	/**
	 * @static $CallRequiresAuth
	 * A list that explicitly marks an api call as requiring authentication or not.
	 * Any call that is not in this list, will require authentication.
	 *
	 * @var array
	 */
	public static $CallRequiresAuth = array("GetProducts"=>false,"GetProductImagesZip"=>false);

	/**
	 * @static Return the currently running version of cart software version
	 * @param array $args
	 * @return string
	 */
	static function GetVersion($args)
	{
		global $settings;
		return $settings["AppVer"];
	}

	/**
	 * Return the number of orders in the system
	 * @param array $args
	 * @return integer
	 */
	static function GetOrderCount($args)
	{
		global $db;

		if (self::$callType == 'soap')
		{
			$funcArgs = func_get_args();
			$args = array(
				'Status' => $funcArgs[0],
				'CustomerId' => $funcArgs[1],
				'LastOrderId' => $funcArgs[2],
				'LastDate' => $funcArgs[3],
			);
		}

		$lastOrder = isset($args["LastOrder"])?$args["LastOrder"]:0;
		$lastDate =  isset($args["LastDate"])?$args["LastDate"]:"1/1/1969";
		$status =  isset($args["Status"])?$args["Status"]:null;
		$customerid =  isset($args["CustomerId"])?$args["CustomerId"]:null;

		if ($lastOrder < 1) $lastOrder = 0;
		if (trim($lastDate) == '') $lastDate = '1/1/1969';
		if (trim($status) == '') $status = null;
		if ($customerid < 1) $customerid = null;

		$sql = "SELECT COUNT(*) AS ordercount FROM ".DB_PREFIX."orders ";
		if ($status)
		{
			$status = $db->escape($status);
			$sql .= " WHERE status = '$status'";
		} else {
			$sql .= " WHERE status <> 'Abandon' AND status <> 'New' ";
		}

		if ($customerid)
		{
			$customerid = $db->escape($customerid);
			$sql .= " and orders.uid = '$customerid'";
		}

		if (is_numeric($lastOrder)) $sql.=" AND order_num >= ".intval($lastOrder);

		if ($lastDate != "all")
		{
			try
			{
				$lastDate = $db->escape($lastDate);
				$lastDateTimeStamp = strtotime($lastDate);
				$lastDate = date("Y-m-d H:i:s", $lastDateTimeStamp);
				$sql.=" AND placed_date>='". $lastDate . "'";
			}
			catch (Exception $e)
			{
				//invalid date string passed
			}
		}
		$db->query($sql);
		$result = $db->moveNext();
		return $result["ordercount"];
	}

	/**
	 * return an array of ApiOrder objects, one for each order in the system
	 * @global DB $db
	 * @param array $args available arguments are: LastOrder, LastDate, StartNumber, BatchSize, Status, CustomerId
	 * @return array an array of ApiOrder objects
	 */
	static function GetOrders($args)
	{
		global $db;

		if (self::$callType == 'soap')
		{
			$funcArgs = func_get_args();
			$args = array(
				'Status' => $funcArgs[0],
				'CustomerId' => $funcArgs[1],
				'LastOrder' => $funcArgs[2],
				'LastDate' => $funcArgs[3],
				'StartNumber' => $funcArgs[4],
				'BatchSize' => $funcArgs[5],
			);
		}
		require_once(PLUGIN_PATH . "lib/ApiOrder.php");
		$lastOrder = isset($args["LastOrder"])?$args["LastOrder"]:0;
		$lastDate =  isset($args["LastDate"])?$args["LastDate"]:"1/1/1969";
		$startNum =  isset($args["StartNumber"])?$args["StartNumber"]:0;
		$batchSize =  isset($args["BatchSize"])?$args["BatchSize"]:1000;
		$status =  isset($args["Status"])?$args["Status"]:null;
		$customerid =  isset($args["CustomerId"])?$args["CustomerId"]:null;

		if ($lastOrder < 1) $lastOrder = 0;
		if (trim($lastDate) == '') $lastDate = '1/1/1969';
		if ($startNum < 0) $startNum = 0;
		if ($batchSize < 1) $batchSize = 1000;
		if (trim($status) == '') $status = null;
		if ($customerid < 1) $customerid = null;

		$sql =
			"SELECT ".DB_PREFIX."orders.*, ".DB_PREFIX."users.*, ".
			"IF(".DB_PREFIX."users.country=1 OR ".DB_PREFIX."users.country=2, bstate.short_name, ".DB_PREFIX."users.province) AS billing_state_name, bcountry.iso_a2 AS billing_country_name, ".
			"IF(".DB_PREFIX."orders.shipping_country=1 OR ".DB_PREFIX."orders.shipping_country=2, sstate.short_name, ".DB_PREFIX."orders.shipping_province) AS shipping_state_name, scountry.iso_a2 AS shipping_country_name ".
			"FROM ".DB_PREFIX."orders ".
			"INNER JOIN ".DB_PREFIX."users ON ".DB_PREFIX."orders.uid=".DB_PREFIX."users.uid AND ".DB_PREFIX."users.removed='No' ".
			"LEFT JOIN ".DB_PREFIX."states AS bstate ON bstate.stid=".DB_PREFIX."users.state ".
			"INNER JOIN ".DB_PREFIX."countries AS bcountry ON bcountry.coid=".DB_PREFIX."users.country ".
			"LEFT JOIN ".DB_PREFIX."states AS sstate ON sstate.stid=".DB_PREFIX."orders.shipping_state ".
			"LEFT JOIN ".DB_PREFIX."countries AS scountry ON scountry.coid=".DB_PREFIX."orders.shipping_country ";
		if ($status)
		{
			$status = $db->escape($status);
			$sql .= " WHERE status = '$status'";
		} else {
			$sql .= " WHERE status <> 'Abandon' AND status <> 'New' ";
		}
		if ($customerid)
		{
			$customerid = $db->escape($customerid);
			$sql .= " and orders.uid = '$customerid'";
		}
		if (is_numeric($lastOrder)) $sql.=" AND order_num > ".intval($lastOrder);
		if ($lastDate != "all")
		{
				$_date = explode("/", $lastDate);
				if (count($_date) == 3 && is_numeric($_date[0]) && is_numeric($_date[1])&& is_numeric($_date[2]))
				{
						$sql.=" AND placed_date >='".$_date[2]."-".$_date[0]."-".$_date[1]." 00:00:01'";
				}
		}
		if (is_numeric($startNum)) $sql.=" AND order_num > ".$startNum;
		if (is_numeric($batchSize)) $sql.=" LIMIT ".$batchSize;
		$db->query($sql);
		$orderRecords = $db->getRecords();
		$returnArray = array();
		foreach ($orderRecords as $orderRecord)
		{
			$order=ApiOrder::LoadFromDataRecord($orderRecord);
			array_push($returnArray, $order);
		}
		return $returnArray;
	}

	/**
	 * Return the number of customers in the system
	 * @param array $args This call does not do anything with the $args
	 * @return integer
	 */
	static function GetCustomerCount($args)
	{
		global $db;
		$sql = "SELECT COUNT(*) AS usercount FROM ".DB_PREFIX."users WHERE removed='No'";
		$db->query($sql);
		$result = $db->moveNext();
		return $result["usercount"];
	}

	/**
	 * return an array of ApiCustomer objects, one for each customer in the system
	 * @global DB $db
	 * @param array $args available arguments are: StartNumber, BatchSize
	 * @return array an array of ApiCustomer objects
	 */
	static function GetCustomers($args)
	{
		global $db;

		if (self::$callType == 'soap')
		{
			$funcArgs = func_get_args();
			$args = array(
				'StartNumber' => $funcArgs[0],
				'BatchSize' => $funcArgs[1],
			);
		}

		require_once(PLUGIN_PATH . "lib/ApiCustomer.php");
		$startNum =  isset($args["StartNumber"])?$args["StartNumber"]:0;
		$batchSize =  isset($args["BatchSize"])?$args["BatchSize"]:1000;

		if ($startNum < 0) $startNum = 0;
		if ($batchSize < 1) $batchSize = 1000;

		$sql =
				"SELECT ".DB_PREFIX."users.*, ".
				"IF(".DB_PREFIX."users.country=1 OR ".DB_PREFIX."users.country=2, bstate.short_name, ".DB_PREFIX."users.province) AS billing_state_name, bcountry.iso_a2 AS billing_country_name ".
				"FROM ".DB_PREFIX."users ".
				"LEFT JOIN ".DB_PREFIX."states AS bstate ON bstate.stid=".DB_PREFIX."users.state ".
				"LEFT JOIN ".DB_PREFIX."countries AS bcountry ON bcountry.coid=".DB_PREFIX."users.country ".
				"WHERE ".DB_PREFIX."users.uid>0 AND ".DB_PREFIX."users.removed='No'";

		if (is_numeric($startNum)) $sql.=" AND ".DB_PREFIX."users.uid >= ".intval($startNum);
		$sql.=" GROUP BY ".DB_PREFIX."users.uid ORDER BY uid ";
		if (is_numeric($batchSize)) $sql.=" LIMIT ".intval($batchSize);
		$db->query($sql);
		$customerRecords = $db->getRecords();
		$returnArray = array();
		foreach ($customerRecords as $customerRecord)
		{
			$customer=ApiCustomer::LoadFromDataRecord($customerRecord);
			array_push($returnArray, $customer);
		}
		return $returnArray;
	}

	static function GetCategories($args)
	{
		global $db;
		require_once(PLUGIN_PATH . "lib/ApiProductCategory.php");
		return ApiProductCategory::LoadByParentId(0);
	}

	static function GetProductGroups($args)
	{
//		global $db;
//		require_once(PLUGIN_PATH . "lib/ApiProductGroup.php");
//		return ApiProductGroup::GetProductGroups();
		$returnArray = array();
	}

	/**
	 * Return the number of products in the system
	 * @param array $args This call does not do anything with the $args
	 * @return integer
	 */
	static function GetProductCount($args)
	{
		global $db;
		$sql = "SELECT COUNT(*) AS productscount FROM ".DB_PREFIX."products";
		$db->query($sql);
		$result = $db->moveNext();
		return $result["productscount"];
	}

	/**
	 * return an array of ApiProduct objects, one for each product in the system that matches the filters set in $args
	 * @global DB $db
	 * @param array $args available arguments are: BatchPage, BatchSize, PrimaryCategory, ProductGroup, ExtraArgs
	 * @return array an array of ApiCustomer objects
	 */
	static function GetProducts($args)
	{
		global $db;
		global $settings;

		if (self::$callType == 'soap')
		{
			$funcArgs = func_get_args();
			$args = array(
				'StartNumber' => $funcArgs[0],
				'BatchSize' => $funcArgs[1],
				'BatchPage' => $funcArgs[2],
				'PrimaryCategory' => $funcArgs[3],
				'ExtraArgs' => $funcArgs[4],
				'ProductGroup' => $funcArgs[5],
				'IncludeVatTaxes' => $funcArgs[6],
			);
		}

		$o = new ShoppingCartProducts($db,$settings);

		$itemsPerPage =  isset($args['BatchSize']) ? $args['BatchSize'] : 1000;
		$currentPage = isset($args['BatchPage']) ? $args['BatchPage'] : 1;
		$primaryCategory = isset($args['PrimaryCategory']) ? $args['PrimaryCategory'] : null;
		$includeVatTaxes = isset($args['IncludeVatTaxes']) ? trim(strtolower($args['IncludeVatTaxes'])) == 'true' : false;
		$includeStealth =  isset($args['includeStealth']) ? $args['includeStealth'] : 0;

		if ($itemsPerPage < 1) $itemsPerPage = 1000;
		if ($currentPage < 1) $currentPage = 1;
		if ($primaryCategory < 1) $primaryCategory = null;

		if ($includeVatTaxes)
		{
			$taxProvider = Tax_ProviderFactory::getTaxProvider();
			$taxProvider->getTaxRates($settings['TaxDefaultCountry'], $settings['TaxDefaultState'], 0);
		}

		// these are the default filters to filter by
		$filters_default = array(
			'mode' => 'catalog',
			'categories' => ($primaryCategory != -1 && !is_null($primaryCategory)) ? array($primaryCategory) : array(),
			'ProductGroup' => null, //Product Group functionality has been removed
			'nested' => isset($args['IncludeNested']) ? trim(strtolower($args['IncludeNested'])) == 'true' : true,
			'CatalogSortBy' => 'title',
			'Page' => $currentPage,
			'CatalogItemsOnPage' => $itemsPerPage,
			'includeStealth' => $includeStealth
		);

		// create an array of extended filters if they were passed
		$filters_extended = array();

		if (isset($args['ExtraArgs']))
		{
			$args = $args['ExtraArgs'];
		}

		foreach ($args as $argKey => $argValue)
		{
			if (strstr($argValue, ","))
			{
				$filters_extended[$argKey] = explode(",", $argValue);
			}
			else
			{
				$filters_extended[$argKey] = $argValue;
			}
		}

		// merge the filter arrays together and get products
		$filters = array_merge($filters_default, $filters_extended);
		$productRecords = $o->getItemsCatalog($filters, 1);

		require_once(PLUGIN_PATH . 'lib/ApiProduct.php');

		$returnArray = array();

		if ($productRecords)
		{
			foreach ($productRecords as $productRecord)
			{
				$product = ApiProduct::LoadFromDataRecord($productRecord);
				array_push($returnArray, $product);
			}
		}

		return $returnArray;
	}

	/**
	 * @static returns zip compressed binary data representing the product images
	 * @global array $settings
	 * @param array $args
	 * @return string
	 */
	static function GetProductImagesZip($args)
	{
		global $settings;
		$zipFileName = $settings["GlobalServerPath"].DS."content".DS."cache".DS."tmp".DS."apiGetProductImages.zip";
		$tempFolder = $settings["GlobalServerPath"].DS."content".DS."cache".DS."tmp".DS."apiZipBuild";
		if (file_exists($tempFolder))
		{
			unlink($tempFolder);
		}
		if (file_exists($zipFileName))
		{
			unlink($zipFileName);
		}

		mkdir($tempFolder);
		$products = self::GetProducts($args);
		foreach ($products as $product)
		{
			if ($product->ImageUrl)
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $product->ImageUrl);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$imgData = curl_exec($ch);
				curl_close($ch);
				$filename = $product->PID;
				$localImgTempPath = "$tempFolder/$filename";
				file_put_contents($localImgTempPath, $imgData);
				$imageinfo = getimagesize($localImgTempPath);
				$mimeType = $imageinfo["mime"];
				if (strstr($mimeType, "jpg") || strstr($mimeType, "jpeg"))
				{
					rename($localImgTempPath, $localImgTempPath.".jpg");
				}
				elseif (strstr($mimeType, "gif"))
				{
					rename($localImgTempPath, $localImgTempPath.".gif");
				}
				elseif (strstr($mimeType, "png"))
				{
					rename($localImgTempPath, $localImgTempPath.".png");
				}
			}
		}

		$a = array();

		if(strpos(strtolower($_SERVER["SERVER_SOFTWARE"]), "win32") or strpos(strtolower("test".$_SERVER["SERVER_SOFTWARE"]), "microsoft") != false) {
				$command_line = $settings["GlobalServerPath"].DS."content".DS."admin".DS."7zip".DS."7z.exe ".
				"a -tzip \"".$zipFileName."\" ".
				"-r \"".$tempFolder.DS."*\"";
				$result = exec($command_line, $a);
		}
		else{
				$command_line =
						"zip -rj \"".$zipFileName."\" \"".$tempFolder."\"";
				$result = exec($command_line, $a);
		}

		$zipContents = file_get_contents($zipFileName);
		return $zipContents;
	}


	/**
	 * Returns your product inventory (stock)
	 * @global DB $db
	 * @param array $args
	 * @return array as array of ApiProductQoh objects
	 */
	static function GetInventory($args)
	{
		global $db;

		if (self::$callType == 'soap')
		{
			$funcArgs = func_get_args();
			$args = array(
				'StartNumber' => $funcArgs[0],
				'BatchSize' => $funcArgs[1],
			);
		}

		require_once(PLUGIN_PATH . "lib/ApiProductQoh.php");
		$startNum =  isset($args["StartNumber"])?$args["StartNumber"]:0;
		$batchSize =  isset($args["BatchSize"])?$args["BatchSize"]:1000;

		if ($startNum < 0) $startNum = 0;
		if ($batchSize < 0) $batchSize = 1000;

		$sql =
				"SELECT t.*
FROM (
SELECT p.pid, p.product_id, '' as product_subid, p.stock
FROM ".DB_PREFIX."products p
WHERE p.inventory_control = 'Yes'".(is_numeric($startNum) && intval($startNum) > 1 ? ' AND p.pid >= '.intval($startNum) : '')."
UNION
SELECT p.pid, p.product_id, inv.product_subid, inv.stock
FROM ".DB_PREFIX."products p
	INNER JOIN ".DB_PREFIX."products_inventory inv ON p.pid = inv.pid AND p.inventory_control IN ('AttrRuleExc', 'AttrRuleInc')
".(is_numeric($startNum) && intval($startNum) > 1 ? 'WHERE p.pid >= '.intval($startNum) : '')."
) t
ORDER BY t.pid";

		if (intval($batchSize) > 0)
		{
				$sql.=" LIMIT ".intval($batchSize);
		}

		$db->query($sql);
		$productRecords = $db->getRecords();
		$returnArray = array();
		foreach ($productRecords as $productRecord)
		{
			$product=ApiProductQoh::LoadFromDataRecord($productRecord);
			array_push($returnArray, $product);
		}
		return $returnArray;
	}

	/**
	 * Update inventory quantities for several products
	 * @global DB $db
	 * @param array $args
	 * @return an array of errors per product
	 */
	static function UpdateInventoryBulk($args)
	{
		global $db;

		$inventoryControl = new Model_InventoryControl();

		if (self::$callType == 'soap')
		{
			$funcArgs = func_get_args();
			$args = array(
				'Update' => $funcArgs[0],
			);
		}

		$returnQuery = array();
		$update = isset($args['Update'])?$args['Update']:'';
		$products = explode('|', $update);
		foreach ($products as $product)
		{
			$productInfo = explode('~', $product);
			$productId = trim($productInfo[0]);
			$newQoh = intval($productInfo[1]);
			$productSubId = isset($productInfo[2]) ? trim($productInfo[2]) : '';

			$db->query('SELECT * FROM '.DB_PREFIX."products WHERE product_id='".$db->escape($productId)."'");

			if (($productRecord = $db->moveNext()) != false)
			{
				$updated = false;
				if ($productRecord['inventory_control'] != 'No')
				{
					$updated = $inventoryControl->updateQuantityOnHand($productRecord['pid'], $productRecord['product_id'], $productSubId, $newQoh, true);
				}

				if ($updated)
				{
					$returnQuery[$productId]='Updated new qoh is '.$newQoh;
				}
				else
				{
					$returnQuery[$productId]='Product is not an inventory item';
				}
			}
			else
			{
				$returnQuery[$productId]='Invalid Product Id';
			}
		}
		return $returnQuery;
	}

	/**
	 * Update inventory quantities for one product
	 * @global DB $db
	 * @param array $args
	 * @return string result string
	 */
	static function UpdateInventory($args)
	{
		global $db;

		$inventoryControl = new Model_InventoryControl();

		if (self::$callType == 'soap')
		{
			$funcArgs = func_get_args();
			$args = array(
				'Update' => $funcArgs[0],
			);
		}

		$update = isset($args['Update'])?$args['Update']:'';

		$productInfo = explode('~', $update);
		$productId = trim($productInfo[0]);
		$newQoh = intval($productInfo[1]);
		$productSubId = isset($productInfo[2]) ? trim($productInfo[2]) : '';

		$db->query('SELECT * FROM '.DB_PREFIX."products WHERE product_id='".$db->escape($productId)."'");

		if (($productRecord = $db->moveNext()) != false)
		{
			$updated = false;
			if ($productRecord['inventory_control'] != 'No')
			{
				$updated = $inventoryControl->updateQuantityOnHand($productRecord['pid'], $productRecord['product_id'], $productSubId, $newQoh, true);
			}

			if ($updated)
			{
				$returnInfo = $productId.' updated new qoh is '.$newQoh;
			}
			else
			{
				$returnInfo = $productId.' is not an inventory item';
			}
		}
		else
		{
			$returnInfo = $productId.' is an invalid Product Id';
		}
		return $returnInfo;
	}

	static function ErrorHandler($errno, $errstr, $errfile, $errline, $errcontext)
	{
		//error_log("There was an error executing an api call: $errstr");
	}

	/**
	 * Returns an associative array of lowercased xml node names that the value should be set as CDATA
	 *
	 * @param string $call 
	 * @return array An array where the key is the ApiCall method name and the value is an array of lowercased node names
	 * @author Sebastian Nievas
	 */
	static function getCdataFields($call = null)
	{
		static $cdataFields = array(
			'getproducts' => array(
				'title', 'optionid', 'caption','description','url', 'thumbnailimageurl', 'imageurl'
			),
			'getcategories' => array(
				'title'
			),
			'getorders' => array(
				'title','optionid', 'caption','firstname','lastname','company','street1','street2','city'
			)
		);

		return (!is_null($call) && array_key_exists(strtolower($call), $cdataFields))
			? $cdataFields[strtolower($call)]
			: array();
	}
}
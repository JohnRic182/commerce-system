<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
class ApiProductGroup extends ApiItemBase
{
	public function GetItemType()
	{
		return "ProductGroup";
	}

	public $Title;
	public $GroupId;
	public $ProductCount;

	public static function LoadFromDataRecord($dataRecord)
	{
		return null;
	}

	public static function LoadById($id)
	{
		return array();
	}

	public static function GetProductGroups()
	{
		return array();
	}
	
	public static function GetProductCount($id)
	{
		return 0;
	}
}
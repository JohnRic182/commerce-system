<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
require_once(PLUGIN_PATH . "lib/ApiProductQoh.php");
require_once(PLUGIN_PATH . "lib/ApiProductOption.php");
class ApiProduct extends ApiProductQoh
{
    public $Price;
	public $DisplayPrice;
    public $Visible;
    public $Taxable;
    public $Weight;
    public $UPC;
    public $Sku;
    public $Title;
    public $URL;
    public $ThumbnailImageUrl;
    public $ImageUrl;
    public $Discontinued;
    public $Options;
    public $Added;
    public $ManufaturerName;
    public $Description;
    public $AmazonId;
    public $AmazonItemCondition;
    public $AmazonIdType;
    public $EbayCategoryId;
    public $YahooPath;
    public $GoogleItemCondition;
    public $GoogleProductType;
    public $PricegrabberCategory;
    public $PricegrabberItemCondition;
    public $PricegrabberPartNumber;
    public $InventoryControl;
    public $CategoryId;
    public $CategoryName;

    public static function LoadFromDataRecord($dataRecord)
    {
        global $db;
        global $settings;
        $returnProductQoh = ApiProductQoh::LoadFromDataRecord($dataRecord);
        $returnProduct = new ApiProduct();
        $returnProduct->PID = $returnProductQoh->PID;
        $returnProduct->ProductId = $returnProductQoh->ProductId;
        $returnProduct->Qoh = $returnProductQoh->Qoh;
        $returnProduct->Visible = $dataRecord["is_visible"];
        $returnProduct->Sku = $dataRecord["product_sku"];
        $returnProduct->Added = $dataRecord["added"];
        $returnProduct->Price = $dataRecord["price"];
		$returnProduct->DisplayPrice = getAdminPrice($dataRecord['price']);

        $returnProduct->Taxable = $dataRecord["is_taxable"];
        $returnProduct->Weight = $dataRecord["weight"];
        $returnProduct->UPC = $dataRecord["product_gtin"];
        $returnProduct->GTIN = $dataRecord["product_gtin"];
        $returnProduct->MPN = $dataRecord["product_mpn"];
        $returnProduct->ManufaturerName = $dataRecord["manufacturer_name"];
        $returnProduct->Description = $dataRecord["description"];
        $returnProduct->Title = $dataRecord["title"];
        $returnProduct->Discontinued = "No";
        $returnProduct->URL = $dataRecord["url"];
        $returnProduct->InventoryControl = $dataRecord["inventory_control"];

        $returnProduct->AmazonId = $dataRecord["amazon_id"];
        $returnProduct->AmazonIdType = $dataRecord["amazon_id_type"];
        $returnProduct->AmazonItemCondition = $dataRecord["amazon_item_condition"];
        $returnProduct->EbayCategoryId = $dataRecord["ebay_cat_id"];
        $returnProduct->YahooPath = $dataRecord["yahoo_path"];
        $returnProduct->GoogleItemCondition = $dataRecord["google_item_condition"];
		$returnProduct->GoogleProductType = $dataRecord["google_product_type"];
        $returnProduct->PricegrabberCategory = $dataRecord["pricegrabber_category"];
        $returnProduct->PricegrabberItemCondition = $dataRecord["pricegrabber_item_condition"];
        $returnProduct->PricegrabberPartNumber = $dataRecord["pricegrabber_part_number"];

        $returnProduct->NextagCategory = $dataRecord["nextag_category"];
        $returnProduct->NextagPartNumber = $dataRecord["nextag_part_number"];
        $returnProduct->NextagItemCondition = $dataRecord["pricegrabber_item_condition"];

        $returnProduct->CategoryId = $dataRecord['cid'];
		$returnProduct->CategoryName = $dataRecord['cat_name'];

        $thumb = "";
        $image = "";
        if ($dataRecord["image_location"] == "Web" && $dataRecord["image_url"] != "")
        {
            $thumb = $dataRecord["image_url"];
            $image = $dataRecord["image_url"];
        }
        else
        {
            if ($dataRecord["image"])
            {
                $image = $settings["GlobalHttpUrl"]."/".$dataRecord["image"];
            }
            if ($dataRecord["thumb"])
            {
                $thumb = $settings["GlobalHttpUrl"]."/".$dataRecord["thumb"];
            }
        }
        $returnProduct->ThumbnailImageUrl = $thumb;
        $returnProduct->ImageUrl = $image;
        $returnProduct->Options = array();

        $db->query("SELECT * FROM ".DB_PREFIX."products_attributes WHERE pid='".$dataRecord["pid"]."' ORDER BY priority");
        if ($db->numRows() > 0)
        {
            while (($optionRecord = $db->moveNext()) != false)
            {
                array_push($returnProduct->Options, ApiProductOption::LoadFromDataRecord($optionRecord,$dataRecord));
            }
        }

        return $returnProduct;
    }
}
?>
<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
class ApiProductOption extends ApiItemBase
{
    public function GetItemType()
    {
        return "ProductOption";
    }

    public $Title;
    public $Caption;
    public $OptionId;
    public $Type;
    public $Price;
    public $Weight;
    public $Priority;
    public $Options;

    public static function LoadFromDataRecord($dataRecord,$productDataRecord)
    {
        $returnOption = new ApiProductOption();
        $returnOption->OptionId = $dataRecord["paid"];
        $returnOption->Title = $dataRecord["name"];
        $returnOption->Caption = $dataRecord["caption"];
        $returnOption->Type = $dataRecord["attribute_type"];
        $returnOption->Price = 0;
        $returnOption->Priority = $dataRecord["priority"];
        $returnOption->Options = array();

        if (in_array($dataRecord['attribute_type'], array('select','radio')))
        {
            $optionValues = array();
            if(parseOptions($dataRecord["options"], $productDataRecord["price"], $productDataRecord["weight"], $optionValues)){
                $sortIdx = 1;
                foreach($optionValues as $opt_name=>$opt_value){
                    $subOption = new ApiProductOption();
                    $subOption->OptionId = $dataRecord["paid"]."-".$opt_value["name"];
                    $subOption->Title = $opt_value["name"];
                    $subOption->Price = number_format($opt_value["price_difference"], 2, ".", "");
                    $subOption->Weight = number_format($opt_value["weight_difference"], 2, ".", "");
                    $subOption->Priority = $sortIdx;
                    $sortIdx++;
                    array_push($returnOption->Options,$subOption);
                }
            }
        }
        return $returnOption;
    }
}
?>
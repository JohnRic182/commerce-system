<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
require_once(PLUGIN_PATH . "lib/ApiOrderItemOption.php");
class ApiOrderItem extends ApiItemBase
{
    public function GetItemType()
    {
        return "OrderLineItem";
    }

    public $PID;
    public $Quantity;
    public $Price;
    public $Taxable;
    public $Weight;
    public $FreeShipping;
    public $ShippingPrice;
    public $TaxRate;
    public $TaxDescription;
    public $DigitalProduct;
	public $ProductType;
    public $UpdatedQuantity;
    public $UpdatedPrice;
    public $ProductId;
    public $ProductSubId;
    public $SKU;
    public $UPC;
    public $Title;
    public $LineItemWeight;
    public $ProductWeight;
    public $Options;

    public static function LoadFromDataRecord($dataRecord)
    {
        $returnOrderItem = new ApiOrderItem();
        $returnOrderItem->PID = $dataRecord["pid"];
		$returnOrderItem->ProductType = $dataRecord['product_type'];
        $returnOrderItem->Quantity = $dataRecord["quantity"];
        $returnOrderItem->Price = $dataRecord["price"];
        $returnOrderItem->Taxable = $dataRecord["is_taxable"];
        $returnOrderItem->Weight = $dataRecord["weight"];
        $returnOrderItem->FreeShipping = $dataRecord["free_shipping"];
        $returnOrderItem->ShippingPrice = $dataRecord["shipping_price"];
        $returnOrderItem->TaxRate = $dataRecord["tax_rate"];
        $returnOrderItem->TaxDescription = $dataRecord["tax_description"];
        $returnOrderItem->DigitalProduct = $dataRecord["product_type"] == 'Digital' ? 1 : 0;
        $returnOrderItem->UpdatedQuantity = $dataRecord["admin_quantity"];
        $returnOrderItem->UpdatedPrice = $dataRecord["admin_price"];
        $returnOrderItem->ProductId = $dataRecord["product_id"];
        $returnOrderItem->ProductSubId = trim($dataRecord['product_sub_id']) <> '' ? trim($dataRecord['product_sub_id']) : $dataRecord["product_id"];
        $returnOrderItem->SKU = $dataRecord["product_sku"];
        $returnOrderItem->UPC = $dataRecord["product_upc"];
		$returnOrderItem->GTIN = $dataRecord["product_gtin"];
		$returnOrderItem->MPN = $dataRecord["product_mpn"];
        $returnOrderItem->Title = $dataRecord["title"];
        $returnOrderItem->ProductWeight = $dataRecord["weight"];
        $returnOrderItem->LineItemWeight = $returnOrderItem->ProductWeight * $returnOrderItem->Quantity;
        $returnOrderItem->Options = array();
        if (trim($dataRecord["options_clean"]) != "")
        {
            $optionStrings = explode("\n", $dataRecord["options_clean"]);
            foreach ($optionStrings as $optionString)
            {
                array_push($returnOrderItem->Options,ApiOrderItemOption::LoadFromString($optionString));
            }
        }

        return $returnOrderItem;
    }    
}
?>
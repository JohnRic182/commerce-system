<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
class ApiOrderItemOption extends ApiItemBase
{
    public function GetItemType()
    {
        return "OrderLineItemOption";
    }

    public $Title;
    public $SelectedOption;
    public $Price;
    public $Weight;

    public static function LoadFromString($optionString)
    {
        $optionObject = new ApiOrderItemOption();

        $optionArray = explode(":", $optionString);
        $optionObject->Title = trim($optionArray[0]);
        if (strstr($optionArray[1], "(") && strstr($optionArray[1], ")"))
        {
            $optionParts = explode("(", $optionArray[1]);
            $optionObject->SelectedOption = trim($optionParts[0]);
            $modifiers = str_replace(")", " ", $optionParts[1]);
            if (strstr($modifiers, ","))
            {
                $modifierParts = explode(",", $modifiers);
                $optionObject->Price = $modifierParts[0];
                $optionObject->Weight = $modifierParts[1];
            }
            else
            {
                $optionObject->Price = $modifiers;
                $optionObject->Weight = 0;
            }
        } else {
            $optionObject->SelectedOption = trim($optionArray[1]);
            $optionObject->Price = 0;
            $optionObject->Weight = 0;
        }
        return $optionObject;
    }
}
?>
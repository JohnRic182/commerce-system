<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
class ApiProductCategory extends ApiItemBase
{
    public function GetItemType()
    {
        return "Catalog";
    }

    public $Title;
    public $CategoryId;
    public $Children;
    public $ProductCount;

    public function GetChildren()
    {
        return ApiProductCategory::LoadByParentId($this->CategoryId);
    }
    public static function LoadFromDataRecord($dataRecord)
    {
        global $db;
        $returnCategory = new ApiProductCategory();
        $returnCategory->Title = $dataRecord["name"];
        $returnCategory->CategoryId = $dataRecord["cid"];
        $returnCategory->Children = self::LoadByParentId($returnCategory->CategoryId);
        $returnCategory->ProductCount = self::GetProductCount($dataRecord['cid']);
        return $returnCategory;
    }

    public static function LoadByParentId($parentid)
    {
        global $db;
        $sql = "select * from ".DB_PREFIX."catalog where parent = ".$db->escape($parentid);
        $db->query($sql);
        $rows = $db->getRecords();
        $returnArray = array();
        foreach ($rows as $row)
        {
            array_push($returnArray,ApiProductCategory::LoadFromDataRecord($row));
        }
        return $returnArray;
    }
    
    public static function GetProductCount($category_id)
    {
    	global $db;
    	$db->query('SELECT COUNT(*) AS product_count FROM '.DB_PREFIX.'products_categories WHERE cid = '.$db->escape($category_id));
    	$db->movenext();
    	return $db->col['product_count'];
    }
}
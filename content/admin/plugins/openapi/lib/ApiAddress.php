<?php

require_once(PLUGIN_PATH . "lib/ApiItemBase.php");

class ApiAddress extends ApiItemBase
{
    public function GetItemType()
    {
        return "Address";
    }

    public $Name;
    public $Street1;
    public $Street2;
    public $City;
    public $State;
    public $Zip;
    public $Country;
}
?>
<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
class ApiProductQoh extends ApiItemBase
{
	public function GetItemType()
	{
		return "Product";
	}

	public $PID;
	public $ProductId;
	public $ProductSubId;
	public $Qoh;

	public static function LoadFromDataRecord($dataRecord)
	{
		$returnProduct = new ApiProductQoh();
		$returnProduct->PID = $dataRecord["pid"];
		$returnProduct->ProductId = $dataRecord["product_id"];
		$returnProduct->ProductSubId = $dataRecord['product_subid'];
		$returnProduct->Qoh = $dataRecord['stock'];

		return $returnProduct;
	}
}

<?php
require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
class ApiShippingInfo extends ApiItemBase
{
    public function GetItemType()
    {
        return "ShippingInfo";
    }

    public $FullName;
    public $Company;
    public $ShippingDigital;
	public $ShippingRequired;

    /**
     *
     * @var ApiAddress
     */
    public $Address;
}
?>
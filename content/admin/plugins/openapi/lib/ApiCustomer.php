<?php

require_once(PLUGIN_PATH . "lib/ApiItemBase.php");
require_once(PLUGIN_PATH . "lib/ApiAddress.php");
require_once(PLUGIN_PATH . "lib/ApiBillingInfo.php");
require_once(PLUGIN_PATH . "lib/ApiShippingInfo.php");
require_once(PLUGIN_PATH . "lib/ApiOrderItem.php");

class ApiCustomer extends ApiItemBase
{
    public function GetItemType()
    {
        return "Customer";
    }

    public $UserId;
    public $UserName;

    /**
     *
     * @var ApiBillingInfo
     */
    public $Billing;
    public $AddressBook;

    public static function LoadFromDataRecord($dataRecord)
    {
        global $db;
        $customer = new ApiCustomer();
        $customer->UserId = $dataRecord["uid"];
        $customer->UserName = $dataRecord["login"];
        $customer->Billing = new ApiBillingInfo();
	    $customer->Billing->UserId = $dataRecord['uid'];
        $customer->Billing->FullName = $dataRecord["fname"]." ".$dataRecord["lname"];
        $customer->Billing->FirstName = $dataRecord["fname"];
        $customer->Billing->LastName = $dataRecord["lname"];
        $customer->Billing->Email = $dataRecord["email"];
        $customer->Billing->Company = $dataRecord["company"];
        $customer->Billing->Phone = $dataRecord["phone"];
        $customer->Billing->Address = new ApiAddress();
        $customer->Billing->Address->Street1 = $dataRecord["address1"];
        $customer->Billing->Address->Street2 = $dataRecord["address2"];
        $customer->Billing->Address->City = $dataRecord["city"];
        $customer->Billing->Address->State = $dataRecord["billing_state_name"];
        $customer->Billing->Address->Zip = $dataRecord["zip"];
        $customer->Billing->Address->Country = $dataRecord["billing_country_name"];

        $db->query(
                "SELECT ".
                "IF(".DB_PREFIX."users_shipping.country=1 OR ".DB_PREFIX."users_shipping.country=2, sstate.short_name, ".DB_PREFIX."users_shipping.province) AS shipping_state_name, ".
                "scountry.iso_a2 AS shipping_country_name, ".
                DB_PREFIX."users_shipping.name AS shipping_name, ".
                DB_PREFIX."users_shipping.company AS shipping_company, ".
                DB_PREFIX."users_shipping.address1 AS shipping_address1, ".
                DB_PREFIX."users_shipping.address2 AS shipping_address2, ".
                DB_PREFIX."users_shipping.city AS shipping_city, ".
                DB_PREFIX."users_shipping.zip AS shipping_zip ".
                "FROM ".DB_PREFIX."users_shipping ".
                "LEFT JOIN ".DB_PREFIX."states AS sstate ON sstate.stid=".DB_PREFIX."users_shipping.state ".
                "LEFT JOIN ".DB_PREFIX."countries AS scountry ON scountry.coid=".DB_PREFIX."users_shipping.country ".
                "WHERE uid='".$dataRecord["uid"]."'"
        );
        $addresses = $db->getRecords();
        
        $customer->AddressBook = array();
        foreach ($addresses as $addressRecord)
        {
            $address = new ApiAddress();
            $address->Name = $addressRecord["shipping_name"];
            $address->Street1 = $addressRecord["shipping_address1"];
            $address->Street2 = $addressRecord["shipping_address2"];
            $address->City = $addressRecord["shipping_city"];
            $address->State = $addressRecord["shipping_state_name"];
            $address->Zip = $addressRecord["shipping_zip"];
            $address->Country = $addressRecord["shipping_country_name"];
            array_push($customer->AddressBook, $address);
        }

        return $customer;
    }
}
?>
<?php

require_once(PLUGIN_PATH. "lib/ApiItemBase.php");
require_once(PLUGIN_PATH . "lib/ApiAddress.php");
require_once(PLUGIN_PATH . "lib/ApiBillingInfo.php");
require_once(PLUGIN_PATH . "lib/ApiShippingInfo.php");
require_once(PLUGIN_PATH . "lib/ApiOrderItem.php");

class ApiOrder extends ApiItemBase
{
    public function GetItemType()
    {
        return "Order";
    }
    
    public $OrderId;
    public $OrderNumber;
    public $CreateDate;
    public $Status;
    public $StatusDate;
    public $PaymentMethod;
    public $PaymentStatus;
    public $ShippingCarrierId;
    public $ShippingMethodId;
    public $ShippingMethodName;
    public $ShippingAmount;
    public $ShippingTaxable;
    public $ShippingTaxRate;
    public $ShippingTaxAmount;
    public $ShippingTaxDescription;
    public $HandlingSeparated;
    public $HandlingFee;
    public $HandlingFeeType;
    public $HandlingAmount;
    public $HandlingTaxable;
    public $HandlingTaxRate;
    public $HandlingTaxAmount;
    public $HandlingTaxDescription;
    public $HandlingText;
    public $SubtotalAmount;
    public $DiscountAmount;
    public $DiscountValue;
    public $DiscountType;
    public $PromoDiscountAmount;
    public $PromoDiscountValue;
    public $PromoDiscountType;
    public $TaxAmount;
    public $TotalAmount;
    public $GiftMessage;
    public $TotalWeight;

    /**
     *
     * @var ApiBillingInfo
     */
    public $Billing;
    public $Shipping;
    public $OrderItems;

    public static function LoadFromDataRecord($dataRecord)
    {
        global $db;
        $order = new ApiOrder();
        $order->OrderId = $dataRecord["oid"];
        $order->OrderNumber = $dataRecord["order_num"];
        $order->CreateDate = $dataRecord["create_date"];
        $order->Status = $dataRecord["status"];
        $order->StatusDate = $dataRecord["status_date"];
        $order->PaymentMethod = $dataRecord["payment_method_name"];
        $order->PaymentStatus = $dataRecord["payment_status"];

		// TODO: change to shipments
        $order->ShippingCarrierId = '';
        $order->ShippingMethodId = '';
        $order->ShippingMethodName = '';
        $order->ShippingAmount = $dataRecord["shipping_amount"];
        $order->ShippingTaxable = $dataRecord["shipping_taxable"];
        $order->ShippingTaxRate = $dataRecord["shipping_tax_rate"];
        $order->ShippingTaxAmount = $dataRecord["shipping_tax_amount"];
        $order->ShippingTaxDescription = $dataRecord["shipping_tax_description"];
        $order->HandlingSeparated = $dataRecord["handling_separated"];
        $order->HandlingFee = $dataRecord["handling_fee"];
        $order->HandlingFeeType = $dataRecord["handling_fee_type"];
        $order->HandlingAmount = $dataRecord["handling_amount"];
        $order->HandlingTaxable = $dataRecord["handling_taxable"];
        $order->HandlingTaxRate = $dataRecord["handling_tax_rate"];
        $order->HandlingTaxAmount = $dataRecord["handling_tax_amount"];
        $order->HandlingTaxDescription = $dataRecord["handling_tax_description"];
        $order->HandlingText = $dataRecord["handling_text"];
        $order->SubtotalAmount = $dataRecord["subtotal_amount"];
        $order->DiscountAmount = $dataRecord["discount_amount"];
        $order->DiscountValue = $dataRecord["discount_value"];
        $order->DiscountType = $dataRecord["discount_type"];
        $order->PromoDiscountAmount = $dataRecord["promo_discount_amount"];
        $order->PromoDiscountValue = $dataRecord["promo_discount_value"];
        $order->PromoDiscountType = $dataRecord["promo_discount_type"];
        $order->TaxAmount = $dataRecord["tax_amount"];
        $order->TotalAmount = $dataRecord["total_amount"];
        $order->GiftMessage = $dataRecord["gift_message"];

        $order->Billing = new ApiBillingInfo();
	$order->Billing->UserId = $dataRecord['uid'];
        $order->Billing->FullName = $dataRecord["fname"]." ".$dataRecord["lname"];
        $order->Billing->FirstName = $dataRecord["fname"];
        $order->Billing->LastName = $dataRecord["lname"];
        $order->Billing->Email = $dataRecord["email"];
        $order->Billing->Company = $dataRecord["company"];
        $order->Billing->Phone = $dataRecord["phone"];
        $order->Billing->Address = new ApiAddress();
        $order->Billing->Address->Street1 = $dataRecord["address1"];
        $order->Billing->Address->Street2 = $dataRecord["address2"];
        $order->Billing->Address->City = $dataRecord["city"];
        $order->Billing->Address->State = $dataRecord["billing_state_name"];
        $order->Billing->Address->Zip = $dataRecord["zip"];
        $order->Billing->Address->Country = $dataRecord["billing_country_name"];

        $order->Shipping = new ApiShippingInfo();
        if ($dataRecord["shipping_required"] == '0')
        {
			$order->Shipping->ShippingRequired = 'No';
			$order->Shipping->ShippingDigital = 'Yes';
        }
		else
		{
			$order->Shipping->ShippingRequired = 'Yes';
			$order->Shipping->ShippingDigital = 'No';
            $order->Shipping->FullName = $dataRecord["shipping_name"];
            $order->Shipping->Company = $dataRecord["shipping_company"];
            $order->Shipping->Address = new ApiAddress();
            $order->Shipping->Address->Street1 = $dataRecord["shipping_address1"];
            $order->Shipping->Address->Street2 = $dataRecord["shipping_address2"];
            $order->Shipping->Address->City = $dataRecord["shipping_city"];
            $order->Shipping->Address->State = $dataRecord["shipping_state_name"];
            $order->Shipping->Address->Zip = $dataRecord["shipping_zip"];
            $order->Shipping->Address->Country = $dataRecord["shipping_country_name"];
        }

        $sql = "SELECT * FROM ".DB_PREFIX."orders_content WHERE oid='".$order->OrderId."'";
        $db->query($sql);

        $order->OrderItems = array();
        while (($productRecord = $db->moveNext()) != 0)
        {
            array_push($order->OrderItems, ApiOrderItem::LoadFromDataRecord($productRecord));
        }
        $totalOrderWeight = 0;
        foreach ($order->OrderItems as $orderitem)
        {
            $totalOrderWeight += $orderitem->LineItemWeight;
        }

        $order->TotalWeight = $totalOrderWeight;
        return $order;
    }
}

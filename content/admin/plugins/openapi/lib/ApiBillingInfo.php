<?php

require_once(PLUGIN_PATH . "lib/ApiItemBase.php");

class ApiBillingInfo extends ApiItemBase
{
    public function GetItemType()
    {
        return "BillingInfo";
    }

    public $UserId;
    public $FullName;
    public $FirstName;
    public $LastName;
    public $Email;
    public $Company;
    public $Phone;

    /**
     *
     * @var ApiAddress
     */
    public $Address;
}
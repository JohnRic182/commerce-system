<?php

//plugin class
class plugin_webgility extends CartPlugin
{
	//protected
	var $plugin_id = "webgility";
	var $plugin_group = "export"; /* Group Name */
	var $plugin_caption = "Webgility eCommerce Connector";
	var $plugin_description = "Automated QuickBooks synchronization, printing labels and order management.";

	//constructor
	function plugin_webgility()
	{
		$this->interface = array();
		$this->interface["title"] = $this->plugin_caption;
		$this->interface["url"] = 'admin.php?p=plugin&plugin_id='.$this->plugin_id;
		$this->interface["class"] = "ic-download";
		$this->interface["group"] = "";

		return $this;
	}

	function runPlugin(&$db, &$settings, &$request)
	{
		parent::runPlugin($db, $settings, $request);
		extract($request);
		print '<h1 class="admin-page-header clearfix"><span class="icon icb-icon"></span><span class="text"><strong>Webgility eCommerce Connector</strong></span></h1>';
		print '<img alt="Webgility Logo" src="images/admin/logo_webgility.gif" align="right" vspace="10"/>';
		print '<ul class="pageNote admin-page-note"><li><span class="icon ic-icon ic-info">&nbsp;</span>Webgility\'s eCommerce Connector saves you hours of work every day by automatically downloading orders from your online store. You can post orders into QuickBooks with a single mouse-click! Better yet, use eCC to synchronize your online store inventory with QuickBooks. eCC integrates with QuickBooks Merchant Service, Authorize.net and PayPal so you can process payments. eCC also helps you generate shipping labels with USPS, UPS WorldShip, Endicia Dazzle or Stamps.com.</li></ul>';
		print '<ul class="pageNote admin-page-note"><li><span class="icon ic-icon ic-info">&nbsp;</span>Your cart is already configured to work with Webgility\'s desktop software.</li></ul>';
		print '<ul class="pageNote admin-page-note"><li><span class="icon ic-icon ic-info">&nbsp;</span>For more information about Webgility, please <a href=\"http://www.cartmanual.com/url/webgility\" target=\"_blank\">click here</a></li></ul>';
	}
}

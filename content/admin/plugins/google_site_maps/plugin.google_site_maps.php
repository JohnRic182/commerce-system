<?php

/**
 * Class plugin_google_site_maps
 */
class plugin_google_site_maps extends CartPlugin
{
	//protected
	var $plugin_id = "google_site_maps";
	var $plugin_group = "export";
	var $plugin_caption = "Google Sitemaps";
	var $plugin_description = "The sitemap protocol allows you to more easily inform search engines about the pages on your website that are available for indexing into the search engines.";

	//constructor
	function plugin_google_site_maps()
	{
		return $this;
	}

	/**
	 * @param $db
	 * @param $settings
	 * @param $request
	 * @return bool
	 */
	function runPlugin(&$db, &$settings, &$request)
	{
		//print_r($request);
		parent::runPlugin($db, $settings, $request);
		extract($request);
		$step = isset($step)?intval($step):1;
		$download = isset($download) ? $download : true;
		switch($step)
		{
			default:
			case 1: {
				global $p;
				if ($p != "google_marketing") echo '<h1 class="admin-page-header clearfix"><span class="icon icb-icon icb-google"></span><span class="text"><strong>Google Site Map Export</strong></span></h1>';
				$text = '<div align="justify">'.
						"Google Sitemap allows you to inform search engines about URLs on your websites that are available for crawling. In its simplest form, a Sitemap that uses the Sitemap Protocol is an XML file that lists URLs for a site. <br><br>".
						"Using this protocol does not guarantee that your webpages will be included in search indexes (Note that using this protocol will not influence the way your pages are ranked by Google).<br><br>".
						"This system will create an XML document that must be saved locally, zipped in gzip format, and uploaded back to the server.<br><br>".
						"Currently Google recommends placing you sitemap at the root directory of your HTML server; that is, place it at http://example.com/sitemap.xml.gz<br><br>".
						"By default the system will assign &lt;changefreq&gt; as weekly and assign its own priority to pages.<br><br>".
						"If you want to adjust this, make the necessary changes in the XML file before you zip it and move back to the server.<br><br>".
						"For more information about Google Site Maps visit <a target=\"_blank\" href=\"http://www.google.com/webmasters/\">http://www.google.com/webmasters/</a>".
						'</div>';
				echo '<ul class="pageNote admin-page-note"><li><span class="icon ic-icon ic-info">&nbsp;</span>' .$text. '</li></ul>';

				break;
			}
			case 2 : {

				$lastmod = date("Y-m-d", time());

				header('Content-Type: text/xml');
				header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
				header('Content-Disposition: inline; filename="sitemap.xml"');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				if ($download) {
					header('Content-Disposition: attachment; filename="sitemap.xml"');
				}
				//header('Pragma: no-cache');

				echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
				echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84">'."\n";

				//export home page
				echo '<url>'."\n";
				echo '<loc>'.$settings["GlobalHttpUrl"].'/</loc>'."\n";
				echo '<lastmod>'.$lastmod.'</lastmod>'."\n";
				echo '<changefreq>weekly</changefreq>'."\n";
     			echo '<priority>1.0</priority>'."\n";
				echo '</url>'."\n";

				//export sitemap
				echo '<url>'."\n";
				echo '<loc>'.$settings["GlobalHttpUrl"].'/index.php?p=site_map</loc>'."\n";
				echo '<lastmod>'.$lastmod.'</lastmod>'."\n";
				echo '<changefreq>weekly</changefreq>'."\n";
     			echo '<priority>0.5</priority>'."\n";
				echo '</url>'."\n";

				//export text pages
				$db->query("SELECT IF(url_custom='', url_default, url_custom) AS url, name FROM ".DB_PREFIX."pages WHERE is_active='Yes'");
				while (($page =$db->moveNext()) != false)
				{
					//export sitemap
					echo '<url>'."\n";
					echo '<loc>'.str_replace("&", "&amp;", UrlUtils::getPageUrl($page["name"], $page["url"], true)).'</loc>'."\n";
					echo '<lastmod>'.$lastmod.'</lastmod>'."\n";
					echo '<changefreq>weekly</changefreq>'."\n";
     				echo '<priority>0.5</priority>'."\n";
					echo '</url>'."\n";
				}

				//export categories
				$db->query("SELECT cid, IF(url_custom='',url_default,url_custom) AS url FROM ".DB_PREFIX."catalog WHERE is_visible='Yes'");

				while (($category = $db->moveNext()) != false)
				{
					//export sitemap
					echo '<url>'."\n";
					echo '<loc>'.str_replace("&", "&amp;", UrlUtils::getCatalogUrl($category["url"], $category["cid"], true)).'</loc>'."\n";
					echo '<lastmod>'.$lastmod.'</lastmod>'."\n";
					echo '<changefreq>weekly</changefreq>'."\n";
     				echo '<priority>0.75</priority>'."\n";
					echo '</url>'."\n";
				}

				//export products
				$db->query("SELECT pid, cid, title, is_hotdeal, is_home, IF(url_custom = '', url_default, url_custom) AS url FROM ".DB_PREFIX."products WHERE is_visible='Yes'");
				while (($product = $db->moveNext()) != false)
				{
					//export sitemap
					$priority = 0.5;
					if($product["is_hotdeal"] == "Yes"){
						$priority = $priority + 0.2;
					}
					if($product["is_home"] == "Yes"){
						$priority = $priority + 0.2;
					}
					echo '<url>'."\n";
					echo '<loc>'.str_replace("&", "&amp;", UrlUtils::getProductUrl($product["url"], $product["pid"], $product["cid"], true)).'</loc>'."\n";
					echo '<lastmod>'.$lastmod.'</lastmod>'."\n";
					echo '<changefreq>weekly</changefreq>'."\n";
     				echo '<priority>'.$priority.'</priority>'."\n";
					echo '</url>'."\n";
				}

				echo '</urlset>'."\n";
				break;
			}
		}

		return true;
	}
}

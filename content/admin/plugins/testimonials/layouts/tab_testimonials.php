<?php
	$perPage = 10;
	$start = isset($start)?intval($start):0;
	$previewLength = 100;
	$testimonialList = $this->getTestimonials($start, $perPage);
?>
<script type="text/javascript">
function setStatus(id,el)
{
	// ajax request to grab products
	$.post(
		"admin.php?p=plugin&plugin_id=testimonials&chart=true",
		{
			action: 'setTestimonialStatus',
			id: id,
			status: $(el).val()
		},
		function(data)
		{
			$('#testimonialRecord_' + id).fadeOut("fast", function(){$('#testimonialRecord_' + id).fadeIn("fast")});

		}
	);

}
function confirmDeleteTestimonial(id)
{
	if (confirm('Are you sure you want to delete this testimonial?'))
	{
		$.post(
			"admin.php?p=plugin&plugin_id=testimonials&chart=true",
			{
				action: 'deleteTestimonial',
				id: id
			},
			function(data)
			{
				$('#testimonialRecord_' + id).fadeOut("fast");
			}
		);

	}
}
</script>
<div id="admin-tab-sheet-testimonials" class="admin-tab-sheet hidden">
	<div class="admin-tab-sheet-wrap">
		<div class="admin-form-header first"><span class="icon ic-icon"></span>Your Testimonials</div>
	
		<div style="padding:7px 0px 10px 32px;">Here is an overview of all submitted Testimonials. Please approve or decline pending Testimonials. Changes are saved when made.</div>
<?php
	if (!empty($testimonialList))
	{
?>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="admin-list-table">
			<thead>
				<tr>
					<th class="icon">&nbsp;</th>
					<th style="width:150px;">Name</th>
					<th style="width:150px;">Company</th>
					<th style="width:75px;">Rating</th>
					<th style="width:150px;">Status</th>
					<th>Testimonial</th>
					<th style="width:80px;text-align:right;">
<?php
		if ($start > 0) echo '<a href="admin.php?p=plugin&plugin_id=testimonials&start='.($start-$perPage).'">Previous</a>';
		if ($this->totalTestimonials > ($start+$perPage)) echo '<a href="admin.php?p=plugin&plugin_id=testimonials&start='.($start+$perPage).'">Next</a>';
?>
					</th>
				</tr>
			</thead>
			<tbody>
<?php
		foreach($testimonialList as $testimonialRecord)
		{
?>
				<tr id="testimonialRecord_<?=$testimonialRecord['id']?>">
					<td class="icon">&nbsp;</td>
					<td style="width:150px;"><?=gs($testimonialRecord["name"])?></td>
					<td style="width:150px;"><?=gs($testimonialRecord["company"])?></td>
					<td style="width:75px;"><?=gs($testimonialRecord["rating"])?></td>
					<td style="width:150px;">
						<select onchange="setStatus('<?=$testimonialRecord['id']?>', this)" class="short">
							<option value="pending">Pending</option>
							<option value="approved" <?=$testimonialRecord["status"]=="approved"?'selected="selected"':''?>>Approved</option>
							<option value="declined" <?=$testimonialRecord["status"]=="declined"?'selected="selected"':''?>>Declined</option>
						</select>
					</td>
					<td><?=gs(substr($testimonialRecord["testimonial"], 0, 100))?>...</td>
					<td style="width:80px;text-align:right;">
						<a href="admin.php?p=plugin&plugin_id=<?=$this->plugin_id?>&testimonialId=<?=$testimonialRecord['id']?>&last_active_tab=create"><img border="0" src="images/icons/a/pencil.png" alt="Click to edit"/></a>
						<a href="#" onclick="confirmDeleteTestimonial('<?=$testimonialRecord['id']?>');return false;"><img src="images/icons/a/delete.png?v={$APP_VERSION}" alt="Click to delete" border="0"/></a>
					</td>
				</tr>

<?php
		}
?>
			</tbody>
		</table>
<?php
	}
	else
	{
		echo '<div style="padding: 7px 0 10px 32px; font-weight: bold;">No testimonials yet</div>';
	}
?>
		&nbsp;
	</div>
</div>
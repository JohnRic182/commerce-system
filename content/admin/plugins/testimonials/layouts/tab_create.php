<?php
	$testimonialId = isset($_REQUEST["testimonialId"]) ? intval($_REQUEST["testimonialId"]) : false;
	if ($testimonialId)
	{
		$testimonialInfo = $this->getTestimonial($testimonialId);
	}

	$testimonial_id = (!empty($testimonialInfo['id']))?$testimonialInfo['id']:'';
	$testimonial_name = (!empty($testimonialInfo['name']))?$testimonialInfo['name']:'';
	$testimonial_company = (!empty($testimonialInfo['company']))?$testimonialInfo['company']:'';
	$testimonial_rating = (!empty($testimonialInfo['rating']))?$testimonialInfo['rating']:'';
	$testimonial_testimonial = (!empty($testimonialInfo['testimonial']))?$testimonialInfo['testimonial']:'';
	$testimonial_status = (!empty($testimonialInfo['status']))?$testimonialInfo['status']:'pending';
?>
<script type="text/javascript"><!--
$(document).ready(function(){
	$("#form-testimonial").submit(function(){
		if ($.trim($("#form-testimonial input[name='testimonial_name']").val()) == "")
		{
			alert("Please enter name");
			$("#form-testimonial input[name='testimonial_name']").focus();
			return false;
		}
		if ($.trim($("#form-testimonial select[name='testimonial_rating']").val()) == "")
		{
			alert("Please select rating");
			$("#form-testimonial select[name='testimonial_rating']").focus();
			return false;
		}
		if ($.trim($("#form-testimonial textarea[name='testimonial_testimonial']").val()) == "")
		{
			alert("Please enter testimonial text");
			$("#form-testimonial textarea[name='testimonial_testimonial']").focus();
			return false;
		}
		return true;
	});
});
--></script>
<div id="admin-tab-sheet-create" class="admin-tab-sheet hidden">
	<div class="admin-tab-sheet-wrap">
		<form method="post" id="form-testimonial" action="admin.php?p=plugin&plugin_id=<?=$this->plugin_id;?>">
			<input type="hidden" name="saveTestimonial" value="1"/>
			<input type="hidden" name="testimonial_id" value="<?=$testimonial_id;?>"/>
			<div class="admin-form-header first"><span class="icon ic-icon"></span>Write Testimonials</div>
			<div style="padding:15px 32px;">
				You can manually enter your own Testimonials here (good for testimonial letters you may have received).
			</div>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="formItemCaption required">Name</td>
					<td class="formItemControl"><input type="text" name="testimonial_name" value="<?=htmlspecialchars($testimonial_name);?>"/></td>
				</tr>
				<tr>
					<td class="formItemCaption">Company</td>
					<td class="formItemControl"><input type="text" name="testimonial_company" value="<?=htmlspecialchars($testimonial_company);?>"/></td>
				</tr>
				<tr>
					<td class="formItemCaption">Rating</td>
					<td class="formItemControl">
						<select name="testimonial_rating" class="short">
							<option value="">-</option>
							<option value="1" <?=($testimonial_rating=='1')?'selected="selected"':'';?>>1</option>
							<option value="2" <?=($testimonial_rating=='2')?'selected="selected"':'';?>>2</option>
							<option value="3" <?=($testimonial_rating=='3')?'selected="selected"':'';?>>3</option>
							<option value="4" <?=($testimonial_rating=='4')?'selected="selected"':'';?>>4</option>
							<option value="5" <?=($testimonial_rating=='5')?'selected="selected"':'';?>>5</option>
							<option value="6" <?=($testimonial_rating=='6')?'selected="selected"':'';?>>6</option>
							<option value="7" <?=($testimonial_rating=='7')?'selected="selected"':'';?>>7</option>
							<option value="8" <?=($testimonial_rating=='8')?'selected="selected"':'';?>>8</option>
							<option value="9" <?=($testimonial_rating=='9')?'selected="selected"':'';?>>9</option>
							<option value="10" <?=($testimonial_rating=='10')?'selected="selected"':'';?>>10</option>
						</select>
					</td>
				</tr>
				<tr valign="top">
					<td class="formItemCaption">Testimonial</td>
					<td class="formItemControl"><textarea name="testimonial_testimonial"><?=htmlspecialchars($testimonial_testimonial);?></textarea></td>
				</tr>
				<tr>
					<td class="formItemCaption">Status</td>
					<td class="formItemControl">
						<select name="testimonial_status" class="short">
							<option value="pending" <?=($testimonial_status=='pending')?'selected="selected"':'';?>>Pending</option>
							<option value="approved" <?=($testimonial_status=='approved')?'selected="selected"':'';?>>Approved</option>
							<option value="declined" <?=($testimonial_status=='declined')?'selected="selected"':'';?>>Declined</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="admin-form-buttons">
				<input type="submit" value="Save changes"/>
				<input type="reset" value="Reset form"/>
			</div>
		</form>
	</div>
</div>
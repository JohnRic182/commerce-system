<?php
	$sql = "SELECT * FROM ".DB_PREFIX."settings WHERE `group_name` = 'testimonials' ORDER BY `priority`";
	$db->query($sql);
	$testimonialSettings = $db->getRecords();
?>
<div id="admin-tab-sheet-settings" class="admin-tab-sheet hidden">
	<div class="admin-tab-sheet-wrap">
		<div class="admin-form-header first"><span class="icon ic-icon"></span>Testimonials Settings</div>
		<form method="post" action="admin.php?p=plugin&plugin_id=<?=$this->plugin_id?>" enctype="multipart/form-data">
			<input type="hidden" name="saveSettings" value="1"/>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
<?php 	
	if (!empty($testimonialSettings))
	{	
		foreach ($testimonialSettings as $testimonialSetting)
		{	
			echo '<tr valign="top"><td class="formItemCaption">'.gs($testimonialSetting["caption"]).'</td><td class="formItemControl">';
			switch ($testimonialSetting["input_type"])
			{
				case "text" :
				{
					echo '<input type="text" name="settings_testimonial['.$testimonialSetting["name"].']" value="'.gs($testimonialSetting["value"]).'">';
					break;
				}
				case "select" :
				{
					echo '<select name="settings_testimonial['.$testimonialSetting["name"].']"  class="short">';
					$options = explode(",", $testimonialSetting["options"]);
					for($c=0; $c<count($options); $c++){
						echo '<option value="'.trim($options[$c]).'" '.(trim($testimonialSetting["value"])==trim($options[$c])?"selected":"").'>'.gs($options[$c]).'</option>';
					}
					echo '</select>';
					break;
				}
				case "label" :
				{
					echo $testimonialSetting["value"];		
					break;
				}
			}
			
			if (trim($testimonialSetting["description"])!="")
			{
				echo '<div class="formHelp">'.$testimonialSetting["description"].'</div>';
			}
			echo '</td></tr>';
			
		}
		
?>
			</table>
			<div class="admin-form-header"><span class="icon ic-icon"></span>Rating Images</div>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="formItemCaption">Full</td>
					<td class="formItemControl">
						<input type="file" name="full"/>
						<img src="<?=$this->getImage('full')?>"/>
					</td>
				</tr>
				<tr>
					<td class="formItemCaption">Half</td>
					<td class="formItemControl">
						<input type="file" name="half"/>
						<img src="<?=$this->getImage('half')?>"/>
					</td>
				</tr>
				<tr>
					<td class="formItemCaption">Empty</td>
					<td class="formItemControl">
						<input type="file" name="empty"/>
						<img src="<?=$this->getImage('empty')?>"/>
					</td>
				</tr>
<?php 
	} 
	else
	{
		echo '<tr><td colspan="2"><p>No settings found</p></td></tr>';
	}
?>
			</table>
			<div class="admin-form-buttons">
				<input type="submit" value="Save Settings"/>
				<input type="reset" value="Reset"/>
			</div>
		</form>
	</div>
</div>
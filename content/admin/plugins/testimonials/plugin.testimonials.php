<?php

/**
 * Class plugin_testimonials
 */
class plugin_testimonials extends CartPlugin
{
	var $plugin_id = "testimonials";
	var $plugin_group = "misc";
	var $plugin_caption = "Testimonials";
	var $plugin_description = "Users can write testimonials, admins can verify / accept them";
	var $totalTestimonials = '0';
	var $imageDirectory = 'content/admin/plugins/testimonials/images/';

	/**
	 * plugin_testimonials constructor.
	 */
	function plugin_testimonials()
	{	
		$this->interface = array();
		$this->interface["title"] = $this->plugin_caption;
		$this->interface["url"] = 'admin.php?p=plugin&plugin_id='.$this->plugin_id;
		$this->interface["class"] = "ic-plugin";
		$this->interface["group"] = "misc";
		return $this;	
	}

	/**
	 * @param $db
	 */
	function setupPlugin(&$db)
	{	
		// used so frontend can pass db and settings
		$this->db = $db;	
	}

	/**
	 * @param $id
	 */
	function delete($id)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."testimonials WHERE id='".intval($id)."'");
	}

	/**
	 * @param $start
	 * @param $howMany
	 * @param string $mode
	 * @return mixed
	 */
	function getTestimonials($start, $howMany, $mode='')
	{	
		$start = isset($start) ? intval($start) : 0;
		$howMany = isset($howMany) ? intval($howMany) : 10;
		$whereClause = "";
		if ($mode != '') $whereClause = "WHERE `status` = '".$this->db->escape($mode)."'";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ".DB_PREFIX."testimonials ".$whereClause." ORDER BY `date_created` DESC LIMIT ".$start.",".$howMany;
		$this->db->query($sql);
		$testimonials = $this->db->getRecords();
		$this->db->query("SELECT FOUND_ROWS() as 'howmany'");
		$this->db->moveNext();
		$this->totalTestimonials = $this->db->col['howmany'];
		return $testimonials;
		
	}

	/**
	 * @param $id
	 * @return bool
	 */
	function getTestimonial($id)
	{		
		$sql = "SELECT * FROM ".DB_PREFIX."testimonials WHERE `id` = ".intval($id)." LIMIT 1";
		$this->db->query($sql);
		$results = $this->db->getRecords();
		
		if (!empty($results)) return $results[0]; else return false;
	}

	/**
	 * @return mixed
	 */
	function getRating()
	{	
		$sql = "SELECT AVG(rating) AS `rating` FROM ".DB_PREFIX."testimonials WHERE status = 'approved' AND rating > 0";
		$this->db->query($sql);
		$this->db->moveNext();
		$rating = $this->db->col['rating'];
		return $rating;
	}

	/**
	 * @param $data
	 * @param bool $isAdminArea
	 * @throws Exception
	 */
	function saveTestimonial($data, $isAdminArea = false)
	{
		global $settings;
		$this->db->reset();
		$this->db->assignStr('name',$data['name']);
		$this->db->assignStr('company',$data['company']);
		$this->db->assign('rating',intval($data['rating']));
		$this->db->assignStr('testimonial',$data['testimonial']);
		$this->db->assignStr('status',$data['status']);
		
		if ((isset($data['id']) && is_numeric($data["id"]) && $data["id"] > 0) && $isAdminArea)
		{
			// update
			$this->db->update(DB_PREFIX.'testimonials',"WHERE `id` = ".intval($data['id']));
		}
		else
		{
			// insert
			$this->db->assign("date_created", "NOW()");
			$this->db->insert(DB_PREFIX.'testimonials');
			if ($settings['testimonials_Email_Notify'] != '')
			{
				// ensure send_mail is defined
				$message = 'New testimonial received on '.$settings['CompanyName'].' website. Login to the admin to view it.';
				Notifications::sendMail(
					$settings['testimonials_Email_Notify'],
					'New Testimonial for '.$settings['CompanyName'],
					$message,$settings["GlobalNotificationEmail"]
				);
			}
		}
	}

	/**
	 * @param $key
	 * @param $value
	 * @return bool
	 */
	function setSetting($key,$value)
	{	
		$this->db->reset();
		$this->db->assignStr('value',$value);
		$this->db->update(DB_PREFIX.'settings',"WHERE `name` = '".$this->db->escape($key)."'");
		return true;
	}

	/**
	 * @param $id
	 * @param $status
	 * @return bool
	 */
	function setStatus($id,$status)
	{	
		$this->db->reset();
		$this->db->assignStr('status',$status);
		$this->db->update(DB_PREFIX.'testimonials',"WHERE `id` = ".intval($id));
		return true;
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	function getImage($name)
	{	
		$possibleImages = array(
			$this->imageDirectory.$name.'.jpg',
			$this->imageDirectory.$name.'.png',
			$this->imageDirectory.$name.'.gif',
		);
		foreach ($possibleImages as $possibleImage)
		{
			if (file_exists($possibleImage)) return $possibleImage;
		}
	}

	/**
	 * TODO: reuse FileUploader service
	 * @param $file
	 * @param $name
	 * @return bool|string
	 */
	private function uploadImage($file,$name)
	{
		$imageTypes = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);

    	if (!empty($file["tmp_name"]))
    	{
			$image_data = @getImageSize($file["tmp_name"]);
			if (in_array($image_data[2], $imageTypes))
			{
				$destination = $this->imageDirectory.$name;
				@unlink($destination."jpg");
				@unlink($destination."png");
				@unlink($destination."gif");
				//add new file extension
				switch ($image_data[2])
				{
					case IMAGETYPE_GIF: $destination.=".gif"; break;
					case IMAGETYPE_JPEG: $destination.=".jpg"; break;
					case IMAGETYPE_PNG: $destination.=".png"; break;
				}
				if (@copy($file["tmp_name"], $destination))
				{
					// image uploaded
					return $destination;
				}
				else
				{
					// unable to upload
					return false;
				}
			}
			else
			{
        		//incorrect file extension
				return false;
			}
		}
		else
		{
     		// not uploaded image?
			return false;
		}
	}
}

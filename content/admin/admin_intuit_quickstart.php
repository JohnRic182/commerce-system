<?php

/**
 * Updates intuit quickstart settings
 */
if (!defined("ENV")) exit();

require_once 'content/vendors/intuit/oauth/config.php';

if (!isset($intuitSync)) $intuitSync = intuit_Services_QBSync::create();

$intuitQuickStart = new Admin_Intuit_QuickStart($db, $settings, Ddm_AdminView::getInstance(), $intuitSync);

print json_encode($intuitQuickStart->action_save());
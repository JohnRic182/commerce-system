/**
 * SandBox jQuery Plugin
 *
 * @package ddm_jquery
 **/

(function(jQuery) {
	
	jQuery.fn.dashBoardDock = function(options){
		var dock = $(this);
		
		$('body .container:last').css('margin-bottom', dock.css('height'));
		
		// default settings
		var settings = {
			'list': null,
			'noResults': null
		}
		
		// extend default settings
		settings = $.extend(settings, options);
		
		// add functionality to menus
		$('#sandbox div.dock ul.menu li').click(function(){
			
			var name = $(this).attr('name');
			
			// hide any menu items that are visible
			$('#sandbox div.ribbon:visible').filter(':not(".'+name+'")').hide('slide', { direction: 'down' });
			
			// toggles the ribbon of the corresponding menu item
			$('#sandbox div.ribbon.'+name).toggle('slide', { direction: 'down' }, 400, function(){
				$('body .container:last').css('margin-bottom', dock.height() + 'px');
			});
		});
	}
	
})(jQuery);

$(document).ready(function(){
	$('#sandbox').dashBoardDock();
});
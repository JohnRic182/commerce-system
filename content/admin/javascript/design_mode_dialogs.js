var iframeForm = null;

$(document).ready(function(){
	/**********************************************
	 ** LOADER 
	 **********************************************/
	$("#design-loader").dialog({
		'modal'			: true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : false,
		'minHeight'		: 110,
		'maxHeight'		: 110,
		'width'         : 420,
		'height'        : 110,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Loading data',
		'dialogClass'	: 'design-mode design-dialog-no-titlebar',
		'closeOnEscape'	: false,
		'open': function(ev, ui)
		{
			DesignMode().correctDialogPosition("#design-loader");
			$(this).parent().children().children('.ui-dialog-titlebar-close').hide();
		}
	});
	
	/**********************************************
	 ** SAVE DIALOG
	 **********************************************/
	$("#design-dialog-save-changes").dialog({
		'modal'			: true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 500,
		'height'        : 399,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Save layout changes',
		'dialogClass'	: 'design-mode',
		'buttons':
		{
			'Cancel': function()
			{
				$(this).dialog("close");
			},
			'Save' : function()
			{
				if (DesignMode().saveChanges())
				{
					$(this).dialog("close");
				}
			}
		},
		'open' : function(){
			
			DesignMode().correctDialogPosition("#design-dialog-save-changes");
			$("#design-dialog-save-changes input[name='saveLayoutScope']:selected").removeAttr("checked");
			$("#design-dialog-save-changes #design-set-none").attr("checked", "checked");
			$("#design-set-selected-options").hide();
			
			$("#design-dialog-save-changes input[name='saveLayoutScope']").each(function(){
				$(this).click(function(){
					if ($(this).val() == 'selected')
					{
						$("#design-set-selected-options").slideDown("fast");
					}
					else
					{
						$("#design-set-selected-options").slideUp("fast");
					}
				});
			});	
		}
	});
	
	/**********************************************
	 *  IMAGES EDITOR
	 **********************************************/
	$("#design-dialog-images-editor").dialog({
		'modal'			: true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 850,
		'height'        : 590,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Site Images',
		'dialogClass'	: 'design-mode',
		'buttons':
		{
			'Close' : function()
			{
				$('#design-dialog-images-editor').dialog("close");
			},
			'Reset': function()
			{
				var frm = $("#design-dialog-images-editor-content").find("form[name='frmImagesUpdate']")[0].reset();
				$("#design-dialog-images-editor-content").find("input[type='file']").each(function(){
					$(this).addClass('file-not-selected').removeClass('file-selected').removeAttr('disabled');
				});
			},
			'Save': function()
			{
				//disable all unused file inputs
				$("#design-dialog-images-editor-content").find("input[type='file'].file-not-selected").attr('disabled', 1);
				var frm = $('#design-dialog-images-editor-content').find('form[name=frmImagesUpdate]');
				var a = $(frm).attr('action');

				if ($(frm).length > 0)
				{
					if ($.browser.msie)
					{
						if (confirm('Do you really want to upload images?'))
						{
							DesignMode().showSpinner('Saving data...');
							$(frm).submit();
						}
					}
					else
					{
						DesignMode().showSpinner('Saving data...');
						$(frm).submit();
					}
				}

				var ic = $('#design-dialog-images-editor').data("iframeCounter") + 1;
				$("#design-dialog-images-editor").data("iframeCounter", ic);
			}
		},
		'open' : function()
		{
			DesignMode().correctDialogPosition("#design-dialog-images-editor");
			
			$("#design-dialog-images-editor").data("iframeCounter", 0);

			
			$("#design-dialog-images-editor-content").data("loads", 0);

			//check how many files already changed (uploaded)
			$("#design-dialog-images-editor-content").find("input[type='file']").each(function(){
				$(this).addClass('file-not-selected');
				$(this).change(function(){
					$(this).removeClass('file-not-selected').addClass('file-selected');
					var u = $("#design-dialog-images-editor-content").find("input[type='file'].file-selected").length;
					//if there more as allowed changes - disable the rest
					if (u >= max_file_uploads)
					{
						$("#design-dialog-images-editor-content").find("input[type='file'].file-not-selected").attr('disabled', 1);
					}
				});
			});
			var l = $("#design-dialog-images-editor-content").data("loads");
			$("#design-dialog-images-editor-content").data("loads", l+1);

			$('#design-dialog-images-editor-iframe').unbind('load').load(function(){
				DesignMode().hideSpinner();
				designMode_loadImagesEditor();
			});
		},
		'close' : function()
		{
			if ($("#design-dialog-images-editor").data("iframeCounter") > 0 || $("#design-dialog-images-editor-content").data("loads") > 1)
			{
				if (confirm("Some of changes you just made require page refresh.\nDo you want to do it now?"))
				{
					DesignMode().showSpinner('Refreshing...');
					document.location.reload();
				}
			}
		}
	});
	
	/**
	 * Layout editor(s)
	 */
	$("#design-dialog-layout-editor").dialog({
		'modal'			: true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 850,
		'height'        : 590,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Layout Editor',
		'dialogClass'	: 'design-mode',
		'buttons':
		{
			'Close' : function()
			{
				$(this).dialog("close");
			},
			'Save' : function()
			{
				DesignMode().layoutEditorSave();
			}
		},
		'open' : function(){
			DesignMode().correctDialogPosition("#design-dialog-layout-editor");
			$("#design-dialog-layout-editor").data("changes-made", false);
			$("#design-dialog-layout-editor .design-dialog-content-wrap").hide();
			$("#design-dialog-layout-editor #design-dialog-layout-editor-" + $("#design-dialog-layout-editor").data("type")).show();
			var s = $("#design-dialog-layout-editor").data("settings");
			
			/**
			 * Catalog
			 */

			$("#design-dialog-layout-editor #CatalogDefaultSortOrder option").removeAttr('selected');
			$("#design-dialog-layout-editor #CatalogDefaultSortOrder option[value='" + s["CatalogDefaultSortOrder"]+ "']").attr('selected', 'selected');
			$("#design-dialog-layout-editor #CatalogItemsOnPage").val(s["CatalogItemsOnPage"]);

			/**
			 * Product
			 */
			$("#design-dialog-layout-editor #RecommendedProductsCount").val(s["RecommendedProductsCount"]);
			$("#design-dialog-layout-editor #CatalogEmailToFriend option").removeAttr("selected");
			$("#design-dialog-layout-editor #CatalogEmailToFriend option[value='" + s["CatalogEmailToFriend"]+ "']").attr('selected', 'selected');
		}
	});
	
	/**********************************************
	 * Fonts Editor
	 **********************************************/
	
	function fontsEditorLoad()
	{
		DesignMode().showSpinner("Reading styles");
		var map = $("#design-dialog-fonts-editor").data("map");
		var html = '<div class="col-wrap clearfix">';
		
		//First step: generate HTML
		jQuery.each(map.fonts, function(groupKey, groupData){
			html = html + 
				'<div class="gap-right gap-bottom"><fieldset class="gap-right clearfix"><legend style="font-size: 1.2em; font-weight: bold; padding: 0 5px;">' + map.fonts[groupKey].group_name + '</legend>' +
				'<table width="100%" cellspacing="0" cellpadding="4" border="0">'+
				'<tr>' +
					'<td colspan="3" style="padding: 4px;">&nbsp;</td>' +
					'<td width="5%" style="width: 5%; text-align: center; padding: 4px; font-weight: bold; padding: 4px;">B</td>' +
					'<td width="5%" style="width: 5%; text-align: center; padding: 4px; font-style: italic;">I</td>' +
					'<td width="5%" style="width: 5%; text-align: center; padding: 4px; text-decoration: underline;">U</td>' +
				'</td></tr>';
			
			jQuery.each(groupData.elements, function(elementKey, elementData){
				html = html + 
					'<tr id="font-' + groupKey + '-' + elementKey + '">' +
						'<td style="width: 25%; padding: 4px;">' + elementData.caption + '</td>' +
						'<td style="width: 40%; padding: 4px;"><div style="position:relative;">' + 
							'<select style="width:240px;" class="font-editor-input" id="font-family-' + groupKey + '-' + elementKey + '">' +
								'<option value="inherit">Inherit</option>' +
								'<option value="Arial,Arial,Helvetica,sans-serif">Arial,Arial,Helvetica,sans-serif</option>' +
								'<option value="Arial Black,Arial Black,Gadget,sans-serif">Arial Black,Arial Black,Gadget,sans-serif</option>' +
								'<option value="Comic Sans MS,Comic Sans MS,cursive">Comic Sans MS,Comic Sans MS,cursive</option>' +
								'<option value="Courier New,Courier New,Courier,monospace">Courier New,Courier New,Courier,monospace</option>' +  
								'<option value="Georgia,Georgia,serif">Georgia,Georgia,serif</option>' +
								'<option value="Impact,Charcoal,sans-serif">Impact,Charcoal,sans-serif</option>' +
								'<option value="Lucida Console,Monaco,monospace">Lucida Console,Monaco,monospace</option>' +
								'<option value="Lucida Sans Unicode,Lucida Grande,sans-serif">Lucida Sans Unicode,Lucida Grande,sans-serif</option>' +
								'<option value="Palatino Linotype,Book Antiqua,Palatino,serif">Palatino Linotype,Book Antiqua,Palatino,serif</option>' + 
								'<option value="Tahoma,Geneva,sans-serif">Tahoma,Geneva,sans-serif</option>' +
								'<option value="Times New Roman,Times,serif">Times New Roman,Times,serif</option>' +
								'<option value="Trebuchet MS,Helvetica,sans-serif">Trebuchet MS,Helvetica,sans-serif</option>' + 
								'<option value="Verdana,Geneva,sans-serif">Verdana,Geneva,sans-serif</option>' +
							'</select>' +
						'</div></td>' +
						'<td style="width: 20%; padding: 4px;"><div style="position:relative;">' +
							'<select style="width:60px;" class="font-editor-select" id="font-size-' + groupKey + '-' + elementKey + '">' +
								'<option value=""></option>' +
								'<option value="6px">6px</option>' +
								'<option value="7px">7px</option>' +
								'<option value="8px">8px</option>' +
								'<option value="9px">9px</option>' +
								'<option value="10px">10px</option>' +
								'<option value="11px">11px</option>' +
								'<option value="12px">12px</option>' +
								'<option value="13px">13px</option>' +
								'<option value="14px">14px</option>' +
								'<option value="15px">15px</option>' +
								'<option value="16px">16px</option>' +
								'<option value="17px">17px</option>' +
								'<option value="18px">18px</option>' +
								'<option value="19px">19px</option>' +
								'<option value="20px">20px</option>' +
								'<option value="21px">21px</option>' +
								'<option value="22px">22px</option>' +
								'<option value="23px">23px</option>' +
								'<option value="24px">24px</option>' +
							'</select>' +
						'</div></td>' +
						'<td style="padding: 4px; text-align: center;">' +
							'<input type="checkbox" id="font-weight-' + groupKey + '-' + elementKey + '" title="Bold"/>' +
						'</td>' +
						'<td style="padding: 4px; text-align: center;">' +
							'<input type="checkbox" id="font-style-' + groupKey + '-' + elementKey + '" title="Italic"/>' +
						'</td>' +
						'<td style="padding: 4px; text-align: center;">' +
							'<input type="checkbox" id="text-decoration-' + groupKey + '-' + elementKey + '" title="Underline"/>' +
						'</td>' +
					'</tr>';
			});
			
			html = html + 
				'</table></fieldset></div>';
		});
		
		$("#design-dialog-fonts-editor-fields").html(html);
		
		//Second step: set values for select
		jQuery.each(map.fonts, function(groupKey, groupData){
			jQuery.each(groupData.elements, function(elementKey, elementData){
				var el = $("#page-preview").find(elementData.selector);
				$('#font-family-' + groupKey + '-' + elementKey).editableSelect();
				$('#font-family-' + groupKey + '-' + elementKey).editableSelectSetValue(el.css('font-family'));
				$('#font-size-' + groupKey + '-' + elementKey).editableSelect();
				$('#font-size-' + groupKey + '-' + elementKey).editableSelectSetValue(el.css('font-size'));
				if (el.css('font-weight') == 'bold') $('#font-weight-' + groupKey + '-' + elementKey).attr('checked', 'checked');
				if (el.css('font-style') == 'italic') $('#font-style-' + groupKey + '-' + elementKey).attr('checked', 'checked');
				if (el.css('text-decoration') == 'underline') $('#text-decoration-' + groupKey + '-' + elementKey).attr('checked', 'checked');
			});
		});
		
		$("#design-dialog-fonts-editor-fields input[type='checkbox'],#design-dialog-fonts-editor-fields input[type='hidden']").change(function(){ $(this).addClass("changed"); });
		
		DesignMode().hideSpinner();
	}
	
	$("#design-dialog-fonts-editor").dialog({
		'modal'			: true,
		'resizable'     : true,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 850,
		'height'        : 550,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Fonts Editor',
		'dialogClass'	: 'design-mode',
		'buttons':
		{
			'Close' : function()
			{
				$(this).dialog("close");
			},
			'Save' : function()
			{
				DesignMode().fontsEditorSave();
			}
		},
		'open' : function(){
			DesignMode().showSpinner("Loading data");
			$("#design-dialog-fonts-editor").data("changes-made", false);
			$("#design-dialog-fonts-editor").data("changesCounter", 0);
			DesignMode().correctDialogPosition("#design-dialog-fonts-editor");
			fontsEditorLoad();
			DesignMode().hideSpinner();
		}
	});
	
	function pausecomp(millis)
	{
		var date = new Date();
		var curDate = null;

		do { curDate = new Date(); } 
		while(curDate-date < millis);
	} 

	/**********************************************
	 * Colors Editor
	 **********************************************/
	function colorsEditorLoad()
	{
		var map = $("#design-dialog-colors-editor").data("map");
		var html = '<div class="col-wrap clearfix">';
		jQuery.each(map.colors, function(groupKey, groupData)
		{
			html = html + 
				'<div class="col-50"><div class="gap-right"><fieldset class="gap-right gap-bottom"><legend>' + map.colors[groupKey].group_name + '</legend>';
		
			jQuery.each(groupData.elements, function(elementKey, elementData)
			{
				var el = $("#page-preview " + elementData.selector);

				var cl = rgbToHex(el.css(elementData.type == "cl" ? 'color' : 'background-color'));
				if (cl == 'transparent' || cl == 'rgba(0, 0, 0, 0)')
				{
					cl = '';
				}
				else if (cl == undefined)
				{
					cl = "";
				}
				if (elementData.caption == 'place-holder')
				{
					html = html + 
						'<div class="col-wrap clearfix" style="padding-top:4px;padding-bottom:4px;min-height:22px;">' +
							'<div class="col-50 gap-left gap-top">&nbsp;</div>' +
							'<div class="col-5">&nbsp;</div>' +
							'<div class="col-40">&nbsp;</div>' +
						'</div>';
				}
				else
				{
					html = html + 
						'<div class="col-wrap clearfix" style="padding-top:4px;padding-bottom:4px;min-height:22px;">' +
							'<div class="col-50 gap-left" style="padding-top:5px;">' + elementData.caption + '</div>' +
							'<div class="col-5" style="padding-top:4px;"><div id="color-' + groupKey + '-' + elementKey + '-preview" style="background-color:' + cl + ';margin:2px;width:10px;height:10px;border:1px solid black;"></div></div>' +
							'<div class="col-40"><input style="width:100px;" class="color-editor-input" id="color-' + groupKey + '-' + elementKey + '" type="text" value="' + cl + '"></div>' +
						'</div>';
				}
			});
			
			html = html + 
				'</fieldset></div></div>';
		});
		$("#design-dialog-colors-editor-fields").html(html);
		
		$('#design-dialog-colors-editor .color-editor-input').each(function(){
			var input = this;
			$(input).focus(function(){
				var input = this;
				$(input).ColorPicker({
					color: '#0000ff',
					onSubmit: function(hsb, hex, rgb, el) {$(el).val('#'+hex);$(el).ColorPickerHide();},
					onBeforeShow: function() {$(input).ColorPickerSetColor(input.value);return false;},
					onChange: function (hsb, hex, rgb) {
						$(input).val('#'+hex).addClass('changed');
						$("#" + $(input).attr("id") + '-preview').css('background-color', '#'+hex);
						$("#design-dialog-colors-editor").data("changes-made", true);
					}
				})
				.change(function(){
					$(this).ColorPickerSetColor(this.value);
					$("#" + $(this).attr("id") + '-preview').css('background-color', this.value);
					$("#design-dialog-colors-editor").data("changes-made", true);
				})
				.blur(function(){
					$(this).ColorPickerSetColor(this.value);
					$("#" + $(this).attr("id") + '-preview').css('background-color', this.value);
					$("#design-dialog-colors-editor").data("changes-made", true);
					$(this).addClass('changed');
				});
			});
		});
	}
	
	$("#design-dialog-colors-editor").dialog({
		'modal'			: true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 850,
		'height'        : 550,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Colors Editor',
		'dialogClass'	: 'design-mode',
		'buttons':
		{
			'Close' : function()
			{
				$(this).dialog("close");
			},
			'Save' : function()
			{
				DesignMode().colorsEditorSave();
			}
		},
		'open' : function(){
			DesignMode().correctDialogPosition("#design-dialog-colors-editor");
			DesignMode().showSpinner("Loading data");
			$("#design-dialog-colors-editor").data("changes-made", false);
			$("#design-dialog-colors-editor").data("changesCounter", 0);
			colorsEditorLoad();
			DesignMode().hideSpinner();
		}
	});
	
	/**********************************************
	 ** STYLES EDITOR 
	 **********************************************/
	$("#design-dialog-styles-editor").dialog({
		'modal'			: true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 890,
		'height'        : 650,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'			: 'Styles Editor',
		'dialogClass'	: 'design-mode',
		'buttons':
		{
			'Close': function()
			{
				if ($("#design-dialog-styles-editor").data("changes-made"))
				{
					if (confirm("You have unsaved changes. Do you want to save them?"))
					{
						DesignMode().showSpinner("Saving changes");
						DesignMode().stylesEditorSetVisualEditors();
						$.post(
							//url
							site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
							//data
							{
								'action'			: 'saveElementStyles',
								'parent-class'		: $("#design-dialog-styles-editor-map li.selected-element").data('parent-class'),
								'parent-selector'	: $("#design-dialog-styles-editor-map li.selected-element").data('parent-selector'),
								'class'				: $("#design-dialog-styles-editor-map li.selected-element").data('class'),
								'selector'			: $("#design-dialog-styles-editor-map li.selected-element").data('selector'),
								'css'				: DesignMode().stylesEditorRenderCss()
							},
							//after post
							function(response){
								//parse response
								var response = JSON.parse(response);
								if (response.status)
								{
									$('#css-designmode').attr('href', response.cssFileUrl);
								}
								DesignMode().hideSpinner();
								$("#design-dialog-styles-editor").data("changes-made", false);
								$("#design-dialog-styles-editor").dialog("close");
							}
						);
					}
					else
					{
						$(this).dialog("close");
					}
				}
				else
				{
					$(this).dialog("close");
				}
				if ($("#design-dialog-styles-editor").data("changes-made-require-refresh"))
				{
					if (confirm("Some of changes you just made require page refresh.\nDo you want to do it now?"))
					{
						DesignMode().showSpinner('Refreshing...');
						document.location.reload();
					}
				}
				$("#design-dialog-styles-editor").data("changes-made-require-refresh", false);
			},
			'Save' : function()
			{
				if ($("#design-dialog-styles-editor").data("changes-made"))
				{
					DesignMode().showSpinner("Saving data");
					DesignMode().stylesEditorSetVisualEditors();
					$.post(
						//url
						site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
						//data
						{
							'action'			: 'saveElementStyles',
							'parent-class'		: $("#design-dialog-styles-editor-map li.selected-element").data('parent-class'),
							'parent-selector'	: $("#design-dialog-styles-editor-map li.selected-element").data('parent-selector'),
							'class'				: $("#design-dialog-styles-editor-map li.selected-element").data('class'),
							'selector'			: $("#design-dialog-styles-editor-map li.selected-element").data('selector'),
							'css'				: DesignMode().stylesEditorRenderCss()
						},
						//after post
						function(response){
							//parse response
							var response = JSON.parse(response);
							if (response.status)
							{
								$('#css-designmode').attr('href', response.cssFileUrl);
							}
							$("#design-dialog-styles-editor").data("changes-made", false);
							DesignMode().hideSpinner();
						}
					);
				}
			}
		},
		'open' : function(){
			DesignMode().correctDialogPosition("#design-dialog-styles-editor");
			if ($(this).data('init') == null)
			{
				//init tabs
				DesignMode().fixTabs("#design-dialog-styles-editor-tabs-ul a");
				$("#design-dialog-styles-editor-tabs")
					.tabs()
					.bind('activate', function(event, ui){
						switch ($(ui.panel).attr("id"))
						{
							case "design-dialog-styles-editor-tab-visual" :
							{
								DesignMode().stylesEditorCssPreview();
								DesignMode().stylesEditorSetVisualEditors();
								break;
							}
							case "design-dialog-styles-editor-tab-css" :
							{
								$("#design-dialog-styles-editor-css").val(DesignMode().stylesEditorRenderCss());
								break;
							}
						}
					}
				);
				//init color pickers
				$('#design-dialog-styles-editor-text-color').ColorPicker({
					color: '#0000ff',
					onSubmit: function(hsb, hex, rgb, el) {$(el).val('#'+hex);$(el).ColorPickerHide();},
					onBeforeShow: function() {$(this).ColorPickerSetColor(this.value);return false;},
					onChange: function (hsb, hex, rgb) {
						$('#design-dialog-styles-editor-text-color').val('#'+hex);
						DesignMode().stylesEditorVisualPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					;}
				})
				.keyup(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				})
				.change(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
					$("#design-dialog-styles-editor").data("changes-made", true);
				});
				
				$('#design-dialog-styles-editor-text-color-a-link').ColorPicker({
					color: '#0000ff',
					onSubmit: function(hsb, hex, rgb, el) {$(el).val('#'+hex);$(el).ColorPickerHide();},
					onBeforeShow: function() {$(this).ColorPickerSetColor(this.value);return false;},
					onChange: function (hsb, hex, rgb) {
						$('#design-dialog-styles-editor-text-color-a-link').val('#'+hex);
						DesignMode().stylesEditorVisualPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					;}
				})
				.keyup(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				})
				.change(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				});
				
				$('#design-dialog-styles-editor-text-color-a-hover').ColorPicker({
					color: '#0000ff',
					onSubmit: function(hsb, hex, rgb, el) {$(el).val('#'+hex);$(el).ColorPickerHide();},
					onBeforeShow: function() {$(this).ColorPickerSetColor(this.value);return false;},
					onChange: function (hsb, hex, rgb) {
						$('#design-dialog-styles-editor-text-color-a-hover').val('#'+hex);
						DesignMode().stylesEditorVisualPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					;}
				})
				.keyup(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				})
				.change(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				});
				
				$('#design-dialog-styles-editor-text-color-a-active').ColorPicker({
					color: '#0000ff',
					onSubmit: function(hsb, hex, rgb, el) {$(el).val('#'+hex);$(el).ColorPickerHide();},
					onBeforeShow: function() {$(this).ColorPickerSetColor(this.value);return false;},
					onChange: function (hsb, hex, rgb) {
						$('#design-dialog-styles-editor-text-color-a-active').val('#'+hex);
						DesignMode().stylesEditorVisualPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					;}
				})
				.keyup(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				})
				.change(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				});
				
				$('#design-dialog-styles-editor-text-color-a-visited').ColorPicker({
					color: '#0000ff',
					onSubmit: function(hsb, hex, rgb, el) {$(el).val('#'+hex);$(el).ColorPickerHide();},
					onBeforeShow: function() {$(this).ColorPickerSetColor(this.value);return false;},
					onChange: function (hsb, hex, rgb) {
						$('#design-dialog-styles-editor-text-color-a-visited').val('#'+hex);
						DesignMode().stylesEditorVisualPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					;}
				})
				.keyup(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				})
				.change(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
				});
				
				$('#design-dialog-styles-editor-background-color').ColorPicker({
					color: '#0000ff',
					onSubmit: function(hsb, hex, rgb, el) {$(el).val('#'+hex);$(el).ColorPickerHide();},
					onBeforeShow: function() {$(this).ColorPickerSetColor(this.value);return false;},
					onChange: function (hsb, hex, rgb) {
						$('#design-dialog-styles-editor-background-color').val('#'+hex);
						DesignMode().stylesEditorVisualPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					}
				})
				.keyup(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
					$("#design-dialog-styles-editor").data("changes-made", true);
				})
				.change(function(){
					$(this).ColorPickerSetColor(this.value);
					DesignMode().stylesEditorVisualPreview();
					$("#design-dialog-styles-editor").data("changes-made", true);
				});
				
				//init selects
				$("#design-dialog-styles-editor-font-family").editableSelect();
				$("#design-dialog-styles-editor-font-size").editableSelect();
				$("#design-dialog-styles-editor-font-weight").editableSelect();
				$("#design-dialog-styles-editor-font-style").editableSelect();
				$("#design-dialog-styles-editor-text-decoration").editableSelect();
				$("#design-dialog-styles-editor-text-align").editableSelect();
				
				$("#design-dialog-styles-editor-background-attachment").editableSelect();
				$("#design-dialog-styles-editor-background-position").editableSelect();
				$("#design-dialog-styles-editor-background-repeat").editableSelect();
				
				$(
					"#design-dialog-styles-editor-font-family," +
					"#design-dialog-styles-editor-font-size," +
					"#design-dialog-styles-editor-font-weight," +
					"#design-dialog-styles-editor-font-style," +
					"#design-dialog-styles-editor-text-decoration," +
					"#design-dialog-styles-editor-text-align," +

					"#design-dialog-styles-editor-background-attachment," +
					"#design-dialog-styles-editor-background-repeat," +
					"#design-dialog-styles-editor-background-position"
				).change(function(){
					DesignMode().stylesEditorVisualPreview();
					$("#design-dialog-styles-editor").data("changes-made", true);
				});
				
				//background checkbox
				$("#design-dialog-styles-editor-background-image-exists").click(function(){
					DesignMode().stylesEditorVisualPreview();
					$("#design-dialog-styles-editor").data("changes-made", true);
				});
				
				/**
				 * Prepare bg image upload
				 */
				$("#design-dialog-styles-editor-background-image-uploadify").uploadify({
					'uploader': global_https_url + '/content/vendors/jquery/uploadify/uploadify.swf',
					'script': site_ajax_url_admin + '/admin.php',
					'cancelImg': global_https_url + '/content/vendors/jquery/uploadify/cancel.png',
					'method':'POST',
					'scriptAccess':'sameDomain',
					'multi':false,
					'auto':true,
					'scriptAccess':'always',
					'buttonImg': global_https_url + '/content/vendors/jquery/uploadify/browse.png',
					'width':'100',
					'height':'22',
					'wmode':'transparent',
					'fileDesc':'Accepted image types are JPEG, PNG and GIF',
					'fileExt':'*.jpg;*.jpeg;*.png;*.gif',
					'sizeLimit':2097152,
					'simUploadLimit':1,
					'folder':'none',
					onSelect : function(event, queueID, fileObj)
					{
						var _class = $("#design-dialog-styles-editor-map li.selected-element").data('class');
						if ($.trim(_class) == "") _class = "_none_";
						
						var selector = $("#design-dialog-styles-editor-map li.selected-element").data('selector');
						if ($.trim(selector) == '') selector = ' ';
						
						$("#design-dialog-styles-editor-background-image-uploadify").uploadifySettings(
							'scriptData',
							{	
								'p'					: 'design_mode',
								'triggerHelper'		: 'true',
								'action'			: 'uploadBackgroung',
								'dmsid'				: dmsid,
								'parent-class'		: $("#design-dialog-styles-editor-map li.selected-element").data('parent-class'),
								'parent-selector'	: $("#design-dialog-styles-editor-map li.selected-element").data('parent-selector'),
								'class'				: _class,
								'selector'			: selector//$("#design-dialog-styles-editor-map li.selected-element").data('selector')
							}
						);
					},
					onError: function(event, queueID, fileObj, errorObj)
					{
						alert(errorObj.type + " error occured:\n" + errorObj.info);
					},
					onComplete: function(event, queueID, fileObj, response, data)
					{
						response = JSON.parse(response);
						$("#design-dialog-styles-editor-background-image").val("url('" + global_https_url + '/' + response.cacheUrl + "?" + Math.floor(Math.random()*10000) + "')");
						DesignMode().stylesEditorVisualPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
						$("#design-dialog-styles-editor").data("changes-made-require-refresh", true);
					}
				});
				
				/**
				 * Prepare image upload
				 */
				$("#design-dialog-styles-editor-image-uploadify").uploadify({
					'uploader': global_https_url + '/content/vendors/jquery/uploadify/uploadify.swf',
					'script': site_ajax_url_admin + '/admin.php',
					'cancelImg': global_https_url + '/content/vendors/jquery/uploadify/cancel.png',
					'method':'POST',
					'scriptAccess':'sameDomain',
					'multi':false,
					'auto':true,
					'scriptAccess':'always',
					'buttonImg': global_https_url + '/content/vendors/jquery/uploadify/browse.png',
					'width':'100',
					'height':'22',
					'wmode':'transparent',
					'fileDesc':'Accepted image types are JPEG, PNG and GIF',
					'fileExt':'*.jpg;*.jpeg;*.png;*.gif',
					'sizeLimit':2097152,
					'simUploadLimit':1,
					'folder':'none',
					onSelect : function(event, queueID, fileObj)
					{
						$("#design-dialog-styles-editor-image-uploadify").uploadifySettings(
							'scriptData',
							{	
								'p'					: 'design_mode',
								'triggerHelper'		: 'true',
								'action'			: 'uploadImage',
								'dmsid'				: dmsid,
								'parent-class'		: $("#design-dialog-styles-editor-map li.selected-element").data('parent-class'),
								'parent-selector'	: $("#design-dialog-styles-editor-map li.selected-element").data('parent-selector'),
								'class'				: $("#design-dialog-styles-editor-map li.selected-element").data('class'),
								'selector'			: $("#design-dialog-styles-editor-map li.selected-element").data('selector')
							}
						);
					},
					onError: function(event, queueID, fileObj, errorObj)
					{
						alert(errorObj.type + " error occured:\n" + errorObj.info);
					},
					onComplete: function(event, queueID, fileObj, response, data)
					{
						response = JSON.parse(response);
						$($("#design-dialog-styles-editor").data("preview-selector"))
							.attr("src", response.imgUrl + "?" + Math.floor(Math.random()*10000))
							.width(response.width)
							.height(response.height);
						$($("#design-dialog-styles-editor-map li.selected-element").data('selector')).attr("src", response.imgUrl + "?" + Math.floor(Math.random()*10000));
						DesignMode().stylesEditorSelectElement($("#design-dialog-styles-editor-map li.selected-element"));
						$("#design-dialog-styles-editor").data("changes-made-require-refresh", true);
					}
				});
				
				//init css editor
				$("#design-dialog-styles-editor-css")
					.change(function(){
						DesignMode().stylesEditorCssPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					})
					.keypress(function(){
						DesignMode().stylesEditorCssPreview();
						$("#design-dialog-styles-editor").data("changes-made", true);
					})
				;
			}
			$(this).data('init', true);
			
			//show notice
			$("#design-dialog-styles-editor-tabs").hide();
			$("#design-dialog-styles-editor-start-text").show();
				
			$(this).data("changes-made", false);
			$(this).data("changes-made-require-refresh", false);
		}
	});
	
	/**********************************************
	 ** HTML EDITOR
	 **********************************************/
	$('#design-dialog-html-editor').dialog({
		'modal'         : true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 850,
		'height'        : 550,
		'stack'         : false,
		'overlay'       : 0.8,
		'dialogClass'	: 'design-mode',
		'title'         : 'Edit Element Source',
		'buttons': { 
			'Close': function() {
				$(this).dialog("close");
			},
			'Restore' : function () {

				if ($(this).data("type") == "custom")
				{
					alert("Current element cannot be restored because it is custom");
					return;
				}

				$('#design-dialog-html-editor').dialog("close");

				switch ($(this).data("location"))
				{
					case "base": {
						alert("You are editing original version of current template file.\nIf you don't want to save your changes now, please close editor window");
						break;
					}
					case "skin": {
						alert("You are editing a themed version of current template file.\nIf you don't want to save your changes now, please close editor window");
					}
					case "custom": {
						if (confirm("Do you really want to restore current template file to its original state?\nPlease note: changes you will made now will be unrevertable"))
						{
							DesignMode().showSpinner("Restoring data");
							var currentLocation = window.location.toString();
							$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
								'location' : currentLocation.toString(),
								'action': 'restoreElementHtml',
								'class': $('#design-dialog-html-editor').data('designElementClass')
							}, function(response){
								DesignMode().hideSpinner();
								if ($.trim(response) != "")
								{
									response = JSON.parse(response);
									
									if (response.status)
									{
										var designElementClass = $('#design-dialog-html-editor').data("designElementClass");
										
										//check for one page checkout / and inside of #main container
										if ($("body").attr("id") == "page-one_page_checkout" && $("." + designElementClass).parents("#main").length > 0)
										{
											$('#design-dialog-html-editor').dialog("close");

											DesignMode().showSpinner('Refreshing...');
											document.location.reload();
										}
										else
										{
											//check for product page blocks
											if ($("." + designElementClass).parents(".element_pages_product_elements_element-blocks").length > 0)
											{
												designElementClass = "element_pages_product_elements_element-blocks";
												$("." + designElementClass).html();
											}
											var el = DesignMode().loadElement(designElementClass, function(html){
												try
												{
													$("#site-container ." + designElementClass).replaceWith(html);
													init(); // re-init everything again
													skinInit();
													DesignMode().initZones('.layout-zone.droppable'); // int zones
													DesignMode().initPanels("#site-container .panel"); // init boxes
													DesignMode().initContextMenu(); // re-set context menu
													setContentWidth();
												}
												catch(err)
												{
												}
											});
										}
									}
									else
									{
										alert(response.message);
									}
								}
							});

						}
					}
				}
			},
			'Save': function() {
				$('#design-dialog-html-editor').dialog("close");

				// save then close
				DesignMode().showSpinner("Saving data");
				var currentLocation = window.location.toString();

				$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
					'location' : currentLocation.toString(),
					'action': 'saveElementHtml',
					'class': $('#design-dialog-html-editor').data('designElementClass'),
					'data': $('#design-dialog-html-editor textarea').val()
					
				}, function(response){
					
					DesignMode().hideSpinner();
					if ($.trim(response) != "")
					{
						response = JSON.parse(response);
						if (response.status)
						{
							DesignMode().showSpinner('Refreshing...');
							document.location.reload();
						}
						else
						{
							alert(response.message);
						}
					}
				});
			}
		},
		'open': function() {
			if ($(this).data('wysiwyg'))
			{
				tinymce.EditorManager.execCommand("mceAddEditor", false, 'html-source-editor');
			}
			
			DesignMode().correctDialogPosition("#design-dialog-html-editor");
		},
		'close': function() {
			if ($(this).data('wysiwyg'))
			{
				tinymce.EditorManager.execCommand('mceRemoveEditor', false, 'html-source-editor');
			}
		}
	});
	
	/**********************************************
	 ** CREATE NEW BOX DIALOG
	 **********************************************/
	$('#design-dialog-new-element').dialog({
		modal         : true,
		resizable     : false,
		autoOpen      : false,
		draggable     : true,
		width         : 845,
		height        : 550,
		//stack         : false,
		//overlay       : 0.8,
		title         : 'Create New Widget',
		dialogClass	  : 'design-mode',
		'open' : function(ev, ui){
			DesignMode().correctDialogPosition("#design-dialog-new-element");
			var updatePanelIdField = true;
			
			$('#design-dialog-new-element-title').val("");
			$('#design-dialog-new-element-id').val("");
			$('#design-dialog-new-element-html').val("");
			$("#design-dialog-new-element-id-error").html("");
			$('#design-dialog-new-element-id').removeClass("error");
			
			//if there already was some input, do not auto generate panel id
			$('#design-dialog-new-element-id')
				.change(function(){
					// replace any non-word characters with underscores
					$(this).val($(this).val().toLowerCase().replace(/\W/g, "-").replace(/_/g, "-"));
					DesignMode().checkNewElementId();
					updatePanelIdField = false;
				})
				.keyup(function(){
					DesignMode().checkNewElementId();
				});
		
			//auto fill in new element id when typing widget name
			$('#design-dialog-new-element-title').keyup(function(){
				if ($.trim($('#design-dialog-new-element-id').val()) == '')
				{
					$('#design-dialog-new-element-id').val('');
					updatePanelIdField = true;
				}
				if (updatePanelIdField)
				{
					var a = $.trim($(this).val().toLowerCase().replace(/\W/g, ' ').replace(/_/g, ' '));
					$('#design-dialog-new-element-id').val(a.replace(/\s+/g, ' ').split(' ').join('-'));

					DesignMode().checkNewElementId();
				}
			});
		},
		
		'buttons': { 
			'Cancel': function()
			{
				// Close the dialog
				$(this).dialog("close");
			},
			'Create': function()
			{
				DesignMode().createCustomElement();
			}
		}
	});
	
	/**********************************************
	 ** EDIT CUSTOM CSS FILE DIALOG
	 **********************************************/
	$('#design-dialog-custom-css-editor').dialog({
		'modal'         : true,
		'resizable'     : false,
		'autoOpen'      : false,
		'draggable'     : true,
		'width'         : 850,
		'height'        : 550,
		'stack'         : false,
		'overlay'       : 0.8,
		'title'         : 'Edit Custom CSS',
		'dialogClass'	: 'design-mode',
		'open' : function(ev, ui){
			DesignMode().correctDialogPosition("#design-dialog-custom-css-editor");
			DesignMode().customCssEditor();
		},
		
		'buttons': { 
			'Cancel': function()
			{
				// Close the dialog
				$(this).dialog("close");
			},
			'Save': function()
			{
				DesignMode().saveCustomCss();
			}
		}
	});

	$('.ui-dialog').css({position:'fixed'});
});
	
$(document).ready(function(){
	
	$('#myTemplatesFilter').filterList({ list: '#idTabSheetmyTemplates ul.templates' });
	$('#freeTemplatesFilter').filterList({ list: '#idTabSheetfreeTemplates ul.templates' });
	$('#premiumTemplatesFilter').filterList({ list: '#idTabSheetpremiumTemplates ul.templates' });

	$('a.preview').fancybox({
		'zoomSpeedIn': 500,
		'zoomSpeedOut': 300,
		'transactionIn': 'elastic',
		'showCloseButton': true,
		'titlePosition': 'inside',
		'titleFormat': formatTitle
	});
	
	$('#confirmSkinDialog').dialog({
		'title'          : 'Switch Themes',
		'autoOpen'       : false,
		'modal'          : true,
		'dialogClass'    : 'skin-selection-dialog',
		'closeOnEscape'  : false,
		'resizable'      : false,
		'width'          : 500,
		'buttons':
		{ 
			'Cancel': function(){
				// close the dialog box
				$('#confirmSkinDialog').dialog('close');
				// reset the selected radio button
				$('#confirmSkinDialog #useLayout_default').attr('checked', true);
			},
			'Save': function(){
				var selectedSkin = $('#confirmSkinDialog').data('target');
				selectedSkin.addClass('activated');
				var activatedSkin = $('li.template:has(a.activated)');

				// Attempt to set the theme in $settings
				$.post('admin.php?p=design_settings&chart=true', {
					'skinName': activatedSkin.attr('title'),
					'layoutConfig': $('#confirmSkinDialog input:checked').val()
				}, function(){
					// upon success, remove the currentTemplate class from li
					$('li.currentTemplate span.templateStatus').html('<a href="#" title="" class="activate">Activate</a>');
					$('li.currentTemplate').removeClass('currentTemplate');
					activatedSkin.addClass('currentTemplate');
					$('li.currentTemplate span.templateStatus').html('Current Theme');
				});

				selectedSkin.removeClass('activated');
				
				// close the dialog box
				$('#confirmSkinDialog').dialog('close');

				if (typeof(DesignMode) != 'undefined' && $('#confirmSkinDialog #useLayout_default').attr('checked'))
				{
					if (confirm("Some of changes you just made require page refresh.\nDo you want to do it now?"))
					{
						DesignMode().showSpinner('Refreshing...');
						document.location.reload();
					}
				}

				// reset the selected radio button
				$('#confirmSkinDialog #useLayout_default').attr('checked', true);
			}
		},
		'open': function(e, ui)
		{
			$('#confirmSkinDialog span.selectedSkin').html($('#confirmSkinDialog').data('target').attr('title'));
			$('#confirmSkinDialog').height('170px');
		}
	});
	
	
	$('ul.templates a.preview_link').click(function(){
		$('#'+$(this).attr('rel')).click();
		return false;
	});
	
	// Controls for activating a theme
	$('li.template a.activate').on('click', function(){
		$('#confirmSkinDialog').data('target', $(this));
		$('#confirmSkinDialog').dialog('open');
		return false;
	});
	
	$('#fancybox-left,#fancybox-right').hover(function(){
		$(this).css({
			'background-color': '#000000',
			'opacity': 0.2
		});
	}, function(){
		$(this).css({
			'background-color': 'transparent',
			'opacity': 1
		});
	});
});

function formatTitle(title, currentArray, currentIndex, currentOpts) {
    return '<div>' + (title && title.length ? '<strong>' + title + '</strong>' : '' ) + '</div>';
}
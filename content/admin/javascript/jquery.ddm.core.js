/**
 * DDM Core Jquery Plugin
 *
 * @package ddm_jquery
 **/

(function(jQuery) {
	
	// create namespace for DDM
	jQuery.ddm = {}
	
	jQuery.ddm.dispatchFlex = function(appname){
		if (navigator.appName.indexOf("Microsoft") != -1)
		{
			return window[appname];
		} else {
			return document[appname];
		}
	};
	
	jQuery.ddm.sprintf = function(string, variables){
		string = string.replace(/%%/g, '%&%&');
		for (var key in variables)
		{
			regexp = new RegExp('%'+key, 'g');
			string = string.replace(regexp, variables[key]);
		}
		string = string.replace(/%&%&/, '%');
		return string;
	}
	
	jQuery.fn.filterList = function(options){
		var settings = {
			'list': null,
			'noResults': null
		}
		
		// extend default settings
		settings = $.extend(settings, options);
		
		
		if ($(settings.list+' li').size() == 0)
		{
			$(settings.list).after('<p id="noResults" class="noResults">No Results</p>');
			settings.noResults = '#noResults';
			$(settings.noResults).hide();
		}
		
		if ($(settings.list+' li').length === 0) $(settings.noResults).fadeIn();
		
		$(this).keyup(function(){
			var noResults = $(settings.noResults);
			var list = settings.list;
			
			if ($.trim($(this).val()) == '')
			{
				noResults.fadeOut().addClass('hide');
				$(list+' li.filtered').fadeIn();
				return;
			}
		
			$(list+' li span.tags:not(:contains('+$.trim($(this).val().toLowerCase())+'))').parent('li').fadeOut().addClass('filtered');
			$(list+' li.filtered span.tags:contains('+$.trim($(this).val().toLowerCase())+')').parent('li').fadeIn().removeClass('filtered');
		
			if ($(list+' li.filtered').length == $(list+' li').length)
			{
				noResults.fadeIn().removeClass('hide');
			}
			else if (!noResults.hasClass('hide'))
			{
				noResults.hide().addClass('hide');
			}
		});
	}
	
	jQuery.ddm.validateIpFormat = function(options){
		// default settings
		var settings = {
			'ip': '',
			'wildcard': false,
			'wildcard_tag': '*',
			'match_ip': false,
			'user_ip': '0.0.0.0'
		}
		
		// extend default settings
		settings = $.extend(settings, options);
		while (settings.user_ip.indexOf('.') != -1) settings.user_ip = settings.user_ip.replace('.', ',');
		
		var valid = true;
		var found_match = false;
		var ip = settings.ip;
		var user_ip = settings.user_ip.split(",");
		var ip_sub = new Array();
		
		ip_sub = ip.split(".");
		if (ip_sub.length < 4) return false;
		
		// check if the user ip matches
		if (settings.match_ip)
		{
			pattern_string = ip;
			while (pattern_string.indexOf(settings.wildcard_tag) != -1) pattern_string = pattern_string.replace(settings.wildcard_tag, '\\d{1,3}');
			while (pattern_string.indexOf('.') != -1) pattern_string = pattern_string.replace('.', ',');
			pattern = new RegExp(pattern_string,'g');
			if (pattern.test(settings.user_ip)) return -1;
		}
		
		for (key in ip_sub)
		{
			if (settings.wildcard && ip_sub[key] != settings.wildcard_tag)
			{
				var sub = parseInt(ip_sub[key]);
				
				if ((isNaN(sub)) || (sub <0) || (sub >255)) {
					valid = false;
				}
			}
		}
		
		return valid;
	}
	
	String.prototype.ucwords = function(){
		return this.replace(/\w+/g, function(l){
			return l.charAt(0).toUpperCase() + l.substr(1).toLowerCase();
		});
	};
	
	String.prototype.repeat = function(n)
	{
		return new Array(n+1).join(this);
	};
	
})(jQuery);
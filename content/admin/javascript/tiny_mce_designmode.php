<?php
	/**
	 * Do not cache output
	 */
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, max-age=0, must-revalidate, post-check=0, pre-check=0");
	header("Pragma: no-cache");

	header('Content-type: application/javascript');

	$base_url = substr($_SERVER['PHP_SELF'], 1, strlen($_SERVER['PHP_SELF']) - 46);
?>

tinymce.init({
	selector: ".mce",
	theme: "modern",
	plugins: [
		"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
		"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
		"table contextmenu directionality emoticons template paste textcolor moxiemanager"
	],
	toolbar1:"fontselect fontsizeselect | forecolor backcolor | bold italic underline strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | justifyleft justifycenter justifyright justifyfull | bullist numlist outdent indent",
	toolbar2:"code removeformat | cut copy paste | undo redo | link unlink anchor | insertfile image media charmap hr",

	plugin_insertdate_dateFormat : "%d-%m-%Y",
	plugin_insertdate_timeFormat : "%H:%M:%S",
	remove_script_host : true,
	relative_urls : false,
	convert_urls : false,
	cleanup : true,
	cleanup_on_startup : true,
	document_base_url : '',
	valid_children : "+body[style]",
	verify_html : false,
	forced_root_block : false,
	extended_valid_elements : "",
	external_link_list_url : "",
	external_image_list_url : "",
	image_dimensions: false,
	image_class_list: [
		{title: 'Responsive', value: 'img-responsive'},
		{title: 'None', value: ''}
	],
	setup: function (editor) {
		editor.on('init', function(args) {
			editor = args.target;

			editor.on('NodeChange', function(e) {
				if (e && e.element.nodeName.toLowerCase() == 'img') {
					tinyMCE.DOM.setAttribs(e.element, {'width': null, 'height': null, 'caption': null});
				}
			});
		});
	}
});
function toggleEditorMode(sEditorID, eleToggleLink)
{
	try
	{
		if ($('#' + sEditorID + '_ifr').length > 0)
		{
			tinymce.EditorManager.execCommand("mceRemoveEditor", false, sEditorID);
			if (eleToggleLink) eleToggleLink.innerHTML = "Show Editor";
		}
		else
		{
			tinymce.EditorManager.execCommand("mceAddEditor", false, sEditorID);
			if (eleToggleLink) eleToggleLink.innerHTML = "Hide Editor";
		}
	}
	catch(e)
	{
		//error handling
	}
}

jQuery(function ($) {
	$.widget("ui.dialog", $.ui.dialog, {
		_allowInteraction: function(event) {
			return !!$(event.target).closest(".mce-container").length || this._super( event );
		}
	});
});
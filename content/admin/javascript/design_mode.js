$(document).ready(function(){

	$("#design-mode").show('slow');

	//reset server session every 5  minutes
	var sessionRefreshInterval = setInterval("DesignMode().pingServer();", 5*60*1000);

	// make the layout zones droppables
	DesignMode().initZones('#site-container .layout-zone.droppable');

	// make the panels draggable and droppable
	DesignMode().initPanels('#site-container .panel');
	
	// create the context menu for all .editable elements
	DesignMode().initContextMenu();

	// init bottom tab
	DesignMode().initDashboard();
	
	// init help tips
	$('.helpertip').each(function(){
		$(this).qtip({
			content: $(this).html(),
			show: 'mouseover',
			hide: 'mouseout',
			position: {
				my: 'top right',
				at: 'bottom left',
				target: $(this)
			},
			style: {name: 'cream', color: '#000000', tip: true}
		});
		$(this).css('cursor', 'pointer').html('');
	});
});

/**
 * Ping server
 */
function designMode_pingServer()
{
	$.post(
		site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true',
		{
			'action'	: 'ping'
		},
		function(response){
			// check is session expired
			if (response != '{"status":1}')
			{
				alert('Your admin session has expired. Please login to continue');
				document.location = site_ajax_url_admin + '/login.php';
			}
		}
	);
}

/**
 * Check admin security
 * @return
 */
function designMode_checkAdminSecurity(data, textStatus)
{
	/**
	console.log(data);
	console.log(textStatus);
	**/
}

/**
 * Init bottom dashboard
 * @return
 */
function designMode_initDashboard()
{
	DesignMode().fixTabs("#design-dashboard-tabs-ul a");

	$("#design-dashboard .design-dashboard-tabs")
		.tabs()
		.on('tabsbeforeactivate', function(event, ui){
			switch ($(ui.newPanel).attr("id"))
			{
				case "design-dashboard-hide" :
				{
					$("#design-dashboard").slideUp("fast", function(){
						$("#design-dashboard-show")
						.show()
						.click(function(){
							$("#design-dashboard-show").hide(function(){
								$("#design-dashboard").slideDown("fast");
							});
							return false;
						})
						.mouseover(function(){$("#design-dashboard-show ul li").addClass("ui-state-hover");})
						.mouseout(function(){$("#design-dashboard-show ul li").removeClass("ui-state-hover");});
					});
					event.stopPropagation();
					return false;
					break;
				}
				case "design-dashboard-exit" :
				{
					window.location = site_admin_url + '/admin.php?p=design_settings&design_mode_flag=Off';
					event.stopPropagation();
					return false;
					break;
				}
				case "design-dashboard-save" :
				{
					$('#design-dialog-save-changes').dialog('open');
					event.stopPropagation();
					return false;
					break;
				}
			}
		})
		.on('tabsactivate', function(event,ui){
			switch ($(ui.newPanel).attr("id"))
			{
				case 'design-dashboard-skins':
				{
					if ($('#design-dashboard-skins').data('wrapper_width') === undefined)
					{
						$('#design-dashboard-skins').data('wrapper_width', true);

						$('.skin-selection-dialog button').click(function(){
							if ($(this).html().toLowerCase() == 'save')
							{
								if (confirm("Some of changes you just made require page refresh.\nDo you want to do it now?"))
								{
									DesignMode().showSpinner('Refreshing...');
									document.location.reload();
								}
							}
						});
					}
					break;
				}
			}
		});

	$("#design-dashboard .design-dashboard-tabs .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *") 
		.removeClass("ui-corner-all ui-corner-top") 
		.addClass("ui-corner-bottom");
	
	$("#design-dashboard .design-dashboard-tabs ul.ui-tabs-nav").append('<li style="float:right;padding:6px 15px 5px 12px;color:snow;"><input id="design-dashboard-outline" type="checkbox"/>&nbsp;<label for="design-dashboard-outline">outline</label></li>');
	$("#design-dashboard .design-dashboard-tabs ul.ui-tabs-nav").prepend('<li style="float:left;padding:6px 15px 5px 12px;color:snow;background-color:#930011;">Design Toolbar</li>');
	
	$("#design-dashboard-tab-save,#design-dashboard-tab-hide,#design-dashboard-tab-help,#design-dashboard-tab-exit").css("float", "right");

	//ini zones checkboxes
	$("#design-dashboard-toggle-left-zone,#design-dashboard-toggle-right-zone").click(function(){
		DesignMode().toggleZone(this);
	});
	
	//show/hide side bars - set initial state
	if ($("#main #left:visible").length > 0) $("#design-dashboard-toggle-left-zone").attr("checked", "checked");
	if ($("#main #right:visible").length > 0) $("#design-dashboard-toggle-right-zone").attr("checked", "checked");

	$('#design-dashboard-create-new-panel').click(function(){
		$('#design-dialog-new-element').dialog('open');
		return false;
	});
	
	$('#design-dashboard-images-editor-images a').click(function(){
		DesignMode().imagesEditor('image');
		return false;
	});
	$('#design-dashboard-images-editor-buttons a').click(function(){
		DesignMode().imagesEditor('button');
		return false;
	});
	
	$('#design-dashboard-styles-editor').click(function(){DesignMode().stylesEditor();return false;});
	$('#design-dashboard-colors-editor').click(function(){DesignMode().colorsEditor();return false;});
	
	$('#design-dashboard-layout-catalog-settings').click(function(){DesignMode().layoutEditor("catalog");return false;});
	$('#design-dashboard-layout-product-settings').click(function(){DesignMode().layoutEditor("product");return false;});
	
	$('#design-dashboard-fonts-editor').click(function(){DesignMode().fontsEditor();return false;});
	$('#design-dashboard-restore-layout').click(function(){DesignMode().restoreLayout();return false;});
	$('#design-dashboard-restore-skin').click(function(){DesignMode().restoreSkin();return false;});
	$('#design-dashboard-clear-skin-cache').click(function(){DesignMode().clearSkinCache();return false;});
	
	$('#design-dashboard-edit-custom-css').click(function(){
		$('#design-dialog-custom-css-editor').dialog('open');
		return false;
	});
	
	$('#design-dashboard-images-editor').click(function(){DesignMode().imagesEditor();return false;});
	
	$('#site-container').css('margin-bottom', '100px');
	
	//set outline mode
	var dashboardOutlineChecked = parseInt($.cookie("design-dashboard-outline-checked") == null ? 1 : $.cookie("design-dashboard-outline-checked"));
	if (dashboardOutlineChecked)
	{
		$("#design-dashboard-outline").attr("checked", true);
	}
	else
	{
		$("#design-dashboard-outline").removeAttr("checked");
	}
		
	$("#design-dashboard-outline").click(function(){
		if ($(this).is(":checked"))
		{
			$.cookie("design-dashboard-outline-checked", 1);
		}
		else
		{
			$.cookie("design-dashboard-outline-checked", 0);
		}
	});
	
	// add change theme control
	$('#design-site-theme').change(function(){
		$('#css-theme').attr('href', skin_themes + $(this).val() + ".css");
	});
	
	DesignMode().initDashboardPanels();
	DesignMode().initDashboardThemes();
}

/**
 * Dock - undock toolbar
 * @param dock
 */
function designMode_initContextMenu()
{
	// Tooltip right-click help for editable areas
	
	$('#header, #footer, #navigation, .editable').each(function(){
		$(this).qtip({
			content: 'Right-click to edit',
			show: {
				solo: true,
				delay: 500
			},
			position: {
				my: 'left center',
				at: 'right center',
				adjust: {x: 10, y : 10, mouse:true}
			},
			style: {name: 'cream', color: '#000000', tip: false},
			api: {
				beforeShow: function() {
					return $("#design-dashboard-outline").is(":checked");
				}
			}
		});
	});

	$('#header, #footer, #navigation, .editable, #site-container a').each(function(){
		$(this).contextMenu(
			{
				'menu'      : 'design-context-menu',
				'beforeShow': function(e, target){
					
					if ($(target).hasClass('panel'))
					{
						$('#design-context-menu li:has(#design-context-menu-remove-action)').show();
						$('#design-context-menu').data("element-to-remove", target);
					}
					else if ($(target).parent('.panel').length)
					{
						$('#design-context-menu li:has(#design-context-menu-remove-action)').show();
						$('#design-context-menu').data("element-to-remove",$(target).parent('.panel'));
					}
					else
					{
						$('#design-context-menu li:has(#design-context-menu-remove-action)').hide();
					}
					
					// added for wysiwyg editor
					if ($(target).hasClass('wysiwyg') || $(target).hasClass('layout-zone-header') || $(target).hasClass('layout-zone-footer') || $(target).hasClass('element_pages_site_home') || $(target).parent('.wysiwyg').length)
					{
						$('#design-context-menu li:has(#design-context-menu-wysiwyg-action)').show();
					}
					else
					{
						$('#design-context-menu li:has(#design-context-menu-wysiwyg-action)').hide();
					}
				}
			},
			function(action, el, pos){
				var addSubSelector = '';
				if ($(el)[0].tagName == 'A')
				{
					var editableEle = el.closest('.editable');
					if (editableEle.length > 0) {
						el = $(editableEle.get(0));
						addSubSelector = ' a';
					}
//					el.closest('.editable').each(function(){
//						el = this;
//						addSubSelector = ' a';
//					});
				}
				if (!el || el == 'undefined' || el == null || !$(el).hasClass('editable')) return;
				
				
				switch (action)
				{
					case 'edit-element-css':
					{
						DesignMode().editElementStyles(el, addSubSelector);
						break;
					}
					case 'edit-element-html-wysiwyg':
					{
						DesignMode().editElementHtml(el, true);
						break;
					}
					case 'edit-element-html':
					{
						DesignMode().editElementHtml(el, false);
						break;
					}
					case 'remove-element':
					{
						$($('#design-context-menu').data("element-to-remove")).remove();
						$("#content .wrap-panels:empty").remove();
						break;
					}
				}
			}
		);
		
		$(this).mouseover(function(){
			if ($("#design-dashboard-outline").is(":checked")) $(this).addClass("outline");
		});
			
		$(this).mouseout(function(){
			if ($("#design-dashboard-outline").is(":checked")) $(this).removeClass("outline");
		});
	});
}

/**
 * Makes layout zones droppables
 */
function designMode_initZones(selector)
{
	$(selector).each(function(){
		$(this).droppable({
			'accept'        : '.draggable',
			'tolerance'     : 'pointer',
			'activeClass'   : 'layout-overlay-hover',
			'drop': function(e, ui){
				/**
				 * Drop element(panel) on zone
				 */
				var newPanel = $(ui.draggable).hasClass("new-element");  
				if (newPanel)
				{
					//load new element
					var draggable = DesignMode().loadElement(DesignMode().getDesignElementClass(ui.draggable), function(data){
						try
						{
							//when new panel dropped on zone
							init(); // re-init everything (like categories tree, etc)
							skinInit();
							DesignMode().initPanels("#site-container .panel"); // re-make panels dragable / dropable
							DesignMode().initContextMenu(); // re-init context menu
							setContentWidth();
						}
						catch(err)
						{
						}
					});
				}
				else
				{
					//or jast move existing one
					var draggable = ui.draggable;
				}
				
				var droppable = e.target;
				
				if ($(droppable).attr("id") == "content")
				{
					if ($(droppable).find(".wrap-panels").length == 0)
					{
						$('<div class="wrap-panels"></div>').appendTo($(droppable));

					}
					$(droppable).find(".wrap-panels").append(draggable);
				}
				else
				{
					$(droppable).append(draggable);
				}
				
				if (!newPanel)
				{
					init(); // re-init everything (like categories tree, etc)
					DesignMode().initPanels("#site-container .panel"); // re-make panels dragable / dropable
					DesignMode().initContextMenu(); // re-init context menu
					setContentWidth();
				}
				
				// remove the ui helper from the zone
				ui.helper.remove();
			}
		});
	});
}

/**
 * Init element(s)
 * @param selector
 * @param options
 * @return
 */
function designMode_initPanels(selector)
{	 
	$(selector).addClass('draggable').each(function(){
		/**
		 * Add overlay
		 */
		if (!$(this).children('div.layout-overlay').length)
		{
			$('<div class="layout-overlay"></div>').css({
				'width'     : $(this).width(),
				'height'    : $(this).height(),
				'opacity'   : 0.8
			}).clone().prependTo($(this));
		}
	
		/**
		 * Make the element draggable
		 */
		$(this).draggable({
			'cursor': 'move',
			'delay': 0,
			'revert': 'valid',
			'revertDuration': 1000,
			'selectable': false,
			'helper': function(){
				var designElementClass = DesignMode().getDesignElementClass(this);
				return $('<div class="design-draggable">' + $('#design-dashboard ul.design-dashboard-panels li.' + designElementClass).html() + '</div>').css('opacity', 0.9);
			},
			'start': function(e,ui){
				$('div.layout-overlay').addClass('layout-overlay-active').toggle();
			},
			'stop': function(){
				// hide all overlays
				$('div.layout-overlay').removeClass('layout-overlay-active').toggle();
			}
		});
		
		/**
		 * Make the element droppable
		 */
		$(this).droppable({
			'accept': '.draggable',
			'tolerance': 'pointer',
			'hoverClass': 'layout-overlay-hover',
			'greedy': true,
			'drop': function(e,ui){
				/**
				 * Drop element(panel) on panel
				 */
				var newPanel = $(ui.draggable).hasClass("new-element");  
				if (newPanel)
				{
					var draggable = DesignMode().loadElement(DesignMode().getDesignElementClass(ui.draggable), function(data){
						try
						{
							init(); // re-init everything (like categories tree, etc)
							skinInit();
							DesignMode().initPanels("#site-container .panel"); // re-make panels dragable / dropable
							DesignMode().initContextMenu(); // re-init context menu
							setContentWidth();
						}
						catch(err)
						{
						}
					});
				}
				else
				{
					var draggable = ui.draggable;
				}
				var droppable = e.target;
				
				var offset = $(this).offset();
				if (ui.offset.top < (offset.top + ($(this).height() / 2)))
					draggable.insertBefore(droppable);
				else
					draggable.insertAfter(droppable);
				
				if (!newPanel)
				{
					init(); // re-init everything (like categories tree, etc)
					DesignMode().initPanels("#site-container .panel"); // re-make panels dragable / dropable
					DesignMode().initContextMenu(); // re-init context menu
					setContentWidth();
				}
				// Remove the helper from display
				ui.helper.remove();
			}
		}).css('cursor', 'move');
	});
}

/**
 * Inits design dashboard panels
 * @return
 */
function designMode_initDashboardPanels()
{
	$('#design-dashboard .design-dashboard-panels li.draggable').each(function(){
		$(this).draggable({
			'cursor': 'move',
			'delay': 0,
			'revert': 'valid',
			'revertDuration': 1000,
			'selectable': false,
			'helper': function(){
				return $('<div class="design-draggable" style="position:inherit;;">' + $(this).html() + '</div>').css('z-index', 1010).css('opacity', 0.9);
			},
			'start': function(){
				$('div.layout-overlay').addClass('layout-overlay-active').toggle();
			},
			'stop': function(){
				$('div.layout-overlay').removeClass('layout-overlay-active').toggle();
			}
		});

		var designElementClassName = DesignMode().getDesignElementClass(this);
		if (/^element\_panels\_panel\-custom/.test(designElementClassName))
		{
			if ($(this).find(".ui-icon-close").length == 0)
			{
				$('<span class="ui-icon ui-icon-close" style="float:right;cursor:pointer;"/>')
					.click(function(){
						if (confirm("Do you really want to delete selected custom widget?\nNote: it also will be removed from all pages where the widget currently resides."))
						{
							DesignMode().showSpinner("Deleting custom widget");
							var designElementClassName = DesignMode().getDesignElementClass($(this).parent());
							$.post(
								//url
								site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
								//data
								{
									'action'	: 'deleteCustomElement',
									'class'		: designElementClassName
								},
								//after post
								function(response){
									if (response != "")
									{
										response = JSON.parse(response);
										if (!response.status) alert(response.message);
									}
									$("." + designElementClassName).remove();
									DesignMode().hideSpinner();
								}
							);
						}	
					})
					.prependTo($(this));
			}
		}
	});
}

function designMode_initDashboardThemes()
{
	var html = '';
	//show current themes
	$.each(currentThemes, function(key, theme){
		html = html + 
			'<li class="' + (theme.current ? 'current' : '') + '" id="dm-theme-' + theme.name + '">' +
				'<div class="title">' + theme.title + '</div>' +
				'<a class="preview" title="' + theme.title + '" href="content/skins/' + theme.name + '/' + theme.fullsizeImage + '"><img src="content/skins/' + theme.name + '/' + theme.thumbnailImage + '"/></a>' +
				'<div class="activate"' + (theme.current ? ' style="display:none;"' : '') + '><a class="activate" data-theme-id="' + theme.name + '" href="#">Activate</a></div>' +
				'<div class="current"' + (!theme.current ? ' style="display:none;"' : '') + '>Currently Used</div>' +
			'</li>';
	});
	
	html = html + 
		'<li class="more-themes"><div>More themes</div><a class="dm-download-themes" href="#more-themes"><span class="download"></span></a><div><a class="dm-download-themes" href="#more-themes">Get more themes</a></div></li>';
	
	$('#design-dashboard .themes-area ul.themes').html(html);
	
	$('#design-dashboard-skins .skin-container-scroller').width($('#design-dashboard .themes-area ul.themes li').length * 190 + 10);
	
	$('#design-dashboard .themes-area ul.themes li a.preview').fancybox({
		'zoomSpeedIn': 500,
		'zoomSpeedOut': 300,
		'transactionIn': 'elastic',
		'showCloseButton': true,
		'titlePosition': 'inside',
		'titleFormat': 'formatTitle'
	});

	// activate theme
	$('#design-dashboard-skins .themes-area ul.themes').on({
		'click' : function(event){
			event.stopPropagation();

			if (!confirm('* * * PLEASE CONFIRM * * *\n\nAre you sure you want to activate the selected theme?\n\nPlease note: activating this theme will discard all layout changes you previously made'))
			{
				return false;
			}

			DesignMode().activateTheme($(this).attr('data-theme-id'));
		
			return false;
		}
	}, 'li a.activate');

	// get available themes
	var themeRepository = document.location.protocol + '//www.cartmanual.com/themes';

	$('#design-dashboard .themes-area ul.themes a.dm-download-themes').click(function(event){
		event.stopPropagation();
		DesignMode().showSpinner('Loading available themes');

		// NOTE: we are calling external URL here, DO NOT use AdminForm.sendRequest
		$.ajax({
			url: themeRepository + '/list?v=' + cartVersionRaw,
			dataType: 'jsonp',
			success: function (data)
			{
				var html = '';
				
				//show loaded themes
				$.each(data.themes, function(key, theme){
					//check is this theme already in a cart
					if (currentThemes[theme.key] == undefined || currentThemes[theme.key] == null)
					{
						var href = themesInstallable ? '#' + theme.key : themeRepository + '/downloads/' + theme.key + '.zip';
						html = html + 
							'<li class="external" id="dm-theme-' + theme.key + '">' + 
								'<div>' + theme.name + '</div>' +
								'<a class="preview" title="' + theme.name + '" href="' + themeRepository + '/downloads/' + theme.preview + '"><img src="' + themeRepository + '/downloads/' + theme.thumbnail + '"/></a>' +
								'<div class="install">' +
									'<a class="' + (themesInstallable ? 'installable' : 'downloadable' ) + '" data-theme-id="' + theme.key + '" href="' + href + '">' +
										(themesInstallable ? 'Install' : 'Download') +
									'</a>' + 
								'</div>' +
								'<div class="activate" style="display:none;"><a class="activate" data-theme-id="' + theme.key + '" href="#">Activate</a></div>' +
								'<div class="current" style="display:none;">Currently Used</div>' +
							'</li>';
					}
				});

				$('#design-dashboard .themes-area ul.themes li.more-themes').remove();

				$('#design-dashboard .themes-area ul.themes').append(html);

				$('#design-dashboard-skins .skin-container-scroller').width($('#design-dashboard .themes-area ul.themes li').length * 190 + 10);

				$('#design-dashboard .themes-area ul.themes li.external a.preview').fancybox({
					'zoomSpeedIn': 500,
					'zoomSpeedOut': 300,
					'transactionIn': 'elastic',
					'showCloseButton': true,
					'titlePosition': 'inside',
					'titleFormat': 'formatTitle'
				}); 
				
				/**
				 * Install & Activate
				 */
				$('#design-dashboard .themes-area ul.themes a.installable').each(function(){
					$(this).click(function(event){
						event.stopPropagation();


						var themeId = $(this).attr('data-theme-id');

						DesignMode().showSpinner('Installing Theme');

						$.get(site_admin_url+'/admin.php?p=design_settings&chart=true&mode=install&id=' + themeId, function(response){
							//response = eval('(' + response + ')');
							DesignMode().hideSpinner();
							
							if (response.result == 'success')
							{
								DesignMode().showSpinner('Reloading Page');
								document.location.reload();
								return false;
							}
							else
							{
								alert('Cannot install theme.');
							}
						});
						
						return false;
					});
				});

				DesignMode().hideSpinner();
			}
		});
		return false;
	});
}

/**
 * Activate theme
 */
function designMode_activateTheme(themeName)
{
	DesignMode().showSpinner('Activating Theme');
	$.get(site_admin_url+'/admin.php?p=design_settings&chart=true&mode=install&id=' + themeName, function(response){
		//response = eval('(' + response + ')');
		if (response.result == 'success')
		{
			DesignMode().showSpinner('Reloading Page');
			document.location.reload();
			return false;
		}
		else
		{
			DesignMode().hideSpinner();
			alert('Cannot activate theme.');
		}
	});
}

/**
 * Shows big load spinner
 * @param msg
 */
function designMode_showSpinner(msg)
{
	if (msg == null) msg = 'Updating data';
	$("#design-loader").html("Please Wait.<br/> " + msg + "...").dialog("open");
}

/**
 * Hides spinner
 */
function designMode_hideSpinner()
{
	$("#design-loader").dialog("close");
}

/**
 * Load new element from server
 * @param ui
 * @return element
 */
function designMode_loadElement(designElementClass, onload)
{
	DesignMode().showSpinner("Loading element from server");
	if (!designElementClass) return false;
	
	/**
	 * Create a new box that has a loading indicator
	 */
	var element = $('<div class="design-mode-loading"><div class="design-mode-loading-wrap">Loading data...</div></div>');
		
	/**
	 * Request the HTML for this element
	 */
	var s = document.location.toString();
	var i = s.indexOf("#");
	if (i > 0) s = s.substr(0, i);
	
	$.post(
		//url
		s, 
		//data
		{
			'action':	'loadElement', 
			'class':	designElementClass
		},
		//aftre post
		function(response)
		{
			DesignMode().hideSpinner();
			if (response != "")
			{
				response = JSON.parse(response);
				if (response.status)
				{
					try
					{
						$(element).replaceWith(response.html);
					}
					catch(err)
					{
						//alert(err.description);
					}
					onload(response.html);
				}
				else
				{
					$(element).remove();
				}	
			}
		}
	);
	
	return element;
}



/**
 * Returns element class name (parsed later to file path)
 * @param el
 */
function designMode_getDesignElementClass(el)
{
	var classes = $(el).attr('class').split(' ');
	for(var i=0; i < classes.length; i++)
	{
		if (/^element\_/.test(classes[i]))
		{
			return (classes[i]);
		}
	}
	return false;
}

/**
 * Returns real class name from design element class name
 * @param className
 * @return string
 */
function designMode_getDesignElementRealClass(className)
{
	var  p = className.lastIndexOf('_');
	if (p >= 0)
	{
		return className.substr(p + 1);
	}
	return false;
}

/**
 * 
 * @param el
 * @return
 */
function designMode_getDesignElementCssClass(el)
{
	var classes = $(el).attr('class').split(' ');
	if (classes.length > 0) return classes[0];
	return false;
}

/**
 * Save lasyou changes
 * @return
 */
function designMode_saveChanges()
{
	// Create a structure that holds the current state of the layout
	var layoutState = {
		// define each zone to preserve the order of the elements
		'zones': {
			'left'      : new Array(),
			'right'     : new Array(),
			'content'   : new Array()
		}
	};

	// Create an array of Element Types
	var elementTypes = new Array();

	$('#main div.layout-zone:visible').each(function(){
		var layoutZoneId = this.id;
		$(this).find('div.panel').each(function(){
			var designElementClass = DesignMode().getDesignElementClass(this);
			layoutState.zones[layoutZoneId].push(designElementClass);			
		});
	});
	var scope = $("#design-dialog-save-changes input[name='saveLayoutScope']:checked").val();
	var page = $('meta[name="ddm-page"]').attr('content');
	var pages = "";
	$("#design-dialog-save-changes input[name='saveLayoutSelected']:checked").each(function(){
		pages = pages + (pages == "" ? "" : ",") + $(this).val();
	});
	
	if (scope == "selected" && pages == "")
	{
		alert("Please select pages");
		return false;
	}

	var theme = $('#design-site-theme option:selected').val();
	
	DesignMode().showSpinner("Saving changes on server");
	$.post(
		//url
		site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
		//data
		{
			'action'		: 'saveLayout',
			'scope'			: scope,
			'page'			: page,
			'pages'			: pages,
			'theme'			: theme,
			'layout'		: JSON.stringify(layoutState)
		},
		//after post
		function(data){
			DesignMode().hideSpinner();
		}
	);

	return true;
}

/**
 * Checks is new element id empty and unique
 * @return
 */
function designMode_checkNewElementId()
{
	var newElementId = $('#design-dialog-new-element-id').val();
	if ($.trim(newElementId) == "")
	{
		//empty
		$("#design-dialog-new-element-id-error").html(" - Please enter id. It is empty now");
		 $('#design-dialog-new-element-id').addClass("error");
		return false;
	}
	var fullId = 'element_panels_panel-' + newElementId;
	
	if ($('.new-element.' + fullId).length > 0)
	{
		//is not unique
		$("#design-dialog-new-element-id-error").html(" - Element with the same ID already exists: " + $('.new-element.' + fullId + ':first').text());
		$('#design-dialog-new-element-id').addClass("error");
		return false;
	}
	$("#design-dialog-new-element-id-error").html("");
	$('#design-dialog-new-element-id').removeClass("error");
	return true;
}

/**
 * Saves new element on a server and creates draggable box for it in tool bar
 * @return
 */
function designMode_createCustomElement()
{
	//check for empty fields
	var errMsg = "";
	var newElementTitle = $('#design-dialog-new-element-title').val();
	var newElementId = $('#design-dialog-new-element-id').val();
	var newElementHtml = $('#design-dialog-new-element-html').val();
	
	//check for errors
	if ($.trim(newElementTitle) == "") errMsg+="\n- Please enter new element title";
	if (!DesignMode().checkNewElementId()) errMsg+="\n- Please check new element id";
	if ($.trim(newElementHtml) == "") errMsg+="\n- Please enter new element HTML";
	if (errMsg != "")
	{
		alert("Pleck check errors:" + errMsg);
		return false;
	}
	
	//save data
	DesignMode().showSpinner("Saving new element");
	
	$.post(
		//url
		site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true',
		//params
		{
			'action': 'createCustomElement',
			'element_type': 'panel', // element type
			'element_id': newElementId, // name
			'element_title': newElementTitle, // title
			'element_html': newElementHtml // html
		}, 
		//callback after save
		function(response)
		{
			DesignMode().hideSpinner();
		
			if (response != "")
			{
				var response = JSON.parse(response);
			
				if (response.status)
				{
					$('#design-dialog-new-element').dialog('close');
					
					var t = $('#design-dialog-new-element-title').val();

					var newPanel = $("<li></li>")
						.addClass("button")
						.addClass("button-panel")
						.addClass("panel")
						.addClass("draggable")
						.addClass("new-element")
						.addClass(response.designElementClassName)
						.html(t.length > 20 ? t.substring(0, 15) + '...' : t)
						.appendTo("#design-dashboard ul.design-dashboard-panels");

					DesignMode().initDashboardPanels();
				}
				else
				{
					alert(response.message);
				}
			}
		}
	);
	return true;
}

/**
 * Edit element HTML
 * @param el
 * @return
 */
function designMode_editElementHtml(el, wysiwyg)
{	
	DesignMode().showSpinner("Loading HTML for editing");
	
	var designElementClass = DesignMode().getDesignElementClass(el);
	if (!designElementClass) return;
	
	/**
	 * Load HTML for element
	 */
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'getElementHtml',
		'class': designElementClass
	}, function(response){
		DesignMode().hideSpinner();
		
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				
				// Displays a warning note to use {literal} smarty tags when using javascript in editor
				if (!$('#design-dialog-html-editor').data('setMessage'))
				{
					$('#design-dialog-html-editor').siblings('.ui-dialog-buttonpane').append('<p style="padding: 10px; font-weight: bold; font-style: italic;">If adding Javascript, wrap {literal}{/literal} tags around it. <a href="http://www.smarty.net/" target="_blank">Click here for more information</a></p>');
					$('#design-dialog-html-editor').data('setMessage', true);
				}
				
				$('#design-dialog-html-editor').data("designElementClass", designElementClass);
				$('#design-dialog-html-editor').data("type", response.type);
				$('#design-dialog-html-editor').data("location", response.location);
				$('#design-dialog-html-editor').data("wysiwyg", wysiwyg);
				$('#design-dialog-html-editor textarea').val(response.html);
				$('#design-dialog-html-editor').dialog('option', 'title', 'HTML Editor: '+ designElementClass.substr(8).split('_').join('/') + '.html');
				$('#design-dialog-html-editor').dialog('open');
			}
			else
			{
				alert(response.message);
			}
		}
	});
}


/**
 * Edit element styles
 * @param el
 * @return
 */
function designMode_editElementStyles(el, subSelector)
{
	var designElementCssClass = DesignMode().getDesignElementCssClass(el);
	if (!designElementCssClass) return;
	$('#design-dialog-styles-editor').data("selected-element", el);
	$('#design-dialog-styles-editor').data("selected-element-subselector", subSelector);
	DesignMode().stylesEditor();
}

/**
 * 
 * @param el
 * @return
 */
function designMode_getEditableChildren(el)
{
	var designElementCssClass = DesignMode.getDesignElementClass(el);
	
	var elements = new Array();
	
	if ($(el).hasClass('panel'))
	{
		elements["Panel"] = [""];
		elements["Header"] = ["h4"];
		elements["Content"] = [".content"];
	}
	
	//custom css for page
	if (/^page\-/.test(designElementCssClass))
	{
		elements["Page"] = [""];
		elements["Page Header"] = ["h1"];
		
		var pageId = $("body").attr("id");
		css += selector + "h1{" + DesignMode().getElementBasicCss($(el).find("h1:first")) + "} /* page header for this page */\n";
	}
}

/**
 * Restores layout to "factory" settings
 * @return
 */
function designMode_restoreLayout()
{
	if (confirm("Do you really want to restore site layout?\nPlease note: this change is not revertable.\nAll layout changes your made will be set to initial state."))
	{
		DesignMode().showSpinner('Restoring layout');
		$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {'action':'restoreLayout'}, 
			function(response){
				if (response != "")
				{
					response = JSON.parse(response);
					
					if (!response.status)
					{
						alert(response.message);
					}
					else
					{
						document.location.reload();
					}
				}
			}
		);
	}
}

/**
 * Restores theme to "factory" settings
 * @return
 */
function designMode_restoreSkin()
{
	if (confirm("Do you really want to restore original theme files?\nPlease note: this change is not revertable.\nAll custom theme files will be lost."))
	{
		DesignMode().showSpinner('Restoring original theme files');
		$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {'action':'restoreSkin'}, 
			function(response){
				if (response != "")
				{
					response = JSON.parse(response);
					if (!response.status)
					{
						alert(response.message);
					}
					else
					{
						document.location.reload();
					}
				}
			}
		);
	}
}

/**
 * Clears current theme cache
 * @return
 */
function designMode_clearSkinCache()
{
	if (confirm("Do you really want to refresh the theme?\n"))
	{
		DesignMode().showSpinner('Refreshing theme cache');
		$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {'action':'clearSkinCache'}, 
			function(response){
				if (response != "")
				{
					response = JSON.parse(response);
					
					if (!response.status)
					{
						alert(response.message);
					}
					else
					{
						document.location.reload();
					}
				}
			}
		);
	}
}



/**
 * Removes all widgets from the checkout pages
 * @return
 */
function designMode_saveCleanCheckout()
{
	if (confirm("Do you really want to remove the left and right columns and widgets on the checkout pages?\nPlease note: this change is not revertable."))
	{
		DesignMode().showSpinner('Removing columns and widgets from all checkout pages.');
		$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {'action':'saveCleanCheckout'}, 
			function(response){
				if (response != "")
				{
					response = JSON.parse(response);
					if (!response.status)
					{
						alert(response.message);
					}
					else
					{
						document.location.reload();
					}
				}
			}
		);
	}
}

/**
 * Shows images editor dialog
 * @return
 */
function designMode_imagesEditor(groupName)
{
	$('#design-dialog-images-editor').data('groupName', groupName);
	designMode_loadImagesEditor();

	var dialogTitle = (groupName == 'image') ? 'Site Images' : 'Site Buttons';
	$('#design-dialog-images-editor').dialog("option", "title", dialogTitle);
	$('#design-dialog-images-editor').dialog("open");
}

function designMode_loadImagesEditor(){
	groupName = $('#design-dialog-images-editor').data('groupName');
	DesignMode().showSpinner("Loading data");
	$.get(site_admin_url+'/admin.php?p=design_images&mode='+groupName+'&printing=true&chart=true&rnd=' + Math.floor(Math.random()*10001), function(data){
		DesignMode().hideSpinner();

		$('#design-dialog-images-editor-content').html(data);
	});
}

function designMode_deleteImage(mode, language, image_id){
	if(confirm('Do you really want to delete this image?')){
		$('#design-dialog-images-editor-iframe').attr('src', site_admin_url+'/admin.php?p=design_images&printing=true&mode='+mode+'&language='+language+'&action=delete_image&image_id='+image_id);
		var l = $("#design-dialog-images-editor-content").data("loads");
		$("#design-dialog-images-editor-content").data("loads", l + 1);
	}

	return false;
}

function designMode_deleteImages(mode, language){
	if(confirm('Do you really want to delete this image?')){
		$('#design-dialog-images-editor-iframe').attr('src', site_admin_url+'/admin.php?p=design_images&printing=true&mode='+mode+'&language='+language+'&action=delete_images');
		var l = $("#design-dialog-images-editor-content").data("loads");
		$("#design-dialog-images-editor-content").data("loads", l + 1);
	}

	return false;
}

/**
 * Show layout editor
 */
function designMode_layoutEditor(editorType)
{
	DesignMode().showSpinner("Loading data");
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'getSettings'
	}, function(response){
		DesignMode().hideSpinner();
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				$("#design-dialog-layout-editor").data("settings", response.settings);
				$("#design-dialog-layout-editor").data("type", editorType);
				if (editorType == 'product')
				{
					$('#design-dialog-layout-editor').dialog({height:260});
				}
				else
				{
					$('#design-dialog-layout-editor').dialog({height:260});
				}
				$('#design-dialog-layout-editor').dialog("open");
			}
			else
			{
				alert(response.message);
			}
		}
	});
}

function designMode_layoutEditorSave()
{
	DesignMode().showSpinner("Saving data");
	switch ($("#design-dialog-layout-editor").data("type"))
	{
		case "catalog" :
		{
			var data = {
				CatalogDefaultSortOrder: $("#CatalogDefaultSortOrder option:selected").val(),
				CatalogItemsOnPage: $("#CatalogItemsOnPage").val()
			};
			break;
		}
		case "product" :
		{
			var data = {
				RecommendedProductsCount: $("#RecommendedProductsCount").val(),
				CatalogEmailToFriend: $("#CatalogEmailToFriend option:selected").val()
			};
			break;
		}
	}
	
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'saveSettings',
		'data': JSON.stringify(data)
	}, function(response){
		DesignMode().hideSpinner();
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				//do nothing
			}
			else
			{
				alert(response.message);
			}
		}
	});
}

/**
 * Show fonts editor
 */
function designMode_fontsEditor()
{
	DesignMode().showSpinner("Loading data");
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'getElementsCssMap',
		'files': 1
	}, function(response){
		DesignMode().hideSpinner();
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				$("#design-dialog-fonts-editor").data("map", response.map);
				$('#design-dialog-fonts-editor').dialog("open");
			}
			else
			{
				alert(response.message);
			}
		}
	});
}

/**
 * Save fonts
 * @return
 */
function designMode_fontsEditorSave()
{
	DesignMode().showSpinner("Saving data");
	
	//comparing existing styles with changed
	var saveChanges = false;
	var map = $("#design-dialog-fonts-editor").data("map");
	jQuery.each(map.fonts, function(groupKey, groupData){
		jQuery.each(groupData.elements, function(elementKey, elementData){
			//edited styles
			var new_ffamily = $("#font-family-" + groupKey + "-" + elementKey).val();
			var new_fsize = $("#font-size-" + groupKey + "-" + elementKey).val();
			var new_fweight = $("#font-weight-" + groupKey + "-" + elementKey).is(':checked') ? 'bold' : 'normal';
			var new_fstyle = $("#font-style-" + groupKey + "-" + elementKey).is(':checked') ? 'italic' : 'normal';
			var new_tdecoration = $("#text-decoration-" + groupKey + "-" + elementKey).is(':checked') ? 'underline' : 'none';
			
			//compare
			var changed = false;
			var ffamilyChanged = false;
			var fsizeChanged = false;
			var fweightChanged = false;
			var fstyleChanged = false;
			var tdecorationChanged = false;
			
			if ($("#font-family-" + groupKey + "-" + elementKey).hasClass("changed")){ffamilyChanged = true;changed = true;}
			if ($("#font-size-" + groupKey + "-" + elementKey).hasClass("changed")) {fsizeChanged = true;changed = true;}
			if ($("#font-weight-" + groupKey + "-" + elementKey).hasClass("changed")) {fweightChanged = true;changed = true;}
			if ($("#font-style-" + groupKey + "-" + elementKey).hasClass("changed")) {fstyleChanged = true;changed = true;}
			if ($("#text-decoration-" + groupKey + "-" + elementKey).hasClass("changed")) {tdecorationChanged = true;changed = true;}
			
			var el = map["fonts"][groupKey]["elements"][elementKey];
			
			if (changed)
			{
				saveChanges = true;
				if (el["css"])
				{
					var css = el["css"];
					var cssArray = new Array();
					$.parsecss(css, function(parsedCss){
						cssArray = parsedCss;
					});

					var css = "";
					for (selector in cssArray)
					{
						if (selector != "containsValue" && selector != "indexOf" && selector == el["selector"])
						{
							css = css + selector + "\n{\n";
							
							var attrExists = false;
							
							for (attr in cssArray[selector])
							{
								if (
									attr != "containsValue" && 
									attr != "indexOf" && 
									cssArray[selector][attr] != null && 
									$.trim(cssArray[selector][attr]) != "" && 
									cssArray[selector][attr] != "inherit"
								)
								{
									//tricky part to put font changes at the end of css
									if (
										((attr == "font-family") && ffamilyChanged) ||
										((attr == "font-size") && fsizeChanged) ||
										((attr == "font-weight") && fweightChanged) ||
										((attr == "font-style") && fstyleChanged) ||
										((attr == "text-decoration") && tdecorationChanged)
									)
									{
										attrExists = true;
									}
									else
									{
										css = css + "\t" + attr + ":" + cssArray[selector][attr] + ";\n";
									}
								}
							}
							if (ffamilyChanged) css = css + "\tfont-family:" + new_ffamily + ";\n";
							if (fsizeChanged) css = css + "\tfont-size:" + new_fsize + ";\n";
							if (fweightChanged) css = css + "\tfont-weight:" + new_fweight + ";\n";
							if (fstyleChanged) css = css + "\tfont-style:" + new_fstyle + ";\n";
							if (tdecorationChanged) css = css + "\ttext-decoration:" + new_tdecoration + ";\n";
							css = css + "}\n";
							//alert(css);
						}
					}
				}
				else
				{
					//generate new css file for elements
					var css = el["selector"] + "\n{\n";
					if (ffamilyChanged) css = css + "\tfont-family:" + new_ffamily + ";\n";
					if (fsizeChanged) css = css + "\tfont-size:" + new_fsize + ";\n";
					if (fweightChanged) css = css + "\tfont-weight:" + new_fweight + ";\n";
					if (fstyleChanged) css = css + "\tfont-style:" + new_fstyle + ";\n";
					if (tdecorationChanged) css = css + "\ttext-decoration:" + new_tdecoration + ";\n";
					css = css + "}\n";
				}
				//update css in elements map
				jQuery.each(map.fonts, function(_groupKey, _groupData){
					jQuery.each(_groupData.elements, function(_elementKey, _elementData){
						if (_elementData["selector"] == el["selector"])
						{
							map["fonts"][_groupKey]["elements"][_elementKey]["css"] = css;
						}
					});
				});
			}
		});
	});
	$("#design-dialog-fonts-editor").data("map", map);
	
	if (saveChanges)
	{
		$.post(
			//url
			site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
			//data
			{
				'action' : 'saveFonts',
				'data' : JSON.stringify($("#design-dialog-fonts-editor").data("map"))
			},
			//after post
			function(response){
				//parse response
				var response = JSON.parse(response);
				if (response.status)
				{
					$('#css-designmode').attr('href', response.cssFileUrl);
				}
				$("#design-dialog-fonts-editor").data("changes-made", false);
				DesignMode().hideSpinner();
			}
		);
	}
	else
	{
		DesignMode().hideSpinner();
	}
}

/**
 * Show colors editor
 */
function designMode_colorsEditor()
{
	DesignMode().showSpinner("Loading data");
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'getElementsCssMap',
		'files': 1
	}, function(response){
		DesignMode().hideSpinner();
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				$("#design-dialog-colors-editor").data("map", response.map);
				$('#design-dialog-colors-editor').dialog("open");
			}
			else
			{
				alert(response.message);
			}
		}
	});
}

/**
 * Save colors
 */
function designMode_colorsEditorSave()
{
	if ($("#design-dialog-colors-editor").data("changes-made"))
	{
		DesignMode().showSpinner("Saving data");
		
		var map = $("#design-dialog-colors-editor").data("map");
		
		$("#design-dialog-colors-editor .color-editor-input.changed").each(function(){
			//extract indexes
			var newColor = $(this).val();
			var s = $(this).attr("id");
			var parts = s.split("-");
			var groupKey = parts[1];
			var elementKey = parts[2];
			var el = map["colors"][groupKey]["elements"][elementKey];
			
			if (el["css"])
			{
				var css = el["css"];
				var cssArray = new Array();
				$.parsecss(css, function(parsedCss){
					cssArray = parsedCss;
				});
				
				var css = "";
				for (selector in cssArray)
				{
					if (selector != "containsValue" && selector != "indexOf" && selector == el["selector"])
					{
						css = css + selector + "\n{\n";
						
						var attrExists = false;
						
						for (attr in cssArray[selector])
						{
							if (
								attr != "containsValue" && 
								attr != "indexOf" && 
								cssArray[selector][attr] != null && 
								$.trim(cssArray[selector][attr]) != "" && 
								cssArray[selector][attr] != "inherit"
							)
							{
								//tricky part to put colors last
								if ((el["type"] == "cl" && attr == "color") || (el["type"] == "bg" && attr == "background-color"))
								{
									attrExists = true;
								}
								else
								{
									css = css + "\t" + attr + ":" + cssArray[selector][attr] + ";\n";
								}
							}
						}
						if (el["type"] == "cl")
						{
							css = css + "\tcolor:" + newColor + ";\n";
						}
						if (el["type"] == "bg")
						{
							css = css + "\tbackground-color:" + newColor + ";\n";
						}
						css = css + "}\n";
						//alert(css);
					}
				}
			}
			else
			{
				var css = el["selector"] + "{\n";
				if (el["type"] == "cl")
				{
					css = css + "\tcolor:" + newColor + ";\n";
				}
				if (el["type"] == "bg")
				{
					css = css + "\tbackground-color:" + newColor + ";\n";
				}
				css = css + "}\n";
			}
			
			//map["colors"][groupKey]["elements"][elementKey]["css"] = css;
			
			jQuery.each(map.colors, function(_groupKey, _groupData){
				jQuery.each(_groupData.elements, function(_elementKey, _elementData){
					if (_elementData["selector"] == el["selector"])
					{
						map["colors"][_groupKey]["elements"][_elementKey]["css"] = css;
					}
				});
			});
			
		});
		
		$("#design-dialog-colors-editor").data("map", map);
		
		$.post(
			//url
			site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
			//data
			{
				'action' : 'saveColors',
				'data' : JSON.stringify($("#design-dialog-colors-editor").data("map"))
			},
			//after post
			function(response){
				//parse response
				var response = JSON.parse(response);
				if (response.status)
				{
					$('#css-designmode').attr('href', response.cssFileUrl);
				}
				$("#design-dialog-colors-editor").data("changes-made", false);
				DesignMode().hideSpinner();
			}
		);
	}
}

/**
 * Shows styles editor dialog
 * @return
 */
function designMode_stylesEditor()
{
	DesignMode().showSpinner("Loading data");
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'getElementsCssMap',
		'files': 0
	}, function(response){
		DesignMode().hideSpinner();
		$("#design-dialog-styles-editor-map").html('');
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				$("#design-dialog-styles-editor").data("map", response.map);
				jQuery.each(response.map.groups, function(group, groupData){
					/**
					 * Add group
					 */
					$('<li/>')
						.html('<span>'+groupData.title+'</span>')
						.data('group', group)
						.attr('id', 'design-dialog-styles-editor-map-group-' + group)
						.appendTo('#design-dialog-styles-editor-map');
					
					/**
					 * Add groups container
					 */
					$('<ul/>')
						.attr('id', 'design-dialog-styles-editor-map-group-' + group + '-container')
						.addClass('invisible')
						.appendTo('#design-dialog-styles-editor-map-group-' + group);
					
					/**
					 * Add groups elements
					 */
					jQuery.each(groupData.elements, function(elementNum, element){
						/**
						 * Add groups element
						 */
						$('<li/>')
							.html('<span>'+element.title+'</span>')
							.attr('id', 'design-dialog-styles-editor-map-group-' + group + '-element-' + elementNum)
							.appendTo('#design-dialog-styles-editor-map-group-' + group + '-container');
						/**
						 * Add groups subelements container
						 */
						$('<ul/>')
							.attr('id', 'design-dialog-styles-editor-map-group-' + group + '-element-' + elementNum + '-container')
							//.addClass('invisible')
							.appendTo('#design-dialog-styles-editor-map-group-' + group + '-element-' + elementNum);
						
						/**
						 * Add type - specific subelements to element
						 */
						if (
							element.type != "undefined" && 
							element.type != null && 
							response.map.types[element.type] != "undefined" &&
							response.map.types[element.type] != null
						)
						{
							jQuery.each(response.map.types[element.type].elements, function(subElementNum, subElement){
								DesignMode().stylesEditorAddNode(group, elementNum, element, subElementNum, subElement);
							});
						}
						/**
						 * Add element - specific subelements to element
						 */
						if (
							element.elements != "undefined" && 
							element.elements != null
						)
						{
							jQuery.each(element.elements, function(subElementNum, subElement){
								DesignMode().stylesEditorAddNode(group, elementNum, element, subElementNum, subElement);
							});
						}
					});					
				});
				
				$('#design-dialog-styles-editor-map').treeview({
					collapsed: true
				});
				
				$('#design-dialog-styles-editor').dialog("open");
				
				//if editor opened from drop-down, click to edit it 
				if (
					$('#design-dialog-styles-editor').data("selected-element") != null && 
					$('#design-dialog-styles-editor').data("selected-element") != "undefined" &&
					$('#design-dialog-styles-editor').data("selected-element")
				)
				{
					var selectedElement = $('#design-dialog-styles-editor').data("selected-element");
					var selectedElementSubselector = $('#design-dialog-styles-editor').data("selected-element-subselector");
					var selectedElementClass = DesignMode().getDesignElementCssClass(selectedElement);
					var selectedElementClassSelector = '.' + selectedElementClass;
					var designElementClass = DesignMode().getDesignElementClass(selectedElement);
					var designElementRealClass = DesignMode().getDesignElementRealClass(designElementClass);
					var selectedElementTag = $(selectedElement)[0].tagName.toLowerCase();
					
					/**
					console.log($(selectedElement)[0].tagName);
					console.log('selectedElementClass:' + selectedElementClass);
					console.log('selectedElementClassSelector:' + selectedElementClassSelector);
					console.log('designElementClass:' + designElementClass);
					console.log('designElementRealClass:' + designElementRealClass);
					console.log('selectedElementTag:' + selectedElementTag);
					console.log('alternative 1:' + selectedElementTag + '.' + designElementRealClass + ' ' + selectedElementSubselector);
					**/

					var selectorMatchNode = false;
					var parentSelectorMatchNode = false;
					var selectorParentSelectorMatchNode = false;
					var tagParentSelectorMatchNode = false;
					var classMatch = false;
					var realClassMatchNode = false;
					var subSelectMatch = false;
					var subSelectMatchNode = false;
					var bodyClasses = $('body').attr('class').split(' ');

					$("#design-dialog-styles-editor-map li.subelement-node").each(function(){
						var theNode = $(this);
						//match by subselect
						
						if (selectedElementSubselector != "")
						{
							var _alt1r = selectedElementTag + '.' + designElementRealClass + selectedElementSubselector;
							var _alt1l = theNode.data('parent-selector') + ' ' + theNode.data('selector');
							var _alt2r = '.page ' + selectedElementClassSelector + selectedElementSubselector;
							var _alt2l = theNode.data('parent-selector') + ' ' + theNode.data('selector');
							var _alt3r = selectedElementTag + selectedElementClassSelector + selectedElementSubselector;
							var _alt3l = theNode.data('parent-selector') + ' ' + theNode.data('selector');

							if (_alt1l == _alt1r || _alt2l == _alt2r || _alt3l == _alt3r)
							{
								subSelectMatchNode = theNode;
							}
							if (subSelectMatchNode === false) {
								$.each(bodyClasses, function(idx, bodyClass) {
									var _alt2r = '.'+bodyClass + ' ' + selectedElementClassSelector + selectedElementSubselector;
									if (_alt2l == _alt2r) {
										subSelectMatchNode = theNode;
									}
								});
							}
						}
						
						
						//match by class and parent class
						if (
							!selectorParentSelectorMatchNode && 
							$(this).data('class') == selectedElementClass &&
							$(this).data('parent-class') == designElementRealClass
						)
						{
							selectorParentSelectorMatchNode = this;
						}
						
						//match by tag name and parent class
						if (
							!selectorParentSelectorMatchNode && 
							$(this).data('selector') == selectedElementTag &&
							$(this).data('parent-class') == designElementRealClass
						)
						{
							tagParentSelectorMatchNode = this;
						}
						
						//match by selector
						if (!selectorMatchNode && $(this).data('selector') == selectedElementClassSelector)
						{
							selectorMatchNode = this;
						}
						
						//match by parent class
						if (!parentSelectorMatchNode && $(this).data('parent-class') == selectedElementClass)
						{
							parentSelectorMatchNode = this;
						}
						
						//match by class
						if (!classMatch && $(this).data('class') == selectedElementClass)
						{
							classMatch = this;
						}
						
						//match by parent
						if (!realClassMatchNode && $(this).data('class') == designElementRealClass)
						{
							realClassMatchNode = this;
						}

						//
					});
					
					var nodeToEdit = false;
					if (subSelectMatchNode) nodeToEdit = subSelectMatchNode;
					else if (selectorParentSelectorMatchNode) nodeToEdit = selectorParentSelectorMatchNode;
					else if (tagParentSelectorMatchNode) nodeToEdit = tagParentSelectorMatchNode;
					else if (selectorMatchNode) nodeToEdit = selectorMatchNode;
					else if (parentSelectorMatchNode) nodeToEdit = parentSelectorMatchNode;
					else if (classMatch) nodeToEdit = classMatch;
					else if (realClassMatchNode) nodeToEdit = realClassMatchNode;
					
					
					/**
					console.log('node:\nparent class:' + $(nodeToEdit).data('parent-class') + '\n' +
							'parent seelctor:' + $(nodeToEdit).data('parent-selector') + '\n' +
							'class:' + $(nodeToEdit).data('class') + '\n' +
							'selector:' + $(nodeToEdit).data('selector'));
					**/
					
					if (nodeToEdit)
					{
						$(nodeToEdit).parents('li:first').removeClass('expandable').addClass('collapsible');
						$(nodeToEdit).parents('li:first').find(".hitarea:first").removeClass('expandable-hitarea').addClass('collapsible-area');
						$(nodeToEdit).parents('li:first').find("ul:first").show();
						$(nodeToEdit).parents('li:first').parents('li:first').removeClass('expandable').addClass('collapsible');
						$(nodeToEdit).parents('li:first').parents('li:first').find(".hitarea:first").removeClass('expandable-hitarea').addClass('collapsible-area');
						$(nodeToEdit).parents('li:first').parents('li:first').find("ul:first").show();
						DesignMode().stylesEditorSelectElement(nodeToEdit);
					}
				}
			}
			else
			{
				alert(response.message);
			}
		}
	});
}

/**
 * Add new low level node to a tree
 * @param element
 * @param subElement
 * @return
 */
function designMode_stylesEditorAddNode(group, elementNum, element, subElementNum, subElement)
{
	$('<li/>')
		.html(subElement.title)
		.data('parent-class', element["parent-class"])
		.data('parent-selector', element["parent-selector"])
		.data('class', subElement["class"])
		.data('selector', subElement["selector"])
		.data('preview', subElement["preview"])
		.data('editors', subElement["editors"])
		.addClass('subelement-node')
		.appendTo('#design-dialog-styles-editor-map-group-' + group + '-element-' + elementNum + '-container')
		.click(function(event){
			var clickedNode = this;
			event.stopPropagation();
			if ($("#design-dialog-styles-editor").data("changes-made"))
			{
				if (confirm("Do you want to save changes?"))
				{
					$.post(
						// url
						site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
						// data
						{
							'action'			: 'saveElementStyles',
							'parent-class'		: $("#design-dialog-styles-editor-map li.selected-element").data('parent-class'),
							'parent-selector'	: $("#design-dialog-styles-editor-map li.selected-element").data('parent-selector'),
							'class'				: $("#design-dialog-styles-editor-map li.selected-element").data('class'),
							'selector'			: $("#design-dialog-styles-editor-map li.selected-element").data('selector'),
							'css'				: DesignMode().stylesEditorRenderCss()
						},
						//after post
						function(response){
							//parse response
							var response = JSON.parse(response);
							if (response.status)
							{
								$('#css-designmode').attr('href', response.cssFileUrl);
							}
							$("#design-dialog-styles-editor").data("changes-made", false);
							DesignMode().stylesEditorSelectElement(clickedNode);
						}
					);
				}
				else
				{
					DesignMode().stylesEditorSelectElement(this);
				}
			}
			else
			{
				DesignMode().stylesEditorSelectElement(this);
			}
			$("#design-dialog-styles-editor").data("changes-made", false);
		});
}

/**
 * Create image restore / delete handlers
 * @return
 */
function designMode_stylesEditorManageImage()
{
	$("#design-mode-styles-editor-button-delete-image").click(function(){
		if (confirm("Do you want to do it?\nNote: you will not be able to restore custom image!"))
		{
			$.post(
				//url
				site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
				//data
				{
					'action' : 'deleteCustomImage',
					'class'	: $("#design-dialog-styles-editor-map .selected-element").data("class")
				},
				//after post
				function(response){
					$("#design-dialog-styles-editor").data("changes-made", true);
					$("#design-dialog-styles-editor").data("changes-made-require-refresh", true);
					var response = JSON.parse(response);
					if (response.status)
					{
						DesignMode().stylesEditorSelectElement($("#design-dialog-styles-editor-map li.selected-element"));
					}
				}
			);		
		}
	});
	$("#design-mode-styles-editor-button-hide-image").click(function(){
		if (confirm("Do you really want to hide selected image?"))
		{
			$.post(
				//url
				site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
				//data
				{
					'action' : 'hideImage',
					'class'	: $("#design-dialog-styles-editor-map .selected-element").data("class")
				},
				//after post
				function(response){
					$("#design-dialog-styles-editor").data("changes-made", true);
					$("#design-dialog-styles-editor").data("changes-made-require-refresh", true);
					var response = JSON.parse(response);
					if (response.status)
					{
						$("." + $("#design-dialog-styles-editor-map .selected-element").data("class")).hide("slow");
						DesignMode().stylesEditorSelectElement($("#design-dialog-styles-editor-map li.selected-element"));
					}
				}
			);		
		}
	});
	$("#design-mode-styles-editor-button-show-image").click(function(){
		if (confirm("Do you really want to show selected image?"))
		{
			$.post(
				//url
				site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
				//data
				{
					'action' : 'showImage',
					'class'	: $("#design-dialog-styles-editor-map .selected-element").data("class")
				},
				//after post
				function(response){
					$("#design-dialog-styles-editor").data("changes-made", true);
					$("#design-dialog-styles-editor").data("changes-made-require-refresh", true);
					var response = JSON.parse(response);
					if (response.status)
					{
						$("." + $("#design-dialog-styles-editor-map .selected-element").data("class")).show("slow");
						DesignMode().stylesEditorSelectElement($("#design-dialog-styles-editor-map li.selected-element"));
					}
				}
			);		
		}
	});
}

/**
 * Setup selected element for editing
 * @param el
 */
function designMode_stylesEditorSelectElement(el)
{
	DesignMode().showSpinner("Loading styles");
	$("#design-dialog-styles-editor-css").val('');
	
	//unset and set active tree node
	$("#design-dialog-styles-editor-map li.selected-element").css('font-weight', 'normal').removeClass('selected-element');
	$(el).css('font-weight', 'bold').addClass("selected-element");
	
	//get selector	
	var _selectors = $(el).data('selector').split(',');
	var _previewSelector = "";
	var _globalPreviewSelector = "";
	for (i=0; i<_selectors.length; i++)
	{
		//_previewSelector = _previewSelector + (i>0?",":"")  + "#design-dialog-styles-editor-preview " + $(el).data('parent-selector') + ' ' + _selectors[i];
		_previewSelector = _previewSelector + (i>0?",":"")  + "#design-dialog-styles-editor-preview " + $(el).data('parent-selector') + ' ' + _selectors[i];
		_globalPreviewSelector = _globalPreviewSelector + (i>0?",":"")  + "#page-preview " + $(el).data('parent-selector') + ' ' + _selectors[i];
	}
	$("#design-dialog-styles-editor").data("preview-selector", _previewSelector);
	$("#design-dialog-styles-editor").data("global-preview-selector", _globalPreviewSelector);
	
	//set preview for element
	$("#design-dialog-styles-editor-preview").html($("#design-dialog-styles-editor").data("map").previews[$(el).data('preview')]);
	
	$("#design-dialog-styles-editor-preview .preview-parent").addClass($(el).data('parent-class'));
	if ($("#design-dialog-styles-editor-preview").hasClass('preview-element'))
	{
		$("#design-dialog-styles-editor-preview .preview-parent").addClass($(el).data('class'));
	}
	
	if ($(el).data('class') != "" && $(el).data('class') != "undefined" && $(el).data('class') != false && $(el).data('class') != null)
	{
		$("#design-dialog-styles-editor-preview .preview-element").addClass($(el).data('class'));
	}
	
	//check for css file on a server
	$.post(
		//url
		site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', 
		//data
		{
			'action'			: 'getElementStyles',
			'parent-class'		: $(el).data('parent-class'),
			'parent-selector'	: $(el).data('parent-selector'),
			'class'				: $(el).data('class'),
			'selector'			: $(el).data('selector')
		},
		//after post
		function(response){
			//preview selector
			var previewSelector = $($("#design-dialog-styles-editor").data("preview-selector"));
			var globalPreviewSelector = $($("#design-dialog-styles-editor").data("global-preview-selector"));
			
			var doRenderCss = false;
			//parse response
			var response = JSON.parse(response);
			if (response.css)
			{
				//if css file exists, set it to textarea
				$("#design-dialog-styles-editor-css").val(response.css);
				$.parsecss(response.css, function(parsedCss){
					//set additional styles from css file - override current global styles
					jQuery.each(parsedCss, function(cssSelector, cssProperties){
						var styles = "";
						jQuery.each(cssProperties, function(cssProperty, cssPropertyValue){
							if (cssProperty == 'background-image')
							{
								//change relative path to full path and add something after ? to refresh
								cssPropertyValue = cssPropertyValue.replace("')", "?" + Math.floor(Math.random()*10000) + "')");
								cssPropertyValue = cssPropertyValue.replace("./../", site_https_url + "/content/skins/_custom/skin/");
							}
							style = styles + cssProperty + ':' + cssPropertyValue + ';';
						});
						$("#design-dialog-styles-editor-preview " + cssSelector).attr('styles', styles);
					});
				});
			}
			else
			{
				//if file does not exist, render it from visual styles
				doRenderCss = true;
				//alert(3);
			}
			
			if (response.image)// && $("#design-dialog-styles-editor-preview .image").length > 0)
			{
				
				$("#design-dialog-styles-editor-preview").html($("#design-dialog-styles-editor").data("map").previews["image"]);
				$("#design-dialog-styles-editor-preview .image").attr("src", response.image.content + "?" + Math.floor(Math.random()*100000));
				
				//show image info
				var info = "";
				var addButtons = false;
				if (response.imageInfo.custom)
				{
					info += '&nbsp;<br/><b>This is custom image</b>' + (response.image.type == "image-hidden" ? "<br/><u>Note:this image is hidden now</u><br/>" : "") + '<br/>';
					info += '<input type="button" value="' + (response.imageInfo.skin ? "Use skin image" : "Delete image") + '" id="design-mode-styles-editor-button-delete-image"/> ';
					addButtons = true;
				}
				else if (response.imageInfo.skin)
				{
					info += '&nbsp;<br/><b>This is skin-level image</b>' + (response.image.type == "image-hidden" ? "<br/><u>Note:this image is hidden now</u><br/>" : "") + '<br/>';
					addButtons = true;
				}
				else if (response.imageInfo.base)
				{
					info += '&nbsp;<br/><b>This is base image</b>' + (response.image.type == "image-hidden" ? "<br/><u>Note:this image is hidden now</u><br/>" : "") + '<br/>';
					addButtons = true;
				}
				
				if (addButtons)
				{
					if (response.image.type == "image")
					{
						info += '<input type="button" value="Hide image" id="design-mode-styles-editor-button-hide-image"/> ';
					}
					else if (response.image.type == "image-hidden")
					{
						info += '<input type="button" value="Show image" id="design-mode-styles-editor-button-show-image"/> ';
					}
				}
				
				$("#design-dialog-styles-editor-preview .preview-image-info").html(info);
				
				DesignMode().stylesEditorManageImage();
			}
			
			//show CSS editors and preset values to visual editor
			$("#design-dialog-styles-editor .design-dialog-style-editor").removeClass("editor-active").hide();
			
			var previewSelectorToUse = doRenderCss && $(globalPreviewSelector).length > 0 ? globalPreviewSelector : previewSelector;
			
			//alert(doRenderCss && $(globalPreviewSelector).length > 0 ? "globalPreviewSelector" : "previewSelector");
			
			//assign editors
			jQuery.each($(el).data("editors"), function(editorNum, editor){
				$("#design-dialog-styles-editor-" + editor).addClass("editor-active").show();
				switch (editor)
				{
					//font
					case "font" :
					{
						//get font data from visual element
						$("#design-dialog-styles-editor-font-family").editableSelectSetValue($(previewSelectorToUse).css("font-family"));
						$("#design-dialog-styles-editor-font-size").editableSelectSetValue($(previewSelectorToUse).css("font-size"));

						var c = $(previewSelectorToUse).css("color");
						if (c != 'transparent' && c != 'rgba(0, 0, 0, 0)')
						{
							$("#design-dialog-styles-editor-text-color").val(rgbToHex(c));
						}
						else
						{
							$("#design-dialog-styles-editor-text-color").val('');
						}

						var w = $(previewSelectorToUse).css("font-weight");
						if (w == '400') w = 'normal';
						if (w == '700') w = 'bold';
						$("#design-dialog-styles-editor-font-weight").editableSelectSetValue(w);
						$("#design-dialog-styles-editor-font-style").editableSelectSetValue($(previewSelectorToUse).css("font-style"));
						$("#design-dialog-styles-editor-text-decoration").editableSelectSetValue($(previewSelectorToUse).css("text-decoration"));
						break;
					}
					
					//align
					case "align" :
					{
						$("#design-dialog-styles-editor-text-align").editableSelectSetValue($(previewSelectorToUse).css("text-align"));
						break;
					}
					//background
					case "background" :
					{
						var c = $(previewSelectorToUse).css("background-color");
						if (c != 'transparent' && c != 'rgba(0, 0, 0, 0)')
						{
							$("#design-dialog-styles-editor-background-color").val(rgbToHex(c));
						}
						else
						{
							$("#design-dialog-styles-editor-background-color").val('');
						}
						
						var bg = $(previewSelectorToUse).css("background-image");
						
						if (bg != 'undefined' && bg != null && bg != 'none')
						{
							if (bg.indexOf('("') >=0) bg = bg.replace('("', "('"); 
							else if (bg.indexOf("('") < 0) bg = bg.replace('(', "('");
							if (bg.indexOf('")') >=0) bg = bg.replace('")', "')"); 
							else if (bg.indexOf("')") < 0) bg = bg.replace(')', "')");
							
							$("#design-dialog-styles-editor-background-alligment").editableSelectSetValue($(previewSelectorToUse).css("background-alligment"));
							$("#design-dialog-styles-editor-background-position").editableSelectSetValue($(previewSelectorToUse).css("background-position"));
							$("#design-dialog-styles-editor-background-repeat").editableSelectSetValue($(previewSelectorToUse).css("background-repeat"));
							
							$("#design-dialog-styles-editor-background-image").val(bg);
							$("#design-dialog-styles-editor-background-image-exists").attr("checked", true);
						}
						else
						{
							$("#design-dialog-styles-editor-background-image").val("none");
							$("#design-dialog-styles-editor-background-image-exists").removeAttr("checked", true);
						}
						break;
					}
				}
			});
			
			if (doRenderCss)
			{
				DesignMode().stylesEditorVisualPreview();
				
				$("#design-dialog-styles-editor-css").val('');
				$("#design-dialog-styles-editor-css").val(DesignMode().stylesEditorRenderCss());
			}
			$("#design-dialog-styles-editor-start-text").hide();
			$("#design-dialog-styles-editor-tabs").show();
			DesignMode().hideSpinner();
			$("#design-dialog-styles-editor").data("changes-made", false);
		}
	);
}

/**
 * Preview css file change
 * @return
 */
function designMode_stylesEditorCssPreview()
{
	//get preview selector
	var css = $("#design-dialog-styles-editor-css").val();
	if (css != "")
	{
		//if css file exists, set it to textarea
		$.parsecss(css, function(parsedCss){
			//set additional styles from css file - override current global styles
			jQuery.each(parsedCss, function(cssSelector, cssProperties){
				var style = "";
				jQuery.each(cssProperties, function(cssProperty, cssPropertyValue){
					if (cssProperty == 'background-image')
					{
						cssPropertyValue = cssPropertyValue.replace("')", "?" + Math.floor(Math.random()*10000) + "')");
						
						if (cssPropertyValue.indexOf('./../') >= 0) 
						{
							cssPropertyValue = cssPropertyValue.replace("./../", "content/cache/skins/" + skin_name + "/");
						}
						else if (!/^http|https\:\/\/?$/.test(cssPropertyValue))
						{
							cssPropertyValue = cssPropertyValue.replace("./", "");
						}
					}
					style = style + cssProperty + ':' + cssPropertyValue + ';';
				});
				$("#design-dialog-styles-editor-preview " + cssSelector).attr('style', style);
			});
		});
	}
}

/**
 * Set prevoew 
 * @return
 */
function designMode_stylesEditorVisualPreview()
{
	var style = "";
	//common - font
	if ($("#design-dialog-styles-editor-font").hasClass('editor-active'))
	{
		
		style = style + 
			($.trim($("#design-dialog-styles-editor-font-family").val()) != "" ? ("font-family:" + $("#design-dialog-styles-editor-font-family").val() + ";") : "") +
			($.trim($("#design-dialog-styles-editor-font-size").val()) != "" ? ("font-size:" + $("#design-dialog-styles-editor-font-size").val() + ";") : "") +
			($.trim($("#design-dialog-styles-editor-font-weight").val()) != "" ? ("font-weight:" + $("#design-dialog-styles-editor-font-weight").val() + ";") : "") +			
			($.trim($("#design-dialog-styles-editor-font-style").val()) != "" ? ("font-style:" + $("#design-dialog-styles-editor-font-style").val() + ";") : "") +			
			($.trim($("#design-dialog-styles-editor-text-decoration").val()) != "" ? ("text-decoration:" + $("#design-dialog-styles-editor-text-decoration").val() + ";") : "") +
			($.trim($("#design-dialog-styles-editor-text-color").val()) != "" ? ("color:" + $("#design-dialog-styles-editor-text-color").val() + ";") : "");
	}
	//align
	if ($("#design-dialog-styles-editor-align").hasClass('editor-active') && $.trim($("#design-dialog-styles-editor-text-align").val()) != "")
	{
		style = style +
			"text-align:" + $("#design-dialog-styles-editor-text-align").val() + ";";
	}
	//background
	if ($("#design-dialog-styles-editor-background").hasClass('editor-active'))
	{
		if ($("#design-dialog-styles-editor-background-image-exists").is(":checked"))
		{
			bg = $("#design-dialog-styles-editor-background-image").val();
			if (bg != "")
			{
				style = style + 
					"background-image:" + bg + ";" +
					"background-attachment:" + $("#design-dialog-styles-editor-background-attachment").val() + ";" +
					"background-position:" + $("#design-dialog-styles-editor-background-position").val() + ";" +
					"background-repeat:" + $("#design-dialog-styles-editor-background-repeat").val() + ";" +
					"background-color:" + $("#design-dialog-styles-editor-background-color").val() + ";";
				
			}
		}
		else
		{
			style = style + "background:none;";
			if($.trim($("#design-dialog-styles-editor-background-color").val()) != "")
			{ 
				style = style + "background-color:" + $("#design-dialog-styles-editor-background-color").val() + ";";
			}
		}
	}
	$($("#design-dialog-styles-editor").data("preview-selector")).attr("style", style);
}

/**
 * Renders css based on user options
 * @return string
 */
function designMode_stylesEditorRenderCss()
{
	//parse current css
	var cssArray = new Array();
	var css = $("#design-dialog-styles-editor-css").val();
	
	if ($.trim(css) != '')
	{
		$.parsecss(css, function(parsedCss){
			cssArray = parsedCss;
		});
	}
	
	//get selector	
	var _selectors = $("#design-dialog-styles-editor-map .selected-element").data("selector").split(',');
	var _selector = "";
	for (i=0; i<_selectors.length; i++)
	{
		_selector = _selector + (i>0?",":"") + 
			$("#design-dialog-styles-editor-map .selected-element").data("parent-selector") + " " + _selectors[i];
	}
	
	var selector = $.trim(_selector);
		//$("#design-dialog-styles-editor-map .selected-element").data("parent-selector") + " " +
		//$("#design-dialog-styles-editor-map .selected-element").data("selector");
	
	
	if (cssArray[selector] == null || cssArray[selector] == 'undefined') cssArray[selector] = new Array();
	
	//common - font
	if ($("#design-dialog-styles-editor-font").hasClass('editor-active'))
	{
		cssArray[selector]['font-family'] = $("#design-dialog-styles-editor-font-family").val();
		cssArray[selector]['font-size'] = $("#design-dialog-styles-editor-font-size").val();
		cssArray[selector]['color'] = $("#design-dialog-styles-editor-text-color").val();
		cssArray[selector]['font-weight'] = $("#design-dialog-styles-editor-font-weight").val();
		cssArray[selector]['font-style'] = $("#design-dialog-styles-editor-font-style").val();
		cssArray[selector]['text-decoration'] = $("#design-dialog-styles-editor-text-decoration").val();
	}
	//align
	if ($("#design-dialog-styles-editor-align").hasClass('editor-active'))
	{
		cssArray[selector]['text-align'] = $("#design-dialog-styles-editor-text-align").val();
	}
	//background
	if ($("#design-dialog-styles-editor-background").hasClass('editor-active'))
	{
		cssArray[selector]['background-color'] = $("#design-dialog-styles-editor-background-color").val();
		
		if ($("#design-dialog-styles-editor-background-image-exists").is(":checked"))
		{
			
			//transform preview bg to css relative path
			var bg = $("#design-dialog-styles-editor-background-image").val();
			
			if ((p = bg.indexOf(skin_images)) >= 0)
			{
				bg = bg.substring(p + skin_images.length);
				var i = bg.indexOf('?');
				if (i >= 0) bg = bg.substring(0, i) + "')";
				bg = "url('./../images/" + bg;
			}
			else if ((p = bg.indexOf(skin_themes)) >= 0)
			{
				bg = bg.substring(p + skin_themes.length);
				var i = bg.indexOf('?');
				if (i >= 0) bg = bg.substring(0, i) + "')";
				bg = "url('./" + bg;
			}
			
			cssArray[selector]['background-image'] = bg;
			cssArray[selector]['background-attachment'] = $("#design-dialog-styles-editor-background-attachment").val();
			cssArray[selector]['background-position'] = $("#design-dialog-styles-editor-background-position").val();
			cssArray[selector]['background-repeat'] = $("#design-dialog-styles-editor-background-repeat").val();
		}
		else
		{
			cssArray[selector]['background-image'] = 'none';
			cssArray[selector]['background-attachment'] = null;;
			cssArray[selector]['background-position'] = null;;
			cssArray[selector]['background-repeat'] = null;;
		}
	}
	
	var css = "";
	for (selector in cssArray)
	{
		if (selector != "containsValue" && selector != "indexOf")
		{
			css = css + selector + "\n{\n";
		
			for (attr in cssArray[selector])
			{
				if (
					attr != "containsValue" && 
					attr != "indexOf" && 
					cssArray[selector][attr] != null && 
					$.trim(cssArray[selector][attr]) != "" && 
					cssArray[selector][attr] != "inherit"
				)
				{
					css = css + "\t" + attr + ":" + cssArray[selector][attr] + ";\n";
				}
			}
			
			css = css + "}\n";
		}
	}
	return css;
}

/**
 * Set values to editors in visual mode
 * @return
 */
function designMode_stylesEditorSetVisualEditors()
{
	//assign editors
	var previewSelector = $($("#design-dialog-styles-editor").data("preview-selector"));
	jQuery.each($("#design-dialog-styles-editor-map li.selected-element").data("editors"), function(editorNum, editor){
		$("#design-dialog-styles-editor-" + editor).addClass("editor-active").show();
		switch (editor)
		{
			//font
			case "font" :
			{
				//get font data from visual element
				$("#design-dialog-styles-editor-font-family").editableSelectSetValue($(previewSelector).css("font-family"));
				$("#design-dialog-styles-editor-font-size").editableSelectSetValue($(previewSelector).css("font-size"));
				$("#design-dialog-styles-editor-text-color").val(rgbToHex($(previewSelector).css("color")));
				var w = $(previewSelector).css("font-weight");
				if (w == '400') w = 'normal';
				if (w == '700') w = 'bold';
				$("#design-dialog-styles-editor-font-weight").editableSelectSetValue(w);
				$("#design-dialog-styles-editor-font-style").editableSelectSetValue($(previewSelector).css("font-style"));
				$("#design-dialog-styles-editor-text-decoration").editableSelectSetValue($(previewSelector).css("text-decoration"));
				break;
			}
			//align
			case "align" :
			{
				$("#design-dialog-styles-editor-text-align").editableSelectSetValue($(previewSelector).css("text-align"));
				break;
			}
			//background
			case "background" :
			{
				$("#design-dialog-styles-editor-background-color").val(rgbToHex($(previewSelector).css("background-color")));
				var bg = $(previewSelector).css("background-image");
				if (bg == 'undefined' || bg == null || bg == 'none')
				{
					$("#design-dialog-styles-editor-background-image-exists").removeAttr('checked');
				}
				else
				{
					$("#design-dialog-styles-editor-background-image-exists").attr('checked', true);
					if (bg.indexOf('("') >=0) bg = bg.replace('("', "('")
					else if (bg.indexOf("('") < 0) bg = bg.replace('(', "('");
					if (bg.indexOf('")') >=0) bg = bg.replace('")', "')")
					else if (bg.indexOf("')") < 0) bg = bg.replace(')', "')");
				}
				$("#design-dialog-styles-editor-background-image").val(bg);
				
				$("#design-dialog-styles-editor-background-attachment").editableSelectSetValue($(previewSelector).css("background-attachment"));
				$("#design-dialog-styles-editor-background-position").editableSelectSetValue($(previewSelector).css("background-position"));
				$("#design-dialog-styles-editor-background-repeat").editableSelectSetValue($(previewSelector).css("background-repeat"));
				break;
			}
		}
	});
}

/**
 * Shows files editor dialog
 * @return
 */
function designMode_filesEditor()
{
	$('#design-dialog-files-editor').dialog("open");
}

/**
 * Corrects dialog position on open
 * @param selector
 * @return
 */
function designMode_correctDialogPosition(selector)
{
	var t = ($(window).height() - parseInt($(selector).parent().height())) / 2;
	if (t < 0) t = 0;
	//alert(t);
	$(selector).parent().css('top', parseInt(t) + 'px');
	//$(selector).parent().css('z-index', 2000);
	//$(".ui-widget-overlay").css('z-index', 1999);
	//$(selector).parent().find('.ui-dialog-titlebar-close').css('float', 'right');
}

function designMode_editElementStylesByClass(className)
{
	var el = $('<div class="' + className + ' element_layouts_zones_content editable"></div>');
	DesignMode().editElementStyles(el);
}


/**
 * Show layout editor
 */
function designMode_customCssEditor()
{
	DesignMode().showSpinner("Loading data");
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'getCustomCssFile'
	}, function(response){
		DesignMode().hideSpinner();
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				$('#custom-css').html(response.data);
				$('#design-dialog-custom-css-editor').dialog("open");
			}
			else
			{
				alert(response.message);
			}
		}
	});
}

/**
 * Show layout editor
 */
function designMode_saveCustomCss()
{
	DesignMode().showSpinner("Loading data");
	$.post(site_ajax_url_admin + '/admin.php?p=design_mode&triggerHelper=true', {
		'action': 'saveCustomCssFile',
		'css_data': $('#custom-css').val()
	}, function(response){
		DesignMode().hideSpinner();
		if (response != "")
		{
			response = JSON.parse(response);
			if (response.status)
			{
				$('#design-dialog-custom-css-editor').dialog("close");
				if (confirm("Some of changes you just made require page refresh.\nDo you want to do it now?")) document.location.reload();
			}
			else
			{
				alert(response.message);
			}
		}
	});
}

/**
 * Fix jquery tabs issue
 * @param selector
 */
function designMode_fixTabs(selector)
{
	var location = document.location;

	var url =
		location.protocol + '//' +
		location.hostname +
		((location.port == 80 || location.port == 443 || location.port == '') ? '' : (':' + location.port)) +
		location.pathname +
		location.search;

	$(selector).each(function(index, a){
		if ($(a).attr('href') != undefined)
		{
			$(a).attr('href', url + $(a).attr('href'));
		}
	});
}

/**
 * Making design mode class
 * @return
 */
function DesignMode()
{
	if (typeof DesignMode.target == 'undefined') this.target = null;
	
	return {
		'initContextMenu': designMode_initContextMenu,
		'initZones': designMode_initZones,
		'initPanels': designMode_initPanels,
		'loadElement': designMode_loadElement,
		'saveChanges': designMode_saveChanges,
		'getDesignElementClass': designMode_getDesignElementClass,
		'getDesignElementRealClass': designMode_getDesignElementRealClass,
		'getDesignElementCssClass': designMode_getDesignElementCssClass,
		'showSpinner': designMode_showSpinner,
		'hideSpinner': designMode_hideSpinner,
		'checkNewElementId': designMode_checkNewElementId,
		'createCustomElement': designMode_createCustomElement,
		'editElementHtml': designMode_editElementHtml,
		'editElementStyles': designMode_editElementStyles,
		'restoreLayout': designMode_restoreLayout,
		'restoreSkin': designMode_restoreSkin,
		'clearSkinCache': designMode_clearSkinCache,
		'customCssEditor': designMode_customCssEditor,
		'saveCustomCss': designMode_saveCustomCss,
		'saveCleanCheckout': designMode_saveCleanCheckout,
		'imagesEditor': designMode_imagesEditor,
		'colorsEditor': designMode_colorsEditor,
		'colorsEditorSave': designMode_colorsEditorSave,
		'fontsEditor': designMode_fontsEditor,
		'fontsEditorSave': designMode_fontsEditorSave,
		'stylesEditor': designMode_stylesEditor,
		'stylesEditorVisualPreview': designMode_stylesEditorVisualPreview,
		'stylesEditorSelectElement': designMode_stylesEditorSelectElement,
		'stylesEditorRenderCss': designMode_stylesEditorRenderCss,
		'stylesEditorCssPreview': designMode_stylesEditorCssPreview,
		'stylesEditorSetVisualEditors': designMode_stylesEditorSetVisualEditors,
		'stylesEditorAddNode': designMode_stylesEditorAddNode,
		'stylesEditorManageImage': designMode_stylesEditorManageImage,
		'filesEditor': designMode_filesEditor,
		'correctDialogPosition': designMode_correctDialogPosition,
		'initDashboard' : designMode_initDashboard,
		'initDashboardPanels' : designMode_initDashboardPanels,
		'initDashboardThemes' : designMode_initDashboardThemes,
		'activateTheme' : designMode_activateTheme,
		'editElementStylesByClass' : designMode_editElementStylesByClass,
		'layoutEditor' : designMode_layoutEditor,
		'layoutEditorSave' : designMode_layoutEditorSave,
		'pingServer' : designMode_pingServer,
		'fixTabs' :designMode_fixTabs,
		// Show/hide zone
		'toggleZone': function(el){
			var zoneId = $(el).val();
			var zoneDesignClass = 'element_layouts_zones_' + zoneId;
			var zoneVisible = $(el).is(':checked');
			
			if ($('#' + zoneId).length)
			{
				// Show / hide zone
				if (zoneVisible) $('#' + zoneId).show(); else $('#' + zoneId).hide();
				setContentWidth();
			}
			else
			{
				/**
				 * Or create zone when needed
				 */
				if (zoneVisible)
				{
					DesignMode().loadElement(zoneDesignClass, function(html){
						if (zoneId == 'left')
						{
							$('#main').prepend(html);
						}
						else
						{
							if ($("#main #left").length > 0)
							{
								$('#main #left').after(html);
							}
							else
							{
								$('#main').prepend(html);
							}
						}
						setContentWidth();
						DesignMode().initZones('.layout-zone.droppable');
					});
				}
			}
		}
	}
}
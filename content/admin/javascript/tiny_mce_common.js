$(document).ready(function(){
	$('textarea.wysiwyg').tinymce({
		theme : "modern",
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons template paste textcolor moxiemanager"
		],
		toolbar1:"bold italic underline strikethrough superscript subscript | fontselect fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | justifyleft justifycenter justifyright justifyfull | bullist numlist outdent indent",
		toolbar2:"cut copy paste | undo redo | link unlink anchor | image media hr | code removeformat",

		menubar:false,
		statusbar: false,
		plugin_insertdate_dateFormat : "%d-%m-%Y",
		plugin_insertdate_timeFormat : "%H:%M:%S",
		remove_script_host : true,
		relative_urls : false,
		convert_urls : false,
		cleanup : true,
		cleanup_on_startup : true,
		document_base_url : '',
		valid_children : "+body[style]",
		verify_html : false,
		forced_root_block : false,
		extended_valid_elements : "",
		external_link_list_url : "",
		external_image_list_url : "",
		height: 300,
		image_dimensions: false,
		image_class_list: [
			{title: 'Responsive', value: 'img-responsive'},
			{title: 'None', value: ''}
		],
		setup: function (editor) {
			editor.on('init', function(args) {
				editor = args.target;
				editor.on('NodeChange', function(e) {
					if (e && e.element.nodeName.toLowerCase() == 'img') {
						tinyMCE.DOM.setAttribs(e.element, {'width': null, 'height': null, 'caption': null});
					}
				});
			});
		}
	});
});

//tinymce.init({
//	selector: ".wysiwyg",
//	theme: "modern",
//	plugins: [
//		"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
//		"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
//		"table contextmenu directionality emoticons template paste textcolor moxiemanager"
//	],
//	toolbar1:"bold italic underline strikethrough superscript subscript | forecolor backcolor | alignleft aligncenter alignright alignjustify | justifyleft justifycenter justifyright justifyfull | bullist numlist outdent indent",
//	toolbar2:"cut copy paste | undo redo | link unlink anchor | image media hr | code removeformat",
//
//	menubar:false,
//	statusbar: false,
//	plugin_insertdate_dateFormat : "%d-%m-%Y",
//	plugin_insertdate_timeFormat : "%H:%M:%S",
//	remove_script_host : true,
//	relative_urls : false,
//	convert_urls : false,
//	cleanup : true,
//	cleanup_on_startup : true,
//	document_base_url : '',
//	verify_html : false,
//	forced_root_block : false,
//	extended_valid_elements : "",
//	external_link_list_url : "",
//	external_image_list_url : ""
//});

//function toggleEditorMode(sEditorID, eleToggleLink)
//{
//	try
//	{
//		if ($('#' + sEditorID + '_ifr').length > 0)
//		{
//            tinymce.EditorManager.execCommand("mceRemoveEditor", false, sEditorID);
//			if (eleToggleLink) eleToggleLink.innerHTML = "Show Editor";
//		}
//		else
//		{
//			tinymce.EditorManager.execCommand("mceAddEditor", false, sEditorID);
//			if (eleToggleLink) eleToggleLink.innerHTML = "Hide Editor";
//		}
//	}
//	catch(e)
//	{
//		//error handling
//	}
//}

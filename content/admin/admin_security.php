<?php
	if (!defined("ENV")) exit();
	
	class AccessManager
	{
		protected static $features = array();
		protected static $allowed = array();
		protected static $level = null;
		protected static $all = false;
		protected static $locked = false;
		
		/**
		 * Sets current access level
		 * @param type $level 
		 */
		public static function setLevel($level)
		{
			if (!self::$locked)
			{
				self::$level = $level;
			}
		}
		
		/**
		 * Returns current access level
		 *
		 * @return type 
		 */
		public static function getLevel()
		{
			return self::$level;
		}

		/**
		 * Sets features / access map
		 *
		 * @param $key
		 */
		public static function setFeature($key)
		{
			if (!self::$locked)
			{
				$key = strtoupper($key);
				// this check prevents from overrides
				if (!isset(self::$features[$key]))
				{
					self::$features[$key] = array();

					// get levels current feature available for
					$a = func_get_args();

					unset($a[0]); // remove key

					foreach ($a as $param)
					{
						$param = is_array($param) ? $param : array($param => true);
						// fancy stupid simple array merge without re-indexing
						self::$features[$key] += $param;
					}

					if (isset(self::$features[$key][self::getLevel()]))
					{
						self::$allowed[$key] = self::$features[$key][self::getLevel()];
					}
				}
			}
		}
		
		/**
		 * Checks does current license level has access to features
		 *
		 * @param string $key
		 *
		 * @return bool
		 */
		public static function checkAccess($key)
		{
			return isset(self::$allowed[$key]) ? self::$allowed[$key] : false;
		}
		
		/**
		 * Return features
		 *
		 * @return type
		 */
		public static function getFeatures()
		{
			return self::$features;
		}
		
		/**
		 * Get allowed features
		 * @return type 
		 */
		public static function getAllowed()
		{
			return self::$allowed;
		}
		
		/**
		 * Show not allowed message 
		 */
		public static function notAllowed($message = 'This feature is not available under your license agreement.')
		{
			echo '<ul class="pageNote admin-page-note"><li><span class="icon ic-icon ic-info">&nbsp;</span><strong>' . $message . ' Please contact your provider to upgrade your application.</strong></li></ul>';
		}
		
		/**
		 * Lock access manager from modifications 
		 */
		public static function lock()
		{
			self::$locked = true;
		}
	}
	
	define('PRIVILEGE_ANY', 1);
	define('PRIVILEGE_ALL', 2);
	define('PRIVILEGE_CATEGORIES', 4);
	define('PRIVILEGE_PRODUCTS', 8);
	define('PRIVILEGE_USERS', 16);
	define('PRIVILEGE_ORDERS', 32);
	define('PRIVILEGE_EMAIL', 64);
	define('PRIVILEGE_ADMIN', 128);
	define('PRIVILEGE_GLOBAL', 256);
	define('PRIVILEGE_BUSINESS', 512);
	define('PRIVILEGE_CONTENT', 1024);
	define('PRIVILEGE_DATABASE', 2048);
	define('PRIVILEGE_STATS', 4096);
	define('PRIVILEGE_MARKETING', 8192);
	define('PRIVILEGE_CCS', 16384);
	define('PRIVILEGE_RECURRING', 65536);
	define('PRIVILEGE_BILLING', 131072);

	$auth_ok = false;

	$loginIntuitAnywhere = $settings['intuit_anywhere_enabled'] == 'Yes';

	/**
	 * Check does admin session exist
	 */
	if (isset($_SESSION['admin_session_id']) && isset($_SESSION['admin_auth_id']) && isset($_SESSION['admin_auth_time']))
	{
		$admin_session_id = $_SESSION['admin_session_id'];
		$admin_auth_id = $_SESSION['admin_auth_id'];
		$admin_auth_time = $_SESSION['admin_auth_time'];
		
		/**
		 * Check access
		 */
		$db->query('
			SELECT *, CONCAT(fname, " ", lname) AS name, NOW() as now, IF (expires=1 AND expiration_date < NOW(), 1, 0) AS expired
			FROM ' . DB_PREFIX . 'admins
			WHERE 
				aid=' . intval($admin_auth_id) . '
				AND active="Yes"
				AND ((session_id LIKE BINARY "' . $db->escape($admin_session_id) . '"))
				AND ((expires = 0) OR (expires = 1 AND expiration_date > NOW()))
		');

		if ($db->moveNext())
		{
			$admin = $db->col;
			$admin['first_name'] = $admin['fname'];
			$admin['last_name'] = $admin['lname'];

			$auth_ok = true;

			// authorize tinymce access
			$_SESSION['admin_tinymce_auth_ok'] = true;
			
			$admin_auth_id = $admin['aid'];
			$auth_rights = explode(',', $admin['rights']);

			/**
			 * Check IP restriction (if applicable)
			 */
			if (trim($settings['SecurityAdminIps']) != '')
			{
				$user_ip = $_SERVER['REMOTE_ADDR'];

				$restricted_ips = explode(',', trim($settings['SecurityAdminIps']));

				$found_ip = false;

				foreach ($restricted_ips as $restricted_ip)
				{
					$restricted_ip = trim($restricted_ip);
					if ($user_ip == $restricted_ip)
					{
						$found_ip = true;
						break;
					}

					$pattern = '/^'.str_replace(array('.', '*'), array('\.', '[0-9]{1,3}'), $restricted_ip).'$/';
					if (preg_match($pattern, $user_ip))
					{
						$found_ip = true;
						break;
					}
				}

				if (($settings['SecurityAdminIpRestriction'] == 'Allow' && !$found_ip) || ($settings['SecurityAdminIpRestriction'] == 'Deny' && $found_ip))
				{
					print '<h1 style="font-family: Verdana; font-weight: normal; font-size: 14px;">Your IP address has been blocked</h1>';
					session_destroy();
					die();
				}

				unset($pattern_array, $user_ip_array, $wildcards);
			}

			/**
			 * Check admin timeout
			 */
			$expiration_time = $admin_auth_time + intval($settings['SecurityAdminTimeOut']) * 60;
			$current_time = time();

			if ($current_time > $expiration_time)
			{
				unset($_SESSION['admin_session_id']);
				unset($_SESSION['admin_auth_id']);
				unset($_SESSION['admin_auth_time']);
				unset($_SESSION['admin_design_mode']);
				session_write_close();
				header('Location: login.php');
				die();
			}
			else
			{
				$_SESSION['admin_auth_time'] = $current_time;
			}

			$auth_admin_username = $admin["username"];
			$db->query("UPDATE ".DB_PREFIX."admins SET last_access=NOW() WHERE aid='".intval($admin_auth_id)."'");

			if (!isset($iono) || is_null($iono) || get_class($iono) != 'iono_keys')
			{
				require_once(dirname(__FILE__)."/../classes/IonoKeys.php");

				/**
				 * Check iono
				 */
				$iono = new IonoKeys();
				if ($settings["ProxyAvailable"] == "YES")
				{			
					$iono->proxy_used = true;
					$iono->proxy_address = $settings["ProxyAddress"];
					$iono->proxy_port = $settings["ProxyPort"];
					if ($settings["ProxyRequiresAuthorization"] == "YES")
					{
						$iono->proxy_requires_auth = true;
						$iono->proxy_username = $settings["ProxyUsername"];
						$iono->proxy_password = $settings["ProxyPassword"];
					}
				}
				$iono->iono_keys_call4520(trim(LICENSE_NUMBER), "b575796b4295", 'content/cache/license/license.key', 604800); //172800 - 2 days
				if ($iono->status == "")
				{
					$iono->get_status_1980();
				}
			}

			AccessManager::setLevel($iono->level);

			if ($iono->level == 99 && !defined('TRIAL_MODE')) defined('TRIAL_MODE', true);

			AccessManager::setFeature('MAX_ADMINS', array(1=>INF, 2=>3, 3=>1, 99=>INF));
			AccessManager::setFeature('MAX_PRODUCTS', array(1=>INF, 2=>100, 3=>10, 99=>INF));
			AccessManager::setFeature('FACEBOOK_STORE', 1, 2, 99);
			AccessManager::setFeature('DOWNLOADS', 1, 2, 99);
			AccessManager::setFeature('FLAT_URL', 1, 2, 99);
			AccessManager::setFeature('PRODUCT_EXPORT', 1, 2, 99);
			AccessManager::setFeature('PAYMENT_PAYPAL_BASIC', 1, 2, 3, 99);
			AccessManager::setFeature('PAYMENT_PAYPAL_ALL', 1, 2, 99);
			AccessManager::setFeature('PAYMENT_PAYPAL_LIVE', 1, 2, 3);
			AccessManager::setFeature('PAYMENT_WORLDPAY', 1, 2, 99);
			AccessManager::setFeature('PAYMENT_AUTHORIZE', 1, 2, 99);
			AccessManager::setFeature('PAYMENT_CHASE', 1, 2, 99);
			AccessManager::setFeature('PAYMENT_ALL', 1, 99);
			AccessManager::setFeature('INVENTORY', 1, 99);
			AccessManager::setFeature('RECOMMENDED', 1, 99);
			AccessManager::setFeature('MOBILE', 1, 99);
			AccessManager::setFeature('QR', 1, 99);
			AccessManager::setFeature('CARDINAL_CENTINEL', 1, 2, 99);
			AccessManager::setFeature('FILEMANAGER', 1, 2, 3);
			AccessManager::setFeature('ENDICIA', 1, 2, 99); // Same as USPS?
			AccessManager::setFeature('CCS', 1);

			AccessManager::lock();

			define("_ACCESS_INTUIT_ANYWHERE", str_replace(' ', '', $label_app_name) == getpp() && $iono->hosted_by_ddm);
			define("_CAN_ENABLE_INTUIT_ANYWHERE", $iono->intuit_anywhere);

			define('_ACCESS_RECURRING_BILLING', $iono->recurring_visible && $iono->hosted_by_ddm);
			define('_CAN_PURCHASE_RECURRING_BILLING', str_replace(' ', '', $label_app_name) == getpp() && $iono->hosted_by_ddm);
			define('_CAN_ENABLE_RECURRING_BILLING', $iono->recurring_can_enable && $iono->hosted_by_ddm);

			define('_ACCESS_PRODUCTS_FILTERS', $iono->products_filters_visible);
			define('_CAN_PURCHASE_PRODUCTS_FILTERS', str_replace(' ', '', $label_app_name) == getpp());
			define('_CAN_ENABLE_PRODUCTS_FILTERS', $iono->products_filters_can_enable);

			if (!AccessManager::checkAccess('FLAT_URL') && $settings['USE_MOD_REWRITE'] != 'NO')
			{
				$db->query('UPDATE '.DB_PREFIX.'settings SET value="NO" WHERE name="USE_MOD_REWRITE"');
				$settings['USE_MOD_REWRITE'] = 'NO';
				Seo::generateHtaccessFile("Off");
			}

			if ((!_ACCESS_RECURRING_BILLING || !_CAN_ENABLE_RECURRING_BILLING) && $settings['RecurringBillingEnabled'] != '0')
			{
				$db->query('UPDATE '.DB_PREFIX.'settings SET value = "0" WHERE name = "RecurringBillingEnabled"');
				$settings['RecurringBillingEnabled'] = '0';
			}

			if ((!_ACCESS_PRODUCTS_FILTERS || !_CAN_ENABLE_PRODUCTS_FILTERS) && $settings['CatalogUseProductFilters'] != '0')
			{
				$db->query('UPDATE '.DB_PREFIX.'settings SET value = "0" WHERE name = "CatalogUseProductFilters"');
				$settings['CatalogUseProductFilters'] = '0';
			}

			$auth_force_update_profile = false;

			unset($_SESSION['admin_force_update_security_question']);
			unset($_SESSION['admin_force_update_password']);
			unset($_SESSION['admin_force_update_profile_return']);

			$intuitSkipSecurity = (defined('INTUIT_TRIAL') && INTUIT_TRIAL && $settings['intuit_anywhere_connected'] == '');

			if (!$intuitSkipSecurity)
			{
				/**
				 * If required, force admin to change password after 90 days
				 */
				if ($admin['security_question_id'] == 0)
				{
					$auth_force_update_profile = true;
					$_SESSION['admin_force_update_security_question'] = true;

					global $p;
					$p = isset($p) ? $p : '';

					if ($p == 'home' || $p == '')
					{
						$_SESSION['admin_force_update_profile_goto_homepage'] = true;
					}
				}

				if ($settings['SecurityForceCompliantAdminPasswords'] == 'YES')
				{
					$passwordAge = (strtotime($admin['now']) - strtotime($admin['password_change_date'])) / 86400; // in days
					if ($passwordAge > 90)
					{
						//force admin to change password
						$auth_force_update_profile = true;
						$_SESSION['admin_force_update_password'] = true;
						$_SESSION['admin_force_update_profile_goto_homepage'] = true;
					}
				}

				if ($admin['force_password_change'] == 1)
				{
					//force admin to change password
					$auth_force_update_profile = true;
					$_SESSION['admin_force_update_password'] = true;
					$_SESSION['admin_force_update_profile_goto_homepage'] = true;
				}
			}

			if (_ACCESS_INTUIT_ANYWHERE && _CAN_ENABLE_INTUIT_ANYWHERE && $loginIntuitAnywhere)
			{
				require_once 'content/vendors/intuit/oauth/config.php';

				$intuitSync = intuit_Services_QBSync::create();

				if ($settings['intuit_anywhere_cron_enabled'] != 'Yes' && !is_null($intuitSync))
				{
					try
					{
						$intuitSync->doSync();
					}
					catch (intuit_AuthorizationFailureException $e)
					{
						if (defined('DEVMODE')) error_log($e->getMessage());

						intuit_ServiceLayer::disconnect();

						$oauth_token = '';
						$oauth_token_secret = '';
					}
				}
			}
		}
		
		if (isset($_COOKIE['admin_redirect']))
		{
			header('Location: admin.php?'.$_COOKIE['admin_redirect']);
			setcookie('admin_redirect', null, time()-3600);
			session_write_close();
			die();
		}
	}

	/**
	 * Get admin privilege
	 *
	 * @param $rights
	 * @return int
	 *
	 * TODO: move into admin class?
	 */
	function getAdminPrivileges($rights)
	{	
		$accessPrivileges = array(
			'all' => PRIVILEGE_ALL,
			'categories' => PRIVILEGE_CATEGORIES,
			'products' => PRIVILEGE_PRODUCTS,
			'users' => PRIVILEGE_USERS,
			'orders' => PRIVILEGE_ORDERS,
			'emails' => PRIVILEGE_EMAIL,
			'admins' => PRIVILEGE_ADMIN,
			'global' => PRIVILEGE_GLOBAL,
			'business' => PRIVILEGE_BUSINESS,
			'content' => PRIVILEGE_CONTENT,
			'database' => PRIVILEGE_DATABASE,
			'stats' => PRIVILEGE_STATS,
			'marketing' => PRIVILEGE_MARKETING,
			'ccs' => PRIVILEGE_CCS,
			'recurring' => PRIVILEGE_RECURRING
		);
		
		$p = PRIVILEGE_ANY;
		
		foreach ($rights as $key => $value) $p+= isset($accessPrivileges[$value]) ? $accessPrivileges[$value] : 0;
		
		return $p;
	}

	/**
	 * Is privilege
	 *
	 * @param $a
	 * @param $rights
	 *
	 * @return bool
	 *
	 * TODO: move into admin class?
	 */
	function isPrivilege($a, $rights)
	{
		if (is_string($a)) return in_array($a, $rights);
		
		for ($i=0; $i<count($a); $i++)
		{
			if (in_array($a[$i], $rights))
			{
				return true;
			}
		}
		return false;
	}
	

	
	if (!$auth_ok)
	{
		// Save current URL
		$redirectQueryString = $_SERVER['QUERY_STRING'];
		if (strpos($redirectQueryString, '__ajax=1') === false && strpos($redirectQueryString, 'chart=1') === false && strpos($redirectQueryString, 'chart=true') === false)
		{
			header("Location: login.php");
			setcookie('admin_redirect', $redirectQueryString);
		} else {
			//header('HTTP/1.0 401 Unauthorized');
			echo json_encode(array('status' => 0, 'auth' => 0, 'message' => 'Admin session expired. Please login.'));
		}

		session_write_close();
		die();
	}

	if (defined('TRIAL_MODE') && TRIAL_MODE) 
	{
		$expiry = intval($iono->key_data['expiry']);
		$now = time();

		$licenseExpirationText = '14 days';

		if ($expiry != 1 && $expiry > $now)
		{
			$hoursToExpire = ceil(($expiry - $now) / 3600);

			if ($hoursToExpire > 48)
			{
				$licenseExpirationText = ceil($hoursToExpire / 24).' days';
			}
			elseif ($hoursToExpire > 24)
			{
				$licenseExpirationText = '1 day';
			}
			else 
			{
				$licenseExpirationText = $hoursToExpire.' hours';
			}
		}

		define('LICENSE_EXPIRATION_TEXT', $licenseExpirationText);

		/**
		 * Ensure to finish quick start guide first and then do security part
		 */
//		if (isset($admin_auth_id) && $admin_auth_id == 1 && $settings['QuickStartCompleted'] != '1')
//		{
//			unset($_SESSION['admin_force_update_security_question']);
//			unset($_SESSION['admin_force_update_password']);
//			unset($_SESSION['admin_force_update_profile_return']);
//			$auth_force_update_profile = false;
//		}
	}

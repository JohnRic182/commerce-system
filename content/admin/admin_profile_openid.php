<?php

	if (!defined("ENV")) exit();

	global $admin_auth_id;

	$request = Framework_Request::createFromGlobals();
	$controller = new Admin_Controller_AdminProfile($request, $db);
	$controller->setCurrentAdminId($admin_auth_id);

	if (isset($_GET['unlink']) && $_GET['unlink'])
	{
		$controller->unlinkOpenIDAction();
	}
	else
	{
		$controller->openIDAction();
	}

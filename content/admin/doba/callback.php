 <?php

	/**
	 * Prepare Doba envinronment
	 */
	set_time_limit(10000);

 	define('ENV', true);
 	if (!defined('__DIR__')) define('__DIR__', dirname(__FILE__));

 	define('BASE_PATH', dirname(dirname(dirname(dirname(__FILE__)))) . '/');
 	chdir(BASE_PATH);

	 //include libraries
 	require_once 'content/vendors/autoload.php';

 	Config::loadApplicationConfig();

	$db = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

	$settingsRepository = new DataAccess_SettingsRepository($db);
	$settings = $settingsRepository->getAll();

	Timezone::setApplicationDefaultTimezone($db, $settings);
	
	/**
	 * Check remote IP
	 */
	$ip = $_SERVER["REMOTE_ADDR"];
	
	if ($settings["doba_callback_mode"] != "Slave")
	{
		if ($ip == "70.184.122.235" || $ip == "24.248.86.174")
		{
			$ip = "64.58.224.132";
		}
	                 
		if (substr($ip, 0, 9) != "64.58.224" || !isset($_POST["xml"]))
		{
			header("HTTP/1.0 401 Unauthorized");
			exit();
		}
	}
	else
	{
		//check password
		if ($_REQUEST["access_key"] != $settings["doba_callback_access_key"])
		{
			header("HTTP/1.0 401 Unauthorized");
			exit();
		}
	}
	
	$dobaCallback = new Doba_CallbackApi($settings, $db);
	$dobaCallback->processCallback($_POST["xml"]);
 	Seo::updateSeoURLs($db, $settings, true, true, false, true);

	// send to slave cards
	if ($settings["doba_callback_mode"] == "Master")
	{
		$urls = explode("\n", $settings["doba_callback_urls"]);
		if (is_array($urls) && count($urls) > 0)
		{
			foreach ($urls as $url)
			{
				if (trim($url) == "")
				{
					$c = curl_init(trim($url));
					curl_setopt($c, CURLOPT_POST, 1); 
					curl_setopt($c, CURLOPT_POSTFIELDS, "xml=".$_POST["xml"]);
					curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0); 
					curl_setopt($c, CURLOPT_RETURNTRANSFER, 1); 
					curl_exec($c);
				}
			}
		}	
	}
	
	header("HTTP/1.0 200 OK");
	


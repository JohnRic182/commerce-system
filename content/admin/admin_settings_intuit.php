<?php
	if (!defined("ENV")) exit();

	if (isset($_REQUEST["refresh_license"]))
	{
		unlink("content/cache/license/license.key");
		//check for proxy settings for iono

		$iono->iono_keys_call4520(trim(LICENSE_NUMBER), "b575796b4295", 'content/cache/license/license.key', 604800); //172800 - 2 days

		if ($iono->status == "")
		{
			$iono->get_status_1980();
		}

		if ($iono->result != 1)
		{
			echo '<script type="text/javascript">document.location="login.php";</script>';
		}
		else
		{
			echo '<script type="text/javascript">document.location="admin.php?p=settings_intuit";</script>';
		}
	}

	$adminView = new Ddm_AdminView();

	if (!_ACCESS_INTUIT_ANYWHERE)
	{
		$adminView->assign('can_access_intuit', false);
		$adminView->assign('can_enable_intuit', false);
	}
	else
	{
		$adminView->assign('can_access_intuit', true);
		$adminView->assign('can_enable_intuit', _CAN_ENABLE_INTUIT_ANYWHERE);

		$data = array(
			"l" => LICENSE_NUMBER,
			"u" => $settings["AdminHttpsUrl"]."/admin.php?p=settings_intuit&refresh_license=1"
		);

		$upgradeUrl = 'https://account.'.getp().'.com/account/purchase-upgrade/'.urlencode(base64_encode(serialize($data))).'.html';

		$adminView->assign('upgradeUrl', $upgradeUrl);

		$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "";
		$nonce = isset($_REQUEST["nonce"]) ? $_REQUEST["nonce"] : "";

		require_once 'content/vendors/intuit/oauth/config.php';

		if (!isset($intuitSync)) $intuitSync = intuit_Services_QBSync::create();

		$taxableTaxCodes = array();
		$nonTaxableTaxCodes = array();
		$expenseAccounts = array();
		$incomeAccounts = array();
		$salesTaxRates = array();

		if (!is_null($intuitSync))
		{
			try
			{
				$taxableTaxCodes = $intuitSync->getTaxableSalesTaxCodes();
				$nonTaxableTaxCodes = $intuitSync->getNonTaxableSalesTaxCodes();

				$expenseAccounts = $intuitSync->getExpenseAccounts();
				$incomeAccounts = $intuitSync->getIncomeAccounts();

				$salesTaxRates = $intuitSync->getSalesTaxRates();
			}
			catch (intuit_AuthorizationFailureException $e)
			{
				if (defined('DEVMODE')) error_log($e->getMessage());

				intuit_ServiceLayer::disconnect();
			}
		}

		$message = '';

		if (_CAN_ENABLE_INTUIT_ANYWHERE)
		{
			$wasIntuitAAEnabled = $settings['intuit_anywhere_enabled'] == 'Yes';

			switch ($action)
			{
				/**
				 * Update ajax status
				 */
				case "update_status" :
				{
					$intuit_anywhere_enabled = isset($_REQUEST["intuit_anywhere_enabled"]) ? $_REQUEST["intuit_anywhere_enabled"] : "";
					if (in_array($intuit_anywhere_enabled, array("Yes", "No")))
					{
						$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape($intuit_anywhere_enabled)."' WHERE name='intuit_anywhere_enabled'");

						if ($wasIntuitAAEnabled && $intuit_anywhere_enabled == 'No')
						{
							intuit_ServiceLayer::disconnect();
							header('Location: admin.php?p=settings_intuit');
							exit();
						}
					}
					break;
				}
				/**
				 * Update settings
				 */
				case "update_settings" :
				{
					if (!Nonce::verify($nonce, "intuit_update_settings"))
					{
						Admin_Flash::setMessage('Invalid nonce', Admin_Flash::TYPE_ERROR);
						header('Location: admin.php?p=settings_intuit');
						session_write_close();
						$db->done();
						exit;
					}

					$form = isset($_POST["form"]) ? $_REQUEST["form"] : array();
					$form = is_array($form) ? $form : array();
					
					foreach ($form as $name=>$value)
					{
						$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape($value)."' WHERE name='".$db->escape($name)."'");
						$settings[$name] = $value;
					}

					if (!isset($intuitSync) || is_null($intuitSync))
					{
						require_once 'content/vendors/intuit/oauth/config.php';

						$intuitSync = intuit_Services_QBSync::create();
					}

					if ($settings['intuit_income_account'] != '')
					{
						foreach ($incomeAccounts as $account)
						{
							if ($account->Id == $settings['intuit_income_account'])
							{
								$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape($account->IdDomain)."' WHERE name='intuit_income_account_domain'");
								$settings['intuit_income_account_domain'] = $account->IdDomain;
								break;
							}
						}
					}
					else
					{
						$account_id = $intuitSync->ensureIncomeAccountExists(getpp().' Sales');
						$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape($account_id)."' WHERE name='intuit_income_account'");
					}

					if ($settings['intuit_default_tax_rate_name'] == '')
					{
						$rate = $intuitSync->ensureSalesTaxRateExists(getpp().' TAX TaxCode Ref');
						if (!is_null($rate))
						{
							$db->reset();
							$db->assignStr('value', $rate->Name);
							$db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_default_tax_rate_name"');
						}
					}

					if ($settings['intuit_zero_tax_rate_name'] == '')
					{
						$rate = $intuitSync->ensureSalesTaxRateExists(getpp().' NON TaxCode Ref');
						if (!is_null($rate))
						{
							$db->reset();
							$db->assignStr('value', $rate->Name);
							$db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_zero_tax_rate_name"');
						}
					}

					if ($settings['intuit_expense_account'] != '')
					{
						foreach ($expenseAccounts as $account)
						{
							if ($account->Id == $settings['intuit_expense_account'])
							{
								$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape($account->IdDomain)."' WHERE name='intuit_expense_account_domain'");
								$settings['intuit_expense_account_domain'] = $account->IdDomain;
								break;
							}
						}
					}

					if ($wasIntuitAAEnabled && $settings['intuit_anywhere_enabled'] == 'No')
					{
						if (isset($intuitSync))
							$intuitSync->disconnect();

						intuit_ServiceLayer::disconnect();
					}

					$message = "Settings are updated";
					break;
				}
			}
		}

		$adminView->assign('message', $message);

		$last_active_tab = isset($_REQUEST["last_active_tab"]) ? $_REQUEST["last_active_tab"] : "main";
		$adminView->assign('last_active_tab', gs($last_active_tab));

		$adminView->assign('intuit_update_settings_nonce', Nonce::create('intuit_update_settings'));

		$formBuilder = new core_Form_FormBuilder('settings', array('label' => 'Intuit Settings', 'class' => 'ic-settings', 'first' => true));

		$formBuilder
			->add('form[intuit_sync_taxes]', 'choice', array('label' => 'Sync Tax Information', 'value' => $settings['intuit_sync_taxes'], 'options' => array(
					'No' => 'No',
					'Yes' => 'Yes',
				)
			));

		if (count($taxableTaxCodes) > 0)
		{
			$taxableTaxCodeOptions = array();
			foreach ($taxableTaxCodes as $taxCode)
			{
				$taxableTaxCodeOptions[$taxCode->Name] = $taxCode->Name;
			}
			$formBuilder->add('form[intuit_taxable_tax_code]', 'choice', array('label' => 'Taxable Tax Code', 'value' => $settings['intuit_taxable_tax_code'], 'options' => $taxableTaxCodeOptions));
		}
		if (count($nonTaxableTaxCodes) > 0)
		{
			$nonTaxableTaxCodeOptions = array();
			foreach ($nonTaxableTaxCodes as $taxCode)
			{
				$nonTaxableTaxCodeOptions[$taxCode->Name] = $taxCode->Name;
			}
			$formBuilder->add('form[intuit_nontaxable_tax_code]', 'choice', array('label' => 'Non-Taxable Tax Code', 'value' => $settings['intuit_nontaxable_tax_code'], 'options' => $nonTaxableTaxCodeOptions));
		}

		if (count($incomeAccounts) > 0)
		{
			$accountsOptions = array('' => '');
			foreach ($incomeAccounts as $account)
			{
				$accountsOptions[$account->Id] = $account->Name;
			}
			$formBuilder->add('form[intuit_income_account]', 'choice', array('label' => 'Income Account', 'value' => $settings['intuit_income_account'], 'options' => $accountsOptions));
		}

		if (count($expenseAccounts) > 0)
		{
			$accountsOptions = array('' => '');
			foreach ($expenseAccounts as $account)
			{
				$accountsOptions[$account->Id] = $account->Name;
			}
			$formBuilder->add('form[intuit_expense_account]', 'choice', array('label' => 'Expense Account', 'value' => $settings['intuit_expense_account'], 'options' => $accountsOptions));
		}

		if (count($salesTaxRates) > 0)
		{
			$options = array('' => '');
			$accountsOptions = array(getpp().' Taxes' => getpp().' Taxes');
			foreach ($salesTaxRates as $rate)
			{
				$options[$rate->Name] = $rate->Name.' ('.number_format($rate->TaxRate, 2, '.', '').')';
			}
			$formBuilder->add('form[intuit_default_tax_rate_name]', 'choice', array('label' => 'Default Taxable Tax Rate', 'value' => $settings['intuit_default_tax_rate_name'], 'options' => $options));
		}

		if (count($salesTaxRates) > 0)
		{
			$options = array('' => '');
			$accountsOptions = array(getpp().' Not Taxed' => getpp().' Not Taxed');
			foreach ($salesTaxRates as $rate)
			{
				if ($rate->TaxRate == 0)
					$options[$rate->Name] = $rate->Name.' ('.number_format($rate->TaxRate, 2, '.', '').')';
			}
			$formBuilder->add('form[intuit_zero_tax_rate_name]', 'choice', array('label' => 'Non-Taxable Tax Rate', 'value' => $settings['intuit_zero_tax_rate_name'], 'options' => $options));
		}

		$formBuilder->add('form[intuit_anywhere_cron_enabled]', 'choice', array('label' => 'Cron Sync Active', 'value' => $settings['intuit_anywhere_cron_enabled'], 'options' => array(
				'No' => 'No',
				'Yes' => 'Yes',
			),
			'note' => 'When cron is active, the sync will be automatic.  Otherwise, the sync will happen when an administrator logs into the admin area.'
		));

		$has_oauth_token = 'No';
		$db->query('SELECT * FROM '.DB_PREFIX.'admin_oauth ');
		while ($row = $db->moveNext())
		{
			if (trim($row['oauth_token']) != '')
			{
				$has_oauth_token = 'Yes';
				break;
			}
		}

		$adminView->assign('has_oauth_token', $has_oauth_token);

		$form = $formBuilder->getForm();
		$adminView->assign('form', $form);

		$adminView->assign('connectUrl', intuit_ServiceLayer::getConnectUrl(true));
	}

	$productsStatus = $db->selectOne("SELECT COUNT(*) as `count`, max(last_sync_date) as last_sync_date
FROM ".DB_PREFIX."intuit_sync s
	INNER JOIN ".DB_PREFIX."products p ON s.id = p.product_id AND s.type = 'item'");

	$customersStatus = $db->selectOne("SELECT COUNT(*) as `count`, max(last_sync_date) as last_sync_date
FROM ".DB_PREFIX."intuit_sync s
	INNER JOIN ".DB_PREFIX."users u ON s.id = u.uid AND s.type = 'user'");

	$ordersStatus = $db->selectOne("SELECT COUNT(*) as `count`, max(last_sync_date) as last_sync_date
FROM ".DB_PREFIX."intuit_sync s
	INNER JOIN ".DB_PREFIX."orders o ON s.id = o.oid AND s.type = 'order'");

	$adminView->assign('productsStatus', $productsStatus);
	$adminView->assign('customersStatus', $customersStatus);
	$adminView->assign('ordersStatus', $ordersStatus);

	$adminView->render('pages/settings/intuit-anywhere');
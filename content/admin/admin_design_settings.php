<?php

	if (!defined("ENV")) exit();

	$request = Framework_Request::createFromGlobals();

	$controller = new Admin_Controller_DesignSettings($request, $db);

	$mode = $request->get('mode');

	$design_mode_flag = $request->get('design_mode_flag');
	if( strtolower($design_mode_flag) == 'off') $mode = 'activatedesigner';

	switch ($mode)
	{
	case 'activate' :
		{
			$controller->activateAction();
			break;
		}
		case 'install' :
		{
			$controller->installAndActivateAction();
			break;
		}
		case 'activatedesigner' :
		{
			$controller->cartDesignerAction();
			break;
		}
		case 'viewsite' :
		{
			$controller->viewSiteAction();
			break;
		}
		default:
		case 'list' :
		{
			$controller->listAction();
		}
	}

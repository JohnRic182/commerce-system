<?php

	//design mode
	$designmode = new Ddm_DesignMode($settings);

	//get languages
	$lang_list = array();
	$language = isset($_REQUEST["language"])?$_REQUEST["language"]:"";
	$db->query("SELECT * FROM ".DB_PREFIX."languages");
	while ($db->movenext())
	{
		$lang_list[strtolower($db->col["code"])] = $db->col["name"];
	}

	//check current language
	$db->query("SELECT * FROM ".DB_PREFIX."languages WHERE code='".$db->escape($language)."'");
	if (!$db->movenext())
	{
		$db->query("SELECT * FROM ".DB_PREFIX."languages WHERE is_default='Yes'");
		if ($db->movenext())
		{
			$language = $db->col["code"];
			$language_id = $db->col["language_id"];
		}
		else
		{
			$language = "english";
			$language_id = 1;
		}
	}
	else
	{
		$language = $db->col["code"];
		$language_id = $db->col["language_id"];
	}
	
	//load captions
	$lang = new Language($db);
	$lang->loadTemplate();
	
	$captions = $lang->getMessages($language_id, $mode."s");
	
	//check mode - image, button or header
	$mode = isset($mode)?trim($mode):"";
	switch ($mode)
	{
		default :
		case "button" : 
		{
			$mode = "button";
			$page_title = "Edit Site Buttons";
			$page_hint = "This section will allow you to add custom buttons to your cart. Click the browse button to locate the button on your computer or enter header caption. Click Save Changes at the bottom of the page to upload your buttons to the cart";
			$checkFileName = "button-add-to-cart";
			$groupKey = "page_buttons";
			break;
		}
		case "header" : 
		{
			$page_title = "Edit Site Headers";
			$page_hint = "This section will allow you to add custom headers to your cart. Click the browse button to locate the header image on your computer or enter header text. Click Save Changes at the bottom of the page to upload your headers to the cart";
			$checkFileName = "headerCheckout";
			$groupKey = "page_headers";
			break;
		}
		case "image" : 
		{
			$page_title = "Edit Default Site Images";
			$page_hint = "This section allows you to change default images into your cart. Click the browse button to locate the new image on your computer. Click Save Changes at the bottom of the page to upload your images to the cart";
			$checkFileName = "imageCart";
			$groupKey = "page_images";
			break;
		}
	}
	
	//load map
	include("content/engine/design/styles/elements/elements-map.php");
	
	//preload images
	$images = view()->getImages(true);
	
	//get elements data
	$elements = array();
	$refreshElements = false;

	foreach ($cssMap["groups"]["common"]["elements"][$groupKey]["elements"] as $el)
	{
		$class = $el["class"];
		
		if (!in_array($class, array("button")))
		{
			$element = array();	
			$element["element_id"] = $class;
			$element["element_name"] = $el["title"];
			$element["element_removable"] = true;
			$element["element_caption"] = array_key_exists($el["lng"], $captions) ? $captions[$el["lng"]]["message"] : "";
			$element["lng"] = $el["lng"];
			
			if (isset($images[$class]))
			{
				$element["image"] = $images[$class];
				$element["image_info"] = $designmode->getImageInfo($class);
			}
			else
			{
				$element["image"] = false;
				$element["image_info"] = false;
			}
			
			$elements[$element["element_id"]] = $element;
		}
	}
	
	$action = isset($action)?trim($action):"";
	$is_error = false;
	$error_message = "";
	
	/**
	 * Use Set
	 */
	if ($action == "use_set")
	{
		$predefined_set = isset($predefined_set)?$predefined_set:"";
		$pi = pathinfo($predefined_set);
				
		$predefined_set = substr($pi["dirname"], 14, strlen($pi["dirname"]));
		
		$set_source_dir = "images/themes/".$predefined_set;
		$set_destination_dir = "content/skins/_custom/skin/images";
		$set_destination_dir_cache = "content/cache/skins/".escapeFileName($settings["DesignSkinName"])."/images";
		
		if (!is_dir($set_destination_dir))
		{
			if (!mkdir($set_destination_dir))
			{
				$is_error = true;
				$error_message = "Can not create ".$mode."s destination directory (".$set_destination_dir."). Please check permissions.";
			}
		}
		
		if (!is_dir($set_destination_dir_cache))
		{
			if (!mkdir($set_destination_dir_cache))
			{
				$is_error = true;
				$error_message = "Can not create ".$mode."s destination directory (".$set_destination_dir_cache."). Please check permissions.";
			}
		}
		
		if (!$is_error && !(is_writable($set_destination_dir) || !is_writable($set_destination_dir_cache)))
		{
			$is_error = true;
			$error_message = "One or both of destination directories are not writable. Please check permissions. (".$set_destination_dir.", ".$set_destination_dir_cache.")";
		}
		
		if (!$is_error)
		{
			unset($element);
			
			foreach ($elements as $i=>$element)
			{
				@unlink($set_destination_dir."/".$element["element_id"].".gif");
				@unlink($set_destination_dir."/".$element["element_id"].".jpg");
				@unlink($set_destination_dir."/".$element["element_id"].".png");
				
				@unlink($set_destination_dir_cache."/".$element["element_id"].".gif");
				@unlink($set_destination_dir_cache."/".$element["element_id"].".jpg");
				@unlink($set_destination_dir_cache."/".$element["element_id"].".png");
				
				$elements[$i]["element_file"] = false;
				$image_extension = null;
				
				if (is_file($set_source_dir.'/'.$element["element_id"].".gif"))
				{
					$image_extension = '.gif';
				}
				elseif (is_file($set_source_dir.'/'.$element["element_id"].".png"))
				{
					$image_extension = '.png';
				}
				elseif (is_file($set_source_dir.'/'.$element["element_id"].".jpg"))
				{
					$image_extension = '.jpg';
				}
				
				if (!is_null($image_extension))
				{
					$image_name = $element['element_id'] . $image_extension;
					copy($set_source_dir . '/' . $image_name, $set_destination_dir . '/' . $image_name);
					copy($set_source_dir . '/' . $image_name, $set_destination_dir_cache . '/' . $image_name);
					chmod($set_destination_dir . '/' . $image_name, FileUtils::getFilePermissionMode());
					chmod($set_destination_dir_cache . '/' . $image_name, FileUtils::getFilePermissionMode());
					$elements[$i]['element_file'] = $set_destination_dir_cache . '/' . $image_name;
					
					if ($settings['CatalogOptimizeImages'] == 'YES')
					{
						$optimizer = new Admin_Service_ImageOptimizer();
						$optimizer->optimize($set_source_dir . '/' . $image_name);
						$optimizer->optimize($set_destination_dir . '/' . $image_name);
						$optimizer->optimize($set_destination_dir_cache . '/' . $image_name);
					}
				}
				
				$db->query("SELECT * FROM ".DB_PREFIX."design_elements WHERE element_id='".$db->escape($element["element_id"])."'");
				
				if ($el = $db->moveNext())
				{
					$db->reset();
					$db->assignStr("type", "image");
					$db->assignStr("content", $elements[$i]["element_file"]);
					$db->update(DB_PREFIX."design_elements",  "WHERE element_id='".$db->escape($element["element_id"])."'");
				}
				else
				{
					$db->reset();
					$db->assignStr("group_id", $mode);
					$db->assignStr("removable", "Yes");
					$db->assignStr("element_id", $element["element_id"]);
					$db->assignStr("name", $element["element_id"]);
					$db->assignStr("type", "image");
					$db->assignStr("content", $elements[$i]["element_file"]);
					$db->insert(DB_PREFIX."design_elements"); 
				}
			}
		}
	}
	
	/**
	 * Update images
	 */
	if ($action == "update_images")
	{
		$edited_captions = isset($edited_captions)?$edited_captions:array();
		$_edited_captions = array();
		
		foreach ($edited_captions as $key=>$value)
		{
			$_edited_captions[$elements[$key]["lng"]] = $value;
		}
		
		$lang->saveMessages($language_id, $mode."s", $_edited_captions);
		$set_destination_dir = "content/skins/_custom/skin/images";
		$set_destination_dir_cache = "content/cache/skins/".escapeFileName($settings["DesignSkinName"])."/images";
		foreach ($edited_captions as $key=>$value)
		{
			$elements[$key]["element_caption"] = $value;
			
			//upload file
			if (isset($_FILES["new_image"]["name"][$key]))
			{
				if ($_FILES["new_image"]["name"][$key] != "")
				{
					$is = getimagesize($_FILES["new_image"]["tmp_name"][$key]);
					
					if ($is[2] == 1 || $is[2] == 2 || $is[2] == 3)
					{
						@unlink($set_destination_dir."/".$key.".gif");
						@unlink($set_destination_dir."/".$key.".jpg");
						@unlink($set_destination_dir."/".$key.".png");

						@unlink($set_destination_dir_cache."/".$key.".gif");
						@unlink($set_destination_dir_cache."/".$key.".jpg");
						@unlink($set_destination_dir_cache."/".$key.".png");
						
						$image_extension = null;
						
						if ($is[2] == 1)
						{
							$image_extension = '.gif';
						}
						elseif ($is[2] == 2)
						{
							$image_extension = '.jpg';
						}
						elseif ($is[2] == 3)
						{
							$image_extension = '.png';
						}
						
						if (!is_null($image_extension))
						{
							$image_name = $key.$image_extension;
							@copy($_FILES["new_image"]["tmp_name"][$key], $set_destination_dir.'/'.$image_name);
							@copy($_FILES["new_image"]["tmp_name"][$key], $set_destination_dir_cache.'/'.$image_name);
							@chmod($set_destination_dir.'/'.$image_name, FileUtils::getFilePermissionMode());
							@chmod($set_destination_dir_cache.'/'.$image_name, FileUtils::getFilePermissionMode());
							$elements[$key]["element_file"] = $set_destination_dir_cache.'/'.$image_name;
							
							if ($settings['CatalogOptimizeImages'] == 'YES')
							{
								$optimizer = new Admin_Service_ImageOptimizer();
								$optimizer->optimize($set_destination_dir . '/' . $image_name);
								$optimizer->optimize($set_destination_dir_cache . '/' . $image_name);
							}
						}
						
						$db->query("UPDATE ".DB_PREFIX."design_elements SET content='".$db->escape($elements[$key]["element_file"])."', type='image' WHERE element_id='".$db->escape($key)."'");
					}
					else
					{
						$is_error = true;
						$error_message = "One or more images are not uploader. Incorrect image type.";
					}
				}
			}
		}
	}
	
	/**
	 * Delete Images
	 */
	if ($action == "delete_image")
	{
		$image_id = isset($_REQUEST["image_id"]) ? escapeFileName($_REQUEST["image_id"]) : "";
		$set_destination_dir = "content/skins/_custom/skin/images";
		$set_destination_dir_cache = "content/cache/skins/".escapeFileName($settings["DesignSkinName"])."/images";
		
		@unlink($set_destination_dir."/".$image_id.".gif");
		@unlink($set_destination_dir."/".$image_id.".jpg");
		@unlink($set_destination_dir."/".$image_id.".png");
				
		@unlink($set_destination_dir_cache."/".$image_id.".gif");
		@unlink($set_destination_dir_cache."/".$image_id.".jpg");
		@unlink($set_destination_dir_cache."/".$image_id.".png");
		
		$db->query("UPDATE ".DB_PREFIX."design_elements SET content='', type='image-hidden' WHERE element_id='".$db->escape($image_id)."'");
		
		$elements[$image_id]["element_file"] = false;
	}
	
	/**
	 * Delete all Images
	 */
	if ($action == "delete_images")
	{
		$set_destination_dir = "content/skins/_custom/skin/images";
		$set_destination_dir_cache = "content/cache/skins/".escapeFileName($settings["DesignSkinName"])."/images";
		
		foreach ($elements as $element_id=>$element)
		{
			@unlink($set_destination_dir."/".$element_id.".gif");
			@unlink($set_destination_dir."/".$element_id.".jpg");
			@unlink($set_destination_dir."/".$element_id.".png");
				
			@unlink($set_destination_dir_cache."/".$element_id.".gif");
			@unlink($set_destination_dir_cache."/".$element_id.".jpg");
			@unlink($set_destination_dir_cache."/".$element_id.".png");
		
			$elements[$element_id]["element_file"] = false;
		}
		
		$db->query("UPDATE ".DB_PREFIX."design_elements SET content='', type='image-hidden' WHERE group_id='".$db->escape($mode)."'");
	}
	
	if ($action != "")
	{
		echo '<script>document.location="admin.php?p=design_images&mode='.$mode.'&printing=true";</script>';
		exit;
	}

if (is_dir("images/themes/".$mode."s"))
{
?>
<form name="frmImagesSet" method="post" action="admin.php?p=design_images&printing=1&mode=<?=$mode?>&chart=true" target="design-dialog-images-editor-iframe">
	<input type="hidden" name="action" value="use_set"/>
	<input type="hidden" name="language" value="<?=$language?>"/>
	<table width="100%" cellpadding="5" cellspacing="0" border="0">
		<tr valign="top">
			<td style="width:200px;">Choose set to use with active language</td>
			<td>
				<select name="predefined_set" size="5" onchange="document.images['imagePreview'].src=this.value;">
<?php
	$first_file = false;
	$dir_lang = dir("images/themes/".$mode."s");
	
	while (false !== ($lang_entry = $dir_lang->read()))
	{
		if ($lang_entry != ".." && $lang_entry != "." && trim($lang_entry) != "" && array_key_exists($lang_entry, $lang_list))
		{
			echo "<optgroup label=\"".ucfirst($lang_entry)."\">";
			$dir_set = dir("images/themes/".$mode."s/".$lang_entry);
			
			while (false !== ($set_entry = $dir_set->read()))
			{
				if ($set_entry != ".." && $set_entry != "." && trim($set_entry) != "")
				{
					$image_file = "images/themes/".$mode."s/".$lang_entry."/".$set_entry."/".$checkFileName;
					
					if (is_file($image_file.".gif"))
					{
						$image_file = $image_file.".gif";
					}
					elseif (is_file($image_file.".png"))
					{
						$image_file = $image_file.".png";
					}
					elseif (is_file($image_file.".jpg"))
					{
						$image_file = $image_file.".jpg";
					}
					else
					{
						$image_file = false;
					}
					
					if ($image_file)
					{
						echo "<option value=".$image_file." ".(!$first_file?"selected":"").">".$set_entry."</option>";
						
						if (!$first_file)
						{
							$first_file = $image_file;
						}
					}
					
				}
			}
			$dir_set->close();
			echo "</optgroup>";
		}
	}
	$dir_lang->close();

?>
				</select>
			</td>
		</tr>
		<tr valign="top">
			<td>Preview</td>
			<td>
				<img src="<?=$first_file?>" id="imagePreview">
			</td>
		</tr>
		<tr>
			<td colspan="2" class="admin-form-buttons" style="border-bottom:1px solid gray;">
				<input type="submit" value="Use this set"/>
			</td>
		</tr>
	</table>
</form>

&nbsp;
<?php 
}
?>
<form name="frmImagesUpdate" method="post" action="admin.php?p=design_images&printing=true&mode=<?=$mode?>&chart=true" enctype="multipart/form-data" target="design-dialog-images-editor-iframe">
	<input type="hidden" name="action" value="update_images">
	<input type="hidden" name="language" value="<?=$language?>">

	<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr valign="top">
		<td style="width:200px;height:40px;">Description</td>
		<td>Caption / Image alt text</td>
	</tr>
	<tr valign="top">
		<td style="padding-bottom:10px;font-weight:bold;">Choose language to edit</td>
		<td style="padding-bottom:10px;">
			<select name="language" style="width:123px;" onchange="document.location='admin.php?p=design_images&printing=1&mode=<?=$mode?>&language=' + this.value">
<?php
	$db->query("SELECT * FROM ".DB_PREFIX."languages ORDER BY is_default, is_active, code");
	
	while($db->movenext())
	{
?>
				<option value="<?=$db->col["code"]?>" <?=$language==$db->col["code"]?"selected":""?>><?=$db->col["name"]?></option>
<?php
	}
?>
			</select>
		</td>
	</tr>
	
<?php
	foreach ($elements as $i=>$element)
	{
		if (($element["element_id"] == "headerBackground") || $element["element_id"] != "headerBackground")
		{
?>
	<tr valign="top">
		<td style="border-top:1px solid #f0f0f0;padding-top:10px;"><?=$element["element_name"]?></td>
		<td style="border-top:1px solid #f0f0f0;padding-top:10px;">
<?php
			if ($element["element_id"] != "headerBackground")
			{
?>
			<input type="text" name="edited_captions[<?=$element["element_id"]?>]" value="<?=htmlspecialchars($element["element_caption"])?>"> &nbsp;
<?php
			}
			else
			{
?>
			<input type="hidden" name="edited_captions[<?=$element["element_id"]?>]" value="">
<?php
			}
?>
			<input type="file" name="new_image[<?=$element["element_id"]?>]">
		</td>
	</tr>
<?php
			if (($mode != "header" || $settings["LayoutBoxHeaderBackground"] == "0") || ($element["element_id"] == "headerBackground" && $settings["LayoutBoxHeaderBackground"] == "1"))
			{
?>
	<tr>
		<td style="padding-bottom:10px;">&nbsp;</td>
		<td style="padding-bottom:10px;">
<?php
				if ($element["image"] && is_file($element["image"]["content"]))
				{
					$is = getimagesize($element["image"]["content"]);
?>
			Current image (<?=$is[0]?>x<?=$is[1]?> pixels, <?=number_format(filesize($element["image"]["content"])/1024, 2)?>kb file size)
<?php
					if ($element["element_removable"])
					{
?>
					<a onclick="return designMode_deleteImage('<?=$mode?>', '<?=$language?>', '<?= $i ?>')">Delete this image</a>
<?php
					}
?>			
			<div style="padding-top:5px;">
				<img src="<?=$element["image"]["content"]?>?<?=rand(10000,99999)?>" <?=$is[0]>200?'width="200"':''?>>
			</div>
<?php
				}
				else
				{
					if ($mode == "button")
					{
						echo "<span style=\"color:gray\"><i>Is it a HTML button. <a href=\"javascript:DesignMode().editElementStylesByClass('".$element["element_id"]."');\">Click here to edit styles.</i></span>";
					}
					else
					{
						echo "<span style=\"color:gray\"><i>There is no image yet. Use default ".$mode.".</i></span>";
					}
				}
?>		
		</td>
	</tr>
<?php	
			}
		}
	}
?>	
	<tr>
		<td colspan="2" height="40" align="center">
			<a onclick="designMode_deleteImages('<?=$mode?>', '<?=$language?>')">Delete all images</a>
		</td>
	</tr>
</table>
</form>

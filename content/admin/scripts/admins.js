var Admins = (function($) {
	var init, handleFormSubmit, handleDeleteFormSubmit, toggleExpirationDate, isEmail;

	init = function() {
		$(document).ready(function() {
			$('.form-administrators-delete').submit(function(){ return false; });

			$('.remove-admin').click(function() {
				var id = $(this).attr('data-id');
				var nonce = $(this).attr('data-nonce');
				AdminForm.confirm(trans.admins.confirm_remove, function() {
					window.location = 'admin.php?p=admin&mode=delete&deleteMode=single&aid=' + id + '&nonce=' + nonce;
				});

				return false;
			});

			$('form[name=form-admin]').submit(function() {
				return handleFormSubmit($(this));
			});

			/**
			 * Handle delete button
			 */
			$('form[name=form-administrators-delete]').submit(function(){
				return handleDeleteFormSubmit($(this));
			});

			$('#field-expires').change(toggleExpirationDate);
			toggleExpirationDate();

			var rightsRadios = $('input[name=rights\\[\\]]').not('#radio_field-rights-_all');
			rightsRadios.change(function() {
				if ($(this).is(':checked')) {
					$('#radio_field-rights-_all')
						.prop('checked', false)
						.change();
				}
			});
			$('#radio_field-rights-_all').change(function() {
				if ($(this).is(':checked')) {
					rightsRadios.filter(':checked').prop('checked', false).change();
				}
			});

			var name = $('.admin-name').text();
			if ($('#user-icon-thumb').length) {
				$('#user-icon-thumb').attr({'width': '38', 'height': '38'});
				AdminForm.avatarInitials(name, 'user-icon-thumb', '16px Arial', false);
			}
		});
	};

	handleFormSubmit = function(theForm) {
		var mode = theForm.find('input[name=mode]').val();

		var errors = [];
		if ($.trim($('#field-fname').val()) == '') {
			errors.push({
				field: '#field-fname',
				message: trans.admins.first_name_required
			});
		}

		if ($.trim($('#field-lname').val()) == '') {
			errors.push({
				field: '#field-lname',
				message: trans.admins.last_name_required
			});
		}

		if ($.trim($('#field-email').val()) == '') {
			errors.push({
				field: '#field-email',
				message: trans.admins.email_required
			});
		}
		else {
			if(!isEmail($('#field-email').val())) {
				errors.push({
					field: '#field-email',
					message: trans.admins.invalid_email
				});
			}
		}
		if ($.trim($('#field-username').val()) == '') {
			errors.push({
				field: '#field-username',
				message: trans.admins.username_required
			});
		}

		var password = $('#field-password').val();
		if (mode == 'insert') {
			if ($.trim(password) == '') {
				errors.push({
					field: '#field-password',
					message: trans.admins.password_required
				});
			}
		}

		if ($.trim(password) != '') {
			if (password != $('#field-password2').val()) {
				errors.push({
					field: '#field-password2',
					message: trans.admins.password_confirmation_mismatch
				});
			} else if (password.length < 8) {
				errors.push({
					field: '#field-password',
					message: trans.admins.password_invalid
				});
			}
		}

		if ($('input[name=rights\\[\\]]:checked').length == 0) {
			errors.push({
				field: '#radio_field-rights-_all',
				message: trans.admins.rights_invalid
			});
		}

		AdminForm.displayValidationErrors(errors);
		return errors.length == 0;
	};

	handleDeleteFormSubmit = function(theForm) {
		var theModal = $(theForm).closest('.modal');
		var deleteMode = $('input[name="form-administrators-delete-mode"]:checked').val();
		var nonce = $('#modal-administrators-delete').find('input[name="nonce"]').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-admin:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=admin&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else
			{
				AdminForm.displayModalAlert(theModal, trans.admins.no_admins_selected , 'danger');
			}
		}
		else if (deleteMode == 'all')
		{
			window.location='admin.php?p=admin&mode=delete&deleteMode=all&nonce=' + nonce;
		}

		return false;
	};

	isEmail = function (email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	toggleExpirationDate = function() {
		var expires = $('#field-expires').val();

		$('select[name^=expiration_date]').closest('.form-group').toggle(expires == '1');
	};



	init();
}(jQuery));

(function($) {
	var init;

	init = function() {
		$(document).ready(function() {
			$('a[data-action="generate-widgets"]').click(function() {
				$('#modal-widgets').modal('show');
				return false;
			});

			$('form[name="form-widgets"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				theForm.find('.btn-primary').html('Please wait...').attr('disabled', 'disabled');
				AdminForm.sendRequest(
					'admin.php?p=widgets&mode=generate&chart=true',
					{
						product_id: $('#field-wg_pid').val(),
						campaign_id: $('#field-wg_campaign_id').val(),
						widget_type: $('#field-widgets_type').val(),
						widget_image_type: $('#field-widgets_image_type').val(),
						widget_style: $('#field-widgets_style').val()
					},
					'Generating Widget',
					function(data) {
						theForm.find('.btn-primary').html('Generate').removeAttr('disabled');
						console.log(data);
						if (data.status == 1) {
							theModal.modal('hide');
							var thePreviewModal = $('#modal-widgets-generated');
							thePreviewModal.find('.preview').val(data.widget_html);
							thePreviewModal.modal('show');
						} else {
							if (data.errors) {
								AdminForm.displayModalErrors(theModal, data.errors);
							} else if (data.message) {
								AdminForm.displayModalAlert(theModal, data.message, 'danger');
							}
						}
					}
				);

				return false;
			});
		});
	};

	init();

}(jQuery));

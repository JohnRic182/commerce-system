$(document).ready(function(){

	$('[data-action="action-campaigns-delete"]').click(function(){
		var deleteMode = $('input[name="form-campaigns-delete-mode"]:checked').val();
		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-campaign:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=drift_marketing&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
			}
			else
			{
				alert(trans.drift_marketing.select_one_campaign);
			}
		}
		else if (deleteMode == 'search')
		{
			window.location='admin.php?p=drift_marketing&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
		}
	});	
});

function removeCampaign(id, nonce) {
	AdminForm.confirm(trans.drift_marketing.confirm_remove_campaign, function() {
		window.location = 'admin.php?p=drift_marketing&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
	});
	return false;
}

function restoreEmailTemplate(id) {
	AdminForm.confirm(trans.drift_marketing.restore_template, function() {
		if (id == null) {
			window.location = 'admin.php?p=drift_marketing&mode=add';
		} else {
			window.location = 'admin.php?p=drift_marketing&mode=update&template=restore&id=' + id;
		}
	});
	return false;
}
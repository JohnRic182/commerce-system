var ProductsPromotions = (function($) {
	var init, renderProductPromotionsTable;

	init = function() {
		$(document).ready(function() {
			$('a[data-action="action-product-promotion-add"]').click(function() {
				$('#field-promotion-gift_quantity,#field-promotion-gift_max_quantity').val('');
				$('#field-promotion-gift_shipping_free').val('Yes').change();
				$('#field-promotion-product-search-str').val('');
				$('#field-promotion-product-search-result').html('');
				$('#field-promotion-id').val('new');
				$('#modal-product-promotion').modal('show');
				return false;
			});

			$('#modal-product-promotion').on('hidden.bs.modal', function (e) {
				$('#modal-product-promotion').find('.form-group .error').remove();
			});

			var promotionSearchTimeOut = null;

			$('#field-promotion-product-search-str').keyup(function() {
				var str = $.trim($(this).val());

				if (str.length > 1) {
					if (promotionSearchTimeOut) clearTimeout(promotionSearchTimeOut);
					promotionSearchTimeOut = setTimeout(function() {
						AdminForm.sendRequest(
							'admin.php?p=product_page_promotions&mode=promotion-product-lookup',
							{
								str : str,
								pid : $('#field-promotion-pid').val()
							},
							null,
							function(data) {
								var searchResults = $('#field-promotion-product-search-result');
								$(searchResults).html('');
								if (data && data.status && data.products) {
									$.each(data.products, function(index, product) {
										$('<option value="' + product.pid + '">' + htmlentities(product.product_id + ' - ' + product.title) +'</option>').appendTo(searchResults);
									});
									$(searchResults).find('option:first').prop('selected', true);
									$(searchResults).scrollTop(0);
								}
							}
						);
					}, 500);
				}
			});

			$('.table-product-promotions')
				.on('click', 'a[data-action="action-product-promotion-edit"]', function() {
					var promotionIndex = $(this).attr('data-target');

					if (productPromotions[promotionIndex] != undefined) {
						var promotion = productPromotions[promotionIndex];

						$('#field-promotion-pid').val(promotion.pid);
						$('#field-promotion-id').val(promotion.product_gift_id);
						$('#field-promotion-gift_quantity').val(promotion.gift_quantity);
						$('#field-promotion-gift_shipping_free').val(promotion.gift_shipping_free);
						$('#field-promotion-gift_max_quantity').val(promotion.gift_max_quantity);

						$('#field-promotion-product-search-str').val(promotion.product_id);
						var promoProductSearchResultEle = $('#field-promotion-product-search-result');
						promoProductSearchResultEle.html('<option value="' + promotion.gift_pid + '">' + htmlentities(promotion.product_id + ' - ' + promotion.title) +'</option>');
						promoProductSearchResultEle.find('option:first').prop('selected', true);
						promoProductSearchResultEle.scrollTop(0);


						$('#modal-product-promotion').modal('show');
					}

					return false;
				})
				.on('click', 'a[data-action="action-product-promotion-delete"]', function() {
					var promotionIndex = $(this).attr('data-target');

					if (productPromotions[promotionIndex] != undefined) {
						var promotion = productPromotions[promotionIndex];

						AdminForm.confirm(trans.products_promotions.confirm_remove, function() {
							AdminForm.sendRequest(
								'admin.php?p=product_page_promotions&mode=promotion-delete',
								{'promotion[id]': promotion.product_gift_id, 'promotion[pid]': promotion.pid},
								'Deleting product promotion',
								function(data) {
									if (data.status) {
										productPromotions = data.promotions;
										renderProductPromotionsTable(productPromotions);
										AdminForm.displayAlert(trans.common.success);
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-' + key, message: message});
											});
										});

										AdminForm.displayErrors(errors);
									}

									return false;
								}
							);
						});
					}

					return false;
				});

			$('#modal-product-promotion').find('form').submit(function() {
				AdminForm.sendRequest(
					'admin.php?p=product_page_promotions&mode=promotion-' + ($('#field-promotion-id').val() == 'new' ? 'add' : 'update'),
					$('#modal-product-promotion').find('form').serialize(),
					'Saving product promotion',
					function(data) {
						if (data.status) {
							productPromotions = data.promotions;
							renderProductPromotionsTable(productPromotions);
							$('#modal-product-promotion').modal('hide');
							AdminForm.displayAlert(trans.common.success);
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: '.field-promotion-' + key, message: message});
								});
							});

							AdminForm.displayValidationErrors(errors);
						}
					}
				);

				return false;
			});

			if (productPromotions == undefined) productPromotions = [];

			renderProductPromotionsTable(productPromotions);
		});
	};

	renderProductPromotionsTable = function(promotions) {
		if (promotions.length == 0) {
			$('.table-product-promotions').hide();
		} else {
			$('.table-product-promotions tbody').html('');
			$(promotions).each(function(index, promotion) {
				var html =
					'<tr>' +
						'<td><a href="#" data-action="action-product-promotion-edit" data-target="' + index + '">' + htmlentities(promotion.product_id + ' - ' + promotion.title) + '</a></td>' +
						'<td>' + htmlentities(promotion.gift_quantity) + '</td>' +
						'<td>' + htmlentities(promotion.gift_max_quantity) + '</td>' +
						'<td>' + htmlentities(promotion.gift_shipping_free == 'Yes' ? 'No' : 'Yes') + '</td>' +
						'<td class="list-actions text-right">' +
							'<a href="#" data-action="action-product-promotion-edit" data-target="' + index + '"><span class="icon icon-edit"></span>Edit</a> '+
							'<a href="#" data-action="action-product-promotion-delete" data-target="' + index + '"><span class="icon icon-delete"></span>Delete</a>' +
						'</td>' +
					'</tr>';
				$(html).appendTo('.table-product-promotions tbody');
			});
			$('.table-product-promotions').show();
		}
	};

	init();

}(jQuery));



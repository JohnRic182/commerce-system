var BestsellerSettings = (function($){
	var init, toggleBestsellers, toggleCustomersAlsoBought;

	init = function() {
		$('#field-CatalogBestsellersAvailable').change(function() {
			toggleBestsellers();
		});
		toggleBestsellers();

		$('#field-CustomerAlsoBoughtAvailable').change(function() {
			toggleCustomersAlsoBought();
		});
		toggleCustomersAlsoBought();
	};

	toggleBestsellers = function() {
		var isEnabled = $('#field-CatalogBestsellersAvailable').is(':checked');

		$('#field-CatalogBestsellersCount').closest('.form-group').toggle(isEnabled);
		$('#field-CatalogBestsellersPeriod').closest('.form-group').toggle(isEnabled);
	};

	toggleCustomersAlsoBought = function() {
		var isEnabled = $('#field-CustomerAlsoBoughtAvailable').is(':checked');

		$('#field-CustomerAlsoBoughtCount').closest('.form-group').toggle(isEnabled);
	};

	init();

	return {
		init: init
	};
}(jQuery));

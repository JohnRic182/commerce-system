(function($) {
	var init, theAreaZips;

	init = function() {
		$(document).ready(function() {
			$('form[name="area-edit"]').submit(function(){
				var theForm = $(this);
				var errors = [];
				var zips = theForm.find('input[name="zip_codes"]');
				if ($.trim(zips.val()) == '') {
					errors.push({
						field: zips,
						message: trans.pikfly.zip_code_required
					});
				}
				var deliveryFee = theForm.find('input[name="delivery_fee"]');
				if ($.trim(deliveryFee.val()) == '') {
					errors.push({
						field: deliveryFee,
						message: trans.pikfly.delivery_fee_required
					});
				} else if (parseInt(deliveryFee.val()) < 0 || isNaN(parseInt(deliveryFee.val()))) {
					errors.push({
						field: deliveryFee,
						message: trans.pikfly.enter_delivery_fee
					});
				}
				var selectedDeliveryDays = theForm.find('input[name^="delivery_days"]:checked');
				if (selectedDeliveryDays.length == 0) {
					errors.push({
						field: theForm.find('input[name^="delivery_days"]'),
						message: trans.pikfly.delivery_day_required
					});
				}

				var deliveryTime = new Date();
				var beforeTime = new Date();

				var before_time_hour = theForm.find('select[name="before_time_hour"]').val();

				var time = before_time_hour.match(/(\d+):(\d+)/);
				var hours = parseInt(time[1], 10);
				if (theForm.find('select[name="before_time_am_pm"]').val() == 'pm') {
					hours += (hours < 12) ? 12 : 0;
				} else {
					hours -= (hours == 12) ? 12 : 0;
				}
				beforeTime.setHours(hours, 0, 0, 0);

				var delivery_time_hour = theForm.find('select[name="last_delivery_time_hour"]').val();
				time = delivery_time_hour.match(/(\d+):(\d+)/);
				hours = parseInt(time[1], 10);
				if (theForm.find('select[name="last_delivery_time_am_pm"]').val() == 'pm') {
					hours += (hours < 12) ? 12 : 0;
				} else {
					hours -= (hours == 12) ? 12 : 0;
				}
				deliveryTime.setHours(hours, 0, 0, 0);

				if (deliveryTime <= beforeTime) {
					errors.push({
						field: theForm.find('select[name^="last_delivery_time_hour"]'),
						message: trans.pikfly.message_delivery_time_same_day
					});
				}

				if (errors.length > 0) {
					AdminForm.displayErrors(errors);
				} else {
					AdminForm.sendRequest(
						theForm.attr('action'),
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=app&key=pikfly';
							} else {
								if (data.message !== undefined) {
									AdminForm.displayAlert(data.message, 'danger');
								} else if (data.errors !== undefined) {
									AdminForm.displayErrors(data.errors);
								}
							}
						}
					);
				}

				return false;
			});

			$('.delete-area').click(function() {
				var theLink = $(this);
				AdminForm.confirm(trans.pikfly.confirm_remove_delivery_area, function() {
					AdminForm.sendRequest(
						'admin.php?p=pikfly&mode=delete-area',
						'id='+theLink.attr('data-ssid')+'&nonce='+theLink.attr('data-nonce'), null,
						function(data) {
							if (data.status == 1) {
								var theArea = theLink.closest('.pikfly-area');
								var zips = theArea.attr('data-zipcodes').split(', ');
								theArea.remove();

								if ($('.pikfly-area').length == 0) {
									document.location = 'admin.php?p=app&key=pikfly';
								}
							} else {
								if (data.message !== undefined) {
									AdminForm.displayAlert(data.message, 'danger');
								}
							}
						}
					);
				});

				return false;
			});

			$('.pikfly-area')
				.on('itemRemoved', 'input', function(event) {
					ZipCodeMap.clearZip(event.item);
				})
				.each(function(idx, pikflyArea) {
					if (typeof(ZipCodeMap) !== 'undefined') {
						var theArea = $(pikflyArea);

						var zips = theArea.attr('data-zipcodes').split(', ');
						var fillColor = theArea.attr('data-color');

						ZipCodeMap.fillZips(zips, fillColor, fillColor);
					}
				});

			theAreaZips = $('#area-zips');
			theAreaZips.tagsinput();

			theAreaZips.on('beforeItemAdd', function(event) {
				var theInput = $(this);
				var thisArea = theInput.closest('.pikfly-area');
				var theColor = thisArea.attr('data-color');
				var ssid = thisArea.attr('data-ssid');

				event.item = $.trim(event.item);
				var zip = event.item;
				var hasZip = false;
				$('.pikfly-area').not('[data-ssid="'+ssid+'"]').each(function(idx, pikflyArea) {
					if (!hasZip) {
						var theArea = $(pikflyArea);
						if (ssid != theArea.attr('data-ssid')) {
							var zips = theArea.attr('data-zipcodes').split(', ');
							if ($.inArray(zip, zips) >= 0) {
								hasZip = true;
							}
						}
					}
				});

				if (!hasZip) {
					var ret = ZipCodeMap.fillZip(event.item, theColor);
					event.cancel = !ret;
				} else {
					event.cancel = true;
				}
			});

			$('body').on('map-changed', function() {
				$('.pikfly-area')
					.each(function(idx, pikflyArea) {
						if (typeof(ZipCodeMap) !== 'undefined') {
							var theArea = $(pikflyArea);

							var fillColor = theArea.attr('data-color');
							var zips = theAreaZips.tagsinput('items');

							ZipCodeMap.fillZips(zips, fillColor, fillColor);
						}
					});
			});
		});

		if (typeof(ZipCodeMap) !== 'undefined') {
			ZipCodeMap.registerPolygonClick(function(zip) {
				zip = zip.toString();
				if ($.inArray(zip, theAreaZips.tagsinput('items')) < 0) {
					theAreaZips.tagsinput('add', zip);
				} else {
					theAreaZips.tagsinput('remove', zip);
				}
			});
		}
	};

	init();
}(jQuery));
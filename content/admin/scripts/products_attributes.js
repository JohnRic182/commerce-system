var GlobalAttributes = (function($) {
	var init, setFields, productAssignAdd, productAssignRemove, productAssignSet, showHideAssignSection;

	init = function() {
		$(document).ready(function(){

			$("div.choose_flag").removeClass("col-md-6");
			$("div.choose_flag").addClass("col-md-12");

			$('#field-assign-products-search-result').dblclick(function(){ productAssignAdd(); });
			$('#field-assigned-products').dblclick(function(){ productAssignRemove(); });
			$('#btn-add-product').click(function(){ productAssignAdd(); return false; });
			$('#btn-remove-product').click(function(){ productAssignRemove(); return false; });

			productAssignSet();
			showHideAssignSection();

			// delete attributes
			$('[data-action="action-globalattributes-delete"]').click(function(){

				var deleteMode = $('input[name="form-globalattributes-delete-mode"]:checked').val();

				if (deleteMode == 'selected')
				{
					var ids = '';
					$('.checkbox-globalattributes:checked').each(function(){
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});

					if (ids != '')
					{
						window.location='admin.php?p=products_attribute&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
					}
					else
					{
						alert(trans.products_attributes.select_delete_attribute);
					}
				}
				else if (deleteMode == 'all')
				{
					window.location='admin.php?p=products_attribute&mode=delete&deleteMode=all' + '&nonce=' + $(this).attr('nonce');
				}
			});

			// check attribute type change
			setFields();
			$('#field-attribute_type').change(function(){
				setFields();
			});

			// handle form errors
			$('form[name=form-globalproductattribute]').submit(function() {
				var errors = [];
				if ($.trim($('#field-name').val()) == '') {
					errors.push({field: '#field-name', message: trans.products_attributes.enter_attribute_name});
				}

				if ($.trim($('#field-caption').val()) == '') {
					errors.push({field: '#field-caption', message: trans.products_attributes.enter_attribute_title});
				}

				if ($('#field-categories- option:selected').length == 0 && $("input[name='assign_type']:checked").val() == 'categories') {
					errors.push({field: '#field-categories-', message: trans.products_attributes.select_category});
				}

				if ($('#field-assigned-products option').length == 0 && $("input[name='assign_type']:checked").val() == 'products') {
					errors.push({field: '#field-assigned-products', message: trans.products_attributes.assing_products});
				}

				var $field_type = $("#field-attribute_type").val();

				switch ($field_type) {
					case 'text':
					case 'textarea': {
						if ($.trim($('#field-text_length').val()) == '') {
							errors.push({field: '#field-text_length', message: trans.products_attributes.enter_text_length});
						} else if (parseInt($.trim($('#field-text_length').val())) <= 0) {
							errors.push({field: '#field-text_length', message: trans.products_attributes.message_text_length_greater});
						}

						break;
					}
					case 'select':
					case 'radio': {
						if ($.trim($(".field-options").val()) == '') {
							errors.push({
								field: '.field-options',
								message: trans.products_attributes.enter_attribute_options
							});
						}

						break;
					}
				}

				AdminForm.displayValidationErrors(errors);
				return errors.length == 0;
			});

			var gaSearchTimeOut = null;

			$('#field-assign-products-search-str').keyup(function() {
				var str = $.trim($(this).val());

				if (str.length > 1) {
					if (gaSearchTimeOut) clearTimeout(gaSearchTimeOut);
					gaSearchTimeOut = setTimeout(function() {
						AdminForm.sendRequest(
							'admin.php?p=products_attribute&mode=get-products',
							{
								str : str
							},
							null,
							function(data) {
								var searchResults = $('#field-assign-products-search-result');
								$(searchResults).html('');
								if (data && data.status && data.products) {
									$.each(data.products, function(index, product) {
										$('<option onclick="return " value="' + product.pid + '">' + htmlentities(product.product_id + ' - ' + product.title) +'</option>').appendTo(searchResults);
									});
									$(searchResults).find('option:first').prop('selected', true);
									$(searchResults).scrollTop(0);
									if (data.products.length == 0)
									{
										AdminForm.displayAlert(trans.products_attributes.no_records, 'info');
									}
								}
								else
								{
									AdminForm.displayAlert(trans.products_attributes.no_records, 'info');
								}
							}
						);
					}, 500);
				}
			});

			$("#radio_field-assign_type_products,#radio_field-assign_type_categories").click(function() {
				showHideAssignSection();
			});

		});
	};

	productAssignAdd = function() {
		var opt = $('#field-assign-products-search-result').find('option:selected');
		if (opt.length > 0)
		{
			if ($('#field-assigned-products').find('[value='+$(opt).val()+']').length == 0)
			{
				$('<option value="' + $(opt).val() + '">' + $(opt).html() + '</option>').appendTo('#field-assigned-products');
			}
			$(opt).remove();
		}
		productAssignSet();
	};

	productAssignRemove = function() {
		var opt = $('#field-assigned-products').find('option:selected');
		if (opt.length > 0)
		{
			$('<option value="' + $(opt).val() + '">' + $(opt).html() + '</option>').appendTo('#field-assign-products-search-result');
			$(opt).remove();
		}
		productAssignSet();
	};

	productAssignSet = function() {
		var s = '';
		$('#field-assigned-products').find('option').each(function(index, option){
			s = s + (s == '' ? '' : ',') + $(option).attr('value');
		});

		$('#products_assigned_selected').val(s);
	};

	setFields = function(){
		var attributeType = $("#field-attribute_type").val();
		var optionsEnabled = attributeType == 'select' || attributeType == 'radio';
		var textEnabled = !optionsEnabled;

		$('#field-text_length').closest('.form-group').toggleClass('required', textEnabled).toggle(textEnabled);
		$('#field-options').closest('.form-group').toggleClass('required', optionsEnabled).toggle(optionsEnabled);
		$('#field-track_inventory').closest('.form-group').toggle(optionsEnabled);
	};

	showHideAssignSection = function()
	{
		var assign_type = $("input[name='assign_type']:checked").val();
		if (assign_type == 'products')
		{
			$("#field-categories-").parent().hide();
			$("#global-attributes-products-assign").show();
		}
		else
		{
			$("#field-categories-").parent().show();
			$("#global-attributes-products-assign").hide();
		}
	}

	init();

}(jQuery));


function removeGlobalAttribute(id, nonce) {
    AdminForm.confirm(trans.products_attributes.confirm_remove_global_attribute, function() {
        window.location = 'admin.php?p=products_attribute&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
    });

    return false;
}
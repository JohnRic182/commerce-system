var Shipping = (function($) {
	var init, updateRealTimeForm, deleteMethod;

	init = function() {
		$(document).ready(function() {
			$('#modal-add-method').on('hidden.bs.modal', function (e) {
				$('#modal-add-method').find('.form-group .error').remove();
			});

			$('#modal-add-flat-rate').on('hidden.bs.modal', function (e) {
				$('#modal-add-flat-rate').find('.form-group .error').remove();
			});

			$('#modal-ups-settings').on('hidden.bs.modal', function (e) {
				$('#modal-ups-settings').find('.form-group .error').remove();
			});

			$('#modal-usps-settings').on('hidden.bs.modal', function (e) {
				$('#modal-usps-settings').find('.form-group .error').remove();
			});

			$('#modal-canada-post-settings').on('hidden.bs.modal', function (e) {
				$('#modal-canada-post-settings').find('.form-group .error').remove();
			});

			var methodType = 'flat';
			var prev = $("#add-method-country").val();
			$('form[name=form-add-method] input[name=method\\[type\\]]').click(function() {
				methodType = $('form[name=form-add-method] input[name=method\\[type\\]]:checked').val();

				if (methodType != 'flat') {
					updateRealTimeForm();
				} else {
					$('#add-method-real-time-form').hide();
				}

				$('#add-method-flat-rate-form').toggle(methodType == 'flat');
			});
			$('form[name=form-add-method] select[name=method\\[country\\]]').change(function() {
				$("#add-method-country option[value='" + prev + "']").attr("selected", false);
				$("#add-method-country option[value='" + $(this).val() + "']").attr("selected", true);
				if (methodType != 'flat') {
					updateRealTimeForm();
				}
				prev = $(this).val();
			});

			$('form[name=form-add-method]').submit(function() {
				var theForm = $(this);
				var theModal = $(this);
				var errors = [];

				var methodType = theForm.find('input[name=method\\[type\\]]:checked').val();

				if (methodType == 'flat') {
					if ($.trim($('#add-method-flat-rate-name').val()) == '') {
						errors.push({
							field: '#add-method-flat-rate-name',
							message: 'Please enter title'
						});
					}

					var ratesErrors = [];
					var flatRatesRow = $('#add-method-rate-flat');
					var priceRatesRow = $('#add-method-rate-price');
					var weightRatesRow = $('#add-method-rate-base_weight');
					if (flatRatesRow.is(':visible')) {
						ratesErrors = ShippingFlatRate.validateFlatRates(flatRatesRow, 0);
					}
					else if (priceRatesRow.is(':visible')) {
						ratesErrors = ShippingFlatRate.validatePriceRates(priceRatesRow, 0);
					}
					else if (weightRatesRow.is(':visible')) {
						ratesErrors = ShippingFlatRate.validateWeightRates(weightRatesRow, 0);
					}

					$.each(ratesErrors, function(idx, error) {
						errors.push(error);
					});

					if (errors.length > 0) {
						AdminForm.displayValidationErrors(errors);
					} else {
						AdminForm.sendRequest(
							'admin.php?p=shipping&mode=new-method',
							theForm.find('input:visible,textarea:visible,select:visible,input[type=hidden]').serialize(), null,
							function(data) {
								if (data.status == 1) {
									document.location = 'admin.php?p=shipping';
								} else {
									if (data.message !== undefined) {
										AdminForm.displayModalAlert(theModal, data.message, 'danger');
									} else if (data.errors !== undefined) {
										AdminForm.displayModalErrors(theModal, data.errors);
									}
								}
							}
						);
					}
				} else {
					AdminForm.sendRequest(
						'admin.php?p=shipping&mode=new-real-time-methods',
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=shipping' + ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : '');
							} else {
								if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								} else if (data.errors !== undefined) {
									AdminForm.displayModalErrors(theModal, data.errors);
								}
							}
						}
					);
				}

				return false;
			});

			$('#add-flat-rate-type').change(function() {
				var type = $(this).val();

				$('#add-flat-rate-flat').toggle(type == 'flat');
				$('#add-flat-rate-price').toggle(type == 'price');
				$('#add-flat-rate-base_weight').toggle(type == 'base_weight');

				$($('#add-flat-rate-flat,#add-flat-rate-price,#add-flat-rate-base_weight').find('input:visible').get(0)).focus();
			});

			$('#add-method-flat-rate-type').change(function() {
				var type = $(this).val();

				$('#add-method-rate-flat').toggle(type == 'flat');
				$('#add-method-flat-rate-price').toggle(type == 'price');
				$('#add-method-rate-base_weight').toggle(type == 'base_weight');

				$($('#add-method-rate-flat,#add-method-rate-price,#add-method-rate-base_weight').find('input:visible').get(0)).focus();
			});

			$('.add-flat-rate').click(function() {
				var coid = $(this).attr('data-country');

				$('#add-flat-rate-country').val(coid);
				$('#modal-add-flat-rate').modal('show');
				return false;
			});

			$('form[name=form-add-flat-rate]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				var errors = [];
				if ($.trim($('#add-flat-rate-name').val()) == '') {
					errors.push({
						field: '#add-flat-rate-name',
						message: trans.shipping.enter_title
					});
				}

				var ratesErrors = [];
				var flatRatesRow = $('#add-flat-rate-flat');
				var priceRatesRow = $('#add-flat-rate-price');
				var weightRatesRow = $('#add-flat-rate-base_weight');
				if (flatRatesRow.is(':visible')) {
					ratesErrors = ShippingFlatRate.validateFlatRates(flatRatesRow, 0);
				}
				else if (priceRatesRow.is(':visible')) {
					ratesErrors = ShippingFlatRate.validatePriceRates(priceRatesRow, 0);
				}
				else if (weightRatesRow.is(':visible')) {
					ratesErrors = ShippingFlatRate.validateWeightRates(weightRatesRow, 0);
				}

				$.each(ratesErrors, function(idx, error) {
					errors.push(error);
				});

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
				} else {
					AdminForm.sendRequest(
						'admin.php?p=shipping&mode=new-method',
						theForm.find('input:visible,textarea:visible,select:visible,input[type=hidden]').serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=shipping' + ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : '');
							} else {
								if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message);
								} else if (data.errors !== undefined) {
									AdminForm.displayModalErrors(theModal, data.errors);
								}
							}
						}
					);
				}

				return false;
			});
		});

		$('#main-content')
			.on('click', '.delete-shipping-method', function() {
				var ssid = $(this).attr('data-ssid');
				var nonce = $('input[name=delete_nonce]').val();

				deleteMethod(ssid, nonce);

				return false;
			})
			.on('click', '.change-method-states', function() {
				var ssid = $(this).attr('data-ssid');

				AdminForm.sendRequest(
					'admin.php?p=shipping&mode=change-states&ssid=' + ssid,
					null, null,
					function(data) {
						var theModal = $('#modal-change-states');
						theModal.find('input[name=ssid]').val(ssid);
						theModal.find('.modal-body')
							.html(data.html)
							.find('.field-checkbox').each(function(idx, ele) {
								forms.initCheckbox($(ele));
								$(ele).find('input')
									.click(function() {
										var val = $(this).val();
										if ($(this).is(':checked') && val == '0') {
											$(this).closest('form').find('input[value!=0]').prop('checked', false).change();
										} else {
											if ($(this).is(':checked')) {
												$(this).closest('form').find('input[value=0]').prop('checked', false).change();
											} else {
												if ($(this).closest('form').find('input[value!=0]:checked').length == 0) {
													$(this).closest('form').find('input[value=0]').prop('checked', true).change();
												}
											}
										}
									});
							})
						;
						theModal.modal('show');
					},
					null, 'GET'
				);
				return false;
			})
			.on('click', '.change-method-countries', function() {
				var ssid = $(this).attr('data-ssid');

				AdminForm.sendRequest(
					'admin.php?p=shipping&mode=change-countries&ssid=' + ssid,
					null, null,
					function(data) {
						var theModal = $('#modal-change-countries');
						theModal.find('input[name=ssid]').val(ssid);
						theModal.find('.modal-body')
							.html(data.html)
							.find('.field-checkbox').each(function(idx, ele) {
								forms.initCheckbox(ele);
								$(ele).find('input')
									.click(function() {
										var val = $(this).val();
										if ($(this).is(':checked') && val == '0') {
											$(this).closest('form').find('input[value!=0]').prop('checked', false).change();
										} else {
											if ($(this).is(':checked')) {
												$(this).closest('form').find('input[value=0]').prop('checked', false).change();
											} else {
												if ($(this).closest('form').find('input[value!=0]:checked').length == 0) {
													$(this).closest('form').find('input[value=0]').prop('checked', true).change();
												}
											}
										}
									});
							});

						theModal.modal('show');
					},
					null, 'GET'
				);

				return false;
			});
	};

	updateRealTimeForm = function() {
		$('input[name=method\\[carrier_id\\]]').unbind('change');
		var countryId = $('form[name=form-add-method] select[name=method\\[country\\]]').val();

		AdminForm.sendRequest(
			'admin.php?p=shipping&mode=real-time-form&coid=' + countryId,
			null, null,
			function(data) {
				var theForm =
				$('#add-method-real-time-form')
					.html(data.html)
					.show();
				$('input[name=method\\[carrier_id\\]]').change(function() {
					var carrierId = $('input[name=method\\[carrier_id\\]]:checked').val();

					theForm.find('.add-method-ups').toggle(carrierId == 'ups');
					theForm.find('.add-method-usps').toggle(carrierId == 'usps');
					theForm.find('.add-method-fedex').toggle(carrierId == 'fedex');
					theForm.find('.add-method-canada_post').toggle(carrierId == 'canada_post');
                    theForm.find('.add-method-vparcel').toggle(carrierId == 'vparcel');
				});
			},
			null, 'GET'
		);
	};

	deleteMethod = function(id, nonce) {
		AdminForm.confirm(trans.shipping.confirm_remove_shipping_method, function() {
			window.location = 'admin.php?p=shipping&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	init();
}(jQuery));

var ShippingFlatRate = (function($) {
	var init,
		enableEdit, cancelEdit, lastSrid = -1,
		validateFlatRates,
		validatePriceRates,
		validateWeightRates,
		validateTitle,
		changeType,
		handleClearRow
		;

	init = function() {
		var mainContent = $('#main-content');
		mainContent.on('click', '.edit-shipping-method', function() {
			var theRow = $(this).closest('tr');
			var ssid = theRow.attr('data-ssid');

			enableEdit(theRow, ssid);

			return false;
		})
			.on('click', '.cancel-shipping-method', function() {
				var ssid = $(this).attr('data-ssid');
				cancelEdit(ssid);

				return false;
			})
			.on('click', '.save-shipping-method', function() {
				var theRow = $(this).closest('tr');
				var ssid = theRow.attr('data-ssid');

				var errors = [];

				var methodRows = $('tr[data-ssid="'+ssid+'"]');
				var type = methodRows.find('select[name*=method_id]').val();

				var lastMax = 0;
				var ratesErrors;
				methodRows.not('.tax-rate-new-rate').each(function(idx, ele) {
					ele = $(ele);
					ratesErrors = ShippingFlatRate.validateTitle(ele);
					if (ratesErrors.length > 0) {
						$.each(ratesErrors, function(idx, error) {
							errors.push(error);
						});
					}

					if (type == 'flat') {
						ratesErrors = ShippingFlatRate.validateFlatRates(ele, lastMax);
						if (ratesErrors.length > 0) {
							$.each(ratesErrors, function(idx, error) {
								errors.push(error);
							});
						}
					} else if (type == 'price') {
						ratesErrors = ShippingFlatRate.validatePriceRates(ele, lastMax);
						if (ratesErrors.length > 0) {
							$.each(ratesErrors, function(idx, error) {
								errors.push(error);
							});
						}
					} else if (type == 'base_weight') {
						ratesErrors = ShippingFlatRate.validateWeightRates(ele, lastMax);
						if (ratesErrors.length > 0) {
							$.each(ratesErrors, function(idx, error) {
								errors.push(error);
							});
						}
					}

					lastMax = parseFloat(ele.find('input[name*=rate_max]').val());
				});

				if (errors.length > 0) {
					AdminForm.displayTableValidationErrors(errors);
				} else {
					var data = methodRows.not('.tax-rate-new-rate').find('input,textarea,select').serialize();
					console.log(data);
					AdminForm.clearAlerts();
					AdminForm.sendRequest(
						'admin.php?p=shipping&mode=update-shipping-method&id='+ssid,
						data,
						null,
						function(data) {
							if (data.status == 1) {
								AdminForm.displayAlert(data.message);
								$('tr[data-ssid="'+ssid+'"].rates-row').remove();
								theRow.replaceWith($(data.html));

								$('tr[data-ssid="'+ssid+'"] .field-checkbox').each(function(idx, ele) {
									forms.initCheckbox(ele);
								});
							} else {
								//TODO: Handle error status
							}
						}
					);
				}

				return false;
			});

		$(document.body).on('change', '.flat-method-type', function() {
			//TODO: Handle change event when method changed
			var theRow = $(this).closest('tr');
			var type = $(this).val();

			changeType(theRow, type);
		});
	};

	changeType = function(theRow, type) {
		var ssid = $(theRow).attr('data-ssid');

		AdminForm.sendRequest(
			'admin.php?p=shipping&mode=edit-method&id=' + ssid + '&change-type=' + type,
			null, null,
			function(data) {
				if (data.status == 0) {
					cancelEdit(ssid);
				} else {
					var newMethodRow = $(data.html);
					theRow.find('input').unbind('blur').unbind('keypress');
					$('tr[data-ssid="'+ssid+'"].rates-row').remove();
					theRow.replaceWith(newMethodRow);

					$('.tax-rate-new-rate input').blur(function() {
						var val = $(this).val();

						if ($.trim(val) != '') {
							var theRow = $(this).closest('tr');
							var newRow = theRow.clone(true);

							var ssid = theRow.attr('data-ssid');
							var theSrid = lastSrid - 1;

							theRow.removeClass('tax-rate-new-rate');
							theRow.find('input,textarea,select').unbind('blur').blur(function() {
								handleClearRow($(this));
							});
							newRow.find('input,textarea,select').each(function(idx, ele) {
								var name = $(ele).attr('name');
								$(ele).attr('name', name.replace('['+lastSrid+']', '['+theSrid+']')).val('');
							});

							newRow.find('select[name$=price_type\\]]').val(theRow.find('select[name$=price_type\\]]').val());

							newRow.find('td').each(function(idx, ele) {
								if ($(ele).attr('rowspan')) {
									$(ele).remove();
								}
							});

							if (!newRow.hasClass('rates-row')) {
								newRow.addClass('rates-row');
							}

							var theTopRow = $($('tr[data-ssid="'+ssid+'"]').get(0));
							var theTd = $(theTopRow.find('td').get(0));
							var rowSpan = parseInt(theTd.attr('rowspan'));
							theTopRow.find('td[rowspan='+rowSpan+']').attr('rowspan', rowSpan + 1);

							newRow.insertAfter(theRow);

							lastSrid = theSrid;
						}
					});

					$('tr[data-ssid="'+ssid+'"] input').keypress(function(e) {
						var code = e.keyCode || e.which;
						if (code == 13) {
							var theRow = $(this).closest('tr');

							$('a[data-ssid="'+theRow.attr('data-ssid')+'"].save-shipping-method').click();
						}
					});

					$('tr[data-ssid="'+ssid+'"] .field-checkbox').each(function(idx, ele) {
						forms.initCheckbox(ele);
					});
				}
			},
			null, 'GET'
		);
	};

	validateTitle = function(theRow) {
		var ele = $(theRow).find('input[name*=carrier_name]');

		var errors = [];
		if (ele.length > 0) {
			if ($.trim(ele.val()) == '') {
				errors.push({
					field: ele,
					message: trans.shipping.enter_title
				});
			}
		}
		return errors;
	};

	validateFlatRates = function(theRow, lastMaxValue) {
		var minEle = $(theRow).find('input[name*=rate_min]');
		var maxEle = $(theRow).find('input[name*=rate_max]');
		var priceEle = $(theRow).find('input[name*=price]');
		var min = parseInt(minEle.val());
		var max = parseInt(maxEle.val());
		var price = priceEle.val();

		var errors = [];
		if (isNaN(min) || min < 0 || min < lastMaxValue || isNaN(lastMaxValue)) {
			errors.push({
				field: minEle,
				message: trans.shipping.enter_min_items
			});
		}
		if (!isNaN(max) && max < min) {
			errors.push({
				field: maxEle,
				message: trans.shipping.enter_max_items
			});
		}
		if ($.trim(price) == '') {
			errors.push({
				field: priceEle,
				message: trans.shipping.enter_price
			});
		}
		return errors;
	};


	validatePriceRates = function(theRow, lastMaxValue) {
		var minEle = $(theRow).find('input[name*=rate_min]');
		var maxEle = $(theRow).find('input[name*=rate_max]');
		var priceEle = $(theRow).find('input[name*=price]');
		var min = parseFloat(minEle.val());
		var max = parseFloat(maxEle.val());
		var price = priceEle.val();

		var errors = [];
		if (isNaN(min) || min < 0 || min < lastMaxValue) {
			errors.push({
				field: minEle,
				message: trans.shipping.enter_min_subtotal
			});
		}
		if (!isNaN(max) && max < min) {
			errors.push({
				field: maxEle,
				message: trans.shipping.enter_max_subtotal
			});
		}
		if ($.trim(price) == '') {
			errors.push({
				field: priceEle,
				message: trans.shipping.enter_price
			});
		}
		return errors;
	};

	validateWeightRates = function(theRow, lastMaxValue) {
		var minEle = $(theRow).find('input[name*=rate_min]');
		var maxEle = $(theRow).find('input[name*=rate_max]');
		var priceEle = $(theRow).find('input[name*=base]');
		var min = parseFloat(minEle.val());
		var max = parseFloat(maxEle.val());
		var price = priceEle.val();

		var errors = [];
		if (isNaN(min) || min < 0 || min < lastMaxValue) {
			errors.push({
				field: minEle,
				message: trans.shipping.enter_min_weight
			});
		}
		if (!isNaN(max) && max < min) {
			errors.push({
				field: maxEle,
				message: trans.shipping.enter_max_weight
			});
		}
		if ($.trim(price) == '') {
			errors.push({
				field: priceEle,
				message: trans.shipping.enter_price
			});
		}
		return errors;
	};

	handleClearRow = function(theInput) {
		var theRow = theInput.closest('tr');
		var ssid = theRow.attr('data-ssid');

		var hasValue = false;
		theRow.find('input,textarea').each(function(idx, ele) {
			if ($.trim($(ele).val()) != '') {
				hasValue = true;
			}
		});

		if (!hasValue) {
			theRow.find('input,textarea').unbind('blur');
			theRow.remove();

			var theTopRow = $($('tr[data-ssid="'+ssid+'"]').get(0));
			var theTd = $(theTopRow.find('td').get(0));
			var rowSpan = parseInt(theTd.attr('rowspan'));
			theTopRow.find('td[rowspan='+rowSpan+']').attr('rowspan', rowSpan - 1);
		}
	};

	enableEdit = function(theRow, ssid) {
		AdminForm.sendRequest(
			'admin.php?p=shipping&mode=edit-method&id=' + ssid,
			null, null,
			function(data) {
				if (data.status == 0) {
					cancelEdit(ssid);
				} else {
					var newMethodRow = $(data.html);
					$('tr[data-ssid="'+ssid+'"].rates-row').remove();
					theRow.replaceWith(newMethodRow);
					$('.tax-rate-new-rate input').blur(function() {
						var val = $(this).val();

						if ($.trim(val) != '') {
							var theRow = $(this).closest('tr');
							var newRow = theRow.clone(true);

							var ssid = theRow.attr('data-ssid');
							var theSrid = lastSrid - 1;

							theRow.removeClass('tax-rate-new-rate');
							theRow.find('input,textarea,select').unbind('blur').blur(function() {
								handleClearRow($(this));
							});
							newRow.find('input,textarea,select').each(function(idx, ele) {
								var name = $(ele).attr('name');
								$(ele).attr('name', name.replace('['+lastSrid+']', '['+theSrid+']')).val('');
							});

							newRow.find('select[name$=price_type\\]]').val(theRow.find('select[name$=price_type\\]]').val());

							newRow.find('td').each(function(idx, ele) {
								if ($(ele).attr('rowspan')) {
									$(ele).remove();
								}
							});

							if (!newRow.hasClass('rates-row')) {
								newRow.addClass('rates-row');
							}

							var theTopRow = $($('tr[data-ssid="'+ssid+'"]').get(0));
							var theTd = $(theTopRow.find('td').get(0));
							var rowSpan = parseInt(theTd.attr('rowspan'));
							theTopRow.find('td[rowspan='+rowSpan+']').attr('rowspan', rowSpan + 1);

							newRow.insertAfter(theRow);

							lastSrid = theSrid;
						}
					});

					$('tr[data-ssid="'+ssid+'"] input').keypress(function(e) {
						var code = e.keyCode || e.which;
						if (code == 13) {
							var theRow = $(this).closest('tr');

							$('a[data-ssid="'+theRow.attr('data-ssid')+'"].save-shipping-method').click();
						}
					});

					$('tr[data-ssid="'+ssid+'"]').not('.tax-rate-new-rate').find('input,textarea').blur(function() {
						handleClearRow($(this));
					});
					$('tr[data-ssid="'+ssid+'"] .field-checkbox').each(function(idx, ele) {
						forms.initCheckbox(ele);
					});
				}
			},
			null, 'GET'
		);
	};

	cancelEdit = function(ssid) {
		var theRow = $('tr[data-ssid="' + ssid + '"]');
		if (theRow.length > 0) {
			AdminForm.sendRequest(
				'admin.php?p=shipping&mode=view-method&id=' + ssid,
				null, null,
				function(data) {
					if (data.status == 0) {
						theRow.remove();
						$('tr[data-ssid="'+ssid+'"]').remove();
					} else {
						var newMethodRow = $(data.html);
						theRow.find('input').unbind('blur').unbind('keypress');
						$('tr[data-ssid="'+ssid+'"].rates-row').remove();
						theRow.replaceWith(newMethodRow);

						$('tr[data-ssid="'+ssid+'"] .field-checkbox').each(function(idx, ele) {
							forms.initCheckbox(ele);
						});
					}
				},
				null, 'GET'
			);
		}
	};

	init();

	return {
		validateFlatRates: validateFlatRates,
		validatePriceRates: validatePriceRates,
		validateWeightRates: validateWeightRates,
		validateTitle: validateTitle
	};
}(jQuery));

var ShippingRealTime = (function($) {
	var init,
		enableEdit, cancelEdit, validateTitle
		;

	init = function() {
		$(document).ready(function() {
			$('.add-real-time').click(function() {
				var countryId = $(this).attr('data-country');
				var carrierId = $(this).attr('data-carrier-id');

				var theModal = $('#modal-add-real-time');
				theModal.find('input[name=method\\[country\\]]').val(countryId);

				AdminForm.sendRequest(
					'admin.php?p=shipping&mode=real-time-form&coid=' + countryId + '&carrier_id=' + carrierId,
					null, null,
					function(data) {
						$('#add-real-time-form')
							.html(data.html)
							.show();
						theModal.find('input[name=method\\[carrier_id\\]]').change(function() {
							var carrierId = $('input[name=method\\[carrier_id\\]]:checked').val();

							theModal.find('.add-method-ups').toggle(carrierId == 'ups');
							theModal.find('.add-method-usps').toggle(carrierId == 'usps');
							theModal.find('.add-method-fedex').toggle(carrierId == 'fedex');
							theModal.find('.add-method-canada_post').toggle(carrierId == 'canada_post');
                            theModal.find('.add-method-vparcel').toggle(carrierId == 'vparcel');
						});
					},
					null, 'GET'
				);

				theModal.modal('show');

				return false;
			});

			$('form[name=form-add-real-time]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				var errors = [];

				if (errors.length > 0) {
					AdminForm.displayModalErrors(theModal, errors);
				} else {
					AdminForm.sendRequest(
						'admin.php?p=shipping&mode=new-real-time-methods',
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=shipping' + ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : '');
							} else {
								if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								} else if (data.errors !== undefined) {
									AdminForm.displayModalErrors(theModal, data.errors);
								}
							}
						}
					);
				}

				return false;
			});

			var handleUSPSInternationalPackageSize = function() {
				var showPackageSizes = $('#field-ShippingUSPSPackageSize').val() != 'REGULAR';

				$('#field-ShippingUSPSPackageWidth').closest('.form-group').toggle(showPackageSizes);
				$('#field-ShippingUSPSPackageLength').closest('.form-group').toggle(showPackageSizes);
				$('#field-ShippingUSPSPackageHeight').closest('.form-group').toggle(showPackageSizes);
			};
			$('#field-ShippingUSPSPackageSize').change(function() {
				handleUSPSInternationalPackageSize();
			});
			handleUSPSInternationalPackageSize();

			$('#form-usps-settings').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var urlPattern =/^(?:(?:https?):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
				var errors = [];

				if ($.trim($('#field-ShippingUSPSUserID').val()) == '') {
					errors.push({
						field: '#field-ShippingUSPSUserID',
						message: trans.shipping.enter_usps_tools_user_id
					});
				}
				if ($.trim($('#field-ShippingUSPSUrl').val()) == '' || !urlPattern.test($('#field-ShippingUSPSUrl').val())) {
					errors.push({
						field: '#field-ShippingUSPSUrl',
						message:  trans.shipping.enter_server_url_ups
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
					return false;
				} else {
					return true;
				}
			});
			$('#form-fedex-settings').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var errors = [];

				if ($.trim($('#field-ShippingFedExAccountNumber').val()) == '') {
					errors.push({
						field: '#field-ShippingFedExAccountNumber',
						message: trans.shipping.enter_fedex_account_number
					});
				}
				if ($.trim($('#field-ShippingFedExApiKey').val()) == '') {
					errors.push({
						field: '#field-ShippingFedExApiKey',
						message: trans.shipping.enter_fedex_api_key
					});
				}
				if ($.trim($('#field-ShippingFedExApiPassword').val()) == '') {
					errors.push({
						field: '#field-ShippingFedExApiPassword',
						message: trans.shipping.enter_fedex_api_password
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
					return false;
				} else {
					return true;
				}
			});
			$('#form-ups-settings').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var errors = [];

				if ($.trim($('#field-ShippingUPSAccessKey').val()) == '') {
					errors.push({
						field: '#field-ShippingUPSAccessKey',
						message: trans.shipping.enter_ups_xml_access_key
					});
				}
				if ($.trim($('#field-ShippingUPSUserID').val()) == '') {
					errors.push({
						field: '#field-ShippingUPSUserID',
						message: trans.shipping.enter_ups_user_id
					});
				}
				if ($.trim($('#field-ShippingUPSUserPassword').val()) == '') {
					errors.push({
						field: '#field-ShippingUPSUserPassword',
						message: trans.shipping.enter_ups_password
					});
				}
				if ($.trim($('#field-ShippingUPSUrl').val()) == '') {
					errors.push({
						field: '#field-ShippingUPSUrl',
						message: trans.shipping.enter_server_url_ups
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
					return false;
				} else {
					return true;
				}
			});
			$('#form-canada-post-settings').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var errors = [];

				if ($.trim($('#field-ShippingCPMerchantID').val()) == '') {
					errors.push({
						field: '#field-ShippingCPMerchantID',
						message: trans.shipping.enter_canada_merchant_id
					});
				}
				if ($.trim($('#field-ShippingCPUrlRating').val()) == '') {
					errors.push({
						field: '#field-ShippingCPUrlRating',
						message: trans.shipping.enter_server_url_canada_post
					});
				}
				if ($.trim($('#field-ShippingCPTAT').val()) == '') {
					errors.push({
						field: '#field-ShippingCPTAT',
						message: trans.shipping.enter_turn_around_time
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
					return false;
				} else {
					return true;
				}
			});
			$('#form-vparcel-settings').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var errors = [];

				if ($.trim($('#field-ShippingVIPparcelauthToken').val()) == '') {
					errors.push({
						field: '#field-ShippingVIPparcelauthToken',
						message: trans.shipping.enter_shipping_vip_parcel_auth_token
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
					return false;
				} else {
					return true;
				}
			});
		});

		var mainContent = $('#main-content');
		mainContent.on('click', '.edit-real-time-method', function() {
			var theRow = $(this).closest('tr');
			var ssid = theRow.attr('data-ssid');

			enableEdit(theRow, ssid);

			return false;
		})
			.on('click', '.cancel-real-time-method', function() {
				var ssid = $(this).attr('data-ssid');
				cancelEdit(ssid);

				return false;
			})
			.on('click', '.save-real-time-method', function() {
				var theRow = $(this).closest('tr');
				var ssid = theRow.attr('data-ssid');

				var errors =  validateTitle(theRow);

				if (errors.length > 0) {
					AdminForm.displayTableValidationErrors(errors);
				} else {
					var data = $('tr[data-ssid="'+ssid+'"]').find('input,textarea,select').serialize();
					console.log(data);
					AdminForm.clearAlerts();
					AdminForm.sendRequest(
						'admin.php?p=shipping&mode=update-shipping-method&id='+ssid,
						data,
						null,
						function(data) {
							if (data.status == 1) {
								AdminForm.displayAlert(data.message);

								$('tr[data-ssid="'+ssid+'"].rates-row').remove();
								theRow.replaceWith($(data.html));

								$('tr[data-ssid="'+ssid+'"] .field-checkbox').each(function(idx, ele) {
									forms.initCheckbox(ele);
								});
							} else {
								//TODO: Handle error status
							}
						}
					);
				}

				return false;
			});
	};

	enableEdit = function(theRow, ssid) {
		AdminForm.sendRequest(
			'admin.php?p=shipping&mode=edit-method&id=' + ssid,
			null, null,
			function(data) {
				if (data.status == 0) {
					cancelEdit(ssid);
				} else {
					var newMethodRow = $(data.html);
					theRow.replaceWith(newMethodRow);

					$('tr[data-ssid="'+ssid+'"] .field-checkbox').each(function(idx, ele) {
						forms.initCheckbox(ele);
					});
					$('tr[data-ssid="'+ssid+'"] input').keypress(function(e) {
						var code = e.keyCode || e.which;
						if (code == 13) {
							var theRow = $(this).closest('tr');

							$('a[data-ssid="'+theRow.attr('data-ssid')+'"].save-real-time-method').click();
						}
					});
				}
			},
			null, 'GET'
		);
	};

	cancelEdit = function(ssid) {
		var theRow = $('tr[data-ssid="' + ssid + '"]');
		if (theRow.length > 0) {
			AdminForm.sendRequest(
				'admin.php?p=shipping&mode=view-method&id=' + ssid,
				null, null,
				function(data) {
					$('tr[data-ssid="' + ssid + '"] input').unbind('keypress');

					if (data.status == 0) {
						theRow.remove();
						$('tr[data-ssid="' + ssid + '"]').remove();
					} else {
						var newMethodRow = $(data.html);
						theRow.replaceWith(newMethodRow);
						$('tr[data-ssid="' + ssid + '"] .field-checkbox').each(function(idx, ele) {
							forms.initCheckbox(ele);
						});
					}
				},
				null, 'GET'
			);
		}
	};

	validateTitle = function(theRow) {
		var ele = $(theRow).find('input[name*=method_name]');

		var errors = [];
		if (ele.length > 0) {
			if ($.trim(ele.val()) == '') {
				errors.push({
					field: ele,
					message:  trans.shipping.enter_title
				});
			}
		}
		return errors;
	};

	init();
}(jQuery));


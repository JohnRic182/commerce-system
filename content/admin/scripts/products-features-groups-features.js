var ProductsFeaturesGroupsFeatures = (function($) {
	var init, renderProductsFeaturesGroupsTable, updateAssignBox;

	updateAssignBox = function() {
		if (productsFeaturesAvailable.length > 0) {
			var assigned = [];
			if (productsFeaturesAssigned.length > 0) {
				$.each(productsFeaturesAssigned, function (index, feature) {
					assigned.push(feature.product_feature_id);
				});
			}

			var options = '';
			$.each(productsFeaturesAvailable, function(index, feature) {
				var selected = $.inArray(feature.product_feature_id, assigned) != -1;
				options +=
					'<option value="' + feature.product_feature_id + '" ' + (selected ? ' selected' : '') + '>' +
					feature.feature_name  + ' (' + feature.feature_id + ')' +
					'</option>';
			});

			if ($('#dual-list-box-assign-features').length > 0) {
				$('#dual-list-box-assign-features').replaceWith(
					'<div class="form-group">' +
						'<select id="select-products-features-group-features" multiple="multiple" name="copy[selected-fields][]"></select>' +
					'</div>'
				);
			}

			$('#select-products-features-group-features').html(options);

			$('#select-products-features-group-features').DualListBox({json: false, title: 'assign-features'}, []);
			$('#modal-feature-group-features span.unselected-title').text(trans.features_groups.available_features);
			$('#modal-feature-group-features span.selected-title').text(trans.features_groups.assign_features);
		}
	};

	init = function() {
		$(document).ready(function() {

			// init values
			if (typeof productsFeaturesAvailable == 'undefined' || productsFeaturesAvailable === null) productsFeaturesAvailable = [];
			if (typeof productsFeaturesAssigned == 'undefined' || productsFeaturesAssigned === null) productsFeaturesAssigned = [];

			updateAssignBox();

			// adding new product feature functionality (add product feature button at edit product feature group page)
			$('a[data-action="action-group-feature-add"]').click(function() {
				$('#modal-feature-group-features-add').modal('show');
				return false;
			});

			// adding new product feature functionality(submit form action at edit product feature group page)
			$('#modal-feature-group-features-add').find('form').submit(function() {
				AdminForm.sendRequest(
					'admin.php?p=feature_groups&mode=group-feature-add',
					$('#modal-feature-group-features-add').find('form').serialize(),
					'Saving group feature',
					function(data) {
						if (data.status) {
							productsFeaturesAvailable = data.productsFeaturesAvailable;
							productsFeaturesAssigned = data.productsFeaturesAssigned;

							updateAssignBox();

							renderProductsFeaturesGroupsTable(productsFeaturesAssigned);

							$('#modal-feature-group-features-add').modal('hide');
							AdminForm.displayAlert(trans.common.success);
						}
						else {
							var errors = [];
							$.each(data.errors, function(field, message) {
								errors.push({
									field: '#field-' + field,
									message: message
								});
							});

							AdminForm.displayValidationErrors(errors);
						}
					}
				);

				return false;
			});

			// assign button action
			$('a[data-action="action-group-feature-assign"]').click(function() {
				if (productsFeaturesAvailable.length > 0) {
					$('#modal-feature-group-features').modal('show');
				} else {
					AdminForm.displayAlert('In order to assign products features, please add them first!');
				}
				return false;
			});

			// save assigned features
			$('#modal-feature-group-features').find('form').submit(function() {
				var theForm = $(this);
				var selected = '';

				theForm.find('select.selected option').each(function(index, el) {
					selected += (selected == '' ? '' : ',') + $(el).val();
				});

				AdminForm.sendRequest(
					'admin.php?p=feature_groups&mode=group-feature-assign',
					{
						feature_group_id: $('#field-product_feature_group_id').val(),
						features_assigned: selected
					},
					'Adding feature group features',
					function(data) {
						if (data.status) {
							productsFeaturesAssigned = data.productsFeaturesAssigned;
							renderProductsFeaturesGroupsTable(productsFeaturesAssigned);

							$('#modal-feature-group-features').modal('hide');
							AdminForm.displayAlert(trans.common.success);
						}
						else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: '.field-group-features-' + key, message: message});
								});
							});

							AdminForm.displayModalErrors($('#modal-feature-group-features').modal(), errors);
						}
					}
				);

				return false;
			});

			$('.table-product-group-features')
				.on('click', 'a[data-action="action-feature-group-features-delete"]', function() {
					var featureIndex = $(this).attr('data-target');

					if (productsFeaturesAssigned[featureIndex] != undefined) {
						var feature = productsFeaturesAssigned[featureIndex];

						AdminForm.confirm(trans.features_groups.confirm_remove, function() {
							AdminForm.sendRequest(
								'admin.php?p=feature_groups&mode=group-feature-delete',
								{
									'feature[product_feature_group_relation_id]': feature.product_feature_group_relation_id,
									'feature[product_feature_group_id]': feature.product_feature_group_id
								},
								'Deleting product feature group features',
								function(data) {
									if (data.status) {
										productsFeaturesAssigned = data.productsFeaturesAssigned;

										renderProductsFeaturesGroupsTable(productsFeaturesAssigned);
										AdminForm.displayAlert(trans.common.success);
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-' + key, message: message});
											});
										});
										AdminForm.displayErrors(errors);
									}
								}
							);
						});
					}
				});

			renderProductsFeaturesGroupsTable(productsFeaturesAssigned);
		});
	};

	/**
	 * @param features
	 */
	renderProductsFeaturesGroupsTable = function(features) {
		var assignFeatures = [];
		var deletedFeatures = [];

		if (features.length == 0) {
			$('.table-product-group-features').hide();
			$('.none-product-group-features').show();
		}
		else {
			$('.none-product-group-features').hide();
			$('.table-product-group-features tbody').html('');

			var priorityOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
			$(features).each(function(index, feature) {
				var html = $('<tr>');
				html.append('<td>' + htmlentities(feature.feature_name) + '</td>');
				html.append('<td>' + htmlentities(feature.feature_id) + '</td>');

				var dropDownOptions = $('<td>');
				dropDownOptions.append(AdminForm.dropDownOptions('priority['+ feature.product_feature_id +']', 'priority-' + feature.product_feature_id, priorityOptions, feature.priority));
				html.append(dropDownOptions);

				html.append('<td class="list-actions text-right"><a href="#" data-action="action-feature-group-features-delete" data-target="' + index + '"><span class="icon icon-delete"></span>Unassign</a></td>');
				$(html).appendTo('.table-product-group-features tbody');

				assignFeatures.push(feature.product_feature_id)
			});

			$('.table-product-group-features').show();
		}

		$('#modal-feature-group-features select.selected option').each(function(index, el) {
			var dropDownValue = $(el).val();
			var dropDownText = $(el).text();

			// returns -1 if the item is not found
			if(assignFeatures.indexOf(dropDownValue) < 0) {
				deletedFeatures.push({value: dropDownValue, text: dropDownText});
				$('#modal-feature-group-features select.unselected').append($('<option>', {value: dropDownValue, text: dropDownText}));
			}
		});

		$.each(deletedFeatures, function(index, el) {
			$('#modal-feature-group-features select.selected option[value="' + el.value + '"]').remove();
		});
	};

	init();

} (jQuery));

$(document).ready(function(){
	$('#modal-custom-field').on('hidden.bs.modal', function () {
		$('#modal-custom-field').find('.form-group .error').remove();
	});

	$('.form-custom-forms-delete').submit(function(){
		return false;
	});

	if ($('form[name="form-custom-form"] input[name="id"]').length > 0 &&
		$('form[name="form-custom-form"] input[name="id"]').val() != '' &&
		$('.table-custom-fields tbody tr').length == 0)
	{
		$('[data-action="action-custom-field-add"]').popover({
			content: 'Click here to add new custom form field',
			placement: 'top',
			trigger: 'manual'
		});
		$('[data-action="action-custom-field-add"]').popover('show');
	}

	$('form[name=form-custom-form]').submit(function() {
		var errors = [];

		if ($.trim($('#field-title').val()) == '') {
			errors.push({
				field: '#field-title',
				message: trans.custom_form.enter_title
			});
		}

		if ($.trim($('#field-form_id').val()) == '') {
			errors.push({
				field: '#field-form_id',
				message: trans.custom_form.enter_form_id
			});
		}

		if ($('#field-redirect_on_submit').is(':checked') && ($.trim($('#field-redirect_on_submit_url').val()) == '')) {
			errors.push({
				field: '#field-redirect_on_submit_url',
				message: trans.custom_form.enter_redirect_url
			});
		}

		if (errors.length > 0) AdminForm.displayValidationErrors(errors);

		return errors.length == 0;
	});

	/**
	 * Handle delete button
	 */
	$('[data-action="action-custom-form-delete"]').click(function(e) {
		e.stopPropagation();
		var formId = $(this).attr('data-form-id');
		var deleteNonce = $(this).attr('data-delete-nonce');
		AdminForm.confirm(trans.custom_form.confirm_delete_custom_form, function() {
			window.location = 'admin.php?p=custom_form&mode=delete&deleteMode=single&id=' + formId + '&nonce=' + deleteNonce;
		});
		return false;
	});
});

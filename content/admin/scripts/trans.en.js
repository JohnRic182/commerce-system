var trans = {
	'admins': {
		'confirm_remove': 'Are you sure you wish to remove this administrator?',
		'name_required': 'Please enter first & last name',
		'first_name_required': 'Please enter first name',
		'last_name_required': 'Please enter last name',
		'email_required': 'Please enter email',
		'username_required': 'Please enter username',
		'password_required': 'Please enter password',
		'password_confirmation_mismatch': 'Password and confirmation are not equal',
		'password_invalid': 'Password is too short.  Minimum length is 8 characters',
		'rights_invalid': 'Please choose at least one privilege!',
		'no_admins_selected': 'Please select at least one administrator to delete',
		'invalid_email': 'Please enter a valid email'
	},
	'api': {
		'username_required': 'Please enter username',
		'password_required': 'Please enter password',
		'token_required': 'Please either generate or enter a security token'
	},
	'apps': {
		'confirm_disable': 'Are you sure you wish to disable {title}?'
	},
	'avalara': {
		'confirm_remove_tax_code': 'Are you sure you wish to remove this tax code?'
	},
	'billing': {
		'select_service_length': 'Please choose service length',
		'enter_reason_cancel_service': 'Please enter reason for canceling the service',
		'confirm_cancel_service': 'Are you sure you wish to cancel service?',
		'message_request_recieve': 'Your request has been received.  Please allow up to 72 business hours for us to process and verify your cancellation.<br>',
		'message_note_cancellation': 'Please note that your cancellation will go into effect as soon as it is received.<br>',
		'message_case_number': 'Case number: ',
		'enter_first_name': 'Please enter first name',
		'enter_last_name': 'Please enter last name',
		'enter_address': 'Please enter an address',
		'enter_city': 'Please enter city',
		'select_country': 'Please select a country',
		'select_state': 'Please select a state',
		'enter_province': 'Please enter province',
		'enter_zip_code': 'Please enter zip / postal code',
		'enter_phone': 'Please enter phone',
		'enter_email': 'Please enter email',
		'message_save_data': 'Saving data',
		'confirm_term_condition': 'To continue, please agree with terms and conditions'
	},
	'backup': {
		'confirm_create_backup': 'Are you sure you wish to create a backup?',
		'confirm_delete_backup': 'Are you sure you wish to delete this backup?',
		'confirm_restore_backup': 'Are you sure you wish to restore this backup?'
	},
	'categories': {
		'no_categories_selected_export': 'Please select at least one category to export',
		'no_categories_selected': 'Please select at least one category to delete',
		'confirm_remove': 'Are you sure you wish to remove this category?',
		'enter_title': 'Please enter title',
		'enter_category_id': 'Please enter category id'
	},
	'common': {
		'add': 'Add',
		'update': 'Update',
		'editing': 'Editing',
		'success': 'Success!',
		'updating': 'Updating',
		'confirm_logout': 'Are you sure you wish to logout?',
		'fix_error': 'Please fix errors'
	},
	'countries': {
		'no_countries_selected': 'Please select at least one country to delete',
		'confirm_remove': 'Are you sure you wish to remove this country?',
		'activate_label': 'Activate Countries',
		'deactivate_label': 'Deactivate Countries',
		'btn_activate_label': 'Activate',
		'btn_deactivate_label': 'Deactivate',
		'activate_text_label1': 'Activate only selected countries',
		'activate_text_label2': 'Activate all countries (all',
		'deactivate_text_label1': 'Deactivate only selected countries',
		'deactivate_text_label2': 'Deactivate all countries (all',
		'no_countries_selected_for_activation': 'Please select at least one country to activate / deactivate',
		'cannot_delete_excluded_countries': 'United States (or Canada) cannot be deleted because they are used in shipping. Please deactivate country instead.'
	},
	'credit_card_storage': {
		'username_required': 'Please enter username',
		'password_required': 'Please enter password',
		'password2_required': 'Please enter password confirmation',
		'password2_must_match': 'Password and password confirmation must match',
		'country_required': 'Please select a country',
		'province_required': 'Please enter a province',
		'city_required': 'Please enter a city',
		'company_name_required': 'Please enter an organization name',
		'company_unit_name_required': 'Please enter an organization unit name',
		'common_name_required': 'Please enter a common name',
		'email_required': 'Please enter email',
		'confirm_regenerate_access': 'Are you sure you wish to regenerate your secure access credentials?<br>You will lose access to any stored credit card information currently in the system!',
		'password_characters_required': 'Password must be at least 6 characters, and have at least 1 upper case character and 1 number or symbol',
		'message_certificate_generated': 'Success! New certificate has been generated.',
		'unexpected_error_occurred': 'An unexpected error occurred',
		'username_minimum_required': 'Username is too short. Minimum length is 8 characters.',
		'message_password_required': 'Password is required',
		'message_password_match': 'Password and password confirmation must match'
	},
	'currencies': {
		'no_currencies_selected': 'Please select at least one currency to delete',
		'no_currencies_selected_update': 'Please select at least one currency to update',
		'message_cannot_remove_currency': 'Oops! You cannot remove default currency.',
		'confirm_remove_currency': 'Are you sure you wish to remove this currency?'
	},
	'customers': {
		'confirm_remove_customer': 'Are you sure you wish to remove this customer',
		'lock_account': 'Lock this account for',
		'confirm_unlock_account': 'Unlock this account, are you sure?'
	},
	'doba': {
		'send_order': 'Are you sure you wish to send this order to Doba?',
		'delete_doba_data': 'This will delete Doba products. Please click confirm button to proceed.'
	},
	'drift_marketing': {
		'select_one_campaign': 'Please select at least one campaign',
		'confirm_remove_campaign': 'Are you sure you wish to remove this email campaign',
		'restore_template': 'Are you sure you want to restore the email template to original settings?',
	},
	'exactor': {
		'message_update_euc': 'Updating EUC codes',
		'enter_tax_name': 'Please enter Tax Name',
		'enter_tax_code': 'Please enter Tax Code',
		'confirm_remove_tax_code': 'Are you sure you wish to remove this tax code?'
	},
	'file_manager': {
		'confirm_replace_file': 'Are you sure you wish to replace this file?',
		'confirm_remove_file': 'Are you sure you wish to remove this file?'
	},
	'forms': {
		'confirm_delete_image': 'Do you really want to delete selected image?',
		'message_image_deleted': 'Image has been successfully deleted',
		'message_image_updated': 'Image has been successfully updated',
		'no_primary_image': 'Please select a Primary Image'
	},
	'forms_control': {
		'enter_text_length': 'Please enter text length',
		'message_text_length_greater': 'Text length must be greater than zero',
		'enter_field_option': 'Please enter field options',
		'enter_checked_value': 'Please enter checked value',
		'enter_unchecked_value': 'Please enter unchecked value',
		'select_custom_field_delete': 'Please select at least one custom field to delete',
		'confirm_delete_custom_field': 'Are you sure you wish to remove this custom field?'
	},
	'gift_certs': {
		'enter_first_name': 'Please enter first name',
		'enter_last_name': 'Please enter last name',
		'enter_email': 'Please enter email',
		'enter_voucher_number': 'Please enter voucher number',
		'enter_amount': 'Please enter amount',
		'enter_positive_balance': 'Please enter a positive balance',
		'select_gift_certificate': 'Please select at least one gift certificate to delete',
		'confirm_remove_gift_certificate': 'Are you sure you wish to remove this gift certificate?'
	},
	'endicia': {
		'pic_number_required': 'Please enter a PIC number',
		'addfunds_passphrase_required': 'Please enter your Endicia passphrase',
		'addfunds_amount_required': 'Please enter the amount of funds to add',
		'current_passphrase_required': 'Please enter your current passphrase',
		'new_passphrase_required': 'Please enter a new Endicia passphrase',
		'confirm_new_passphrase_required': 'Please enter a new confirm passphrase',
		'confirm_new_passphrase_not_match': 'Your new passphrase and confirm passphrase do not match'
	},
	'form_fields': {
		'name_required': 'Please enter field name',
		'caption_required': 'Please enter field caption'

	},
	'import': {
		'assign_required_fields': 'Please assign required fields',
		'uploading_data': 'Uploading data',
		'importing_data': 'Importing data',
		'invalid_type': 'Invalid file type'
	},
	'languages': {
		'set_language_default': 'Set this language as default?',
		'select_language': 'Please select at least one language',
		'cannot_remove_language': 'You cannot remove default language!',
		'confirm_remove_language': 'Are you sure you wish to remove this language?'
	},
	'login': {
		'message_incorrect_username_email': 'Provided username or e-mail is incorrect'
	},
	'manufacturers': {
		'enter_title': 'Please enter title',
		'enter_manufacturer_id': 'Please enter manufacturer id',
		'select_manufacturer_delete': 'Please select at least one manufacturer to delete',
		'confirm_delete_manufacturer': 'Are you sure you wish to remove this manufacturer?'
	},
	'order_forms': {
		'enter_name': 'Please enter form name',
		'enter_thank_you_page_url': 'Please enter thank you page url',
		'confirm_delete_order_form': 'Are you sure you wish to remove this order form?',
		'select_order_form_delete': 'Please select at least one order form to delete',
		'confirm_delete_product': 'Are you sure you wish to remove this product?',
		'product_added': 'Product has been added',
		'product_deleted': 'Product has been deleted',
		'no_results_found': 'No results found!',
		'form_url_copy_success': 'Form link copied to clipboard',
	},
	'orders': {
		'no_orders_selected_print': 'Please select at least one order to print',
		'no_orders_selected_export': 'Please select at least one order to export',
		'no_orders_selected_update': 'Please select at least one order to update',
		'no_orders_selected_delete': 'Please select at least one order to delete',
		'no_orders_selected_fulfill': 'Please select at least one order to fulfill',
		'invalid_number': 'Please enter a valid number',
		'return_items': 'Are you sure you wish to return all items to inventory?',
		'no_items_fulfill': 'No line items selected for fulfillment',
		'title_required': 'Please enter title',
		'note_required': 'Please enter note',
		'confirm_remove': 'Are you sure you wish to remove this order?',
		'message_cannot_retrieve_credit_card': 'Cannot retrieve credit card data',
		'order_bystate_empty': 'No order available.',
		'order_none_fulfilled': 'No orders require attention',
		'recently_none_abandon': 'No recent abandon orders',
		'status_options': {
			'new': 'New',
			'abandon': 'Abandon',
			'process': 'Process',
			'backorder': 'Backorder',
			'canceled': 'Canceled',
			'completed': 'Completed',
			'failed': 'Failed',
		},
		'fulfillment_options': {
			'completed': 'Completed',
			'partial': 'Partial',
			'pending': 'Pending'
		},
	},
	'to_fulfilled': {
		'order_id': 'Order ID',
		'fulfilled': 'Fulfilled',
		'amount': 'Amount',
	},
	'abandons': {
		'order_id': 'Order ID',
		'status': 'Status',
		'amount': 'Amount',
	},
	'by_state_province': {
		'orders': 'Orders',
		'amount': 'Total Amount',
		'shipping_address': 'Shipping Address',
	},
	'order_endicia': {
		'select_package_type': 'You must choose a package type',
		'select_delivery_service': 'You must choose a delivery service',
		'enter_package_weight': 'You must enter the package weight',
		'enter_package_value': 'You must declare the package\'s value',
		'enter_package_description': 'You must fill out the package description',
		'enter_package_length': 'You must enter the package length',
		'enter_package_width': 'You must enter the package width',
		'enter_package_height': 'You must enter the package height',
		'message_maximum_weight_standard': 'The maximum weight for a standard letter is 3.5 ounces',
		'message_maximum_length_package': 'The maximum length of a package allowed by the USPS is 130 inches',
		'message_maximum_width_package': 'The maximum width of a package allowed by the USPS is 130 inches',
		'message_maximum_height_package': 'The maximum height of a package allowed by the USPS is 130 inches',
		'message_certified_mail_first_class': 'Certified Mail can only be selected for First-Class or Priority Mail types',
		'message_certified_mail_letter': 'Critical Mail can only be used with Letter or Flat Package types',
		'message_cannot_select_return_receipt': 'You cannot select Return Receipt with International Mail',
		'message_standard_mail_cannot_weigh': 'Standard Mail cannot weigh more than 16 ounces',
		'message_restricted_delivery_selected': 'Restricted Delivery must be selected in conjunction with Insurance (amount must be over $200) or Certified Mail',
		'message_cannot_ship_international_mail': 'You cannot ship International Mail to the United States',
		'message_express_mail_weigh_more': 'Express Mail must weigh more than 13 ounces',
		'message_generate_shipping_label': 'Generating shipping label',
		'confirm_endicia_passphrase': 'You must confirm your Endicia Account Passphrase.',
		'message_add_amount_account': 'You must provide an amount to add to your Endicia account.',
		'message_unable_add_fund': 'We were unable to Add Funds to your account'
	},
	'order_notifications': {
		'select_order_notification': 'Please select at least one order notification',
		'confirm_delete_notification': 'Are you sure you wish to remove this order notification?'
	},
	'order_stamps': {
		'shipping_weight_required': 'Shipping weight is required',
		'message_shipping_rates': 'Getting shipping rates',
		'confirm_generate_label': 'Are you sure you wish to generate this label?<br><br>',
		'message_deduct_funds': 'Note: This will deduct funds from your Stamps.com account',
		'message_generate_label': 'Generating shipping label',
		'confirm_cancel_label': 'Are you sure you wish to cancel this label?',
		'cannot_select_option': 'You cannot select this option as it may not be used\nin combination with'
	},
	'order_vparcel': {
		'select_package_type': 'You must choose a package type',
		'select_delivery_service': 'You must choose a delivery service',
		'message_explanation_cannot_empty': 'Explanation cannot be empty!',
		'message_select_parcel': 'You need to choose at least one item for parcel',
		'message_exemption_required': 'Exemption is required for shipments to an international destination',
		'message_weight_more': 'The weight has to be  more than 0',
		'message_generating_shipping_label': 'Generating shipping label'
	},
	'payment_methods': {
		'title_required': 'Please enter a title',
		'confirm_remove_payment': 'Are you sure you wish to disable this payment?',
		'select_payment_method': 'Please choose payment method'
	},
	'fraud_methods': {
		'confirm_disable_method': 'Are you sure you wish to disable this fraud detection solution?',
	},
	'payment_profiles': {
		'confirm_remove_payment_profile': 'Are you sure you wish to remove this payment profile?'
	},
	'pikfly': {
		'zip_code_required': 'At least 1 zip code is required',
		'delivery_fee_required': 'Delivery fee is required',
		'enter_delivery_fee': 'Please enter a valid Delivery fee',
		'delivery_day_required': 'At least 1 delivery day is required',
		'message_delivery_time_same_day': 'Last delivery time must be after order by time for same day delivery',
		'confirm_remove_delivery_area': 'Are you sure you wish to remove this delivery area?'
	},
	'products': {
		'no_products_selected_export': 'Please select at least one product to export',
		'no_products_selected_delete': 'Please select at least one product to delete',
		'no_products_selected_edit': 'Please select at least one product to edit',
		'confirm_remove': 'Are you sure you wish to remove this product?',
		'confirm_remove_test_products': 'Are you sure you wish to delete the sample products?',
		'confirm_add_test_products': 'Do you wish to add the sample products for this theme?',
		'enter_shipping_price': 'Please enter valid shipping price',
		'no_primary_image' : 'Current product does not have a primary image.',
		'enter_title' : 'Please enter product name',
		'enter_product_id' :'Please enter product ID',
		'enter_product_price' : 'Please enter product price',
		'enter_product_category' : 'Please select product category',
		'minimum_exceed_maximum' : 'Minimum order must not exceed with maximum order',
		'available_feature_groups' : 'Available Groups',
		'assigned_features_groups' : 'Assigned Groups'
	},
	'products_features': {
		'enter_feature_name': 'Please enter Feature Name',
		'enter_feature_id': 'Please enter Feature ID',
		'confirm_remove_product_feature': 'Please confirm that you want to delete selected product feature. It will also delete this feature from assigned groups and products. <strong>Please note: this action is irreversible.</strong>',
		'no_product_feature_selected': 'Please select at least one product feature to delete'
	},
	'features_groups': {
		'confirm_remove': 'Are you sure you wish to un-assign the selected feature? Please note: this action will only un-assign selected feature from the current group, but feature still will exist globally',
		'enter_feature_group_name': 'Please enter Feature Group Name',
		'enter_feature_group_id': 'Please enter Feature Group ID',
		'confirm_remove_feature_group': 'Please confirm that you want to delete selected product features group. It will also delete this group from assigned products. <strong>Please note: this action is irreversible.</strong>',
		'no_feature_group_selected': 'Please select at least one feature group to delete',
		'choose_group_features': 'Please choose at least one feature',
		'available_features': 'Available Features',
		'assign_features': 'Assigned Features',
		'available_features_groups': 'Available Groups',
		'assign_features_groups': 'Assigned Groups'
	},
	'products_attributes': {
		'confirm_remove': 'Are you sure you wish to delete the selected attribute?',
		'choose_global_attribute': 'Please choose at least one global attribute',
		'select_delete_attribute': 'Please select at least one attribute to delete',
		'enter_attribute_name': 'Please enter attribute name',
		'enter_attribute_title': 'Please enter attribute title',
		'select_category': 'Please choose at least one category',
		'assing_products': 'Please assign at least one product',
		'enter_text_length': 'Please enter text length',
		'message_text_length_greater': 'Text length must be greater than zero',
		'enter_attribute_options': 'Please enter attribute options',
		'confirm_remove_global_attribute': 'Are you sure you wish to remove this global attribute?',
		'no_records': 'There are no records found',
	},
	'products_family': {
		'enter_title': 'Please enter title',
		'select_group': 'Please select at least one group',
		'confirm_remove_product': 'Are you sure you wish to remove this recommended products group?'
	},
	'products_features_groups': {
		'confirm_remove': 'Are you sure you wish to remove this feature group?'
	},
	'promo_codes': {
		'enter_name': 'Please enter Name',
		'enter_promo_code': 'Please enter Promo Code',
		'enter_discount': 'Please enter Discount',
		'discount_numeric_required': 'Discount must be numeric',
		'enter_minimum_order_subtotal': 'Please enter Minimum Order Subtotal',
		'minimum_order_subtotal_greater': 'Minimum Order Subtotal must be numeric and greater or equal to zero',
		'select_promo_code': 'Please select at least one promo code',
		'confirm_remove_promo_code': 'Are you sure you wish to remove this promo code?',
		'field_start_date': 'Please enter Start Date',
		'field_end_date': 'Please enter End Date',
	},
	'products_promotions': {
		'confirm_remove': 'Are you sure you wish to delete the selected product promotion?'
	},
	'products_quantity_discounts': {
		'confirm_remove_quantity_discount': 'Do you really want to delete this quantity discount?'
	},
	'products_reviews': {
		'select_product_review': 'Please select at least one product review',
		'confirm_remove_product_review': 'Are you sure you wish to remove this product review?'
	},
	'products_variants': {
		'message_add_attributes_variant': 'In order to create product variant please first add an attributes with variant flag turned on!',
		'confirm_delete_product_variant': 'Are you sure you wish to delete selected product variant?'
	},
	'products_dashboard': {
		'product_id': 'Product ID',
		'product_name': 'Product Name',
		'quantity': 'Quantity Sold',
		'quantity_on_hand': 'Quantity On Hand',
		'amount_sold': 'Amount Sold',
		'no_top_products': 'No Top Performing Products',
		'no_low_inventory_products': 'No Low Inventory Products',
		'no_non_performing_products': 'No Non Performing Products',
		'no_popular_categories': 'No Popular Categories',
		'no_popular_manufacturers': 'No Popular Manufacturers',
		'catalog': {
			'id': 'Category ID',
			'name': 'Category Name',
			'products_sold': 'Products Sold',
		},
		'manufacturers': {
			'id': 'Manufacturer ID',
			'name': 'Manufacturer Name',
			'products_sold': 'Products Sold',
		}
	},
	'stamps': {
		'amount_required': 'Please enter a purchase amount',
		'confirm_carrier_pickup': 'Are you sure that you want to request a carrier pickup?'
	},
	'states': {
		'no_states_selected': 'Please select at least one state / province to delete',
		'confirm_remove': 'Are you sure you wish to remove this state / province?'
	},
	'qr_codes': {
		'select_campaign': 'Please select at least one campaign',
		'confirm_remove_campaign': 'Are you sure you wish to remove this campaign?'
	},
	'quickbooks_quickstart': {
		'select_income_account': 'Please select an income account'
	},
	'recurring_profiles': {
		'message_updating_profile_status': 'Updating recurring profile status',
		'select_update_recurring_profile': 'Please select at least one recurring profile to update'
	},
	'search_engine_optimization': {
		'confirm_generate_htaccess': 'Are you sure you wish to generate the .htaccess file?',
		'confirm_generate_urls': 'Are you sure you wish to regenerate URLs?'
	},
	'security_profile': {
		'enter_password': 'Please enter password',
		'enter_password_confirmation': 'Please enter password confirmation',
		'message_passwords_not_equal': 'Password and password confirmation are not equal!',
		'select_security_question': 'Please choose security question',
		'enter_answer_security_question': 'Please enter answer for security question'
	},
	'shipping': {
		'enter_title': 'Please enter title',
		'confirm_remove_shipping_method': 'Are you sure you wish to remove this shipping method?',
		'enter_min_items': 'Please enter a valid min items',
		'enter_max_items': 'Please enter a valid max items',
		'enter_price': 'Please enter a valid price',
		'enter_min_subtotal': 'Please enter a valid min subtotal',
		'enter_max_subtotal': 'Please enter a valid max subtotal',
		'enter_min_weight': 'Please enter a valid min weight',
		'enter_max_weight': 'Please enter a valid max weight',
		'enter_usps_tools_user_id': 'Please enter your USPS tools user id',
		'enter_server_url_ups': 'Please enter a server URL for USPS rates',
		'enter_fedex_account_number': 'Please enter your primary FedEx account number',
		'enter_fedex_api_key': 'Please enter a FedEx API key',
		'enter_fedex_api_password': 'Please enter a FedEx API password',
		'enter_ups_xml_access_key': 'Please enter a UPS XML access key',
		'enter_ups_user_id': 'Please enter a UPS user id',
		'enter_ups_password': 'Please enter a UPS password',
		'enter_canada_merchant_id': 'Please enter a Canada Post merchant id',
		'enter_server_url_canada_post': 'Please enter a server URL for Canada Post rates',
		'enter_turn_around_time': 'Please enter a turn around time',
		'enter_shipping_vip_parcel_auth_token': 'Please enter a shipping VIP parcel auth token'
	},
	'shipping_address': {
		'enter_shipping_orig_name': 'Please enter a shipping name',
		'enter_street_address': 'Please enter a street address',
		'enter_city': 'Please enter a city',
		'enter_province': 'Please enter a province',
		'enter_zipcode': 'Please enter a zip / postal code'
	},
	'taxes': {
		'enter_title': 'Please enter Title',
		'enter_key_id': 'Please enter Key / ID',
		'enter_tax_rate': 'Please enter tax rate',
		'message_tax_rate_greater': 'Tax rate has to be > 0',
		'select_delete_tax_rate': 'Please select at least one tax rate to delete',
		'select_delete_tax_class': 'Please select at least one tax class to delete',
		'confirm_remove_tax_rate': 'Are you sure you wish to remove this tax rate?',
		'confirm_tax_class': 'Are you sure you wish to remove this tax class?'
	},
	'textpages': {
		'select_delete_page': 'Please select at least one page to delete',
		'confirm_remove_page': 'Are you sure you wish to remove this site page?',
		'publish_header_label': 'Show Pages',
		'unpublish_header_label': 'Hide Pages',
		'publish_option_selected': 'Show only selected pages',
		'publish_option_all': 'Show all pages',
		'unpublish_option_selected': 'Hide only selected pages',
		'unpublish_option_all': 'Hide all pages',
		'publish_btn_label': 'Show',
		'unpublish_btn_label': 'Hide',
		'select_to_publish': 'Select at least one page to show',
		'select_to_unpublish': 'Select at least one page to hide'
	},
	'snippets': {
		'select_delete_snippet': 'Please select at least one snippet to delete',
		'confirm_remove_snippet': 'Are you sure you wish to remove this site snippet?',
	},
	'thumbs_generator': {
		'confirm_potential_loss_data': 'WARNING: Potential for loss of data.  Ensure that you have a recent backup of your images. Do you wish to proceed?'
	},
	'testimonials': {
		'name_required': 'Please enter name',
		'testimonial_required': 'Please enter a Testimonial',
		'no_testimonials_selected': 'Please select at least one testimonial',
		'confirm_remove_testimonial': 'Are you sure you wish to remove this testimonial?',
		'no_notification_email': 'Please enter a Notification Email',
		'no_testimonials_per_page': 'Please enter a Testimonials Per Page'
	},
	'custom_form': {
		'enter_title': 'Please enter form title',
		'enter_form_id': 'Please enter form ID',
		'enter_redirect_url': 'Please enter correct redirect URL',
		'confirm_delete_custom_form': 'Are you sure wish to remove this custom form? All fields and data will be removed too.',
		'confirm_delete_custom_form_post': 'Are you sure wish to remove this form record?',
		'no_records_selected_delete': 'Please select at least one record to delete',
		'no_records_selected_export': 'Please select at least one record to export',
	},
	'custom_field': {
		'custom_field': 'Custom Field',
		'confirm_remove': 'Are you sure you wish to remove this custom field?'
	},
	'store_information': {
		'company_name': 'Company Name',
		'street_address': 'Street Address',
		'street_address_two': 'Street Address Line 2',
		'city': 'City',
		'state': 'State',
		'province': 'Province',
		'country': 'Country',
		'zip_postal': 'Zip / Postal Code',
		'phone': 'Phone',
		'fax': 'Fax',
	},
	'widget': {
		'name_required': 'Name is required',
		'description_required': 'Description is required',
	},
	'quick_start_guide': {
		'next': 'Next Step',
		'back': 'Go back',
		'start': 'Launch the Quick Guide',
		'intro': 'Welcome to PinnacleCart Quick Start Guide',
		'title': '<span class="text-green">Quick Start</span> Guide',
		'step_one': {
			'general_information': 'General Information',
			'contact_information': 'Contact Information',
			'industry_specifics': 'Industry Specifics',
			'logo': 'Logo',
			'company_name': 'Enter company name here',
			'street_name': 'Enter Street Address here',
			'suite_name': 'Enter Suite/Apt',
			'country': 'Country',
			'province': 'State / Province',
			'state': 'State / Province',
			'city': 'City',
			'zip': 'Zip / Postal Code',
			'zip_tooltip': 'This is required if your company is using shipping rates from a major carrier like USPS, FED-EX, UPS, or CANADA POST. Typically used as the location where packages are shipped from.',
			'phone': 'Enter Phone Number',
			'fax': 'Enter Fax Number',
			'email': 'Enter Email',
			'industry': 'Industry',
			'revenue': 'Revenue',
			'employees': 'Employees',
			'upload_logo': 'Upload Logo',
			'upload_company_logo': 'Upload Company Logo (PNG, JPEG or GIF recommended)',
			'upload_company_logo_tooltip': 'This will be used as the primary visual representation of your brand throughout the store. We recommend a file size of less than 500KB. Upload a logo at a similar height to which you\'d like it displayed.',
		},
		'step_two': {
			'social_media_accounts': 'Social Media Accounts',
			'sharing_settings': 'Sharing Settings',
			'fb_label': 'http://facebook.com/yourstore',
			'twitter_label': 'http://twitter.com/yourstore',
			'instagram_label': 'http://instagram.com/yourstore',
			'pinterest_label': 'http://pinterest.com/yourstore',
			'youtube_label': 'http://www.youtube.com/yourstore',
			'snapchat_label': 'http://snapchat.com/yourstore',
			'googleplus_label': 'http://plug.google.com/yourstore',
			'blogger_label': 'http://www.blogger.com/yourstore',
			'enable_facebook_like_button': 'Enable Facebook Like button',
			'enable_facebook_comments': 'Enable Facebook comments',
			'facebook_application_id': 'Facebook Application ID',
			'enable_tweet_button': 'Enable Tweet button',
			'enable_google_plus_button': 'Enable Google Plus button',
			'enable_pinterest_button': 'Enable Pinterest button',
			'enable_youtube_button': 'Enable Youtube button',
			'enable_blogger_button': 'Enable Blogger button',
			'enable_snapchat_button': 'Enable Snapchat button',
			'enable_instagram_button': 'Enable Instagram button',
			'block_all_pinterest_sharing': 'Block all Pinterest sharing',
			'enable_delicious_button': 'Enable Delicious button ',
			'sign_up': 'Sign Up',
		},
		'step_three': {
			'site_information': 'Site Information',
			'seo_settings': 'SEO Settings',
			'site_title': 'Site Title (Recommended length - less than 60 characters)',
			'site_title_tooltip': 'Google typically displays the first 50-60 characters of a title tag. The title of web site is meant to be an accurate and concise description of what can be found in the site. It is critical to both the user experience and search engine optimization.',
			'meta_description': 'Meta Description (Recommended length - less than 160 characters)',
			'meta_description_tooltip': 'Site descriptions (sometimes called "Meta Descriptions") are commonly used on search engine result pages (SERPs) to display preview snippets for a given page. Think of the meta description as advertising copy. It draws readers to a website from the SERP and thus, is an extremely important part of search marketing.',
			'enable_friend_urls': 'Enable search engine friendly URLs',
			'use_lower_case_urls': 'Use lower case URLs',
			'enable_auto_metadata_generation': 'Enable auto metadata generation',
			'force_secure_urls': 'Force secure URLs',
			'url_preference': 'URL Preference',
			'sitemap_url': 'Sitemap URL',
			'enable_seo_friendly_urls': 'Enable search engine friendly URLs',
			'enable_seo_friendly_urls_tooltip': 'For optimal positioning in all major search sites. - Recommended',
			'enable_flat_urls_generation': 'Enable flat URLs generation?',
			'use_lowercase_letters_in_urls': 'Use lowercase letters in URLs',
			'use_lowercase_letters_in_urls_tooltip': 'For optimal positioning in all major search sites. - Recommended',
			'enable_meta_title_and_description_auto_generation': 'Enable meta title & description auto generation',
			'enable_meta_title_and_description_auto_generation_tooltip': 'The system will attempt to generate a title and description for all of your products based on the product title and product description you enter. You can overwrite the auto-generated information by going to the Search Engine Settings on the product page and entering your won.',
			'seo_www_preference': 'URL preference',
			'order_form_url_template_note': 'Template Variables:<br>%OrderFormName%, %OrderFormDbId% - at least one variable required',
			'copy_to_clipboard': 'Copy To Clipboard',
			'enable_flat_urls_generation_tooltip': 'This feature is not available under your license agreement. Please contact your provider to upgrade your application.',
		},
		'step_four': {
			'select_shipping_carriers': 'Select Your Shipping Carriers',
			'shipping_carriers_note': 'This screen is intended to get you started with shipping and doesn\'t provide you will all your shipping options. For more options, go to "Settings" an "Shipping" after you have completed this guide.',
			'select_payment_gateway': 'Select Your Payment Gateway',
			'payment_gateway_note': 'To simplify your set-up, we have set "Custom Payment" as your default payment method. With this selected, no payment information will be collected and you\'ll be able to easily complete an order. To collect payments, you will need to select a payment gateway. If you do not have a payment gateway, Paypal is a very simple and easy way to get stated. Click the "Connect with Paypal" to begin.',
			'connect_with_paypal': 'Connect with Paypal',
			'taxes': 'Taxes',
			'setup_taxes': 'Set up your taxes',
			'enter_rate_name': 'Enter rate name',
			'give_tax_rate_name': 'Give this tax rate a name (this will not appear to your customers)',
			'tax_class': 'Tax class',
			'country': 'Country',
			'state': 'State',
			'state_province': 'State / Province',
			'tax_rate_percentage': 'Tax Rate, %',
			'title_payment_method': 'Title of this Payment Method',
			'text_checkout_page': 'Enter text to appear on the checkout page',
			'thank_you_instructions': 'Enter text you\'d like to appear when a customer completes an order',
		},
	},
	'category': {
		'confirm_delete_product_feature_group' : 'Do you really want to remove selected products features group from this category?',
		'available_feature_groups' : 'Available Groups',
		'assigned_features_groups' : 'Assigned Groups',
	}
};

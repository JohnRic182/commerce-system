(function ( $ ) {
	$.fn.reportRanges = function (options) {

		var settings = $.extend({
			hasWeek: true,
			hasMonth: true,
			hasQuarter: true,
			hasYear: true,
			hasCustom: true,
			currentRangeType: 'week',
			dateFrom: null,
			dateTo: null,
			showDateAsText: true,
			callback: null,
		}, options);

		/**
		 * Class methods
		 */
		this.getRangeStartDate = function(rangeType) {
			var baseDate = moment();
			switch (rangeType) {
				case 'week': baseDate.startOf('week'); break;
				case 'month': baseDate.startOf('month'); break;
				case 'quarter': baseDate.startOf('quarter'); break;
				case 'year': baseDate.startOf('year'); break;
			}

			return baseDate.format('Y-MM-DD');
		};

		this.getRangeEndDate = function(rangeType) {
			var baseDate = moment();
			switch (rangeType) {
				case 'week': baseDate.endOf('week'); break;
				case 'month': baseDate.endOf('month'); break;
				case 'quarter': baseDate.endOf('quarter'); break;
				case 'year': baseDate.endOf('year'); break;
			}

			return baseDate.format('Y-MM-DD');
		};

		this.getRangeAsText = function () {
			return moment(this.dateFrom).format('MMM Do YYYY') + ' - ' + moment(this.dateTo).format('MMM Do YYYY');
		};

		/**
		 * Class variables
		 */

		this.dateFrom = settings.dateFrom == null ? this.getRangeStartDate(settings.currentRangeType) : moment(settings.dateFrom);
		this.dateTo = settings.dateTo == null ? this.getRangeEndDate(settings.currentRangeType) : moment(settings.dateTo);
		this.rangeType = settings.currentRangeType;

		/**
		 * Build HTML
		 */
		var html = '<div class="col-xs-12">';

		if (settings.hasWeek) html += '<a href="#week" data-value="week" class="btn' + (settings.currentRangeType == 'week' ? ' btn-primary' : '') + ' btn-range">Week</a> ';
		if (settings.hasMonth) html += '<a href="#month" data-value="month" class="btn' + (settings.currentRangeType == 'month' ? ' btn-primary' : '') + ' btn-range">Month</a> ';
		if (settings.hasQuarter) html += '<a href="#quarter" data-value="quarter" class="btn' + (settings.currentRangeType == 'quarter' ? ' btn-primary' : '') + ' btn-range">Quarter</a> ';
		if (settings.hasYear) html += '<a href="#year" data-value="year" class="btn' + (settings.currentRangeType == 'year' ? ' btn-primary' : '') + ' btn-range">Year</a> ';
		if (settings.hasCustom) {
			html = html +
				'<a href="#custom" data-value="custom" class="btn' + (settings.currentRangeType == 'custom' ? ' btn-primary' : '') + ' btn-range btn-custom-range">Custom<b class="caret"></b></a>';
		}

		if (settings.showDateAsText) {
			html += '<span class="pull-right date-range-text">' + this.getRangeAsText() + '</span>';
		}

		html += '</div>';

		$(this).html(html);

		/**
		 * Handle selection
		 */

		var obj = this;

		if (settings.hasCustom) {
			$(this).find('.btn-custom-range').daterangepicker({
				startDate: moment(this.dateFrom),
				endDate: moment(this.dateTo),
				linkedCalendars: false,
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}, function(start, end){
				obj.dateFrom = moment(start).format('Y-MM-DD');
				obj.dateTo = moment(end).format('Y-MM-DD');

				if (settings.showDateAsText) {
					$(obj).find('.date-range-text').html(obj.getRangeAsText());
				}

				if (settings.callback != null) settings.callback(obj.rangeType, obj.dateFrom, obj.dateTo);

			});
		}

		$(this).find('.btn-range').click(function(e){
			e.stopPropagation();

			obj.rangeType = $(this).attr('data-value');

			$(obj).find('.btn-range').removeClass('btn-primary');
			$(this).addClass('btn-primary');

			if (obj.rangeType == 'custom') {

			} else {
				obj.dateFrom = obj.getRangeStartDate(obj.rangeType);
				obj.dateTo = obj.getRangeEndDate(obj.rangeType);

				if (settings.showDateAsText) {
					$(obj).find('.date-range-text').html(obj.getRangeAsText());
				}

				if (settings.callback != null) settings.callback(obj.rangeType, obj.dateFrom, obj.dateTo);
			}

			return false;
		});

		return this;
	};
}( jQuery ));
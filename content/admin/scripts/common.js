$(document).ready(function () {
	var selectingAll = false;
	$('#selectAll').change(function () {
		selectingAll = true;
		var val = $(this).is(':checked');
		if (val) {
			$('input[name^=row]').prop('checked', 'checked');
		} else {
			$('input[name^=row]').removeProp('checked');
		}
		selectingAll = false;
	});

	$('input[name^=row]').change(function () {
		if (selectingAll) return;

		$('#selectAll').prop('checked', $('input[name^=row]').not(':checked').length == 0);
	});

	var alerts = $('.alert.fade');
	setTimeout(function () {
		alerts.fadeIn('slow', function () {
			$(this).addClass('in');
		});
	}, 250);

	if (alerts.length > 0) {
		setTimeout(function () {
			$(alerts).alert('close');
		}, 6000);
	}

	$('.modal').on('hide.bs.modal', function () {
		$(this).find('.alerts').remove();
		var form = $(this).find('form');
		if (form.length > 0) {
			form.get(0).reset();
		}
	})
		.on('hidden.bs.modal', function () {
			var modals = $('.modal:visible');
			var backdrop = $('.modal-backdrop');
			if (modals.length > 0 && backdrop.length == 0) {
				var backdrop = $('<div></div>').addClass('modal-backdrop').addClass('fade').addClass('in');

				$('body').append(backdrop);
			} else if (modals.length == 0 && backdrop.length > 0) {
				backdrop.remove();
			}
		})
	;

	$('.logout').click(function () {
		AdminForm.confirm(trans.common.confirm_logout, function () {
			logout();
		});

		return false;
	});

	$('.tree li:has(ul)').addClass('parent_li');
	$('.tree li.parent_li > span').on('click', function (e) {
		var children = $(this).parent('li.parent_li').find(' > ul > li');
		if (children.is(":visible")) {
			children.hide('fast');
			$(this).find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
		} else {
			children.show('fast');
			$(this).find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
		}
		e.stopPropagation();
	});

	$('#field-meta_title').keyup(function() {
		limitMetaCounter('field-meta_title', 'meta-title-limit-info', 69);
	})
	.change(function() {
		limitMetaCounter('field-meta_title', 'meta-title-limit-info', 69);
	});

	$('#field-meta_description').keyup(function() {
		limitMetaCounter('field-meta_description', 'meta-description-limit-info', 156);
	})
	.change(function() {
		limitMetaCounter('field-meta_description', 'meta-description-limit-info', 156);
	});
	
	//handle dynamic address book country, state and province onchange event
	initDynamicAddressBookFieldEvent();

	var spinnerShowFlag = false;
	$('#quick-start-guide-wizard').on('show.bs.modal', function(e) {
		if (!spinnerShowFlag) {
			AdminForm.displaySpinner('Please wait while we are fetching your data');
		}
	});

	$('#quick-start-guide-wizard').on('loaded.bs.modal', function(e) {
		$('#quick-start-guide-wizard').removeClass('hidden');
		AdminForm.hideSpinner();
		spinnerShowFlag = true;
	});

	$('#quick-start-guide-wizard').on('hidden.bs.modal', function () {
		if (typeof qsgAutostart != 'undefined' && qsgAutostart == true) {
			window.location.reload();
		}
	});
});

function logout() {
	if (typeof(intuitConnectionActive) !== 'undefined' && intuitConnectionActive) {
		intuit.ipp.anywhere.logout(function () {
			$('#frmLogout').submit();
		});
	} else {
		$('#frmLogout').submit();
	}
}

var AdminForm = (function () {
	var displayErrors, removeErrors, displayAlert, confirm, clearAlerts, displayModalErrors, displayValidationErrors, displayTableValidationErrors, displayModalAlert, displaySpinner, hideSpinner, sendRequest, imagePreview, getContent, avatarInitials, copyToClipboard, dropDownOptions;

	displayErrors = function (errors) {
		if (errors.length > 0) {
			$('.alerts').html('');
			var $ul = $('<ul></ul>');
			for (var i = 0; i < errors.length; i++) {
				//AdminForm.displayAlert(errors[i].message, 'danger');
				var $li = $('<li></li>').html(errors[i].message);
				$ul.append($li);
				if ($(errors[i].field).parent().hasClass('input-container')) {
					$(errors[i].field).parent().addClass('has-error');
				} else {
					$(errors[i].field).closest('.form-group').addClass('has-error');
				}
			}
			AdminForm.displayAlert(trans.common.fix_error + $ul.get(0).outerHTML, 'danger');
		}
	};

	removeErrors = function () {
		$('.form-group').removeClass('has-error');
		$('.input-container.has-error').removeClass('has-error');
	};

	clearAlerts = function () {
		$('.alerts').html('');
	};

	displayAlert = function (message, type) {
		if (type == null || type == undefined) type = 'success';
		$('.alerts').html($('<div class="alert alert-' + type + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>'));

		setTimeout(function () {
			$('.alert').not('.alert-danger').alert('close');
		}, 6000);
	};

	displayModalErrors = function (modal, errors) {
		if (errors.length > 0) {
			$('.alerts').html('');
			var $ul = $('<ul></ul>');
			for (var i = 0; i < errors.length; i++) {
				var $li = $('<li></li>').html(errors[i].message);
				$ul.append($li);
				modal.find(errors[i].field).closest('.form-group').addClass('has-error');
			}
			AdminForm.displayModalAlert(modal, trans.common.fix_error + $ul.get(0).outerHTML, 'danger');
		}
	};

	displayValidationErrors = function (errors) {
		var formFieldsWithErrors = false;
		if (errors.length > 0) {
			$.each(errors, function (key, error) {
				$(error.field).parent().find('.help-block.error').remove();
				$(error.field).parent().addClass('has-error has-feedback');
				$(error.field).parent().find('.warning-sign').removeClass('hidden');
				$(error.field).parent().append('<p class="help-block error"><small>' + error.message + '</small></p>');
			});

			formFieldsWithErrors = $('#quick-start-guide-form').find('.form-group.has-error .error');
		}

		return formFieldsWithErrors;
	};

	displayTableValidationErrors = function (errors) {
		if (errors.length > 0) {
			$.each(errors, function (key, error) {
				error.field.closest('td').find('.error').remove();
				error.field.closest('td').find('.form-group').addClass('has-error');
				error.field.closest('td').append('<p class="text-danger error"> <small>'+error.message+'</small> </p>');
			});
		}
	}

	displayModalAlert = function (modal, message, type) {
		if ($(modal).find('.alerts').length == 0) {
			$(modal).find('.modal-body').prepend($('<div class="alerts"></div>'));
		}

		$(modal).find('.alerts').html($('<div class="alert alert-' + type + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>'));
	};

	confirm = function (message, callback) {
		var confirmModal = $('#modal-confirm');

		confirmModal.find('.modal-body').html('<div class="col-xs-12">' + message + '</div>');

		confirmModal.find('.btn').unbind('click').click(function () {
			confirmModal.modal('hide');
		});

		confirmModal.find('.btn-primary').unbind('click').click(function () {
			confirmModal.modal('hide');
			callback();
		});

		confirmModal.on('shown.bs.modal', function () {
			$(this).find('.btn-primary').focus();
		});

		confirmModal.modal('show');
	};

	imagePreview = function (imageUrl) {
		var imagePreviewModal = $('#modal-image-preview');
		imagePreviewModal.find('.modal-body img').attr('src', imageUrl)
		imagePreviewModal.find('.btn-primary').unbind('click').click(function () {
			imagePreviewModal.modal('hide');
		});

		imagePreviewModal.modal('show');
	};

	displaySpinner = function (message) {
		if (typeof message === 'undefined' || message === null || $.trim(message) === '') {
			message = 'Updating data';
		}

		var theModal = $('#modal-spinner');
		theModal.find('.spinner-message').html(message + "...");

		theModal.modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		});
	};

	hideSpinner = function () {
		var theModal = $('#modal-spinner');
		theModal.modal('hide');
		$(theModal).on('hidden.bs.modal', function () {
			if ($('.modal-backdrop').length) {
				$('body').addClass('modal-open');
			}
		});
	};

	sendRequest = function (url, data, message, onSuccessCallback, onErrorCallback, method, dataType) {
		//TODO: Fix this method signature, see jquery
		try {
			if (typeof message === 'undefined') message = null;
			if (message != null) displaySpinner(message);

			if (url.indexOf('?') < 0) {
				url += '?';
			}

			$.ajax({
				url: url + '&__ajax=1',
				method: method === undefined || method === null ? 'POST' : method,
				dataType: dataType === undefined || dataType == null ? 'json' : dataType,
				data: data,
				success: function (response, status) {
					AdminForm.hideSpinner();
					if (typeof response.status !== undefined && response.status == 0 && typeof response.auth !== undefined && response.auth == 0) {
						AutoLogout.displayLogin();
					} else {
						if (typeof onSuccessCallback !== 'undefined' && onSuccessCallback != null && onSuccessCallback) {
							onSuccessCallback(response, status);
						}
					}
				},
				error: function (jqXHR, status) {
					if (message != null) hideSpinner();
					if (typeof onErrorCallback !== 'undefined' && onErrorCallback != null && onErrorCallback) {
						onErrorCallback(jqXHR, status);
					}
				}
			});
		}
		catch (err) {
			if (message != null) hideSpinner();
			alert(err);
		}
	};

	getContent = function (url, onSuccessCallback, message, dataType, minTime) {
		//TODO: Fix this method signature, see jquery
		try {
			displaySpinner(message);
			if (minTime === undefined) minTime = 300;

			var startTime = new Date().getTime();

			$.ajax({
				url: url + '&__ajax=1',
				method: 'GET',
				dataType: dataType === undefined || dataType == null ? 'json' : dataType,
				success: function (response, status) {
					var handleSuccess = function () {
						AdminForm.hideSpinner();
						if (typeof onSuccessCallback !== 'undefined' && onSuccessCallback != null && onSuccessCallback) {
							onSuccessCallback(response, status);
						}
					};
					var diff = minTime - (new Date().getTime() - startTime);
					if (diff > 0) {
						setTimeout(function () {
							handleSuccess();
						}, diff);
					} else {
						handleSuccess();
					}
				},
				error: function (jqXHR, status) {
					hideSpinner();
					if (typeof onErrorCallback !== 'undefined' && onErrorCallback != null && onErrorCallback) {
						onErrorCallback(jqXHR, status);
					}
				}
			});
		}
		catch (err) {
			hideSpinner();
			alert(err);
		}
	};

	avatarInitials = function (name, canvasId, font, everyWord) {
		var colors = [
			'#719A31', '#1628AA', '#94711D', '#B4A67F', '#2C2A1A', '#A7BF0B', '#5A3877', '#42365F', '#5006CA', '#18CBE8',
			'#3F3E03', '#277634', '#4A1A00', '#FE079F', '#256437', '#AEDB09', '#000000', '#70A5E7', '#88DB77', '#CF17BA',
			'#6B88EF', '#9D69AA', '#76B3D7', '#1E38F2', '#6CB8E2', '#F92947'
		];

		var nameSplit = name.split(' ');
		var initials = '';
		if (everyWord == true)
		{
			$.each(nameSplit, function(index, value) {
				initials+= value.charAt(0).toUpperCase();
			});
		}
		else
		{
			initials = nameSplit[0].charAt(0).toUpperCase();
		}

		var charIndex = initials.charCodeAt(0) - 65;
		var colorIndex = charIndex % 26;

		var canvas = document.getElementById(canvasId);
		var context = canvas.getContext('2d');

		var canvasWidth = $(canvas).attr('width');
		var canvasHeight = $(canvas).attr('height');
		var canvasCssWidth = canvasWidth;
		var canvasCssHeight = canvasHeight;

		if (window.devicePixelRatio)
		{
			$(canvas).attr('width', canvasWidth * window.devicePixelRatio);
			$(canvas).attr('height', canvasHeight * window.devicePixelRatio);
			$(canvas).css('width', canvasCssWidth);
			$(canvas).css('height', canvasCssHeight);
			context.scale(window.devicePixelRatio, window.devicePixelRatio);
		}

		context.fillStyle = colors[colorIndex];
		context.fillRect (0, 0, canvas.width, canvas.height);
		context.font = font;
		context.textAlign = 'center';
		context.fillStyle = '#FFFFFF';
		context.fillText(initials, canvasCssWidth / 2, canvasCssHeight / 1.5);
	};

	copyToClipboard = function(elem) {
		// create hidden text element, if it doesn't already exist
		var targetId = "_hiddenCopyText_";
		var origSelectionStart, origSelectionEnd;

		var target = document.getElementById(targetId);
		if (target == null) {
			// can just use the original source element for the selection and copy
			target = document.createElement("textarea");
			target.style.position = "absolute";
			target.style.left = "-9999px";
			target.style.top = "0";
			target.id = targetId;
			document.body.appendChild(target);
		}
		target.textContent = elem.value;

		// select the content
		var currentFocus = document.activeElement;
		target.focus();
		target.setSelectionRange(0, target.value.length);

		// copy the selection
		var succeed;
		try {
			succeed = document.execCommand("copy");
		} catch(e) {
			succeed = false;
		}
		// restore original focus
		if (currentFocus && typeof currentFocus.focus === "function") {
			currentFocus.focus();
		}

		// clear temporary content
		target.textContent = "";

		return succeed;
	};

	dropDownOptions = function(elemName, elemId, options, selected) {
		var dropDown = $("<select></select>").attr("id", elemId).attr("name", elemName).attr("class", "form-control");

		$.each(options, function (i, el) {
			var currSelected = (el == selected ? 'selected="selected"' : '');
			dropDown.append("<option " + currSelected + ">" + el + "</option>");
		});
		return dropDown;
	};

	return {
		displayErrors: displayErrors,
		removeErrors: removeErrors,
		displayAlert: displayAlert,
		displayModalErrors: displayModalErrors,
		displayValidationErrors: displayValidationErrors,
		displayTableValidationErrors: displayTableValidationErrors,
		displayModalAlert: displayModalAlert,
		confirm: confirm,
		imagePreview: imagePreview,
		displaySpinner: displaySpinner,
		hideSpinner: hideSpinner,
		sendRequest: sendRequest,
		getContent: getContent,
		clearAlerts: clearAlerts,
		avatarInitials: avatarInitials,
		copyToClipboard: copyToClipboard,
		dropDownOptions: dropDownOptions
	}
}());

/**
 * AutoLogout functionality, will display a message to the admin
 * when they have 1 minute left in their session.
 *
 * Once their session has expired, they will be displayed an inline login form
 */
var AutoLogout = (function () {
	var logoutTimer = null, init, reset, displayCountdown, displayLogin, checkForTimeout, handleAuthRefresh;

	var logoutCountdown = 0, logoutCountdownTimer, authRefreshTimer;

	var $;

	init = function (jquery, document) {
		$ = jquery;

		$(document).bind('keypress scroll click dblclick', function () {
			reset();
		});

		authRefreshTimer = setTimeout(function () {
			handleAuthRefresh();
		}, 5 * 60 * 1000);

		reset();
	};

	handleAuthRefresh = function () {
		$.ajax({
			url: 'admin.php?chart=1',
			method: 'GET',
			success: function (data) {
				authRefreshTimer = setTimeout(function () {
					handleAuthRefresh();
				}, 5 * 60 * 1000);
			}
		});
	};

	reset = function () {
		if ($('#modal-login:visible').length > 0 || $('#modal-auto-logout:visible').length > 0) {
			return;
		}

		if (logoutTimer !== null) {
			clearInterval(logoutTimer);
		}

		if (typeof(adminAutoLogoutTimeInMinutes) !== 'undefined') {
			logoutTimer = setTimeout(function () {
				displayCountdown();
			}, (adminAutoLogoutTimeInMinutes - 1) * 60 * 1000);
		}
	};

	displayCountdown = function () {
		logoutCountdown = 60;

		var autoLogoutModal = $('#modal-auto-logout');
		autoLogoutModal.find('.seconds').html(logoutCountdown);

		if (logoutCountdownTimer !== null) {
			clearInterval(logoutCountdownTimer);
		}

		logoutCountdownTimer = setInterval(function () {
			checkForTimeout();
		}, 1000);

		autoLogoutModal.modal({
			backdrop: false,
			keyboard: false
		});
		$('body').append($('<div class="modal-backdrop fade in auto-logout-form-backdrop"></div>'));

		autoLogoutModal.find('.logout-button').click(function () {
			logout();
		});
		autoLogoutModal.find('.cancel-button').click(function () {
			reset();
			if (logoutCountdownTimer !== null) {
				clearInterval(logoutCountdownTimer);
				logoutCountdownTimer = null;
			}

			$.ajax({
				url: 'admin.php?chart=1',
				method: 'GET',
				success: function (data) {
				}
			});

			$('.modal-backdrop').remove();
			$('#modal-auto-logout').modal('hide');
		});
		var mceWindow = $("div.mce-window");
		if (mceWindow.length > 0 && mceWindow.is(':visible')) {
			$("#mce-modal-block").zIndex(1049);
			mceWindow.zIndex(1050);
		}
	};

	displayLogin = function () {
		$('#modal-auto-logout').modal('hide');
		$('.auto-logout-form-backdrop').remove();

		$.ajax({
			url: 'login.php',
			data: 'action=logout'
		});

		var loginModal = $('#modal-login');
		loginModal.modal({
			backdrop: false,
			keyboard: false
		});
		$('body').append($('<div class="modal-backdrop fade in login-form-backdrop"></div>'));

		$('#field-login-username').focus();

		$('form[name=form-login]').submit(function () {
			var passwordField = $('#field-login-password');
			var usernameField = $('#field-login-username');
			if ($.trim(passwordField.val()) == '' || $.trim(usernameField.val()) == '') {
				passwordField.closest('.form-group').addClass('has-error');
				usernameField.closest('.form-group').addClass('has-error');

				return false;
			}

			$.ajax({
				url: 'login.php',
				data: $('form[name=form-login]').serialize(),
				method: 'POST',
				success: function (data) {
					if (!data || data.blocked) {
						document.location = 'login.php';
					} else if (data.status) {
						$('.login-form-backdrop').remove();
						$('#modal-login').modal('hide');
						$('#field-login-password').val('');
						$('#field-login-username').val('');
					} else {
						var errors = [];
						errors.push({field: '#field-login-password', message: data.error_message});
						AdminForm.displayModalErrors(loginModal, errors);
					}
				}
			});

			return false;
		});
	};

	checkForTimeout = function () {
		logoutCountdown--;

		if (logoutCountdown <= 0) {
			if (logoutCountdownTimer !== null) {
				clearInterval(logoutCountdownTimer);
				logoutCountdownTimer = null;
			}
			if (logoutTimer !== null) {
				clearInterval(logoutTimer);
				logoutTimer = null;
			}

			if (($("div.mce-window").length > 0) && $("div.mce-window").is(':visible')) {
				if ($("button.mce-close").length > 0) {
					$("button.mce-close").trigger('click');
				}
			}

			displayLogin();
		} else {
			$('#modal-auto-logout').find('.seconds').html(logoutCountdown);
		}
	};

	//return this;
	return {
		init: init,
		displayLogin: displayLogin
	};
}());

function getAdminPrice(amount) {
	var v = Math.abs(amount) * adminCurrency.exchange_rate;
	v = v.toFixed(adminCurrency.decimal_places) +  '';
	v = v.replace('.', adminCurrency.symbol_decimal);
	v = v.replace(/(\d)(?=(\d{3})+\.)/g, "$1" + adminCurrency.symbol_separating);
	return (amount < 0 ? '-' : '') + adminCurrency.symbol_left + v + adminCurrency.symbol_right;
}

function getAdminNumber(amount) {
	var v = Math.abs(amount) + '';
	var v = v.replace(/(\d)(?=(\d{3})+\.)/g, "$1" + adminCurrency.symbol_separating);
	return (amount < 0 ? '-' : '') + v;
}

function get_html_translation_table(table, quote_style) {
	var entities = {},
		hash_map = {},
		decimal;
	var constMappingTable = {},
		constMappingQuoteStyle = {};
	var useTable = {},
		useQuoteStyle = {};

	// Translate arguments
	constMappingTable[0] = 'HTML_SPECIALCHARS';
	constMappingTable[1] = 'HTML_ENTITIES';
	constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
	constMappingQuoteStyle[2] = 'ENT_COMPAT';
	constMappingQuoteStyle[3] = 'ENT_QUOTES';

	useTable = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
	useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() :
		'ENT_COMPAT';

	if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
		throw new Error('Table: ' + useTable + ' not supported');
		// return false;
	}

	entities['38'] = '&amp;';
	if (useTable === 'HTML_ENTITIES') {
		entities['160'] = '&nbsp;';
		entities['161'] = '&iexcl;';
		entities['162'] = '&cent;';
		entities['163'] = '&pound;';
		entities['164'] = '&curren;';
		entities['165'] = '&yen;';
		entities['166'] = '&brvbar;';
		entities['167'] = '&sect;';
		entities['168'] = '&uml;';
		entities['169'] = '&copy;';
		entities['170'] = '&ordf;';
		entities['171'] = '&laquo;';
		entities['172'] = '&not;';
		entities['173'] = '&shy;';
		entities['174'] = '&reg;';
		entities['175'] = '&macr;';
		entities['176'] = '&deg;';
		entities['177'] = '&plusmn;';
		entities['178'] = '&sup2;';
		entities['179'] = '&sup3;';
		entities['180'] = '&acute;';
		entities['181'] = '&micro;';
		entities['182'] = '&para;';
		entities['183'] = '&middot;';
		entities['184'] = '&cedil;';
		entities['185'] = '&sup1;';
		entities['186'] = '&ordm;';
		entities['187'] = '&raquo;';
		entities['188'] = '&frac14;';
		entities['189'] = '&frac12;';
		entities['190'] = '&frac34;';
		entities['191'] = '&iquest;';
		entities['192'] = '&Agrave;';
		entities['193'] = '&Aacute;';
		entities['194'] = '&Acirc;';
		entities['195'] = '&Atilde;';
		entities['196'] = '&Auml;';
		entities['197'] = '&Aring;';
		entities['198'] = '&AElig;';
		entities['199'] = '&Ccedil;';
		entities['200'] = '&Egrave;';
		entities['201'] = '&Eacute;';
		entities['202'] = '&Ecirc;';
		entities['203'] = '&Euml;';
		entities['204'] = '&Igrave;';
		entities['205'] = '&Iacute;';
		entities['206'] = '&Icirc;';
		entities['207'] = '&Iuml;';
		entities['208'] = '&ETH;';
		entities['209'] = '&Ntilde;';
		entities['210'] = '&Ograve;';
		entities['211'] = '&Oacute;';
		entities['212'] = '&Ocirc;';
		entities['213'] = '&Otilde;';
		entities['214'] = '&Ouml;';
		entities['215'] = '&times;';
		entities['216'] = '&Oslash;';
		entities['217'] = '&Ugrave;';
		entities['218'] = '&Uacute;';
		entities['219'] = '&Ucirc;';
		entities['220'] = '&Uuml;';
		entities['221'] = '&Yacute;';
		entities['222'] = '&THORN;';
		entities['223'] = '&szlig;';
		entities['224'] = '&agrave;';
		entities['225'] = '&aacute;';
		entities['226'] = '&acirc;';
		entities['227'] = '&atilde;';
		entities['228'] = '&auml;';
		entities['229'] = '&aring;';
		entities['230'] = '&aelig;';
		entities['231'] = '&ccedil;';
		entities['232'] = '&egrave;';
		entities['233'] = '&eacute;';
		entities['234'] = '&ecirc;';
		entities['235'] = '&euml;';
		entities['236'] = '&igrave;';
		entities['237'] = '&iacute;';
		entities['238'] = '&icirc;';
		entities['239'] = '&iuml;';
		entities['240'] = '&eth;';
		entities['241'] = '&ntilde;';
		entities['242'] = '&ograve;';
		entities['243'] = '&oacute;';
		entities['244'] = '&ocirc;';
		entities['245'] = '&otilde;';
		entities['246'] = '&ouml;';
		entities['247'] = '&divide;';
		entities['248'] = '&oslash;';
		entities['249'] = '&ugrave;';
		entities['250'] = '&uacute;';
		entities['251'] = '&ucirc;';
		entities['252'] = '&uuml;';
		entities['253'] = '&yacute;';
		entities['254'] = '&thorn;';
		entities['255'] = '&yuml;';
	}

	if (useQuoteStyle !== 'ENT_NOQUOTES') {
		entities['34'] = '&quot;';
	}
	if (useQuoteStyle === 'ENT_QUOTES') {
		entities['39'] = '&#39;';
	}
	entities['60'] = '&lt;';
	entities['62'] = '&gt;';

	// ascii decimals to real symbols
	for (decimal in entities) {
		if (entities.hasOwnProperty(decimal)) {
			hash_map[String.fromCharCode(decimal)] = entities[decimal];
		}
	}

	return hash_map;
}


function htmlentities(string, quote_style, charset, double_encode) {
	var hash_map = get_html_translation_table('HTML_ENTITIES', quote_style),
		symbol = '';
	string = string == null ? '' : string + '';

	if (!hash_map) return false;

	if (quote_style && quote_style === 'ENT_QUOTES') hash_map["'"] = '&#039;';

	if (!!double_encode || double_encode == null) {
		for (symbol in hash_map) {
			if (hash_map.hasOwnProperty(symbol)) {
				string = string.split(symbol)
					.join(hash_map[symbol]);
			}
		}
	} else {
		string = string.replace(/([\s\S]*?)(&(?:#\d+|#x[\da-f]+|[a-zA-Z][\da-z]*);|$)/g, function (ignore, text, entity) {
			for (symbol in hash_map) {
				if (hash_map.hasOwnProperty(symbol)) {
					text = text.split(symbol)
						.join(hash_map[symbol]);
				}
			}

			return text + entity;
		});
	}

	return string;
}

function check_file_size(filesize){
	var response;
	$.ajax({
		url: 'admin.php?p=file_validate&action=checkFileSize',
		dataType: 'json',
		method: 'POST',
		data: {
			filesize: filesize
		},
		async: false,
		success: function(result) {
			response = result;
		}
	});

	return response;
}

function limitMetaCounter(meta_element, limit_info, limit)
{
	meta_string = $('#'+meta_element).val();
	chars_left = limit - meta_string.length;
	if (chars_left < 0) chars_left = 0;
	$('#'+limit_info).html("Max "+limit+" characters (" + chars_left + " left)");
}

function showMetaCounter()
{
	if ($('#field-meta_title').length == 1)
	{
		$('#field-meta_title').after('<div id="meta-title-limit-info">Max 69 characters (69 left)</id>');
	}
	if ($('#field-meta_description').length == 1)
	{
		$('#field-meta_description').after('<div id="meta-description-limit-info">Max 156 characters (156 left)</id>');
	}
}

function initDynamicAddressBookFieldEvent() {
	$("#addressbook-form-body fieldset").each(function() {
		var countryInput = $(this).find('.addressbook-input-address-country');
		var stateInput = $(this).find('.addressbook-input-address-state');
		var provinceInput = $(this).find('.addressbook-input-address-province');
	
		countryInput.empty();
		if ( countryInput.hasClass("all-countries-value") ){
			$(countryInput).append('<option value="0">All Countries</option>');
		}
	
		$.each(countriesStates, function(countryIndex, country){
			$(countryInput).append('<option value="' + country.id + '">' + country.name + '</option>');
		});
	
		$(countryInput)
			.val($(countryInput).attr('data-value'))
			.change(function(){
				var countryId = $(this).val();
				if ( countryId == 0 )
				{
				  $(stateInput)
					.attr('disabled', 'disabled')
					.closest('.form-group').hide();
				}
				else{
					$.each(countriesStates, function(countryIndex, country){
						if (country.id == countryId)
						{
							if (country.states)
							{
								$(stateInput).html('');
	
								if ( stateInput.hasClass("all-states-value") ){
									$(stateInput).append('<option value="0">All States</option>');
								}
	
								$.each(country.states, function(stateIndex, state){
									$(stateInput).append('<option value="' + state.id + '">' + state.name + '</option>');
								});
	
								stateInput.find('option[value="'+$(stateInput).attr('data-value')+'"]').prop('selected', 'selected');
								$(provinceInput)
									.attr('disabled', 'disabled')
									.closest('.form-group').hide();
								$(stateInput)
									.removeAttr('disabled')
									.closest('.form-group').removeClass('hidden').show();
							}
							else
							{
								$(stateInput)
									.attr('disabled', 'disabled')
									.closest('.form-group').hide();
								$(provinceInput)
									.removeAttr('disabled')
									.closest('.form-group').removeClass('hidden').show();
							}
						}
					});
				}
			})
			.change();
	});
}
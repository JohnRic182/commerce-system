var forms = (function() {
	var init, initAddressForm, ensureTooltips, initCheckbox, getPasswordStrength, setPasswordMeter, generatePassword, setPassword, confirmCopy, previewUpdate, trimString, validation;

	init = function() {
		$('body').on('load', '.field-checkbox', function() {
			initCheckbox(this);
		});

		$(document).ready(function() {

			$('.generate-id-source-field').on('keyup', function() {
				var element = $(this);
				var container = $(element).parent().parent();
				var generatedIdField = $(container).find('.generated-id-field');
				if (element.val()) {
					$(generatedIdField).parent().removeClass('has-error has-feedback');
					$(generatedIdField).parent().find('.warning-sign, .help-block.error').addClass('hidden');
				} else {
					$(generatedIdField).parent().addClass('has-error has-feedback');
					$(generatedIdField).parent().find('.warning-sign, .help-block.error').removeClass('hidden');
				}
			});

			var formFieldsWithErrors = $('form').find('.form-group.has-error .error');

			if (formFieldsWithErrors.length > 0)
			{
				$('.form-group.field-checkbox,.form-group.field-text,.form-group.field-money,.form-group.field-select,.form-group.field-integer,.form-group.field-numeric,.form-group.field-template').css({
					'min-height': '95px',
					'margin-bottom': '0px'
				});
			}

			$('form').on('submit', function() {
				var formFieldsWithErrors = $('form').find('.form-group.has-error .error');
				if (formFieldsWithErrors.length > 0)
				{
					$('.form-group.field-checkbox,.form-group.field-text,.form-group.field-money,.form-group.field-select,.form-group.field-integer,.form-group.field-numeric,.form-group.field-template')
						.not('td .form-group.field-checkbox')
						.css({
						'min-height': '95px',
						'margin-bottom': '0px'
					});
				}
			});

			$('#quick-start-guide-wizard .later, #quick-start-guide-wizard .previous, #quick-start-guide-wizard .go-back').on('click', function() {
				var formFieldsWithErrors = $('#quick-start-guide-form').find('.form-group.has-error .error');

				if (formFieldsWithErrors.length > 0)
				{
					$('.form-group.field-checkbox,.form-group.field-text,.form-group.field-money,.form-group.field-select,.form-group.field-integer,.form-group.field-numeric,.form-group.field-template')
					   .not('td .form-group.field-checkbox')
					   .css({
					   	    'min-height': '',
						    'margin-bottom': ''
					   });
				}

				$.each(formFieldsWithErrors, function(index, value) {
					$(value).addClass('hidden');
				});
			});

			$('button.btn.btn-primary').on('click', function() {
				if (!$(this).attr('data-action')) {
					if ($('.form-group.has-error .warning-sign').length) {
						$('.form-group.has-error .warning-sign').addClass('hidden');
						$('.form-group.has-error .error').addClass('hidden');
					}
				}
			});

			$('#copy-clipboard').on('click', function() {
				AdminForm.copyToClipboard($('#password-generated')[0]);
				return false;
			});

			$('body').on('input change paste', 'input, select, textarea', function() {
				var element = $(this);
				if ($(this).attr('data-required')) {
					if (element.val() && element.val().length > 0) {
						$(this).closest('div.form-group').removeClass('has-error has-feedback');
						$(this).parent().find('.warning-sign, .help-block.error').addClass('hidden');
					} else {
						$(this).closest('div.form-group').addClass('has-error has-feedback');
						$(this).parent().find('.warning-sign, .help-block.error').removeClass('hidden');
					}
				}

				if ($(this).attr('data-validators')) {
					var validators = $(this).data('validators').split('|');
					$(validators).each(function(index, validator) {
						var result = validation(element.val(), validator, element);
						if (result.status == false) {
							return false;
						}
					});
				}
			});

			$('input.hasDatepicker').change(function() {
				if ($(this).attr('data-required')) {
					if ($(this).val() && $(this).val().length > 0) {
						$(this).closest('div.form-group').removeClass('has-error has-feedback');
						$(this).parent().find('.warning-sign, .help-block.error').addClass('hidden');
					}
				}
			});

			$('table').on('input', 'input, select', function() {
				if ($(this).val() && $(this).val().length > 0)
				{
					$(this).closest('div.form-group').removeClass('has-error has-feedback');
					$(this).parent().find('.warning-sign, .help-block.error').addClass('hidden');
				}
				else
				{
					$(this).closest('div.form-group').addClass('has-error has-feedback');
					$(this).parent().find('.warning-sign, .help-block.error').removeClass('hidden');
				}
			});

			// this will open all accordion which has required fields errors
			$('div .has-error').each(function() {
				$(this).closest('div.collapse').removeClass('collapse').addClass('collapse-in').closest('fieldset').addClass('open');
			});

			$(window).load(function() {
				$('.modal').on('show.bs.modal', function () {
					var formGroups = $('.modal').find('.form-group.required');
					$(formGroups).each(function(index, element) {
						if ($(element).hasClass('required')) {
							$(element).addClass('has-error has-feedback');
							$(element).find('.warning-sign').removeClass('hidden');
						}

						if ($(element).find('input, textarea, select').val()) {
							$(element).removeClass('has-error has-feedback');
							$(element).find('.warning-sign').addClass('hidden');
						}
					});
				});

				if ($('.form-group.has-error .warning-sign').length) {
					$('.form-group.has-error .warning-sign').removeClass('hidden');
					$('.form-group.has-error .error').removeClass('hidden');
				}
			});

			var collapsePanels = $('fieldset .collapse');
			collapsePanels.on('show.bs.collapse', function() {
				$(this).closest('fieldset').addClass('open')
					.find('button[data-toggle=collapse]').html('Close<span class="caret"></span>');
			});
			collapsePanels.on('hide.bs.collapse', function() {
				$(this).closest('fieldset').removeClass('open')
					.find('button[data-toggle=collapse]').html('Edit<span class="caret"></span>');
			});

			$('fieldset').has('.collapse').find('legend').click(function() {
				$(this).parent('fieldset').find('button[data-toggle=collapse]').click();
			}).css('cursor', 'pointer');

			$('form').each(function(idx, ele) {
				$($(ele).find('fieldset').get(0)).addClass('fieldset-primary');
			});

			$('.field-checkbox').each(function(idx, ele) {
				initCheckbox(ele);
			});

			$('#regions,#tags').each(function (el, i) {
				$(this).tokenfield('setTokens', $(this).attr('value').split(';'));
			});

			ensureTooltips();
			tokenFields();

			/**
			 * Multiple external image fields
			 */
			$('.field-image-multiple .button-file-external-add')
				.click(function() {
				$('#modal-field-external-image-multiple-add').modal('show');
			});

			$('button[data-action="action-external-image-add"]').click(function () {
				var externalButton = $('.field-image-multiple .button-file-external-add');
				var inputContainer = $(
					'<div class="' + externalButton.data('field-name') + '_' + externalButton.data('counter') + '">' +
						'<input type="hidden" name="' + externalButton.data('field-name') + '[' + externalButton.data('counter') + '][image_url]" value="' + $('#modal-field-external-image-multiple-add-url').val() + '"/>' +
						'<input type="hidden" name="' + externalButton.data('field-name') + '[' + externalButton.data('counter') + '][image_alt_text]" value="' + $('#modal-field-external-image-multiple-add-alt-text').val() + '"/>' +
						'<input type="hidden" name="' + externalButton.data('field-name') + '[' + externalButton.data('counter') + '][image_caption]" value="' + $('#modal-field-external-image-multiple-add-caption').val() + '"/>' +
						'<input type="hidden" name="' + externalButton.data('field-name') + '[' + externalButton.data('counter') + '][image_priority]" value="' + $('#modal-field-external-image-multiple-add-priority').val() + '"/>' +
					'</div>'

				);
				inputContainer.insertBefore($('.field-image-multiple .button-file-add'))

				var imageUploadPreview = $(
					'<div class="field-image-multiple-preview ' + externalButton.data('field-name') + '_' + externalButton.data('counter') + '_preview">' +
					'<canvas></canvas>' +
					'<div><a href="#" class="btn" data-action="field-image-multiple-delete" data-role="button" data-target="' + externalButton.data('field-name') + '_' + externalButton.data('counter') +'">Delete</a></div>' +
					'<div class="text-center"> &nbsp; </div>' +
					'</div>'
				).insertBefore($('.field-image-multiple .button-file-add'));

				var imageUploadPreviewCanvas = $(imageUploadPreview).find('canvas');
				var imageUploadPreviewDelete = $(imageUploadPreview).find('[data-action="field-image-multiple-delete"]');

				$(imageUploadPreviewDelete).click(function(){
					var deleteButton = this;
					AdminForm.confirm(trans.forms.confirm_delete_image, function() {
						var target = $(deleteButton).attr('data-target');
						$('.' + target + '_preview, .' + target).remove();
						AdminForm.displayAlert(trans.forms.message_image_deleted, 'success');
					});
					return false;
				});

				var img = new Image();
				img.onload = function() {
					var width = this.width / (this.height / 150);
					var height = 150; // always 100 pixels
					imageUploadPreviewCanvas.attr({ width: width, height: height });
					imageUploadPreviewCanvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
				};
				img.src = $('#modal-field-external-image-multiple-add-url').val();

				$('.field-image-multiple .button-file-external-add').data('counter', $('.field-image-multiple .button-file-external-add').data('counter') + 1);
				$('#modal-field-external-image-multiple-add').modal('hide');
			})

			/**
			 * Multiple image fields
			 */
			$('.field-image-multiple .button-file-add')
				.data('counter', 1)
				.click(function() {
				var addButton = this;
				var parent =  $(this).closest('.field-image-multiple');
				var inputFieldName = $(this).attr('data-field-name');
				var fileInput = $(parent).find('input.clean[type="file"]');

				if (fileInput.length == 0) {
					var counter = $(addButton).data('counter');
					fileInput = $('<input type="file" class="clean ' + inputFieldName + '-' + counter + '-file " name="' + inputFieldName +'[]">').appendTo(parent);
					$(addButton).data('counter', counter+1);

					/**
					 * Handle file change
					 */
					$(fileInput).change(function(){
						var self = this;
						if (self.files != undefined && self.files[0]) {
							var file = this.files[0];

							if (!file || file.name == undefined || file.size == undefined) return;

							var fileSize = check_file_size(file.size);
							if (fileSize.status == 0){
								alert(fileSize.message);
								return;
							}

							var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
							if ('|jpg|png|jpeg|gif|'.indexOf(type) === -1) {
								alert('Please choose image file!');
								return;
							}

							$(self).removeClass('clean');

							var imageUploadPreview = $(
								'<div class="field-image-multiple-preview ' + inputFieldName + '-' + counter + '-preview">' +
									'<canvas></canvas>' +
									'<div><a href="#" class="btn" data-action="field-image-multiple-delete" data-role="button" data-target="' + inputFieldName + '-' + counter +'">Delete</a></div>' +
									'</div>'
							).insertBefore(addButton);

							var imageUploadPreviewCanvas = $(imageUploadPreview).find('canvas');
							var imageUploadPreviewDelete = $(imageUploadPreview).find('[data-action="field-image-multiple-delete"]');

							$(imageUploadPreviewDelete).click(function(){
								var deleteButton = this;
								AdminForm.confirm(trans.forms.confirm_delete_image, function() {
									var target = $(deleteButton).attr('data-target');
									$('.' + target + '-preview,.' + target + '-file').remove();
									AdminForm.displayAlert(trans.forms.message_image_deleted, 'success');
								});
								return false;
							});

							var reader = new FileReader();

							reader.onload = function(e) {
								var data = e.target.result;
								var dataParts = data.split(',');

								var img = new Image();
								img.onload = function() {
									var width = this.width / (this.height / 150);
									var height = 150; // always 100 pixels
									imageUploadPreviewCanvas.attr({ width: width, height: height });
									imageUploadPreviewCanvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
								};
								img.src = e.target.result;
							};
							reader.readAsDataURL(file);
						}
					});
				}

				$(fileInput).click();
			});

			// TODO: make image lists sortable, attache priority update
//		$('.field-image-multiple ul').sortable();
//		$('.field-image-multiple ul').disableSelection();

			$('.field-image-multiple a[data-action="field-image-multiple-preview"]').click(function(){
				var self = this;
				AdminForm.imagePreview($(self).attr('href'));
				return false;
			});

			$('.field-image-multiple a[data-action="field-image-multiple-edit"]').click(function(){
				var self = this;
				var imageEditModal = $('#modal-field-image-multiple-edit');

				var parent = $(self).closest('.field-image-multiple-preview');
				var src = $(parent).find('a[data-action="field-image-multiple-preview"] img').attr('src');
				var mainImage = $(parent).find('a[data-action="field-image-multiple-preview"]').attr('href');
				var primary = $(self).attr('data-image-primary') == '1';
				var location = $(self).attr('data-image-location');
				var productName = $('#field-title').val();

				imageEditModal.find('img.image-preview').attr('src', src);
				imageEditModal.find('img.image-preview').attr('alt', productName);
				src = (mainImage.indexOf('}') != -1) ? mainImage.slice(0,-1) : mainImage;

				var mainImageArr = mainImage.split('?');
				mainImage = mainImageArr[0];

				var siteUrl = new RegExp(/^.*\//);
				var rootUrl = (location.indexOf("Web") >= 0 ? '' : siteUrl.exec(window.location.href));

				imageEditModal.find('.image-info').html(
					'Image file: <a href="' + mainImage + '" target="_blank" > ' + mainImage + '</a><br>' +
					'<span style="text-decoration: underline; cursor: pointer" data-clipboard-text="' + rootUrl + mainImage + '" id="copy-clipboard" class="copy-clipboard">Copy URL</span><br>' +
					'<input id="_hiddenCopyText_" class="copy-clipboard-url hidden" value="' + rootUrl + mainImage + '" type="text">' +
					'Image location: ' + location + '<br>' +
					(
						$(self).attr('data-image-size') != 'x' && $(self).attr('data-image-file-size') != '' ?
						('Image size: ' + $(self).attr('data-image-size') + ' / ' + Math.round($(self).attr('data-image-file-size')/1024, 2) + 'kb') : ''
					) +
					(primary ? '<br><br><strong>This is primary product image</strong>' : '')
				);
				$(imageEditModal).find('.modal-field-image-multiple-edit-caption,.modal-field-image-multiple-edit-priority,.modal-field-image-multiple-edit-primary').toggle(!primary);
				$('#modal-field-image-multiple-edit-alt-text').val($(self).attr('data-image-alt-text'));
				$('#modal-field-image-multiple-edit-caption').val($(self).attr('data-image-caption'));
				$('#modal-field-image-multiple-edit-priority').val($(self).attr('data-image-priority'));
				$('#modal-field-image-multiple-edit-primary').prop('checked', false).removeAttr('checked').change();

				imageEditModal.find('.btn-primary').unbind('click').click(function(){
					var newAltText = $('#modal-field-image-multiple-edit-alt-text').val();
					var newCaption = $('#modal-field-image-multiple-edit-caption').val();
					var newPriority = $('#modal-field-image-multiple-edit-priority').val();
					var makePrimary = $('#modal-field-image-multiple-edit-primary').is(':checked');

					imageEditModal.modal('hide');

					AdminForm.sendRequest(
						$(self).attr('href'),
						{
							altText: newAltText,
							caption: newCaption,
							priority: newPriority,
							makePrimary: makePrimary ? '1' : '0'
						},
						'',
						function(result) {
							// update image property
							$(self).attr('data-image-alt-text', newAltText);
							$(self).attr('data-image-caption', newCaption);
							$(self).attr('data-image-priority', newPriority);

							var dateTime = new Date();
							var imageId = $(self).attr('data-image-id');

							var currentPrimary = $(self).closest('.field-image-multiple').find('.field-image-multiple-preview-primary');
							var currentSecondary = $(self).closest('.field-image-multiple').find('.field-image-multiple-preview-' + imageId);

							// update primary image
							if (result.primaryImage) {
								// initialize target swap secondary and primary image to avoid image
								// look like duplicate during switching of images
								$(currentPrimary).find('img').attr('src', null);
								$(currentSecondary).find('img').attr('src', null);

								$(currentPrimary).find('a[data-action="field-image-multiple-preview"]').attr('href', result.primaryImage.imageFileName);
								$(currentPrimary).find('a[data-action="field-image-multiple-edit"]')
									.attr('data-image-url', result.primaryImage.imageFileName)
									.attr('data-image-alt-text', result.primaryImage.altText)
									.attr('data-image-caption', result.primaryImage.caption)
									.attr('data-image-location', result.primaryImage.location);
								$(currentPrimary).find('a[data-action="field-image-multiple-preview"]').attr('href', result.primaryImage.imageFileName);
								$(currentPrimary).find('img').attr('src', result.primaryImage.imageFileName + '?rnd=' + dateTime.getTime());
							}

							// update secondary images
							if ($(currentPrimary).length > 0) {
								if (result.secondaryImages != undefined && result.secondaryImages) {
									$.each(result.secondaryImages, function(index, secondaryImage){

										if (secondaryImage.image_id == imageId) {
											$(currentSecondary).find('a[data-action="field-image-multiple-preview"]').attr('href', secondaryImage.imageFileName);
											$(currentSecondary).find('a[data-action="field-image-multiple-edit"]')
												.attr('data-image-url', secondaryImage.imageFileName)
												.attr('data-image-alt-text', secondaryImage.altText)
												.attr('data-image-caption', secondaryImage.caption)
												.attr('data-image-location', secondaryImage.location)
												.attr('data-image-priority', secondaryImage.priority);
											$(currentSecondary).find('img').attr('src', secondaryImage.imageFileName + '?rnd=' + dateTime.getTime());
										}
									});
								}
							}

							AdminForm.displayAlert(trans.forms.message_image_updated, 'success');
						}
					);
				});

				imageEditModal.modal('show');

				return false;
			});

			$('.field-image-multiple a[data-action="field-image-multiple-delete"]').click(function(){
				var self = this;
				AdminForm.confirm(trans.forms.confirm_delete_image, function () {
					AdminForm.sendRequest(
						$(self).attr('href'),
						{},
						'Please wait',
						function (data) {
							$(self).closest('.field-image-multiple-preview').remove();
							if (data.imageId) {
								if(data.primaryImageChanged) {
									$('.field-image-multiple-preview-' + data.imageId).append('<div class="text-center">Primary Image </div>');
									$('.field-image-multiple-preview-' + data.imageId).find("a[data-action='field-image-multiple-edit']").attr('href', data.editUrl)
										.attr('data-image-id', 'primary')
										.attr('data-image-primary', 1);
									$('.field-image-multiple-preview-' + data.imageId).find("a[data-action='field-image-multiple-delete']").attr('href', data.deleteUrl);
								}
								AdminForm.displayAlert(trans.forms.message_image_deleted, 'success');
							} else {
								AdminForm.displayAlert(trans.products.no_primary_image, 'danger');
							}
						}
					);
				});
				return false;
			});

			/**
			 * Image fields
			 */
			$('.field-image input[type="file"]').change(function() {
				var self = this;

				if (self.files != undefined && self.files[0]) {

					var file = self.files[0];

					if (!file || file.name == undefined || file.size == undefined) return;

					var fileSize = check_file_size(file.size);
					if (fileSize.status == 0){
						alert(fileSize.message);
						return;
					}

					var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';

					if ('|jpg|png|jpeg|gif|'.indexOf(type) === -1) {
						alert('Please choose image file!');
						return;
					}

					var parent = $(self).parent();
					var canvas = $(self).parent().find('canvas');
					var image = $(self).parent().find('img');

					var imageDelete = $('<div class="field-image-delete"><a href="#" class="btn" data-action="field-image-delete" data-role="button" data-target="">Delete this image</a></div>');
					if (parent.find('a[data-action="field-image-delete"]').length == 0) {
						imageDelete.appendTo(parent.find('.field-image-preview'));
					}

					$(imageDelete).click(function () {
						AdminForm.confirm(trans.forms.confirm_delete_image, function() {
							$(self).val(null);
							parent.find('.field-image-preview').html('<canvas></canvas>');
							AdminForm.displayAlert(trans.forms.message_image_deleted, 'success');
						});
						return false;
					});

					var reader = new FileReader();

					reader.onload = function(e) {
						var data = e.target.result;
						var dataParts = data.split(',');

						var img = new Image();
						img.onload = function(){
							var width = this.width / (this.height / 100);
							var height = 100; // always 100 pixels
							canvas.attr({ width: width, height: height });
							canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);

							$(image).hide();
							$(canvas).show();
						};
						img.src = e.target.result;
					};

					reader.readAsDataURL(file);
				}
			});

			$('.field-image a[data-action="field-image-delete"]').click(function(){
				var self = this;
				AdminForm.confirm(trans.forms.confirm_delete_image, function() {
					AdminForm.sendRequest(
						$(self).attr('href'),
						{},
						'Please wait',
						function(data){
							$('.field-image input[type="file"]').val(null);
							$(self).closest('.field-image-preview').html('<canvas></canvas>');
							AdminForm.displayAlert(trans.forms.message_image_deleted, 'success');

							var hasAvatar = ($("#user-avatar-thumb") != undefined ? true : false);
							if (hasAvatar) {
								$('a.dropdown-toggle').find('#user-avatar-thumb').remove();
								$('span.avatar-path').empty();

								var name = $('.admin-name').text();
								var avatarCanvas = $('<canvas/>')
									.attr('id', 'user-icon-thumb')
									.attr('width', 38)
									.attr('height', 38)
									.addClass('img-circle');
								$('a.dropdown-toggle').prepend(avatarCanvas);
								AdminForm.avatarInitials(name, 'user-icon-thumb', '16px Arial', false);
							}
						}
					);
				});
				return false;
			});

			/**
			 * Address forms with countries and states
			 */
			if (typeof countriesStates !== 'undefined') {
				$('.form-address').each(function(formIndex, form) {
					initAddressForm(form);
				});
			}

			/**
			 * Password
			 */
			if ($('.field-password.meter').length > 0)
			{
				$('.field-password.form-group input[type=password]')
					.change(function(){
						setPasswordMeter(
							$(this).closest('.form-group'),
							getPasswordStrength($(this).val())
						);
					})
					.keyup(function(){
						setPasswordMeter(
							$(this).closest('.form-group'),
							getPasswordStrength($(this).val())
						);
					});
			}

			/**
			 * Recurring fields
			 */
			if ($('.field-recurring-settings').length > 0)
			{
				$('.field-recurring-settings .custom-checkbox').change(function(){
					var id = '#' + $(this).attr('data-field-id');
					var trial = $(this).closest('.field-recurring-settings').hasClass('trial');

					if ($(this).is(':checked'))
					{
						$(id + '-standard-fields').hide();
						$(id + '-custom-fields').show().removeClass('hidden');
					}
					else
					{
						$(id + '-custom-fields').hide();
						$(id + '-standard-fields').show().removeClass('hidden');

						if (trial)
						{
							//alert(1);
							var periodCycles = $(this).closest('.field-recurring-settings').find('.period-cycles');
							$(periodCycles).val('1');
							$(periodCycles).change();
						}
					}
				});

				$('.field-recurring-settings .period-frequency-and-unit')
					.change(function(){
						var trial = $(this).closest('.field-recurring-settings').hasClass('trial');
						var id = '#' + $(this).attr('data-field-id');
						var val = $(this).val();
						var parts = val.split('-');
						$(id + '_period_frequency').val(parts[0]);
						$('.field-recurring-settings .period-frequency[data-field-id="' + $(this).attr('data-field-id') + '"]').val(parts[0]);

						$(id + '_period_unit').val(parts[1]);
						$('.field-recurring-settings .period-unit[data-field-id="' + $(this).attr('data-field-id') + '"]').val(parts[1]);
					});

				$('.field-recurring-settings .period-frequency').change(function(){
					var id = '#' + $(this).attr('data-field-id');
					$(id + '_period_frequency').val($(this).val());

					var periodFrequencyAndUnit = $(this).closest('.field-recurring-settings').find('.period-frequency-and-unit');
					var periodUnit = $(this).closest('.field-recurring-settings').find('.period-unit');

					var newPeriodFrequencyAndUnitValue = $(this).val() + '-' + $(periodUnit).val();
					if ($(periodFrequencyAndUnit).find('option[value="' + newPeriodFrequencyAndUnitValue + '"]').length > 0)
					{
						$(periodFrequencyAndUnit).val(newPeriodFrequencyAndUnitValue);
					}
				});

				$('.field-recurring-settings .period-unit').change(function(){
					var id = '#' + $(this).attr('data-field-id');
					$(id + '_period_unit').val($(this).val());

					var periodFrequencyAndUnit = $(this).closest('.field-recurring-settings').find('.period-frequency-and-unit');
					var periodFrequency = $(this).closest('.field-recurring-settings').find('.period-frequency');

					var newPeriodFrequencyAndUnitValue = $(periodFrequency).val() + '-' + $(this).val();
					if ($(periodFrequencyAndUnit).find('option[value="' + newPeriodFrequencyAndUnitValue + '"]').length > 0)
					{
						$(periodFrequencyAndUnit).val(newPeriodFrequencyAndUnitValue);
					}
				});

				$('.field-recurring-settings .period-cycles').change(function(){
					var id = '#' + $(this).attr('data-field-id');
					$(id + '_period_cycles').val($(this).val());

					$('.field-recurring-settings .period-cycles[data-field-id="' + $(this).attr('data-field-id') + '"]').val($(this).val());
				});
			}
		});

		/**
		 * SEO Search Results Preview
		 */
		$('.seo-preview').each(function() {
			$(this).keyup(function (){
				previewUpdate(this);
			});
		});

		// auto generated id field
		$('.generate-id-source-field').keyup(function () {
			var id = $(this).val();
			var lowercase = id.toLowerCase();
			var stripChars = lowercase.replace(/[^\w\s]|_/gi, ' ');
			var generatedId = $.trim(stripChars).replace(/[\s+]/g, '_');

			var container = $(this).parent().parent();
			if ($(container).find('.generated-id-field').hasClass('autogenerate'))
			{
				$(container).find('.generated-id-field').val(generatedId);
			}
			else if ($(container).find('.generated-id-field').val().length === 0)
			{
				$(container).find('.generated-id-field').addClass('autogenerate');
				$(container).find('.generated-id-field').val(generatedId);
			}
		});

		$('.generated-id-field').keyup(function () {
			$('.generated-id-field').removeClass('autogenerate');
		});

		$('#btn-generate-password').click(function() {
			generatePassword();
		});
		$('#use-password').click(function() {
			setPassword();
		});
		$("#password-confirmed-copy").click( function(){
			confirmCopy();
		});

	};

	trimString = function(string, limit) {
		if (string.length > limit) {
			var lastSpace = string.substring(0,limit).lastIndexOf(' ');
			string = string.substring(0,lastSpace);
		}

		return string;
	};

	previewUpdate = function(el) {
		var value = $('.' + el.id).val().trim();
		var uri = cartDomain + '/' + $('.' + el.id).val().trim();

		value = trimString(value, el.maxlength);

		$('.prev-' + el.id).hasClass('is_url') ? $('.prev-' + el.id).html(uri) : $('.prev-' + el.id).html(value);;
	};

	getPasswordStrength = function(str) {
		if (str == '') return 0;
		var points = 0;
		if (str.length >= 8) points++;
		if (/[a-z]/.test(str) && /[A-Z]/.test(str)) points++;
		if (/\d+/.test(str)) points++;
		if (/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/.test(str)) points++;
		if (str.length > 12) points++;

		return points;
	}

	setPasswordMeter = function(selector, strength)
	{
		var titles = ['Too short', 'Very weak', 'Weak', 'Good', 'Strong', 'Very strong']
		var progressBar = $(selector).find('.field-password-meter');

		$(progressBar).css('width', strength * 20 +'%').attr('aria-valuenow', strength * 20);
		$(progressBar).removeClass('progress-bar-warning').removeClass('progress-bar-danger');

		if (strength < 2) $(progressBar).addClass('progress-bar-danger')
		else if (strength < 4) $(progressBar).addClass('progress-bar-warning')
		else $(progressBar).addClass('progress-bar-success');

		$(selector).find('.field-password-strength').html(titles[strength]);
	}

	initCheckbox = function(ele) {
		$(ele).find('label').append('<span class="sprite"></span>');
		var cb = $(ele).find('input[type=checkbox]');
		if (cb.is(':disabled')) {
			$(ele).find('label,.sprite').addClass('not-clickable');
		}

		var toggleCbSprite = function(cbEle, ele) {
			if ($(cbEle).is(':checked')) {
				$(cbEle).siblings('.sprite').addClass('checked');
			} else {
				$(cbEle).siblings('.sprite').removeClass('checked');
			}
		};
		$(ele).find('input[type=checkbox]').change(function() {
			toggleCbSprite(this, ele);
		}).focus(function() {
			$(this).siblings('.sprite').addClass('active');
		}).blur(function() {
			$(this).siblings('.sprite').removeClass('active');
		});
		cb.each(function(idx, cbEle) {
			toggleCbSprite(cbEle, ele);
		});

		var rb = $(ele).find('input[type=radio]');
		rb.click(function() {
			var theForm = $(this).closest('form');
			theForm.find('input[name="'+$(this).attr('name')+'"]').closest('.form-group').find('.sprite').removeClass('checked');
			$(this).siblings('.sprite').addClass('checked');
		}).focus(function() {
			$(this).siblings('.sprite').addClass('active');
		}).blur(function() {
			$(this).siblings('.sprite').removeClass('active');
		});
		rb.filter(':checked').siblings('.sprite').addClass('checked');
	};

	tokenFields = function ()
	{
		$('.tokenfield').each(function (el, i) {
			$(this).tokenfield('setTokens', $(this).attr('value').split(';'));
			$(this).parent().removeClass('form-group');
			$("label[for='"+$(this).attr('id')+"']").css('font-weight', 400);
		});

		$('.tokenfield').tokenfield({
			delimiter: ',',
			createTokensOnBlur: true
		});
	};

	ensureTooltips = function() {
		$('.tooltip-popover').each(function(idx, ele) {
			var parentFormGroup = $(ele).closest('.form-group,th');
			$(ele).popover({
				'animation': true,
				'html': true,
				'placement': 'top',
				'selector': false,
				'trigger': 'hover',
				'title': '',
				'content': parentFormGroup.find('.tooltip').html(),
				'delay': 0,
				'container': false
			}).click(function() {
				$(this).popover('toggle');
			}).mouseenter(function() {
				if ($(this).siblings('.popover:visible').length == 0) {
					$(this).popover('show');
				}
			});

			$(ele).closest('form').find('input,select,textarea,button,a').focus(function() {
				var popoverEle = $('.popover:visible');
				var parentFormGroup = $(this).closest('.form-group,th');
				if (popoverEle.length > 0 && parentFormGroup.find('.popover:visible').length == 0) {
					popoverEle.hide();
				}
				return true;
			});
		});

		$(window).on('blur', function(e) {
			if ($(this).is('input,select,textarea')) {
				var popoverEle = $(this).closest('.form-group,th').find('.popover:visible');
				if (popoverEle.length > 0) {
					popoverEle.siblings('.tooltip-popover').popover('hide');
				}
			}
		});
	};

	initAddressForm = function(form) {
		var countryInput = $(form).find('.input-address-country');
		var stateInput = $(form).find('.input-address-state');
		var provinceInput = $(form).find('.input-address-province');
		var zipInput = $(form).find('.input-address-zip');

		// 21.05.2014 Suflihim
		countryInput.empty();
		countryInput.each(function() {
			if ( $(this).hasClass("all-countries-value") ){
				$(this).append('<option value="0">All Countries</option>');
			}
		});
		// 21.05.2014 Suflihim end

		$.each(countriesStates, function(countryIndex, country){
			if (country.id != '') {
				$(countryInput).append('<option value="' + country.id + '" has_provinces="' + country.has_provinces + '" zip_required="' + country.zip_required + '">' + country.name + '</option>');
			}
		});

		$(countryInput)
			.val($(countryInput).attr('data-value'))
			.change(function(){
				var countryId = $(this).val();
				var zip_required = $('option:selected', this).attr('zip_required');
				var has_provinces = $('option:selected', this).attr('has_provinces');

				// do not required zip / postal input if the country no zip
				if (zip_required == '0')
				{
					$(zipInput).parent().closest('div').removeClass('required');

					$(zipInput).parent().removeClass('has-error');
					$(zipInput).parent().removeClass('required');
					$(zipInput).parent().find('.warning-sign, .help-block.error').addClass('hidden');
					$(zipInput).removeAttr('data-required');
				} else {
					$(zipInput).parent().closest('div').addClass('required');

					if ($(zipInput).val() == '') {
						$(zipInput).attr('data-required', 1);
						$(zipInput).parent().find('.warning-sign, .help-block.error').removeClass('hidden');
						$(zipInput).parent().addClass('has-error required has-feedback');
					}
				}

				// do not required state/provinces input if the country does not required
				if (has_provinces == '0')
				{
					$(provinceInput).parent().closest('div').removeClass('required');

					$(provinceInput).parent().removeClass('has-error');
					$(provinceInput).parent().removeClass('required');
					$(provinceInput).parent().find('.warning-sign, .help-block.error').addClass('hidden');
					$(provinceInput).removeAttr('data-required');
				} else {
					$(provinceInput).parent().closest('div').addClass('required');

					if ($(provinceInput).val() == '') {
						$(provinceInput).attr('data-required', 1);
						$(provinceInput).parent().find('.warning-sign, .help-block.error').removeClass('hidden');
						$(provinceInput).parent().addClass('has-error required has-feedback');
					}
				}

				if (countryId > 0) {
					countryInput.closest('div').removeClass('has-error');
				}

				// 21.05.2014 Suflihim
				// Hide "states fields group" on "all countries"
				if ( countryId == 0 ) {
					$(stateInput)
						.attr('disabled', 'disabled')
						.closest('.form-group').hide();

					$(provinceInput)
						.attr('disabled', 'disabled')
						.closest('.form-group').hide();
				} else {
					$.each(countriesStates, function(countryIndex, country){
						if (country.id == countryId)
						{
							if (country.states)
							{
								$(stateInput).html('');

								// 21.05.2014 Suflihim
								stateInput.each(function() {
									if ( $(this).hasClass("all-states-value") ){
										$(this).append('<option value="0">All States</option>');
									}
								})
								// 21.05.2014 Suflihim end

								$.each(country.states, function(stateIndex, state){
									$(stateInput).append('<option value="' + state.id + '">' + state.name + '</option>');
								});

								stateInput.find('option[value="'+$(stateInput).attr('data-value')+'"]').prop('selected', 'selected');

								if ($(stateInput).val() > 0) {
									$(stateInput).closest('div').removeClass('has-error');
								}

								$(provinceInput)
									.attr('disabled', 'disabled')
									.closest('.form-group').hide();
								$(stateInput)
									.removeAttr('disabled')
									.closest('.form-group').removeClass('hidden').show();
							}
							else
							{
								$(stateInput)
									.attr('disabled', 'disabled')
									.closest('.form-group').hide();
								$(provinceInput)
									.removeAttr('disabled')
									.closest('.form-group').removeClass('hidden').show();
							}
						}
					});
				}
				// 21.05.2014 Suflihim end
			})
			.change();
	};

	generatePassword = function(){

		$.ajax({
			url: 'admin.php?p=profile&mode=generatePassword',
			success: function (response, status) {
				$('#password-generated').val(response.password);
				$('#copy-clipboard').removeAttr('disabled');
			}
		});

	}

	setPassword = function(){
		var sourcePassword = $('#password-generated').val();

		$('#field-password').val(sourcePassword);
		$('#field-password2').val(sourcePassword);
		$('#modal-generator').modal('hide');
		setPasswordMeter(
			$('.generate-password-row').closest('.form-group'),
			getPasswordStrength($('#field-password').val())
		);

		$('#field-password, #field-password2').closest('div.form-group').removeClass('has-error has-feedback');
		$('#field-password, #field-password2').closest('div.form-group').find('.warning-sign, .help-block.error').addClass('hidden');
	}

	confirmCopy = function(){
		if( $('#password-confirmed-copy').is(':checked') )
		{
			$('#use-password').removeAttr('disabled');
		}
		else
		{
			$('#use-password').attr('disabled','disabled');
		}
	}

	validation = function (data, validator, element) {
		var response = {};
		switch (validator) {
			case 'required':
				// Need to put code here for required
				break;

			case 'alpha':
				// Need to put code here for alphabetic characters
				break;

			case 'numeric':
				response.status = $.isNumeric(data);
				// This is for numeric data replacement
				if (element && typeof (element) != 'undefined') {
					var newData = element.val().replace(/[^0-9\.]+/g, '');
					if (newData.split('.').length > 2) {
						newData = newData.replace(/\.+$/, '');
					}
					element.val(newData);
				}
				break;

			case 'alnum':
				// Need to put code here for alphanumeric characters
				break;

			default:
				break;
		}



		return response;
	}

	init();

	return {
		init: init,
		ensureTooltips: ensureTooltips,
		initAddressForm: initAddressForm,
		initCheckbox: initCheckbox
	};
}());

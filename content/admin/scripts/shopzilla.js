var Shopzilla = (function($) {
	var init;

	init = function() {
		$('#shopzilla-export').click(function() {
			$('#iframe-download').attr('src', 'admin.php?p=shopzilla&mode=export');
			return false;
		});
	};

	init();
}(jQuery));
var CheckoutSettings = (function() {
	var init, toggleGiftCard,
		$;

	init = function(jQuery) {
		$ = jQuery;

		$('#field-GiftCardActive').change(function() {
			toggleGiftCard();
		});

		$('#field-MinOrderNumber, #field-MinOrderSubtotalLevel0').keypress(function(e) {
			var evt = (e) ? e : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;

			if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
				return false;
			}
			return true;
		});

		toggleGiftCard();
	};

	toggleGiftCard = function() {
		$('#field-GiftCardMessageLength').closest('.form-group').toggle($('#field-GiftCardActive').is(':checked'));
	};

	return {
		init: init
	};
}());

$(document).ready(function() {
	CheckoutSettings.init($);
});
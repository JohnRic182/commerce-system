(function($) {
	var init;

	init = function() {
		$(document).ready(function() {
			$('.cleanup-images').click(function() {
				AdminForm.confirm(trans.thumbs_generator.confirm_potential_loss_data, function() {
					AdminForm.sendRequest('admin.php?p=thumbs_generator&mode=cleanup-images',
						null, null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=thumbs_generator';
							} else if (data.message !== undefined) {
								AdminForm.displayAlert(data.message, 'danger');
							}
						});
				});
				return false;
			});

			$('.sync-secondary-images').click(function() {
				AdminForm.sendRequest('admin.php?p=thumbs_generator&mode=sync-secondary-images',
					null, null,
					function(data) {
						if (data.status == 1) {
							document.location = 'admin.php?p=thumbs_generator';
						} else if (data.message !== undefined) {
							AdminForm.displayAlert(data.message, 'danger');
						}
					});
				return false;
			});

			$('form[name="thumbs_generator_generate"]').submit(function() {
				var theForm = $(this);
				var nonce = theForm.find('input[name="nonce"]').val();

				var progressModal = $('#modal-generate-progress');

				var theProgressBar, theProgressContainer, theProgressLabel;

				theProgressBar = progressModal.find('.progress-bar');
				theProgressContainer = progressModal.find('.progress');
				theProgressLabel = progressModal.find('.progress-bar span');

				theProgressLabel.html('0% Completed');
				theProgressBar.css('width', 0);
				theProgressContainer.show();

				var doRequest = function() {
					if (!progressModal.is(':visible')) {
						progressModal.modal({
							keyboard: false
						}).modal('show');
					}

					AdminForm.sendRequest('admin.php?p=thumbs_generator&mode=generate',
						'nonce='+nonce, null,
						function(data) {
							if (data.status == 1) {
								if (data.result.progress < 100) {
									var percentage = Math.round(data.result.progress);
									theProgressLabel.html(percentage+'% Completed');
									theProgressBar.css('width', percentage+'%');
									doRequest();
								} else {
									theProgressBar.css('width', '100%');
									theProgressLabel.html('100% Completed');

									setTimeout(function() {
										progressModal.modal('hide');
										document.location = 'admin.php?p=thumbs_generator';
									}, 500);
								}
								console.log(data.result);
							} else {
								progressModal.modal('hide');
								AdminForm.displayAlert(data.message, 'danger');
							}
						})
				};

				doRequest();
				return false;
			});
		});
	};

	init();
}(jQuery));
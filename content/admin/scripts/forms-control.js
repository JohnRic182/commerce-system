function HideAndShowFields(){
    var $field_type = $("#field-field_type").val();
    $("#field-text_length").closest('div').hide();
    $("label[for='field-text_length']").closest('div').hide();

    $(".field-options").closest('div').hide();
    $("label[for='field-options']").closest('div').hide();

    $(".field-value_checked").closest('div').hide();
    $("label[for='field-value_checked']").closest('div').hide();

    $(".field-value_unchecked").closest('div').hide();
    $("label[for='field-value_unchecked']").closest('div').hide();

    switch ($field_type) {
        case 'text':
        case 'textarea':
            $("#field-text_length").closest('div').show();
            $("label[for='field-text_length']").closest('div').show();
            break;

        case 'select':
        case 'radio':
            $(".field-options").closest('div').show();
            $("label[for='field-options']").closest('div').show();
            break;

        case 'checkbox':
            $(".field-value_checked").closest('div').show();
            $("label[for='field-value_checked']").closest('div').show();

            $(".field-value_unchecked").closest('div').show();
            $("label[for='field-value_unchecked']").closest('div').show();
            break;
    }
}

$(document).ready(function(){

    $('form[name=form-forms_control-standard] .radio-list div').removeClass('col-xs-12');
    $('form[name=form-forms_control-standard] .radio-list div').addClass('col-xs-4');

    HideAndShowFields();
    $('form[name=form-customfield]').submit(function() {
        var errors = [];
        if ($.trim($('#field-field_name').val()) == '') {
            errors.push({
                field: '#field-field_name',
                message: trans.form_fields.name_required
            });
        }

        if ($.trim($('#field-field_caption').val()) == '') {
            errors.push({
                field: '#field-field_caption',
                message: trans.form_fields.caption_required
            });
        }

        var $field_type = $("#field-field_type").val();

        switch ($field_type) {
            case 'text':
            case 'textarea':
                var i_val = parseInt($.trim($('#field-text_length').val()));

                if ($.trim($('#field-text_length').val()) == '') {
                    errors.push({
                        field: '#field-text_length',
                        message: trans.forms_control.enter_text_length
                    });
                }
                else if (i_val <= 0) {
                    errors.push({
                        field: '#field-text_length',
                        message: trans.forms_control.message_text_length_greater
                    });
                }
                break;

            case 'select':
            case 'radio':
                if ($.trim($(".field-options").val()) == '') {
                    errors.push({
                        field: '.field-options',
                        message: trans.forms_control.enter_field_option
                    });
                }
                break;

            case 'checkbox':
                if ($.trim($('#field-value_checked').val()) == '') {
                    errors.push({
                        field: '#field-value_checked',
                        message: trans.forms_control.enter_checked_value
                    });
                }

                if ($.trim($('#field-value_unchecked').val()) == '') {
                    errors.push({
                        field: '#field-value_unchecked',
                        message: trans.forms_control.enter_unchecked_value
                    });
                }
                break;
        }

        AdminForm.displayValidationErrors(errors);
        return errors.length == 0;
    });

    $("#field-field_type").change(function(){
        HideAndShowFields();
    });
});

$('[data-action="action-customfields-delete"]').click(function(){

    var deleteMode = $('input[name="form-customfields-delete-mode"]:checked').val();

    if (deleteMode == 'selected')
    {
        var ids = '';
        $('.checkbox-customfield:checked').each(function(){
            var id = $(this).val();
            ids += (ids == '' ? '' : ',') + id;
        });
        if (ids != '')
        {
            window.location='admin.php?p=forms_control&mode=deletecustomfield&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
        }
        else
        {
            alert(trans.forms_control.select_custom_field_delete);
        }
    }
    else if (deleteMode == 'all')
    {
        window.location='admin.php?p=forms_control&mode=deletecustomfield&deleteMode=all' + '&nonce=' + $(this).attr('nonce');
    }
});


function removeCustomField(id, nonce) {
    AdminForm.confirm(trans.forms_control.confirm_delete_custom_field, function() {
        window.location = 'admin.php?p=forms_control&mode=deletecustomfield&deleteMode=single&id=' + id + '&nonce=' + nonce;
    });

    return false;
}

function pushAddCUstomFieldButton(url){
    window.location=url;
}
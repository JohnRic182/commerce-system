var CreditCardStorage = (function($) {
	var init, handleGenerateCertificate, handleActiveToggle, handleSettingsFormSubmit, pwdStrengthPoints;

	init = function() {
		$('.regenerate-certificate').click(function() {
			AdminForm.confirm(trans.credit_card_storage.confirm_regenerate_access, function() {
				$('#modal-generate-certificate').modal('show');
			});

			return false;
		});

		$('form[name=form-generate-certificate]').submit(function() {
			handleGenerateCertificate($(this));
			return false;
		});

		$('#field-ccsSettings-SecurityCCSActive').change(function() {
			handleActiveToggle();
		});
		handleActiveToggle();

		$('form[name="form-settings_ccs"]').submit(function() {
			return handleSettingsFormSubmit($(this));
		});
	};

	handleActiveToggle = function() {
		var visible = $('#field-ccsSettings-SecurityCCSActive').is(':checked');
		$('#field-ccsSettings-SecurityCCSStoredFor').closest('.form-group').toggle(visible);
		$('#secureAccess').toggle(visible);
	};

	handleGenerateCertificate = function(theForm) {
		var theModal = theForm.closest('.modal');
		var errors = [];

		if ($.trim(theForm.find('.field-dn-ccs_username').val()) == '') {
			errors.push({
				field: theForm.find('.field-dn-ccs_username'),
				message: trans.credit_card_storage.username_required
			});
		}

		var password = $.trim(theForm.find('.field-dn-ccs_password').val());
		var password2 = $.trim(theForm.find('.field-dn-ccs_password2').val());
		if (password == '') {
			errors.push({
				field: theForm.find('.field-dn-ccs_password'),
				message: trans.credit_card_storage.password_required
			});
		}

		if (password2 == '') {
			errors.push({
				field: theForm.find('.field-dn-ccs_password2'),
				message: trans.credit_card_storage.password2_required
			});
		} else if (password != '' && password != password2) {
			errors.push({
				field: theForm.find('.field-dn-ccs_password2'),
				message: trans.credit_card_storage.password2_must_match
			});
		} else if (pwdStrengthPoints(password) < 3) {
			errors.push({
				field: theForm.find('.field-dn-ccs_password'),
				message: trans.credit_card_storage.password_characters_required
			});
		}

		if (errors.length > 0) {
			AdminForm.displayValidationErrors(errors);
		} else {
			AdminForm.sendRequest(
				"admin.php?p=settings_ccs&mode=generate_certificate",
				theForm.serialize(), null,
				function(data) {
					if (data.status == 'Success')
					{
						$('#modal-generate-certificate').modal('hide');
						AdminForm.displayAlert(trans.credit_card_storage.message_certificate_generated, "success");
					}
					else if (data.status == 'Error')
					{
						alert(data.errors);
					}
					else
					{
						alert(trans.credit_card_storage.unexpected_error_occurred);
					}
				}
			);
		}
		return false;
	};

	handleSettingsFormSubmit = function(theForm) {
		var isEnabled = $('#field-ccsSettings-SecurityCCSActive').is(':checked');

		var errors = [];
		if (isEnabled && theForm.find('.field-dn-ccs_username:visible').length > 0) {
			var usernameEle = theForm.find('.field-dn-ccs_username');
			if ($.trim(usernameEle.val()) == '' || $.trim(usernameEle.val()).length < 8) {
				errors.push({
					field: usernameEle,
					message: trans.credit_card_storage.username_minimum_required
				});
			}
			var passwordEle = theForm.find('.field-dn-ccs_password');
			var password2Ele = theForm.find('.field-dn-ccs_password2');
			if ($.trim(passwordEle.val()) == '') {
				errors.push({
					field: passwordEle,
					message: trans.credit_card_storage.message_password_required
				});
			} else if (password2Ele.val() != passwordEle.val()) {
				errors.push({
					field: password2Ele,
					message: trans.credit_card_storage.message_password_match
				});
			} else if (pwdStrengthPoints(passwordEle.val()) < 3) {
				errors.push({
					field: passwordEle,
					message: trans.credit_card_storage.password_characters_required
				});
			}
		}

		if (errors.length > 0) {
			AdminForm.displayValidationErrors(errors)
		}

		return errors.length === 0;
	};

	/**
	 * Password strength
	 * @param password
	 * @return int
	 */
	pwdStrengthPoints = function(password) {
		var points   = 0;
		if (password.length >= 6) points++; // 1 point for length over 6
		if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) points++; // 1 lower & uppercase characters
		if (password.match(/\d+/)) points++; // 1 point for a number
		if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) points++; // 1 point for special character
		if (password.length > 12) points++; // 1 point for length over 12
		if (points > 4) points = 4;
		return points;
	};

	init();
}(jQuery));

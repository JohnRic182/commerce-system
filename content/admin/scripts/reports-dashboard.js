var ReportsDashboard =  (function($) {
	var init, reportRange, loadSummary, loadGraph;

	var plotMain = null;

	var firstRun = true;

	/**
	 * Init function
	 */
	init = function () {
		$(document).ready(function(){

			reportRange = $('#reports-dashboard-ranges').reportRanges({
				currentRangeType: 'month',
				callback: function(rangeType, start, end) {
					loadSummary(reportRange.rangeType, reportRange.dateFrom, reportRange.dateTo);
					loadGraph(reportRange.rangeType, reportRange.dateFrom, reportRange.dateTo);
				}
			});

			loadSummary(reportRange.rangeType, reportRange.dateFrom, reportRange.dateTo);
			loadGraph(reportRange.rangeType, reportRange.dateFrom, reportRange.dateTo);

			$(window).resize(function () {
				if(plotMain != null) plotMain.replot({resetAxes: false});
			});
		});
	};

	/**
	 * Load summaries
	 *
	 * @param rangeType
	 * @param dateFrom
	 * @param dateTo
	 */
	loadSummary = function(rangeType, dateFrom, dateTo) {
		var dateFromText = moment(dateFrom).format('YYYY-MM-DD');
		var dateToText = moment(dateTo).format('YYYY-MM-DD');

		$.ajax({
			url: 'admin.php?p=reports&mode=summary&summary_type=orders_this_period,orders_prev_period,revenue_this_period,revenue_prev_period,conversions_this_period,conversions_prev_period&from=' + dateFromText + '&to=' + dateToText,
			success: function(data) {
				$('#reports-dashboard-cards')
					.html('')
					.reportCard({
						name: 'revenue-total',
						title: 'Total Revenue',
						subTitle: '&nbsp;',
						append: true,
						animate: firstRun,
						type: 'currency',
						value: data.revenue_this_period,
						valueCompareTo: data.revenue_prev_period,
						valueCompareToText: 'Compared to $val prev. period',
						animateValue: true,
						valueIcon: true,
						buttonDetailsUrl: 'admin.php?p=report&report_type=dates&range_type=' + rangeType + '&from=' + dateFromText + '&to=' + dateToText
					})
					.reportCard({
						name: 'orders',
						title: 'Orders',
						subTitle: '&nbsp;',
						append: true,
						animate: firstRun,
						animateDelay: 100,
						type: 'numeric',
						value: data.orders_this_period,
						valueCompareTo: data.orders_prev_period,
						valueCompareToText: 'Compared to $val prev. period',
						animateValue: true,
						valueIcon: true,
						buttonDetailsUrl: 'admin.php?p=report&report_type=orders&range_type=' + rangeType + '&from=' + dateFromText + '&to=' + dateToText
					})
					.reportCard({
						name: 'revenue-per-order',
						title: 'Revenue per order',
						subTitle: '&nbsp;',
						append: true,
						animate: firstRun,
						animateDelay: 200,
						type: 'currency',
						value: data.orders_this_period > 0 ? data.revenue_this_period / data.orders_this_period : 0,
						valueCompareTo: data.orders_prev_period > 0 ? data.revenue_prev_period / data.orders_prev_period : 0,
						valueCompareToText: 'Compared to $val prev. period',
						animateValue: true,
						valueIcon: true
					})
					.reportCard({
						name: 'conversion',
						title: 'Conversion',
						subTitle: '&nbsp;',
						append: true,
						animate: firstRun,
						animateDelay: 300,
						type: 'percent',
						value: data.conversions_this_period,
						valueCompareTo: data.conversions_prev_period,
						valueCompareToText: 'Compared to $val prev. period',
						animateValue: true,
						animateValueStart: 0,
						valueIcon: true
					});
				firstRun = false;
			}
		})
	};

	/**
	 * Load graph function
	 *
	 * @param rangeType
	 * @param dateFrom
	 * @param dateTo
	 */
	loadGraph = function(rangeType, dateFrom, dateTo) {

		var mainGrapthTickInterval;
		var mainGraphBarMargin;
		var mainGraphBarWidth;
		var mainGraphMarkerSize;
		var gapDays;

		switch (rangeType) {
			case 'week' : {
				mainGrapthTickInterval = '1 days';
				mainGraphBarWidth = mainGraphBarMargin = 15;
				mainGraphMarkerSize = 7;
				gapDays = 0;
				break;
			}
			case 'quarter' : {
				mainGrapthTickInterval = '7 days';
				mainGraphBarWidth = mainGraphBarMargin = 6;
				mainGraphMarkerSize = 5;
				gapDays = 0;
				break;
			}
			case 'year' : {
				mainGrapthTickInterval = '1 months';
				mainGraphBarWidth = mainGraphBarMargin = 2;
				mainGraphMarkerSize = 2;
				gapDays = 0;
				break;
			}
			default:
			case 'month' : {
				mainGrapthTickInterval = '2 days';
				mainGraphBarWidth = mainGraphBarMargin = 15;
				mainGraphMarkerSize = 7;
				gapDays = 1;
				break;
			}
		}

		$.ajax({
			'url': 'admin.php?__ajax=1&p=report&report_type=dates&range_type=' + rangeType + '&from=' + moment(dateFrom).format('Y-MM-DD') + '&to=' + moment(dateTo).format('Y-MM-DD') + '&filter[order_status]=any&export=1&export_type=json&',
			success: function (data) {

				var graphTotalPerDay = [];
				var graphOrdersPerDay = [];

				if (data.data && data.data.days) {
					$('#reports-dashboard-graph-title').text(data.title);

					var reportRunningDate = moment(data.range.from);
					var reportEndDate = moment(data.range.to);
					var daysDiff = reportEndDate.diff(reportRunningDate, 'days');

					for (var i = 0; i <= daysDiff; i++) {
						var valueFound = false;
						var dateFormatted = reportRunningDate.format('Y-MM-DD');

						$.each(data.data.days, function (index, day) {

							var reportDate = moment(day['report_date']);

							if (reportRunningDate.isSame(reportDate)) {
								graphTotalPerDay.push([dateFormatted, parseFloat(day['orders_total_amount'])]);
								graphOrdersPerDay.push([dateFormatted, parseInt(day['orders_count'])]);

								valueFound = true;
							}
						});

						if (!valueFound) {
							graphTotalPerDay.push([dateFormatted, 0]);
							graphOrdersPerDay.push([dateFormatted, 0]);
						}

						reportRunningDate.add(1, 'd');
					}

					var rangeMin = moment(dateFrom).subtract(gapDays, 'day').format('Y-MM-DD');;
					var rangeMax = moment(dateTo).add(gapDays, 'day').format('Y-MM-DD');

					/**
					 * Main graph
					 */
					if (plotMain != null) plotMain.destroy();

					plotMain = $.jqplot('reports-dashboard-graph', [graphTotalPerDay, graphOrdersPerDay], {
						//title: data.title,
						animate: !$.jqplot.use_excanvas,
						legend: { show: true, placement: 'inside' },
						grid: {
							background: '#f9f9f9',
							drawBorder: true,
							shadow: false,
							gridLineColor: '#dddddd',
							gridLineWidth: 1,
							borderWidth: 0
						},
						series: [
							{
								label: 'Sales per day',
								renderer: $.jqplot.BarRenderer,
								rendererOptions: {
									barPadding: 1,
									barMargin: mainGraphBarMargin,
									barDirection: 'vertical',
									barWidth: mainGraphBarWidth,
									shadow: false,
									animation: {
										speed: 500
									}
								},
							},
							{
								label: 'Order per day',
								xaxis: 'x2axis',
								yaxis: 'y2axis',
								lineWidth: 1,
								rendererOptions: {
									smooth: true,
									animation: {
										speed: 500
									}
								},
								markerOptions: { style: 'filledCircle', size: mainGraphMarkerSize }
							}
						],
						seriesColors: ['#AEC690', '#0099cc'],
						axes: {
							xaxis: {
								renderer: $.jqplot.DateAxisRenderer,
								tickOptions: { formatString: '%b %#d, ’%y' },
								min: rangeMin,
								max: rangeMax,
								tickInterval: mainGrapthTickInterval
							},
							yaxis: { tickOptions: {prefix: '$'}, min: 0	},
							x2axis: {
								renderer: $.jqplot.DateAxisRenderer,
								tickOptions: { formatString: ' ' },
								min: rangeMin,
								max: rangeMax,
								tickInterval: mainGrapthTickInterval,
								drawMajorTickMarks: false
							},
							y2axis: {
								min: 0,
								tickOptions: { showGridline: false }
							}
						},

						cursor: { zoom: true, looseZoom: true }
					});
				}
			}
		});
	};


	init();

}(jQuery));
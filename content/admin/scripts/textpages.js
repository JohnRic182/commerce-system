$(document).ready(function(){

	/**
	 * Handle delete button
	 */
	$('[data-action="action-pages-delete"]').click(function(){

		var deleteMode = $('input[name="form-textpages-delete-mode"]:checked').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-textpage:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=page&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
			}
			else
			{
				AdminForm.displayModalAlert($('#modal-textpages-delete'), trans.textpages.select_delete_page, 'danger');
			}
		}
		else if (deleteMode == 'search')
		{
			window.location='admin.php?p=page&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
		}
	});

	/**
	 * Handle title & id changes
	 */
	function pagesUpdateSeoUrl() {
		if (PagesSeoUrlSettings != undefined) {

			var url = PagesSeoUrlSettings.template
				.replace('%PageName%', $('#field-title').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.trim()
				.replace(/\s+/g, PagesSeoUrlSettings.joiner);

			if (PagesSeoUrlSettings.lowercase) url = url.toLocaleLowerCase();
			$('#field-url_custom').attr('placeholder', url);
		}
	}

	$('#field-title')
		.keyup(function(){pagesUpdateSeoUrl();})
		.change(function(){pagesUpdateSeoUrl();});

	// handle publish / unpublish pages
	$('form[name=form-textpages-publishunpublish]').submit(function () {
		return handlePublishSubmit($(this));
	});

	showMetaCounter();
});

function publishAction(publishMode) {
	$('.hidden-input-mode').val(publishMode);
    if (publishMode == 'publish') {
		$('.pu-header-label').html(trans.textpages.publish_header_label);
		$('.radio-btn-label1').html(trans.textpages.publish_option_selected);
		$('.radio-btn-label2').html(trans.textpages.publish_option_all);
		$('.pu-btn-submit').html(trans.textpages.publish_btn_label);
	} else {
		$('.pu-header-label').html(trans.textpages.unpublish_header_label);
		$('.radio-btn-label1').html(trans.textpages.unpublish_option_selected);
		$('.radio-btn-label2').html(trans.textpages.unpublish_option_all);
		$('.pu-btn-submit').html(trans.textpages.unpublish_btn_label);
	}
}

function handlePublishSubmit(formElement) {
	var theModal = formElement.closest('.modal');
    var publishMode = $('input[name="form-publishunpublish-mode"]:checked').val();
	var publishAction = $('input[name="mode"]').val();
	var nonce = formElement.find('input[name=nonce]').val();
	if (publishMode == 'selected')
	{
		var ids = '';
		$('.checkbox-textpage:checked').each(function(){
			var id = $(this).val();
			ids += (ids == '' ? '' : ',') + id;
		});
		
		if (ids != '')
		{
			window.location='admin.php?p=page&mode=publish&publishMode=selected&publishAction='+publishAction+'&ids=' + ids + '&nonce=' +  nonce;
		}
		else
		{
			var flag = publishAction == 'publish' ? 'select_to_publish' : 'select_to_unpublish';
			AdminForm.displayModalAlert(theModal, trans.textpages[flag], 'danger');
		}
	}
	else if (publishMode == 'all')
	{
		window.location='admin.php?p=page&mode=publish&publishMode=all&publishAction=' + publishAction + '&nonce=' + nonce;
	}
	return false;
}

function removeTextPage(id, nonce) {
	AdminForm.confirm(trans.textpages.confirm_remove_page, function() {
		window.location = 'admin.php?p=page&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
	});

	return false;
}
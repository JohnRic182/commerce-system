$(document).ready(function(){
	$('#modal-recurring-profile-update-status .btn-primary').click(function(){
		AdminForm.displaySpinner(trans.recurring_profiles.message_updating_profile_status);
		$(this).attr('disabled', 'disabled');
	});

	$('#modal-recurring-profiles-bulk-update-status form').submit(function(){
		var form = this;
		var updateMode = $(form).find('input[name="updateMode"]:checked').val();
		if (updateMode == 'selected')
		{
			var ids = '';
			$('.checkbox-recurring-profile:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});

			if (ids != '')
			{
				$(form).find('input[name=ids]').val(ids);
			}
			else
			{
				alert(trans.recurring_profiles.select_update_recurring_profile);
				return false;
			}
		}

		return true;
	});
});
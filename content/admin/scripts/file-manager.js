var FileManager = (function() {
	var init, removeFile, checkFile;

	init = function($) {
		$('form[name=form-file-manager-upload]').submit(function() {
			var theForm = $(this);
			var filename = $('#field-new_file').val();

			if ($.trim(filename) == '')
			{
				//TODO: Error
				return false;
			}

			AdminForm.displaySpinner('Please wait');
			$.ajax({
				url: 'admin.php?p=filemanager&folder='+currentFolder+'&file='+filename+'&action=check-exists',
				method: 'GET',
				success: function(data) {
					var uploadFiles = function() {
						AdminForm.hideSpinner();
						$.ajax({
							url: theForm.attr('action'),
							method: 'POST',
							data: new FormData(theForm.get(0)),
							cache: false,
							contentType: false,
							processData: false,
							success: function() {
								document.location = 'admin.php?p=filemanager&folder='+currentFolder;
							}
						});
					};
					if (data)
					{
						AdminForm.confirm(trans.file_manager.confirm_replace_file, function() {
							uploadFiles();
						});
					}
					else
					{
						uploadFiles();
					}
				}
			});

			return false;
		});
	};

	removeFile = function(folder, file, nonce) {
		AdminForm.confirm(trans.file_manager.confirm_remove_file, function() {
			window.location = 'admin.php?p=filemanager&folder='+folder+'&file='+file+'&nonce='+nonce+'&action=delete';
		});
	};

	checkFile = function(){
		$("#field-new_file").change(function(){
			var fileSize = check_file_size(this.files[0].size);

			if (fileSize.status == 0){
				alert(fileSize.message);
				return;
			}
		});
	};

	return {
		'init': init,
		'removeFile': removeFile,
		'checkFile': checkFile
	};
}());

$(document).ready(function() {
	FileManager.init($);
	FileManager.checkFile();
});
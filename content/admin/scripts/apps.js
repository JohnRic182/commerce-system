var Apps = (function($) {
	var init, handleFilter, disableApp;

	init = function() {
		$('#category_filter').change(function() {
			handleFilter();
		});
		$('#price_filter').change(function() {
			handleFilter();
		});

		$('.app-disable').click(function() {
			return disableApp($(this));
		});

		$('.app-activate').click(function() {
			if ($.trim(activateFormUrl) != '') {
				AdminForm.sendRequest(
					activateFormUrl,
					null, null, function(data) {
						if (data.status == 1) {
							var theModal = $('#modal-activate');
							var theBody = theModal.find('.modal-body');
							theBody.html(data.html);
							theBody.find('.field-checkbox').each(function(idx, ele) {
								forms.initCheckbox($(ele));
							});
							theModal.modal('show');
						}
					},
					null, 'GET'
				);

				return false;
			} else {
				return true;
			}
		});

		$('form[name="form-app-activate"]').submit(function() {
			var theModal = $(this).closest('.modal');
			var theForm = $(this);

			AdminForm.sendRequest(
				activateFormUrl,
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						AdminForm.hideSpinner();
						theModal.modal('hide');

						document.location = 'admin.php?p=apps';
					} else {
						AdminForm.hideSpinner();

						if (data.errors !== undefined) {
							AdminForm.displayValidationErrors(data.errors);
						} else {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						}
					}
				}
			);

			return false;
		});


		$('form[name="form-app-subscribe"]').submit(function(){
			if (!$('#field-tc_agree').is(':checked')) {
				AdminForm.displayAlert('Please agree with our terms and conditions!', 'warning');
			} else {
				AdminForm.sendRequest(
					'admin.php?p=apps&action=subscribe',
					{
						app_key: $('input[name="app_key"]').val()
					},
					'Activating Subscription',
					function(data) {
						if (data && typeof data.status != 'undefined') {
							if (data.status == 1) {
								document.location = (typeof data.redirect_url != 'undefined') ? data.redirect_url : document.location;
							} else {
								if (typeof data.error != 'undefined') {
									AdminForm.displayAlert(data.error, 'danger');
								} else {
									AdminForm.displayAlert('Unknown error. Please try again.', 'danger');
								}
							}
						} else {
							AdminForm.displayAlert('Unknown error. Please try again.', 'danger');
						}
					},
					null, 'POST'
				);
			}

			return false;
		});

	};

	handleFilter = function() {
		var categoryFilter = $('#category_filter').val();
		var priceFilter = $('#price_filter').val();

		if (priceFilter === undefined) priceFilter = '';

		if ($.trim(priceFilter) == '') {
			document.location = 'admin.php?p=apps&category=' + categoryFilter;
		} else {
			document.location = 'admin.php?p=apps&action=app-center&category=' + categoryFilter + '&price=' + priceFilter;
		}
	};

	disableApp = function(theLink) {
		var appKey = theLink.attr('data-key');
		var title = theLink.attr('data-title');
		AdminForm.confirm(trans.apps.confirm_disable.replace('{title}', title), function() {
			document.location = 'admin.php?p=apps&action=deactivate&key=' + appKey;
		});
	};

	init();

	return {
		disableApp: disableApp
	};
}(jQuery));

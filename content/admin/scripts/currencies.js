var Currencies = (function($) {
	var init, editCurrency, removeCurrency,
		handleCurrencyForm, handleDeleteFormSubmit, handleUpdateRatesFormSubmit;

	init = function() {
		$(document).ready(function(){
			$('form[name="form-add-currency-default"]').submit(function() {
				return handleCurrencyForm($(this), 'modal-add-currency-default');
			});

			$('form[name="form-add-currency-custom"]').submit(function() {
				return handleCurrencyForm($(this),'modal-add-currency-custom');
			});

			$('.edit-currency').click(function() {
				return editCurrency($(this).attr('data-id'));
			});

			$('.remove-currency').click(function() {
				return removeCurrency($(this).attr('data-id'), $(this).attr('data-nonce'));
			});

			$(".default-currency-radio-button:checked").addClass('current-default');

			$('.default-currency-radio-button')
				.click(function(event){
					var id = $(this).attr('id');
					var default_id = $(".current-default").attr('id');
					event.preventDefault();

					if ( default_id !== id ){
						$(this).triggerHandler('change');
					}
				})
				.change(function(){
					var currency_id = $(this).val();
					AdminForm.confirm('Set this currency as default?', function() {
						$.post(
							"admin.php?p=currency",
							{
								mode: "set_default",
								id: currency_id
							},
							onAjaxSuccess,
							"json"
						);

						$(".current-default").removeClass('current-default');
						$('#id-default-currency-'+currency_id)
							.prop("checked", true)
							.addClass('current-default');
					});
					return false;
				});

			$('form[name=form-currencies-delete]').submit(function(){
				return handleDeleteFormSubmit($(this));
			});

			$('form[name=form-update-exchange-rates]').submit(function() {
				return handleUpdateRatesFormSubmit($(this));
			});
		});
	};

	handleUpdateRatesFormSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var nonce = theForm.find('input[name=nonce]');
		var deleteMode = $('input[name="form-update-exchange-rates-mode"]:checked').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-currency:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=currency&mode=update_exchange_rates&updateMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else
			{
				AdminForm.displayModalAlert(theModal, trans.currencies.no_currencies_selected_update, 'danger');
			}
		}
		else if (deleteMode == 'search')
		{
			window.location='admin.php?p=currency&mode=update_exchange_rates&updateMode=search' + '&nonce=' + nonce;
		}

		return false;
	};

	handleDeleteFormSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var deleteMode = $('input[name="form-currencies-delete-mode"]:checked').val();
		var nonce = theForm.find('input[name=nonce]').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-currency:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=currency&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else
			{
				AdminForm.displayModalAlert(theModal, trans.currencies.no_currencies_selected, 'danger');
			}
		}
		else if (deleteMode == 'search')
		{
			window.location='admin.php?p=currency&mode=delete&deleteMode=search' + '&nonce=' + nonce;
		}

		return false;
	};

	handleCurrencyForm = function(theForm, modal_name) {
		var theModal = $("#"+modal_name);
		var errors = [];

		AdminForm.sendRequest(
			theForm.attr('action'),
			theForm.serialize(),
			null,
			function(data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();
					theModal.modal('hide');
					document.location = 'admin.php?p=currencies';
				} else {
					AdminForm.hideSpinner();

					if (data.errors !== undefined) {
						$.each(data.errors, function(errKey, errMsg){
							errors.push({
								field: '.field-'+errKey,
								message: errMsg
							});
						});
						AdminForm.displayValidationErrors(errors);
					} else {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			},
			function() {
				AdminForm.hideSpinner();
			}
		);

		return false;
	};

	editCurrency = function(id) {
		AdminForm.sendRequest(
			'admin.php?p=currency&mode=update&id='+id,
			null,
			null,
			function(data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();

					$('#currency-edit-wrapper').html(data.html);

					var theModal = $('#modal-edit-currency');
					$(theModal.find('fieldset').get(0)).addClass('fieldset-primary');

					$('form[name="form-edit-currency"]').submit(function() {
						return handleCurrencyForm($(this),'modal-edit-currency');
					});

					forms.ensureTooltips();

					theModal.modal('show');
				}
			},
			null,
			'GET'
		);

		return false;
	};

	removeCurrency = function(id, nonce) {
		if ($('#id-default-currency-'+id+":checked").length   == 1) {
			var errors = [];
			errors.push({
				field: '#id-default-currency-'+id,
				message: trans.currencies.message_cannot_remove_currency
			});
		}
		else{
			AdminForm.confirm(trans.currencies.confirm_remove_currency, function() {
				window.location = 'admin.php?p=currency&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
			});
		}

		AdminForm.displayErrors(errors);
		return false;
	};

	onAjaxSuccess = function(data)
	{
		$.each(data, function(index,value) {
			$("#span-id-exchange-rate-" + value.currency_id).html(value.exchange_rate);
		});

		$("#modal-confirm").modal('hide');
	};

	init();
}(jQuery));


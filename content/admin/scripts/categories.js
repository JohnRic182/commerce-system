var Categories = (function($) {
	var init, handleFormSubmit, handleExportSubmit, handleDeleteSubmit,
		categoriesUpdateSeoUrl, removeCategory;

	init = function() {
		$(document).ready(function() {
			$('form[name=form-category]').submit(function() {
				return handleFormSubmit();
			});

			$('form[name=form-categories-export]').submit(function() {
				return handleExportSubmit($(this));
			});
			$('form[name=form-category-delete]').submit(function() {
				return handleDeleteSubmit($(this));
			});

			$('#field-name,#field-key_name').bind('keyup change', function() {
				categoriesUpdateSeoUrl();
			});

			$('.remove-category').click(function() {
				return removeCategory($(this));
			})

			showMetaCounter();

		});
	};

	handleFormSubmit = function() {
		var errors = [];
		if ($.trim($('#field-name').val()) == '') {
			errors.push({
				field: '#field-name',
				message: trans.categories.enter_title
			});
		}
		if ($.trim($('#field-key_name').val()) == '') {
			errors.push({
				field: '#field-key_name',
				message: trans.categories.enter_category_id
			});
		}

		AdminForm.displayValidationErrors(errors);

		return errors.length == 0;
	};

	handleExportSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var exportMode = $('input[name="form-categories-export-mode"]:checked').val();
		var nonce = $('#modal-categories-export').find('input[name="nonce"]').val();

		if (exportMode == 'selected')
		{
			var ids = '';
			$('.checkbox-category:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});

			if (ids != '')
			{
				window.location='admin.php?p=category&mode=export&exportMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else
			{
				AdminForm.displayModalAlert(theModal, trans.categories.no_categories_selected_export, 'danger');
			}
		}
		else if (exportMode == 'all')
		{
			window.location='admin.php?p=category&mode=export&exportMode=all&nonce=' + nonce;
		}

		return false;
	};

	handleDeleteSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var deleteMode = $('input[name="form-categories-delete-mode"]:checked').val();
		var nonce = $('#modal-categories-delete').find('input[name="nonce"]').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-category:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});

			if (ids != '')
			{
				window.location='admin.php?p=category&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else
			{
				AdminForm.displayModalAlert(theModal, trans.categories.no_categories_selected, 'danger');
			}
		}
		else if (deleteMode == 'all')
		{
			window.location='admin.php?p=category&mode=delete&deleteMode=all&nonce=' + nonce;
		}

		return false;
	};

	/**
	 * Handle title & id changes
	 */
	categoriesUpdateSeoUrl = function() {
		if (categoriesSeoUrlSettings != undefined) {
			var url = categoriesSeoUrlSettings.template
				.replace('%CategoryName%', $('#field-name').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.replace('%CategoryKey%', $('#field-key_name').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.replace('%CategoryDbId%', '[id]')
				.trim()
				.replace(/\s+/g, categoriesSeoUrlSettings.joiner);

			if (categoriesSeoUrlSettings.lowercase) url = url.toLocaleLowerCase();
			$('#field-url_custom').attr('placeholder', url);
		}
	};

	removeCategory = function(theLink) {
		var id = theLink.attr('data-id');
		var nonce = theLink.attr('data-nonce');
		AdminForm.confirm(trans.categories.confirm_remove, function() {
			window.location = 'admin.php?p=category&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	init();
}(jQuery));

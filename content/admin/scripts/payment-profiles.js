var PaymentProfiles = (function(){
	var init, handlePaymentProfilePersist, handlePaymentProfileDeleteClick, handlePaymentProfileAddClick, handlePaymentProfileEditClick,
		handlePaymentProfileChangeClick, handlePaymentProfileChangePersistClick, $;

	/**
	 * Init function
	 *
	 * @param jQuery
	 */
	init = function(jQuery) {
		$ = jQuery;

		$('button[data-action="action-payment-profile"]').click(function() {
			handlePaymentProfilePersist($(this));
			return false;
		});

		$('body').on('click', '.payment-profile-add', function() {
			handlePaymentProfileAddClick($(this));
			return false;
		});

		$('body').on('click', '.payment-profile-update', function() {
			handlePaymentProfileEditClick($(this));
			return false;
		});

		$('body').on('click', '.payment-profile-delete', function() {
			handlePaymentProfileDeleteClick($(this));
			return false;
		});

		$('body').on('click', '.payment-profile-change', function() {
			handlePaymentProfileChangeClick($(this));
			return false;
		});
	};

	/**
	 * Show payment profile edit form
	 *
	 * @param theButton
	 */
	handlePaymentProfileAddClick = function(theButton) {
		$(theButton).attr('disabled', 'disabled');

		AdminForm.sendRequest(
			'admin.php?p=user_payment_profiles&action=ajax_add_form',
			{
				userId: $(theButton).attr('data-user-id'),
				paymentMethodId: $(theButton).attr('data-payment-method-id'),
				actionSource: 'customer'
			},
			'Loading payment profile form',
			function(data) {
				$(theButton).removeAttr('disabled');

				if (data.status == 1) {
					var editModal = $('#modal-add-payment-profile');
					if (editModal.length > 0) {
						editModal.find('button[data-action="action-payment-profile"]').unbind('click');
					}

					$('#edit-payment-profile').html(data.html);
					var editModal = $('#modal-add-payment-profile');
					editModal.find('form').each(function(idx, ele) {
						$($(ele).find('fieldset').get(0)).addClass('fieldset-primary');
					});
					forms.initAddressForm(editModal.find('form'));
					forms.initCheckbox(editModal.find('.field-checkbox'));
					editModal.find('button[data-action="action-payment-profile"]').click(function() {
						handlePaymentProfilePersist($(this));
						return false;
					});

					editModal.modal('show');

					editModal.data('mode', 'add');
				}
			}
		);
	};

	/**
	 * Show payment profile edit form
	 *
	 * @param theButton
	 */
	handlePaymentProfileEditClick = function(theButton) {
		$(theButton).attr('disabled', 'disabled');

		var paymentProfile = theButton.closest('.payment-profile-view-container').find('.payment-profile-view');
		var id = $(paymentProfile).attr('id').replace('payment-profile-', '');

		AdminForm.sendRequest(
			'admin.php?p=user_payment_profiles&action=ajax_edit_form',
			{
				paymentProfileId: id
			},
			'Loading payment profile',
			function(data) {
				$(theButton).removeAttr('disabled');

				if (data.status == 1) {
					var editModal = $('#modal-edit-payment-profile');
					if (editModal.length > 0) {
						editModal.find('button[data-action="action-payment-profile"]').unbind('click');
					}

					$('#edit-payment-profile').html(data.html);
					var editModal = $('#modal-edit-payment-profile');
					editModal.find('form').each(function(idx, ele) {
						$($(ele).find('fieldset').get(0)).addClass('fieldset-primary');
					});
					forms.initAddressForm(editModal.find('form'));
					forms.initCheckbox(editModal.find('.field-checkbox'));
					editModal.find('button[data-action="action-payment-profile"]').click(function() {
						handlePaymentProfilePersist($(this));
						return false;
					});

					editModal.find('input,select,textarea').each(function() {
						if ($(this).attr('id') != 'field-payment_profile-is_primary') {
							$(this).change(function(){
								editModal.data('changed', true);
							});
						}
					});

					editModal.modal('show');

					editModal.data('mode', 'edit');
					editModal.data('changed', false);
				}
			}
		);
	};

	BrainTreeCardTokenization = function(_paymentMethodFormEle, theButton){
		var cur_nonce = $(_paymentMethodFormEle).find("input[name='payment_method_nonce']").val();
		if (cur_nonce == ''){
			braintreeclient = new braintree.api.Client({
				clientToken: bt_token
			});
			if (braintreeclient){
				var _number =  $(_paymentMethodFormEle).find("#field-payment_profile-cc_number").val();
				var _cardholderName =  $(_paymentMethodFormEle).find("#field-payment_profile-first_name").val()+' '+$(_paymentMethodFormEle).find("#field-payment_profile-last_name").val();
				var _expirationMonth =  $(_paymentMethodFormEle).find("#field-payment_profile-cc_expiration").val();
				var _expirationYear =  $(_paymentMethodFormEle).find("select[name='payment_profile[cc_expiration][year]']").val();

				var _errors = [];

				if (_number == '') {
					_errors.push({field:'#field-payment_profile-cc_number', message:'Please enter valid credit card number'});
				}

				if (_cardholderName == '') {
					_errors.push({field:'#field-payment_profile-first_name', message:'Please enter valid cardholder Name '});
				}

				if (_errors.length > 0)
				{
					var theModal = theButton.closest('.modal');
					$(_paymentMethodFormEle).find("input[name='payment_method_nonce']").val('');
					$(theButton).removeAttr('disabled');
					AdminForm.displayModalErrors(theModal, _errors);
					return false;
				}

				braintreeclient.tokenizeCard({
					number: _number,
					cardholderName: _cardholderName,
					expirationMonth: _expirationMonth,
					expirationYear: _expirationYear
				}, function (err, nonce) {
					if (err){
						alert(err);
						return false;
					}
					else{
						$(_paymentMethodFormEle).find("input[name='payment_method_nonce']").val(nonce);
						cur_nonce = $(_paymentMethodFormEle).find("input[name='payment_method_nonce']").val();
						_handlePaymentProfilePersist(theButton);
					}
				});
			}
			else{
				_errors.push({field:'', message:'Braintree API error. Please contact site administrator.'});
				AdminForm.displayModalErrors(theModal, _errors);
				return false;
			}
		}
	};

	/**
	 * Save edited payment profile
	 *
	 * @param theButton
	 */
	handlePaymentProfilePersist = function(theButton) {
		var theModal = theButton.closest('.modal');
		var theForm = theButton.closest('form');
		var paymentProfileId = $.trim($(theForm).find('input[name="paymentProfileId"]').val());
		var isNew = paymentProfileId == '0';

		if (theForm.find('#id_pm_nonce').length > 0 && theForm.find('#id_pm_nonce').val() == '')
		{
			var bttoken = BrainTreeCardTokenization(theForm, theButton);
		}
		else
		{
			_handlePaymentProfilePersist(theButton);
		}
	};

	_handlePaymentProfilePersist = function(theButton) {
		$(theButton).attr('disabled', 'disabled');

		var theModal = theButton.closest('.modal');
		var theForm = theButton.closest('form');
		var paymentProfileId = $.trim($(theForm).find('input[name="paymentProfileId"]').val());
		var isNew = paymentProfileId == '0';

		var url = 'admin.php?p=user_payment_profiles&action=ajax_persist';
		var data = $(theForm).serialize();
		if ($(theModal).data('mode') == 'edit' &&  !$(theModal).data('changed') && $('#field-payment_profile-is_primary').is(':checked')) {
			url = 'admin.php?p=user_payment_profiles&action=ajax_set_primary';
			data = {
				paymentProfileId: paymentProfileId
			};
		}
		//AdminForm.removeErrors();
		AdminForm.sendRequest(
			url,
			data,
			'Saving payment profile',
			function(data) {

				$(theButton).removeAttr('disabled');

				if (data.status == 1) {
					var existingProfileView = $('#payment-profile-' + data.paymentProfileId);
					if (existingProfileView.length > 0) {

						if ($('#field-payment_profile-is_primary').is(':checked'))
						{
							$('.payment-profile-primary-label').hide();
						}

						existingProfileView.replaceWith(data.html);
						existingProfileView = $('#payment-profile-' + data.paymentProfileId);

						// TODO: check where it is used
						existingProfileView.find('.payment-profile-delete').click(function() {
							handlePaymentProfileDeleteClick($(this));
							return false;
						});
						// TODO: check where it is used
						existingProfileView.find('.payment-profile-update').click(function() {
							handlePaymentProfileEditClick($(this));
							return false;
						});
					} else {
						if (isNew) {
							if ($('#field-payment_profile-is_primary').is(':checked')) {
								if ($('.payment-profile-primary-label').length) {
									$('.payment-profile-primary-label').hide();
								}
							}
							$('#payment-profiles-body').append(
								'<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 payment-profile-view-container">' +
								data.html +
								'<div style="margin-top: 10px;">' +
								'<a class="btn payment-profile-update" data-role="button" href="#">Edit</a> ' +
								'<a class="btn payment-profile-delete" data-role="button" href="#">Delete</a>' +
								'</div>' +
								'</div>'
							);
						} else {
							$('#payment-profiles-body').append(data.html);
						}
					}

					AdminForm.displayAlert(trans.common.success);
					theModal.modal('hide');
				} else {
					var _errors = [];

					if (data.message != undefined && data.message && data.message != '') {
						_errors.push({field:'foo', message:data.message});
					}

					$.each(data.errors, function(index, error){
						_errors.push({field:'#' + error.field, message:error.message});
					});
					AdminForm.displayValidationErrors(_errors);
				}
			}
		);
	};

	/**
	 * Display payment profile change dialog
	 *
	 * @param theButton
	 */
	handlePaymentProfileChangeClick = function(theButton) {
		$(theButton).attr('disabled', 'disabled');

		var paymentProfile = theButton.closest('.payment-profile-view-container').find('.payment-profile-view');
		var userId = theButton.attr('data-user-id');
		var recurringProfileId = theButton.attr('data-recurring-profile-id');
		var currentPaymentProfileId = $(paymentProfile).attr('id').replace('payment-profile-', '');

		AdminForm.sendRequest(
			'admin.php?p=user_payment_profiles&action=ajax_recurring_change_list',
			{
				userId: userId,
				recurringProfileId: recurringProfileId,
				currentPaymentProfileId: currentPaymentProfileId
			},
			'Loading payment profiles',
			function(data) {
				$(theButton).removeAttr('disabled');

				if (data.status == 1) {

					var changeModal = $('#modal-change-payment-profile');
					if (changeModal.length > 0) changeModal.find('.payment-profile-view').unbind('click');

					$('#change-payment-profile').html(data.html);
					$('#modal-change-payment-profile #payment-profile-' + currentPaymentProfileId).addClass('payment-profile-selected');

					$('#modal-change-payment-profile .payment-profile-view').click(function(){
						$('#modal-change-payment-profile .payment-profile-view').removeClass('payment-profile-selected');
						$(this).addClass('payment-profile-selected');
					});

					var changeModal = $('#modal-change-payment-profile');
					changeModal.find('button[data-action="action-change-payment-profile"]').click(function(){
						var theButton = this;
						$(theButton).attr('disabled', 'disabled');
						handlePaymentProfileChangePersistClick(changeModal, theButton, userId, recurringProfileId, currentPaymentProfileId);
						return false;
					});

					changeModal.modal('show');
				}
			}
		);
	};

	/**
	 * Handle payment profile change save button
	 *
	 * @param theModal
	 * @param theButton
	 * @param userId
	 * @param recurringProfileId
	 */
	handlePaymentProfileChangePersistClick = function(theModal, theButton, userId, recurringProfileId, currentPaymentProfileId) {
		$(theButton).attr('disabled', 'disabled');

		var newPaymentProfileId = $('#modal-change-payment-profile .payment-profile-selected').attr('id').replace('payment-profile-', '');
		AdminForm.sendRequest(
			'admin.php?p=user_payment_profiles&action=ajax_recurring_change_persist',
			{
				userId: userId,
				recurringProfileId: recurringProfileId,
				paymentProfileId: newPaymentProfileId
			},
			'Changing payment profile',
			function(data) {
				AdminForm.hideSpinner();
				$(theButton).removeAttr('disabled');

				if (data.status == 1) {
					$('.recurring-billing-payment-profile .payment-profile-view').replaceWith(data.html);
					AdminForm.displayAlert(trans.common.success);
					theModal.modal('hide');

				} else {
					var _errors = [];

					if (data.message != undefined && data.message && data.message != '') {
						_errors.push({field:'foo', message:data.message});
					}

					$.each(data.errors, function(index, error){
						_errors.push({field:'#' + error.field, message:error.message});
					});
					AdminForm.displayModalErrors(theModal, _errors);
				}
			}
		);
	};

	/**
	 * Handle payment profile delete
	 *
	 * @param theButton
	 */
	handlePaymentProfileDeleteClick = function(theButton) {
		var paymentProfile = theButton.closest('.payment-profile-view-container').find('.payment-profile-view');
		var id = $(paymentProfile).attr('id').replace('payment-profile-', '');

		AdminForm.confirm(trans.payment_profiles.confirm_remove_payment_profile, function() {
			AdminForm.sendRequest(
				'admin.php?p=user_payment_profiles&action=ajax_remove',
				{
					paymentProfileId: id
				},
				'Removing payment profile',
				function(data) {
					if (data.status == 1) {
						paymentProfile.closest('.payment-profile-view-container').remove();
						$('#payment-profile-'+data.primaryPaymentProfileId).find('.payment-profile-primary-label').removeAttr('style');
						AdminForm.displayAlert(trans.common.success);
					} else {
						AdminForm.displayAlert(data.message, 'danger');
					}
				}
			);
		});
	};

	return {
		init: init
	};
}());

$(document).ready(function() {
	PaymentProfiles.init($);
});
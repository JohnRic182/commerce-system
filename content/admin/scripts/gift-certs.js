var GiftCertificates = (function($){
	var init, removeGiftCert;

	init = function() {
		$('form[name="form-settings"]').submit(function() {
			var theForm = $(this);
			var theModal = $(this).closest('.modal');

			AdminForm.displaySpinner();

			AdminForm.sendRequest(
				theForm.attr('action'),
				theForm.serialize(),
				null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=gift_cert';
					} else {
						AdminForm.hideSpinner();
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				},
				function() {
					AdminForm.hideSpinner();
				}
			);

			return false;
		});

		$('.form-gift-certs-delete').submit(function(){
			return false;
		});

		$('form[name=form-gift-cert]').submit(function() {
			var mode = $(this).find('input[name=mode]').val();

			var errors = [];
			if ($.trim($('#field-first_name').val()) == '') {
				errors.push({
					field: '#field-first_name',
					message: trans.gift_certs.enter_first_name
				});
			}
			if ($.trim($('#field-last_name').val()) == '') {
				errors.push({
					field: '#field-last_name',
					message: trans.gift_certs.enter_last_name
				});
			}
			if ($.trim($('#field-email').val()) == '') {
				errors.push({
					field: '#field-email',
					message: trans.gift_certs.enter_email
				});
			}
			if ($.trim($('#field-voucher').val()) == '') {
				errors.push({
					field: '#field-voucher',
					message: trans.gift_certs.enter_voucher_number
				});
			}
			if (mode == 'add' && $.trim($('#field-gift_amount').val()) == '') {
				errors.push({
					field: '#field-gift_amount',
					message: trans.gift_certs.enter_amount
				});
			}
			if (mode == 'update') {
				var balanceText = $.trim($('#field-balance').val());
				var balance = parseFloat(balanceText);
				if ($.trim(balanceText) == '' || balance < 0) {
					errors.push({
						field: '#field-balance',
						message: trans.gift_certs.enter_positive_balance
					});
				}
			}

			AdminForm.displayValidationErrors(errors);

			return errors.length == 0;
		});

		/**
		 * Handle delete button
		 */
		$('[data-action="action-gift-cert-delete"]').click(function(){
			var deleteMode = $('input[name="form-gift-cert-delete-mode"]:checked').val();
			var nonce = $('#modal-gift-cert-delete').find('input[name="nonce"]').val();

			if (deleteMode == 'selected')
			{
				var ids = '';
				$('.checkbox-gift-cert:checked').each(function(){
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				if (ids != '')
				{
					window.location='admin.php?p=gift_cert&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
				}
				else
				{
					alert(trans.gift_certs.select_gift_certificate);
				}
			}
			else if (deleteMode == 'search')
			{
				window.location='admin.php?p=gift_cert&mode=delete&deleteMode=search&nonce=' + nonce;
			}
		});
	};

	removeGiftCert = function(id, nonce) {
		AdminForm.confirm(trans.gift_certs.confirm_remove_gift_certificate, function() {
			window.location = 'admin.php?p=gift_cert&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	$(document).ready(function() {
		init();
	});

	return {
		removeGiftCert: removeGiftCert
	}
}(jQuery));

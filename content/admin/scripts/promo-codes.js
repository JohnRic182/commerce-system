$(document).ready(function(){
	$('form[name=form-settings]').submit(function() {
		var theForm = $(this);
		var theModal = theForm.closest('.modal');

		AdminForm.sendRequest(
			theForm.attr('action'),
			theForm.serialize(), null,
			function(data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();
					theModal.modal('hide');
					AdminForm.clearAlerts();
					AdminForm.displayAlert(data.message);
				} else {
					if (data.errors != undefined) {
						AdminForm.displayModalErrors(theModal, data.errors);
					} else if (data.message != undefined) {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			}
		);

		return false;
	});

	$('form[name=form-promo-code]').submit(function() {
		var errors = [];
		if ($.trim($('#field-name').val()) == '') {
			errors.push({
				field: '#field-name',
				message: trans.promo_codes.enter_name
			});
		}
		
		if ($.trim($('#field-promo_code').val()) == '') {
			errors.push({
				field: '#field-promo_code',
				message: trans.promo_codes.enter_promo_code
			});
		}
		
		if ($.trim($('#field-discount').val()) == '') {
			errors.push({
				field: '#field-discount',
				message: trans.promo_codes.enter_discount
			});
		} else if ($.trim($('#field-discount').val()) != '' && $.trim($('#field-discount').val()) <= 0) {
			errors.push({
				field: '#field-discount',
				message: trans.promo_codes.discount_numeric_required
			});
		}
		
		if ($.trim($('#field-min_amount').val()) == '') {
			errors.push({
				field: '#field-min_amount',
				message: trans.promo_codes.enter_minimum_order_subtotal
			});
		} else if ($.trim($('#field-min_amount').val()) != '' && $.trim($('#field-min_amount').val()) < 0) {
			errors.push({
				field: '#field-min_amount',
				message: trans.promo_codes.minimum_order_subtotal_greater
			});
		}

		if ($.trim($('#field-start_date').val()) == '') {
			errors.push({
				field: '#field-start_date',
				message: trans.promo_codes.field_start_date
			});
		}

		if ($.trim($('#field-end_date').val()) == '') {
			errors.push({
				field: '#field-end_date',
				message: trans.promo_codes.field_end_date
			});
		}
	
		AdminForm.displayValidationErrors(errors);
		return errors.length == 0;
	});
	
	/** 
	 * Handle delete button
	 */
	$('[data-action="action-promo-codes-delete"]').click(function(){

		var deleteMode = $('input[name="form-promo-codes-delete-mode"]:checked').val();
		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-promo-code:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=promotions&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
			}
			else
			{
				alert(trans.promo_codes.select_promo_code);
			}
		}
		else if (deleteMode == 'search')
		{
			window.location='admin.php?p=promotions&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
		}
	});
	
	
	
	
	// Manage product group and categories for promo codes
	$("#field-categories").change(function(){
		UpdateAvailableProducts();
	});

	$('#field-categories').change();
        UpdateAssignedProductsList();

        $('#btnAddProduct').click(function(){
        	
			$("#btnAddProduct").attr("disabled", "disabled");
			$("#btnRemoveProduct").attr("disabled", "disabled");
			var promo_id = $("#promo_id").val();
			var selectedProducts = [];
			$('#field-products :selected').each(function(i, selectedOption){
				selectedProducts[i] = $(selectedOption).val();
			});

			var selectedPromoCategories = [];
			$('#field-promo_categories- :selected').each(function(i, selectedOption){
				selectedPromoCategories[i] = $(selectedOption).val();
			});
	
			$.getJSON("admin.php?p=promotions&mode=assignProducts&promo_id=" + escape(promo_id) + '&products=' + escape(selectedProducts) + '&promo_categories=' + escape(selectedPromoCategories), 
				function(data){
					if (data.result)
					{
						UpdateAssignedProductsList();
						UpdateAvailableProducts();
					}
					$("#btnAddProduct").removeAttr("disabled");
					$("#btnRemoveProduct").removeAttr("disabled");
				});
			
			return false;
        });

        $('#btnRemoveProduct').click(function(){
			$("#btnAddProduct").attr("disabled", "disabled");
			$("#btnRemoveProduct").attr("disabled", "disabled");		
			var promo_id = $("#promo_id").val();
			var selectedProducts = [];
			$('#field-selectedProducts :selected').each(function(i, selectedOption){
				selectedProducts[i] = $(selectedOption).val();
			});

			var selectedPromoCategories = [];
			$('#field-promo_categories- :selected').each(function(i, selectedOption){
				selectedPromoCategories[i] = $(selectedOption).val();
			});

			$.getJSON("admin.php?p=promotions&mode=unassignProducts&promo_id=" + escape(promo_id) + '&products=' + escape(selectedProducts) + '&promo_categories=' + escape(selectedPromoCategories), 
			function(data){
				if (data.result)
				{
					UpdateAssignedProductsList();
					UpdateAvailableProducts();
				}
				$("#btnAddProduct").removeAttr("disabled");
				$("#btnRemoveProduct").removeAttr("disabled");
			});
			return false;		
        });
              
	function UpdateAvailableProducts()
	{
		var promo_id = $("#promo_id").val();
		var selectedCategories = [];
		$('#field-categories :selected').each(function(i, selectedOption){
			selectedCategories[i] = $(selectedOption).val();
		});
		$.getJSON("admin.php?p=promotions&promo_id="+promo_id+"&mode=getProducts&categories=" + escape(selectedCategories), function(data){
			$("#field-products").html("");
			$(data.products).each(function(i, product){
				var o = $("<option></option>").val(product.pid).html(product.title);
				$("#field-products").append(o);
			});                        
		});
		return false;
	}

	function UpdateAssignedProductsList()
	{
		var promo_id = $("#promo_id").val();
		$.getJSON("admin.php?p=promotions&mode=getAssignedProducts&promo_id=" + escape(promo_id), function(data){
			$("#field-selectedProducts").html("");
			$(data.products).each(function(i, product){
				var o = $("<option></option>").val(product.pid).html(product.title);
				$("#field-selectedProducts").append(o);
			});
		});
	}

	checkPromoType();	
	$("#field-promo_type").change(function () {
		checkPromoType();
	});

});


function checkPromoType() {
	if ($("#field-promo_type").val() == "Product") {
		$("#field-min_amount_type").closest('.form-group').show();
		$("#products-body").parent().show();
		$("#selected_products-body").parent().show();
		$("#field-selectedProducts").parent().show();
		$("#buttons-body").parent().show();
		$("#categories-body").parent().show();
	}
	else {
		$("#field-min_amount_type").closest('.form-group').hide();
		$("#products-body").parent().hide();
		$("#selected_products-body").parent().hide();
		$("#field-selectedProducts").parent().hide();
		$("#buttons-body").parent().hide();
		$("#categories-body").parent().hide();
	}
}

function removePromoCode(id, nonce) {
	AdminForm.confirm(trans.promo_codes.confirm_remove_promo_code, function() {
		window.location = 'admin.php?p=promotions&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
	});

	return false;
}
var PaymentMethods = (function($) {
	var init, editSettings, disablePaymentMethod, activatePaymentMethod, initBTConnectButton;

	init = function() {
		$(document).ready(function() {
			if ($.trim(bt_connectUrl) != '' && !btConnected)
			{
				initBTConnectButton();
			}
			$('#field-payment_method_id').change(function() {
				AdminForm.removeErrors();
				var paymentMethodId = $.trim($('#field-payment_method_id').val());
				if (paymentMethodId != '') {
					AdminForm.sendRequest(
						'admin.php?p=payment_method&action=activate-form&payment_method_id=' + paymentMethodId, null, null,
						function(data) {
							if (data.status == 1) {
								var paymentMethodForm = $('#payment-method-form');
								paymentMethodForm.html(data.html);
								forms.ensureTooltips();
								$(paymentMethodForm.find('.form-control').get(0)).focus();
							} else {
								var errors = [];
								errors.push({field:'#field-payment_method_id', 'message': data.message});
								AdminForm.displayModalErrors($('#modal-add-payment').modal(), errors);
							}
						},
						null, 'GET'
					);
				} else {
					$('#payment-method-form').html('');
				}
			});

			$('form[name=form-add-custom-payment],form[name=form-update-custom-payment]').submit(function() {
				var theModal = $(this).closest('.modal');
				var title = $.trim($(this).find('.field-title').val());

				if (title == '') {
					var errors = [];
					errors.push({
						field: '.field-title',
						message: trans.payment_methods.title_required
					});
					AdminForm.displayModalErrors(theModal, errors);

					return false;
				} else {
					AdminForm.sendRequest(
						$(this).attr('action'),
						$(this).serialize(),
						'Please wait',
						function(data) {
							if (data.status) {
								document.location = 'admin.php?p=payment_methods';
							} else {
								var errors = [];
								$.each(data.errors, function(key, value) {
									$.each(value, function(idx, message) {
										errors.push({
											field: '.field-'+key,
											message: message
										});
									});
								});

								AdminForm.displayModalErrors(theModal, errors);
							}
						}
					);
				}

				return false;
			});

			$('#modal-update-custom-payment').on('show.bs.modal', function() {
				$(this).find('.has-error').removeClass('has-error');
			}).on('shown.bs.modal', function() {
				$(this).find('input,select,textarea').get(0).focus();
			});

			$('.edit-payment-method').click(function() {
				var theLink = $(this);
				//console.log('click:  '+theLink.attr('data-id')+' '+theLink.attr('data-method-type'));
				editSettings(theLink.attr('data-id'), theLink.attr('data-method-type'));
				return false;
			});

			$('.disable-payment-method').click(function() {
				var theLink = $(this);
				disablePaymentMethod(theLink.attr('data-id'), theLink.attr('data-nonce'));
				return false;
			});

			$('.activate-payment-method').click(function() {
				var theLink = $(this);
				activatePaymentMethod(theLink.attr('data-id'), theLink.attr('data-nonce'));
				return false;
			});

			$('#modal-add-payment').on('show.bs.modal', function() {
				$('#field-payment_method_id').closest('.form-group').show();
				$('#payment-method-form').html('');
				$('input[name=fromQS]').val($('#quick-start-wrapper').length > 0 ? '1' : '0');
			});

			$('form[name=form-add-payment]').submit(function() {
				var paymentMethodId = $('#field-payment_method_id').val();
				var errors = [];
				if (paymentMethodId == '') {
					errors.push({field:'#field-payment_method_id', 'message': trans.payment_methods.select_payment_method});
					AdminForm.displayModalErrors($('#modal-add-payment').modal(), errors);
					return false;
				} else if (paymentMethodId == 'custom') {
					var theModal = $(this).closest('.modal');
					var title = $.trim($(this).find('.field-title').val());

					if (title == '') {
						errors.push({
							field: '.field-title',
							message: trans.payment_methods.title_required
						});
						AdminForm.displayModalErrors(theModal, errors);
					} else {
						AdminForm.sendRequest(
							'admin.php?p=payment_method&action=update-custom',
							$(this).serialize(),
							'Please wait',
							function(data) {
								if (data.status) {
									document.location = 'admin.php?p=payment_methods';
								} else {
									var errors = [];
									$.each(data.errors, function(key, value) {
										$.each(value, function(idx, message) {
											errors.push({
												field: '.field-'+key,
												message: message
											});
										});
									});

									AdminForm.displayModalErrors(theModal, errors);
								}
							}
						);
					}
					return false;
				}
				return true;
			});
		});
	};

	editSettings = function(paymentMethodId, type) {
		if (type == 'custom') {
			AdminForm.sendRequest(
				'admin.php?p=payment_method&action=update-custom&payment_method_id=' + paymentMethodId,
				null, null,
				function(data) {
					if (data.status == 1) {
						var theModal = $('#modal-update-custom-payment');
						if ($.trim(data.html) == '') {
							theModal.modal('hide');
						} else {
							theModal.find('input[name=payment_method_id]').val(paymentMethodId);
							theModal.find('.modal-body').html(data.html);
							theModal.modal('show');
						}
					}
				},
				null, 'GET'
			);
		} else {
			//console.log(' go to location  '+'admin.php?p=payment_method&payment_method_id=' + paymentMethodId +  ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : ''));
			document.location = 'admin.php?p=payment_method&payment_method_id=' + paymentMethodId +  ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : '');
		}

		return false;
	};

	activatePaymentMethod = function(paymentMethodId) {
		var paymentMethodIdEle = $('#field-payment_method_id');
		paymentMethodIdEle.val(paymentMethodId).change();

		$('#modal-add-payment').modal('show');

		paymentMethodIdEle.closest('.form-group').hide();

		return false;
	};

	disablePaymentMethod = function(paymentMethodId, nonce) {
		AdminForm.confirm(trans.payment_methods.confirm_remove_payment, function() {
			AdminForm.sendRequest(
				'admin.php?p=payment_method&action=disable&payment_method_id=' + paymentMethodId+'&nonce=' + nonce,
				{},
				'Please wait',
				function() {
					document.location = 'admin.php?p=payment_methods' + ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : '');
				}
			);
		});

		return false;
	};
	initBTConnectButton = function()
	{
		var partner = new BraintreeOAuthConnect({
			connectUrl: bt_connectUrl,
			container: 'modal-bt-activate',
			//environment: 'sandbox',
			onError: function (errorObject) {
				//console.warn(errorObject.message);
			}
		});
	}

	init();

	return {
		editSettings: editSettings,
		disablePaymentMethod: disablePaymentMethod
	};
}(jQuery));
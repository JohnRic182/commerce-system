var QuickStartGuide = (function($) {
	var init, showQuickStartGuide, showContent, showConfirmation, skipConfirmation, showFinalConfirmation, showRecommendations, progressBar, stepOne, stepTwo, stepThree, stepFour, handleImageLogo, roundNavPillDone;

	init = function() {
		$(document).ready(function () {

			// Let's initialize some of our form elements
			forms.initAddressForm($('.quick-start-guide-modal').find('form'));
			forms.ensureTooltips();

			$(".auto-select").focus(function() {
				$(this).select();
			});

			var initTab, initNavigation, initIndex, initTotal;

			$('.field-image input[type="file"]').on('change', handleImageLogo);

			$('#quick-start-guide-form .field-checkbox').each(function(idx, ele) {
				forms.initCheckbox(ele);
			});

			$('.copy-to-clipboard').on('click', function() {
				AdminForm.copyToClipboard($('.copy-target input')[0]);
				alert("Sitemap URL has been copied to clipboard");
				return false;
			});

			var methodType = 'flat';
			$('#quick-start-guide-form .method-type').click(function() {
				methodType = $('#quick-start-guide-form .method-type:checked').val();

				if (methodType != 'flat') {
					$('#flat-rate-method-form').hide();
					$('#real-time-methods-form').show();

					var countryId = $('#quick-start-guide-form #add-method-country option:selected').val();

					AdminForm.sendRequest(
						'admin.php?p=quick_start_guide&mode=real-time-methods-form&coid=' + countryId + '&__ajax=1',
						null,
						'Please wait while we are fetching your data',
						function(response) {
							var theForm = $('#real-time-methods-form').html(response.html);
							$('input[name=method\\[carrier_id\\]]').on('change', function() {
								var carrierId = $('input[name=method\\[carrier_id\\]]:checked').val();
								theForm.find('.add-method-ups').toggle(carrierId == 'ups');
								theForm.find('.add-method-usps').toggle(carrierId == 'usps');
								theForm.find('.add-method-fedex').toggle(carrierId == 'fedex');
								theForm.find('.add-method-canada_post').toggle(carrierId == 'canada_post');
								theForm.find('.add-method-vparcel').toggle(carrierId == 'vparcel');
							});

							if (response.hasRealTime) {
								$('#quick-start-guide-form #save-shipping-carriers').show();
							} else {
								$('#quick-start-guide-form #save-shipping-carriers').hide();
							}
						},
						null,
						'GET'
					);
				} else {
					$('#real-time-methods-form').hide();
					$('#flat-rate-method-form').show();

					$('#quick-start-guide-form #save-shipping-carriers').show();
				}
			});

			$('#quick-start-guide-wizard').bootstrapWizard({
				previousSelector: '.wizard .previous',
				nextSelector: '.wizard .next',
				finishSelector: '.wizard .finish',
				onInit: function (tab, navigation, index) {
					initTab = tab;
					initNavigation = navigation;
					initIndex = index;
					initTotal = navigation.find('li').length;
				},
				onTabClick: function(tab, navigation, index, clickedIndex) {
					var navLi = $(navigation).find('li');

					if ($(navLi[clickedIndex]).hasClass('quick-start-guide-round-nav-pills-done')) {
						roundNavPillDone(false, clickedIndex);

						if (clickedIndex == 0) {
							$('#quick-start-guide-wizard .pager.wizard .go-back, #quick-start-guide-wizard .pager.wizard .finish').parent().addClass('hidden');
						}

						showContent(clickedIndex);

						return true;
					} else {
						return false;
					}
				},
				onPrevious: function (tab, navigation, index) {
					$('#quick-start-guide-wizard .tab-confirmation .content').empty();
					if (index == 0) {
						$('#quick-start-guide-wizard .pager.wizard .go-back').parent().addClass('hidden');
					}

					var current = index + 1;

					if(current >= initTotal) {
						$('#quick-start-guide-wizard').find('.pager .finish').show();
						$('#quick-start-guide-wizard').find('.pager .finish').parent().removeClass('hidden');
						$('#quick-start-guide-wizard').find('.pager .finish').removeClass('disabled');
					} else {
						$('#quick-start-guide-wizard').find('.pager .finish').hide();
						$('#quick-start-guide-wizard').find('.pager .finish').parent().addClass('hidden');
						$('#quick-start-guide-wizard').find('.pager .finish').addClass('disabled');
					}

					roundNavPillDone(false, index);
				},
				onNext: function (tab, navigation, index) {
					$('#quick-start-guide-wizard .tab-confirmation .content').empty();

					roundNavPillDone();

					switch (index) {
						case 1:
						case '1':
							stepOne(tab, navigation, index);
							break;
						case 2:
						case '2':
							stepTwo(tab, navigation, index);
							break;
						case 3:
						case '3':
							stepThree(tab, navigation, index);
							break;
						case 4:
						case '4':
							stepFour(tab, navigation, index);
							break;
						default:
							break;
					}

					return false;
				}
			});

			$('#quick-start-guide-wizard .get-started').on('click', function() {
				$('#quick-start-guide-wizard .tab-introduction').addClass('hidden');
				$('#quick-start-guide-wizard .footer-introduction').addClass('hidden');

				$('#quick-start-guide-wizard .tab-content').removeClass('hidden');
				$('#quick-start-guide-wizard .footer-content').removeClass('hidden');
			});

			$('#quick-start-guide-wizard .pager .later').on('click', function() {
				var index = parseInt($('#quick-start-guide-wizard .navbar').find('li.active a').attr('data-step'));
				var current = index + 1;

				if (index > 0)
				{
					$('#quick-start-guide-wizard .next').removeAttr('disabled').removeClass('disabled');
					$('#quick-start-guide-wizard .pager.wizard .go-back').removeClass('hidden');
				} else {
					$('#quick-start-guide-wizard .next').removeAttr('disabled').addClass('disabled');
					$('#quick-start-guide-wizard .pager.wizard .go-back').addClass('hidden');
				}

				if (index < initTotal) {
					$('#quick-start-guide-wizard').bootstrapWizard('show', index);
					$('#quick-start-guide-wizard .pager.wizard .go-back').css({'display': 'block'});
					if ($('#quick-start-guide-wizard .pager.wizard .go-back').parent().hasClass('hidden') || $('#quick-start-guide-wizard .pager.wizard .go-back').parent().is(':hidden')) {
						$('#quick-start-guide-wizard .pager.wizard .go-back').parent().removeClass('hidden');
					}
				} else {
					$('.quick-start-guide-modal').modal('hide');
				}

				if(current >= initTotal) {
					$('#quick-start-guide-wizard').find('.pager .finish').show();
					$('#quick-start-guide-wizard').find('.pager .finish').parent().removeClass('hidden');
					$('#quick-start-guide-wizard').find('.pager .finish').removeClass('disabled');
				} else {
					$('#quick-start-guide-wizard').find('.pager .finish').hide();
					$('#quick-start-guide-wizard').find('.pager .finish').parent().addClass('hidden');
					$('#quick-start-guide-wizard').find('.pager .finish').addClass('disabled');
				}

				roundNavPillDone();
			});

			$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-recommendations .go-back').on('click', function() {
				$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').attr({'disabled': 'disabled'}).addClass('disabled');

				showContent();
				roundNavPillDone();
			});

			$('.tab-select .field-payment_method_id').on('change', function() {
				var paymentMethodId = $('option:selected', this).val();
				var paymentMethodActive = $('option:selected', this).attr('data-active');
				var paymentMethod = $('option:selected', this).attr('data-payment-method');

				AdminForm.sendRequest(
					'admin.php?p=quick_start_guide&mode=fetch-payment-method-form',
					{
						'payment_method_id': paymentMethodId
					},
					'Please wait while we are fetching your data',
					function (response) {
						if (response.status == 1) {
							if (paymentMethodActive == true || paymentMethodActive == 'true') {
								$('#payment-connect-disconnect-btn').html('<span>Disconnect</span> with ' + paymentMethod);
							}

							if (paymentMethodActive == false || paymentMethodActive == 'false') {
								$('#payment-connect-disconnect-btn').html('<span>Connect</span> with ' + paymentMethod);
							}

							$('#payment-form .dynamic-content').html(response.html);
							forms.ensureTooltips();
						} else {
							console.log('Error');
						}
					},
					null,
					'POST',
					'json'
				);
			});

			$('#save-payment').on('click', function() {
				var paymentMethodId = $('#payment-gateway #field-payment_method_id option:selected').val();
				var selectedPaymentMethod = $('#payment-gateway #field-payment_method_id option:selected');

				var active = selectedPaymentMethod.attr('data-active');
				var paymentMethod = selectedPaymentMethod.attr('data-payment-method');
				var data = $('#payment-gateway input, #payment-gateway textarea, #payment-gateway select').serialize() + '&payment_method_id=' + paymentMethodId;

				if (typeof $('#payment-gateway').find('.field-title').val() != 'undefined' && !$.trim($('#payment-gateway').find('.field-title').val())) {
					var errors = [];
					errors.push({
						field: '.field-title',
						message: trans.payment_methods.title_required
					});
					AdminForm.displayValidationErrors(errors);
					return false;
				} else {
					AdminForm.sendRequest(
						'admin.php?p=quick_start_guide&mode=activate-payment',
						{
							'data': data
						},
						'Please wait while we are saving your data',
						function (response) {
							console.log(response);
							if (response.status == 1) {
								if (response.active == true) {
									selectedPaymentMethod.attr('data-active', response.active);
									selectedPaymentMethod.html(paymentMethod + ' (active)');
									$('#payment-connect-disconnect-btn span').html('Disconnect');
								} else {
									selectedPaymentMethod.attr('data-active', response.active);
									selectedPaymentMethod.html(paymentMethod);
									$('#payment-connect-disconnect-btn span').html('Connect');
								}

								$('.alert-modal').removeClass('hidden').html('Success!');

								setTimeout(function () {
									$('.alert-modal').not('.alert-danger').addClass('hidden');
								}, 3000);
							} else {
								console.log('Error');
							}
						},
						null,
						'GET',
						'json'
					);
				}
			});

			$('input[name=method\\[carrier_id\\]]').on('change', function() {
				var carrierId = $('input[name=method\\[carrier_id\\]]:checked').val();

				$('#real-time-methods-form').find('.add-method-ups').toggle(carrierId == 'ups');
				$('#real-time-methods-form').find('.add-method-usps').toggle(carrierId == 'usps');
				$('#real-time-methods-form').find('.add-method-fedex').toggle(carrierId == 'fedex');
				$('#real-time-methods-form').find('.add-method-canada_post').toggle(carrierId == 'canada_post');
				$('#real-time-methods-form').find('.add-method-vparcel').toggle(carrierId == 'vparcel');
			});

			$('#quick-start-guide-form #add-method-country').change(function() {
				var countryId = $('option:selected', this).val();

				AdminForm.sendRequest(
					'admin.php?p=quick_start_guide&mode=real-time-methods-form&coid=' + countryId + '&__ajax=1',
					null,
					'Please wait while we are fetching your data',
					function(response) {
						var theForm = $('#real-time-methods-form').html(response.html);
						$('input[name=method\\[carrier_id\\]]').on('change', function() {
							var carrierId = $('input[name=method\\[carrier_id\\]]:checked').val();
							theForm.find('.add-method-ups').toggle(carrierId == 'ups');
							theForm.find('.add-method-usps').toggle(carrierId == 'usps');
							theForm.find('.add-method-fedex').toggle(carrierId == 'fedex');
							theForm.find('.add-method-canada_post').toggle(carrierId == 'canada_post');
							theForm.find('.add-method-vparcel').toggle(carrierId == 'vparcel');
						});

						if (response.hasRealTime) {
							$('#quick-start-guide-form #save-shipping-carriers').show();
						} else {
							$('#quick-start-guide-form #save-shipping-carriers').hide();
						}
					},
					null,
					'GET'
				);
			});

			$('#save-shipping-carriers').on('click', function() {
				methodType = $('#quick-start-guide-form .method-type:checked').val();
				if (methodType == 'flat' || $('#quick-start-guide-form #flat-rate-method-form').is(':visible')) {
					var data = $('#quick-start-guide-form .shipping-country input, #quick-start-guide-form .shipping-country textarea, #quick-start-guide-form .shipping-country select, #quick-start-guide-form #flat-rate-method-form input, #quick-start-guide-form #flat-rate-method-form textarea, #quick-start-guide-form #flat-rate-method-form select').serialize();

					if (!$.trim($('#quick-start-guide-form #flat-rate-method-form').find('.flat-rate-method-name').val())) {
						var errors = [];
						errors.push({
							field: '.flat-rate-method-name',
							message: 'Title is required'
						});

						AdminForm.displayValidationErrors(errors);

						return false;
					} else {
						AdminForm.sendRequest(
							'admin.php?p=quick_start_guide&mode=add-flat-rate-method',
							data,
							'Please wait while we are saving your data',
							function (response) {
								if (response.status == 1) {
									$('.alert-modal').removeClass('hidden').html('Success!');

									setTimeout(function () {
										$('.alert-modal').not('.alert-danger').addClass('hidden');
									}, 3000);
								}
							}
						);
					}
				}

				if (methodType == 'real-time' || $('#quick-start-guide-form #real-time-methods-form').is(':visible')) {
					var carrierId = $('#quick-start-guide-form input[name=method\\[carrier_id\\]]:checked').val();

					if (carrierId == 'ups' || $('#quick-start-guide-form .add-method-ups').is(':visible')) {
						var data = $('#quick-start-guide-form .shipping-country input, #quick-start-guide-form .shipping-country textarea, #quick-start-guide-form .shipping-country select, #quick-start-guide-form #add-method-carrier input, #quick-start-guide-form #add-method-carrier textarea, #quick-start-guide-form #add-method-carrier select, #quick-start-guide-form .add-method-ups input, #quick-start-guide-form .add-method-ups textarea, #quick-start-guide-form .add-method-ups select, #quick-start-guide-form #add-based-methods input, #quick-start-guide-form #add-based-methods textarea, #quick-start-guide-form #add-based-methods select').serialize();
					}

					if (carrierId == 'usps' || $('#quick-start-guide-form .add-method-usps').is(':visible')) {
						var data = $('#quick-start-guide-form .shipping-country input, #quick-start-guide-form .shipping-country textarea, #quick-start-guide-form .shipping-country select, #quick-start-guide-form #add-method-carrier input, #quick-start-guide-form #add-method-carrier textarea, #quick-start-guide-form #add-method-carrier select, #quick-start-guide-form .add-method-usps input, #quick-start-guide-form .add-method-usps textarea, #quick-start-guide-form .add-method-usps select, #quick-start-guide-form #add-based-methods input, #quick-start-guide-form #add-based-methods textarea, #quick-start-guide-form #add-based-methods select').serialize();
					}

					if (carrierId == 'fedex' || $('#quick-start-guide-form .add-method-fedex').is(':visible')) {
						var data = $('#quick-start-guide-form .shipping-country input, #quick-start-guide-form .shipping-country textarea, #quick-start-guide-form .shipping-country select, #quick-start-guide-form #add-method-carrier input, #quick-start-guide-form #add-method-carrier textarea, #quick-start-guide-form #add-method-carrier select, #quick-start-guide-form .add-method-fedex input, #quick-start-guide-form .add-method-fedex textarea, #quick-start-guide-form .add-method-fedex select, #quick-start-guide-form #add-based-methods input, #quick-start-guide-form #add-based-methods textarea, #quick-start-guide-form #add-based-methods select').serialize();
					}

					if (carrierId == 'canada_post' || $('#quick-start-guide-form .add-method-canada_post').is(':visible')) {
						var data = $('#quick-start-guide-form .shipping-country input, #quick-start-guide-form .shipping-country textarea, #quick-start-guide-form .shipping-country select, #quick-start-guide-form #add-method-carrier input, #quick-start-guide-form #add-method-carrier textarea, #quick-start-guide-form #add-method-carrier select, #quick-start-guide-form .add-method-canada_post input, #quick-start-guide-form .add-method-canada_post textarea, #quick-start-guide-form .add-method-canada_post select, #quick-start-guide-form #add-based-methods input, #quick-start-guide-form #add-based-methods textarea, #quick-start-guide-form #add-based-methods select').serialize();
					}

					if (carrierId == 'vparcel' || $('#quick-start-guide-form .add-method-vparcel').is(':visible')) {
						var data = $('#quick-start-guide-form .shipping-country input, #quick-start-guide-form .shipping-country textarea, #quick-start-guide-form .shipping-country select, #quick-start-guide-form #add-method-carrier input, #quick-start-guide-form #add-method-carrier textarea, #quick-start-guide-form #add-method-carrier select, #quick-start-guide-form .add-method-vparcel input, #quick-start-guide-form .add-method-vparcel textarea, #quick-start-guide-form .add-method-vparcel select, #quick-start-guide-form #add-based-methods input, #quick-start-guide-form #add-based-methods textarea, #quick-start-guide-form #add-based-methods select').serialize();
					}

					AdminForm.sendRequest(
						'admin.php?p=quick_start_guide&mode=add-real-time-methods',
						data,
						'Please wait while we are saving your data',
						function (response) {
							if (response.status == 1) {
								$('.alert-modal').removeClass('hidden').html('Success!');

								setTimeout(function () {
									$('.alert-modal').not('.alert-danger').addClass('hidden');
								}, 3000);
							}
						}
					);
				}
			});

			$('#save-taxes').on('click', function() {
				var taxes = {};
				taxes['rate_description'] = $.trim($('#quick-start-guide-form .step-four #taxes #field-rate_description').val());
				taxes['class_id'] = $.trim($('#quick-start-guide-form .step-four #taxes #field-class_id').val());
				taxes['coid'] = $.trim($('#quick-start-guide-form .step-four #taxes #field-coid').val());
				taxes['stid'] = $.trim($('#quick-start-guide-form .step-four #taxes #field-stid').val());
				taxes['tax_rate'] = $.trim($('#quick-start-guide-form .step-four #taxes #field-tax_rate').val());

				AdminForm.displaySpinner('Saving your data');

				AdminForm.sendRequest(
					'admin.php?p=quick_start_guide&mode=save-taxes',
					{
						'taxes': taxes
					},
					'Please wait while we are saving your data',
					function (response) {
						if (response.status == 1) {
							$('.alert-modal').removeClass('hidden').html('Success!');

							setTimeout(function () {
								$('.alert-modal').addClass('hidden');
							}, 3000);
						} else {
							var errors = [];
							$.each(response.errors, function(key, error) {
								errors.push({
									field: '.field-'+key,
									message: error
								});
							});
							var formFieldsWithErrors = AdminForm.displayValidationErrors(errors);

							if (formFieldsWithErrors.length > 0)
							{
								$('.form-group.field-checkbox,.form-group.field-text,.form-group.field-money,.form-group.field-select,.form-group.field-integer,.form-group.field-numeric,.form-group.field-template')
									.not('td .form-group.field-checkbox')
									.css({
										'min-height': '95px',
										'margin-bottom': '0px'
									});
							}
						}
					},
					null,
					'POST',
					'json'
				);

				return false;
			});

			$('.quick-start-guide-modal').on('show.bs.modal', function (e) {
				if (typeof countriesStates !== 'undefined') {
					$('.form-address').each(function(formIndex, form) {
						forms.initAddressForm(form);
					});
				}

				$('#quick-start-guide-form .company-logo').removeClass('hidden');
				$('#quick-start-guide-form .field-image-preview').html('<canvas></canvas>');

				roundNavPillDone();
			});

			$('#quick-start-guide-wizard .quick-start-guide-close a').on('click', function() {
				$('.quick-start-guide-modal').modal('hide');
			});
		});
	};

	/**
	 * This will handle the logo when user add image logo: change the canvas to the newly added image and the button save will appear
	 * @param e
	 */
	handleImageLogo = function(e) {
		var self = this;
		if (self.files != undefined && self.files[0]) {
			var file = self.files[0];
			if (!file || file.name == undefined || file.size == undefined) return;

			var fileSize = check_file_size(file.size);
			if (fileSize.status == 0) {
				alert(fileSize.message);
				e.stopImmediatePropagation();
				return;
			}

			var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';

			if ('|jpg|png|jpeg|gif|'.indexOf(type) === -1) {
				alert('Please choose image file!');
				e.stopImmediatePropagation();
				return;
			}

			var parent = $(self).parent();
			var canvas = $(self).parent().find('canvas');
			var image = $(self).parent().find('img');

			var imageSave = $('<div class="field-image-save" style="width:50%"><a href="#" class="btn btn-primary" data-action="field-image-save" data-role="button" data-target="">Save</a></div>');
			if (parent.find('a[data-action="field-image-save"]').length == 0) {
				imageSave.appendTo(parent.find('.field-image-preview'));
			}

			$('#quick-start-guide-form .company-logo').addClass('hidden');

			$(imageSave).click(function () {
				var file_data = $("#field-qsg_logo").prop("files")[0];
				var form_data = new FormData();
				form_data.append("logo", file_data);

				var url = 'admin.php?p=quick_start_guide&mode=save-image-logo&__ajax=1';
				AdminForm.displaySpinner('Uploading image logo');
				$.ajax({
					url : url,
					type : 'POST',
					data : form_data,
					processData: false,
					contentType: false,
					success : function(data) {
						AdminForm.hideSpinner();
					}
				});
				return false;
			});

			var reader = new FileReader();
			reader.onload = function (e) {
				var img = new Image();
				img.onload = function () {
					var width = this.width / (this.height / 100);
					var height = 100; // always 100 pixels
					canvas.attr({width: width, height: height});
					canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
					$(canvas).addClass('img-responsive').css({'height': 'auto' }).show();
				};
				img.src = e.target.result;
			};
			reader.readAsDataURL(file);
		}
		e.stopImmediatePropagation();
	}

	/**
	 * Call this function if you want to show the QSG modal
	 */
	showQuickStartGuide = function () {
		$('.quick-start-guide-modal').modal('show');
	}

	/**
	 * Show the tab content of QSG wizard by passing a value or index
	 * @param index
	 */
	showContent = function (index) {
		$('#quick-start-guide-wizard .tab-content, #quick-start-guide-wizard .footer-content').removeClass('hidden');
		$('#quick-start-guide-wizard .tab-confirmation, #quick-start-guide-wizard .tab-recommendations, #quick-start-guide-wizard .footer-confirmation, #quick-start-guide-wizard .footer-recommendations').addClass('hidden');

		$('#quick-start-guide-wizard .footer-confirmation > div')
			.removeClass('col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10')
			.addClass('col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6');

		$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').removeClass('hidden').addClass('disabled').attr({'disabled': 'disabled'});
		$('#quick-start-guide-wizard .footer-confirmation .close-qsg, #quick-start-guide-wizard .footer-confirmation .continue-recommendations').attr({'disabled': 'disabled'}).addClass('hidden disabled');

		if (index >= 0) {
			$('#quick-start-guide-wizard').bootstrapWizard('show', index);
			if ($('#quick-start-guide-wizard .finish').hasClass('hidden')) {
				$('#quick-start-guide-wizard .finish').closest('div').addClass('hidden');
			}
		}

		if (parseInt($('#quick-start-guide-wizard .navbar').find('li.active a').attr('data-step')) == 1) {
			$('#quick-start-guide-wizard').find('.pager .previous').hide();
		}
	}

	/**
	 * Show the content for confirmation after the each step when user click "Next Step"
	 * @param tab
	 * @param navigation
	 * @param index
	 * @param content
	 */
	showConfirmation = function (tab, navigation, index, content) {
		var total = navigation.find('li').length;
		var current = index+1;

		$('#quick-start-guide-wizard .tab-confirmation .content').html(content);

		$('#quick-start-guide-wizard .tab-content, #quick-start-guide-wizard .footer-content').addClass('hidden');
		$('#quick-start-guide-wizard .tab-confirmation, #quick-start-guide-wizard .footer-confirmation').removeClass('hidden');

		$('#quick-start-guide-wizard .footer-confirmation .proceed').on('click', function() {
			$('#quick-start-guide-wizard .tab-content, #quick-start-guide-wizard .footer-content').removeClass('hidden');
			$('#quick-start-guide-wizard .tab-confirmation, #quick-start-guide-wizard .footer-confirmation').addClass('hidden');
			$('#quick-start-guide-wizard').bootstrapWizard('show', index);

			$('#quick-start-guide-wizard .pager.wizard .go-back').css({'display': 'block'});
			if ($('#quick-start-guide-wizard .pager.wizard .go-back').parent().hasClass('hidden') || $('#quick-start-guide-wizard .pager.wizard .go-back').parent().is(':hidden')) {
				$('#quick-start-guide-wizard .pager.wizard .go-back').parent().removeClass('hidden');
			}

			$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').attr({'disabled': 'disabled'}).addClass('disabled');

			if(current >= total) {
				$('#quick-start-guide-wizard').find('.pager .finish').show();
				$('#quick-start-guide-wizard').find('.pager .finish').parent().removeClass('hidden');
				$('#quick-start-guide-wizard').find('.pager .finish').removeClass('disabled');
			} else {
				$('#quick-start-guide-wizard').find('.pager .finish').hide();
				$('#quick-start-guide-wizard').find('.pager .finish').parent().addClass('hidden');
				$('#quick-start-guide-wizard').find('.pager .finish').addClass('disabled');
			}

			roundNavPillDone();
		});
	}

	skipConfirmation = function (navigation, index) {
		var total = navigation.find('li').length;
		var current = index+1;

		$('#quick-start-guide-wizard').bootstrapWizard('show', index);

		$('#quick-start-guide-wizard .pager.wizard .go-back').css({'display': 'block'});
		if ($('#quick-start-guide-wizard .pager.wizard .go-back').parent().hasClass('hidden') || $('#quick-start-guide-wizard .pager.wizard .go-back').parent().is(':hidden')) {
			$('#quick-start-guide-wizard .pager.wizard .go-back').parent().removeClass('hidden');
		}

		if(current >= total) {
			$('#quick-start-guide-wizard').find('.pager .finish').show();
			$('#quick-start-guide-wizard').find('.pager .finish').parent().removeClass('hidden');
			$('#quick-start-guide-wizard').find('.pager .finish').removeClass('disabled');
		} else {
			$('#quick-start-guide-wizard').find('.pager .finish').hide();
			$('#quick-start-guide-wizard').find('.pager .finish').parent().addClass('hidden');
			$('#quick-start-guide-wizard').find('.pager .finish').addClass('disabled');
		}

		roundNavPillDone();
	}

	/**
	 * Show the content for final confirmation when user click's "Finish"
	 * @param tab
	 * @param navigation
	 * @param index
	 * @param content
	 */
	showFinalConfirmation = function (tab, navigation, index, content) {
		$('#quick-start-guide-wizard .tab-confirmation .content').html(content);

		$('#quick-start-guide-wizard .tab-content, #quick-start-guide-wizard .footer-content').addClass('hidden');
		$('#quick-start-guide-wizard .tab-confirmation, #quick-start-guide-wizard .footer-confirmation').removeClass('hidden');

		$('#quick-start-guide-wizard .footer-confirmation .close-qsg').on('click', function() {
			$('.quick-start-guide-modal').modal('hide');
			showContent(0);
		});

		$('#quick-start-guide-wizard .footer-confirmation .continue-recommendations').on('click', function() {
			showRecommendations();
		});
	}

	/**
	 * Show the content for recommendations when user click's "Continue to recommendations"
	 */
	showRecommendations = function() {
		$('#quick-start-guide-wizard .tab-confirmation, #quick-start-guide-wizard .footer-confirmation').addClass('hidden');
		$('#quick-start-guide-wizard .tab-recommendations, #quick-start-guide-wizard .footer-recommendations').removeClass('hidden');

		$('#quick-start-guide-wizard .no-thanks').on('click', function() {
			$('.quick-start-guide-modal').modal('hide');
			showContent(0);
		});

		$('#quick-start-guide-wizard .recommendations-finish').on('click', function() {
			$('.quick-start-guide-modal').modal('hide');
			showContent(0);
		});

		roundNavPillDone(true);
	}

	/**
	 * Show the progress bar whenever the user finish each steps this will be show at the confirmation content, and the percentage will vary how many steps have you done
	 * @param timeout
	 * @param interval
	 * @param percentage
	 */
	progressBar = function (timeout, interval, percentage) {
		setTimeout(function () {
			$('.progress-bar').each(function() {
				var progressBar = $(this);
				var valuemax = progressBar.attr("aria-valuemax");
				var currentPercentage = percentage;

				var progress = setInterval(function() {
					if (currentPercentage >= valuemax) {
						clearInterval(progress);

						$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').removeAttr('disabled').removeClass('disabled');
					} else {
						currentPercentage +=1;
						progressBar.css('width', (currentPercentage)+'%');
					}

					progressBar.text((currentPercentage)+'%');

				}, interval);
			});
		}, timeout);
	}

	/**
	 * Show the Step One content on QSG wizard
	 * @param tab
	 * @param navigation
	 * @param index
	 * @returns {boolean}
	 */
	stepOne = function (tab, navigation, index) {
		var stepOneData = $('#quick-start-guide-form .step-one input, #quick-start-guide-form .step-one textarea, #quick-start-guide-form .step-one select').serialize();

		AdminForm.sendRequest(
			'admin.php?p=quick_start_guide&mode=save-step-one',
			{
				'data': stepOneData
			},
			'Please wait while we are saving your data',
			function (response) {
				if (response.status == 1) {
					var content  = '<div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">';
						content += response.content;
						content += '</div>';

					// showConfirmation(tab, navigation, index, content);
					skipConfirmation(navigation, index);
					$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').removeAttr('disabled').removeClass('disabled');

					$('#quick-start-guide-wizard .next').removeAttr('disabled').removeClass('disabled');
					$('#quick-start-guide-wizard .pager.wizard .go-back').removeClass('hidden');
				} else {
					var errors = [];
					$.each(response.errors, function(key, error) {
						errors.push({
							field: '.field-'+key,
							message: error
						});
					});
					var formFieldsWithErrors = AdminForm.displayValidationErrors(errors);

					if (formFieldsWithErrors.length > 0)
					{
						$('.step-one .form-group.field-checkbox, .step-one .form-group.field-text, .step-one .form-group.field-money, .step-one .form-group.field-select, .step-one .form-group.field-integer, .step-one .form-group.field-numeric, .step-one .form-group.field-template')
						   .not('td .form-group.field-checkbox')
						   .css({
								'min-height': '95px',
								'margin-bottom': '0px'
						   });
					}

					$('#quick-start-guide-wizard .next').removeAttr('disabled').removeClass('disabled');
				}
			},
			null,
			'POST',
			'json'
		);

		return false;
	}

	/**
	 * Show the Step Two content on QSG wizard
	 * @param tab
	 * @param navigation
	 * @param index
	 * @returns {boolean}
	 */
	stepTwo = function (tab, navigation, index) {
		var stepTwoData = $('#quick-start-guide-form .step-two input, #quick-start-guide-form .step-two textarea, #quick-start-guide-form .step-two select').serialize();

		AdminForm.sendRequest(
			'admin.php?p=quick_start_guide&mode=save-step-two',
			{
				'data': stepTwoData
			},
			'Please wait while we are saving your data',
			function (response) {
				if (response.status == 1) {
					var content  = '<div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">';
						content += response.content;
						content += '</div>';

					// showConfirmation(tab, navigation, index, content);
					skipConfirmation(navigation, index);
					$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').removeAttr('disabled').removeClass('disabled');

					progressBar(200, 25, 25);
				} else {
					var errors = [];
					$.each(response.errors, function(key, error) {
						errors.push({
							field: '.field-'+key,
							message: error
						});
					});
					var formFieldsWithErrors = AdminForm.displayValidationErrors(errors);

					if (formFieldsWithErrors.length > 0)
					{
						$('.step-two .form-group.field-checkbox, .step-two .form-group.field-text, .step-two .form-group.field-money, .step-two .form-group.field-select, .step-two .form-group.field-integer, .step-two .form-group.field-numeric, .step-two .form-group.field-template')
						   .not('td .form-group.field-checkbox')
						   .css({
								'min-height': '95px',
								'margin-bottom': '0px'
						   });
					}
				}
			},
			null,
			'POST',
			'json'
		);

		return false;
	}

	/**
	 * Show the Step Three content on QSG wizard
	 * @param tab
	 * @param navigation
	 * @param index
	 * @returns {boolean}
	 */
	stepThree = function (tab, navigation, index) {
		var stepThreeData = $('#quick-start-guide-form .step-three input, #quick-start-guide-form .step-three textarea, #quick-start-guide-form .step-three select').serialize();

		AdminForm.sendRequest(
			'admin.php?p=quick_start_guide&mode=save-step-three',
			{
				'data': stepThreeData
			},
			'Please wait while we are saving your data',
			function (response) {
				if (response.status == 1) {
					var content  = '<div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">';
						content += response.content;
						content += '</div>';

					// showConfirmation(tab, navigation, index, content);
					skipConfirmation(navigation, index);
					$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').removeAttr('disabled').removeClass('disabled');
				} else {
					var errors = [];
					$.each(response.errors, function(key, error) {
						errors.push({
							field: '.field-'+key,
							message: error
						});
					});
					var formFieldsWithErrors = AdminForm.displayValidationErrors(errors);

					if (formFieldsWithErrors.length > 0)
					{
						$('.step-three .form-group.field-checkbox, .step-three .form-group.field-text, .step-three .form-group.field-money, .step-three .form-group.field-select, .step-three .form-group.field-integer, .step-three .form-group.field-numeric, .step-three .form-group.field-template')
						   .not('td .form-group.field-checkbox')
						   .css({
								'min-height': '95px',
								'margin-bottom': '0px'
						   });
					}
				}
			},
			null,
			'POST',
			'json'
		);

		return false;
	}

	/**
	 * Show the Step Four content on QSG wizard
	 * @param tab
	 * @param navigation
	 * @param index
	 * @returns {boolean}
	 */
	stepFour = function (tab, navigation, index) {
		var content  = '<div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">';
			content +=      '<img src="images/admin/quick-start-guide/cheers-beer.png?v={$APP_VERSION}" class="img-center img-responsive">';
			content +=      '<h3>Your store is now set up!</h3>';
			content +=      '<p>On the following page, we have some recommended tasks; take a look!</p>';
			content += '</div>';

		$('#quick-start-guide-wizard .footer-confirmation > div')
			.removeClass('col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6')
			.addClass('col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10');

		$('#quick-start-guide-wizard .footer-confirmation .go-back, #quick-start-guide-wizard .footer-confirmation .proceed').addClass('hidden');
		$('#quick-start-guide-wizard .footer-confirmation .close-qsg, #quick-start-guide-wizard .footer-confirmation .continue-recommendations').removeAttr('disabled').removeClass('hidden disabled');

		showFinalConfirmation(tab, navigation, index, content);

		return false;
	}

	/**
	 * This function handles the changing of color and cursor at the top round nav pill of QSG Wizard
	 * @param final
	 * @param currentIndex
	 */
	roundNavPillDone = function (final, currentIndex) {
		var roundNavPills = $('#quick-start-guide-wizard .modal-header .nav-pills li');
		$('#quick-start-guide-wizard .modal-header .nav-pills li').removeClass('quick-start-guide-round-nav-pills-done');
		$('#quick-start-guide-wizard .modal-header .nav-pills li a').addClass('cursor-default');

		if (final == true) {
			$(roundNavPills).addClass('quick-start-guide-round-nav-pills-done');
			$(roundNavPills).find('a').removeClass('cursor-default');
		} else {
			$.each(roundNavPills, function(key, data) {
				if ($(data).hasClass('active')) {
					if (currentIndex == key) {
						$(roundNavPills[currentIndex]).removeClass('quick-start-guide-round-nav-pills-done');
						$(roundNavPills[currentIndex]).find('a').addClass('cursor-default');
					}
					return false;
				} else {
					if (currentIndex == key) {
						$(roundNavPills[currentIndex]).removeClass('quick-start-guide-round-nav-pills-done');
						$(roundNavPills[currentIndex]).find('a').addClass('cursor-default');
						return false;
					} else {
						$(data).addClass('quick-start-guide-round-nav-pills-done');
						$(data).find('a').removeClass('cursor-default');
					}
				}
			});
		}
	}

	init();
}(jQuery));

var ReportsSales =  (function($) {
	var init, reportRange, loadSummary;

	var loadedCount = 0;

	var firstRun = true;

	/**
	 * Init function
	 */
	init = function () {
		$(document).ready(function(){

			AdminForm.displaySpinner('Gathering intelligence');

			reportRange = $('#reports-dashboard-ranges').reportRanges({
				currentRangeType: 'month',
				callback: function(rangeType, start, end) {
					loadedCount = 0;
					AdminForm.displaySpinner('Gathering intelligence');
					loadSummary(reportRange.rangeType, reportRange.dateFrom, reportRange.dateTo);
				}
			});

			loadSummary(reportRange.rangeType, reportRange.dateFrom, reportRange.dateTo);

			$(window).resize(function () {
				if(plotMain != null) plotMain.replot({resetAxes: false});
			});
		});
	};

	/**
	 * Load summaries
	 *
	 * @param rangeType
	 * @param dateFrom
	 * @param dateTo
	 */
	loadSummary = function(rangeType, dateFrom, dateTo) {
		var dateFromText = moment(dateFrom).format('YYYY-MM-DD');
		var dateToText = moment(dateTo).format('YYYY-MM-DD');

		$.ajax({
			url: 'admin.php?p=reports&mode=summary&summary_type=orders_this_period,orders_prev_period,revenue_this_period,revenue_prev_period,conversions_this_period,conversions_prev_period&from=' + dateFromText + '&to=' + dateToText,
			success: function(data) {
				$('#reports-dashboard-cards')
					.html('')
					.reportCard({
						name: 'revenue-total',
						title: 'Total Revenue',
						subTitle: '&nbsp;',
						append: firstRun,
						animate: false,
						type: 'currency',
						value: data.revenue_this_period,
						valueCompareTo: data.revenue_prev_period,
						valueCompareToText: 'Compared to $val prev. period',
						valueCompareToInverse: true,
						animateValue: true,
						valueIcon: true,
						buttonDetailsUrl: 'admin.php?p=report&report_type=dates&range_type=' + rangeType + '&from=' + dateFromText + '&to=' + dateToText
					})
					.reportCard({
						name: 'orders',
						title: 'Orders',
						subTitle: '&nbsp;',
						append: true,
						animate: firstRun,
						animateDelay: 100,
						type: 'numeric',
						value: data.orders_this_period,
						valueCompareTo: data.orders_prev_period,
						valueCompareToText: 'Compared to $val prev. period',
						animateValue: true,
						valueIcon: true,
						buttonDetailsUrl: 'admin.php?p=report&report_type=orders&range_type=' + rangeType + '&from=' + dateFromText + '&to=' + dateToText
					})
					.reportCard({
						name: 'revenue-per-order',
						title: 'Revenue per order',
						subTitle: '&nbsp;',
						append: true,
						animate: firstRun,
						animateDelay: 200,
						type: 'currency',
						value: data.orders_this_period > 0 ? data.revenue_this_period / data.orders_this_period : 0,
						valueCompareTo: data.orders_prev_period > 0 ? data.revenue_prev_period / data.orders_prev_period : 0,
						valueCompareToText: 'Compared to $val prev. period',
						animateValue: true,
						valueIcon: true
					})
					.reportCard({
						name: 'conversion',
						title: 'Conversion',
						subTitle: '&nbsp;',
						append: true,
						animate: firstRun,
						animateDelay: 300,
						type: 'percent',
						value: data.conversions_this_period,
						valueCompareTo: data.conversions_prev_period,
						valueCompareToText: 'Compared to $val prev. period',
						animateValue: true,
						animateValueStart: 100,
						valueIcon: true
					});
				firstRun = false;

				loadedCount++;

				if (loadedCount > 0) AdminForm.hideSpinner();
			}
		})
	};

	init();

}(jQuery));
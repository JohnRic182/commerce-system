var Billing = (function($) {
	var init, updateCheckout, placeOrder;

	init = function() {
		$(document).ready(function() {
			/**
			 * Purchase forms
			 */
			$('[data-action="action-purchase"]').click(function(){
				document.location =
					('admin.php?p=billing&mode=checkout&params=' + encodeURIComponent($(this).attr('data-params')));
				return false;
			});

			$('#form-billing-checkout').submit(function(){
				placeOrder();
				return false;
			});

			$('a[data-action="checkout-use-stored-card"]').click(function(){
				$('#field-cc-cardNumber, #field-cc-cvv, #cc-info-body .field-date-month, #cc-info-body a[data-action="checkout-use-stored-card"]').closest('.form-group').hide();
				$('#cc-info-body .cc-bg').removeClass('hidden').show();
				$('a[data-action="checkout-use-new-card"]').closest('.form-group').removeClass('hidden').show();
				$('#field-cc-paymentProfileUsed').val('1');
				return false;
			});

			$('a[data-action="checkout-use-new-card"]').click(function(){
				$('#cc-info-body .cc-bg').hide();
				$('a[data-action="checkout-use-new-card"]').closest('.form-group').hide();
				$('#field-cc-cardNumber, #field-cc-cvv, #cc-info-body .field-date-month, #cc-info-body a[data-action="checkout-use-stored-card"]').closest('.form-group').removeClass('hidden').show();
				$('#field-cc-paymentProfileUsed').val('0');
				return false;
			});

			/**
			 * Cancel form
			 */
			$('form[name="form-billing-cancel"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				var errors = [];
				if ($.trim(theForm.find('select[name="service_length"]').val()) == '') {
					errors.push({
						field: 'select[name="service_length"]',
						message: trans.billing.select_service_length
					})
				}
				if ($.trim(theForm.find('textarea[name="main_reason"]').val()) == '') {
					errors.push({
						field: 'textarea[name="main_reason"]',
						message: trans.billing.enter_reason_cancel_service
					})
				}

				if (errors.length == 0) {
					AdminForm.confirm(
						trans.billing.confirm_cancel_service,
						function() {
							AdminForm.sendRequest(
								'admin.php?p=billing&mode=request-cancellation',
								theForm.serialize(), null,
								function(data) {
									if (data.status == 1) {
										theModal.modal('hide');
										AdminForm.displayAlert(trans.billing.message_request_recieve +
											trans.billing.message_note_cancellation +
											'<strong>'+ trans.billing.message_case_number + data.ticketId + '</strong>'
										);
									} else {
										if (data.errors !== undefined) {
											AdminForm.displayModalErrors(theModal, data.errors);
										} else if (data.message !== undefined) {
											AdminForm.displayModalAlert(theModal, data.message, 'danger');
										}
									}
								}
							)
						}
					)
				} else {
					AdminForm.displayModalErrors(theModal, errors);
				}

				return false;
			});

			$('.message-popup')
				.css('cursor', 'pointer')
				.click(function() {
					var id = $(this).attr('data-id');
					$.each(messages, function(idx, message) {
						if (message.mid == id) {
							$('#modal-message-label').html(message.text);
							var theModal = $('#modal-message');
							theModal.find('.modal-body').html(message.popup);
							theModal.modal('show');
						}
					});
				});

			$('.edit-card').click(function() {
				var profileId = $(this).attr('data-id');

				var theModal = $('#modal-billing-profile-update-card');
				theModal.find('input[name="profile_id"]').val(profileId);
				theModal.modal('show');

				return false;
			});

			$('form[name="form-billing-profile-add-card"],form[name="form-billing-profile-update-card"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				var errors = [];
				//TODO: Validation

				if (errors.length > 0) {
					AdminForm.displayModalErrors(theModal, errors);
				} else {
					AdminForm.sendRequest(
						'admin.php?p=billing&mode=update-card',
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=billing';
							} else {
								if (data.errors !== undefined) {
									//TODO:
									AdminForm.displayModalErrors(theModal, data.errors);
								} else if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								}
							}
						}
					)
				}

				return false;
			});

			$('#modal-billing-profile-update').on('shown.bs.modal', function() {
				var stateInput = $(this).find('.input-address-state');
				stateInput.find('option[value="'+$(stateInput).attr('data-value')+'"]').prop('selected', 'selected');
			});

			$('form[name="form-billing-profile-update"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				AdminForm.clearAlerts();
				theForm.find('.has-error').removeClass('has-error');

				var errors = [];

				if ($.trim(theForm.find('.field-profile-firstName').val()) == '') {
					errors.push({
						field: '.field-profile-firstName',
						message: trans.billing.enter_first_name
					});
				}
				if ($.trim(theForm.find('.field-profile-lastName').val()) == '') {
					errors.push({
						field: '.field-profile-lastName',
						message: trans.billing.enter_last_name
					});
				}
				if ($.trim(theForm.find('.field-profile-address1').val()) == '') {
					errors.push({
						field: '.field-profile-address1',
						message: trans.billing.enter_address
					});
				}
				if ($.trim(theForm.find('.field-profile-city').val()) == '') {
					errors.push({
						field: '.field-profile-city',
						message: trans.billing.enter_city
					});
				}
				if ($.trim(theForm.find('.field-profile-country').val()) == '') {
					errors.push({
						field: '.field-profile-country',
						message: trans.billing.select_country
					});
				}
				var country = theForm.find('.field-profile-country').val();
				if ((country == 'US' || country == 'CA') && $.trim(theForm.find('.field-profile-state').val()) == '') {
					errors.push({
						field: '.field-profile-state',
						message: trans.billing.select_state
					});
				} else if (theForm.find('.field-profile-province').is(':visible') && ($.trim(theForm.find('.field-profile-province').val()) == '')) {
					errors.push({
						field: '.field-profile-province',
						message: trans.billing.enter_province
					});
				}
				if ($.trim(theForm.find('.field-profile-zip').val()) == '') {
					errors.push({
						field: '.field-profile-zip',
						message: trans.billing.enter_zip_code
					});
				}
				if ($.trim(theForm.find('.field-profile-phone').val()) == '') {
					errors.push({
						field: '.field-profile-phone',
						message: trans.billing.enter_phone
					});
				}
				if ($.trim(theForm.find('.field-profile-email').val()) == '') {
					errors.push({
						field: '.field-profile-email',
						message: trans.billing.enter_email
					});
				}

				if (errors.length > 0) {
					AdminForm.displayModalErrors(theModal, errors);
				} else {
					AdminForm.sendRequest(
						'admin.php?p=billing&mode=update-profile',
						theForm.serialize(), trans.billing.message_save_data,
						function(data) {
							if ((data.status !== undefined) && (data.status == 1)) {
								document.location = 'admin.php?p=billing&mode=profile';
							} else {
								if (data.errors !== undefined) {
									errors = [];
									$.each(data.errors, function(idx, error) {
										errors.push({
											field: '.field-profile-'+error.field,
											message: error.message
										});
									});
									AdminForm.displayModalErrors(theModal, errors);

								} else if ((data.error != undefined) && (data.error_description !== undefined)) {
									AdminForm.displayModalAlert(theModal, data.error_description, 'danger');

								} else if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								}
							}
						}
					)
				}
				return false;
			});

			if ($('#form-billing-checkout').length > 0) {
				updateCheckout();
			}

			$('#field-promo-promo_code').change(function() {
				updateCheckout();
			});
		});
	};

	updateCheckout = function() {
		AdminForm.removeErrors();
		AdminForm.sendRequest(
			'admin.php?p=billing&mode=checkout-update',
			$('#form-billing-checkout').serialize(),
			'Updating total',
			function (data) {

				if (data.orderSubtotal != undefined && data.orderSubtotal != data.orderTotal) {
					$('#billing-checkout-subtotal-row').show();
					$('#billing-checkout-subtotal').html('$' + data.orderSubtotal.toFixed(2));
				} else {
					$('#billing-checkout-subtotal-row').hide();
				}

				$('.billing-checkout-total').html('$' + data.orderTotal.toFixed(2));

				var html = '';
				$.each(data.orderLineItems, function(index, orderLineItem) {
					var price = parseFloat(orderLineItem.price);
					html += '<tr><td>' + orderLineItem.name + '</td><td class="text-right">' +(price < 0 ? '-' : '') + '$' + Math.abs(price.toFixed(2)) + '</td></tr>';
				});

				$('#billing-checkout-order-content tbody').html(html);

				var errors = [];

				if (data.message != undefined && data.message) {
					errors.push({field: 'foo', message: data.message});
				}

				if (data.errors != undefined && data.errors.length > 0) {
					$.each(data.errors, function(index, error) {
						errors.push({field: 'input[name="' + error.field + '"]', message: error.message});
					});
				}

				if (errors.length > 0) AdminForm.displayErrors(errors);
			}
		);
	};

	placeOrder = function() {
		AdminForm.removeErrors();

		if (!$('#field-tc_agree').is(':checked'))
		{
			AdminForm.displayAlert(trans.billing.confirm_term_condition, 'danger');
			return;
		}

		AdminForm.sendRequest(
			'admin.php?p=billing&mode=checkout-place-order',
			$('#form-billing-checkout').serialize(),
			'Processing payment',
			function (data) {
				if (data.status == 1) {
					AdminForm.displaySpinner('Finishing order');
					document.location = 'admin.php?p=billing&mode=checkout-thank-you';
				} else {
					var errors = [];

					if (data.message != undefined && data.message) {
						errors.push({field: 'foo', message: data.message});
					}

					if (data.errors != undefined && data.errors.length > 0) {
						$.each(data.errors, function(index, error) {
							errors.push({field: 'input[name="' + error.field + '"]', message: error.message});
						});
					}

					if (errors.length > 0) AdminForm.displayErrors(errors);
				}
			}
		);
	};

	init();

	return {
		updateCheckout: updateCheckout
	};

}(jQuery));
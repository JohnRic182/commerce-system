var CountriesStates = (function($) {
	var init, editCountry, editState, removeCountry, removeState,
		handleCountryForm, handleStateForm, handleCountriesDeleteSubmit, handleStatesDeleteSubmit;

	init = function() {
		$('form[name="form-add-country"]').submit(function () {
			return handleCountryForm($(this), 'add');
		});

		$('form[name=form-countries-delete]').submit(function () {
			return handleCountriesDeleteSubmit($(this));
		});

		$('form[name="form-add-state"]').submit(function () {
			var coid = $('form[name="form-add-state"] input[name="coid"]').val();
			return handleStateForm($(this), 'add', coid);
		});

		$('form[name=form-states-delete]').submit(function () {
			return handleStatesDeleteSubmit($(this));
		});

		$('.edit-state').click(function() {
			var theLink = $(this);
			var id = theLink.attr('data-id');
			var coid = theLink.attr('data-coid');

			return editState(id, coid);
		});

		$('.delete-state').click(function() {
			var theLink = $(this);
			var id = theLink.attr('data-id');
			var coid = theLink.attr('data-coid');
			var nonce = theLink.attr('data-nonce');

			return removeState(id, coid, nonce);
		});
		
		// for bulk activate / deactivate of countries
		$('.countries-activatedeactivate').click(function() {
			var actionMode = $(this).attr('mode');
			$('.hidden-input-mode').val(actionMode);
			if (actionMode == 'activate') {
				$('.ad-label').html(trans.countries.activate_label);
				$('.ad-btn-label').html(trans.countries.btn_activate_label);
				$('.ad-text-label1').html(trans.countries.activate_text_label1);
				$('.ad-text-label2').html(trans.countries.activate_text_label2);
			} else {
				$('.ad-label').html(trans.countries.deactivate_label);
				$('.ad-btn-label').html(trans.countries.btn_deactivate_label);
				$('.ad-text-label1').html(trans.countries.deactivate_text_label1);
				$('.ad-text-label2').html(trans.countries.deactivate_text_label2);
			}
		});
		
		// handle activate / deactivate countries
		$('form[name=form-countries-activatedeactivate]').submit(function () {
			return handleCountriesActivateDeactivateSubmit($(this));
		});
	};

	handleCountryForm = function(theForm, mode) {
		var theModal = $("#modal-" + mode + "-country");
		var errors = [];
		AdminForm.sendRequest(
			theForm.attr('action'),
			theForm.serialize(),
			null,
			function (data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();
					theModal.modal('hide');
					document.location = 'admin.php?p=countries';
				} else {
					AdminForm.hideSpinner();

					if (data.errors !== undefined) {
						$.each(data.errors, function (errKey, errMsg) {
							errors.push({
								field: '.field-' + errKey,
								message: errMsg
							});
						});
						AdminForm.displayValidationErrors(errors);
					} else {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			},
			function () {
				AdminForm.hideSpinner();
			}
		);
		return false;
	};

	handleCountriesDeleteSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var nonce = theForm.find('input[name=nonce]').val();
		var deleteMode = $('input[name="form-countries-delete-mode"]:checked').val();
		var pageparam = '';
		if (cur_paginator_page > 1) {
			pageparam = '&page=' + cur_paginator_page;
		}

		var ids = '';
		var hasExcludedCountries = 0;
		$('.checkbox-country:checked').each(function () {
			var id = $(this).val();
			if (id == 1 || id == 2) {
				hasExcludedCountries = 1;
				return false;
			}
			ids += (ids == '' ? '' : ',') + id;
		});

		if (deleteMode == 'selected') {
			if (ids != '') {
				window.location = 'admin.php?p=countries&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce + pageparam;
			}
			else {
				if (hasExcludedCountries == 1) {
					AdminForm.displayModalAlert(theModal, trans.countries.cannot_delete_excluded_countries, 'danger');
				}
				else {
					AdminForm.displayModalAlert(theModal, trans.countries.no_countries_selected, 'danger');
				}
			}
		}
		else if (deleteMode == 'all') {
			hasExcludedCountries = 1;
			if (hasExcludedCountries == 1) {
				AdminForm.displayModalAlert(theModal, trans.countries.cannot_delete_excluded_countries, 'danger');
			}
			else {
				window.location = 'admin.php?p=countries&mode=delete&deleteMode=all&nonce=' + nonce + pageparam;
			}
		}

		return false;
	};

	handleCountriesActivateDeactivateSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var nonce = theForm.find('input[name=nonce]').val();
		var activationAction = theForm.find('input[name=mode]').val();
		var activationMode = $('input[name="form-countries-activatedeactivate-mode"]:checked').val();
		var pageparam = '';
		if (cur_paginator_page > 1) {
			pageparam = '&page=' + cur_paginator_page;
		}
		if (activationMode == 'selected') {
			var ids = '';
			$('.checkbox-country:checked').each(function () {
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});

			if (ids != '') {
				window.location = 'admin.php?p=countries&mode=activation&activationMode=selected&ids=' + ids + '&activationAction=' +activationAction+ '&nonce=' + nonce + pageparam;
			}
			else {
				AdminForm.displayModalAlert(theModal, trans.countries.no_countries_selected_for_activation, 'danger');
			}
		}
		else if (activationMode == 'all') {
			window.location = 'admin.php?p=countries&mode=activation&activationMode=all&activationAction=' +activationAction+'&nonce=' + nonce + pageparam;
		}

		return false;
	};
	
	handleStateForm = function(theForm, mode, coid) {
		var theModal = $("#modal-" + mode + "-state");
		var errors = [];
		AdminForm.sendRequest(
			theForm.attr('action'),
			theForm.serialize(),
			null,
			function (data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();
					theModal.modal('hide');
					var cpage = $('input[name="countriespage"]').val();
					var pageparam = '&countriespage=' + cpage;
					if (cur_paginator_page > 1) {
						pageparam += '&page=' + cur_paginator_page;
					}
					document.location = 'admin.php?p=states&coid=' + coid + pageparam;
				} else {
					AdminForm.hideSpinner();

					if (data.errors !== undefined) {
						$.each(data.errors, function (errKey, errMsg) {
							errors.push({
								field: '.field-' + errKey,
								message: errMsg
							});
						});

						AdminForm.displayValidationErrors(errors);
					} else {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			},
			function () {
				AdminForm.hideSpinner();
			}
		);
		return false;
	};

	handleStatesDeleteSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var nonce = theForm.find('input[name=nonce]').val();
		var coid = theForm.find('input[name=coid]').val();
		var deleteMode = $('input[name="form-states-delete-mode"]:checked').val();

		var pageparam = '&countriespage=' + cur_countries_page;
		if (cur_paginator_page > 1) {
			pageparam += '&page=' + cur_paginator_page;
		}

		if (deleteMode == 'selected') {
			var ids = '';
			$('.checkbox-state:checked').each(function () {
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});

			if (ids != '') {
				window.location = 'admin.php?p=states&mode=delete&deleteMode=selected&coid=' + coid + '&ids=' + ids + '&nonce=' + nonce + pageparam;
			}
			else {
				AdminForm.displayModalAlert(theModal, trans.states.no_states_selected, 'danger');
			}
		}
		else if (deleteMode == 'all') {
			window.location = 'admin.php?p=states&mode=delete&deleteMode=all&coid=' + coid + '&nonce=' + nonce + pageparam;
		}

		return false;
	};

	editCountry = function(id) {
		AdminForm.sendRequest(
			'admin.php?p=countries&mode=update&id=' + id,
			null,
			null,
			function (data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();

					var countryEditWrapper = $('#country-edit-wrapper');
					countryEditWrapper.html(data.html);
					var editModal = countryEditWrapper.find('#modal-edit-country');

					$(editModal).find('.field-checkbox').each(function (idx, el) {
						forms.initCheckbox(el);
					});


					$('form[name="form-edit-country"]').submit(function () {
						return handleCountryForm($(this), 'edit');
					});

					$(editModal.find('fieldset').get(0)).addClass('fieldset-primary');
					$('#modal-edit-country').modal('show');
				}
			},
			null,
			'GET'
		);

		return false;
	};

	editState = function(id, coid) {
		AdminForm.sendRequest(
			'admin.php?p=states&mode=update&id=' + id,
			null,
			null,
			function (data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();

					$('#state-edit-wrapper').html(data.html);

					$('form[name="form-edit-state"]').submit(function () {
						return handleStateForm($(this), 'edit', coid);
					});

					$('#modal-edit-state').modal('show');
				}
			},
			null,
			'GET'
		);

		return false;
	};

	removeCountry = function(id, nonce) {
		AdminForm.confirm(trans.countries.confirm_remove, function () {
			var pageparam = '';
			if (cur_paginator_page > 1) {
				pageparam = '&page=' + cur_paginator_page;
			}

			if (id == 1 || id == 2) {
				AdminForm.displayAlert(trans.countries.cannot_delete_excluded_countries, 'danger');
			} else {
				window.location = 'admin.php?p=countries&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce + pageparam;
			}
		});

		return false;
	};

	removeState = function(id, coid, nonce) {
		var pageparam = '&countriespage=' + cur_countries_page;
		if (cur_paginator_page > 1) {
			pageparam += '&page=' + cur_paginator_page;
		}

		AdminForm.confirm(trans.states.confirm_remove, function () {
			window.location = 'admin.php?p=states&mode=delete&deleteMode=single&coid=' + coid + '&id=' + id + '&nonce=' + nonce + pageparam;
		});

		return false;
	};

	init();

	return {
		editCountry: editCountry,
		removeCountry: removeCountry,
		removeState: removeState
	}
}(jQuery));


var ProductVariants = (function($) {
	var init, renderProductVariantsTable;

	init = function() {
		$(document).ready(function(){
			$('#modal-product-variant').on('hidden.bs.modal', function () {
				$('#modal-product-variant').find('.form-group .error').remove();
			});

			$('a[data-action="action-product-variant-add"]').click(function(){
				if (productAttributes != undefined && productAttributes.length > 0) {
					var html = '';
					$.each(productAttributes, function(index, attribute){
						if (attribute.is_active == 'Yes' && attribute.track_inventory == '1' && attribute.options_parsed) {
							var fieldErrorCss = "has-error has-feedback";
							
							html +=
								'<div class="form-group field-select '+ fieldErrorCss +' col-sm-6 col-xs-12">' +
									'<select id="field-variant-attribute-' + attribute.paid + '" class="form-control field-variant-attribute-' + attribute.paid + '" name="variant[attributes][' + attribute.paid + ']" data-required="1">';

							html += '<option value="" disabled selected="selected">' + htmlentities(attribute.caption) + '</option>';
							$.each(attribute.options_parsed, function(optionIndex, option){
								html += '<option value="' + htmlentities(option.name) +'">' + option.name + '</option>';
							});

							html +=	'</select>' +
								'</div>';
						}
					});

					if (html != '') {
						$('.modal-product-variant-attributes').html(html);
						var variantInventory = $('#radio_field-inventory_control_AttrRuleExc').is(':checked');
						$('#field-variant-stock, #field-variant-stock_warning').closest('.form-group').toggle(variantInventory);
						$('#field-variant-product_subid, #field-variant-product_sku, #field-variant-stock,#field-variant-stock_warning').val('');
						$('#field-variant-id').val('new');
						$('#field-variant-is_active').prop('checked', true).change();
						$('#field-variant-inventory_control').val($('form[name=frmProduct] input[name=inventory_control]:checked').val());
						$('#modal-product-variant').modal('show');

						return false;
					}
				}

				AdminForm.displayAlert(trans.products_variants.message_add_attributes_variant);
				return false;
			});

			$('.table-product-variants')
				.on('click', 'a[data-action="action-product-variant-edit"]', function(){

					var variantInventory = $('#radio_field-inventory_control_AttrRuleExc').is(':checked');
					$('#field-variant-stock, #field-variant-stock_warning').closest('.form-group').toggle(variantInventory);

					var variantIndex = $(this).attr('data-target');

					if (productVariants[variantIndex] != undefined) {
						var variant = productVariants[variantIndex];

						var theModal = $('#modal-product-variant');
						$('#field-variant-pid').val(variant.pid);
						$('#field-variant-id').val(variant.pi_id);
						theModal.find('.modal-product-variant-attributes').html('<div class="col-xs-12 "><strong>Attributes values</strong>:<br/>' + variant.attributes_list.replace('\n', '<br/>') + '<br/>&nbsp;</div>');
						$('#field-variant-product_subid').val(variant.product_subid);
						$('#field-variant-product_sku').val(variant.product_sku);
						$('#field-variant-is_active').prop('checked', variant.is_active == '1').change();
						$('#field-variant-stock').val(variant.stock);
						$('#field-variant-stock_warning').val(variant.stock_warning);
						$('#field-variant-inventory_control').val($('form[name=frmProduct] input[name=inventory_control]:checked').val());
						theModal.modal('show');
					}

					return false;
				})
				.on('click', 'a[data-action="action-product-variant-delete"]', function(){
					var variantIndex = $(this).attr('data-target');

					if (productVariants[variantIndex] != undefined) {
						var variant = productVariants[variantIndex];

						AdminForm.confirm(trans.products_variants.confirm_delete_product_variant, function(){
							AdminForm.sendRequest(
								'admin.php?p=product_page_variants&mode=variant-delete',
								{'variant[id]': variant.pi_id, 'variant[pid]': variant.pid, 'variant[inventory_control]': $('form[name=frmProduct] input[name=inventory_control]').val()},
								'Deleting product variant',
								function(data){
									if (data.status) {
										if (data.productStock != null) $('#field-stock').val(data.productStock);
										productVariants = data.variants;
										renderProductVariantsTable(productVariants);
										AdminForm.displayAlert('Product variant has been deleted');
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-' + key, message: message});
											});
										});

										AdminForm.displayErrors(errors);
									}
								}
							);
						});
					}

					return false;
				});

			$('#modal-product-variant').find('form').submit(function(){
				var variantId = parseInt($('#field-variant-id').val());
				AdminForm.sendRequest(
					'admin.php?p=product_page_variants&mode=variant-' + (isNaN(variantId) || variantId < 1 ? 'add' : 'update'),
					$('#modal-product-variant').find('form').serialize(),
					'Saving product variant',
					function(data){
						if (data.status) {
							if (data.productStock != null) $('#field-stock').val(data.productStock);
							productVariants = data.variants;
							renderProductVariantsTable(productVariants);
							$('#modal-product-variant').modal('hide');

							AdminForm.displayAlert('Product variant has been ' + ($('#field-variant-id').val() == 'new' ? 'added' : 'updated'));
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: '.field-variant-' + key, message: message});
								});
							});

							console.log(errors);

							AdminForm.displayValidationErrors(errors);
						}
					}
				);

				return false;
			});

			renderProductVariantsTable(productVariants == undefined ? [] : productVariants);
		});
	};

	renderProductVariantsTable =  function(variants) {
		if (variants.length == 0) {
			$('.table-product-variants').hide();
		} else {
			var variantInventory = $('#radio_field-inventory_control_AttrRuleExc').is(':checked');

			$('.table-product-variants tbody').html('');
			$(variants).each(function(index, variant){
				var html =
					'<tr>' +
						'<td><a href="#" data-action="action-product-variant-edit" data-target="' + index + '">' + htmlentities(variant.product_subid) + '</a></td>' +
						'<td><a href="#" data-action="action-product-variant-edit" data-target="' + index + '">' + htmlentities(variant.product_sku) + '</a></td>' +
						'<td>' + htmlentities(variantInventory ? variant.stock : 'n/a') + '</td>' +
						'<td>' + htmlentities(variantInventory ? variant.stock_warning : 'n/a') + '</td>' +
						'<td>' + variant.attributes_list.replace('\n', '<br>') + '</td>' +
						'<td>' + htmlentities(variant.is_active == '1' ? 'Yes' : 'No') + '</td>' +
						'<td class="list-actions text-right">' +
							'<a href="#" data-action="action-product-variant-edit" data-target="' + index + '"><span class="icon icon-edit"></span>Edit</a> '+
							'<a href="#" data-action="action-product-variant-delete" data-target="' + index + '"><span class="icon icon-delete"></span>Delete</a>' +
						'</td>' +
					'</tr>';
				$(html).appendTo('.table-product-variants tbody');
			});
			$('.table-product-variants').show();
		}
	};

	init();

	return {
		renderProductVariantsTable : renderProductVariantsTable
	}

}(jQuery));



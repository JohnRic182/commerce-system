$(document).ready(function() {
	var handleGoogleAnalyticsToggle = function() {
		var enabled = $('#field-GoogleAnalyticsActive').is(':checked');

		$('#field-GoogleAnalytics').closest('.form-group').toggle(enabled);
	};
	$('#field-GoogleAnalyticsActive').change(function() { handleGoogleAnalyticsToggle(); });
	handleGoogleAnalyticsToggle();

	var handleGoogleAnalyticsConversionToggle = function() {
		var enabled = $('#field-GoogleConversionActive').is(':checked');

		$('#field-GoogleConversionTrackingCode').closest('.form-group').toggle(enabled);
	};
	$('#field-GoogleConversionActive').change(function() { handleGoogleAnalyticsConversionToggle(); });
	handleGoogleAnalyticsConversionToggle();

	var handleGoogleAdwordsToggle = function() {
		var enabled = $('#field-GoogleAdwordsConversionActive').is(':checked');

		$('#field-CoogleAdwordsTrackingCode').closest('.form-group').toggle(enabled);
	};
	$('#field-GoogleAdwordsConversionActive').change(function() { handleGoogleAdwordsToggle(); });
	handleGoogleAdwordsToggle();

	$('#field-generate_site_map_button, #button-generate-site-map-top-right').click(function(e) {
		e.preventDefault();
		$('#field-mode').val('generate');
		$('form[name=form-google_marketing]').submit();
	});
	$('.btn-primary').click(function(e) {
		e.preventDefault();
		$('#field-mode').val('');
		$('form[name=form-google_marketing]').submit();
	});
});
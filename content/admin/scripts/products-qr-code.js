var ProductsQRCode = (function($) {
	var init;

	init = function() {
		$(document).ready(function() {
			$('a[data-action="generate-qr-code"]').click(function() {
				$('#modal-qr-code').modal('show');
				return false;
			});

			$('#field-qr_generate_media').change(function() {
				$('#field-qr_generate_error_check').removeAttr('disabled');
				$('#field-qr_generate_default_size').removeAttr('disabled');

				var e = $('#field-qr_generate_error_check').val();
				var s = $('#field-qr_generate_default_size').val();

				switch ($(this).val())
				{
					case '1' : e = 'L'; s = '4'; break;
					case '2' : e = 'L'; s = '8'; break;
					case '3' : e = 'M'; s = '8'; break;
					case '4' : e = 'M'; s = '12'; break;
					case '5' : e = 'Q'; s = '4'; break;
					case '6' : e = 'H'; s = '4'; break;
				}
				$('#field-qr_generate_error_check').val(e);
				$('#field-qr_generate_default_size').val(s);
				if ($(this).val() != '0') {
					$('#field-qr_generate_error_check').attr('disabled', 'disabled');
					$('#field-qr_generate_default_size').attr('disabled', 'disabled');
				}
			});

			$('#field-qr_generate_media').change();

			$('#modal-qr-code').find('form').submit(function() {
				$('#modal-qr-code .btn-primary').html('Please wait...').attr('disabled', 'disabled');
				AdminForm.sendRequest(
					'admin.php?p=marketing_qr_ajax&chart=true',
					{
						action: 'generate_product_qr',
						product_id: $('#field-qr_pid').val(),
						campaign_id: $('#field-qr_campaign_id').val(),
						url_type: $('#field-qr_generate_product_url_type').val(),
						image_type: $('#field-qr_generate_image_type').val(),
						error_check: $('#field-qr_generate_error_check').val(),
						default_size: $('#field-qr_generate_default_size').val()
					},
					'Generating QR code',
					function(data) {
						$('#modal-qr-code .btn-primary').html('Generate').removeAttr('disabled');
						$('#modal-qr-code').modal('hide');
						$('#modal-qr-code-generated').data('path', data.path);
						$('#modal-qr-code-generated .modal-body img.preview').attr('src', data.url);
						$('#modal-qr-code-generated .modal-body img.preview').attr('src', data.url);
						$('#modal-qr-code-generated .modal-body div.link').html(data.link_url);
						$('#modal-qr-code-generated').modal('show');
					}
				);

				$('#modal-qr-code-generated .btn-primary').click(function() {
					var src = 'admin.php?p=marketing_qr_ajax&chart=true&action=download_qr&path=' + $('#modal-qr-code-generated').data('path');
					$('#modal-qr-code-generated iframe').attr('src', src);
					$('#modal-qr-code-generated').modal('hide');

					return false;
				});

				return false;
			});
		});
	};

	init();

}(jQuery));

var QuickbooksQuickStart = (function($) {
	var init, handleTaxesToggle;

	init = function() {
		$(document).ready(function() {
			$('#field-intuit_sync_taxes').change(function() { handleTaxesToggle(); });
			handleTaxesToggle();

			$('#modal-quickbooks-quick-start').modal({
				'keyboard': false
			});

			$('form[name=form-quickbooks-quick-start]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var errors = [];

				if ($.trim($('#field-intuit_income_account').val()) == '') {
					errors.push({
						'field': '#field-intuit_income_account',
						'message': trans.quickbooks_quickstart.select_income_account
					});
				}
//				if ($.trim($('#field-intuit_expense_account').val()) == '') {
//					errors.push({
//						'field': '#field-intuit_expense_account',
//						'message': 'Please select an expense account'
//					});
//				}

				if (errors.length > 0) {
					AdminForm.displayModalErrors(theModal, errors);
				} else {
					AdminForm.sendRequest(
						'admin.php?p=intuit_quickstart',
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = document.location;
							} else {
								if (data.errors !== undefined) {
									AdminForm.displayModalErrors(theModal, data.errors);
								} else if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								}
							}
						}
					)
				}

				return false;
			});
		});
	};

	handleTaxesToggle = function() {
		var isDesktop = qbVersion !== 'online';

		var syncsTaxes = $('#field-intuit_sync_taxes').is(':checked');
		$('#field-intuit_taxable_tax_code').closest('.form-group').toggle(syncsTaxes && isDesktop);
		$('#field-intuit_nontaxable_tax_code').closest('.form-group').toggle(syncsTaxes && isDesktop);
		$('#field-intuit_default_tax_rate_name').closest('.form-group').toggle(syncsTaxes && isDesktop);
		$('#field-intuit_zero_tax_rate_name').closest('.form-group').toggle(syncsTaxes && isDesktop);

	};

	init();
}(jQuery));
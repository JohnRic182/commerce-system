var Stamps = (function($) {
	var init, handlePurchasePostageSubmit, handleCarrierPickupSubmit;

	init = function() {
		$(document).ready(function() {
			$('form[name=form-purchase-postage]').submit(function() {
				return handlePurchasePostageSubmit($(this));
			});

			$('form[name=form-search-scan-forms]').submit(function() {
				var year = $('select[name=search_date\\[year\\]]').val();
				var month = $('select[name=search_date\\[month\\]]').val();
				var day = $('select[name=search_date\\[day\\]]').val();
				document.location = 'admin.php?p=stamps&mode=scan-forms&search_date='+
					year + '-' + month + '-' + day;

				return false;
			});

			$('form[name=form-generate-scan-form]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				AdminForm.sendRequest(
					'admin.php?p=stamps&mode=generate-scan-form',
					theForm.serialize(), null,
					function(data) {
						if (data.status == 1) {
							//TODO: Display scan forms
						} else {
							if (data.message !== undefined) {
								AdminForm.displayModalAlert(theModal, data.message, 'danger');
							} else if (data.errors !== undefined) {
								AdminForm.displayModalErrors(theModa, data.errors);
							}
						}
					}
				);

				return false;
			});

			$('.link_stamps_scan_search_view').click(function() {
				//TODO: Handle view scan form

				return false;
			});

			$('.pre-populate-carrier-fields').click(function() {
				var theForm = $(this).closest('form');
				theForm.find('input[name=action]').val('pre-populate');
				theForm.submit();

				return false;
			});

			$('form[name="form-carrier-pickup"]').submit(function() {
				return handleCarrierPickupSubmit($(this));
			});
		});
	};

	handlePurchasePostageSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		AdminForm.clearAlerts();

		var errors = [];

		if ($.trim(theForm.find('input[name=addfunds_amount]').val()) == '') {
			errors.push({
				field: 'input[name=addfunds_amount]',
				message: trans.stamps.amount_required
			});
		}

		if (errors.length > 0) {
			AdminForm.displayModalErrors(theModal, errors);
		} else {
			AdminForm.sendRequest(
				'admin.php?p=stamps&mode=purchase-postage',
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=app&key=stamps';
					} else {
						if (data.message !== undefined) {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						} else if (data.errors !== undefined) {
							AdminForm.displayModalErrors(theModal, data.errors);
						}
					}
				}
			);
		}

		return false;
	};

	handleCarrierPickupSubmit = function(theForm) {
		AdminForm.clearAlerts();

		var errors = [];

		if (errors.length > 0) {
			AdminForm.displayErrors(errors);
		} else {
			if (theForm.find('input[name=action]').val() == 'pre-populate') {
				return true;
			} else {
				AdminForm.confirm(trans.stamps.confirm_carrier_pickup, function() {
					AdminForm.sendRequest(
						theForm.attr('action'),
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=stamps';
							} else {
								if (data.message !== undefined) {
									AdminForm.displayAlert(data.message, 'danger');
								}
							}
						}
					)
				});
			}
		}

		return false;
	};

	init();
}(jQuery));
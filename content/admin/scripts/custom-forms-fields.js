var CustomFields = (function($) {
	var init, hideAndShowFields, renderCustomFieldsTable, autoGenerateId;

	/**
	 * Initialize
	 */
	init = function() {
		$(document).ready(function() {

			$('#field-customField-field_type').change(function(){
				hideAndShowFields();
			});

			$('a[data-action="action-custom-field-add"]').click(function() {
				$('.modal-header h4#modal-custom-field-label').html(trans.custom_field.custom_field);
				$('#field-customField-field_name').addClass('generate-id-source-field');
				$('#field-customField-field_code').addClass('generate-id-field autogenerate');
				$('#field-customField-field_name,#field-customField-field_code,#field-customField-field_caption,#field-customField-priority,#field-customField-text_length,#field-customField-options,#field-customField-value_checked,#field-customField-value_unchecked').val('');
				$('#field-customField-field_id').val('new');
				$('#field-customField-field_type').val('text').change();
				$('#field-customField-is_active').prop('checked', true).change();
				$('#field-customField-is_required').prop('checked', true).change();
				$('#field-customField-priority').val($('.table-custom-fields tbody tr').length + 1);
				$('#field-customField-text_length').val('20');

				$('#modal-custom-field').modal('show');
				return false;
			});

			$('#field-customField-field_name').keyup(function() {
				autoGenerateId(this);
			});

			$('#field-customField-field_code').keyup(function () {
				$('div.modal-body input.generate-id-field').removeClass('autogenerate');
			});

			$('.table-custom-fields')
				.on('click', 'a[data-action="action-custom-field-edit"]', function() {
					var customFieldIndex = $(this).attr('data-target');

					if (customFields[customFieldIndex] != undefined) {
						var customField = customFields[customFieldIndex];
						$('.modal-header h4#modal-custom-field-label').html(trans.common.editing + " " + trans.custom_field.custom_field + " " + customField.field_name);
						$('#field-customField-field_name').addClass('generate-id-source-field');
						$('#field-customField-field_code').addClass('generate-id-field');
						$('#field-customField-field_id').val(customField.field_id);
						$('#field-customField-field_name').val(customField.field_name);
						$('#field-customField-field_code').val(customField.field_code);
						$('#field-customField-field_caption').val(customField.field_caption);
						$('#field-customField-is_active').prop('checked', customField.is_active == '1').change();
						$('#field-customField-is_required').prop('checked', customField.is_required == '1').change();
						$('#field-customField-field_type').val(customField.field_type).change();
						$('#field-customField-priority').val(customField.priority);
						$('#field-customField-text_length').val(customField.text_length);
						$('#field-customField-options').val(customField.options);
						$('#field-customField-value_checked').val(customField.value_checked);
						$('#field-customField-value_unchecked').val(customField.value_unchecked);
						$('#modal-custom-field').modal('show');
					}

					return false;
				})
				.on('click', 'a[data-action="action-custom-field-delete"]', function() {
					var customFieldIndex = $(this).attr('data-target');

					if (customFields[customFieldIndex] != undefined) {
						var customField = customFields[customFieldIndex];

						AdminForm.confirm(trans.custom_field.confirm_remove, function() {
							AdminForm.sendRequest(
								'admin.php?p=custom_form_field&mode=delete',
								{'customField[field_id]': customField.field_id, 'customField[custom_form_id]': customField.custom_form_id},
								'Deleting custom form field',
								function(data) {
									if (data.status) {
										customFields = data.formFields;
										renderCustomFieldsTable(customFields);
										AdminForm.displayAlert(trans.common.success);
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-' + key, message: message});
											});
										});
										AdminForm.displayErrors(errors);
									}
								}
							);
						});
					}

					return false;
				});

			$('#modal-custom-field').find('form').submit(function() {
				AdminForm.sendRequest(
					'admin.php?p=custom_form_field&mode=' + ($('#field-customField-field_id').val() == 'new' ? 'add' : 'update'),
					$('#modal-custom-field').find('form').serialize(),
					'Saving custom field',
					function(data) {
						if (data.status) {
							customFields = data.customFields;
							renderCustomFieldsTable(customFields);
							$('#modal-custom-field').modal('hide');
							AdminForm.displayAlert(trans.common.success);
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: '.field-customField-' + key, message: message});
								});
							});
							AdminForm.displayValidationErrors(errors);
						}
					}
				);

				return false;
			});

			if (customFields == undefined) customFields = [];

			renderCustomFieldsTable(customFields);
		});
	};

	/**
	 * Hide and show form fields
	 */
	hideAndShowFields = function (){
		var fieldType = $("#field-customField-field_type").val();
		$("#field-customField-text_length").closest('div').hide();
		$("label[for='field-customField-text_length']").closest('div').hide();

		$(".field-customField-options").closest('div').hide();
		$("label[for='field-customField-options']").closest('div').hide();

		$(".field-customField-value_checked").closest('div').hide();
		$("label[for='field-customField-value_checked']").closest('div').hide();

		$(".field-customField-value_unchecked").closest('div').hide();
		$("label[for='field-customField-value_unchecked']").closest('div').hide();

		switch (fieldType) {
			case 'text':
			case 'textarea':
			{
				$("#field-customField-text_length").closest('div').show();
				$("label[for='field-customField-text_length']").closest('div').show();
				break;
			}
			case 'select':
			case 'radio':
			{
				$(".field-customField-options").closest('div').show();
				$("label[for='field-customField-options']").closest('div').show();
				break;
			}
			case 'checkbox':
			{
				$(".field-customField-value_checked").closest('div').addClass('clear-both').show();
				$("label[for='field-customField-value_checked']").closest('div').show();

				$(".field-customField-value_unchecked").closest('div').show();
				$("label[for='field-customField-value_unchecked']").closest('div').show();
				break;
			}
		}
	}

	/**
	 * Render custom fields table
	 *
	 * @param customFields
     */
	renderCustomFieldsTable = function(customFields) {
		var fieldTypes = [];
		fieldTypes['select'] = 'Drop-down';
		fieldTypes['radio'] = 'Radio buttons';
		fieldTypes['text'] = 'Text input';
		fieldTypes['textarea'] = 'Textarea input';
		fieldTypes['checkbox'] = 'Checkbox';

		if (customFields.length == 0) {
			$('.table-custom-fields,.group-variants').hide();
		} else {
			$('.table-custom-fields tbody').html('');
			$(customFields).each(function(index, customField) {
				var html =
					'<tr>' +
						'<td><a href="#" data-action="action-custom-field-edit" data-target="' + index + '">' + htmlentities(customField.field_name) + '</a></td>' +
						'<td><a href="#" data-action="action-custom-field-edit" data-target="' + index + '">' + htmlentities(customField.field_code) + '</a></td>' +
						'<td><a href="#" data-action="action-custom-field-edit" data-target="' + index + '">' + htmlentities(customField.field_caption == '' ? 'n/a' : customField.field_caption) + '</a></td>' +
						'<td>' + htmlentities(customField.priority) + '</td>' +
						'<td>' + htmlentities(fieldTypes[customField.field_type]) + '</td>' +
						'<td>' + htmlentities(customField.is_active == '1' ? 'Yes' : 'No') + '</td>' +
						'<td>' + htmlentities(customField.is_required == '1' ? 'Yes' : 'No') + '</td>' +
						'<td class="list-actions text-right">' +
							'<a href="#" data-action="action-custom-field-edit" data-target="' + index + '"><span class="icon icon-edit"></span>Edit</a> '+
							'<a href="#" data-action="action-custom-field-delete" data-target="' + index + '"><span class="icon icon-delete"></span>Delete</a>' +
						'</td>' +
					'</tr>';
				$(html).appendTo('.table-custom-fields tbody');
			});
			$('.table-custom-fields,.group-variants').show();
		}
	};

	autoGenerateId = function (el) {
		var val = $(el).val();
		var lowercase = val.toLowerCase();
		var stripChars = lowercase.replace(/[^\w\s]|_/gi, ' ');
		var generatedId = $.trim(stripChars).replace(/\s+/g, '_')

		if ($('div.modal-body input.generate-id-field').hasClass('autogenerate'))
		{
			$('div.modal-body input.generate-id-field').val(generatedId);
			if (generatedId) {
				$('div.modal-body input.generate-id-field').closest('div.form-group').removeClass('has-error has-feedback');
				$('div.modal-body input.generate-id-field').parent().find('.warning-sign, .help-block.error').addClass('hidden');
			} else {
				$('div.modal-body input.generate-id-field').closest('div.form-group').addClass('has-error has-feedback');
				$('div.modal-body input.generate-id-field').parent().find('.warning-sign, .help-block.error').removeClass('hidden');
			}
		}
		else if ($('div.modal-body input.generate-id-field').val().length === 0)
		{
			$('div.modal-body input.generate-id-field').addClass('autogenerate');
			$('div.modal-body input.generate-id-field').val(generatedId);
		}
	};

	init();

}(jQuery));

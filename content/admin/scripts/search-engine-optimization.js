function generateHtaccess(nonce) {
	AdminForm.confirm(trans.search_engine_optimization.confirm_generate_htaccess, function() {
		$("#id-generate_htaccess").val('Yes');
		$('form[name=form-search_optimization]').submit();
	});

	return false;
}

function RegenerateURLs(nonce) {
	AdminForm.confirm(trans.search_engine_optimization.confirm_generate_urls, function() {
		$("#id-regenerate_urls").val('Yes');
		$('form[name=form-search_optimization]').submit();
	});

	return false;
}
var Search = (function($) {
	var selectedIndex = 0;
	var searchResults = $('#search-results');
	var itemsCount = 0;

	var init, showSearch;

	init = function() {
		$(document).ready(function(){
			$('#main-nav .pi-search').attr('accesskey', 'f');
			$('#main-nav .pi-search').click(function(){
				showSearch();
				return false;
			});

			$('#modal-admin-area-search').on('shown.bs.modal', function() {
				$('#field-search-text').focus();
			})

			var adminSearchTimeOut = null;

			$('#field-search-text').keyup(function(e) {
				var code = e.keyCode || e.which;

				switch (code) {
					case 13: { // enter
						var href = $(searchResults).find('li.selected a').attr('href');
						if (href != null) {
							document.location = href;
						}

						break;
					}
					case 38: { // up
						var prev = $(searchResults).find('.selected').prevAll('.link:first');
						if (prev.length > 0) {
							$(searchResults).find('.selected').removeClass('selected');
							$(prev).addClass('selected');
						}
						break;
					}
					case 40: { // down
						var next = $(searchResults).find('.selected').nextAll('.link:first');
						if (next.length > 0) {
							$(searchResults).find('.selected').removeClass('selected');
							$(next).addClass('selected');
						}
						break;
					}
					case 37: // left
					case 39: { // right
						// ignore
						break;
					}
					default: {
						var str = $.trim($(this).val());

						if (str.length > 0) {

							$(searchResults).html('');
							var html = '';

							/**
							 * Local search
							 */
							var siteMapResultsCount = 0;
							var lowerStr = str.toLowerCase();
							lowerStr = lowerStr
								.replace(/\s+(a|the)\s+/, ' ')
								.replace(/(add|create|insert)\s+(new?)\s+/, 'add ')
								.replace(/(delete|remove|destroy)\s+((current|existing)?)\s+/, 'delete ');

							$.each(adminSiteMap, function(index, siteItem){
								if (siteItem.k.search(lowerStr) != -1) {
									if (siteMapResultsCount < 5) {
										html += '<li class="item link"><a href="' + siteItem.u + '">' + (siteItem.t) + '</a></li>';
									}
									siteMapResultsCount++;
								}
							});

							if (siteMapResultsCount > 0) {
								html = '<li class="group">Admin Area (' + siteMapResultsCount + ')</li>' + html;
								$(searchResults).html('<ul>' + html + '</ul>').show();
								$(searchResults).find('li.link:first').addClass('selected');
							}

							/**
							 * Server search
							 */
							if (adminSearchTimeOut) clearTimeout(adminSearchTimeOut);

							adminSearchTimeOut = setTimeout(function() {
								AdminForm.sendRequest(
									'admin.php?p=search',
									{
										str : str
									},
									null,
									function(data) {
										// note: adding results to local search html
										if (data && data.status && data.results && data.count > 0) {
											$.each(data.results, function(index, item) {
												if (item.type = 'group') {
													if (item.url === undefined) {
														html += '<li class="group">' + (item.title) + ' (' + item.count + ')</li>';
													} else {
														html += '<li class="group link"><a href="' + item.url + '">' + (item.title) + ' (' + item.count + ')</a></li>';
													}
													$.each(item.items, function(subIndex, subItem) {
														html += '<li class="item link"><a href="' + subItem.url + '">' + (subItem.title) + '</a></li>';
													});
												} else {
													html += '<li class="item link"><a href="' + item.url + '">' + (item.title) + '</a></li>';
												}
											});
											$(searchResults).html('<ul>' + html + '</ul>').show();
											$(searchResults).find('li.link:first').addClass('selected');
										} else if (siteMapResultsCount == 0){
											$(searchResults).hide();
										}

										$('#search-support').slideDown('slow');
									},
									null, 'GET'
								);
							}, 500);
						} else {
							$(searchResults).hide();
						}
					}
				}
			});

			$('#search-results').on('mouseover', 'ul li.link', function () {
				$('#search-results ul').find('.selected').removeClass('selected');
				$(this).addClass('selected');
			});
		});
	};

	showSearch = function() {
		$('#field-search-text').val('');
		$('#search-results').html('');
		$('#search-results,#search-support ').hide();
		$('#modal-admin-area-search').modal('show');

		setTimeout(function(){
			$('[data-toggle="search-tooltip"]').tooltip('show');
		}, 4000);
	};

	init();

	return {

	};

}(jQuery));
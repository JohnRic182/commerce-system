var CategoriesProductsFeaturesGroups = (function($) {
	var init, renderCategoryProductsFeaturesGroupsTable;

	init = function() {
		$(document).ready(function() {

			// init values
			if (typeof categoryProductsFeaturesGroupsAvailable == 'undefined' || categoryProductsFeaturesGroupsAvailable === null) categoryProductsFeaturesGroupsAvailable = [];
			if (typeof categoryProductsFeaturesGroupsAssigned == 'undefined' || categoryProductsFeaturesGroupsAssigned === null) categoryProductsFeaturesGroupsAssigned = [];

			// set select element options
			if (categoryProductsFeaturesGroupsAvailable.length > 0) {
				var assigned = [];
				if (categoryProductsFeaturesGroupsAssigned.length > 0) {
					$.each(categoryProductsFeaturesGroupsAssigned, function (index, feature) {
						assigned.push(feature.product_feature_group_id);
					});
				}

				var options = '';
				$.each(categoryProductsFeaturesGroupsAvailable, function(index, feature) {
					var selected = $.inArray(feature.product_feature_group_id, assigned) != -1;
					options +=

						'<option value="' + feature.product_feature_group_id + '" ' + (selected ? ' selected' : '') + '>' +
							feature.feature_group_name + ' (' + feature.feature_group_id + ')' +
						'</option>';
				});

				$('#select-category-products-features-group-features').html(options);
				$('#select-category-products-features-group-features').DualListBox({json: false}, []);
				$('#modal-assign-feature-groups span.unselected-title').text(trans.features_groups.available_features_groups);
				$('#modal-assign-feature-groups span.selected-title').text(trans.features_groups.assign_features_groups);
			}

			// assign button action
			$('a[data-action="action-category-product-feature-group-assign"]').click(function() {
				if (categoryProductsFeaturesGroupsAvailable.length > 0) {
					$('#modal-assign-feature-groups').modal('show');
				} else {
					AdminForm.displayAlert('In order to assign products features groups, please add them first!');
				}
				return false;
			});

			// save assigned groups
			$('#modal-assign-feature-groups').find('form').submit(function() {
				var theForm = $(this);
				var selected = '';

				theForm.find('select.selected option').each(function(index, el) {
					selected += (selected == '' ? '' : ',') + $(el).val();
				});

				AdminForm.sendRequest(
					'admin.php?p=category&mode=assign-product-feature-group',
					{
						cid: $('#field-cid_products_features_groups').val(),
						products_features_groups: selected
					},
					'Adding products features groups to a category',
					function(data) {
						if (data.status) {
							categoryProductsFeaturesGroupsAssigned = data.categoryProductsFeaturesGroupsAssigned;
							renderCategoryProductsFeaturesGroupsTable(categoryProductsFeaturesGroupsAssigned);

							$('#modal-assign-feature-groups').modal('hide');
							AdminForm.displayAlert(trans.common.success);
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: key, message: message});
								});
							});

							AdminForm.displayModalErrors($('#modal-assign-feature-groups').modal(), errors);
						}
					}
				);

				return false;
			});

			// handle table actions
			$('.table-category-products-features-groups')
				.on('click', 'a[data-action="action-category-product-feature-group-delete"]', function(){
					var groupIndex = $(this).attr('data-target');

					if (typeof categoryProductsFeaturesGroupsAssigned[groupIndex] != undefined) {
						var group = categoryProductsFeaturesGroupsAssigned[groupIndex];

						AdminForm.confirm(trans.category.confirm_delete_product_feature_group, function(){
							AdminForm.sendRequest(
								'admin.php?p=category&mode=delete-product-feature-group',
								{
									'cid': group.cid,
									'product_feature_group_id': group.product_feature_group_id
								},
								'Deleting products features group',
								function(data){
									if (data.status) {
										categoryProductsFeaturesGroupsAssigned = data.categoryProductsFeaturesGroupsAssigned;
										renderCategoryProductsFeaturesGroupsTable(categoryProductsFeaturesGroupsAssigned);

										AdminForm.displayAlert(trans.common.success);
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-group-features-' + key, message: message});
											});
										});
										AdminForm.displayErrors(errors);
									}
								}
							);
						});
					}

					return false;
				});

			renderCategoryProductsFeaturesGroupsTable(categoryProductsFeaturesGroupsAssigned);

		});
	};

	/**
	 * Render table
	 *
	 * @param categoryProductsFeaturesGroups
	 */
	renderCategoryProductsFeaturesGroupsTable = function(categoryProductsFeaturesGroups) {
		var groupsTable = $('.table-category-products-features-groups');
		var assignFeatures = [];
		var deletedFeatures = [];

		if (categoryProductsFeaturesGroups && categoryProductsFeaturesGroups.length == 0) {
			$(groupsTable).hide();
			$('.none-product-group-features').show();
		} else {
			$(groupsTable).find('tbody').html('');
			$('.none-product-group-features').hide();
			var priorityOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
			$(categoryProductsFeaturesGroups).each(function(index, group) {
				var html = $("<tr>");
				html.append('<td>' + htmlentities(group.feature_group_name) + '</td>');
				html.append('<td>' + htmlentities(group.feature_group_id) + '</td>');

				var dropDownOptions = $('<td class="form-group field-select">');
				dropDownOptions.append(AdminForm.dropDownOptions("product_features_priorities["+ group.product_feature_group_id +"]", "priority-" + group.product_feature_group_id, priorityOptions, group.priority));

				html.append(dropDownOptions);
				html.append('<td class="list-actions text-right"><a href="#" data-action="action-category-product-feature-group-delete" data-target="' + index + '"><span class="icon icon-delete"></span>Delete</a> </td>');

				$(html).appendTo('.table-category-products-features-groups tbody');

				assignFeatures.push(group.product_feature_group_id);
			});
			$(groupsTable).show();
		}

		// update dual box as well
		$('#modal-assign-feature-groups select.selected option').each(function(index, el) {
			var dropDownValue = $(el).val();
			var dropDownText = $(el).text();

			// returns -1 if the item is not found
			if (assignFeatures.indexOf(dropDownValue) < 0) {
				deletedFeatures.push({value: dropDownValue, text: dropDownText});
				$('#modal-assign-feature-groups select.unselected').append($('<option>', {value: dropDownValue, text: dropDownText}));
			}
		});

		$.each(deletedFeatures, function(index, el) {
			$('#modal-assign-feature-groups select.selected option[value="' + el.value + '"]').remove();
		});
	};

	init();

}(jQuery));

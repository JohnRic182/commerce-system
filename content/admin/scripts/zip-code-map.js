var ZipCodeMap = (function($) {
	var init, theMap, thePolygons = {}, fillZips, fillZip, clearZips, clearZip, registerPolygonClick, loadPolygons;

	var polygonClickCallback = null, loading = false;

	var defaultStrokeColor = '#3c5f87', defaultFillColor = '#ffffff';

	init = function() {
		$(document).ready(function() {
			L.mapbox.accessToken = 'pk.eyJ1IjoicGFydG5lcmFwaXVwZGF0ZXMiLCJhIjoiUm1fTk9ESSJ9.AYAS--TxoTQxC5z-zb7L5w';
			theMap = L.mapbox.map('pikfly-map', 'partnerapiupdates.b98bf263');

			var geocoder = L.mapbox.geocoder('mapbox.places-v1');
			geocoder.query(originAddress, function(err, data) {
				if (data.latlng) {
					theMap.setView(data.latlng, 12);
					L.marker(data.latlng, {
						clickable: false,
						draggable: false,
						keyboard: false,
						title: 'Business Location',
						alt: 'Business Location'
					}).addTo(theMap);
				}
				loadPolygons();

				theMap.on('moveend', function(evt) {
					loadPolygons();
				});
			});
		});
	};

	fillZips = function(zips, fillColor, strokeColor) {
		$.each(zips, function(idx, zip) {
			fillZip(zip, fillColor, strokeColor);
		});
	};

	fillZip = function(zip, fillColor, strokeColor) {
		if (thePolygons.hasOwnProperty(zip)) {
			thePolygons[zip].setStyle({
				color: defaultStrokeColor,
				opacity: 1,
				weight: 1,
				fillColor: fillColor,
				fillOpacity: 0.2,
				dashArray: '5,5'
			});

			return true;
		}

		return false;
	};

	clearZips = function(zips) {
		$.each(zips, function(idx, zip) {
			fillZip(zip, defaultFillColor, defaultStrokeColor);
		});
	};

	clearZip = function(zip) {
		fillZip(zip, defaultFillColor, defaultStrokeColor);
	};

	registerPolygonClick = function(callback) {
		polygonClickCallback = callback;
	};

	loadPolygons = function() {
		if (loading) return;

		loading = true;
		var bounds = theMap.getBounds();
		var north = bounds.getNorth();
		var west = bounds.getWest();
		var east = bounds.getEast();
		var south = bounds.getSouth();
		$.ajax({
			url: pikflyApiUrl+'zip-codes.json?north='+north+'&east='+east+'&south='+south+'&west='+west,
			method: 'GET',
			dataType: 'json',
			beforeSend: function(xhr, settings) {
				xhr.setRequestHeader("X-Access-Token", "Bearer "+pikflyToken);
			},
			success: function(data) {
				$.each(thePolygons, function(idx, thePolygon) {
					thePolygon.off('click').off('mouseover').off('mouseout');
					theMap.removeLayer(thePolygon);
				});

				$.each(data.zip_codes, function(idx, zip) {
					var zipCoordinates = [];
					for (var j = 0; j < zip.boundaries.length; j++) {
						zipCoordinates.push({lat: zip.boundaries[j].lat, lng: zip.boundaries[j].lng});
					}
					var polygon = L.polygon(zipCoordinates,{
						color: defaultStrokeColor,
						opacity: 1,
						weight: 1,
						fillColor: defaultFillColor,
						fillOpacity: 0.2,
						dashArray: '5,5',
						zip: zip.zip_code
					});
					polygon.addTo(theMap);

					thePolygons[zip.zip_code] = polygon;

					polygon.on('click', function() {
						if (polygonClickCallback !== null) {
							polygonClickCallback(this.options.zip);
						}
					}).on('mouseover', function() {
						$('#zip').html(this.options.zip);
					}).on('mouseout', function() {
						$('#zip').html('');
					});

					$('body').trigger('map-changed');
				});

				loading = false;
			}
		});
	};

	init();

	return {
		fillZips: fillZips,
		fillZip: fillZip,
		clearZips: clearZips,
		clearZip: clearZip,
		registerPolygonClick: registerPolygonClick
	};
}(jQuery));
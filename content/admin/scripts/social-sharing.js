var SocialSharing = (function($) {
	var init;

	init = function() {
		$(document).ready(function() {
			var handleFacebookCommentsToggle = function() {
				var enabled = $('#field-facebookCommentsProduct').is(':checked');

				$('#field-facebookAppId').closest('.form-group').toggle(enabled);
			};
			$('#field-facebookCommentsProduct').change(function() { handleFacebookCommentsToggle(); });
			handleFacebookCommentsToggle();
		});
	};

	init();
}(jQuery));
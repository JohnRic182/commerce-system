var BongoCheckout = (function($) {
	var init, isRunning = false, markStopped;

	init = function() {
		$('button[data-action="action-export-sync"]').click(function() {
			var theModal = $(this).closest('.modal');
			var exportMode = theModal.find('input[name="form-bongo-export-mode"]:checked').val();

			theModal.modal('hide');
			$('#iframe-download').attr('src', 'admin.php?p=bongo&mode=export&form-bongo-export-mode=' + exportMode + '&chart=1');
			return false;
		});

		$('button[data-action="action-bongo-sync"]').click(function() {
			if (isRunning) return false;

			isRunning = true;
			var theModal = $(this).closest('.modal');

			$(this).attr('disable', 'disable');

			var syncMode = theModal.find('input[name="form-bongo-sync-mode"]:checked').val();

			AdminForm.sendRequest(
				'admin.php?p=bongo&mode=sync-start&form-bongo-sync-mode=' + syncMode + '&chart=1',
				{},
				'Please wait',
				function(data) {
					var totalProducts = parseInt(data.count);

					if (totalProducts > 0) {
						BongoApiExport.init(theModal, syncMode, totalProducts);
					} else {
						theModal.modal('hide');
					}
				}
			);

			return false;
		});

		$('#modal-bongo-sync').on('show.bs.modal', function() {
			var theModal = $(this);

			if (!isRunning) {
				theModal.find('.progress').hide();
				theModal.find('button[data-action="action-bongo-sync"]').removeAttr('disable');
			}
		});
	};

	markStopped = function() {
		isRunning = false;
	};

	init();

	return {
		markStopped: markStopped
	};
}(jQuery));

var BongoApiExport = (function() {
	var processBongoApiExport, totalProducts, syncMode, totalProcessed;

	var theModal, theProgressBar, theProgressContainer, theProgressLabel;

	init = function(modal, mode, products) {
		theModal = modal;
		totalProducts = products;
		totalProcessed = 0;
		syncMode = mode;

		theProgressBar = theModal.find('.progress-bar');
		theProgressContainer = theModal.find('.progress');
		theProgressLabel = theModal.find('.progress-bar span');

		theProgressLabel.html('0% Completed');
		theProgressBar.css('width', 0);
		theProgressContainer.show();

		processBongoApiExport();
	};

	processBongoApiExport = function() {
		AdminForm.sendRequest(
			'admin.php?p=bongo&mode=sync-batch&form-bongo-sync-mode=' + syncMode + '&offset='+totalProcessed + '&chart=1',
			{},
			'Please wait',
			function(data) {
				var processed = parseInt(data.processed);

				if (processed > 0) {
					totalProcessed = processed;

					if (totalProcessed < totalProducts) {
						var percentage = Math.round(totalProcessed / totalProducts * 100);
						theProgressLabel.html(percentage+'% Completed');
						theProgressBar.css('width', percentage+'%');

						processBongoApiExport();
					} else {
						theProgressBar.css('width', '100%');

						BongoCheckout.markStopped();

						setTimeout(function() {
							theModal.modal('hide');
						}, 500);
					}
				} else {
					theModal.modal('hide');
				}
			}
		);
	};

	return {
		init: init
	};
}());
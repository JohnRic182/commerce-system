(function($) {
	var init;

	init = function() {
		$(document).ready(function() {
			$('#modal-company-shipping-address').on('show.bs.modal', function() {
				var theForm = $('#form-company-shipping-address');
				theForm.get(0).reset();

				forms.initAddressForm(theForm);
			});

			$('#modal-company-shipping-address').on('hidden.bs.modal', function (e) {
				$('#modal-company-shipping-address').find('.form-group .warning-sign').addClass('hidden');
				$('#modal-company-shipping-address').find('.form-group .error').remove();
			});

			$('#form-company-shipping-address').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				var errors = [];
				if ($.trim($('#field-ShippingOriginName').val()) == '') {
					errors.push({
						field: '#field-ShippingOriginName',
						message: trans.shipping_address.enter_shipping_orig_name
					});
				}
				if ($.trim($('#field-ShippingOriginAddressLine1').val()) == '') {
					errors.push({
						field: '#field-ShippingOriginAddressLine1',
						message: trans.shipping_address.enter_street_address
					});
				}
				if ($.trim($('#field-ShippingOriginCity').val()) == '') {
					errors.push({
						field: '#field-ShippingOriginCity',
						message: trans.shipping_address.enter_city
					});
				}
				var provinceField = $('#field-ShippingOriginProvince');
				if (provinceField.closest('.form-group').is(':visible') && $.trim(provinceField.val()) == '') {
					errors.push({
						field: '#field-ShippingOriginProvince',
						message: trans.shipping_address.enter_province
					});
				}
				if ($.trim($('#field-ShippingOriginZip').val()) == '' && $('#field-ShippingOriginZip').attr('data-required') == 1) {
					errors.push({
						field: '#field-ShippingOriginZip',
						message: trans.shipping_address.enter_zipcode
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
					return false;
				}

				return true;
			});
		});
	};

	init();
}(jQuery));

$(document).ready(function(){

	moment().format();

	var plotOrders = null;
	var plotRevenue = null;
	var plotItems = null;
	var plotMain = null;

	$.jqplot.config.enablePlugins = true;

	$('#home-page-graph-progress').circleProgress({
		value: 0.6
	}).on('circle-animation-progress', function(event, progress) {
		$(this).find('span').html(parseInt(100 * progress * 0.6) + '<i>%</i>');
	});

	$('.widget').each(function(index, widget) {
		setTimeout(function(){
			$(widget).slideDown(500);
		}, index * 200);
	});

	$('.home-page-graph-container-buttons a').click(function(){
		$('.home-page-graph-container-buttons a').removeClass('btn-primary');
		$(this).addClass('btn-primary');
		loadGraph($(this).attr('href').substr(1));
	});

	/**
	 * Load graph function
	 *
	 * @param period
	 */
	var loadGraph = function(period) {

		var dateFrom;
		var mainGrapthTickInterval;
		var mainGraphBarMargin;
		var mainGraphBarWidth;
		var mainGraphMarkerSize;
		var microGraphBarWidth;
		var gapDays;

		switch (period) {
			case 'week' : {
				dateFrom = moment().subtract(7, 'days').format('Y-MM-DD');
				mainGrapthTickInterval = '1 days';
				mainGraphBarWidth = mainGraphBarMargin = 15;
				mainGraphMarkerSize = 7;
				microGraphBarWidth = 15;
				gapDays = 0;
				break;
			}
			case 'quarter' : {
				dateFrom = moment().subtract(3, 'months').format('Y-MM-DD');
				mainGrapthTickInterval = '7 days';
				mainGraphBarWidth = mainGraphBarMargin = 6;
				mainGraphMarkerSize = 5;
				microGraphBarWidth = 2;
				gapDays = 0;
				break;
			}
			case 'year' : {
				dateFrom = moment().subtract(1, 'years').format('Y-MM-DD');
				mainGrapthTickInterval = '1 months';
				mainGraphBarWidth = mainGraphBarMargin = 2;
				mainGraphMarkerSize = 2;
				microGraphBarWidth = 2;
				gapDays = 0;
				break;
			}
			default:
			case 'month' : {
				dateFrom = moment().subtract(1, 'months').format('Y-MM-DD');
				mainGrapthTickInterval = '2 days';
				mainGraphBarWidth = mainGraphBarMargin = 15;
				mainGraphMarkerSize = 7;
				microGraphBarWidth = 6;
				gapDays = 1;
				break;
			}
		}

		var dateTo = moment().format('Y-MM-DD');
		var animationInterval = null;
		var animationPosition = 0;

		$.ajax({
			'url': 'admin.php?__ajax=1&p=report&report_type=dates&from=' + dateFrom + '&to=' + dateTo + '&filter[order_status]=any&export=1&export_type=json&',
			success: function (data) {

				var graphTotalPerDay = [];
				var graphOrdersPerDay = [];
				var graphItemsPerDay = [];

				var ordersCount = 0;
				var ordersTotal = 0;
				var itemsCount = 0;

				if (data.data && data.data.days) {

					$('#home-page-graph-title').text(data.title);
					$('#home-page-graph-title-shorten').text(data.title_shorten);
					$('#home-page-graph-title-xsmall').text(data.title_xsmall);

					var reportRunningDate = moment(data.range.from);
					var reportEndDate = moment(data.range.to);
					var daysDiff = reportEndDate.diff(reportRunningDate, 'days');

					for (var i = 0; i <= daysDiff; i++) {
						var valueFound = false;
						var dateFormatted = reportRunningDate.format('Y-MM-DD');

						$.each(data.data.days, function (index, day) {

							var reportDate = moment(day['report_date']);

							if (reportRunningDate.isSame(reportDate)) {
								graphTotalPerDay.push([dateFormatted, parseFloat(day['orders_total_amount'])]);
								graphOrdersPerDay.push([dateFormatted, parseInt(day['orders_count'])]);
								graphItemsPerDay.push([dateFormatted, parseInt(day['items_count'])]);

								ordersCount += parseInt(day['orders_count']);
								ordersTotal += parseFloat(day['orders_total_amount']);
								itemsCount += parseInt(day['items_count']);

								valueFound = true;
							}
						});

						if (!valueFound) {
							graphTotalPerDay.push([dateFormatted, 0]);
							graphOrdersPerDay.push([dateFormatted, 0]);
							graphItemsPerDay.push([dateFormatted, 0]);
						}

						reportRunningDate.add(1, 'd');
					}

					animationPosition = 0;

					animationInterval = setInterval(function () {
						$('.home-page-order-count').html(getAdminNumber(Math.round(ordersCount / 20 * animationPosition)));
						$('.home-page-items-count').html(getAdminNumber(Math.round(itemsCount / 20 * animationPosition)));
						$('.home-page-revenue').html(getAdminPrice(Math.round(ordersTotal / 20 * animationPosition)));

						animationPosition++;

						if (animationPosition == 20) {
							clearInterval(animationInterval);
							$('.home-page-order-count').html(getAdminNumber(ordersCount));
							$('.home-page-items-count').html(getAdminNumber(itemsCount));
							$('.home-page-revenue').html(getAdminPrice(ordersTotal));
						}
					}, 37);

					var rangeMin = moment(dateFrom).subtract(gapDays, 'day').format('Y-MM-DD');;
					var rangeMax = moment(dateTo).add(gapDays, 'day').format('Y-MM-DD');

					/**
					 * Revenue
					 */
					if (plotRevenue != null) plotRevenue.destroy();

					plotRevenue = $.jqplot('home-page-graph-revenue', [graphTotalPerDay], {
						animate: !$.jqplot.use_excanvas,
						grid: {
							show: false,
							background: '#ffffff',
							borderColor: '#e3e3e3',
							drawBorder: true,
							borderWidth: 1,
							shadow: false,
							gridLineWidth: 0
						},
						series: [{
							renderer: $.jqplot.BarRenderer,
							rendererOptions: {
								barPadding: 1,
								barMargin: 0,
								barDirection: 'vertical',
								barWidth: microGraphBarWidth,
								shadow: false
							}
						}],
						seriesColors: ['#777777'],
						axesDefaults: {showTickMarks: false},
						axes: {
							xaxis: {
								renderer: $.jqplot.DateAxisRenderer, min: rangeMin, max: rangeMax,
								tickOptions: { show: false },
								rendererOptions: { drawBaseline: false }
							},
							yaxis: {
								min: 0,
								tickOptions: { show: false },
								rendererOptions: { drawBaseline: false }
							},
						},
						cursor: {zoom: false}
					});

					/**
					 * Orders
					 */
					if (plotOrders != null) plotOrders.destroy();

					plotOrders = $.jqplot('home-page-graph-orders', [graphOrdersPerDay], {
						animate: !$.jqplot.use_excanvas,
						grid: {
							show: false,
							background: '#ffffff',
							borderColor: '#e3e3e3',
							drawBorder: true,
							borderWidth: 1,
							shadow: false,
							gridLineWidth: 0
						},
						series: [{
							renderer: $.jqplot.BarRenderer,
							rendererOptions: {
								barPadding: 1,
								barMargin: 0,
								barDirection: 'vertical',
								barWidth: microGraphBarWidth,
								shadow: false
							}
						}],
						seriesColors: ['#777777'],
						axesDefaults: {showTickMarks: false},
						axes: {
							xaxis: {
								renderer: $.jqplot.DateAxisRenderer, min: rangeMin, max: rangeMax,
								tickOptions: { show: false },
								rendererOptions: { drawBaseline: false }
							},
							yaxis: {
								min: 0,
								tickOptions: { show: false },
								rendererOptions: { drawBaseline: false }
							},
						},
						cursor: {zoom: false}
					});

					/**
					 * Items
					 */
					if (plotItems != null) plotItems.destroy();

					plotItems = $.jqplot('home-page-graph-items', [graphItemsPerDay], {
						animate: !$.jqplot.use_excanvas,
						grid: {
							show: false,
							background: '#ffffff',
							borderColor: '#e3e3e3',
							drawBorder: true,
							borderWidth: 1,
							shadow: false,
							gridLineWidth: 0
						},
						series: [{
							renderer: $.jqplot.BarRenderer,
							rendererOptions: {
								barPadding: 1,
								barMargin: 0,
								barDirection: 'vertical',
								barWidth: microGraphBarWidth,
								shadow: false
							}
						}],
						seriesColors: ['#777777'],
						axesDefaults: { showTickMarks: false },
						axes: {
							xaxis: {
								renderer: $.jqplot.DateAxisRenderer, min: rangeMin, max: rangeMax,
								tickOptions: { show: false },
								rendererOptions: { drawBaseline: false }
							},
							yaxis: {
								min: 0,
								tickOptions: { show: false },
								rendererOptions: { drawBaseline: false }
							},
						},
						cursor: {zoom: false}
					});

					/**
					 * Main graph
					 */
					if (plotMain != null) plotMain.destroy();

					plotMain = $.jqplot('home-page-graph-main', [graphTotalPerDay, graphOrdersPerDay], {
						//title: data.title,
						animate: !$.jqplot.use_excanvas,
						legend: { show: true, placement: 'inside' },
						grid: {
							background: '#f9f9f9',
							drawBorder: true,
							shadow: false,
							gridLineColor: '#dddddd',
							gridLineWidth: 1,
							borderWidth: 0
						},
						series: [
							{
								label: 'Sales per day',
								renderer: $.jqplot.BarRenderer,
								rendererOptions: {
									barPadding: 1,
									barMargin: mainGraphBarMargin,
									barDirection: 'vertical',
									barWidth: mainGraphBarWidth,
									shadow: false,
									animation: {
										speed: 500
									}
								},
							},
							{
								label: 'Order per day',
								xaxis: 'x2axis',
								yaxis: 'y2axis',
								lineWidth: 1,
								rendererOptions: {
									smooth: true,
									animation: {
										speed: 500
									}
								},
								markerOptions: { style: 'filledCircle', size: mainGraphMarkerSize }
							}
						],
						seriesColors: ['#AEC690', '#0099cc'],
						axes: {
							xaxis: {
								renderer: $.jqplot.DateAxisRenderer,
								tickOptions: { formatString: '%b %#d, ’%y' },
								min: rangeMin,
								max: rangeMax,
								tickInterval: mainGrapthTickInterval
							},
							yaxis: { tickOptions: {prefix: '$'}, min: 0	},
							x2axis: {
								renderer: $.jqplot.DateAxisRenderer,
								tickOptions: { formatString: ' ' },
								min: rangeMin,
								max: rangeMax,
								tickInterval: mainGrapthTickInterval,
								drawMajorTickMarks: false
							},
							y2axis: {
								min: 0,
								tickOptions: { showGridline: false }
							}
						},

						cursor: { zoom: true, looseZoom: true }
					});
				}
			}
		});
	};

	loadGraph($('.home-page-graph-container-buttons a.btn-primary').attr('href').substr(1));

	$(window).resize(function () {
		if (plotOrders != null) plotOrders.replot({resetAxes: false});
		if (plotRevenue != null) plotRevenue.replot({resetAxes: false});
		if (plotItems != null) plotItems.replot({resetAxes: false});
		if (plotMain != null) plotMain.replot({resetAxes: false});
	});
});

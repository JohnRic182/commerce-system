var tables = (function() {
	var init, filter, searchFormInit;

	init = function() {
		$(document).ready(function () {
			$('.table-list').each(function(){
				var table = this;

				$(table).find('thead .sortable').each(function(){
					$(this).html(
						'<a href="#" class="link-sort">' + $(this).html() + '</a>' +
						'&nbsp;<a href="#" class="link-sort-asc"><span class="glyphicon glyphicon-triangle-top"></span></a>'  +
						'<a href="#" class="link-sort-desc"><span class="glyphicon glyphicon-triangle-bottom disabled"></span></a>'
					);
				});

				var searchFormSelector= $(table).attr('data-search-form');
				var orderByField = $(table).attr('data-order-by-field');
				var orderDirField = $(table).attr('data-order-dir-field');

				var searchForm = $(searchFormSelector);
				var searchFormInitialParams = {};
				searchFormInitialParams = searchFormInit(searchForm, searchFormInitialParams);
				$(table).data('searchFormInitialParams', searchFormInitialParams);

				$('.field-searchParams-advanced_search').change(function() {
					var searchFormInitialParams = {};
					searchFormInitialParams = searchFormInit(searchForm, searchFormInitialParams);
					$(table).data('searchFormInitialParams', searchFormInitialParams);
				});

				var orderBy = $(searchForm).find('input[name="' + orderByField + '"]').val();
				var orderDir = $(searchForm).find('input[name="' + orderDirField + '"]').val();

				$(table).find('th[data-sort-field="' + orderBy + '"] .link-sort-' + orderDir).addClass('link-sort-active');

				/**
				 * Link
				 */
				$(table).find('.link-sort').each(function(){
					$(this).click(function(event){
						event.preventDefault();
						searchFormInitialParams = $(table).data('searchFormInitialParams');
						searchFormInitialParams[orderByField] = $(this).closest('th').attr('data-sort-field');
						searchFormInitialParams[orderDirField] = $(this).parent().find('.link-sort-asc').hasClass('link-sort-active') ? 'desc' : 'asc';
						document.location = '?' + $.param(searchFormInitialParams);
						return false;
					});
				});

				/**
				 * Arrow up
				 */
				$(table).find('.link-sort-asc').each(function(){
					$(this).click(function(event){
						event.preventDefault();
						searchFormInitialParams = $(table).data('searchFormInitialParams');
						searchFormInitialParams[orderByField] = $(this).closest('th').attr('data-sort-field');
						searchFormInitialParams[orderDirField] = 'asc';
						document.location = '?' + $.param(searchFormInitialParams);
						return false;
					});
				});

				/**
				 * Arrow down
				 */
				$(table).find('.link-sort-desc').each(function(){
					$(this).click(function(event){
						event.preventDefault();
						searchFormInitialParams = $(table).data('searchFormInitialParams');
						searchFormInitialParams[orderByField] = $(this).closest('th').attr('data-sort-field');
						searchFormInitialParams[orderDirField] = 'desc';
						document.location = '?' + $.param(searchFormInitialParams);
						return false;
					});
				});
			});
		});
	};

	searchFormInit = function (searchForm, searchFormInitialParams) {
		$(searchForm).find('select,input').each(function(){
			var input = this;
			if ($(input).attr('name') == 'searchParams[advanced_search]') {
				if($('.field-searchParams-advanced_search').next().hasClass('checked')) {
					searchFormInitialParams[$(input).attr('name')] = 'on';
				} else {
					searchFormInitialParams[$(input).attr('name')] = 'off';
				}
			} else {
				searchFormInitialParams[$(input).attr('name')] = $(input).val();
			}
		});

		return searchFormInitialParams;
	};

	/**
	 * TODO: filters
	 */
	filter = function() {
		$('.table-list tbody tr').each(function(){
			var tr = this;

			var trVisible = false;

			$(tr).find('td').each(function(){
				var td = this;

				var tdText = $(td).text();

				if (tdText.indexOf('_1') >= 0) {
					trVisible = true;
				}
			});

			$(tr).toggle(trVisible);

		});
	};

	init();

	return {
		init: init
	};
}());


var OrderNotifications = (function() {
	var init, removeOrderNotification;

	init = function() {
		$('form[name="form-settings"]').submit(function() {
			var theForm = $(this);
			var theModal = $(this).closest('.modal');

			AdminForm.sendRequest(
				theForm.attr('action'),
				theForm.serialize(),
				null,
				function(data) {
					if (data.status == 1) {
						AdminForm.hideSpinner();
						theModal.modal('hide');
						AdminForm.displayAlert(data.message);
					} else {
						AdminForm.hideSpinner();
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				},
				function() {
					AdminForm.hideSpinner();
				}
			);

			return false;
		});

		$('[data-action="action-notifications-delete"]').click(function(){
			var deleteMode = $('input[name="form-notifications-delete-mode"]:checked').val();
			if (deleteMode == 'selected')
			{
				var ids = '';
				$('.checkbox-notification:checked').each(function(){
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				if (ids != '')
				{
					window.location='admin.php?p=order_notifications&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
				}
				else
				{
					alert(trans.order_notifications.select_order_notification);
				}
			}
			else if (deleteMode == 'search')
			{
				window.location='admin.php?p=order_notifications&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
			}
		});
	};

	removeOrderNotification = function(id, nonce) {
		AdminForm.confirm(trans.order_notifications.confirm_delete_notification, function() {
			window.location = 'admin.php?p=order_notifications&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});
		return false;
	};

	$(document).ready(function() {
		init();
	});

	return {
		removeOrderNotification: removeOrderNotification
	};
}(jQuery));

var ProductsAttributes = (function($) {
	var init, renderProductAttributesTable;

	init = function() {
		$(document).ready(function() {
//			$('#attribute-options-popover').popover({
//				animation:true,
//				html: true,
//				placement: 'right'
//			});
//
//			$('#field-attribute-options')
//				.focus(function(){
//					$('#attribute-options-popover').popover('show');
//				})
//				.blur(function(){$('#attribute-options-popover').popover('hide');});

			$('#modal-product-attribute').on('hidden.bs.modal', function (e) {
				$('#modal-product-attribute').find('.form-group .error').remove();
			});

			$('a[data-action="action-product-attribute-add"]').click(function() {
				$('#field-attribute-name,#field-attribute-caption,#field-attribute-text_length,#field-attribute-options').val('');
				$('#field-attribute-id').val('new');
				$('#field-attribute-attribute_type').val('select').change();
				$('#field-attribute-priority').val(5);
				$('#field-attribute-is_active').prop('checked', true).change();
				$('#field-attribute-track_inventory').prop('checked', true).change();

				$('#field-attribute-attribute_required').prop('checked', false).change();

				$('#modal-product-attribute').modal('show');
				return false;
			});

			$('a[data-action="action-global-attribute-add"]').click(function() {
				var productGlobalAttributesEle = $('#modal-product-global-attributes');
				productGlobalAttributesEle.find('input[type="checkbox"]').each(function(index, el) {
					$(el).prop('checked', false).change();
				});
				productGlobalAttributesEle.modal('show');

				return false;
			});

			$('.table-product-attributes')
				.on('click', 'a[data-action="action-product-attribute-edit"]', function() {
					var attributeIndex = $(this).attr('data-target');

					if (productAttributes[attributeIndex] != undefined) {
						var attribute = productAttributes[attributeIndex];

						$('#field-attribute-pid').val(attribute.pid);
						$('#field-attribute-name').val(attribute.name);
						$('#field-attribute-caption').val(attribute.caption);
						$('#field-attribute-text_length').val(attribute.text_length);
						$('#field-attribute-options').val(attribute.options);
						$('#field-attribute-id').val(attribute.paid);
						$('#field-attribute-attribute_type').val(attribute.attribute_type).change();
						$('#field-attribute-priority').val(attribute.priority);
						$('#field-attribute-is_active').prop('checked', attribute.is_active == 'Yes').change();

						$('#field-attribute-attribute_required').prop('checked', attribute.attribute_required == 'Yes').change();

						$('#field-attribute-track_inventory').prop('checked', attribute.track_inventory == '1').change();
						$('#modal-product-attribute').modal('show');
					}

					return false;
				})
				.on('click', 'a[data-action="action-product-attribute-delete"]', function() {
					var attributeIndex = $(this).attr('data-target');

					if (productAttributes[attributeIndex] != undefined) {
						var attribute = productAttributes[attributeIndex];

						AdminForm.confirm(trans.products_attributes.confirm_remove, function() {
							AdminForm.sendRequest(
								'admin.php?p=product_page_attributes&mode=attribute-delete',
								{'attribute[id]': attribute.paid, 'attribute[pid]': attribute.pid},
								'Deleting product attribute',
								function(data) {
									if (data.status) {
										productAttributes = data.attributes;
										renderProductAttributesTable(productAttributes);
										AdminForm.displayAlert(trans.common.success);
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-' + key, message: message});
											});
										});
										AdminForm.displayErrors(errors);
									}
								}
							);
						});
					}

					return false;
				});

			$('#field-attribute-attribute_type').change(function() {
				var type = $(this).val();
				$('#field-attribute-text_length').closest('.form-group').toggle(type == 'text' || type == 'textarea');
				$('#field-attribute-track_inventory').closest('.form-group').toggle(type == 'select' || type == 'radio');
				$('#field-attribute-options').closest('.form-group').toggle(type == 'select' || type == 'radio');
			}).change();

			$('#modal-product-attribute').find('form').submit(function() {
				AdminForm.sendRequest(
					'admin.php?p=product_page_attributes&mode=attribute-' + ($('#field-attribute-id').val() == 'new' ? 'add' : 'update'),
					$('#modal-product-attribute').find('form').serialize(),
					'Saving product attribute',
					function(data) {
						if (data.status) {
							productAttributes = data.attributes;
							renderProductAttributesTable(productAttributes);
							$('#modal-product-attribute').modal('hide');

							AdminForm.displayAlert(trans.common.success);
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: '.field-attribute-' + key, message: message});
								});
							});

							AdminForm.displayValidationErrors(errors);
						}
					}
				);

				return false;
			});

			$('#modal-product-global-attributes').find('form').submit(function() {
				var theForm = $(this);
				var selected = '';
				theForm.find('input[type="checkbox"]:checked').each(function(index, el) {
					selected += (selected == '' ? '' : ',') + $(el).val();
				});

				if (selected == '') {
					var errors = [];
					errors.push({field: '', message: trans.products_attributes.choose_global_attribute});
					AdminForm.displayModalErrors($('#modal-product-global-attributes').modal(), errors)
				} else {
					AdminForm.sendRequest(
						'admin.php?p=product_page_attributes&mode=attribute-add-global',
						{productId: $('#field-global_attribute_pid').val(), globalAttributes: selected},
						'Adding global attribuites',
						function(data) {
							if (data.status) {
								productAttributes = data.attributes;
								renderProductAttributesTable(productAttributes);
								$('#modal-product-global-attributes').modal('hide');
								AdminForm.displayAlert(trans.common.success);
							} else {
								var errors = [];
								$.each(data.errors, function(key, value) {
									$.each(value, function(idx, message) {
										errors.push({field: '.field-attribute-' + key, message: message});
									});
								});

								AdminForm.displayModalErrors($('#modal-product-global-attributes').modal(), errors);
							}
						}
					);
				}

				return false;
			});

			if (productAttributes == undefined) productAttributes = [];

			renderProductAttributesTable(productAttributes);
		});
	};

	renderProductAttributesTable = function(attributes) {
		var types = [];
		types['select'] = 'Drop-down';
		types['radio'] = 'Radio buttons';
		types['text'] = 'Text input';
		types['textarea'] = 'Textarea input';

		if (attributes.length == 0) {
			$('.table-product-attributes,.group-variants').hide();
		} else {
			$('.table-product-attributes tbody').html('');
			$(attributes).each(function(index, attribute) {
				var html =
					'<tr>' +
						'<td><a href="#" data-action="action-product-attribute-edit" data-target="' + index + '">' + htmlentities(attribute.name) + '</a></td>' +
						'<td><a href="#" data-action="action-product-attribute-edit" data-target="' + index + '">' + htmlentities(attribute.caption) + '</a></td>' +
						'<td>' + htmlentities(attribute.priority) + '</td>' +
						'<td>' + htmlentities(types[attribute.attribute_type]) + (attribute.is_modifier == 'Yes' ? ' / price modifier' : '') +'</td>' +
						'<td>' + htmlentities(attribute.track_inventory == '1' ? 'Yes' : 'No') + '</td>' +
						'<td>' + htmlentities(attribute.is_active) + '</td>' +
						'<td>' + htmlentities(attribute.attribute_required) + '</td>' +
						'<td class="list-actions text-right">' +
							'<a href="#" data-action="action-product-attribute-edit" data-target="' + index + '"><span class="icon icon-edit"></span>Edit</a> '+
							'<a href="#" data-action="action-product-attribute-delete" data-target="' + index + '"><span class="icon icon-delete"></span>Delete</a>' +
						'</td>' +
					'</tr>';
				$(html).appendTo('.table-product-attributes tbody');
			});
			$('.table-product-attributes,.group-variants').show();
		}
	};

	init();

}(jQuery));

var ProductsQuantityDiscounts = (function($) {
	var init, renderProductQuantityDiscountsTable;

	init = function() {
		$(document).ready(function() {
			$('#modal-product-quantity-discount').on('hidden.bs.modal', function () {
				$('#modal-product-quantity-discount').find('.form-group .error').remove();
			});

			$('a[data-action="action-product-quantity-discount-add"]').click(function() {
				$('#field-qd-range_min,#field-qd-range_max,#field-qd-discount').val('');
				$('#field-qd-discount_type').val('percent').change();
				$('#field-qd-apply_to_wholesale').prop('checked', false).change();
				$('#field-qd-free_shipping').prop('checked', false).change();
				$('#field-qd-is_active').prop('checked', true).change();
				$('#field-qd-id').val('new');
				$('#modal-product-quantity-discount').modal('show');
				return false;
			});

			$('.table-product-quantity-discounts')
				.on('click', 'a[data-action="action-product-quantity-discount-edit"]', function() {
					var qdIndex = $(this).attr('data-target');

					if (productQuantityDiscounts[qdIndex] != undefined) {
						var qd = productQuantityDiscounts[qdIndex];

						$('#field-qd-pid').val(qd.pid);
						$('#field-qd-id').val(qd.qd_id);

						$('#field-qd-range_min').val(qd.range_min);
						$('#field-qd-range_max').val(qd.range_max);
						$('#field-qd-discount').val(qd.discount);
						$('#field-qd-discount_type').val(qd.discount_type);
						$('#field-qd-free_shipping').prop('checked', qd.free_shipping == 'Yes').change();
						$('#field-qd-apply_to_wholesale').prop('checked', qd.apply_to_wholesale == 'Yes').change();
						$('#field-qd-is_active').prop('checked', qd.is_active == 'Yes').change();
						$('#modal-product-quantity-discount').modal('show');
					}

					return false;
				})
				.on('click', 'a[data-action="action-product-quantity-discount-delete"]', function() {
					var qdIndex = $(this).attr('data-target');

					if (productQuantityDiscounts[qdIndex] != undefined) {
						var qd = productQuantityDiscounts[qdIndex];

						AdminForm.confirm(trans.products_quantity_discounts.confirm_remove_quantity_discount, function() {
							AdminForm.sendRequest(
								'admin.php?p=product_page_qd&mode=qd-delete',
								{'qd[id]': qd.qd_id, 'qd[pid]': qd.pid},
								'Deleting quantity discount?',
								function(data) {
									if (data.status) {
										productQuantityDiscounts = data.discounts;
										renderProductQuantityDiscountsTable(productQuantityDiscounts);
										AdminForm.displayAlert(trans.common.success);
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-qd-' + key, message: message});
											});
										});

										AdminForm.displayErrors(errors);
									}

									return false;
								}
							);
						});
					}

					return false;
				});

			$('#modal-product-quantity-discount').find('form').submit(function() {
				AdminForm.sendRequest(
					'admin.php?p=product_page_qd&mode=qd-' + ($('#field-qd-id').val() == 'new' ? 'add' : 'update'),
					$('#modal-product-quantity-discount').find('form').serialize(),
					'Saving quantity discount',
					function(data) {
						if (data.status) {
							productQuantityDiscounts = data.discounts;
							renderProductQuantityDiscountsTable(productQuantityDiscounts);
							$('#modal-product-quantity-discount').modal('hide');
							AdminForm.displayAlert(trans.common.success);
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: '.field-qd-' + key, message: message});
								});
							});

							AdminForm.displayValidationErrors(errors);
						}
					}
				);

				return false;
			});

			if (productQuantityDiscounts == undefined) productQuantityDiscounts = [];

			renderProductQuantityDiscountsTable(productQuantityDiscounts);
		});
	};

	renderProductQuantityDiscountsTable = function(discounts) {
		if (discounts.length == 0) {
			$('.table-product-quantity-discounts').hide();
		} else {
			$('.table-product-quantity-discounts tbody').html('');
			$(discounts).each(function(index, discount) {
				var html =
					'<tr>' +
						'<td><a href="#" data-action="action-product-quantity-discount-edit" data-target="' + index + '">' + htmlentities(discount.range_min) + '</a></td>' +
						'<td><a href="#" data-action="action-product-quantity-discount-edit" data-target="' + index + '">' + htmlentities(discount.range_max) + '</a></td>' +
						'<td>' + htmlentities(discount.discount + (discount.discount_type == 'percent' ? '%' : ' (amount)')) + '</td>' +
						'<td>' + htmlentities(discount.free_shipping == 'Yes' ? 'Yes' : 'No') + '</td>' +
						'<td>' + htmlentities(discount.apply_to_wholesale == 'Yes' ? 'Yes' : 'No') + '</td>' +
						'<td>' + htmlentities(discount.is_active == 'Yes' ? 'Yes' : 'No') + '</td>' +
						'<td class="list-actions text-right">' +
							'<a href="#" data-action="action-product-quantity-discount-edit" data-target="' + index + '"><span class="icon icon-edit"></span>Edit</a> '+
							'<a href="#" data-action="action-product-quantity-discount-delete" data-target="' + index + '"><span class="icon icon-delete"></span>Delete</a>' +
						'</td>' +
					'</tr>';
				$(html).appendTo('.table-product-quantity-discounts tbody');
			});
			$('.table-product-quantity-discounts').show();
		}
	};

	init();

}(jQuery));


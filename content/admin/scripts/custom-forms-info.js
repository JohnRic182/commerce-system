/**
 * Created by jim on 9/7/16.
 */
var CustomInfo = (function($) {
	var init;

	/**
	 * Initialize
	 */
	init = function() {
		$(document).ready(function() {
			$(window).load(function() {
				$('#modal-info').modal({
					show: true
				});
			});
		});
	};

	init();
}(jQuery));
$(document).ready(function(){
	$('form[name="form-customers-delete"],form[name="form-customers-export"]').submit(function(){
		return false;
	});

    $('#field-button_profile_add').click(function(event){
		event.preventDefault();
    });
	$('#modal-customers-search input[type=date]').datepicker({ dateFormat: 'yy-mm-dd' });

	/**
	 * Handle export button
	 */
	$('[data-action="action-customers-export"]').click(function(){
		var exportMode = $('input[name="form-customers-export-mode"]:checked').val();
		var nonce = $('#modal-customers-export').find('input[name="nonce"]').val();

		if (exportMode == 'selected'){
			var ids = '';
			$('.checkbox-customers:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != ''){
				window.location='admin.php?p=customer&mode=export&exportMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else{
				alert('Please select at least one customer to export');
			}
		}
		else if (exportMode == 'all'){
			window.location='admin.php?p=customer&mode=export&exportMode=all&nonce=' + nonce;
		}

		return false;
	});

	/**
	 * Handle delete button
	 */
	$('[data-action="action-customers-delete"]').click(function(){

		var deleteMode = $('input[name="form-customers-delete-mode"]:checked').val();

		if (deleteMode == 'selected'){
			var ids = '';
			$('.checkbox-customers:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != ''){
				window.location='admin.php?p=customer&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
			}
			else{
				alert('Please select at least one customer to delete');
			}
		}
		else if (deleteMode == 'search') {
			window.location='admin.php?p=customer&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
		}
	});

	$('#field-searchParams-from_date, #field-searchParams-to_date').datepicker({
		beforeShow: customRange,
		dateFormat: 'yy-mm-dd'
	});

});

function customRange (input) {
	if (input.id == 'field-searchParams-to_date') {
		return {
			minDate: jQuery('#field-searchParams-from_date').datepicker("getDate")
		};
	} else if (input.id == 'field-searchParams-from_date') {
		return {
			maxDate: jQuery('#field-searchParams-to_date').datepicker("getDate")
		};
	}
};

function EditPaymentProfile(id, uid) {
	AdminForm.sendRequest(
		'admin.php?p=user_payment_profiles&uid='+uid+'&action=ajax_edit_form&paymentProfileId='+id,
		{},
		'Please wait',
		function(data){
			var response = JSON.parse(data);
			$('#modal-paymentprofile .modal-body *').remove();
			$('#modal-paymentprofile .modal-body').append(response.html);
		}
	);
	return false;
}

function removeCustomer(id, nonce) {
    AdminForm.confirm(trans.customers.confirm_remove_customer, function() {
        window.location = 'admin.php?p=customer&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
    });

    return false;
}

function lockCustomer(id) {
    AdminForm.confirm(trans.customers.lock_account + SecurityAccountBlockingHours + ' hours, are you sure?', function() {
        window.location = 'admin.php?p=customer&mode=lockaccount&id='+ id;
    });

    return false;
}

function unlockCustomer(id) {
	AdminForm.confirm(trans.customers.confirm_unlock_account, function() {
		window.location = 'admin.php?p=customer&mode=unlockaccount&id='+ id;
	});

	return false;
}



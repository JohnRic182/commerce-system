var Products = (function($) {
	var init, productsUpdateSeoUrl, removeProduct, editProduct;

	init = function() {
		$(document).ready(function() {


			$('form[name="frmProduct"]').submit(function(){
				var errors = [];
				var productType = $('#field-product_type').val();
				AdminForm.removeErrors();

				if (productType == 'Tangible') {

					if (!$('#field-free_shipping').is(':checked') && $('#field-product_level_shipping').is(':checked')) {
						$('.shipping-method-active:checked').each(function(){
							var fRex = /^\d+(\.\d+)?$/;
							var ssid = $(this).attr('data-ssid');
							var shippingPrice = $('input[name="shipping_method[' + ssid + '][price]"]').val();
							if (!fRex.test(shippingPrice)) { errors.push({field: 'input[name="shipping_method[' + ssid + '][price]"]', message: trans.products.enter_shipping_price}); }
						});
					}
				}

				if ($.trim($('input[name="title"]').val()) == '') {
					errors.push({
						field: '#field-title',
						message: trans.products.enter_title
					});
				}

				if ($.trim($('input[name="product_id"]').val()) == '') {
					errors.push({
						field: '#field-product_id',
						message: trans.products.enter_product_id
					});
				}

				if ($.trim($('input[name="price"]').val()) == '') {
					errors.push({
						field: '#field-price',
						message: trans.products.enter_product_price
					});
				}

				if ($.trim($('select[name="cid"]').find(":selected").val()) == '0') {
					errors.push({
						field: '#field-cid',
						message: trans.products.enter_product_category
					});
				}

				if ($.trim($('input[name="max_order"]').val()) > 0) {
					if ($.trim($('input[name="min_order"]').val()) > $.trim($('input[name="max_order"]').val())) {
						errors.push({
							field: '#field-min-order',
							message: trans.products.minimum_exceed_maximum
						});
					}
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
					return false;
				}

				return true;
			});

			$("a.products-list-widget").click(function(){
				var pid_field = $("#field-wg_pid");
				$(pid_field).val($(this).attr('product_id'));
			});


			$('.remove-test-products').click(function() {
				var nonce = $(this).attr('data-nonce');
				AdminForm.confirm(
					trans.products.confirm_remove_test_products,
					function() {
						document.location = 'admin.php?p=product&mode=delete&deleteMode=test-products&nonce=' + nonce;
					}
				);

				return false;
			});

			$('#modal-products-export').on('show.bs.modal', function() {
				$(this).find('input[type=checkbox]:checked').prop('checked', false).change();
				$(this).find('.sprite.checked').removeClass('checked');
			});

			$('#modal-products-export-check-all').on('click', function() {
				$('#modal-products-export').find('input[type=checkbox]').prop('checked', true).change();
				$('#modal-products-export').find('.sprite').addClass('checked');
			});

			$('#modal-products-export-uncheck-all').on('click', function() {
				$('#modal-products-export').find('input[type=checkbox]:checked').prop('checked', false).change();
				$('#modal-products-export').find('.sprite.checked').removeClass('checked');
			});

			$('form[name="form-products-export"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var exportMode = $('input[name="form-products-export-mode"]:checked').val();
				var nonce = theForm.find('input[name="nonce"]').val();

				var fields = '';
				theForm.find('input[type=checkbox]:checked').each(function() {
					fields = fields + (fields == '' ? '' : ',') + $(this).attr('value');
				});

				if (exportMode == 'selected') {
					var ids = '';
					$('.checkbox-product:checked').each(function() {
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});

					if (ids != '') {
						url = 'admin.php?p=product&mode=export&exportMode=selected&fields=' + fields + '&ids=' + ids + '&nonce=' + nonce;
						$('#export-iframe').attr('src', url);
						theModal.modal('hide');
					}
					else {
						AdminForm.displayModalAlert(theModal, trans.products.no_products_selected_export, 'danger');
					}
				}
				else if (exportMode == 'all') {
					url = 'admin.php?p=product&mode=export&exportMode=all&fields=' + fields + '&nonce=' + nonce;
					$('#export-iframe').attr('src', url);
					theModal.modal('hide');
				}

				return false;
			});

			$('form[name="form-products-delete"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var deleteMode = $('input[name="form-products-delete-mode"]:checked').val();
				var nonce = theForm.find('input[name="nonce"]').val();

				if (deleteMode == 'selected') {
					var ids = '';
					$('.checkbox-product:checked').each(function() {
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});
					if (ids != '') {
						window.location='admin.php?p=product&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
					} else {
						AdminForm.displayModalAlert(theModal, trans.products.no_products_selected_delete, 'danger');
					}
				}
				else if (deleteMode == 'search') {
					window.location='admin.php?p=product&mode=delete&deleteMode=search&nonce=' + nonce;
				}

				return false;
			});

			$('#field-title,#field-product_id')
				.keyup(function(){ productsUpdateSeoUrl(); })
				.change(function(){ productsUpdateSeoUrl(); });

			/**
			 * Product type changes
			 */
			$('#field-product_type').change(function() {
				var type = $(this).val();
				$('#group-digital').toggle(type == 'Digital');
				// hide the product level shipping methods if not tangible
				if (type != 'Tangible') {
					$('#product-shipping-methods').hide();
				} else {
					if ($('#field-product_level_shipping').is(':checked')) {
						$('#product-shipping-methods').show();
					} else {
						$('#product-shipping-methods').hide();
					}
				}

				$('#field-weight,#field-dimension-width,#field-free_shipping,#field-products_location_id,.field-product-level-shipping-wrap,#product-shipping-methods').closest('.form-group').toggle(type == 'Tangible');
				$('#field-digital_product_file,#field-digital_product_file_upload').closest('.form-group').toggle(type == 'Digital');

				$('#field-same_day_delivery').closest('.form-group').toggle(type == 'Tangible');
				$('#radio_field-recurring_billing_data-auto_complete_options-_auto_complete_initial_order').closest('.form-group').toggle(type != 'Tangible');

			}).change();

			/**
			 * Product shipping price change
			 */
			$('#field-product_level_shipping').change(function() {
				var productLevelShipping = $(this).is(':checked');
				$('#product-shipping-methods').toggle(productLevelShipping);
			}).change();

			/**
			 * Free shipping change
			 */
			$('#field-free_shipping').change(function() {
				var productFreeShipping = $(this).is(':checked');
				if (productFreeShipping) {
					$('.field-product-level-shipping-wrap,#product-shipping-methods').hide();
				} else {
					$('.field-product-level-shipping-wrap').show();
					$('#field-product_level_shipping').change();
				}
			}).change();

			/**
			 * Inventory settings changes
			 */
			$('input[name="inventory_control"]').change(function() {
				var val = $('input[name="inventory_control"]:checked').val();
				$('#field-stock, #field-stock_warning').closest('.form-group').toggle(val=='Yes');
				$('#field-inventory_rule').closest('.form-group').toggle(val!='No');

				if (typeof(ProductVariants) !== 'undefined')
					ProductVariants.renderProductVariantsTable(productVariants == undefined ? [] : productVariants);

			});

			$('input[name="inventory_control"]:first').change();

			$('.delete-product').click(function() {
				var id = $(this).attr('data-id');
				var nonce = $(this).attr('data-nonce');

				return removeProduct(id, nonce);
			});

			$('.copy-product').click(function() {
				var id = $(this).data('id');
				$("#productId").val( id );
			});

			$(window).load(function() {
				$('#modal-field-image-multiple-edit').on('click', '.copy-clipboard', function() {
					$($('.copy-clipboard-url')[0]).removeClass("hidden").addClass("visible");
					AdminForm.copyToClipboard($('.copy-clipboard-url')[0]);
					$($('.copy-clipboard-url')[0]).removeClass("visible").addClass("hidden");
					alert("Image URL has been copied to clipboard");
					return false;
				});
			});

			showMetaCounter();

			/* Show/Hide options for Change Price */
			$('#field-edit-change_price').change(function(){
				if($(this).is(":checked")){
					$('#field-edit-state').parent().removeClass('hidden');
					$('#field-edit-price_change').parent().removeClass('hidden');
					$('#field-edit-currency').parent().removeClass('hidden');
					$('#field-edit-price_round').parent().parent().removeClass('hidden');
				}else {
					$('#field-edit-state').parent().addClass('hidden');
					$('#field-edit-price_change').parent().addClass('hidden');
					$('#field-edit-currency').parent().addClass('hidden');
					$('#field-edit-price_round').parent().parent().addClass('hidden');
				}
			});

			/* Show/Hide options for Change Sale Price */
			$('#field-edit-change_sale_price').change(function(){
				if($('#field-edit-change_sale_price option:selected').val() == '1'){
					$('.form-description').css('display', 'block');
					$('#field-edit-price_change2').parent().removeClass('hidden');
					$('#field-edit-currency2').parent().removeClass('hidden');
					$('#field-edit-price_round2').parent().parent().removeClass('hidden');
				}else {
					$('.form-description').css('display', 'none');
					$('#field-edit-price_change2').parent().addClass('hidden');
					$('#field-edit-currency2').parent().addClass('hidden');
					$('#field-edit-price_round2').parent().parent().addClass('hidden');
				}
			});

			$('form[name="form-products-edit"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var editMode = $('input[name="form-products-edit-mode"]:checked').val();
				var nonce = theForm.find('input[name="nonce"]').val();

				return editProduct(editMode, theModal, theForm, nonce);
			});

		});
	};

	productsUpdateSeoUrl = function() {
		if (productsSeoUrlSettings != undefined) {
			var url = productsSeoUrlSettings.template
				// TODO: add %CategoryKey%, %CategoryName%, %CategoryDbId%, %CategoryPath%, %CategoryURL%
				.replace('%ProductName%', $('#field-title').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.replace('%ProductId%', $('#field-product_id').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.replace('%ProductDbId%', '[id]')
				.replace('%CategoryKey%', '[category-key]')
				.replace('%CategoryName%', '[category-name]')
				.replace('%CategoryDbId%', '[category-id]')
				.replace('%CategoryPath%', '[category-path]')
				.replace('%CategoryURL%', '[category-url]')
				.trim()
				.replace(/\s+/g, productsSeoUrlSettings.joiner);

			if (productsSeoUrlSettings.lowercase) url = url.toLocaleLowerCase();
			$('#field-url_custom').attr('placeholder', url);
		}
	};

	removeProduct = function(id, nonce) {
		AdminForm.confirm(trans.products.confirm_remove, function() {
			window.location = 'admin.php?p=product&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	editProduct = function(editMode, theModal, theForm, nonce) {
		AdminForm.confirm('Are you sure want to update selected products?', function() {

			var params = '';

			// Change Price
			var isChangeProductPrice = ($('#field-edit-change_price').siblings("span").hasClass('checked')) ? 1 : 0;
			params += '&is_change_product_price=' + isChangeProductPrice;
			if (isChangeProductPrice) {
				var state = $('#field-edit-state').val();
				var productPrice = $('#field-edit-price_change').val();
				var roundProductPrice = ($('#field-edit-price_round').siblings("span").hasClass('checked')) ? 1 : 0;
				var productPriceDiscountType = $('#field-edit-currency').val();
				params += '&state=' + state + '&product_price=' + productPrice + '&round_product_price=' + roundProductPrice + '&product_price_discount_type=' + productPriceDiscountType;
			}

			// Change Sale Price
			var changeProductSalePrice = $('#field-edit-change_sale_price').val();
			params += '&is_change_product_sale_price=' + changeProductSalePrice;
			if (changeProductSalePrice) {
				var productSalePrice = $('#field-edit-price_change2').val();
				var roundProductSalePrice = ($('#field-edit-price_round2').siblings("span").hasClass('checked')) ? 1 : 0;
				var productSalePriceDiscountType = $('#field-edit-currency2').val();
				params += '&product_sale_price=' + productSalePrice + '&round_product_sale_price=' + roundProductSalePrice + '&product_sale_price_discount_type=' + productSalePriceDiscountType;
			}

			var category = $('#field-edit-products_category').val();
			var visibility = $('#field-edit-storefront').val();
			var taxClass = $('#field-edit-tax_class').val();
			var callForPrice = $('#field-edit-call_price').val();
			var isHotDeal = $('#field-edit-hot_deal').val();
			var isHomePage = $('#field-edit-homepage').val();

			params += '&nonce=' + nonce + '&category=' + category + '&visibility=' + visibility + '&tax_class=' + taxClass + '&call_for_price=' + callForPrice + '&is_hot_deal=' + isHotDeal + '&is_home_page=' + isHomePage;

			if (editMode == 'selected') {
				var ids = '';
				$('.checkbox-product:checked').each(function() {
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});

				if (ids != '') {
					window.location='admin.php?p=product&mode=bulk-update&edit_mode=selected&ids=' + ids + params;

				} else {
					AdminForm.displayModalAlert(theModal, trans.products.no_products_selected_edit, 'danger');
				}
			} else {
				window.location='admin.php?p=product&mode=bulk-update&edit_mode=search' + params;
			}

			return false;

		});

		return false;
	};

	init();

}(jQuery));

var BackupRestore = (function($) {
	var init;

	init = function() {
		$(document).ready(function() {
			$('.remove-backup').click(function() {
				var theLink = $(this);

				AdminForm.confirm(trans.backup.confirm_delete_backup, function() {
					AdminForm.sendRequest(
						'admin.php?p=backup_restore&mode=delete-backup',
						'file_name='+theLink.attr('data-id')+'&nonce='+theLink.attr('data-nonce'), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=backup_restore';
							} else {
								if (data.message !== undefined) {
									AdminForm.displayAlert(data.message, 'danger');
								}
							}
						}
					)
				});
				return false;
			});

			$('.restore-backup').click(function() {
				var theLink = $(this);

				AdminForm.confirm(trans.backup.confirm_restore_backup, function() {
					AdminForm.sendRequest(
						'admin.php?p=backup_restore&mode=restore-backup',
						'file_name='+theLink.attr('data-id')+'&nonce='+theLink.attr('data-nonce'), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=backup_restore';
							} else {
								if (data.message !== undefined) {
									AdminForm.displayAlert(data.message, 'danger');
								}
							}
						}
					)
				});
				return false;
			});

			$('form[name="form-create-backup"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				AdminForm.confirm(trans.backup.confirm_create_backup, function() {

					AdminForm.sendRequest(
						'admin.php?p=backup_restore&mode=create-backup',
						theForm.serialize(), 'Please wait. Backing up',
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=backup_restore';
							} else {
								if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								}
							}
						}
					);
				});

				return false;
			});
		});
	};

	init();
}(jQuery));
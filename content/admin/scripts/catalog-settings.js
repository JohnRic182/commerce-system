var CatalogSettings = (function($) {
	var init, handleDisplayPriceRanges, handleDisplayCategoryThumbnailFields, handleDisplayRecommendedProducts, handleDisplaySiblingProducts, handlePredictiveSearch;

	init = function() {
		$(document).ready(function() {
			$('#field-CatalogUsePriceRanges').change(function() {
				handleDisplayPriceRanges();
			});
			handleDisplayPriceRanges();
			$('#field-CatalogCategoryThumbAuto').change(function() {
				handleDisplayCategoryThumbnailFields();
			});
			handleDisplayCategoryThumbnailFields();
			$('#field-EnableRecommendedProducts').change(function() {
				handleDisplayRecommendedProducts();
			});
			handleDisplayRecommendedProducts();
			$('#field-EnableSiblingProducts').change(function() {
				handleDisplaySiblingProducts();
			});
			handleDisplaySiblingProducts();
			$('#field-PredictiveSearchEnabled').change(function() {
				handlePredictiveSearch();
			});
			handlePredictiveSearch();
		});
	};

	handlePredictiveSearch = function() {
		var enabled = $('#field-PredictiveSearchEnabled').is(':checked');

		$('#field-PredictiveSearchResultsCount').closest('.form-group').toggle(enabled);
		$('#field-PredictiveSearchInProductsDescription').closest('.form-group').toggle(enabled);
		$('#field-PredictiveSearchInCategories').closest('.form-group').toggle(enabled);
		$('#field-PredictiveSearchInManufacturers').closest('.form-group').toggle(enabled);
	}

	handleDisplayPriceRanges = function() {
		var enabled = $('#field-CatalogUsePriceRanges').is(':checked');

		$('#field-min-price_range_1').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_2').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_3').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_4').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_5').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_6').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_7').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_8').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_9').closest('.form-group').toggle(enabled);
		$('#field-min-price_range_10').closest('.form-group').toggle(enabled);
	};

	handleDisplayCategoryThumbnailFields = function() {
		var enabled = $('#field-CatalogCategoryThumbAuto').is(':checked');

		$('#field-CatalogCategoryThumbSize').closest('.form-group').toggle(enabled);
		$('#field-CatalogCategoryThumbType').closest('.form-group').toggle(enabled);
		$('#field-CatalogCategoryThumbResizeRule').closest('.form-group').toggle(enabled);
	};

	handleDisplayRecommendedProducts = function() {
		$('#field-RecommendedProductsCount').closest('.form-group').toggle($('#field-EnableRecommendedProducts').is(':checked'));
	};

	handleDisplaySiblingProducts = function() {
		$('#field-SiblingProductsCount').closest('.form-group').toggle($('#field-EnableSiblingProducts').is(':checked'));
	};

	init();
}(jQuery));
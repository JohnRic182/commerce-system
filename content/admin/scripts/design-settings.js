$(document).ready(function(){
	if (themeInstalled == 1 && themeHasTestProducts == 1) {
		AdminForm.confirm(
			trans.products.confirm_add_test_products,
			function() {
				AdminForm.sendRequest(
					'admin.php?p=product&mode=add-test-products',
					null, null,
					function(data) {
						if (data.status == 1) {
							document.location = 'admin.php?p=design_settings'
						}
					}
				);
			}
		);
	}

	$('.theme-selection')
		.on('click', '.theme-preview', function() {
			var theModal = $('#id-themes-gallery');

			theModal.find('img.modal-img').attr('src', $(this).attr('data-img-src')).attr('width', 600).attr('height', 600);
			theModal.find('.modal-title').text(decodeURIComponent($(this).attr('title')));

			theModal.modal('show');

			return false;
		})
		.on('click', '.theme-activate', function() {
			var id = $(this).attr('data-theme-id');

			setParamsForActivation(id, 'activate')

		})
		.on('click', '.theme-install', function() {
			var button = this;
			// Show installing graphic...
			var id = $(this).attr('data-theme-id');

			setParamsForActivation(id, 'install')

		});

	$('[data-action="action-theme-activate"]').click(function(){

		changeThemeMode = $('input[name="form-theme-activate-mode"]:checked').val();
		id = $('input[name="form-theme-id"]').val();
		activateAction = $('input[name="form-theme-action"]').val();

		window.location = 'admin.php?p=design_settings&mode='+activateAction+'&id='+id+'&change_mode='+changeThemeMode;

		return false;
	});

	// NOTE: we are calling external URL here, DO NOT use AdminForm.sendRequest
	$.ajax({
		url: themeRepository+'/list?v='+cartVersion,
		dataType: 'jsonp',
		success: function (data)
		{
			var n = 1;
			$.each(data.themes, function(key, entry){
				var action = '<button data-external="'+themeRepository+'/downloads/' + entry.key + '.zip" class="download"></button>';

				if ($.inArray(entry.key, existingThemes) == -1)
				{

					var html = generateThemeItem({
						'key': entry.key,
						'title': entry.name,
						'thumbnailImageUrl': themeRepository+'/downloads/' + entry.thumbnail,
						'fullsizeImageUrl': themeRepository+'/downloads/' + entry.preview
					});

					$('#links').append(html);
				}
			});
		}
	});

}); // end $(document).ready()

function setParamsForActivation(id, activateAction) {
	$('input[name="form-theme-action"]').val(activateAction);
	$('input[name="form-theme-id"]').val(id);
	$('#modal-theme-activate').modal('show');
}

function generateThemeItem(settings)
{
	var defaults = {
		'title': 'Theme Title',
		'thumbnailImageUrl': 'images/admin/theme-nothumbnail.jpg',
		'fullsizeImageUrl': 'images/admin/theme-nopreview.jpg',
		'mode': 'activate',
		'key': null
	};

	settings = $.extend(defaults, settings);

	return '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 theme-box">'+
		'<div class="theme-thumbnail text-center">'+
		'<a href="#"  data-img-src="'+settings.fullsizeImageUrl+ '" id="preview_'+settings.key+'" class="theme-preview" title="'+encodeURI(settings.title)+'">' +
		'<img data-theme="'+settings.key+'" src="'+settings.fullsizeImageUrl+ '" class="img-responsive" title="'+encodeURI(settings.title)+'">'+
		'</a>'+
		'</div>'+
		'<h3 class="text-center">'+settings.title+'</h3>'+
		'<div class="btn-toolbar text-center">'+
		'<a href="#" data-img-src="'+settings.fullsizeImageUrl+ '" title="'+encodeURI(settings.title)+'" class="btn theme-preview">Preview</a>'+
		'<button data-theme-id="'+settings.key+'" class="theme-install btn btn-primary">Activate</button>'+
		'</div>'+
		'</div>';
}
$(document).ready(function(){

	showHideThankYouPageUrl();

	updateTotals();

	initSpinners();

	$("#radio_field-thank_you_page_default").click(function () {
		showHideThankYouPageUrl();
	});

	$("#radio_field-thank_you_page_custom").click(function () {
		showHideThankYouPageUrl();
	});

	$("#delete-button").click(function() {
		removeOrderForm(id, nonce);
	});

	$(".product-quantity").change(function () {
		updateTotals();
	});

	$('.form-order-forms-delete').submit(function(){
		return false;
	});

	$('form[name=form-order-form]').submit(function() {
		var errors = [];
		if ($.trim($('#field-name').val()) == '') {
			errors.push({
				field: '#field-name',
				message: trans.order_forms.enter_name
			});
		}

		if ($("#radio_field-thank_you_page_custom").prop("checked") && $.trim($("#field-thank_you_page_url").val()) == '')
		{
			errors.push({
				field: '#field-thank_you_page_url',
				message: trans.order_forms.enter_thank_you_page_url
			});
		}

		AdminForm.displayValidationErrors(errors);

		return errors.length == 0;
	});

	/**
	 * Handle delete button
	 */
	$('[data-action="action-order-forms-delete"]').click(function() {
		var deleteMode = $('input[name="form-order-forms-delete-mode"]:checked').val();
		var nonce = $('#modal-order-forms-delete').find('input[name="nonce"]').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-order-form:checked').each(function() {
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '') {
				window.location='admin.php?p=order_form&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else
			{
				alert(trans.order_forms.select_order_form_delete);
			}
		}
		else if (deleteMode == 'search')
		{
			window.location='admin.php?p=order_form&mode=delete&deleteMode=search&nonce=' + nonce;
		}
	});

	$('#modal-product-attribute').find('form').submit(function(){
		AdminForm.sendRequest(
			'admin.php?p=order_form&mode=product-add',
			$('#modal-product-attribute').find('form').serialize(),
			'Saving product',
			function(data){
				if (data.status) {

					product = data.product;
					addProductToTable(product);
					$('#modal-product-attribute').modal('hide');
					AdminForm.displayAlert(trans.order_forms.product_added);
				} else {
					$('#modal-product-attribute').modal('hide');
					AdminForm.displayAlert(data.error, 'danger');
				}
			}
		);

		return false;
	});

	if ($("#search-product").length > 0)
	{
		$( "#search-product" ).autocomplete({
	        source: "admin.php?p=order_form&mode=getProducts",
	        minLength: 2,
	        select: function( event, ui ) {

	        	if (ui.item.attributes_count > 0)
	        	{
	        		showAttributesForm(ui.item.attributes, ui.item.pid);
	        	}
	        	else
	        	{
	        		html = '<input type="hidden" name="pid" value="'+ui.item.pid+'" /><input type="hidden" name="ofid" value="'+ofid+'" />';
	    			$('.modal-product-attributes').html(html);
	        		$('#modal-product-attribute').find('form').submit();
	        	}
	        },
	        response: function(event, ui) {
	            if (!ui.content.length) {
	            	$(".alert").hide();
	                AdminForm.displayAlert(trans.order_forms.no_results_found, 'info');
	            } else {
	            	$(".alert").hide();
	            }
	        }

	      })
	      .data( "uiAutocomplete" )._renderItem = function( ul, item ) {
	    	return $( "<li></li>" )
	                .data( "item.autocomplete", item )
	                .append( "<a>" + item.title + " - " + item.product_id + "</a>" )
	                .appendTo( ul );
	        };
	}

	/**
	 * Handle title & id changes
	 */
	function orderFormUpdateSeoUrl() {
		if (orderFormSeoUrlSettings != undefined) {
			var url = orderFormSeoUrlSettings.template
				.replace('%OrderFormName%', $('#field-name').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.replace('%OrderFormDbId%', '[id]')
				.trim()
				.replace(/\s+/g, orderFormSeoUrlSettings.joiner);

			if (orderFormSeoUrlSettings.lowercase) url = url.toLocaleLowerCase();
			$('#field-url_custom').attr('placeholder', url);
		}
	}

	$('#field-name')
		.keyup(function() {
			orderFormUpdateSeoUrl();
		})
		.change(function() {
			orderFormUpdateSeoUrl();
		});

	function addProductToTable(product)
	{

		attributes = product.attributes_list !== undefined ? product.attributes_list : '';
		attributes = attributes.replace("\n", "<br/>");
		price = parseFloat(product.price).toFixed(2);

		html = '<tr id="product-row-'+product.ofpid+'">'+
			'<td>'+product.product_id+'</td>'+
			'<td>'+product.title+'</td>'+
			'<td>'+ attributes +'</td>'+
			'<td>'+product.product_type+'</td>'+
			'<td>$'+ price +'</td>'+
			'<td><input price="'+product.price+'" class="product-quantity" style="width: 50px;" type="text" name="quantity['+product.ofpid+']" value="'+product.quantity+'" /></td>'+
			'<td class="list-actions text-right">'+
				'<a href="#" onclick="return removePid('+product.ofpid+');"><span class="icon icon-delete"></span>Delete</a>'+
				'<a target="_blank" href="admin.php?p=product&mode=update&id='+product.pid+'"><span class="icon icon-edit"></span>Edit</a>'+
			'</td>'+
		'</tr>';

		$(".table-order-form-products tbody").append(html);

		initSpinners();

		updateTotals();
	}

	function showAttributesForm(productAttributes, pid) {

		if (productAttributes != undefined && productAttributes.length > 0) {
			var html = '';
			$.each(productAttributes, function(index, attribute){
				if (attribute.is_active == 'Yes' && attribute.options_parsed) {
					html +=
						'<div class="form-group field-select col-sm-6 col-xs-12">' +
							'<label for="field-attribute-' + attribute.paid + '">' + htmlentities(attribute.name)+ '</label>' +
							'<select id="field-attribute-' + attribute.paid + '" class="form-control field-attribute-attribute_type" name="attributes[' + attribute.paid + ']">';

					$.each(attribute.options_parsed, function(optionIndex, option){
						html += '<option value="' + htmlentities($.trim(option.name)) +'">' + htmlentities($.trim(option.name)) + '</option>';
					});

					html += '</select>' +
						'</div>';
				}
			});

			html += '<input type="hidden" name="pid" value="'+pid+'" /><input type="hidden" name="ofid" value="'+ofid+'" />';

			if (html != '') {

				$('.modal-product-attributes').html(html);
				$('#modal-product-attribute').modal('show');

				return false;
			}
		}

		AdminForm.displayAlert(trans.products_variants.message_add_attributes_variant);
		return false;
	};


	$(".copy-button").click(function() {
		ofid = $(this).attr('ofid');

		AdminForm.copyToClipboard($('#copy-field-' + ofid )[0]);
		AdminForm.displayAlert(trans.order_forms.form_url_copy_success);
	    return false;
	});
});

function showHideThankYouPageUrl()
{
	if ($("#radio_field-thank_you_page_custom").prop("checked"))
	{
		$("#field-thank_you_page_url").parent().show();
	}
	else
	{
		$("#field-thank_you_page_url").parent().hide();
	}
}

function removeOrderForm(id, nonce) {
	AdminForm.confirm(trans.order_forms.confirm_delete_order_form, function() {
		window.location = 'admin.php?p=order_form&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
	});

	return false;
}

function removePid(ofpid)
{
	AdminForm.confirm(trans.order_forms.confirm_delete_product, function() {

		AdminForm.sendRequest(
				'admin.php?p=order_form&mode=product-delete&ofpid='+ofpid,
				'',
				'Deleting product',
				function(data){
					if (data.status) {
						$("#product-row-"+ofpid).remove();
						updateTotals();
						AdminForm.displayAlert(trans.order_forms.product_deleted);
					} else {
						AdminForm.displayAlert(data.error, 'danger');
					}
				}
			);

		return false;
	});
}

function updateTotals()
{
	var price_total = 0;
	var quantity_total = 0;
	if ($('.product-quantity').length > 0)
	{
		$('.product-quantity').each(function (key, val){
			qty = parseInt($(this).val(), 10);
			price_total += parseFloat($(this).attr('price'))*qty;
			quantity_total += qty;
		});
		$("#product-row-totals").show();
	}
	else
	{
		$("#product-row-totals").hide();
	}
	$("#price-total").html('$'+parseFloat(price_total).toFixed(2));
	$("#quantity-total").html(quantity_total);
}

function initSpinners()
{
	$(".product-quantity").spinner({
		spin : function(event, ui) {
			if (ui.value < 1) {
                $(this).spinner("value", 1);
                return false;
            }
        },
        stop: function(event, ui) {
        	updateTotals();
        }
	});
}
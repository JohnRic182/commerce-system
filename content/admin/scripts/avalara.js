var AvalaraTaxes = (function($) {
	var init, removeTaxCode;

	init = function() {

		$(document).ready(function() {
			$('#modal-add-tax-code').on('hidden.bs.modal', function (e) {
				$('#modal-add-tax-code').find('.form-group .error').remove();
			});

			$('#modal-update-tax-code').on('hidden.bs.modal', function (e) {
				$('#modal-update-tax-code').find('.form-group .error').remove();
			});

			$('form[name=form-add-tax-code]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				var taxName = $('#field-tax_code_name').val();
				var taxCode = $('#field-tax_code_code').val();

				var errors = [];
				if ($.trim(taxName) == '') {
					errors.push({
						field: '#field-tax_code_name',
						message: 'Please enter Tax Name'
					});
				}
				if ($.trim(taxCode) == '') {
					errors.push({
						field: '#field-tax_code_code',
						message: 'Please enter Tax Code'
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
				} else {
					AdminForm.sendRequest(
						'admin.php?p=avalara&mode=add-tax-code',
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=avalara&mode=tax-codes'
							} else {
								if ($.trim(data.message) != '') {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								} else if (data.errors != undefined) {
									AdminForm.displayModalErrors(theModal, data.errors);
								}
							}
						}
					);
				}

				return false;
			});

			$('form[name="form-tax-codes-delete"]').submit(function() {
				var theForm = $(this);
				var theModal = $(this).closest('.modal');
				var deleteMode = $('input[name="form-tax-codes-delete-mode"]:checked').val();
				var nonce = theForm.find('input[name="nonce"]').val();

				if (deleteMode == 'selected')
				{
					var ids = '';
					$('.checkbox-tax-code:checked').each(function(){
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});
					if (ids != '')
					{
						window.location='admin.php?p=avalara&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
					}
					else
					{
						AdminForm.displayModalAlert(theModal, 'Please select at least one tax code to delete', 'danger');
					}
				}
				else if (deleteMode == 'search')
				{
					window.location='admin.php?p=avalara&mode=delete&deleteMode=search&nonce=' + nonce;
				}

				return false;
			});

			$('form[name=form-update-codes]').submit(function() {
				var theForm = $(this);
				var theModal = $(this).closest('.modal');
				var errors = [];

				if ($.trim(theForm.find('#update-codes-username').val()) == '') {
					errors.push({
						field: '#update-codes-username',
						message: 'Avalara Username is required'
					});
				}
				if ($.trim(theForm.find('#update-codes-password').val()) == '') {
					errors.push({
						field: '#update-codes-password',
						message: 'Avalara Password is required'
					});
				}

				if (errors.length == 0) {
					AdminForm.sendRequest(
						theForm.attr('action'),
						theForm.serialize(), trans.common.updating,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=avalara&mode=tax-codes';
							} else {
								if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								}
							}
						}
					)
				}

				return false;
			});

			$('.edit-tax-code').click(function() {
				var id = $(this).attr('data-id');

				AdminForm.sendRequest(
					'admin.php?p=avalara&mode=update-tax-code&id='+id,
					null, null,
					function(data) {
						var theModal = $('#modal-update-tax-code');

						theModal.find('input[name=id]').val(id);
						theModal.find('.modal-body').html(data);

						theModal.modal('show');
					}, null, 'GET', 'html'
				);

				return false;
			});

			$('form[name=form-update-tax-code]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var errors = [];

				AdminForm.sendRequest('admin.php?p=avalara&mode=update-tax-code',
					theForm.serialize(), null,
					function(data) {
						if (data.status == 1) {
							document.location = 'admin.php?p=avalara&mode=tax-codes';
						} else {
							if (data.message !== undefined) {
								AdminForm.displayModalAlert(theModal, data.message, 'danger');
							} else if (data.errors !== undefined) {
								$.each(data.errors, function(idx, error) {
									errors.push({
										field: '.field-'+error.field,
										message: error.message
									});
								});
								AdminForm.displayValidationErrors(errors);
							}
						}
					}
				);
				return false;
			});

			$('#modal-add-avalara-tax-code').on('show.bs.modal', function() {
				var theModal = $(this);
				AdminForm.sendRequest('admin.php?p=avalara_tree',
					null, null,
					function(data) {
						theModal.find('.avalara_category').unbind('click');
						theModal.find('.tree li.parent_li > span').unbind('click');

						theModal.find('.tree').html(data);

						theModal.find('.tree li:has(ul)').addClass('parent_li');
						theModal.find('.tree li.parent_li > span').on('click', function (e) {
							var children = $(this).parent('li.parent_li').find(' > ul > li');
							if (children.is(":visible")) {
								children.hide('fast');
								$(this).find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
							} else {
								children.show('fast');
								$(this).find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
							}
							e.stopPropagation();
						});

						theModal.find('.avalara-tax-code').click(function() {
							var theAnchor = $(this);
							var taxCode = theAnchor.attr('data-tax-code');

							var nonce = $('form[name="form-add-tax-code"] input[name="nonce"]').val();
							AdminForm.sendRequest(
								'admin.php?p=avalara&mode=add-avalara-tax-code',
								'tax_code_code='+taxCode+'&nonce='+nonce, null,
								function(resp) {
									if (resp.status == 1) {
										theModal.modal('hide');
										document.location = 'admin.php?p=avalara&mode=tax-codes';
									} else {
										if (resp.message !== undefined) {
											AdminForm.displayModalAlert(theModal, resp.message, 'danger');
										} else {
											AdminForm.displayModalErrors(theModal, resp.errors);
										}
									}
								}
							);
						});
					}, null, 'GET', 'html'
				);
			});
		});
	};

	removeTaxCode = function(id, nonce) {

		AdminForm.confirm(trans.avalara.confirm_remove_tax_code, function() {
			window.location = 'admin.php?p=avalara&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	init();

	return {
		removeTaxCode: removeTaxCode
	};
}(jQuery));
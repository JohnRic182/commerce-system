var DobaProducts = (function($) {
	var init, loadProducts, getPrice, importWatchlist, removeDobaData;

	var productsPerPage = 20;

	init = function() {
		$(document).ready(function() {
			AdminForm.sendRequest(
				'admin.php?p=doba_json&action=getWatchlists', null, 'Please wait',
				function(data) {
					var jsMessageArea = $('#doba-js-messages');
					jsMessageArea.hide();
					if (data.status == 1) {
						if (data.count > 0) {
							var thePrototype = $('#watchlist-prototype');
							$.each(data.data.watchlist, function(i, watchList) {
								var theWatchlist = thePrototype.clone();

								var theImportBtn = theWatchlist.find('.import-btn');
								theImportBtn.click(function() {
									var theWatchList = $(this).closest('.watchlist').find('.watchlist-panel');
									importWatchlist(theWatchList);
								});

								theWatchlist.attr('id', 'watchlist-'+watchList.watchlist_id);
								theWatchlist.find('h2').html(watchList.name).attr('data-target', '#watchlist-panel-'+watchList.watchlist_id);
								theWatchlist.find('button').attr('data-target', '#watchlist-panel-'+watchList.watchlist_id);
								theWatchlist.find('.watchlist-panel').attr('id', 'watchlist-panel-'+watchList.watchlist_id);

								$('#doba-watchlists').append(theWatchlist);
								theWatchlist.show();
							});
						} else {
							jsMessageArea.show();
							$('#no-watch-lists').show();
						}
					} else {
						var errorMsg = $('#error-message');
						errorMsg.find('.message').html('Server returned error: ' + data.message);
						errorMsg.show();
						jsMessageArea.show();
					}
				},
				null, 'GET'
			);

			$('.delete-doba-data').click(function() {
				return removeDobaData();
			});
		});

		$('#main-content').on('show.bs.collapse', '.watchlist-panel', function() {
			var theWatchList = $(this);
			loadProducts(theWatchList, 1);
		});
	};

	loadProducts = function(theWatchList, page, callback) {

		var id = theWatchList.attr('id').replace('watchlist-panel-', '');
		if (page === undefined) {
			page = 1;
		}
		var refresh = page == 1 && theWatchList.attr('data-is-set') !== '1';

		var url = 'admin.php?p=doba_json&action=getWatchlistProducts&id='+id+'&limit='+productsPerPage+'&page='+page;
		if (refresh) {
			url += '&refresh=1';
		}

		AdminForm.sendRequest(
			url, null, 'Please wait',
			function(data) {
				if (data.status == 1) {
					theWatchList.attr('data-items-count', data.items_count);
					var numPages = Math.ceil(parseInt(data.items_count) / productsPerPage);
					theWatchList.attr('data-item-pages', numPages);

					var theTBody = theWatchList.find('tbody');
					theTBody.empty();
					$.each(data.items, function(i, item) {
						var theRow = $(
							'<tr>' +
								'<td>'+item.name+'</td><td>'+item.description+'</td>' +
								'<td class="text-right">$'+item.price+'</td>' +
								'<td class="text-right">$'+item.msrp+'</td>' +
								'<td class="text-right">$'+item.drop_ship_fee+'</td>' +
								'<td class="text-right">$'+item.ship_cost+'</td>' +
								'<td class="text-right">$'+getPrice(item.price, item.msrp)+'</td>' +
								'<td class="text-right">'+item.stock+' ('+item.qty_avail+')</td></tr>');
						theTBody.append(theRow);
					});

					var pagerText = theWatchList.find('.custom-pager .text');
					var start = (page - 1) * productsPerPage + 1;
					var end = page >= numPages ? data.items_count : page * productsPerPage;
					pagerText.html(start + '-' + end + ' <span class="of">of</span> ' + data.items_count);

					if (page > 1) {
						theWatchList.find('.page-left').removeAttr('disabled').unbind('click').click(function(e) {
							e.stopPropagation();
							loadProducts(theWatchList, page - 1);
							return false;
						});
					} else {
						theWatchList.find('.page-left').attr('disabled', 'disabled');
					}

					if (page < numPages) {
						theWatchList.find('.page-right').removeAttr('disabled').unbind('click').click(function(e) {
							e.stopPropagation();
							loadProducts(theWatchList, page + 1);
							return false;
						});

					} else {
						theWatchList.find('.page-right').attr('disabled', 'disabled');
					}
				} else {
					//TODO:
				}
				theWatchList.attr('data-is-set', '1');

				if (callback !== undefined) {
					callback();
				}
			},
			null, 'GET'
		);
	};

	getPrice = function(itemPrice, msrp) {
		var result = 0;

		var i = parseFloat(itemPrice);
		var m = parseFloat(msrp);

		switch (dobaPriceOption)
		{
			case "item_price" :result = i; break;
			case "item_price_inc" :result = i + (i / 100) * dobaPriceModifier; break;
			case "item_price_dec" :result = i - (i / 100) * dobaPriceModifier; break;
			case "msrp_price" :result = m; break;
			case "msrp_price_inc" :result = m + (m / 100) * dobaPriceModifier; break;
			case "msrp_price_dec" :result = m - (m / 100) * dobaPriceModifier; break;
		}

		result = Math.round(result*100)/100;

		if (dobaPriceOption99 == "Yes")
		{
			result = Math.ceil(result) - 0.01;
		}

		return result;
	};

	importWatchlist = function(theWatchList) {
		var id = theWatchList.attr('id').replace('watchlist-panel-', '');
		var page = 1;
		var totalPages = theWatchList.attr('data-item-pages');
		var progressModal = $('#modal-import-progress');

		var theProgressBar, theProgressContainer, theProgressLabel;

		theProgressBar = progressModal.find('.progress-bar');
		theProgressContainer = progressModal.find('.progress');
		theProgressLabel = progressModal.find('.progress-bar span');

		theProgressLabel.html('0% Completed');
		theProgressBar.css('width', 0);
		theProgressContainer.show();

		var doRequest = function() {
			if (!progressModal.is(':visible')) {
				progressModal.modal({
					keyboard: false
				}).modal('show');
			}

			AdminForm.sendRequest(
				'admin.php?p=doba_json&action=updateProductsSet&__ajax=1',
				{
					id: id,
					page: page,
					limit: productsPerPage,
					import: 1
				},
				null,//'Please wait',
				function(data) {
					if (page < totalPages) {
						page += 1;

						var percentage = Math.round(page / totalPages * 100);
						theProgressLabel.html(percentage+'% Completed');
						theProgressBar.css('width', percentage+'%');
						doRequest();
					} else {
						theProgressBar.css('width', '100%');

						setTimeout(function() {
							progressModal.modal('hide');
						}, 500);
					}
				}
			);
		};

		if (totalPages > 0) {
			doRequest();
		} else {
			loadProducts(theWatchList, 1, doRequest);
		}
	};

	removeDobaData = function() {
		AdminForm.confirm(trans.doba.delete_doba_data, function() {
			window.location = 'admin.php?p=doba&mode=delete-doba-products';
		});

		return false;
	};

	init();
}(jQuery));

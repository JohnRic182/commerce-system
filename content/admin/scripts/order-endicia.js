var EndiciaOrders = (function($) {
	var init, handleRefundClick,
		endiciaSetPackageDimensions, endiciaOnMailClassChange, endiciaControlDeliveryService,
		endiciaGetPostageSubmit, endiciaShowLabelDialog, endiciaAddFundsSubmit, endiciaResetCheckbox,
		endiciaGetIsChecked, endiciaGetRadioValue, endiciaGetValue, endiciaSetfocus, endiciaIsEmpty;

	init = function() {
		$(document).ready(function() {
			$('.endicia-refund').click(function() {
				return handleRefundClick(this);
			});

			$('input.number').keypress(function(evt) {
				var num = $(this).val();

				var charCode = (evt.which) ? evt.which : evt.keyCode;
				if (num.indexOf('.') >= 0 && charCode == 46) {
					return false;
				}

				return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
			});

			$('#endiciaLabelIframe').load(function(){

				var iframeBody = $('#endiciaLabelIframe').contents().find('body');
				var iframeHead = $('#endiciaLabelIframe').contents().find('head');

				$(iframeHead).append('<link rel="stylesheet" type="text/css" href="content/admin/styles/print.css?v={$APP_VERSION}">');

				var ua = window.navigator.userAgent;
				var msie = ua.indexOf("MSIE ");

				if (msie > 0)
				{
					var doc = document.getElementById('endiciaLabelIframe').contentDocument;
					if (!doc || doc == null)
					{
						var doc = document.getElementById('frame').contentWindow.document;
					}
					doc.createStyleSheet('content/admin/styles/print.css');
					var printCss = doc.createStyleSheet('content/admin/styles/print-ie.css');
					printCss.media = 'print';
				}
				else if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase()))
				{
					$(iframeHead).append('<link rel="stylesheet" type="text/css" href="content/admin/styles/print-chrome.css?v={$APP_VERSION}" media="print">');
				}
			});

			$('#modal-order-endicia-view').find('.btn-primary').click(function(){
				window.frames["endiciaLabelIframe"].focus();
				window.frames["endiciaLabelIframe"].print()
			});

			$('#modal-order-endicia').find('form').submit(function(){
				endiciaGetPostageSubmit();
				return false;
			});

			$('#field-endicia_MailShape').change(function(){
				endiciaControlDeliveryService();
			});

			$('#field-endicia_MailClass').change(function(){
				endiciaOnMailClassChange();
			});

			endiciaControlDeliveryService();

			$('.endicia-view-label-link').each(function(index, a){
				$(a).click(function(e){
					e.stopPropagation();
					endiciaShowLabelDialog($(a).attr('href'), $(a).attr('rel'), false);
					return false;
				})
			});
		});
	};

	handleRefundClick = function(ele) {
		var picNum = $(ele).attr('data-picnum');
		var theForm = $('form[name="order-endicia-refund"]');
		theForm.unbind('submit');

		var theModal = $('#modal-order-endicia-refund');

		theForm.submit(function() {
			theForm.find('input[name="pic_number"]').val(picNum);
			AdminForm.sendRequest(
				'admin.php?p=endicia&mode=request-refund',
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = document.location;
					} else {
						if (data.errors !== undefined) {
							AdminForm.displayModalErrors(theModal, data.errors);
						} else if (data.message !== undefined) {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						}
					}
				}
			);

			return false;
		});

		theModal.modal('show');

		return false;
	};

	endiciaSetPackageDimensions = function (length, width, height, appendExpressMail) {
		var shipToCountryCode = endiciaGetValue('endicia_shiptocountrycode');

		$('#endicia_PackageDimension_length').val(length);
		$('#endicia_PackageDimension_width').val(width);
		$('#endicia_PackageDimension_height').val(height);

		if (appendExpressMail) {
			if (shipToCountryCode == 'US') {
				$('#field-endicia_MailClass').append('<option value="Express">Express Mail</option>');
			} else {
				$('#field-endicia_MailClass').append('<option value="ExpressMailInternational">Express Mail International</option>');
			}
		}
	};

	endiciaOnMailClassChange = function() {
		var mailShape = endiciaGetValue('endicia_MailShape');
		var mailClass = endiciaGetValue('endicia_MailClass');

		if (mailShape == 'Parcel' && mailClass == 'ParcelSelect') {
			$('#field-endicia_SortType,#field-endicia_EntryFacility').closest('.form-group').slideDown('fast');
		} else {
			$('#field-endicia_SortType,#field-endicia_EntryFacility').closest('.form-group').slideUp('fast');
		}
		//set mailclass selected value
		endiciaSetOptionValue('field-endicia_MailClass', mailClass);
	};

	endiciaControlDeliveryService =function()
	{
		$("#field-endicia_MailShape").find("option[value='Choose']").remove();

		var packtype = endiciaGetValue('endicia_MailShape');
		var shipToCountryCode = endiciaGetValue('endicia_shiptocountrycode');

		$('#field-endicia_PackageDimension-length').val('');
		$('#field-endicia_PackageDimension-height').val('');
		$('#field-endicia_PackageDimension-width').val('');

		var mailClassEle = $('#field-endicia_MailClass');
		if (packtype == 'Parcel' || packtype == 'LargeParcel' ) {
			mailClassEle.children().remove().end();
			if (shipToCountryCode == 'US') {
				mailClassEle.append('<option value="First">First Class Mail</option>');
				mailClassEle.append('<option value="Express">Express Mail</option>');
				mailClassEle.append('<option value="Priority">Priority Mail</option>');
				mailClassEle.append('<option value="ParcelPost">Standard Post</option>');
				mailClassEle.append('<option value="ParcelSelect">Parcel Select</option>');
			} else {
				mailClassEle.append('<option value="FirstClassMailInternational">First-Class Mail International</option>');
				mailClassEle.append('<option value="ExpressMailInternational">Express Mail International</option>');
				mailClassEle.append('<option value="PriorityMailInternational">Priority Mail International</option>');
			}

			if ((packtype == 'Parcel') && (shipToCountryCode == 'US')) {
				mailClassEle.append('<option value="LibraryMail">Library Mail</option> ');
				mailClassEle.append('<option value="MediaMail">Media Mail</option> ');
			}
		} else if (packtype == 'Letter') {
			mailClassEle.children().remove().end();

			if (shipToCountryCode == 'US') {
				mailClassEle.append('<option value="First">First Class Mail</option>');
				mailClassEle.append('<option value="CriticalMail">Critical Mail</option>');
			} else {
				mailClassEle.append('<option value="FirstClassMailInternational">First-Class Mail International</option>');
			}

			endiciaSetPackageDimensions(8.375, 0, 4.125, false);
		} else if (packtype == 'Card') {
			mailClassEle.children().remove().end();

			if (shipToCountryCode == 'US') {
				mailClassEle.append('<option value="First">First Class Mail</option>');
			}

			endiciaSetPackageDimensions(6.9, 0, 4.9, false);
		}
		else if (packtype == 'Flat') {
			mailClassEle.children().remove().end();

			if (shipToCountryCode == 'US') {
				mailClassEle.append('<option value="First">First Class Mail</option>');
				mailClassEle.append('<option value="Priority">Priority Mail</option>');
				mailClassEle.append('<option value="CriticalMail">Critical Mail</option>');
			}
			else {
				mailClassEle.append('<option value="FirstClassMailInternational">First-Class Mail International</option>');
				mailClassEle.append('<option value="PriorityMailInternational">Priority Mail International</option>');
			}

			$('#pwidth').val('0');
		}
		else if(packtype == 'SmallFlatRateBox' || packtype == 'MediumFlatRateBox' || packtype == 'LargeFlatRateBox' ||
			packtype == 'SmallFlatRateEnvelope' || packtype == 'FlatRateEnvelope' || packtype == 'FlatRateLegalEnvelope' ||
			packtype == 'FlatRatePaddedEnvelope' || packtype == 'FlatRateWindowEnvelope' ||
			packtype == 'FlatRateCardboardEnvelope' || packtype == 'FlatRateGiftCardEnvelope')
		{
			mailClassEle.children().remove().end();

			if (shipToCountryCode == 'US') {
				mailClassEle.append('<option value="Priority">Priority Mail</option>');
			}
			else {
				mailClassEle.append('<option value="PriorityMailInternational">Priority Mail International</option>');
			}

			if (packtype == 'SmallFlatRateBox') {
				endiciaSetPackageDimensions(8.625, 1.625, 5.375, false);
			}

			if (packtype == 'MediumFlatRateBox') {
				endiciaSetPackageDimensions(13.625, 3.375, 11.875, true);
			}

			if (packtype == 'LargeFlatRateBox') {
				endiciaSetPackageDimensions(12, 5.5, 12, false);
			}

			if (packtype == 'FlatRateEnvelope') {
				endiciaSetPackageDimensions(12.5, 0, 9.5, true);
			}

			if (packtype == 'SmallFlatRateEnvelope') {
				endiciaSetPackageDimensions(6, 0, 10);
			}

			if (packtype == 'FlatRateWindowEnvelope') {
				endiciaSetPackageDimensions(5, 0, 10);
			}

			if (packtype == 'FlatRatePaddedEnvelope') {
				endiciaSetPackageDimensions(12.5, 0, 9.5, false);
			}

			if (packtype == 'FlatRateLegalEnvelope') {
				endiciaSetPackageDimensions(15, 0, 9.5, true);
			}

			if (packtype == 'FlatRateGiftCardEnvelope' || packtype == 'FlatRateCardboardEnvelope') {
				endiciaSetPackageDimensions(10, 0, 7, false);
			}
		}

		endiciaOnMailClassChange();
	};

	endiciaGetPostageSubmit = function() {
		var modal = $('#modal-order-endicia');
		var errors = [];

		if ((endiciaGetValue('endicia_MailShape')) == 'Choose') {
			errors.push({field: '#field-endicia_MailShape', message: trans.order_endicia.select_package_type});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_MailShape');
			return false;
		}

		if ((endiciaGetValue('endicia_MailClass')) == 'ChooseFirst') {
			errors.push({field: '#field-endicia_MailClass', message: trans.order_endicia.select_delivery_service});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_MailClass');
			return false;
		}

		if (endiciaIsEmpty('endicia_WeightOz')) {
			errors.push({field: '#field-endicia_WeightOz', message: trans.order_endicia.enter_package_weight});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_WeightOz');
			return false;
		}

		if (endiciaIsEmpty('endicia_DeclaredValue')) {
			errors.push({field: '#field-endicia_DeclaredValue', message: trans.order_endicia.enter_package_value});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_DeclaredValue');
			return false;
		}

		if (endiciaIsEmpty('endicia_PackageDescription')) {
			errors.push({field: '#field-endicia_PackageDescription', message: trans.order_endicia.enter_package_description});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_PackageDescription');
			return false;
		}

		if (endiciaIsEmpty('endicia_PackageDimension_length')) {
			errors.push({field: '#field-endicia_PackageDimension_length', message: trans.order_endicia.enter_package_length});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_PackageDimension_length');
			return false;
		}

		if (endiciaIsEmpty('endicia_PackageDimension_width')) {
			errors.push({field: '#field-endicia_PackageDimension_width', message: trans.order_endicia.enter_package_width});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_PackageDimension_width');
			return false;
		}

		if (endiciaIsEmpty('endicia_PackageDimension_height')) {
			errors.push({field: '#field-endicia_PackageDimension_height', message: trans.order_endicia.enter_package_height});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_PackageDimension_height');
			return false;
		}

		var shipToCountryCode = endiciaGetValue('endicia_shiptocountrycode');
		var mpack = endiciaGetValue('endicia_MailShape');
		var mweight = parseFloat(endiciaGetValue('endicia_WeightOz'));
		var mclass = endiciaGetValue('endicia_MailClass');
		var mvalue = parseFloat(endiciaGetValue('endicia_DeclaredValue'));
		var mshiptocountry = endiciaGetValue('endicia_shiptocountry');
		var mlength = parseFloat(endiciaGetValue('endicia_PackageDimension_length'));
		var mwidth = parseFloat(endiciaGetValue('endicia_PackageDimension_width'));
		var mheight = parseFloat(endiciaGetValue('endicia_PackageDimension_height'));

		if (mpack == 'Letter' && mweight > 3.5) {
			errors.push({field: '#field-endicia_WeightOz', message: trans.order_endicia.message_maximum_weight_standard});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_WeightOz');
			return false;
		}

		if (mlength > 130) {
			errors.push({field: '#field-endicia_PackageDimension_length', message: trans.order_endicia.message_maximum_length_package});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_PackageDimension_length');
			return false;
		}

		if (mwidth > 130) {
			errors.push({field: '#field-endicia_PackageDimension_width', message: trans.order_endicia.message_maximum_width_package});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_PackageDimension_width');
			return false;
		}

		if (mheight > 130) {
			errors.push({field: '#field-endicia_PackageDimension_height', message: trans.order_endicia.message_maximum_height_package});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_PackageDimension_height');
			return false;
		}

		if ((endiciaGetIsChecked('endicia_CertifiedMail')) && ((mclass != 'First') && (mclass != 'PriorityMail'))) {
			errors.push({field: '#field-endicia_CertifiedMail', message: trans.order_endicia.message_certified_mail_first_class});
			AdminForm.displayModalErrors(modal, errors);
			endiciaResetCheckbox('endicia_CertifiedMail');
			endiciaSetfocus('endicia_CertifiedMail');
			return false;
		}

		if ((mclass == 'CriticalMail') && ((mpack != 'Letter') &&  (mpack != 'Flat'))) {
			errors.push({field: '#field-endicia_CriticalMail', message: trans.order_endicia.message_certified_mail_letter});
			AdminForm.displayModalErrors(modal, errors);
			endiciaResetCheckbox('endicia_CriticalMail');
			return false;
		}

		if ((mshiptocountry != 'United States') && (endiciaGetIsChecked('endicia_ReturnReceipt'))) {
			errors.push({field: '#field-endicia_ReturnReceipt', message: trans.order_endicia.message_cannot_select_return_receipt});
			AdminForm.displayModalErrors(modal, errors);
			endiciaResetCheckbox('endicia_ReturnReceipt');
			endiciaSetfocus('endicia_ReturnReceipt');
			return false;
		}

		if ((mclass == 'endicia_StandardMail') && (mweight > 16)) {
			errors.push({field: '#field-endicia_MailClass', message: trans.order_endicia.message_standard_mail_cannot_weigh});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_MailClass');
			return false;
		}

		if ((endiciaGetIsChecked('endicia_RestrictedDelivery')) && ((mvalue < 200) || (!endiciaGetIsChecked('endicia_CertfiedMail')))) {
			errors.push({field: '#field-endicia_RestrictedDelivery', message: trans.order_endicia.message_restricted_delivery_selected})
			AdminForm.displayModalErrors(modal, errors);
			endiciaResetCheckbox('endicia_RestrictedDelivery');
			endiciaSetfocus('endicia_RestrictedDelivery');
			return false;
		}

		if ((shipToCountryCode == 'US') && mclass == ('ExpressMailInternational')) {
			errors.push({field: '#field-endicia_MailShape', message:  trans.order_endicia.message_cannot_ship_international_mail});
			AdminForm.displayModalErrors(modal, errors);
			return false;
		}

		if ((mclass == 'Express' || mclass == 'ExpressMailInternational') && (mweight < 13)) {
			errors.push({field: '#field-endicia_WeightOz', message: trans.order_endicia.message_express_mail_weigh_more});
			AdminForm.displayModalErrors(modal, errors);
			endiciaSetfocus('endicia_WeightOz');
			return false;
		}

		var modeEle = $('input[name=mode]');
		var _formMode = modeEle.val();
		modeEle.val('getPostage');

		AdminForm.displaySpinner(trans.order_endicia.message_generate_shipping_label);

		var newForm = $('<form></form>');

		$(modal).find('form input,form select,form textarea').each(function(index, input){
			var newInput = $(input).clone();
			var newName = $(input).attr('name').replace('endicia_', '');

			if (newName == 'PackageDimension_width') newName = 'Width';
			if (newName == 'PackageDimension_height') newName = 'Height';
			if (newName == 'PackageDimension_length') newName = 'Length';

			$(newInput).removeAttr('id');
			$(newInput).attr('name', newName);

			$(newForm).append(newInput);
		});

		var formData = $(newForm).serialize();

		AdminForm.sendRequest(
			'admin.php?p=endicia_generatepostagelabel&chart=true&action=generate',
			formData,
			'Please wait',
			function(data) {
				$('input[name=mode]').val(_formMode);

				AdminForm.hideSpinner();

				if (data.result == 1) {
					$('#modal-order-endicia').modal('hide');
					endiciaShowLabelDialog(data.labelUrl, data.trackingNumber, true);
				} else {
					alert(data.error);
				}
			},
			function() {
				$('input[name=mode]').val(_formMode);
				AdminForm.hideSpinner();
			}
		);

		modeEle.val(_formMode);

		return false;
	};

	endiciaShowLabelDialog = function(labelUrl, trackingNumber, fromGenerate) {
		var ifrm = document.getElementById('endiciaLabelIframe');
		ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;

		iframeHead = '<link rel="stylesheet" type="text/css" href="content/admin/styles/new/print.css?v={$APP_VERSION}">';

		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");

		if (msie > 0) {
			iframeHead += '<link rel="stylesheet" type="text/css" href="content/admin/styles/new/print-ie.css?v={$APP_VERSION}" media="print">';
		} else if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
			iframeHead += '<link rel="stylesheet" type="text/css" href="content/admin/styles/new/print-chrome.css?v={$APP_VERSION}" media="print">';
		}

		ifrm.document.open();
		ifrm.document.write('<html><head>' + iframeHead + '</head><body style="padding:0px;margin:0px;"><img class="print-label-image" style="border:none;display:block;margin: 0 auto;" src="admin.php?p=endicia&action=view-label&f=' + labelUrl + '"></body></html>');
		ifrm.document.close();

		var theModal = $('#modal-order-endicia-view');

		if (fromGenerate) {
			theModal.on('hide.bs.modal', function() {
				document.location = 'admin.php?p=order&id='+currentOrderId;
			});
		}

		theModal.data('endiciaLabelImageUrl', 'admin.php?p=endicia&action=view-label&f=' + labelUrl)
			.modal('show');
	};

	endiciaAddFundsSubmit = function(form) {
		if (endiciaIsEmpty('passconfirm')) {
			alert(trans.order_endicia.confirm_endicia_passphrase);
			endiciaSetfocus('passconfirm');
			return false;
		}

		if (endiciaIsEmpty('amount_to_add')) {
			alert(trans.order_endicia.message_add_amount_account);
			return false;
		}

		$('#endiciapostagestep1').hide();
		$('#endiciapostagestep2').show();

		AdminForm.sendRequest(
			'admin.php?p=endicia&action=add-funds-to-account&chart=true',
			$('#endiciapostagestep1 input').serialize(),
			'Please wait',
			function(response) {
				if(response.result == 1) {
					$('#endiciapostagestep2').hide();
					$('#endiciabalance').hide();
					$('#endiciabalanceupdated').show();
					$('#updatebal').html(response.newbal);
					$('#endiciaAddPostage').modal('hide');
				}
				else {
					if(response.result == 0) {
						$('#endiciapostagestep2').hide();
						$('#endiciapostagestep1').show();
						$('#error1').show();
					}
					else {
						$('#endiciapostagestep2').hide();
						$('#endiciapostagestep1').show();

						var msg = response.message ?
							response.message :
							trans.order_endicia.message_unable_add_fund;

						$('#error2').text(msg).show();
					}
				}
			}
		);

		return false;
	};

	endiciaResetCheckbox = function(inputName) {
		return $('input[name='+inputName+']').attr('checked', false);
	};

	endiciaGetIsChecked = function(inputName) {
		return $('input[name='+inputName+']').is(':checked');
	};

	endiciaGetRadioValue = function(inputName) {
		return $('input[name='+inputName+']:checked').val();
	};

	endiciaGetValue = function(inputName) {
		return $('[name='+inputName+']').val();
	};

	endiciaSetfocus = function(inputName) {
		$('input[name='+inputName+']').focus();
	};

	endiciaIsEmpty = function(inputName) {
		return $.trim($('input[name='+inputName+']').val()) == '';
	};

	endiciaSetOptionValue = function (fieldId, selectedValue) {
		$("#"+fieldId+" option").each(function() {
			if ($(this).val() == selectedValue) {
				 $(this).attr("selected","selected");
            }
			else {
				$(this).removeAttr("selected");
			}
		});
	}
	
	init();
}(jQuery));


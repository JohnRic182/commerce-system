var OrderStamps = (function($) {
	var init, handleShippingRateChange, currentShippingRateEle, setRateOptions, currentRateOptions = false,
		checkDependencies, enteredInsuranceAmount, handleViewLabel;

	init = function() {
		$(document).ready(function() {
			$('#modal-order-stamps').on('show.modal.bs', function() {
				var theModal = $(this);
				$('#stamps-generate-label-panel').html('').hide();
				$('.generate-label-button').hide();
				$('.get-rates-button').show();
				$('#stamps-get-rates-panel').show();
				theModal.find('form').get(0).reset();
			});

			$('form[name="form-order-stamps"]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				AdminForm.clearAlerts();

				if ($('#stamps-get-rates-panel').is(':visible')) {
					//get rates
					var lbsField = theForm.find('input[name="stamps_rate_weight_lbs"]');
					var weight_lbs = parseFloat(lbsField.val());
					var weight_oz = parseFloat(theForm.find('input[name="stamps_rate_weight_oz"]').val());
					var insuranceAmount = parseFloat(theForm.find('input[name="stamps_insured_value"]').val());
					var declaredValueAmount = parseFloat(theForm.find('input[name="stamps_declared_value"]').val());

					enteredInsuranceAmount = insuranceAmount > 0;

					if (isNaN(weight_lbs)) weight_lbs = 0;
					if (isNaN(weight_oz)) weight_oz = 0;

					var errors = [];
					if (weight_lbs <= 0 && weight_oz <= 0) {
						errors.push({
							field: lbsField,
							message: trans.order_stamps.shipping_weight_required
						});
					}

					if (errors.length > 0) {
						AdminForm.displayModalErrors(theModal, errors);
					} else {
						AdminForm.sendRequest(
							'admin.php?p=order_stamps&mode=get-rates',
							theForm.serialize(), trans.order_stamps.message_shipping_rates,
							function(data) {
								if (data.status == 1) {
									currentRateOptions = data.options;
									$('#stamps-get-rates-panel').hide();
									$('.get-rates-button').hide();
									$('#stamps-generate-label-panel').html(data.html).show();
									$('.generate-label-button').show();

									theForm.find('select[name="ContentType"]').change(function() {
										var typeVal = $(this).val();
										theForm.find('input[name="ContentDescription"]').closest('.form-group').toggle(typeVal == 'Other');
									});

									currentShippingRateEle = theForm.find('select[name="shipping_rate"]');
									currentShippingRateEle.change(function() {
										handleShippingRateChange();
									});
									currentShippingRateEle.closest('.form-group').removeClass('col-sm-6');
									handleShippingRateChange();
									theForm.find('input[name="stamps_label_unique_id"]').val(data.unique_id);
								} else {
									if (data.message !== undefined) {
										AdminForm.displayModalAlert(theModal, data.message, 'danger');
									} else if (data.errors !== undefined) {
										AdminForm.displayModalErrors(theModal, data.errors);
									}
								}
							}
						);
					}
				} else {
					//generate label
					AdminForm.confirm(trans.order_stamps.confirm_generate_label +
						trans.order_stamps.message_deduct_funds, function() {
						AdminForm.sendRequest(
							'admin.php?p=order_stamps&mode=generate-label',
							theForm.serialize(), trans.order_stamps.message_generate_label,
							function(data) {
								if (data.status == 1) {
									theModal.modal('hide');
									handleViewLabel(
										'admin.php?p=order_stampscom_ajax&printing&action=label_view_iframe&oid=' +
										currentOrderId + '&olid=' + data.olid, true
									);
								} else {
									if (data.message !== undefined) {
										AdminForm.displayModalAlert(theModal, data.message, 'danger');
									} else if (data.errors !== undefined) {
										AdminForm.displayModalErrors(theModal, data.errors);
									}
								}
							}
						);
					});
				}

				return false;
			});

			$('.stamps-view-label-link').click(function() {
				var olid = $(this).attr('data-olid');
				handleViewLabel(
					'admin.php?p=order_stampscom_ajax&printing&action=label_view_iframe&oid=' +
					currentOrderId + '&olid=' + olid, false
				);
				return false;
			});

			$('.stamps-cancel').click(function() {
				var olid = $(this).attr('data-olid');

				AdminForm.confirm(trans.order_stamps.confirm_cancel_label, function() {
					AdminForm.sendRequest(
						'admin.php?p=order_stamps&mode=cancel-label&oid='+currentOrderId,
						'olid='+olid, null,
						function(data) {
							if (data.status == 1) {
								document.location = 'admin.php?p=order&id='+currentOrderId;
							} else {
								if (data.errors !== undefined) {
									AdminForm.displayErrors(data.errors);
								} else if (data.message !== undefined) {
									AdminForm.displayAlert(data.message, 'danger');
								}
							}
						}
					);
				});

				return false;
			});
		});
	};

	handleViewLabel = function(src, fromGenerate) {
		$('#stampsLabelIframe').attr('src', src);
		$('#stampsLabelIframe').load(function(){

			var iframeBody = $('#stampsLabelIframe').contents().find('body');
			var iframeHead = $('#stampsLabelIframe').contents().find('head');

			$(iframeHead).append('<link rel="stylesheet" type="text/css" href="content/admin/styles/print.css?v={$APP_VERSION}">');

			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE ");

			if (msie > 0)
			{
				var doc = document.getElementById('stampsLabelIframe').contentDocument;
				if (!doc || doc == null)
				{
					var doc = document.getElementById('frame').contentWindow.document;
				}
				doc.createStyleSheet('content/admin/styles/print.css');
				var printCss = doc.createStyleSheet('content/admin/styles/print-ie.css');
				printCss.media = 'print';
			}
			else if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase()))
			{
				$(iframeHead).append('<link rel="stylesheet" type="text/css" href="content/admin/styles/print-chrome.css?v={$APP_VERSION}" media="print">');
			}
		});

		var theModal = $('#modal-order-stamps-view');
		theModal.find('.print-button').click(function() {
			window.frames['stampsLabelIframe'].focus();
			window.frames['stampsLabelIframe'].print();
		});

		if (fromGenerate) {
			theModal.on('hide.bs.modal', function() {
				document.location = 'admin.php?p=order&id='+currentOrderId;
			});
		}

		theModal.modal('show');

		return false;
	};

	handleShippingRateChange = function() {
		var rateId = currentShippingRateEle.val();

		setRateOptions(rateId);
	};

	setRateOptions = function(rateId) {
		//TODO: If there are no valid combination for an option, it should be hidden

		if (!currentRateOptions) return;

		var insuranceHtml = '';
		var confirmationHtml = '';
		var addOnsHtml = '';

		if (currentRateOptions[rateId] != undefined)
		{
			var rateServiceType = currentRateOptions[rateId]['type'];

			if (currentRateOptions[rateId]['addons'] != undefined)
			{
				$.each(currentRateOptions[rateId]['addons'], function(addOnCode, addOnData){
					var addOnRate = parseFloat(addOnData.amount);

					if (addOnCode == 'US-A-INS' || addOnCode == 'SC-A-INS')
					{
						if (insuranceHtml == '')
						{
							insuranceHtml =
								'Insurance:<br>' +
									'<input type="radio" class="stamps-add-on-option" id="stamps-option-insurance-none" name="stamps_rate_addons[insurance]" value="_NONE"> ' +
									'<label for="stamps-option-insurance-none">None</label><br>';

						}
						insuranceHtml += '<input type="radio" class="stamps-add-on-option" id="stamps-option-' + addOnCode + '" name="stamps_rate_addons[insurance]" value="' + addOnCode + '"> ' +
							'<label for="stamps-option-' + addOnCode + '">' + addOnData.name + (addOnRate > 0 ? ' - $'  + addOnRate.toFixed(2) : '') + '</label><br>';
					}
					else if (addOnCode == 'US-A-DC' || addOnCode == 'US-A-SC')
					{
						if (confirmationHtml == '')
						{
							confirmationHtml =
								'Confirmation:<br>';

							if (rateServiceType != 'US-PM')
							{
								//TODO: This was not being assigned to anything?!?
//								'<input type="radio" class="stamps-add-on-option" id="stamps-option-confirmation-none" name="stamps_rate_addons[confirmation]" value="_NONE"> ' +
//								'<label for="stamps-option-confirmation-none">None</label><br>';
							}
						}
						confirmationHtml += '<input type="radio" class="stamps-add-on-option" id="stamps-option-' + addOnCode + '" name="stamps_rate_addons[confirmation]" value="' + addOnCode + '"> ' +
							'<label for="stamps-option-' + addOnCode + '">' + addOnData.name + (addOnRate > 0 ? ' - $'  + addOnRate.toFixed(2) : '') + '</label><br>';
					}
					else
					{
						if (addOnsHtml == '') addOnsHtml = 'Other Options:<br>';
						addOnsHtml += '<input type="checkbox" class="stamps-add-on-option" id="stamps-option-' + addOnCode + '" name="stamps_rate_addons[]" value="' + addOnCode + '"> ' +
							'<label for="stamps-option-' + addOnCode + '">' + addOnData.name + (addOnRate > 0 ? ' - $'  + addOnRate.toFixed(2) : '') + '</label><br>';
					}
				});
			}
		}

		if (!enteredInsuranceAmount) insuranceHtml = '';

		var panel = $('#addons-body');
		panel.html('<div class="col-xs-12">'+confirmationHtml + (confirmationHtml != '' ? '<br>' : '') + insuranceHtml + (confirmationHtml + insuranceHtml != '' ? '<br>' : '') + addOnsHtml+'</div>');

		panel.find('input[name="stamps_rate_addons[insurance]"]:first').attr('checked', 'checked').prop('checked', 1);

		panel.find('input[name="stamps_rate_addons[confirmation]"]:first').attr('checked', 'checked').prop('checked', 1);

		$('.stamps-add-on-option').each(function(index, addOnOption){
			$(addOnOption).click(function(){
				checkDependencies($(this).attr('value'), $(this).prop('checked'));
			});
		});
	};

	checkDependencies = function(addOnOptionCode, isChecked) {
		var rateId = currentShippingRateEle.val();
		var stopped = false;
		if (addOnOptionCode != '_NONE' && currentRateOptions[rateId]['addons'] != undefined) {
			if (isChecked) {
				// check if there selected option for which one is prohibited
				$.each(currentRateOptions[rateId]['addons'], function(addOnCode, addOnData){
					if (stopped) return;
					if (addOnOptionCode != addOnCode && $('#stamps-option-' + addOnCode).prop('checked') && $.inArray(addOnOptionCode, addOnData['prohibited']) > -1)
					{
						alert(trans.order_stamps.cannot_select_option + addOnData.name);
						$('#stamps-option-' + addOnOptionCode).prop('checked', false);

						if ((addOnOptionCode == 'US-A-DC') || (addOnOptionCode == 'US-A-SC'))
						{
							$('#stamps-option-confirmation-none').prop('checked', true);
						}

						if ((addOnOptionCode == 'US-A-INS') || (addOnOptionCode == 'SC-A-INS'))
						{
							$('#stamps-option-insurance-none').prop('checked', true);
						}

						stopped = true;
					}
				});
			}
		}
		return true;
	};

	init();
}(jQuery));

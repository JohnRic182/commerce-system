var Import = (function($) {
	var init;

	init = function() {
		$('#form-import-start').submit(function(e){
			AdminForm.displaySpinner(trans.import.uploading_data);
		});

		$('#form-import-assign').submit(function(e) {
			$.each(importFields, function(fieldName, field) {
				if (field.required) {
					var fieldAssigned = false;
					$('#form-import-assign').find('.assigned-field').each(function() {
						if ($(this).val() == fieldName) fieldAssigned = true;
					});

					if (!fieldAssigned) {
						AdminForm.displayAlert(trans.import.assign_required_fields, 'danger');

						e.preventDefault();
						return false;
					}
				}
			});

			return true;
		});

		$("#field-bulk").change(function(){
			var fileSize = check_file_size(this.files[0].size);
			var ext = $('#field-bulk').val().split('.').pop().toLowerCase();

			if($.inArray(ext, ['csv']) == -1) {
				alert(trans.import.invalid_type);
				$('#field-bulk').val('');
				$(this).closest('div.form-group').addClass('has-error has-feedback');
				$(this).parent().find('.warning-sign, .help-block.error').removeClass('hidden');
				return;
			}

			if (fileSize.status == 0){
				alert(fileSize.message);
				$(this).closest('div.form-group').addClass('has-error has-feedback');
				$(this).parent().find('.warning-sign, .help-block.error').removeClass('hidden');
				return;
			}

			if (fileSize.status){
				$(this).closest('div.form-group').removeClass('has-error has-feedback');
				$(this).parent().find('.warning-sign').addClass('hidden');
				return;
			}

		});
	};

	init();
}(jQuery));
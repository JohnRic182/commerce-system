var ProductReviews = (function($) {
	var init, toggleProductReviewsSettings, toggleProductReviewsAutoApproveRatings;

	toggleProductReviewsSettings = function() {
		var isEnabled = $('#field-productReviewSettings-ReviewsEnable').is(':checked');

		$('#field-productReviewSettings-ReviewsAutoApprove').closest('.form-group').toggle(isEnabled);
		$('#field-productReviewSettings-ReviewsAutoApproveRating').closest('.form-group').toggle(isEnabled);
		$('#field-productReviewSettings-ReviewsAutoApprovePurchased').closest('.form-group').toggle(isEnabled);
	};

	toggleProductReviewsAutoApproveRatings = function() {
		var enabled = $('#field-productReviewSettings-ReviewsAutoApprove').is(':checked');

		$('#field-productReviewSettings-ReviewsAutoApproveRating').closest('.form-group').toggle(enabled);
	};

	init = function() {
		$(document).ready(function() {
			$('#modal-settings').find('.form-group').removeClass('col-md-6').removeClass('col-sm-6');

			$('form[name="form-settings"]').submit(function() {
				var theForm = $(this);
				var theModal = $(this).closest('.modal');

				AdminForm.sendRequest(
					theForm.attr('action'),
					theForm.serialize(),
					null,
					function(data) {
						if (data.status == 1) {
							document.location = 'admin.php?p=products_reviews';
						} else {
							if (data.errors !== undefined) {
								AdminForm.displayModalErrors(theModal, data.errors);
							} else {
								AdminForm.displayModalAlert(theModal, data.message, 'danger');
							}
						}
					}
				);

				return false;
			});

			$('#field-productReviewSettings-ReviewsEnable').change(function() {
				toggleProductReviewsSettings();
			});
			toggleProductReviewsSettings();

			$('#field-productReviewSettings-ReviewsAutoApprove').change(function() {
				toggleProductReviewsAutoApproveRatings();
			});
			toggleProductReviewsAutoApproveRatings();

			/**
			 * Handle update review status button
			 */
			$('[data-action="action-products-reviews-update-statuses"]').click(function(){

				var updateMode = $('input[name="form-products-reviews-update-statuses-mode"]:checked').val();
				var new_status = $('select[name="form-products-reviews-update-statuses-new-status"]').val();

				if (updateMode == 'selected')
				{
					var ids = '';
					$('.checkbox-product-review:checked').each(function(){
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});
					if (ids != '')
					{
						window.location='admin.php?p=products_review&mode=update_product_review_status&new_status='+new_status+'&updateMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
					}
					else
					{
						alert(trans.products_reviews.select_product_review);
					}
				}
				else if (updateMode == 'search')
				{
					window.location='admin.php?p=products_review&mode=update_product_review_status&new_status='+new_status+'&updateMode=search' + '&nonce=' + $(this).attr('nonce');
				}
			});
			
			/**
			 * Handles delete reviews
			 */
			$('[data-action="action-products-reviews-delete"]').click(function() {
				var deleteMode = $('input[name="form-products-reviews-delete-mode"]:checked').val();
				if (deleteMode == 'selected') {
					var ids = '';
					$('.checkbox-product-review:checked').each(function(){
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});
					if (ids != '') {
						window.location='admin.php?p=products_review&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
					} else {
						alert(trans.products_reviews.select_product_review);
					}
				}
				else if (deleteMode == 'search') {
					window.location='admin.php?p=products_review&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
				}
			});
		});
	};
	
	/**
	 * Handles delete per review
	 */
	$(".delete-review").click(function() {
		var id = $(this).attr("review-id");
		var nonce = $(this).attr("nonce");
		AdminForm.confirm(trans.products_reviews.confirm_remove_product_review, function() {
			window.location = 'admin.php?p=products_review&mode=delete&id=' + id + '&nonce=' + nonce;
		});
	});

	init();

}(jQuery));

(function ( $ ) {
	$.fn.reportCard = function (options) {
		var settings = $.extend({
			name: 'report-card',
			type: 'numeric',
			class: 'col-xs-12 col-sm-6 col-md-6 col-lg-3',
			title: 'ReportCard',
			subTitle: '&nbsp;',
			dataSource: null,
			dataSourceUrl: null,
			dataSourceCallback: null,
			append: false,
			animate: false,
			animateSpeed: 500,
			animateDelay: 0,

			value: 0,

			valueIcon: false,
			valueCompareTo: 0,

			valueCompareToText: '&nbsp;',
			valueCompareToInverse: false,

			animateValue: false,
			animateValueSpeed: 720,
			animateValueSteps: 20,
			animateValueDelay: 0,
			animateValueStart: 0, // for now it is always 0
			valueFormatCallback: null,

			buttonDetailsText: 'More',
			buttonDetailsUrl: null,
		}, options);

		var obj = this;
		var cardId = 'report-card-' + settings.name;

		/**
		 * Get value icon HTML
		 */
		var getValueIconHtml= function() {
			var iconClass = 'glyphicon-pause';
			var iconColor = 'gray';
			if (settings.value > settings.valueCompareTo) {
				iconClass = 'glyphicon-triangle-top';
				iconColor = settings.valueCompareToInverse ? 'red' : 'green';
			} else if (settings.value < settings.valueCompareTo) {
				iconClass = 'glyphicon-triangle-bottom';
				iconColor = settings.valueCompareToInverse ? 'green' : 'red';
			}

			return ' <span class="glyphicon ' + iconClass + '" style="font-size:0.7em;color:' + iconColor + '"></span>';
		};

		/**
		 * Formats value
		 * @returns {*}
		 */
		var formatValue = function(value) {

			if (settings.valueFormatCallback != null) return settings.valueFormatCallback(value);

			switch (settings.type) {
				case 'currency': return getAdminPrice(value); break;
				case 'percent': return getAdminNumber(value) + '%'; break;
				default:
				case 'numeric': return getAdminNumber(value); break;
			}
		};

		/**
		 * Animate value
		 */
		var animateValue = function() {

			var animationPosition = 0;
			var runningValue = settings.animateValueStart;
			var increment = (settings.value - settings.animateValueStart) / settings.animateValueSteps;

			var animationInterval = setInterval(function () {

				$('#' + cardId + ' .report-card-value').html(formatValue(Math.round(runningValue*100) / 100) + (settings.valueIcon ? getValueIconHtml() : ''));

				runningValue += increment;
				animationPosition++;

				if (animationPosition == settings.animateValueSteps) {
					clearInterval(animationInterval);
					$('#' + cardId + ' .report-card-value').html(formatValue(Math.round(settings.value * 100) / 100) + (settings.valueIcon ? getValueIconHtml() : ''));
				}
			}, settings.animateValueSpeed / settings.animateValueSteps);
		};

		/**
		 * Render HTML
		 */
		var card = $('<div id="' + cardId + '" class="report-card ' + settings.class +'" >' +
			'<div class="well">' +
				'<div class="row">' +
					'<div class="col-xs-12">' +
						'<h3 class="report-card-title">' + settings.title + '</h3>' +
						'<h4 class="report-card-subtitle">' + settings.subTitle + '</h4>' +
						'<div class="report-card-value">' +
							formatValue(settings.animateValue ? settings.animateValueStart: settings.value) +
							(settings.valueIcon ? getValueIconHtml() : '') +
						'</div>' +
						'<div class="report-card-value-compared-to">' + settings.valueCompareToText.replace('$val', formatValue(settings.valueCompareTo)) + '</div>' +
						'<div class="report-card-buttons">' +
							(settings.buttonDetailsUrl != null ? '<a href="' + settings.buttonDetailsUrl + '" class="btn btn-default">' + settings.buttonDetailsText + '</a>' : '&nbsp;') +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>');

		settings.append ? $(this).append(card) : $(this).html(card);

		/**
		 * Show card
		 */
		if (settings.animate) {
			if (settings.animateDelay) {
				setTimeout(function(){
					$(card).slideDown(settings.animateSpeed);
					if (settings.animateValue) animateValue();
				}, settings.animateDelay);
			} else {
				$(card).slideDown(settings.animateSpeed);
				if (settings.animateValue) animateValue();
			}
		} else {
			$(card).show();
			if (settings.animateValue) animateValue();
		}

		return this;
	};
}( jQuery ));
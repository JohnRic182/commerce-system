$(document).ready(function() {
	var toggleWholesaleForm = function() {
		var wholesaleType = $('#field-WholesaleDiscountType').val();
		if (wholesaleType == 'NO') {
			$('#field-WholesaleMaxLevels').closest('.form-group').hide();
			$('#field-WholesaleDiscount1').closest('.form-group').hide();
			$('#field-MinOrderSubtotalLevel1').closest('.form-group').hide();
			$('#field-WholesaleDiscount2').closest('.form-group').hide();
			$('#field-MinOrderSubtotalLevel2').closest('.form-group').hide();
			$('#field-WholesaleDiscount3').closest('.form-group').hide();
			$('#field-MinOrderSubtotalLevel3').closest('.form-group').hide();
		} else if (wholesaleType == 'PRODUCT') {
			$('#field-WholesaleMaxLevels').closest('.form-group').show();

			var levels = parseInt($('#field-WholesaleMaxLevels').val());
			$('#field-MinOrderSubtotalLevel1').closest('.form-group').show();
			$('#field-MinOrderSubtotalLevel2').closest('.form-group').toggle(levels > 1);
			$('#field-MinOrderSubtotalLevel3').closest('.form-group').toggle(levels > 2);

			$('#field-WholesaleDiscount1').closest('.form-group').hide();
			$('#field-WholesaleDiscount2').closest('.form-group').hide();
			$('#field-WholesaleDiscount3').closest('.form-group').hide();
		} else {
			$('#field-WholesaleMaxLevels').closest('.form-group').show();
			$('#field-WholesaleDiscount1').closest('.form-group').show();
			$('#field-MinOrderSubtotalLevel1').closest('.form-group').show();

			var levels = parseInt($('#field-WholesaleMaxLevels').val());
			$('#field-WholesaleDiscount2').closest('.form-group').toggle(levels > 1);
			$('#field-WholesaleDiscount3').closest('.form-group').toggle(levels > 2);
			$('#field-MinOrderSubtotalLevel2').closest('.form-group').toggle(levels > 1);
			$('#field-MinOrderSubtotalLevel3').closest('.form-group').toggle(levels > 2);
		}
	};

	$('#field-WholesaleDiscountType,#field-WholesaleMaxLevels').change(function() {
		toggleWholesaleForm();
	});

	toggleWholesaleForm();
});
(function($) {
    var init, handleRefundClick,
        vparcelOnMailClassChange, vparcelControlDeliveryService,
        vparcelGetPostageSubmit, vparcelShowLabelDialog,
        vparcelGetValue, vparcelSetfocus;

    init = function() {
        $(document).ready(function() {

            $('.vparcel-refund').click(function() {
                return handleRefundClick($(this).attr('data-picnum'));
            });

            $('input.number').keypress(function(evt) {
                var num = $(this).val();

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (num.indexOf('.') >= 0 && charCode == 46) {
                    return false;
                }

                return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
            });

            $("#vparcel-cancel-but").click(function(){
                $("span.sprite").removeClass("checked");
            });


            $('#vparcelLabelIframe').load(function(){

                var iframeBody = $('#vparcelLabelIframe').contents().find('body');
                var iframeHead = $('#vparcelLabelIframe').contents().find('head');

                $(iframeHead).append('<link rel="stylesheet" type="text/css" href="content/admin/styles/print.css?v={$APP_VERSION}">');

                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0)
                {
                    var doc = document.getElementById('vparcelLabelIframe').contentDocument;
                    if (!doc || doc == null)
                    {
                        var doc = document.getElementById('frame').contentWindow.document;
                    }
                    doc.createStyleSheet('content/admin/styles/print.css');
                    var printCss = doc.createStyleSheet('content/admin/styles/print-ie.css');
                    printCss.media = 'print';
                }
                else if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase()))
                {
                    $(iframeHead).append('<link rel="stylesheet" type="text/css" href="content/admin/styles/print-chrome.css?v={$APP_VERSION}" media="print">');
                }
            });

            $('#modal-order-vparcel-view').find('.btn-primary').click(function(){
                window.frames["vparcelLabelIframe"].focus();
                window.frames["vparcelLabelIframe"].print()
            });

            $('#modal-order-vparcel').find('form').submit(function(){
                vparcelGetPostageSubmit();
                return false;
            });

            $('#field-vparcel_MailClass').change(function(){
                vparcelOnMailClassChange();
            });

            $('#field-vparcel_labelType').change(function(){
                vparcelControlDeliveryService();
                var firstval =  $('select[name="vparcel_mailClass"]').find('option:visible:first').val();
                $('select[name="vparcel_mailClass"]').val(firstval);
            });

            vparcelControlDeliveryService();

            $('.vparcel-view-label-link').each(function(index, a){
                $(a).click(function(e){
                    e.stopPropagation();
                    vparcelShowLabelDialog($(a).attr('href'), $(a).attr('rel'), false);
                    return false;
                })
            });
        });
    };

    handleRefundClick = function(picNum) {

        var theForm = $('form[name="order-vparcel-refund"]');
        theForm.unbind('submit');

        var theModal = $('#modal-order-vparcel-refund');

        theForm.submit(function() {
            theForm.find('input[name="pic_number"]').val(picNum);
            AdminForm.sendRequest(
                'admin.php?p=vparcel&mode=request-refund',
                theForm.serialize(), null,
                function(data) {
                    if (data.status == 1) {
                        theModal.modal('hide');
                        document.location = 'admin.php?p=order&id='+currentOrderId;
                    } else {
                        if (data.errors !== undefined) {
                            AdminForm.displayModalErrors(theModal, data.errors);
                        } else if (data.message !== undefined) {
                            AdminForm.displayModalAlert(theModal, data.message, 'danger');
                        }
                    }
                }
            );

            return false;
        });

        theModal.modal('show');

        return false;
    };

    vparcelOnMailClassChange = function() {
        var mailShape = vparcelGetValue('vparcel_MailShape');
        var mailClass = vparcelGetValue('vparcel_MailClass');

        if (mailShape == 'Parcel' && mailClass == 'ParcelSelect') {
            $('#field-vparcel_SortType,#field-vparcel_EntryFacility').closest('.form-group').slideDown('fast');
        } else {
            $('#field-vparcel_SortType,#field-vparcel_EntryFacility').closest('.form-group').slideUp('fast');
        }
    };

    vparcelControlDeliveryService = function()
    {
        var ltype = vparcelGetValue('vparcel_labelType');
        var modal = $('#modal-order-vparcel');

        if (ltype == 'domestic'){
            $("fieldset[id='advanced']").hide();

            $("fieldset[id='service']").show();

            $(modal).find('select[name="vparcel_mailClass"] option').each(function(index, option){
                if ($(option).val().indexOf('International') !== -1){
                    $(option).hide();
                }
                else{
                    $(option).show();
                }
            });

        }
        else{
            $("fieldset[id='advanced']").show();
            $("fieldset[id='service']").hide();

            var firstval =  $('select[name="vparcel_service"]').find('option:visible:first').val();
            $('select[name="vparcel_service"]').val(firstval);

            $(modal).find('select[name="vparcel_mailClass"] option').each(function(index, option){
                if ($(option).val().indexOf('International') !== -1){
                    $(option).show();
                }
                else{
                    $(option).hide();
                }
            });
        }

        $('#field-vparcel_PackageDimension-length').val('');
        $('#field-vparcel_PackageDimension-height').val('');
        $('#field-vparcel_PackageDimension-width').val('');

        vparcelOnMailClassChange();
    };

    vparcelGetPostageSubmit = function() {
        var modal = $('#modal-order-vparcel');
        var errors = [];
        var ltype = vparcelGetValue('vparcel_labelType');

	    AdminForm.clearAlerts();

        if ((vparcelGetValue('vparcel_MailShape')) == 'Choose') {
            errors.push({field: '#field-vparcel_MailShape', message: trans.order_vparcel.select_package_type});
            AdminForm.displayModalErrors(modal, errors);
            vparcelSetfocus('vparcel_MailShape');
            return false;
        }

        if ((vparcelGetValue('vparcel_MailClass')) == 'ChooseFirst') {
            errors.push({field: '#field-vparcel_MailClass', message: trans.order_vparcel.select_delivery_service});
            AdminForm.displayModalErrors(modal, errors);
            vparcelSetfocus('vparcel_MailClass');
            return false;
        }

        if ((vparcelGetValue('vparcel_category')) == 'Other') {
            if ((vparcelGetValue('vparcel_contentsExplanation')) == '') {
                errors.push({field: '#field-vparcel_contentsExplanation', message: trans.order_vparcel.message_explanation_cannot_empty});
                AdminForm.displayModalErrors(modal, errors);
                vparcelSetfocus('vparcel_contentsExplanation');
                return false;
            }
        }

        var checkedItems = $(".shippingtems:checked").length;

        if (ltype == 'international'){
            if (checkedItems <= 0){
                errors.push({field: '#field-vparcel_labelType', message: trans.order_vparcel.message_select_parcel});
                AdminForm.displayModalErrors(modal, errors);
                vparcelSetfocus('vparcel_labelType');
                return false;
            }

            if ((vparcelGetValue('vparcel_eelPfc')) == '') {
                errors.push({field: '#field-vparcel_eelPfc', message: trans.order_vparcel.message_exemption_required});
                AdminForm.displayModalErrors(modal, errors);
                vparcelSetfocus('vparcel_eelPfc');
                return false;
            }
        }

        var shipToCountryCode = vparcelGetValue('vparcel_shiptocountrycode');
        var mpack = vparcelGetValue('vparcel_MailShape');
        var mweight = parseFloat(vparcelGetValue('vparcel_WeightOz'));
        var mclass = vparcelGetValue('vparcel_MailClass');
        var mvalue = parseFloat(vparcelGetValue('vparcel_DeclaredValue'));
        var mshiptocountry = vparcelGetValue('vparcel_shiptocountry');
        var mlength = parseFloat(vparcelGetValue('vparcel_PackageDimension_length'));
        var mwidth = parseFloat(vparcelGetValue('vparcel_PackageDimension_width'));
        var mheight = parseFloat(vparcelGetValue('vparcel_PackageDimension_height'));

        if (mweight <= 0) {
            errors.push({field: '#field-vparcel_WeightOz', message: trans.order_vparcel.message_weight_more});
            AdminForm.displayModalErrors(modal, errors);
            vparcelSetfocus('vparcel_WeightOz');
            return false;
        }

        var modeEle = $('input[name=mode]');
        var _formMode = modeEle.val();
        modeEle.val('getPostage');

        AdminForm.displaySpinner(trans.order_vparcel.message_generating_shipping_label);

        var newForm = $('<form></form>');

        $(modal).find('form input,form select,form textarea').each(function(index, input){

            var newName = $(input).attr('name').replace('vparcel_', '');
            var newval = $(input).val();
            if ($(input).attr('type') == 'checkbox'){
                if ($(input).is(":checked")){
                    newval = 1;
                }
                else{
                    newval = 0;
                }
            }

            var newInput = $('<input name="'+newName+'" value="'+newval+'"></input>');
            $(newForm).append(newInput);
        });

        var formData = $(newForm).serialize();

        AdminForm.sendRequest(
            'admin.php?p=vparcel&mode=request-generatepostagelabel',
            formData,
            'Please wait',
            function(data) {
                $('input[name=mode]').val(_formMode);

                AdminForm.hideSpinner();

                if (data.result == 1) {
                    $('#modal-order-vparcel').modal('hide');
                    vparcelShowLabelDialog(data.labelUrl, data.trackingNumber, true);
                } else {
	                AdminForm.displayModalAlert($(modal), data.error, 'danger');
                }
            },
            function() {
                $('input[name=mode]').val(_formMode);
                AdminForm.hideSpinner();
            }
        );

        modeEle.val(_formMode);

        return false;
    };

    vparcelShowLabelDialog = function(labelUrl, trackingNumber, fromGenerate) {
        var ifrm = document.getElementById('vparcelLabelIframe');
        var ltype = vparcelGetValue('vparcel_labelType');

        ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;

        iframeHead = '<link rel="stylesheet" type="text/css" href="content/admin/styles/print.css?v={$APP_VERSION}">';

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0) {
            iframeHead += '<link rel="stylesheet" type="text/css" href="content/admin/styles/print-ie.css?v={$APP_VERSION}" media="print">';
        } else if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
            iframeHead += '<link rel="stylesheet" type="text/css" href="content/admin/styles/print-chrome.css?v={$APP_VERSION}" media="print">';
        }

        ifrm.document.open();
        ifrm.document.write('<html><head>' + iframeHead + '</head><body style="padding:0px;margin:0px;"><img class="print-label-image" style="border:none;display:block;margin: 0 auto;" src="admin.php?p=vparcel&action=request-viewlabel&f=' + labelUrl + '"></body></html>');
        ifrm.document.close();

        var theModal = $('#modal-order-vparcel-view');
        if (ltype == 'international'){
            theModal.find("div.modal-dialog").width(1000);
        }

        if (fromGenerate) {
            theModal.on('hide.bs.modal', function() {
                document.location = 'admin.php?p=order&id='+currentOrderId;
            });
        }

        theModal.data('vparcelLabelImageUrl', 'admin.php?p=vparcel&action=request-viewlabel&f=' + labelUrl)
            .modal('show');
    };

    vparcelGetValue = function(inputName) {
        return $('[name='+inputName+']').val();
    };

    vparcelSetfocus = function(inputName) {
        $('input[name='+inputName+']').focus();
    };

    init();
}(jQuery));
var FraudMethods = (function($) {
    var init, editSettings, disableFraudMethod, activateFraudMethod;

    init = function() {
        $(document).ready(function() {
            $('#field-fraud_method_id').change(function() {
                AdminForm.removeErrors();
                var fraudMethodId = $.trim($('#field-fraud_method_id').val());
                if (fraudMethodId != '') {
                    AdminForm.sendRequest(
                        'admin.php?p=fraud_method&action=activate-form&fraud_method_id=' + fraudMethodId, null, null,
                        function(data) {
                            if (data.status == 1) {
                                var fraudMethodForm = $('#fraud-method-form');
                                fraudMethodForm.html(data.html);
                                forms.ensureTooltips();
                                $(fraudMethodForm.find('.form-control').get(0)).focus();
                            } else {
                                var errors = [];
                                errors.push({field:'#field-fraud_method_id', 'message': data.message});
                                AdminForm.displayModalErrors($('#modal-add-fraud').modal(), errors);
                            }
                        },
                        null, 'GET'
                    );
                } else {
                    $('#fraud-method-form').html('');
                }
            });

            if ($("#field-form-fraudlabs_ApiKey").length > 0){
                var closestDiv = $('#field-form-fraudlabs_ApiKey').closest('div');
                var linkToSugnUp = '<label>Please sign up for an API key at <a href="http://www.fraudlabspro.com/pricing?utm_source=module&utm_medium=banner&utm_term=pinnaclecart&utm_campaign=module%20banner">FraudLabsPro.com</a></label>';
                closestDiv.append(linkToSugnUp);
            }


            $('#modal-update-custom-fraud').on('show.bs.modal', function() {
                $(this).find('.has-error').removeClass('has-error');
            }).on('shown.bs.modal', function() {
                $(this).find('input,select,textarea').get(0).focus();
            });

            $('.edit-fraud-method').click(function() {
                var theLink = $(this);
                editSettings(theLink.attr('data-id'), theLink.attr('data-method-type'));
                return false;
            });

            $('.disable-fraud-method').click(function() {
                var theLink = $(this);
                disableFraudMethod(theLink.attr('data-id'));
                return false;
            });

            $('.activate-fraud-method').click(function() {
                var theLink = $(this);
                var activeMethod = $(".disable-fraud-method");
                if (activeMethod.length > 0 && (theLink.attr('data-id') != activeMethod.attr('data-id')))
                {
                    var confirmationText = 'In order to enable ' + theLink.attr('data-method-caption') + ', we will need to disable ' + activeMethod.attr('data-method-caption') +'. Proceed?';
                    AdminForm.confirm(confirmationText, function()
                    {
                        activateFraudMethod(theLink.attr('data-id'));
                    });
                }
                else
                {
                    activateFraudMethod(theLink.attr('data-id'));
                }
                return false;
            });

            $('#modal-add-fraud').on('show.bs.modal', function() {
                $('#field-fraud_method_id').closest('.form-group').show();
                $('#fraud-method-form').html('');
                $('input[name=fromQS]').val($('#quick-start-wrapper').length > 0 ? '1' : '0');
            });

            $('form[name=form-add-fraud]').submit(function() {
                var fraudMethodId = $('#field-fraud_method_id').val();
                var errors = [];
                if (fraudMethodId == '') {
                    errors.push({field:'#field-fraud_method_id', 'message': trans.fraud_methods.select_fraud_method});
                    AdminForm.displayModalErrors($('#modal-add-fraud').modal(), errors);
                    return false;
                } else if (fraudMethodId == 'custom') {
                    var theModal = $(this).closest('.modal');
                    var title = $.trim($(this).find('.field-title').val());

                    if (title == '') {
                        errors.push({
                            field: '.field-title',
                            message: trans.fraud_methods.title_required
                        });
                        AdminForm.displayModalErrors(theModal, errors);
                    } else {
                        AdminForm.sendRequest(
                            'admin.php?p=fraud_method&action=update-custom',
                            $(this).serialize(),
                            'Please wait',
                            function(data) {
                                if (data.status) {
                                    document.location = 'admin.php?p=fraud_methods';
                                } else {
                                    var errors = [];
                                    $.each(data.errors, function(key, value) {
                                        $.each(value, function(idx, message) {
                                            errors.push({
                                                field: '.field-'+key,
                                                message: message
                                            });
                                        });
                                    });

                                    AdminForm.displayModalErrors(theModal, errors);
                                }
                            }
                        );
                    }
                    return false;
                }
                return true;
            });
        });
    };

    editSettings = function(fraudMethodId, type) {
        if (type == 'custom') {
            AdminForm.sendRequest(
                'admin.php?p=fraud_method&action=update-custom&fraud_method_id=' + fraudMethodId,
                null, null,
                function(data) {
                    if (data.status == 1) {
                        var theModal = $('#modal-update-custom-fraud');
                        if ($.trim(data.html) == '') {
                            theModal.modal('hide');
                        } else {
                            theModal.find('input[name=fraud_method_id]').val(fraudMethodId);
                            theModal.find('.modal-body').html(data.html);
                            theModal.modal('show');
                        }
                    }
                },
                null, 'GET'
            );
        } else {
            document.location = 'admin.php?p=fraud_method&fraud_method_id=' + fraudMethodId +  ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : '');
        }

        return false;
    };

    activateFraudMethod = function(fraudMethodId) {
        var fraudMethodIdEle = $('#field-fraud_method_id');
        fraudMethodIdEle.val(fraudMethodId).change();
        $('#modal-add-fraud').modal('show');

        fraudMethodIdEle.closest('.form-group').hide();

        return false;
    };

    disableFraudMethod = function(fraudMethodId) {
        AdminForm.confirm(trans.fraud_methods.confirm_disable_method, function() {
            AdminForm.sendRequest(
                'admin.php?p=fraud_method&action=disable&fraud_method_id=' + fraudMethodId,
                {},
                'Please wait',
                function() {
                    document.location = 'admin.php?p=fraud_methods' + ($('#quick-start-wrapper').length > 0 ? '&qsg_hide=1' : '');
                }
            );
        });

        return false;
    };

    init();

    return {
        editSettings: editSettings,
        disableFraudMethod: disableFraudMethod
    };
}(jQuery));
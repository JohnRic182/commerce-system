var ProductsFeatures = (function($) {
	var init, renderProductFeaturesTable, handleProductFeatureEdit, getProductFeatureValues;

	init = function() {
		$(document).ready(function() {

			// init values
			if (typeof productProductsFeaturesGroupsAvailable == 'undefined' || productProductsFeaturesGroupsAvailable === null) productProductsFeaturesGroupsAvailable = [];
			if (typeof productProductsFeaturesGroupsAssigned == 'undefined' || productProductsFeaturesGroupsAssigned === null) productProductsFeaturesGroupsAssigned = [];

			// set select element options
			if (productProductsFeaturesGroupsAvailable.length > 0) {
				var assigned = [];
				if (productProductsFeaturesGroupsAssigned.length > 0) {
					$.each(productProductsFeaturesGroupsAssigned, function (index, feature) {
						assigned.push(feature.product_feature_group_id);
					});
				}

				var options = '';
				$.each(productProductsFeaturesGroupsAvailable, function(index, feature) {
					var selected = $.inArray(feature.product_feature_group_id, assigned) != -1;
					options +=

						'<option value="' + feature.product_feature_group_id + '" ' + (selected ? ' selected' : '') + '>' +
						feature.feature_group_name + ' (' + feature.feature_group_id + ')' +
						'</option>';
				});

				$('#select-product-products-features-group-features').html(options);
				$('#select-product-products-features-group-features').DualListBox({json: false}, []);
				$('#modal-assign-feature-groups span.unselected-title').text(trans.features_groups.available_features_groups);
				$('#modal-assign-feature-groups span.selected-title').text(trans.features_groups.assign_features_groups);
			}

			// assign button action
			$('a[data-action="action-product-feature-group-assign"]').click(function() {
				if (productProductsFeaturesGroupsAvailable.length > 0) {
					$('#modal-assign-feature-groups').modal('show');
				} else {
					AdminForm.displayAlert('In order to assign products features groups, please add them first!');
				}
				return false;
			});

			// save assigned groups
			$('#modal-assign-feature-groups').find('form').submit(function() {
				var theForm = $(this)
				var selected = '';

				theForm.find('select.selected option').each(function(index, el) {
					selected += (selected == '' ? '' : ',') + $(el).val();
				});

				AdminForm.sendRequest(
					'admin.php?p=product_feature&mode=product-feature-group-assign',
					{
						pid: $('#field-pid_products_features_groups').val(),
						products_features_groups: selected
					},
					'Adding products features groups to the current product',
					function(data) {
						if (data.status) {
							productProductsFeaturesGroupsAssigned = data.productProductsFeaturesGroupsAssigned;
							renderProductFeaturesTable(productProductsFeaturesGroupsAssigned);

							$('#modal-assign-feature-groups').modal('hide');
							AdminForm.displayAlert(trans.common.success);
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({field: key, message: message});
								});
							});

							AdminForm.displayModalErrors($('#modal-add-products-features-groups').modal(), errors);
						}
					}
				);
				return false;
			});

			// handle table actions
			$('.table-product-features')
				.on('click', 'a[data-action="action-product-feature-group-edit"]', function() {
					handleProductFeatureEdit($(this));
					return false;
				})
				.on('click', 'a[data-action="action-product-feature-group-delete"]', function() {
					var groupIndex = $(this).attr('data-target');

					if (typeof productProductsFeaturesGroupsAssigned[groupIndex] != undefined) {
						var group = productProductsFeaturesGroupsAssigned[groupIndex];

						AdminForm.confirm(trans.products_features_groups.confirm_remove, function() {
							AdminForm.sendRequest(
								'admin.php?p=product_feature&mode=product-feature-group-delete',
								{
									'pid': group.pid,
									'product_feature_group_id': group.product_feature_group_id
								},
								'Deleting products features group',
								function(data) {
									if (data.status) {
										productProductsFeaturesGroupsAssigned = data.productProductsFeaturesGroupsAssigned;
										renderProductFeaturesTable(productProductsFeaturesGroupsAssigned);

										AdminForm.displayAlert(trans.common.success);
									} else {
										var errors = [];
										$.each(data.errors, function(key, value) {
											$.each(value, function(idx, message) {
												errors.push({field: '.field-group-features-' + key, message: message});
											});
										});
										AdminForm.displayErrors(errors);
									}
								}
							);
						});
					}
				});

			renderProductFeaturesTable(productProductsFeaturesGroupsAssigned);
		});
	};

	/**
	 * Render table
	 *
	 * @param productProductsFeaturesGroupsAvailable
	 */
	renderProductFeaturesTable = function(productProductsFeaturesGroupsAssigned) {
		var groupsContainer = $('.products-features-container');
		var assignFeatures = [];
		var deletedFeatures = [];

		if (productProductsFeaturesGroupsAssigned.length == 0) {
			$(groupsContainer).hide();
		} else {

			var html = '';

			$(productProductsFeaturesGroupsAssigned).each(function(groupIndex, group){

				html +=
					'<div class="clearfix" id="products_features_group_' + group.product_feature_group_id + '"><h3>' + htmlentities(group.feature_group_name) + '</h3>';

				var options = (typeof group.options !== undefined && group.options && group.options.length > 0) ? group.options : null;

				if (typeof group.features !== undefined && group.features && group.features.length > 0) {
					$.each(group.features, function (featureIndex, feature) {
						var optionsHtml = '';
						var dataListId = 'products_features_options_' + group.product_feature_group_id + '_' + feature.product_feature_id;
						if (options) {
							$.each(options, function (optionIndex, option) {
								if (option.product_feature_group_id == group.product_feature_group_id && option.product_feature_id == feature.product_feature_id) {
									optionsHtml += '<option value="' + option.product_feature_option_value.replace('"', '&quot;') + '">';
								}
							});
						}

						var currentInput = $('#products_features_group_' + group.product_feature_group_id + ' input[name="product_feature[' + group.product_feature_group_id + '][' + feature.product_feature_id + ']"]');
						if (currentInput.length > 0) {
							var optionValue = $(currentInput).val();
						} else {
							var optionValue = $.trim(feature.product_feature_option_value);
						}

						html +=
							'<div class="form-group field-text col-sm-6 col-xs-12">' +
								'<label>' + htmlentities(feature.feature_name) + '</label>' +
								'<input class="form-control input-product-feature-option" type="text" autocomplete="off" ' + (optionsHtml == '' ? '' : ('list="' + dataListId + '" ')) + 'name="product_feature[' + group.product_feature_group_id + '][' + feature.product_feature_id + ']" value="' + optionValue.replace('"', '&quot;') + '">' +
								(optionsHtml == '' ? '' : ('<datalist id="' + dataListId + '">' + optionsHtml + '</datalist>')) +
							'</div>';
					});
				} else {
					html += '<div class="col-xs-12">This products features group does not have features assigned yet</div>';
				}

				html += '</div>';

				assignFeatures.push(group.product_feature_group_id);
			});

			$(groupsContainer).html(html);
			$(groupsContainer).show();
		}

		// update dual box as well
		$('#modal-assign-feature-groups select.selected option').each(function(index, el) {
			var dropDownValue = $(el).val();
			var dropDownText = $(el).text();

			// returns -1 if the item is not found
			if (assignFeatures.indexOf(dropDownValue) < 0) {
				deletedFeatures.push({value: dropDownValue, text: dropDownText});
				$('#modal-assign-feature-groups select.unselected').append($('<option>', {value: dropDownValue, text: dropDownText}));
			}
		});

		$.each(deletedFeatures, function(index, el) {
			$('#modal-assign-feature-groups select.selected option[value="' + el.value + '"]').remove();
		});
	};


	init();

	return {
		renderProductFeaturesTable : renderProductFeaturesTable
	}

}(jQuery));

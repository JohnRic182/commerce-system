var Endicia = (function($) {
	var init, handleChangePassphraseSubmit, handleRequestRefundSubmit, handleAddFundsSubmit;

	init = function() {
		$(document).ready(function() {
			$('form[name=form-change-passphrase]').submit(function() {
				return handleChangePassphraseSubmit($(this));
			});
			$('form[name=form-request-refund]').submit(function() {
				return handleRequestRefundSubmit($(this));
			});
			$('form[name=form-add-funds]').submit(function() {
				return handleAddFundsSubmit($(this));
			});

			$('#modal-change-passphrase').on('hidden.bs.modal', function () {
				$('#modal-change-passphrase').find('.form-group .error').remove();
			});

			$('#modal-request-refund').on('hidden.bs.modal', function () {
				$('#modal-request-refund').find('.form-group .error').remove();
			});

			$('#modal-add-funds').on('hidden.bs.modal', function () {
				$('#modal-add-funds').find('.form-group .error').remove();
			});
		});
	};

	handleChangePassphraseSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var errors = [];

		if ($.trim(theForm.find('input[name=change_existing_passphrase]').val()) == '') {
			errors.push({
				field: 'input[name=change_existing_passphrase]',
				message: trans.endicia.current_passphrase_required
			});
		}

		var newPassphrase = $.trim(theForm.find('input[name=change_new_passphrase]').val());
		var newPassphrase2 = $.trim(theForm.find('input[name=change_new_passphrase2]').val());
		if (newPassphrase == '') {
			errors.push({
				field: 'input[name=change_new_passphrase]',
				message: trans.endicia.new_passphrase_required
			});
		}

		if (newPassphrase2 == '') {
			errors.push({
				field: 'input[name=change_new_passphrase2]',
				message: trans.endicia.confirm_new_passphrase_required
			});
		} else if (newPassphrase != '' && newPassphrase != newPassphrase2) {
			errors.push({
				field: 'input[name=change_new_passphrase2]',
				message: trans.endicia.confirm_new_passphrase_not_match
			});
		}

		if (errors.length > 0) {
			AdminForm.displayValidationErrors(errors);
		} else {
			AdminForm.sendRequest(
				'admin.php?p=endicia&mode=change-password',
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=app&key=endicia';
					} else {
						if (data.message !== undefined) {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						} else if (data.errors !== undefined) {
							AdminForm.displayModalErrors(theModal, data.errors);
						}
					}
				}
			);
		}

		return false;
	};

	handleRequestRefundSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var errors = [];

		if ($.trim(theForm.find('input[name=pic_number]').val()) == '') {
			errors.push({
				field: 'input[name=pic_number]',
				message: trans.endicia.pic_number_required
			});
		}

		if (errors.length > 0) {
			AdminForm.displayValidationErrors(errors);
		} else {
			AdminForm.sendRequest(
				'admin.php?p=endicia&mode=request-refund',
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=app&key=endicia';
					} else {
						if (data.message !== undefined) {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						} else if (data.errors !== undefined) {
							AdminForm.displayModalErrors(theModal, data.errors);
						}
					}
				}
			);
		}

		return false;
	};

	handleAddFundsSubmit = function(theForm) {
		var theModal = theForm.closest('.modal');
		var errors = [];

		if ($.trim(theForm.find('input[name=addfunds_passphrase]').val()) == '') {
			errors.push({
				field: 'input[name=addfunds_passphrase]',
				message: trans.endicia.addfunds_passphrase_required
			});
		}

		if ($.trim(theForm.find('input[name=addfunds_amount]').val()) == '') {
			errors.push({
				field: 'input[name=addfunds_amount]',
				message: trans.endicia.addfunds_amount_required
			});
		}

		if (errors.length > 0) {
			AdminForm.displayValidationErrors(errors);
		} else {
			AdminForm.sendRequest(
				'admin.php?p=endicia&mode=add-funds',
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=app&key=endicia';
					} else {
						if (data.message !== undefined) {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						} else if (data.errors !== undefined) {
							AdminForm.displayModalErrors(theModal, data.errors);
						}
					}
				}
			);
		}

		return false;
	};

	init();
}(jQuery));
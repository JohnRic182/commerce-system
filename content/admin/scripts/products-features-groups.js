var ProductsFeaturesGroups = (function($) {
	var init, handleFormSubmit, handleDeleteSubmit;

	init = function() {
		$(document).ready(function() {
			$('form[name="form-feature-groups"]').submit(function(){
				return  handleFormSubmit();
			});

			$('[data-action="action-features-groups-delete"]').click(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var deleteMode = $('input[name="form-features-groups-delete-mode"]:checked').val();
				var nonce = $('#modal-features-groups-delete').find('input[name="nonce"]').val();

				if (deleteMode == 'selected') {
					var ids = '';

					$('.checkbox-group-feature:checked').each(function() {
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});

					if (ids != '') {
						window.location='admin.php?p=feature_groups&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
					}
					else {
						AdminForm.displayModalAlert(theModal, trans.features_groups.no_feature_group_selected, 'danger');
					}
				}
				else if (deleteMode == 'search') {
					window.location='admin.php?p=feature_groups&mode=delete&deleteMode=search&nonce=' + nonce;
				}
			});
		});
	};

	handleFormSubmit = function() {
		var errors = [];
		if ($.trim($('#field-feature_group_name').val()) == '') {
			errors.push({
				field: '#field-feature_group_name',
				message: trans.features_groups.enter_feature_group_name
			});
		}
		if ($.trim($('#field-feature_group_id').val()) == '') {

			errors.push({
				field: '#field-feature_group_id',
				message: trans.features_groups.enter_feature_group_id
			});
		}

		AdminForm.displayValidationErrors(errors);

		return errors.length == 0;
	};

	handleDeleteSubmit = function(id, nonce) {
		AdminForm.confirm(trans.features_groups.confirm_remove_feature_group, function() {
			window.location = 'admin.php?p=feature_groups&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});
	};

	init();

	return {
		removeFeatureGroup: handleDeleteSubmit
	}

} (jQuery));

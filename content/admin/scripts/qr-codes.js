var QRCodes = (function($) {
	var init, editQrCode, removeCampaign,
		handleQrCodeForm;

	init = function() {
		$(document).ready(function(){
			$('form[name="form-add-campaign"]').submit(function() {
				return handleQrCodeForm($(this), 'modal-add-campaign');
			});

			$('form[name="form-settings"]').submit(function() {
				var theForm = $(this);
				var theModal = $(this).closest('.modal');

				AdminForm.displaySpinner();

				AdminForm.sendRequest(
					theForm.attr('action'),
					theForm.serialize(),
					null,
					function(data) {
						if (data.status == 1) {
							AdminForm.hideSpinner();
							theModal.modal('hide');
							document.location = 'admin.php?p=settings_qr';
						} else {
							AdminForm.hideSpinner();

							if (data.errors !== undefined) {
								var errors = [];
								$.each(data.errors, function(idx, error) {
									errors.push({
										field: '.field-'+error.field,
										message: error.message
									})
								});
								AdminForm.displayModalErrors(theModal, errors);
							} else {
								AdminForm.displayModalAlert(theModal, data.message, 'danger');
							}
						}
					},
					function() {
						AdminForm.hideSpinner();
					}
				);

				return false;
			});


			/**
			 * Handle delete button
			 */
			$('[data-action="action-campaigns-delete"]').click(function(){
				var deleteMode = $('input[name="form-campaigns-delete-mode"]:checked').val();
				if (deleteMode == 'selected')
				{
					var ids = '';
					$('.checkbox-campaign:checked').each(function(){
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});
					if (ids != '')
					{
						window.location='admin.php?p=settings_qr&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
					}
					else
					{
						alert(trans.qr_codes.select_campaign);
					}
				}
				else if (deleteMode == 'search')
				{
					window.location='admin.php?p=settings_qr&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
				}
			});


			$("#qr-generate-button").click(function(){
				var theModal = $(this).closest('.modal');
				AdminForm.displaySpinner();

				var error_check = 'L';
				var default_size = '4';
				var media = theModal.find('select[name="qrSettings\\[qr_media\\]"]').val();
				switch (media)
				{
					case '1': error_check = 'L'; default_size = '4'; break;
					case '2': error_check = 'L'; default_size = '8'; break;
					case '3': error_check = 'M'; default_size = '8'; break;
					case '4': error_check = 'M'; default_size = '12'; break;
					case '5': error_check = 'Q'; default_size = '4'; break;
					case '6': error_check = 'H'; default_size = '4'; break;
				}

				AdminForm.sendRequest(
					'admin.php?p=marketing_qr_ajax&chart=true',
					{
						action: 'generate_custom',
						image_type: theModal.find('select[name="qrSettings\\[qr_image_type\\]"]').val(),
						text: theModal.find('input[name="qrSettings\\[GlobalHttpUrl\\]"]').val(),
						error_check: error_check,
						default_size: default_size
					},
					'Please wait',
					function(data) {
						AdminForm.hideSpinner();
						$("#modal-generated-image").data('path', data.path);
						$("#dialog-qr-generated-image").attr("src", data.url);
						$('#dialog-qr-generated-image').addClass('show-dialog');
					},
					function(data) {
						AdminForm.hideSpinner();
						alert('Error generating image.');
					}
				);

				return false;
			});

			$("#dialog-qr-generated-image").load(function(){
				var generatedImage = $(this);
				if (generatedImage.hasClass('show-dialog'))
				{
					$("#modal-generated-image").modal('show');
				}
				generatedImage.addClass('show-dialog');
			});

			$('[data-action="action-download-image"]').click(function(){
				var src = 'admin.php?p=marketing_qr_ajax&chart=true&action=download_qr&path=' + $("#modal-generated-image").data('path');
				$('#iframe-download').attr('src', src);
				document.location = src;
			})
		});
	};

	handleQrCodeForm = function(theForm, modal_name) {
		var theModal = $("#"+modal_name);
		var errors = [];

		AdminForm.displaySpinner();

		AdminForm.sendRequest(
			theForm.attr('action'),
			theForm.serialize(),
			null,
			function(data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();
					theModal.modal('hide');
					document.location = 'admin.php?p=settings_qr';
				} else {
					AdminForm.hideSpinner();
					if (data.errors !== undefined) {
						$.each(data.errors, function(errKey, errMsg){
							errors.push({
								field: '.field-'+errKey,
								message: errMsg
							});
						});
						AdminForm.displayModalErrors(theModal, errors);
					} else {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			},
			function() {
				AdminForm.hideSpinner();
			}
		);

		return false;
	};

	editQrCode = function(id) {
		AdminForm.sendRequest(
			'admin.php?p=settings_qr&mode=update&id='+id,
			null,
			null,
			function(data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();

					$('#qr-code-edit-wrapper').html(data.html);

					var theModal = $('#modal-edit-campaign');
					$(theModal.find('fieldset').get(0)).addClass('fieldset-primary');

					$('form[name="form-edit-campaign"]').submit(function() {
						return handleQrCodeForm($(this), 'modal-edit-campaign');
					});

					theModal.modal('show');
				}
			},
			null,
			'GET'
		);

		return false;
	};

	removeCampaign = function(id, nonce) {
		AdminForm.confirm(trans.qr_codes.confirm_remove_campaign, function() {
			window.location = 'admin.php?p=settings_qr&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	init();

	return {
		editQrCode: editQrCode,
		removeCampaign: removeCampaign
	};
}(jQuery));






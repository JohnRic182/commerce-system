(function($) {
	var init, handleToggleEnableRecurringBilling;

	init = function() {
		$(document).ready(function() {
			$('#field-recurring_billing_data-trial_enabled')
				.change(function() {
					$('.recurring-billing-data-trial-wrapper').toggle($(this).is(':checked'));
				})
				.change();

			var enableRecurringEle = $('#field-enable_recurring_billing');
			if (enableRecurringEle.length > 0) {
				enableRecurringEle.change(function() {
					handleToggleEnableRecurringBilling();
				});
				handleToggleEnableRecurringBilling();
			}
		});
	};

	handleToggleEnableRecurringBilling = function() {
		var enableRecurringEle = $('#field-enable_recurring_billing');

		var isEnabled = enableRecurringEle.is(':checked');

		$('#group-recurring').find('.form-group').toggle(isEnabled);
		enableRecurringEle.closest('.form-group').show();

		var productType = $('#field-product_type').val();
		$('#radio_field-recurring_billing_data-auto_complete_options-_auto_complete_initial_order').closest('.form-group').toggle(productType != 'Tangible');

		/**
		 * Make recurring fields required when needed
		 */
		if (isEnabled) {
			$('select[data-recurring-required="1"],input[data-recurring-required="1"]').closest('.form-group').addClass('required');
			if ($('input[name="recurring_billing_data[trial][period_unit]"]:checked').val() != '') {
				$('select[data-recurring-trial-required="1"],input[data-recurring-trial-required="1"]').closest('.form-group').addClass('required');
			}
			$('.recurring-billing-data-trial-wrapper').toggle($('#field-recurring_billing_data-trial_enabled').is(':checked'));
		} else {
			$('select[data-recurring-required="1"],input[data-recurring-required="1"]').closest('.form-group').removeClass('required');
			$('select[data-recurring-trial-required="1"],input[data-recurring-trial-required="1"]').closest('.form-group').removeClass('required');
		}
	};

	init();
}(jQuery));

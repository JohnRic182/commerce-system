var Testimonials = (function($){
	var toggleTestimonialsSettings, handleSettingsFormSubmit, handleUpdateStatusesSubmit;

	toggleTestimonialsSettings = function() {
		var isEnabled = $('#field-testimonialsSettings-testimonials_Enabled').is(':checked');

		if (isEnabled == true) {
			$('#field-testimonialsSettings-testimonials_Enabled').attr('checked', 'checked');
		} else {
			$('#field-testimonialsSettings-testimonials_Enabled').removeAttr('checked');
		}

		$('#field-testimonialsSettings-testimonials_Email_Notify').closest('.form-group').toggle(isEnabled);
		$('#field-testimonialsSettings-testimonials_Per_Page').closest('.form-group').toggle(isEnabled);
		$('#field-testimonialsSettings-testimonials_Use_Rating').closest('.form-group').toggle(isEnabled);
		$('#field-testimonialsSettings-testimonials_Use_Captcha').closest('.form-group').toggle(isEnabled);
	};

	$(document).ready(function() {
		$('form[name="form-settings"]').submit(function() {
			return handleSettingsFormSubmit($(this));
		});

		$('#field-testimonialsSettings-testimonials_Per_Page').keypress(function(e) {
			var evt = (e) ? e : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;

			if (charCode > 31 && (charCode < 48 || charCode > 57)) {
				return false;
			}
			return true;
		});

		$('#field-testimonialsSettings-testimonials_Enabled').change(function() {
			toggleTestimonialsSettings();
		});
		toggleTestimonialsSettings();

		$('form[name=form-testimonial]').submit(function() {
			var errors = [];
			if ($.trim($('#field-name').val()) == '') {
				errors.push({
					field: '#field-name',
					message: trans.testimonials.name_required
				});
			}

			if ($.trim($('#field-testimonial').val()) == '') {
				errors.push({
					field: '#field-testimonial',
					message: trans.testimonials.testimonial_required
				});
			}

			AdminForm.displayValidationErrors(errors);
			return errors.length == 0;
		});

		$('form[name=form-testimonials-update-statuses]').submit(function() {
			return handleUpdateStatusesSubmit($(this));
		});

		/**
		 * Handle delete button
		 */
		$('[data-action="action-testimonials-delete"]').click(function(){

			var deleteMode = $('input[name="form-testimonials-delete-mode"]:checked').val();

			if (deleteMode == 'selected')
			{
				var ids = '';
				$('.checkbox-testimonial:checked').each(function(){
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				if (ids != '')
				{
					window.location='admin.php?p=testimonial&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
				}
				else
				{
					AdminForm.displayModalAlert($('#modal-testimonials-delete'), trans.testimonials.no_testimonials_selected, 'danger');
				}
			}
			else if (deleteMode == 'search')
			{
				window.location='admin.php?p=testimonial&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
			}
		});

	});

	handleUpdateStatusesSubmit = function(theForm) {
		var theModal = $(theForm).closest('.modal');

		var updateMode = $('input[name="form-testimonials-update-statuses-mode"]:checked').val();
		var new_status = $('select[name="form-testimonials-update-statuses-new-status"]').val();

		if (updateMode == 'selected')
		{
			var ids = '';
			$('.checkbox-testimonial:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=testimonial&mode=update_testimonial_status&new_status='+new_status+'&updateMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
			}
			else
			{
				AdminForm.displayModalAlert(theModal, trans.testimonials.no_testimonials_selected, 'danger');
			}
		}
		else if (updateMode == 'search')
		{
			window.location='admin.php?p=testimonial&mode=update_testimonial_status&new_status='+new_status+'&updateMode=search' + '&nonce=' + $(this).attr('nonce');
		}

		return false;
	};

	handleSettingsFormSubmit = function(theForm) {
		var theModal = $(theForm).closest('.modal');

		AdminForm.displaySpinner();

		AdminForm.sendRequest(
			theForm.attr('action'),
			theForm.serialize(),
			null,
			function(data) {
				if (data.status == 1) {
					document.location = 'admin.php?p=testimonials';
				} else {
					AdminForm.hideSpinner();

					if (data.errors !== undefined) {
						AdminForm.removeErrors();
						AdminForm.displayValidationErrors(data.errors);
					} else {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			},
			function() {
				AdminForm.hideSpinner();
			}
		);

		return false;
	};
}(jQuery));

function removeTestimonial(id, nonce) {
	AdminForm.confirm(trans.testimonials.confirm_remove_testimonial, function() {
		window.location = 'admin.php?p=testimonial&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
	});

	return false;
}

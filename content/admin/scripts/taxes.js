var Taxes = (function($) {
	var init, handleTaxClassForm, editTaxClass, removeTaxRate, removeTaxClass;

	init = function() {
		$(document).on('submit', 'form[name="form-add-tax-class"],form[name="form-edit-tax-class"]', function() {
			var theModal = $(this).closest('.modal');
			var theForm = $(this);

			var errors = [];
			if ($.trim(theForm.find('.field-class_name').val()) == '') {
				errors.push({
					field: '.field-class_name',
					message: trans.taxes.enter_title
				});
			}

			if ($.trim(theForm.find('.field-key_name').val()) == '') {
				errors.push({
					field: '.field-key_name',
					message: trans.taxes.enter_key_id
				});
			}
			if (errors.length > 0) {
				AdminForm.displayValidationErrors(errors);
				return  false;
			}

			return handleTaxClassForm($(this));
		});
		
		$('a[data-target="#modal-settings"]').click(function() {
			var theModal = $('#modal-settings');
			forms.initAddressForm(theModal.find('form'));
		});

		$('form[name="form-settings"]').submit(function() {
			var theForm = $(this);
			var theModal = $(this).closest('.modal');

			AdminForm.sendRequest(
				theForm.attr('action'),
				theForm.serialize(),
				null,
				function(data) {
					if (data.status == 1) {
						AdminForm.hideSpinner();
						theModal.modal('hide');
						AdminForm.displayAlert(data.message);
						document.location = 'admin.php?p=taxes';
					} else {
						AdminForm.hideSpinner();

						if (data.errors !== undefined) {
							AdminForm.displayModalErrors(theModal, data.errors);
						} else {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						}
					}
				},
				function() {
					AdminForm.hideSpinner();
				}
			);

			return false;
		});

		$('form[name=form-taxes_rate]').submit(function() {
			var errors = [];
			if ($.trim($('#field-rate_description').val()) == '') {
				errors.push({
					field: '#field-rate_description',
					message: trans.taxes.enter_title
				});
			}

			var taxRateEle = $('#field-tax_rate');
			var f_val = parseFloat($.trim(taxRateEle.val()));

			if ($.trim(taxRateEle.val()) == '') {
				errors.push({
					field: '#field-tax_rate',
					message: trans.taxes.enter_tax_rate
				});
			}
			else if (f_val <= 0) {
				errors.push({
					field: '#field-tax_rate',
					message: trans.taxes.message_tax_rate_greater
				});
			}
			AdminForm.displayValidationErrors(errors);
			return errors.length == 0;
		});

		/**
		 * Handle delete button
		 */
		$('[data-action="action-tax_rate-delete"]').click(function(){

			var deleteMode = $('input[name="form-tax_rate-delete-mode"]:checked').val();

			if (deleteMode == 'selected')
			{
				var ids = '';
				$('.checkbox-tax_rate:checked').each(function(){
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				if (ids != '')
				{
					window.location='admin.php?p=taxes_rate&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
				}
				else
				{
					alert(trans.taxes.select_delete_tax_rate);
				}
			}
			else if (deleteMode == 'all')
			{
				window.location='admin.php?p=taxes_rate&mode=delete&deleteMode=all' + '&nonce=' + $(this).attr('nonce');
			}
		});


		$('[data-action="action-taxclasses-delete"]').click(function() {
			var deleteMode = $('input[name="form-taxclasses-delete-mode"]:checked').val();

			if (deleteMode == 'selected')
			{
				var ids = '';
				$('.checkbox-taxclasses:checked').each(function(){
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				if (ids != '')
				{
					window.location='admin.php?p=taxes_class&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
				}
				else
				{
					alert(trans.taxes.select_delete_tax_class);
				}
			}
			else if (deleteMode == 'all')
			{
				window.location='admin.php?p=taxes_class&mode=delete&deleteMode=all' + '&nonce=' + $(this).attr('nonce');
			}
		});
	};

	handleTaxClassForm = function(theForm) {
		var theModal = $(theForm).closest('.modal');

		AdminForm.sendRequest(
			theForm.attr('action'),
			theForm.serialize(),
			null,
			function(data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();
					theModal.modal('hide');
					document.location = 'admin.php?p=taxes_classes';
				} else {
					AdminForm.hideSpinner();

					if (data.errors !== undefined) {
						var errors = [];
						$.each(data.errors, function(idx, error) {
							errors.push({
								field: '.field-'+error.field,
								message: error.message
							});
						});
						AdminForm.displayModalErrors(theModal, errors);
					} else {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			},
			function() {
				AdminForm.hideSpinner();
			}
		);

		return false;
	};

	editTaxClass = function(id) {
		AdminForm.sendRequest(
			'admin.php?p=taxes_class&mode=update&id='+id,
			null,
			null,
			function(data) {
				if (data.status == 1) {
					AdminForm.hideSpinner();

					$('#tax-class-edit-wrapper').html(data.html);

					var theModal = $('#modal-edit-tax-class');
					$(theModal.find('fieldset').get(0)).addClass('fieldset-primary');

					theModal.modal('show');
				}
			},
			null,
			'GET'
		);

		return false;
	};

	removeTaxRate = function(id, nonce) {
		AdminForm.confirm(trans.taxes.confirm_remove_tax_rate, function() {
			window.location = 'admin.php?p=taxes_rate&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};


	removeTaxClass = function(id, nonce) {
		AdminForm.confirm(trans.taxes.confirm_tax_class, function() {
			window.location = 'admin.php?p=taxes_class&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	init();

	return {
		editTaxClass: editTaxClass,
		removeTaxRate: removeTaxRate,
		removeTaxClass: removeTaxClass
	};
}(jQuery));


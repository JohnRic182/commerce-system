$(document).ready(function() {

	$('#form-security-profile').submit(function(e) {
		var errors = [];

		if ($('#field-password').length > 0) {

			if ($.trim($('#field-password').val()) == '') {
				errors.push({
					field: '#field-password',
					message: trans.security_profile.enter_passwordsecurity-form
				});
			}

			if ($.trim($('#field-password2').val()) == '') {
				errors.push({
					field: '#field-password2',
					message: trans.security_profile.enter_password_confirmation
				});
			}

			if (($.trim($('#field-password2').val()) !== '') && ($.trim($('#field-password').val()) !== '')) {
				if ($.trim($('#field-password2').val()) !== $.trim($('#field-password').val())) {
					errors.push({
						field: '#field-password2',
						message: trans.security_profile.message_passwords_not_equal
					});
				}
			}
		}

		if ($('#field-security_question_id').length > 0) {

			if ($.trim($('#field-security_question_id').val()) == '') {
				errors.push({
					field: '#field-security_question_id',
					message: trans.security_profile.select_security_question
				});
			}

			if ($.trim($('#field-security_question_answer').val()) == '') {
				errors.push({
					field: '#field-security_question_answer',
					message: trans.security_profile.enter_answer_security_question
				});
			}
		}

		if (errors.length > 0) {
			AdminForm.displayModalErrors($("#modal-security-profile").modal(), errors);
		} else {
			$.post(
				'admin.php',
				$('#form-security-profile').serialize(),
				function (data) {
					if (data.status == 1) {
						$('#modal-security-profile').modal('hide');

						if (typeof qsgAutostart != 'undefined' && qsgAutostart) {
							if ($('.pi-quick-start-guide-mobile').is(':visible')) {
								$('.pi-quick-start-guide-mobile').trigger('click');
							} else {
								$('.pi-quick-start-guide-desktop').trigger('click');
							}

							$(window).on('load', function() {
								$('.quick-start-guide-modal').modal('show');
							});
						} else {
							window.location.reload();
						}
					}
					else {
						jQuery.each(data.errors, function (errKey, errMsg) {
							errors.push({
								field: errKey,
								message: errMsg
							});
						});

						AdminForm.displayModalErrors($("#modal-security-profile").modal(), errors);
					}
				}
			);
		}

		return false;
	})
	.on('keypress', 'input[type=text],input[type=password]', function(e) {
		if (e.keyCode == '13') {
			$('#form-security-profile').submit();
			return false;
		}
	});

	$("#modal-security-profile").modal('show');
});
$(document).ready(function(){
	$('.form-custom-forms-delete').submit(function(){
		return false;
	});

	$('form[name=form-custom-form]').submit(function() {
		var errors = [];
		if ($.trim($('#field-title').val()) == '') {
			errors.push({
				field: '#field-title',
				message: trans.custom_form.enter_title
			});
		}
		if ($.trim($('#field-form_id').val()) == '') {
			errors.push({
				field: '#field-form_id',
				message: trans.custom_form.enter_form_id
			});
		}

		AdminForm.displayErrors(errors);

		return errors.length == 0;
	});

	/**
	 * Handle data view
	 */
	$('[data-action="action-custom-form-post-view"]').click(function(e) {
		e.stopPropagation();

		var postId = $(this).attr('data-form-post-id');
		var title = [];

		$('.table-custom-form-data thead th.data-title').each(function(index, cell){
			console.log($(cell).text());
			title[index] = $(cell).text();
		});

		var html = '<table class="table table-bordered">';

		$('#custom-form-post-id-' + postId + ' .data-cell').each(function(index, cell){
			var t = $.trim($(cell).text());
			if (t.length > 50) {
				html += '<tr><td class="col-sm-4">' + title[index] + '</td><td class="col-sm-8"><textarea class="custom-form-data-view-cell" readonly>' + t + '</textarea></td></tr>';
			} else {
				html += '<tr><td class="col-sm-4">' + title[index] + '</td><td class="col-sm-8">' + t + '</td></tr>';
			}



		});

		html += '</table>';

		console.log(html);


		$('#modal-custom-form-view-data .modal-body').html(html);

		$('#modal-custom-form-view-data').modal('show');

		return false;
	});


	/**
	 * Handle delete button next to record
	 */
	$('[data-action="action-custom-form-post-delete"]').click(function(e) {
		e.stopPropagation();
		var formId = $(this).attr('data-form-id');
		var postId = $(this).attr('data-form-post-id');
		var deleteNonce = $(this).attr('data-delete-nonce');
		AdminForm.confirm(trans.custom_form.confirm_delete_custom_form_post, function() {
			window.location = 'admin.php?p=custom_form_data&mode=delete&deleteMode=single&form_id=' + formId + '&post_id=' + postId + '&nonce=' + deleteNonce;
		});
		return false;
	});

	/**
	 * Bulk delete
	 */
	$('form[name="form-custom-form-data-export"]').submit(function() {
		var theForm = $(this);
		var theModal = theForm.closest('.modal');
		var exportMode = $('input[name="form-custom-form-data-export-mode"]:checked').val();
		var formId = theForm.find('input[name="form_id"]').val();

		if (exportMode == 'selected') {
			var postIds = '';
			$('.checkbox-custom-form-post:checked').each(function() {
				var id = $(this).val();
				postIds += (postIds == '' ? '' : ',') + id;
			});
			if (postIds != '') {
				window.location='admin.php?p=custom_form_data&mode=export&exportMode=selected&form_id=' + formId + '&post_ids=' + postIds;
			} else {
				AdminForm.clearAlerts();
				AdminForm.displayModalAlert(theModal, trans.custom_form.no_records_selected_export, 'danger');
			}
		}
		else if (exportMode == 'search') {
			window.location='admin.php?p=custom_form_data&mode=export&exportMode=search&form_id=' + formId;;
		}

		return false;
	});

	/**
	 * Bulk delete
	 */
	$('form[name="form-custom-form-data-delete"]').submit(function() {
		var theForm = $(this);
		var theModal = theForm.closest('.modal');
		var deleteMode = $('input[name="form-custom-form-data-delete-mode"]:checked').val();
		var formId = theForm.find('input[name="form_id"]').val();
		var nonce = theForm.find('input[name="nonce"]').val();

		if (deleteMode == 'selected') {
			var postIds = '';
			$('.checkbox-custom-form-post:checked').each(function() {
				var id = $(this).val();
				postIds += (postIds == '' ? '' : ',') + id;
			});
			if (postIds != '') {
				window.location='admin.php?p=custom_form_data&mode=delete&deleteMode=selected&form_id=' + formId + '&post_ids=' + postIds + '&nonce=' + nonce;
			} else {
				AdminForm.clearAlerts();
				AdminForm.displayModalAlert(theModal, trans.custom_form.no_records_selected_delete, 'danger');
			}
		}
		else if (deleteMode == 'search') {
			window.location='admin.php?p=custom_form_data&mode=delete&deleteMode=search&form_id=' + formId + '&nonce=' + nonce;
		}

		return false;
	});
});

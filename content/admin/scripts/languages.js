var Languages = (function($) {
	var init, removeLanguage;

	init = function() {
		$(document).ready(function(){
			$(".default-language-radio-button:checked").addClass('current-default');

			$('.default-language-radio-button')
				.click(function(event){
					var id = $(this).attr('id');
					var default_id = $(".current-default").attr('id');
					event.preventDefault();

					if ( default_id !== id ){
						$(this).triggerHandler('change');
					}
				})
				// set default language
				.change(function(){
					var language_id = $(this).val();
					AdminForm.confirm(trans.languages.set_language_default, function() {
						$.post(
							"admin.php?p=language",
							{
								mode: "set_default",
								id: language_id
							},
							onAjaxSuccess,
							"json"
						);

						$(".current-default").removeClass('current-default');
						$('#id-default-language-'+language_id)
							.prop("checked", true)
							.addClass('current-default');
					});
					return false;
				});

			// bulk delete languages
			$('[data-action="action-languages-delete"]').click(function() {
				var deleteMode = $('input[name="form-languages-delete-mode"]:checked').val();
				if (deleteMode == 'selected')
				{
					var ids = '';
					$('.checkbox-language:checked').each(function(){
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});
					if (ids != '') {
						window.location='admin.php?p=language&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
					} else {
						alert(trans.languages.select_language);
					}
				} else if (deleteMode == 'search') {
					window.location='admin.php?p=language&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
				}
			});

			// for the add language form, if existing language selected, clear & hide file upload input
			$('#field-template_lang').change(function() {
				if ($('#field-template_lang').find('option:selected').val() != '') {

					var control = $("#field-template_file");
					control.wrap('<form>').closest('form').get(0).reset();
					control.unwrap();

					control.parent().hide();
				} else {
					$("#field-template_file").parent().show();
				}
			});

		});
	};

	removeLanguage = function(id, nonce) {
		if ($('#id-default-language-'+id+":checked").length == 1) {
			var errors = [];
			errors.push({
				field: '#id-default-language-'+id,
				message: trans.languages.cannot_remove_language
			});
		}
		else{
			AdminForm.confirm(trans.languages.confirm_remove_language, function() {
				window.location = 'admin.php?p=language&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
			});
		}

		AdminForm.displayErrors(errors);
		return false;
	};

	onAjaxSuccess = function(data) {
		$("#active_val_" + data.lang_made_visible).html('Yes');
		$("#modal-confirm").modal('hide');
	};

	init();

	return {
		removeLanguage: removeLanguage
	};
}(jQuery));


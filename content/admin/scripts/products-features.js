var ProductsFeatures = (function($) {
	var init, handleFormSubmit, handleDeleteSubmit;

	init = function() {
		$(document).ready(function() {
			$('form[name="form-products-features"]').submit(function(){
				return handleFormSubmit();
			});

			$('[data-action="action-product-feature-group-add"]').click(function () {
				var id = $('input[name="id"]').val();
				window.location='admin.php?p=feature_groups&mode=add&product_feature_id=' + id;
			});

			$('[data-action="action-products-features-delete"]').click(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');
				var deleteMode = $('input[name="form-products-features-delete-mode"]:checked').val();
				var nonce = $('#modal-products-features-delete').find('input[name="nonce"]').val();

				if (deleteMode == 'selected') {
					var ids = '';
					$('.checkbox-product-feature:checked').each(function() {
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});

					if (ids != '') {
						window.location='admin.php?p=products_feature&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
					}
					else {
						AdminForm.displayModalAlert(theModal, trans.products_features.no_product_feature_selected, 'danger');
					}
				}
				else if (deleteMode == 'search') {
					window.location='admin.php?p=products_feature&mode=delete&deleteMode=search&nonce=' + nonce;
				}
			});
		});

	};

	handleFormSubmit = function() {
		var errors = [];
		if ($.trim($('#field-feature_name').val()) == '') {
			errors.push({
				field: '#field-feature_name',
				message: trans.products_features.enter_feature_name
			});
		}
		if ($.trim($('#field-feature_id').val()) == '') {
			errors.push({
				field: '#field-feature_id',
				message: trans.products_features.enter_feature_id
			});
		}

		AdminForm.displayValidationErrors(errors);

		return errors.length == 0;
	};

	handleDeleteSubmit = function(id, nonce) {
		AdminForm.confirm(trans.products_features.confirm_remove_product_feature, function() {
			window.location = 'admin.php?p=products_feature&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});
	};

	init();

	return {
		removeProductFeature: handleDeleteSubmit
	}

}(jQuery));
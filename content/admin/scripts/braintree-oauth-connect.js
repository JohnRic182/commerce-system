var BrainTreeAuth = (function($) {
    var init;

    init = function() {
        $(document).ready(function() {


            $("#bt-auth").click(function(e){
                var theModal = $(this).closest('.modal');
                e.preventDefault();
                var client_id = $("#field-braintree_clientid").val();
                var client_secret = $("#field-braintree_clientsecret").val();
                var  params = [];

                if (client_id && client_secret){
                    AdminForm.sendRequest(
                        'admin.php?p=braintree&mode=auth',
                        {
                            'clientid':client_id,
                            'clientsecret':client_secret
                        },
                        null,
                        function(data) {
                            console.log(data);
                            if (data.status == 1) {
                                var connectUrl = data.connect_url_from_server;
                                $("#client-settings").hide();
                                $("#bt-connect-btn").show();

                                var partner = new BraintreeOAuthConnect({
                                    connectUrl: connectUrl,
                                    container: 'modal-bt-activate',
                                    //environment: 'sandbox',
                                    onError: function (errorObject) {
                                        console.warn(errorObject.message);
                                    }
                                });
                            } else {
                                if (data.message !== undefined) {
                                    AdminForm.displayModalAlert(theModal, data.message, 'danger');
                                } else if (data.errors !== undefined) {
                                    AdminForm.displayModalErrors(theModal, data.errors);
                                }
                            }
                        }
                    );
                }
                else{
                    AdminForm.displayModalAlert(theModal, "Settings shouldn't been empty", 'danger');
                }
            });
        });
    }

    init();
}(jQuery));
var ProductFamily = (function($) {
	var init, updateAvailableProducts, updateAssignedProductsList, remove;

	init = function() {
		$(document).ready(function() {
			$('form[name=form-products-families-add]').submit(function() {
				var theForm = $(this);
				var theModal = theForm.closest('.modal');

				var errors = [];
				if ($.trim(theForm.find('.field-name').val()) == '') {
					errors.push({
						field: '.field-name',
						message: trans.products_family.enter_title
					});
				}

				if (errors.length > 0) {
					AdminForm.displayValidationErrors(errors);
				} else {
					AdminForm.sendRequest(
						theForm.attr('action'),
						theForm.serialize(), null,
						function(data) {
							if (data.status == 1 && data.id != undefined) {
								document.location = 'admin.php?p=products_family&mode=update&id=' + data.id;
							} else {
								if (data.message !== undefined) {
									AdminForm.displayModalAlert(theModal, data.message, 'danger');
								} else if (data.errors !== undefined) {
									AdminForm.displayValidationErrors(data.errors);
								}
							}
						}
					)
				}

				return false;
			});

			$('form[name=form-productsfamily]').submit(function() {
				var theForm = $(this);
				var errors = [];
				if ($.trim(theForm.find('.field-name').val()) == '') {
					errors.push({field: '.field-name', message: trans.products_family.enter_title});
				}
				AdminForm.displayValidationErrors(errors);

				return errors.length == 0;
			});

			//Handle delete button
			$('[data-action="action-products-families-delete"]').click(function() {

				var deleteMode = $('input[name="form-products-families-delete-mode"]:checked').val();
				if (deleteMode == 'selected')
				{
					var ids = '';
					$('.checkbox-products_families:checked').each(function() {
						var id = $(this).val();
						ids += (ids == '' ? '' : ',') + id;
					});
					if (ids != '')
					{
						window.location='admin.php?p=products_family&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
					}
					else
					{
						alert(trans.products_family.select_group);
					}
				}
				else if (deleteMode == 'search')
				{
					window.location='admin.php?p=products_family&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
				}
			});

			// Manage product group section
			$("#field-categories").change(function() {
				updateAvailableProducts();
			});

			$('#field-categories').change();
			updateAssignedProductsList();

			$('#btnAddProduct').click(function() {
				$("#btnAddProduct").attr("disabled", "disabled");
				$("#btnRemoveProduct").attr("disabled", "disabled");
				var pf_id = $("#field-pf_id").val();
				var selectedProducts = [];
				$('#field-products').find(':selected').each(function(i, selectedOption) {
					selectedProducts[i] = $(selectedOption).val();
				});

				$.getJSON("admin.php?p=products_family&mode=assignProducts&pf_id=" + escape(pf_id) + '&products=' + escape(selectedProducts),
					function(data) {
						if (data.result) {
							updateAssignedProductsList();
							updateAvailableProducts();
						}
						$("#btnAddProduct").removeAttr("disabled");
						$("#btnRemoveProduct").removeAttr("disabled");
					});

				return false;
			});

			$('#btnRemoveProduct').click(function() {
				$("#btnAddProduct").attr("disabled", "disabled");
				$("#btnRemoveProduct").attr("disabled", "disabled");
				var pf_id = $("#field-pf_id").val();
				var selectedProducts = [];
				$('#field-selectedProducts').find(':selected').each(function(i, selectedOption) {
					selectedProducts[i] = $(selectedOption).val();
				});

				$.getJSON("admin.php?p=products_family&mode=unassignProducts&pf_id=" + escape(pf_id) + '&products=' + escape(selectedProducts), function(data) {
					if (data.result) {
						updateAssignedProductsList();
						updateAvailableProducts();
					}
					$("#btnAddProduct").removeAttr("disabled");
					$("#btnRemoveProduct").removeAttr("disabled");
				});
				return false;
			});
		});
	};

	updateAvailableProducts = function() {
		var pf_id = $("#field-pf_id").val();
		var selectedCategories = [];
		$('#field-categories').find(':selected').each(function(i, selectedOption) {
			selectedCategories[i] = $(selectedOption).val();
		});
		$.getJSON("admin.php?p=products_family&pf_id="+pf_id+"&mode=getProducts&categories=" + escape(selectedCategories), function(data) {
			$("#field-products").html("");
			$(data.products).each(function(i, product) {
				var o = $("<option></option>").val(product.pid).html(product.title);
				$("#field-products").append(o);
			});
		});

		//return false;
	};

	updateAssignedProductsList = function() {
		var pf_id = $("#field-pf_id").val();
		$.getJSON("admin.php?p=products_family&mode=getAssignedProducts&pf_id=" + escape(pf_id), function(data) {
			$("#field-selectedProducts").html("");
			$(data.products).each(function(i, product) {
				var o = $("<option></option>").val(product.pid).html(product.title);
				$("#field-selectedProducts").append(o);
			});
		});
	};

	remove = function(id, nonce) {
		AdminForm.confirm(trans.products_family.confirm_remove_product, function() {
			window.location = 'admin.php?p=products_family&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	init();

	return {
		remove: remove
	};
}(jQuery));

var WholesaleCentral = (function($) {
	var init;

	init = function() {
		$('#wholesale-central-sync').click(function() {
			AdminForm.sendRequest(
				'admin.php?p=wholesale_central&mode=sync&chart=1',
				null, 'Uploading products',
				function(data) {
					AdminForm.hideSpinner();
					if (data.status == 0) {
						AdminForm.displayAlert(data.message, 'danger');
					}
				}
			);

			return false;
		});
		$('#wholesale-central-export').click(function() {
			$('#iframe-download').attr('src', 'admin.php?p=wholesale_central&mode=export&chart=1');
			return false;
		});
	};

	init();
}(jQuery));
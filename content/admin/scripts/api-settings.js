var ApiSettings = (function($) {
	var init, handleDisplaySettingsForm;

	init = function() {
		$(document).ready(function() {
			handleDisplaySettingsForm();
			$('#field-plugin_openapi_active').change(function() {
				handleDisplaySettingsForm();
			});

			$('form[name=form-settings]').submit(function() {
				return handleSettingsFormSubmit($(this));
			});
			$('#token-generate').click(function() {
				var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
				var token = "";
				var length = 64;
				for (x=0; x<length; x++) token += chars.charAt(Math.floor(Math.random() * 62));
				$("#field-plugin_openapi_token").val(token);
				$("#field-plugin_openapi_token").closest('div.form-group').removeClass('has-error');
				$("#field-plugin_openapi_token").next('span.warning-sign').addClass('hidden');
				$("#field-plugin_openapi_token").next('span.warning-sign').next('p.error').addClass('hidden');
				return false;
			});
		});
	};

	handleSettingsFormSubmit = function(theForm) {
		if ($('#field-plugin_openapi_active').is(':checked')) {
			var errors = [];

			if ($.trim($('#field-plugin_openapi_username').val()) == '') {
				errors.push({
					field: '#field-plugin_openapi_username',
					message: trans.api.username_required
				});
			}
			if ($.trim($('#field-plugin_openapi_password').val()) == '') {
				errors.push({
					field: '#field-plugin_openapi_password',
					message: trans.api.password_required
				});
			}
			if ($.trim($('#field-plugin_openapi_token').val()) == '') {
				errors.push({
					field: '#field-plugin_openapi_token',
					message: trans.api.token_required
				});
			}
		}

		AdminForm.displayValidationErrors(errors);

		return errors.length == 0;
	};

	handleDisplaySettingsForm = function() {
		var apiActive = $('#field-plugin_openapi_active').is(':checked');

		$('#field-plugin_openapi_username').closest('.form-group').toggle(apiActive);
		$('#field-plugin_openapi_password').closest('.form-group').toggle(apiActive);
		$('#field-plugin_openapi_token').closest('.form-group').toggle(apiActive);
	};

	init();
}(jQuery));

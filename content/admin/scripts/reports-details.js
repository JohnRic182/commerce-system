var ReportsDashboard =  (function($) {
    var init, reportRange, loadSummary, loadGraph;

    var plotMain = null;

    /**
     * Init function
     */
    init = function () {
        $(document).ready(function(){

            reportRange = $('#reports-dashboard-ranges').reportRanges({
                currentRangeType: reportRangeType,
                dateFrom: reportDateFrom,
                dateTo: reportDateTo,
                callback: function(rangeType, start, end) {
                    AdminForm.displaySpinner('Updating report');
                    document.location = 'admin.php?p=report&report_type=' + reportType + '&range_type=' + rangeType + '&from=' + start + '&to=' + end
                }
            });

        });
    };
    init();

}(jQuery));
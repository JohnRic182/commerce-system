$(document).ready(function(){
	$('.form-manufacturers-delete').submit(function(){
		return false;
	});

	$('form[name=form-manufacturer]').submit(function() {
		var errors = [];
		if ($.trim($('#field-manufacturer_name').val()) == '') {
			errors.push({
				field: '#field-manufacturer_name',
				message: trans.manufacturers.enter_title
			});
		}
		if ($.trim($('#field-manufacturer_code').val()) == '') {
			errors.push({
				field: '#field-manufacturer_code',
				message: trans.manufacturers.enter_manufacturer_id
			});
		}

		AdminForm.displayValidationErrors(errors);

		return errors.length == 0;
	});

	/**
	 * Handle delete button
	 */
	$('[data-action="action-manufacturers-delete"]').click(function() {
		var deleteMode = $('input[name="form-manufacturers-delete-mode"]:checked').val();
		var nonce = $('#modal-manufacturers-delete').find('input[name="nonce"]').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-manufacturer:checked').each(function() {
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '') {
				window.location='admin.php?p=manufacturer&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
			}
			else
			{
				alert(trans.manufacturers.select_manufacturer_delete);
			}
		}
		else if (deleteMode == 'search')
		{
			window.location='admin.php?p=manufacturer&mode=delete&deleteMode=search&nonce=' + nonce;
		}
	});

	/**
	 * Handle title & id changes
	 */
	function manufacturersUpdateSeoUrl() {
		if (manufacturersSeoUrlSettings != undefined) {
			var url = manufacturersSeoUrlSettings.template
				.replace('%ManufacturerName%', $('#field-manufacturer_name').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.replace('%ManufacturerKey%', $('#field-manufacturer_code').val().replace(/[^\w\/]|_/g, ' ').replace(/[\/]|_/g, ' ').trim())
				.replace('%ManufacturerDbId%', '[id]')
				.trim()
				.replace(/\s+/g, manufacturersSeoUrlSettings.joiner);

			if (manufacturersSeoUrlSettings.lowercase) url = url.toLocaleLowerCase();
			$('#field-url_custom').attr('placeholder', url);
		}
	}

	$('#field-manufacturer_name,#field-manufacturer_code')
		.keyup(function() {
			manufacturersUpdateSeoUrl();
		})
		.change(function() {
			manufacturersUpdateSeoUrl();
		});

	showMetaCounter();
});

function removeManufacturer(id, nonce) {
	AdminForm.confirm(trans.manufacturers.confirm_delete_manufacturer, function() {
		window.location = 'admin.php?p=manufacturer&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
	});

	return false;
}
(function($) {
	var init, handleCampaignForm;

	init = function() {
		$('form[name="form-widgets-settings"]').submit(function() {
			var theForm = $(this);
			var theModal = theForm.closest('.modal');

			AdminForm.clearAlerts();

			AdminForm.sendRequest(
				'admin.php?p=widgets&mode=settings',
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=app&key=widgets';
					} else {
						if (data.message) {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						} else if (data.errors) {
							AdminForm.displayModalErrors(theModal, data.errors);
						}
					}
				}
			);
			return false;
		});
		$('form[name="add-campaign"]').submit(function() {
			return handleCampaignForm($(this), 'add');
		});

		$('.edit-campaign').click(function() {
			var id = $(this).attr('data-id');

			AdminForm.sendRequest(
				'admin.php?p=widgets&mode=update&id='+id,
				null,
				null,
				function(data) {
					if (data.status == 1) {
						AdminForm.hideSpinner();

						$('#edit-campaign-wrapper').html(data.html);

						var theModal = $('#modal-edit-campaign');
						$(theModal.find('fieldset').get(0)).addClass('fieldset-primary');

						$('form[name="form-edit-campaign"]').submit(function() {
							return handleCampaignForm($(this), 'update');
						});

						theModal.modal('show');
					}
				},
				null,
				'GET'
			);

			return false;
		});

		$('.delete-campaign').click(function() {
			var id = $(this).attr('data-id');
			var nonce = $(this).attr('data-nonce');

			AdminForm.confirm('Are you sure you wish to remove this campaign?', function() {
				AdminForm.sendRequest('admin.php?p=widgets&mode=delete&deleteMode=single',
					'id='+id+'&nonce='+nonce, null,
					function(data) {
						if (data.status == 1) {
							document.location = 'admin.php?p=app&key=widgets';
						}
					}
				);
			});

			return false;
		});

		/**
		 * Handle delete button
		 */
		$('[data-action="action-campaigns-delete"]').click(function(){
			var deleteMode = $('input[name="form-campaigns-delete-mode"]:checked').val();
			if (deleteMode == 'selected')
			{
				var ids = '';
				$('.checkbox-campaign:checked').each(function(){
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				if (ids != '')
				{
					window.location='admin.php?p=widgets&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
				}
				else
				{
					alert('Please select at least one campaign');
				}
			}
			else if (deleteMode == 'search')
			{
				window.location='admin.php?p=widgets&mode=delete&deleteMode=search' + '&nonce=' + $(this).attr('nonce');
			}
		});
	};

	handleCampaignForm = function(theForm, mode) {
		var theModal = theForm.closest('.modal');

		var errors = [];

		if ($.trim(theForm.find('input[name="name"]').val()) == '') {
			errors.push({
				field: theForm.find('input[name="name"]'),
				message: trans.widget.name_required
			});
			errors.push({
				field: theForm.find('input[name="description"]'),
				message: trans.widget.description_required
			});
		}

		AdminForm.clearAlerts();

		if (errors.length > 0) {
			AdminForm.displayValidationErrors(errors);
		} else {
			AdminForm.sendRequest(
				'admin.php?p=widgets&mode='+mode,
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=app&key=widgets';
					} else {
						if (data.message) {
							AdminForm.displayModalAlert(theModal, data.message, 'danger');
						} else if (data.errors) {
							AdminForm.displayValidationErrors(data.errors);
						}
					}
				}
			);
		}
		return false;
	};

	init();
}(jQuery));
var Reports = (function($) {
	var init, customRange, setStatisticCss,
		setDateRange, dateRangePicker, getDateRangePicker,
		setDateFrom, setDateTo, setRangeType,
		getDateFrom, getDateTo, getRangeType;

	/**
	 * Init function
	 */
	init = function() {
		$(document).ready(function(){

			moment().format();

			setDateRange(getDateFrom(), getDateTo());

			if (typeof $.jqplot != 'undefined') {
				$.jqplot.config.enablePlugins = true;
			}

			$('.widget').each(function (index, widget) {
				setTimeout(function () {
					$(widget).slideDown(500);
				}, index * 200);
			});

			$('[data-action="export-report"]').click(function(event){
				event.preventDefault();
				var form_data = $('form[name="form-reports-search"]').serialize();
				location.href = 'admin.php?' + form_data + '&export=Yes';
			});

			$('a.link-date-range').click(function(){
				var url = $(this).attr('href');
				url.replace('$rangeType', encodeURIComponent(rangeType));
				url.replace('$dateFrom', encodeURIComponent(dateFrom));
				url.replace('$dateFrom', encodeURIComponent(dateTo));

				window.location = url;

				return false;
			});

			dateRangePicker = $('#report-range').daterangepicker({
				startDate: moment($("#field-from").val()),
				endDate: moment($("#field-to").val()),
				linkedCalendars: false,
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}, setDateRange);

			$('#hidden-date-from, #hidden-date-to').datepicker({
				beforeShow: customRange,
				dateFormat: 'yy-mm-dd'
			});
		});
	};

	/**
	 *
	 */
	getDateRangePicker = function() {
		return dateRangePicker;
	}

	/**
	 *
	 * @param input
	 * @returns {*}
	 */
	customRange = function  (input) {
		if (input.id == 'field-to') {
			return {
				minDate: jQuery('#field-from').datepicker("getDate")
			};
		} else if (input.id == 'field-from') {
			return {
				maxDate: jQuery('#field-to').datepicker("getDate")
			};
		}
	};

	/**
	 *
	 * @param start
	 * @param end
	 */
	setDateRange = function(start, end) {

		$('#report-range span').html(moment(start).format('MMM D, YYYY') + ' - ' + moment(end).format('MMM D, YYYY'));

		if ($(dateRangePicker).data('daterangepicker') != undefined) {
			dateRangePicker.data('daterangepicker').setStartDate(moment(start));
			dateRangePicker.data('daterangepicker').setEndDate(moment(end));
		}
	};

	/**
	 *
	 * @param placeHolder
	 * @param statisticCurrent
	 * @param statisticPrevious
	 */
	setStatisticCss = function(placeHolder, statisticCurrent, statisticPrevious) {
		var currStatistic = parseFloat(statisticCurrent.toString().replace(/[^0-9.]/gi, ''));
		var prevStatistic = parseFloat(statisticPrevious.toString().replace(/[^0-9.]/gi, ''));

		if (currStatistic > prevStatistic) {
			$(placeHolder + ' .text-center').removeClass("red").addClass("green");
			$(placeHolder +" span.glyphicon")
				.removeClass("glyphicon-pause")
				.removeClass("glyphicon-triangle-bottom")
				.addClass("glyphicon-triangle-top");
		}
		else {
			if (currStatistic == prevStatistic) {
				$(placeHolder + ' .text-center').removeClass("green").removeClass("red").addClass("text-warning");
				$(placeHolder +" span.glyphicon")
					.removeClass("glyphicon-triangle-top")
					.removeClass("glyphicon-triangle-bottom")
					.addClass("glyphicon-pause");
			}
			else {
				$(placeHolder + ' .text-center').removeClass("green").addClass("red");
				$(placeHolder +" span.glyphicon")
					.removeClass("glyphicon-pause")
					.removeClass("glyphicon-triangle-top")
					.addClass("glyphicon-triangle-bottom");
			}
		}
	};

	setDateFrom = function (dateFrom) {
		$('#hidden-date-from').val(moment(dateFrom).format('Y-MM-DD'));
	};

	setDateTo = function (dateTo) {
		$('#hidden-date-to').val(moment(dateTo).format('Y-MM-DD'));
	};

	setRangeType = function (rangeType) {
		return $('#hidden-range-type').val(rangeType);
	};

	getDateFrom = function () {
		return moment($('#hidden-date-from').val()).format('Y-MM-DD');
	};

	getDateTo = function () {
		return moment($('#hidden-date-to').val()).format('Y-MM-DD');
	};

	getRangeType = function () {
		return $('#hidden-range-type').val();
	};

	return {
		init: init,
		setStatisticCss: setStatisticCss,
		setDateRange: setDateRange,
		getDateRangePicker: getDateRangePicker,
		setDateFrom: setDateFrom,
		setDateTo: setDateTo,
		setRangeType: setRangeType,
		getDateFrom: getDateFrom,
		getDateTo: getDateTo,
		getRangeType: getRangeType
	}

}(jQuery));

// ensure that reports are initialized first
Reports.init();

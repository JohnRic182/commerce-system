var Orders = (function($) {
	var init, removeOrder, initOrders, initOrder, customRange, toggleAdvancedSearch, handlePaymentProcessStatusChange;

	init = function() {
		$(document).ready(function() {
			initOrders();
			initOrder();

			$('#field-searchParams-from, #field-searchParams-to').datepicker({
				beforeShow: customRange,
				dateFormat: 'yy-mm-dd'
			});
		});
	};

	toggleAdvancedSearch = function() {
		if ($('#field-searchParams-advanced_search').is(':checked')) {
			$('#field-searchParams-state').closest('.form-group').show().removeClass('hidden');
			$('#field-searchParams-zip_code').closest('.form-group').show().removeClass('hidden');
			$('#field-searchParams-notes_content').closest('.form-group').show().removeClass('hidden');
			$('#field-searchParams-promo_code').closest('.form-group').show().removeClass('hidden');
			$('#field-searchParams-product_name').closest('.form-group').show().removeClass('hidden');
		}
		else {
			$('#field-searchParams-state').closest('.form-group').hide().addClass('hidden');
			$('#field-searchParams-zip_code').closest('.form-group').hide().addClass('hidden');
			$('#field-searchParams-notes_content').closest('.form-group').hide().addClass('hidden');
			$('#field-searchParams-promo_code').closest('.form-group').hide().addClass('hidden');
			$('#field-searchParams-product_name').closest('.form-group').hide().addClass('hidden');
		}
	};

	customRange = function (input) {
		if (input.id == 'field-searchParams-to') {
			return {
				minDate: jQuery('#field-searchParams-from').datepicker("getDate")
			};
		} else if (input.id == 'field-searchParams-from') {
			return {
				maxDate: jQuery('#field-searchParams-to').datepicker("getDate")
			};
		}
	};

	initOrders = function() {
		$('#field-searchParams-advanced_search').change(function() {
			toggleAdvancedSearch();
		});
		toggleAdvancedSearch();

		$('.form-orders-delete').submit(function(){
			return false;
		});

		$('#orders-print-invoices').click(function() {
			var ids = '';
			var selectedOrders = $('.checkbox-order:checked');
			selectedOrders.each(function(idx, order) {
				if (ids.length > 0) ids += ',';
				ids += $(order).val();
			});

			if (ids != '') {
				var win = window.open('admin.php?p=order_invoice&action=bulk-invoices&ids='+ids, '_blank');
				win.focus();
			} else {
				AdminForm.displayAlert(trans.orders.no_orders_selected_print, 'danger');
			}

			return false;
		});

		$('form[name="form-order-export"]').submit(function(){
			var theForm = $(this);
			var theModal = theForm.closest('.modal');

			var exportMode = $('input[name="form-orders-export-mode"]:checked').val();
			var nonce = theForm.find('input[name="nonce"]').val();

			if (exportMode == 'selected') {
				var ids = '';
				$('.checkbox-order:checked').each(function() {
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});

				if (ids != '') {
					window.location='admin.php?p=order&mode=export&exportMode=selected&ids=' + ids + '&nonce=' + nonce;
					$('#modal-orders-export').modal('hide');
				}
				else {
					AdminForm.displayModalAlert(theModal, trans.orders.no_orders_selected_export, 'danger');
				}
			}
			else if (exportMode == 'search') {
				window.location='admin.php?p=order&mode=export&exportMode=search&nonce=' + nonce;
				$('#modal-orders-export').modal('hide');
			}
			return false;
		});

		$('form[name="form-orders-bulk-update-status"]').submit(function(){
			var theForm = $(this);
			var theModal = theForm.closest('.modal');

			var updateMode = $('input[name="updateMode"]:checked').val();

			if (updateMode == 'selected') {
				var ids = '';
				$('.checkbox-order:checked').each(function() {
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});

				if (ids != '') {
					$('input[name=ids]').val(ids);
					//$('#modal-orders-bulk-update-status').modal('hide');
					return true;
				}
				else {
					AdminForm.displayModalAlert(theModal, trans.orders.no_orders_selected_update, 'danger');
				}
			}
			else if (updateMode == 'search') {
				//$('#modal-orders-bulk-update-status').modal('hide');
				return true;
			}
			return false;
		});

		$('form[name="form-orders-delete"]').submit(function() {
			var theForm = $(this);
			var theModal = theForm.closest('.modal');

			var deleteMode = $('input[name="form-orders-delete-mode"]:checked').val();
			var nonce = theForm.find('input[name="nonce"]').val();

			if (deleteMode == 'selected') {
				var ids = '';
				$('.checkbox-order:checked').each(function() {
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				if (ids != '') {
					window.location='admin.php?p=order&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' + nonce;
				}
				else {
					AdminForm.displayModalAlert(theModal, trans.orders.no_orders_selected_delete, 'danger');
				}
			}
			else if (deleteMode == 'search') {
				window.location='admin.php?p=order&mode=delete&deleteMode=search&nonce=' + nonce;
			}

			return false;
		});

		$('form[name="form-orders-bulk-fulfillment"]').submit(function() {
			var theForm = $(this);
			var theModal = theForm.closest('.modal');

			var fulfillMode = $('input[name="form-orders-fulfill-mode"]:checked').val();

			if (fulfillMode == 'selected') {
				var ids = '';
				$('.checkbox-order:checked').each(function(){
					var id = $(this).val();
					ids += (ids == '' ? '' : ',') + id;
				});
				$('#bulk-fulfillment-ids').val(ids);
				if ($.trim(ids) == '') {
					AdminForm.displayModalAlert(theModal, trans.orders.no_orders_selected_fulfill, 'danger');
					return false;
				}
			}

			AdminForm.sendRequest(
				theForm.attr('action'),
				theForm.serialize(), null,
				function(data) {
					if (data.status) {
						document.location = 'admin.php?p=orders';
					} else {
						var errors = [];
						$.each(data.errors, function(key, value) {
							$.each(value, function(idx, message) {
								errors.push({
									field: '.field-'+key,
									message: message
								});
							});
						});

						AdminForm.displayModalErrors(theModal, errors);
					}
				}
			);

			return false;
		});

		$('.remove-order').click(function() {
			var theLink = $(this);

			return removeOrder(theLink.attr('data-id'), theLink.attr('data-nonce'));
		});
	};

	initOrder = function() {
		$('#edit-order-edit').click(function() {
			$('#edit-order-edit').hide();
			$('#edit-order-cancel,#edit-order-save').show();

			$('.order-editable-amount').hide();
			$('.order-editable').show();
		});

		$('#edit-order-cancel').click(function() {
			$('#edit-order-edit').show();
			$('#edit-order-cancel, #edit-order-save').hide();

			$('.order-editable-amount').show();
			$('.order-editable').hide();
		});

		$('#form-order').submit(function() {
			var errors = [];
			$('input[name^=prices],input[name^=quantities],#new_discount_amount,#new_promo_discount_amount').each(function(idx, element) {
				var val = parseInt($(element).val());

				if (val < 0) {
					errors.push({
						field: '#'+$(element).attr('id'),
						message: trans.orders.invalid_number
					});
				}
			});

			if (errors.length > 0) {
				AdminForm.displayErrors(errors);
				return false;
			}

			return true;
		});

		$('#return-inventory-btn').click(function() {
			AdminForm.confirm(trans.orders.return_items, function() {
				$('#edit-order-action').val('return-inventory');
				$('#form-order').submit();
			});
		});

		$('#send-doba-order').click(function() {
			AdminForm.confirm(trans.doba.send_order, function() {
				AdminForm.sendRequest(
					'admin.php?p=doba_json&action=sendOrder&oid=' + currentOrderId, null, null,
					function(data) {
						if (data.status == 1) {
							document.location = 'admin.php?p=order&id=' + currentOrderId;
						} else {
							AdminForm.displayAlert(data.message, 'danger');
						}
					},
					null, 'GET'
				);
			});
			return false;
		});

		$('form[name="form-fund-doba-order"]').submit(function(){
			var theForm = $(this);
			var theModal = theForm.closest('.modal');

			AdminForm.sendRequest(
				'admin.php?p=doba_json&chart=true&action=fundOrder',
				theForm.serialize(), null,
				function(data) {
					if (data.status == 1) {
						document.location = 'admin.php?p=order&id=' + currentOrderId;
					} else {
						AdminForm.displayModalAlert(theModal, data.message, 'danger');
					}
				}
			);

			return false;
		});

		$('form[name="form-credit-card-data"]').submit(function() {
			var theForm = $(this);
			var theModal = $(this).closest('.modal');

			AdminForm.removeErrors();
			AdminForm.clearAlerts();
			AdminForm.sendRequest(
				'admin.php?p=order&mode=stored-card&id=' + currentOrderId,
				theForm.serialize(), null,
				function(data) {
					if (data && data.status != undefined) {
						if (data.data != undefined && data.status == 1 && data.data.fn != undefined) {
							var details = $('<div></div>').html(
								data.data.fn + ' ' + data.data.ln + '<br>'+
								(data.data.ct != null ? data.data.ct + '<br>' : '') +
								data.data.cc + '<br>' +
								data.data.em + '/' + data.data.ey
							);

							$('#cc_details').html(details);
							$('#stored-credit-card-form').hide();
							$('#stored-credit-card-panel').show();

							theModal.find('.modal-footer > div').hide();
						} else {
							AdminForm.displayModalAlert(theModal, data.message != undefined ? data.message : trans.orders.message_cannot_retrieve_credit_card, 'danger');
						}
					} else {
						AdminForm.displayModalAlert(theModal, trans.orders.message_cannot_retrieve_credit_card, 'danger');
					}
				}
			);

			return false;
		});

		$('#remove-credit-card').click(function() {
			var theModal = $(this).closest('.modal');

			AdminForm.removeErrors();
			AdminForm.clearAlerts();

			AdminForm.sendRequest(
				'admin.php?p=order&mode=remove-card&id=' + currentOrderId,
				null, null,
				function(data) {
					if (data && data.status != undefined) {
						if (data.status == 1) {
							theModal.modal('hide');
							$('#btn-stored-credit-card').remove();
						} else {
							AdminForm.displayModalAlert(theModal, data.message != undefined ? data.message : trans.orders.message_cannot_retrieve_credit_card, 'danger');
						}
					} else {
						AdminForm.displayModalAlert(theModal, trans.orders.message_cannot_retrieve_credit_card, 'danger');
					}
				}
			);
		});

		$('form[name="form-add-order-note"]').submit(function() {
			var theForm = $(this);
			var theModal = theForm.closest('.modal');
			var title = $.trim(theForm.find('.field-title').val());
			var note = $.trim(theForm.find('.field-note').val());

			if (title == '' || note == '') {
				var errors = [];
				if (title == '') {
					errors.push({
						field: '.field-title',
						message: trans.orders.title_required
					});
				}
				if (title == '') {
					errors.push({
						field: '.field-note',
						message: trans.orders.note_required
					});
				}
				AdminForm.displayModalErrors(theModal, errors);

				return false;
			} else {
				AdminForm.sendRequest(
					theForm.attr('action'),
					theForm.serialize(), null,
					function(data) {
						if (data.status) {
							document.location = 'admin.php?p=order&id='+currentOrderId;
						} else {
							var errors = [];
							$.each(data.errors, function(key, value) {
								$.each(value, function(idx, message) {
									errors.push({
										field: '.field-'+key,
										message: message
									});
								});
							});

							AdminForm.displayModalErrors(theModal, errors);
						}
					}
				);
			}

			return false;
		});

		/*
		 * Edit tracking_company and tracking_number;
		 */
		$('form[name="form-order-edit-shipment"]').submit(function() {
			var theForm = $(this);

			AdminForm.displaySpinner();

			AdminForm.sendRequest(
				theForm.attr('action'),
				theForm.serialize(), null,
				function(data) {
					AdminForm.hideSpinner();
					if (data.status) {
						AdminForm.displayAlert( 'Success!', 'success');
						document.location = 'admin.php?p=order&id='+currentOrderId;
					} else {
						var errors = [];
						$.each(data.errors, function(key, value) {
							$.each(value, function(idx, message) {
								errors.push({
									field: '.field-'+key,
									message: message
								});
							});
						});
						AdminForm.displayErrors(errors);
					}
				},
				function() {
					AdminForm.hideSpinner();
				}
			);
			return false;
		});

		$("#order-shipments-collapse-button").click(function (){

			if ($("#group-order-shipments").hasClass('open')) {
				$("#order-shipments-save-button").hide();
			}
			else {
				$("#order-shipments-save-button").show();
			}
		});

		var addFulfillmentModalEle = $('#modal-order-add-fulfillment');

		/*
		var labelInputs = addFulfillmentModalEle.find('input[name="stamps_label"]');
		if (labelInputs.length > 0) {
			var handleLabelToggle = function() {
				var enabled = $('input[name="stamps_label"]:checked').val() == '0';

				$('#field-tracking_number').closest('.form-group').toggle(enabled);
				$('#field-tracking_company').closest('.form-group').toggle(enabled);
			};
			labelInputs.change(function() {
				handleLabelToggle();
			});
			handleLabelToggle();
		} else {
			labelInputs = addFulfillmentModalEle.find('input[name="endicia_label"]');
			var handleLabelToggle = function() {
				var enabled = $('input[name="endicia_label"]:checked').val() == '0';

				$('#field-tracking_number').closest('.form-group').toggle(enabled);
				$('#field-tracking_company').closest('.form-group').toggle(enabled);
			};
			labelInputs.change(function() {
				handleLabelToggle();
			});
			handleLabelToggle();
		}
		*/

//		labelInputs.each(function(idx, ele) {
//			forms.initCheckbox($(ele).closest('div'));
//		});

		addFulfillmentModalEle.find('input[name^=quantity]').blur(function() {
			var val = parseInt($(this).val());
			var max = parseInt($(this).attr('max'));

			if (val > max) {
				$(this).val(max);
				val = max;
			}

			var hasQuantityLessThanMax = val < max;

			if (!hasQuantityLessThanMax) {
				$('#modal-order-add-fulfillment').find('input[name^=quantity]').each(function(idx, input) {
					var val = $(input).val();
					var max = $(input).attr('max');

					if (val < max) {
						hasQuantityLessThanMax = true;
					}
				});
			}

			$('#field-capture_amount_fulfillment').closest('.form-group').toggle(hasQuantityLessThanMax);

			$('#field-complete_order_fulfillment').closest('.form-group').toggle(!hasQuantityLessThanMax);
		});

		$('form[name="form-order-add-fulfillment"]').submit(function() {
			var theForm = $(this);
			var theModal = theForm.closest('.modal');
			var hasItemToFulfill = false;
			theForm.find('input[name^=quantity]').each(function(idx, input) {
				if (parseInt($(input).val()) > 0) {
					hasItemToFulfill = true;
				}
			});

			if (!hasItemToFulfill) {
				var errors = [];
				errors.push({
					field: '',
					message: trans.orders.no_items_fulfill
				});

				AdminForm.displayModalErrors(theModal, errors);
			} else {
				AdminForm.sendRequest(
					$(theForm).attr('action'),
					$(theForm).serialize(), null,
					function(data) {
						if (data.status) {
							document.location = 'admin.php?p=order&id='+currentOrderId ;
						} else {
							if (data.errors !== undefined && data.errors.length > 0) {
								var errors = [];
								$.each(data.errors, function(key, value) {
									$.each(value, function(idx, message) {
										errors.push({
											field: '.field-'+key,
											message: message
										});
									});
								});

								AdminForm.displayModalErrors(theModal, errors);
							} else if (data.message !== undefined && data.message) {
								AdminForm.displayModalAlert(theModal, data.message, 'danger');
							}
						}
					}
				);
			}

			return false;
		});

		$('#modal-order-process-payment').on('show.bs.modal', function() {
			handlePaymentProcessStatusChange();
			$(this).find('.field-status').change(function() {
				handlePaymentProcessStatusChange();
			})
		});
	};

	handlePaymentProcessStatusChange = function() {
		var theModal = $('#modal-order-process-payment');

		var status = theModal.find('.field-status').val();
		if (status == 'capture') {
			theModal.find('.field-capture_amount').closest('.form-group').removeClass('hidden').show();
			theModal.find('.field-refund_amount').closest('.form-group').hide();
		} else if (status == 'refund') {
			theModal.find('.field-refund_amount').closest('.form-group').removeClass('hidden').show();
			theModal.find('.field-capture_amount').closest('.form-group').hide();
		} else {
			theModal.find('.field-refund_amount').closest('.form-group').hide();
			theModal.find('.field-capture_amount').closest('.form-group').hide();
		}
	};

	removeOrder = function(id, nonce) {
		AdminForm.confirm(trans.orders.confirm_remove, function() {
			window.location = 'admin.php?p=order&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
		});

		return false;
	};

	init();
}(jQuery));

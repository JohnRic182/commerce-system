$(document).ready(function(){
    $('#btnDownload').click(function(){
        $('#frmDownload').submit();
    });

    var form = $('#form-generate-certificate');

    $(form).submit(function(e){
        $(form).find('.form-group').removeClass('has-error');
        var errors = [];

        $(form).find('.form-group.required').each(function(idx, formGroup) {
            var label = $(formGroup).find('label');
            var input = $(formGroup).find('input,select,texarea');
            var value = $.trim(input.val());
            if (value == '' && label.length != 0) {
                errors.push({
                    'field': input,
                    'message': 'Please enter ' + label.html()
                });
            }
        });

        if (errors.length > 0)
        {
            e.preventDefault();

            AdminForm.displayModalErrors(form, errors);
            return false;
        }

        return true;
    });
});

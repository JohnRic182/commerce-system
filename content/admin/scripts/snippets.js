$(document).ready(function(){
	$('form[name=form-snippet]').submit(function() {
		var errors = [];
		if ($.trim($('#field-name').val()) == '') {
			errors.push({
				field: '#field-name',
				message: 'Please enter name'
			});
		}
		if ($.trim($('#field-snippet_id').val()) == '') {
			errors.push({
				field: '#field-snippet_id',
				message: 'Please enter snippet id'
			});
		}
		if ($.trim($('#field-content').val()) == '') {
			errors.push({
				field: '#field-content',
				message: 'Please enter content'
			});
		}
		AdminForm.displayErrors(errors);

		return errors.length == 0;
	});

	$('#field-name')
	.keyup(function(){ autoFillSnippetId(); })
	.change(function(){ autoFillSnippetId(); });

	$('#field-snippet_id')
	.keyup(function(){ stopAutoFillSnippetId(); });

	/**
	 * Handle delete button
	 */
	$('[data-action="action-snippets-delete"]').click(function(){

		var deleteMode = $('input[name="form-snippets-delete-mode"]:checked').val();

		if (deleteMode == 'selected')
		{
			var ids = '';
			$('.checkbox-snippet:checked').each(function(){
				var id = $(this).val();
				ids += (ids == '' ? '' : ',') + id;
			});
			if (ids != '')
			{
				window.location='admin.php?p=snippet&mode=delete&deleteMode=selected&ids=' + ids + '&nonce=' +  $(this).attr('nonce');
			}
			else
			{
				alert(trans.snippets.select_delete_snippet);
			}
		}
		else if (deleteMode == 'all')
		{
			window.location='admin.php?p=snippet&mode=delete&deleteMode=all' + '&nonce=' + $(this).attr('nonce');
		}
	});

	if ($("#field-snippet_id").val() == '')
	{
		can_autofill = true;
	}

});

function removeSnippet(id, nonce) {
	AdminForm.confirm(trans.snippets.confirm_remove_snippet, function() {
		window.location = 'admin.php?p=snippet&mode=delete&deleteMode=single&id=' + id + '&nonce=' + nonce;
	});

	return false;
}

var can_autofill = false;

function autoFillSnippetId()
{
	if (can_autofill)
	{
		field_name = $("#field-name").val();
		var re = new RegExp(' ', 'g');
		field_name = field_name.replace(re, '_');

		$("#field-snippet_id").val(field_name);
	}
};

function stopAutoFillSnippetId()
{
	can_autofill = false;
	if ($("#field-snippet_id").val() == '')
	{
		can_autofill = true;
	}
}
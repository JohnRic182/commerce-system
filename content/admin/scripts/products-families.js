var ProductFamilies = (function($) {
	var init,
		productAddFamily, productRemoveFamily, productSetFamilies;

	init = function() {
		$(document).ready(function(){
			$('#product-families-available').dblclick(function(){ productAddFamily(); });
			$('#product-families-assigned').dblclick(function(){ productRemoveFamily(); });
			$('#btn-add-family').click(function(){ productAddFamily(); return false; });
			$('#btn-remove-family').click(function(){ productRemoveFamily(); return false; });
			productSetFamilies();
		});
	};

	productAddFamily = function() {
		var opt = $('#product-families-available').find('option:selected');
		if (opt.length > 0)
		{
			for (var i = 0; i < opt.length; i++) {
				$('<option value="' + $(opt[i]).val() + '">' + $(opt[i]).html() + '</option>').appendTo('#product-families-assigned');
				$(opt[i]).remove();
			}
		}
		productSetFamilies();
	};

	productRemoveFamily = function() {
		var opt = $('#product-families-assigned').find('option:selected');
		if (opt.length > 0)
		{
			for (var i = 0; i < opt.length; i++) {
				$('<option value="' + $(opt[i]).val() + '">' + $(opt[i]).html() + '</option>').appendTo('#product-families-available');
				$(opt[i]).remove();
			}
		}
		productSetFamilies();
	};

	productSetFamilies = function() {
		var s = '';
		$('#product-families-assigned').find('option').each(function(index, option){
			s = s + (s == '' ? '' : ',') + $(option).attr('value');
		});

		$('#product_families_selected').val(s);
	};

	init();

}(jQuery));






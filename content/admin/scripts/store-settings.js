var StoreSettings = (function($) {
	var init, toggleStoreClosedMessage,
		toggleEmailSettings, toggleCaptchaSettings;

	init = function() {
		$(document).ready(function() {
			var toggleTimezones = function() {
				if ($('#field-LocalizationUseSystemTimezone').is(':checked')) {
					$('#field-LocalizationDefaultTimezone').closest('.form-group').hide();
				} else {
					$('#field-LocalizationDefaultTimezone').closest('.form-group').removeClass('hidden').show();
				}
			};

			$('#field-LocalizationUseSystemTimezone').change(function() {
				toggleTimezones();
			});

			toggleTimezones();

			$('#field-StoreClosed').change(function() {
				toggleStoreClosedMessage();
			});
			toggleStoreClosedMessage();

			$('#field-EmailSendmailEngine').change(function() {
				toggleEmailSettings();
			});
			toggleEmailSettings();

			$('#field-captchaMethod').change(function() {
				toggleCaptchaSettings();
			});

			toggleCaptchaSettings();
		});
	};

	toggleStoreClosedMessage = function() {
		$('#field-StoreClosedMessage').closest('.form-group').toggle(!$('#field-StoreClosed').is(':checked'));
	};

	toggleEmailSettings = function() {
		var emailEngine = $('#field-EmailSendmailEngine').val();

		$('#field-EmailSMTPSecure').closest('.form-group').toggle(emailEngine == 'phpmailer');
		$('#field-EmailSMTPServer').closest('.form-group').toggle(emailEngine == 'phpmailer');
		$('#field-EmailSMTPPort').closest('.form-group').toggle(emailEngine == 'phpmailer');
		$('#field-EmailSMTPLogin').closest('.form-group').toggle(emailEngine == 'phpmailer');
		$('#field-EmailSMTPPassword').closest('.form-group').toggle(emailEngine == 'phpmailer');
	};

	toggleCaptchaSettings = function() {
		var captchaMethodEnabled = $('#field-captchaMethod').is(':checked');
		$('#field-captchaReCaptchaPublicKey').closest('.form-group').toggle(captchaMethodEnabled);
		$('#field-captchaReCaptchaPrivateKey').closest('.form-group').toggle(captchaMethodEnabled);
	};

	init();
}(jQuery));
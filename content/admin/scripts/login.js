(function($) {
	var init;

	init = function() {
		$('#forgotPassword').click(function() {
			$('#frmLogin').hide();
			$('#frmResetValidate').show();

			return false;
		});

		$('#frmResetValidate').submit(function() {
			$('#form-error').hide();
			var theForm = $(this);
			AdminForm.sendRequest('login.php',
				theForm.serialize(), null,
				function(data) {
					if (data.verified) {
						theForm.hide();
						var theRecoveryForm = $('#frmResetRecovery');
						theRecoveryForm.find('input[name=username]').val(theForm.find('input[name=username]').val());
						theRecoveryForm.find('input[name=email]').val(theForm.find('input[name=email]').val());
						$('#security_question').html(data.q);
						theRecoveryForm.show();
					} else {
						$('#form-error').html(trans.login.message_incorrect_username_email).show();
					}
				}
			);

			return false;
		});

		$('#frmLogin').find('input[name=username]').focus();
	};

	init();
}(jQuery));
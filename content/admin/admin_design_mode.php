<?php

if (!defined("ENV")) exit();

if (!isset($_SESSION['admin_design_mode']) || !$_SESSION['admin_design_mode']) die();

if (isset($_REQUEST['action']))
{
	$designmode = new Ddm_DesignMode($settings);

	switch ($_REQUEST['action'])
	{
		/**
		 * Ping
		 */
		case "ping" :
		{
			$response = array("status"=>1);
			print json_encode($response);
			break;
		}

		/**
		 * Save layout
		 */
		case 'saveLayout' :
		{
			if (isset($_POST['scope']) && isset($_POST['page']) && 
				isset($_POST['pages']) && isset($_POST['layout'])
			)
			{
				print $designmode->saveLayout(
					$_POST['scope'], $_POST['page'], $_POST['pages'], $_POST['layout'], 
					$settings['LayoutSiteWidth'], isset($_POST["theme"]) ? $_POST["theme"] : null
				);
			}
			break;
		}
		
		/**
		 * Restore layout changes
		 */
		case 'restoreLayout' :
		{
			print $designmode->restoreLayout();
			break;
		}
		
		case 'clearSkinCache' :
		{
			print $designmode->clearSkinCache();
			break;
		}
		
		/**
		 * Restore theme changes
		 */
		case 'restoreSkin' :
		{
			print $designmode->restoreSkin();
			break;
		}
		
		/**
		 * Saves all checkout pages without columns or boxes
		 */
		case 'saveCleanCheckout':
		{
			print $designmode->saveCleanCheckout();
			break;
		}
		
		/**
		 * Gets the contents of the custom css file
		 */
		case 'getCustomCssFile':
		{
			print $designmode->getCustomCssFile();
			break;
		}
		
		/**
		 * Saves the contents of the custom css file
		 */
		case 'saveCustomCssFile':
		{
			$css_data = isset($_POST['css_data']) ? $_POST['css_data'] : '';
			print $designmode->saveCustomCssFile($css_data);
			break;
		}
		
		/**
		 * Creates a new element
		 */
		case 'createCustomElement':
		{	
			if (isset($_POST['element_type']) && isset($_POST['element_id']) && isset($_POST['element_title']) && isset($_POST['element_html']))
			{
				print $designmode->createCustomElement($_POST['element_type'], $_POST['element_id'], $_POST['element_title'], $_POST['element_html']);
			}
			break;
		}
		
		/**
		 * Returns element html for editing
		 */
		case 'getElementHtml':
		{	
			print $designmode->getElementHtml($_REQUEST['class']);
			break;
		}
		
		/**
		 * Returns settings array
		 */
		case 'getSettings':
		{
			$response = array("status"=>1, "settings"=>$settings);
			print json_encode($response);
			break;
		}
		
		/**
		 * Save settings
		 */
		case 'saveSettings': 
		{
			$data = json_decode($_POST["data"], true);
			foreach ($data as $key=>$value)
			{
				$db->query("UPDATE ".DB_PREFIX."settings SET value='".$db->escape($value)."' WHERE name='".$db->escape($key)."'");
				$settings[$key] = $value;
			}
			
			$response = array("status"=>1);
			print json_encode($response);
			
			break;			
		}
		
		/**
		 * Returns css map for elements
		 */
		case 'getElementsCssMap':
		{
			print $designmode->getElementsCssMap($_REQUEST['files']);
			break;				
		}
		
		/**
		 * Saves element HTML after editing
		 */
		case 'saveElementHtml':
		{	
			print $designmode->saveElementHtml($_REQUEST['class'], $_REQUEST['data']);
			break;
		}
		
		/**
		 * Saves colors changes
		 */
		case 'saveColors':
		{	
			print $designmode->saveColors(json_decode($_REQUEST['data'], true));
			break;
		}
		
		/**
		 * Saves fonts changes
		 */
		case 'saveFonts':
		{	
			print $designmode->saveFonts(json_decode($_REQUEST['data'], true));
			break;
		}
		
		/**
		 * Restores element html
		 */
		case 'restoreElementHtml':
		{
			print $designmode->restoreElementHtml($_REQUEST['class']);
			break;	
		}
		
		/**
		 * Get element styles
		 */
		case 'getElementStyles':
		{
			print $designmode->getElementStyles($_REQUEST['parent-class'], $_REQUEST['parent-selector'], $_REQUEST['class'], $_REQUEST['selector']);
			break;
		}
		/**
		 * Save element stles
		 */
		case 'saveElementStyles':
		{
			print $designmode->saveElementStyles($_REQUEST['parent-class'], $_REQUEST['parent-selector'], $_REQUEST['selector'], $_REQUEST['css']);
			break;
		}
		/**
		 * Handle background image uploads
		 */
		case 'uploadBackgroung': 
		{
			print $designmode->uploadBackground($_REQUEST['parent-class'], $_REQUEST['parent-selector'], $_REQUEST['class'] == "_none_" ? "" : $_REQUEST['class'], $_REQUEST['selector'] == ' ' ? '' : $_REQUEST['selector'], $_FILES["Filedata"]);
			break;
		}
		
		/**
		 * Handle image uploads
		 */
		case 'uploadImage': 
		{
			print $designmode->uploadImage($_REQUEST['parent-class'], $_REQUEST['parent-selector'], $_REQUEST['class'], $_REQUEST['selector'], $_FILES["Filedata"]);
			break;
		}
		
		/**
		 * Deletes custom image
		 */
		case 'deleteCustomImage':
		{
			print $designmode->deleteCustomImage($_REQUEST['class']);
			break;
		}
		
		/**
		 * Hides image
		 */
		case 'showImage':
		{
			print $designmode->showImage($_REQUEST['class']);
			break;
		}
		
		/**
		 * Hides image
		 */
		case 'hideImage':
		{
			print $designmode->hideImage($_REQUEST['class']);
			break;
		}
		
		/**
		 * Returns element CSS from content/skin/_custom/css/
		 */
		case 'getElementCss':
		{
			print $designmode->getElementCss($_REQUEST['class']);
			break;
		}
		
		/**
		 * Saves element CSS after editing
		 */
		case 'saveElementCss':
		{	
			print $designmode->saveElementCss($_REQUEST['class'], $_REQUEST['data']);
			break;
		}
		
		/**
		 * Deletes custom element
		 */
		case 'deleteCustomElement':
		{
			if (isset($_POST['class']))
			{
				print $designmode->deleteCustomElement($_POST['class']);
			}
			break;
		}
	}
}

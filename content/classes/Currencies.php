<?php 
/**
 * @class Currencies
 */
class Currencies implements CurrenciesInterface
{
	public $db = false;
	public $errors = array();

	protected $defaultCurrency = false;

	function Currencies(&$db)
	{
		$this->db = $db;
		return $this;
	}
	
	function getCurrencies()
	{
		$this->db->query("SELECT * FROM ".DB_PREFIX."currencies ORDER BY is_default, title");
		return $this->db->numRows() > 0 ? $this->db->getRecords() : false;
	}
	
	function getExchangeRates()
	{
		$rates = array();
		
		$r = $this->db->query("SELECT code, exchange_rate FROM ".DB_PREFIX."currencies ORDER BY is_default, title");
		
		while (($c = $this->db->moveNext($r)) != false)
		{
			$rates[$c['code']] = $c['exchange_rate'];
		}
		
		return $rates;
	}
	
	function getDefaultCurrency()
	{
		if (is_null($this->defaultCurrency) || $this->defaultCurrency === false)
		{
			$this->db->query("SELECT * FROM ".DB_PREFIX."currencies WHERE is_default='Yes'");
			if ($this->db->moveNext())
			{
				$this->defaultCurrency = $this->db->col;
				$_SESSION["default_currency"] = $this->db->col;
			}
		}
		
		return $this->defaultCurrency;
	}
	
	function getCurrency($currency_id)
	{
		$this->db->query("SELECT * FROM ".DB_PREFIX."currencies WHERE currency_id='".intval($currency_id)."'");
		if ($this->db->moveNext())
		{
			return $this->db->col;
		}
		return false;
	}
	
	function getCurrentCurrency()
	{
		if (!isset($_SESSION["current_currency"]) || is_null($_SESSION['current_currency']) || $_SESSION['current_currency'] == '' || !is_array($_SESSION['current_currency']))
		{
			$_SESSION["current_currency"] = $this->getDefaultCurrency();
		}
		else
		{
			$_SESSION["current_currency"] = $this->getCurrency($_SESSION["current_currency"]["currency_id"]);
		}

		return $_SESSION['current_currency'];
	}
	
	function setCurrentCurrency($currency_id)
	{
		if (($current_currency = $this->getCurrency($currency_id)) != false)
		{
			$_SESSION["current_currency"] = $current_currency;
		}
		else
		{
			$_SESSION["current_currency"] = $this->getDefaultCurrency();
		}
	}

	function clearDefaultCurrency()
	{
		$this->defaultCurrency = false;
	}

	function addCurrency($post_data)
	{
		$this->db->reset();
		$this->db->assignStr("title", $post_data["title"]);
		$this->db->assignStr("code", $post_data["code"]);
		$this->db->assignStr("symbol_left", $post_data["symbol_left"]);
		$this->db->assignStr("symbol_right", $post_data["symbol_right"]);
		$this->db->assignStr("decimal_places", intval($post_data["decimal_places"]));
		$this->db->assignStr("exchange_rate", floatval($post_data["exchange_rate"]));
		$currency_id = $this->db->insert(DB_PREFIX."currencies");
		
		if (isset($post_data["is_default"]))
		{
			$this->db->query("UPDATE ".DB_PREFIX."currencies SET is_default='No'");
			$this->db->query("UPDATE ".DB_PREFIX."currencies SET is_default='Yes' WHERE currency_id='".intval($currency_id)."'");

			$this->updateExchangeRates();
		}

		$this->clearDefaultCurrency();

		return $currency_id;
	}
	
	function updateCurrencies($post_data)
	{
		$currentDefaultCurrency = $this->getDefaultCurrency();
		$newDefaultCurrencyId = false;
		foreach ($post_data["code"] as $currency_id => $value)
		{
			$this->db->reset();
			$this->db->assignStr("title", $post_data["title"][$currency_id]);
			$this->db->assignStr("code", $post_data["code"][$currency_id]);
			$this->db->assignStr("symbol_left", $post_data["symbol_left"][$currency_id]);
			$this->db->assignStr("symbol_right", $post_data["symbol_right"][$currency_id]);
			$this->db->assignStr("decimal_places", $post_data["decimal_places"][$currency_id]);
			$this->db->assignStr("exchange_rate", $post_data["exchange_rate"][$currency_id]);
			$this->db->assignStr("is_default", $post_data["is_default"] == $currency_id ? "Yes":"No");
			$this->db->update(DB_PREFIX."currencies", "WHERE currency_id='".intval($currency_id)."'");

			if ($post_data["is_default"] == $currency_id)
			{
				$newDefaultCurrencyId = $currency_id;
			}
		}

		$this->clearDefaultCurrency();

		if ($newDefaultCurrencyId != $currentDefaultCurrency['currency_id'])
		{
			$this->updateExchangeRates();
		}

		return true;
	}
	
	function deleteCurrency($currency_id)
	{
		$this->db->query("SELECT * FROM ".DB_PREFIX."currencies WHERE currency_id='".intval($currency_id)."' AND is_default='No'");
		if ($this->db->moveNext())
		{
			$this->db->query("DELETE FROM ".DB_PREFIX."currencies WHERE currency_id='".intval($currency_id)."'");	
		}

		$this->clearDefaultCurrency();

		return true;
	}
	
	function updateExchangeRates()
	{
		$cx = new CurrencyExchange();
		$cx->getData();
		
		//get base currency
		$this->db->query("SELECT * FROM ".DB_PREFIX."currencies WHERE is_default='Yes'");
		if ($this->db->moveNext())
		{
			$def = $this->db->col;
		}
		$this->db->reset();
		$this->db->assign("exchange_rate", 1.000000);
		$this->db->update(DB_PREFIX."currencies", "WHERE currency_id=".intval($def["currency_id"]));
		
		if (array_key_exists($def["code"], $cx->Rates))
		{
			//get currencies
			$cur = $this->getCurrencies();
			for ($i=0; $i<count($cur); $i++)
			{
				if (array_key_exists($cur[$i]["code"], $cx->Rates))
				{
					$rate = $cx->Convert($def["code"], $cur[$i]["code"]);
					if ($rate)
					{
						$this->db->query("UPDATE ".DB_PREFIX."currencies SET exchange_rate='".$rate."' WHERE currency_id='".intval($cur[$i]["currency_id"])."'");
					}
					else
					{
						$this->errors[] = $cur[$i]["code"]." is not a recognized currency identifier. Exchange rate is set to 1.00. Please update it manually.";
						$this->db->query("UPDATE ".DB_PREFIX."currencies SET exchange_rate='1.0000000000' WHERE currency_id='".intval($cur[$i]["currency_id"])."'");
					}
				}
				else
				{
					$this->errors[] = $cur[$i]["code"]." is not a recognized currency identifier. Exchange rate is set to 1.00. Please update it manually.";
					$this->db->query("UPDATE ".DB_PREFIX."currencies SET exchange_rate='1.0000000000' WHERE currency_id='".intval($cur[$i]["currency_id"])."'");
				}
			}
		}
		else
		{
			$this->errors[] = $def["code"]." is not a recognized currency identifier. All exchange rates are set to 1.00. Please update them manually.";
			$this->db->reset();
			$this->db->assign("exchange_rate", 1.000000);
			$this->db->update(DB_PREFIX."currencies", "");
		}
	}
}

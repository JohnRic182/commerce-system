<?php

/**
 * Class AdminOrderProvider
 */
class AdminOrderProvider
{
	public static function editOrder($order_data, ORDER $order, &$shipping_error_message)
	{
		global $db, $settings;

		$orderRepository = DataAccess_OrderRepository::getInstance($db, $settings);

		/**
		 * Check are there any changes to be saved
		 */
		$changed =
			round($_REQUEST['new_discount_value'], 5) != PriceHelper::round($order_data['discount_value'], 5) ||
			$_REQUEST['new_discount_type'] != $order_data['discount_type'];

		/**
		 * Update order items prices and quantities
		 */
		$prices = isset($_REQUEST['prices']) ? $_REQUEST['prices'] : array();
		$prices = is_array($prices) ? $prices : array();
		$quantities = isset($_REQUEST['quantities']) ? $_REQUEST['quantities'] : array();
		$quantities = is_array($quantities) ? $quantities : array();

		if (count($prices) > 0)
		{
			$lineItemIds = array_keys($prices);
			$products = $orderRepository->getOrderItemsProducts($order->getId(), $lineItemIds);

			$priceChanges = array();
			foreach ($prices as $ocid => $price)
			{
				if (array_key_exists($ocid, $order->lineItems))
				{
					/* @var Model_LineItem $item */
					$item = $order->lineItems[$ocid];
					$quantity = intval($quantities[$ocid]);

					if ($price != $item->getAdminPrice() || $quantity != $item->getAdminQuantity())
					{
						$changed = true;

						if ($price != $item->getAdminPrice())
						{
							$priceChanges[$ocid] = $price;
						}

						$item->setAdminQuantity($quantity);

						$orderRepository->persistLineItem($order, $item, false);
					}
				}
			}
		}

		/**
		 * Update / modify order
		 */
		if ($changed)
		{
			$discountRepository = new DataAccess_DiscountRepository($db);

			// Override discount calculator
			$adminDiscount = new Calculator_AdminDiscount($settings, $orderRepository, $discountRepository);
			Calculator_Discount::setInstance($adminDiscount);

			/**
			 * Check discounts
			 */
			$discount_type = isset($_REQUEST['new_discount_type']) ? trim($_REQUEST['new_discount_type']) : 'none';
			$discount_value = isset($_REQUEST['new_discount_value']) ? $_REQUEST['new_discount_value'] : '0';
			$discount_value = $discount_type == "percent" ? $discount_value : round($discount_value, 2);

			if ($discount_value <= 0)
			{
				$discount_type = 'none';
				$discount_value = 0;
			}

			$order->setDiscountType($discount_type);
			$order->setDiscountValue($discount_value);

			$event = new Events_OrderEvent(Events_OrderEvent::ON_UPDATE_ITEMS);
			$event->setOrder($order);

			Events_EventHandler::handle($event);

			foreach ($priceChanges as $ocid => $price)
			{
				if (!isset($order->lineItems[$ocid])) continue;

				/* @var Model_LineItem $item */
				$item = $order->lineItems[$ocid];

				$priceBeforeChange = $item->getPrice();
				$finalPrice = $price;
				$priceWithTax = $price;

				/* @var Model_Product $product */
				$product = isset($products[$item->getPid()]) ? $products[$item->getPid()] : null;
				if ($product)
				{
					$product->setFinalPrice(PriceHelper::round($price));
					$product->setTaxValues();

					$finalPrice = $product->getFinalPrice();
					$priceWithTax = $product->getPriceWithTax();
				}

				$item->setFinalPrice($finalPrice);
				$item->setPriceWithTax($priceWithTax);
				$item->setPrice($priceBeforeChange);

				$orderRepository->persistLineItem($order, $item, false);
			}

			$shipping_error_message = '';
			$order->recalcEverything($shipping_error_message, false);
		}
	}
}
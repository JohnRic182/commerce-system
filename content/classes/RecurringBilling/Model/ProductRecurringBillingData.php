<?php
/**
 * Class RecurringBilling_Model_ProductRecurringBillingData
 */
class RecurringBilling_Model_ProductRecurringBillingData
{
	const UNIT_DAY = 'Day';
	const UNIT_WEEK = 'Week';
	const UNIT_MONTH = 'Month';
	const UNIT_YEAR = 'Year';

	protected $billingCustom = false;
	protected $billingPeriodUnit = self::UNIT_MONTH;
	protected $billingPeriodFrequency = 1;
	protected $billingPeriodCycles = 0;
	protected $billingAmount = 0;

	protected $trialEnabled = false;
	protected $trialCustom = false;
	protected $trialPeriodUnit = self::UNIT_MONTH;
	protected $trialPeriodFrequency = 1;
	protected $trialPeriodCycles = 1;
	protected $trialAmount = 0;

	protected $startDateDelayUnit = '';
	protected $startDateDelay = 0;

	protected $customerCanDefineStartDate = false;
	protected $maxStartDateDelay = 0;

	protected $scheduleDescription = '';
	protected $maxPaymentFailures = 3;

	protected $autoCompleteInitialOrder = false;
	protected $autoCompleteRecurringOrders = false;

	protected $customerCanSuspend = false;
	protected $customerCanCancel = false;

	protected $adminCanAdjustBilling = true;
	protected $adminCanAdjustTrial = true;

	protected $customerNotifications = array('created', 'failure', 'expire', 'one_remaining', 'suspended', 'reactivated', 'canceled', 'completed');
	protected $adminNotifications = array('created', 'failure', 'expire', 'one_remaining', 'suspended', 'reactivated', 'canceled', 'completed');

	/**
	 * Class constructor
	 *
	 * @param array|null $data
	 */
	public function __construct($data = null)
	{
		if (!is_null($data) && is_array($data))
		{
			$this->hydrateFromArray($data);
		}
	}

	/**
	 * Hydrate product recurring data from an array
	 *
	 * @param array $data
	 */
	public function hydrateFromArray($data)
	{
		if (!is_array($data) || count($data) == 0) return;

		$this->setBillingCustom(isset($data['billing_custom']) && $data['billing_custom'] == '1');
		if (isset($data['billing_period_unit'])) $this->setBillingPeriodUnit($data['billing_period_unit']);
		if (isset($data['billing_period_frequency'])) $this->setBillingPeriodFrequency(intval($data['billing_period_frequency']));
		if (isset($data['billing_period_cycles'])) $this->setBillingPeriodCycles(intval($data['billing_period_cycles']));
		if (isset($data['billing_amount'])) $this->setBillingAmount(floatval($data['billing_amount']));

		$this->setTrialEnabled(isset($data['trial_enabled']) && $data['trial_enabled'] == '1');
		$this->setTrialCustom(isset($data['trial_custom']) && $data['trial_custom'] == '1');
		if (isset($data['trial_period_unit'])) $this->setTrialPeriodUnit($data['trial_period_unit']);
		if (isset($data['trial_period_frequency'])) $this->setTrialPeriodFrequency(intval($data['trial_period_frequency']));
		if (isset($data['trial_period_cycles'])) $this->setTrialPeriodCycles(intval($data['trial_period_cycles']));
		if (isset($data['trial_amount'])) $this->setTrialAmount(floatval($data['trial_amount']));

		if (isset($data['start_date_delay_unit'])) $this->setStartDateDelayUnit($data['start_date_delay_unit']);
		if (isset($data['start_date_delay'])) $this->setStartDateDelay(intval($data['start_date_delay']));

		if (isset($data['customer_can_define_start_date'])) $this->setCustomerCanDefineStartDate(isset($data['customer_can_define_start_date']) && $data['customer_can_define_start_date'] == '1');
		if (isset($data['max_start_date_delay'])) $this->setMaxStartDateDelay(intval($data['max_start_date_delay']));
		if (isset($data['schedule_description'])) $this->setScheduleDescription($data['schedule_description']);
		if (isset($data['max_payment_failures'])) $this->setMaxPaymentFailures(intval($data['max_payment_failures']));

		$this->setAutoCompleteInitialOrder(isset($data['auto_complete_initial_order']) && $data['auto_complete_initial_order'] == '1');
		$this->setAutoCompleteRecurringOrders(isset($data['auto_complete_recurring_orders']) && $data['auto_complete_recurring_orders'] == '1');

		$this->setCustomerCanSuspend(isset($data['customer_can_suspend']) && $data['customer_can_suspend'] == '1');
		$this->setCustomerCanCancel(isset($data['customer_can_cancel']) && $data['customer_can_cancel'] == '1');

		$this->setAdminCanAdjustBilling(isset($data['admin_can_adjust_billing']) && $data['admin_can_adjust_billing'] == '1');
		$this->setAdminCanAdjustTrial(isset($data['admin_can_adjust_trial']) && $data['admin_can_adjust_trial'] == '1');

		if (isset($data['customer_notifications'])) $this->setCustomerNotifications(is_array($data['customer_notifications']) ? $data['customer_notifications'] : explode(',', $data['customer_notifications']));
		if (isset($data['admin_notifications'])) $this->setAdminNotifications(is_array($data['admin_notifications']) ? $data['admin_notifications'] : explode(',', $data['admin_notifications']));
	}

	/**
	 * Hydrate product recurring data from a form
	 *
	 * @param $data
	 */
	public function hydrateFromForm($data)
	{
		/**
		 * Transform into array data
		 */
		$autoCompleteOptions = isset($data['auto_complete_options']) && is_array($data['auto_complete_options']) ? $data['auto_complete_options'] : array();
		$data['auto_complete_initial_order'] = isset($autoCompleteOptions['auto_complete_initial_order']) || in_array('auto_complete_initial_order', $autoCompleteOptions) ? '1' : '0';
		$data['auto_complete_recurring_orders'] = isset($autoCompleteOptions['auto_complete_recurring_orders']) || in_array('auto_complete_recurring_orders', $autoCompleteOptions) ? '1' : '0';
		unset($data['auto_complete_options']);

		$modificationRules = isset($data['modification_rules']) && is_array($data['modification_rules']) ? $data['modification_rules'] : array();
		$data['customer_can_suspend'] = isset($modificationRules['customer_can_suspend']) || in_array('customer_can_suspend', $modificationRules) ? '1' : '0';
		$data['customer_can_cancel'] = isset($modificationRules['customer_can_cancel']) || in_array('customer_can_cancel', $modificationRules) ? '1' : '0';
		$data['admin_can_adjust_billing'] = isset($modificationRules['admin_can_adjust_billing']) || in_array('admin_can_adjust_billing', $modificationRules) ? '1' : '0';
		$data['admin_can_adjust_trial'] = isset($modificationRules['admin_can_adjust_trial']) || in_array('admin_can_adjust_trial', $modificationRules) ? '1' : '0';
		unset($data['modification_rules']);

		$data['billing_custom'] = isset($data['billing']['custom']);
		if (isset($data['billing']['amount'])) $data['billing_amount'] = $data['billing']['amount'];
		if (isset($data['billing']['period_unit'])) $data['billing_period_unit'] = $data['billing']['period_unit'];
		if (isset($data['billing']['period_frequency'])) $data['billing_period_frequency'] = $data['billing']['period_frequency'];
		if (isset($data['billing']['period_cycles'])) $data['billing_period_cycles'] = $data['billing']['period_cycles'];
		unset($data['billing']);

		$data['trial_custom'] = isset($data['trial']['custom']);
		if (isset($data['trial']['amount'])) $data['trial_amount'] = $data['trial']['amount'];
		if (isset($data['trial']['period_unit'])) $data['trial_period_unit'] = $data['trial']['period_unit'];
		if (isset($data['trial']['period_frequency'])) $data['trial_period_frequency'] = $data['trial']['period_frequency'];
		if (isset($data['trial']['period_cycles'])) $data['trial_period_cycles'] = $data['trial']['period_cycles'];
		unset($data['trial']);

		// note: unit comes first as array will become scalar. this is why there no unset used
		if (isset($data['start_date_delay']['delay_unit'])) $data['start_date_delay_unit'] = $data['start_date_delay']['delay_unit'];
		if (isset($data['start_date_delay']['delay'])) $data['start_date_delay'] = $data['start_date_delay']['delay'];

		$this->hydrateFromArray($data);
	}

	/**
	 * Save as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		return array(
			'billing_custom' => $this->getBillingCustom() ? 1 : 0,
			'billing_period_unit' => $this->getBillingPeriodUnit(),
			'billing_period_frequency' => $this->getBillingPeriodFrequency(),
			'billing_period_cycles' => $this->getBillingPeriodCycles(),
			'billing_amount' => $this->getBillingAmount(),

			'trial_enabled' => $this->getTrialEnabled() ? 1 : 0,
			'trial_custom' => $this->getTrialCustom() ? 1 : 0,
			'trial_period_unit' => $this->getTrialPeriodUnit(),
			'trial_period_frequency' => $this->getTrialPeriodFrequency(),
			'trial_period_cycles' => $this->getTrialPeriodCycles(),
			'trial_amount' => $this->getTrialAmount(),

			'start_date_delay_unit' => $this->getStartDateDelayUnit(),
			'start_date_delay' => $this->getStartDateDelay(),

			'customer_can_define_start_date' => $this->getCustomerCanDefineStartDate() ? 1 : 0,
			'max_start_date_delay' => $this->getMaxStartDateDelay(),
			'schedule_description' => $this->getScheduleDescription(),
			'max_payment_failures' => $this->getMaxPaymentFailures(),

			'auto_complete_initial_order' => $this->getAutoCompleteInitialOrder() ? 1 : 0,
			'auto_complete_recurring_orders' => $this->getAutoCompleteRecurringOrders() ? 1 : 0,

			'customer_can_suspend' => $this->getCustomerCanSuspend() ? 1 : 0,
			'customer_can_cancel' => $this->getCustomerCanCancel() ? 1 : 0,

			'admin_can_adjust_billing' => $this->getAdminCanAdjustBilling() ? 1 : 0,
			'admin_can_adjust_trial' => $this->getAdminCanAdjustTrial() ? 1 : 0,

			'customer_notifications' => $this->getCustomerNotifications(),
			'admin_notifications' => $this->getAdminNotifications()
		);
	}

	/**
	 * Set admin can adjust billing
	 *
	 * @param boolean $adminCanAdjustBilling
	 */
	public function setAdminCanAdjustBilling($adminCanAdjustBilling)
	{
		$this->adminCanAdjustBilling = $adminCanAdjustBilling;
	}

	/**
	 * Get admin can adjust billing
	 *
	 * @return boolean
	 */
	public function getAdminCanAdjustBilling()
	{
		return $this->adminCanAdjustBilling;
	}

	/**
	 * @param boolean $adminCanAdjustTrial
	 */
	public function setAdminCanAdjustTrial($adminCanAdjustTrial)
	{
		$this->adminCanAdjustTrial = $adminCanAdjustTrial;
	}

	/**
	 * @return boolean
	 */
	public function getAdminCanAdjustTrial()
	{
		return $this->adminCanAdjustTrial;
	}

	/**
	 * @param mixed $adminNotifications
	 */
	public function setAdminNotifications($adminNotifications)
	{
		$this->adminNotifications = is_array($adminNotifications) ? $adminNotifications : explode(',', $adminNotifications);;
	}

	/**
	 * @return mixed
	 */
	public function getAdminNotifications()
	{
		return $this->adminNotifications;
	}

	/**
	 * @param mixed $autoCompleteInitialOrder
	 */
	public function setAutoCompleteInitialOrder($autoCompleteInitialOrder)
	{
		$this->autoCompleteInitialOrder = $autoCompleteInitialOrder;
	}

	/**
	 * @return mixed
	 */
	public function getAutoCompleteInitialOrder()
	{
		return $this->autoCompleteInitialOrder;
	}

	/**
	 * @param mixed $autoCompleteRecurringOrders
	 */
	public function setAutoCompleteRecurringOrders($autoCompleteRecurringOrders)
	{
		$this->autoCompleteRecurringOrders = $autoCompleteRecurringOrders;
	}

	/**
	 * @return mixed
	 */
	public function getAutoCompleteRecurringOrders()
	{
		return $this->autoCompleteRecurringOrders;
	}

	/**
	 * Set billing custom
	 *
	 * @param bool $billingCustom
	 */
	public function setBillingCustom($billingCustom)
	{
		$this->billingCustom = $billingCustom;
	}

	/**
	 * Get billing custom
	 *
	 * @return mixed
	 */
	public function getBillingCustom()
	{
		return $this->billingCustom;
	}


	/**
	 * @param mixed $billingAmount
	 */
	public function setBillingAmount($billingAmount)
	{
		$this->billingAmount = $billingAmount;
	}

	/**
	 * @return mixed
	 */
	public function getBillingAmount()
	{
		return $this->billingAmount;
	}

	/**
	 * @param mixed $billingPeriodCycles
	 */
	public function setBillingPeriodCycles($billingPeriodCycles)
	{
		$this->billingPeriodCycles = $billingPeriodCycles;
	}

	/**
	 * @return mixed
	 */
	public function getBillingPeriodCycles()
	{
		return $this->billingPeriodCycles;
	}

	/**
	 * @param mixed $billingPeriodFrequency
	 */
	public function setBillingPeriodFrequency($billingPeriodFrequency)
	{
		$this->billingPeriodFrequency = $billingPeriodFrequency;
	}

	/**
	 * @return mixed
	 */
	public function getBillingPeriodFrequency()
	{
		return $this->billingPeriodFrequency;
	}

	/**
	 * @param mixed $billingPeriodUnit
	 */
	public function setBillingPeriodUnit($billingPeriodUnit)
	{
		$this->billingPeriodUnit = $billingPeriodUnit;
	}

	/**
	 * @return mixed
	 */
	public function getBillingPeriodUnit()
	{
		return $this->billingPeriodUnit;
	}

	/**
	 * @param mixed $customerCanCancel
	 */
	public function setCustomerCanCancel($customerCanCancel)
	{
		$this->customerCanCancel = $customerCanCancel;
	}

	/**
	 * @return mixed
	 */
	public function getCustomerCanCancel()
	{
		return $this->customerCanCancel;
	}

	/**
	 * @param mixed $customerCanDefineStartDate
	 */
	public function setCustomerCanDefineStartDate($customerCanDefineStartDate)
	{
		$this->customerCanDefineStartDate = $customerCanDefineStartDate;
	}

	/**
	 * @return mixed
	 */
	public function getCustomerCanDefineStartDate()
	{
		return $this->customerCanDefineStartDate;
	}

	/**
	 * @param mixed $customerCanSuspend
	 */
	public function setCustomerCanSuspend($customerCanSuspend)
	{
		$this->customerCanSuspend = $customerCanSuspend;
	}

	/**
	 * @return mixed
	 */
	public function getCustomerCanSuspend()
	{
		return $this->customerCanSuspend;
	}

	/**
	 * @param mixed $customerNotifications
	 */
	public function setCustomerNotifications($customerNotifications)
	{
		$this->customerNotifications = is_array($customerNotifications) ? $customerNotifications : explode(',', $customerNotifications);
	}

	/**
	 * @return mixed
	 */
	public function getCustomerNotifications()
	{
		return $this->customerNotifications;
	}

	/**
	 * @param mixed $maxPaymentFailures
	 */
	public function setMaxPaymentFailures($maxPaymentFailures)
	{
		$this->maxPaymentFailures = $maxPaymentFailures;
	}

	/**
	 * @return mixed
	 */
	public function getMaxPaymentFailures()
	{
		return $this->maxPaymentFailures;
	}

	/**
	 * @param mixed $startDateDelayUnit
	 */
	public function setStartDateDelayUnit($startDateDelayUnit)
	{
		$this->startDateDelayUnit = $startDateDelayUnit;
	}

	/**
	 * @return mixed
	 */
	public function getStartDateDelayUnit()
	{
		return $this->startDateDelayUnit;
	}

	/**
	 * @param mixed $startDateDelay
	 */
	public function setStartDateDelay($startDateDelay)
	{
		$this->startDateDelay = $startDateDelay;
	}

	/**
	 * @return mixed
	 */
	public function getStartDateDelay()
	{
		return $this->startDateDelay;
	}

	/**
	 * @param mixed $maxStartDateDelay
	 */
	public function setMaxStartDateDelay($maxStartDateDelay)
	{
		$this->maxStartDateDelay = $maxStartDateDelay;
	}

	/**
	 * @return mixed
	 */
	public function getMaxStartDateDelay()
	{
		return $this->maxStartDateDelay;
	}

	/**
	 * @param mixed $scheduleDescription
	 */
	public function setScheduleDescription($scheduleDescription)
	{
		$this->scheduleDescription = $scheduleDescription;
	}

	/**
	 * @return mixed
	 */
	public function getScheduleDescription()
	{
		return $this->scheduleDescription;
	}

	/**
	 * Set trial enabled
	 *
	 * @param bool $trialEnabled
	 */
	public function setTrialEnabled($trialEnabled)
	{
		$this->trialEnabled = $trialEnabled;
	}

	/**
	 * Get trial enabled
	 *
	 * @return mixed
	 */
	public function getTrialEnabled()
	{
		return $this->trialEnabled;
	}

	/**
	 * Set trial custom
	 *
	 * @param bool $trialCustom
	 */
	public function setTrialCustom($trialCustom)
	{
		$this->trialCustom = $trialCustom;
	}

	/**
	 * Get trial custom
	 *
	 * @return mixed
	 */
	public function getTrialCustom()
	{
		return $this->trialCustom;
	}


	/**
	 * @param mixed $trialAmount
	 */
	public function setTrialAmount($trialAmount)
	{
		$this->trialAmount = $trialAmount;
	}

	/**
	 * @return mixed
	 */
	public function getTrialAmount()
	{
		return $this->trialAmount;
	}

	/**
	 * @param mixed $trialPeriodCycles
	 */
	public function setTrialPeriodCycles($trialPeriodCycles)
	{
		$this->trialPeriodCycles = $trialPeriodCycles;
	}

	/**
	 * @return mixed
	 */
	public function getTrialPeriodCycles()
	{
		return $this->trialPeriodCycles;
	}

	/**
	 * @param mixed $trialPeriodFrequency
	 */
	public function setTrialPeriodFrequency($trialPeriodFrequency)
	{
		$this->trialPeriodFrequency = $trialPeriodFrequency;
	}

	/**
	 * @return mixed
	 */
	public function getTrialPeriodFrequency()
	{
		return $this->trialPeriodFrequency;
	}

	/**
	 * @param mixed $trialPeriodUnit
	 */
	public function setTrialPeriodUnit($trialPeriodUnit)
	{
		$this->trialPeriodUnit = $trialPeriodUnit;
	}

	/**
	 * @return mixed
	 */
	public function getTrialPeriodUnit()
	{
		return $this->trialPeriodUnit;
	}
}
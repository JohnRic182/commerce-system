<?php
/**
 * Class RecurringBilling_Model_RecurringProfile
 */
class RecurringBilling_Model_RecurringProfile
{
	const STATUS_PENDING = 'Pending';
	const STATUS_ACTIVE = 'Active';
	const STATUS_SUSPENDED = 'Suspended';
	const STATUS_CANCELED = 'Canceled';
	const STATUS_COMPLETED = 'Completed';

	const SUSPENSION_REASON_NONE = 'None';
	const SUSPENSION_REASON_USER = 'User';
	const SUSPENSION_REASON_ADMIN = 'Admin';
	const SUSPENSION_REASON_FAILURE = 'Failure';

	const UNIT_DAY = 'Day';
	const UNIT_WEEK = 'Week';
	const UNIT_MONTH = 'Month';
	const UNIT_YEAR = 'Year';

	/** @var int $profileId  */
	protected $profileId;

	/** @var int $userId */
	protected $userId;

	/** @var int $initialOrderId */
	protected $initialOrderId;

	/** @var int $initialOrderLineItemId */
	protected $initialOrderLineItemId = 0;

	/** @var Model_LineItem */
	protected $lineItem = null;

	/** @var float $initialShippingAmount */
	protected $initialShippingAmount;

	/** @var float $initialTaxAmount */
	protected $initialTaxAmount;

	/** @var float $initialAmount */
	protected $initialAmount;

	/** @var int $paymentProfileId */
	protected $paymentProfileId;

	/** @var int $paymentMethodId */
	protected $paymentMethodId;

	/** @var string $status */
	protected $status = self::STATUS_PENDING;

	/** @var string $suspensionReason */
	protected $suspensionReason = self::SUSPENSION_REASON_NONE;

	/** @var string $suspensionReasonDescription */
	protected $suspensionReasonDescription = '';

	/** @var DateTime $dateCreated */
	protected $dateCreated;

	/** @var DateTime $dateUpdated */
	protected $dateUpdated;

	/** @var DateTime $dateStart */
	protected $dateStart;

	/** @var DateTime $dateStart */
	protected $dateNextBilling;

	/** @var int $itemsQuantity */
	protected $itemsQuantity;

	/** @var string $billingPeriodUnit */
	protected $billingPeriodUnit;

	/** @var int $billingPeriodFrequency */
	protected $billingPeriodFrequency;

	/** @var int $billingPeriodCycles */
	protected $billingPeriodCycles;

	/** @var float $billingAmount */
	protected $billingAmount;

	/** @var bool $trialEnabled  */
	protected $trialEnabled = false;

	/** @var string $trialPeriodUnit */
	protected $trialPeriodUnit;

	/** @var int $trialPeriodFrequency */
	protected $trialPeriodFrequency;

	/** @var int trialPeriodCycles */
	protected $trialPeriodCycles;

	/** @var float $trialAmount */
	protected $trialAmount;

	/** @var int $billingSequenceNumber */
	protected $billingSequenceNumber = 0;

	/** @var int $trialSequenceNumber */
	protected $trialSequenceNumber = 0;

	/** @var Model_Address $shippingAddress */
	protected $shippingAddress;

	protected $additionalInfo;

	/** @var bool $notifiedCCExpire */
	protected $notifiedCCExpire = false;

	/** @var string $productName */
	protected $productName = '';

	/** @var string $subscriberName */
	protected $subscriberName;

	//TODO: Add date_last_billing


	/**
	 * Class constructor
	 *
	 * @param array data
	 */
	public function __construct($data = null)
	{
		if (is_array($data) && count($data))
		{
			$this->hydrateFromArray($data);
		}

		if ($this->getTrialSequenceNumber() < 0) $this->setTrialSequenceNumber(0);
		if ($this->getBillingSequenceNumber() < 0) $this->setBillingSequenceNumber(0);
	}

	/**
	 * Hydrate from an array
	 *
	 * @param $data
	 */
	public function hydrateFromArray($data)
	{
		$this->setProfileId($data['recurring_profile_id']);
		$this->setUserId($data['user_id']);

		$this->setInitialOrderId($data['initial_order_id']);
		$this->setInitialOrderLineItemId($data['initial_order_line_item_id']);
		$this->setInitialShippingAmount($data['initial_shipping_amount']);
		$this->setInitialTaxAmount($data['initial_tax_amount']);
		$this->setInitialAmount($data['initial_amount']);

		$this->setPaymentProfileId($data['payment_profile_id']);
		$this->setPaymentMethodId($data['payment_method_id']);

		$this->setStatus($data['status']);
		$this->setSuspensionReason($data['suspension_reason']);
		$this->setSuspensionReasonDescription($data['suspension_reason_description']);

		$this->setDateCreated(new DateTime($data['date_created']));
		$this->setDateUpdated(new DateTime($data['date_updated']));

		if ($data['date_start'] != '0000-00-00')
		{
			$this->setDateStart(new DateTime($data['date_start']));
		}
		else
		{
			$this->setDateStart(null);
		}
		$this->setDateNextBilling(new DateTime($data['date_next_billing']));

		$this->setBillingPeriodUnit($data['billing_period_unit']);
		$this->setBillingPeriodFrequency($data['billing_period_frequency']);
		$this->setBillingPeriodCycles($data['billing_period_cycles']);
		$this->setBillingAmount($data['billing_amount']);

		$this->setTrialEnabled($data['trial_enabled'] == 1);
		$this->setTrialPeriodUnit($data['trial_period_unit']);
		$this->setTrialPeriodFrequency($data['trial_period_frequency']);
		$this->setTrialPeriodCycles($data['trial_period_cycles']);
		$this->setTrialAmount($data['trial_amount']);

		$this->setItemsQuantity($data['items_quantity']);

		$this->setBillingSequenceNumber($data['billing_sequence_number']);
		$this->setTrialSequenceNumber($data['trial_sequence_number']);

		$this->setShippingAddress($data['shipping_address_info'] != '' ? new Model_Address(@unserialize($data['shipping_address_info'])) : null);

		$this->setNotifiedCCExpire($data['notified_cc_expire'] == 1);

		if (isset($data['product_name'])) $this->setProductName($data['product_name']);
		if (isset($data['subscriber_name'])) $this->setSubscriberName($data['subscriber_name']);
	}

	/**
	 * Hydrate from form
	 *
	 * @param $data
	 */
	public function hydrateFromForm($data)
	{
		if (isset($data['next_billing_date']) && is_array($data['next_billing_date']))
		{
			$nextBillingDate = $data['next_billing_date']['year'].'-'.$data['next_billing_date']['month'].'-'.$data['next_billing_date']['day'];
			$this->setDateNextBilling(new DateTime($nextBillingDate));
		}

		if (isset($data['trial_enabled'])) $this->setTrialEnabled($data['trial_enabled'] == '1');
		if (isset($data['trial_sequence_number'])) $this->setTrialSequenceNumber($data['trial_sequence_number']);
		if (isset($data['trial_period_cycles'])) $this->setTrialPeriodCycles($data['trial_period_cycles']);
		if (isset($data['trial_amount'])) $this->setTrialAmount($data['trial_amount']);
		if (isset($data['billing_sequence_number'])) $this->setBillingSequenceNumber($data['billing_sequence_number']);
		if (isset($data['billing_period_cycles'])) $this->setBillingPeriodCycles($data['billing_period_cycles']);
		if (isset($data['billing_amount'])) $this->setBillingAmount($data['billing_amount']);
		if (isset($data['items_quantity'])) $this->setItemsQuantity($data['items_quantity']);
	}

	/**
	 * Create from line item
	 *
	 * @param ORDER $order
	 * @param Model_LineItem $lineItem
	 *
	 * @return RecurringBilling_Model_RecurringProfile|null
	 */
	public static function createFromLineItem(ORDER $order, Model_LineItem $lineItem)
	{
		if ($lineItem->getEnableRecurringBilling())
		{
			/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
			$recurringBillingData = $lineItem->getRecurringBillingData();

			/** @var RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData */
			$productRecurringBillingData = $lineItem->getProductRecurringBillingData();

			/** USER $user */
			$user = $order->getUser();

			/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
			$recurringProfile = new self();

			$recurringProfile->setUserId($user->getId());

			$recurringProfile->setInitialOrderId($order->getId());
			$recurringProfile->setInitialOrderLineItemId($lineItem->getId());

			$recurringProfile->setInitialShippingAmount($order->getShippingAmount());
			$recurringProfile->setInitialTaxAmount($order->getTaxAmount());
			$recurringProfile->setInitialAmount($lineItem->getSubtotal());

			$recurringProfile->setPaymentProfileId($recurringBillingData->getPaymentProfileId());
			$recurringProfile->setPaymentMethodId($order->getPaymentMethodId());

			$recurringProfile->setStatus(self::STATUS_PENDING);
			$recurringProfile->setSuspensionReason(self::SUSPENSION_REASON_NONE);
			$recurringProfile->setSuspensionReasonDescription('');

			$recurringProfile->setDateCreated(new DateTime());
			$recurringProfile->setDateUpdated(new DateTime());

			$recurringProfile->setDateStart($recurringBillingData->getStartDate());

			// calculate next payment date
			$nextBillingDate = $recurringBillingData->getStartDate() != null ? $recurringBillingData->getStartDate() : new DateTime();

			$startDateDelayUnits = array(
				RecurringBilling_Model_RecurringProfile::UNIT_DAY => 'days',
				RecurringBilling_Model_RecurringProfile::UNIT_WEEK => 'weeks',
				RecurringBilling_Model_RecurringProfile::UNIT_MONTH => 'months',
				RecurringBilling_Model_RecurringProfile::UNIT_YEAR => 'years',
			);

			$nextBillingDate->modify('+'.$productRecurringBillingData->getStartDateDelay().' '.$startDateDelayUnits[$productRecurringBillingData->getStartDateDelayUnit()]);
			$recurringProfile->setDateNextBilling($nextBillingDate);

			$recurringProfile->setBillingPeriodUnit($recurringBillingData->getBillingPeriodUnit());
			$recurringProfile->setBillingPeriodFrequency($recurringBillingData->getBillingPeriodFrequency());
			$recurringProfile->setBillingPeriodCycles($recurringBillingData->getBillingPeriodCycles());
			$recurringProfile->setBillingAmount($recurringBillingData->getBillingAmount());

			$recurringProfile->setTrialEnabled($recurringBillingData->getTrialEnabled());
			$recurringProfile->setTrialPeriodUnit($recurringBillingData->getTrialPeriodUnit());
			$recurringProfile->setTrialPeriodFrequency($recurringBillingData->getTrialPeriodFrequency());
			$recurringProfile->setTrialPeriodCycles($recurringBillingData->getTrialPeriodCycles());
			$recurringProfile->setTrialAmount($recurringBillingData->getTrialAmount());

			$recurringProfile->setItemsQuantity($lineItem->getAdminQuantity());

			if ($lineItem->getProductType() == Model_Product::TANGIBLE)
			{
				$recurringProfile->setShippingAddress(new Model_Address($order->getShippingAddress()));
			}

			return $recurringProfile;
		}

		return null;
	}

	/**
	 * Set line item
	 *
	 * @param Model_LineItem $lineItem
	 */
	public function setLineItem(Model_LineItem $lineItem)
	{
		$this->lineItem = $lineItem;
	}

	/**
	 * Get line item
	 *
	 * @return Model_LineItem
	 */
	public function getLineItem()
	{
		if (is_null($this->lineItem))
		{
			$orderRepository = DataAccess_OrderRepository::getInstance();
			$lineItemData = $orderRepository->getLineItemById($this->getInitialOrderLineItemId());
			$this->lineItem = new Model_LineItem($lineItemData);
		}

		return $this->lineItem;
	}

	/**
	 * Set additional information
	 *
	 * @param mixed $additionalInfo
	 */
	public function setAdditionalInfo($additionalInfo)
	{
		$this->additionalInfo = $additionalInfo;
	}

	/**
	 * Get additional information
	 *
	 * @return mixed
	 */
	public function getAdditionalInfo()
	{
		return $this->additionalInfo;
	}

	/**
	 * Set billing amount
	 *
	 * @param float $billingAmount
	 */
	public function setBillingAmount($billingAmount)
	{
		$this->billingAmount = $billingAmount;
	}

	/**
	 * Get billing amount
	 *
	 * @return float
	 */
	public function getBillingAmount()
	{
		return $this->billingAmount;
	}

	/**
	 * Set date created
	 *
	 * @param DateTime $dateCreated
	 */
	public function setDateCreated($dateCreated)
	{
		$this->dateCreated = $dateCreated;
	}

	/**
	 * Get date created
	 *
	 * @return DateTime
	 */
	public function getDateCreated()
	{
		return $this->dateCreated;
	}

	/**
	 * Set next billing date
	 *
	 * @param DateTime $dateNextBilling
	 */
	public function setDateNextBilling($dateNextBilling)
	{
		$this->dateNextBilling = $dateNextBilling;
	}

	/**
	 * Get next billing date
	 *
	 * @return DateTime
	 */
	public function getDateNextBilling()
	{
		return $this->dateNextBilling;
	}

	/**
	 * Set start date
	 *
	 * @param DateTime|null $dateStart
	 */
	public function setDateStart($dateStart = null)
	{
		$this->dateStart = $dateStart;
	}

	/**
	 * Get start date
	 *
	 * @return DateTime|null
	 */
	public function getDateStart()
	{
		return $this->dateStart;
	}

	/**
	 * Set date updated
	 *
	 * @param DateTime $dateUpdated
	 */
	public function setDateUpdated(DateTime $dateUpdated)
	{
		$this->dateUpdated = $dateUpdated;
	}

	/**
	 * Get date updated
	 *
	 * @return DateTime
	 */
	public function getDateUpdated()
	{
		return $this->dateUpdated;
	}

	/**
	 * Set initial amount
	 *
	 * @param float $initialAmount
	 */
	public function setInitialAmount($initialAmount)
	{
		$this->initialAmount = $initialAmount;
	}

	/**
	 * Get initial amount
	 *
	 * @return float
	 */
	public function getInitialAmount()
	{
		return $this->initialAmount;
	}

	/**
	 * Set initial order id
	 *
	 * @param int $initialOrderId
	 */
	public function setInitialOrderId($initialOrderId)
	{
		$this->initialOrderId = $initialOrderId;
	}

	/**
	 * Get initial order id
	 *
	 * @return int
	 */
	public function getInitialOrderId()
	{
		return $this->initialOrderId;
	}

	/**
	 * Set initial line item id
	 *
	 * @param int $initialOrderLineItemId
	 */
	public function setInitialOrderLineItemId($initialOrderLineItemId)
	{
		$this->initialOrderLineItemId = $initialOrderLineItemId;
	}

	/**
	 * Get initial line item id
	 *
	 * @return int
	 */
	public function getInitialOrderLineItemId()
	{
		return $this->initialOrderLineItemId;
	}

	/**
	 * Set payment profile id
	 *
	 * @param int $paymentProfileId
	 */
	public function setPaymentProfileId($paymentProfileId)
	{
		$this->paymentProfileId = $paymentProfileId;
	}

	/**
	 * Get payment profile id
	 *
	 * @return int
	 */
	public function getPaymentProfileId()
	{
		return $this->paymentProfileId;
	}

	/**
	 * Set initial shipping amount
	 *
	 * @param float $initialShippingAmount
	 */
	public function setInitialShippingAmount($initialShippingAmount)
	{
		$this->initialShippingAmount = $initialShippingAmount;
	}

	/**
	 * Get initial shipping amount
	 *
	 * @return float
	 */
	public function getInitialShippingAmount()
	{
		return $this->initialShippingAmount;
	}

	/**
	 * Set initial tax amount
	 *
	 * @param float $initialTaxAmount
	 */
	public function setInitialTaxAmount($initialTaxAmount)
	{
		$this->initialTaxAmount = $initialTaxAmount;
	}

	/**
	 * Get initial tax amount
	 *
	 * @return float
	 */
	public function getInitialTaxAmount()
	{
		return $this->initialTaxAmount;
	}

	/**
	 * Set payment method id
	 *
	 * @param int $paymentMethodId
	 */
	public function setPaymentMethodId($paymentMethodId)
	{
		$this->paymentMethodId = $paymentMethodId;
	}

	/**
	 * Get payment method id
	 *
	 * @return int
	 */
	public function getPaymentMethodId()
	{
		return $this->paymentMethodId;
	}

	/**
	 * Set billing period frequency
	 *
	 * @param int $billingPeriodFrequency
	 */
	public function setBillingPeriodFrequency($billingPeriodFrequency)
	{
		$this->billingPeriodFrequency = $billingPeriodFrequency;
	}

	/**
	 * Get billing period frequency
	 *
	 * @return int
	 */
	public function getBillingPeriodFrequency()
	{
		return $this->billingPeriodFrequency;
	}

	/**
	 * Set billing period cycles
	 *
	 * @param int $billingPeriodCycles
	 */
	public function setBillingPeriodCycles($billingPeriodCycles)
	{
		$this->billingPeriodCycles= $billingPeriodCycles;
	}

	/**
	 * Get billing period cycles
	 *
	 * @return int
	 */
	public function getBillingPeriodCycles()
	{
		return $this->billingPeriodCycles;
	}

	/**
	 * Set billing period unit
	 *
	 * @param string $billingPeriodUnit
	 */
	public function setBillingPeriodUnit($billingPeriodUnit)
	{
		$this->billingPeriodUnit = $billingPeriodUnit;
	}

	/**
	 * Get billing period unit
	 *
	 * @return string
	 */
	public function getBillingPeriodUnit()
	{
		return $this->billingPeriodUnit;
	}

	/**
	 * @param mixed $profileId
	 */
	public function setProfileId($profileId)
	{
		$this->profileId = $profileId;
	}

	/**
	 * @return mixed
	 */
	public function getProfileId()
	{
		return $this->profileId;
	}

	/**
	 * Set shipping address
	 *
	 * @param Model_Address $shippingAddress
	 */
	public function setShippingAddress(Model_Address $shippingAddress = null)
	{
		$this->shippingAddress = $shippingAddress;
	}

	/**
	 * Get shipping address
	 *
	 * @return Model_Address|null
	 */
	public function getShippingAddress()
	{
		return $this->shippingAddress;
	}

	/**
	 * Set status
	 *
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * Get status
	 *
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Set suspension reason
	 *
	 * @param string $suspensionReason
	 */
	public function setSuspensionReason($suspensionReason)
	{
		$this->suspensionReason = $suspensionReason;
	}

	/**
	 * Get suspension reason
	 *
	 * @return string
	 */
	public function getSuspensionReason()
	{
		return $this->suspensionReason;
	}

	/**
	 * Set suspension reason description
	 *
	 * @param string $suspensionReasonDescription
	 */
	public function setSuspensionReasonDescription($suspensionReasonDescription)
	{
		$this->suspensionReasonDescription = $suspensionReasonDescription;
	}

	/**
	 * Get suspension reason description
	 *
	 * @return string
	 */
	public function getSuspensionReasonDescription()
	{
		return $this->suspensionReasonDescription;
	}

	/**
	 * Set trial amount
	 *
	 * @param float $trialAmount
	 */
	public function setTrialAmount($trialAmount)
	{
		$this->trialAmount = $trialAmount;
	}

	/**
	 * Get trial amount
	 *
	 * @return float
	 */
	public function getTrialAmount()
	{
		return $this->trialAmount;
	}

	/**
	 * Set items quantity
	 *
	 * @param $itemsQuantity
	 */
	public function setItemsQuantity($itemsQuantity)
	{
		$this->itemsQuantity = $itemsQuantity;
	}

	/**
	 * Get items quantity
	 *
	 * @return mixed
	 */
	public function getItemsQuantity()
	{
		return $this->itemsQuantity;
	}

	/**
	 * Set trial enabled
	 *
	 * @param bool $trialEnabled
	 */
	public function setTrialEnabled($trialEnabled)
	{
		$this->trialEnabled = $trialEnabled;
	}

	/**
	 * Get trial enabled
	 *
	 * @return mixed
	 */
	public function getTrialEnabled()
	{
		return $this->trialEnabled;
	}

	/**
	 * Set trial period frequency
	 *
	 * @param int $trialPeriodFrequency
	 */
	public function setTrialPeriodFrequency($trialPeriodFrequency)
	{
		$this->trialPeriodFrequency = $trialPeriodFrequency;
	}

	/**
	 * Get trial period frequency
	 *
	 * @return int
	 */
	public function getTrialPeriodFrequency()
	{
		return $this->trialPeriodFrequency;
	}

	/**
	 * Set trial period cycles
	 *
	 * @param int $trialPeriodCycles
	 */
	public function setTrialPeriodCycles($trialPeriodCycles)
	{
		$this->trialPeriodCycles = $trialPeriodCycles;
	}

	/**
	 * Get trial period cycles
	 *
	 * @return int
	 */
	public function getTrialPeriodCycles()
	{
		return $this->trialPeriodCycles;
	}

	/**
	 * Set trial period unit
	 *
	 * @param string $trialPeriodUnit
	 */
	public function setTrialPeriodUnit($trialPeriodUnit)
	{
		$this->trialPeriodUnit = $trialPeriodUnit;
	}

	/**
	 * Get trial period unit
	 *
	 * @return string
	 */
	public function getTrialPeriodUnit()
	{
		return $this->trialPeriodUnit;
	}

	/**
	 * Set user id
	 *
	 * @param int $userId
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;
	}

	/**
	 * Get user id
	 *
	 * @return int
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * Set billing sequence number
	 *
	 * @param int $billingSequenceNumber
	 */
	public function setBillingSequenceNumber($billingSequenceNumber)
	{
		$this->billingSequenceNumber = $billingSequenceNumber;
	}

	/**
	 * Get billing sequence number
	 *
	 * @return int
	 */
	public function getBillingSequenceNumber()
	{
		return $this->billingSequenceNumber;
	}

	/**
	 * Set trial sequence number
	 *
	 * @param $trialSequenceNumber
	 */
	public function setTrialSequenceNumber($trialSequenceNumber)
	{
		$this->trialSequenceNumber = $trialSequenceNumber;
	}

	/**
	 * Get trial sequence number
	 *
	 * @return int
	 */
	public function getTrialSequenceNumber()
	{
		return $this->trialSequenceNumber;
	}

	/**
	 * @return int
	 */
	public function getTotalBillingCycles()
	{
		if ($this->getTrialEnabled())
		{
			return $this->getTotalBillingCycles() > 0 ? $this->getTrialPeriodCycles() + $this->getTotalBillingCycles() : 0;
		}
		else
		{
			return $this->getTotalBillingCycles() > 0 ? $this->getTotalBillingCycles() : 0;
		}
	}

	/**
	 * Set is cc expiration notification has been sent
	 *
	 * @param boolean $notifiedCCExpire
	 */
	public function setNotifiedCCExpire($notifiedCCExpire)
	{
		$this->notifiedCCExpire = $notifiedCCExpire;
	}

	/**
	 * Get is cc expiration notification has been sent
	 *
	 * @return boolean
	 */
	public function getNotifiedCCExpire()
	{
		return $this->notifiedCCExpire;
	}

	/**
	 * Set product name
	 *
	 * @param string $productName
	 */
	public function setProductName($productName)
	{
		$this->productName = $productName;
	}

	/**
	 * Get product name
	 *
	 * @return string
	 */
	public function getProductName()
	{
		return $this->productName;
	}

	/**
	 * Set subscriber name
	 *
	 * @param string $subscriberName
	 */
	public function setSubscriberName($subscriberName)
	{
		$this->subscriberName = $subscriberName;
	}

	/**
	 * Get subscriber name
	 * @return string
	 */
	public function getSubscriberName()
	{
		return $this->subscriberName;
	}

	/**
	 * Returns whether the recurring profile has more trial billing cycles
	 *
	 * @return bool
	 */
	public function hasMoreTrialCycles()
	{
		return $this->getTrialEnabled() && $this->getTrialSequenceNumber() < $this->getTrialPeriodCycles();
	}

	/**
	 * Returns whether the recurring profile has more billing cycles, either trial or regular
	 *
	 * @return bool
	 */
	public function hasMoreBillingCycles()
	{
		return $this->hasMoreTrialCycles() ||
				$this->getBillingPeriodCycles() == 0 ||
				$this->getBillingSequenceNumber() < $this->getBillingPeriodCycles();
	}

	/**
	 * Check can profile be activated
	 *
	 * @return bool
	 */
	public function canActivate()
	{
		return $this->getStatus() != self::STATUS_COMPLETED && $this->getStatus() != self::STATUS_ACTIVE;
	}

	/**
	 * Check can profile be suspended
	 *
	 * @return bool
	 */
	public function canSuspend()
	{
		return !in_array($this->getStatus(), array(self::STATUS_SUSPENDED, self::STATUS_COMPLETED, self::STATUS_CANCELED));
	}

	/**
	 * Check can profile be canceled
	 *
	 * @return bool
	 */
	public function canCancel()
	{
		return $this->getStatus() != self::STATUS_COMPLETED && $this->getStatus() != self::STATUS_CANCELED;
	}

	/**
	 * Activate recurring billing profile
	 *
	 * @param null $dateNextBilling
	 * @param bool $byUser
	 * @param string|null $reason
	 */
	public function activate($dateNextBilling = null, $byUser = false, $reason = null)
	{
		if (trim($this->getPaymentProfileId()) == '' || $this->getPaymentProfileId() == 0)
		{
			$this->suspend(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_FAILURE, 'Payment profile is empty');
			return;
		}

		if (!$this->hasMoreBillingCycles())
		{
			$this->complete($byUser);
			return;
		}

		if (!is_null($dateNextBilling)) $this->setDateNextBilling($dateNextBilling);

		$oldStatus = $this->getStatus();

		$this->setStatus(self::STATUS_ACTIVE);

		RecurringBilling_Events_RecurringBillingEvent::dispatchStatusChangeEvent($this, $oldStatus, array('byUser' => $byUser, 'reason' => $reason));
	}

	/**
	 * Complete recurring profile
	 *
	 * @param bool $byUser
	 * @param string|null $reason
	 */
	public function complete($byUser = false, $reason = null)
	{
		$oldStatus = $this->getStatus();

		$this->setStatus(self::STATUS_COMPLETED);

		RecurringBilling_Events_RecurringBillingEvent::dispatchStatusChangeEvent($this, $oldStatus, array('byUser' => $byUser, 'reason' => $reason));
	}

	/**
	 * Suspend recurring billing profile
	 *
	 * @param $suspensionReason
	 * @param $suspensionReasonDescription
	 */
	public function suspend($suspensionReason = self::SUSPENSION_REASON_NONE, $suspensionReasonDescription = '')
	{
		$oldStatus = $this->getStatus();

		$this->setStatus(self::STATUS_SUSPENDED);
		$this->setSuspensionReason($suspensionReason);
		$this->setSuspensionReasonDescription($suspensionReasonDescription);

		RecurringBilling_Events_RecurringBillingEvent::dispatchStatusChangeEvent($this, $oldStatus, array('byUser' => $suspensionReason == self::SUSPENSION_REASON_USER, 'reason' => $suspensionReasonDescription));
	}

	/**
	 * Cancel recurring billing profile
	 *
	 * @param bool $byUser
	 * @param string|null $reason
	 */
	public function cancel($byUser = false, $reason = null)
	{
		$oldStatus = $this->getStatus();

		$this->setStatus(self::STATUS_CANCELED);

		RecurringBilling_Events_RecurringBillingEvent::dispatchStatusChangeEvent($this, $oldStatus, array('byUser' => $byUser, 'reason' => $reason));
	}

	/**
	 * Fire credit card about to expire event
	 */
	public function creditCardAboutToExpire()
	{
		RecurringBilling_Events_RecurringBillingEvent::dispatchCreditCardAboutToExpireEvent($this);
	}

	/**
	 * Calculates next billing date
	 *
	 * @throws InvalidArgumentException
	 */
	protected function calculateNextBillingDate($unit, $frequency)
	{
		$nextBillingDate = new DateTime();
		$units = array(self::UNIT_DAY => 'day', self::UNIT_WEEK => 'week', self::UNIT_MONTH => 'month', self::UNIT_YEAR => 'year');
		if (isset($units[$unit]))
		{
			$nextBillingDate->modify('+'.$frequency.$units[$unit].($frequency > 1 ? 's' : ''));
			$this->setDateNextBilling($nextBillingDate);
		}
		else
		{
			throw new InvalidArgumentException($unit.' is not a valid unit');
		}
	}

	/**
	 * Start next billing cycle
	 */
	public function finishCurrentBillingCycle()
	{
		if ($this->getStatus() != self::STATUS_ACTIVE)
		{
			return;
		}

		/**
		 * Recalculate cycles
		 */
		if ($this->hasMoreTrialCycles())
		{
			// set next trial cycle
			$this->setTrialSequenceNumber($this->getTrialSequenceNumber() + 1);
			$this->calculateNextBillingDate($this->getTrialPeriodUnit(), $this->getTrialPeriodFrequency());
			RecurringBilling_Events_RecurringBillingEvent::dispatchFinishBillingCycleEvent($this);
		}
		else
		{
			// check are there more "normal" billing cycles in future
			if ($this->hasMoreBillingCycles())
			{
				$this->setBillingSequenceNumber($this->getBillingSequenceNumber() + 1);
				$this->calculateNextBillingDate($this->getBillingPeriodUnit(), $this->getBillingPeriodFrequency());
				RecurringBilling_Events_RecurringBillingEvent::dispatchFinishBillingCycleEvent($this);
			}

			if (!$this->hasMoreBillingCycles())
			{
				// finish all recurring cycles
				$this->complete();
			}
		}
	}
}
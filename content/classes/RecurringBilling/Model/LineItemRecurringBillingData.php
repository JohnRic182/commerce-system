<?php
/**
 * Class RecurringBilling_Model_LineItemRecurringBillingData
 */
class RecurringBilling_Model_LineItemRecurringBillingData
{
	const UNIT_DAY = 'Day';
	const UNIT_WEEK = 'Week';
	const UNIT_MONTH = 'Month';
	const UNIT_YEAR = 'Year';

	protected $billingPeriodUnit = self::UNIT_MONTH;
	protected $billingPeriodFrequency = 1;
	protected $billingPeriodCycles = 0;
	protected $billingAmount = 0;

	protected $trialEnabled = false;
	protected $trialPeriodUnit = self::UNIT_MONTH;
	protected $trialPeriodFrequency = 1;
	protected $trialPeriodCycles = 0;
	protected $trialAmount = 0;

	/** @var DateTime|null $startDate */
	protected $startDate = null;

	/** @var int $paymentProfileId */
	protected $paymentProfileId = 0;

	/**
	 * Class constructor
	 *
	 * @param array|null $data
	 */
	public function __construct($data = null)
	{
		if (!is_null($data) && is_array($data))
		{
			$this->hydrateFromArray($data);
		}
	}

	/**
	 * Hydrate product recurring data from an array
	 *
	 * @param array $data
	 */
	public function hydrateFromArray($data)
	{
		if (!is_array($data) || count($data) == 0) return;

		if (isset($data['billing_period_unit'])) $this->setBillingPeriodUnit($data['billing_period_unit']);
		if (isset($data['billing_period_frequency'])) $this->setBillingPeriodFrequency(intval($data['billing_period_frequency']));
		if (isset($data['billing_period_cycles'])) $this->setBillingPeriodCycles(intval($data['billing_period_cycles']));
		if (isset($data['billing_amount'])) $this->setBillingAmount(floatval($data['billing_amount']));

		if (isset($data['trial_enabled'])) $this->setTrialEnabled($data['trial_enabled'] == '1');
		if (isset($data['trial_period_unit'])) $this->setTrialPeriodUnit($data['trial_period_unit']);
		if (isset($data['trial_period_frequency'])) $this->setTrialPeriodFrequency(intval($data['trial_period_frequency']));
		if (isset($data['trial_period_cycles'])) $this->setTrialPeriodCycles(intval($data['trial_period_cycles']));
		if (isset($data['trial_amount'])) $this->setTrialAmount(floatval($data['trial_amount']));

		if (isset($data['start_date'])) $this->setStartDate(trim($data['start_date']) != '' ? new DateTime('@'.$data['start_date']) : null );

		if (isset($data['payment_profile_id'])) $this->setPaymentProfileId(intval($data['payment_profile_id']));
	}

	/**
	 * Hydrate product recurring data from a form
	 *
	 * @param RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData
	 */
	public function hydrateFromProductRecurringData(RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData)
	{
		if (!is_null($productRecurringBillingData))
		{
			$this->setBillingAmount($productRecurringBillingData->getBillingAmount());
			$this->setBillingPeriodUnit($productRecurringBillingData->getBillingPeriodUnit());
			$this->setBillingPeriodFrequency($productRecurringBillingData->getBillingPeriodFrequency());
			$this->setBillingPeriodCycles($productRecurringBillingData->getBillingPeriodCycles());

			$this->setTrialEnabled($productRecurringBillingData->getTrialEnabled());
			$this->setTrialAmount($productRecurringBillingData->getTrialAmount());
			$this->setTrialPeriodUnit($productRecurringBillingData->getTrialPeriodUnit());
			$this->setTrialPeriodFrequency($productRecurringBillingData->getTrialPeriodFrequency());
			$this->setTrialPeriodCycles($productRecurringBillingData->getTrialPeriodCycles());
		}
	}

	/**
	 * Save as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		/** @var DateTime|null $startDate */
		$startDate = $this->getStartDate();

		return array(
			'billing_period_unit' => $this->getBillingPeriodUnit(),
			'billing_period_frequency' => $this->getBillingPeriodFrequency(),
			'billing_period_cycles' => $this->getBillingPeriodCycles(),
			'billing_amount' => $this->getBillingAmount(),

			'trial_enabled' => $this->getTrialEnabled() ? 1 : 0,
			'trial_period_unit' => $this->getTrialPeriodUnit(),
			'trial_period_frequency' => $this->getTrialPeriodFrequency(),
			'trial_period_cycles' => $this->getTrialPeriodCycles(),
			'trial_amount' => $this->getTrialAmount(),

			'start_date' => is_null($startDate) ? '' : $startDate->format('U'),
			'payment_profile_id' => $this->getPaymentProfileId()
		);
	}

	/**
	 * Set billing amount
	 *
	 * @param mixed $billingAmount
	 */
	public function setBillingAmount($billingAmount)
	{
		$this->billingAmount = $billingAmount;
	}

	/**
	 * Get billing amount
	 *
	 * @return mixed
	 */
	public function getBillingAmount()
	{
		return $this->billingAmount;
	}

	/**
	 * Set billing period cycles
	 *
	 * @param mixed $billingPeriodCycles
	 */
	public function setBillingPeriodCycles($billingPeriodCycles)
	{
		$this->billingPeriodCycles = $billingPeriodCycles;
	}

	/**
	 * Get billing period cycles
	 *
	 * @return mixed
	 */
	public function getBillingPeriodCycles()
	{
		return $this->billingPeriodCycles;
	}

	/**
	 * Set billing period frequency
	 *
	 * @param mixed $billingPeriodFrequency
	 */
	public function setBillingPeriodFrequency($billingPeriodFrequency)
	{
		$this->billingPeriodFrequency = $billingPeriodFrequency;
	}

	/**
	 * Get billing period frequency
	 *
	 * @return mixed
	 */
	public function getBillingPeriodFrequency()
	{
		return $this->billingPeriodFrequency;
	}

	/**
	 * Set billing period unit
	 *
	 * @param mixed $billingPeriodUnit
	 */
	public function setBillingPeriodUnit($billingPeriodUnit)
	{
		$this->billingPeriodUnit = $billingPeriodUnit;
	}

	/**
	 * Get billing period unit
	 *
	 * @return mixed
	 */
	public function getBillingPeriodUnit()
	{
		return $this->billingPeriodUnit;
	}

	/**
	 * Set start date
	 *
	 * @param DateTime|null $startDate
	 */
	public function setStartDate($startDate)
	{
		$this->startDate = $startDate;
	}

	/**
	 * Get start date
	 *
	 * @return DateTime|null
	 */
	public function getStartDate()
	{
		return $this->startDate;
	}

	/**
	 * Set trial amount
	 *
	 * @param mixed $trialAmount
	 */
	public function setTrialAmount($trialAmount)
	{
		$this->trialAmount = $trialAmount;
	}

	/**
	 * Set trial enabled
	 *
	 * @param bool $trialEnabled
	 */
	public function setTrialEnabled($trialEnabled)
	{
		$this->trialEnabled = $trialEnabled;
	}

	/**
	 * Get trial enabled
	 *
	 * @return mixed
	 */
	public function getTrialEnabled()
	{
		return $this->trialEnabled;
	}

	/**
	 * Get trial amount
	 *
	 * @return mixed
	 */
	public function getTrialAmount()
	{
		return $this->trialAmount;
	}

	/**
	 * Set trial period cycles
	 *
	 * @param mixed $trialPeriodCycles
	 */
	public function setTrialPeriodCycles($trialPeriodCycles)
	{
		$this->trialPeriodCycles = $trialPeriodCycles;
	}

	/**
	 * Get trial period cycles
	 *
	 * @return mixed
	 */
	public function getTrialPeriodCycles()
	{
		return $this->trialPeriodCycles;
	}

	/**
	 * Set trial period frequency
	 *
	 * @param mixed $trialPeriodFrequency
	 */
	public function setTrialPeriodFrequency($trialPeriodFrequency)
	{
		$this->trialPeriodFrequency = $trialPeriodFrequency;
	}

	/**
	 * Get trial period frequency
	 *
	 * @return mixed
	 */
	public function getTrialPeriodFrequency()
	{
		return $this->trialPeriodFrequency;
	}

	/**
	 * Set trial period unit
	 *
	 * @param mixed $trialPeriodUnit
	 */
	public function setTrialPeriodUnit($trialPeriodUnit)
	{
		$this->trialPeriodUnit = $trialPeriodUnit;
	}

	/**
	 * Get trial period unit
	 *
	 * @return mixed
	 */
	public function getTrialPeriodUnit()
	{
		return $this->trialPeriodUnit;
	}

	/**
	 * Set payment profile id
	 *
	 * @param $paymentProfileId
	 */
	public function setPaymentProfileId($paymentProfileId)
	{
		$this->paymentProfileId = $paymentProfileId;
	}

	/**
	 * Get payment profile id
	 *
	 * @return int
	 */
	public function getPaymentProfileId()
	{
		return $this->paymentProfileId;
	}
}


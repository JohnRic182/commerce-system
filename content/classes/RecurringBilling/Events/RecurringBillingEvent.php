<?php
/**
 * Class RecurringBilling_Events_RecurringBillingEvent
 */
class RecurringBilling_Events_RecurringBillingEvent extends Events_Event
{
	const ON_STATUS_CHANGE = 'on-recurring-billing-status-change';
	const ON_FINISH_BILLING_CYCLE = 'on-recurring-billing-finish-billing-cycle';
	const ON_CC_ABOUT_TO_EXPIRE = 'on-recurring-billing-cc-about-to-expire';
	const ON_ONE_REMAINING = 'on-recurring-billing-one-remaining';
	const ON_FAILED_TRANSACTION = 'on-recurring-billing-failed-transaction';

	/**
	 * Set order
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 *
	 * @return RecurringBilling_Events_RecurringBillingEvent
	 */
	public function setRecurringProfile(RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$this->setData('recurringProfile', $recurringProfile);
		return $this;
	}

	/**
	 * Returns order
	 *
	 * @return RecurringBilling_Model_RecurringProfile
	 */
	public function getRecurringProfile()
	{
		return $this->getData('recurringProfile');
	}

	/**
	 * Set line item
	 *
	 * @param Model_LineItem $lineItem
	 */
	public function setLineItem(Model_LineItem $lineItem)
	{
		$this->setData('lineItem', $lineItem);
	}

	/**
	 * Get line items
	 */
	public function getLineItem()
	{
		$this->getData('lineItem');
	}

	/**
	 * Dispatch status change event
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $oldStatus
	 * @param array|null $data
	 */
	public static function dispatchStatusChangeEvent(RecurringBilling_Model_RecurringProfile $recurringProfile, $oldStatus, $data = null)
	{
		if ($recurringProfile->getStatus() != $oldStatus)
		{
			$event = new self(RecurringBilling_Events_RecurringBillingEvent::ON_STATUS_CHANGE);

			$event
				->setRecurringProfile($recurringProfile)
				->setData('oldStatus', $oldStatus)
				->setData('newStatus', $recurringProfile->getStatus());

			if (!is_null($data) && is_array($data))
			{
				foreach ($data as $key => $value) $event->setData($key, $value);
			}

			Events_EventHandler::handle($event);
		}
	}

	/**
	 * Dispatch status change event
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	public static function dispatchFinishBillingCycleEvent(RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$event = new self(RecurringBilling_Events_RecurringBillingEvent::ON_FINISH_BILLING_CYCLE);

		$event->setRecurringProfile($recurringProfile);

		Events_EventHandler::handle($event);
	}

	/**
	 * Dispatch credit card about to expire event
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	public static function dispatchCreditCardAboutToExpireEvent(RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$event = new self(RecurringBilling_Events_RecurringBillingEvent::ON_CC_ABOUT_TO_EXPIRE);

		$event->setRecurringProfile($recurringProfile);

		Events_EventHandler::handle($event);
	}
}
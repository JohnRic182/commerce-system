<?php

interface RecurringBilling_Cron_RecurringOrderHelperInterface
{
	/**
	 * @param $userId
	 *
	 * @return USER
	 */
	public function getUserById($userId);

	/**
	 * @param $orderId
	 * @param USER $user
	 *
	 * @return ORDER
	 */
	public function getOrderById($orderId, USER $user);

	/**
	 * @param USER $user
	 *
	 * @return ORDER
	 */
	public function createOrder(USER $user);

	/**
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param ORDER $order
	 *
	 * @return string
	 */
	public function processPayment(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order);

	/**
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param ORDER $order
	 *
	 * @return void
	 */
	public function finishOrder(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order);
}
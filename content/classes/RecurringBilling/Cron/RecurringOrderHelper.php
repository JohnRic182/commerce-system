<?php

class RecurringBilling_Cron_RecurringOrderHelper implements RecurringBilling_Cron_RecurringOrderHelperInterface
{
	/** @var DB $db */
	protected $db;
	protected $settings;

	protected $paymentProcessors;

	/** @var Payment_Provider $paymentProvider */
	protected $paymentProvider;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param $settings
	 */
	public function __construct(DB $db, &$settings)
	{
		$this->db = $db;
		$this->settings = $settings;

		$this->paymentProcessors = array();

		// TODO: get from registry
		$this->paymentProvider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings)));
	}

	/**
	 * Get user by id
	 *
	 * @param $userId
	 *
	 * @return USER
	 */
	public function getUserById($userId)
	{
		return USER::getById($this->db, $this->settings, $userId);
	}

	/**
	 * Ggt order by id
	 *
	 * @param $orderId
	 * @param USER $user
	 *
	 * @return ORDER
	 */
	public function getOrderById($orderId, USER $user)
	{
		$order = ORDER::getById($this->db, $this->settings, $user, $orderId);

		if (!is_null($order))
		{
			$order->hydrate();
		}

		return $order;
	}

	/**
	 * Create a new order
	 *
	 * @param USER $user
	 *
	 * @return ORDER
	 */
	public function createOrder(USER $user)
	{
		return new ORDER($this->db, $this->settings, $user);
	}

	/**
	 * Process payment
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param ORDER $order
	 *
	 * @return bool|string
	 */
	public function processPayment(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order)
	{
		$paymentProcessor = $this->getPaymentProcessor($recurringProfile->getPaymentMethodId());

		$paymentProcessor->clearErrors();

		if (!is_null($paymentProcessor))
		{
			$ret = $paymentProcessor->processRecurringBillingPayment($recurringProfile, $order);

			if ($ret !== 'success')
			{
				$order->setPaymentAttemptsCount($order->getPaymentAttemptsCount() + 1);
				$order->persist();
			}

			return $ret;
		}
		else
		{
			return 'fatal';
		}
	}

	/**
	 * Get payment processor
	 *
	 * @param $paymentMethodId
	 *
	 * @return PAYMENT_PROCESSOR
	 */
	protected function getPaymentProcessor($paymentMethodId)
	{
		/**
		 * Process payment
		 */
		if (!isset($paymentProcessors[$paymentMethodId]))
		{
			$this->paymentProcessors[$paymentMethodId] = $this->paymentProvider->getPaymentProcessorById($paymentMethodId);
		}

		return $this->paymentProcessors[$paymentMethodId];
	}

	/**
	 * Finish order
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param ORDER $order
	 */
	public function finishOrder(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order)
	{
		// payment successful
		$customFields = new CustomFields($this->db);

		if (!$order->getShippingRequired() && $order->getPaymentStatus() === ORDER::PAYMENT_STATUS_RECEIVED)
		{
			$order->setStatus(ORDER::STATUS_COMPLETED);
		}
		else
		{
			$order->setStatus(ORDER::STATUS_PROCESS);
		}

		$lineItem = $recurringProfile->getLineItem();
		$productRecurringBillingData = $lineItem->getProductRecurringBillingData();

		// complete order
		$order->closeOrder(
			$order->getStatus() == ORDER::STATUS_COMPLETED && $productRecurringBillingData->getAutoCompleteRecurringOrders() ? ORDER::STATUS_COMPLETED : ORDER::STATUS_PROCESS,
			ORDER::PAYMENT_STATUS_RECEIVED,
			array(), $customFields, 'Recurring', null, true
		);
	}
}
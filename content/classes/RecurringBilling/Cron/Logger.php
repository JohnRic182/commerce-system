<?php
/**
 * Class RecurringBilling_Cron_Logger
 */
class RecurringBilling_Cron_Logger implements RecurringBilling_Cron_LoggerInterface
{
	/**
	 * Log
	 * @param $message
	 */
	public function log($message)
	{
		if (defined('DEVMODE'))
		{
			@file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/cache/log/cron-recurring-billing.txt', date('r').': '.$message."\r\n", FILE_APPEND);
		}

		echo date('r').': '.$message."\r\n";
	}
}
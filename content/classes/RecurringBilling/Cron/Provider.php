<?php
/**
 * Class RecurringBilling_Cron_Provider
 */
class RecurringBilling_Cron_Provider
{
	/** @var DB $db */
	protected $db;
	protected $settings;

	/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository */
	protected $recurringProfileRepository;

	/** @var DataAccess_ProductsRepository $productsRepository */
	protected $productsRepository;

	/** @var DataAccess_OrderRepositoryInterface $orderRepository */
	protected $orderRepository;

	/** @var RecurringBilling_Cron_RecurringOrderHelperInterface $recurringOrderHelper */
	protected $recurringOrderHelper;

	/** @var RecurringBilling_Cron_LoggerInterface RecurringBilling_Cron_LoggerInterface */
	protected $logger;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param array $settings
	 * @param RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository
	 * @param DataAccess_ProductsRepository $productsRepository
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 * @param RecurringBilling_Cron_RecurringOrderHelperInterface $recurringOrderHelper
	 * @param RecurringBilling_Cron_LoggerInterface $logger
	 */
	public function __construct(DB $db, array &$settings, RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository,
		DataAccess_ProductsRepository $productsRepository, DataAccess_OrderRepositoryInterface $orderRepository,
		RecurringBilling_Cron_RecurringOrderHelperInterface $recurringOrderHelper,
		RecurringBilling_Cron_LoggerInterface $logger)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->recurringProfileRepository = $recurringProfileRepository;
		$this->productsRepository = $productsRepository;
		$this->orderRepository = $orderRepository;
		$this->recurringOrderHelper = $recurringOrderHelper;
		$this->logger = $logger;
	}

	/**
	 * Handle on failed event
	 */
	protected function onFailedTransaction(RecurringBilling_Model_RecurringProfile $recurringProfile, $transactionResponse = '')
	{
		/** @var RecurringBilling_Events_RecurringBillingEvent $event */
		$event = new RecurringBilling_Events_RecurringBillingEvent(RecurringBilling_Events_RecurringBillingEvent::ON_FAILED_TRANSACTION);
		$event->setRecurringProfile($recurringProfile);
		$event->setData('transaction_response', $transactionResponse);
		Events_EventHandler::handle($event);
	}

	/**
	 * Process recurring profiles
	 */
	public function process()
	{
		$this->logger->log('Processing recurring payments');

		$recurringProfiles = $this->recurringProfileRepository->getActiveRecurringProfiles();

		while ($recurringProfileData = $this->db->moveNext($recurringProfiles))
		{
			/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
			$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);

			// Catch all for recurring profiles that have been manually updated
			// somehow to have no more billing cycles
			if (!$recurringProfile->hasMoreBillingCycles())
			{
				$recurringProfile->complete();
				continue;
			}

			//$isTrial = $recurringProfile->isInTrialCycle();

			/** @var int $orderId */
			$orderId = $this->recurringProfileRepository->getCurrentSequenceOrderId($recurringProfile);

			/** @var ORDER $order */
			$order = $this->loadOrCreateOrder($orderId, $recurringProfile);

			/**
			 * Check do we have order ready and recalculate everything
			 */
			if (!is_null($order))
			{
				if ($order->getStatus() == ORDER::STATUS_CANCELED ||
					$order->getStatus() == ORDER::STATUS_COMPLETED ||
					$order->getPaymentStatus() == ORDER::PAYMENT_STATUS_RECEIVED)
				{
					// Manually processed?, skip this billing cycle
					$recurringProfile->finishCurrentBillingCycle();
					$this->recurringProfileRepository->persist($recurringProfile);
					continue;
				}

				$this->setRecurringProfileData($recurringProfile, $order);
			}
			else
			{
				/**
				 * Suspend on fatal error
				 */
				$this->onFailedTransaction($recurringProfile);
				$recurringProfile->suspend(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_FAILURE, 'Fatal order error');
				continue;
			}

			/**
			 * Process payment
			 */
			if ($order->getTotalAmount() > 0)
			{
				$paymentStatus = $this->recurringOrderHelper->processPayment($recurringProfile, $order);
			}
			else
			{
				$order->setPaymentStatus(ORDER::PAYMENT_STATUS_RECEIVED);
				$paymentStatus = 'success';
			}

			/**
			 * Finish order in case of successful payment
			 */
			if ($paymentStatus == 'success')
			{
				$this->recurringOrderHelper->finishOrder($recurringProfile, $order);
				$recurringProfile->finishCurrentBillingCycle();
			}
			else
			{
				$this->onFailedTransaction($recurringProfile, '');

				/** @var Model_LineItem $lineItem */
				$lineItem = $recurringProfile->getLineItem();

				/** @var RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData */
				$productRecurringBillingData = $lineItem->getProductRecurringBillingData();

				/** Increase attempts counter */
				$order->checksFailedPaymentAttempts($productRecurringBillingData->getMaxPaymentFailures());

				/**
				 * Handle payment error
				 */
				if ($paymentStatus == 'fatal')
				{
					/**
					 * Suspend on fatal error
					 */
					$recurringProfile->suspend(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_FAILURE, 'Fatal payment error');
				}
				else if ($order->getStatus() == ORDER::STATUS_FAILED)
				{
					$recurringProfile->suspend(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_FAILURE, 'Repeating payment error');
				}
			}

			$this->recurringProfileRepository->persist($recurringProfile);
		}

		$this->logger->log('Processed '.$this->db->numRows($recurringProfiles).' recurring profile(s)');
		$this->logger->log('Processing recurring payments finished');
	}

	/**
	 * Set recurring profile data
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param ORDER $order
	 */
	protected function setRecurringProfileData(RecurringBilling_Model_RecurringProfile $recurringProfile, ORDER $order)
	{
		$order->getOrderData();
		$order->getOrderItems();

		//TODO: BillingAddressInfo

		//TODO: TaxAmount
		//TODO: ShippingAmount

		/** @var Model_LineItem $item */
		foreach ($order->lineItems as $item)
		{
			$amount = $recurringProfile->hasMoreTrialCycles() ?
				$recurringProfile->getTrialAmount() :
				$recurringProfile->getBillingAmount();

			//Ensure that child line item does not have recurring billing information
			$item->setRecurringBillingData(null);
			$item->setFinalPrice($amount);

			$this->orderRepository->persistLineItem($order, $item, false);
			break;
		}

		$shippingErrorMessage = '';
		$order->recalcEverything($shippingErrorMessage);
	}

	/**
	 * Load or create a new order
	 *
	 * @param $orderId
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 *
	 * @return null|ORDER
	 */
	protected function loadOrCreateOrder($orderId, RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$order = null;

		$user = $this->recurringOrderHelper->getUserById($recurringProfile->getUserId());

		if (!is_null($orderId) && intval($orderId) > 0)
		{
			$order = $this->recurringOrderHelper->getOrderById($orderId, $user);

			if (is_null($order))
			{
				$recurringProfile->suspend(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_FAILURE, 'Cannot find order by id: '.$orderId);

				$this->logger->log('Cannot find order by id: '.$orderId.'. Recurring profile id: '.$recurringProfile->getProfileId());
				return null;
			}
			else if ($order->getPaymentStatus() == ORDER::PAYMENT_STATUS_RECEIVED)
			{
				return $order;
			}
		}
		else
		{
			/**
			 * Get initial order
			 */
			$initialOrder = $this->recurringOrderHelper->getOrderById($recurringProfile->getInitialOrderId(), $user);
			$initialItem = $this->orderRepository->getLineItemById($recurringProfile->getInitialOrderLineItemId());
			$initialShipments = $initialOrder->getShipments();

			if ($initialItem && !is_null($initialItem))
			{
				$product = $this->productsRepository->getProductByProductId($initialItem['product_id']);

				if (!is_null($product))
				{
					$order = $this->recurringOrderHelper->createOrder($user);
					$order->setOrderCreatedDate(new DateTime());
					$order->setStatusChangeDate(new DateTime());
					$order->setUserId($user->getId());
					$order->setStatus(ORDER::STATUS_NEW);
					$order->setPaymentStatus(ORDER::PAYMENT_STATUS_PENDING);
					$order->setOrderType(ORDER::ORDER_TYPE_RECURRING);
					$order->updatePaymentMethod($initialOrder->paymentMethodId);
					$order->persist();

					// set order num here
					$order->setOrderNum();

					if ($order->_addItem($product, $recurringProfile->getItemsQuantity(), array()))
					{
						$order->getOrderData();
						$order->getOrderItems();
						$orderShipments = $order->getShipments();

						if ($initialItem['product_type'] == Model_Product::TANGIBLE)
						{
							$shippingAddress = $recurringProfile->getShippingAddress();

							// TODO: change product edit logic so products that are in recurring profiles cannot change type
							if ($shippingAddress && !is_null($shippingAddress))
							{
								$itemShipments = array();

								foreach ($initialShipments as $initialShipment)
								{
									if ($initialShipment['id'] == $initialItem['shipment_id'])
									{
										$db = $this->db;
										$db->reset();
										$db->assignStr('shipping_ssid', $initialShipment['shipping_ssid']);
										$db->assignStr('shipping_carrier_id', $initialShipment['shipping_carrier_id']);
										$db->assignStr('shipping_method_id', $initialShipment['shipping_method_id']);
										$db->assignStr('shipping_cm_name', $initialShipment['shipping_cm_name']);
										$db->update(DB_PREFIX.'orders_shipments', 'WHERE id='.intval(key($orderShipments)));

										break;
									}
								}

								$order->updateShippingAddress($recurringProfile->getShippingAddress()->toArray(), false, $itemShipments);
							}

							$order->updatePaymentMethod($recurringProfile->getPaymentMethodId());
						}

						$order->reupdateItems();
						$shippingErrorMessage = '';
						$order->recalcEverything($shippingErrorMessage);

						/**
						 * Store new order / billing profile relation
						 */
						$this->recurringProfileRepository->saveCurrentSequenceOrderId(
							$recurringProfile->getProfileId(),
							$order->getId(),
							$recurringProfile
						);
					}
					else
					{
						$this->logger->log('Cannot add item to order '.$orderId.' Recurring profile id: '.$recurringProfile->getProfileId());
						return null;
					}
				}
				else
				{
					$this->logger->log('Product not found for order '.$orderId.' Recurring profile id: '.$recurringProfile->getProfileId());
					return null;
				}
			}
			else
			{
				// note: we don't call even here because event will trigger notifications that depend on product that is missing
				$recurringProfile->setStatus(RecurringBilling_Model_RecurringProfile::STATUS_SUSPENDED);
				$recurringProfile->setSuspensionReason(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_FAILURE);
				$recurringProfile->setSuspensionReasonDescription('Cannot find recurring product');
				$this->recurringProfileRepository->persist($recurringProfile);

				$this->logger->log('Initial line item not found for order '.$orderId.' Recurring profile id: '.$recurringProfile->getProfileId());
				return null;
			}
		}

		return $order;
	}
}
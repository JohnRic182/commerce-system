<?php
/**
 * Class RecurringBilling_Plugin
 */
class RecurringBilling_Plugin
{
	/**
	 * Init recurring billing plugin
	 *
	 * @param Framework_Registry $registry
	 */
	public static function init(Framework_Registry $registry)
	{
		$settings = DataAccess_SettingsRepository::getInstance();

		if ($settings->get('RecurringBillingEnabled', false))
		{
			global $db;

			if (!defined('CRON'))
			{
				Events_EventHandler::registerListener(
					Events_OrderEvent::ON_FIND_EXISTING_LINE_ITEM,
					array('RecurringBilling_Listener_RecurringBilling', 'onOrderFindExistingLineItem')
				);

				Events_EventHandler::registerListener(
					Events_OrderEvent::ON_ADD_ITEM,
					array('RecurringBilling_Listener_RecurringBilling', 'onOrderAddItem')
				);

				Events_EventHandler::registerListener(
					Events_OrderEvent::ON_STATUS_CHANGE,
					array('RecurringBilling_Listener_RecurringBilling', 'onProcessCloseOrder')
				);

				Events_EventHandler::registerListener(
					Events_OrderStatusEvent::ON_PROCESS_PAYMENT,
					array('RecurringBilling_Listener_RecurringBilling', 'onProcessPayment')
				);

				Events_EventHandler::registerListener(
					PaymentProfiles_Events_PaymentProfileEvent::ON_BEFORE_REMOVE,
					array('RecurringBilling_Listener_RecurringBilling', 'onBeforeRemovePaymentProfile')
				);

				Events_EventHandler::registerListener(
					PaymentProfiles_Events_PaymentProfileEvent::ON_UPDATE,
					array('RecurringBilling_Listener_RecurringBilling', 'onUpdatePaymentProfile')
				);

				Events_EventHandler::registerListener(
					'user-removed',
					array('RecurringBilling_Listener_RecurringBilling', 'onUserRemoved')
				);
			}

			/** @var DataAccess_UserRepository $userRepository */
			$userRepository = DataAccess_UserRepository::getInstance();

			/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository */
			$recurringProfileRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $settings);

			/** @var RecurringBilling_Listener_Notifications $notificationsListener */
			$notificationsListener = new RecurringBilling_Listener_Notifications($db, $settings, $userRepository, $recurringProfileRepository);

			/**
			 * Register listeners
			 */
			Events_EventHandler::registerListener(
				RecurringBilling_Events_RecurringBillingEvent::ON_STATUS_CHANGE,
				array($notificationsListener, 'onStatusChanged')
			);

			Events_EventHandler::registerListener(
				RecurringBilling_Events_RecurringBillingEvent::ON_ONE_REMAINING,
				array($notificationsListener, 'onDueToComplete')
			);

			Events_EventHandler::registerListener(
				RecurringBilling_Events_RecurringBillingEvent::ON_CC_ABOUT_TO_EXPIRE,
				array($notificationsListener, 'onCardExpiration')
			);

			Events_EventHandler::registerListener(
				RecurringBilling_Events_RecurringBillingEvent::ON_FAILED_TRANSACTION,
				array($notificationsListener, 'onTransactionFailed')
			);
		}
	}
}
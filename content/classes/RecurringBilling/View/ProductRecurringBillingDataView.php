<?php
/**
 * Class RecurringBilling_View_ProductRecurringBillingDataView
 */
class RecurringBilling_View_ProductRecurringBillingDataView
{
	// TODO: add more things here

	/** @var string $scheduleDescription */
	public $scheduleDescription = '';

	/**
	 * Class constructor
	 * @param RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData
	 */
	public function __construct(RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData)
	{
		$this->scheduleDescription = $productRecurringBillingData->getScheduleDescription();
	}
}
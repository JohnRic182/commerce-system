<?php
/**
 * Class RecurringBilling_View_RecurringProfileView
 */
class RecurringBilling_View_RecurringProfileView
{
	/** @var int $profileId */
	public $profileId;

	/** @var int $userId */
	public $userId;

	/** @var int $initialOrderId */
	public $initialOrderId;

	/** @var int $initialOrderLineItemId */
	public $initialOrderLineItemId;

	/** @var int $initialShippingAmount */
	public $initialShippingAmount;

	/** @var float $initialTaxAmount */
	public $initialTaxAmount;

	/** @var float $paymentMethodId */
	public $initialAmount;

	/** @var int $paymentMethodId */
	public $paymentProfileId;

	/** @var int $paymentMethodId */
	public $paymentMethodId;

	/** @var string $status */
	public $status;

	/** @var string $suspensionReason */
	public $suspensionReason;

	/** @var string $suspensionReasonDescription */
	public $suspensionReasonDescription;

	/** @var DateTime $dateCreated */
	public $dateCreated;

	/** @var DateTime $dateUpdated */
	public $dateUpdated;

	/** @var DateTime $dateStart */
	public $dateStart;

	/** @var DateTime $dateNextBilling */
	public $dateNextBilling;

	/** @var int $itemsQuantity */
	public $itemsQuantity;

	/** @var int $billingPeriodUnit */
	public $billingPeriodUnit;

	/** @var int $billingPeriodFrequency */
	public $billingPeriodFrequency;

	/** @var int $billingPeriodCycles */
	public $billingPeriodCycles;

	/** @var float $billingAmount */
	public $billingAmount;

	/** @var bool $trialEnabled  */
	public $trialEnabled;

	/** @var string $trialPeriodUnit */
	public $trialPeriodUnit;

	/** @var int $trialPeriodFrequency */
	public $trialPeriodFrequency;

	/** @var int $trialPeriodCycles */
	public $trialPeriodCycles;

	/** @var float $trialAmount */
	public $trialAmount;

	/** @var int $billingSequenceNumber */
	public $billingSequenceNumber;

	/** @var int $trialSequenceNumber */
	public $trialSequenceNumber;

	/** @var int $currentBillingCycle */
	public $currentBillingCycle;

	/** @var array $shippingAddress */
	public $shippingAddress;

	/** @var array $additionalInfo */
	public $additionalInfo;

	/** @var string $productName */
	public $productName = '';

	/** @var string $subscriberName */
	public $subscriberName;

//	public $cardExpired = false;

	/** @var boolean $hasMoreBillingCycles  */
	public $hasMoreBillingCycles;

	/** @var boolean $hasMoreTrialCycles  */
	public $hasMoreTrialCycles;

	/** @var string $trialDescription */
	public $trialDescription = '';

	/** @var string $billingDescription */
	public $billingDescription = '';

	/** @var bool $canActivate */
	public $canActivate = false;

	/** @var bool $canSuspend */
	public $canSuspend = false;

	/** @var bool $canCancel */
	public $canCancel = false;

	/**
	 * Class constructor
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param array &$msg
	 * @param bool $isAdmin
	 */
	public function __construct(RecurringBilling_Model_RecurringProfile $recurringProfile, &$msg, $isAdmin = false)
	{
		$this->profileId = $recurringProfile->getProfileId();
		$this->userId = $recurringProfile->getUserId();

		$this->initialOrderId = $recurringProfile->getInitialOrderId();
		$this->initialOrderLineItemId = $recurringProfile->getInitialOrderLineItemId();
		$this->initialShippingAmount = $recurringProfile->getInitialShippingAmount();
		$this->initialTaxAmount = $recurringProfile->getInitialTaxAmount();
		$this->initialAmount = $recurringProfile->getInitialAmount();

		$this->paymentProfileId = $recurringProfile->getPaymentProfileId();
		$this->paymentMethodId = $recurringProfile->getPaymentMethodId();

		$this->status = $recurringProfile->getStatus();
		$this->suspensionReason = $recurringProfile->getSuspensionReason();
		$this->suspensionReasonDescription = $recurringProfile->getSuspensionReasonDescription();

		// fix date formats
		$this->dateCreated = $recurringProfile->getDateCreated()->format('m/d/Y');
		$this->dateUpdated = $recurringProfile->getDateUpdated()->format('m/d/Y');
		$this->dateStart = !is_null($recurringProfile->getDateStart()) ? $recurringProfile->getDateStart()->format('m/d/Y') : null;
		$this->dateNextBilling = !is_null($recurringProfile->getDateNextBilling()) ? $recurringProfile->getDateNextBilling()->format('m/d/Y') : null;

		$this->itemsQuantity = $recurringProfile->getItemsQuantity();

		$this->hasMoreTrialCycles = $recurringProfile->hasMoreTrialCycles();
		$this->hasMoreBillingCycles = $recurringProfile->hasMoreBillingCycles();

		$this->billingPeriodUnit = $recurringProfile->getBillingPeriodUnit();
		$this->billingPeriodFrequency = $recurringProfile->getBillingPeriodFrequency();
		$this->billingPeriodCycles = $recurringProfile->getBillingPeriodCycles();
		$this->billingAmount = $recurringProfile->getBillingAmount();

		$this->trialEnabled = $recurringProfile->getTrialEnabled();
		$this->trialPeriodUnit = $recurringProfile->getTrialPeriodUnit();
		$this->trialPeriodFrequency = $recurringProfile->getTrialPeriodFrequency();
		$this->trialPeriodCycles = $recurringProfile->getTrialPeriodCycles();
		$this->trialAmount = $recurringProfile->getTrialAmount();

		if ($recurringProfile->hasMoreTrialCycles())
		{
			$this->billingSequenceNumber = 0;
			$this->trialSequenceNumber = $recurringProfile->getTrialSequenceNumber() + 1;
			$this->currentBillingCycle = $this->trialSequenceNumber;
		}
		else if ($recurringProfile->hasMoreBillingCycles())
		{
			// note, do not add 1 to trial here as trial already completed and sequence number contains actual trial billing cycles number
			$this->trialSequenceNumber = $recurringProfile->getTrialEnabled() ? $recurringProfile->getTrialSequenceNumber() : 0;

			$this->billingSequenceNumber = $recurringProfile->getBillingSequenceNumber() + 1;
			$this->currentBillingCycle = $this->billingSequenceNumber + $this->trialSequenceNumber;
		}
		else
		{
			$this->billingSequenceNumber = 0;
			$this->trialSequenceNumber = 0;
			$this->currentBillingCycle = 0;
		}

		$this->shippingAddressInfo = !is_null($recurringProfile->getShippingAddress()) ? $recurringProfile->getShippingAddress()->toArray() : null;

		$this->additionalInfo = $recurringProfile->getAdditionalInfo();

		$this->productName = $recurringProfile->getProductName();
		$this->subscriberName = $recurringProfile->getSubscriberName();

//		$this->inTrial = $recurringProfile->hasMoreTrialCycles();

		$this->canActivate = $recurringProfile->canActivate();
		$this->canSuspend = $recurringProfile->canSuspend();
		$this->canCancel = $recurringProfile->canCancel();

		$this->trialDescription = $this->getRecurringTrialDescription($recurringProfile, $msg);
		$this->billingDescription = $this->getRecurringBillingDescription($recurringProfile, $msg);

//		$cardExpires = strtotime($this->dateCardExpires);
//		$this->cardExpired = $cardExpires < time();
	}

	/**
	 * Get recurring profiles views
	 *
	 * @param array $recurringProfileModels
	 * @param array $msg
	 * @param bool $isAdmin
	 *
	 * @return array
	 */
	public static function getRecurringProfilesView($recurringProfileModels, &$msg, $isAdmin = false)
	{
		$profiles = array();

		/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
		foreach ($recurringProfileModels as $recurringProfile)
		{
			$profiles[] = new RecurringBilling_View_RecurringProfileView($recurringProfile, $msg, $isAdmin);
		}

		return $profiles;
	}

	/**
	 * Get recurring trial description
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $msg
	 *
	 * @return mixed
	 */
	protected function getRecurringTrialDescription(RecurringBilling_Model_RecurringProfile $recurringProfile, &$msg)
	{
		if ($recurringProfile->getTrialEnabled())
		{
			$message = $recurringProfile->getTrialPeriodCycles() > 1 ? 'description_with_billing_period_cycles' : 'description_with_one_billing_period_cycle';

			$params = array(
				'msg' => $msg['recurring'][$message],
				'amount' => ($recurringProfile->getItemsQuantity() > 1 ? $recurringProfile->getItemsQuantity().' x '  : '') . getPrice($recurringProfile->getTrialAmount()),
				'freq' => $recurringProfile->getTrialPeriodFrequency(),// > 1 ? $recurringProfile->getTrialPeriodFrequency() : 'a',
				'unit' => $msg['recurring'][($recurringProfile->getTrialPeriodFrequency() > 1 ? 'plural' : 'singular').'_'.$recurringProfile->getTrialPeriodUnit()],
				'cycles' => $recurringProfile->getTrialPeriodCycles(),
				'times' => $msg['recurring'][($recurringProfile->getTrialPeriodCycles() > 1 ? 'plural' : 'singular').'_times']
			);

			$smarty = null;

			return smarty_function_lang($params, $smarty);
		}

		return null;
	}

	/**
	 * Get billing description
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $msg
	 *
	 * @return mixed
	 */
	protected function getRecurringBillingDescription(RecurringBilling_Model_RecurringProfile $recurringProfile, &$msg)
	{
		if ($recurringProfile->getBillingPeriodCycles() > 0)
		{
			if ($recurringProfile->getBillingPeriodCycles() > 1)
			{
				$message = 'description_with_billing_period_cycles';
			}
			else
			{
				$message = 'description_with_one_billing_period_cycle';
			}
		}
		else
		{
			$message = 'description_without_billing_period_cycles';
		}

		$params = array(
			'msg' => $msg['recurring'][$message],
			'amount' => ($recurringProfile->getItemsQuantity() > 1 ? $recurringProfile->getItemsQuantity().' x '  : '') . getPrice($recurringProfile->getBillingAmount()),
			'freq' => $recurringProfile->getBillingPeriodFrequency() > 1 ? $recurringProfile->getBillingPeriodFrequency() : '',
			'unit' => $msg['recurring'][($recurringProfile->getBillingPeriodFrequency() > 1 ? 'plural' : 'singular').'_'.$recurringProfile->getBillingPeriodUnit()],
			'cycles' => $recurringProfile->getBillingPeriodCycles(),
			'times' => $msg['recurring'][($recurringProfile->getBillingPeriodCycles() > 1 ? 'plural' : 'singular').'_times'],
		);

		$smarty = null;

		return smarty_function_lang($params, $smarty);
	}
}
<?php
/**
 * Class RecurringBilling_DataAccess_RecurringProfileRepository
 */
class RecurringBilling_DataAccess_RecurringProfileRepository
{
	/** @var DB $db */
	protected $db;

	/** @var DataAccess_SettingsRepository $settings */
	protected $settings;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->db = $db;
		$this->settings = $settings;
	}

	/**
	 * @param $searchParams
	 * @return int
	 */
	public function getCount($searchParams)
	{
		$result = $this->db->selectOne('
			SELECT COUNT(rp.recurring_profile_id) AS c
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users AS u ON u.uid = rp.user_id
			INNER JOIN '.DB_PREFIX.'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN '.DB_PREFIX.'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			'.$this->getSearchQuery($searchParams)
		);

		return intval($result['c']);
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 'o.*', $logic = 'AND')
	{
		$db = $this->db;

		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		switch ($orderBy)
		{
			/**
			 * New order by params
			 */
			case 'profile_id_asc' : $orderBy = 'rp.recurring_profile_id'; break;
			case 'profile_id_desc' : $orderBy = 'rp.recurring_profile_id DESC'; break;
			case 'product_name_asc' : $orderBy = 'oc.title'; break;
			case 'product_name_desc' : $orderBy = 'oc.title DESC'; break;
			case 'subscriber_name_asc' : $orderBy = 'subscriber_name'; break;
			case 'subscriber_name_desc' : $orderBy = 'subscriber_name DESC'; break;

			/**
			 * Old order by params
			 */
			case 'profile_id': $orderBy = 'rp.recurring_profile_id'; break;
			case 'product_name': $orderBy = 'oc.title'; break;
			case 'next_billing_date': $orderBy = 'rp.date_next_billing'; break;
			case 'next_billing_date_desc': $orderBy = 'rp.date_next_billing DESC'; break;
		}

		// select data from database
		$queryResult = $this->db->query('
			SELECT rp.*, CONCAT(u.fname, " ", u.lname) AS subscriber_name, oc.title AS product_name
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users AS u ON u.uid = rp.user_id
			INNER JOIN '.DB_PREFIX.'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN '.DB_PREFIX.'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			'.$this->getSearchQuery($searchParams).' ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);

		$profiles = array();

		// TODO: move outside, should not be here
		$addressRepository = new DataAccess_AddressRepository($db);

		while ($recurringProfileData = $db->moveNext($queryResult))
		{
			$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);

			if (!is_null($recurringProfile->getShippingAddress()))
			{
				$addressRepository->hydrateAddress($recurringProfile->getShippingAddress());
			}

			$profiles[] = $recurringProfile;
		}

		return $profiles;
	}

	/**
	 * @param $searchParams
	 * @return string
	 */
	protected function getSearchQuery($searchParams)
	{
		$db = $this->db;

		$where = array();

		if (isset($searchParams['user_id']) && intval($searchParams['user_id']) > 0)
		{
			$where[] = 'rp.user_id = '.intval($searchParams['user_id']);
		}

		if (isset($searchParams['status']) && $searchParams['status'] != 'any')
		{
			$where[] = 'rp.status = "'.$db->escape($searchParams['status']).'"';
		}

		if (isset($searchParams['profile_id']) && trim($searchParams['profile_id']) != '')
		{
			$where[] = 'rp.recurring_profile_id = "'.intval($searchParams['profile_id']).'"';
		}

		if (isset($searchParams['order_id']) && trim($searchParams['order_id']) != '')
		{
			$where[] = 'o.order_num = "'.intval($searchParams['order_id']).'"';
		}

		if (isset($searchParams['subscriber_name']) && trim($searchParams['subscriber_name']) != '')
		{
			$where[] = 'u.lname LIKE "%'.$db->escape(trim($searchParams['subscriber_name'])).'%"';
		}

		if (isset($searchParams['product_name']) && trim($searchParams['product_name']) != '')
		{
			$where[] = 'oc.title LIKE "%'.$db->escape(trim($searchParams['product_name'])).'%"';
		}

		if (isset($searchParams['date_created_from']) && trim($searchParams['date_created_from']) != '')
		{
			$where[] = 'rp.date_created >="'.$searchParams['date_created_from'].'"';
		}

		if (isset($searchParams['date_created_to']) && trim($searchParams['date_created_to']) != '')
		{
			$where[] = 'rp.date_created <="'.$searchParams['date_created_to'].'"';
		}

		return count($where) > 0 ? ' WHERE '.implode(' AND ', $where).' ' : '';
	}

	/**
	 * Get recurring profile by id
	 *
	 * @param $profileId
	 * @param $userId
	 *
	 * @return RecurringBilling_Model_RecurringProfile|null
	 */
	public function get($profileId, $userId = false)
	{
		/** @var DB $db */
		$db = $this->db;

		if ($recurringProfileData = $db->selectOne('
			SELECT
				rp.*,
			 	CONCAT(u.fname, " ", u.lname) AS subscriber_name,
				oc.title AS product_name
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users AS u ON u.uid = rp.user_id
			INNER JOIN '.DB_PREFIX.'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN '.DB_PREFIX.'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			WHERE
				rp.recurring_profile_id = '.intval($profileId).($userId !== false ? ' AND rp.user_id = '.intval($userId) : ''))
		)
		{
			$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);

			// TODO: move outside, should not be here
			if (!is_null($recurringProfile->getShippingAddress()))
			{
				$addressRepository = new DataAccess_AddressRepository($db);
				$addressRepository->hydrateAddress($recurringProfile->getShippingAddress());
			}

			return $recurringProfile;
		}

		return null;
	}

	/**
	 * Get recurring profiles by userId
	 *
	 * @param $userId
	 *
	 * @return array
	 */
	public function getByUserId($userId)
	{
		/** @var DB $db */
		$db = $this->db;
		$ret = array();

		$addressRepository = new DataAccess_AddressRepository($db);

		if ($recurringProfilesData = $db->selectAll('
			SELECT
				rp.*,
			 	CONCAT(u.fname, " ", u.lname) AS subscriber_name,
				oc.title AS product_name
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users AS u ON u.uid = rp.user_id
			INNER JOIN '.DB_PREFIX.'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN '.DB_PREFIX.'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			WHERE
				rp.user_id = '.intval($userId))
		)
		{
			foreach ($recurringProfilesData as $recurringProfileData)
			{
				$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);

				// TODO: move outside, should not be here
				if (!is_null($recurringProfile->getShippingAddress()))
				{
					$addressRepository->hydrateAddress($recurringProfile->getShippingAddress());
				}

				$ret[] = $recurringProfile;
			}
		}

		return $ret;
	}

	/**
	 * Get recurring profiles report
	 *
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 *
	 * @return mixed
	 */
	public function getRecurringProfilesReport(DateTime $dateFrom, DateTime $dateTo)
	{
		/** @var DB $db */
		$db = $this->db;

		return $db->query('
			SELECT
				rp.*,
				SUM(ro.total_amount) AS amount_collected,
			 	CONCAT(u.fname, " ", u.lname) AS subscriber_name,
				oc.title AS product_name,
				DATE_FORMAT(rp.date_created, "%M %e, %Y") AS date_created,
				DATE_FORMAT(rp.date_next_billing, "%M %e, %Y") AS date_next_billing
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users AS u ON u.uid = rp.user_id
			INNER JOIN '.DB_PREFIX.'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN '.DB_PREFIX.'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			LEFT JOIN '.DB_PREFIX.'recurring_profiles_orders AS rpo ON rp.recurring_profile_id = rpo.recurring_profile_id
			LEFT JOIN '.DB_PREFIX.'orders AS ro ON rpo.order_id = ro.oid AND ro.payment_status="Received"
			WHERE
				rp.date_created >= "'.$dateFrom->format('Y-m-d H:i:s').'" AND rp.date_created <="'.$dateTo->format('Y-m-d H:i:s').'"
			GROUP BY
				rp.recurring_profile_id
			ORDER BY
			 	rp.recurring_profile_id
		');
	}

	/**
	 * Get active recurring profiles
	 *
	 * @return mixed
	 */
	public function getProfilesWhereCardExpiresSoon()
	{
		return $this->db->query('
			SELECT rp.*
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users_payment_profiles AS upp ON rp.payment_profile_id = upp.usid AND rp.date_next_billing >= upp.cc_exp_date AND upp.cc_exp_date IS NOT NULL
			WHERE rp.status != "Completed" AND rp.status != "Canceled" AND rp.notified_cc_expire = 0
		');
	}

	/**
	 * Get recurring profile be recurring order id
	 *
	 * @param $orderId
	 * @return mixed
	 */
	public function getProfileByRecurringOrderId($orderId)
	{
		return $this->db->selectOne('
			SELECT rp.*
			FROM '.DB_PREFIX.'recurring_profiles_orders AS rpo
			INNER JOIN '.DB_PREFIX.'recurring_profiles AS rp ON rpo.recurring_profile_id=rp.recurring_profile_id
			WHERE rpo.order_id='.intval($orderId).'
		');
	}

	/**
	 * Get active recurring profiles
	 *
	 * @return mixed
	 */
	public function getActiveRecurringProfiles()
	{
		return $this->db->query('SELECT * FROM '.DB_PREFIX.'recurring_profiles WHERE status="Active" AND date_next_billing <= DATE(NOW())');
	}

	/**
	 * Get current sequence order id
	 *
	 * @param RecurringBilling_Model_RecurringProfile
	 *
	 * @return int|null
	 */
	public function getCurrentSequenceOrderId(RecurringBilling_Model_RecurringProfile $profile)
	{
		$recurringProfileId = $profile->getProfileId();
		$isTrial = $profile->hasMoreTrialCycles();

		$orderData = $this->db->selectOne('
			SELECT order_id
			FROM '.DB_PREFIX.'recurring_profiles_orders
			WHERE
				recurring_profile_id = '.intval($recurringProfileId).'
				AND is_trial = '.($isTrial ? '1' : '0').'
				AND sequence_number = '.intval($isTrial ? $profile->getTrialSequenceNumber() : $profile->getBillingSequenceNumber())
		);

		return $orderData ? $orderData['order_id'] : null;
	}

	/**
	 * Set current current sequence number
	 *
	 * @param $recurringProfileId
	 * @param $orderId
	 * @param RecurringBilling_Model_RecurringProfile $profile
	 */
	public function saveCurrentSequenceOrderId($recurringProfileId, $orderId, RecurringBilling_Model_RecurringProfile $profile)
	{
		$isTrial = $profile->hasMoreTrialCycles();

		$this->db->reset();
		$this->db->assign('recurring_profile_id', intval($recurringProfileId));
		$this->db->assign('order_id', intval($orderId));
		$this->db->assign('is_trial', $isTrial ? 1 : 0);
		$this->db->assign('sequence_number', intval($isTrial ? $profile->getTrialSequenceNumber() : $profile->getBillingSequenceNumber()));
		$this->db->insert(DB_PREFIX.'recurring_profiles_orders');
	}

	/**
	 * Get recurring profile by line item id
	 *
	 * @param $lineItemId
	 *
	 * @return RecurringBilling_Model_RecurringProfile|null
	 */
	public function getByLineItemId($lineItemId)
	{
		/** @var DB $db */
		$db = $this->db;

		if ($recurringProfileData = $db->selectOne('SELECT * FROM '.DB_PREFIX.'recurring_profiles WHERE initial_order_line_item_id = '.intval($lineItemId)))
		{
			$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);

			// TODO: move outside, should not be here
			$addressRepository = new DataAccess_AddressRepository($db);
			$addressRepository->hydrateAddress($recurringProfile->getShippingAddress());

			return $recurringProfile;
		}

		return null;
	}

	/**
	 * Persist recurring profile
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	public function persist(RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$this->db->reset();
		$this->db->assignStr('user_id', $recurringProfile->getUserId());

		$this->db->assignStr('initial_order_id', $recurringProfile->getInitialOrderId());
		$this->db->assignStr('initial_order_line_item_id', $recurringProfile->getInitialOrderLineItemId());
		$this->db->assignStr('initial_shipping_amount', $recurringProfile->getInitialShippingAmount());
		$this->db->assignStr('initial_tax_amount', $recurringProfile->getInitialTaxAmount());
		$this->db->assignStr('initial_amount', $recurringProfile->getInitialAmount());

		$this->db->assignStr('payment_method_id', $recurringProfile->getPaymentMethodId());
		$this->db->assignStr('payment_profile_id', $recurringProfile->getPaymentProfileId());

		$this->db->assignStr('status', $recurringProfile->getStatus());
		$this->db->assignStr('suspension_reason', $recurringProfile->getSuspensionReason());
		$this->db->assignStr('suspension_reason_description', $recurringProfile->getSuspensionReasonDescription());

		if (!is_null($recurringProfile->getDateStart()))
		{
			$this->db->assignStr('date_start', $recurringProfile->getDateStart()->format('Y-m-d H:i:s'));
		}

		$this->db->assignStr('date_next_billing', $recurringProfile->getDateNextBilling()->format('Y-m-d H:i:s'));

		$this->db->assignStr('billing_period_unit', $recurringProfile->getBillingPeriodUnit());
		$this->db->assignStr('billing_period_frequency', $recurringProfile->getBillingPeriodFrequency());
		$this->db->assignStr('billing_period_cycles', $recurringProfile->getBillingPeriodCycles());
		$this->db->assignStr('billing_amount', $recurringProfile->getBillingAmount());

		$this->db->assignStr('trial_enabled', $recurringProfile->getTrialEnabled() ? '1' : '0');
		$this->db->assignStr('trial_period_unit', $recurringProfile->getTrialPeriodUnit());
		$this->db->assignStr('trial_period_frequency', $recurringProfile->getTrialPeriodFrequency());
		$this->db->assignStr('trial_period_cycles', $recurringProfile->getTrialPeriodCycles());
		$this->db->assignStr('trial_amount', $recurringProfile->getTrialAmount());

		$this->db->assignStr('items_quantity', $recurringProfile->getItemsQuantity());

		if (!is_null($recurringProfile->getShippingAddress()))
		{
			$this->db->assignStr('shipping_address_info', serialize($recurringProfile->getShippingAddress()->toArray()));
		}

		$this->db->assignStr('billing_sequence_number', $recurringProfile->getBillingSequenceNumber());
		$this->db->assignStr('trial_sequence_number', $recurringProfile->getTrialSequenceNumber());

		$this->db->assignStr('notified_cc_expire', $recurringProfile->getNotifiedCCExpire() ? 1 : 0);

		//TODO: additional_info

		if ($recurringProfile->getProfileId() > 0)
		{
			// set actual date
			$recurringProfile->setDateUpdated(new DateTime());
			$this->db->assignStr('date_updated', $recurringProfile->getDateCreated()->format('Y-m-d H:i:s'));

			$this->db->update(DB_PREFIX.'recurring_profiles', ' WHERE recurring_profile_id = '.intval($recurringProfile->getProfileId()));
		}
		else
		{
			// set actual dates
			$recurringProfile->setDateCreated(new DateTime());
			$recurringProfile->setDateUpdated(new DateTime());
			$this->db->assignStr('date_created', $recurringProfile->getDateCreated()->format('Y-m-d H:i:s'));
			$this->db->assignStr('date_updated', $recurringProfile->getDateUpdated()->format('Y-m-d H:i:s'));

			$this->db->insert(DB_PREFIX.'recurring_profiles');
			$recurringProfile->setProfileId($this->db->insertId());
		}
	}

	/**
	 * Search for recurring profile
	 *
	 * @param array $searchParams
	 * @param int $pg
	 * @param int $pageSize
	 * @return array
	 */
	public function search($searchParams = array(), $pg = 1, $pageSize = 20)
	{
		$db = $this->db;

		$queryParams = array();

		if (isset($searchParams['user_id']) && intval($searchParams['user_id']) > 0)
		{
			$queryParams[] = 'rp.user_id = '.intval($searchParams['user_id']);
		}

		if (isset($searchParams['status']) && $searchParams['status'] != 'any')
		{
			$queryParams[] = 'rp.status = "'.$db->escape($searchParams['status']).'"';
		}

		if (isset($searchParams['profile_id']) && trim($searchParams['profile_id'] != ''))
		{
			$queryParams[] = 'rp.recurring_profile_id = "'.intval($searchParams['profile_id']).'"';
		}

		if (isset($searchParams['order_id']) && trim($searchParams['order_id'] != ''))
		{
			$queryParams[] = 'o.order_num = "'.intval($searchParams['order_id']).'"';
		}

		if (isset($searchParams['subscriber_name']) && intval($searchParams['subscriber_name'] != ''))
		{
			$queryParams[] = 'u.lname LIKE "%'.$db->escape(trim($searchParams['subscriber_name'])).'%"';
		}

		if (isset($searchParams['product_name']) && trim($searchParams['product_name'] != ''))
		{
			$queryParams[] = 'oc.title LIKE "%'.$db->escape(trim($searchParams['product_name'])).'%"';
		}

		$query = implode('AND ', $queryParams);

		if (strlen($query) > 0)
		{
			$query = 'WHERE '.$query;
		}

		$countResult = $db->selectOne('
			SELECT COUNT(rp.recurring_profile_id) AS c
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users AS u ON u.uid = rp.user_id
			INNER JOIN '.DB_PREFIX.'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN '.DB_PREFIX.'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			'.$query
		);

		 if ($pageSize <= 0)
		{
			$pagesCount = 1;
		}
		else
		{
			$pagesCount = floor($countResult['c'] / $pageSize) + ($countResult['c'] % $pageSize > 0 ? 1 : 0);
		}

		if ($pg > $pagesCount)
        {
			$countResult['c'] = 0;
        }
		
		$limit = '';
		if ($pageSize > 0)
		{
			$offset = ($pg - 1) * $pageSize;
			$limit = 'LIMIT '.$offset.','.$pageSize;
		}

		$orderByOptions = array(
			'profile_id' => 'rp.recurring_profile_id',
			'profile_id_desc' => 'rp.recurring_profile_id DESC',
			'product_name' => 'oc.title',
			'product_name_desc' => 'oc.title DESC',
			'next_billing_date' => 'rp.date_next_billing',
			'next_billing_date_desc' => 'rp.date_next_billing DESC',
		);

		$orderBy = isset($searchParams['sort_by']) && isset($orderByOptions[$searchParams['sort_by']]) ? $orderByOptions[$searchParams['sort_by']] : $orderByOptions['profile_id_desc'];

		$queryResult = $db->query('
			SELECT
				rp.*,
				CONCAT(u.fname, " ", u.lname) AS subscriber_name,
				oc.title AS product_name
			FROM '.DB_PREFIX.'recurring_profiles AS rp
			INNER JOIN '.DB_PREFIX.'users AS u ON u.uid = rp.user_id
			INNER JOIN '.DB_PREFIX.'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN '.DB_PREFIX.'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			'.$query.'
			ORDER BY '.$orderBy.' '.$limit
		);

		$profiles = array();

		// TODO: move outside, should not be here
		$addressRepository = new DataAccess_AddressRepository($db);

		while ($recurringProfileData = $db->moveNext($queryResult))
		{
			$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);

			if (!is_null($recurringProfile->getShippingAddress()))
			{
				$addressRepository->hydrateAddress($recurringProfile->getShippingAddress());
			}

			$profiles[] = $recurringProfile;
		}
		return array(
			'count' => $countResult['c'],
			'items' => $profiles,
			'pages_count' => $pagesCount
		);
	}

	/**
	 * Get profiles by ids
	 *
	 * @param array $profileIds
	 * @return array
	 */
	public function getByIds(array $profileIds)
	{
		if (count($profileIds) > 0)
		{
			$db = $this->db;

			$temp = array();
			foreach ($profileIds as $v)
			{
				if (intval($v) > 0)
				{
					$temp[] = intval($v);
				}
			}
			$profileIds = $temp;

			$db->query('SELECT rp.* FROM '.DB_PREFIX.'recurring_profiles AS rp WHERE rp.recurring_profile_id IN ('.implode(',', $profileIds).')');

			$profiles = array();

			// TODO: move outside, should not be here
			$addressRepository = new DataAccess_AddressRepository($db);

			while ($recurringProfileData = $this->db->moveNext())
			{
				$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);

				if (!is_null($recurringProfile->getShippingAddress()))
				{
					$addressRepository->hydrateAddress($recurringProfile->getShippingAddress());
				}

				$profiles[] = $recurringProfile;
			}

			return $profiles;
		}

		return array();
	}

	/**
	 * Get order for profile
	 *
	 * @param $profileId
	 * @return array|bool
	 */
	public function getOrdersForProfile($profileId)
	{
		$this->db->query('
			SELECT o.*
				, DATE_FORMAT(o.status_date, "'.$this->settings->get('LocalizationDateTimeFormat').'") AS status_date
				, DATE_FORMAT(o.placed_date, "'.$this->settings->get('LocalizationDateTimeFormat').'") AS placed_date
			FROM '.DB_PREFIX.'orders o
			INNER JOIN '.DB_PREFIX.'recurring_profiles_orders po ON po.order_id = o.oid AND po.recurring_profile_id = '.intval($profileId).'
			ORDER BY o.placed_date DESC
		');

		if ($this->db->numRows() > 0)
		{
			return $this->db->getRecords();
		}

		return false;
	}
}
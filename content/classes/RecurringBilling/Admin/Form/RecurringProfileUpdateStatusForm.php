<?php

/**
 * Class RecurringBilling_Admin_Form_RecurringProfileBulkUpdateStatusForm
 */
class RecurringBilling_Admin_Form_RecurringProfileUpdateStatusForm
{
	/**
	 * @param $canActivate
	 * @param $canSuspend
	 * @param $canCancel
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($canActivate, $canSuspend, $canCancel)
	{
		$updateStatusFormBuilder = new core_Form_FormBuilder('update-status');

		$statusOptions = array();//array('' => 'Keep current');
		if ($canActivate) $statusOptions['Active'] = 'Activate';
		if ($canSuspend) $statusOptions['Suspended'] = 'Suspend';
		if ($canCancel) $statusOptions['Canceled'] = 'Cancel';

		$updateStatusFormBuilder->add('recurring_profile[status]', 'choice', array('label' => 'Set new profile status', 'options' => $statusOptions));

		return $updateStatusFormBuilder;
	}

	/**
	 * @param $canActivate
	 * @param $canSuspend
	 * @param $canCancel
	 * @return core_Form_Form
	 */
	public function getForm($canActivate, $canSuspend, $canCancel)
	{
		return $this->getFormBuilder($canActivate, $canSuspend, $canCancel)->getForm();
	}
}

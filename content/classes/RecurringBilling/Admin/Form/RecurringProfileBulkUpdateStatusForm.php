<?php

/**
 * Class RecurringBilling_Admin_Form_RecurringProfileBulkUpdateStatusForm
 */
class RecurringBilling_Admin_Form_RecurringProfileBulkUpdateStatusForm
{
	/**
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder()
	{
		$updateStatusFormBuilder = new core_Form_FormBuilder('update-status');
		$updateStatusFormBuilder->add(
			'status', 'choice',
			array(
				'label' => 'Set new profile status',
				'options' => array(
					//'' => 'Keep current',
					'Active' => 'Activate',
					'Suspended' => 'Suspend',
					'Canceled' => 'Cancel'
				)
			)
		);

		return $updateStatusFormBuilder;
	}

	/**
	 * @return core_Form_Form
	 */
	public function getForm()
	{
		return $this->getFormBuilder()->getForm();
	}

}
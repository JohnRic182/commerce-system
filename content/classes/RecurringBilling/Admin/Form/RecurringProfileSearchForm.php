<?php

/**
 * Class RecurringBilling_Admin_Form_RecurringProfileSearchForm
 */
class RecurringBilling_Admin_Form_RecurringProfileSearchForm
{
	/**
	 * @param $searchParams
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($searchParams)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[profile_id]', 'text', array('wrapperClass' => 'col-sm-12 col-md-4', 'label' => 'Profile ID', 'value' => $searchParams['profile_id']));
		$formBuilder->add('searchParams[order_id]', 'text', array('wrapperClass' => 'col-sm-12 col-md-4', 'label' => 'Initial order ID', 'value' => $searchParams['order_id']));
		$formBuilder->add('searchParams[status]', 'choice', array('wrapperClass' => 'col-sm-12 col-md-4', 'label' => 'Profile status', 'value' => $searchParams['status'], 'options' => array(
			'any' => 'Any',
			'Active' => 'Active',
			'Suspended' => 'Suspended',
			'Canceled' => 'Canceled',
			'Completed' => 'Completed'
		)));

		$formBuilder->add('searchParams[subscriber_name]', 'text', array('wrapperClass' => 'col-sm-12 col-md-4', 'label' => 'Customer\'s last name', 'value' => $searchParams['subscriber_name']));
		$formBuilder->add('searchParams[product_name]', 'text', array('wrapperClass' => 'col-sm-12 col-md-4', 'label' => 'Product name', 'value' => $searchParams['product_name']));

		return $formBuilder;
	}

	/**
	 * @param $searchParams
	 * @return core_Form_Form
	 */
	public function getForm($searchParams)
	{
		return $this->getFormBuilder($searchParams)->getForm();
	}
}
<?php
/**
 * Class Admin_Form_AddressForm
 */
class RecurringBilling_Admin_Form_RecurringProfileForm
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;

	/**
	 * Class constructor
	 *
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
	}

	/**
	 * Render recurring form
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $adminCanAdjustTrial
	 * @param $adminCanAdjustBilling
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder(RecurringBilling_Model_RecurringProfile $recurringProfile, $adminCanAdjustTrial, $adminCanAdjustBilling)
	{
		$recurringProfileFormBuilder = new core_Form_FormBuilder('form-recurring-profile');
		$propertiedGroup = new core_Form_FormBuilder('form-recurring-profile-properties', array('label' => 'recurring.profile.properties_title', 'class'=>'ic-invoice', 'wrapperClass' => 'form-address'));

		$recurringProfileFormBuilder->add($propertiedGroup);

		/**
		 * Trial period settings
		 */
		if ($recurringProfile->hasMoreTrialCycles())
		{
			if ($adminCanAdjustTrial)
			{
				$propertiedGroup->add('recurring_profile[next_billing_date]', 'date', array('label'=>'recurring.profile.next_billing_date', 'value' =>  $recurringProfile->getDateNextBilling(), 'required' => true));
				$propertiedGroup->add('recurring_profile[trial_sequence_number]', 'static', array('label'=>'recurring.profile.trial_sequence_number', 'value' =>  $recurringProfile->getTrialSequenceNumber() + 1));
				$propertiedGroup->add('recurring_profile[trial_period_cycles]', 'static', array('label'=>'recurring.profile.trial_period_cycles', 'value' =>  $recurringProfile->getTrialPeriodCycles()));
				//TODO: put here actually used currency sign
				$propertiedGroup->add('recurring_profile[trial_amount]', 'money', array('label'=>'recurring.profile.trial_amount', 'note' => 'recurring.profile.trial_amount_note', 'validators'=> 'money', 'value' =>  myNum($recurringProfile->getTrialAmount()), 'required' => true));
			}
			else
			{
				$propertiedGroup->add('recurring_profile[next_billing_date]', 'static', array('label'=>'recurring.profile.next_billing_date', 'value' =>  $recurringProfile->getDateNextBilling()->format('m/d/Y')));
				$propertiedGroup->add('recurring_profile[trial_sequence_number]', 'static', array('label'=>'recurring.profile.trial_sequence_number', 'value' =>  $recurringProfile->getTrialSequenceNumber() + 1));
				$propertiedGroup->add('recurring_profile[trial_period_cycles]', 'static', array('label'=>'recurring.profile.trial_period_cycles', 'value' =>  $recurringProfile->getTrialPeriodCycles()));
				//TODO: put here actually used currency sign
				$propertiedGroup->add('recurring_profile[trial_amount]', 'static', array('label'=>'recurring.profile.trial_amount', 'value' =>  getAdminPrice($recurringProfile->getTrialAmount())));
			}
			$inTrialCycle = true;
		}
		else
		{
			$inTrialCycle = false;
		}

		/**
		 * Billing period settings
		 */
		if ($recurringProfile->hasMoreBillingCycles())
		{
			if ($adminCanAdjustBilling)
			{
				if (!$inTrialCycle)
				{
					$propertiedGroup->add('recurring_profile[next_billing_date]', 'date', array('label'=>'recurring.profile.next_billing_date', 'value' =>  $recurringProfile->getDateNextBilling(), 'required' => true));
				}

				if ($inTrialCycle)
				{
					$propertiedGroup->add('recurring_profile[billing_sequence_number]', 'static', array('label'=>'recurring.profile.billing_sequence_number', 'value' => 'recurring.profile.billing_sequence_number_note'));
				}
				else
				{
					$propertiedGroup->add('recurring_profile[billing_sequence_number]', 'static', array('label'=>'recurring.profile.billing_sequence_number', 'value' =>  $recurringProfile->getBillingSequenceNumber() + 1));
				}

				$propertiedGroup->add('recurring_profile[billing_period_cycles]', 'static', array('label'=>'recurring.profile.billing_period_cycles', 'value' =>  $recurringProfile->getBillingPeriodCycles() > 0 ? $recurringProfile->getBillingPeriodCycles() : 'until cancelled'));
				//TODO: put here actually used currency sign
				$propertiedGroup->add('recurring_profile[billing_amount]', 'money', array('label'=>'recurring.profile.billing_amount', 'note' => 'recurring.profile.billing_amount_note', 'value' =>  myNum($recurringProfile->getBillingAmount()), 'required' => true, 'validators'=> 'money'));
				$propertiedGroup->add('recurring_profile[items_quantity]', 'integer', array('label'=>'recurring.profile.items_quantity', 'value' =>  intval($recurringProfile->getItemsQuantity()), 'required' => true, 'validators'=> 'int'));
			}
			else
			{
				if (!$inTrialCycle)
				{
					$propertiedGroup->add('recurring_profile[next_billing_date]', 'static', array('label'=>'recurring.profile.next_billing_date', 'value' =>  $recurringProfile->getDateNextBilling()->format('m/d/Y')));
				}
				$propertiedGroup->add('recurring_profile[billing_sequence_number]', 'static', array('label'=>'recurring.profile.billing_sequence_number', 'note' => $inTrialCycle ? 'recurring.profile.billing_sequence_number_note' : '',  'value' =>  $recurringProfile->getBillingSequenceNumber()));
				$propertiedGroup->add('recurring_profile[billing_period_cycles]', 'static', array('label'=>'recurring.profile.billing_period_cycles', 'value' =>  $recurringProfile->getBillingPeriodCycles() > 0 ? $recurringProfile->getBillingPeriodCycles() : 'until cancelled'));
				//TODO: put here actually used currency sign
				$propertiedGroup->add('recurring_profile[billing_amount]', 'static', array('label'=>'recurring.profile.billing_amount', 'value' =>  getAdminPrice($recurringProfile->getBillingAmount())));
				$propertiedGroup->add('recurring_profile[items_quantity]', 'static', array('label'=>'recurring.profile.items_quantity', 'value' =>  intval($recurringProfile->getItemsQuantity())));
			}
		}
		else
		{
			$propertiedGroup->add('recurring_profile[billing_sequence_number]', 'static', array('label'=>'recurring.profile.billing_sequence_number', 'value' => 'recurring.profile.billing_completed'));
		}

		return $recurringProfileFormBuilder;
	}

	/**
	 * Get recurring profile form
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $adminCanAdjustTrial
	 * @param $adminCanAdjustBilling
	 *
	 * @return core_Form_Form
	 */
	public function getForm(RecurringBilling_Model_RecurringProfile $recurringProfile, $adminCanAdjustTrial, $adminCanAdjustBilling)
	{
		return $this->getBuilder($recurringProfile, $adminCanAdjustTrial, $adminCanAdjustBilling)->getForm();
	}
}
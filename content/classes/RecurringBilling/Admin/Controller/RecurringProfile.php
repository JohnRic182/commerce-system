<?php
/**
 * Class RecurringBilling_Admin_Controller_RecurringProfile
 */
class RecurringBilling_Admin_Controller_RecurringProfile extends Admin_Controller
{
	protected $menuItem = array('primary' => 'customers', 'secondary' => '');

	/**
	 * @var array
	 */
	protected $searchParamsDefaults = array(
		'profile_id' => '',
		'order_id' => '',
		'status' => RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE,
		'subscriber_name' => '',
		'product_name' => '',
		'orderBy' => 'profile_id_desc',
		'logic' => 'AND'
	);

	/**
	 * @return bool
	 */
	public function canAccessRecurringBilling()
	{
		return defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING && _CAN_PURCHASE_RECURRING_BILLING;
	}

	/**
	 * @return bool
	 */
	public function canActivateRecurringBilling()
	{
		return defined('_CAN_ENABLE_RECURRING_BILLING') && _CAN_ENABLE_RECURRING_BILLING;
	}

	/**
	 * Pre-activate action
	 */
	public function preActivateAction()
	{
		$adminView = $this->getView();

		$licenseParts = explode('-', LICENSE_NUMBER);
		$licenseProductId = $licenseParts[2];

		$upgradeUrl = 'https://account.'.getp().'.com/checkout.php?action=checkout/buy-now&sku='.($licenseProductId == 6 ? 'licensed-recurring-billing' : 'hosted-recurring-billing' ).'&license_id='.$licenseParts[1].'&cart_url='.base64_encode($this->getSettings()->get('GlobalHttpsUrl'));
		if (Admin_HostedHelper::isHosted())
		{
			//TODO:
		}

		$adminView->assign('upgradeUrl', $upgradeUrl);

		$adminView->assign('recurringBillingMethodsHtml', $this->getRecurringBillingMethodsHtml());
	}

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$adminView = $this->getView();
		$adminView->assign('recurringBillingMethodsHtml', $this->getRecurringBillingMethodsHtml());

		$adminView->assign('helpTag', '9030');

		$adminView->assign('body', 'templates/pages/recurring-profiles/app-index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get recurring billing methods HTML
	 *
	 * @return string
	 */
	protected function getRecurringBillingMethodsHtml()
	{
		$settings = DataAccess_SettingsRepository::getInstance();
		$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($this->getDb(), $settings);

		$recurringPaymentMethods = $paymentMethodRepository->getRecurringPaymentMethods();
		$recurringBillingMethodsHtml = '';

		/** @var Payment_Method $recurringPaymentMethod */
		foreach ($recurringPaymentMethods as $recurringPaymentMethod)
		{
			$recurringBillingMethodsHtml .=
				'<li><a href="admin.php?p=payment_method&mode=update&payment_method_id='.$recurringPaymentMethod->getCode().'">'.
				$recurringPaymentMethod->getName().' '.($recurringPaymentMethod->getIsActive() ? '(active)' : '(not active)').
				'</a></li>';
		}

		return $recurringBillingMethodsHtml;
	}

	/**
	 * Activate recurring billing
	 */
	public function activateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('RecurringBillingEnabled' => '1'));

		if ($settings->get('AllowCreateAccount') == 'No')
		{
			$settings->save(array('AllowCreateAccount' => 'Allow user to decide'));
			Admin_Flash::setMessage('Please note: system made changes in Order/Cart settings and set "Allow Create Account" to "Allow user to decide" because user account is required for recurring payments', Admin_Flash::TYPE_INFO);
		}

		$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($this->getDb(), $settings);
		$recurringPaymentMethods = $paymentMethodRepository->getRecurringPaymentMethods();
		$activeRecurringPaymentMethodExists = false;

		/** @var Payment_Method $recurringPaymentMethod */
		foreach ($recurringPaymentMethods as $recurringPaymentMethod)
		{
			if ($recurringPaymentMethod->getIsActive()) $activeRecurringPaymentMethodExists = true;
		}

		if (!$activeRecurringPaymentMethodExists)
		{
			Admin_Flash::setMessage('Please note: there are no payment gateways enabled that support recurring billing', Admin_Flash::TYPE_ERROR);
		}

		if (array_key_exists('firstdata', $recurringPaymentMethods)) $settings->save(array('firstdata_Payment_Profiles' => 'Yes'));
	}

	/**
	 * Deactivate recurring billing
	 */
	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('RecurringBillingEnabled' => '0'));
	}

	/**
	 * List recurring profiles
	 */
	public function listAction()
	{
		$settings = $this->getSettings();
		$request = $this->getRequest();
		$session = $this->getSession();

		// TODO: get rid og global dependency here
		global $msg;
		require_once($settings->get('GlobalServerPath').'/content/languages/'.escapeFileName($settings->get('ActiveLanguage')).'.php');

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('recurringProfilesSearchParams', array()); else $session->set('recurringProfilesSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('recurringProfilesSearchOrderBy', 'profile_id'); else $session->set('recurringProfilesSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('recurringProfilesSearchOrderDir', 'desc'); else $session->set('recurringProfilesSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		/**
		 * Get data from DB
		 */
		$repository = $this->getRepository();
		$itemsCount = $repository->getCount($searchParams);

		/**
		 * Show results
		 */
		$adminView = $this->getView();
		$recurringProfileSearchForm = new RecurringBilling_Admin_Form_RecurringProfileSearchForm();
		$adminView->assign('searchForm', $recurringProfileSearchForm->getForm($searchParams));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);

			$recurringProfiles = $repository->getList(
				$paginator->sqlOffset,
				$paginator->itemsPerPage,
				$orderBy . '_' . $orderDir,
				$searchParams,
				'o.*', $searchParams['logic']
			);

			$recurringProfilesViews = array();
			foreach ($recurringProfiles as $key => $recurringProfile)
			{
				$recurringProfilesViews[$key] = new RecurringBilling_View_RecurringProfileView($recurringProfile, $msg, true);
			}

			$adminView->assign('recurringProfiles', $recurringProfilesViews);
		}
		else
		{
			$adminView->assign('recurringProfiles', null);
		}

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '2002');

		$recurringProfileUpdateStatusForm = new RecurringBilling_Admin_Form_RecurringProfileBulkUpdateStatusForm();
		$adminView->assign('updateStatusForm', $recurringProfileUpdateStatusForm->getForm());

		$adminView->assign('body', 'templates/pages/recurring-profiles/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * View / update recurring profile
	 */
	public function detailsAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();
		$settingsValues = $settings->getAll();
		$db = $this->getDb();

		// get recurring profile
		$id = intval($request->get('id', 0));

		$recurringProfileRepository = $this->getRepository();
		$recurringProfile = $recurringProfileRepository->get($id);

		if (is_null($recurringProfile))
		{
			Admin_Flash::setMessage('Cannot find recurring profile by provided ID', Admin_Flash::TYPE_ERROR);
			$this->redirect('recurring_profiles');
			return;
		}

		// get related line line item
		$orderRepository = new DataAccess_OrderRepository($db, $settingsValues);
		$lineItemData = $orderRepository->getLineItemById($recurringProfile->getInitialOrderLineItemId());

		if (is_null($lineItemData) || !$lineItemData)
		{
			Admin_Flash::setMessage('Cannot find related recurring item', Admin_Flash::TYPE_ERROR);
			$this->redirect('recurring_profiles');
			return;
		}

		$lineItem = new Model_LineItem($lineItemData);
		$productRecurringBillingData = $lineItem->getProductRecurringBillingData();

		$adminCanAdjustTrial = $productRecurringBillingData->getAdminCanAdjustTrial();
		$adminCanAdjustBilling = $productRecurringBillingData->getAdminCanAdjustBilling();

		// get payment profile
		$paymentProfileRepository = $this->get('repository_payment_profile');
		$paymentProfile = $paymentProfileRepository->getById($recurringProfile->getPaymentProfileId());

		//$addressFormBuilder = new Admin_Form_AddressForm($settings);
		$recurringProfileFormBuilder = new RecurringBilling_Admin_Form_RecurringProfileForm($settings);

		if ($request->isPost())
		{
			$formData = $request->request->get('recurring_profile', array());

			// TODO: validate data here
			$recurringProfile->hydrateFromForm($formData);

			if (isset($formData['status']) && trim($formData['status']) != '')
			{
				$newStatus = $formData['status'];

				if ($recurringProfile->getStatus() != $newStatus)
				{
					if (!$this->updateStatus($recurringProfile, $newStatus))
					{
						Admin_Flash::setMessage('Cannot change recurring profile status', Admin_Flash::TYPE_ERROR);
					}
				}
			}

			$recurringProfileRepository->persist($recurringProfile);

			// TODO: add on recurring profile change event here?
			Admin_Flash::setMessage('common.success');
		}

		/** @var DataAccess_UserRepository $userRepository */
		$userRepository = new DataAccess_UserRepository($db, $settingsValues);

		/** @var core_Form_Form $recurringProfileForm */
		$recurringProfileForm = $recurringProfileFormBuilder->getForm($recurringProfile, $adminCanAdjustTrial, $adminCanAdjustBilling);

		/**
		 * Render view
		 */
		$adminView = $this->getView();

		// Load messages
		global $msg;
		require_once($settings->get('GlobalServerPath').'/content/languages/'.escapeFileName($settings->get('ActiveLanguage')).'.php');

		$adminView->assign('nonce', Nonce::create('recurring_profile_update'));
		$adminView->assign('recurringProfile', new RecurringBilling_View_RecurringProfileView($recurringProfile, $msg, true));
		$adminView->assign('paymentProfile', !is_null($paymentProfile) ? new PaymentProfiles_View_PaymentProfileView($paymentProfile) : null);
		$adminView->assign('user', $userRepository->getById($recurringProfile->getUserId()));
		$adminView->assign('initialOrder', $orderRepository->getOrderData($recurringProfile->getInitialOrderId()));
		$adminView->assign('lineItem', new View_LineItemViewModel($lineItem));
		$adminView->assign('recurringOrders', $recurringProfileRepository->getOrdersForProfile($recurringProfile->getProfileId()));
		$adminView->assign('recurringProfileForm', $recurringProfileForm);

		if ($lineItem->getProductType() == Model_Product::TANGIBLE)
		{
			$shippingAddress = $recurringProfile->getShippingAddress();

			if ($shippingAddress && !is_null($shippingAddress))
			{
				$adminView->assign('shippingAddress', new View_AddressViewModel($shippingAddress));
			}
		}

		$countriesRepository = new DataAccess_CountryRepository($db);
		$countriesStates = $countriesRepository->getCountriesStates();
		$adminView->assign('countriesStates', json_encode($countriesStates));

		$canActivate = $recurringProfile->canActivate();
		$canSuspend = $recurringProfile->canSuspend();
		$canCancel = $recurringProfile->canCancel();

		if ($canActivate || $canSuspend || $canCancel)
		{
			$recurringProfileUpdateStatusForm = new RecurringBilling_Admin_Form_RecurringProfileUpdateStatusForm();
			$adminView->assign('updateStatusForm', $recurringProfileUpdateStatusForm->getForm($canActivate, $canSuspend, $canCancel));
			$adminView->assign('canUpdateStatus', true);
		}
		else
		{
			$adminView->assign('canUpdateStatus', false);
		}

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '2003');

		$adminView->assign('body', 'templates/pages/recurring-profiles/details.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Bulk update profiles
	 */
	public function bulkStatusAction()
	{
		$request = $this->getRequest();

		$updateMode = $request->get('updateMode');
		$newStatus = $request->get('status');

		// check status
		if (!in_array($newStatus, array(RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE, RecurringBilling_Model_RecurringProfile::STATUS_CANCELED, RecurringBilling_Model_RecurringProfile::STATUS_SUSPENDED)))
		{
			Admin_Flash::setMessage('Please select new status for recurring profiles', Admin_Flash::TYPE_ERROR);
			$this->redirect('recurring_profiles');
		}

		// select profiles
		$profiles = null;

		$repository = $this->getRepository();

		if ($updateMode == 'selected')
		{
			$profileIds = explode(',', $request->request->get('ids', ''));

			if (count($profileIds) > 0)
			{
				$profiles = $repository->getByIds($profileIds);
			}
		}
		else
		{
			$session = $this->getSession();
			$searchParams = array_merge($this->searchParamsDefaults, $session->get('recurringProfilesSearchParams', array()));
			$profiles = $repository->getList(null, null, null, $searchParams);
		}

		if ($profiles !== null && is_array($profiles) && count($profiles))
		{
			$updateError = false;
			$updated = false;
			foreach ($profiles as $profile)
			{
				/** @var RecurringBilling_Model_RecurringProfile $profile */
				$ret = $this->updateStatus($profile, $newStatus);
				if ($ret)
				{
					$updated = true;
				}
				else
				{
					$updateError = true;
				}
			}

			if ($updateError)
			{
				Admin_Flash::setMessage('At least one recurring profile was not updated', Admin_Flash::TYPE_ERROR);
			}

			if ($updated)
			{
				Admin_Flash::setMessage('common.success');
			}
		}
		else
		{
			Admin_Flash::setMessage('No recurring profiles selected', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('recurring_profiles');
	}

	/**
	 * Get recurring profile repository
	 *
	 * @return RecurringBilling_DataAccess_RecurringProfileRepository
	 */
	protected function getRepository()
	{
		static $repository = null;

		if (is_null($repository))
		{
			$repository = new RecurringBilling_DataAccess_RecurringProfileRepository($this->getDb(), $this->getSettings());
		}

		return $repository;
	}

	/**
	 * Update recurring profile status
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $newStatus
	 *
	 * @return bool
	 */
	protected function updateStatus(RecurringBilling_Model_RecurringProfile $recurringProfile, $newStatus)
	{
		switch ($newStatus)
		{
			case RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE:
			{
				return $this->activateProfile($recurringProfile);
			}
			case RecurringBilling_Model_RecurringProfile::STATUS_SUSPENDED:
			{
				return $this->suspendProfile($recurringProfile);
			}
			case RecurringBilling_Model_RecurringProfile::STATUS_CANCELED:
			{
				return $this->cancelProfile($recurringProfile);
			}
		}

		return false;
	}

	/**
	 * Return shipping address form
	 */
	public function ajaxEditShippingAddressFormAction()
	{
		//TODO: add proper error handling
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$recurringProfileId = $request->getRequest()->getInt('recurringProfileId', 0);
		$actionSource = $request->getRequest()->get('actionSource', '');

		if ($recurringProfile = $this->getRepository()->get($recurringProfileId))
		{
			$adminView = $this->getView();
			$adminView->assign('recurringProfileId', $recurringProfileId);
			$adminView->assign('actionSource', $actionSource);

			// get shipping form
			$shippingAddressFormBuilder = new core_Form_FormBuilder('form-shipping-address');
			$addressForm = new Admin_Form_AddressForm($settings);
			$addressFormBuilder = $addressForm->getBuilder($recurringProfile->getShippingAddress(), Admin_Form_AddressForm::ADDRESS_MODE_SHIPPING, 'address.shipping_address');
			$shippingAddressFormBuilder->add($addressFormBuilder);

			$adminView->assign('shippingAddressForm', $shippingAddressFormBuilder->getForm());
			$response = array(
				'status' => 1,
				'html' => $adminView->fetch('pages/recurring-profiles/shipping-address-form')
			);
		}
		else
		{
			$response = array('status' => 0, 'message' => 'Cannot find recurring profile');
		}

		$this->renderJson($response);
	}

	/**
	 * Save shipping address
	 */
	public function ajaxSaveShippingAddressFormAction()
	{
		//TODO: add proper error handling
		$request = $this->getRequest();
		$settings = $this->getSettings();
		$db = $this->getDb();

		$recurringProfileRepository = $this->getRepository();

		try
		{
			$recurringProfileId = $request->getRequest()->getInt('recurringProfileId', 0);
			$actionSource = $request->getRequest()->get('actionSource', '');

			$recurringProfile = $recurringProfileRepository->get($recurringProfileId);

			if (is_null($recurringProfile)) throw new Framework_Exception('Canno find recurring profile by id');

			// get data from input
			$shippingAddressData = $request->get('shipping_address', array());

			// validate address
			$settingsValues = $settings->getAll();
			$shippingAddressErrors = AddressValidator::validateInput($db, $settingsValues, Model_Address::ADDRESS_SHIPPING, $shippingAddressData, null);

			// check for errors
			if (count($shippingAddressErrors) > 0) throw new Framework_Exception('Please fix errors', $shippingAddressErrors);

			// save changes
			$shippingAddress = $recurringProfile->getShippingAddress();
			$shippingAddress->hydrateFromArray(array_merge($shippingAddress->getArrayDefaults(), $shippingAddressData));

			// TODO: should not be here, address must be hydrate somewhere else (in persist?)
			$addressRepository = new DataAccess_AddressRepository($db);
			$addressRepository->hydrateAddress($shippingAddress);

			$recurringProfileRepository->persist($recurringProfile);

			// render new view
			$adminView = $this->getView();
			$adminView->assign('updateStatusForm', $this->getUpdateStatusForm()->getForm());
			$adminView->assign('shippingAddress', new View_AddressViewModel($shippingAddress));

			$response = array(
				'status' => 1,
				'message' => 'Shipping address has been updated',
				'html' => $adminView->fetch('pages/recurring-profiles/shipping-address-view')
			);
		}
		catch (Framework_Exception $e)
		{
			$response = array('status' => 0, 'message' => $e->getMessage());

			$errorDetails = $e->getDetails();

			if (count($errorDetails) > 0)
			{
				$fieldsErrors = array();

				foreach ($errorDetails as $key => $value) $fieldsErrors['shipping_address['.$key.']'] = $value;

				$response['errors'] = $fieldsErrors;
			}
		}

		$this->renderJson($response);
	}

	/**
	 * Activate recurring profile
	 *
	 * @param RecurringBilling_Model_RecurringProfile $profile
	 *
	 * @return bool
	 */
	protected function activateProfile(RecurringBilling_Model_RecurringProfile $profile)
	{
		if ($profile->canActivate())
		{
			$profile->activate();

			return true;
		}

		return false;
	}

	/**
	 * Suspend recurring profile
	 *
	 * @param RecurringBilling_Model_RecurringProfile $profile
	 *
	 * @return bool
	 */
	protected function suspendProfile(RecurringBilling_Model_RecurringProfile $profile)
	{
		if ($profile->canSuspend())
		{
			$profile->suspend(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_ADMIN, '');

			return true;
		}

		return false;
	}

	/**
	 * Cancel recurring profile
	 *
	 * @param RecurringBilling_Model_RecurringProfile $profile
	 *
	 * @return bool
	 */
	protected function cancelProfile(RecurringBilling_Model_RecurringProfile $profile)
	{
		if ($profile->canCancel())
		{
			$profile->cancel();

			return true;
		}

		return false;
	}
}
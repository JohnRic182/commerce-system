<?php
/**
 * Class RecurringBilling_Front_Controller_RecurringProfile
 */
class RecurringBilling_Front_Controller_RecurringProfile extends Front_Controller
{
	/**
	 * List users profiles
	 */
	public function listAction()
	{
		global $msg;

		/** @var USER $user */
		$user = $this->getCurrentUser();

		/** @var Ddm_View $view */
		$view = $this->getView();

		if (!$user->isAuthorized())
		{
			$view->assign('body', 'templates/pages/account/auth-error.html');
			return;
		}

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$profileSearch = $request->get('profileSearch', false);

		if (!is_array($profileSearch))
		{
			$profileSearch = array('status' => 'any');
		}

		$profileSearch['user_id'] = $user->getId();

		/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository */
		$recurringProfileRepository = $this->getRepository();

		$pg = $request->query->get('pg', 1);
		$results = $recurringProfileRepository->search($profileSearch, $pg, 20);

		if ($results['count'] > 0)
		{
			$view->assign('recurringProfiles', RecurringBilling_View_RecurringProfileView::getRecurringProfilesView($results['items'], $msg, false));
		}
		$view->assign("paginator", 
			$paginate = paginate(
				$pg, //current page
				$results['pages_count'],
				20, // page size
				'index.php?p=recurring_profiles'
			)
		);
		
		// TODO: should return view model
		$view->assign('results', $results);
		$view->assign('body', 'templates/pages/account/recurring-profiles.html');
	}

	/**
	 * View recurring profile details
	 */
	public function detailsAction()
	{
		global $msg;

		/** @var USER $user */
		$user = $this->getCurrentUser();

		/** @var Ddm_View $view */
		$view = $this->getView();

		if (!$user->isAuthorized())
		{
			$view->assign('body', 'templates/pages/account/auth-error.html');
			return;
		}

		/** @var DB $db */
		$db = $this->getDb();

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_SettingsRepository $settings */
		$settings = $this->getSettings();
		$settingsValues = $settings->getAll();

		/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository */
		$recurringProfileRepository = $this->getRepository();

		$profileId = $request->query->getInt('id', 0);

		/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
		$recurringProfile = $recurringProfileRepository->get($profileId, $user->getId());

		if (is_null($recurringProfile)) $this->redirect('404');

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->get('repository_payment_profile');

		/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
		$paymentProfile = $paymentProfileRepository->getById($recurringProfile->getPaymentProfileId(), $user->getId());

//		if (is_null($paymentProfile))
//		{
//			// TODO: show proper error message
//			$this->redirect('404');
//		}

		/** @var DataAccess_OrderRepository $orderRepository */
		$orderRepository = new DataAccess_OrderRepository($db, $settingsValues);

		/** @var array $lineItemData */
		$lineItemData = $orderRepository->getLineItemById($recurringProfile->getInitialOrderLineItemId());

		if (!$lineItemData)
		{
			// TODO: set redirect to 404 here
			$this->redirect('404');
		}

		/** @var Model_LineItem $lineItem */
		$lineItem = new Model_LineItem($lineItemData);

		/** @var RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData */
		$productRecurringBillingData = $lineItem->getProductRecurringBillingData();

		$customerCanActivate =
			!is_null($paymentProfile) &&
			$recurringProfile->getStatus() != RecurringBilling_Model_RecurringProfile::STATUS_CANCELED &&
			$recurringProfile->canActivate() && $productRecurringBillingData->getCustomerCanSuspend();
		$customerCanSuspend = $recurringProfile->canSuspend() && $productRecurringBillingData->getCustomerCanSuspend();
		$customerCanCancel = $recurringProfile->canCancel() && $productRecurringBillingData->getCustomerCanCancel();

		$itemShippable = true;

		/** @var array $messages */
		$messages = array();

		/**
		 * Handle changes
		 * TODO: should be ajax?
		 * TODO: should be separated actions?
		 */
		if ($request->isPost())
		{
			$action = $request->getRequest()->get('action');

			switch ($action)
			{
				// set new profile status
				case 'change-profile-status' :
				{
					$newStatus = $request->getRequest()->get('profile_new_status', '');
					$newStatusSet = false;

					//TODO: Check activate,suspend,cancel functions for correct event firings
					switch ($newStatus)
					{
						case RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE:
						{
							if ($customerCanActivate)
							{
								$recurringProfile->activate();
								$newStatusSet = true;
							}


							break;
						}
						case RecurringBilling_Model_RecurringProfile::STATUS_SUSPENDED:
						{
							if ($customerCanSuspend)
							{
								$recurringProfile->suspend(RecurringBilling_Model_RecurringProfile::SUSPENSION_REASON_USER);
								$newStatusSet = true;
							}
							break;
						}
						case RecurringBilling_Model_RecurringProfile::STATUS_CANCELED:
						{
							if ($customerCanCancel)
							{
								$recurringProfile->cancel();
								$newStatusSet = true;
							}
							break;
						}
					}

					if ($newStatusSet)
					{
						$messages[] = $msg['recurring']['recurring_status_changed'];
						$recurringProfileRepository->persist($recurringProfile);
					}

					break;
				}

				// set new payment profile
				case 'change-payment-profile' :
				{
					$paymentProfileId = $request->getRequest()->getInt('payment_profile_id', 0);

					/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
					$paymentProfile = $paymentProfileRepository->getById($paymentProfileId, $user->getId());

					if (is_null($paymentProfile))
					{
						// TODO: show proper error message
						$this->redirect('404');
					}

					$recurringProfile->setPaymentProfileId($paymentProfile->getId());
					$recurringProfileRepository->persist($recurringProfile);

					break;
				}

				// update shipping address
				case 'update-shipping-address' :
				{
					// get data from input
					$shippingAddressData = $request->getRequest()->get('form', array());

					// validate address
					$shippingAddressErrors = AddressValidator::validateInput($db, $settingsValues, Model_Address::ADDRESS_SHIPPING, $shippingAddressData, null);

					$shippingAddress = $recurringProfile->getShippingAddress();
					$shippingAddress->hydrateFromArray(array_merge($shippingAddress->getArrayDefaults(), $shippingAddressData));

					// TODO: should not be here, address must be hydrate somewhere else (in persist?)
					$addressRepository = new DataAccess_AddressRepository($db);
					$addressRepository->hydrateAddress($shippingAddress);

					// check for errors
					if (count($shippingAddressErrors) > 0)
					{
						$messages = $shippingAddressErrors;
					}
					else
					{
						// save changes
						$recurringProfileRepository->persist($recurringProfile);
					}

					break;
				}
			}

			// reupdate statuses
			$customerCanActivate =
				$recurringProfile->getStatus() != RecurringBilling_Model_RecurringProfile::STATUS_CANCELED &&
				$recurringProfile->canActivate() && $productRecurringBillingData->getCustomerCanSuspend();
			$customerCanSuspend = $recurringProfile->canSuspend() && $productRecurringBillingData->getCustomerCanSuspend();
			$customerCanCancel = $recurringProfile->canCancel() && $productRecurringBillingData->getCustomerCanCancel();
		}

		$paymentProfiles = $paymentProfileRepository->getByUserId($user->getId(), true, true);

		/**
		 * Render view
		 */
		$view->assign('messages', $messages);

		$view->assign('profile', new RecurringBilling_View_RecurringProfileView($recurringProfile, $msg));
		$view->assign('lineItem', new View_LineItemViewModel($lineItem));
		if ($paymentProfile && !is_null($paymentProfile))
		{
			$view->assign('paymentProfile', new PaymentProfiles_View_PaymentProfileView($paymentProfile));
		}
		$view->assign('paymentProfiles', PaymentProfiles_View_PaymentProfileView::getPaymentProfilesView($paymentProfiles));
		$view->assign('initialOrder', $orderRepository->getOrderData($recurringProfile->getInitialOrderId()));
		$view->assign('orders', $recurringProfileRepository->getOrdersForProfile($recurringProfile->getProfileId()));

		$view->assign('customerCanActivate', $customerCanActivate);
		$view->assign('customerCanSuspend', $customerCanSuspend);
		$view->assign('customerCanCancel', $customerCanCancel);
		$view->assign('itemShippable', $itemShippable);

		$view->assign('profileStatusActive', RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE);
		$view->assign('profileStatusSuspended', RecurringBilling_Model_RecurringProfile::STATUS_SUSPENDED);
		$view->assign('profileStatusCancelled', RecurringBilling_Model_RecurringProfile::STATUS_CANCELED);

		if ($lineItem->getProductType() == Model_Product::TANGIBLE)
		{
			$shippingAddress = $recurringProfile->getShippingAddress();

			if ($shippingAddress && !is_null($shippingAddress))
			{
				$view->assign('shippingAddress', new View_AddressViewModel($shippingAddress));
			}
		}

		$months = array();
		for ($k = 1; $k <= 12; $k++)
		{
			$months[($k < 10 ? '0'.$k : $k)] = $k;
		}
		$view->assign('cc_expiration_months', $months);

		$years = array();
		for ($k = date("Y"); $k <= date("Y")+10; $k++)
		{
			$years[] = $k;
		}
		$view->assign('cc_expiration_years', $years);

		//country / state data
		$regions = new Regions($this->getDb());
		$view->assign("countries_states", json_encode($regions->getCountriesStatesForSite()));

		$view->assign('body', 'templates/pages/account/recurring-profile.html');
	}

	/**
	 * Get recurring profile repository
	 *
	 * @return RecurringBilling_DataAccess_RecurringProfileRepository
	 */
	protected function getRepository()
	{
		static $repository = null;

		if (is_null($repository))
		{
			$repository = new RecurringBilling_DataAccess_RecurringProfileRepository($this->getDb(), $this->getSettings());
		}

		return $repository;
	}
}

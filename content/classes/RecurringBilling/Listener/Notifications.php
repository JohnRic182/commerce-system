<?php
/**
 * Class RecurringBilling_Listener_Notifications
 */
class RecurringBilling_Listener_Notifications
{
	/** @var DB $db */
	protected $db;

	/** @var DataAccess_SettingsRepository $settings */
	protected $settings;

	/** @var DataAccess_UserRepository $userRepository */
	protected $userRepository;

	/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringRepository */
	protected $recurringRepository;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_UserRepository $userRepository
	 * @param DataAccess_SettingsRepository $settings
	 * @param RecurringBilling_DataAccess_RecurringProfileRepository $recurringRepository
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings, DataAccess_UserRepository $userRepository, RecurringBilling_DataAccess_RecurringProfileRepository $recurringRepository)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->userRepository = $userRepository;
		$this->recurringRepository = $recurringRepository;
	}

	/**
	 * Handle on recurring billing transaction fail event
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 */
	public function onTransactionFailed(RecurringBilling_Events_RecurringBillingEvent $event)
	{
		/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
		$recurringProfile = $event->getRecurringProfile();

		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'failure'),
			'templates/emails/recurring_profile_transaction_failed',
			array('transaction_response' => $event->getData('transaction_response')),
			'failure', array('all', 'recurring')
		);
	}

	/**
	 * Handle on card expiration event
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 */
	public function onCardExpiration(RecurringBilling_Events_RecurringBillingEvent $event)
	{
		/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
		$recurringProfile = $event->getRecurringProfile();

		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'expire'),
			'templates/emails/recurring_profile_card_expiration',
			array(),
			'expire',
			array('all', 'recurring')
		);

		$recurringProfile->setNotifiedCCExpire(true);

		$this->recurringRepository->persist($recurringProfile);
	}

	/**
	 * Handle on due to complete event
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 */
	public function onDueToComplete(RecurringBilling_Events_RecurringBillingEvent $event)
	{
		/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
		$recurringProfile = $event->getRecurringProfile();

		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'one_remaining'),
			'templates/emails/recurring_profile_due_completion',
			array(),
			'one_remaining',
			array('all', 'recurring')
		);
	}

	/**
	 * Handle on status change event
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 */
	public function onStatusChanged(RecurringBilling_Events_RecurringBillingEvent $event)
	{
		/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
		$recurringProfile = $event->getRecurringProfile();

		/** @var status $oldStatus */
		$oldStatus = $event->getData('oldStatus');

		/** @var status $newStatus */
		$newStatus = $event->getData('newStatus');

		if ($oldStatus == RecurringBilling_Model_RecurringProfile::STATUS_PENDING && $newStatus == RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE)
		{
			$this->onCreated($event, $recurringProfile);
		}
		else if ($newStatus == RecurringBilling_Model_RecurringProfile::STATUS_ACTIVE && ($oldStatus == RecurringBilling_Model_RecurringProfile::STATUS_SUSPENDED || $oldStatus == RecurringBilling_Model_RecurringProfile::STATUS_CANCELED))
		{
			$this->onReactivated($event, $recurringProfile);
		}
		else if ($newStatus == RecurringBilling_Model_RecurringProfile::STATUS_CANCELED)
		{
			$this->onCanceled($event, $recurringProfile);
		}
		else if ($newStatus == RecurringBilling_Model_RecurringProfile::STATUS_SUSPENDED)
		{
			$this->onSuspended($event, $recurringProfile);
		}
		else if ($newStatus == RecurringBilling_Model_RecurringProfile::STATUS_COMPLETED)
		{
			$this->onCompleted($event, $recurringProfile);
		}
	}

	/**
	 * Handle completed event
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	protected function onCompleted(RecurringBilling_Events_RecurringBillingEvent $event, RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'completed'),
			'templates/emails/recurring_profile_completed',
			array(),
			'completed',
			array('all', 'recurring')
		);
	}

	/**
	 * Handle new profile created
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	protected function onCreated(RecurringBilling_Events_RecurringBillingEvent $event, RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'created'),
			'templates/emails/recurring_profile_created',
			array(),
			'created',
			array('all', 'recurring')
		);
	}

	/**
	 * Handle status change - cancel
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	protected function onCanceled(RecurringBilling_Events_RecurringBillingEvent $event, RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'canceled'),
			'templates/emails/recurring_profile_canceled',
			array(),
			'canceled',
			array('all', 'recurring')
		);
	}

	/**
	 * Handle status change - suspended
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	protected function onSuspended(RecurringBilling_Events_RecurringBillingEvent $event, RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'suspended'),
			'templates/emails/recurring_profile_suspended',
			array(),
			'suspended',
			array('all', 'recurring')
		);
	}

	/**
	 * Handle status change - reactivated
	 *
	 * @param RecurringBilling_Events_RecurringBillingEvent $event
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 */
	protected function onReactivated(RecurringBilling_Events_RecurringBillingEvent $event, RecurringBilling_Model_RecurringProfile $recurringProfile)
	{
		$this->sendNotification(
			$recurringProfile,
			$this->getSubject($recurringProfile, 'reactivated'),
			'templates/emails/recurring_profile_reactivated',
			array(),
			'reactivated',
			array('all', 'recurring')
		);
	}

	/**
	 * Get message subject
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $subjectKey
	 * @return mixed
	 */
	protected function getSubject(RecurringBilling_Model_RecurringProfile $recurringProfile, $subjectKey)
	{
		global $msg;

		return str_replace(
			'%profile_id',
			$recurringProfile->getProfileId(),
			isset($msg['email']['subject_recurring_billing_'.$subjectKey]) ? $msg['email']['subject_recurring_billing_'.$subjectKey] : 'Recurring profile #%profile_id status update message'
		);
	}

	/**
	 * TODO: move this in external class / function
	 *
	 * @param RecurringBilling_Model_RecurringProfile $recurringProfile
	 * @param $messageSubject
	 * @param $templatePrefix
	 * @param $viewParams
	 * @param $notificationKey
	 * @param array $adminPrivileges
	 */
	protected function sendNotification(RecurringBilling_Model_RecurringProfile $recurringProfile, $messageSubject, $templatePrefix, $viewParams, $notificationKey, $adminPrivileges = array('all'))
	{
		global $msg;

		/** @var DataAccess_SettingsRepository $settings */
		$settings = $this->settings;

		$settingsValues = $settings->getAll();

		if (!isset($msg) || $msg === null)
		{
			require_once($settingsValues['GlobalServerPath'].'/content/languages/'.escapeFileName($settingsValues['ActiveLanguage']).".php");
		}

		/** @var DB $db */
		$db = $this->db;

		/** @var Model_LineItem $lineItem */
		$lineItem = $recurringProfile->getLineItem();

		/** @var RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData */
		$productRecurringBillingData = $lineItem->getProductRecurringBillingData();

		$customerNotifications = $productRecurringBillingData->getCustomerNotifications();
		$adminNotifications = $productRecurringBillingData->getAdminNotifications();

		$sendUserEmails = in_array($notificationKey, $customerNotifications);
		$sendAdminEmails = in_array($notificationKey, $adminNotifications);

		if (!$sendUserEmails && !$sendAdminEmails) return;

		$user = $this->getUser($recurringProfile->getUserId());

		// TODO: add more advanced error handling handling here
		if (!$user || is_null($user)) return;

		foreach ($viewParams as $key => $value)
		{
			view()->assign($key, $value);
		}

		view()->assign('EmailTitle', 'Subscription Details');
		view()->assign('profile', new RecurringBilling_View_RecurringProfileView($recurringProfile, $msg, true));
		view()->assign('msg', $msg);

		$order = $this->getOrder($recurringProfile->getInitialOrderId(), $user);
		$orderViewModel = new View_OrderViewModel($settingsValues, $msg);
		$orderViewModel->build($db, $order);
		view()->assign('order', $orderViewModel);
		view()->assign('lineItem', new View_LineItemViewModel($order->lineItems[$recurringProfile->getInitialOrderLineItemId()]));

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->get('repository_payment_profile');

		/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
		$paymentProfile = $paymentProfileRepository->getById($recurringProfile->getPaymentProfileId(), $user->getId());

		if ($paymentProfile && !is_null($paymentProfile))
		{
			view()->assign('paymentProfile', new PaymentProfiles_View_PaymentProfileView($paymentProfile));
		}

		if ($lineItem->getProductType() == Model_Product::TANGIBLE)
		{
			$shippingAddress = $recurringProfile->getShippingAddress();

			if ($shippingAddress && !is_null($shippingAddress))
			{
				view()->assign('shippingAddress', new View_AddressViewModel($recurringProfile->getShippingAddress()));
			}
		}

		$bodyHtml = view()->fetch($templatePrefix.'_html.html');

		if ($sendUserEmails)
		{
			$body = $bodyHtml;
			Notifications::sendMail($user->data['email'], stripslashes($settings->get('CompanyName').': '.$messageSubject), $body, '');
		}

		if ($sendAdminEmails)
		{
			$rights = "rights LIKE '%all%'";
			foreach ($adminPrivileges as $ap)
			{
				if ($ap != 'all')
				{
					$rights .= " OR rights LIKE '%".$ap."'";
				}
			}

			$result = $this->db->query('SELECT * FROM '.DB_PREFIX."admins WHERE active='Yes' AND (".$rights.") AND (receive_notifications LIKE '%recurring%')");

			while ($row = $this->db->moveNext($result))
			{
				$body = $bodyHtml;

				Notifications::sendMail($row['email'], stripslashes($settings->get('CompanyName').': '.$messageSubject), $body, '');
			}
		}
	}

	protected function get($key)
	{
		global $registry;

		return $registry->get($key);
	}

	/**
	 * Get user account
	 *
	 * @param $userId
	 * @return array|null|USER
	 */
	protected function getUser($userId)
	{
		// TODO: get rid of global dependency here
		global $user;

		if (!is_null($user) && $user->getId() == $userId)
		{
			return $user;
		}

		return $this->userRepository->getById($userId);
	}

	protected function getOrder($orderId, USER $user)
	{
		global $order;
		if ($order !== null && $order->getId() == $orderId)
		{
			return $order;
		}

		$settingsValues = $this->settings->getAll();
		$order = ORDER::getById($this->db, $settingsValues, $user, $orderId);
		$order->getOrderData();
		$order->getOrderItems();
		return $order;
	}
}
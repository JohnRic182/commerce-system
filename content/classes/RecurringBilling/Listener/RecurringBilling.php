<?php

/**
 * Class RecurringBilling_Listener_RecurringBilling
 */
class RecurringBilling_Listener_RecurringBilling
{
	/**
	 * @param Events_Event $event
	 */
	public static function onUserRemoved(Events_Event $event)
	{
		global $db;

		$uid = $event->getData('uid');

		/** @var DataAccess_SettingsRepository $settingsRepository */
		$settingsRepository = DataAccess_SettingsRepository::getInstance();

		/** @var RecurringBilling_DataAccess_RecurringProfileRepository $repository */
		$recurringProfileRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $settingsRepository);

		$recurringProfiles = $recurringProfileRepository->getByUserId($uid);
		/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
		foreach ($recurringProfiles as $recurringProfile)
		{
			if (
				$recurringProfile->getStatus() == RecurringBilling_Model_RecurringProfile::STATUS_CANCELED ||
				$recurringProfile->getStatus() == RecurringBilling_Model_RecurringProfile::STATUS_COMPLETED
			)
			{
				continue; //Ignore Canceled or Completed
			}

			$recurringProfile->cancel(false, 'Account removed');
			$recurringProfileRepository->persist($recurringProfile);
		}
	}

	/**
	 * On remove payment profile, ensure that no recurring profiles are relying on the payment profile
	 *
	 * @param PaymentProfiles_Events_PaymentProfileEvent $event
	 */
	public static function onBeforeRemovePaymentProfile(PaymentProfiles_Events_PaymentProfileEvent $event)
	{
		global $registry, $msg;

		$paymentProfile = $event->getPaymentProfile();
		$paymentProfileId = $paymentProfile->getId();

		if ($paymentProfileId > 0)
		{
			$db = $registry->get('db');

			$result = $db->selectOne('
				SELECT count(*) as total
				FROM '.DB_PREFIX.'recurring_profiles
				WHERE
					payment_profile_id = '.intval($paymentProfileId).'
					AND status NOT IN ("'.(RecurringBilling_Model_RecurringProfile::STATUS_CANCELED).'", "'.(RecurringBilling_Model_RecurringProfile::STATUS_COMPLETED).'")
				'
			);

			if ($result && $result['total'] > 0)
			{
				$event->setData('canRemove', false);
				$event->stopPropagation(true);
				$event->setError(
					isset($msg['account']['payment_profile_used_by_recurring']) && $msg['account']['payment_profile_used_by_recurring'] != '' ?
					$msg['account']['payment_profile_used_by_recurring'] :
					'Cannot remove payment method because it is used for recurring payments'
				);
			}
		}
	}

	/**
	 * On payment profile update - reset cc expire notification flag
	 *
	 * @param PaymentProfiles_Events_PaymentProfileEvent $event
	 */
	public static function onUpdatePaymentProfile(PaymentProfiles_Events_PaymentProfileEvent $event)
	{
		global $registry;

		$paymentProfile = $event->getPaymentProfile();
		$paymentProfileId = $paymentProfile->getId();

		if ($paymentProfileId > 0)
		{
			$db = $registry->get('db');
			$db->query('UPDATE '.DB_PREFIX.'recurring_profiles SET notified_cc_expire=0 WHERE payment_profile_id='.intval($paymentProfileId));
		}
	}

	/**
	 * On find existing line item
	 *
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderFindExistingLineItem(Events_OrderEvent $event)
	{
		/** @var Model_LineItem $foundLineItem */
		$foundLineItem = $event->getData('item');

		/** @var RecurringBilling_Model_LineItemRecurringBillingData $foundLineItemRecurringBillingData */
		$foundLineItemRecurringBillingData = $foundLineItem->getRecurringBillingData();

		if ($foundLineItem->getEnableRecurringBilling() && !is_null($foundLineItemRecurringBillingData))
		{
			$customData = $event->getData('customData', array());

			if (isset($customData['recurring_start_date']) && trim($customData['recurring_start_date']) != '')
			{
				$recurringStartDate = new DateTime(trim($customData['recurring_start_date']));
				if ($recurringStartDate != $foundLineItemRecurringBillingData->getStartDate())
				{
					$event->setData('item', null);
					return;
				}
			}
		}
	}

	/**
	 * On add item to a cart
	 *
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderAddItem(Events_OrderEvent $event)
	{
		/** @var ORDER $order */
		$order = $event->getOrder();

		/** @var Model_LineItem $orderLineItem */
		$orderLineItem = $event->getData('item');

		if ($orderLineItem->getEnableRecurringBilling())
		{
			/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
			$recurringBillingData = $orderLineItem->getRecurringBillingData();

			if (!is_null($recurringBillingData))
			{
				/**
				 * Store custom data (start date, ...)
				 */
				$customData = $event->getData('customData', array());

				if (isset($customData['recurring_start_date']) && trim($customData['recurring_start_date']) != '')
				{
					$recurringStartDate = new DateTime(trim($customData['recurring_start_date']));
					$recurringBillingData->setStartDate($recurringStartDate);
				}

				/**
				 * Save changes
				 */
				$orderLineItem->setRecurringBillingData($recurringBillingData);
			}

			$orderRepository = DataAccess_OrderRepository::getInstance();
			$orderRepository->persistLineItem($order, $orderLineItem);
		}
	}

	/**
	 * Check are there new recurring orders and create recurring profiles from them
	 *
	 * @param Events_OrderEvent $event
	 */
	public static function onProcessCloseOrder(Events_OrderEvent $event)
	{
		// TODO: should not be global, remove global dependencies here
		global $db, $settings;

		/** @var ORDER $order */
		$order = $event->getOrder();

		if (
			$order->getOrderType() != ORDER::ORDER_TYPE_RECURRING
			&& $order->hasRecurringBillingItems()
			&& ($event->getData('oldOrderStatus') == ORDER::STATUS_ABANDON || $event->getData('oldOrderStatus') == ORDER::STATUS_PROCESS)
			&& $order->getStatus() == ORDER::STATUS_COMPLETED
		)
		{
			/** @var DataAccess_SettingsRepository $settingsRepository */
			$settingsRepository = DataAccess_SettingsRepository::getInstance();

			/** @var RecurringBilling_DataAccess_RecurringProfileRepository $repository */
			$recurringProfileRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $settingsRepository);

			/** @var Model_LineItem $item */
			foreach ($order->lineItems as $item)
			{
				if ($item->getEnableRecurringBilling())
				{
					/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
					$recurringProfile = $recurringProfileRepository->getByLineItemId($item->getId());

					if (is_null($recurringProfile))
					{
						/**
						 * Create new recurring profile and activate it
						 */
						$recurringProfile = RecurringBilling_Model_RecurringProfile::createFromLineItem($order, $item);
						$recurringProfileRepository->persist($recurringProfile);

						// change status from non-existing to pending
						RecurringBilling_Events_RecurringBillingEvent::dispatchStatusChangeEvent($recurringProfile, null);

						/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
						$paymentProfileRepository = new PaymentProfiles_DataAccess_PaymentProfileRepository($db);

						/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
						$paymentProfile = $paymentProfileRepository->getById($recurringProfile->getPaymentProfileId(), $recurringProfile->getUserId());

						if (!is_null($paymentProfile))
						{
							$recurringProfile->activate();
						}

						$recurringProfileRepository->persist($recurringProfile);
					}
				}
			}
		}
	}

	/**
	 * Process payment
	 *
	 * @param Events_OrderStatusEvent $event
	 */
	public static function onProcessPayment(Events_OrderStatusEvent $event)
	{
		/** @var ORDER $order */
		$order = $event->getOrder();

		if ($order->getOrderType() == ORDER::ORDER_TYPE_RECURRING || !$order->hasRecurringBillingItems())
		{
			return;
		}

		$orderStatus = $event->getOrderStatus();
		$paymentStatus = $event->getPaymentStatus();

		if ($order->getShippingRequired() || $paymentStatus != ORDER::PAYMENT_STATUS_RECEIVED || $orderStatus != ORDER::STATUS_COMPLETED)
		{
			return;
		}

		$autoCompleteInitialOrder = true;

		/** @var Model_LineItem $item */
		foreach ($order->lineItems as $item)
		{
			if ($item->getEnableRecurringBilling())
			{
				/** @var RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData */
				$productRecurringBillingData = $item->getProductRecurringBillingData();
				if (is_object($productRecurringBillingData) && !$productRecurringBillingData->getAutoCompleteInitialOrder())
				{
					$autoCompleteInitialOrder = false;
					break;
				}
			}
		}

		if (!$autoCompleteInitialOrder)
		{
			$order->setStatus(ORDER::STATUS_PROCESS);
			$event->setOrderStatus(ORDER::STATUS_PROCESS);
		}
	}
}
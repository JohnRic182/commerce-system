<?php

/**
 * Class RecurringBilling_Listener_RecurringBillingCron
 */
class RecurringBilling_Listener_RecurringBillingCron
{
	/**
	 * Check for profiles where cc will expire soon
	 *
	 * @param Events_CronEvent $event
	 */
	public static function onCheckCardsExpiration(Events_CronEvent $event)
	{
		// TODO: should not be global, remove global dependencies here
		global $db;

		/** @var DataAccess_SettingsRepository $settingsRepository */
		$settingsRepository = DataAccess_SettingsRepository::getInstance();

		/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository */
		$recurringProfileRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $settingsRepository);

		$recurringProfiles = $recurringProfileRepository->getProfilesWhereCardExpiresSoon();

		while ($recurringProfileData = $db->moveNext($recurringProfiles))
		{
			/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
			$recurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);
			$recurringProfile->creditCardAboutToExpire();
		}
	}

	/**
	 * Process recurring billing profiles
	 *
	 * @param Events_CronEvent $event
	 */
	public static function onProcessRecurringBilling(Events_CronEvent $event)
	{
		// TODO: should not be global, remove global dependencies here
		global $db, $settings;

		/** @var DataAccess_SettingsRepository $settingsRepository */
		$settingsRepository = DataAccess_SettingsRepository::getInstance();

		$settingsRepository->set('DisplayPricesWithTax', 'No');

		/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringProfileRepository */
		$recurringProfileRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $settingsRepository);
		$productsRepository = new DataAccess_ProductsRepository($db);
		$orderRepository = new DataAccess_OrderRepository($db, $settings);

		$provider = new RecurringBilling_Cron_Provider($db, $settings, $recurringProfileRepository,
			$productsRepository, $orderRepository,
			new RecurringBilling_Cron_RecurringOrderHelper($db, $settings),
			new RecurringBilling_Cron_Logger()
		);
		$provider->process();
	}
}
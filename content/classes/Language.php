<?php

/**
 * @class Language
 */
class Language
{
	protected $languages_dir = "content/languages/";
	protected $languages_cache = "content/cache/languages/";
	protected $languages_cache_url = "content/cache/languages/";
	
	public $dictionary = array();
	
	public $languages = array();
	public $active_languages = array();
	public $categories = array();
	public $templates = array();
	public $default_language_id = false;
	private $_db = null;
	private $_settings = null;

	public $languages_loaded = false;
	
	/**
	 * Return database class instance
	 * @return DB
	 */
	public function db()
	{
		return $this->_db;
	}
	
	/**
	 * Class constructor
	 * @param $db
	 * @return unknown_type
	 */
	public function __construct(&$db)
	{
		global $settings;

		$this->_db = $db;
		$this->_settings = $settings;
		$this->getLanguages();
		return $this;
	}
	
	/**
	 * Get language dictionary
	 * @param string $language_code
	 * @return array
	 */
	public function getDictionary($language_code)
	{
		if (!array_key_exists($language_code, $this->dictionary))
		{
			$language_code = escapeFileName($language_code);
			$msg = array();
			include_once($this->languages_dir.$language_code.".php");
			$this->dictionary[$language_code] = $msg;
			unset($msg);
		}
		return $this->dictionary[$language_code];
	}
	
	/**
	 * Generate language dictionary cache for javascript
	 * @param $language_code
	 * @return unknown_type
	 */
	public function getJavascriptCache($language_code)
	{
		$this->getDictionary($language_code);
		
		$language_file_name = escapeFileName($language_code);
		$language_file_path = $this->languages_dir.$language_file_name.".php";
		
		$file_php_stat = stat($language_file_path);
		$file_php_mdate = $file_php_stat["mtime"];
		
		$language_file_js_path = $this->languages_cache.$language_file_name.".js";
		
		//check is file was created before
		if (!is_file($language_file_js_path))
		{
			$regenerate_file = true;
		}
		
		//check existing file date
		else
		{
			$file_js_stat = stat($language_file_js_path);
			$file_js_mdate = $file_js_stat["mtime"];
			$regenerate_file = $file_php_mdate > $file_js_mdate;
		}
		
		if ($regenerate_file)
		{
			$js = array();
			foreach ($this->dictionary[$language_code]["javascript"] as $key=>$value) 
			{
				$js[$key] = str_replace('\\n', chr(13), $value);
			}
			file_put_contents($this->languages_cache.$language_file_name.".js",  "var msg = new Object(".json_encode($js).");");
			chmod($this->languages_cache.$language_file_name.'.js', FileUtils::getFilePermissionMode());
			FileUtils::setDirectoryPermissions(array($this->languages_cache), FileUtils::getDirectoryPermissionMode());
		}
		
		return $this->languages_cache_url.$language_file_name.".js?".md5($language_code.$file_php_mdate);
	}
	
	/**
	 * Sets and returns languages array
	 * @return mixed
	 */
	public function getLanguages()
	{
		$this->db()->query("SELECT * FROM ".DB_PREFIX."languages ORDER BY is_default, name");
		$this->languages = array();
		$this->active_languages = array();
		
		while ($this->db()->moveNext())
		{
			if (file_exists($this->languages_dir.$this->db()->col["code"].".php"))
			{
				$language_id = $this->db()->col["language_id"];
				$this->languages[$language_id] = $this->db()->col;
				$this->languages[$language_id]["file"] = $this->db()->col["code"].".php";
				if ($this->db()->col["is_active"] == "Yes")
				{
					$this->active_languages[$language_id] = $this->db()->col;
				}
				if ($this->db()->col["is_default"] == "Yes")
				{
					$this->default_language_id = $language_id;
				}
			}
		}		
		return count($this->languages) > 0 ? $this->languages : false;
	}
	
	/**
	 * Delete language
	 * @param int $language_id
	 * @return boolean
	 */
	function deleteLanguage($language_id)
	{
		$this->db()->query("SELECT * FROM ".DB_PREFIX."languages WHERE language_id='".intval($language_id)."'");
		if ($this->db()->moveNext())
		{
			unlink($this->languages_dir.$this->db()->col["code"].".php");
			$this->db()->query("DELETE FROM ".DB_PREFIX."languages WHERE language_id='".intval($language_id)."'");
			return true;
		}
		return false;
	}

	/**
	 * Returns active languages
	 * @return mixed
	 */
	function getActiveLanguages()
	{
		return count($this->active_languages) > 0 ? $this->active_languages : false;
	}
		
	/**
	 * Sets defaulslanguage
	 * @param int $language_id
	 * @return nothing
	 */
	function setDefaultLanguage($language_id)
	{
		$this->db()->query("UPDATE ".DB_PREFIX."languages SET is_default='No' WHERE is_default='Yes'");
		$this->db()->query("UPDATE ".DB_PREFIX."languages SET is_default='Yes', is_active='Yes' WHERE language_id='".intval($language_id)."'");
		$this->db()->query("SELECT * FROM ".DB_PREFIX."languages WHERE language_id='".intval($language_id)."'");
		$language = $this->db()->moveNext();
		$this->db()->query("UPDATE ".DB_PREFIX."settings SET value='".$language["code"]."' WHERE name='ActiveLanguage'");
	}
	
	/**
	 * Resturns defauls language
	 * @return mixed
	 */
	function getDefaultLanguage()
	{
		if ($this->default_language_id)
		{
			return $this->languages[$this->default_language_id];
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Return active language data
	 * @param $language_id
	 * @return mixed
	 */
	function getActiveLanguageById($language_id)
	{
		return array_key_exists($language_id, $this->active_languages) ? $this->active_languages[$language_id] : false;
	}
	
	/**
	 * Create a new language
	 * @param $lang_data
	 * @return mixed
	 */
	function addLanguage($lang_data)
	{
		$this->db()->query("SELECT * FROM ".DB_PREFIX."languages WHERE code = '".$this->db()->escape(strtolower($lang_data["code"]))."'");
		if (!$this->db()->moveNext())
		{
			$this->db()->reset();
			$this->db()->assignStr("name", $lang_data["name"]);
			$this->db()->assignStr("code", strtolower($lang_data["code"]));
			$this->db()->assignStr("is_active", isset($lang_data["is_active"])?"Yes":"No");
			$language_id = $this->db()->insert(DB_PREFIX."languages");
			return $language_id;
		}
		else
		{
			return false;
		}
	}
	
	
	/**
	 * Opens language file and merges it with default template so language will have all missed messages added
	 * @param unknown_type $code
	 */
	function completeLanguage($code)
	{
		//read lang file
		$languageFile = $this->languages_dir.$code.".php";
		if (!@include($languageFile)) return false;
		$language = $msg;
		
		//read lang template
		if (!@include($this->languages_dir."_template.php")) return false;;
		$template = $msg;
		
		unset($msg);
		
		foreach ($template as $sectionKey=>$section)
		{
			if (isset($language[$sectionKey]))
			{
				$language[$sectionKey] = array_merge($template[$sectionKey], $language[$sectionKey]);
				
				/**
				foreach ($section as $messageKey=>$message)
				{
					if (!isset($language[$sectionKey][$messageKey]))
					{
						$language[$sectionKey][$messageKey] = $message;
					}
				}
				**/
			}
			else
			{
				$language[$sectionKey] = $section;
			}
		}
		
		//save changes
		$f = fopen($languageFile, "w+");
		if ($f)
		{
			fputs($f, "<?php\n");
			fputs($f, "\$msg = ");
			$export = var_export($language, true);
			fputs($f, $export, strlen($export));
			fputs($f, ";\n?>");
			return true;
		}
		return false;
	}
	
	/**
	 * Update language
	 * @param $language_id
	 * @param $lang_data
	 * @return mixed
	 */
	function updateLanguage($language_id, $lang_data)
	{
		$this->db()->query("SELECT * FROM ".DB_PREFIX."languages WHERE language_id='".intval($language_id)."'");
		$this->db()->moveNext();
		$current_lang_data = $this->db()->col;
		
		if ($current_lang_data["code"] != $lang_data["code"])
		{
			$this->renameLanguage($current_lang_data["code"], $lang_data["code"]);
		}
		
		$this->db()->reset();
		$this->db()->assignStr("name", $lang_data["name"]);
		$this->db()->assignStr("code", strtolower($lang_data["code"]));
		$this->db()->assignStr("is_active", isset($lang_data["is_active"])?"Yes":"No");
		$this->db()->update(DB_PREFIX."languages", "WHERE language_id='".intval($language_id)."'");
	}

	function renameLanguage($oldCode, $newCode)
	{
		@rename($this->languages_dir.$oldCode.".php", $this->languages_dir.$newCode.".php");
	}

	/**
	 * Load language template
	 * @return mixed
	 */
	function loadTemplate()
	{
		//load template
		if (@include($this->languages_dir."_template.php"))
		{
			//get categories
			foreach ($msg as $cat_id=>$values)
			{
				$this->categories[$cat_id] = array(
					"id"=>$cat_id,
					"name"=>ucwords(str_replace("_", " ", $cat_id))
				);
			}
			//get messages
			$this->templates = $msg;
		}
		return count($this->categories)>0?$this->languages:false;
	}
	
	/**
	 * Get language messages
	 * @param $language_id
	 * @param $cat_id
	 * @return mixed
	 */
	function getMessages($language_id, $cat_id)
	{
		$messages = array();
		//load language file

		if (include($this->languages_dir.$this->languages[$language_id]["file"]))
		{
			//
			$msg = isset($msg)?$msg:array();
			$msg = is_array($msg)?$msg:array();
		
			//set initial messages to template messages
			$t = $this->templates[$cat_id];
			$l = array_key_exists($cat_id, $msg)?$msg[$cat_id]:array();
			$a = array();
			
			//compare template / language files
			foreach ($t as $key=>$value)
			{
				if (array_key_exists($key, $l))
				{
					$a[$key] = array(
						"key"=>$key,
						"template"=>$value,
						"message"=>$l[$key]
					);
				}
				else
				{
					$a[$key] = array(
						"key"=>$key,
						"template"=>$value,
						"message"=>$value
					);
				}
			}
			return $a;
		}
		return false;
	}
	
	/**
	 * Save messages
	 * @param $lang_id
	 * @param $cat_id
	 * @param $messages
	 * @return mixed
	 */
	function saveMessages($lang_id, $cat_id, $messages)
	{
		if (@include($this->languages_dir.$this->languages[$lang_id]["file"]))
		{
			$f = @fopen($this->languages_dir.$this->languages[$lang_id]["file"], "w+");
			if ($f)
			{
				fputs($f, "<?php\n");
				fputs($f, "\$msg = ");
				
				$msg[$cat_id] = isset($msg[$cat_id]) ? array_merge($msg[$cat_id], $messages) : $messages;

				$export = var_export($msg, true);

				fputs($f, $export, strlen($export));
				fputs($f, ";\n?>");
				fclose($f);
			}
		}
	}

	/**
	 * @param $langId
	 * @param $msg
	 */
	public function saveFile($langId, $msg)
	{
		file_put_contents(
			$this->languages_dir . $this->languages[$langId]['file'],
			"<?php\n\$msg = " . var_export($msg, true) . ";\n"
		);
	}
}


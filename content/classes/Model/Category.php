<?php

/**
 * Class Model_Category
 */
class Model_Category
{
	/** @var int $id */
	protected $id = 0;

	/** @var int|null $parentId */
	protected $parentId = null;

	/** @var int $level */
	protected $level;

	/** @var int $priority */
	protected $priority;

	/** @var bool $isVisible */
	protected $isVisible;

	/** @var bool $listSubcategories */
	protected $listSubcategories;

	/** @var bool $listSubcategoriesImages */
	protected $listSubcategoriesImages;

	/** @var string $urlHash */
	protected $urlHash;

	/** @var string $urlDefault */
	protected $urlDefault;

	/** @var string $urlCustom */
	protected $urlCustom;

	/** @var string $keyName */
	protected $keyName;

	/** @var string $metaTitle */
	protected $metaTitle;

	/** @var string $metaDescription */
	protected $metaDescription;

	/** @var string $headerTitle */
	protected $headerTitle;

	/** @var string $name */
	protected $name;

	/** @var string $description */
	protected $description;

	/** @var string $descriptionBottom */
	protected $descriptionBottom;

	/** @var string $categoryPath */
	protected $categoryPath;

	/** @var string $exactorEucCode */
	protected $exactorEucCode;

	/** @var string $avalaraTaxCode */
	protected $avalaraTaxCode;

	/**
	 * @param array|null $data
	 */
	public function __construct($data = null)
	{
		if (!is_null($data) && !is_array($data))
		{
			$this->hydrateFromArray($data);
		}
	}

	/**
	 * @param array $data
	 */
	public function hydrateFromArray($data)
	{
		$this->setId($data['cid']);
		$this->setParentId($data['parent']);
		$this->setLevel($data['level']);
		$this->setPriority($data['priority']);
		$this->setIsVisible($data['is_visible'] == 'Yes');
		$this->setUrlHash($data['url_hash']);
		$this->setUrlDefault($data['url_default']);
		$this->setUrlCustom($data['url_custom']);
		$this->setKeyName($data['key_name']);
		$this->setMetaTitle($data['meta_title']);
		$this->setMetaDescription($data['meta_description']);
		$this->setHeaderTitle($data['category_header']);
		$this->setName($data['name']);
		$this->setDescription($data['description']);
		$this->setDescriptionBottom($data['description_bottom']);
		$this->setCategoryPath($data['category_path']);
		$this->setExactorEucCode($data['exactor_euc_code']);
		$this->setAvalaraTaxCode($data['avalara_tax_code']);
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return array(
			'cid' => $this->getId(),
			'parent' => $this->getParentId(),
			'level' => $this->getLevel(),
			'priority' => $this->getPriority(),
			'is_visible' => $this->getIsVisible() ? 'Yes' : 'No',
			'url_hash' => $this->getUrlHash(),
			'url_default' => $this->getUrlDefault(),
			'url_custom' => $this->getUrlCustom(),
			'key_name' => $this->getKeyName(),
			'meta_title' => $this->getMetaTitle(),
			'meta_description' => $this->getMetaDescription(),
			'category_header' => $this->getHeaderTitle(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'description_bottom' => $this->getDescriptionBottom(),
			'category_path' => $this->getCategoryPath(),
			'exactor_euc_code' => $this->getExactorEucCode(),
			'avalara_tax_code' => $this->getAvalaraTaxCode()
		);
	}

	/**
	 * @param string $avalaraTaxCode
	 */
	public function setAvalaraTaxCode($avalaraTaxCode)
	{
		$this->avalaraTaxCode = $avalaraTaxCode;
	}

	/**
	 * @return string
	 */
	public function getAvalaraTaxCode()
	{
		return $this->avalaraTaxCode;
	}

	/**
	 * @param string $categoryPath
	 */
	public function setCategoryPath($categoryPath)
	{
		$this->categoryPath = $categoryPath;
	}

	/**
	 * @return string
	 */
	public function getCategoryPath()
	{
		return $this->categoryPath;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $descriptionBottom
	 */
	public function setDescriptionBottom($descriptionBottom)
	{
		$this->descriptionBottom = $descriptionBottom;
	}

	/**
	 * @return string
	 */
	public function getDescriptionBottom()
	{
		return $this->descriptionBottom;
	}

	/**
	 * @param string $exactorEucCode
	 */
	public function setExactorEucCode($exactorEucCode)
	{
		$this->exactorEucCode = $exactorEucCode;
	}

	/**
	 * @return string
	 */
	public function getExactorEucCode()
	{
		return $this->exactorEucCode;
	}

	/**
	 * @param string $headerTitle
	 */
	public function setHeaderTitle($headerTitle)
	{
		$this->headerTitle = $headerTitle;
	}

	/**
	 * @return string
	 */
	public function getHeaderTitle()
	{
		return $this->headerTitle;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param boolean $isVisible
	 */
	public function setIsVisible($isVisible)
	{
		$this->isVisible = $isVisible;
	}

	/**
	 * @return boolean
	 */
	public function getIsVisible()
	{
		return $this->isVisible;
	}

	/**
	 * @param string $keyName
	 */
	public function setKeyName($keyName)
	{
		$this->keyName = $keyName;
	}

	/**
	 * @return string
	 */
	public function getKeyName()
	{
		return $this->keyName;
	}

	/**
	 * @param int $level
	 */
	public function setLevel($level)
	{
		$this->level = $level;
	}

	/**
	 * @return int
	 */
	public function getLevel()
	{
		return $this->level;
	}

	/**
	 * @param string $metaDescription
	 */
	public function setMetaDescription($metaDescription)
	{
		$this->metaDescription = $metaDescription;
	}

	/**
	 * @return string
	 */
	public function getMetaDescription()
	{
		return $this->metaDescription;
	}

	/**
	 * @param string $metaTitle
	 */
	public function setMetaTitle($metaTitle)
	{
		$this->metaTitle = $metaTitle;
	}

	/**
	 * @return string
	 */
	public function getMetaTitle()
	{
		return $this->metaTitle;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param int|null $parentId
	 */
	public function setParentId($parentId)
	{
		$this->parentId = $parentId;
	}

	/**
	 * @return int|null
	 */
	public function getParentId()
	{
		return $this->parentId;
	}

	/**
	 * @param int $priority
	 */
	public function setPriority($priority)
	{
		$this->priority = $priority;
	}

	/**
	 * @return int
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 * @param string $urlCustom
	 */
	public function setUrlCustom($urlCustom)
	{
		$this->urlCustom = $urlCustom;
	}

	/**
	 * @return string
	 */
	public function getUrlCustom()
	{
		return $this->urlCustom;
	}

	/**
	 * @param string $urlDefault
	 */
	public function setUrlDefault($urlDefault)
	{
		$this->urlDefault = $urlDefault;
	}

	/**
	 * @return string
	 */
	public function getUrlDefault()
	{
		return $this->urlDefault;
	}

	/**
	 * @param string $urlHash
	 */
	public function setUrlHash($urlHash)
	{
		$this->urlHash = $urlHash;
	}

	/**
	 * @return string
	 */
	public function getUrlHash()
	{
		return $this->urlHash;
	}



}
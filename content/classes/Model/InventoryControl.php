<?php

class Model_InventoryControl
{
	public function updateQuantityOnHand($pid, $product_id, $product_subid, $stock, $makeVisible = false)
	{
		global $db;

		if (trim($product_subid) != '')
		{
			$product = $db->selectOne('SELECT inv.pi_id, inv.stock_warning
FROM '.DB_PREFIX.'products p INNER JOIN '.DB_PREFIX.'products_inventory inv ON p.pid = inv.pid
WHERE inv.product_subid = "'.$db->escape($product_subid).'" AND p.pid = '.intval($pid));

			if ($product)
			{
				$db->reset();

				$db->assignStr('stock', $stock);

				if ($makeVisible && $stock >= $product['stock_warning'])
				{
					$db->assignStr('is_active', '1');
				}

				$db->update(DB_PREFIX.'products_inventory', 'WHERE pi_id='.intval($product['pi_id']));

				$productStock = $db->selectOne('SELECT SUM(stock) as stock FROM '.DB_PREFIX.'products_inventory WHERE pid = '.intval($pid));
				$db->reset();
				$db->assign('stock', intval($productStock['stock']));
				$db->update(DB_PREFIX.'products', 'WHERE pid = '.intval($pid));

				return true;
			}
		}
		else
		{
			$product = $db->selectOne('SELECT p.pid, p.stock_warning
FROM '.DB_PREFIX.'products p
WHERE p.pid = '.intval($pid).' AND p.product_id = "'.$db->escape($product_id).'"');

			if ($product)
			{
				$db->reset();
				$db->assignStr('stock', $stock);

				if ($makeVisible && $stock >= $product['stock_warning'])
				{
					$db->assignStr('is_visible', 'Yes');
				}

				$db->update(DB_PREFIX.'products', 'WHERE pid='.intval($product['pid']));
				return true;
			}
		}

		return false;
	}

	public function decrementStock($pid, $stock, $inventoryId = 0)
	{
		global $db;

		if ($stock < 1)
		{
			return 0;
		}

		$pid = intval($pid);
		$stock = intval($stock);
		$inventoryId = intval($inventoryId);
		if ($inventoryId > 0)
		{
			$db->query('SELECT p.pid FROM '.DB_PREFIX.'products p WHERE p.pid = '.$pid.' AND inventory_control IN ("AttrRuleExc", "AttrRuleInc")');
			if ($db->numRows() < 1)
			{
				return false;
			}

			$db->query('UPDATE '.DB_PREFIX."products_inventory SET stock = stock - ".$stock." WHERE pi_id=".$inventoryId." AND stock >= ".$stock);

			if ($db->numRowsAffected() > 0)
			{
				$db->query('UPDATE '.DB_PREFIX.'products p
INNER JOIN (
	SELECT pi.pid, sum(pi.stock) as stock
	FROM '.DB_PREFIX.'products_inventory pi
	WHERE pi.pid = '.$pid.'
	GROUP BY pi.pid
) t ON p.pid = t.pid AND p.pid = '.$pid.'
SET p.stock = t.stock');

				//It was modified
				$row = $db->selectOne("SELECT pinv.stock, pinv.stock_warning, p.min_order, p.stock as product_stock
FROM ".DB_PREFIX."products_inventory pinv
	INNER JOIN ".DB_PREFIX."products p ON pinv.pid = p.pid
WHERE pi_id=".$inventoryId);
				if ($row)
				{
					if ($row["stock"] <= $row["stock_warning"])
					{
						Notifications::emailOutOfStock($db, $pid, $row["stock"], $inventoryId);
					}

					if (!is_null($row["min_order"]) && ($row["min_order"]>0) && ($row['product_stock'] < $row["min_order"]))
					{
						Notifications::emailOutOfStock($db, $pid, $row["stock"], 0);
					}
				}

				return $stock;
			}
			else
			{
				//TODO: Handle partial stock/backorder?
//					$db->query('SELECT stock FROM '.DB_PREFIX.'products_inventory WHERE pi_id = '.intval($inventoryId));
//
//					if ($row = $db->moveNext())
//					{
//						if ($row['stock'] > 0)
//						{
//
//						}
//					}
			}
		}
		else
		{
			$db->query('SELECT p.pid FROM '.DB_PREFIX.'products p WHERE p.pid = '.$pid.' AND inventory_control = "Yes"');
			if ($db->numRows() < 1)
			{
				return false;
			}

			$db->query('UPDATE '.DB_PREFIX."products SET stock = stock - ".$stock." WHERE pid=".$pid." AND inventory_control = 'Yes' AND stock >= ".$stock);

			if ($db->numRowsAffected() > 0)
			{
				$db->query('SELECT stock, stock_warning, min_order FROM '.DB_PREFIX.'products WHERE pid = '.$pid);
				if ($row = $db->moveNext())
				{
					if ((!is_null($row['stock_warning']) && $row['stock'] <= $row['stock_warning']) ||
						(!is_null($row["min_order"]) && ($row["min_order"]>0) && ($row['stock'] < $row["min_order"])))
					{
						Notifications::emailOutOfStock($db, $pid, $row['stock'], 0);
					}
				}

				return $stock;
			}
			else
			{
				//TODO: Handle partial stock/backorder?
			}
		}

		return 0;
	}

	public function incrementStock($pid, $stock, $inventoryId = 0)
	{
		global $db;

		$pid = intval($pid);
		$stock = intval($stock);
		$inventoryId = intval($inventoryId);

		if ($inventoryId > 0)
		{
			$db->query('SELECT p.pid FROM '.DB_PREFIX.'products p WHERE p.pid = '.$pid.' AND inventory_control IN ("AttrRuleExc", "AttrRuleInc")');
			if ($db->numRows() < 1)
			{
				return false;
			}

			$db->query('UPDATE '.DB_PREFIX.'products_inventory SET stock = stock + '.$stock." WHERE pi_id = ".$inventoryId);
			if ($db->numRowsAffected() > 0)
			{
				$db->query('UPDATE '.DB_PREFIX.'products p
INNER JOIN (
	SELECT pi.pid, sum(pi.stock) as stock
	FROM '.DB_PREFIX.'products_inventory pi
	WHERE pi.pid = '.$pid.'
	GROUP BY pi.pid
) t ON p.pid = t.pid AND p.pid = '.$pid.'
SET p.stock = t.stock');

				return true;
			}
		}
		else
		{
			$db->query('SELECT p.pid FROM '.DB_PREFIX.'products p WHERE p.pid = '.$pid.' AND inventory_control = "Yes"');
			if ($db->numRows() < 1)
			{
				return false;
			}

			$db->query('UPDATE '.DB_PREFIX.'products SET stock = stock + '.$stock.' WHERE pid = '.intval($pid)." AND inventory_control = 'Yes'");
			if ($db->numRowsAffected() > 0)
			{
				return true;
			}
		}

		return false;
	}
}
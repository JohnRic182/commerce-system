<?php
/**
 * Class Address
 */
class Model_Address
{
	const ADDRESS_BILLING = 'Billing';
	const ADDRESS_SHIPPING = 'Shipping';
	const ADDRESS_PAYMENT = 'Payment';

	const ADDRESS_TYPE_RESIDENTIAL = 'Residential';
	const ADDRESS_TYPE_BUSINESS = 'Business';

	protected $addressType;
	protected $firstName;
	protected $lastName;
	protected $name;
	protected $company;
	protected $addressLine1;
	protected $addressLine2;
	protected $city;
	protected $stateId;
	protected $stateName;
	protected $stateCode;
	protected $province;
	protected $countryId;
	protected $countryName;
	protected $countryIso2;
	protected $countryIso3;
	protected $countryIsoNumber;
	protected $zip;

	protected $customFields;

	/**
	 * Class constructor
	 *
	 * @param array|null $data
	 * @param array|null $customFields
	 */
	public function __construct($data = null, $customFields = null)
	{
		if (!is_null($data) && is_array($data))
		{
			$this->hydrateFromArray($data, $customFields);
		}
	}

	/**
	 * Hydrate address from an array
	 *
	 * @param $data
	 * @param null $customFields
	 */
	public function hydrateFromArray($data, $customFields = null)
	{
		if (isset($data['address_type']))
		{
			$this->setAddressType($data['address_type'] == 'Residential' ? self::ADDRESS_TYPE_RESIDENTIAL : self::ADDRESS_TYPE_BUSINESS);
		}
		else
		{
			$this->setAddressType(isset($data['company']) && trim($data['company']) == '' ? self::ADDRESS_TYPE_RESIDENTIAL : self::ADDRESS_TYPE_BUSINESS);
		}

		if (isset($data['fname'])) $this->setFirstName($data['fname']);
		if (isset($data['lname'])) $this->setLastName($data['lname']);

		if (isset($data['name']))
		{
			$this->setName($data['name']);
			if (!isset($data['fname']) || !isset($data['lname']))
			{
				$parts = preg_split( '/\s+/', $data['name']);
				if (count($parts) > 0)
				{
					$this->setFirstName($parts[0]);
					unset($parts[0]);
					$this->setLastName(implode(' ', $parts));
				}
			}
		}
		else
		{
			$this->setName($this->getFirstName().' '.$this->getLastName());
		}

		$this->setCompany(isset($data['company']) ? $data['company'] : '');
		$this->setAddressLine1($data['address1']);
		$this->setAddressLine2($data['address2']);
		$this->setCity($data['city']);
		$this->setProvince($data['province']);
		$this->setStateId(intval(isset($data['state_id']) ? $data['state_id'] : $data['state']));
		$this->setCountryId(intval(isset($data['country_id']) ? $data['country_id'] : $data['country']));
		$this->setZip($data['zip']);

		// extra data
		$this->setStateName(isset($data['state_name']) ? $data['state_name'] : $data['state']);
		$this->setStateCode(isset($data['state_abbr']) ? $data['state_abbr'] : '');

		$this->setCountryName(isset($data['country_name']) ? $data['country_name'] : '');
		$this->setCountryIso2(isset($data['country_iso_a2']) ? $data['country_iso_a2'] : '');
		$this->setCountryIso3(isset($data['country_iso_a3']) ? $data['country_iso_a3'] : '');
		$this->setCountryIsoNumber(isset($data['country_iso_number']) ? $data['country_iso_number'] : '');

		$this->setCustomFields($customFields);
	}

	/**
	 * Set custom fields
	 *
	 * @param mixed $customFields
	 */
	public function setCustomFields($customFields)
	{
		$this->customFields = $customFields;
	}

	/**
	 * Get custom fields
	 *
	 * @return mixed
	 */
	public function getCustomFields()
	{
		return $this->customFields;
	}

	/**
	 * Set address line 1
	 * @param string $addressLine1
	 */
	public function setAddressLine1($addressLine1)
	{
		$this->addressLine1 = $addressLine1;
	}

	/**
	 * Get address line 1
	 *
	 * @return string
	 */
	public function getAddressLine1()
	{
		return $this->addressLine1;
	}

	/**
	 * Set address line 2
	 *
	 * @param string $addressLine2
	 */
	public function setAddressLine2($addressLine2)
	{
		$this->addressLine2 = $addressLine2;
	}

	/**
	 * Get address line 2
	 *
	 * @return string
	 */
	public function getAddressLine2()
	{
		return $this->addressLine2;
	}

	/**
	 * Set address type
	 *
	 * @param string $addressType
	 */
	public function setAddressType($addressType)
	{
		$this->addressType = $addressType;
	}

	/**
	 * Get address type
	 *
	 * @return string
	 */
	public function getAddressType()
	{
		return $this->addressType;
	}

	/**
	 * Set city
	 *
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * Get city
	 *
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Set company
	 *
	 * @param string $company
	 */
	public function setCompany($company)
	{
		$this->company = $company;
	}

	/**
	 * Get company
	 *
	 * @return string
	 */
	public function getCompany()
	{
		return $this->company;
	}

	/**
	 * Set country id
	 *
	 * @param int $countryId
	 */
	public function setCountryId($countryId)
	{
		$this->countryId = $countryId;
	}

	/**
	 * Get country id
	 *
	 * @return int
	 */
	public function getCountryId()
	{
		return $this->countryId;
	}

	/**
	 * Set first name
	 *
	 * @param mixed $firstName
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}

	/**
	 * Get first name
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Set last name
	 *
	 * @param string $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * Get last name
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set province
	 *
	 * @param string $province
	 */
	public function setProvince($province)
	{
		$this->province = $province;
	}

	/**
	 * Get province
	 *
	 * @return mixed
	 */
	public function getProvince()
	{
		return $this->province;
	}

	/**
	 * Set state id
	 *
	 * @param int $stateId
	 */
	public function setStateId($stateId)
	{
		$this->stateId = $stateId;
	}

	/**
	 * Get state id
	 *
	 * @return int
	 */
	public function getStateId()
	{
		return $this->stateId;
	}

	/**
	 * Set zip code
	 *
	 * @param string $zip
	 */
	public function setZip($zip)
	{
		$this->zip = $zip;
	}

	/**
	 * Get zip code
	 *
	 * @return string
	 */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * Set country iso 2
	 *
	 * @param mixed $countryIso2
	 */
	public function setCountryIso2($countryIso2)
	{
		$this->countryIso2 = $countryIso2;
	}

	/**
	 * Get country iso 2 code
	 *
	 * @return mixed
	 */
	public function getCountryIso2()
	{
		return $this->countryIso2;
	}

	/**
	 * Set country iso 3 code
	 *
	 * @param mixed $countryIso3
	 */
	public function setCountryIso3($countryIso3)
	{
		$this->countryIso3 = $countryIso3;
	}

	/**
	 * Ggt country iso 3 code
	 *
	 * @return mixed
	 */
	public function getCountryIso3()
	{
		return $this->countryIso3;
	}

	/**
	 * Set country iso number
	 *
	 * @param mixed $countryIsoNumber
	 */
	public function setCountryIsoNumber($countryIsoNumber)
	{
		$this->countryIsoNumber = $countryIsoNumber;
	}

	/**
	 * Get country iso number
	 *
	 * @return mixed
	 */
	public function getCountryIsoNumber()
	{
		return $this->countryIsoNumber;
	}

	/**
	 * Set country name
	 *
	 * @param mixed $countryName
	 */
	public function setCountryName($countryName)
	{
		$this->countryName = $countryName;
	}

	/**
	 * Get country name
	 *
	 * @return mixed
	 */
	public function getCountryName()
	{
		return $this->countryName;
	}

	/**
	 * Set state code
	 *
	 * @param mixed $stateCode
	 */
	public function setStateCode($stateCode)
	{
		$this->stateCode = $stateCode;
	}

	/**
	 * Get state code
	 *
	 * @return mixed
	 */
	public function getStateCode()
	{
		return $this->stateCode;
	}

	/**
	 * Set state name
	 *
	 * @param mixed $stateName
	 */
	public function setStateName($stateName)
	{
		$this->stateName = $stateName;
	}

	/**
	 * Get state name
	 *
	 * @return mixed
	 */
	public function getStateName()
	{
		return $this->stateName;
	}

	/**
	 * Return address data as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		return array(
			'address_type' => $this->getAddressType(),
			'name' => $this->getName(),
			'fname' => $this->getFirstName(),
			'lname' => $this->getLastName(),
			'company' => $this->getCompany(),
			'address1' => $this->getAddressLine1(),
			'address2' => $this->getAddressLine2(),
			'city' => $this->getCity(),
			'province' => $this->getProvince(),
			'state' => $this->getStateId(),
			'state_name' => $this->getStateName(),
			'state_abbr' => $this->getStateCode(),
			'country' => $this->getCountryId(),
			'country_name' => $this->getCountryName(),
			'country_iso_a2' => $this->getCountryIso2(),
			'country_iso_a3' => $this->getCountryIso3(),
			'country_iso_number' => $this->getCountryIsoNumber(),
			'zip' => $this->getZip()
		);
	}

	/**
	 * Return default array
	 *
	 * @return array
	 */
	public function getArrayDefaults()
	{
		return array(
			'address_type' => self::ADDRESS_TYPE_RESIDENTIAL,
			'name' => '',
			'fname' => '',
			'lname' => '',
			'company' => '',
			'address1' => '',
			'address2' => '',
			'city' => '',
			'province' => '',
			'state' => 0,
			'state_name' => '',
			'state_abbr' => '',
			'country' => 0,
			'country_name' => '',
			'country_iso_a2' => '',
			'country_iso_a3' => '',
			'country_iso_number' => '',
			'zip' => ''
		);
	}

}
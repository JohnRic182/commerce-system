<?php

class Model_FulfillmentItem
{
	protected $id;
	protected $fulfillmentId;
	protected $orderId;
	protected $lineItemId;
	protected $shipmentId;
	protected $quantity;
	protected $createdAt;
	protected $updatedAt;

	public function __construct($data = null)
	{
		if ($data !== null)
		{
			if (is_array($data))
			{
				$this->id = isset($data['id']) ? intval($data['id']) : 0;
				if (isset($data['fulfillment_id'])) $this->fulfillmentId = intval($data['fulfillment_id']);
				if (isset($data['oid'])) $this->orderId = intval($data['oid']);
				if (isset($data['shipment_id'])) $this->shipmentId = intval($data['shipment_id']);
				if (isset($data['ocid']))
				{
					$this->lineItemId = intval($data['ocid']);
				}
				else if (isset($data['line_item_id']))
				{
					$this->lineItemId = intval($data['line_item_id']);
				}
				$this->quantity = intval($data['quantity']);
				$this->createdAt = isset($data['created_at']) ? new DateTime($data['created_at']) : new DateTime();
				$this->updatedAt = isset($data['updated_at']) ? new DateTime($data['updated_at']) : new DateTime();
			}
			else if ($data instanceof Model_LineItem)
			{
				$this->orderId = $data->getOrderId();
				$this->lineItemId = $data->getId();
				$this->shipmentId = $data->getShipmentId();
				$this->quantity = $data->getFinalQuantity() - $data->getFulfilledQuantity();
				$this->createdAt = new DateTime();
				$this->updatedAt = new DateTime();
			}
		}
		else
		{
			$this->createdAt = new DateTime();
			$this->updatedAt = new DateTime();
		}
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * @return mixed
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param mixed $fulfillmentId
	 */
	public function setFulfillmentId($fulfillmentId)
	{
		$this->fulfillmentId = $fulfillmentId;
	}

	/**
	 * @return mixed
	 */
	public function getFulfillmentId()
	{
		return $this->fulfillmentId;
	}

	/**
	 * @param mixed $lineItemId
	 */
	public function setLineItemId($lineItemId)
	{
		$this->lineItemId = $lineItemId;
	}

	/**
	 * @return mixed
	 */
	public function getLineItemId()
	{
		return $this->lineItemId;
	}

	/**
	 * @return int
	 */
	public function getShipmentId()
	{
		return $this->shipmentId;
	}

	/**
	 * @param int $shipmentId
	 */
	public function setShipmentId($shipmentId)
	{
		$this->shipmentId = $shipmentId;
	}

	/**
	 * @param mixed $orderId
	 */
	public function setOrderId($orderId)
	{
		$this->orderId = $orderId;
	}

	/**
	 * @return mixed
	 */
	public function getOrderId()
	{
		return $this->orderId;
	}

	/**
	 * @param mixed $quantity
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param mixed $updatedAt
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;
	}

	/**
	 * @return mixed
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}
}
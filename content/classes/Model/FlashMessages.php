<?php
/**
 * Class Flash
 */
class Model_FlashMessages
{
	const TYPE_SUCCESS = 'success';
	const TYPE_ERROR = 'error';
	const TYPE_INFO = 'info';

	protected $session;
	protected $sessionKey;

	/**
	 * Class constructor
	 *
	 * @param Framework_Session $session
	 * @param string $sessionKey
	 */
	public function __construct(Framework_Session $session, $sessionKey = 'flash-messages')
	{
		$this->session = $session;
		$this->sessionKey = $sessionKey;
	}

	/**
	 * Set flash message
	 *
	 * @param string $message
	 * @param string $type
	 * @param string | null $messageKey
	 */
	public function setMessage($message, $type = self::TYPE_SUCCESS, $messageKey = null)
	{
		$messages = $this->session->get($this->sessionKey, array());
		if (!is_null($messageKey))
		{
			$messages[$type][$messageKey] = array('message' => $message, 'type' => $type);
		}
		else
		{
			$messages[$type][] = array('message' => $message, 'type' => $type);
		}
		$this->session->set($this->sessionKey, $messages);
	}

	/**
	 * Set multiple messages at once
	 *
	 * @param array $messages
	 * @param string $status
	 */
	public function setMessages(array $messages, $status = self::TYPE_SUCCESS)
	{
		foreach ($messages as $messageKey => $message)
		{
			$this->setMessage($message, $status, is_numeric($messageKey) ? null : $messageKey);
		}
	}

	/**
	 * Check does flash message exist
	 *
	 * @return bool
	 */
	public function hasMessages()
	{
		$messages = $this->session->get($this->sessionKey, array());
		return count($messages) > 0;
	}

	/**
	 * Returns flash message
	 *
	 * @param null $type
	 * @param bool $reset
	 *
	 * @return mixed
	 */
	public function getMessages($type = null, $reset = true)
	{
		$messages = $this->session->get($this->sessionKey, array());

		if (!is_null($type))
		{
			$messagesByType = array();

			if (isset($messages[$type]))
			{
				$messagesByType =  $messages[$type];
				unset($messages[$type]);
				$this->session->set($this->sessionKey, $messages);
			}

			return $messagesByType;
		}

		if ($reset) $this->session->remove($this->sessionKey);

		return $messages;
	}
}
<?php

//TODO: should this class be TaxClassRate to communicate that this is the tax rate for the Tax Class
//which is how it is used
//$taxRates->getTaxRate($classId) returns an instance of this class
class Model_TaxRate
{
	protected $classId;
	protected $taxRate = 0;
	protected $multiplier;
	protected $description;

	protected $taxRates = array();
	protected $isComputed = false;

	protected $displayWithTax = false;

	protected $extraData = array();

	public function __construct($classId, $taxRate = 0, $multiplier = 1, $description = '')
	{
		$this->classId = $classId;
		$this->taxRate = $taxRate;
		$this->multiplier = $multiplier;
		$this->description = $description;
	}

	public function getExtraData()
	{
		return $this->extraData;
	}

	public function getExtraDataValue($key)
	{
		return array_key_exists($key, $this->extraData) ? $this->extraData[$key] : null;
	}

	public function setExtraDataValue($key, $value)
	{
		$this->extraData[$key] = $value;
	}

	/**
	 * Gets classId
	 *
	 * @return integer
	 */
	public function getClassId()
	{
		return $this->classId;
	}
	/**
	 * Sets classId
	 *
	 * @param integer $classId
	 */
	public function setClassId($classId)
	{
		$this->classId = $classId;
	}

	/**
	 * Gets taxRate
	 *
	 * @return float
	 */
	public function getTaxRate()
	{
		if (!$this->isComputed)
		{
			$this->computeTaxRate();
		}

		return $this->taxRate;
	}
	/**
	 * Sets taxRate
	 *
	 * @param float $taxRate
	 */
	public function setTaxRate($taxRate)
	{
		$this->taxRate = $taxRate;
	}

	/**
	 * Gets multiplier
	 *
	 * @return float
	 */
	public function getMultiplier()
	{
		return $this->multiplier;
	}
	/**
	 * Sets multiplier
	 *
	 * @param float $multiplier
	 */
	public function setMultiplier($multiplier)
	{
		$this->multiplier = $multiplier;
	}

	/**
	 * Gets description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	/**
	 * Sets description
	 *
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * Add a tax rate value to this rate
	 * Note: when multiple tax rates, they will be summed / compounded depending on the priority value
	 * rates at the same priority will be summed
	 * rates at different priorities will be compounded, priorities sorted in ascending order
	 * 
	 * @param int    $priority        the priority
	 * @param float  $taxRate         the tax rate
	 * @param string $rateDescription the tax rate description
	 */
	public function addTaxRate($priority, $taxRate, $rateDescription)
	{
		if (!array_key_exists($priority, $this->taxRates))
		{
			$this->taxRates[$priority] = array('tax_sum' => 0, 'description' => '');
		}

		$this->taxRates[$priority]['tax_sum'] += $taxRate;

		$description = $this->taxRates[$priority]['description'];
		$this->taxRates[$priority]['description'] .=  (trim($description) != '' ? ' + ' : '').$rateDescription.' ('.myNum($taxRate).'%)';

		$this->isComputed = false;
	}

	/**
	 * Computes the combined tax rate for this tax class
	 * 
	 * @return void
	 */
	public function computeTaxRate()
	{
		if ($this->getClassId() > 0)
		{
			$start = 100;

			ksort($this->taxRates); // Ensure priorities sorted ascending

			$description = '';
			foreach ($this->taxRates as $priority => $taxData)
			{
				$start += $start / 100 * $taxData['tax_sum'];
				$description .= (trim($description) != '' ? ' + ' : '').$taxData['description'];
			}

			$this->setDescription($description);
			$this->setMultiplier($start / 100);
			$this->setTaxRate(($this->getMultiplier() - 1));
		}

		$this->isComputed = true;
	}

	/**
	 * Calculate the tax amount
	 * 
	 * @param  float $amount the amount to calculate the tax
	 * @return float         the tax amount
	 */
	public function calculateTax($amount)
	{
		if (!$this->isComputed)
		{
			$this->computeTaxRate();
		}

		return $amount * $this->getTaxRate();
	}
}
<?php

/**
 * Class Model_NotificationEmailRepositoryItem
 */
class Model_NotificationEmailItem
{
	private $title = '';
	private $id = '';
	private $content = '';
	private $template_html_location = '';

	private $email_path_base = '';
	private $email_path_skin = '';
	private $email_path_custom = '';
	private $email_path_cache = '';

	/**
	 * @param $params
	 */
	public function __construct($params)
	{
		$this->title = $params['title'];
		$this->id = $params['id'];
		$this->setEmailPathBase($params['email_path_base']);
		$this->setEmailPathCache($params['email_path_cache']);
		$this->setEmailPathCustom($params['email_path_custom']);
		$this->setEmailPathSkin($params['email_path_skin']);
		$this->loadContent();
	}

	/**
	 * Load content
	 */
	protected function loadContent()
	{
		if (is_file($this->email_path_custom.$this->id.'_html.html'))
		{
			$file_path = $this->email_path_custom.$this->id.'_html.html';
			$this->template_html_location = 'custom';
		}
		elseif (is_file($this->email_path_skin.$this->id.'_html.html'))
		{
			$file_path = $this->email_path_skin.$this->id.'_html.html';
			$this->template_html_location = 'skin';
		}
		else
		{
			$file_path = $this->email_path_base.$this->id.'_html.html';
			$this->template_html_location = 'base';
		}
		$this->content = file_get_contents($file_path);
	}

	/**
	 * @param string $email_path_skin
	 */
	public function setEmailPathSkin($email_path_skin)
	{
		$this->email_path_skin = $email_path_skin;
	}

	/**
	 * @return string
	 */
	public function getEmailPathSkin()
	{
		return $this->email_path_skin;
	}

	/**
	 * @param string $email_path_custom
	 */
	public function setEmailPathCustom($email_path_custom)
	{
		$this->email_path_custom = $email_path_custom;
	}

	/**
	 * @return string
	 */
	public function getEmailPathCustom()
	{
		return $this->email_path_custom;
	}

	/**
	 * @param string $email_path_cache
	 */
	public function setEmailPathCache($email_path_cache)
	{
		$this->email_path_cache = $email_path_cache;
	}

	/**
	 * @return string
	 */
	public function getEmailPathCache()
	{
		return $this->email_path_cache;
	}

	/**
	 * @param string $email_path_base
	 */
	public function setEmailPathBase($email_path_base){
		$this->email_path_base = $email_path_base;
	}

	/**
	 * @return string
	 */
	public function getEmailPathBase()
	{
		return $this->email_path_base;
	}

	/**
	 * @return string
	 */
	public function getEmailTitle()
	{
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getEmailTemplateId()
	{
		return $this->id;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		$ret = array(
			'id' => $this->getEmailTemplateId(),
			'title' => $this->getEmailTitle(),
			'content' => $this->content,
		);

		return $ret;
	}
}
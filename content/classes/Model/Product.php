<?php
/**
 * Class Model_Product
 */
class Model_Product
{
	const TANGIBLE = 'Tangible';
	const VIRTUAL = 'Virtual';
	const DIGITAL = 'Digital';

	protected static $productsRepository;

	protected $values;

	/**
	 * Class constructor
	 *
	 * @param array $values
	 */
	public function __construct(array $values)
	{
		$this->values = &$values;
		$this->setFinalPrice($this->getPrice());

		$recurringBillingData = null;

		// get billing data stored on product level
		if (isset($this->values['recurring_billing_data']) && $this->values['recurring_billing_data'] != '')
		{
			$recurringBillingDataArray = @unserialize($this->values['recurring_billing_data']);
			if ($recurringBillingDataArray && is_array($recurringBillingDataArray))
			{
				$recurringBillingData = new RecurringBilling_Model_ProductRecurringBillingData($recurringBillingDataArray);
			}
		}

		$this->setRecurringBillingData($recurringBillingData);
	}

	/**
	 * @return array
	 */
	public function getValues()
    {
	    return $this->values;
    }

	/**
	 * @return array
	 */
	public function toArray()
	{
		/**
		 * @return array
		 */
		$result = $this->values;

		if (isset($result['recurring_billing_data']) && !is_null($result['recurring_billing_data']) && $result['recurring_billing_data'] instanceof RecurringBilling_Model_ProductRecurringBillingData)
		{
			/** @var RecurringBilling_Model_ProductRecurringBillingData $rbd */
			$rbd = $result['recurring_billing_data'];
			$result['recurring_billing_data'] = $rbd->toArray();
		}

		return $result;
	}

	/**
	 * Gets the pid
	 *
	 * @return int
	 */
	public function getPid()
	{
		return $this->values['pid'];
	}

	/**
	 * Sets the pid
	 *
	 * @param int $pid
	 */
	public function setPid($pid)
	{
		$this->values['pid'] = $pid;
	}

	/**
	 * Gets the product type
	 *
	 * @return string
	 */
	public function getProductType()
	{
		return $this->values['product_type'];
	}

	/**
	 * Sets the product type
	 *
	 * @param string $productType
	 */
	public function setProductType($productType)
	{
		$this->values['product_type'] = $productType;
	}

	/**
	 * Gets the product id
	 *
	 * @return string
	 */
	public function getProductId()
	{
		return $this->values['product_id'];
	}

	/**
	 * Sets the product id
	 *
	 * @param string $productId
	 */
	public function setProductId($productId)
	{
		$this->values['product_id'] = $productId;
	}

	/**
	 * Get date added
	 */
	public function getDateAdded()
	{
		$this->values['added'];
	}

	/**
	 * @param $dateAdded
	 */
	public function setDateAdded($dateAdded)
	{
		$this->value['added'] = $dateAdded;
	}

	/**
	 * Get date updated
	 */
	public function getDateUpdated()
	{
		$this->values['updated'];
	}

	/**
	 * @param $dateAdded
	 */
	public function setDateUpdated($dateAdded)
	{
		$this->value['updated'] = $dateAdded;
	}

	/**
	 * Gets the attributes count
	 *
	 * @return int
	 */
	public function getAttributesCount()
	{
		return $this->values['attributes_count'];
	}

	/**
	 * Sets the attributes count
	 *
	 * @param int $attributesCount
	 */
	public function setAttributesCount($attributesCount)
	{
		$this->values['attributes_count'] = $attributesCount;
	}

	/**
	 * Gets the weight
	 *
	 * @return float
	 */
	public function getWeight()
	{
		return $this->values['weight'];
	}

	/**
	 * Sets the weight
	 *
	 * @param float $weight
	 */
	public function setWeight($weight)
	{
		$this->values['weight'] = $weight;
	}

	/**
	 * Gets the inventory control parameter
	 *
	 * @return string
	 */
	public function getInventoryControl()
	{
		return $this->values['inventory_control'];
	}

	/**
	 * Sets the inventory control parameter
	 *
	 * Valid values: Yes, AttrRuleExc, AttrRuleInc, No
	 *
	 * @param string $inventoryControl
	 */
	public function setInventoryControl($inventoryControl)
	{
		$this->values['inventory_control'] = $inventoryControl;
	}

	/**
	 * Gets the price
	 *
	 * @return float
	 */
	public function getPrice()
	{
		return $this->values['price'];
	}

	/**
	 * Sets the price
	 *
	 * @param float $price
	 */
	public function setPrice($price)
	{
		$this->values['price'] = $price;
	}

	/**
	 * Gets the final price
	 *
	 * @return float
	 */
	public function getFinalPrice()
	{
		return $this->values['final_price'];
	}

	/**
	 * Sets the final price
	 *
	 * @param float $price
	 */
	public function setFinalPrice($price)
	{
		$this->values['final_price'] = $price;
	}

	/**
	 * Gets the price with tax added
	 *
	 * @return float
	 */
	public function getPriceWithTax()
	{
		return isset($this->values['price_withtax']) ? $this->values['price_withtax'] : $this->getFinalPrice();
	}

	/**
	 * Sets the price with tax added
	 *
	 * @param float $price
	 */
	public function setPriceWithTax($price)
	{
		$this->values['price_withtax'] = $price;
	}

	/**
	 * Get the min order quantity
	 *
	 * @return int
	 */
	public function getMinOrder()
	{
		return $this->values['min_order'];
	}

	/**
	 * Sets the min order quantity
	 *
	 * @param int $minOrder
	 */
	public function setMinOrder($minOrder)
	{
		$this->values['min_order'] = $minOrder;
	}

	/**
	 * Get the max order quantity
	 *
	 * @return int
	 */
	public function getMaxOrder()
	{
		return $this->values['max_order'];
	}

	/**
	 * Sets the max order quantity
	 *
	 * @param int $maxOrder
	 */
	public function setMaxOrder($maxOrder)
	{
		$this->values['max_order'] = $maxOrder;
	}

	/**
	 * Get the stock quantity
	 *
	 * @return int
	 */
	public function getStock()
	{
		return $this->values['stock'];
	}

	/**
	 * Sets the stock quantity
	 *
	 * @param int $stock
	 */
	public function setStock($stock)
	{
		$this->values['stock'] = $stock;
	}

	/**
	 * Get the price for wholesale level 1
	 *
	 * @return float
	 */
	public function getPriceLevel1()
	{
		return $this->values['price_level_1'];
	}

	/**
	 * Sets the price for wholesale level 1
	 *
	 * @param float $priceLevel1
	 */
	public function setPriceLevel1($priceLevel1)
	{
		$this->values['price_level_1'] = $priceLevel1;
	}

	/**
	 * Get the price for wholesale level 3
	 *
	 * @return float
	 */
	public function getPriceLevel2()
	{
		return $this->values['price_level_2'];
	}

	/**
	 * Sets the price for wholesale level 2
	 *
	 * @param float $priceLevel2
	 */
	public function setPriceLevel2($priceLevel2)
	{
		$this->values['price_level_2'] = $priceLevel2;
	}

	/**
	 * Get the price for wholesale level 3
	 *
	 * @return float
	 */
	public function getPriceLevel3()
	{
		return $this->values['price_level_3'];
	}

	/**
	 * Sets the price for wholesale level 3
	 *
	 * @param float $priceLevel3
	 */
	public function setPriceLevel3($priceLevel3)
	{
		$this->values['price_level_3'] = $priceLevel3;
	}

	/**
	 * Get is doba
	 *
	 * @return string
	 */
	public function getIsDoba()
	{
		return $this->values['is_doba'];
	}

	/**
	 * Sets is doba
	 *
	 * Valid values: Yes or No
	 *
	 * @param $isDoba
	 */
	public function setIsDoba($isDoba)
	{
		$this->values['is_doba'] = $isDoba;
	}

	/**
	 * Get is free shipping
	 *
	 * @return string
	 */
	public function getFreeShipping()
	{
		return $this->values['free_shipping'];
	}

	/**
	 * Sets is free shipping
	 *
	 * Valid values: Yes or No
	 *
	 * @param $freeShipping
	 */
	public function setFreeShipping($freeShipping)
	{
		$this->values['free_shipping'] = $freeShipping;
	}

	/**
	 * Get is product level shipping
	 *
	 * @return bool
	 */
	public function getProductLevelShipping()
	{
		return $this->values['product_level_shipping'] == '1';
	}

	/**
	 * Set is product level shipping
	 *
	 * @param bool $value
	 */
	public function setProductLevelShipping($value)
	{
		$this->values['product_level_shipping'] = $value ? '1' : '0';
	}

	/**
	 * Get is same day delivery
	 *
	 * @return bool
	 */
	public function getSameDayDelivery()
	{
		return $this->values['same_day_delivery'] == '1';
	}

	/**
	 * Set is same day delivery
	 *
	 * @param bool $value
	 */
	public function setSameDayDelivery($value)
	{
		$this->values['same_day_delivery'] = $value ? '1' : '0';
	}

	/**
	 * Get the digital product filename
	 *
	 * @return string
	 */
	public function getDigitalProductFile()
	{
		return $this->values['digital_product_file'];
	}

	/**
	 * Sets the digital product filename
	 *
	 * @param $digitalProductFile
	 */
	public function setDigitalProductFile($digitalProductFile)
	{
		$this->values['digital_product_file'] = $digitalProductFile;
	}

	/**
	 * Get the products location id
	 *
	 * @return int
	 */
	public function getProductsLocationId()
	{
		return $this->values['products_location_id'];
	}

	/**
	 * Sets the products location id
	 *
	 * @param $productsLocationId
	 */
	public function setProductsLocationId($productsLocationId)
	{
		$this->values['products_location_id'] = $productsLocationId;
	}

	/**
	 * Set product SKU
	 *
	 * @param mixed $productSku
	 */
	public function setProductSku($productSku)
	{
		$this->values['product_sku'] = $productSku;
	}

	/**
	 * Get product SKU
	 *
	 * @return mixed
	 */
	public function getProductSku()
	{
		return $this->values['product_sku'];
	}

	/**
	 * Set product Upc
	 *
	 * @param mixed $productUpc
	 */
	public function setProductUpc($productUpc)
	{
		$this->values['product_upc'] = $productUpc;
	}

	/**
	 * Get product Upc
	 *
	 * @return mixed
	 */
	public function getProductUpc()
	{
		return $this->values['product_upc'];
	}

	/**
	 * Set product MPN
	 *
	 * @param mixed $productMpn
	 */
	public function setProductMpn($productMpn)
	{
		$this->values['product_mpn'] = $productMpn;
	}

	/**
	 * Get product MPN
	 *
	 * @return mixed
	 */
	public function getProductMpn()
	{
		return $this->values['product_mpn'];
	}

	/**
	 * Set product GTIN
	 *
	 * @param mixed $productGtin
	 */
	public function setProductGtin($productGtin)
	{
		$this->values['product_gtin'] = $productGtin;
	}

	/**
	 * Get product GTIN
	 *
	 * @return mixed
	 */
	public function getProductGtin()
	{
		return $this->values['product_gtin'];
	}

	/**
	 * Get the title
	 *
	 * @param $title
	 */
	public function setTitle($title)
	{
		$this->values['title'] = $title;
	}

	/**
	 * Sets the title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->values['title'];
	}

	/**
	 * Get the category id
	 *
	 * @param $cid
	 */
	public function setCid($cid)
	{
		$this->values['cid'] = $cid;
	}

	/**
	 * Sets the category id
	 *
	 * @return int
	 */
	public function getCid()
	{
		return $this->values['cid'];
	}

	/**
	 * Get is taxable
	 *
	 * @return string
	 */
	public function getIsTaxable()
	{
		return $this->values['is_taxable'];
	}

	/**
	 * Sets is taxable
	 *
	 * Valid values: Yes or No
	 *
	 * @param $isTaxable
	 */
	public function setIsTaxable($isTaxable)
	{
		$this->values['is_taxable'] = $isTaxable;
	}


	/**
	 * Get the tax class id
	 *
	 * @return int
	 */
	public function getTaxClassId()
	{
		return $this->values['tax_class_id'];
	}

	/**
	 * Sets the tax class id
	 *
	 * @param $taxClassId
	 */
	public function setTaxClassId($taxClassId)
	{
		$this->values['tax_class_id'] = $taxClassId;
	}

	/**
	 * Get the tax rate
	 *
	 * @return float
	 */
	public function getTaxRate()
	{
		return isset($this->values['tax_rate']) ? $this->values['tax_rate'] : 0;
	}

	/**
	 * Sets the tax rate
	 *
	 * @param $taxRate
	 */
	public function setTaxRate($taxRate)
	{
		$this->values['tax_rate'] = $taxRate;
	}

	/**
	 * Get the tax description
	 *
	 * @return string
	 */
	public function getTaxDescription()
	{
		return isset($this->values['tax_description']) ? $this->values['tax_description'] : '';
	}

	/**
	 * Sets the tax description
	 *
	 * @param $taxDescription
	 */
	public function setTaxDescription($taxDescription)
	{
		$this->values['tax_description'] = $taxDescription;
	}

	/**
	 * Get enable recurring billing
	 *
	 * @return bool
	 */
	public function getEnableRecurringBilling()
	{
		return isset($this->values['enable_recurring_billing']) ? $this->values['enable_recurring_billing'] == 1 : false;
	}

	/**
	 * Set recurring billing data
	 *
	 * @param RecurringBilling_Model_ProductRecurringBillingData|null $recurringBillingData
	 */
	public function setRecurringBillingData(RecurringBilling_Model_ProductRecurringBillingData $recurringBillingData = null)
	{
		$this->values['recurring_billing_data'] = $recurringBillingData;
	}

	/**
	 * Get recurring billing data
	 *
	 * @return RecurringBilling_Model_ProductRecurringBillingData|null
	 */
	public function getRecurringBillingData()
	{
		return isset($this->values['recurring_billing_data']) ? $this->values['recurring_billing_data'] : null;
	}

	/**
	 * Calculate the product final pricing based on user level, attributes, and taxes
	 *
	 * @param $userLevel
	 * @param array $attributesData
	 * @param array $settings
	 */
	public function calculateProductPricing($userLevel, array &$attributesData, array &$settings)
	{
		$this->setFinalPrice($this->getPrice());

		$this->setWholesaleProductLevelPricing($userLevel, $settings['WholesaleMaxLevels'], $settings['WholesaleDiscountType']);

		$this->setAttributePricing($attributesData['attributes']);

		$this->setWholesaleGlobalDiscountPricing($userLevel, $settings['WholesaleMaxLevels'],
			$settings['WholesaleDiscount1'], $settings['WholesaleDiscount2'], $settings['WholesaleDiscount3'],
			$settings['WholesaleDiscountType']
		);

		$this->setTaxValues();
	}

	/**
	 * Set tax values
	 */
	public function setTaxValues()
	{
		$taxRate = 0;
		$taxDescription = '';

		$taxProvider = Tax_ProviderFactory::getTaxProvider();

		if ($this->getIsTaxable() == 'Yes')
		{
			if ($this->getTaxRate() > 0)
			{
				$taxRate = $this->getTaxRate();
			}
			elseif ($this->getTaxClassId() > 0 && $taxProvider->hasTaxRate($this->getTaxClassId()))
			{
				$taxRate = $taxProvider->getTaxRate($this->getTaxClassId(), false);
				$taxDescription = $taxProvider->getTaxRateDescription($this->getTaxClassId());
			}
			else
			{
				$this->setIsTaxable('No');
			}
		}

		$this->setTaxRate($taxRate);
		$this->setTaxDescription($taxDescription);

		if ($this->getIsTaxable() == 'Yes' && $taxProvider->getDisplayPricesWithTax())
		{
			$this->calculateVATTax();
		}
	}

	/**
	 * Calculate VAT tax
	 */
	public function calculateVATTax()
	{
		if ($this->getTaxRate() > 0)
		{
			$priceWithTax = PriceHelper::priceWithTax($this->getFinalPrice(), $this->getTaxRate());
			$this->setPriceWithTax($priceWithTax);
		}
	}

	/**
	 * Set wholesale product level pricing
	 *
	 * @param $userLevel
	 * @param $wholesaleMaxLevels
	 * @param $wholesaleDiscountType
	 */
	public function setWholesaleProductLevelPricing($userLevel, $wholesaleMaxLevels, $wholesaleDiscountType)
	{
		$finalPrice = $this->getFinalPrice();

		if ($wholesaleDiscountType == 'PRODUCT')
		{
			if ($userLevel > 0 && $this->getPriceLevel1() > 0 && $wholesaleMaxLevels > 0) $finalPrice = $this->getPriceLevel1();
			if ($userLevel > 1 && $this->getPriceLevel2() > 0 && $wholesaleMaxLevels > 1) $finalPrice = $this->getPriceLevel2();
			if ($userLevel > 2 && $this->getPriceLevel3() > 0 && $wholesaleMaxLevels > 2) $finalPrice = $this->getPriceLevel3();
		}

		$this->setFinalPrice($finalPrice);
	}


	/**
	 * Set attribute pricing
	 *
	 * @param $lineItemAttributes
	 */
	public function setAttributePricing($lineItemAttributes)
	{
		$finalPrice = $this->getFinalPrice();

		$finalComputedPrice = $finalPrice;

		foreach ($lineItemAttributes as $attribute)
		{
			$modPrice = 0;

			if ($attribute['attribute_value'] * 1 != 0)
			{
				if ($attribute['attribute_value_type'] == 'amount')
				{
					$modPrice += $attribute['attribute_value'];
				}
				else
				{
					$modPrice += PriceHelper::round($finalPrice * $attribute['attribute_value'] / 100);
				}
			}

			$finalComputedPrice += $modPrice;
		}

		$this->setFinalPrice($finalComputedPrice);
	}

	/**
	 * Set wholesale global discount pricing
	 *
	 * @param $userLevel
	 * @param $wholesaleMaxLevels
	 * @param $wholesaleDiscount1
	 * @param $wholesaleDiscount2
	 * @param $wholesaleDiscount3
	 * @param $wholesaleDiscountType
	 */
	public function setWholesaleGlobalDiscountPricing($userLevel, $wholesaleMaxLevels, $wholesaleDiscount1, $wholesaleDiscount2, $wholesaleDiscount3, $wholesaleDiscountType)
	{
		$finalPrice = $this->getFinalPrice();

		if ($wholesaleDiscountType == 'GLOBAL')
		{
			$wholesaleDiscount = false;

			if ($userLevel > 0 && $wholesaleMaxLevels > 0) $wholesaleDiscount = $wholesaleDiscount1;
			if ($userLevel > 1 && $wholesaleMaxLevels > 1) $wholesaleDiscount = $wholesaleDiscount2;
			if ($userLevel > 2 && $wholesaleMaxLevels > 2) $wholesaleDiscount = $wholesaleDiscount3;

			if ($wholesaleDiscount !== false)
			{
				$this->setFinalPrice(PriceHelper::round($finalPrice * (100 - $wholesaleDiscount) / 100));
			}
		}
	}

	/**
	 * Parse attributes
	 *
	 * @param array $selectedAttributesOptions
	 * @param array $productAttributes
	 * @param bool $attributesUseAttributeIdKey
	 *
	 * @return array
	 */
	public function parseAttributes(array &$selectedAttributesOptions, $productAttributes, $attributesUseAttributeIdKey = true)
	{
		$productPrice = $this->getFinalPrice();
		$productWeight = $this->getWeight();
		$inventoryControl = $this->getInventoryControl();

		$selectedOptions = '';
		$selectedOptionsClean = '';
		$selectedAttributes = array();
		$inventoryList = '';

		$modWeight = $productWeight;

		if (count($selectedAttributesOptions) > 0)
		{
			foreach ($productAttributes as $attribute)
			{
				$attributeId = $attribute['paid'];

				$selectedAttribute = array();
				$selectedAttribute['attribute_value'] = 0;
				$selectedAttribute['attribute_value_type'] = 'amount';

				if (($attributesUseAttributeIdKey && array_key_exists($attributeId, $selectedAttributesOptions)) || (!$attributesUseAttributeIdKey && array_key_exists($attribute['name'], $selectedAttributesOptions)))
				{
					$attributeValue = $attributesUseAttributeIdKey ? $selectedAttributesOptions[$attributeId] : $selectedAttributesOptions[$attribute['name']];

					if (in_array($attribute['attribute_type'], array('select', 'radio')))
					{
						$options = array();

						if (parseOptions($attribute['options'], $productPrice, $productWeight, $options))
						{
							$selectedOption = $attributeValue;

							$selectedOptions .= $attribute['name'].' : '.$selectedOption;
							$selectedOptions .= ($attribute['is_modifier'] == 'Yes' ? $options[$selectedOption]['modifier'] : '')."\r\n";

							if ($attribute['is_modifier'] == 'Yes')
							{
								$selectedOptionsClean .= $attribute['name'].' : '.$options[$selectedOption]['full']."\r\n";

								$modWeight += $options[$selectedOption]['weight_difference'];

								$selectedAttribute['attribute_value'] = $options[$selectedOption]['option_value'];
								$selectedAttribute['attribute_value_type'] = $options[$selectedOption]['option_value_type'];
							}
							else
							{
								$selectedOptionsClean .= $attribute['name'].' : '.$options[$selectedOption]['full']."\r\n";
							}

							if ($attribute['track_inventory'] == '1')
							{
								$inventoryList .= ($inventoryList == '' ? '' : "\n").$attribute['paid'].': '.$selectedOption;
							}
						}
					}
					else
					{
						$selectedOption = $attributeValue;

						$selectedOptions .= $attribute['name'].' : '.$selectedOption."\r\n";
						$selectedOptionsClean .= $attribute['name'].' : '.$selectedOption."\r\n";
					}
				}

				$selectedAttributes[] = $selectedAttribute;
			}
			$selectedOptions = trim($selectedOptions);
			$selectedOptionsClean = trim($selectedOptionsClean);
		}

		return array(
			'weight' => $modWeight,
			'options' => $selectedOptions,
			'options_clean' => $selectedOptionsClean,
			'attributes' => $selectedAttributes,
			'inventoryList' => $inventoryList,
		);
	}
}
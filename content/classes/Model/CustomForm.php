<?php

/**
 * Class Model_CustomForm
 */
class Model_CustomForm
{
	protected $id = null;
	protected $isActive = false;
	protected $isTitleVisible = false;
	protected $isRecaptcha = false;
	protected $columnsCount = 1;
	protected $saveData = true;
	protected $emailNotification = true;
	protected $redirectOnSubmit = false;
	protected $formId = '';
	protected $title = '';
	protected $emailNotificationSubject = '';
	protected $redirectOnSubmitUrl = '';
	protected $description;
	protected $thankYouMessage;

	/**
	 * Model_CustomForm constructor.
	 * @param null $data
	 */
	public function __construct($data = null)
	{
		if ($data != null && is_array($data)) $this->hydrate($data);
	}

	/**
	 * @param $data
	 */
	public function hydrate($data)
	{
		if (isset($data['id'])) $this->setId($data['id']);
		if (isset($data['columns_count'])) $this->setColumnsCount(intval($data['columns_count']));

		if (isset($data['is_active'])) $this->setIsActive($data['is_active'] == '1');
		if (isset($data['is_title_visible'])) $this->setIsTitleVisible($data['is_title_visible'] == '1');
		if (isset($data['is_recaptcha'])) $this->setIsRecaptcha($data['is_recaptcha'] == '1');
		if (isset($data['save_data'])) $this->setSaveData($data['save_data'] == '1');
		if (isset($data['email_notification'])) $this->setEmailNotification($data['email_notification'] == '1');
		if (isset($data['redirect_on_submit'])) $this->setRedirectOnSubmit($data['redirect_on_submit'] == '1');

		if (isset($data['form_id'])) $this->setFormId($data['form_id']);
		if (isset($data['title'])) $this->setTitle($data['title']);
		if (isset($data['email_notification_subject'])) $this->setEmailNotificationSubject($data['email_notification_subject']);
		if (isset($data['redirect_on_submit_url'])) $this->setRedirectOnSubmitUrl($data['redirect_on_submit_url']);
		if (isset($data['description'])) $this->setDescription($data['description']);
		if (isset($data['thank_you_message'])) $this->setThankYouMessage($data['thank_you_message']);
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return array(
			'id' => $this->getId(),
			'is_active' => $this->getIsActive() ? '1' : '0',
			'is_title_visible' => $this->getIsTitleVisible() ? '1' : '0',
			'is_recaptcha' => $this->getIsRecaptcha() ? '1' : '0',
			'columns_count' => $this->getColumnsCount(),
			'save_data' => $this->getSaveData() ? '1' : '0',
			'email_notification' => $this->getEmailNotification() ? '1' : '0',
			'redirect_on_submit' => $this->getRedirectOnSubmit() ? '1' : '0',
			'form_id' => $this->getFormId(),
			'title' => $this->getTitle(),
			'email_notification_subject' => $this->getEmailNotificationSubject(),
			'redirect_on_submit_url' => $this->getRedirectOnSubmitUrl(),
			'description' => $this->getDescription(),
			'thank_you_message' => $this->getThankYouMessage()
		);
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return boolean
	 */
	public function getIsActive()

	{
		return $this->isActive;
	}

	/**
	 * @param boolean $isActive
	 */
	public function setIsActive($isActive)
	{
		$this->isActive = $isActive;
	}

	/**
	 * @return int
	 */
	public function getColumnsCount()
	{
		return $this->columnsCount;
	}

	/**
	 * @param int $columnsCount
	 */
	public function setColumnsCount($columnsCount)
	{
		$this->columnsCount = $columnsCount;
	}

	/**
	 * @return boolean
	 */
	public function getSaveData()
	{
		return $this->saveData;
	}

	/**
	 * @param boolean $saveData
	 */
	public function setSaveData($saveData)
	{
		$this->saveData = $saveData;
	}

	/**
	 * @return boolean
	 */
	public function getEmailNotification()
	{
		return $this->emailNotification;
	}

	/**
	 * @param boolean $emailNotification
	 */
	public function setEmailNotification($emailNotification)
	{
		$this->emailNotification = $emailNotification;
	}

	/**
	 * @return boolean
	 */
	public function getRedirectOnSubmit()
	{
		return $this->redirectOnSubmit;
	}

	/**
	 * @param boolean $redirectOnSubmit
	 */
	public function setRedirectOnSubmit($redirectOnSubmit)
	{
		$this->redirectOnSubmit = $redirectOnSubmit;
	}

	/**
	 * @return string
	 */
	public function getFormId()
	{
		return $this->formId;
	}

	/**
	 * @param string $formId
	 */
	public function setFormId($formId)
	{
		$this->formId = $formId;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getEmailNotificationSubject()
	{
		return $this->emailNotificationSubject;
	}

	/**
	 * @param string $emailNotificationSubject
	 */
	public function setEmailNotificationSubject($emailNotificationSubject)
	{
		$this->emailNotificationSubject = $emailNotificationSubject;
	}

	/**
	 * @return string
	 */
	public function getRedirectOnSubmitUrl()
	{
		return $this->redirectOnSubmitUrl;
	}

	/**
	 * @param string $redirectOnSubmitUrl
	 */
	public function setRedirectOnSubmitUrl($redirectOnSubmitUrl)
	{
		$this->redirectOnSubmitUrl = $redirectOnSubmitUrl;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getThankYouMessage()
	{
		return $this->thankYouMessage;
	}

	/**
	 * @param mixed $thankYouMessage
	 */
	public function setThankYouMessage($thankYouMessage)
	{
		$this->thankYouMessage = $thankYouMessage;
	}

	/**
	 * @return boolean
	 */
	public function getIsRecaptcha()
	{
		return $this->isRecaptcha;
	}

	/**
	 * @param boolean $isRecaptcha
	 */
	public function setIsRecaptcha($isRecaptcha)
	{
		$this->isRecaptcha = $isRecaptcha;
	}

	/**
	 * @return boolean
	 */
	public function getIsTitleVisible()
	{
		return $this->isTitleVisible;
	}

	/**
	 * @param boolean $isTitleVisible
	 */
	public function setIsTitleVisible($isTitleVisible)
	{
		$this->isTitleVisible = $isTitleVisible;
	}
}
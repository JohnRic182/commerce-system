<?php

/**
 * Class Model_LineItem
 */
class Model_LineItem
{
	protected $values;

	/**
	 * Class constructor
	 *
	 * @param array $lineItemValues
	 */
	public function __construct(&$lineItemValues)
	{
		$this->values = &$lineItemValues;

		/**
		 * Get recurring billing data stored on line item level
		 */
		$recurringBillingData = null;

		if (isset($this->values['recurring_billing_data']) && $this->values['recurring_billing_data'] != '')
		{
			$recurringBillingDataArray = @unserialize($this->values['recurring_billing_data']);
			if ($recurringBillingDataArray && is_array($recurringBillingDataArray))
			{
				$recurringBillingData = new RecurringBilling_Model_LineItemRecurringBillingData($recurringBillingDataArray);
			}
		}

		$this->setRecurringBillingData($recurringBillingData);

		/**
		 * get recurring billing data stored on product level
		 */
		$productRecurringBillingData = null;

		if (isset($this->values['product_recurring_billing_data']) && $this->values['product_recurring_billing_data'] != '')
		{
			$productRecurringBillingDataArray = @unserialize($this->values['product_recurring_billing_data']);
			if ($productRecurringBillingDataArray && is_array($productRecurringBillingDataArray))
			{
				$productRecurringBillingData = new RecurringBilling_Model_ProductRecurringBillingData($productRecurringBillingDataArray);
			}
		}

		$this->setProductRecurringBillingData($productRecurringBillingData);

		$this->parseValues();
	}

	/**
	 * Sets the admin price
	 *
	 * @param float $adminPrice
	 */
	public function setAdminPrice($adminPrice)
	{
		$this->values['admin_price'] = $adminPrice;

		// Update product_total
		// These values for backward support with themes / display
		$this->values['product_price'] = $adminPrice;
		$this->values['product_total'] = $this->getSubtotal();
	}

	/**
	 * Gets the admin price
	 *
	 * @return float
	 */
	public function getAdminPrice()
	{
		return $this->values['admin_price'];
	}

	/**
	 * Sets the price
	 *
	 * @param float $price
	 */
	public function setPrice($price)
	{
		$this->values['price'] = $price;
	}

	/**
	 * Gets the price
	 *
	 * @return float
	 */
	public function getPrice()
	{
		return $this->values['price'];
	}

	/**
	 * Sets the admin quantity
	 *
	 * @param int $adminQuantity
	 */
	public function setAdminQuantity($adminQuantity)
	{
		$this->values['admin_quantity'] = $adminQuantity;

		// Update product_total
		$this->values['product_total'] = $this->getSubtotal();
	}

	/**
	 * Gets the admin quantity
	 *
	 * @return int
	 */
	public function getAdminQuantity()
	{
		return $this->values['admin_quantity'];
	}

	/**
	 * Sets the quantity
	 *
	 * @param int $quantity
	 */
	public function setQuantity($quantity)
	{
		$this->values['quantity'] = $quantity;
	}

	/**
	 * Gets the quantity
	 *
	 * @return int
	 */
	public function getQuantity()
	{
		return $this->values['quantity'];
	}

	/**
	 * Sets the final price
	 *
	 * @param float $finalPrice
	 */
	public function setFinalPrice($finalPrice)
	{
		$this->setPrice($finalPrice);
		$this->setAdminPrice($finalPrice);
	}

	/**
	 * Gets the final price
	 *
	 * @return float
	 */
	public function getFinalPrice()
	{
		return $this->getAdminPrice();
	}

	/**
	 * Sets the final quantity
	 *
	 * @param int $finalQuantity
	 */
	public function setFinalQuantity($finalQuantity)
	{
		$this->setQuantity($finalQuantity);
		$this->setAdminQuantity($finalQuantity);
	}

	/**
	 * Gets the final quantity
	 *
	 * @return int
	 */
	public function getFinalQuantity()
	{
		return $this->getAdminQuantity();
	}

	/**
	 * Sets the discount amount
	 *
	 * @param float $discountAmount
	 */
	public function setDiscountAmount($discountAmount)
	{
		$this->values['discount_amount'] = $discountAmount;
	}

	/**
	 * Gets the discount amount
	 *
	 * @return float
	 */
	public function getDiscountAmount()
	{
		return $this->values['discount_amount'];
	}

	/**
	 * Sets the discount amount with tax
	 *
	 * @param float $discountAmountWithTax
	 */
	public function setDiscountAmountWithTax($discountAmountWithTax)
	{
		$this->values['discount_amount_with_tax'] = $discountAmountWithTax;
	}

	/**
	 * Gets the discount amount with tax
	 *
	 * @return float
	 */
	public function getDiscountAmountWithTax()
	{
		return $this->values['discount_amount_with_tax'];
	}

	/**
	 * Sets the promo code discount amount
	 *
	 * @param float $promoDiscountAmount
	 */
	public function setPromoDiscountAmount($promoDiscountAmount)
	{
		$this->values['promo_discount_amount'] = $promoDiscountAmount;
	}

	/**
	 * Gets the promo code discount amount
	 *
	 * @return float
	 */
	public function getPromoDiscountAmount()
	{
		return $this->values['promo_discount_amount'];
	}

	/**
	 * Sets the promo code discount amount with tax
	 *
	 * @param float $promoDiscountAmountWithTax
	 */
	public function setPromoDiscountAmountWithTax($promoDiscountAmountWithTax)
	{
		$this->values['promo_discount_amount_with_tax'] = $promoDiscountAmountWithTax;
	}

	/**
	 * Gets the promo code discount amount with tax
	 *
	 * @return float
	 */
	public function getPromoDiscountAmountWithTax()
	{
		return $this->values['promo_discount_amount_with_tax'];
	}

	/**
	 * Sets the line item attribute value
	 *
	 * @param float $attributeValue
	 */
	public function setAttributeValue($attributeValue)
	{
		$this->values['attribute_value'] = $attributeValue;
	}

	/**
	 * Gets the line item attribute modifier value
	 *
	 * @return float
	 */
	public function getAttributeValue()
	{
		return $this->values['attribute_value'];
	}

	/**
	 * Sets the line item attribute value type
	 *
	 * Valid values: amount or percentage
	 *
	 * @param string $attributeValueType
	 */
	public function setAttributeValueType($attributeValueType)
	{
		$this->values['attribute_value_type'] = $attributeValueType;
	}

	/**
	 * Gets the line item attribute value type
	 *
	 * @return string
	 */
	public function getAttributeValueType()
	{
		return $this->values['attribute_value_type'];
	}

	/**
	 * Sets product type
	 *
	 * Valid values: Tangible, Virtual, or Digital
	 *
	 * @param string $productType
	 */
	public function setProductType($productType)
	{
		$this->values['product_type'] = $productType;
	}

	/**
	 * Gets product type
	 *
	 * @return string
	 */
	public function getProductType()
	{
		return $this->values['product_type'];
	}

	/**
	 * Sets the number of times the digital item has been downloaded
	 *
	 * @param int $digitalProductDownloads
	 */
	public function setDigitalProductDownloads($digitalProductDownloads)
	{
		$this->values['digital_product_downloads'] = $digitalProductDownloads;
	}

	/**
	 * Gets the number of times the digital item has been downloaded
	 *
	 * @return int
	 */
	public function getDigitalProductDownloads()
	{
		return $this->values['digital_product_downloads'];
	}

	/**
	 * Sets the digital product filename
	 *
	 * @param string $digitalProductFile
	 */
	public function setDigitalProductFile($digitalProductFile)
	{
		$this->values['digital_product_file'] = $digitalProductFile;
	}

	/**
	 * Gets the digital product filename
	 *
	 * @return string
	 */
	public function getDigitalProductFile()
	{
		return $this->values['digital_product_file'];
	}

	/**
	 * Sets the digital product key
	 *
	 * @param string $digitalProductKey
	 */
	public function setDigitalProductKey($digitalProductKey)
	{
		$this->values['digital_product_key'] = $digitalProductKey;
	}

	/**
	 * Gets the digital product key
	 *
	 * @return string
	 */
	public function getDigitalProductKey()
	{
		return $this->values['digital_product_key'];
	}

	/**
	 * Sets whether this line item has free shipping
	 *
	 * Valid values: Yes or No
	 *
	 * @param string $freeShipping
	 */
	public function setFreeShipping($freeShipping)
	{
		$this->values['free_shipping'] = $freeShipping;
	}

	/**
	 * Gets whether this line item has free shipping
	 *
	 * @return string
	 */
	public function getFreeShipping()
	{
		return $this->values['free_shipping'];
	}

	/**
	 * Sets whether this line items parent product has free shipping
	 *
	 * Valid values: Yes or No
	 *
	 * @param string $productFreeShipping
	 */
	public function setProductFreeShipping($productFreeShipping)
	{
		$this->values['product_free_shipping'] = $productFreeShipping;
	}

	/**
	 * Gets whether this line item has free shipping
	 *
	 * @return string
	 */
	public function getProductFreeShipping()
	{
		return $this->values['product_free_shipping'];
	}

	/**
	 * @return mixed
	 */
	public function getProductLevelShipping()
	{
		return $this->values['product_level_shipping'] == '1';
	}

	/**
	 * Valid values: 1 or 0
	 */
	public function setProductLevelShipping($value)
	{
		$this->values['product_level_shipping'] = $value ? '1' : '0';
	}

	/**
	 * Get is same day delivery
	 *
	 * @return bool
	 */
	public function getSameDayDelivery()
	{
		return $this->values['same_day_delivery'] == '1';
	}

	/**
	 * Set is same day delivery
	 *
	 * @param bool $value
	 */
	public function setSameDayDelivery($value)
	{
		$this->values['same_day_delivery'] = $value ? '1' : '0';
	}

	/**
	 * Sets the inventory id
	 *
	 * @param int $inventoryId
	 */
	public function setInventoryId($inventoryId)
	{
		$this->values['inventory_id'] = $inventoryId;
	}

	/**
	 * Gets the inventory id
	 *
	 * @return int
	 */
	public function getInventoryId()
	{
		return $this->values['inventory_id'];
	}

	/**
	 * Sets the inventory control
	 *
	 * @param string $inventoryControl
	 */
	public function setInventoryControl($inventoryControl)
	{
		$this->values['inventory_control'] = $inventoryControl;
	}

	/**
	 * Gets the inventory control
	 *
	 * @return string
	 */
	public function getInventoryControl()
	{
		return $this->values['inventory_control'];
	}

	/**
	 * Sets is doba item
	 *
	 * Valid values: Yes or No
	 *
	 * @param string $isDoba
	 */
	public function setIsDoba($isDoba)
	{
		$this->values['is_doba'] = $isDoba;
	}

	/**
	 * Gets is doba item
	 *
	 * @return string
	 */
	public function getIsDoba()
	{
		return $this->values['is_doba'];
	}

	/**
	 * Sets is gift product item
	 *
	 * Valid values: Yes or No
	 *
	 * @param string $isGift
	 */
	public function setIsGift($isGift)
	{
		$this->values['is_gift'] = $isGift;
	}

	/**
	 * Gets is gift product item
	 *
	 * @return string
	 */
	public function getIsGift()
	{
		return $this->values['is_gift'];
	}

	/**
	 * Set is shipping price
	 *
	 * @param string $isShippingPrice
	 */
	public function setIsShippingPrice($isShippingPrice)
	{
		$this->values['is_shipping_price'] = $isShippingPrice;
	}

	/**
	 * Get is shipping price
	 *
	 * @return string
	 */
	public function getIsShippingPrice()
	{
		return $this->values['is_shipping_price'];
	}

	/**
	 * Set quantity from stock
	 *
	 * @param int $quantityFromStock
	 */
	public function setQuantityFromStock($quantityFromStock)
	{
		$this->values['quantity_from_stock'] = $quantityFromStock;
	}

	/**
	 * Get quantity from stock
	 *
	 * @return int
	 */
	public function getQuantityFromStock()
	{
		return $this->values['quantity_from_stock'];
	}

	/**
	 * Set is stock changed
	 *
	 * @param string $isStockChanged
	 */
	public function setIsStockChanged($isStockChanged)
	{
		$this->values['is_stock_changed'] = $isStockChanged;
	}

	/**
	 * Get is stock changes
	 *
	 * @return string
	 */
	public function getIsStockChanged()
	{
		return $this->values['is_stock_changed'];
	}

	/**
	 * Set is taxable
	 *
	 * @param string $isTaxable
	 */
	public function setIsTaxable($isTaxable)
	{
		$this->values['is_taxable'] = $isTaxable;
	}

	/**
	 * Get is taxable
	 *
	 * @return string
	 */
	public function getIsTaxable()
	{
		return $this->values['is_taxable'];
	}

	/**
	 * Set line item id
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->values['ocid'] = $id;
	}

	/**
	 * Get line item id
	 *
	 * @return mixed
	 */
	public function getId()
	{
		return $this->values['ocid'];
	}

	/**
	 * Set line item options
	 *
	 * @param mixed $options
	 */
	public function setOptions($options)
	{
		$this->values['options'] = $options;
	}

	/**
	 * Get line item options
	 *
	 * @return mixed
	 */
	public function getOptions()
	{
		return $this->values['options'];
	}

	/**
	 * Set options clean
	 *
	 * @param mixed $optionsClean
	 */
	public function setOptionsClean($optionsClean)
	{
		$this->values['options_clean'] = $optionsClean;
	}

	/**
	 * Get options clean
	 *
	 * @return mixed
	 */
	public function getOptionsClean()
	{
		return $this->values['options_clean'];
	}

	/**
	 * Set order id
	 *
	 * @param mixed $orderId
	 */
	public function setOrderId($orderId)
	{
		$this->values['oid'] = $orderId;
	}

	/**
	 * Get order id
	 *
	 * @return mixed
	 */
	public function getOrderId()
	{
		return $this->values['oid'];
	}

	/**
	 * Set product id
	 *
	 * @param mixed $pid
	 */
	public function setPid($pid)
	{
		$this->values['pid'] = $pid;
	}

	/**
	 * Get product id
	 *
	 * @return mixed
	 */
	public function getPid()
	{
		return $this->values['pid'];
	}

	/**
	 * Set price before quantity discount
	 *
	 * @param mixed $priceBeforeQuantityDiscount
	 */
	public function setPriceBeforeQuantityDiscount($priceBeforeQuantityDiscount)
	{
		$this->values['price_before_quantity_discount'] = $priceBeforeQuantityDiscount;
	}

	/**
	 * Get price before quantity discount
	 *
	 * @return mixed
	 */
	public function getPriceBeforeQuantityDiscount()
	{
		return $this->values['price_before_quantity_discount'];
	}

	/**
	 * Set product GTIN
	 *
	 * @param mixed $productGtin
	 */
	public function setProductGtin($productGtin)
	{
		$this->values['product_gtin'] = $productGtin;
	}

	/**
	 * Get product GTIN
	 *
	 * @return mixed
	 */
	public function getProductGtin()
	{
		return $this->values['product_gtin'];
	}

	/**
	 * Set product id
	 *
	 * @param mixed $productId
	 */
	public function setProductId($productId)
	{
		$this->values['product_id'] = $productId;
	}

	/**
	 * Get product id
	 *
	 * @return mixed
	 */
	public function getProductId()
	{
		return $this->values['product_id'];
	}

	/**
	 * Set product MPN
	 *
	 * @param mixed $productMpn
	 */
	public function setProductMpn($productMpn)
	{
		$this->values['product_mpn'] = $productMpn;
	}

	/**
	 * Get product MPN
	 *
	 * @return mixed
	 */
	public function getProductMpn()
	{
		return $this->values['product_mpn'];
	}

	/**
	 * Set product removed
	 *
	 * @param string $productRemoved
	 */
	public function setProductRemoved($productRemoved)
	{
		$this->values['product_removed'] = $productRemoved;
	}

	/**
	 * Get product remove
	 *
	 * @return string
	 */
	public function getProductRemoved()
	{
		return $this->values['product_removed'];
	}

	/**
	 * Set product SKU
	 *
	 * @param mixed $productSku
	 */
	public function setProductSku($productSku)
	{
		$this->values['product_sku'] = $productSku;
	}

	/**
	 * Get product SKU
	 *
	 * @return mixed
	 */
	public function getProductSku()
	{
		return $this->values['product_sku'];
	}

	/**
	 * Set product sub id
	 *
	 * @param mixed $productSubId
	 */
	public function setProductSubId($productSubId)
	{
		$this->values['product_sub_id']= $productSubId;
	}

	/**
	 * Get product sub id
	 *
	 * @return mixed
	 */
	public function getProductSubId()
	{
		return $this->values['product_sub_id'];
	}

	/**
	 * Set product Upc
	 *
	 * @param mixed $productUpc
	 */
	public function setProductUpc($productUpc)
	{
		$this->values['product_upc'] = $productUpc;
	}

	/**
	 * Get product Upc
	 *
	 * @return mixed
	 */
	public function getProductUpc()
	{
		return $this->values['product_upc'];
	}

	/**
	 * Set product location id
	 *
	 * @param mixed $productsLocationId
	 */
	public function setProductsLocationId($productsLocationId)
	{
		$this->values['products_location_id'] = $productsLocationId;
	}

	/**
	 * Get product location id
	 *
	 * @return mixed
	 */
	public function getProductsLocationId()
	{
		return $this->values['products_location_id'];
	}

	/**
	 * Set product shipping price
	 *
	 * @param mixed $shippingPrice
	 */
	public function setShippingPrice($shippingPrice)
	{
		$this->values['shipping_price'] = $shippingPrice;
	}

	/**
	 * Get shipping price
	 *
	 * @return mixed
	 */
	public function getShippingPrice()
	{
		return $this->values['shipping_price'];
	}

	/**
	 * Set tax description
	 *
	 * @param string $taxDescription
	 */
	public function setTaxDescription($taxDescription)
	{
		$this->values['tax_description'] = $taxDescription;
	}

	/**
	 * Get tax description
	 *
	 * @return string
	 */
	public function getTaxDescription()
	{
		return $this->values['tax_description'];
	}

	/**
	 * Set tax rate
	 *
	 * @param float $taxRate
	 */
	public function setTaxRate($taxRate)
	{
		$this->values['tax_rate'] = $taxRate;
	}

	/**
	 * Get tax rate
	 *
	 * @return float
	 */
	public function getTaxRate()
	{
		return $this->values['tax_rate'];
	}

	/**
	 * Set tax exemption
	 *
	 * @param string $taxExempt
	 */
	public function setTaxExempt($taxExempt)
	{
		$this->values['tax_exempt'] = $taxExempt;
	}

	/**
	 * Get tax rate
	 *
	 * @return string
	 */
	public function getTaxExempt()
	{
		return $this->values['tax_exempt'];
	}

	/**
	 * Set line total tax amount
	 *
	 * @param float $tax_amount
	 */
	public function setTaxAmount($tax_amount)
	{
		$this->values['tax_amount'] = $tax_amount;
	}

	/**
	 * Get line total tax amount
	 *
	 * @return float
	 */
	public function getTaxAmount()
	{
		return $this->values['tax_amount'];
	}

	/**
	 * Set product title (name)
	 *
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->values['title'] = $title;
	}

	/**
	 * Get product title / name
	 *
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->values['title'];
	}

	/**
	 * Set product weight
	 *
	 * @param mixed $weight
	 */
	public function setWeight($weight)
	{
		$this->values['weight'] = $weight;
	}

	/**
	 * Get product weight
	 *
	 * @return mixed
	 */
	public function getWeight()
	{
		return $this->values['weight'];
	}

	/**
	 * @param $code
	 */
	public function setExactorEucCode($code)
	{
		$this->values['exactor_euc_code'] = $code;
	}

	/**
	 * @return mixed
	 */
	public function getExactorCategoryEucCode()
	{
		return $this->values['cat_exactor_euc_code'];
	}

	/**
	 * @param $code
	 */
	public function setExactorCategoryEucCode($code)
	{
		$this->values['cat_exactor_euc_code'] = $code;
	}

	/**
	 * @return mixed
	 */
	public function getExactorEucCode()
	{
		return $this->values['exactor_euc_code'];
	}

	/**
	 * Set line item attributes
	 *
	 * @param $attributes
	 */
	public function setAttributes($attributes)
	{
		$this->values['attributes'] = $attributes;
	}

	/**
	 * Get line item attributes
	 *
	 * @return mixed
	 */
	public function getAttributes()
	{
		return $this->values['attributes'];
	}

	/**
	 * Set inventory list
	 *
	 * @param $inventoryList
	 */
	public function setInventoryList($inventoryList)
	{
		$this->values['inventoryList'] = $inventoryList;
	}

	/**
	 * Get inventory list
	 *
	 * @return mixed
	 */
	public function getInventoryList()
	{
		return $this->values['inventoryList'];
	}

	/**
	 * Set price with tax
	 *
	 * @param $priceWithTax
	 */
	public function setPriceWithTax($priceWithTax)
	{
		if (strtolower($this->getIsGift()) == 'yes')
			$priceWithTax = 0;

		$this->values['price_withtax'] = $priceWithTax;
	}
	/**
	 * Get price with tax
	 *
	 * @return mixed
	 */
	public function getPriceWithTax()
	{
		if (strtolower($this->getIsGift()) == 'yes') return 0;

		return isset($this->values['price_withtax']) ? $this->values['price_withtax'] : $this->getFinalPrice();
	}

	/**
	 * Get subtotal
	 *
	 * @return float
	 */
	public function getSubtotal($withTax = false)
	{
		if (strtolower($this->getIsGift()) == 'yes') return 0;

		return PriceHelper::round(($withTax ? $this->getPriceWithTax() : $this->getFinalPrice()) * $this->getFinalQuantity());
	}

	/**
	 * Get total
	 *
	 * @return float
	 */
	public function getTotal()
	{
		return $this->getSubtotal() - $this->getDiscountTotal();
	}

	/**
	 * Get line item total discount
	 *
	 * @return float
	 */
	public function getDiscountTotal()
	{
		$discountTotal = $this->getDiscountAmount() + $this->getPromoDiscountAmount();

		if ($discountTotal > $this->getSubtotal())
		{
			$discountTotal = $this->getSubtotal();
		}

		return $discountTotal;
	}

	/**
	 * @return float
	 */
	public function getDiscountTotalWithTax()
	{
		$discountTotal = $this->getDiscountAmountWithTax() + $this->getPromoDiscountAmountWithTax();

		if ($discountTotal > $this->getSubtotal(true))
		{
			$discountTotal = $this->getSubtotal(true);
		}

		return $discountTotal;
	}

	/**
	 * @return bool
	 */
	public function getFulfillmentStatus()
	{
		return isset($this->values['fulfillment_status']) ? $this->values['fulfillment_status'] : false;
	}

	/**
	 * @param $fulfillmentStatus
	 */
	public function setFulfillmentStatus($fulfillmentStatus)
	{
		$this->values['fulfillment_status'] = $fulfillmentStatus;
	}

	/**
	 * @return mixed
	 */
	public function getFulfilledQuantity()
	{
		return $this->values['fulfilled_quantity'];
	}

	/**
	 * @param $quantity
	 */
	public function setFulfilledQuantity($quantity)
	{
		$this->values['fulfilled_quantity'] = $quantity;
	}

	/**
	 * @return int
	 */
	public function getQuantityToFulfill()
	{
		if ($this->getFinalQuantity() > $this->getFulfilledQuantity())
		{
			return $this->getFinalQuantity() - $this->getFulfilledQuantity();
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Set shipment id
	 *
	 * @param mixed $shipmentId
	 */
	public function setShipmentId($shipmentId)
	{
		$this->values['shipment_id'] = $shipmentId;
	}

	/**
	 * Get shipment id
	 *
	 * @return mixed
	 */
	public function getShipmentId()
	{
		return $this->values['shipment_id'];
	}

	/**
	 * @return array
	 */
	public function parseOptions()
	{
		$options = array();

		//parse options
		$s = explode("\n", $this->values["options"]);

		for ($j=0; $j<count($s); $j++)
		{
			$pos = strpos($s[$j], " : ");

			if ($pos !== false)
			{
				$name = trim(substr($s[$j], 0, $pos));
				$value = trim(substr($s[$j], $pos + 3));

				$pos2 = strpos($value, "(");
				if ($pos2 !== false)
				{
					$value = trim(substr($value, 0, $pos2));
				}

				$options[$name] = $value;
			}
		}

		return $options;
	}

	/**
	 * Parse values
	 */
	protected function parseValues()
	{
		if ($this->values['product_id'] == 'gift_certificate')
		{
			$this->values['url'] = $this->values['cat_url'] = 'index.php?p=gift_certificate';
		}
		else
		{
			$this->values['url'] = UrlUtils::getProductUrl($this->values['product_url'], $this->values['pid'], $this->values['cid']);
			$this->values['cat_url'] = UrlUtils::getCatalogUrl($this->values['cat_url'], $this->values['cid']);
		}

		//inventory, min / max order
		if ($this->values["inventory_control"] == "Yes")
		{
			$stock = $this->values['stock'] + $this->getQuantityFromStock();
			$this->values['allowed_max'] = $this->values['max_order'] > 0 && $this->values['max_order'] < $stock ? $this->values['max_order'] : $stock;
		}
		else
		{
			$this->values["allowed_max"] = 0;
		}

		$options = $this->parseOptions();

		foreach ($options as $name => $value)
		{
			$this->values["options_parsed"][] = array(
				"name" => $name,
				"value" => $value
			);
		}

		if (strtolower($this->getIsGift()) == 'yes')
		{
			$this->setFinalPrice(0);
		}

		$this->values['product_total'] = $this->getSubtotal();
	}

	/**
	 * Return as array
	 *
	 * @return array
	 */
	public function &toArray()
	{
		return $this->values;
	}

	/**
	 * Set attributes data
	 *
	 * @param array $attributesData
	 */
	public function setAttributesData(array $attributesData)
	{
		$this->setWeight($this->getWeight() + $attributesData['weight']);
		$this->setOptions($attributesData['options']);
		$this->setOptionsClean($attributesData['options_clean']);
		$this->setAttributes($attributesData['attributes']);
		$this->setInventoryList($attributesData['inventoryList']);
	}

	/**
	 * Check is this a recurring billing item
	 *
	 * @return bool
	 */
	public function getEnableRecurringBilling()
	{
		return isset($this->values['enable_recurring_billing']) ? $this->values['enable_recurring_billing'] : false;
	}

	/**
	 * Set recurring billing data
	 *
	 * @param RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData
	 */
	public function setRecurringBillingData(RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData = null)
	{
		$this->values['recurring_billing_data'] = $recurringBillingData;
		$this->values['enable_recurring_billing'] = !is_null($recurringBillingData);
	}

	/**
	 * Return recurring billing data
	 *
	 * @return RecurringBilling_Model_LineItemRecurringBillingData|null
	 */
	public function getRecurringBillingData()
	{
		return $this->values['recurring_billing_data'];
	}

	/**
	 * Set product recurring billing data
	 *
	 * @param RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData
	 */
	public function setProductRecurringBillingData(RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData = null)
	{
		$this->values['product_recurring_billing_data'] = $productRecurringBillingData;
	}

	/**
	 * Return product billing data
	 *
	 * @return RecurringBilling_Model_ProductRecurringBillingData|null
	 */
	public function getProductRecurringBillingData()
	{
		return $this->values['product_recurring_billing_data'];
	}

	/**
	 * Set product
	 *
	 * @param Model_Product $product
	 */
	public function setDataFromProduct(Model_Product $product)
	{
		$this->setFreeShipping($product->getFreeShipping());
		$this->setPriceWithTax($product->getPriceWithTax());
		$this->setTaxRate($product->getTaxRate());
		$this->parseValues();
		$this->setPriceBeforeQuantityDiscount($product->getFinalPrice());

		$recurringBillingData = null;

		if ($product->getEnableRecurringBilling())
		{
			/** @var RecurringBilling_Model_ProductRecurringBillingData $productRecurringBillingData */
			$productRecurringBillingData = $product->getRecurringBillingData();

			if (!is_null($productRecurringBillingData))
			{
				/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
				$recurringBillingData = new RecurringBilling_Model_LineItemRecurringBillingData();
				$recurringBillingData->hydrateFromProductRecurringData($productRecurringBillingData);
			}
		}

		$this->setRecurringBillingData($recurringBillingData);
	}

	/**
	 * Create a new gift line item (product promotions)
	 *
	 * @param $orderId
	 * @param $product
	 * @param $quantity
	 *
	 * @return Model_LineItem
	 */
	public static function createNewGiftLineItem($orderId, Model_Product $product, $quantity)
	{
		$attributesData = array(
			'weight' => $product->getWeight(),
			'options' => '',
			'options_clean' => '',
			'attributes' => array(), // Value not needed on update
			'inventoryList' => '',
		);

		return self::createNewLineItem($orderId, $product, $quantity, $attributesData, 'Yes');
	}

	/**
	 * Check is line item matches other line item
	 *
	 * @param Model_LineItem $lineItem
	 *
	 * @return bool
	 */
	public function matches(Model_LineItem $lineItem)
	{
		$matches =
			$this->getProductId() == $lineItem->getProductId()
			&& serialize($this->getAttributes()) == serialize($lineItem->getAttributes());

		return $matches;
	}

	/**
	 * Create a new line item
	 *
	 * @param $orderId
	 * @param $product
	 * @param $quantity
	 * @param $attributesData
	 * @param string $isGift
	 * @return Model_LineItem
	 */
	public static function createNewLineItem($orderId, Model_Product $product, $quantity, &$attributesData, $isGift = 'No')
	{
		$values = array(
			'ocid' => 0,
			'oid' => $orderId,
			'pid' => $product->getPid(),
			'shipment_id' => 0,
			'quantity' => $quantity,
			'admin_quantity' => $quantity,
			'price_before_quantity_discount' => $product->getPrice(),
			'price' => $product->getPrice(),
			'admin_price' => $product->getFinalPrice(),
			'final_price' => $product->getFinalPrice(),
			'attribute_value' => 0,
			'attribute_value_type' => 'amount',
			'is_taxable' => $product->getIsTaxable(),
			'is_doba' => $product->getIsDoba() === true || $product->getIsDoba() == 'Yes' ? 'Yes' : 'No',
			'is_stock_changed' => 'No',
			'weight' => $product->getWeight(),
			'free_shipping' => $product->getFreeShipping(),
			'product_free_shipping' => $product->getFreeShipping(),
			'product_level_shipping' => $product->getProductLevelShipping(),
			'same_day_delivery' => $product->getSameDayDelivery(),
			'is_shipping_price' => 'No',
			'shipping_price' => 0,
			'tax_rate' => 0,
			'tax_amount' => 0,
			'tax_description' => '',
			'tax_exempt' => 0,
			'product_type' => $product->getProductType(),
			'digital_product_file' => $product->getDigitalProductFile(),
			'digital_product_key' => $product->getProductType() == Model_Product::DIGITAL ? md5(uniqid(rand(),1)) : '',
			'digital_product_downloads' => 0,
			'is_gift' => $isGift,
			'gift_parent' => 0,
			'product_removed' => 'No',
			'inventory_id' => 0,
			'products_location_id' => $product->getProductsLocationId(),
			'options' => '',
			'options_clean' => '',
			'product_id' => $product->getProductId(),
			'product_sub_id' => '',
			'product_sku' => $product->getProductSku(),
			'product_upc' => $product->getProductUpc(),
			'product_gtin' => $product->getProductGtin(),
			'product_mpn' => $product->getProductMpn(),
			'title' => $product->getTitle(),
			'discount_amount' => 0,
			'discount_amount_with_tax' => 0,
			'promo_discount_amount' => 0,
			'promo_discount_amount_with_tax' => 0,
			'product_price' => $product->getPrice(),
			'price_withtax' => $product->getPrice(),
			'product_url' => '',
			'cat_url' => '',
			'inventory_control' => $product->getInventoryControl(),
			'cid' => $product->getCid(),
			'quantity_from_stock' => 0,
			'fulfillment_status' => 'pending',
			'fulfilled_quantity' => 0,
			'stock' => $product->getStock(),
			'max_order' => $product->getMaxOrder(),
			'min_order' => $product->getMinOrder(),
		);

		$lineItem = new self($values);

		$lineItem->setDataFromProduct($product);
		$lineItem->setAttributesData($attributesData);

		return $lineItem;
	}

	// Disable cloning for phpunit testing
	private function __clone() {}
}
<?php

class Model_Fulfillment
{
	const STATUS_PENDING = 'pending';
	const STATUS_COMPLETED = 'completed';

	protected $id;
	protected $orderId;
	protected $status = Model_Fulfillment::STATUS_PENDING;
	protected $trackingCompany;
	protected $trackingNumber;
	protected $createdAt;
	protected $updatedAt;
	protected $items = array();

	public function __construct($data = null)
	{
		if ($data !== null)
		{
			if (is_array($data))
			{
				$this->id = isset($data['id']) ? intval($data['id']) : 0;
				$this->orderId = isset($data['oid']) ? intval($data['oid']) : 0;
				$this->status = $data['status'];
				$this->trackingCompany = $data['tracking_company'];
				$this->trackingNumber = $data['tracking_number'];
				$this->createdAt = isset($data['created_at']) ? new DateTime($data['created_at']) : new DateTime();
				$this->updatedAt = isset($data['updated_at']) ? new DateTime($data['updated_at']) : new DateTime();
			}
		}
		else
		{
			$this->createdAt = new DateTime();
			$this->updatedAt = new DateTime();
		}
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $createdAt
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;
	}

	/**
	 * @return mixed
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * @param mixed $orderId
	 */
	public function setOrderId($orderId)
	{
		$this->orderId = $orderId;
	}

	/**
	 * @return mixed
	 */
	public function getOrderId()
	{
		return $this->orderId;
	}

	/**
	 * @param mixed $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param mixed $trackingCompany
	 */
	public function setTrackingCompany($trackingCompany)
	{
		$this->trackingCompany = $trackingCompany;
	}

	/**
	 * @return mixed
	 */
	public function getTrackingCompany()
	{
		return $this->trackingCompany;
	}

	/**
	 * @param mixed $trackingNumber
	 */
	public function setTrackingNumber($trackingNumber)
	{
		$this->trackingNumber = $trackingNumber;
	}

	/**
	 * @return mixed
	 */
	public function getTrackingNumber()
	{
		return $this->trackingNumber;
	}

	/**
	 * @param mixed $updatedAt
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;
	}

	/**
	 * @return mixed
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}

	/**
	 * @param array $items
	 */
	public function setItems($items)
	{
		$this->items = $items;
	}

	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}

	public function addItem(Model_FulfillmentItem $item)
	{
		$this->items[$item->getLineItemId()] = $item;
	}

	/**
	 * @param ORDER $order
	 * @param bool $includeTangibleProducts
	 * @return bool|Model_Fulfillment
	 */
	public static function createFulfillmentFromOrder(ORDER $order, $includeTangibleProducts = true)
	{
		$items = array();

		/** @var Model_LineItem $lineItem */
		foreach ($order->lineItems as $lineItem)
		{
			$lineItem->setOrderId($order->getId());

			$productType = $lineItem->getProductType();
			if ($lineItem->getProductId() == 'gift_certificate') $productType = Model_Product::VIRTUAL;

			if (!$includeTangibleProducts &&  $productType == Model_Product::TANGIBLE) continue;

			$fulfillmentItem = new Model_FulfillmentItem($lineItem);

			if ($fulfillmentItem->getQuantity() > 0) $items[$lineItem->getId()] = $fulfillmentItem;
		}

		if (count($items) > 0)
		{
			$fulfillment = new Model_Fulfillment();
			$fulfillment->orderId = $order->getId();
			$fulfillment->items = $items;

			return $fulfillment;
		}

		return false;
	}
}
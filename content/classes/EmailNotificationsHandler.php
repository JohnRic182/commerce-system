<?php

class EmailNotificationsHandler
{
	protected static $instance;

	protected function emailProductsLocationNotifyWrapper($db, $oid)
	{
		emailProductsLocationsNotify($db, $oid);
	}

	public static function getInstance()
	{
		if (!self::$instance)
			self::$instance = new EmailNotificationsHandler();

		return self::$instance;
	}

	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	
	public static function emailProductsLocationsNotify($settings, $db, $oid, $orderStatusSetToCompleted = false, $paymentStatusSetToReceived = false, $paymentStatusIsReceived = false)
	{
		if ($settings["ProductsLocationsSendOption"] == "user" && $paymentStatusSetToReceived)
		{
			// Notify when order is being completed and payment status set to received in one step
			self::getInstance()->emailProductsLocationNotifyWrapper($db, $oid);
		}
		else if ($orderStatusSetToCompleted)
		{
			if ($settings["ProductsLocationsSendOption"] == "admin" && $paymentStatusIsReceived)
			{
				// Notify when order is being completed and payment has already been received
				self::getInstance()->emailProductsLocationNotifyWrapper($db, $oid);
			}
		}
	}
}
<?php

class Wishlist
{
		// Wishlist properties
		var $uid = 0, $pid = 0;
		var $db;
		var $messages, $errors;
		var $wishlistCount = 0;
		var $wishlistList = array();
		var $wishlistAttributes = array();
		
		
		// set wishlist error message
		function setError($str){
			$this->errors[count($this->errors)] = $str;
		}
		
		public function __construct($db, $uid)
		{
			global $_SESSION;
			$this->uid = $uid;
			$this->db = $db;
			$this->errors = array();
			$this->messages = array();
			if($this->uid != "" && $this->uid > 0){
				$this->db->query("SELECT * FROM ".DB_PREFIX."users_wishlist WHERE uid='".$this->uid."'");
				$this->wishlistList = $this->db->getRecords();
				$this->wishlistCount = $this->db->numRows();
				
			}
		}
		/*
		function getWishlistProductList($wlid){
			$this->db->query("SELECT * FROM ".DB_PREFIX."users_wishlist_products WHERE wlid='".$wlid."'");
			$wish_list_products = $this->db->getRecords();
			return($wish_list_products);
		}*/
		function getWishlistProduct($wlpid){
			$wl_product = array();
			$this->db->query("SELECT *, ".DB_PREFIX."products.product_id FROM ".DB_PREFIX."users_wishlist_products 
							 INNER JOIN ".DB_PREFIX."products ON ".DB_PREFIX."products.pid = ".DB_PREFIX."users_wishlist_products.pid
							  WHERE wlpid='".$wlpid."' ");
			if($this->db->moveNext()){
				$wl_product = $this->db->col;
				$product_attributes =array();
				if(strlen(trim($wl_product["options"]))>0)
				{
					$attribute_options= explode("\n", $wl_product["options"]);	
					$attribute_id= explode(",", $wl_product["attribute_id"]);
					foreach ($attribute_id as $key=>$val)
					{
						$att_opts = explode(":", $attribute_options[$key]);
						$product_attributes[$val] =trim($att_opts[1]);
					}
				}
				$wl_product["product_attributes"] = $product_attributes;
				return($wl_product);
			}
			return false;
		}
		
		function getWishlistData($wlid){
			global $user, $userCookie;
			$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
			$list = false;
			$this->db->query("SELECT wlid, name FROM ".DB_PREFIX."users_wishlist WHERE wlid='".$wlid."' AND uid='".$this->uid."'");
			if($this->db->moveNext()){
				$wl_data = $this->db->col;
				$this->db->query("SELECT * FROM ".DB_PREFIX."users_wishlist_products WHERE wlid='".$wlid."'");
				$wish_list_products = $this->db->getRecords();
				$wl_products = array();
				$products = new ShoppingCartProducts($this->db, $settings);
				if(count($wish_list_products)>0){ 											
					foreach($wish_list_products as $wl_product){
						$this->db->query("SELECT pid, product_id, cid, title, price, image_location, image_url 
									FROM ".DB_PREFIX."products 
									WHERE pid ='".$wl_product["pid"]."'");
						if($this->db->moveNext()){
							$product = $this->db->col;
							$product["attributes"] ="";
							if(strlen(trim($wl_product["options"]))> 0){
								$attribute_options= explode("\n", $wl_product["options"]);
								$selected_att = array();
								$i = 0;
								foreach($attribute_options as $att=>$val){
									$product["attributes"] .= (strlen($product["attributes"])>0?"<br />":"").$val; 
									$att_val = explode(":", $val);
									$selected_att[$att] = $att_val[1];
									$prod = $products->getProductById($product["pid"], $userLevel,0);
									$sel_att = trim($selected_att[$att]);
									$attribute_id = explode(",", $wl_product["attribute_id"]);
									$attribute_id = $attribute_id[$i];
									$product["price"] = $prod["attributes"][$attribute_id]["options"][$sel_att]["price"];
									$i++;
								}
							}
							$product["wlpid"]= $wl_product["wlpid"];	
							$p_details = $products->getProductById($product["pid"], $userLevel);								
							$product["url"] = $p_details["url"];
							$product["thumb"] = $p_details["thumb"];									
							//$product["inventory_hashes"] = $p_details["inventory_hashes"];

							if (!isset($product['out_of_stock'])) $product["out_of_stock"] = "";

							//----------------inventory check
							if($p_details["inventory_control"] == "AttrRuleExc" || $p_details["inventory_control"] == "AttrRuleInc" ){
								if($p_details["inventory_hashes"] !=""){
									$product["inventory_hashes"] = $p_details["inventory_hashes"];
									foreach($p_details["attributes"] as $p_attr){
										$product_attributes_track_inventory[$p_attr["paid"]] = $p_attr["track_inventory"];										
									}
									$attribute_ids = explode(",", $wl_product["attribute_id"]);
									$attributes_list = "";
									foreach($attribute_ids as $k=>$attr_id){
										if($product_attributes_track_inventory[$attr_id] ==1){
											$attributes_list = $attributes_list.($attributes_list == "" ? "" : "\n").trim($attr_id).": ".trim($selected_att[$k]);
										}
									}
									$current_hash = md5($attributes_list);
									$inventory_hashes = array();
									$inventory_hashes_array = array();
									foreach($p_details["inventory_hashes"] as $inventory_hash){
										$inventory_hashes[$inventory_hash["attributes_hash"]] = $inventory_hash["stock"];
										$inventory_warnings[$inventory_hash["attributes_hash"]] = $inventory_hash["stock_warning"];
										$inventory_hashes_array[]=$inventory_hash["attributes_hash"];
									}
										
									$product["attributes_list"] = $attributes_list;
									$product["current_hash"] = $current_hash;
									$product["out_of_stock"] = "Yes";
									$hash_exist = false;
									if(count($inventory_hashes_array) > 0){ 
										if(in_array($current_hash,$inventory_hashes_array)){
											$hash_exist = true;
											if(($inventory_hashes[$current_hash] > $inventory_warnings[$current_hash]) && ($inventory_hashes[$current_hash] > 0)){
												$product["out_of_stock"] = "No";											
											}
											else{
												$product["out_of_stock"] = "Yes";												
											}
										}
									}
									if(!$hash_exist){
										if($p_details["inventory_control"] == "AttrRuleExc"){
											$product["out_of_stock"] = "Yes";
										}
										if($p_details["inventory_control"] == "AttrRuleInc"){
											$product["out_of_stock"] = "No";											
										}											
									}
								}	
							}
							elseif($p_details["inventory_control"] == "Yes"){
								if($p_details["out_of_stock"] >0)
									$product["out_of_stock"] = "Yes";
								else	
									$product["out_of_stock"] = "No";
							}
							//-----------------------------											
							$wl_products[]= $product;
						}
					}
				}
				$list= array(						
					"wlid" => $wl_data["wlid"],
					"name" => $wl_data["name"],
					"wishlist_products" => $wl_products,
				);
					
			}
			
			return ($list);
		}

		function addNewWishlist($wishlist_name)
		{
			$this->db->reset();
			$this->db->assign("uid", $this->uid);
			$this->db->assignStr("name", $wishlist_name);				
			$this->db->assign("create_date", "NOW()");
			$this->db->assign("update_date", "NOW()");				
			$wl_id = $this->db->insert(DB_PREFIX."users_wishlist");
			return $wl_id;				
		}
		
		function updateWishlist($wlid, $wishlist_name ="")
		{
			global $msg;
			if ($wlid>0)
			{
				$table_fields = "";
				if ($wishlist_name !="")
				{
					$table_fields = "name ='".$wishlist_name."',";
				}
				$this->db->query("
					UPDATE ".DB_PREFIX."users_wishlist
					SET
					$table_fields
					update_date = NOW()
					WHERE uid='".$this->uid."' AND wlid='".$wlid."'");
				$this->messages = $msg["wishlist"]["nameUpdates"];
			}
			else
			{
				$this->messages= "Errors";
			}
		}

		function deleteWishlist($wlid)
		{
			global $msg;
			if($wlid > 0)
			{
				$this->db->query("SELECT name FROM ".DB_PREFIX."users_wishlist WHERE wlid='".$wlid."' AND uid='".$this->uid."'");
				if($this->db->moveNext())
				{
					$this->messages = $this->db->col["name"]."</b> ".$msg["wishlist"]["wishlistDeleted"];
					$this->db->query("DELETE FROM ".DB_PREFIX."users_wishlist WHERE  wlid='".$wlid."'");
					$this->db->query("DELETE FROM ".DB_PREFIX."users_wishlist_products WHERE  wlid='".$wlid."'");
				}
				else
				{
					$this->messages= "Errors";
				}
			}
			else
			{
				$this->messages= "Errors";
			}
		}

		function updateWishlistProducts($wlid, $pid){
			global $msg, $_SESSION;
			$wl_att = (isset($_SESSION["wl_att"])?$_SESSION["wl_att"]:array());			
			if($pid != 0){
				$this->db->reset();					
				$this->db->assign("wlid", $wlid);
				//$db->assignStr("product_id", $product_id);
				$this->db->assign("pid", $pid);	
				if(count($wl_att)>0){
					$attribute_id = "";
					$options = "";	
					foreach($wl_att as $att_id=>$val){
						$attribute_id .= (strlen($attribute_id)>0?",":"").$att_id;
						$options .= (strlen($options)>0?"\n":"").$val;
					}
					$this->db->assignStr("options", $options);
					$this->db->assignStr("attribute_id", $attribute_id);						
				}
				$this->db->insert(DB_PREFIX."users_wishlist_products");

				unset($_SESSION['wl_att']);
			}

		}

		function deleteWishlistProduct($wlpid)
		{
			global $msg;
			$this->db->query("SELECT * FROM ".DB_PREFIX."users_wishlist_products WHERE wlpid='".$wlpid."'");
			if($this->db->moveNext())
			{
				$list = $this->getWishlistData($this->db->col['wlid']);
				if ($list)
				{
					$this->db->query("DELETE FROM ".DB_PREFIX."users_wishlist_products WHERE  wlpid ='".$wlpid ."'");
					$this->messages = $msg["wishlist"]["wishlist_updated"];
				}
				else
				{
					$this->messages= "Errors";
				}
			}
			else
			{
				$this->messages= "Errors";
			}
		}
}

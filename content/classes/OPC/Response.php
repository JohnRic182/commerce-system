<?php

class OPC_Response
{
	protected $errors = array();
	protected $data = array();

	/**
	 * Saves error into array
	 *
	 * @param int, array, or string $msg
	 */
	public function setError($msg)
	{
		$a = is_array($msg) ? $msg : array($msg);
		foreach ($a as $msg)
		{
			switch ($msg)
			{
				case OPC_MSG_ERROR_DATA_MISSED : $this->errors[] = ''; break;
				default : $this->errors[] = $msg;
			}
		}
	}

	/**
	 * Return errors
	 *
	 * @returns bool | array
	 */
	public function errors()
	{
		return count($this->errors) > 0 ? $this->errors : false;
	}

	/**
	 * Sets data value to be send in response
	 *
	 * @param string $varName
	 * @param mixed $varValue
	 */
	public function setData($varName, $varValue)
	{
		$this->data[$varName] = $varValue;
	}

	public function getData($varName)
	{
		return isset($this->data[$varName])?$this->data[$varName]:null;
	}

	/**
	 * Build OPC response
	 *
	 * @return string
	 */
	public function getResponse()
	{
		/**
		 * Exactor plug-in modification:
		 * We integrated into this file to handle actions from one checkout page
		 * (regular requests and AJAX requests).
		 * Check if we have Exactor plug-in error in session and if we have
		 * return
		 */
		$request_action = null;

		if (!empty($_REQUEST['action']))
		{
			$request_action = $_REQUEST['action'];
		}
		// use checks only if request action is processPayment. Otherwise
		// (when we return error on saveBillingData or getPaymentForm action,
		// some part of form will not be displayed and checkout page will not work properly)
		if ($request_action == null || $request_action != null && in_array($request_action, array('saveBillingData', 'getPaymentForm', 'saveShippingAddress')))
		{
			if (isset($_SESSION['Exactor_error_message']))
			{
				$exactor_error_message = $_SESSION['Exactor_error_message'];
				$this->setError($exactor_error_message);
				unset($_SESSION['Exactor_error_message']);
			}

			if (isset($_SESSION['Avalara_error_message']))
			{
				$error_message = $_SESSION['Avalara_error_message'];
				$this->setError($error_message);
				unset($_SESSION['Avalara_error_message']);
			}
		}
		// end of Exactor plug-in modification

		$a = array(
			'result' => count($this->errors) == 0,
			'errors' => $this->errors,
			'data' => $this->data
		);

		return json_encode($a);
	}

	/**
	 * Unserialize data
	 *
	 * @param string $data
	 *
	 * @return array
	 */
	public function unserialize($data)
	{
		parse_str($data, $arr);
		return $arr;
	}
}
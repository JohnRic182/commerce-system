<?php

/**
* 
*/
class Chart
{
	protected $title;
	protected $data = array();
	protected $type;
	protected $series = array();
	
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function setData(array $data)
	{
		$this->data = $data;
	}
	
	public function getData()
	{
		return $this->data;
	}
	
	public function setType($type)
	{
		$this->type = $type;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function addSeries(ChartSeries $series)
	{
		$this->series[] = $series;
	}
	
	public function getSeries()
	{
		return $this->series;
	}
	
	/**
	 * Returns a JSON encoded string that can be used with the jquery flot plugin
	 *
	 * @return string JSON encoded string
	 */
	public function asFlot()
	{
		$chart_data = array();
		
		$values = array();
		
		foreach ($this->getSeries() as $series)
		{
			foreach ($series->getData() as $data_key => $series_data)
			{
				$values[] = $series_data['value'];
				if ($series->getXAxisMode() === ChartSeries::AXIS_MODE_TIME) { $data_key *= 1000; }
				if ($series->getYAxisMode() === ChartSeries::AXIS_MODE_TIME) { $series_data['value'] *= 1000; }
				
				$chart_data[] = array($data_key, $series_data['value'], $series_data['label']);
			}
		}
		
		return json_encode(array(
			'min' => 0,
			'max' => (float) max($values),
			'yLabelFormat' => $series->getYAxisFormat(),
			'title' => $series->getTitle(),
			'data' => $chart_data
		));
	}
}
<?php

/**
 * Class Session
 */
class Session
{
	protected static $instance = null;
	
	/**
	 * Force singleton
	 */
	protected function __construct() {}
	
	/**
	 * Sets an instance
	 *
	 * @param Session $instance 
	 * @return void
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	
	/**
	 * Returns the singleton instance of the session
	 *
	 * @return Session
	 * @author Sebastian Nievas
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) self::$instance = new self();
		return self::$instance;
	}
	
	/**
	 * Sets the session name with native session_name()
	 *
	 * @param string $name The name of the session
	 * @return void
	 */
	public function setName($name)
	{
		session_name($name);
	}
	
	/**
	 * Starts the session, sends native session_start()
	 *
	 * @return bool Whether the session was successfully started
	 */
	public function start()
	{
		return session_start();
	}
	
	/**
	 * Sets the session cookie params with native session_set_cookie_params()
	 *
	 * @param string $lifetime Lifetime of the session cookie, defined in seconds.
	 * @param string $path Path on the domain where the cookies will work.
	 * @param string $domain Cookie domain
	 * @param string $secure If TRUE cookie will only be sent over secure connections
	 * @param string $httponly This option is only available in PHP 5.2+ and will be ignored otherwise
	 * @return void
	 */
	public function setCookieParams($lifetime, $path = null, $domain = null, $secure = null, $httponly = false)
	{
		if (floatval(phpversion()) >= 5.2)
		{
			session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
		}
		else
		{
			session_set_cookie_params($lifetime, $path, $domain, $secure);
		}
	}
	
	/**
	 * Retrieves the session cookie params with native session_get_cookie_params()
	 *
	 * @return array
	 * @author Sebastian Nievas
	 */
	public function getCookieParams()
	{
		return session_get_cookie_params();
	}
	
	/**
	 * Sets the session ID that should be used when initialized with native session_id()
	 *
	 * @return string The Session ID
	 */
	public function setId($name = null)
	{
		return session_id($name);
	}
	
	/**
	 * Retrieves the current session ID with native session_id()
	 *
	 * @return string The session ID, if one has not been set yet, '' is returned instead
	 */
	public function getId()
	{
		return session_id();
	}
	
	/**
	 * Sets the session save path through ini_set(session.save_path, $path)
	 *
	 * @param string $path The session save path
	 * @return void
	 */
	public function setSavePath($path)
	{
		ini_set("session.save_path", $path);
	}
	
	/**
	 * Sets the session cache limiter to a new value
	 *
	 * @param string $value The cache limiter value
	 * @return string The cache limiter value
	 */
	public function setCacheLimiter($value)
	{
		session_cache_limiter($value);
	}
	
	/**
	 * Sets the session cache expire value in minutes with native session_cache_expire
	 *
	 * @param string $value The value of session cache expire in minutes
	 * @return int The current cache expire in minutes
	 */
	public function setCacheExpire($value)
	{
		return session_cache_expire($value);
	}

	public function ensureCookieUpdated($name, $lifetime = 0, $path = null, $domain = null, $secure = null, $httponly = null)
	{
		if (floatval(phpversion()) >= 5.2)
		{
			setcookie($name, $this->getId(), time() + $lifetime, $path, $domain, $secure, $httponly);
		}
		else
		{
			setcookie($name, $this->getId(), time() + $lifetime, $path, $domain, $secure);
		}
	}

	public function destroy($name, $path = null, $domain = null, $secure = null, $httponly = null)
	{
		if (floatval(phpversion()) >= 5.2)
		{
			setcookie($name, '', time() - 42000, $path, $domain, $secure, $httponly);
		}
		else
		{
			setcookie($name, '', time() - 42000, $path, $domain, $secure);	
		}

		session_destroy();
	}
}

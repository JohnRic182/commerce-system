<?php

/**
* 
*/
class Countries
{
	
	/**
	 * Returns a MYSQL record array of the country by its Full Name
	 *
	 * @param string $name The country full name
	 * @return void
	 * @author Sebastian Nievas
	 */
	public static function getCountryByFullName($name)
	{
		db()->reset();
		db()->query('SELECT * FROM '.DB_PREFIX.'countries WHERE LOWER(name) = "'.db()->escape(strtolower($name)).'"');
		if (db()->moveNext()) return db()->col;
		
		return null;
	}
}

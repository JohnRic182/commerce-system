<?php

/**
 * Class View_OrderLinesViewModel
 */
class View_OrderLinesViewModel extends View_Model
{
	/** @var array $lineItems */
	protected $lineItems = array();

	/**
	 * Prepare data for order line items
	 *
	 * @param ORDER $order
	 * @param boolean $opc
	 * @param boolean $admin
	 */
	public function build(ORDER $order, $opc = true, $admin = false)
	{
		$msg = $this->msg;

		$this->resetLineItems();

		/**
		 * Subtotal
		 */
		if ($this->settings['DisplayPricesWithTax'] == 'YES')
		{
			$this->addLineItem('subtotal', $msg['opc']['invoice_subtotal'], '', $order->getSubtotalAmountWithTax(), $admin);
		}
		else
		{
			$this->addLineItem('subtotal', $msg['opc']['invoice_subtotal'], '', $order->getSubtotalAmount(), $admin);
		}

		/**
		 * Discount
		 */
		if ($order->getDiscountAmount() > 0)
		{
			$this->addLineItem('discount', $msg['opc']['invoice_discount'], '', 0 - ($this->settings['DisplayPricesWithTax'] == 'YES' ? $order->getDiscountAmountWithTax() : $order->getDiscountAmount()), $admin);
		}

		/**
		 * Promo discount
		 */
		if ($order->getPromoDiscountAmount() > 0 && $order->getPromoType() != 'Shipping')
		{
			$this->addLineItem(
				'promo',
				$msg['opc']['invoice_promo_discount'].($opc ? '' : ($order->getPromoDiscountType() == 'percent' ? ' '.round($order->promoDiscountValue, 2).'%' : '')),
				'',
				0 - ($this->settings['DisplayPricesWithTax'] == 'YES' ? $order->getPromoDiscountAmountWithTax() : $order->getPromoDiscountAmount()),
				$admin
			);
		}

		$shippingAmount = $order->getShippingAmount();

		if ($this->settings['DisplayPricesWithTax'] == 'YES')
		{
			$shippingAmount += $order->getShippingTaxAmount();
		}

		/**
		 * Shipping amount
		 */
		if ($opc)
		{
			if ($shippingAmount > 0)
			{
				$this->addLineItem('shipping', $msg['opc']['invoice_shipping'], '', $shippingAmount, $admin);
			}
		}
		else if ($this->settings['ShippingCalcEnabled'] == 'YES')
		{
			$this->addLineItem('shipping', $msg['opc']['invoice_shipping'], '', $shippingAmount, $admin);

			// TODO: add orders shipments here
		}

		/**
		 * Shipping promo discount
		 */
		if ($order->getPromoDiscountAmount() > 0 && $order->getPromoType() == 'Shipping')
		{
			$this->addLineItem('promo-shipping', $msg['opc']['invoice_promo_discount'], '',
				0 - ($this->settings['DisplayPricesWithTax'] == 'YES' ? $order->getPromoDiscountAmountWithTax() : $order->getPromoDiscountAmount()),
				$admin);
		}

		/**
		 * Handling
		 */
		if ($order->getHandlingSeparated() && $order->getHandlingAmount() > 0)
		{
			$handlingAmount = $order->getHandlingAmount();
			if ($this->settings['DisplayPricesWithTax'] == 'YES')
			{
				$handlingAmount += $order->getHandlingTaxAmount();
			}
			$this->addLineItem('handling', $msg['opc']['invoice_handling'], '', $handlingAmount, $admin);
		}

		/**
		 * Taxes
		 */
		$taxLine = $this->settings['DisplayPricesWithTax'] == 'YES' ? 'tax-included' : 'tax';
		$taxText = $this->settings['DisplayPricesWithTax'] == 'YES' ? $msg['opc']['invoice_tax_included_in_subtotal'] : $msg['opc']['invoice_tax'];

		if ($order->getTaxExempt() != ORDER::TAX_EXEMPT_NO)
		{
			$this->addLineItem(
				$taxLine, $taxText,
				$order->getTaxExempt() == ORDER::TAX_EXEMPT_YES ? $msg['opc']['invoice_tax_exempt'] : $msg['opc']['invoice_tax_exempt_partial'],
				$order->getTaxAmount(),
				$admin
			);
		}
		else if ($order->getTaxAmount() > 0)
		{
			$this->addLineItem($taxLine, $taxText, '', $order->getTaxAmount(), $admin);
		}

		/**
		 * Total
		 */
		$totalAmount = $order->getTotalAmount();

		$this->addLineItem('total', $msg['opc']['invoice_total'], '', $totalAmount, $admin);

		/**
		 * Gift certificate remaining amount to be paid
		 */
		if ($order->getGiftCertificateAmount() > 0)
		{
			$this->addLineItem('gift-certificate', $msg['opc']['invoice_gift_certificate'], '', $order->getGiftCertificateAmount(), $admin);

			if ($order->getGiftCertificateAmount() < $order->getTotalAmount())
			{
				$this->addLineItem('remaining', $msg['opc']['invoice_remaining_amount'], '', $order->getTotalAmount() - $order->getGiftCertificateAmount(), $admin);
			}
		}

		/**
		 * Recurring trial and billing amount
		 */

		if ($opc && $this->settings['RecurringBillingEnabled'] == '1' && $order->getOrderType() != ORDER::ORDER_TYPE_RECURRING && $order->hasRecurringBillingItems() && !$admin)
		{
			$this->addLineItem('recurring', $msg['opc']['invoice_recurring_payments'], '', null, $admin);

			$itemsText = '';

			$orderItems = $order->getOrderItemsExtended($msg);

			foreach ($orderItems as $orderItem)
			{
				if ($orderItem['enable_recurring_billing'] && !is_null($orderItem['recurring_billing_data']))
				{
					/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
					$recurringBillingData = $orderItem['recurring_billing_data'];

					$itemText = '<div class="recurring-product">'.$orderItem['title'].'</u><br>';

					if ($recurringBillingData->getStartDate() != null)
					{
						$itemText .=
							'<div class="recurring-payment clearfix">'.
								'<div class="recurring-payment-title">'.$msg['recurring']['billing_starting_at'].'</div>'.
								'<div class="recurring-payment-description">'.strftime($msg['common']['date_format'], $recurringBillingData->getStartDate()->getTimestamp()).'</div>'.
							'</div>';
					}

					if ($recurringBillingData->getTrialEnabled())
					{
						$itemText .=
							'<div class="recurring-payment clearfix">'.
								'<div class="recurring-payment-title">'.$msg['recurring']['trial_period'].'</div>'.
								'<div class="recurring-payment-description">'.$orderItem['recurring_trial_description'].'</div>'.
							'</div>';
					}

					$itemText .=
						'<div class="recurring-payment clearfix">'.
							'<div class="recurring-payment-title">'.$msg['recurring']['billing_period'].'</div>'.
							'<div class="recurring-payment-description">'.$orderItem['recurring_billing_description'].'</div>'.
						'</div>';

					$itemText .= '</div>';

					$itemsText .= $itemText;
				}
			}

			$this->addLineItem('recurring-items', $itemsText, '', null, $admin);
		}


	}

	/**
	 * Return data as array
	 *
	 * @return array
	 */
	public function asArray()
	{
		$lineItems = $this->getLineItems();

		// put result in correct order
		$orderViewList = array(
			'subtotal', 'discount', 'promo', 'shipping', 'handling', 'promo-shipping', 'tax', 'tax-exempt',
			'total', 'tax-included', 'gift-certificate', 'remaining', 'recurring', 'recurring-items'
		);

		$result = array();
		foreach ($orderViewList as $lineItemKey)
		{
			if (isset($lineItems[$lineItemKey]))
			{
				$result[$lineItemKey] = $lineItems[$lineItemKey];
			}
		}

		return array('lineItems' => $result);
	}

	/**
	 * Return order line items
	 *
	 * @return array
	 */
	public function getLineItems()
	{
		return $this->lineItems;
	}

	/**
	 * Return line item by key
	 *
	 * @param string $key
	 *
	 * @return mixed
	 */
	public function getLineItem($key)
	{
		return isset($this->lineItems[$key]) ? $this->lineItems[$key] : null;
	}

	/**
	 * Reset order line items
	 */
	protected function resetLineItems()
	{
		$this->lineItems = array();
	}

	/**
	 * Add order amount line item
	 *
	 * @param string $key
	 * @param string $title
	 * @param string $subtitle
	 * @param float $amount
	 * @param bool $admin
	 */
	public function addLineItem($key, $title, $subtitle, $amount, $admin = false)
	{
		$this->lineItems[$key] = array(
			'title' => $title,
			'subtitle' => $subtitle,
			'amount' => !is_null($amount) ? $this->formatPrice($amount, $admin) : ''
		);
	}
}
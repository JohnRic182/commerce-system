<?php

/**
 * Class View_OrderShipmentsViewModel
 */
class View_OrderShipmentsViewModel extends View_Model
{
	/**
	 * @param array $shipments
	 *
	 * @return array
	 */
	public function getShipmentsView(array $shipments)
	{
		$result = array();
		$n = 1;

		ksort($shipments);

		foreach ($shipments as $shipmentId => $shipment)
		{
			$shipment['num'] = $n;
			$result[$shipmentId] = $shipment;

			$n++;
		}

		return $result;
	}
}

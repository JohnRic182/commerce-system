<?php
/**
 * Class View_Model
 */
class View_Model
{
	protected $settings;
	protected $msg;

	/**
	 * Class constructor
	 *
	 * @param array $settings
	 * @param array $msg
	 */
	public function __construct(&$settings, &$msg)
	{
		$this->settings = $settings;
		$this->msg = $msg;
	}

	/**
	 * Format price (currencies, decimal places, etc)
	 *
	 * @param $amount
	 * @param bool $admin
	 *
	 * @return string
	 */
	protected function formatPrice($amount, $admin = false)
	{
		return $admin ? getAdminPrice($amount) : getPrice($amount);
	}
}
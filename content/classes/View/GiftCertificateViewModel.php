<?php

class View_GiftCertificateViewModel extends View_Model
{
	protected $giftCertificateUsed = false;
	protected $orderHasRemainingAmount = false;
	protected $paymentFormText = "";

	/**
	 * Build cart
	 * @param  ORDER  $order [description]
	 * @return [type]		[description]
	 */
	public function build(ORDER $order)
	{
		$this->setGiftCertificateUsed($order->getGiftCertificateAmount() > 0);
		$this->setOrderHasRemainingAmount($order->getRemainingAmountToBePaid() > 0);

		if ($this->getGiftCertificateUsed())
		{
			$paymentFormText = 
				'Total  amount on your gift certificate is '.$this->formatPrice($order->getGiftCertificateAmount()).'.<br/>'.
				$this->formatPrice($order->getGiftCertificateAmountUsedBeforePayment()).' will be used for current order. ';

			if ($order->getRemainingAmountToBePaid()  > 0)
			{
				$paymentFormText .= 
					'You still have to pay '.$this->formatPrice($order->getRemainingAmountToBePaid()).
					'. Please select payment method and complete order.';
			}
			else
			{
				$paymentFormText .= 
					'No additional payment required for this order. Please click "Place Order" button to complete.';
			}

			$this->setPaymentFormText($paymentFormText);
		}
		else
		{
			$this->setPaymentFormText("");
		}
	}

	/**
	 * Returns view as array
	 * @return array
	 */
	public function asArray()
	{
		return array(
			"giftCertificateUsed" => $this->getGiftCertificateUsed(),
			"orderHasRemainingAmount" => $this->getOrderHasRemainingAmount(),
			"paymentFormText" => $this->getPaymentFormText()
		);
	}

	/**
	 * Set is gift certificate was used for order
	 * @param bool $giftCertificateUsed
	 */
	public function setGiftCertificateUsed($giftCertificateUsed)
	{
		$this->giftCertificateUsed = $giftCertificateUsed;
	}

	/**
	 * Returs does gift certificate used
	 * 
	 * @return bool
	 */
	public function getGiftCertificateUsed()
	{
		return $this->giftCertificateUsed;
	}

	/**
	 * Setter for order has remaining amount
	 *
	 * @param string $orderHasRemainingAmount value to set
	 */
	public function setOrderHasRemainingAmount($orderHasRemainingAmount)
	{
		$this->orderHasRemainingAmount = $orderHasRemainingAmount;
	}
	
	/**
	 * Getter for order has remaining amount
	 *
	 * @return string
	 */
	public function getOrderHasRemainingAmount()
	{
		return $this->orderHasRemainingAmount;
	}
	

	/**
	 * Setter for payment form text
	 *
	 * @param string $paymentFormText value to set
	 */
	public function setPaymentFormText($paymentFormText)
	{
		$this->paymentFormText = $paymentFormText;
	}
	
	/**
	 * Getter for payment form text
	 *
	 * @return string
	 */
	public function getPaymentFormText()
	{
		return $this->paymentFormText;
	}
}
<?php
/**
 * Class View_AddressViewMode
 */
class View_AddressViewModel
{
	public $addressType;
	public $firstName;
	public $lastName;
	public $name;
	public $company;
	public $addressLine1;
	public $addressLine2;
	public $city;
	public $stateId;
	public $stateName;
	public $stateCode;
	public $province;
	public $countryId;
	public $countryName;
	public $countryIso2;
	public $countryIso3;
	public $countryIsoNumber;
	public $zip;

	protected $customFields;

	/**
	 * Class constructor
	 *
	 * @param Model_Address $address
	 */
	public function __construct(Model_Address $address)
	{
		$this->addressType = $address->getAddressType() == Model_Address::ADDRESS_TYPE_BUSINESS ? 'Business' : 'Residential';
		$this->firstName = $address->getFirstName();
		$this->lastName = $address->getLastName();
		$this->name = $address->getName();
		$this->company = $address->getCompany();
		$this->addressLine1 = $address->getAddressLine1();
		$this->addressLine2 = $address->getAddressLine2();
		$this->city = $address->getCity();
		$this->stateId = $address->getStateId();
		$this->stateName = $address->getStateName();
		$this->stateCode = $address->getStateCode();
		$this->province = $address->getProvince();
		$this->countryId = $address->getCountryId();
		$this->countryName = $address->getCountryName();
		$this->countryIso2 = $address->getCountryIso2();
		$this->countryIso3 = $address->getCountryIso3();
		$this->countryIsoNumber = $address->getCountryIsoNumber();
		$this->zip = $address->getZip();
	}
}
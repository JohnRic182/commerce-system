<?php
/**
 * Class View_OrderLineItemsViewModel
 */
class View_OrderLineItemsViewModel extends View_Model
{
	/**
	 * Get recurring trial description
	 *
	 * @param RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData
	 * @param $itemsQuantity
	 * @param $msg
	 *
	 * @return mixed|string
	 */
	public static function getRecurringTrialDescription(RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData, $itemsQuantity, $msg)
	{
		if ($recurringBillingData->getTrialEnabled())
		{
			$message = $recurringBillingData->getTrialPeriodCycles() > 1 ? 'description_with_billing_period_cycles' : 'description_with_one_billing_period_cycle';

			$params = array(
				'msg' => $msg['recurring'][$message],
				'amount' => ($itemsQuantity > 1 ? $itemsQuantity.' x '  : '') . getPrice($recurringBillingData->getTrialAmount()),
				'freq' => $recurringBillingData->getTrialPeriodFrequency() > 1 ? $recurringBillingData->getTrialPeriodFrequency() : '',
				'unit' => $msg['recurring'][($recurringBillingData->getTrialPeriodFrequency() > 1 ? 'plural' : 'singular').'_'.$recurringBillingData->getTrialPeriodUnit()],
				'cycles' => $recurringBillingData->getTrialPeriodCycles(),
				'times' => $msg['recurring'][($recurringBillingData->getTrialPeriodCycles() > 1 ? 'plural' : 'singular').'_times'],
			);

			$smarty = null;

			return smarty_function_lang($params, $smarty);
		}

		return null;
	}

	/**
	 * Get recurring billing description
	 *
	 * @param RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData
	 * @param $itemsQuantity
	 * @param $msg
	 *
	 * @return mixed
	 */
	public static function getRecurringBillingDescription(RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData, $itemsQuantity, $msg)
	{
		if ($recurringBillingData->getBillingPeriodCycles() > 0)
		{
			if ($recurringBillingData->getBillingPeriodCycles() > 1)
			{
				$message = 'description_with_billing_period_cycles';
			}
			else
			{
				$message = 'description_with_one_billing_period_cycle';
			}
		}
		else
		{
			$message = 'description_without_billing_period_cycles';
		}

		$params = array(
			'msg' => $msg['recurring'][$message],
			'amount' => ($itemsQuantity > 1 ? $itemsQuantity.' x '  : '') . getPrice($recurringBillingData->getBillingAmount()),
			'freq' => $recurringBillingData->getBillingPeriodFrequency(),// > 1 ? $recurringBillingData->getBillingPeriodFrequency() : 'a',
			'unit' => $msg['recurring'][($recurringBillingData->getBillingPeriodFrequency() > 1 ? 'plural' : 'singular').'_'.$recurringBillingData->getBillingPeriodUnit()],
			'cycles' => $recurringBillingData->getBillingPeriodCycles(),
			'times' => $msg['recurring'][($recurringBillingData->getBillingPeriodCycles() > 1 ? 'plural' : 'singular').'_times'],
		);

		$smarty = null;

		return smarty_function_lang($params, $smarty);
	}
}
<?php
/**
 * Class View_ShippingQuotesViewModel
 */
class View_ShippingQuotesViewModel extends View_Model
{
	/**
	 * Get shipping quotes view
	 *
	 * @param ORDER $order
	 * @param Tax_ProviderInterface $taxProvider
	 * @param $rates
	 * @param bool $shippingQuoteCall
	 * @param int $shippingQuoteCountryId
	 * @param int $shippingQuoteStateId
	 *
	 * @return mixed
	 */
	public function getShippingQuotesView(ORDER $order, Tax_ProviderInterface $taxProvider, $rates, $shippingQuoteCall = false, $shippingQuoteCountryId = 0, $shippingQuoteStateId = 0)
	{
		// add handling when needed
		if ($rates && isset($rates['shipments']) && is_array($rates['shipments']) && count($rates['shipments']) > 0)
		{
			foreach ($rates['shipments'] as &$shipment)
			{
				if (!$shipment['handling_separated'] && isset($shipment['shipping_methods']) && is_array($shipment['shipping_methods']))
				{
					foreach ($shipment['shipping_methods'] as &$shippingMethod)
					{
						if ($shippingMethod['carrier_id'] == 'custom' && floatval($shippingMethod['shipping_price']) <= 0)
						{
							$shippingMethod['shipping_price'] = 0;
						}
						else
						{
							$shippingMethod['shipping_price'] += $shipment['handling_amount'];
						}
					}
				}
			}
		}

		if ($rates && $this->showPricesWithTaxes())
		{
			$shippingTaxClassId = (int) $this->settings['ShippingTaxClassId'];

			// reread tax rates for shipping quote region
			if ($shippingQuoteCall)
			{
				$taxProvider->getTaxRates(intval($shippingQuoteCountryId), intval($shippingQuoteStateId), $order->getUserLevel());
			}

			if ($taxProvider->hasTaxRate($shippingTaxClassId))
			{
				$shippingTaxRate = $taxProvider->getTaxRate($shippingTaxClassId, false);

				if (is_array($rates['shipments']))
				{
					foreach ($rates['shipments'] as &$shipment)
					{
						foreach ($shipment['shipping_methods'] as &$shippingMethod)
						{
							$shippingMethod['shipping_price'] =
								$shippingMethod['shipping_price'] + round($shippingMethod['shipping_price'] / 100 * $shippingTaxRate, 2);
						}
					}
				}
			}

			// set taxes back to region based on order data
			if ($shippingQuoteCall)
			{
				$taxProvider->getTaxRates($order->getTaxCountryId(), $order->getTaxStateId(), $order->getUserLevel());
			}
		}

		$priceAtProductLevel = 0;

		if (isset($rates['shipments']) && is_array($rates['shipments']) && count($rates['shipments']) > 0)
		{
			ksort($rates['shipments']);

			$n = 1;
			foreach ($rates['shipments'] as $shipmentId => $_shipment)
			{
				if (isset($_shipment['shipping_methods']) && is_array($_shipment['shipping_methods']))
				{
					$rates['shipments'][$shipmentId]['num'] = $n++;

					foreach ($_shipment['shipping_methods'] as $ssid => $_shippingMethod)
					{
						$rates['shipments'][$shipmentId]['shipping_methods'][$ssid]['shipping_price'] =
							$this->formatPrice($_shippingMethod['shipping_price'] + $priceAtProductLevel);
					}
				}

				if (isset($_shipment['items']) && is_array($_shipment['items']))
				{
					foreach ($_shipment['items'] as $ocid => $item)
					{
						$rates['shipments'][$shipmentId]['items'][$ocid] = array(
							'title' => $item['title'],
							'options_clean' => $item['options_clean'],
							'quantity' => $item['quantity'],
							'free_shipping' => $item['free_shipping'] == 'Yes'
						);
					}
				}
			}
		}

		return $rates;
	}

	/**
	 * Return true is prices must be displayed with taxes
	 *
	 * @return bool
	 */
	protected function showPricesWithTaxes()
	{
		$taxProvider = Tax_ProviderFactory::getTaxProvider();

		return $taxProvider->getDisplayPricesWithTax() && (bool) $this->settings['ShippingTaxable'] && $this->settings['TaxAddress'] == 'Shipping';
	}
}



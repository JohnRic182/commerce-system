<?php
/**
 * Class View_LineItemViewModel
 */
class View_LineItemViewModel
{
	/** @var mixed $id */
	public $id;

	/** @var string $title */
	public $title;

	/** @var string $productId */
	public $productId;

	/** @var int $pid */
	public $pid;

	/** @var string $options */
	public $options;

	/** @var null|\RecurringBilling_View_ProductRecurringBillingDataView $productRecurringBillingData */
	public $productRecurringBillingData = null;

	/**
	 * Class constructor
	 *
	 * @param Model_LineItem $lineItem
	 * TODO: add more fields needed for view
	 */
	public function __construct(Model_LineItem $lineItem)
	{
		$this->id = $lineItem->getId();
		$this->title = $lineItem->getTitle();
		$this->productId = $lineItem->getProductId();
		$this->pid = $lineItem->getPid();
		$this->options = $lineItem->getOptions();

		$this->productRecurringBillingData = new RecurringBilling_View_ProductRecurringBillingDataView($lineItem->getProductRecurringBillingData());
	}
}
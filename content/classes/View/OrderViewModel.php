<?php
/**
 * Class View_OrderViewModel
 */
class View_OrderViewModel extends View_Model
{
	public $orderNumber;
	public $orderType;
	public $orderCreateDate;
	public $statusChangeDate;
	public $oid;
	public $subtotalAmount;
	public $discountAmount;
	public $discountType;
	public $discountValue;
	public $promoDiscountAmount;
	public $promoDiscountType;
	public $promoDiscountValue;
	public $shippingAmount;
	public $handlingSeparated;
	public $handlingAmount;
	public $handlingText;
	public $taxAmount;
	public $taxExempt;
	public $totalAmount;

	public $shippingRequired;
	public $shippingName;
	public $shippingCompany;
	public $shippingAddressType;
	public $shippingAddress1;
	public $shippingAddress2;
	public $shippingCity;
	public $shippingZip;
	public $shippingState;
	public $shippingCountry;

	public $giftMessage;

	public $shippingTrackingNumber;
	public $shippingTrackingNumberType;
	public $shippingTrackingLink;
	public $shippingTracking;

	public $paymentStatus;
	public $status;

	public $lineItems;
	public $bongoData = false;
	public $bongoOrder = false;

	/**
	 * Set view model variables
	 *
	 * @param DB $db
	 * @param ORDER $order
	 */
	public function build(DB $db, ORDER $order)
	{
		$this->orderNumber = $order->getOrderNumber();
		$this->orderType = $order->getOrderType();
		$this->orderCreateDate = $order->getOrderCreatedDate()->format('m/d/Y');
		$this->statusChangeDate = $order->getStatusChangeDate()->format('m/d/Y');
		$this->oid = $order->getId();
		$this->subtotalAmount = $order->getSubtotalAmount();
		$this->discountAmount = $order->getDiscountAmount();
		$this->discountType = $order->getDiscountType();
		$this->discountValue = $order->getDiscountValue();
		$this->promoDiscountAmount = $order->getPromoDiscountAmount();
		$this->promoDiscountType = $order->getPromoDiscountType();
		$this->promoDiscountValue = $order->getPromoDiscountValue();
		$this->shippingAmount = $order->getShippingAmount();
		$this->handlingSeparated = $order->getHandlingSeparated();
		$this->handlingAmount = $order->getHandlingAmount();
		$this->handlingText = $order->getHandlingText();
		$this->taxAmount = $order->getTaxAmount();
		$this->taxExempt = $order->getTaxExempt();
		$this->totalAmount = $order->getTotalAmount();

		$this->shippingRequired = $order->getShippingRequired();

		$shippingAddress = $order->getShippingAddress();

		$this->shippingName = $shippingAddress['name'];
		$this->shippingCompany = $shippingAddress['company'];
		$this->shippingAddressType = $shippingAddress['address_type'];
		$this->shippingAddress1 = $shippingAddress['address1'];
		$this->shippingAddress2 = $shippingAddress['address2'];
		$this->shippingCity = $shippingAddress['city'];
		$this->shippingZip = $shippingAddress['zip'];
		$this->shippingState = $shippingAddress['state_id'] > 0 ? $shippingAddress['state'] : $shippingAddress['province'];
		$this->shippingCountry = $shippingAddress['country'];

		$this->giftMessage = $order->getGiftMessage();

		$this->shippingTrackingNumber = $order->getShippingTrackingNumber();
		$this->shippingTrackingNumberType = $order->getShippingTrackingNumberType();
		$this->shippingTrackingLink = $this->getShippingTrackingLink($this->shippingTrackingNumberType, $this->shippingTrackingNumber, $this->msg);
		$this->shippingTracking = $order->getShippingTracking();

		$this->paymentStatus = $order->getPaymentStatus();
		$this->status = $order->getStatus();

		$this->lineItems = $order->items;

		foreach ($this->lineItems as $key=>$lineItem)
		{
			if (isset($lineItem['enable_recurring_billing']) && $lineItem['enable_recurring_billing'] && isset($lineItem['recurring_billing_data']) && !is_null($lineItem['recurring_billing_data']))
			{
				$settingsRepository = DataAccess_SettingsRepository::getInstance();

				/** @var RecurringBilling_DataAccess_RecurringProfileRepository $recurringBillingRepository */
				$recurringBillingRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $settingsRepository);

				/** @var RecurringBilling_Model_RecurringProfile $recurringProfile */
				$recurringProfile = $recurringBillingRepository->getByLineItemId($lineItem['ocid']);

				/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
				$recurringBillingData = $lineItem['recurring_billing_data'];

				$itemsQuantity = is_null($recurringProfile) ? $lineItem['admin_quantity'] : $recurringProfile->getItemsQuantity();

				$this->lineItems[$key]['recurring_trial_description'] = View_OrderLineItemsViewModel::getRecurringTrialDescription($recurringBillingData, $itemsQuantity, $this->msg);
				$this->lineItems[$key]['recurring_billing_description'] = View_OrderLineItemsViewModel::getRecurringBillingDescription($recurringBillingData, $itemsQuantity, $this->msg);
				$this->lineItems[$key]['recurring_profile'] = $recurringProfile ? new RecurringBilling_View_RecurringProfileView($recurringProfile, $this->msg, false) : null;
			}
		}
	}

	/**
	 * Set is this order is a Bongo order
	 *
	 * @param $bongoOrder
	 */
	public function setBongoOrder($bongoOrder)
	{
		$this->bongoOrder = $bongoOrder;
	}

	/**
	 * Set Bongo data
	 *
	 * @param $bongoData
	 */
	public function setBongoData($bongoData)
	{
		$this->bongoData = $bongoData;
	}

	/**
	 * Get shipping tracking link
	 *
	 * @param $shippingTrackingNumberType
	 * @param $shippingTrackingNumber
	 * @param $msg
	 *
	 * @return string
	 */
	private function getShippingTrackingLink($shippingTrackingNumberType, $shippingTrackingNumber, &$msg)
	{
		if (trim($shippingTrackingNumber) == '' || trim($shippingTrackingNumberType) == '') return '';

		switch (strtolower($shippingTrackingNumberType))
		{
			case 'ups':
				return '<a target="_blank" href="http://wwwapps.ups.com/etracking/tracking.cgi?tracknums_displayed=5&TypeOfInquiryNumber=T&HTMLVersion=4.0&InquiryNumber1='.$shippingTrackingNumber.'&InquiryNumber2=&InquiryNumber3=&track=Track">'.$msg['cart']['click_to_track'].'</a>';
			case 'usps':
				return '<a target="_blank" href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum='.$shippingTrackingNumber.'">'.$msg['cart']['click_to_track'].'</a>';
			case 'fedex':
				return '<a target="_blank" href="http://www.fedex.com/cgi-bin/tracking?action=track&language=english&last_action=alttrack&ascend_header=1&cntry_code=us&initial=x&mps=y&tracknumbers='.$shippingTrackingNumber.'">'.$msg['cart']['click_to_track'].'</a>';
			case 'canadapost':
				return '<a target="_blank" href="http://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?trackingNumber='.$shippingTrackingNumber.'">'.$msg['cart']['click_to_track'].'</a>';
		}
	}
}
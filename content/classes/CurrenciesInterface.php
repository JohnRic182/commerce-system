<?php

interface CurrenciesInterface
{
	public function getDefaultCurrency();

	public function getExchangeRates();
}
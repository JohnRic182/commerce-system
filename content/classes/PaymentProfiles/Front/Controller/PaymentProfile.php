<?php
/**
 * Class PaymentProfiles_Front_Controller_PaymentProfile
 */
class PaymentProfiles_Front_Controller_PaymentProfile extends Front_Controller
{
	/**
	 * List payment profiles
	 */
	public function listAction()
	{
		/** @var Ddm_View $view */
		$view = $this->getView();

		/** @var USER $user */
		$user = $this->getCurrentUser();

		if ($user && $user->isAuthenticated())
		{
			/** @var Framework_Request $request */
			$request = $this->getRequest();

			/** @var Model_FlashMessages $flash */
			$flash = $this->getFlashMessages();

			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $this->get('repository_payment_profile');
			$userAction = $request->get('ua', '');

			/**
			 * Delete payment profile
			 */
			if ($userAction == USER_DELETE_PAYMENT_PROFILE)
			{
				$paymentProfileId = $request->get('payment_profile_id', 0);

				try
				{
					$deletePaymentProfile = $paymentProfileRepository->getById($paymentProfileId, $user->getId());

					if (!is_null($deletePaymentProfile))
					{
						$this->getPaymentProfilesProvider()->deletePaymentProfile($deletePaymentProfile, $user);
						$flash->setMessage('Payment method has been removed');
						$this->_redirect(null, array('p'=>'payment_profiles'), true);
					}
				}
				catch (Framework_Exception $e)
				{
					$flash->setMessage($e->getMessage(), Model_FlashMessages::TYPE_ERROR);

					$errorDetails = $e->getDetails();

					if (count($errorDetails) > 0)
					{
						unset($errorDetails['fname'], $errorDetails['lname'], $errorDetails['country_id'], $errorDetails['state_id']);

						$fieldsErrors = array();

						foreach ($errorDetails as $key => $value) $fieldsErrors['form['.$key.']'] = $value;

						$flash->setMessages($fieldsErrors, Model_FlashMessages::TYPE_ERROR);
					}
				}
			}

			/**
			 * Add a new payment profile
			 */
			$newPaymentProfile = null;

			if ($request->isPost() && $userAction == USER_ADD_PAYMENT_PROFILE)
			{
				$form = $request->request->get('form', array());
				$form = is_array($form) ? $form : array();

				$paymentMethodId = isset($form['payment_method_id']) ? intval($form['payment_method_id']) : 0;
				$payment_method_nonce = $request->request->get('payment_method_nonce', false);

				try
				{
					$newPaymentProfile = new PaymentProfiles_Model_PaymentProfile();
					$newPaymentProfile->setUserId($user->getId());
					$newPaymentProfile->hydrateFromForm($form);
					if ($payment_method_nonce)
					{
						$form['payment_method_nonce'] = $payment_method_nonce;
						$additionalData = $form;
						$newPaymentProfile->setAdditionalParams($additionalData);
					}

					$this->getPaymentProfilesProvider()->addPaymentProfile($newPaymentProfile, $user, $paymentMethodId);
					$flash->setMessage('Payment method has been added');
					$this->_redirect(null, array('p'=>'payment_profiles'), true);
				}
				catch (Framework_Exception $e)
				{
					$flash->setMessage($e->getMessage(), Model_FlashMessages::TYPE_ERROR);

					$errorDetails = $e->getDetails();

					if (count($errorDetails) > 0)
					{
						unset($errorDetails['fname'], $errorDetails['lname'], $errorDetails['country_id'], $errorDetails['state_id']);

						$fieldsErrors = array();

						foreach ($errorDetails as $key => $value) $fieldsErrors['form['.$key.']'] = $value;

						$flash->setMessages($fieldsErrors, Model_FlashMessages::TYPE_ERROR);
					}
				}
			}

			if (is_null($newPaymentProfile))
			{
				$newPaymentProfile = new PaymentProfiles_Model_PaymentProfile();
				$newPaymentProfile->setBillingData(new PaymentProfiles_Model_BillingData());
			}

			/**
			 * Build view
			 */

			$pprovider = $this->getPaymentProvider();
			$additionalData = array();
			if ($pprovider)
			{
				$_paymentMethodId = $paymentProfileRepository->getPaymentProfileEnabledPaymentMethodId();
				$gateway = $pprovider->getPaymentProcessorById($_paymentMethodId);
				if ($gateway)
				{
					$additionalData = $gateway->getTemplateData();
					if ($additionalData)
					{
						$additionalData['profilename'] = $gateway->id;
					}
					$newPaymentProfile->setAdditionalParams($additionalData);
				}
			}
			$this->assignCountriesStates($view);
			$this->assignExpirationValues($view);

			$view->assign('paymentProfiles', PaymentProfiles_View_PaymentProfileView::getPaymentProfilesView($paymentProfileRepository->getByUserId($user->getId())));
			$view->assign('newPaymentProfile', new PaymentProfiles_View_PaymentProfileView($newPaymentProfile));
			$view->assign('paymentMethodId', $paymentProfileRepository->getPaymentProfileEnabledPaymentMethodId());

			$view->assign('flashMessages', $flash->getMessages());
			$view->assign('body', 'templates/pages/account/payment-profiles.html');
		}
		else
		{
			// TODO: need redirect to auth-error page?
			$view->assign('body', 'templates/pages/account/auth-error.html');
		}
	}

	/**
	 * Edit payment profile
	 */
	public function editAction()
	{
		/** @var Ddm_View $view */
		$view = $this->getView();

		/** @var USER $user */
		$user = $this->getCurrentUser();

		if ($user && $user->isAuthenticated())
		{
			/** @var Framework_Request $request */
			$request = $this->getRequest();

			/** @var Model_FlashMessages $flash */
			$flash = $this->getFlashMessages();

			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $this->get('repository_payment_profile');

			$paymentProfileId = $request->get('payment_profile_id', 0);
			$editPaymentProfile = $paymentProfileRepository->getById($paymentProfileId, $user->getId());

			if (!is_null($editPaymentProfile))
			{
				if ($request->isPost() && $request->get('ua', '') == USER_UPDATE_PAYMENT_PROFILE)
				{
					$form = $request->request->get('form', array());
					$form = is_array($form) ? $form : array();
					$payment_method_nonce = $request->request->get('payment_method_nonce', false);

					try
					{
						$editPaymentProfile->hydrateFromForm($form);
						if ($payment_method_nonce)
						{
							$form['payment_method_nonce'] = $payment_method_nonce;
							$additionalData = $form;
							$editPaymentProfile->setAdditionalParams($additionalData);
						}

						$this->getPaymentProfilesProvider()->updatePaymentProfile($editPaymentProfile, $user);
						$flash->setMessage('Payment method has been updated');
						$this->_redirect(null, array('p'=>'payment_profiles'), true);
					}
					catch (Framework_Exception $e)
					{
						$flash->setMessage($e->getMessage(), Model_FlashMessages::TYPE_ERROR);

						$errorDetails = $e->getDetails();

						if (count($errorDetails) > 0)
						{
							unset($errorDetails['fname'], $errorDetails['lname'], $errorDetails['country_id'], $errorDetails['state_id']);

							$fieldsErrors = array();

							foreach ($errorDetails as $key => $value) $fieldsErrors['form['.$key.']'] = $value;

							$flash->setMessages($fieldsErrors, Model_FlashMessages::TYPE_ERROR);
						}
					}
				}

				/**
				 * Build view
				 */
				$this->assignCountriesStates($view);
				$this->assignExpirationValues($view);

				$pprovider = $this->getPaymentProvider();
				$additionalData = array();
				if ($pprovider)
				{
					$_paymentMethodId = $editPaymentProfile->getPaymentMethodId();
					$gateway = $pprovider->getPaymentProcessorById($_paymentMethodId);
					if ($gateway)
					{
						$additionalData = $gateway->getTemplateData();
						if ($additionalData)
						{
							$additionalData['profilename'] = $gateway->id;
						}
						$editPaymentProfile->setAdditionalParams($additionalData);
					}
				}
				$view->assign('editPaymentProfile', new PaymentProfiles_View_PaymentProfileView($editPaymentProfile));
				$view->assign('flashMessages', $flash->getMessages());
				$view->assign('body', 'templates/pages/account/payment-profile-edit.html');
			}
			else
			{
				$flash->setMessage('Cannot find payment profile by id', Model_FlashMessages::TYPE_ERROR);
				$this->_redirect(null, array('p'=>'payment_profiles'), true);
			}
		}
		else
		{
			// TODO: need redirect to auth-error page?
			$view->assign('body', 'templates/pages/account/auth-error.html');
		}
	}

	/**
	 * Get payment profile provider instance
	 *
	 * @return PaymentProfiles_Provider_PaymentProfile
	 */
	public function getPaymentProfilesProvider()
	{
		/** @var PaymentProfiles_Provider_PaymentProfile $provider */
		static $provider = null;

		if (is_null($provider))
		{
			/** @var DB $db */
			$db = $this->get('db');

			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $this->get('repository_payment_profile');

			/** @var DataAccess_SettingsRepository $settings */
			$settings = $this->getSettings();

			$provider = new PaymentProfiles_Provider_PaymentProfile($db, $settings, $paymentProfileRepository);
		}

		return $provider;
	}

	/**
	 * @param Ddm_View $view
	 */
	protected function assignCountriesStates(Ddm_View $view)
	{
		$db = $this->get('db');

		$regions = new Regions($db);
		$view->assign('countries_states', json_encode($regions->getCountriesStatesForSite()));
	}

	/**
	 * @param Ddm_View $view
	 */
	protected function assignExpirationValues(Ddm_View $view)
	{
		$months = array();

		for ($k = 1; $k <= 12; $k++)
		{
			$months[($k < 10 ? '0'.$k : $k)] = $k;
		}

		$view->assign('cc_expiration_months', $months);

		$years = array();

		for ($k = date('Y'); $k <= date('Y') + 10; $k++)
		{
			$years[] = $k;
		}

		$view->assign('cc_expiration_years', $years);
	}

	protected function getPaymentProvider()
	{
		$db = $this->get('db');
		$settings = $this->getSettings();

		return new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, $settings));
	}
}
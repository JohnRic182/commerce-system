<?php

/**
 * Class PaymentProfiles_Admin_Form_PaymentProfileSearchForm
 */
class PaymentProfiles_Admin_Form_PaymentProfileSearchForm
{
	/**
	 * @param $searchParams
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($searchParams)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[profile_id]', 'text', array('label' => 'Profile ID', 'value' => $searchParams['profile_id']));
		$formBuilder->add('searchParams[order_id]', 'text', array('label' => 'Order ID', 'value' => $searchParams['order_id']));
		$formBuilder->add('searchParams[status]', 'choice', array('label' => 'Profile status', 'value' => $searchParams['status'], 'options' => array(
			'any' => 'Any',
			'Active' => 'Active',
			'Suspended' => 'Suspended',
			'Canceled' => 'Canceled',
			'Completed' => 'Completed'
		)));

		$formBuilder->add('searchParams[subscriber_name]', 'text', array('label' => 'Customer\'s last name', 'value' => $searchParams['subscriber_name']));
		$formBuilder->add('searchParams[product_name]', 'text', array('label' => 'Product name', 'value' => $searchParams['product_name']));

		$formBuilder->add('sort_by', 'choice', array('label' => 'Sort By', 'value' => $searchParams['sort_by'], 'options' => array(
			'profile_id' => 'Profile ID',
			'profile_id_desc' => 'Profile ID (descending)',
			'product_name' => 'Product name',
			'product_name_desc' => 'Product name (descending)',
			'next_billing_date' => 'Next billing date',
			'next_billing_date_desc' => 'Next billing date (descending)'
		)));


		return $formBuilder;
	}

	/**
	 * @param $searchParams
	 * @return core_Form_Form
	 */
	public function getForm($searchParams)
	{
		return $this->getFormBuilder($searchParams)->getForm();
	}
}
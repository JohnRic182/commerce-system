<?php
/**
 * Class Admin_Form_AddressForm
 */
class PaymentProfiles_Admin_Form_PaymentProfileForm
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;

	/**
	 * Class constructor
	 *
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
	}

	/**
	 * Get payment profile form builder
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return core_Form_Form
	 */
	public function getBuilder(PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$paymentProfileFormBuilder = new core_Form_FormBuilder('form-payment-profile');

		/**
		 * Billing information
		 */
		$billingInfoGroup = new core_Form_FormBuilder('form-payment-profile-billing', array('label' => 'Billing Information', 'class'=>'ic-invoice', 'wrapperClass' => 'form-address'));

		$paymentProfileFormBuilder->add($billingInfoGroup);

		$billingData = $paymentProfile->getBillingData();
		$billingAddress = $billingData->getAddress();

		// TODO: reuse address form here?
		$arrayName = 'payment_profile';

		$billingInfoGroup->add($arrayName.'[name]', 'text', array('label' => 'Credit card name', 'required'=>false, 'value' => $paymentProfile->getName()));
		$billingInfoGroup->add($arrayName.'[first_name]', 'text', array('label'=>'address.first_name', 'value' => $billingAddress->getFirstName(), 'required' => true));
		$billingInfoGroup->add($arrayName.'[last_name]', 'text', array('label'=>'address.last_name', 'value' => $billingAddress->getLastName(), 'required' => true));
		$billingInfoGroup->add($arrayName.'[address1]', 'text', array('label'=>'address.address1', 'value' => $billingAddress->getAddressLine1(), 'required' => true));
		$billingInfoGroup->add(
			$arrayName.'[address2]',
			'text',
			array(
				'label'=>'address.address2',
				'value' => $billingAddress->getAddressLine2(),
				'required' => $this->settings->get('FormsBillingAddressLine2') == 'Required'
			));
		$billingInfoGroup->add($arrayName.'[city]', 'text', array('label'=>'address.city', 'value' => $billingAddress->getCity(), 'required' => true));

		$billingInfoGroup->add(
			$arrayName.'[country]', 'choice',
			array(
				'label' => 'address.country',
				'value' => $billingAddress->getCountryId(),
				'required' => true,
				'options' => array(),
				'attr' => array('data-value' => $billingAddress->getCountryId(), 'class' => 'input-address-country')
			)
		);

		$billingInfoGroup->add(
			$arrayName.'[state]', 'choice',
			array(
				'label' => 'address.state',
				'value' => $billingAddress->getStateId(),
				'required' => true,
				'options' => array(),
				'attr' => array('data-value' => $billingAddress->getStateId(), 'class' => 'input-address-state')
			)
		);

		$billingInfoGroup->add(
			$arrayName.'[province]', 'text',
			array(
				'label' => 'address.province',
				'value' => $billingAddress->getProvince(),
				'required' => true,
				'attr' => array('class' => 'input-address-province')
			)
		);

		$billingInfoGroup->add($arrayName.'[zip]', 'text', array('label'=>'address.zip', 'value' => $billingAddress->getZip(), 'required' => true, 'attr' => array('class' => 'input-address-zip short')));

		$billingInfoGroup->add(
			$arrayName.'[phone]',
			'text',
			array(
				'label'=>'Phone',
				'value' => $billingData->getPhone(),
				'required' => $this->settings->get('FormsBillingPhone') == 'Required'
			));

		/**
		 * CC information
		 */
		$ccInfoGroup = new core_Form_FormBuilder('form-payment-profile-cc', array('label' => 'Card Information', 'class'=>'ic-invoice', 'wrapperClass' => 'form-address'));

		$paymentProfileFormBuilder->add($ccInfoGroup);

		$ccInfoGroup->add('tokenization', 'template', array('file' => 'templates/pages/payment-profiles/payment-profile-additional-fields.html',
															'value' => array('profile' => $paymentProfile->getAdditionalParams())));

		$ccInfoGroup->add($arrayName.'[cc_number]', 'text', array('label' => 'Card number', 'required'=>true, 'validators'=>'cc', 'attr' => array('autocomplete' => 'off')));
		$ccInfoGroup->add($arrayName.'[cc_expiration]', 'datemonthyear', array('label' => 'Expiration date', 'value' => $paymentProfile->getExpirationDate()));

		//TODO: CVV2 value
		//$ccInfoGroup->add($arrayName.'cc_expiration', 'cc-expiration', array('label' => 'Expiration Date'));

		if ($paymentProfile->getId() < 1 || !$paymentProfile->getIsPrimary())
		{
			$ccInfoGroup->add($arrayName.'[is_primary]', 'checkbox', array('value' => 'Yes', 'label' => 'Is primary', 'current_value' => $paymentProfile->getIsPrimary()));
		}
		else
		{
			$ccInfoGroup->add($arrayName.'[is_primary]', 'hidden', array('value' => 'Yes'));
			$ccInfoGroup->add($arrayName.'[is_primary_label]', 'static', array('label' => '&nbsp;', 'value' => '<strong>This payment profile is primary</strong>'));
		}

		$ccInfoGroup->add('paymentProfileId', 'hidden', array('value' => $paymentProfile->getId()));
		$ccInfoGroup->add('userId', 'hidden', array('value' => $paymentProfile->getUserId()));
		$ccInfoGroup->add('paymentMethodId', 'hidden', array('value' => $paymentProfile->getPaymentMethodId()));

		return $paymentProfileFormBuilder;
	}

	/**
	 * Get payment profile form
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 * @return mixed
	 */
	public function getForm(PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		return $this->getBuilder($paymentProfile)->getForm();
	}
}
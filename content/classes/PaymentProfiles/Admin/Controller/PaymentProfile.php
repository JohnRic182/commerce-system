<?php

/**
 * Class PaymentProfiles_Admin_Controller_PaymentProfile
 */
class PaymentProfiles_Admin_Controller_PaymentProfile extends Admin_Controller
{
	/** @var PaymentProfiles_DataAccess_PaymentProfileRepository  */
	protected $repository = null;

	/** @var Payment_Provider */
	protected $payment_provider = null;

	/**
	 * List action (old velsion)
	 */
	public function userPageTabListAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$userId = $request->query->getInt('uid', 0);

		// TODO: display proper error message here
		if ($userId == 0) return;

		$paymentProfileRepository = $this->getPaymentProfileRepository();

		$adminView = $this->getView();

		$adminView->assign('uid', $userId);
		$adminView->assign('userId', $userId);
		$adminView->assign('paymentProfilePaymentMethodId', $paymentProfileRepository->getPaymentProfileEnabledPaymentMethodId());
		$adminView->assign('paymentProfiles', PaymentProfiles_View_PaymentProfileView::getPaymentProfilesView($paymentProfileRepository->getByUserId($userId)));

		// TODO: load new profile form
		$adminView->assign('paymentProfileForm', $this->getPaymentProfileForm(new PaymentProfiles_Model_PaymentProfile()));

		$adminView->render('pages/payment-profiles/user-page-tab-list');
	}

	/**
	 * Add form action
	 */
	public function ajaxAddFormAction()
	{
		//TODO: add proper error handling
		$request = $this->getRequest();
		$userId = intval($request->get('userId', 0));
		$paymentMethodId = intval($request->get('paymentMethodId', 0));

		if ($userId == 0 || $paymentMethodId == 0)
		{
			$response = array('status' => 0, 'message' => 'User or payment method id is undefined');
		}
		else
		{
			$paymentProfile = new PaymentProfiles_Model_PaymentProfile();
			$paymentProfile->setUserId($userId);
			$paymentProfile->setPaymentMethodId($paymentMethodId);
			$pprovider = $this->getPaymentProvider();
			$additionalData = array();
			if ($pprovider)
			{
				$gateway = $pprovider->getPaymentProcessorById($paymentMethodId);
				if ($gateway)
				{
					$additionalData = $gateway->getTemplateData();
					if ($additionalData)
					{
						$additionalData['profilename'] = $gateway->id;
					}
					$paymentProfile->setAdditionalParams($additionalData);
				}
			}

			$adminView = $this->getView();
			$adminView->assign('actionSource', $request->getRequest()->get('actionSource', ''));
			$adminView->assign('paymentProfileForm', $this->getPaymentProfileForm($paymentProfile));

			$response = array(
				'status' => 1,
				'html' => $adminView->fetch('pages/payment-profiles/modal-add-payment-profile-form')
			);
		}

		$this->renderJson($response);
	}

	/**
	 * Edit form action
	 */
	public function ajaxEditFormAction()
	{
		//TODO: add proper error handling
		$request = $this->getRequest();

		$paymentProfileId = intval($request->get('paymentProfileId'));
		if ($paymentProfile = $this->getPaymentProfileRepository()->getById($paymentProfileId))
		{
			$paymentMethodId = $paymentProfile->getPaymentMethodId();
			$pprovider = $this->getPaymentProvider();
			$additionalData = array();
			if ($pprovider)
			{
				$gateway = $pprovider->getPaymentProcessorById($paymentMethodId);
				if ($gateway)
				{
					$additionalData = $gateway->getTemplateData();
					if ($additionalData)
					{
						$additionalData['profilename'] = $gateway->id;
					}
					$paymentProfile->setAdditionalParams($additionalData);
				}
			}
			$adminView = $this->getView();
			$adminView->assign('actionSource', $request->getRequest()->get('actionSource', ''));
			$adminView->assign('paymentProfileForm', $this->getPaymentProfileForm($paymentProfile));

			$response = array(
				'status' => 1,
				'html' => $adminView->fetch('pages/payment-profiles/modal-edit-payment-profile-form')
			);
		}
		else
		{
			$response = array('status' => 0, 'message' => 'Cannot find payment profile');
		}

		// TODO: return ajax response object
		$this->renderJson($response);
	}

	/**
	 * Persist action
	 */
	public function ajaxPersistAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->getPaymentProfileRepository();

		$paymentProfileId = $request->getRequest()->getInt('paymentProfileId', 0);
		$userId = $request->getRequest()->getInt('userId', 0);
		$paymentMethodId = $request->getRequest()->getInt('paymentMethodId', 0);

		$actionSource = $request->getRequest()->get('actionSource', '');

		$form = $request->request->get('payment_profile', array());
		$form = is_array($form) ? $form : array();

		$payment_method_nonce = $request->request->get('payment_method_nonce', false);

		/** @var DB $db */
		$db = $this->getDb();

		/** @var array $settings */
		$settings = $this->getSettings()->getAll();
		try
		{
			if ($paymentProfileId > 0)
			{
				// update current payment profile
				$editPaymentProfile = $paymentProfileRepository->getById($paymentProfileId);
				if (is_null($editPaymentProfile)) throw new Framework_Exception('Cannot find payment profile');

				$editPaymentProfile->hydrateFromForm($form);
				if ($payment_method_nonce)
				{
					$form['payment_method_nonce'] = $payment_method_nonce;
					$additionalData = $form;
					$editPaymentProfile->setAdditionalParams($additionalData);
				}

				$user = USER::getById($db, $settings, $editPaymentProfile->getUserId());

				if (is_null($user)) throw new Framework_Exception('Cannot find user by id');

				$this->getPaymentProfilesProvider()->updatePaymentProfile($editPaymentProfile, $user);

				$adminView = $this->getView();
				$adminView->assign('paymentProfile', new PaymentProfiles_View_PaymentProfileView($editPaymentProfile));

				$response = array(
					'status' => 1,
					'paymentProfileId' => $editPaymentProfile->getId(),
					//'message' => 'Payment method has been updated',
					'primary' => $editPaymentProfile->getIsPrimary(),
					'html' => $adminView->fetch('pages/payment-profiles/payment-profile-view') // view only, no frame, no links
				);
			}
			else if ($userId > 0 && $paymentMethodId > 0)
			{
				// create new payment profile
				$user = USER::getById($db, $settings, $userId);

				if (is_null($user)) throw new Framework_Exception('Cannot find user by id');

				$newPaymentProfile = new PaymentProfiles_Model_PaymentProfile();
				$newPaymentProfile->setUserId($user->getId());
				$newPaymentProfile->hydrateFromForm($form);

				if ($payment_method_nonce)
				{
					$form['payment_method_nonce'] = $payment_method_nonce;
					$additionalData = $form;
					$newPaymentProfile->setAdditionalParams($additionalData);
				}

				$this->getPaymentProfilesProvider()->addPaymentProfile($newPaymentProfile, $user, $paymentMethodId);

				$adminView = $this->getView();
				$adminView->assign('paymentProfile', new PaymentProfiles_View_PaymentProfileView($newPaymentProfile));

				$response = array(
					'status' => 1,
					'paymentProfileId' => $newPaymentProfile->getId(),
					//'message' => 'Payment method has been added',
					'primary' => $newPaymentProfile->getIsPrimary(),
					'html' => $adminView->fetch(
						$actionSource == 'recurring-profile' ? 'pages/payment-profiles/recurring-payment-profile' : 'pages/payment-profiles/payment-profile-view'
					) // with frame and links
				);
			}
			else
			{
				throw new Framework_Exception('Unknown action');
			}
		}
		catch (Framework_Exception $e)
		{
			$errorDetails = $e->getDetails();
			$errors = array();
			if (count($errorDetails) > 0)
			{
				foreach ($errorDetails as $key => $value) $errors[] = array('field' => 'field-payment_profile-'.$key, 'message' => $value);
			}

			$response = array(
				'status' => 0,
				'message' => $e->getMessage(),
				'errors' => $errors,
			);
		}

		$this->renderJson($response);
	}

	/**
	 * Set primary action
	 */
	public function ajaxSetPrimaryAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$paymentProfileId = $request->getRequest()->getInt('paymentProfileId', 0);
		try
		{
			if ($paymentProfileId > 0)
			{
				/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
				$paymentProfileRepository = $this->getPaymentProfileRepository();

				// update current payment profile
				$editPaymentProfile = $paymentProfileRepository->getById($paymentProfileId);
				if (is_null($editPaymentProfile)) throw new Framework_Exception('Cannot find payment profile');

				$editPaymentProfile->setIsPrimary(true);
				$paymentProfileRepository->persist($editPaymentProfile);

				$adminView = $this->getView();
				$adminView->assign('paymentProfile', new PaymentProfiles_View_PaymentProfileView($editPaymentProfile));

				$response = array(
					'status' => 1,
					'paymentProfileId' => $editPaymentProfile->getId(),
					'primary' => $editPaymentProfile->getIsPrimary(),
					'html' => $adminView->fetch('pages/payment-profiles/payment-profile-view') // view only, no frame, no links
				);
			}
			else
			{
				throw new Framework_Exception('Cannot find payment profile');
			}
		}
		catch (Framework_Exception $e)
		{
			$errorDetails = $e->getDetails();

			$errors = array();
			if (count($errorDetails) > 0)
			{
				foreach ($errorDetails as $key => $value) $errors[] = array('field' => 'field-payment_profile-'.$key, 'message' => $value);
			}

			$response = array(
				'status' => 0,
				'message' => $e->getMessage(),
				'errors' => $errors,
			);
		}

		$this->renderJson($response);
	}


	/**
	 * Remove action
	 */
	public function ajaxRemoveAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->getPaymentProfileRepository();

		/** @var DB $db */
		$db = $this->getDb();

		/** @var array $settings */
		$settings = $this->getSettings()->getAll();

		try
		{
			$paymentProfileId = $request->getRequest()->getInt('paymentProfileId', 0);
			if ($paymentProfileId == 0 || is_null($deletePaymentProfile = $paymentProfileRepository->getById($paymentProfileId)))
			{
				throw new Framework_Exception('Cannot find profile by id');
			}

			$user = USER::getById($db, $settings, $deletePaymentProfile->getUserId());

			if (is_null($user))
			{
				throw new Framework_Exception('Cannot find user by id');
			}

			$isPrimary = $deletePaymentProfile->getIsPrimary();
			$userId = $deletePaymentProfile->getUserId();
			$newPrimaryId = 0;

			$this->getPaymentProfilesProvider()->deletePaymentProfile($deletePaymentProfile, $user);

			if ($isPrimary)
			{
				$newPrimaryPaymentProfile = $paymentProfileRepository->getPrimaryPaymentProfile($userId);

				if (!is_null($newPrimaryPaymentProfile))
				{
					$newPrimaryId = $newPrimaryPaymentProfile->getId();
				}
			}

			$response = array('status' => 1, 'message' => 'Payment method has been removed', 'primaryPaymentProfileId' => $newPrimaryId);

		}
		catch (Framework_Exception $e)
		{
			$response = array('status' => 0, 'message' => $e->getMessage());

			if (count($e->getDetails()) > 0)
			{
				$response['message'] .= ':<br>'.implode('<br>', $e->getDetails());
			}
		}

		$this->renderJson($response);
	}

	/**
	 * Change payment profile list
	 */
	public function ajaxRecurringChangeListAction()
	{
		$request = $this->getRequest();

		try
		{
			$userId = intval($request->get('userId', 0));
			$currentPaymentProfileId = intval($request->get('currentPaymentProfileId', 0));
			$recurringProfileId = intval($request->get('recurringProfileId', 0));

			if ($userId == 0 || $recurringProfileId == 0) throw new Framework_Exception('User id or recurring profile id is incorrect');

			$recurringProfileRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($this->getDb(), $this->getSettings());
			$recurringProfile = $recurringProfileRepository->get($recurringProfileId, $userId);

			if (is_null($recurringProfile)) throw new Framework_Exception('Cannot find recurring profile by id');

			$paymentProfileRepository = $this->getPaymentProfileRepository();

			$adminView = $this->getView();
			$adminView->assign('userId', $userId);
			$adminView->assign('recurringProfileId', $recurringProfileId);
			$adminView->assign('currentPaymentProfileId', $currentPaymentProfileId);
			$adminView->assign('paymentProfilePaymentMethodId', $paymentProfileRepository->getPaymentProfileEnabledPaymentMethodId());
			$adminView->assign(
				'paymentProfiles',
				PaymentProfiles_View_PaymentProfileView::getPaymentProfilesView($paymentProfileRepository->getByUserId($userId, true, true))
			);

			$response = array(
				'status' => 1,
				'html' => $adminView->fetch('pages/payment-profiles/modal-change-payment-profile-form')
			);
		}
		catch (Framework_Exception $e)
		{
			$response = array('status' => 0, 'message' => $e->getMessage());

			if (count($e->getDetails()) > 0)
			{
				$response['message'] .= ':<br>'.implode('<br>', $e->getDetails());
			}
		}

		$this->renderJson($response);
	}

	/**
	 * Persis payment profile changes in billing profile
	 */
	public function ajaxRecurringChangePersistAction()
	{
		//TODO: add proper error handling
		$request = $this->getRequest();

		try
		{
			$userId = intval($request->get('userId', 0));
			$recurringProfileId = intval($request->get('recurringProfileId', 0));
			$paymentProfileId = intval($request->get('paymentProfileId', 0));

			if ($userId == 0 || $recurringProfileId == 0 || $paymentProfileId == 0) throw new Framework_Exception('User id, payment profile id, or recurring profile id is incorrect');

			$recurringProfileRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($this->getDb(), $this->getSettings());
			$recurringProfile = $recurringProfileRepository->get($recurringProfileId, $userId);

			if (is_null($recurringProfile)) throw new Framework_Exception('Cannot find recurring profile by id');

			$paymentProfile = $this->getPaymentProfileRepository()->getById($paymentProfileId, $userId);

			if (is_null($recurringProfile)) throw new Framework_Exception('Cannot find payment profile by id');

			$recurringProfile->setPaymentProfileId($paymentProfile->getId());
			$recurringProfileRepository->persist($recurringProfile);

			$adminView = $this->getView();
			$adminView->assign('paymentProfile', new PaymentProfiles_View_PaymentProfileView($paymentProfile));

			$response = array(
				'status' => 1,
				'paymentProfileId' => $paymentProfile->getId(),
				'message' => 'New payment profile has been set',
				'primary' => $paymentProfile->getIsPrimary(),
				'html' => $adminView->fetch('pages/payment-profiles/recurring-payment-profile')
			);
		}
		catch (Framework_Exception $e)
		{
			$response = array('status' => 0, 'message' => $e->getMessage());

			if (count($e->getDetails()) > 0)
			{
				$response['message'] .= ':<br>'.implode('<br>', $e->getDetails());
			}
		}

		$this->renderJson($response);
	}

	/**
	 * Get payment profile form
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return core_Form_FormBuilder
	 */
	protected function getPaymentProfileForm(PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$paymentProfileForm = new PaymentProfiles_Admin_Form_PaymentProfileForm($this->getSettings());
		return $paymentProfileForm->getForm($paymentProfile);
	}

	/**
	 * Get repository
	 *
	 * @return null|PaymentProfiles_DataAccess_PaymentProfileRepository
	 */
	protected function getPaymentProfileRepository()
	{
		//return $this->get('repository_payment_profile');
		if (is_null($this->repository))
		{
			$this->repository = new PaymentProfiles_DataAccess_PaymentProfileRepository($this->getDb());
		}

		return $this->repository;
	}

	protected function getPaymentProvider()
	{
		if (is_null($this->payment_provider))
		{
			$this->payment_provider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($this->getDb(), $this->getSettings()));
		}

		return $this->payment_provider;
	}

	/**
	 * Get payment profile provider instance
	 *
	 * @return PaymentProfiles_Provider_PaymentProfile
	 */
	protected function getPaymentProfilesProvider()
	{
		/** @var PaymentProfiles_Provider_PaymentProfile $provider */
		static $provider = null;

		if (is_null($provider))
		{
			/** @var DB $db */
			$db = $this->get('db');

			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $this->getPaymentProfileRepository();

			/** @var DataAccess_SettingsRepository $settings */
			$settings = $this->getSettings();

			$provider = new PaymentProfiles_Provider_PaymentProfile($db, $settings, $paymentProfileRepository);
		}

		return $provider;
	}
}
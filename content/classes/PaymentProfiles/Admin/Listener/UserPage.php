<?php
/**
 * Class PaymentProfiles_Admin_Listener_UserPage
 */
class PaymentProfiles_Admin_Listener_UserPage
{
	/**
	 * Add a user page tab
	 *
	 * @param Admin_Events_PageTabsEvent $event
	 */
	public function onPageTabs(Admin_Events_PageTabsEvent $event)
	{
		/** @var USER $user */
		$user = $event->getData('user');

		if ($user === null || (is_array($user) && $user['login'] == 'ExpressCheckoutUser')) return;

		/** @var Framework_Translator $translator */
		$translator = Framework_Translator::getInstance();

		$event->addTab('payment-profiles', $translator->trans('payment_profiles.payment_profiles'));
	}

	/**
	 * Render user page tab content
	 *
	 * @param Admin_Events_PageRenderEvent $event
	 */
	public function onPageRender(Admin_Events_PageRenderEvent $event)
	{
		/** @var USER $user */
		$user = $event->getData('user');

		if ($user === null || (is_array($user) && $user['login'] == 'ExpressCheckoutUser')) return;

		/** @var DB $db */
		$db = $event->getData('db');

		/** @var Framework_Request $request */
		$request = $event->getData('request');

		/** @var PaymentProfiles_Admin_Controller_PaymentProfile $controller */
		$controller = new PaymentProfiles_Admin_Controller_PaymentProfile($request, $db);

		$controller->userPageTabListAction();
	}
}
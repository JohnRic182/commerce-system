<?php
/**
 * Class PaymentProfiles_Provider_PaymentProfile
 */
class PaymentProfiles_Provider_PaymentProfile
{
	/** @var DB */
	protected $db;

	/** @var DataAccess_SettingsRepository $settings */
	protected $settings;

	/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
	protected $paymentProfileRepository;


	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 * @param PaymentProfiles_DataAccess_PaymentProfileRepository $repository
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings, PaymentProfiles_DataAccess_PaymentProfileRepository $repository)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->paymentProfileRepository = $repository;
	}

	/**
	 * Add a new payment profile
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $newPaymentProfile
	 * @param USER $user
	 * @param $paymentMethodId
	 *
	 * @throws Exception
	 */
	public function addPaymentProfile(PaymentProfiles_Model_PaymentProfile $newPaymentProfile, USER $user, $paymentMethodId)
	{
		/** @var DB $db */
		$db = $this->db;

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->paymentProfileRepository;

		/** @var PAYMENT_PROCESSOR $paymentProcessor */
		$paymentProcessor = $this->getPaymentProcessor($paymentMethodId);

		// TODO: translate this OR provide error code?
		if (!$paymentProcessor) throw new Framework_Exception('Cannot find payment method by id');

		$newPaymentProfile->setPaymentMethodId($paymentMethodId);

		$validationErrors = $this->validate($newPaymentProfile);

		if ($validationErrors)
		{
			throw new Framework_Exception('Please enter all required fields', $validationErrors);
		}

		// TODO: add event here

		if (!$paymentProcessor->addPaymentProfile($db, $user, $newPaymentProfile))
		{
			throw new Framework_Exception($paymentProcessor->error_message);
		}

		$paymentProfileRepository->persist($newPaymentProfile);

		/**
		 * Handle on add event
		 */
		$onUpdateEvent = new PaymentProfiles_Events_PaymentProfileEvent(PaymentProfiles_Events_PaymentProfileEvent::ON_ADD);
		$onUpdateEvent
			->setPaymentProfile($newPaymentProfile)
			->setData('paymentGateway', $paymentProcessor);
		Events_EventHandler::handle($onUpdateEvent);
	}

	/**
	 * Update payment profile
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $editPaymentProfile
	 * @param USER $user
	 *
	 * @throws Exception
	 */
	public function updatePaymentProfile(PaymentProfiles_Model_PaymentProfile $editPaymentProfile, USER $user)
	{
		/** @var DB $db */
		$db = $this->db;

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->paymentProfileRepository;

		/** @var PAYMENT_PROCESSOR $paymentProcessor */
		$paymentProcessor = $this->getPaymentProcessor($editPaymentProfile->getPaymentMethodId());

		if (!$paymentProcessor) throw new Framework_Exception('Cannot find payment method by id');

		$validationErrors = $this->validate($editPaymentProfile);

		if ($validationErrors)
		{
			throw new Framework_Exception('Please enter all required fields', $validationErrors);
		}

		// TODO: add event here

		if (!$paymentProcessor->updatePaymentProfile($db, $user, $editPaymentProfile))
		{
			throw new Framework_Exception($paymentProcessor->error_message);
		}

		$paymentProfileRepository->persist($editPaymentProfile);

		/**
		 * Handle on update event
		 */
		$onUpdateEvent = new PaymentProfiles_Events_PaymentProfileEvent(PaymentProfiles_Events_PaymentProfileEvent::ON_UPDATE);
		$onUpdateEvent
			->setPaymentProfile($editPaymentProfile)
			->setData('paymentGateway', $paymentProcessor);
		Events_EventHandler::handle($onUpdateEvent);
	}

	/**
	 * Delete payment profile
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $deletePaymentProfile
	 * @param USER $user
	 *
	 * @throws Exception
	 */
	public function deletePaymentProfile(PaymentProfiles_Model_PaymentProfile $deletePaymentProfile, USER $user)
	{
		/** @var DB $db */
		$db = $this->db;

		/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
		$paymentProfileRepository = $this->paymentProfileRepository;

		/** @var PAYMENT_PROCESSOR $paymentProcessor */
		$paymentProcessor = $this->getPaymentProcessor($deletePaymentProfile->getPaymentMethodId());

		if (!$paymentProcessor) throw new Framework_Exception('Cannot find payment gateway by id');
		/**
		 * Process on before delete payment profile event / filter
		 */
		$beforeRemoveEvent = new PaymentProfiles_Events_PaymentProfileEvent(PaymentProfiles_Events_PaymentProfileEvent::ON_BEFORE_REMOVE);
		$beforeRemoveEvent
			->setPaymentProfile($deletePaymentProfile)
			->setData('paymentGateway', $paymentProcessor);
		Events_EventHandler::handle($beforeRemoveEvent);

		if (!$beforeRemoveEvent->getData('canRemove', true))
		{
			throw new Framework_Exception('Cannot remove payment profile', $beforeRemoveEvent->hasErrors() ? $beforeRemoveEvent->getErrors() : array());
		}

		if (!$paymentProcessor->deletePaymentProfile($db, $user, $deletePaymentProfile))
		{
			throw new Framework_Exception($paymentProcessor->error_message);
		}

		$onRemoveEvent = new PaymentProfiles_Events_PaymentProfileEvent(PaymentProfiles_Events_PaymentProfileEvent::ON_REMOVE);
		$onRemoveEvent
			->setPaymentProfile($deletePaymentProfile)
			->setData('paymentGateway', $paymentProcessor);
		Events_EventHandler::handle($onRemoveEvent);

		$paymentProfileRepository->removeProfile($deletePaymentProfile);
	}

	/**
	 * Validate payment profile
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return array | bool
	 *
	 * @throws Framework_Exception
	 */
	protected function validate(PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$db = $this->db;
		$settings = $this->settings;
		$settingsValues = $settings->getAll();
		$validationErrors = array();

		// address validation first
		$addressData = $paymentProfile->getBillingData()->toArray();
		$addressValidationErrors = AddressValidator::validateInput($db, $settingsValues, Model_Address::ADDRESS_PAYMENT, $addressData);

		foreach ($addressValidationErrors as $key => $value)
		{
			$validationErrors[$key] = $value;
		}


		$additionalParams = $paymentProfile->getAdditionalParams();
		$verifyCard = true;
		if ($additionalParams)
		{
			if ((isset($additionalParams['payment_method_nonce']) && $additionalParams['payment_method_nonce']))
			{
				$verifyCard = false;
			}
		}

		if ($verifyCard)
		{
			// validate cc
			if (!validate($paymentProfile->getBillingData()->getCcNumber(), VALIDATE_CC))
			{
				$validationErrors['cc_number'] = 'Please enter valid credit card number';
			}

			$currentYear = date('Y');
			$currentMonth = date('n');

			$ccYear = intval($paymentProfile->getBillingData()->getCcExpirationYear());
			$ccMonth = intval($paymentProfile->getBillingData()->getCcExpirationMonth());

			if ($ccYear < $currentYear || ($ccYear == $currentYear && $ccMonth < $currentMonth))
			{
				$validationErrors['cc_expiration'] = 'Card expiration date cannot be in the past';
			}
		}


		return count($validationErrors) > 0 ? $validationErrors : false;
	}

	/**
	 * Get payment gateway class instance
	 *
	 * @param $paymentMethodId
	 *
	 * @return null|PAYMENT_PROCESSOR
	 */
	protected function getPaymentProcessor($paymentMethodId)
	{
		/** @var DB $db */
		$db = $this->db;

		static $provider = null;

		if (is_null($provider))
		{
			// TODO: get from registry
			$provider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db)));
		}

		return $provider->getPaymentProcessorById($paymentMethodId);
	}
}
<?php
/**
 * Class PaymentProfiles_Model_PaymentProfile
 */
class PaymentProfiles_Model_PaymentProfile
{
	protected $id = null;
	protected $userId = 0;
	protected $paymentMethodId = 0;
	protected $externalProfileId = '';
	protected $isPrimary = false;
	protected $name = '';
	protected $additionalParams = array();

	/** @var PaymentProfiles_Model_BillingData $billingData */
	protected $billingData = null;

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		/** @var PaymentProfiles_Model_BillingData billingData */
		$this->billingData = new PaymentProfiles_Model_BillingData();
	}

	/**
	 * Set data from database
	 *
	 * @param array $data
	 */
	public function hydrateFromDb(array $data)
	{
		if (isset($data['usid'])) $this->setId($data['usid']);
		elseif (isset($data['profile_id'])) $this->setId($data['profile_id']);
		elseif (isset($data['payment_profile_id'])) $this->setId($data['payment_profile_id']);
		else $this->setId(0);

		if (isset($data['uid'])) $this->setUserId($data['uid']);
		elseif (isset($data['user_id'])) $this->setUserId($data['user_id']);
		else $this->setUserId(0);

		if (isset($data['pid'])) $this->setPaymentMethodId($data['pid']);
		elseif (isset($data['payment_method_id'])) $this->setPaymentMethodId($data['payment_method_id']);
		else $this->setPaymentMethodId(0);

		if (isset($data['ext_profile_id'])) $this->setExternalProfileId($data['ext_profile_id']);
		elseif (isset($data['external_id'])) $this->setExternalProfileId($data['external_id']);
		elseif (isset($data['token'])) $this->setExternalProfileId($data['token']);
		else $this->setExternalProfileId('');

		$this->setIsPrimary(isset($data['is_primary']) && $data['is_primary'] == 'Yes');

		if (isset($data['name'])) $this->setName($data['name']);

		if (isset($data['billing_data']))
		{
			$billingData = is_array($data['billing_data']) ? $data['billing_data'] : @unserialize($data['billing_data']);
			$billingData = $billingData ? $billingData : array();
			$this->getBillingData()->hydrateFromArray($billingData);
		}
	}

	/**
	 * Build object instance from database
	 *
	 * @param array $data
	 *
	 * @return PaymentProfiles_Model_PaymentProfile
	 */
	public static function buildFromDb(array $data)
	{
		/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
		$paymentProfile = new PaymentProfiles_Model_PaymentProfile();
		$paymentProfile->hydrateFromDb($data);
		return $paymentProfile;
	}

	/**
	 * Hydrate from form
	 *
	 * @param $data
	 */
	public function hydrateFromForm(array $data)
	{
		if (isset($data['name'])) $this->setName($data['name']);

		if (isset($data['is_primary']) && in_array(strtolower($data['is_primary']), array('yes', '1', 'on')))
		{
			$this->setIsPrimary(true);
		}

		$this->getBillingData()->hydrateFromArray($data);
	}

	/**
	 * Set id
	 *
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return intval($this->id);
	}

	/**
	 * Set user id
	 *
	 * @param int $userId
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;
	}

	/**
	 * Get user id
	 *
	 * @return int
	 */
	public function getUserId()
	{
		return intval($this->userId);
	}

	/**
	 * Set payment method id
	 *
	 * @param int $paymentMethodId
	 */
	public function setPaymentMethodId($paymentMethodId)
	{
		$this->paymentMethodId = $paymentMethodId;
	}

	/**
	 * Get payment method id
	 *
	 * @return int
	 */
	public function getPaymentMethodId()
	{
		return $this->paymentMethodId;
	}

	/**
	 * Set external profile id
	 *
	 * @param strong $externalProfileId
	 */
	public function setExternalProfileId($externalProfileId)
	{
		$this->externalProfileId = $externalProfileId;
	}

	/**
	 * Get external profile id
	 *
	 * @return string
	 */
	public function getExternalProfileId()
	{
		return $this->externalProfileId;
	}

	/**
	 * Set is primary
	 *
	 * @param boolean $isPrimary
	 */
	public function setIsPrimary($isPrimary)
	{
		$this->isPrimary = $isPrimary;
	}

	/**
	 * Get is primary
	 *
	 * @return boolean
	 */
	public function getIsPrimary()
	{
		return $this->isPrimary;
	}

	/**
	 * Set billing data
	 *
	 * @param PaymentProfiles_Model_BillingData $billingData
	 */
	public function setBillingData(PaymentProfiles_Model_BillingData $billingData)
	{
		$this->billingData = $billingData;
	}

	/**
	 * Get billing data
	 *
	 * @return PaymentProfiles_Model_BillingData
	 */
	public function getBillingData()
	{
		return $this->billingData;
	}

	/**
	 * Set name
	 *
	 * @param $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return array
	 */
	public function getAdditionalParams()
	{
		return $this->additionalParams;
	}

	/**
	 * @param array $additionalParams
	 */
	public function setAdditionalParams($additionalParams)
	{
		$this->additionalParams = $additionalParams;
	}

	

	public function getExpirationDate()
	{
		$billingData = $this->getBillingData();
		if ($billingData !== null)
		{
			$ccExpirationYear = $billingData->getCcExpirationYear();
			$ccExpirationMonth = $billingData->getCcExpirationMonth();

			if (trim($ccExpirationMonth) == '' || trim($ccExpirationYear) == '') return null;

			//Y-m-t - gives the last day of the month
			return new DateTime(date('Y-m-t H:i:s', strtotime($billingData->getCcExpirationYear().'-'.$billingData->getCcExpirationMonth().'-1 23:59:59')));
		}

		return null;
	}
}


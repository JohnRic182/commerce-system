<?php
/**
 * Class PaymentProfiles_Model_BillingData
 */
class PaymentProfiles_Model_BillingData
{
	/** @var string */
	protected $name;

	/** @var  Model_Address */
	protected $address;

	/** @var string */
	protected $phone;

	protected $ccNumber;
	protected $ccExpirationMonth;
	protected $ccExpirationYear;
	protected $ccExpirationDate;
	protected $ccType;
	protected $ccCode;

	/**
	 * Class constructor
	 *
	 * @param array|null $data
	 */
	public function __construct($data = null)
	{
		$this->address = new Model_Address();

		if (!is_null($data) && is_array($data))
		{
			$this->hydrateFromArray($data);
		}
	}

	/**
	 * Hydrate data
	 *
	 * @param array|null $data
	 */
	public function hydrateFromArray($data = null)
	{
		// TODO: fix global dependency here
		global $db;

		$address = $this->getAddress();

		if (isset($data['first_name']))
		{
			$address->setFirstName($data['first_name']);
		}
		else if (isset($data['fname']))
		{
			$address->setFirstName($data['fname']);
		}

		if (isset($data['last_name']))
		{
			$address->setLastName($data['last_name']);
		}
		else if (isset($data['lname']))
		{
			$address->setLastName($data['lname']);
		}

		$this->setName(trim($address->getFirstName().' '.$address->getLastName()));

		if (isset($data['address1'])) $address->setAddressLine1($data['address1']);
		if (isset($data['address2'])) $address->setAddressLine2($data['address2']);
		if (isset($data['city'])) $address->setCity($data['city']);

		$country = null;

		if (isset($data['country_id']))
		{
			$country = getCountryData($db, $data['country_id']);

			if (is_null($country)) $address->setCountryId($data['country_id']);
		}

		if (is_null($country) && isset($data['country']))
		{
			$country = getCountryData(
				$db,
				is_numeric($data['country']) ? $data['country'] : false,
				!is_numeric($data['country']) ? $data['country'] : false
			);

			if (!is_null($country))
			{
				// We use countries ISO 2 char code in billing profiles
				is_numeric($data['country']) ? $address->setCountryId($data['country']) : $address->setCountryIso2($data['country']);
			}
		}

		if (!is_null($country))
		{
			$address->setCountryName($country['name']);
			$address->setCountryId($country['coid']);
			$address->setCountryIso2($country['iso_a2']);
			$address->setCountryIso3($country['iso_a3']);
			$address->setCountryIsoNumber($country['iso_number']);
		}

		$state = null;

		if (isset($data['state_id']))
		{
			$state = getStateData($db, $data['state_id']);

			if (is_null($state)) $address->setStateId($data['state_id']);
		}

		if (is_null($state) && isset($data['state']))
		{
			$address->setStateCode($data['state']);

			$state = getStateData(
				$db,
				is_numeric($data['state']) ? $data['state'] : false,
				!is_numeric($data['state']) ? $data['state'] : false
			);

			if (is_null($state))
			{
				is_numeric($data['state']) ? $address->setStateId($data['state']) : $address->setStateCode($data['state']);
			}
		}

		if (!is_null($state))
		{
			$address->setStateName($state['name']);
			$address->setStateId($state['stid']);
			$address->setStateCode($state['short_name']);
		}

		if (isset($data['province']) && trim($data['province']) != '')
		{
			$address->setProvince($data['province']);
		}
		else
		{
			$address->setProvince($address->getStateName());
		}

		if ($address->getName() == '')
		{
			$address->setName($address->getFirstName() . ' ' . $address->getLastName());
		}

		if (isset($data['zip'])) $address->setZip($data['zip']);

		if (isset($data['phone'])) $this->setPhone($data['phone']);

		// TODO: make sure that cc is stored masked in database - use $this->maskCc()
		if (isset($data['cc_number'])) $this->setCcNumber($data['cc_number']);

		if (isset($data['cc_expiration']) && isset($data['cc_expiration']['month']) && isset($data['cc_expiration']['year']))
		{
			$this->setCcExpirationMonth($data['cc_expiration']['month']);
			$this->setCcExpirationYear($data['cc_expiration']['year']);
		}
		else
		{
			if (isset($data['cc_expiration_month'])) $this->setCcExpirationMonth($data['cc_expiration_month']);
			if (isset($data['cc_expiration_year'])) $this->setCcExpirationYear($data['cc_expiration_year']);
		}

		if (isset($data['cc_expiration_date']))
		{
			$this->setCcExpirationDate($data['cc_expiration_date']);
		}
		else if (isset($data['cc_expiration_month']) && isset($data['cc_expiration_year']))
		{
			$this->setCcExpirationDate($data['cc_expiration_month'] . '/' . $data['cc_expiration_year']);
		}

		if (isset($data['credit_card_type'])) $this->setCcType($data['credit_card_type']);

		// TODO: optimize this
		if (isset($data['cc_cvv2'])) $this->setCcCode($data['cc_cvv2']);
		else if (isset($data['cc_cvv'])) $this->setCcCode($data['cc_cvv']);
		else if (isset($data['cc_code'])) $this->setCcCode($data['cc_code']);
		else if (isset($data['cvv'])) $this->setCcCode($data['cc_code']);
		else if (isset($data['cvv2'])) $this->setCcCode($data['cc_code']);
		else $this->setCcCode('');
	}

	/**
	 * Return data as an array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$address = $this->getAddress();

		return array(
			'first_name' => $address->getFirstName(),
			'last_name' => $address->getLastName(),
			'address1' => $address->getAddressLine1(),
			'address2' => $address->getAddressLine2(),
			'city' => $address->getCity(),
			'country' => $address->getCountryIso2(),
			'country_id' => $address->getCountryId(),
			'state' => $address->getStateCode(),
			'state_id' => $address->getStateId(),
			'province' => $address->getProvince(),
			'zip' => $address->getZip(),
			'phone' => $this->getPhone(),
			'cc_number' => $this->getCcNumber(),
			'cc_expiration_month' => $this->getCcExpirationMonth(),
			'cc_expiration_year' => $this->getCcExpirationYear(),
			'cc_expiration_date' => $this->getCcExpirationDate(),
			'credit_card_type' => $this->getCcType()
		);
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set address
	 *
	 * @param \Model_Address $address
	 */
	public function setAddress($address)
	{
		$this->address = $address;
	}

	/**
	 * Get address
	 *
	 * @return \Model_Address
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Set cc expiration date
	 *
	 * @param mixed $ccExpirationDate
	 */
	public function setCcExpirationDate($ccExpirationDate)
	{
		$this->ccExpirationDate = $ccExpirationDate;
	}

	/**
	 * Get cc expiration date
	 *
	 * @return mixed
	 */
	public function getCcExpirationDate()
	{
		return $this->ccExpirationDate;
	}

	/**
	 * Set cc expiration month
	 *
	 * @param mixed $ccExpirationMonth
	 */
	public function setCcExpirationMonth($ccExpirationMonth)
	{
		$this->ccExpirationMonth = $ccExpirationMonth;
	}

	/**
	 * Get cc expiration month
	 *
	 * @return mixed
	 */
	public function getCcExpirationMonth()
	{
		return $this->ccExpirationMonth;
	}

	/**
	 * Set cc expiration year
	 *
	 * @param mixed $ccExpirationYear
	 */
	public function setCcExpirationYear($ccExpirationYear)
	{
		$this->ccExpirationYear = $ccExpirationYear;
	}

	/**
	 * Get cc expiration year
	 *
	 * @return mixed
	 */
	public function getCcExpirationYear()
	{
		return $this->ccExpirationYear;
	}

	/**
	 * Set cc number
	 *
	 * @param mixed $ccNumber
	 */
	public function setCcNumber($ccNumber)
	{
		$this->ccNumber = $ccNumber;
	}

	/**
	 * Get cc number
	 *
	 * @return mixed
	 */
	public function getCcNumber()
	{
		return $this->ccNumber;
	}

	/**
	 * Set cc type
	 *
	 * @param mixed $ccType
	 */
	public function setCcType($ccType)
	{
		$this->ccType = $ccType;
	}

	/**
	 * Get cc type
	 *
	 * @return mixed
	 */
	public function getCcType()
	{
		return $this->ccType;
	}

	public function setCcCode($ccCode)
	{
		$this->ccCode = $ccCode;
	}

/**
 * @return mixed
 */
public function getCcCode()
	{
		return $this->ccCode;
	}

	/**
	 * Set phone
	 *
	 * @param mixed $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * Get phone
	 *
	 * @return mixed
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Mask credit card
	 *
	 * @param $cc
	 *
	 * @return string
	 */
	public static function maskCc($cc)
	{
		return str_repeat('X', 12).substr($cc, -4);
	}
}
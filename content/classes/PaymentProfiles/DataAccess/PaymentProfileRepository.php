<?php
/**
 * Class Payment_DataAccess_PaymentProfileRepository
 */
class PaymentProfiles_DataAccess_PaymentProfileRepository
{
	/** @var DB $db */
	protected $db;

	const VALIDATE_ADD = 'validate-add';
	const VALIDATE_UPDATE = 'validate-update';

	/**
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		$this->db = $db;
	}

	/**
	 * Get payment profile by id
	 *
	 * @param $id
	 * @param null $userId
	 *
	 * @return PaymentProfiles_Model_PaymentProfile|null
	 */
	public function getById($id, $userId = null)
	{
		$data = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'users_payment_profiles WHERE usid='.intval($id).(!is_null($userId) ? ' AND uid='.intval($userId) : ''));

		return $data ? PaymentProfiles_Model_PaymentProfile::buildFromDb($data) : null;
	}


	public function getByExtProfileId($id, $userId = null)
	{
		$data = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'users_payment_profiles WHERE ext_profile_id="'.trim($id).'"'.(!is_null($userId) ? ' AND uid='.intval($userId) : ''));

		return $data ? PaymentProfiles_Model_PaymentProfile::buildFromDb($data) : null;
	}

	/**
	 * Get user's primary payment profile
	 *
	 * @param $userId
	 *
	 * @return null|PaymentProfiles_Model_PaymentProfile
	 */
	public function getPrimaryPaymentProfile($userId)
	{
		$data = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'users_payment_profiles WHERE uid='.intval($userId).' AND is_primary="Yes"');

		return $data ? PaymentProfiles_Model_PaymentProfile::buildFromDb($data) : null;
	}

	/**
	 * Get payment profiles by user ID
	 *
	 * @param $userId
	 * @param bool $onlyActive
	 * @param bool $onlySupportsRecurring
	 *
	 * @return array
	 */
	public function getByUserId($userId, $onlyActive = true, $onlySupportsRecurring = false)
	{
		$results = $this->db->selectAll('
			SELECT upp.*, pm.id, pm.class
			FROM '.DB_PREFIX.'users_payment_profiles upp
			INNER JOIN '.DB_PREFIX.'payment_methods pm ON upp.pid = pm.pid'.($onlyActive ? ' AND pm.active="Yes"' : '').' WHERE upp.uid='.intval($userId).'
			'.($onlySupportsRecurring ? ' AND pm.supports_recurring_billing = 1' : '').'
			ORDER BY pm.id, upp.is_primary asc, upp.usid
		');

		$profiles = array();

		foreach ($results as $values)
		{
			$profile = PaymentProfiles_Model_PaymentProfile::buildFromDb($values);

			if (!is_null($profile)) $profiles[$profile->getId()] = $profile;
		}

		return $profiles;
	}


	/**
	 * Get payment method that supports payment profile
	 *
	 * @return int | null
	 *
	 * TODO: move into payment methods repository?
	 */
	public function getPaymentProfileEnabledPaymentMethodId()
	{
		$paymentMethodsResult = $this->db->selectAll('SELECT pid, id, class FROM '.DB_PREFIX.'payment_methods WHERE active="Yes" ORDER BY priority');

		if ($paymentMethodsResult)
		{
			foreach ($paymentMethodsResult as $paymentMethodData)
			{
				if ($paymentMethodData['class'] != '')
				{
					$paymentMethodClass = $paymentMethodData['class'];

					if (class_exists($paymentMethodClass))
					{
						$tmpGatewayInstance = new $paymentMethodClass;

						if ($tmpGatewayInstance->supportsPaymentProfiles())
						{
							return $paymentMethodData['pid'];
						}
					}
				}
			}
		}

		return null;
	}

	/**
	 * Save payment profile
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 */
	public function persist(PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$db = $this->db;

		$isPrimary = $paymentProfile->getIsPrimary();

		if ($isPrimary)
		{
			$db->query('UPDATE '.DB_PREFIX.'users_payment_profiles SET is_primary="No" WHERE uid='.intval($paymentProfile->getUserId()));
		}
		else
		{
			$row = $db->selectOne('
				SELECT COUNT(*) as total
				FROM '.DB_PREFIX.'users_payment_profiles up
				INNER JOIN '.DB_PREFIX.'payment_methods p ON up.pid = p.pid AND p.active = "Yes"
				WHERE up.is_primary = "Yes" AND up.uid = '.intval($paymentProfile->getUserId())
			);

			$isPrimary = $row['total'] == 0;
		}

		$paymentProfile->setIsPrimary($isPrimary);

		$db->reset();
		$db->assign('uid', $paymentProfile->getUserId());
		$db->assign('pid', $paymentProfile->getPaymentMethodId());
		$db->assignStr('ext_profile_id', $paymentProfile->getExternalProfileId());
		$db->assignStr('is_primary', $paymentProfile->getIsPrimary() ? 'Yes' : 'No');
		$db->assignStr('name', $paymentProfile->getName());
		// Y-m-t - gives the last day of the month

		if ($paymentProfile->getExpirationDate() !== null)
		{
			$db->assignStr('cc_exp_date', $paymentProfile->getExpirationDate()->format('Y-m-t H:i:s'));
		}
		else
		{
			$db->assign('cc_exp_date', 'null');
		}

		$billingDataArray = $paymentProfile->getBillingData()->toArray();
		// mask cc
		$billingDataArray['cc_number'] = PaymentProfiles_Model_BillingData::maskCc($billingDataArray['cc_number']);
		$db->assignStr('billing_data', serialize($billingDataArray));

		if ($paymentProfile->getId() > 0)
		{
			$db->update(DB_PREFIX.'users_payment_profiles', 'WHERE usid='.intval($paymentProfile->getId()));
		}
		else
		{
			$paymentProfile->setId($db->insert(DB_PREFIX.'users_payment_profiles'));
		}

		return $paymentProfile;
	}

	/**
	 * Remove the payment profile
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $profile
	 *
	 * @return int | bool
	 */
	public function removeProfile(PaymentProfiles_Model_PaymentProfile $profile)
	{
		if ($profile->getId() < 1) return;

		$db = $this->db;

		$isPrimary = $profile->getIsPrimary();

		$db->query('DELETE FROM '.DB_PREFIX.'users_payment_profiles WHERE usid = '.intval($profile->getId()));

		if ($isPrimary)
		{
			$nextPrimary = $this->db->selectOne('
				SELECT up.usid, up.is_primary FROM '.DB_PREFIX.'users_payment_profiles up
				INNER JOIN '.DB_PREFIX.'payment_methods p ON up.pid=p.pid AND p.active="Yes"
				WHERE up.uid = '.intval($profile->getUserId()).' ORDER BY up.usid DESC'
			);

			if ($nextPrimary && $nextPrimary['is_primary'] == 'No')
			{
				$this->db->query('UPDATE '.DB_PREFIX.'users_payment_profiles SET is_primary = "Yes" WHERE usid='.intval($nextPrimary['usid']));
			}
		}
	}
}
<?php
/**
 * Class PaymentProfiles_Plugin
 */
class PaymentProfiles_Plugin
{
	/**
	 * Init method
	 *
	 * @param Framework_Registry $registry
	 */
	public static function init(Framework_Registry $registry)
	{
		$registry->setService('repository_payment_profile', 'PaymentProfiles_DataAccess_PaymentProfileRepository', array('@db'));

//		if (!defined('CRON'))
//		{
//			$listener = new PaymentProfiles_Admin_Listener_UserPage();
//
//			Events_EventHandler::registerListener(
//				'admin.user.page_tabs',
//				array($listener, 'onPageTabs')
//			);
//
//			Events_EventHandler::registerListener(
//				'admin.user.page_render',
//				array($listener, 'onPageRender')
//			);
//		}
	}
}

<?php
/**
 * Class PaymentProfiles_Events_PaymentProfileEvent
 */
class PaymentProfiles_Events_PaymentProfileEvent extends Events_Event
{
	const ON_BEFORE_ADD = 'on-payment-profiles-before-payment-profile-add';
	const ON_ADD = 'on-payment-profiles-payment-profile-add';
	const ON_BEFORE_UPDATE = 'on-payment-profiles-before-payment-profile-update';
	const ON_UPDATE = 'on-payment-profiles-payment-profile-update';
	const ON_BEFORE_REMOVE = 'on-payment-profiles-before-payment-profile-remove';
	const ON_REMOVE = 'on-payment-profiles-payment-profile-remove';

	/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
	protected $paymentProfile;

	/**
	 * Set payment profile
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $paymentProfile
	 *
	 * @return $this
	 */
	public function setPaymentProfile(PaymentProfiles_Model_PaymentProfile $paymentProfile)
	{
		$this->paymentProfile = $paymentProfile;
		return $this;
	}

	/**
	 * Get payment profile
	 *
	 * @return PaymentProfiles_Model_PaymentProfile
	 */
	public function getPaymentProfile()
	{
		return $this->paymentProfile;
	}
}
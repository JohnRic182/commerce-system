<?php
/**
 * Class PaymentProfiles_View_PaymentProfileView
 */
class PaymentProfiles_View_PaymentProfileView
{
	public $id;
	public $userId;
	public $paymentMethodId;
	public $externalProfileId;
	public $isPrimary;
	public $billingData;
	public $additionalParams;
	public $name;

	/**
	 * Class constructor
	 *
	 * @param PaymentProfiles_Model_PaymentProfile $profile
	 */
	public function __construct(PaymentProfiles_Model_PaymentProfile $profile)
	{
		$this->id = $profile->getId();
		$this->userId = $profile->getUserId();
		$this->paymentMethodId = $profile->getPaymentMethodId();
		$this->externalProfileId = $profile->getExternalProfileId();
		$this->isPrimary = $profile->getIsPrimary();
		$this->billingData = new PaymentProfiles_View_BillingDataView($profile->getBillingData());
		$this->name = $profile->getName();
		$this->additionalParams = $profile->getAdditionalParams();
	}

	/**
	 * Get payment profiles views
	 *
	 * @param array $profileModels
	 *
	 * @return array
	 */
	public static function getPaymentProfilesView($profileModels)
	{
		$profiles = array();

		/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
		foreach ($profileModels as $paymentProfile)
		{
			$profiles[] = new PaymentProfiles_View_PaymentProfileView($paymentProfile);
		}

		return $profiles;
	}
}
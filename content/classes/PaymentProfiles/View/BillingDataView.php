<?php
/**
 * Class PaymentProfiles_View_PaymentProfileView
 */
class PaymentProfiles_View_BillingDataView
{
	public $firstName;
	public $lastName;
	public $city;
	public $stateId;
	public $stateName;
	public $stateCode;
	public $province;
	public $countryId;
	public $countryName;
	public $countryIso2;
	public $countryIso3;
	public $countryIsoNumber;
	public $zip;

	public $phone;
	public $ccNumber;
	public $ccExpirationMonth;
	public $ccExpirationYear;
	public $ccExpirationDate;
	public $ccType;

	public $name;

	/**
	 * Class constructor
	 *
	 * @param PaymentProfiles_Model_BillingData $billingData
	 */
	public function __construct(PaymentProfiles_Model_BillingData $billingData)
	{
		/** @var Model_Address $address */
		$address = $billingData->getAddress();

		$this->firstName = $address->getFirstName();
		$this->lastName = $address->getLastName();
		$this->addressLine1 = $address->getAddressLine1();
		$this->addressLine2 = $address->getAddressLine2();
		$this->city = $address->getCity();
		$this->stateId = $address->getStateId();
		$this->stateName = $address->getStateName();
		$this->stateCode = $address->getStateCode();
		$this->province = $address->getProvince();
		$this->countryId = $address->getCountryId();
		$this->countryName = $address->getCountryName();
		$this->countryIso2 = $address->getCountryIso2();
		$this->countryIso3 = $address->getCountryIso3();
		$this->countryIsoNumber = $address->getCountryIsoNumber();
		$this->zip = $address->getZip();
		$this->phone = $billingData->getPhone();
		$this->ccNumber = $billingData->maskCc($billingData->getCcNumber());
		$this->ccExpirationMonth = $billingData->getCcExpirationMonth();
		$this->ccExpirationYear = $billingData->getCcExpirationYear();
		$this->ccExpirationDate = $billingData->getCcExpirationDate();
		$this->ccType = $billingData->getCcType();

		$this->name = $billingData->getName();
	}
}
<?php 

define("ERR_MANUFACTURER_CODE_EXISTS", 2001);
define("ERR_MANUFACTURER_IMAGE_UPLOAD", 2002);

/**
 * @class Manufacturers
 */
class Manufacturers
{
	public $db = false;
	public $settings = null;
	public $is_error = false;
	public $error_code = array();
	
	//constructor
	function Manufacturers($db, $settings)
	{
		$this->db = $db;
		$this->settings = $settings;
		return $this;
	}
	
	//insert a new manufacturer into database
	function insertManufacturer($manufacturer)
	{
		//check manufacturer code
		$this->db->query("SELECT * FROM ".DB_PREFIX."manufacturers WHERE manufacturer_code LIKE '".$this->db->escape($manufacturer["manufacturer_code"])."'");
		if ($this->db->moveNext())
		{
			$this->is_error = true;
			$this->error_code[] = ERR_MANUFACTURER_CODE_EXISTS;
			$manufacturer["manufacturer_code"] = substr(md5(uniqid(rand(), true)), rand(1, 16), 12);
		}
		$this->db->reset();
		$this->db->assignStr("is_visible", array_key_exists("is_visible", $manufacturer) && $manufacturer['is_visible'] ? 1 : 0);
		$this->db->assignStr("manufacturer_code", $manufacturer["manufacturer_code"]);
		$this->db->assignStr("manufacturer_name", $manufacturer["manufacturer_name"]);
		$this->db->assignStr("url_custom", $manufacturer["manufacturer_url"]);
		$manufacturer_id = $this->db->insert(DB_PREFIX."manufacturers");
		$this->uploadManufacturerImage($manufacturer_id);
		return $manufacturer_id;
	}
	
	//update manufacturer info
	function updateManufacturer($manufacturer, $manufacturer_id)
	{
		//check manufacturer code
		$this->db->query("SELECT * FROM ".DB_PREFIX."manufacturers WHERE manufacturer_code LIKE '".$this->db->escape($manufacturer["manufacturer_code"])."' AND manufacturer_id <> '".$this->db->escape($manufacturer_id)."'");
		if ($this->db->moveNext())
		{
			$this->is_error = true;
			$this->error_code[] = ERR_MANUFACTURER_CODE_EXISTS;
			$manufacturer["manufacturer_code"] = substr(md5(uniqid(rand(), true)), rand(1, 16), 12);
		}
		$this->db->reset();
		$this->db->assignStr("is_visible", array_key_exists("is_visible", $manufacturer) && $manufacturer['is_visible'] ? 1 : 0);
		$this->db->assignStr("manufacturer_code", $manufacturer["manufacturer_code"]);
		$this->db->assignStr("manufacturer_name", $manufacturer["manufacturer_name"]);
		$this->db->assignStr("url_custom", $manufacturer["manufacturer_url"]);
		$this->db->update(DB_PREFIX."manufacturers", "WHERE manufacturer_id='".$this->db->escape($manufacturer_id)."'");
		return $manufacturer_id;
	}
	
	//bulk update manufacturers info
	function updateManufacturers($data)
	{
		// TODO - not very sade way, replace with $data['..'
		extract($data);
		foreach ($manufacturer_name as $manufacturer_id=>$m_name)
		{
			$manufacturer = array(
				"manufacturer_code" => $manufacturer_code[$manufacturer_id],
				"manufacturer_name" => $manufacturer_name[$manufacturer_id],
				"manufacturer_url" => $manufacturer_url[$manufacturer_id]
			);
			
			if (isset($is_visible[$manufacturer_id]))
			{
				$manufacturer["is_visible"] = 1;
			}
			
			$this->updateManufacturer($manufacturer, $manufacturer_id);
			$this->uploadManufacturerImage($manufacturer_id, "_".$manufacturer_id);
		}
	}
	
	
	//upload manufacturer image and generate thumb
	function uploadManufacturerImage($manufacturer_id, $logo_suffix = "")
	{
		
		if (!empty($_FILES["manufacturer_logo".$logo_suffix]["name"]) && is_file($_FILES["manufacturer_logo".$logo_suffix]["tmp_name"]))
		{
			$is = getimagesize($_FILES['manufacturer_logo'.$logo_suffix]["tmp_name"]);
			if ($is && ($is[2] == 1 || $is[2] == 2 || $is[2] == 3))
			{
				$this->deleteManufacturerImage($manufacturer_id);
				$img_type = array(1=>"gif", 2=>"jpg", 3=>"png");
				$img_full_path = "images/manufacturers/".$manufacturer_id.".".$img_type[$is[2]];
				
				if (@move_uploaded_file($_FILES['manufacturer_logo'.$logo_suffix]["tmp_name"], $img_full_path))
				{
					ImageUtility::generateManufacturerImage($img_full_path, $img_full_path);
					chmod($img_full_path, FileUtils::getFilePermissionMode());
					return true;
				}
			}
			$this->is_error = true;
			$this->error_code[] = ERR_MANUFACTURER_IMAGE_UPLOAD;
			return false;
		}
		return true;
	}
	
	//delete manufacturer image
	function deleteManufacturerImage($manufacturer_id)
	{
		@unlink("images/manufacturers/".$manufacturer_id.".jpg");
		@unlink("images/manufacturers/".$manufacturer_id.".gif");
		@unlink("images/manufacturers/".$manufacturer_id.".png");
	}
	
	//get manufacturer image
	function getManufacturerImageById($manufacturer_id)
	{
		$prefix = "images/manufacturers/".$manufacturer_id.".";
		$prefix2 = "images/manufacturers/thumbs/".$manufacturer_id.".";
		$img_file = false;
		$img_file2 = false;
		$data = array();

		if (file_exists($prefix."png")) $img_file = $prefix."png";
		if (file_exists($prefix."gif")) $img_file = $prefix."gif";
		if (file_exists($prefix."jpg")) $img_file = $prefix."jpg";

		if (file_exists($prefix2."png")) $img_file2 = $prefix2."png";
		if (file_exists($prefix2."gif")) $img_file2 = $prefix2."gif";
		if (file_exists($prefix2."jpg")) $img_file2 = $prefix2."jpg";
		if ($img_file)
		{
			$data = array("file"=>$img_file, "info"=>getimagesize($img_file));
			if ($img_file2)
			{
				$data = array_merge($data, array("thumb_file"=>$img_file2, "thumb_info"=>getimagesize($img_file2)));
			}
			return $data;
		}
		return false;
	}
	
	//get manufacturer by id
	function getManufacturerById($manufacturer_id, $for_site = false)
	{
		$this->db->query("
			SELECT *, IF(url_custom='', url_default, url_custom) AS url FROM ".DB_PREFIX."manufacturers 
			WHERE manufacturer_id='".$manufacturer_id."'".($for_site?"AND ".DB_PREFIX."manufacturers.is_visible=1":"")."
		");
		if ($this->db->moveNext())
		{
			$img_file = $this->getManufacturerImageById($manufacturer_id);
			$data =  $this->db->col;
			$data["img"] = $img_file["file"];
			$data["thumb"] = $img_file["thumb_file"];
			$data["url"] = UrlUtils::getManufacturerUrl($data["url"], $manufacturer_id);
			return $data;
		}
		else
		{
			return false;
		}
	}
	
	//delete manufacturer
	function deleteManufacturer($manufacturer_id)
	{
		$this->deleteManufacturerImage($manufacturer_id);
		$this->db->query("DELETE FROM ".DB_PREFIX."manufacturers WHERE manufacturer_id='".$manufacturer_id."'");
	}
	
	//getManufacturers
	function getManufacturers($for_site = false)
	{
		$this->db->query("SELECT *, IF(url_custom='', url_default, url_custom) AS url FROM ".DB_PREFIX."manufacturers ".($for_site?" WHERE is_visible=1 ":"")." ORDER BY manufacturer_name");
		if ($this->db->numRows() > 0)
		{
			$list = $this->db->getRecords();
			for ($i=0; $i<count($list); $i++)
			{
				$list[$i]["url"] = UrlUtils::getManufacturerUrl($list[$i]["url"], $list[$i]["manufacturer_id"]);
				$list[$i]["image"] = $this->getManufacturerImageById($list[$i]["manufacturer_id"]);
			}
			return $list;
		}
		return false;
	}
	
	//getManufacturersPaginated
	function getManufacturersPaginated($page, $items_on_page)
	{
		$this->db->query(
			"SELECT *, IF(url_custom='', url_default, url_custom) AS url ".
			"FROM ".DB_PREFIX."manufacturers ".
			"ORDER BY manufacturer_name, manufacturer_id ".
			"LIMIT ".(($page-1)* $items_on_page).", ".intval($items_on_page));
		
		if ($this->db->numRows() > 0)
		{
			$list = $this->db->getRecords();
			for ($i=0; $i<count($list); $i++)
			{
				$list[$i]["url"] = Seo::getManufacturerSEOUrl($list[$i]["url"], $list[$i]["manufacturer_id"]);
				$list[$i]["image"] = $this->getManufacturerImageById($list[$i]["manufacturer_id"]);
			}
			return $list;
		}
		return false;
	}
	
	//getManufacturersCount
	function getManufacturersCount($for_site = false)
	{
		$this->db->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."manufacturers ".($for_site?" WHERE is_visible=1 ":""));
		if ($this->db->moveNext())
		{
			return $this->db->col["c"];
		}
		return false;
	}
	
	//search manufacturers
	function searchManufacturers($params)
	{
		$query = "SELECT manufacturer_id, is_visible, manufacturer_code, manufacturer_name, manufacturer_website FROM ".DB_PREFIX."manufacturers ";
		$where = "";
		if (isset($params["manufacturer_name"]) && (trim($params["manufacturer_name"]) != ""))
		{
			$where.=($where!=""?" AND ":"")."manufacturers.manufacturer_name LIKE '%".$this->db->escape($params["manufacturer_name"])."%' ";
		}
		if (isset($params["manufacturer_code"]) && (trim($params["manufacturer_code"]) != ""))
		{
			$where.=($where!=""?" AND ":"")."manufacturers.manufacturer_code LIKE '%".$this->db->escape($params["manufacturer_code"])."%' ";
		}
		
		$order = isset($params["order"])?trim($params["order"]):"name_a";
		$order = in_array($order, array("name_a", "name_d", "code_a", "code_d")) ? $order : "name_a";
		switch($order)
		{
			case "name_a" : $order_by = DB_PREFIX."manufacturers.manufacturer_name, manufacturers.manufacturer_code"; break;
			case "name_d" : $order_by = DB_PREFIX."manufacturers.manufacturer_name DESC, manufacturers.manufacturer_code"; break;
			case "code_a" : $order_by = DB_PREFIX."manufacturers.manufacturer_code, manufacturers.manufacturer_name"; break;
			case "code_d" : $order_by = DB_PREFIX."manufacturers.manufacturer_code DESC, manufacturers.manufacturer_name"; break;
		}
		
		if ($where != "")
		{
			$query = "SELECT * FROM ".DB_PREFIX."manufacturers WHERE ".$where." ORDER BY ".$order_by;
			$this->db->query($query);
			return $this->db->numRows() > 0 ? $this->db->getRecords() : false;
		}
		else
		{
			$query = "SELECT * FROM ".DB_PREFIX."manufacturers ORDER BY ".$order_by;
			$this->db->query($query);
			return $this->db->numRows() > 0 ? $this->db->getRecords() : false;
		}
	}
}

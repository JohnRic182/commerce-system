<?php

/**
* 
*/
class Seo
{
	/**
	 * Generates the .htaccess file
	 *
	 * @throws Exception On error when creating the .htaccess file
	 * @return void
	 * @author Sebi
	 */
	public static function generateHtaccessFile($rewrite_mode = 'Off')
	{
		global $settings;
		
		if (!in_array($rewrite_mode, array('Off', 'On'))) $rewrite_mode = 'Off';
		
		@unlink(".htaccess.back");
		@copy(".htaccess", ".htaccess.back");
		$f = @fopen(".htaccess", "w");
		
		if (!$f)
		{
			throw new Exception('Could not open the .htaccess file for writing', 100);
		}
		else
		{
			$pu = parse_url("http://foo.com/".$_SERVER["REQUEST_URI"]);
			$pi = pathinfo($pu["path"]);
			
			$rewriteBase = str_replace('\\', '/', $pi["dirname"]);
			
			if (defined("APP_INSTALL") && APP_INSTALL)
			{
				$rewriteBase = str_replace("/install", "", $rewriteBase);
			}
			
			$cond = '\#(.)*|\?(.)*';
			$d = dir($settings["GlobalServerPath"]);
			
			while (false !== ($entry = $d->read()))
			{
				if (!in_array($entry, array(".", "..")))
				{
	   				if (is_file($entry))
					{
						$cond .= ($cond == "" ? "" : "|").(preg_quote($entry)."(.)*");
					}
					elseif (is_dir($entry))
					{
						$cond .= ($cond == "" ? "" : "|").($entry."\\/(.)*");
					}
				}
			}
			
			$cond = str_replace(' ', '\ ', $cond);
			
			$d->close();
			
			$htaccess = 
				"<IfModule mod_headers.c>\n".
				"\tHeader unset ETag\n".
				"\tFileETag None\n".
				"\t<FilesMatch \"\.(ico|flv|jpg|jpeg|png|gif|js|css)$\">\n".
				"\t\tHeader unset Last-Modified\n".
				"\t\tHeader set Expires \"Fri, 21 Dec 2020 00:00:00 GMT\"\n".
				"\t\tHeader set Cache-Control \"public, no-transform\"\n".
				"\t</FilesMatch>\n".
				"</IfModule>\n\n".
                 
				"<IfModule mod_rewrite.c>\n".
				"\tRewriteEngine\t".$rewrite_mode."\n".
				"\tRewriteBase\t".str_replace("//", "/", $rewriteBase)."\n\n";
			
			$www_preference = '';
			if (isset($settings['seo_www_preference']))
			{
				$url_parsed = parse_url($settings['GlobalHttpUrl']);
				$host_no_www = $url_parsed['host'];
				if (preg_match('/^www\.(.+)/i', $host_no_www, $m))
				{
					$host_no_www = $m[1];
				}

				if ($settings['seo_www_preference'] == 'www')
				{
					$www_preference = 	"\tRewriteCond\t%{HTTP_HOST} !^www\." . $host_no_www . "$ [NC]\n".
								(strtolower($url_parsed['scheme']) == 'https' ?
								"\tRewriteCond\t%{HTTPS} on\n" : "\tRewriteCond\t%{HTTPS} off\n").
								"\tRewriteCond\t%{REQUEST_URI} !^/?(admin|login).php(.*)$ [NC]\n".
								"\tRewriteRule\t^(.+)\$	" . $url_parsed['scheme'] . "://www." . $host_no_www . "%{REQUEST_URI} [R=301,L]\n\n";
				}
				else if ($settings['seo_www_preference'] == 'nonwww')
				{
					$www_preference = 	"\tRewriteCond\t%{HTTP_HOST} ^www\." . $host_no_www . "$ [NC]\n".
								(strtolower($url_parsed['scheme']) == 'https' ?
								"\tRewriteCond\t%{HTTPS} on\n" : "\tRewriteCond\t%{HTTPS} off\n").
								"\tRewriteCond\t%{REQUEST_URI} !^/?(admin|login).php(.*)$ [NC]\n".
								"\tRewriteRule\t^(.+)\$	" . $url_parsed['scheme'] . "://" . $host_no_www . "%{REQUEST_URI} [R=301,L]\n\n";
				}
			}
			$htaccess .= $www_preference;

			if (isset($settings['SearchHtaccessOverrides']) and trim($settings['SearchHtaccessOverrides']) != '')
			{
				$htaccess .= $settings['SearchHtaccessOverrides']."\n\n";
			}

			$htaccess .= "\tRewriteCond\t%{QUERY_STRING} ^\$\n". 
				"\tRewriteRule\t^((.)?)\$\tindex.php?p=home [L]\n\n".

				"\tRewriteCond\t%{REQUEST_FILENAME} -f\n".
				"\tRewriteRule\t^(.*)$ $1 [QSA,L]\n\n".

				"\tRewriteRule ^admin$ admin.php [L,R]\n\n".

				"\tRewriteCond\t$1 !^(".$cond.")\n".
				"\tRewriteRule\t^(.+)$ index.php?url=$1&%{QUERY_STRING} [L]\n".
				"</IfModule>\n\n".
			
				"<IfModule mod_deflate.c>\n".
				"\tSetOutputFilter DEFLATE\n".
				"</IfModule>\n\n";

			if (!@fputs($f, $htaccess, strlen($htaccess)))
			{
				throw new Exception('Could not write to the .htaccess file', 200);
			}
			fclose($f);
		}
	}
	
	/**
	 * Generate unique URL's for products and categories that may have duplicates
	 * @return integer Number of rows updated
	 */
	public static function generateUniqueUrls()
	{
		db()->reset();
		// Figure our which url_hashes have duplicates
		$sql = '
			SELECT SUM(1) AS totalcount, url_hash 
			FROM 
			(
				(SELECT url_hash FROM '.DB_PREFIX.'products)
				UNION ALL
				(SELECT url_hash FROM '.DB_PREFIX.'catalog)
				UNION ALL
				(SELECT url_hash FROM '.DB_PREFIX.'pages)
				UNION ALL
				(SELECT url_hash FROM '.DB_PREFIX.'manufacturers)
			) AS url_tables 
			GROUP BY url_hash HAVING totalcount > 1';
		
		db()->query($sql);
		
		// If we don't have any results, there aren't any duplicates to process
		if (db()->numRows() == 0) return 0;
		
		// Store the url hashes in an array to implode later
		$url_hashes = array();
		while (db()->moveNext())
		{
			$url_hashes[db()->col['url_hash']] = db()->col['url_hash'];
		}
				
		// A map of products and categories and their respective urls
		$url_map = array();
		
		// Compile a list of potential candidates based on the keywords that may conflict later for url suggestions
		$candidates = array();
		
		// Query all rows that had a duplicate by searching the url_hash
		$sql = '
			(SELECT pid as id, "products" as type, url_hash, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'products WHERE url_hash IN ("'.implode('","', $url_hashes).'"))
			UNION
			(SELECT cid as id, "catalog" as type, url_hash, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'catalog WHERE url_hash IN ("'.implode('","', $url_hashes).'"))
			UNION
			(SELECT manufacturer_id as id, "manufacturers" as type, url_hash, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'manufacturers WHERE url_hash IN ("'.implode('","', $url_hashes).'"))		
			UNION
			(SELECT pid as id, "pages" as type, url_hash, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'pages WHERE url_hash IN ("'.implode('","', $url_hashes).'"))
			ORDER BY url_hash, id';
		db()->query($sql);
		
		// Create our maps and add to conflicts array
		while (db()->moveNext())
		{
			$url_map[db()->col['type']][db()->col['id']] = db()->col['url'];
			$candidates[] = '^'.preg_replace('/\/?$|\.[a-z]+?$/','', db()->col['url']).'-[a-z]+([\./]?|\.[a-z]+)';
		}

		$candidates_chunks = array_chunk(array_unique($candidates), 100);

		$conflicts = array();
		
		foreach ($candidates_chunks as $candidates)
		{
			// Search for both products and categories
			$sql = '
				(SELECT pid as id, "products" as type, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'products WHERE (url_custom = "" AND url_default REGEXP "'.implode('|', $candidates).'") OR (url_custom REGEXP "'.implode('|', $candidates).'"))
				UNION
				(SELECT cid as id, "catalog" as type, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'catalog WHERE (url_custom = "" AND url_default REGEXP "'.implode('|', $candidates).'") OR (url_custom REGEXP "'.implode('|', $candidates).'"))
				UNION
				(SELECT manufacturer_id as id, "manufacturers" as type, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'manufacturers WHERE (url_custom = "" AND url_default REGEXP "'.implode('|', $candidates).'") OR (url_custom REGEXP "'.implode('|', $candidates).'"))		
				UNION
				(SELECT pid as id, "pages" as type, IF(url_custom="",url_default,url_custom) AS url FROM '.DB_PREFIX.'pages WHERE (url_custom = "" AND url_default REGEXP "'.implode('|', $candidates).'") OR (url_custom REGEXP "'.implode('|', $candidates).'"))
			';
			db()->query($sql);
				
			// store each potential conflict
			while (db()->moveNext())
			{
				$conflicts[db()->col['url']] = array('id' => db()->col['id'], 'type' => db()->col['type'], 'update' => false);
			}
		}
		
		// An array of suggestions by type (product/category) and indexed by their native id (pid/cid)
		$suggestions = array();
		
		// url suffix
		$suffix = '-a';
		
		// sort the url_map manually so products have highest precedence
		$url_map = array(
			'products' => isset($url_map['products']) ? $url_map['products'] : array(),
			'catalog' => isset($url_map['catalog']) ? $url_map['catalog'] : array(),
			'manufacturers' => isset($url_map['manufacturers']) ? $url_map['manufacturers'] : array(),
			'pages' => isset($url_map['pages']) ? $url_map['pages'] : array()
		);
		
		// loop through each type (product/category)
		foreach ($url_map as $type => $data)
		{
			$current_url = null;
			
			if (empty($data)) continue;
			
			// loop through each duplicate entry
			foreach ($data as $key => $url)
			{
				
				if (is_null($current_url) || $url != $current_url)
				{
					$suffix = '-a';
				}
				
				// if (in_array($url, array_keys($conflicts)) && $conflicts[$url]['id'] == $key) continue;
				
				if (!in_array($url, array_keys($conflicts)) || (in_array($url, array_keys($conflicts)) && $conflicts[$url]['id'] == $key))
				{
					$suggestion = $url;
				}
				else
				{
					do
					{
						if ($suffix == '-z')
						{
							$suffix = '-aa';
						} elseif ($suffix == '-zz')
						{
							$suffix = '-aaa';
						}
						$suggestion = self::getUrlSuggestion($url, $suffix);
						$suffix++;
					}
					while (in_array($suggestion, array_keys($conflicts)));
				}
				
				$conflicts[$suggestion] = array(
					'id' => $key,
					'type' => $type,
					'update' => true
				);
				
				$current_url = $url;
			}
		}
		
		
		
		// Update each record with its suggestion
		$total_updated = 0;
		
		foreach ($conflicts as $url => $data)
		{
			if ($data['update'] === true)
			{
				$id = $data['id'];
				$type = $data['type'];
				
				switch ($type)
				{
					case 'catalog': $key_name = 'cid'; break;
					case 'manufacturers': $key_name = 'manufacturer_id'; break;
					default: $key_name = 'pid';
				}
				
				db()->reset();
				db()->assignStr('url_hash', md5($url));
				db()->assignStr('url_custom', $url);
				db()->update(DB_PREFIX.$type, "WHERE {$key_name} = {$id} AND url_default != '{$url}' LIMIT 1");
				$total_updated++;
			}
		}
		
		return $total_updated;
	}
	
	/**
	 * Gets a URL string as a suggestion for the given URL and suffix
	 *
	 * @param string $url The URL that will be transformed
	 * @param string $suffix The suffix that wlll be appended to the given $url
	 * @return string The suggested URL string
	 */
	private static function getUrlSuggestion($url, $suffix)
	{
		return preg_replace('/(\/?$|\.[a-z]+?$)/',$suffix.'$1', $url, 1);
	}

	/**
	 * Updates SEO urls
	 *
	 * @param $db
	 * @param $settings
	 * @param bool $categories
	 * @param bool $products
	 * @param bool $pages
	 * @param bool $manufacturers
	 */
	public static function updateSeoURLs($db, $settings, $categories=true, $products=true, $pages=true, $manufacturers=true, $orderForms=true)
	{
		set_time_limit(10000);

		if ($categories || $products)
		{
			$categoryRepository = new DataAccess_CategoryRepository($db);
			$categoryRepository->updateCategoriesPath();
		}

		//category template changed
		if ($categories)
		{
			//render URL
			$db->query("UPDATE ".DB_PREFIX."catalog SET url_default='".$db->escape($settings["SearchURLCategoryTemplate"])."'");

			$query = self::setCategoryUrlSQL($settings["SearchAutoGenerateLowercase"] == "YES", $settings["SearchURLJoiner"]);
			$db->query($query);

			//update hash
			$db->query("UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."catalog SET url_hash=MD5(IF(url_custom='', url_default, url_custom))");
		}

		//product template changed
		if ($products)
		{
			//render url
			$db->query("UPDATE ".DB_PREFIX."products SET url_default='".$db->escape($settings["SearchURLProductTemplate"])."'");

			$query = self::setProductUrlSQL($settings["SearchAutoGenerateLowercase"] == "YES", $settings["SearchURLJoiner"]);
			$db->query($query);

			//remove possible trailing spaces
			//$db->query("UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."products SET url_default=REPLACE('//', '/', REPLACE('///', '/', url_default));");

			//update hash
			$db->query("UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."products SET url_hash=MD5(IF(url_custom='', url_default, url_custom))");
		}

		//manufacturers
		if ($manufacturers)
		{
			$db->query("UPDATE ".DB_PREFIX."manufacturers SET url_default='".$db->escape($settings["SearchURLManufacturerTemplate"])."'");

			$query = self::setManufacturerUrlSQL($settings['SearchAutoGenerateLowercase'] == 'YES', $settings['SearchURLJoiner']);
			$db->query($query);

			if ($duplicates = $db->selectAll("
				SELECT m.manufacturer_id
				FROM " . DB_PREFIX . "manufacturers m
				INNER JOIN " . DB_PREFIX . "catalog c ON m.url_default = c.url_default
			"))
			{
				$duplicate_ids = array();
				foreach ($duplicates as $dup)
				{
					$duplicate_ids[] = $dup['manufacturer_id'];
				}

				$url_template = $settings['SearchURLManufacturerTemplate'];
				$add_slash = false;
				if (preg_match('/^(.+)\/$/', $url_template, $m))
				{
					$url_template = $m[1];
					$add_slash = true;
				}

				$db->query("
					UPDATE ".DB_PREFIX."manufacturers
					SET url_default= CONCAT('".$db->escape($url_template)."', '-', manufacturer_id, '" . ($add_slash ? '/' : '') . "')
					WHERE manufacturer_id IN (" . join(',', $duplicate_ids) . ")
				");
				$query = self::setManufacturerUrlSQL($settings['SearchAutoGenerateLowercase'] == 'YES', $settings['SearchURLJoiner'],
					"manufacturer_id IN (" . join(',', $duplicate_ids) . ")"
				);
				$db->query($query);
			}

			//update hash
			$db->query("UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."manufacturers SET url_hash=MD5(IF(url_custom='', url_default, url_custom))");
		}

		//page template changed
		if ($pages)
		{
			//render url
			$db->query("UPDATE ".DB_PREFIX."pages SET url_default='".$db->escape($settings["SearchURLPageTemplate"])."'");

			$query = self::setPageUrlSQL($settings["SearchAutoGenerateLowercase"] == "YES", $settings["SearchURLJoiner"]);
			$db->query($query);

			//update hash
			$db->query("UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."pages SET url_hash=MD5(IF(url_custom='', url_default, url_custom))");
			$db->query("UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."pages SET name = IF(url_custom='', url_default, url_custom)");
		}

		//order forms
		if ($orderForms)
		{
			$db->query("UPDATE ".DB_PREFIX."order_forms SET url_default='".$db->escape($settings["SearchURLOrderFormTemplate"])."'");

			$query = self::setOrderFormUrlSQL($settings['SearchAutoGenerateLowercase'] == 'YES', $settings['SearchURLJoiner']);

			$db->query($query);

			if ($duplicates = $db->selectAll("
				SELECT m.ofid
				FROM " . DB_PREFIX . "order_forms m
				INNER JOIN " . DB_PREFIX . "catalog c ON m.url_default = c.url_default
			"))
			{
				$duplicate_ids = array();
				foreach ($duplicates as $dup)
				{
					$duplicate_ids[] = $dup['ofid'];
				}

				$url_template = $settings['SearchURLOrderFormTemplate'];
				$add_slash = false;
				if (preg_match('/^(.+)\/$/', $url_template, $m))
				{
					$url_template = $m[1];
					$add_slash = true;
				}

				$db->query("
					UPDATE ".DB_PREFIX."order_forms
					SET url_default= CONCAT('".$db->escape($url_template)."', '-', ofid, '" . ($add_slash ? '/' : '') . "')
					WHERE ofid IN (" . join(',', $duplicate_ids) . ")
				");
				$query = self::setOrderFormUrlSQL($settings['SearchAutoGenerateLowercase'] == 'YES', $settings['SearchURLJoiner'],
					"ofid IN (" . join(',', $duplicate_ids) . ")"
				);
				$db->query($query);
			}

			//update hash
			$db->query("UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."order_forms SET url_hash=MD5(IF(url_custom='', url_default, url_custom))");
		}
	}

	/**
	 * Returns category URL SQL
	 * @param $template
	 * @return string
	 */
	public static function setCategoryUrlSQL($lowerCase = false, $joiner = "-", $where = false)
	{
		//lowerCase does not work - MySQL issue - http://bugs.mysql.com/bug.php?id=12903
		$joiner = db()->escape($joiner);

		$r = self::getMySQLReplace(
			array(
				"'_'","'-'","'%CategoryName%'", "'%CategoryKey%'", "'%CategoryDbId%'", "'%CategoryPath%'",
				"'~'", "'`'", "'!'", "'@'", "'#'", "'$'", "'%'", "'^'", "'&'", "'*'", "'™'",
				"'®'", "'℠'", "'₤'", "'£'", "'¥'", "'€'",
				"'('", "')'", "' '", "'_'", "'-'", "'+'", "'='", "'{'", "'}'", "'['",
				"']'", "':'", "';'", "'\"'", "'\\''", "'<'", "'>'", "','", "'?'",
				"'{$joiner}{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}'",
				"'./'", "'/.'", "'-/'", "'/-'", "'///'", "'//'", "'777underscore777'", "'777dash777'"
			),
			array(
				"'777underscore777'","'777dash777'","c.name", "c.key_name", "c.cid", "c.category_path",
				"'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "''", "''",
				"''", "''", "''", "''", "''", "''",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'/'", "'/'", "'/'", "'/'", "'/'", "'/'", "'_'", "'-'"
			),
			"c.url_default"
		);
		return "UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."catalog AS c
				SET c.url_default = ".($lowerCase?"LCASE(":"")."TRIM(BOTH '".$joiner."' FROM ".$r.")".($lowerCase?")":"")."
				WHERE c.cid > 0 ".($where ? (" AND (".$where.")") : "");
	}


	/**
	 * Returns category URL SQL
	 * @param $template
	 * @return string
	 */
	public static function setManufacturerUrlSQL($lowerCase = false, $joiner = "-", $where = false)
	{
		//lowerCase does not work - MySQL issue - http://bugs.mysql.com/bug.php?id=12903
		$joiner = db()->escape($joiner);

		$r = self::getMySQLReplace(
			array(
				"'_'","'-'","'%ManufacturerName%'", "'%ManufacturerKey%'", "'%ManufacturerDbId%'",
				"'~'", "'`'", "'!'", "'@'", "'#'", "'$'", "'%'", "'^'", "'&'", "'*'", "'™'",
				"'®'", "'℠'", "'₤'", "'£'", "'¥'", "'€'",
				"'('", "')'", "' '", "'_'", "'-'", "'+'", "'='", "'{'", "'}'", "'['",
				"']'", "':'", "';'", "'\"'", "'\\''", "'<'", "'>'", "','", "'?'",
				"'{$joiner}{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}'",
				"'./'", "'/.'", "'-/'", "'/-'", "'///'", "'//'", "'777underscore777'", "'777dash777'"
			),
			array(
				"'777underscore777'","'777dash777'","c.manufacturer_name", "c.manufacturer_code", "c.manufacturer_id",
				"'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "''", "''",
				"''", "''", "''", "''", "''", "''",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'/'", "'/'", "'/'", "'/'", "'/'", "'/'", "'_'", "'-'"
			),
			"c.url_default"
		);
		return "UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."manufacturers AS c
				SET c.url_default = ".($lowerCase?"LCASE(":"")."TRIM(BOTH '".$joiner."' FROM ".$r.")".($lowerCase?")":"")."
				WHERE c.manufacturer_id > 0 ".($where ? (" AND (".$where.")") : "");
	}

	/**
	 * Returns order form URL SQL
	 * @param $template
	 * @return string
	 */
	public static function setOrderFormUrlSQL($lowerCase = false, $joiner = "-", $where = false)
	{
		//lowerCase does not work - MySQL issue - http://bugs.mysql.com/bug.php?id=12903
		$joiner = db()->escape($joiner);

		$r = self::getMySQLReplace(
			array(
				"'_'","'-'","'%OrderFormName%'", "'%OrderFormDbId%'",
				"'~'", "'`'", "'!'", "'@'", "'#'", "'$'", "'%'", "'^'", "'&'", "'*'", "'™'",
				"'®'", "'℠'", "'₤'", "'£'", "'¥'", "'€'",
				"'('", "')'", "' '", "'_'", "'-'", "'+'", "'='", "'{'", "'}'", "'['",
				"']'", "':'", "';'", "'\"'", "'\\''", "'<'", "'>'", "','", "'?'",
				"'{$joiner}{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}'",
				"'./'", "'/.'", "'-/'", "'/-'", "'///'", "'//'", "'777underscore777'", "'777dash777'"
			),
				array(
				"'777underscore777'","'777dash777'","c.name", "c.ofid",
				"'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "''", "''",
				"''", "''", "''", "''", "''", "''",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'/'", "'/'", "'/'", "'/'", "'/'", "'/'", "'_'", "'-'"
			),
			"c.url_default"
		);

		return "UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."order_forms AS c
				SET c.url_default = ".($lowerCase?"LCASE(":"")."TRIM(BOTH '".$joiner."' FROM ".$r.")".($lowerCase?")":"")."
				WHERE c.ofid > 0 ".($where ? (" AND (".$where.")") : "");
	}

	/**
	 * Returns product URL SQL
	 * @param $template
	 * @return string
	 */
	public static function setProductUrlSQL($lowerCase = false, $joiner = "-", $where = false)
	{
		//lowerCase does not work - MySQL issue - http://bugs.mysql.com/bug.php?id=12903
		$joiner = db()->escape($joiner);

		$r = self::getMySQLReplace(
			array(
				"'_'","'-'","'%ProductId%'", "'%ProductName%'", "'%ProductDbId%'",
				"'%CategoryName%'", "'%CategoryKey%'", "'%CategoryDbId%'", "'%CategoryPath%'", "'%CategoryURL%'",
				"'~'", "'`'", "'!'", "'@'", "'#'", "'$'", "'%'", "'^'", "'&'", "'*'", "'™'",
				"'®'", "'℠'", "'₤'", "'£'", "'¥'", "'€'",
				"'('", "')'", "' '", "'_'", "'-'", "'+'", "'='", "'{'", "'}'", "'['",
				"']'", "':'", "';'", "'\"'", "'\\''", "'<'", "'>'", "','", "'?'",
				"'{$joiner}{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}'",
				"'./'", "'/.'", "'-/'", "'/-'", "'///'", "'//'", "'777underscore777'", "'777dash777'"
			),
			array(
				"'777underscore777'","'777dash777'","p.product_id", "p.title", "p.pid",
				"IF(c.url_custom='', c.name, c.url_custom)", "c.key_name", "c.cid", "c.category_path", "IF(c.url_custom='', c.url_default, c.url_custom)",
				"'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "''", "''",
				"''", "''", "''", "''", "''", "''",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'/'", "'/'", "'/'", "'/'", "'/'", "'/'", "'_'", "'-'"
			),
			"p.url_default"
		);

		return "UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."products AS p, ".DB_PREFIX."catalog AS c
				SET p.url_default = ".($lowerCase?"LCASE(":"")."TRIM(BOTH '".$joiner."' FROM ".$r.")".($lowerCase?")":"")."
				WHERE p.cid = c.cid ".($where ? (" AND (".$where.")") : "");
	}

	/**
	 * Returns page URL SQL
	 *
	 * @param bool $lowerCase
	 * @param string $joiner
	 * @param bool $where
	 *
	 * @return string
	 */
	public static function setPageUrlSQL($lowerCase = false, $joiner = "-", $where = false)
	{
		//lowerCase does not work - MySQL issue - http://bugs.mysql.com/bug.php?id=12903
		$joiner = db()->escape($joiner);

		$r = self::getMySQLReplace(
			array(
				"'%PageName%'", "'%PageTitle%'",
				"'~'", "'`'", "'!'", "'@'", "'#'", "'$'", "'%'", "'^'", "'&'", "'*'", "'™'",
				"'®'", "'℠'", "'₤'", "'£'", "'¥'", "'€'",
				"'('", "')'", "' '", "'_'", "'-'", "'+'", "'='", "'{'", "'}'", "'['",
				"']'", "':'", "';'", "'\"'", "'\\''", "'<'", "'>'", "','", "'?'",
				"'{$joiner}{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}{$joiner}'", "'{$joiner}{$joiner}'",
				"'./'", "'/.'", "'-/'", "'/-'"
			),
			array(
				"p.title", "p.title",
				"'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "''", "''",
				"''", "''", "''", "''", "''", "''",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "''", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'{$joiner}'", "'{$joiner}'", "'{$joiner}'", "'{$joiner}'",
				"'/'", "'/'", "'/'", "'/'"
			),
			"p.url_default"
		);

		return "UPDATE LOW_PRIORITY IGNORE ".DB_PREFIX."pages AS p
				SET p.url_default = ".($lowerCase?"LCASE(":"")."TRIM(BOTH '".$joiner."' FROM ".$r.")".($lowerCase?")":"")."
				WHERE p.pid > 0 ".($where ? (" AND (".$where.")") : "");

	}

	/**
	 * @param $url
	 * @param $mid
	 * @param bool $page
	 * @param bool $force_http
	 * @return string
	 */
	public static function getManufacturerSEOUrl($url, $mid, $page = false, $force_http = false)
	{
		global $settings;
		if ($page && intval($page) != 1)
		{
			$pageNum = $settings["SearchURLManufacturerPaginatorTemplate"];
			if (($l = strlen($url)) > 0)
			{
				if ($url[$l-1] == "/")
				{
					$url = substr($url, 0, $l-1).$pageNum."/";
				}
				else
				{
					$p = pathinfo($url);
					if (isset($p['extension']) && $p['extension'] != "")
					{
						$url = substr($url, 0, $l - strlen($p['extension'])-1).$pageNum.".".$p['extension'];
					}
					else
					{
						$url.=$pageNum;
					}
				}
				$url = str_replace("%PageNumber%", $page, $url);
			}
		}

		return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? $settings["GlobalHttpsUrl"] : $settings["GlobalHttpUrl"])."/".$url;
	}

	/**
	 * Generate MySQL replacement SQL
	 * @param $patterns
	 * @param $replacements
	 * @param $subject
	 * @return string
	 */
	public static function getMySQLReplace($patterns, $replacements, $subject)
	{
		$s = $subject;
		for ($i=0; $i<count($patterns); $i++)
		{
			$s = "REPLACE(".$s.", ".$patterns[$i].",".$replacements[$i].")";
		}
		return $s;
	}
}

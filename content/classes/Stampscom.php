<?php

/**
 * Class Stamps.com
 */
class Stampscom
{
	protected $settings;
	protected $_soap_client_type = null;
	protected $_soap_client_obj = null;
	protected $_is_use_authenticator_token = false;
	protected $_is_use_test_server = false;
	protected $_authenticator_token = null;
	protected $_api_return = null;

	protected $_wsdl_file_path = '';
	protected $wsdl_file_path_live = 'content/vendors/stampscom/swsimv25.live.wsdl';
	protected $wsdl_file_path_testing = 'content/vendors/stampscom/swsimv25.testing.wsdl';
	protected $stampscom_integration_id = '59bb823e-5929-403b-906e-8d165d9b4e67';

	protected $accessPurchasePostage = null;
	protected $accessGenerateScanForms = null;
	protected $accessCarrierPickup = null;
	protected $postageBalance = null;
	protected $postageControlTotal = null;

	/**
	 * Stampscom constructor.
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
	}

	/**
	 * @return mixed
	 */
	protected function getUsername()
	{
		return $this->settings->get('StampscomUsername');
	}

	/**
	 * @return mixed
	 */
	protected function getPassword()
	{
		return $this->settings->get('StampscomPassword');
	}

	/**
	 * @return bool
	 */
	protected function isTestMode()
	{
		return defined('DEVMODE') && DEVMODE && $this->settings->get('StampscomTestMode') == 'Yes';
	}

	/**
	 * @return bool
	 */
	public function refreshAccountInformation()
	{
		$this->accessCarrierPickup = true;

		$stamps_return = $this->apiMethodGetAccountInfo();

		if (is_array($stamps_return) && isset($stamps_return['AccountInfo']))
		{
			$this->postageBalance = $stamps_return['AccountInfo']['PostageBalance']['AvailablePostage'];
			$this->postageControlTotal = $stamps_return['AccountInfo']['PostageBalance']['ControlTotal'];

			$this->accessPurchasePostage = $stamps_return['AccountInfo']['Capabilities']['CanPurchasePostage'] == 'true';

			$this->accessGenerateScanForms = $stamps_return['AccountInfo']['Capabilities']['CanCreateSCANForm'] == 'true';
			return true;
		}

		return false;
	}

	/**
	 * @return null
	 */
	public function canPurchasePostage()
	{
		if ($this->accessPurchasePostage === null)
		{
			$this->refreshAccountInformation();
		}

		return $this->accessPurchasePostage;
	}

	/**
	 * @return null
	 */
	public function canGenerateScanForms()
	{
		if ($this->accessPurchasePostage === null)
		{
			$this->refreshAccountInformation();
		}

		return $this->accessPurchasePostage;
	}

	/**
	 * @return null
	 */
	public function canCarrierPickup()
	{
		if ($this->accessCarrierPickup === null)
		{
			$this->refreshAccountInformation();
		}

		return $this->accessCarrierPickup;
	}

	/**
	 * @return null
	 */
	public function getPostageBalance()
	{
		if ($this->postageBalance === null)
		{
			$this->refreshAccountInformation();
		}

		return $this->postageBalance;
	}

	/**
	 * @return null
	 */
	public function getPostageControlTotal()
	{
		if ($this->postageControlTotal === null)
		{
			$this->refreshAccountInformation();
		}

		return $this->postageControlTotal;
	}

	/**
	 * Connect to the API, authenticating as necessary, and call a web method
	 *
	 * @param string $method The name of the web method to call
	 * @param array $_arguments The web method arguments
	 *
	 * @return array
	 */
	public function apiCall($method='AuthenticateUser', $_arguments=array())
	{
		$arguments = $this->_apiCallPrepareArguments($_arguments);
		$this->_api_return = $this->_apiCallClient($method, $arguments);

		$this->log($arguments);
		$this->log($this->_api_return);

		if (isset($this->_api_return['Authenticator']))
		{
			unset($this->_api_return['Authenticator']);
		}

		if (isset($this->_api_return['authenticator']))
		{
			unset($this->_api_return['authenticator']);
		}

		return $this->_api_return;
	}

	/**
	 * Logging
	 *
	 * @param $values
	 */
	protected function log($values)
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			$logPath = dirname(dirname(__FILE__)).'/cache/log';
			@file_put_contents($logPath.'/shipping-stamps.txt', print_r($values,1), FILE_APPEND);
		}
	}

	/**
	 * Connect to the API using a client, authenticating as necessary and call a web method
	 *
	 * @param string $method The name of the web method to call
	 * @param array $arguments The web method arguments
	 * @throws Exception
	 *
	 * @return mixed
	 */
	protected function _apiCallClient($method, $arguments)
	{
		$connection_type = 'soap';
		$soap_client_type = 'nusoap';

		switch ($connection_type)
		{
			case 'soap':
			{
				return $this->_callSoapClient($method, $arguments, $soap_client_type);
			}
			default:
			{
				throw new Exception("Attempted to use unknown connection type '$connection_type'");
			}
		}
	}

	/**
	 * Prepare the API arguments by adding additional information and normalizing other information
	 *
	 * @param array $_arguments The web method arguments
	 *
	 * @return array The modified arguments array
	 */
	protected function _apiCallPrepareArguments($_arguments=array())
	{
		$arguments = array();

		// Deal with the case that namespace is included in the argument for whatever reason
		if (is_array($_arguments))
		{
			foreach ($_arguments as $k => $v)
			{
				if (preg_match('/(?:^|:(?:authenticator|credentials))$/i', $k))
				{
					continue;
				}
				$arguments[$k] = $v;
			}
		}

		$arguments = $this->_apiCallPrepareArgumentsAuth($arguments);

		return $arguments;
	}

	/**
	 * Prepare the API arguments by adding authentication information
	 *
	 * @param array $_arguments The web method arguments
	 * @throws Exception
	 *
	 * @return array The modified arguments array
	 */
	protected function _apiCallPrepareArgumentsAuth($_arguments=array())
	{
		$arguments = array();

		if (is_array($_arguments))
		{
			$arguments = $_arguments;
		}

		if ($this->_is_use_authenticator_token)
		{
			throw new Exception("Using Authenticator tokens for SWSIM authentication is disabled.");
		}
		else
		{
			if (isset($arguments['Authenticator']))
			{
				unset($arguments['Authenticator']);
			}

			$arguments['Credentials'] = array(
				'IntegrationID' => $this->stampscom_integration_id,
				'Username' => $this->getUsername(),
				'Password' => $this->getPassword(),
			);
		}

		return $arguments;
	}

	/**
	 * Call a method using SOAP
	 *
	 * @param string $method The SOAP method to call
	 * @param array $arguments The SOAP method arguments
	 * @param string $soap_client_type
	 * @throws Exception
	 *
	 * @return array The SOAP response array
	 */
	protected function _callSoapClient($method, $arguments, $soap_client_type='nusoap')
	{
		$wsdl_file_path_old = $this->_wsdl_file_path;
		if ($this->isTestMode())
		{
			$this->_wsdl_file_path = $this->wsdl_file_path_testing;
		}
		else
		{
			$this->_wsdl_file_path = $this->wsdl_file_path_live;
		}

		if ($wsdl_file_path_old !== $this->_wsdl_file_path)
		{
			$this->_soap_client_obj = null;
			$this->_soap_client_type = null;
		}

		switch ($soap_client_type)
		{
			case 'nusoap' :
			{
				return $this->_callSoapClientNusoap($method, $arguments);
			}
			case 'phpext' :
			{
				return $this->_callSoapClientPhpext($method, $arguments);
			}
			default :
			{
				throw new Exception("Unknown SOAP client type '$soap_client_type'.");
			}
		}
	}

	/**
	 * Call a method using the SOAP client "nusoap"
	 *
	 * @param string $method The SOAP method to call
	 * @param array $arguments The SOAP method arguments
	 * @throws Exception
	 *
	 * @return array The SOAP response array
	 */
	protected function _callSoapClientNusoap($method, $arguments)
	{
		if (!class_exists('nusoap_client'))
		{
			// This tries to load the class directly since it won't be auto-loaded
			require_once("content/vendors/nusoap/nusoap.php");

			if (!class_exists('nusoap_client'))
			{
				throw new Exception("SOAP class newsoap_client was requested, but not available.");
			}
		}

		if ($this->_soap_client_type !== 'nusoap')
		{
			$this->_soap_client_obj = null;
			$this->_soap_client_type = null;
		}

		if (!is_object($this->_soap_client_obj))
		{
			$this->_soap_client_obj = new nusoap_client($this->_wsdl_file_path, true);

			if (!is_object($this->_soap_client_obj))
			{
				throw new Exception("Unable to create instance of class newsoap_client.");
			}
		}

		$this->_soap_client_type = 'nusoap';
		$this->_soap_client_obj->debugLevel = 1000;
		$return = $this->_soap_client_obj->call($method, $arguments);

		return $return;
	}

	/**
	 * Call a method using the PHP SoapClient extension
	 *
	 * @param string $method The SOAP method to call
	 * @param array $arguments The SOAP method arguments
	 * @throws Exception
	 *
	 * @return array The SOAP response array
	 */
	protected function _callSoapClientPhpext($method, $arguments)
	{
		if (!extension_loaded('soap'))
		{
			throw new Exception("The PHP SOAP extension is not loaded.");
		}

		if (!class_exists('SoapClient'))
		{
			throw new Exception("The PHP soap extension class SoapClient was requested, but not available.");
		}

		if ($this->_soap_client_type !== 'phpext')
		{
			$this->_soap_client_obj = null;
			$this->_soap_client_type = null;
		}

		throw new Exception("Cannot use the PHP soap extension and SoapClient class at this time");
		// ini_set("soap.wsdl_cache_enabled", "0");
		// $this->_soap_client_obj = new SoapClient($this->_wsdl_file_path, array('trace' => 1));
		// $this->_soap_client_type = 'phpext';
	}

	/**
	 * @return array
	 */
	public function apiMethodAuthenticateUser()
	{
		$method = 'AuthenticateUser';
		$arguments = array();
		$return = $this->apiCall($method, $arguments);

		return $return;
	}

	/**
	 * Get account info
	 *
	 * @return array
	 */
	public function apiMethodGetAccountInfo()
	{
		$method = 'GetAccountInfo';
		$arguments = array();
		$return = $this->apiCall($method, $arguments);

		return $return;
	}

	/**
	 * Purchase postage
	 *
	 * @param $_arguments
	 *
	 * @return array
	 */
	public function apiMethodPurchasePostage($_arguments)
	{
		$method = 'PurchasePostage';
		$return = $this->apiCall($method, $_arguments);

		return $return;
	}

	/**
	 * Purchase status
	 *
	 * @param $_arguments
	 *
	 * @return array
	 */
	public function apiMethodGetPurchaseStatus($_arguments)
	{
		$method = 'GetPurchaseStatus';
		$return = $this->apiCall($method, $_arguments);

		return $return;
	}

	/**
	 * Get rates
	 *
	 * @param $_arguments
	 *
	 * @return array
	 */
	public function apiMethodGetRates($_arguments)
	{
		$method = 'GetRates';
		$return = $this->apiCall($method, $_arguments);

		return $return;
	}

	/**
	 * Cleanse address
	 *
	 * @param $_address
	 *
	 * @return array
	 */
	public function apiMethodCleanseAddress($_address)
	{
		$method = 'CleanseAddress';
		$arguments = array('Address' => $_address);
		$addresses = array('original' => $_address, 'cleansed' => array(), 'candidates' => array());
		$return = $this->apiCall($method, $arguments);

		if ($return)
		{
			if (isset($return['Address']) && is_array($return['Address']))
			{
				$tmp_address = $return['Address'];
				$addresses['cleansed'] = $tmp_address;
			}

			if (isset($return['CandidateAddresses']) && is_array($return['CandidateAddresses']) && isset($return['CandidateAddresses']['Address']) && is_array($return['CandidateAddresses']['Address']))
			{
				foreach ($return['CandidateAddresses']['Address'] as $tmp_address)
				{
					$addresses['candidates'][] = $tmp_address;
				}
			}
		}

		return $addresses;
	}

	/**
	 * Carrier pickup call
	 *
	 * @param $_arguments
	 *
	 * @return array
	 */
	public function apiMethodCarrierPickup($_arguments)
	{
		$return = $this->apiCall('CarrierPickup', $_arguments);

		return $return;
	}
}


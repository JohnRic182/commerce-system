<?php

/**
 * Class ShoppingCartProducts
 */
class ShoppingCartProducts
{
	/** @var DB */
	public $db;
	public $settings;
	public $items_count = 0;
	public $pages_count = 0;
	public $current_page = 0;
	public $bestsellersCount = 0;
	public $productImages = array();
	public $productImagesPath = "images/products/";
	public $productThumbsPath = "images/products/thumbs/";
	public $productThumbs2Path = "images/products/preview/";
	public $parent = "0";

	/**
	 * Class Constructor
	 *
	 * @param $db
	 * @param $settings
	 */
	public function __construct(&$db, &$settings)
	{
		//assign DB & settings
		$this->db = $db;
		$this->settings = $settings;
		return $this;
	}

	/**
	 * Get Product Image
	 *
	 * @param string $product_id
	 * @param string $image_location
	 * @param string $image_url
	 * @param string $url_prefix
	 *
	 * @return bool|mixed|string
	 */
	public function getProductImage($product_id, $image_location, $image_url, $url_prefix = '')
	{
		$product_id = strtolower($product_id);

		if ($image_location == 'Web' && $image_url != '')
		{
			if (isSslMode())
			{
				return str_replace('http://', 'https://', $image_url);
			}
			else
			{
				return $image_url;
			}
		}
		else
		{
			$product_id = str_replace(array('\'', '/', '*', ':', '?', '<', '>', '|', '"', "'"), '_', $product_id);

			if (is_file($this->productImagesPath.$product_id.'.jpg'))
			{
				return $url_prefix.$this->productImagesPath.$product_id.'.jpg';
			}

			if (is_file($this->productImagesPath.$product_id.'.gif'))
			{
				return $url_prefix.$this->productImagesPath.$product_id.'.gif';
			}

			if (is_file($this->productImagesPath.$product_id.'.png'))
			{
				return $url_prefix.$this->productImagesPath.$product_id.'.png';
			}
		}

		return false;
	}
	
	/**
	 * Get Product Thumb
	 *
	 * @param string $product_id
	 * @param string $image_location
	 * @param string $image_url
	 * @return string thumb path
	 */
	public function getProductThumb($product_id, $image_location, $image_url)
	{
		$product_id = strtolower($product_id);
		if ($image_location == "Web" && $image_url != "")
		{
			if (isSslMode())
			{
				//return substr($image_url, 0, 5) == "https" ? $image_url : false;
				return str_replace('http://', 'https://', $image_url);
			}
			else
			{
				return $image_url;
			}
		}
		else
		{
			for ($k=0; $k<strlen($product_id); $k++)
			{
				if(in_array($product_id[$k], array("\\", "/", "*", ":", "?", "<", ">", "|", '"', "'")))$product_id[$k] = "_";
			}
			if (is_file($this->productThumbsPath.$product_id.".jpg"))
			{
				return $this->productThumbsPath.$product_id.".jpg";
			}
			if (is_file($this->productThumbsPath.$product_id.".png"))
			{
				return $this->productThumbsPath.$product_id.".png";
			}
			if (is_file($this->productThumbsPath.$product_id.".gif"))
			{
				return $this->productThumbsPath.$product_id.".gif";;
			}
		}
		return false;
	}
	
	/**
	 * Get Product Thumb2
	 *
	 * @param string $product_id
	 * @param string $image_location
	 * @param string $image_url
	 * @return string thumb path
	 */
	public function getProductThumb2($product_id, $image_location, $image_url){
		$product_id = strtolower($product_id);

		if ($image_location == "Web" && $image_url != "")
		{
			return $image_url;
		}
		else
		{
			for ($k=0; $k<strlen($product_id); $k++)
			{
				if (in_array($product_id[$k], array("\\", "/", "*", ":", "?", "<", ">", "|", '"', "'")))
				{
					$product_id[$k] = "_";
				}
			}

			if (is_file($this->productThumbs2Path.$product_id.".jpg"))
			{
				return $this->productThumbs2Path.$product_id.".jpg";
			}

			if (is_file($this->productThumbs2Path.$product_id.".png"))
			{
				return $this->productThumbs2Path.$product_id.".png";
			}

			if (is_file($this->productThumbs2Path.$product_id.".gif"))
			{
				return $this->productThumbs2Path.$product_id.".gif";;
			}
		}
		return false;
	}

	/**
	 * Calculate taxes
	 *
	 * @param $product
	 */
	protected function _calcTaxes(&$product)
	{
		$currencies = new Currencies($this->db);
		$current_currency = $currencies->getCurrentCurrency();
		$decimals = $current_currency['decimal_places'];

		$_price2 = $product["price2"];
		$_price = $product["price"];
		$_tax_rate = -1;

		$taxProvider = Tax_ProviderFactory::getTaxProvider();

		//calc taxes
		if ($taxProvider->getDisplayPricesWithTax() && $product["is_taxable"] == "Yes")
		{
			$_price = $_price + $taxProvider->calculateItemTax($product['tax_class_id'], $_price);
			if (!is_null($_price2)) $_price2 = $_price2 + $taxProvider->calculateItemTax($product['tax_class_id'], $_price2);
			$_tax_rate = $taxProvider->getTaxRate($product['tax_class_id'], false);
		}

		$product["price"] = PriceHelper::round($_price, $decimals);
		if (!is_null($_price2)) $product["price2"] = PriceHelper::round($_price2, $decimals);
		$product["tax_rate"] = $_tax_rate;
	}

	/**
	 * Return product data by id
	 *
	 * @param $id
	 * @param $level
	 * @param int $parent
	 *
	 * @return bool
	 *
	 * TODO: Reuse getItemDetails here - a lot of code is duplicate
	 */
	public function getProductById($id, $level, $parent = 0)
	{
		$id = intval($id);
		$parent = intval($parent);

		$r = $this->db->query('
			SELECT 
				p.*,
				IF(p.url_custom="", p.url_default,p. url_custom) AS product_url,
				IF(c.url_custom="", c.url_default,c. url_custom) AS cat_url,
				c.cid AS cat_id, c.name AS cat_name, m.manufacturer_name AS manufacturer_name
			FROM ' . DB_PREFIX . 'products AS p
			INNER JOIN ' . DB_PREFIX . 'catalog AS c ON ' . ($parent > 0 ? ' ' . ' c.cid=' . $parent  : ' p.cid = c.cid ') . '
			LEFT JOIN ' . DB_PREFIX . 'manufacturers AS m ON p.manufacturer_id = m.manufacturer_id AND m.is_visible=1
			WHERE p.pid=' . $id . ' AND p.is_visible="Yes" AND c.is_visible="Yes"
			AND ' . $this->_getInventorySql()
		);
		
		if ($this->db->moveNext($r))
		{
			$product = $this->db->col;

			//save price before taxes
			$product['price_no_tax'] = $product['price'];
			$product['price2_no_tax'] = $product['price2'];
			
			$this->_calcTaxes($product);

			//url
			$product['url'] = UrlUtils::getProductUrl($product['product_url'], $product['pid'], $product['cid']);
			$product['print_url'] = $this->settings['GlobalHttpUrl'] . '/' . $this->settings['INDEX'] . '?p=product&amp;id=' . $id .
				'&amp;parent=' . intval($product['cid']) . '&amp;is_print_version=true';
			$product['cat_url'] = UrlUtils::getCatalogUrl($product['cat_url'], $product['cat_id']);

			//inventory, min / max order
			if ($product['inventory_control'] != 'No')
			{
				$product['allowed_max'] = $product['max_order'] > 0 ? ($product['max_order'] > $product['stock'] ? $product['stock'] : $product['max_order']) : $product['stock'];
			}
			else
			{
				$product['allowed_max'] = 0;
			}

			//product primary image & thumb
			$product['image'] = $this->getProductImage($product['product_id'], $product['image_location'], $product['image_url']);
			$product['thumb'] = $this->getProductThumb($product['product_id'], $product['image_location'], $product['image_url']);
			$product['preview'] = $this->getProductThumb2($product['product_id'], $product['image_location'], $product['image_url']);

			if ($product['image'])
			{
				if ($product['image_location'] == 'Local')
				{
					$img_data = @getimagesize($product['image']);
					if ($img_data)
					{
						$product['image_width'] = $img_data[0];
						$product['image_height'] = $img_data[1];
						$product['image_size'] = $img_data[3];
					}
					$product['product_page_image_url']  = $product['preview'] ? $product['preview'] : $product['image'];
				}
				else
				{
					$product['product_page_image_url'] = $product['image_url'];
					$product['image_size'] = '';
				}
			}

			//get secondary images
			$this->db->query('SELECT * FROM ' . DB_PREFIX . 'products_images WHERE pid=' . $id . ' AND is_visible="Yes" ORDER BY image_priority, iid');

			if ($this->db->numRows() > 0)
			{
				$product['secondary_images'] = $this->db->getRecords();

				for ($imageIndex = 0; $imageIndex < count($product['secondary_images']); $imageIndex++)
				{
					if ($product['secondary_images'][$imageIndex]['image_location'] == 'Local')
					{
						$product['secondary_images'][$imageIndex]['img_src'] = 'images/products/secondary/' . $product['secondary_images'][$imageIndex]['filename'] . '.' . $product['secondary_images'][$imageIndex]['type'];
						$tf = 'images/products/secondary/thumbs/' . $product['secondary_images'][$imageIndex]['filename'] . '.' . $product['secondary_images'][$imageIndex]['type'];
						$product['secondary_images'][$imageIndex]['thumb_src'] = file_exists($tf) ? $tf : false;

						if (!$product['secondary_images'][$imageIndex]['thumb_src'])
						{
							$product['secondary_images'][$imageIndex]['thumb_size'] = ' height="' . $this->settings['CatalogThumbSize'] . '" ';
						}
						else
						{
							$product['secondary_images'][$imageIndex]['thumb_size'] = '';
						}
					}
					else if ($product['secondary_images'][$imageIndex]['image_location'] == 'Web')
					{
						$product['secondary_images'][$imageIndex]['img_src'] = $product['secondary_images'][$imageIndex]['image_url'];
					}
				}
			}
			else
			{
				$product['secondary_images'] = false;
			}

			if ($product['attributes_count'] > 0)
			{
				//get attributes
				$r = $this->db->query('
					SELECT * FROM ' . DB_PREFIX . 'products_attributes
					WHERE ' . DB_PREFIX . 'products_attributes.pid = ' . $id . ' AND is_active="Yes"
					ORDER BY priority, name, is_modifier
				');

				while ($this->db->moveNext($r))
				{
					$attribute = $this->db->col;
					$attribute['options'] = array();
					$paid = $this->db->col['paid'];
					parseOptions($this->db->col['options'], $product['price_no_tax'], $product['weight'], $attribute['options']);
					$product['attributes'][$paid] = $attribute;
				}
			}

			$product['price_wholesale'] = $product['price_no_tax'];
			if ($this->settings['WholesaleDiscountType'] == 'PRODUCT')
			{
				if ($level > 0 && $product['price_level_1'] > 0 && $this->settings['WholesaleMaxLevels'] > 0) $product['price_wholesale'] = $product['price_level_1'];
				if ($level > 1 && $product['price_level_2'] > 0 && $this->settings['WholesaleMaxLevels'] > 1) $product['price_wholesale'] = $product['price_level_2'];
				if ($level > 2 && $product['price_level_3'] > 0 && $this->settings['WholesaleMaxLevels'] > 2) $product['price_wholesale'] = $product['price_level_3'];
			}

			if ($this->settings['WholesaleDiscountType'] == 'GLOBAL')
			{
				if($level > 0 && $this->settings['WholesaleMaxLevels'] > 0) $product['price_wholesale'] = $product['price_no_tax'] / 100 * (100-$this->settings['WholesaleDiscount1']);
				if($level > 1 && $this->settings['WholesaleMaxLevels'] > 1) $product['price_wholesale'] = $product['price_no_tax'] / 100 * (100-$this->settings['WholesaleDiscount2']);
				if($level > 2 && $this->settings['WholesaleMaxLevels'] > 2) $product['price_wholesale'] = $product['price_no_tax'] / 100 * (100-$this->settings['WholesaleDiscount3']);
			}

			$taxProvider = Tax_ProviderFactory::getTaxProvider();
			//vat tax / wholesale price start
			if ($taxProvider->getDisplayPricesWithTax() && $product['is_taxable'] == 'Yes')
			{
				$product['price_wholesale'] = $product['price_wholesale'] + $taxProvider->calculateItemTax($product['tax_class_id'], $product['price_wholesale']);
			}
			//vat tax / wholesale price finish

			$this->db->query('SELECT * FROM ' . DB_PREFIX . 'products_quantity_discounts WHERE pid = ' . $id . ' AND is_active = "Yes" ORDER BY range_min');
			$r = $product['quantity_discounts'] = array();

			while ($this->db->moveNext($r))
			{
				if (($this->db->col['apply_to_wholesale'] == 'Yes' && $level > 0) || $this->db->col['apply_to_wholesale'] != 'Yes')
				{
					$iii = count($product['quantity_discounts']);
					$product['quantity_discounts'][$iii] = $this->db->col;
					
					$product['quantity_discounts'][$iii]['discount'] = (strtolower($product['quantity_discounts'][$iii]['discount_type']) == 'percent')
						? PriceHelper::round((float) $product['quantity_discounts'][$iii]['discount'])
						: $product['quantity_discounts'][$iii]['discount'];
				}
			}

			$product['quantity_discounts'] = count($product['quantity_discounts']) > 0 ? $product['quantity_discounts'] : false;

			if (($product['inventory_control'] == 'Yes' || $product['inventory_control'] == 'AttrRuleExc' || $product['inventory_control'] == 'AttrRuleInc') && 
				$product['inventory_rule'] == 'OutOfStock' && 
				intval($product['stock']) === 0
			)
			{
				$product['out_of_stock'] = 1;
			}
			else
			{
				$product['out_of_stock'] = 0;
			}

			$product['inventory_hashes'] = $this->db->selectAll('SELECT * FROM ' . DB_PREFIX . 'products_inventory WHERE pid=' . $id . ' AND is_active=1');
			if (!is_array($product['inventory_hashes']) || count($product['inventory_hashes']) == 0) unset($product['inventory_hashes']);

			$this->db->query('
				SELECT
					psp.price,
					ss.method_name
				FROM ' . DB_PREFIX . 'products_shipping_price AS psp
				INNER JOIN '.DB_PREFIX.'shipping_selected AS ss ON psp.ssid = ss.ssid
				WHERE psp.pid=' . $id . ' AND psp.is_price="Yes"
			');

			if ($this->db->numRows())
			{
				$product['shipping_price'] = array();

				while ($this->db->moveNext())
				{
					$product['shipping_price'][] = array(
						'method_name' => $this->db->col['method_name'],
						'price' => $this->db->col['price']
					);
				}
			}
			else
			{
				$product['shipping_price'] = false;
			}

			//manufacturer image begins here
			$manufacturer_image = 'images/manufacturers/'.$product['manufacturer_id'];
			if (file_exists($manufacturer_image.'.jpg'))
			{
				$product['manufacturer_image'] = $manufacturer_image.'.jpg';
			}
			elseif (file_exists($manufacturer_image.'.gif'))
			{
				$product['manufacturer_image'] = $manufacturer_image.'.gif';
			}
			elseif (file_exists($manufacturer_image.'.png'))
			{
				$product['manufacturer_image'] = $manufacturer_image.'.png';
			}
			else
			{
				$product['manufacturer_image'] = '';
			}

			$recurring_billing_data = trim($product['recurring_billing_data']) != '' ? @unserialize($product['recurring_billing_data']) : array();
			$product['recurring_billing_data'] = is_array($recurring_billing_data) ? $recurring_billing_data : array();

			//TODO: VAT taxes and recurring billing amount / trial amount

			//manufacturer image ends here
			return $product;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Get item details
	 *
	 * @param $item
	 * @param int $level
	 *
	 * @return mixed
	 */
	public function getItemDetails($item, $level = 0)
	{
		$item['price_no_tax'] = $item['price'];
		$item['price2_no_tax'] = $item['price2'];

		$this->_calcTaxes($item);

		//set URLs
		$item['url'] = UrlUtils::getProductUrl($item['product_url'], $item['pid'], $item['cid']);
		$item['cat_url'] = UrlUtils::getCatalogUrl($item['cat_url'], $item['cid']);

		//inventory, min / max order
		if ($item['inventory_control'] == 'Yes')
		{
			$item['allowed_max'] = $item['max_order'] > 0 ? ($item['max_order'] > $item['stock'] ? $item['stock'] : $item['max_order']) : $item['stock'];
		}
		else
		{
			$item['allowed_max'] = 0;
		}

		//images
		if ($item['image'] = $image_path = $this->getProductImage($item['product_id'], $item['image_location'], $item['image_url']))
		{
			$item['thumb'] = $thumb_path = $this->getProductThumb($item['product_id'], $item['image_location'], $item['image_url']);
			$item['thumb'] = $item['thumb'] ? $item['thumb'] : $item['image'];

			if ($item['image_location'] == 'Web' || $this->settings['CatalogThumbType'] == 'Square')
			{
				$item['thumb_size'] = ' width="' . $this->settings['CatalogThumbSize'] . '" height="' . $this->settings['CatalogThumbSize'] . '" ';
			}
			else
			{
				$is = @getImageSize($thumb_path);
				if ($is)
				{
					$item['thumb_size'] = $is[0] > $is[1] ? ' width="' . $this->settings['CatalogThumbSize'].'" ' : ' height="' . $this->settings['CatalogThumbSize'] . '" ';
				}
			}
		}
		else
		{
			$item['thumb'] = false;
		}

		//wholesale prices
		$item['price_wholesale'] = $item['price_no_tax'];
		if ($this->settings['WholesaleDiscountType'] == 'PRODUCT')
		{
			if ($level > 0 && $item['price_level_1'] > 0 && $this->settings['WholesaleMaxLevels'] > 0) $item['price_wholesale'] = $item['price_level_1'];
			if ($level > 1 && $item['price_level_2'] > 0 && $this->settings['WholesaleMaxLevels'] > 1) $item['price_wholesale'] = $item['price_level_2'];
			if ($level > 2 && $item['price_level_3'] > 0 && $this->settings['WholesaleMaxLevels'] > 2) $item['price_wholesale'] = $item['price_level_3'];
		}
		if ($this->settings['WholesaleDiscountType'] == 'GLOBAL')
		{
			if ($level > 0 && $this->settings['WholesaleMaxLevels'] > 0) $item['price_wholesale'] = $item['price_no_tax'] / 100 * (100 - $this->settings['WholesaleDiscount1']);
			if ($level > 1 && $this->settings['WholesaleMaxLevels'] > 1) $item['price_wholesale'] = $item['price_no_tax'] / 100 * (100 - $this->settings['WholesaleDiscount2']);
			if ($level > 2 && $this->settings['WholesaleMaxLevels'] > 2) $item['price_wholesale'] = $item['price_no_tax'] / 100 * (100 - $this->settings['WholesaleDiscount3']);
		}

		$taxProvider = Tax_ProviderFactory::getTaxProvider();
		//vat tax / wholesale price start
		if ($taxProvider->getDisplayPricesWithTax() && $item['is_taxable'] == 'Yes')
		{
			$item['price_wholesale'] = $item['price_wholesale'] + $taxProvider->calculateItemTax($item['tax_class_id'], $item['price_wholesale']);
		}
		//vat tax / wholesale price finish

		//stock
		$item['out_of_stock'] = $item['inventory_control'] == 'Yes' && $item['inventory_rule'] == 'OutOfStock' && intval($item['stock']) === 0 ? 1 : 0;

		//manufacturer image start here
		$manufacturer_image = 'images/manufacturers/'.$item['manufacturer_id'];
		if (file_exists($manufacturer_image.'.jpg')) $item['manufacturer_image'] = $manufacturer_image.'.jpg';
		elseif(file_exists($manufacturer_image.'.gif')) $item['manufacturer_image'] = $manufacturer_image.'.gif';
		elseif(file_exists($manufacturer_image.'.png')) $item['manufacturer_image'] = $manufacturer_image.'.png';
		else $item['manufacturer_image'] = '';
		//manufacturer image ends here

		$recurring_billing_data = trim($item['recurring_billing_data']) != '' ? @unserialize($item['recurring_billing_data']) : array();
		$item['recurring_billing_data'] = is_array($recurring_billing_data) ? $recurring_billing_data : array();

		//TODO: VAT taxes and recurring billing amount / trial amount

		return $item;
	}


	/**
	 * Cleanup sub products
	 * @param $product
	 */
	public function cleanupSubProducts(&$product)
	{
		if ($product['inventory_control'] == 'AttrRuleExc' && $product["inventory_rule"] == 'Hide')
		{
			$attributes_defined = array();

			if (!empty($product['inventory_hashes']))
			{
				foreach ($product['inventory_hashes'] as $_inventory_hash)
				{
					$attributes = explode("\n", $_inventory_hash['attributes_list']);
					if ($_inventory_hash['stock'] > 0)
					{
						foreach ($attributes as $attr)
						{
							$attributes_defined[$attr] = true;
						}
					}
				}
				unset($_inventory_hash);
			}

			foreach ($product['attributes'] as $attributes_key => $attributes)
			{
				if ($attributes['track_inventory'] == 1)
				{
					foreach ($attributes['options'] as $key => $attribute_option)
					{
						$option_hash = $attributes['name'].': '.$attribute_option['name'];
						if (!array_key_exists($option_hash, $attributes_defined))
						{
							unset($product['attributes'][$attributes_key]['options'][$key]);
						}
					}
				}
			}
		}
		else if ($product['inventory_control'] == 'AttrRuleInc')
		{
			$inventory_hashes = array();
			$inventory_stock = array();

			if (!empty($product['inventory_hashes']))
			{
				foreach ($product['inventory_hashes'] as $_inventory_hash)
				{
					$inventory_hashes[] = $_inventory_hash['attributes_list'];
					$inventory_stock[$_inventory_hash['attributes_list']] = $_inventory_hash['stock'];
				}
				unset($_inventory_hash);
			}

			foreach ($product['attributes'] as $attributes_key => $attributes)
			{
				// loop through attribute options
				foreach ($attributes['options'] as $key => $attribute_option)
				{
					$option_hash = $attributes['name'].': '.$attribute_option['name'];

					if ($product["inventory_rule"] == 'Hide' && isset($inventory_stock[$option_hash]) && intval($inventory_stock[$option_hash]) == 0)
					{
						unset($product['attributes'][$attributes_key]['options'][$key]);
					}

					if (!in_array($option_hash, $inventory_hashes) && $product["out_of_stock"] == 1)
					{
						$product["out_of_stock"] = 0;
					}
				}
			}
		}
	}

	/**
	 * Get products families
	 *
	 * @param $pid
	 * @param $level
	 *
	 * @return mixed
	 */
	public function getProductFamilies($pid, $level)
	{
		if ($this->settings['RecommendedProductsCount'] == 0)
		{
			return false;
		}

		$this->db->query("
			SELECT ".DB_PREFIX."products_families.* 
			FROM ".DB_PREFIX."products_families_dependencies
			INNER JOIN ".DB_PREFIX."products_families ON ".DB_PREFIX."products_families.pf_id = ".DB_PREFIX."products_families_dependencies.pf_id
			WHERE ".DB_PREFIX."products_families_dependencies.pid=".intval($pid)."
		");

		$f = $this->db->getRecords();

		if ($f)
		{
			for ($i=0; $i<count($f); $i++)
			{
				//get products in family
				$this->db->query("SELECT * FROM ".DB_PREFIX."products_families_content WHERE pf_id=".intval($f[$i]["pf_id"])." AND pid <> ".intval($pid)." ORDER BY RAND()");
				$a = $this->db->getRecords();
				$f[$i]["products"] = array();
				if ($a)
				{
					for ($j = 0; $j < count($a); $j++)
					{
						$pr = $this->getProductById($a[$j]["pid"], $level);
						if ($pr)
						{
							$f[$i]["products"][] = $pr;
							if(count($f[$i]['products']) >= intval($this->settings['RecommendedProductsCount']))
							{
									break;
							}
						}
					}
				}
				$f[$i]["products"] = count($f[$i]["products"]) > 0 ? $f[$i]["products"] : false;
			}
		}
		return $f;
	}

	/**
	 * Select products from the same category
	 *
	 * @param $pid
	 * @param $level
	 * @param $parent
	 * @param int $count
	 *
	 * @return array|bool
	 */
	public function getProductSiblings($pid, $level, $parent, $count = 10)
	{
		$a = $this->db->selectOne("
			SELECT COUNT(*) as c
			FROM ".DB_PREFIX."products AS p
			INNER JOIN ".DB_PREFIX."catalog AS c ON c.cid=p.cid
			WHERE p.cid=".intval($parent)." AND p.pid <> ".intval($pid)." AND p.is_visible = 'Yes' AND c.is_visible = 'Yes'
		");

		$resultsCount = $a['c'];
		
		if ($resultsCount == 0) return false;

		$siblings = array();
		
		if ($resultsCount < 500)
		{
			$this->db->query("
				SELECT p.pid
				FROM ".DB_PREFIX."products AS p
				INNER JOIN ".DB_PREFIX."catalog AS c ON c.cid=p.cid
				WHERE p.cid=".intval($parent)." AND p.pid <> ".intval($pid)." AND p.is_visible='Yes' AND c.is_visible = 'Yes'
				ORDER BY RAND()
				LIMIT ".intval($count)
			);
			
            $products = $this->db->getRecords();
			
			foreach ($products as $product)
			{
				if (($pr = $this->getProductById($product['pid'], $level)) != false) 		
				{
					$siblings[] = $pr;
				}
			}
		}
		else
		{
			$offsets = array();

			for ($c = 1; $c <= $count * 10; $c++)
			{
				$offset = mt_rand(0, $resultsCount);
				if (isset($offsets[$offset])) continue;
				
				$product = $this->db->selectOne("
					SELECT p.pid
					FROM ".DB_PREFIX."products AS p
					INNER JOIN ".DB_PREFIX."catalog AS c ON c.cid=p.cid
					WHERE p.cid=".intval($parent)." AND p.pid <> ".$pid." AND p.is_visible='Yes' AND c.is_visible = 'Yes'
					LIMIT ".intval($offset).",1
				");

				if ($product)
				{
					if (($pr = $this->getProductById($product['pid'], $level)) != false) 		
					{
						$siblings[] = $pr;
					}

					$offsets[$offset] = 1;
				}

				if (count($siblings) >= $count || count($siblings) >= $resultsCount) break;
			}
		}
		
		return count($siblings) > 0 ? $siblings : false;
	}
	
/* for future usage, query is working :)	
	function getOrderRecommendedProducts($oid){
		$this->db->query("
			SELECT products.*
			FROM ".DB_PREFIX."orders_content
			INNER JOIN products_families_dependencies ON products_families_dependencies.pid = orders_content.pid
			INNER JOIN products_families ON products_families.pf_id = products_families_dependencies.pf_id
			INNER JOIN products_families_content ON products_families_content.pf_id = products_families.pf_id
			INNER JOIN products ON products.pid = products_families_content.pid AND products.is_visible='Yes'

			LEFT JOIN orders_content AS products ON oc.pid = products.pid
			WHERE orders_content.oid = '1' AND oc.pid IS NULL
		");
	}
*/

	/**
	 * Get free products
	 *
	 * @param $pid
	 * @param $level
	 *
	 * @return array|bool
	 */
	public function getFreeProducts($pid, $level)
	{
		$this->db->query("SELECT ".DB_PREFIX."products_gifts.* FROM ".DB_PREFIX."products_gifts WHERE pid=".intval($pid));

		if ($this->db->numRows()>0)
		{
			$a = $this->db->getRecords();
			$b = array();

			for ($i = 0; $i < count($a); $i++)
			{
				$pr = $this->getProductById($a[$i]["gift_pid"], $level);
				if ($pr)
				{
					$pr["free_product_quantity"] = $a[$i]["gift_quantity"];
					$b[] = $pr;
				}
			}
			return count($b) > 0 ? $b : false;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Returns array for products updates email
	 *
	 * @param $from
	 * @param $to
	 * @param $level
	 * @param bool $limit
	 *
	 * @return array|bool
	 */
	public function getItemsProductUpdates($from, $to, $level, $limit = false)
	{
		$query = $this->db->query("
			SELECT ".DB_PREFIX."products.*, 
				IF(".DB_PREFIX."products.url_custom='',".DB_PREFIX."products.url_default,".DB_PREFIX."products.url_custom) AS product_url,
				".DB_PREFIX."catalog.name AS cat_name,
				IF(".DB_PREFIX."catalog.url_custom='',".DB_PREFIX."catalog.url_default,".DB_PREFIX."catalog.url_custom) AS cat_url
			FROM ".DB_PREFIX."products 
			INNER JOIN ".DB_PREFIX."catalog ON ".DB_PREFIX."products.cid = ".DB_PREFIX."catalog.cid AND ".DB_PREFIX."catalog.is_visible = 'Yes'
			WHERE ".DB_PREFIX."products.is_visible = 'Yes' AND ".DB_PREFIX."products.added BETWEEN '".$from." 00:00:01' AND '".$to." 23:59:59' AND ".DB_PREFIX."products.product_id != 'gift_certificate'
			ORDER BY ".DB_PREFIX."products.is_hotdeal, ".DB_PREFIX."products.title
			".($limit?("LIMIT ".intval($limit)):"")."
		");
		
		if ($this->db->numRows($query) > 0)
		{
			$result = array();

			while ($this->db->moveNext($query))
			{
				$result[] = $this->getItemDetails($this->db->col, $level);
			}

			return $result;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Get bestsellers by order content
	 *
	 * @param $oid
	 * @param $level
	 *
	 * @return array|bool
	 */
	public function getItemsBestSellersByOrderContent($oid, $level)
	{
		//get current order items
		$this->db->query("SELECT pid FROM ".DB_PREFIX."orders_content WHERE oid=".intval($oid)." GROUP BY pid");
		$order_items = array();

		while ($this->db->moveNext())
		{
			$order_items[] = $this->db->col["pid"];
		}
		
		//select completed orders whose have these products
		$this->db->query("
			SELECT ".DB_PREFIX."orders_content.oid
			FROM ".DB_PREFIX."orders_content 
			INNER JOIN ".DB_PREFIX."orders ON ".DB_PREFIX."orders.oid = ".DB_PREFIX."orders_content.oid AND ".DB_PREFIX."orders.status = 'Completed' AND ".DB_PREFIX."orders.payment_status = 'Received'
			WHERE pid IN (".implode(",", $order_items).")
			GROUP BY ".DB_PREFIX."orders_content.oid	
		");
		$orders_with_items = array();
		
		while ($this->db->moveNext())
		{
			$orders_with_items[] = $this->db->col["oid"];
		}
		
		if (count($orders_with_items) > 0 && count($order_items) > 0)
		{
			//select other products from these orders
			$query = $this->db->query("
				SELECT 
					".DB_PREFIX."products.*,
					IF(".DB_PREFIX."products.url_custom='',".DB_PREFIX."products.url_default,".DB_PREFIX."products.url_custom) AS product_url,
					".DB_PREFIX."catalog.name AS cat_name,
					IF(".DB_PREFIX."catalog.url_custom='',".DB_PREFIX."catalog.url_default,".DB_PREFIX."catalog.url_custom) AS cat_url,
					SUM(".DB_PREFIX."orders_content.admin_price * ".DB_PREFIX."orders_content.admin_quantity) AS product_amount,
					SUM(".DB_PREFIX."orders_content.admin_quantity) AS product_quantity
				FROM ".DB_PREFIX."orders_content
				INNER JOIN ".DB_PREFIX."products ON ".DB_PREFIX."products.pid = ".DB_PREFIX."orders_content.pid AND ".DB_PREFIX."products.is_visible = 'Yes'
				INNER JOIN ".DB_PREFIX."catalog ON ".DB_PREFIX."catalog.cid = ".DB_PREFIX."products.cid AND ".DB_PREFIX."catalog.is_visible = 'Yes'
				WHERE ".DB_PREFIX."orders_content.oid IN (".implode(",", $orders_with_items).")
					AND ".DB_PREFIX."orders_content.pid NOT IN (".implode(",", $order_items).")
				HAVING product_amount IS NOT NULL OR product_quantity IS NOT NULL
				ORDER BY product_quantity DESC, product_amount DESC
				LIMIT ".intval($this->settings["CustomerAlsoBoughtCount"])."
			");
			
			$this->bestsellersCount = $this->db->numRows($query);
			
			if ($this->bestsellersCount > 0)
			{
				$result = array();

				while ($this->db->moveNext($query))
				{
					$result[] = $this->getItemDetails($this->db->col, $level);
				}
				return $result;
			}
		}
		
		return false;
	}

	/**
	 * Return BestSellers products
	 *
	 * @param $params
	 * @param $level
	 *
	 * @return array|bool|int
	 */
	public function getItemsBestSellers($params, $level)
	{
		$this->bestsellersCount = 0;
		
		if ($this->settings['CatalogBestsellersAvailable'] == 'YES')
		{
			$CatalogBestsellersPeriod = isset($params['CatalogBestsellersPeriod']) ? $params['CatalogBestsellersPeriod'] : 'Month';
			$CatalogBestsellersCount = isset($params['CatalogBestsellersCount']) ? $params['CatalogBestsellersCount'] : 3;

			//choose term
			switch($CatalogBestsellersPeriod)
			{
				case "Year" : $period = "1 YEAR"; break;
				case "6 Months" : $period = "6 MONTH"; break;
				case "3 Months" : $period = "3 MONTH"; break;
				case "2 Months" : $period = "2 MONTH"; break;
				default:
				case "Month": $period = "1 MONTH"; break;
			}
			//build categories select
			$where = "";
			$categories = isset($params['categories']) ? $params['categories'] : array();
			$categories = is_array($categories) ? $categories : array();
			
			if (count($categories) > 0)
			{
				$inner = "INNER JOIN ".DB_PREFIX."products_categories ON ".DB_PREFIX."products_categories.pid = ".DB_PREFIX."products.pid AND (";

				for ($i=0; $i< count($categories); $i++)
				{
					$inner .= ($i > 0 ? " OR " : " ")." ".DB_PREFIX."products_categories.cid='".intval($categories[$i])."' ";
				}
				$inner .= ")";
			}
			else
			{
				$inner = "";
			}
			
			$query = "
				SELECT
					".DB_PREFIX."products.*,
					IF(".DB_PREFIX."products.url_custom='',".DB_PREFIX."products.url_default,".DB_PREFIX."products.url_custom) AS product_url,
					".DB_PREFIX."catalog.name AS cat_name,
					IF(".DB_PREFIX."catalog.url_custom='',".DB_PREFIX."catalog.url_default,".DB_PREFIX."catalog.url_custom) AS cat_url,
					SUM(".DB_PREFIX."orders_content.admin_quantity) AS product_quantity
				FROM ".DB_PREFIX."orders_content
				INNER JOIN ".DB_PREFIX."orders ON ".DB_PREFIX."orders.oid=".DB_PREFIX."orders_content.oid
				INNER JOIN ".DB_PREFIX."products ON ".DB_PREFIX."products.pid = ".DB_PREFIX."orders_content.pid
				".$inner."
				INNER JOIN ".DB_PREFIX."catalog ON ".DB_PREFIX."catalog.cid = ".DB_PREFIX."products.cid AND ".DB_PREFIX."catalog.is_visible='Yes'
				WHERE 
					".DB_PREFIX."catalog.key_name <> 'gift_cert' AND
					".DB_PREFIX."products.is_visible='Yes' AND
					DATE_ADD(".DB_PREFIX."orders.placed_date, INTERVAL ".($period).")>NOW() AND 
					".DB_PREFIX."orders.status='Completed' AND 
					".DB_PREFIX."orders.payment_status='Received' ".$where."
				GROUP BY ".DB_PREFIX."orders_content.pid
				ORDER BY
					product_quantity DESC
				LIMIT ".intval($CatalogBestsellersCount)."
			";

			// cache query results

			$cache = new FileCache();

			$cacheId = md5($query);

			$data = $cache->getCachedData($cacheId, 4*3600);

			if ($cache->getResyncCache())
			{
				$data = $this->db->selectAll($query);
				$cache->cacheData($cacheId, $data);
			}
            else
            {
                // recheck for products availability
                $ids = array();

				if ($data && is_array($data))
				{
					foreach ($data as $item)
					{
						$ids[] = $item["pid"];
					}

					$availableItems = array();

					if (count($ids) > 0)
					{
						$this->db->query("
							SELECT p.pid, p.stock, p.inventory_control, p.price, p.price2
							FROM ".DB_PREFIX."products AS p
								INNER JOIN ".DB_PREFIX."catalog AS c ON c.cid = p.cid AND c.is_visible = 'Yes'
							WHERE p.pid IN(".implode(",", $ids).") AND p.is_visible = 'Yes'
						");

						while (($item =  $this->db->moveNext()) != false)
						{
							$availableItems[$item["pid"]] = $item;
						}
					}

					foreach ($data as $index => $item)
					{
						if (isset($availableItems[$item['pid']]))
						{
							$data[$index]["is_visible"] = 'Yes';
							$data[$index]['inventory_control'] = $availableItems[$item["pid"]]['inventory_control'];
							$data[$index]['stock'] = $availableItems[$item['pid']]['stock'];
							$data[$index]['price'] = $availableItems[$item['pid']]['price'];
							$data[$index]['price2'] = $availableItems[$item['pid']]['price2'];
						}
						else
						{
							$data[$index]["is_visible"] = 'No';
						}
					}
				}
            }

			if ($data && is_array($data) && count($data))
			{
				$this->bestsellersCount = count($data);

				if ($this->bestsellersCount > 0)
				{
					$result = array();

					foreach ($data as $item)
					{
						if ($item["is_visible"] == "Yes" && (in_array($item['inventory_control'], array('No', 'AttrRuleInc')) || $item['stock'] > 0))
						{
							$itemDetails = $this->getItemDetails($item, $level);
							$result[] = $itemDetails;
						}
					}

					return count($result) > 0 ? $result : 0;
				}
			}
		}

		return false;
	}

	/**
	 * Return Items for Catalog Page
	 *
	 * @param $params
	 * @param $level
	 *
	 * @return array|bool
	 */
	public function getItemsCatalog($params, $level)
	{
		$mode = isset($params['mode']) ? $params['mode'] : 'catalog';
		$search_in = isset($params['search_in']) ? $params['search_in'] : 'title';
		$CatalogNewestDays = isset($params['CatalogNewestDays']) ? $params['CatalogNewestDays'] : '15';
		$manufacturer_id = isset($params['manufacturer_id']) ? $params['manufacturer_id'] : '0';
		$nested = isset($params['nested']) ? $params['nested'] : false;
		$categories = isset($params['categories']) ? $params['categories'] : null;
		$parent = isset($params['parent']) ? $params['parent'] : 0;
		$frontEndCall = isset($params['front']) ? $params['front'] : false;
		$keywords = isset($params['keywords']) ? $params['keywords'] : array();
		$pids = isset($params['pids']) ? $params['pids'] : array();
		$includeStealth = isset($params['includeStealth']) ? $params['includeStealth'] : false;

		if ($frontEndCall)
		{
			$productFields =
				'p.pid,p.cid,p.manufacturer_id,p.is_hotdeal,p.is_taxable,p.inventory_control,p.inventory_rule,p.stock,' .
				'p.stock_warning,p.weight,p.free_shipping,p.same_day_delivery,p.digital_product,' .
				'p.price,p.price2,p.price_level_1,p.price_level_2,p.price_level_3,p.shipping_price,' .
				'p.tax_class_id,p.call_for_price,p.priority,p.attributes_count,p.min_order,p.max_order' .
				'p.rating,p.review_count,p.product_id,p.image_location,p.image_url,p.image_alt_text,' .
				'p.title,p.overview,';
		}
		else
		{
			$productFields = 'p.*,';
		}

		//build categories select
		$where = '';
		$inner = '';

		switch ($mode)
		{
			case 'search' : 
			{
				reset($keywords);

				if (count($keywords) > 0 )
				{
					$where = ' AND (';
					for ($i=0; $i < count($keywords); $i++)
					{
						switch (strtolower($keywords[$i]))
						{
							case '(' :
							case ')' :
							case 'and' :
							case 'or' : 
							{
								$where .= ' ' . $keywords[$i] . ' ';
								break;
							}
							default : 
							{
								switch ($search_in)
								{
									case 'id':
									{
										$where .= '(p.product_id LIKE "%' . $this->db->escape($keywords[$i]) .'%")';
										break;
									}
									case 'name' : 
									{
										$where .= '(p.title LIKE "%' . $this->db->escape($keywords[$i]) . '%")';
										break;
									}
									case 'keywords':
									{
										$where .= '(p.search_keywords LIKE "%' . $this->db->escape($keywords[$i]) . '%")';
										break;
									}
									default : 
									{
										$where .=
											'(p.title LIKE "%' . $this->db->escape($keywords[$i]) . '%" ' .
											'OR p.search_keywords LIKE "%' . $this->db->escape($keywords[$i]) . '%" ' .
											'OR p.description LIKE "%' . $this->db->escape($keywords[$i]) . '%" ' .
											'OR p.product_id LIKE "%' . $this->db->escape($keywords[$i]) . '%")';
										break;
									}
								}
								
								break;
							}
						}
					}
					$where.= ')';

					$where .= ' AND c.is_visible = "Yes"';
				}
				break;
			}
			case 'home' : 
			{
				$where = ' AND p.is_home="Yes"';
				break;
			}
			case 'hot' : 
			{
				$where = ' AND p.is_hotdeal="Yes"';
				break;
			}
			case 'new' : 
			{
				$where = ' AND DATE_ADD(p.added, INTERVAL ' . intval($CatalogNewestDays) . ' DAY) > NOW()';
				break;
			}
			case 'manufacturer' : 
			{
				$where = ' AND p.manufacturer_id=' . intval($manufacturer_id) . ' ';
				break;

			}
			default:
			case 'catalog': 
			{
				if ($pids)
				{
					$where = ' AND p.pid IN (' . implode(',', $pids) . ')';
				}

				if ($nested && !is_null($categories) && is_array($categories))
				{
					if (count($categories) > 0)
					{
						$inner = 'INNER JOIN ' . DB_PREFIX . 'products_categories AS pc ON pc.pid = p.pid AND (';
						
						for ($i=0; $i< count($params['categories']); $i++)
						{
							$inner .= ($i > 0 ? ' OR ' : '') . ' pc.cid=' . intval($categories[$i]) . ' ';
						}
						$inner .= ')';
					}
					else
					{
						$inner .= '';
					}
				}
				else
				{
					$inner = 'INNER JOIN ' . DB_PREFIX . 'products_categories AS pc ON pc.pid = p.pid AND pc.cid=' . intval($parent) . ' ';
				}
				break;
			}
		}
		
		if (isset($CatalogPriceRangeMin) && isset($CatalogPriceRangeMax))
		{
			$where.= ' AND p.price >= ' . number_format($CatalogPriceRangeMin, 2, '.', '') . ' AND p.price <= ' . number_format($CatalogPriceRangeMax, 2, '.', '') . ' ';
		}

		// Hides our digital product when disbaled in the admin area
		if ($this->settings['EnableDigitalDownload'] == '')
		{
			$where.= ' AND p.product_type != "' . Model_Product::DIGITAL . '"';
		}

		if (!$includeStealth)
		{
			$where .= ' AND p.is_stealth = 0 ';
		}

		//preselect
		$this->db->query('
			SELECT DISTINCT p.pid
			FROM ' . DB_PREFIX . 'products AS p
			' . $inner . '
			INNER JOIN ' . DB_PREFIX . 'catalog AS c ON p.cid=c.cid AND c.is_visible="Yes"
			WHERE p.is_visible="Yes" AND p.product_id <> "gift_certificate" ' . $where . ' AND ' . $this->_getInventorySql()
		);
		
		$this->items_count = $this->db->numRows();

		if (intval($params['CatalogItemsOnPage']) <= 0)
		{
			$this->pages_count = 1;
		}
		else
		{
			$this->pages_count = floor($this->items_count / $params['CatalogItemsOnPage']) +  ((($this->items_count % $params['CatalogItemsOnPage']) > 0 ) ? 1 : 0);
		}

		$this->current_page = $params['Page'];
		$this->current_page = ($this->current_page<1)?1:$this->current_page;
		
		if ($this->current_page > $this->pages_count)
		{
			$this->items_count = 0;
		}
		
		if ($this->items_count)
		{
			//sort by
			switch ($params['CatalogSortBy'])
			{
				default:
				case 'priority' : $order_by = 'p.priority, p.title'; break;
				case 'name' : $order_by = 'p.title'; break;
				case 'id' : $order_by = 'p.product_id'; break;
				case 'price' : $order_by = 'p.price, p.title'; break;
				case 'price_desc' : $order_by = 'p.price DESC, p.title'; break;
				case 'date' : $order_by = 'p.added DESC, p.title'; break;
			}

			//final select
			$query = $this->db->query('
				SELECT DISTINCT ' . $productFields .'
					IF(p.url_custom="", p.url_default,p.url_custom) AS product_url,
					c.name AS cat_name,
					IF(c.url_custom="",c.url_default,c.url_custom) AS cat_url,
					manufacturer_name
				FROM ' . DB_PREFIX . 'products AS p
				' . $inner . '
				INNER JOIN ' . DB_PREFIX . 'catalog AS c ON p.cid=c.cid AND c.is_visible="Yes"
				LEFT JOIN ' . DB_PREFIX . 'manufacturers AS m ON p.manufacturer_id = m.manufacturer_id AND m.is_visible=1
				WHERE p.is_visible="Yes" AND p.product_id <> "gift_certificate" AND ' . $this->_getInventorySql() . '
				' . $where . '
				ORDER BY ' . $order_by.
				(intval($params["CatalogItemsOnPage"]) != -1 ? ' LIMIT ' . intval(($this->current_page-1) * $params['CatalogItemsOnPage']) . ', ' . intval($params['CatalogItemsOnPage']) : '')
			);
			
			if ($this->db->numRows()>0)
			{
				$result = array();

				while ($this->db->moveNext($query))
				{
					$result[] = $this->getItemDetails($this->db->col, $level);
				}

				return $result;
			}
		}

		return false;
	}

	/**
	 * Get inventory SQL
	 *
	 * @return string
	 */
	public function _getInventorySql()
	{
		// pull data if
		return '
		(
			-- No stock control, pull the data
				p.inventory_control = "No"
			OR 
			-- Has any stock at all
				p.stock > 0
			OR 
			-- Has Display Out Of Stock rule
				p.inventory_rule = "OutOfStock"
			OR
				(p.inventory_control = "AttrRuleInc" AND p.stock >= 0)
		)
		';
	}
}

<?php

/**
 * Class Shipping
 */
class Shipping
{
	/**
	 * @param $db
	 * @return array|bool
	 */
	public static function getShippingCountries($db)
	{
		$db->query("SELECT country FROM " . DB_PREFIX . "shipping_selected");
		$a = array();
		while ($db->moveNext())
		{
			$cc = explode(",", $db->col["country"]);

			for ($i = 0; $i < count($cc); $i++)
			{
				$cc[$i] = trim($cc[$i]);
				if ($cc[$i] != '' && !in_array($cc[$i], $a)) $a[] = $cc[$i];
			}
		}
		return count($a) > 0 ? $a : false;
	}

	/**
	 * Canada Post - Likuid Communication
	 *
	 * @global type $settings
	 * @global type $cp_xml_result
	 * @global type $cp_orderid
	 * @global type $cp_delivery
	 * @param type $db
	 * @param type $data
	 * @param type $from
	 * @param type $to
	 * @param type $weight
	 * @param type $items
	 * @param type $items_price_based
	 * @param string $error_message
	 *
	 * @return type
	 */
	public static function getCanadaPostShippingPrice($db, $data, $from, $to, $weight, $items, $items_price_based, &$error_message)
	{
		global $settings;
		global $cp_xml_result;
		global $cp_orderid;

		if (!isset($cp_xml_result))
		{
			$db->query("SELECT name FROM " . DB_PREFIX . "states WHERE stid='" . (isset($to["state"]) ? intval($to["state"]) : 0) . "'");
			$to_state = ($db->moveNext() ? $state = $db->col["name"] : " ");

			$CanadaPost = new Shipping_CanadaPost($settings["ShippingCPUrlRating"], $settings["ShippingCPMerchantID"], $settings);
			$CanadaPost->setOrigin($settings["ShippingOriginZip"]);
			$CanadaPost->setDestination(" ", $to_state, $to["country_iso_a2"], $to["zip"]);
			$CanadaPost->setOption("tat", $settings["ShippingCPTAT"]);
			if ($settings["ShippingCPItemsValue"] != "no")
			{
				$CanadaPost->setOption("itemsvalue", $items_price_based);
			}

			$securityId = isset($_COOKIE["_pcod"]) ? $_COOKIE["_pcod"] : "";
			$securityId = $securityId == "" ? isset($_POST["_pcod"]) ? $_POST["_pcod"] : $securityId : $securityId;
			$securityId = $securityId == "" ? isset($_GET["_pcod"]) ? $_GET["_pcod"] : $securityId : $securityId;

			$db->query("SELECT * FROM " . DB_PREFIX . "orders_content oc JOIN " . DB_PREFIX . "orders o ON (oc.oid = o.oid) WHERE security_id='" . $db->escape($securityId) . "'");

			while ($db->moveNext())
			{
				$schar = array('"', "'", '<','>','&');
				$replace   = array('&quot', '&apos;', '&lt;', '&gt;', '&amp;');
				$title = str_replace($schar, $replace, $db->col["title"]);
				$CanadaPost->addItem($db->col["quantity"], $db->col["weight"], 1, 1, 1, $title);
			}

			$CanadaPost->createXMLRequest();
			$cp_xml_result = $CanadaPost->getQuote();
		}

		if (is_array($cp_xml_result))
		{
			foreach ($cp_xml_result as $cp_method)
			{
				if ($cp_method["id"] == $data["method_id"])
				{
					global $cp_delivery;
					$cp_delivery = $cp_method["delivery"];
					$price = $cp_method["rate"];
					if ($price > 0)
					{
						//check advanced fee
						if ($data["fee_type"] == "percent")
						{
							$price = $price + $price / 100 * $data["fee"];
						}

						if ($data["fee_type"] == "amount")
						{
							$price = $price + $data["fee"];
						}
					}
					return normalizeNumber($price);
				}
			}
		}
		$error_message = "Please choose other shipping service.<br>We're so sorry, but selected one is not available for your order.";
		return false;
	}

	/**
	 * USPS Domestic
	 *
	 * @param $db
	 * @param $data
	 * @param $from
	 * @param $to
	 * @param $weight
	 * @param $error_message
	 * @param string $defaultCurrencyCode
	 * @param array $currencyExchangeRates
	 * @return bool|float
	 */
	public static function getUSPSDomesticShippingPrice($db, $data, $from, $to, $weight, &$error_message, $defaultCurrencyCode = 'USD', $currencyExchangeRates = array())
	{
		global $settings;
		$usps = new Shipping_USPS();
		$usps->setUSPSSettings();

		$usps->setDestZip($to["zip"]);
		$usps->setOrigZip($from["zip"]);

		$weightInPounds = self::getWeightInPounds($weight, $settings['LocalizationWeightUnits']);

		$weightPounds = $weightInPounds['pounds']; //floor($weight);
		$weightOunces = $weightInPounds['ounces']; //round(($weight - floor($weight)) * 16, 1);

		if ($data["method_id"] == "First Class")
		{
			$method_name = explode(" ", $data["method_name"]);

			if (isset($method_name[2]))
			{
				$usps->setFirstClassMethod($method_name[2]);
			}
		}

		$usps->setWeight($weightPounds, $weightOunces);
		$usps->setService($data["method_id"]);
		$usps->setMachinable("True");

		$price = $usps->getDomesticPrice();

		if ($price > 0)
		{
			// apply currency exchange rate
			if ($defaultCurrencyCode != 'USD' && isset($currencyExchangeRates['USD']))
			{
				$price = (float)$price / $currencyExchangeRates['USD'];
			}

			//check advanced fee
			if ($data["fee_type"] == "percent")
			{
				$price = $price + $price / 100 * $data["fee"];
			}

			if ($data["fee_type"] == "amount")
			{
				$price = $price + $data["fee"];
			}

			return $price;
		}
		else
		{
			$error_message = "Please choose other shipping service.<br>We're so sorry, but selected one is not available for your order.";
			return false;
		}
	}

	/**
	 * USPS International
	 *
	 * @param $db
	 * @param $data
	 * @param $from
	 * @param $to
	 * @param $weight
	 * @param $error_message
	 * @param string $defaultCurrencyCode
	 * @param array $currencyExchangeRates
	 * @return bool|float|type
	 */
	public static function getUSPSInternationalShippingPrice($db, $data, $from, $to, $weight, &$error_message, $defaultCurrencyCode = 'USD', $currencyExchangeRates = array())
	{
		global $settings;
		$usps = new Shipping_USPS();
		$usps->setUSPSSettings();

		$usps->setDestZip($to["zip"]);
		$usps->setOrigZip($from["zip"]);

		$weightInPounds = self::getWeightInPounds($weight, $settings['LocalizationWeightUnits']);

		$weightPounds = $weightInPounds['pounds'];
		$weightOunces = $weightInPounds['ounces'];
		$usps->setWeight($weightPounds, $weightOunces);

		$usps->setCountry($to["country"]);
		$usps->setCountryCode($to["country_iso_a2"]);
		$usps->setMailType($data["method_id"]);
		$price = $usps->getInternationalPrice();

		if ($price > 0)
		{
			// apply currency exchange rate
			if ($defaultCurrencyCode != 'USD' && isset($currencyExchangeRates['USD']))
			{
				$price = (float)$price / $currencyExchangeRates['USD'];
			}

			//check advanced fee
			if ($data["fee_type"] == "percent")
			{
				$price = $price + $price / 100 * $data["fee"];
			}
			if ($data["fee_type"] == "amount")
			{
				$price = $price + $data["fee"];
			}

			return $price;
		}
		else
		{
			$error_message = "Please choose other shipping service.<br>We're so sorry, but selected one is not available for your order.";
			return false;
		}
	}

	/**
	 * @param $weight
	 * @param string $weightUnit
	 * @return array
	 */
	public static function getWeightInPounds($weight, $weightUnit = 'lbs')
	{
		$weightInPounds = $weight;

		if ($weightUnit == 'kg')
		{
			$weightInPounds = $weight * 2.20462;
		}

		return array(
			'pounds' => floor($weightInPounds),
			'ounces' => round(($weightInPounds - floor($weightInPounds)) * 16 , 1),
		);
	}

	/**
	 * Calculates shipping price (based on method & type)
	 *
	 * @param $db
	 * @param $from
	 * @param $to
	 * @param $items
	 * @param $weight
	 * @param $itemsCount
	 * @param $itemsPriceBasedPrice
	 * @param $ssid
	 * @param $error_message
	 * @param string $defaultCurrencyCode
	 * @param array $currencyExchangeRates
	 * @return bool|float|mixed
	 */
	public static function getShippingPrice($db, $from, $to, $items, $weight, $itemsCount, $itemsPriceBasedPrice, $ssid, &$error_message, $defaultCurrencyCode = 'USD', $currencyExchangeRates = array())
	{
		global $settings;

		$shippingRepository = new DataAccess_ShippingRepository($db);

		$data = $shippingRepository->getShippingMethodData($ssid);

		if ($data)
		{
			$itemsPriceBasedPrice = is_array($itemsPriceBasedPrice) ? $itemsPriceBasedPrice : array('before_discount' => $itemsPriceBasedPrice, 'after_discount' => $itemsPriceBasedPrice);
			$itemsPriceBasedPrice = $itemsPriceBasedPrice[$data['method_calc_mode']];

			switch ($data["carrier_id"])
			{
				case "canada_post" :
				{
					return self::getCanadaPostShippingPrice($db, $data, $from, $to, $weight, $itemsCount, $itemsPriceBasedPrice, $error_message);
				}

				case "usps_domestic" :
				{
					return self::getUSPSDomesticShippingPrice($db, $data, $from, $to, $weight, $error_message, $defaultCurrencyCode, $currencyExchangeRates);
				}
				case "usps_international" :
				{
					return self::getUSPSInternationalShippingPrice($db, $data, $from, $to, $weight, $error_message, $defaultCurrencyCode, $currencyExchangeRates);
				}

				case "ups" :
				{
					$ups = new Shipping_UPS();
					$ups->setServer($settings["ShippingUPSUrl"]);
					$ups->setUserName($settings["ShippingUPSUserID"]);
					$ups->setPass($settings["ShippingUPSUserPassword"]);
					$ups->setAccessKey($settings["ShippingUPSAccessKey"]);
					$ups->setResidential(isset($to["address_type"]) && ($to["address_type"] == "Residential") ? 1 : 0);
					$ups->setPickupType($settings["ShippingUPSRateChart"]);
					$ups->setContainer($settings["ShippingUPSContainer"]);
					$ups->setWeightUnit(strtoupper($settings["LocalizationWeightUnits"]) == "LBS" ? "LBS" : "KGS");
					$ups->setLengthUnit($settings["LocalizationLengthUnits"]);
					$ups->setCountry($to["country_iso_a2"], $from["country_iso_a2"]);
					$ups->setDestZip($to["zip"]);
					$ups->setOrigZip($from["zip"]);
					$ups->setWeight($weight);
					$ups->setService($data["method_id"]);

					$ups->setDefaultCurrency($defaultCurrencyCode);
					$ups->setCurrencyExchangeRates($currencyExchangeRates);

					$ups->setShippingFeeType($data["fee_type"]);
					$ups->setShippingFeeValue($data["fee"]);

					return $ups->getShippingRate($error_message);
				}

				case "fedex_api_express_us" :
				case "fedex_api_freight_us" :
				case "fedex_api_ground_us" :
				case "fedex_api_intr" :
				case "fedex_api_intr_freight" :
				{
					$fedex = new Shipping_Fedex();
					$fedex->setApiKey($settings['ShippingFedExApiKey']);
					$fedex->setApiPassword($settings['ShippingFedExApiPassword']);
					$fedex->setShippingAccount($settings['ShippingFedExAccountNumber']);
					$fedex->setBillingAccount($settings['ShippingFedExBillingAccountNumber']);
					$fedex->setFreightAccount($settings['ShippingFedExFreightAccountNumber']);
					$fedex->setMeterNumber($settings['ShippingFedExMeter']);

					$fedex->setWeightUnits($settings['LocalizationWeightUnits'] == 'kg' ? 'KG' : 'LB');
					$fedex->setTotalWeight($weight);
					$fedex->setOriginAddress($from);
					$fedex->setDestinationAddress($to);
					$fedex->setServiceType($data['method_id']);
					$fedex->setRateRule($settings["ShippingFedExRateRule"]);
					$fedex->setRateRequestType($settings["ShippingFedExRateRequestType"]);

					$fedex->setFreightClass($settings['ShippingFedExFreightClass']);
					$fedex->setFreightPackaging($settings['ShippingFedExFreightPackaging']);
					$fedex->setFreightPackagingWeight($settings['ShippingFedExFreightPackagingWeight']);
					$fedex->setDropOffType($settings['ShippingFedExDropOffType']);

					$fedex->setPreferredCurrency($defaultCurrencyCode);
					$fedex->setDefaultCurrency($defaultCurrencyCode);
					$fedex->setCurrencyExchangeRates($currencyExchangeRates);

					$fedex->setShippingFeeType($data["fee_type"]);
					$fedex->setShippingFeeValue($data["fee"]);

					return $fedex->getShippingRate($error_message);
				}

				case "vparcel" :
				{
					$vparcel = new Shipping_VIPparcel($db, $settings, $data);
					$vparcel->setFrom($from);
					$vparcel->setTo($to);
					$vparcel->setWeight($weight);
					$vparcel->setItemsCount($itemsCount);
					$vparcel->setItemsPriceBasedPrice($itemsPriceBasedPrice);
					$vparcel->setShippingFeeType($data['fee_type']);
					$vparcel->setShippingFeeValue($data['fee']);
					return $vparcel->getShippingRate();
				}

				case "custom" :
				{
					$custom = new Shipping_Custom($shippingRepository);

					$custom->setShippingMethodId($data["ssid"]);
					$custom->setShippingMethodType($data["method_id"]);
					$custom->setItems($items);
					$custom->setWeight($weight);
					$custom->setItemsCount($itemsCount);
					$custom->setItemsPriceBasedPrice($itemsPriceBasedPrice);

					return $custom->getShippingRate($error_message);
				}

				case "doba" :
				{
					$dobaOrder = new Doba_OrderApi($settings, $db);

					$dobaItems = array();
					foreach ($items as $item)
					{
						$dobaItems[] = array('item_id' => $item['product_id'], 'quantity' => $item['quantity']);
					}

					return $dobaOrder->getShippingQuote($to, $dobaItems);
				}

				case "free" :
				case 'virtual' :
				case 'digital' :
				{
					return 0;
				}

				case 'zip_code':
				{
					$zipCode = new Shipping_ZipCode($from, $to, $shippingRepository, $settings);

					$zipCode->setShippingMethodId($data['ssid']);
					$zipCode->setShippingMethodType($data['method_id']);
					$zipCode->setWeight($weight);
					$zipCode->setItemsCount($itemsCount);
					$zipCode->setItemsPriceBasedPrice($itemsPriceBasedPrice);

					return $zipCode->getShippingRate($error_message);
				}
			}
		}

		return false;
	}

	/**
	 * GET SHIPPING QUOTES
	 * If handling included in shipping, returns data with handling included
	 *
	 * @param DB $db
	 * @param $settings
	 * @param USER $user
	 * @param ORDER $order
	 * @param bool $shippingQuoteCall
	 * @param bool $shippingQuoteAddress
	 * @param bool $ignoreFreeShipping
	 * @param string $defaultCurrencyCode
	 * @param array $currencyExchangeRates
	 * @param bool $bongoActive
	 * @param $bongoUsed
	 * @return array
	 */
	public static function getShippingQuotes(
		DB $db, &$settings, USER $user, ORDER $order,
		$shippingQuoteCall = false, $shippingQuoteAddress = false, $ignoreFreeShipping = false,
		$defaultCurrencyCode = 'USD', $currencyExchangeRates = array(),
		$bongoActive = false, &$bongoUsed
	)
	{
		/** @var DataAccess_ShippingRepository $shippingRepository */
		$shippingRepository = new DataAccess_ShippingRepository($db);

		/**
		 * Get order items
		 */
		$orderItems = $shippingRepository->getOrderItems($order->getId());

		/**
		 * Get shipping origin address
		 */
		$addressFrom = $shippingRepository->getShippingOriginAddress($order->getId(), $settings);

		/**
		 * Get shipping destination address
		 */
		if ($shippingQuoteCall)
		{
			$addressTo = $shippingQuoteAddress;
		}
		else
		{
			$addressData = $shippingRepository->getOrderShippingAddress($order->getId());

			$addressTo = array(
				'address_type' => $addressData['shipping_address_type'],
				'name' => $addressData['shipping_name'],
				'address1' => $addressData['shipping_address1'],
				'address2' => $addressData['shipping_address2'],
				'city' => $addressData['shipping_city'],
				'state' => $addressData['state_name'],
				'state_name' => $addressData['state_name'],
				'state_id' => $addressData['state_id'],
				'state_abbr' => $addressData['state_abbr'],
				'state_code' => $addressData['state_abbr'],
				'country' => $addressData['country_name'],
				'country_name' => $addressData['country_name'],
				'country_id' => $addressData['country_id'],
				'country_iso_a2' => $addressData['country_iso_a2'],
				'country_iso_a3' => $addressData['country_iso_a3'],
				'country_iso_number' => $addressData['country_iso_number'],
				'zip' => strtoupper($addressData['country_iso_a2']) == 'US' ? substr(trim($addressData['shipping_zip']), 0, 5) : trim($addressData['shipping_zip'])
			);
		}

		$bongoUsed = false;

		// delivery to bongo instead of international address
		if ($bongoActive && strtoupper($addressTo['country_iso_a2']) != 'US')
		{
			$addressTo = Payment_Bongo_BongoUtil::getBongoShippingAddress($db, $settings, $addressTo, $bongoUsed);
		}

		// TODO: why do we check it here?
		$order->checkShippingRequired();

		$shipping_error = isset($shipping_error) ? $shipping_error : false;
		$shipping_error_message = isset($shipping_error_message) ? $shipping_error_message : "";

		/**
		 * Get promo and discounts percentages
		 */
		$promoDiscountPercent = $order->getPromoDiscountAmount() > 0 ? $order->getPromoDiscountType() == 'percent' ? $order->getPromoDiscountValue() : $order->getPromoDiscountValue() / ($order->getSubtotalAmount() / 100) : 0;
		$discountPercent = $order->getDiscountAmount() > 0 ? $order->getDiscountType() == "percent" ? $order->getDiscountValue() : $order->getDiscountValue() / ($order->getSubtotalAmount() / 100) : 0;

		/**
		 * Select ALL available shipping methods
		 */
		$activeShippingMethods = $shippingRepository->getActiveShippingMethods();
		$availableMethods = array();
		$dobaMethod = null;
		$freeMethod = null;
		$virtualMethod = null;
		$digitalMethod = null;
		$shippingMethodsCount = 0;

		/**
		 * Get shipping methods applicable to shipping location
		 * TODO: must be moved under shipments cycle when shipments will have own shipping address
		 */
		foreach ($activeShippingMethods as $activeShippingMethod)
		{
			$acceptCountries = explode(',', $activeShippingMethod['country']);
			$acceptStates = explode(',', $activeShippingMethod['state']);

			/**
			 * Check do we ship to selected country / state
			 */
			if (
				// skip free & unshippable methods
				(in_array($activeShippingMethod['carrier_id'], array('free', 'unshippable'))) ||
				// hande the rest
				(
					($activeShippingMethod['country'] == 0 || in_array($addressTo['country_id'], $acceptCountries)) &&
					(($activeShippingMethod['country'] != 0) || ($activeShippingMethod['country'] == 0 && $addressTo['country_id'] != $settings['ShippingOriginCountry'])) &&
					($activeShippingMethod['country_exclude'] != $addressTo['country_id'])
				)
			)

			{
				if ($activeShippingMethod['state'] == 0 || in_array($addressTo['state_id'], $acceptStates))
				{
					// TODO: should be governed in a different way
					if ($activeShippingMethod['carrier_id'] == 'doba' && $activeShippingMethod['method_id'] == 'doba')
					{
						$dobaMethod = $activeShippingMethod;
					}
					else if ($activeShippingMethod['carrier_id'] == 'free' && $activeShippingMethod['method_id'] == 'free')
					{
						$freeMethod = $activeShippingMethod;
						$freeMethod['shipping_price'] = 0;
					}
					else if ($activeShippingMethod['carrier_id'] == 'unshippable' && $activeShippingMethod['method_id'] == 'virtual')
					{
						$virtualMethod = $activeShippingMethod;
						$virtualMethod['shipping_price'] = 0;
					}
					else if ($activeShippingMethod['carrier_id'] == 'unshippable' && $activeShippingMethod['method_id'] == 'digital')
					{
						$digitalMethod = $activeShippingMethod;
						$digitalMethod['shipping_price'] = 0;
					}
					else
					{
						$availableMethods[] = $activeShippingMethod;
					}
				}
			}
		}

		/**
		 * Get shipping rates for each shipment
		 */
		$shipments = $order->getShipments();
		foreach ($shipments as $shipmentId => &$shipment)
		{
			$shipment['items'] = array();

			$shippingMethods = false;
			$shippingAmountForPriceBased = 0;
			$shippingAmountForPriceBasedExists = false;

			$zipCodeEligible = false;
			$orderBy = null;
			$deliverBy = null;
			$dobaItems = array();
			$virtualItems = array();
			$digitalItems = array();

			$shippingWeight = 0;
			$shippingItemsCount = 0;

			$itemsTotalCount = 0;
			$itemsFreeShippingCount = 0;

			/**
			 * Sort items by shipments, get quantity, weight, and other data per shipment
			 */
			foreach ($orderItems as $item)
			{
				if ($item['shipment_id'] == $shipmentId)
				{
					$itemsTotalCount++;
					if ($item['free_shipping'] == 'Yes') $itemsFreeShippingCount++;

					$shipment['items'][$item['ocid']] = $item;

					if ($item['same_day_delivery'] == '1' && $settings['PikflyEnabled'] == 'Yes') $zipCodeEligible = true;

					switch ($shipment['shipping_type'])
					{
						case 'product': break;
						case 'doba' : $dobaItems[] = array('item_id' => $item['product_id'], 'quantity' => $item['quantity']); break;
						case 'virtual' : $virtualItems[] = $item; break;
						case 'digital' : $digitalItems[] = $item; break;
						case 'local': break;
						default:
						{
							if ($item['product_type'] == Model_Product::TANGIBLE && ($item['free_shipping'] == 'No' || ($item['free_shipping'] == 'Yes' && $ignoreFreeShipping)))
							{
								$shippingItemsCount += $item['quantity'];

								if ($item['weight'] > 0)
								{
									$shippingWeight += $item['weight'] * $item['quantity'];
								}

								if ($item['price'] > 0)
								{
									$shippingAmountForPriceBased += $item['price'] * $item['quantity'];
									$shippingAmountForPriceBasedExists = true;
								}
							}
						}
					}
				}
			}

			$addFreeShippingMethod = !$ignoreFreeShipping && ($itemsTotalCount == $itemsFreeShippingCount);

			/**
			 * Get shipment shipping methods and calculate shipping price per shipment per method
			 */
			switch ($shipment['shipping_type'])
			{
				case 'product':
				{
					$item = reset($shipment['items']);
					// TODO: add shipping price per each or per order
					// TODO: move into repository
					$shippingMethods = $db->selectAll('
						SELECT ss.*,
							ss.carrier_name AS method_name,
							psp.price * ' . intval($item['quantity']) . ' AS shipping_price
						FROM ' . DB_PREFIX . 'products_shipping_price psp
						INNER JOIN ' . DB_PREFIX . 'shipping_selected ss ON psp.ssid = ss.ssid AND ss.hidden="No"
						WHERE psp.pid=' . intval($item['pid']) . ' AND  psp.is_price="Yes"
					');

					break;
				}
				case 'doba' :
				{
					if ($dobaMethod != null)
					{
						$dobaOrder = new Doba_OrderApi($settings, $db);

						$shippingAmountDoba = $dobaOrder->getShippingQuote($addressTo, $dobaItems);

						if ($shippingAmountDoba != false)
						{
							$dobaMethod['shipping_price'] = $shippingAmountDoba;
							$shippingMethods = array($dobaMethod['ssid'] => $dobaMethod);
						}
					}
					break;
				}
				case 'virtual' :
				{
					if ($virtualMethod != null && count($virtualItems) > 0)
					{
						$virtualMethod['shipping_price'] = 0;
						$shippingMethods = array($virtualMethod['ssid'] => $virtualMethod);
					}
					break;
				}
				case 'digital' :
				{
					if ($digitalMethod != null && count($digitalItems) > 0)
					{
						$digitalMethod['shipping_price'] = 0;
						$shippingMethods = array($digitalMethod['ssid'] => $digitalMethod);
					}
					break;
				}
				case 'local':
				default:
				{
					$shippingMethods = array();

					if ($addFreeShippingMethod && $freeMethod)
					{
						$shippingMethods[$freeMethod['ssid']] = $freeMethod;
					}

					if (count($availableMethods))
					{
						if ($shippingAmountForPriceBasedExists)
						{
							$shippingAmountForPriceBasedDiscounted = $shippingAmountForPriceBased -
								$shippingAmountForPriceBased / 100 * $promoDiscountPercent -
								$shippingAmountForPriceBased / 100 * $discountPercent;
						}
						else
						{
							$shippingAmountForPriceBasedDiscounted = $shippingAmountForPriceBased;
						}

						/**
						 * Check ranges
						 */
						foreach ($availableMethods as $availableMethod)
						{
							$availableMethod['shipping_price'] = 0;
							$mayBeAdded = false;

							// check custom shipping methods
							if ($availableMethod['carrier_id'] == 'custom' && $availableMethod['method_id'] != 'product')
							{
								$rateValue = null;

								// check items count, price based and weight ranges
								if ($availableMethod['method_id'] == 'flat')
								{
									$rateValue = $shippingItemsCount;
								}
								else if ($availableMethod['method_id'] == 'price' && $shippingAmountForPriceBasedExists)
								{
									$rateValue = $availableMethod['method_calc_mode'] == 'before_discount' ? $shippingAmountForPriceBased : $shippingAmountForPriceBasedDiscounted;
								}
								else if ($availableMethod['method_id'] == 'base_weight' || $availableMethod['method_id'] == 'weight')
								{
									$rateValue = $shippingWeight;
								}

								if ($rateValue !== null)
								{
									if ($db->selectOne('SELECT * FROM ' . DB_PREFIX . 'shipping_custom_rates WHERE ssid=' . intval($availableMethod['ssid']) . ' AND rate_min<=' . floatval($rateValue) . ' AND (rate_max = 0 OR rate_max >= ' . floatval($rateValue) . ') ORDER BY rate_min LIMIT 1'))
									{
										$mayBeAdded = true;
									}
								}
							}
							else if ($availableMethod['carrier_id'] == 'zip_code')
							{
								$mayBeAdded = $zipCodeEligible;
							}
							//now for Fedex, UPS and USPS
							else
							{
								//check weight is in allowed range
								if ($availableMethod['weight_min'] <= $shippingWeight && $availableMethod['weight_max'] >= $shippingWeight && $availableMethod['exclude'] == 'No')
								{
									$mayBeAdded = true;
								}
								//check weight is now in disallowed range
								if ($availableMethod['weight_min'] >= $shippingWeight && $availableMethod['weight_max'] <= $shippingWeight && $availableMethod['exclude'] == 'Yes')
								{
									$mayBeAdded = true;
								}
							}

							if ($shipment['shipping_type'] == 'local' && $settings['PikflyShowAlternativeMethods'] != 'Yes' && $availableMethod['carrier_id'] != 'zip_code')
							{
								$mayBeAdded = false;
							}

							/**
							 * Calc shipping price
							 */
							if ($mayBeAdded)
							{
								$error_message = '';

								$shippingMethod = Shipping_MethodFactory::getMethod($availableMethod['ssid']);

								$resultShippingPrice = $shippingMethod->calculate(
									$db, $addressFrom, $addressTo,
									$shipment['items'],
									$shippingWeight,
									$shippingItemsCount,
									array(
										'before_discount' => $shippingAmountForPriceBased,
										'after_discount' => $shippingAmountForPriceBasedDiscounted
									), //price based
									$error_message,
									$defaultCurrencyCode,
									$currencyExchangeRates
								);

								if ($error_message != '' || $resultShippingPrice === false)
								{
									$mayBeAdded = false;
								}
								else
								{
									$resultShippingPrice = PriceHelper::round($resultShippingPrice);
									$availableMethod['shipping_price'] = $resultShippingPrice;
								}

								if ($mayBeAdded)
								{
									$availableMethod['method_name'] = gs($availableMethod['carrier_name']) . ($availableMethod['carrier_id'] == 'custom' || $availableMethod['carrier_id'] == 'zip_code' ? '' : ' - ' . gs($availableMethod['method_name']));
									$shippingMethods[$availableMethod['ssid']] = $availableMethod;
								}
							}
						}
					}
				}
			}

			/**
			 * TODO: Review this code
			 */
			$timeHelper = new Pikfly_TimeHelper($db, $settings);

			if (is_array($shippingMethods) && count($shippingMethods) > 0)
			{
				foreach ($shippingMethods as &$m)
				{
					$found = false;
					if ($shipment['shipping_type'] == 'local')
					{
						$timeResult = $timeHelper->getNextDeliveryByMethod($m['ssid']);
						if ($timeResult)
						{
							$found = true;
							$orderByTime = $timeResult['orderByTime'];
							$deliverByTime = $timeResult['deliverByTime'];
							$m['orderByTime'] = $orderByTime->format('g:i A');
							$m['deliverByTime'] = $deliverByTime->format('g:i A');
							$m['deliveryDay'] = $timeResult['deliveryDay'];
						}
					}
					if (!$found)
					{
						$m['orderByTime'] = null;
						$m['deliverByTime'] = null;
						$m['deliveryDay'] = null;
					}
					unset($m);
				}
			}

			/**
			 * TODO: End of review this code
			 */
			$shipment['shipping_methods'] = $shippingMethods;
			$shipment['shipping_methods_count'] = $shippingMethods ? count($shippingMethods) : 0;

			$shippingMethodsCount += $shipment['shipping_methods_count'];
		}

		/**
		 * Add handling to shipping prices
		 */

		if ($settings['ShippingHandlingSeparated'] != '1')
		{
			// get handling amount when shipping is not free and price is not 0
			$handlingAmount = $order->calculateHandling($order->isShippingFree(), false, true);

			if ($shipments)
			{
				$shipmentsCount = count($shipments);

				$handlingAmountPerShipment = floor(($handlingAmount / $shipmentsCount) * 100) / 100;
				$handlingAmountPerFirstShipment = $handlingAmount - ($shipmentsCount - 1) * $handlingAmountPerShipment;

				$first = true;
				foreach ($shipments as $shipmentId => &$shipment)
				{
					$shipment['handling_separated'] = false;
					$shipment['handling_amount'] = $first ? $handlingAmountPerFirstShipment : $handlingAmountPerShipment;
					$first = false;
				}
			}
		}
		else
		{
			foreach ($shipments as $shipmentId => &$shipment)
			{
				$shipment['handling_separated'] = false;
				$shipment['handling_amount'] = 0;
			}
		}

		unset($_SESSION['USPS_InternationalCacheResponse']);
		unset($_SESSION['USPS_InternationalCacheResponseEnvelope']);
		unset($_SESSION['USPS_InternationalCacheResponsePackage']);
		unset($_SESSION['USPS_InternationalCacheResponsePostcardsorAerogrammes']);

		return
			array(
				'shipments' => $shipments,
				'shipping_methods_count' => $shippingMethodsCount,
				'shipping_error' => $shipping_error,
				'shipping_error_message' => $shipping_error_message,
				//'shipping_weight' => $shippingWeight
			);
	}

	/**
	 * Filter alternative shipping quotes (ignored free products) by comparing to standard shipping quotes and removing duplicates
	 *
	 * @param $rates
	 * @param $ratesA
	 *
	 * @return array|bool
	 */
	public static function filterAlternativeShippingQuotes($rates, $ratesA)
	{
		if ($ratesA && is_array($ratesA) && isset($ratesA['shipments']) && is_array($ratesA['shipments']))
		{
			$shippingMethodsCount = 0;

			foreach ($ratesA['shipments'] as $shipmentId => $shipment)
			{
				if (isset($shipment['shipping_methods']) && is_array($shipment['shipping_methods']))
				{
					$_rates = array();

					// combine rates
					foreach ($shipment['shipping_methods'] as $rateA)
					{
						$ssidFound = false;

						// add alternative method only when price is different or it does not existing in standard methods
						if ($rates && isset($rates['shipments'][$shipmentId]['shipping_methods']) && is_array($rates['shipments'][$shipmentId]['shipping_methods']))
						{
							foreach ($rates['shipments'][$shipmentId]['shipping_methods'] as $rate)
							{
								if ($rateA['ssid'] == $rate['ssid'])
								{
									$ssidFound = true;
									if ($rateA['shipping_price'] != $rate['shipping_price']) $_rates[] = $rateA;
								}
							}
						}

						if (!$ssidFound) $_rates[] = $rateA;
					}

					$ratesA['shipments'][$shipmentId]['shipping_methods'] = $_rates;
					$ratesA['shipments'][$shipmentId]['shipping_methods_count'] = count($_rates);

					$shippingMethodsCount += $ratesA['shipments'][$shipmentId]['shipping_methods_count'];
				}
			}

			$_ratesA = $ratesA;

			foreach ($ratesA['shipments'] as $shipmentId => $shipment)
			{
				if ($shipment['shipping_methods_count'] < 1)
				{
					unset($_ratesA['shipments'][$shipmentId]);
				}
			}

			$ratesA = $_ratesA;
			$ratesA['shipping_methods_count'] = $shippingMethodsCount;

			return $ratesA;
		}

		return false;
	}
}

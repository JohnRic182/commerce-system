<?php

class Pikfly_Admin_Controller_Pikfly extends Admin_Controller
{
	protected static $zipCodeColors = array(
		'1' => '#1c87f4',
		'2' => '#cc37d3',
		'3' => '#e88612',
		'4' => '#e31d55',
		'0' => '#3fc715',
	);

	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$adminView = $this->getView();

		$adminView->assign('deliveryAreas', $this->toViews($this->getDeliveryAreas()));

		$adminView->assign('delete_nonce', Nonce::create('pikfly_area_delete'));
		$adminView->assign('currency', $this->getCurrency());

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9032');

		$adminView->assign('body', 'templates/pages/pikfly/list.html');
		$adminView->render('layouts/default');
	}

	public function addAreaAction()
	{
		$nextPosition = 1;
		$deliveryAreas = $this->getDeliveryAreas();
		foreach ($deliveryAreas as $a)
		{
			if ($a['position'] >= $nextPosition)
			{
				$nextPosition = $a['position'] + 1;
			}
		}

		$deliveryArea = array(
			'id' => null,
			'ssid' => null,
			'zip_codes' => '',
			'before_time' => '14:00',
			'delivery_days' => 'Monday,Tuesday,Wednesday,Thursday,Friday',
			'last_delivery_time' => '20:00',
			'delivery_fee' => '',
			'delivery_fee_type' => 'amount',
			'position' => $nextPosition,
		);

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$this->handleAreaAction($deliveryArea, 'add');
			return;
		}

		$adminView = $this->getView();

		$adminView->assign('nonce', Nonce::create('pikfly_area_add'));
		$adminView->assign('deliveryAreas', $this->toViews($deliveryAreas));
		$adminView->assign('deliveryArea', $this->toView($deliveryArea));
		$adminView->assign('currency', $this->getCurrency());

		$adminView->assign('PikflyApiUrl', $this->getPikflyApiUrl());

		$stateCode = '';
		$db = $this->getDb();
		$settings = $this->getSettings();
		$stateData = $db->selectOne('SELECT short_name FROM '.DB_PREFIX.'states WHERE stid = '.intval($settings->get('ShippingOriginState')));
		if ($stateData)
		{
			$stateCode = $stateData['short_name'];
		}
		$adminView->assign('ShippingOriginStateCode', $stateCode);

		$adminView->assign('mode', 'add-area');

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9032');

		$adminView->assign('PikflyToken', '7fff9e0c-0c64-46aa-92ec-eb57885188aa');

		$adminView->assign('body', 'templates/pages/pikfly/edit-area.html');
		$adminView->render('layouts/default');
	}

	public function editAreaAction()
	{
		$request = $this->getRequest();

		$ssid = intval($request->get('id'));

		$deliveryArea = $this->getDeliveryArea($ssid);

		if (!$deliveryArea)
		{
			//TODO: Message
			$this->redirect('app', array('key' => 'pikfly'));
			return;
		}

		if ($request->isPost())
		{
			$this->handleAreaAction($deliveryArea, 'edit');
			return;
		}

		$deliveryAreas = $this->getDeliveryAreas();
		$idx = false;
		foreach ($deliveryAreas as $i => $a)
		{
			if ($a['ssid'] == $ssid)
			{
				$idx = $i;
				break;
			}
		}
		if ($idx) unset($deliveryAreas[$idx]);

		$adminView = $this->getView();

		$adminView->assign('nonce', Nonce::create('pikfly_area_edit'));
		$adminView->assign('deliveryAreas', $this->toViews($deliveryAreas));
		$adminView->assign('deliveryArea', $this->toView($deliveryArea));
		$adminView->assign('currency', $this->getCurrency());

		$adminView->assign('PikflyApiUrl', $this->getPikflyApiUrl());

		$stateCode = '';
		$db = $this->getDb();
		$settings = $this->getSettings();
		$stateData = $db->selectOne('SELECT short_name FROM '.DB_PREFIX.'states WHERE stid = '.intval($settings->get('ShippingOriginState')));
		if ($stateData)
		{
			$stateCode = $stateData['short_name'];
		}
		$adminView->assign('ShippingOriginStateCode', $stateCode);

		$adminView->assign('mode', 'edit-area');

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9032');

		$adminView->assign('PikflyToken', '7fff9e0c-0c64-46aa-92ec-eb57885188aa');

		$adminView->assign('body', 'templates/pages/pikfly/edit-area.html');
		$adminView->render('layouts/default');
	}

	protected function handleAreaAction(&$deliveryArea, $mode = 'add')
	{
		$request = $this->getRequest();
		if (!Nonce::verify($request->get('nonce'), 'pikfly_section_'.$mode, false))
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => trans('common.invalid_nonce')
			));

			return;
		}

		$deliveryArea['zip_codes'] = $request->request->get('zip_codes');
		$deliveryArea['before_time_hour'] = $request->request->get('before_time_hour');
		$deliveryArea['before_time_am_pm'] = $request->request->get('before_time_am_pm');
		$deliveryArea['delivery_days'] = $request->request->getArray('delivery_days');
		$deliveryArea['last_delivery_time_hour'] = $request->request->get('last_delivery_time_hour');
		$deliveryArea['last_delivery_time_am_pm'] = $request->request->get('last_delivery_time_am_pm');
		$deliveryArea['delivery_fee'] = $request->request->get('delivery_fee');
		$deliveryArea['delivery_fee_type'] = $request->request->get('delivery_fee_type');

		$result = array(
			'status' => 0,
		);
		//TODO: Validation

		$before_time_hour = intval($deliveryArea['before_time_hour']);
		$before_time_am_pm = trim($deliveryArea['before_time_am_pm']) != '' ? $deliveryArea['before_time_am_pm'] : 'PM';

		if (strtolower($before_time_am_pm) == 'pm' && $before_time_hour < 12)
		{
			$before_time_hour += 12;
		}
		$deliveryArea['before_time'] = $before_time_hour.':00:00';

		$last_delivery_time_hour = intval($deliveryArea['last_delivery_time_hour']);
		$last_delivery_time_am_pm = trim($deliveryArea['last_delivery_time_am_pm']) != '' ? $deliveryArea['last_delivery_time_am_pm'] : 'PM';

		if (strtolower($last_delivery_time_am_pm) == 'pm' && $last_delivery_time_hour < 12)
		{
			$last_delivery_time_hour += 12;
		}
		$deliveryArea['last_delivery_time'] = $last_delivery_time_hour.':00:00';

		$this->persistDeliveryArea($deliveryArea);

		$result['status'] = 1;

		$this->renderJson($result);
		return;
	}

	public function deleteAreaAction()
	{
		$request = $this->getRequest();

		$ssid = intval($request->get('id'));

		$nonce = $request->request->get('nonce');

		if (!Nonce::verify($nonce, 'pikfly_area_delete', false))
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => trans('common.invalid_nonce'),
			));
			return;
		}

		//TODO: Handle re-ordering positions
		$this->removeDeliveryArea($ssid);

		$this->renderJson(array(
			'status' => 1,
		));
	}

	public function settingsAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$settingsDefaults = array(
			'settings' => array(
				'PikflyTimezone' => '', 'PikflyShowAlternativeMethods' => 'No',
			),
			'address' => array(
				'ShippingOriginName' => '', 'ShippingOriginAddressLine1' => '', 'ShippingOriginAddressLine2' => '',
				'ShippingOriginCity' => '', 'ShippingOriginCountry' => '1', 'ShippingOriginState' => '',
				'ShippingOriginProvince' => '', 'ShippingOriginZip' => '', 'ShippingOriginPhone' => ''
			),
		);

		// get accepted settings arrays
		$accepted = array();
		foreach ($settingsDefaults as $settingsGroupKey => $settingsGroup)
		{
			$accepted[$settingsGroupKey] = array_keys($settingsGroup);
		}

		$data = array();
		foreach ($accepted as $settingsGroupKey => $settingsGroup)
		{
			$data[$settingsGroupKey] = $settings->getByKeys($settingsGroup);
			if (isset($data[$settingsGroupKey]['ShippingOriginCity']) && $data[$settingsGroupKey]['ShippingOriginCity'] == 'City')
			{
				$data[$settingsGroupKey]['ShippingOriginCity'] = '';
			}
		}

		$form = false;
		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'pikfly_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('pikfly', array('mode' => 'settings'));
				return;
			}

			$mode = $request->get('mode');
			if ($mode == 'address' || $mode == 'settings')
			{
				$settingsData = array_merge($settingsDefaults[$mode], $request->request->all());
				$settings->persist($settingsData);

				Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
				$this->redirect('pikfly', array('mode' => 'settings'));
				return;
			}
		}

		$countryRepository = new DataAccess_CountryRepository($this->getDb());

		$countriesStates = $countryRepository->getCountriesStatesForSettings();

		$formBuilder = new Admin_Form_ShippingAdvancedSettingsForm();

		$adminView = $this->getView();

		$adminView->assign('countriesStates', json_encode($countriesStates));
		$adminView->assign('shippingAddressForm', $formBuilder->getOriginAddressForm($data['address']));

		$countryId = intval($data['address']['ShippingOriginCountry']);
		$stateId = intval($data['address']['ShippingOriginState']);

		$data['address']['ShippingOriginCountryName'] = 'n/a';
		$data['address']['ShippingOriginStateName'] = 'n/a';

		foreach ($countriesStates as $country)
		{
			if ($country['id'] == $countryId)
			{
				$data['address']['ShippingOriginCountryName'] = $country['name'];
				if ($country['states'] && isset($country['states'][$stateId]))
				{
					$data['address']['ShippingOriginStateName'] = $country['states'][$stateId]['name'];
				}
				else
				{
					$data['address']['ShippingOriginStateName'] = $data['address']['ShippingOriginProvince'];
				}

				break;
			}
		}

		if (trim($data['settings']['PikflyTimezone'] == ''))
		{
			$db = $this->getDb();
			$stateCode = null;
			$stateData = false;
			$stateId = intval($data['address']['ShippingOriginState']);
			if ($stateId > 0)
			{
				$stateData = $db->selectOne('SELECT short_name FROM '.DB_PREFIX.'states WHERE stid = '.$stateId);
			}
			else
			{
				$stateName = $settings->get('CompanyState');
				if (trim($stateName) != '')
				{
					$stateData = $db->selectOne('SELECT short_name FROM '.DB_PREFIX.'states WHERE name = "'.$db->escape($stateName).'"');
				}
			}
			if ($stateData)
			{
				$stateCode = $stateData['short_name'];
			}

			if ($stateCode)
			{
				$timezonesMap = array(
					'eastern' => array('ME','VT','NH','MA','RI','CT','NY','NJ','MI','PA','IN','OH','DE','MD','DC','KY',
						'WV','VA','NC','SC','GA','FL'),
					'central' => array('ND','SD','MN','WI','NE','IA','IL','KS','MO','OK','AR','TN','TX','LA','MS','AL'),
					'mountain' => array('MT','ID','WY','UT','CO','AZ','NM'),
					'pacific' => array('WA','OR','CA','NV'),
					'alaskan' => array('AK'),
					'hawaiian' => array('HI'),
				);

				foreach ($timezonesMap as $timezone => $stateCodes)
				{
					if (in_array($stateCode, $stateCodes))
					{
						$data['settings']['PikflyTimezone'] = $timezone;
						break;
					}
				}
			}
		}
		$adminView->assign('shippingAddressData', $data['address']);

		$formBuilder = new Admin_Form_PikflySettingsForm();
		$adminView->assign('settingsForm', $formBuilder->getBuilder($data['settings']));

		$adminView->assign('settingsNonce', Nonce::create('pikfly_settings'));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9032');

		$adminView->assign('body', 'templates/pages/pikfly/settings.html');
		$adminView->render('layouts/default');
	}

	protected function &getDeliveryAreas()
	{
		$db = $this->getDb();

		$deliveryAreas = $db->selectAll('SELECT m.*
FROM '.DB_PREFIX.'shipping_selected s
	INNER JOIN '.DB_PREFIX.'shipping_zip_methods m ON s.ssid = m.ssid
WHERE carrier_id = "zip_code"
ORDER BY s.ssid ASC');

		return $deliveryAreas;
	}

	protected function &getDeliveryArea($ssid)
	{
		$db = $this->getDb();

		$deliveryArea = $db->selectOne('SELECT m.*
FROM '.DB_PREFIX.'shipping_selected s
	INNER JOIN '.DB_PREFIX.'shipping_zip_methods m ON s.ssid = m.ssid
WHERE carrier_id = "zip_code" AND s.ssid = '.intval($ssid));

		return $deliveryArea;
	}

	protected function removeDeliveryArea($ssid)
	{
		$db = $this->getDb();

		$deliveryArea = $db->selectOne('SELECT * FROM '.DB_PREFIX.'shipping_zip_methods WHERE ssid = '.intval($ssid));
		if ($deliveryArea)
		{
			$db->query('DELETE FROM '.DB_PREFIX.'shipping_zip_methods WHERE ssid = '.intval($ssid));
			$db->query('DELETE FROM '.DB_PREFIX.'shipping_selected WHERE ssid = '.intval($ssid));
		}
	}

	protected function &toViews(&$deliveryAreas)
	{
		$ret = array();

		foreach ($deliveryAreas as &$deliveryArea)
		{
			$ret[] = $this->toView($deliveryArea);
		}

		return $ret;
	}

	protected function &toView(&$deliveryArea)
	{
		$before_time = strtotime($deliveryArea['before_time']);
		$last_delivery_time = strtotime($deliveryArea['last_delivery_time']);

		$ret = array(
			'id' => isset($deliveryArea['id']) ? $deliveryArea['id'] : '',
			'ssid' => isset($deliveryArea['ssid']) ? $deliveryArea['ssid'] : '',
			'title' => trans('pikfly.delivery_area').' '.$deliveryArea['position'],
			'zip_codes' => implode(', ', explode(',', $deliveryArea['zip_codes'])),
			'before_time' => date('g:i A', $before_time),
			'before_time_hour' => date('g', $before_time),
			'before_time_am_pm' => strtolower(date('A', $before_time)),
			'delivery_days' => explode(',', $deliveryArea['delivery_days']),
			'last_delivery_time' => date('g:i A', $last_delivery_time),
			'last_delivery_time_hour' => date('g', $last_delivery_time),
			'last_delivery_time_am_pm' => strtolower(date('A', $last_delivery_time)),
			'delivery_fee' => $deliveryArea['delivery_fee'],
			'delivery_fee_type' => $deliveryArea['delivery_fee_type'],
			'position' => $deliveryArea['position'],
			'color' => self::$zipCodeColors[$deliveryArea['position'] % 5],
		);

		return $ret;
	}

	protected function persistDeliveryArea(&$deliveryArea)
	{
		$db = $this->getDb();

		if (!isset($deliveryArea['ssid']) || intval($deliveryArea['ssid']) < 1)
		{
			$db->reset();
			$db->assignStr('carrier_id', 'zip_code');
			$db->assignStr('carrier_name', 'Same Day Delivery');
			$db->assignStr('method_id', 'flat');
			$db->assignStr('method_calc_mode', 'before_discount');
			$db->assignStr('method_name', 'Per Order');
			$db->assignStr('priority', 5);
			$db->assignStr('country', 1);
			$db->assignStr('state', 0);
			$db->assignStr('weight_min', 0);
			$db->assignStr('weight_max', 100000);
			$db->assignStr('fee', 0);
			$db->assignStr('fee_type', 'amount');
			$db->assignStr('exclude', 'No');
			$db->assignStr('hidden', 'No');
			$db->insert(DB_PREFIX.'shipping_selected');

			$deliveryArea['ssid'] = $db->insertId();
		}

		$db->reset();
		$db->assign('ssid', intval($deliveryArea['ssid']));
		$db->assignStr('zip_codes', $deliveryArea['zip_codes']);
		$db->assignStr('before_time', $deliveryArea['before_time']);
		$db->assignStr('delivery_days', implode(',', $deliveryArea['delivery_days']));
		$db->assignStr('last_delivery_time', $deliveryArea['last_delivery_time']);
		$db->assign('delivery_fee', floatval($deliveryArea['delivery_fee']));
		$db->assignStr('delivery_fee_type', $deliveryArea['delivery_fee_type']);
		$db->assign('position', intval($deliveryArea['position']));

		if (isset($deliveryArea['id']) && intval($deliveryArea['id']) > 0)
		{
			$db->update(DB_PREFIX.'shipping_zip_methods', 'WHERE id = '.intval($deliveryArea['id']));
		}
		else
		{
			$db->insert(DB_PREFIX.'shipping_zip_methods');
			$deliveryArea['id'] = $db->insertId();
		}
	}

	public function activateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('PikflyEnabled' => 'Yes'));
	}

	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('PikflyEnabled' => 'No'));
	}

	/**
	 * @return bool
	 */
	protected function getCurrency()
	{
		global $admin_currency;

		return $admin_currency;
	}

	/**
	 * @return string
	 */
	protected function getPikflyApiUrl()
	{
		$baseUrlProduction = 'https://api.pikfly.com/api/';
		$baseUrlTest = 'https://dev.pikfly.com/api/';

		return defined('PIKFLY_TEST_MODE') && PIKFLY_TEST_MODE ?
			(defined('PIKFLY_TEST_URL') && trim(PIKFLY_TEST_URL) != '' ? PIKFLY_TEST_URL : $baseUrlTest) : $baseUrlProduction;
	}
}
<?php

class Pikfly_TimeHelper
{
	/** @var DB */
	protected $db;

	/** @var array */
	protected $settings;

	public function __construct(DB $db, array &$settings)
	{
		$this->db = $db;
		$this->settings = $settings;
	}

	public function canBeDeliveredToday()
	{
		$nextDeliverByTime = $this->getNextDeliverByTime();
		if ($nextDeliverByTime === null) return false;

		$timezone = $this->getTimezone();
		if ($timezone == '') $timezone = date('e');

		$methodTimezone = new DateTimeZone($timezone);

		$tomorrow = new DateTime('tomorrow', $methodTimezone);
		$diff = $nextDeliverByTime->diff($tomorrow);

		return $diff->invert != 1;
	}

	public function getNextOrderByTime($ssid = false)
	{
		$db = $this->db;

		/** @var DateTime $nextOrderByTime */
		$nextOrderByTime = null;

		$timezone = $this->getTimezone();
		if ($timezone == '') $timezone = date('e');

		$methods = $db->selectAll('SELECT *
FROM '.DB_PREFIX.'shipping_zip_methods m
ORDER BY m.id');

		foreach ($methods as $method)
		{
			if ($ssid && $method['ssid'] != $ssid) continue;

			$orderByTime = null;

			$deliveryDays = explode(',', strtolower($method['delivery_days']));

			if (count($deliveryDays) == 0) continue;

			$localTimezone = new DateTimeZone(date('e'));
			$methodTimezone = new DateTimeZone($timezone);

			$today = new DateTime('today', $methodTimezone);
			if (in_array(strtolower($today->format('l')), $deliveryDays))
			{
				$now = new DateTime('now', $localTimezone);

				$beforeTime = DateTime::createFromFormat('H:i:s', $method['before_time'], $methodTimezone);

				$diff = $now->diff($beforeTime);
				if ($diff->invert != 1)
				{
					$orderByTime = DateTime::createFromFormat('Y-m-d  H:i:s', $today->format('Y-m-d').' '.$method['before_time'], $methodTimezone);
				}
			}

			if ($orderByTime === null)
			{
				$day = new DateTime('today', $methodTimezone);

				for ($i = 1; $i < 7; $i++)
				{
					$interval = new DateInterval('P1D');
					$day->add($interval);
					if (in_array(strtolower($day->format('l')), $deliveryDays))
					{
						$orderByTime = DateTime::createFromFormat('Y-m-d H:i:s', $day->format('Y-m-d').' '.$method['before_time'], $methodTimezone);
						break;
					}
				}
			}

			if ($nextOrderByTime === null)
			{
				$nextOrderByTime = $orderByTime;
			}
			else
			{
				$diff = $nextOrderByTime->diff($orderByTime);
				if ($diff->invert) $nextOrderByTime = $orderByTime;
			}
		}

		return $nextOrderByTime;
	}

	public function getNextDeliverByTime()
	{
		$db = $this->db;

		/** @var DateTime $nextOrderByTime */
		$nextOrderByTime = null;
		/** @var DateTime $nextDeliverByTime */
		$nextDeliverByTime = null;

		$timezone = $this->getTimezone();
		if ($timezone == '') $timezone = date('e');

		$methods = $db->selectAll('SELECT *
FROM '.DB_PREFIX.'shipping_zip_methods m
ORDER BY m.id');

		foreach ($methods as $method)
		{
			$result = $this->calculateNextDeliverByTime($method, $timezone);

			if ($result === null) continue;

			$orderByTime = $result['orderByTime'];
			$deliverByTime = $result['deliverByTime'];

			if ($nextOrderByTime === null)
			{
				$nextOrderByTime = $orderByTime;
				$nextDeliverByTime = $deliverByTime;
			}
			else
			{
				$diff = $nextOrderByTime->diff($orderByTime);
				if ($diff->invert)
				{
					$nextOrderByTime = $orderByTime;
					$nextDeliverByTime = $deliverByTime;
				}
			}
		}

		return $nextDeliverByTime;
	}

	protected function calculateNextDeliverByTime(array &$method, $timezone)
	{

		/** @var DateTime $orderByTime */
		$orderByTime = null;
		/** @var DateTime $deliverByTime */
		$deliverByTime = null;

		$deliveryDay = '';

		$deliveryDays = explode(',', strtolower($method['delivery_days']));

		if (count($deliveryDays) == 0) return null;

		$localTimezone = new DateTimeZone(date('e'));
		$methodTimezone = new DateTimeZone($timezone);

		$today = new DateTime('today', $methodTimezone);
		if (in_array(strtolower($today->format('l')), $deliveryDays))
		{
			$now = new DateTime('now', $localTimezone);

			$beforeTime = DateTime::createFromFormat('H:i:s', $method['before_time'], $methodTimezone);

			$diff = $now->diff($beforeTime);
			if ($diff->invert != 1)
			{
				$orderByTime = DateTime::createFromFormat('Y-m-d  H:i:s', $today->format('Y-m-d').' '.$method['before_time'], $methodTimezone);
				$deliverByTime = DateTime::createFromFormat('Y-m-d  H:i:s', $today->format('Y-m-d').' '.$method['last_delivery_time'], $methodTimezone);
				$deliveryDay = 'Today';
			}
		}

		if ($orderByTime === null)
		{
			$day = new DateTime('today', $methodTimezone);

			for ($i = 1; $i < 7; $i++)
			{
				$interval = new DateInterval('P1D');
				$day->add($interval);
				if (in_array(strtolower($day->format('l')), $deliveryDays))
				{
					$orderByTime = DateTime::createFromFormat('Y-m-d H:i:s', $day->format('Y-m-d').' '.$method['before_time'], $methodTimezone);
					$deliverByTime = DateTime::createFromFormat('Y-m-d H:i:s', $day->format('Y-m-d').' '.$method['last_delivery_time'], $methodTimezone);
					$deliveryDay = $i == 1 ? 'Tomorrow' : $day->format('l');
					break;
				}
			}
		}

		if ($orderByTime === null) return null;

		return array(
			'orderByTime' => $orderByTime,
			'deliverByTime' => $deliverByTime,
			'deliveryDay' => $deliveryDay,
		);
	}

	public function getNextDeliveryByMethod($ssid)
	{
		$db = $this->db;

		$timezone = $this->getTimezone();
		if ($timezone == '') $timezone = date('e');

		$method = $db->selectOne('SELECT *
FROM '.DB_PREFIX.'shipping_zip_methods m
WHERE m.ssid = '.intval($ssid));

		if (!$method) return null;

		$result = $this->calculateNextDeliverByTime($method, $timezone);

		if ($result === null) return null;

		return $result;
	}

	protected function getTimezone()
	{
		$db = $this->db;

		$timezone = $this->settings['PikflyTimezone'];
		$stateCode = null;
		$stateData = false;
		$stateId = intval($this->settings['ShippingOriginState']);
		if ($stateId > 0)
		{
			$stateData = $db->selectOne('SELECT short_name FROM '.DB_PREFIX.'states WHERE stid = '.$stateId);
		}
		else
		{
			$stateName = $this->settings['CompanyState'];
			if (trim($stateName) != '')
			{
				$stateData = $db->selectOne('SELECT short_name FROM '.DB_PREFIX.'states WHERE name = "'.$db->escape($stateName).'"');
			}
		}
		if ($stateData)
		{
			$stateCode = $stateData['short_name'];
		}

		if (trim($timezone) == '')
		{
			if ($stateCode)
			{
				$timezonesMap = array(
					'eastern' => array('ME','VT','NH','MA','RI','CT','NY','NJ','MI','PA','IN','OH','DE','MD','DC','KY',
						'WV','VA','NC','SC','GA','FL'),
					'central' => array('ND','SD','MN','WI','NE','IA','IL','KS','MO','OK','AR','TN','TX','LA','MS','AL'),
					'mountain' => array('MT','ID','WY','UT','CO','AZ','NM'),
					'pacific' => array('WA','OR','CA','NV'),
					'alaskan' => array('AK'),
					'hawaiian' => array('HI'),
				);

				foreach ($timezonesMap as $t => $stateCodes)
				{
					if (in_array($stateCode, $stateCodes))
					{
						$timezone = $t;
						break;
					}
				}
			}
		}

		if (trim($timezone) != '')
		{
			switch ($timezone)
			{
				case 'eastern':
					$timezone = 'America/New_York';
					break;
				case 'central':
					$timezone = 'America/Chicago';
					break;
				case 'mountain':
					if ($stateCode == 'AZ')
					{
						$timezone = 'America/Phoenix';
					}
					else
					{
						$timezone = 'America/Denver';
					}
					break;
				case 'pacific':
					$timezone = 'America/Los_Angeles';
					break;
				case 'alaskan':
					$timezone = 'America/Anchorage';
					break;
				case 'hawaiian':
					$timezone = 'Pacific/Honolulu';
					break;
			}
		}

		return $timezone;
	}
}
<?php

/**
 * Class DriftEmailCampaign
 */
class DriftEmailCampaign
{
    private $_campaignid;
    private $_title;
    private $_htmlEmail;
    private $_timeFrame;
    private $_emailSubject;
    private $_db;

    public function  __construct(&$db)
    {
        $this->_db = $db;
    }
    
    public function SetCampaignId($newCampaignId)
    {
        $this->_campaignid = $newCampaignId;
    }
    
    public function GetCampaignId()
    {
        return $this->_campaignid;
    }
    
    public function SetTitle($newTitle)
    {
        $this->_title = $newTitle;
    }
    
    public function GetTitle()
    {
        return $this->_title;
    }

    public function SetEmailSubject($subject)
    {
        $this->_emailSubject = $subject;
    }
    
    public function GetEmailSubject()
    {
        return $this->_emailSubject;
    }

    public function SetHtmlEmail($htmlEmail)
    {
        $this->_htmlEmail = $htmlEmail;
    }
    
    public function GetHtmlEmail()
    {
        return $this->_htmlEmail;
    }
    
    public function SetTimeframe($timeframe)
    {
        $this->_timeFrame = $timeframe;
    }
    
    public function GetTimeframe()
    {
        return $this->_timeFrame;
    }
    
    public function Save()
    {
        $this->_db->reset();
        $this->_db->assignStr("name", $this->_title);
        $this->_db->assignStr("htmlEmail", $this->_htmlEmail);
        $this->_db->assignStr("emailSubject", $this->_emailSubject);
        $this->_db->assign("timeframe", $this->_timeFrame);
        
        if ($this->_campaignid)
        {
            $this->_db->update(DB_PREFIX."drift_email_campaigns", "WHERE id='".intval($this->_campaignid)."'");
        }
        else
        {
            $this->_campaignid = $this->_db->insert(DB_PREFIX."drift_email_campaigns");
        }
    }
    
    public static function DeleteById(&$db, $id)
    {
        $db->query("DELETE FROM ".DB_PREFIX."drift_email_campaigns WHERE `id`='".intval($id)."'");
    }
    
    public static function LoadAll(&$db, $order_by=null)
    {
        if (!$order_by)
        {
            $order_by = "name";
        }
        
        $db->query("SELECT ".DB_PREFIX."drift_email_campaigns.* FROM ".DB_PREFIX."drift_email_campaigns ORDER BY ".$order_by);
        $records = $db->getRecords();

        $returnArray = array();
        foreach ($records as $record)
        {
            $newCampaign = self::LoadByDataRow($db, $record);
            array_push($returnArray,$newCampaign);
        }
        return $returnArray;
    }
    
    public static function LoadById(&$db, $id)
    {
        $db->query("SELECT ".DB_PREFIX."drift_email_campaigns.* FROM ".DB_PREFIX."drift_email_campaigns where `id` = $id");

        $newCampaign = null;
        if ($db->numRows() > 0)
        {
            $records = $db->getRecords();
            $record = $records[0];
            $newCampaign = self::LoadByDataRow($db, $record);
        }
        return $newCampaign;
    }

	public function GetOrders(&$db, $campaignId)
	{
		$records = array();
		$db->query("SELECT a.uid,a.oid,a.status_date, b.uid FROM ".DB_PREFIX."orders a, ".DB_PREFIX."users b
                WHERE a.status = 'Abandon'
                    AND a.uid IS NOT NULL
                    AND b.uid = a.uid
                    AND a.oid NOT IN (SELECT oid FROM ".DB_PREFIX."drift_email_history WHERE campaign_id = " . $campaignId . ")");

		if ($db->numRows() > 0)
		{
			$records = $db->getRecords();
		}

		return $records;
	}

	public function GetOrdersContent(&$db, $oid)
	{
		$records = array();
		$db->query("SELECT a.*, b.image_url, b.image_location FROM ".DB_PREFIX."orders_content a, ".DB_PREFIX."products b
                WHERE a.oid = ". $oid ." AND b.pid = a.pid");

		if ($db->numRows() > 0)
		{
			$records = $db->getRecords();
		}

		return $records;
	}

    public static function LoadByDataRow($db, $record)
    {
        $newCampaign = new DriftEmailCampaign($db);
        $newCampaign->SetCampaignId($record["id"]);
        $newCampaign->SetTitle($record["name"]);
        $newCampaign->SetHtmlEmail($record["htmlEmail"]);
        $newCampaign->SetTimeframe($record["timeframe"]);
        $newCampaign->SetEmailSubject($record["emailSubject"]);
        return $newCampaign;
    }
}

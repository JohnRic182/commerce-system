<?php

class OrderProvider
{
	/**
	 * Refresh subtotals only
	 * 
	 * @param ORDER $order
	 */
	public static function refreshSubtotals(ORDER $order)
	{
		$order->checkShippingRequired();
		$order->recalcSubtotals();
	}

	/**
	 * Add item to an order
	 *
	 * @param ORDER $order
	 * @param $oa_id
	 * @param $oa_quantity
	 * @param array $oa_attributes
	 * @param array $customData
	 *
	 * @return bool
	 */
	public static function addItem(ORDER $order, $oa_id, $oa_quantity, $oa_attributes = array(), $customData = array())
	{
		return $order->addItem($oa_id, $oa_quantity, $oa_attributes, $customData);
	}

	/**
	 * Update order item
	 * 
	 * @param ORDER $order
	 * @param array $oa_quantity
	 * 
	 * @return bool
	 */
	public static function updateItems(ORDER $order, $oa_quantity)
	{
		$updateResult = true;

		if (count($oa_quantity) > 0)
		{
			$order->errors = array();
			$updateResult = $order->updateItems($oa_quantity);

			self::refreshSubtotals($order);
		}

		return $updateResult;
	}


	/**
	 * Remove order items
	 * 
	 * @param ORDER $order
	 * @param int $ocid
	 * @param $promo_code [description]
	 * 
	 * @return bool
	 */
	public static function removeItem(ORDER $order, $ocid, $promo_code = '')
	{
		$order->removeItem($ocid);

		$promo_result = self::applyPromo($order, $promo_code);

		self::refreshSubtotals($order);

		return $promo_result;
	}

	/**
	 * Clear order items
	 * 
	 * @param ORDER $order
	 */
	public static function clearItems(ORDER $order)
	{
		$order->clearItems();
		self::refreshSubtotals($order);
	}

	/**
	 * Apply promo code
	 * 
	 * @param ORDER $order
	 * @param string $promo_code
	 * @param boolean $forceReset
	 * 
	 * @return mixed
	 */
	public static function applyPromo(ORDER $order, $promo_code, $forceReset = false)
	{
		$promo_result = true;

		// Force reset when the user had the possiblity of updating the promo code
		if ($forceReset) $order->resetPromo();

		if (trim($promo_code) != '')
		{
			// Reset here when the promo code is just being checked for validity
			if (!$forceReset) $order->resetPromo();

			$promo_result = $order->checkPromoCode($promo_code);
		}

		return $promo_result;
	}

	public static function isReadyForPayment(ORDER $order, USER $user)
	{
		$shipping_error_message = '';
		$order->recalcEverything($shipping_error_message);

		$validBilling = true;
		$validShipping = true;

		if (trim($user->data['fname'] == '') || trim($user->data['lname'] == ''))
		{
			$validBilling = false;
		}

		//Ensure shipping address loaded
		if (count($order->shippingAddress) == 0) $order->getShippingAddress();

		if ($order->getShippingRequired() &&
			(!isset($order->shippingAddress['name']) || trim($order->shippingAddress['name']) == '' ||
				!isset($order->shippingAddress['address1']) || trim($order->shippingAddress['address1']) == ''))
		{
			$validShipping = false;
		}

		return $validBilling && $validShipping;
	}

	/**
	 * Complete order
	 *
	 * @param ORDER $order
	 * @param USER $user
	 * @param $form
	 * @param $customFields
	 * @param $orderStatus
	 * @param $paymentStatus
	 * @param null $orderSource
	 * @param null $offsiteCampaignId
	 * @param bool $sendNotification
	 * @return bool
	 */
	public static function completeOrder(
		ORDER $order, USER $user, $form, $customFields,
		$orderStatus = ORDER::STATUS_PROCESS, $paymentStatus = ORDER::PAYMENT_STATUS_PENDING,
		$orderSource = null, $offsiteCampaignId = null, $sendNotification = true
	)
	{
		$isReadyForPayment = OrderProvider::isReadyForPayment($order, $user);
		$validPayment = true;

		if ($order->paymentMethodName != 'Free' && ($order->paymentMethodId == 0 || $order->paymentIsRealtime != 'No'))
		{
			$validPayment = false;
		}

		$valid = $isReadyForPayment && $validPayment;

		if ($valid)
		{
			OrderProvider::closeOrder($order, $form, $customFields, $orderStatus, $paymentStatus,
				$orderSource, $offsiteCampaignId, $sendNotification);
		}

		return $valid;
	}

	/**
	 * Close order
	 *
	 * @param ORDER $order
	 * @param $form
	 * @param $customFields
	 * @param $orderStatus
	 * @param $paymentStatus
	 * @param null $orderSource
	 * @param null $offsiteCampaignId
	 * @param bool $sendNotification
	 */
	public static function closeOrder(
		ORDER $order, $form, $customFields,
		$orderStatus = ORDER::STATUS_PROCESS, $paymentStatus = ORDER::PAYMENT_STATUS_PENDING,
		$orderSource = null, $offsiteCampaignId = null, $sendNotification = true
	)
	{
		$_user = $order->getUser();
		$_oid = $order->getId();

		$order->closeOrder(
			$orderStatus,
			$paymentStatus,
			$form,
			$customFields,
			$orderSource,
			$offsiteCampaignId,
			$sendNotification
		);

		$_db = $order->db();
		$_settings = $order->getSettingsRepository()->getAll();
		$event_order = ORDER::getById($_db, $_settings, $_user, $_oid);
		$event_order->getOrderData();
		$event = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_ORDER_CHECK);
		$event->setOrder($event_order);
		Events_EventHandler::handle($event);

		if (!$event->hasErrors())
		{
			$checkResult = $event->getData('CheckOrderResult');

			if($checkResult['fraud_status'] == FraudService_Provider_FraudServiceProvider::STATUS_REVIEW)
			{
				$newevent = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_REVIEW_ACTION);
				$newevent->setOrder($event_order);
				$newevent->setData('CheckOrderResult', $checkResult);
				Events_EventHandler::handle($newevent);
			}
			else if($checkResult['fraud_status'] == FraudService_Provider_FraudServiceProvider::STATUS_REJECT)
			{
				$newevent = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_REJECT_ACTION);
				$newevent->setOrder($event_order);
				$newevent->setData('CheckOrderResult', $checkResult);
				Events_EventHandler::handle($newevent);
			}
		}
	}

	/**
	 * Fail order
	 *
	 * @param ORDER $order
	 * @param $form
	 * @param bool $isMobile
	 * @param null $orderSource
	 * @param null $offsiteCampaignId
	 */
	public static function failOrder(ORDER $order, $form,  $isMobile = false, $orderSource = null, $offsiteCampaignId = null)
	{
		$order->failOrder($form, $isMobile, $orderSource, $offsiteCampaignId);
	}

	/**
	 * Recalculate the order in the event of the user login (in case user level changes)
	 * 
	 * @param ORDER $order
	 * @param bool $resetDiscounts
	 * @return bool
	 */
	public static function reupdateOrder(ORDER $order, $resetDiscounts = true)
	{
		$ret = $order->reupdateItems();

		self::refreshSubtotals($order);

		return $ret;
	}

	/**
	 * Update shipping address information
	 *
	 * @param ORDER $order
	 * @param $address
	 * @param bool $customFieldsData
	 * @param array $shipmentsMethods
	 */
	public static function updateShippingAddress(ORDER $order, $address, $customFieldsData = false, array $shipmentsMethods = array())
	{
		global $settings;

		$order->updateShippingAddress($address, $customFieldsData, $shipmentsMethods);

		$order->persist();
		$order->getShippingAddressFromDb();

		if ($settings['TaxAddress'] == 'Shipping')
		{
			$order->reupdateItems();
		}
	}

	/**
	 * Update payment method
	 *
	 * @param $payment_method_id
	 *
	 * @return boolean
	 */
	public static function updatePaymentMethod(ORDER $order, $paymentMethodId)
	{
		if ($order->updatePaymentMethod($paymentMethodId))
		{
			$order->persist();
			return true;
		}

		return false;
	}

	public static function updateOrderFormID(ORDER $order, $id)
	{
		if ($order->updateOrderFormID($id))
		{
			$order->persist();
			return true;
		}

		return false;
	}
}
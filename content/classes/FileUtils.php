<?php

/**
 * A utlity class to assist with file system operations
 */
class FileUtils
{
	/**
	 * Recursively sets the permissions to all directories in a path
	 *
	 * @param array $paths An array of paths
	 * @param string $mode 
	 * @return array An array of paths that permissions have been applied to
	 */
	public static function setDirectoryPermissions($paths, $mode = 0755)
	{
		if (!is_array($paths)) $paths = array($paths);
		
		$paths_to_chmod = array();
		foreach ($paths as $path)
		{
			$path_info = explode('/', $path);

			$chmod_path = '';
			foreach ($path_info as $dir)
			{
				$chmod_path .= $dir.'/';
				if (!in_array($chmod_path, $paths_to_chmod)) $paths_to_chmod[] = $chmod_path;
			}
		}

		foreach ($paths_to_chmod as $path)
		{
			@chmod($path, $mode);
		}
		
		return $paths_to_chmod;
	}
	
	/**
	 * Returns the file permission of the filepath
	 *
	 * @param string $filepath The path to the file/directory to be checked
	 * @param string $refresh Whether to refresh the check
	 * @return string A string representation of the file permission value
	 */
	public static function getFilePermission($filepath, $refresh = false)
	{
		static $_stats = array();

		if (!isset($_stats[$filepath]) || $refresh)
		{
			if ($refresh) clearstatcache(); $_stats[$filepath] = stat($filepath);
		}
		
		return sprintf('0%o', 0777 & $_stats[$filepath]['mode']);
	}
	
	public static function getFileExtension($filename)
	{
		return substr($filename, strrpos($filename, '.') + 1);
	}

	public static function downloadFile($url, $file = false)
	{
		global $settings;

		$c = curl_init($url);

		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}
			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		if (trim(ini_get('open_basedir')) == '') curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($c, CURLOPT_MAXREDIRS, 5);

		if ($file)
		{
			$f = fopen($settings["GlobalServerPath"]."/".$file, "w");
			curl_setopt($c, CURLOPT_FILE, $f);	
		}
		else
		{
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		}

		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

		$buffer = curl_exec($c);
		if (curl_errno($c) > 0)
		{
			curl_close($c);
			return false;
		}

		curl_close($c);
		if ($file && $f) fclose($f);
		return $buffer;
	}
	
	public static function removeDirectory($directory, &$errors = null, $dryrun = false)
	{
		if (is_null($errors)) $errors = array();
		$dryrun = (bool) $dryrun;
		
		if (is_dir($directory) && $handle = opendir($directory))
		{
			while (($file = readdir($handle)) !== false)
			{
				if ($file != '.' && $file != '..')
				{
					$filename = $directory.DIRECTORY_SEPARATOR.$file;
					
					if (is_dir($filename))
					{
						self::removeDirectory($filename, $errors);
					}
					else
					{
						$response = ($dryrun)
							? is_writable($filename)
							: @unlink($filename);
							
						if (!$response)
						{
							$errors[] = 'Could not remove file '.$filename;
						}
					}
				}
			}
		}
		
		$response = ($dryrun) ? is_writable($directory)	: rmdir($directory);
		
		if (!$response)
		{
			$errors[] = 'Could not remove directory '.$directory;
		}
		
		return $errors;
	}

	protected static $instance;

	public function file_exists($filename)
	{
		return file_exists($filename);
	}

	public function getimagesize($filename)
	{
		return @getimagesize($filename);
	}

	public function unlink($filename, $context = null)
	{
		if (is_file($filename))
		{
			return is_null($context) ? @unlink($filename) : @unlink($filename, $context);
		}

		return false;
	}

	public function stripFilename($filename)
	{
		$temp = $filename;
		for ($k=0; $k<strlen($temp); $k++)
		{
			if(in_array($temp[$k], array("\\", "/", "*", ":", "?", "<", ">", "|", '"', "'")))$temp[$k] = "_";
		}
		return $temp;
	}

	public function deleteImages($filename, $directory)
	{
		$this->unlink($directory."/".$filename.".jpg");
		$this->unlink($directory."/".$filename.".png");
		$this->unlink($directory."/".$filename.".gif");
	}

	public static function getInstance()
	{
		if (!self::$instance)
		{
			self::$instance = new FileUtils();
		}

		return self::$instance;
	}

	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}

	public function is_writable($filename)
	{
		return is_writable($filename);
	}

	public function is_dir($dir)
	{
		return is_dir($dir);
	}

	/**
	 * Returns the set server directory permission mode
	 *
	 * @return int The directory permission mode
	 */
	public static function getDirectoryPermissionMode()
	{
		if (defined('DIRECTORY_PERMISSION_MODE')) return DIRECTORY_PERMISSION_MODE;

		$settings = settings();
		return octdec($settings['DirectoryPermissionMode']);
	}

	/**
	 * Returns the set server file permission mode
	 *
	 * @return int The file permission mode
	 */
	public static function getFilePermissionMode()
	{
		if (defined('FILE_PERMISSION_MODE')) return FILE_PERMISSION_MODE;

		$settings = settings();
		return octdec($settings['FilePermissionMode']);
	}
}

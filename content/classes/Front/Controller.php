<?php
/**
 * Class Front_Controller
 */
class Front_Controller extends Framework_Controller
{
	/**
	 * Get view instance
	 *
	 * @return Ddm_View
	 */
	public function getView()
	{
		return Ddm_View::getInstance();
	}

	/**
	 * Get the current user
	 *
	 * @return USER
	 */
	protected function getCurrentUser()
	{
		global $user;

		return $user;
	}

	/**
	 * Return base path
	 *
	 * @return string
	 */
	protected function getBasePath()
	{
		return 'index.php';
	}

	/**
	 * Get a registered service
	 *
	 * @param $key
	 *
	 * @return null|object
	 */
	protected function get($key)
	{
		global $registry;

		return $registry->get($key);
	}

	/**
	 * Redirect, note - different from back end redirect
	 * TODO: improve, do "REAL" redirect
	 *
	 * @param string | null $url
	 * @param array $query
	 * @param bool $https
	 */
	public function _redirect($url = null, $query = array(), $https = false)
	{
		// TODO: remove globals
		global $db, $url_https, $url_http, $backurl, $_SESSION;

		// redirect to payment_profiles page?
		$backurl = ($url ? $url : ($https ? $url_https : $url_http)) . (count($query) > 0 ? http_build_query($query) : '');

		$_SESSION["HTTP_REFERER"] = (isSslMode() ? "https://" :"http://").$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

		$_mysession = $_SESSION;
		foreach ($_mysession as $_mysessionvar=>$_mysessionkey)
		{
			$_SESSION[$_mysessionvar] = $_mysessionkey;
		}

		_session_write_close();

		$db->done();

		header("Location: ".$backurl."\n");
		die();
	}
}

<?php

/**
 * Class Controller_WholesaleCentral
 */
class Controller_WholesaleCentral extends Admin_ViewController
{
	protected $wholesaleCentralClient = null;
	protected $debug = false;
	
	public function __construct(WholesaleCentral $client, DB $db, $settings)
	{
		parent::__construct($db, $settings);
		$this->wholesaleCentralClient = $client;
	}
	
	public function indexAction()
	{
		//TODO: check if we need it
	}
	
	public function updateAction($data)
	{
		$data['WholesaleCentralEnableCron'] = (isset($data['WholesaleCentralEnableCron'])) ? 1 : 0;
		$data['WholesaleCentralUseOverviewDescription'] = (isset($data['WholesaleCentralUseOverviewDescription'])) ? 1 : 0;
		
		foreach ($data as $field => $value)
		{
			$this->db->reset();
			$this->db->assignStr('value', $value);
			$this->db->update(DB_PREFIX."settings", "WHERE name = '".$this->db->escape($field)."'");
			$this->settings[$field] = $value;
		}
		
		return array(
			'status' => 'success',
			'code' => 1
		);
	}
	
	public function sendFeedAction($filename)
	{
		try
		{
			$this->wholesaleCentralClient->generateDataFeed($this->getProductData(), $filename, true);
			$this->wholesaleCentralClient->sendDataFeed($filename);
			
			$this->db->reset();
			$this->db->assignStr('value', date('Y-m-d g:i:s'));
			$this->db->update(DB_PREFIX.'settings', 'WHERE name = "WholesaleCentralLastUpload"');
			
			$response = array(
				'status' => 'success',
				'code' => 1
			);
		}
		catch (Exception $e)
		{
			$response = array(
				'status' => 'error',
				'code' => 2,
				'message' => $e->getMessage()
			);
		}
		
		return $response;
	}
	
	public function downloadFeedAction($filename)
	{
		
		if ($this->wholesaleCentralClient->generateDataFeed($this->getProductData(), $filename))
		{
			if (FALSE !== ($fp = fopen($filename, 'r')))
			{
				$download_filename = 'ezfeed-wholesalecentral.txt';
				
				header('Content-Type: text/plain');
				
				if ($this->debug !== true)
				{
					header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
					header('Content-Disposition: inline; filename="'.$download_filename.'"');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Content-Disposition: attachment; filename="'.$download_filename.'"');
					header('Pragma: no-cache');
				}
				
				while ($buffer = fread($fp, 2000))
				{
					print $buffer;
				}
				exit;
			}
		}
		
		return false;
	}
	
	private function getProductData()
	{
		global $currencies;

		$storeUrl = $this->settings['GlobalHttpUrl'];
		$useOverview = (intval($this->settings['WholesaleCentralUseOverviewDescription']) == 1);

		$currencies->getDefaultCurrency();

		$this->db->query('
			SELECT
				p.pid,
				p.cid,
				p.product_id,
				IF(p.url_custom != "", p.url_custom, p.url_default) product_url,
				p.title,
				p.overview,
				p.description,
				p.image_location,
				p.image_url,
				p.call_for_price,
				p.stock,
				p.price,
				p.inventory_control,
				p.inventory_rule,
				c.name as category_name
			FROM '.DB_PREFIX.'products p
			JOIN '.DB_PREFIX.'catalog c ON p.cid = c.cid
				WHERE p.is_visible = "Yes"
			LIMIT 10000
		');
		
		$data = array();
		$product = new ShoppingCartProducts($this->db, $this->settings);

		while ($col = $this->db->moveNext())
		{
			// Figure out whether we should display this product or not based on inventory
			if ($col['inventory_control'] != 'No' && $col['inventory_rule'] == 'Hide')
			{
				if ($col['inventory_control'] == 'Yes' && intval($col['stock']) <= 0)
				{
					continue;
				}
			}
			
			// Product name, description, and category
			$productName = strip_tags($col['title']);
			$productDescription = strip_tags(($useOverview) ? $col['overview'] : $col['description']);
			$productCategory = $col['category_name'];
			
			// Generate product URL based on current settings
			if (strtoupper($this->settings['USE_MOD_REWRITE']) == 'YES')
			{
				$productUrl = $storeUrl.'/'.$col['product_url'];
			}
			else
			{
				$productUrl = $storeUrl.'/'.$this->settings["INDEX"]."?p=product&id=".$col['pid'];
			}

			// Figure out the price
			if ($this->settings['VisitorSeePrice'] == 'NO')
			{
				// TODO: Do we want this to be configurable in admin area?
				$productPrice = 'Login to view price';
			}
			else
			{
				$productPrice = ($col['call_for_price'] == 'Yes') ? 'Call for price' : getAdminPrice($col['price']);
			}
			
			// product image url
			$imageUrl = $product->getProductImage($col['product_id'], $col['image_location'], $col['image_url']);
			if (strtolower($col['image_location']) == 'local')
			{
				$imageUrl = $storeUrl.'/'.$imageUrl;
			}
			
			$data[] = array(
				'product_url' => $productUrl,
				'product_name' => $productName,
				'product_description' => $productDescription,
				'image_url' => $imageUrl,
				'product_category' => $productCategory,
				'product_price' => $productPrice
			);
		}

		return $data;
	}
	
	public function setDebug($mode)
	{
		$this->debug = (bool) $mode;
	}
}

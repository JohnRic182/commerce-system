<?php 
/**
 * Recent items
 */
class ShoppingCartRecentItems{
	private $_itemsLimit = 7;
	
	/**
	 * Add item to recent
	 */
	public function add($url, $text)
	{
		global $_SESSION;
		$urlMD5 = md5($url);
		$items = isset($_SESSION["userRecentItems"]) ? $_SESSION["userRecentItems"] : array();
		if (count($items) >= $this->_itemsLimit)
		{
			$c = array_key_exists($urlMD5, $items) ? 0 : 1;
			$_items = array_reverse($items);
			$items = array();
			foreach ($_items as $itemKey=>$item)
			{
				if (count($items) < $this->_itemsLimit - $c) $items[$itemKey] = $item;	
			}
			$items = array_reverse($items);
		}
		if (array_key_exists($urlMD5, $items))
		{
			unset($items[$urlMD5]);				
		}
		$items[$urlMD5] = array("url"=>$url, "text"=>$text);
		$_SESSION["userRecentItems"] = $items;
	}
	
	/**
	 * Return recent items
	 */
	public function getItems()
	{
		global $_SESSION;
		return isset($_SESSION["userRecentItems"]) ? (is_array($_SESSION["userRecentItems"]) && count($_SESSION["userRecentItems"]) ? array_reverse($_SESSION["userRecentItems"]) : false) : false;
	}
}


<?php
/**
 * Handling tax calculator
 */
class Calculator_HandlingTax
{
	protected static $instance;

	protected $settings;
	protected $taxProvider;

	/**
	 * Class constructor
	 * 
	 * @param array $settings
	 * @param Tax_ProviderInterface $taxProvider
	 */
	public function __construct(&$settings, Tax_ProviderInterface $taxProvider)
	{
		$this->settings = $settings;
		$this->taxProvider = $taxProvider;
	}

	/**
	 * Reset order data
	 * 
	 * @param  ORDER  $order
	 */
	public function reset(ORDER $order)
	{
		$order->setHandlingTaxable(false);
		$order->setHandlingTaxClassId(0);
		$order->setHandlingTaxRate(0);
		$order->setHandlingTaxDescription('');
		$order->setHandlingTaxAmount(0);
	}

	/**
	 * Calculate handling amount
	 * 
	 * @param ORDER $order
	 * 
	 * @return float
	 */
	public function calculate(ORDER $order)
	{
		$handlingTaxable = (bool) $this->settings["ShippingHandlingTaxable"];
		$handlingTaxClassId = $this->settings["ShippingHandlingTaxClassId"];
		$handlingTaxRate = 0;
		$handlingTaxDescription = "";
		$handlingTaxAmount = 0;

		if ($handlingTaxable)
		{
			$handlingAmount = $order->getHandlingAmount();

			if ($handlingAmount > 0)
			{
				$taxProvider = $this->taxProvider;

				if ($taxProvider->hasTaxRate($handlingTaxClassId))
				{
					$handlingTaxRate = $taxProvider->getTaxRate($handlingTaxClassId, false);
					$handlingTaxDescription = $taxProvider->getTaxRateDescription($handlingTaxClassId);
					$handlingTaxAmount = PriceHelper::round($handlingAmount / 100 * $handlingTaxRate);
				}
			}
		}
		else
		{
			$handlingTaxable = false;
			$handlingTaxClassId = 0;
		}
		
		$order->setHandlingTaxable($handlingTaxable);
		$order->setHandlingTaxClassId($handlingTaxClassId);
		$order->setHandlingTaxRate($handlingTaxRate);
		$order->setHandlingTaxDescription($handlingTaxDescription);
		$order->setHandlingTaxAmount($handlingTaxAmount);

		return $handlingTaxAmount;
	}

	/**
	 * Get handling tax calculator class instance
	 * 
	 * @param array $settings
	 * @param Tax_ProviderInterface $taxProvider
	 * 
	 * @return Calculator_HandlingTax
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance(&$settings, Tax_ProviderInterface $taxProvider = null)
	{
		if (is_null(self::$instance))
		{
			if (is_null($taxProvider))
			{
				$taxProvider = Tax_ProviderFactory::getTaxProvider();
			}

			self::$instance = new self($settings, $taxProvider);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_HandlingTax $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
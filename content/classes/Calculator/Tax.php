<?php
/**
 * Tax calculations class
 */
class Calculator_Tax
{
	protected static $instance;

	/**
	 * @var $taxProvider Tax_TaxServiceInterface
	 */
	protected $taxService;

	/**
	 * Calculator_Tax constructor.
	 * @param $taxService
	 */
	public function __construct($taxService)
	{
		$this->taxService = $taxService;
	}

	/**
	 * Reset order data
	 * 
	 * @param  ORDER  $order
	 */
	public function reset(ORDER $order)
	{
		$order->setTaxAmount(0);
	}

	/**
	 * Calculate
	 *
	 * @param ORDER $order
	 * @return float
	 */
	public function calculate(ORDER $order)
	{
		$this->taxService->calculateTax($order);

		return $order->getTaxAmount();
	}

	/**
	 * Returns shipping tax calculator class instance
	 *
	 * @param Tax_TaxServiceInterface $taxService
	 * @return Calculator_Tax
	 */
	public static function getInstance(Tax_TaxServiceInterface $taxService = null)
	{
		if (is_null(self::$instance))
		{
			if (is_null($taxService))
			{
				$taxService = Tax_ProviderFactory::getTaxService();
			}

			self::$instance = new self($taxService);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_Tax $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
}
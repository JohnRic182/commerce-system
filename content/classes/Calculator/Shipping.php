<?php
/**
 * Calculate shipping
 */
class Calculator_Shipping
{
	protected static $instance;

	protected $settings;
	protected $shippingRepository;
	protected $orderRepository;
	protected $currencies;

	/**
	 * Class constructor
	 *
	 * @param array $settings
	 * @param DataAccess_ShippingRepositoryInterface $shippingRepository
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 * @param CurrenciesInterface $currencies
	 */
	public function __construct(
		&$settings,
		DataAccess_ShippingRepositoryInterface $shippingRepository,
		DataAccess_OrderRepositoryInterface $orderRepository,
		CurrenciesInterface $currencies
	)
	{
		$this->settings = $settings;
		$this->shippingRepository = $shippingRepository;
		$this->orderRepository = $orderRepository;
		$this->currencies = $currencies;
	}

	/**
	 * Reset order data
	 * 
	 * @param  ORDER  $order
	 */
	public function reset(ORDER $order)
	{
		// Reset line item shipping prices
		$this->orderRepository->resetLineItemShippingPrices($order->getId());

		$order->setShippingAmount(0);
	}

	/**
	 * Calculate
	 *
	 * @param DB $db
	 * @param $settings
	 * @param ORDER $order
	 * @param array $shipmentsMethods
	 * @param $error_message
	 *
	 * @return float|int
	 */
	public function calculate(DB $db, &$settings, ORDER $order, array $shipmentsMethods, &$error_message)
	{
		/**
 		 * Select/calc data required for shipping price calculation
 		 */
		$shippingAmountForPriceBased = array();
		$shippingAmountForPriceBasedExists = false;
		$shippingItems = array();
		$shippingWeight = array();
		$shippingItemsCount = array();

		$shipments = $order->getShipments();

		$shippingRepository = $this->shippingRepository;

		$result = $shippingRepository->getOrderItems($order->getId());

		if ($result) foreach ($result as $item)
		{
			$shipmentId = $item['shipment_id'];

			if (!$order->isItemShippable($item, $shipments[$shipmentId]['shipping_ignore_free'] == 1)) continue;

			if (!isset($shippingItems[$shipmentId])) $shippingItems[$shipmentId] = array();
			if (!isset($shippingWeight[$shipmentId])) $shippingWeight[$shipmentId] = 0;
			if (!isset($shippingItemsCount[$shipmentId])) $shippingItemsCount[$shipmentId] = 0;
			if (!isset($shippingAmountForPriceBased[$shipmentId])) $shippingAmountForPriceBased[$shipmentId] = 0;

			$shippingItems[$shipmentId][] = $item;
			$shippingWeight[$shipmentId] = $shippingWeight[$shipmentId] + $item['weight'] * $item['admin_quantity'];
			$shippingItemsCount[$shipmentId] = $shippingItemsCount[$shipmentId] + $item['admin_quantity'];
			$shippingAmountForPriceBased[$shipmentId] += $item['admin_price'] * $item['admin_quantity'];
			$shippingAmountForPriceBasedExists = true;
		}

		$addressFrom = $shippingRepository->getShippingOriginAddress($order->getId(), $settings);

		/**
		 * Prepare to(destination) shipping address
		 */
		$addressData = $shippingRepository->getOrderShippingAddress($order->getId());
		$addressTo = array(
			'address_type' => $addressData['shipping_address_type'],
			'name' => $addressData['shipping_name'],
			'address1' => $addressData['shipping_address1'],
			'address2' => $addressData['shipping_address2'],
			'city' => $addressData['shipping_city'],
			'state' => $addressData['state_name'],
			'state_name' => $addressData['state_name'],
			'state_id' => $addressData['state_id'],
			'state_abbr' => $addressData['state_abbr'],
			'state_code' => $addressData['state_abbr'],
			'country' => $addressData['country_name'],
			'country_name' => $addressData['country_name'],
			'country_id' => $addressData['country_id'],
			'country_iso_a2' => $addressData['country_iso_a2'],
			'country_iso_a3' => $addressData['country_iso_a3'],
			'country_iso_number' => $addressData['country_iso_number'],
			'zip' => strtoupper($addressData['country_iso_a2']) == 'US' ? substr(trim($addressData['shipping_zip']), 0, 5) : trim($addressData['shipping_zip'])
		);

		// delivery to bongo instead of international address
		// TODO: move into event?
		$bongoUsed = false;
		if ($order->paymentGatewayId == 'bongocheckout' && strtoupper($addressTo['country_iso_a2']) != 'US')
		{
			$addressTo = Payment_Bongo_BongoUtil::getBongoShippingAddress($db, $settings, $addressTo, $bongoUsed);
		}

		if ($shippingAmountForPriceBasedExists)
		{
			/**
			 * Check discounts
			 */
			$promoDiscountPercent = $order->getPromoDiscountPercentage();
			$discountPercent = $order->getDiscountPercentage();

			foreach ($shippingAmountForPriceBased as $shipmentId => $value)
			{
				$shippingAmountForPriceBasedDiscounted[$shipmentId] = $value - $value / 100 * $promoDiscountPercent - $value / 100 * $discountPercent;
			}
		}
		else
		{
			$shippingAmountForPriceBasedDiscounted	= $shippingAmountForPriceBased;
		}


		// try to calc
		$currencies = $this->currencies;
		$defaultCurrency = $currencies->getDefaultCurrency();
		$currencyExchangeRates = $currencies->getExchangeRates();

		$error_message = '';

		$resultShippingPrice = 0;

		foreach ($shipmentsMethods as $shipmentId => $shippingMethod)
		{
			/** @var Shipping_Method $shippingMethod */
			if ($shippingMethod && $shippingMethod->getShippingMethodId() != '')
			{
				$shipmentShippingPrice = $shippingMethod->calculate(
					$db,
					$addressFrom,
					$addressTo,
					isset($shippingItems[$shipmentId]) ? $shippingItems[$shipmentId] : array(),
					isset($shippingWeight[$shipmentId]) ? $shippingWeight[$shipmentId] : 0,
					isset($shippingItemsCount[$shipmentId]) ? $shippingItemsCount[$shipmentId] : 0,
					array(
						'before_discount' => isset($shippingAmountForPriceBased[$shipmentId]) ? $shippingAmountForPriceBased[$shipmentId] : 0,
						'after_discount' => isset($shippingAmountForPriceBasedDiscounted[$shipmentId]) ? $shippingAmountForPriceBasedDiscounted[$shipmentId] : 0
					), //price based
					$error_message,
					$defaultCurrency['code'],
					$currencyExchangeRates
				);

				if ($shipmentShippingPrice === false) $shipmentShippingPrice = 0;
				$db->query('UPDATE ' . DB_PREFIX . 'orders_shipments SET shipping_amount=' . PriceHelper::round($shipmentShippingPrice) . ' WHERE id=' . intval($shipmentId));

				$resultShippingPrice += $shipmentShippingPrice;
			}
		}

		if ($order->getShippingRequired() && $resultShippingPrice === false)
		{
			if (trim($error_message) == '')
			{
				global $msg;
				$error_message = $msg['cart']['shipping_error'];
			}
		}

		$shippingAmount = $resultShippingPrice;
		$shippingAmount = PriceHelper::round($shippingAmount);
		$order->setShippingAmount($shippingAmount);

		return $shippingAmount;
	}

	/**
	 * Returns class instance
	 *
	 * @param $settings
	 * @param DataAccess_ShippingRepositoryInterface $shippingRepository
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 * @param CurrenciesInterface $currencies
	 *
	 * @return Calculator_Shipping
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance(
		&$settings,
		DataAccess_ShippingRepositoryInterface $shippingRepository = null,
		DataAccess_OrderRepositoryInterface $orderRepository = null,
		CurrenciesInterface $currencies = null
	)
	{
		if (is_null(self::$instance))
		{
			if (is_null($shippingRepository))
			{
				global $db;

				$shippingRepository = new DataAccess_ShippingRepository($db);
			}

			if (!$orderRepository)
			{
				$orderRepository = DataAccess_OrderRepository::getInstance();
			}

			if (is_null($currencies))
			{
				global $db;

				$currencies = new Currencies($db);
			}

			self::$instance = new self($settings, $shippingRepository, $orderRepository, $currencies);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_Shipping $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
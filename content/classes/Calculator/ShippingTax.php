<?php

/**
 * Shipping Tax Calculator
 */
class Calculator_ShippingTax
{
	protected static $instance;

	protected $settings;
	protected $taxProvider;
	
	/**
	 * Class constructor
	 *
	 * @param array $settings
	 * @param Tax_ProviderInterface $taxProvider
	 */
	public function __construct(&$settings, Tax_ProviderInterface $taxProvider)
	{
		$this->settings = $settings;
		$this->taxProvider = $taxProvider;
	}

	/**
	 * Reset order data
	 * 
	 * @param  ORDER  $order
	 */
	public function reset(ORDER $order)
	{
		$order->setShippingTaxable(false);
		$order->setShippingTaxClassId(0);
		$order->setShippingTaxRate(0);
		$order->setShippingTaxDescription('');
		$order->setShippingTaxAmount(0);
	}

	/**
	 * Calculate shipping tax
	 * 
	 * @param  ORDER   $order
	 * 
	 * @return float
	 */
	public function calculate(ORDER $order)
	{
		$shippingTaxable = (bool) $this->settings["ShippingTaxable"];
		$shippingTaxClassId = $this->settings["ShippingTaxClassId"];
		$shippingTaxRate = 0;
		$shippingTaxDescription = "";
		$shippingTaxAmount = 0;
		
		$shippingAmount = $order->getShippingAmount();
		
		if ($shippingAmount > 0 && $shippingTaxable)
		{
			$taxProvider = $this->taxProvider;

			if ($taxProvider->hasTaxRate($shippingTaxClassId))
			{
				$shippingTaxRate = $taxProvider->getTaxRate($shippingTaxClassId, false);
				$shippingTaxDescription = $taxProvider->getTaxRateDescription($shippingTaxClassId);
				$shippingTaxAmount = PriceHelper::round($shippingAmount / 100 * $shippingTaxRate);
			}
		}
		else
		{
			$shippingTaxable = false;
		}

		$order->setShippingTaxable($shippingTaxable);
		$order->setShippingTaxClassId($shippingTaxClassId);
		$order->setShippingTaxRate($shippingTaxRate);
		$order->setShippingTaxDescription($shippingTaxDescription);
		$order->setShippingTaxAmount($shippingTaxAmount);

		return $shippingTaxAmount;
	}

	/**
	 * Returns shipping tax calculator class instance
	 *
	 * @param array $settings
	 * @param Tax_ProviderInterface $taxProvider
	 *
	 * @return Calculator_ShippingTax
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance(&$settings, Tax_ProviderInterface $taxProvider = null)
	{
		if (is_null(self::$instance))
		{
			if (is_null($taxProvider))
			{
				$taxProvider = Tax_ProviderFactory::getTaxProvider();
			}

			self::$instance = new self($settings, $taxProvider);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_ShippingTax $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
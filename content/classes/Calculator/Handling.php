<?php
/**
 * Handling amount calculator
 */
class Calculator_Handling
{
	protected static $instance;

	protected $settings;

	/**
	 * Class constructor
	 * 
	 * @param array $settings
	 */
	public function __construct(&$settings)
	{
		$this->settings = $settings;
	}

	/**
	 * Reset order data
	 * 
	 * @param  ORDER  $order
	 */
	public function reset(ORDER $order)
	{
		$order->setHandlingSeparated(false);
		$order->setHandlingFee(0);
		$order->setHandlingFeeType('percent');
		$order->setHandlingText('');
		$order->setHandlingAmount(0);
	}

	/**
	 * Calculate handling amount
	 *
	 * @param ORDER $order
	 * @param bool $shippingIsFree
	 * @param bool $addToShippingWhenNeeded
	 * @param bool $ignoreZeroShippingPrice
	 *
	 * @return float
	 */
	public function calculate(ORDER $order, $shippingIsFree = false, $addToShippingWhenNeeded = true, $ignoreZeroShippingPrice = false)
	{
		global $msg;

		// TODO: pass shipments?
		if (1)//$order->getShippingCarrierId() != 'zip_code')
		{
			//common
			$handlingFee = $this->settings['ShippingHandlingFee'];
			$handlingFeeType = $this->settings['ShippingHandlingFeeType'];
			$handlingFeeWhenShippingFree = $this->settings['ShippingHandlingFeeWhenFreeShipping'];
			$handlingText = $msg['opc']['invoice_handling'];
			$handlingSeparated = (bool) $this->settings['ShippingHandlingSeparated'];

			//get handling amount
			if ($shippingIsFree)
			{
				$handlingAmount = $handlingFee = PriceHelper::round($handlingFeeWhenShippingFree);
				$handlingFeeType = 'amount';
				$handlingSeparated = true;
			}
			elseif (!$ignoreZeroShippingPrice && $order->getShippingAmount() == 0)
			{
				$handlingAmount = $handlingFee = PriceHelper::round($handlingFeeWhenShippingFree);
				$handlingFeeType = 'amount';
				// note: handling is not forced to be separated here
			}
			else
			{
				if ($handlingFeeType == 'percent')
				{
					$handlingAmount = PriceHelper::round(($order->getSubtotalAmount() - $order->getDiscountAmount() - $order->getPromoDiscountAmount()) / 100 * $handlingFee);
				}
				else
				{
					$handlingAmount = $handlingFee = PriceHelper::round($handlingFee);
				}
			}

			$order->setHandlingFee($handlingFee);
			$order->setHandlingFeeType($handlingFeeType);
			$order->setHandlingText($handlingText);
			$order->setHandlingAmount($handlingAmount);
			$order->setHandlingSeparated($handlingSeparated);

			if (!$handlingSeparated && $handlingAmount > 0 && $addToShippingWhenNeeded)
			{
				$order->setShippingAmount($order->getShippingAmount() + $handlingAmount);
			}
			return $handlingAmount;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Get handling calculator class instance
	 * 
	 * @param array $settings
	 * 
	 * @return self
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance(&$settings)
	{
		if (is_null(self::$instance))
		{
			self::$instance = new self($settings);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_Handling $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
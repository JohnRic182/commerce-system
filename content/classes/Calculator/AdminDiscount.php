<?php
/**
 * Discount amount calculator
 */
class Calculator_AdminDiscount extends Calculator_Discount
{
	/**
	 * Calculate discount
	 *
	 * @param ORDER $order
	 * @param bool $reset
	 *
	 * @return string
	 */
	public function calculate(ORDER $order, $reset = true)
	{
		$subtotal = $order->calculateSubtotal(false, true);

		$discountAmount = 0;
		$discountAmountWithTax = 0;

		$dp = $this->getDiscountPercentage($subtotal, $order->getDiscountType(), $order->getDiscountValue());

		foreach ($order->lineItems as $item)
		{
			/* @var $item Model_LineItem */
			if ($item->getProductId() == 'gift_certificate')
				continue;

			$lineSubTotal = $item->getSubtotal(false);

			$lineDiscountAmount = PriceHelper::round($lineSubTotal * $dp / 100);
			$item->setDiscountAmount($lineDiscountAmount);
			$discountAmount += $lineDiscountAmount;

			$priceWithTax = $item->getPriceWithTax();

			$lineDiscountAmountWithTax = PriceHelper::round($priceWithTax * $dp / 100 * $item->getFinalQuantity());
			$item->setDiscountAmountWithTax($lineDiscountAmountWithTax);
			$discountAmountWithTax += $lineDiscountAmountWithTax;

			$this->orderRepository->updateLineItemDiscount($order->getId(), $item->getId(), $lineDiscountAmount, $lineDiscountAmountWithTax, $item->getPromoDiscountAmount(), $item->getPromoDiscountAmountWithTax());
		}

		$order->setDiscountAmount($discountAmount);
		$order->setDiscountAmountWithTax($discountAmountWithTax);

		return $order->getDiscountAmount();
	}

	public function reset(ORDER $order)
	{
		$this->orderRepository->resetLineItemDiscounts($order->getId());
	}
}
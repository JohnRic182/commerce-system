<?php
/**
 * Subtotal calculations class
 */
class Calculator_Subtotal
{
	protected static $instance;

	protected $orderRepository;

	/**
	 * Class constructor
	 *
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 */
	public function __construct(DataAccess_OrderRepositoryInterface $orderRepository)
	{
		$this->orderRepository = $orderRepository;
	}

	/**
	 * Reset order data
	 * 
	 * @param  ORDER  $order
	 */
	public function reset(ORDER $order)
	{
		$order->setSubtotalAmount(0);
		$order->setSubtotalAmountWithTax(0);
	}

	/**
	 * Calculate subtotal amount
	 *
	 * @param ORDER $order
	 * @param bool $persist
	 * @param bool $excludeGiftCertificates
	 *
	 * @return int
	 */
	public function calculate(ORDER $order, $persist = true, $excludeGiftCertificates = false)
	{
		$subtotalAmount = 0;
		foreach ($order->lineItems as $item)
		{
			/* @var $item Model_LineItem */
			if ($excludeGiftCertificates && $item->getProductId() == 'gift_certificate')
				continue;

			$subtotalAmount += $item->getSubtotal();
		}

		if ($persist)
		{
			$order->setSubtotalAmount($subtotalAmount);
			$order->setSubtotalAmountWithTax($subtotalAmount);
		}

		return $subtotalAmount;
	}

	/**
	 * Get instance
	 *
	 * @param bool $orderRepository
	 * @return Calculator_Subtotal
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance($orderRepository = false)
	{
		if (is_null(self::$instance))
		{
			if (!$orderRepository)
			{
				$orderRepository = DataAccess_OrderRepository::getInstance();
			}

			self::$instance = new self($orderRepository);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_Subtotal $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
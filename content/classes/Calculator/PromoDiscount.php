<?php

/**
 * Calculate promo discount
 */
class Calculator_PromoDiscount
{
	protected static $instance;

	protected $orderRepository;
	protected $promoCodeRepository;
	protected $settings;

	/**
	 * Class constructor
	 *
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 * @param DataAccess_PromoCodeRepositoryInterface $promoCodeRepository
	 *
	 * @param $settings
	 */
	public function __construct(DataAccess_OrderRepositoryInterface $orderRepository, DataAccess_PromoCodeRepositoryInterface $promoCodeRepository, &$settings)
	{
		$this->orderRepository = $orderRepository;
		$this->promoCodeRepository = $promoCodeRepository;
		$this->settings = $settings;
	}

	/**
	 * Reset promo code data
	 *
	 * @param ORDER $order
	 * @param bool $justAmount
	 */
	public function reset(ORDER $order, $justAmount = false)
	{
		if ($justAmount)
		{
			$order->setPromoDiscountAmount(0);
			$order->setPromoDiscountAmountWithTax(0);

			foreach ($order->lineItems as $item)
			{
				/** @var Model_LineItem $item */
				$item->setPromoDiscountAmount(0);
				$item->setPromoDiscountAmountWithTax(0);
				$this->orderRepository->updateLineItemDiscount($order->getId(), $item->getId(), $item->getDiscountAmount(), $item->getDiscountAmountWithTax(), 0, 0);
			}
		}
		else
		{
			$this->setOrderPromoData($order, 'Global', 0, 'none', 0, 0, 0);

			foreach ($order->lineItems as $item)
			{
				/** @var Model_LineItem $item */
				$item->setPromoDiscountAmount(0);
				$item->setPromoDiscountAmountWithTax(0);
				$this->orderRepository->updateLineItemDiscount($order->getId(), $item->getId(), $item->getDiscountAmount(), $item->getDiscountAmountWithTax(), 0, 0);
			}
		}
	}

	/**
	 * Calculate
	 * 
	 * @param ORDER $order
	 * @param boolean $reset
	 *
	 * @return mixed
	 */
	public function calculate(ORDER $order, $reset = true, $taxesCalculated = false)
	{
		// Calculate only if promo enabled and was campaign was set
		if ($this->settings['DiscountsPromo'] == 'YES' && $order->getPromoCampaignId() > 0)
		{
			$subtotalAmount = $order->calculateSubtotal(false, true);

			if ($reset)
			{
				// reread promo data from data storage
				$promoData = $this->promoCodeRepository->getPromoCodeByPromoCampaignId($order->getPromoCampaignId());
			}
			else
			{
				// get data from order
				$promoData = array(
					'promo_type' => $order->getPromoType(),
					'discount_type' => $order->getPromoDiscountType(),
					'discount' => $order->getPromoDiscountValue(),
					'min_amount' => 0,
					'min_amount_type' => 'order_subtotal',
					'pid' => $order->getPromoCampaignId(),
				);
			}

			if (!is_null($promoData) && is_array($promoData))
			{
				$result = $this->calculatePromoDiscountAmount($order, $promoData, $subtotalAmount, true, $taxesCalculated);

				if ($result) return $order->getPromoDiscountAmount();
			}
		}

		$this->reset($order);

		return 0.00;
	}

	/**
	 * @param ORDER $order
	 * @param $promoCode
	 * @return null
	 */
	public function checkPromoDiscount(ORDER $order, $promoCode)
	{
		$subtotalAmount = $order->calculateSubtotal(false, true);

		$promoData = $this->promoCodeRepository->getPromoCode($promoCode);

		if (!is_null($promoData) && is_array($promoData))
		{
			if ($this->calculatePromoDiscountAmount($order, $promoData, $subtotalAmount, false))
			{
				return $promoData;
			}
		}

		return null;
	}

	/**
	 * Get promo code discount amount
	 *
	 * @param ORDER $order
	 * @param $promoData
	 * @param $subtotalAmount
	 * @param $persist
	 *
	 * @return bool
	 */
	protected function calculatePromoDiscountAmount(ORDER $order, $promoData, $subtotalAmount, $persist = true, $taxesCalculated = false)
	{
		switch ($promoData["promo_type"])
		{
			case 'Product': return $this->calculateProductBasedPromo($order, $promoData, $subtotalAmount, $persist, $taxesCalculated);
			case 'Shipping': return $this->calculateShippingBasedPromo($order, $promoData, $subtotalAmount, $persist, $taxesCalculated);
			case 'Global': return $this->calculateGlobalBasedPromo($order, $promoData, $subtotalAmount, $persist, $taxesCalculated);
		}

		return false;
	}

	/**
	 * Calculate global promo discount
	 *
	 * @param ORDER $order
	 * @param array $promoData
	 * @param float $subtotalAmount
	 * @param bool $persist
	 *
	 * @return bool
	 */
	protected function calculateGlobalBasedPromo(ORDER $order, $promoData, $subtotalAmount, $persist, $taxesCalculated)
	{
		if ($subtotalAmount < $promoData["min_amount"])
		{
			return false;
		}

		$this->calculateDiscountsForLineItems($order, $order->lineItems, $promoData, 1, $taxesCalculated);

		if ($persist)
		{
			$this->setOrderPromoData(
				$order, $promoData['promo_type'], $promoData['pid'], $promoData['discount_type'], $promoData['discount'],
				$order->getPromoDiscountAmount(), $order->getPromoDiscountAmountWithTax()
			);
		}

		return true;
	}

	/**
	 * Calculate shipping based promo discount
	 *
	 * @param ORDER $order
	 * @param array $promoData
	 * @param float $subtotalAmount
	 * @param bool $persist
	 *
	 * @return bool
	 */
	protected function calculateShippingBasedPromo(ORDER $order, $promoData, $subtotalAmount, $persist, $taxesCalculated)
	{
		if ($subtotalAmount < $promoData['min_amount'])
		{
			return false;
		}

		$shippingAmount = $order->getShippingAmount();
		$shippingAmountWithTax = $order->getShippingAmount() + $order->getShippingTaxAmount();

		// always calculate without handling
		if (!$order->getHandlingSeparated())
		{
			$handlingAmount = $order->getHandlingAmount();
			$handlingAmountWithTax = $order->getHandlingTaxAmount();
			$handlingAmountTaxAmount = $order->getHandlingTaxAmount();

			$shippingAmount = $shippingAmount - $handlingAmount;
			$shippingAmountWithTax = $shippingAmountWithTax - $handlingAmount - $handlingAmountTaxAmount;
		}

		if ($persist)
		{
			if ($promoData['discount_type'] == 'amount')
			{
				$order->setPromoDiscountAmount(PriceHelper::round($promoData['discount']));
				$order->setPromoDiscountAmountWithTax(PriceHelper::round($promoData['discount'] * (1 + $order->getShippingTaxRate() / 100)));
			}
			else
			{
				$order->setPromoDiscountAmount(PriceHelper::round($shippingAmount / 100 * $promoData['discount']));
				$order->setPromoDiscountAmountWithTax(PriceHelper::round($shippingAmountWithTax / 100 * $promoData['discount']));
			}

			if ($order->getPromoDiscountAmount() > $shippingAmount)
			{
				$order->setPromoDiscountAmount($shippingAmount);
			}

			if ($order->getPromoDiscountAmountWithTax() > $shippingAmountWithTax)
			{
				$order->setPromoDiscountAmountWithTax($shippingAmountWithTax);
			}

			$this->setOrderPromoData($order, $promoData['promo_type'], $promoData['pid'], $promoData['discount_type'], $promoData['discount'], $order->getPromoDiscountAmount(), $order->getPromoDiscountAmountWithTax());
		}

		return true;
	}

	/**
	 * @param ORDER $order
	 * @param $promoData
	 * @param $subtotalAmount
	 * @param $persist
	 * @param $taxesCalculated
	 * @return bool
	 */
	protected function calculateProductBasedPromo(ORDER $order, $promoData, $subtotalAmount, $persist, $taxesCalculated)
	{
		$promoProducts = $this->promoCodeRepository->getPromoProducts($order->getId(), $promoData['pid']);

		if (count($promoProducts) > 0)
		{
			$items = array();
			$subtotal = 0;
			$totalQuantity = 0;

			foreach ($promoProducts as $product)
			{
				foreach ($order->lineItems as $item)
				{
					/* @var $item Model_LineItem */
					if ($item->getPid() == $product['pid'] && $item->getIsGift() == 'No')
					{
						$items[] = $item;

						$totalQuantity += $item->getFinalQuantity();

						$subtotal += $item->getPriceWithTax() * $item->getFinalQuantity();
					}
				}
			}

			if ($promoData['min_amount_type'] == 'order_subtotal')
			{
				$subtotal = $subtotalAmount;
			}

			if ($subtotal < $promoData['min_amount'])
			{
				return false;
			}

			$this->calculateDiscountsForLineItems($order, $items, $promoData, $totalQuantity, $taxesCalculated);

			if ($persist)
			{
				$this->setOrderPromoData($order, $promoData['promo_type'], $promoData['pid'], $promoData['discount_type'], $promoData['discount'], $order->getPromoDiscountAmount(), $order->getPromoDiscountAmountWithTax());
			}

			return true;
		}

		return false;
	}

	/**
	 * Calculate discount for line items
	 *
	 * @param ORDER $order
	 * @param $items
	 * @param $promoData
	 *
	 * @return float|int
	 */
	protected function calculateDiscountsForLineItems(ORDER $order, $items, &$promoData, $maxQuantity, $taxesCalculated)
	{
		$orderId = $order->getId();

		$totalQuantity = 0;
		$subtotal = 0;
		$subtotalWithTax = 0;
		foreach ($items as $item)
		{
			/* @var $item Model_LineItem */
			$subtotal += $item->getSubtotal();
			$subtotalWithTax += $item->getSubtotal(true);
			$totalQuantity += $item->getFinalQuantity();
		}

		if ($totalQuantity == $maxQuantity && $promoData['discount_type'] == 'amount')
		{
			$discountAmount = 0;
			$discountAmountWithTax = 0;
			foreach ($items as $item)
			{
				$discount = $promoData['discount'];
				if ($discount > $item->getFinalPrice())
				{
					$discount = $item->getSubtotal(false);
				}
				$lineDiscountAmount = $discount * $item->getFinalQuantity();
				$item->setPromoDiscountAmount($lineDiscountAmount);
				$discountAmount += $lineDiscountAmount;

				$discountWithTax = $promoData['discount'];
				if ($taxesCalculated)
				{
					$discountWithTax = PriceHelper::priceWithTax($promoData['discount'], $item->getTaxRate());
				}

				if ($discountWithTax > $item->getPriceWithTax())
				{
					$lineDiscountAmountWithTax = $item->getSubtotal(true);
				}
				else
				{
					$lineDiscountAmountWithTax = $discountWithTax * $item->getFinalQuantity();
				}

				$item->setPromoDiscountAmountWithTax(PriceHelper::round($lineDiscountAmountWithTax));
				$discountAmountWithTax += $lineDiscountAmountWithTax;

				$this->orderRepository->updateLineItemDiscount($orderId, $item->getId(), $item->getDiscountAmount(), $item->getDiscountAmountWithTax(), $item->getPromoDiscountAmount(), $item->getPromoDiscountAmountWithTax());
			}
		}
		else
		{
			if ($totalQuantity > $maxQuantity)
			{
				$totalQuantity = $maxQuantity;
			}

			$dp = $this->getDiscountPercentage($subtotal, $promoData['discount_type'], $promoData['discount'], $totalQuantity);

			$discountAmount = 0;
			$discountAmountWithTax = 0;
			foreach ($items as $item)
			{
				$lineSubTotal = $item->getSubtotal(false);

				$lineDiscountAmount = PriceHelper::round($lineSubTotal * $dp / 100);
				$item->setPromoDiscountAmount($lineDiscountAmount);
				$discountAmount += $lineDiscountAmount;

				$priceWithTax = $item->getPriceWithTax();

				$lineDiscountAmountWithTax = PriceHelper::round($priceWithTax * $dp / 100 * $item->getFinalQuantity());
				$item->setPromoDiscountAmountWithTax($lineDiscountAmountWithTax);
				$discountAmountWithTax += $lineDiscountAmountWithTax;

				$this->orderRepository->updateLineItemDiscount($orderId, $item->getId(), $item->getDiscountAmount(), $item->getDiscountAmountWithTax(), $item->getPromoDiscountAmount(), $item->getPromoDiscountAmountWithTax());
			}
		}
		$order->setPromoDiscountAmount($discountAmount);
		$order->setPromoDiscountAmountWithTax($discountAmountWithTax);
	}

	/**
	 * Get promo discount percentage
	 *
	 * @param $subtotalAmount
	 * @param $discountType
	 * @param $discountValue
	 *
	 * @return float|int
	 */
	protected function getDiscountPercentage($subtotalAmount, $discountType, $discountValue, $discountQuantity)
	{
		//determinate discount percent
		$dp = 0;

		if ($subtotalAmount > 0)
		{
			if ($discountType == 'percent')
			{
				$dp = $discountValue;
			}
			else if ($discountType == 'amount')
			{
				$dp = $discountValue * $discountQuantity / ($subtotalAmount / 100);
			}
		}

		return $dp > 100 ? 100 : $dp;
	}

	/**
	 * Sets order properties for promo code
	 *
	 * @param ORDER $order
	 * @param string $promoType
	 * @param int $promoCampaignId
	 * @param string $promoDiscountType
	 * @param float $promoDiscountValue
	 * @param float $promoDiscountAmount
	 */
	protected function setOrderPromoData(ORDER $order, $promoType, $promoCampaignId, $promoDiscountType, $promoDiscountValue, $promoDiscountAmount, $promoDiscountAmountWithTax)
	{
		$order->setPromoType($promoType);
		$order->setPromoCampaignId($promoCampaignId);
		$order->setPromoDiscountType($promoDiscountType);
		$order->setPromoDiscountValue($promoDiscountValue);
		$order->setPromoDiscountAmount(PriceHelper::round($promoDiscountAmount));
		$order->setPromoDiscountAmountWithTax(PriceHelper::round($promoDiscountAmountWithTax));
	}

	/**
	 * Returns class instance
	 *
	 * @param $settings
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 * @param DataAccess_PromoCodeRepository $promoCodeRepository
	 *
	 * @return Calculator_PromoDiscount
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance(&$settings, DataAccess_OrderRepositoryInterface $orderRepository = null, DataAccess_PromoCodeRepository $promoCodeRepository = null)
	{
		if (is_null(self::$instance))
		{
			if (is_null($orderRepository))
			{
				$orderRepository = DataAccess_OrderRepository::getInstance();
			}

			if (is_null($promoCodeRepository))
			{
				global $db;
				$promoCodeRepository = new DataAccess_PromoCodeRepository($db, $settings);
			}

			self::$instance = new self($orderRepository, $promoCodeRepository, $settings);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_PromoDiscount $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
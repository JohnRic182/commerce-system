<?php
/**
 * Total calculations class
 */
class Calculator_Total
{
	protected static $instance;

	protected $settings;

	public function __construct(&$settings)
	{
		$this->settings = $settings;
	}

	/**
	 * Reset order data
	 * 
	 * @param  ORDER  $order
	 */
	public function reset(ORDER $order)
	{
		$order->setTotalAmount(0);
	}

	/**
	 * Calculate total amount
	 * 
	 * @param  ORDER  $order
	 *
	 * @return float The total amount
	 */
	public function calculate(ORDER $order)
	{
		$subtotal = $order->getSubtotalAmount();
		$taxAmount = $order->getTaxAmount(); // includes subtotal, handling and shipping taxes (if applicable)

		$totalDiscounts = $order->getDiscountAmount();
		if ($order->getPromoType() != 'Shipping')
		{
			$totalDiscounts += $order->getPromoDiscountAmount();
		}
		if ($totalDiscounts > $subtotal)
		{
			$totalDiscounts = $subtotal;
		}

		$shippingAmount = $order->getShippingAmount();
		if ($order->getPromoType() == 'Shipping')
		{
			$shippingDiscountAmount = $order->getPromoDiscountAmount();
			if ($order->getPromoDiscountAmount() > $shippingAmount)
			{
				$shippingDiscountAmount = $shippingAmount;
			}
			$totalDiscounts += $shippingDiscountAmount;
		}

		$totalAmount =
			$subtotal
			+ $shippingAmount // may contain handling amount
			+ ($order->getHandlingSeparated() ? $order->getHandlingAmount() : 0) // if not separated then included in shipping
			- $totalDiscounts
			+ $taxAmount
		;

		$order->setTotalAmount(PriceHelper::round($totalAmount));

		return $totalAmount;
	}

	/**
	 * Get instance
	 *
	 * @param mixed $settings
	 * @return Calculator_Total
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance(&$settings = false)
	{
		if (is_null(self::$instance))
		{
			if (!$settings)
			{
				global $settings;
			}

			self::$instance = new self($settings);
		}

		return self::$instance;
	}

	/**
	 * Set instance
	 *
	 * @param Calculator_Total $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
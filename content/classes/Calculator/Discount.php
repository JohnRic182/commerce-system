<?php
/**
 * Discount amount calculator
 */
class Calculator_Discount
{
	protected static $instance;

	protected $settings;
	protected $orderRepository;
	protected $discountRepository;

    /**
     * Class constructor
     *
     * @param $settings
     * @param DataAccess_OrderRepositoryInterface $orderRepository
     * @param DataAccess_DiscountRepositoryInterface $discountRepository
     */
    public function __construct(&$settings, DataAccess_OrderRepositoryInterface $orderRepository, DataAccess_DiscountRepositoryInterface $discountRepository)
	{
		$this->settings = $settings;
		$this->orderRepository = $orderRepository;
		$this->discountRepository = $discountRepository;
	}

	/**
	 * Reset discounts
	 *
	 * @param ORDER $order
	 */
	public function reset(ORDER $order)
	{
		$order->setDiscountAmount(0);
		$order->setDiscountAmountWithTax(0);
		$order->setDiscountValue(0);
		$order->setDiscountType('none');

		foreach ($order->lineItems as $item)
		{
			/** @var Model_LineItem $item */
			$item->setDiscountAmount(0);
			$item->setPromoDiscountAmount(0);
			$item->setDiscountAmountWithTax(0);
			$item->setPromoDiscountAmountWithTax(0);
		}

		$this->orderRepository->resetLineItemDiscounts($order->getId());
	}

	/**
	 * Calculate discount
	 *
	 * @param ORDER $order
	 * @param bool $reset
	 *
	 * @return string
	 */
	public function calculate(ORDER $order, $reset = true)
	{
		$subtotal = $order->calculateSubtotal(false, true);

		$discountAmount = 0;
		$discountAmountWithTax = 0;

		if ($reset)
		{
			$this->reset($order);

			if ($order->getUserLevel() < 1)
			{
				if ($this->settings["DiscountsActive"] == "YES")
				{
					$discount = $this->discountRepository->getActiveDiscount($subtotal);

					if ($discount)
					{
						$order->setDiscountType($discount["type"]);
						$order->setDiscountValue($discount["discount"]);

						$dp = $this->getDiscountPercentage($subtotal, $order->getDiscountType(), $order->getDiscountValue());

						foreach ($order->lineItems as $item)
						{
							/* @var $item Model_LineItem */
							if ($item->getProductId() == 'gift_certificate')
								continue;

							$lineSubTotal = $item->getSubtotal(false);
							$priceWithTax = $item->getPriceWithTax();

							$lineDiscountAmount = PriceHelper::round($lineSubTotal * $dp / 100);
							$item->setDiscountAmount($lineDiscountAmount);
							$discountAmount += $lineDiscountAmount;

							$lineDiscountAmountWithTax = PriceHelper::round($priceWithTax * $dp / 100 * $item->getFinalQuantity());
							$item->setDiscountAmountWithTax($lineDiscountAmountWithTax);
							$discountAmountWithTax += $lineDiscountAmountWithTax;

							$this->orderRepository->updateLineItemDiscount($order->getId(), $item->getId(), $lineDiscountAmount, $lineDiscountAmountWithTax, $item->getPromoDiscountAmount(), $item->getPromoDiscountAmountWithTax());
						}
					}
				}
			}
		}
		else
		{
			foreach ($order->lineItems as $item)
			{
				/* @var $item Model_LineItem */
				$discountAmount += $item->getDiscountAmount();
				$discountAmountWithTax += $item->getDiscountAmountWithTax();
			}
		}

		$order->setDiscountAmount($discountAmount);
		$order->setDiscountAmountWithTax($discountAmountWithTax);

		return $order->getDiscountAmount();
	}

	/**
	 * Get discount percentage
	 *
	 * @param $subtotalAmount
	 * @param $discountType
	 * @param $discountValue
	 *
	 * @return float|int
	 */
	protected function getDiscountPercentage($subtotalAmount, $discountType, $discountValue)
	{
		//determinate discount percent
		$dp = 0;

		if ($subtotalAmount > 0)
		{
			if ($discountType == 'percent')
			{
				$dp = $discountValue;
			}
			else if ($discountType == 'amount')
			{
				$dp = $discountValue / ($subtotalAmount / 100);
			}
		}

		return $dp > 100 ? 100 : $dp;
	}

	/**
	 * Get class instance
	 *
	 * @param $settings
	 * @param bool $orderRepository
	 * @param bool $discountRepository
	 *
	 * @return Calculator_Discount
	 */
	// @codeCoverageIgnoreStart
	public static function getInstance(&$settings, $orderRepository = false, $discountRepository = false)
	{
		if (is_null(self::$instance))
		{
			if (!$orderRepository)
			{
				$orderRepository = DataAccess_OrderRepository::getInstance();
			}
			if (!$discountRepository)
			{
				$discountRepository = new DataAccess_DiscountRepository();
			}

			self::$instance = new self($settings, $orderRepository, $discountRepository);
		}

		return self::$instance;
	}

	/**
	 * Get instance
	 *
	 * @param Calculator_Discount $instance
	 */
	public static function setInstance($instance)
	{
		self::$instance = $instance;
	}
	// @codeCoverageIgnoreEnd
}
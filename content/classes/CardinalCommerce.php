<?php 

/**
 * @class CardinalCommerce 
 */
class CardinalCommerce
{
	public $is_error = false;
	public $is_fatal_error = false;
	public $error_message = "";

	public $version = "1.6";
	public $pan = "";

	/**
	 * Class constructor
	 * @return this
	 */
	public function __construct()
	{
		global $settings;
		$this->is_error = false;
		$this->error_message = "";
		$_post_url = $settings["cardinalcommerce_URL"];
		$this->post_url = trim($_post_url)==""?"https://centineltest.cardinalcommerce.com/maps/txns.asp":trim($_post_url);
		return $this;
	}

	/**
	 * Processes XML request
	 * @param $xml
	 * @return unknown_type
	 */
	public function processRequest($xml)
	{
		global $settings;
		$ch = curl_init($this->post_url);

		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($ch, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($ch, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($ch, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "cmpi_msg=".$xml);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

		curl_setopt($ch, CURLOPT_TIMEOUT, 300);

		curl_setopt($ch, CURLOPT_SSLVERSION, 1);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($ch, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

		$requestXml = xml2array($xml);
		$this->log('Request:', $requestXml);
		$result = curl_exec($ch);

		if (curl_errno($ch) > 0)
		{
			$this->is_error = true;
			$this->error_message = curl_error($ch);
		}
		elseif ($result == "")
		{
			$this->is_error = true;
			$this->error_message = PAYMENT_TRANSACTION_ERROR_TEXT;
		}
		else
		{
			$resultArray = xml2array($result);
			$this->log('Response:', $resultArray);

			$xmlParser = new XMLParser();
			$parsed = $xmlParser->parse($result);

			if ($parsed["CardinalMPI"][0]["ErrorNo"][0] != "0")
			{
				$this->is_error = true;
				$this->is_fatal_error = true;
				$this->error_message = "Payment gateway response: ".$parsed["CardinalMPI"][0]["ErrorDesc"][0];

				$errorNo = $parsed["CardinalMPI"][0]["ErrorNo"][0];
				switch ($errorNo)
				{
					case 350:
					case 1001:
					case 1002:
					case 1051:
					case 1055:
					case 1060:
					case 1120:
					case 1125:
					case 1130:
					case 1150:
					case 1160:
					case 1140:
					case 1355:
					case 1360:
					case 1390:
					case 1400:
					case 1710:
					case 1755:
					case 1789:
					case 1085:
					case 2001:
					case 2003:
					case 2006:
					case 2007:
					case 2009:
					case 2010:
					case 4000:
					case 4020:
					case 4025:
					case 4240:
					case 4243:
					case 4245:
					case 4268:
					case 4310:
					case 4375:
					case 4400:
					case 4770:
					case 4780:
					case 4790:
					case 4800:
					case 4810:
					case 4820:
					case 4900:
					case 4930:
					case 4951:
					case 4963:
					case 4965:
						$this->is_fatal_error = false;
						break;
				}
			}
			else
			{
				return $parsed;
			}
		}
		return false;
	}

	/**
	 * Writer message into log
	 * @param string $s
	 */
	public function log($s, $values = array())
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			if (!is_array($values))
			{
				$values = array($values);
			}

			if ($values)
			{
				if (array_key_exists('CardNumber', $values))
				{
					$values['CardNumber'] = str_repeat('X', 16);
				}
				if (array_key_exists('TransactionPwd', $values))
				{
					$values['TransactionPwd'] = 'XXXXX';
				}
			}

			file_put_contents(
				dirname(dirname(__FILE__))."/cache/log/payment-cardinal.txt",
				date("Y/m/d-H:i:s")." - "."\n".(is_array($s) ? array2text($s) : $s).(count($values) > 0 ? "\n".array2text($values) : '')."\n\n\n",
				FILE_APPEND
			);
		}
	}

	/**
	 * Get cmpi_lookup xml
	 * @param $card_number
	 * @param $card_exp_month
	 * @param $card_exp_year
	 * @return string
	 */
	public function getLookupXML($first_name, $last_name, $card_number, $card_exp_month, $card_exp_year, $currency)
	{
		global $settings;
		$currency = strtoupper($currency);
		$currencies = array("USD" => "840", "EUR" => "978", "JPY" => "392", "CAD" => "124", "GBP" => "826");
		if (!array_key_exists($currency, $currencies)) $currency = "USD";

		$xml = 
			"<CardinalMPI>".
				"<MsgType>cmpi_lookup</MsgType>".
				"<Version>".utf8_encode($this->version)."</Version>".
				"<ProcessorId>".utf8_encode($settings["cardinalcommerce_Processor_Id"])."</ProcessorId>".
				"<MerchantId>".utf8_encode($settings["cardinalcommerce_Merchant_Id"])."</MerchantId>".
				"<TransactionPwd>".utf8_encode($settings["cardinalcommerce_Password"])."</TransactionPwd>".
				"<TransactionType>C</TransactionType>".
				"<Amount>".utf8_encode(number_format($this->common_vars["order_total_amount"]["value"], 2, "", ""))."</Amount>".
				"<CurrencyCode>".$currencies[$currency]."</CurrencyCode>".
				"<CardNumber>".utf8_encode($card_number)."</CardNumber>".
				"<CardExpMonth>".utf8_encode($card_exp_month)."</CardExpMonth>".
				"<CardExpYear>".utf8_encode($card_exp_year)."</CardExpYear>".
				"<OrderNumber>".utf8_encode($this->common_vars["order_id"]["value"])."</OrderNumber>".
				"<UserAgent>".utf8_encode($_SERVER["HTTP_USER_AGENT"])."</UserAgent>".
				"<BrowserHeader>".utf8_encode($_SERVER["HTTP_ACCEPT"])."</BrowserHeader>".
				"<Recurring>N</Recurring>".
				"<EMail>".utf8_encode($this->common_vars["billing_email"]["value"])."</EMail>".
				"<IPAddress>".utf8_encode($_SERVER["REMOTE_ADDR"])."</IPAddress>".
				"<BillingFirstName>".utf8_encode($first_name)."</BillingFirstName>".
				"<BillingLastName>".utf8_encode($last_name)."</BillingLastName>".
			"</CardinalMPI>";
		return $xml;
	}

	/**
	 * Get cmpi_authenticate xml
	 * @param $PAResPayload
	 * @return string
	 */
	public function getAuthenticateXML($PAResPayload)
	{
		global $settings;
		$xml = 
			"<CardinalMPI>".
				"<MsgType>cmpi_authenticate</MsgType>".
				"<Version>1.6</Version>".
				"<ProcessorId>".utf8_encode($settings["cardinalcommerce_Processor_Id"])."</ProcessorId>".
				"<MerchantId>".utf8_encode($settings["cardinalcommerce_Merchant_Id"])."</MerchantId>".
				"<TransactionPwd>".utf8_encode($settings["cardinalcommerce_Password"])."</TransactionPwd>".
				"<TransactionType>C</TransactionType>".
				"<TransactionId>".utf8_encode($_SESSION["CardinalTransactionId"])."</TransactionId>".
				"<PAResPayload>".urlencode($PAResPayload)."</PAResPayload>".
			"</CardinalMPI>";
		return $xml;
	}
}

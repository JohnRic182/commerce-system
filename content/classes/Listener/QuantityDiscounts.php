<?php

/**
 * Class Listener_QuantityDiscounts
 */
class Listener_QuantityDiscounts
{
	protected static $orderRepository;
	protected static $productsRepository;

	/**
	 * @return DataAccess_OrderRepository
	 */
	protected static function getOrderRepository()
	{
		if (is_null(self::$orderRepository))
		{
			self::$orderRepository = DataAccess_OrderRepository::getInstance();
		}

		return self::$orderRepository;
	}

	/**
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 */
	public static function setOrderRepository(DataAccess_OrderRepositoryInterface $orderRepository)
	{
		self::$orderRepository = $orderRepository;
	}

	/**
	 * @return DataAccess_ProductsRepository
	 */
	protected static function getProductsRepository()
	{
		if (is_null(self::$productsRepository))
		{
			global $db;
			self::$productsRepository = new DataAccess_ProductsRepository($db);
		}

		return self::$productsRepository;
	}

	/**
	 * @param DataAccess_ProductsRepositoryInterface $productsRepository
	 */
	public static function setProductsRepository(DataAccess_ProductsRepositoryInterface $productsRepository)
	{
		self::$productsRepository = $productsRepository;
	}

	/**
	 * @param Events_OrderEvent $event
	 */
	public static function checkQuantityDiscounts(Events_OrderEvent $event)
	{
		$order = $event->getOrder();

		$lineItemsMap = array();

		/** @var Model_LineItem $item */
		foreach ($order->lineItems as $item)
		{
			/* @var $item Model_LineItem */

			if (strtolower($item->getIsGift()) == 'yes' || $item->getProductId() == 'gift_certificate') continue;

			if (!isset($lineItemsMap[$item->getPid()]))
			{
				$lineItemsMap[$item->getPid()] = array();
			}

			$lineItemsMap[$item->getPid()][] = $item;
		}

		foreach ($lineItemsMap as $pid => $items)
		{
			$lineItemsTotal = 0;
			$quantity = 0;
			foreach ($items as $item)
			{
				/* @var $item Model_LineItem */

				$lineItemsTotal += $item->getPriceBeforeQuantityDiscount() * $item->getFinalQuantity();

				$quantity += $item->getFinalQuantity();
			}

			$item->setFreeShipping($item->getProductFreeShipping());

			$discount = self::getProductsRepository()->getProductsQuantityDiscounts($pid, $quantity);

			if ($discount)
			{
				if (($order->getUserLevel() < 1) || ($order->getUserLevel() > 0 && $discount["apply_to_wholesale"] == "Yes"))
				{
					if ($discount["discount_type"] == "amount")
					{
						$discountAmount = $discount['discount'];

						if ($discountAmount >= $lineItemsTotal)
						{
							$discountAmount = $lineItemsTotal;
						}
						else if ($discountAmount < 0)
						{
							$discountAmount = 0;
						}

						$discountPercentage = ($discountAmount * $quantity) / $lineItemsTotal;
					}
					else
					{
						$discountPercentage = $discount['discount'] / 100;
					}

					if ($discountPercentage > 1)
					{
						$discountPercentage = 1;
					}
					else if ($discountPercentage < 0)
					{
						$discountPercentage = 0;
					}

					/** Model_LineItem $item */
					foreach ($items as $item)
					{
						$item->setFinalPrice(PriceHelper::round($item->getPriceBeforeQuantityDiscount() - $item->getPriceBeforeQuantityDiscount() * $discountPercentage));

						$priceWithTax = PriceHelper::priceWithTax($item->getPriceBeforeQuantityDiscount(), $item->getTaxRate());

						//$priceWithTax = $item->getPriceWithTax();

						$item->setPriceWithTax(PriceHelper::round($priceWithTax - $priceWithTax * $discountPercentage));

						if ($discount["free_shipping"] == "Yes")
						{
							$item->setFreeShipping('Yes');
						}

						self::getOrderRepository()->updateLineItemQuantityPricing($order->getId(), $item->getId(), $item->getFinalPrice(), $item->getPriceWithTax(), $item->getFreeShipping());
					}
				}
			}
		}
	}
}
<?php

class Listener_GiftCertificate
{
	/**
	 * Handle on fulfillment completed event
	 *
	 * @param Events_FulfillmentEvent $event
	 */
	public static function onFulfillmentCompleted(Events_FulfillmentEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();
		$fulfillment = $event->getFulfillment();
		$oid = $order->getId();

		if ($fulfillment->getStatus() !== Model_Fulfillment::STATUS_COMPLETED) return;

		$fulfillmentHasGiftCert = false;
		foreach ($fulfillment->getItems() as $item)
		{
			/** @var Model_FulfillmentItem $item */
			$lineItemId = $item->getLineItemId();
			if (isset($order->lineItems[$lineItemId]))
			{
				/** @var Model_LineItem $lineItem */
				$lineItem = $order->lineItems[$lineItemId];
				if ($lineItem->getProductId() == 'gift_certificate')
				{
					$fulfillmentHasGiftCert = true;
					break;
				}
			}
		}

		if ($fulfillmentHasGiftCert)
		{
			$repository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
			$gift_data = $repository->getGiftCertificateByOrderId($oid);

			if ($gift_data !== false)
			{
				// the user has ordered a gift certificate
				$repository->setGiftCertificateOrdered($gift_data);

				if ($gift_data['email_gift'] == '1')
				{
					view()->initNotificationsSmarty();
					Notifications::emailGiftCertificate($gift_data);
				}
			}
		}
	}
}
<?php

class Listener_ProductLocations
{
	/**
	 * Handle on order status change event
	 * 
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderStatusChange(Events_OrderEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();
		$oid = $order->getId();

		$oldOrderStatus = $event->getData('oldOrderStatus');
		$oldPaymentStatus = $event->getData('oldPaymentStatus');
		$newOrderStatus = $event->getData('newOrderStatus');
		$newPaymentStatus = $event->getData('newPaymentStatus');

		if ($oldPaymentStatus != ORDER::PAYMENT_STATUS_RECEIVED && $newPaymentStatus == ORDER::PAYMENT_STATUS_RECEIVED && $settings["ProductsLocationsSendOption"] == "user")
		{
			view()->initNotificationsSmarty();
			Notifications::emailProductsLocationsNotify($db, $oid);
		}
		else if ($oldOrderStatus != ORDER::STATUS_COMPLETED && $newOrderStatus == ORDER::STATUS_COMPLETED && $settings['ProductsLocationsSendOption'] == 'admin' && $order->getPaymentStatus() == ORDER::PAYMENT_STATUS_RECEIVED)
		{
			//require_once dirname(dirname(dirname(__FILE__))).'/admin/admin_init_smarty.php';

			Notifications::emailProductsLocationsNotify($db, $oid);
		}
	}
}
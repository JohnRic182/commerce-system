<?php

class Listener_ProductGifts
{
	protected static $orderRepository;

	protected static function getOrderRepository()
	{
		if (is_null(self::$orderRepository))
		{
			self::$orderRepository = DataAccess_OrderRepository::getInstance();
		}

		return self::$orderRepository;
	}

	public static function setOrderRepository(DataAccess_OrderRepositoryInterface $orderRepository)
	{
		self::$orderRepository = $orderRepository;
	}

	private static $currentlyRunning = false;

	public static function checkFreeProducts(Events_OrderEvent $event)
	{
		if (self::$currentlyRunning) return;

		self::$currentlyRunning = true;

		$orderRepository = self::getOrderRepository();

		$order = $event->getOrder();

		$items = array();
		$lineItemsIdsToDelete = array();
		$productQuantitiesMap = array();
		$lineItemsToDelete = array();
		foreach ($order->lineItems as $id => $item)
		{
			/* @var $item Model_LineItem */
			if ($item->getIsGift() == 'Yes')
			{
				$lineItemsIdsToDelete[] = $id;
				$lineItemsToDelete[] = $item;
			}
			else
			{
				$items[] = $item->toArray();

				if (array_key_exists($item->getPid(), $productQuantitiesMap))
				{
					$productQuantitiesMap[$item->getPid()] += $item->getFinalQuantity();
				}
				else
				{
					$productQuantitiesMap[$item->getPid()] = $item->getFinalQuantity();
				}
			}
		}

		foreach ($lineItemsIdsToDelete as $id)
		{
			unset($order->lineItems[$id]);
		}
		$order->items = $items;

		if (count($lineItemsToDelete) > 0)
		{
			$event = new Events_OrderEvent(Events_OrderEvent::ON_REMOVE_ITEM);
			$event->setOrder($order);
			$event->setData('removedItems', $lineItemsToDelete);
			Events_EventHandler::handle($event);
		}

		$orderRepository->deleteLineItems($order->getId(), $lineItemsIdsToDelete);

		$productsGiftsData = $orderRepository->getProductsGiftsData($order->getId());

		$productGiftsMap = array();
		foreach ($productsGiftsData as $productGiftData)
		{
			$originalPid = $productGiftData['original_pid'];

			if (array_key_exists($originalPid, $productQuantitiesMap) && $productGiftData['gift_min_quantity'] > 0)
			{
				if (!isset($productGiftsMap[$originalPid])) $productGiftsMap[$originalPid] = array();

				if (isset($productGiftsMap[$originalPid][$productGiftData['pid']]) && $productGiftsMap[$originalPid][$productGiftData['pid']])
				{
					//Do not allow for more than gifts if we've already added for another instance of the same product
					continue;
				}

				$quantity = $productQuantitiesMap[$originalPid];
				if ($quantity >= $productGiftData['gift_min_quantity'])
				{
					$multiplier = floor($quantity / $productGiftData['gift_min_quantity']);
					$giftQuantity = $multiplier * $productGiftData['gift_quantity'];

					if ($giftQuantity > $productGiftData['gift_max_quantity'])
					{
						$giftQuantity = $productGiftData['gift_max_quantity'];
					}

					if ($giftQuantity > 0)
					{
						$productGiftsMap[$originalPid][$productGiftData['pid']] = true;

							// Duplicates a bit of $order->addItem(
						$productGiftData['free_shipping'] = $productGiftData['gift_shipping_free'];
						$lineItem = Model_LineItem::createNewGiftLineItem($order->getId(), new Model_Product($productGiftData), $giftQuantity);

						$shipmentRepository = new DataAccess_OrderShipmentRepository($order->db(), $order->getSettingsRepository());
						$shipmentRepository->assignShipment($lineItem);

						$orderRepository->persistLineItem($order, $lineItem, true);

						$order->lineItems[$lineItem->getId()] = $lineItem;
						$order->items[] = $lineItem->toArray();
					}
				}
			}
		}

		self::$currentlyRunning = false;
	}
}
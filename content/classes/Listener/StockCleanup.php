<?php

class Listener_StockCleanup
{
	public static function onCron()
	{
		global $db, $settings;

		$orderIds = $db->selectAll('SELECT o.oid, o.uid
FROM '.DB_PREFIX.'orders o
	INNER JOIN '.DB_PREFIX.'orders_content oc ON o.oid = oc.oid AND oc.is_stock_changed = "Yes" AND (o.status = "New" OR o.status = "Abandon")
WHERE DATE_ADD(o.status_date, INTERVAL 2 HOUR) < NOW()
GROUP BY o.oid');

		foreach ($orderIds as $orderId)
		{
			$user = USER::getById($db, $settings, $orderId['uid']);
			$order = ORDER::getById($db, $settings, $user, $orderId['oid']);

			$user->getUserData();
			$order->getOrderData();
			$order->getOrderItems(true);

			Listener_StockControl::returnToInventory($order);
		}
	}
}

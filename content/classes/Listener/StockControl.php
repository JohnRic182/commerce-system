<?php

class Listener_StockControl
{
	/**
	 * Handle on order status change event
	 * 
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderStatusChange(Events_OrderEvent $event)
	{
		global $settings;

		/** @var ORDER $order */
		$order = $event->getOrder();

		$oldOrderStatus = $event->getData('oldOrderStatus');
		$oldPaymentStatus = $event->getData('oldPaymentStatus');
		$newOrderStatus = $event->getData('newOrderStatus');
		$newPaymentStatus = $event->getData('newPaymentStatus');

		if ($event->getEventName() == Events_OrderEvent::ON_BEFORE_PAYMENT && $settings['AllowOutOfInventoryOrders'] == 'Yes') return;

		if (
			(($settings['InventoryStockUpdateAt'] == 'Order Placed' || $settings['InventoryStockUpdateAt'] == 'Add To Cart') && $event->getEventName() == Events_OrderEvent::ON_BEFORE_PAYMENT) ||
			(in_array($oldOrderStatus, array(ORDER::STATUS_ABANDON, ORDER::STATUS_NEW)) && in_array($newOrderStatus, array(ORDER::STATUS_PROCESS, ORDER::STATUS_COMPLETED)) && $settings["InventoryStockUpdateAt"] == "Order Placed")
			|| ($newPaymentStatus == ORDER::PAYMENT_STATUS_RECEIVED && $oldPaymentStatus != ORDER::PAYMENT_STATUS_RECEIVED && $settings["InventoryStockUpdateAt"] == "Payment Received")
			|| ($newOrderStatus == ORDER::STATUS_COMPLETED && $oldOrderStatus != ORDER::STATUS_COMPLETED && $settings["InventoryStockUpdateAt"] == "Order Completed")
		)
		{
			self::stockControl($order);

			/** @var Model_LineItem $item */
			foreach ($order->lineItems as $item)
			{
				if ($item->getInventoryControl() == 'No' || ($item->getInventoryControl() == 'AttrRuleInc' && $item->getInventoryId() == '0')) continue;

				if ($item->getFinalQuantity() > $item->getQuantityFromStock())
				{
					global $msg;
					$event->setError(sprintf($msg['cart']['error_stock_limited'], $item->getTitle()));
				}
			}
		}
		else if ($event->getEventName() == Events_OrderEvent::ON_FAILED_PAYMENT ||
			($newOrderStatus == ORDER::STATUS_CANCELED && $oldOrderStatus != ORDER::STATUS_CANCELED) ||
			$newOrderStatus == ORDER::STATUS_FAILED)
		{
			self::returnToInventory($order);
		}
	}

	/**
	 * Handle line item updates when inventory stock update at "Add To Cart"
	 *
	 * @param Events_OrderEvent $event
	 */
	public static function checkItemStockLevels(Events_OrderEvent $event)
	{
		global $settings;

		$order = $event->getOrder();
		/** @var Model_LineItem $item */
		$item = $event->getData('item');

		if (!is_null($item))
		{
			if ($item->getQuantityFromStock() > 0 || $settings['InventoryStockUpdateAt'] == 'Add To Cart')
			{
				if ($event->getEventName() == Events_OrderEvent::ON_REMOVE_ITEM)
				{
					$item->setFinalQuantity(0);
				}

				self::adjustStockLevels($order, $item);
			}
			return;
		}

		$items = $event->getData('removedItems');
		if (!is_null($items) && is_array($items) && count($items))
		{
			foreach ($items as $item)
			{
				if ($item->getQuantityFromStock() > 0)
				{
					$item->setFinalQuantity(0);
					self::adjustStockLevels($order, $item);
				}
			}
		}

		foreach ($order->lineItems as $item)
		{
			if ($item->getQuantityFromStock() > 0 || $settings['InventoryStockUpdateAt'] == 'Add To Cart')
			{
				self::adjustStockLevels($order, $item);
			}
		}
	}

	/**
	 * Adjust the stock levels on item update
	 *
	 * @param ORDER $order
	 * @param Model_LineItem $item
	 */
	protected static function adjustStockLevels(ORDER $order, Model_LineItem $item)
	{
		$inventoryControl = self::getInventoryControl();

		$quantityFromStock = $item->getQuantityFromStock();
		$stockAdjustment = $item->getFinalQuantity() - $quantityFromStock;

		if ($stockAdjustment == 0)
		{
			return;
		}

		if ($stockAdjustment > 0)
		{
			$decrementQuantity = $inventoryControl->decrementStock($item->getPid(), $stockAdjustment, $item->getInventoryControl() != 'Yes' ? $item->getInventoryId() : 0);
			if ($decrementQuantity === false)
			{
				return;
			}

			$quantityFromStock += $decrementQuantity;

			$item->setQuantityFromStock($quantityFromStock);

			if ($item->getFinalQuantity() > $item->getQuantityFromStock())
			{
				global $msg;
				$order->setError(sprintf($msg['cart']['error_stock_limited'], $item->getTitle()));
			}

			$orderRepository = new DataAccess_OrderRepository();
			$orderRepository->persistLineItem($order, $item);
		}
		else if ($stockAdjustment < 0)
		{
			if ($inventoryControl->incrementStock($item->getPid(), 0 - $stockAdjustment, $item->getInventoryControl() != 'Yes' ? $item->getInventoryId() : 0))
			{
				$quantityFromStock += $stockAdjustment; // $stockAdjustment is negative, add to subtract
				$item->setQuantityFromStock($quantityFromStock);

				$orderRepository = new DataAccess_OrderRepository();
				$orderRepository->persistLineItem($order, $item);
			}
		}
	}

	/**
	 * Stock control
	 * at this point inventory control based on 2 "after checkout" conditions
	 * 1) stock_warning should be < then stock
	 * 2) min_order should be <= then stock
	 */
	public static function stockControl(ORDER $order)
	{
		global $db;

		$inventoryControl = self::getInventoryControl();

		// select orders products with allowed stock control from database
		$db->query("
			SELECT
				oc.ocid,
				oc.pid,
				oc.admin_quantity,
				oc.quantity_from_stock,
				oc.inventory_id,
				p.inventory_control
			FROM ".DB_PREFIX."orders_content oc
			LEFT JOIN ".DB_PREFIX."products p ON oc.pid = p.pid AND
				(p.inventory_control = 'Yes' OR p.inventory_control = 'AttrRuleExc' OR p.inventory_control = 'AttrRuleInc')
			WHERE 
				oc.oid=".$order->getId()." AND oc.admin_quantity > oc.quantity_from_stock
		");

		$items = array();
		while ($db->moveNext())
		{
			$items[$db->col["ocid"]] = array(
				"pid"=>$db->col["pid"],
				"quantity"=> $db->col["admin_quantity"] - $db->col['quantity_from_stock'],
				"inventory_id"=>$db->col["inventory_id"],
				'inventory_control' => $db->col['inventory_control'],
			);
		}

		// update stock for products
		reset($items);
		foreach ($items as $itemId => $itemData)
		{
			if (!isset($order->lineItems[$itemId])) continue;

			/** @var Model_LineItem $item */
			$item = $order->lineItems[$itemId];

			$stockDecremented = $inventoryControl->decrementStock($itemData['pid'], $itemData['quantity'], $itemData["inventory_id"]);

			if ($stockDecremented > 0)
			{
				//TODO: Move this into inventoryControl decrementStock?
				//Increment quantity from stock
				$quantityFromStock = $item->getQuantityFromStock() + $stockDecremented;
				$item->setQuantityFromStock($quantityFromStock);
				$item->setIsStockChanged('Yes');

				$db->reset();
				$db->assign('quantity_from_stock', $quantityFromStock);
				$db->assignStr("is_stock_changed", "Yes");
				$db->update(DB_PREFIX."orders_content", "WHERE ocid=".intval($itemId));
			}
		}
	}

	/**
	 * Public function return items to inventory
	 *
	 * @param ORDER $order
	 */
	public static function returnToInventory(ORDER $order)
	{
		global $db;

		$inventoryControl = self::getInventoryControl();

		$db->query("
			SELECT
				oc.ocid,
				oc.quantity_from_stock,
				oc.pid,
				oc.product_id,
				oc.inventory_id
			FROM ".DB_PREFIX."orders_content oc
				INNER JOIN ".DB_PREFIX."products p ON oc.pid = p.pid AND p.inventory_control <> 'No'
				LEFT OUTER JOIN ".DB_PREFIX."products_inventory pinv ON oc.inventory_id = pinv.pi_id
			WHERE oc.oid = ".$order->getId()." AND oc.quantity_from_stock > 0
		");

		if ($db->numRows() > 0)
		{
			$items = $db->getRecords();

			foreach ($items as $itemData)
			{
				/** @var Model_LineItem $item */
				$item = $order->lineItems[$itemData['ocid']];

				$inventoryControl->incrementStock($itemData['pid'], $itemData['quantity_from_stock'], $itemData['inventory_id']);

				$item->setQuantityFromStock(0);
				$item->setIsStockChanged('No');
			}
			$db->query("UPDATE ".DB_PREFIX."orders_content SET is_stock_changed='No', quantity_from_stock = 0 WHERE oid=".$order->getId());
		}
	}

	protected static $inventoryControl;
	protected static function getInventoryControl()
	{
		if (is_null(self::$inventoryControl))
		{
			self::$inventoryControl = new Model_InventoryControl();
		}

		return self::$inventoryControl;
	}
}
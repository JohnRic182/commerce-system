<?php

class Payment_PayPal_FinancingApiException extends Exception
{
	public $errors;

	public function __construct($errors)
	{
		parent::__construct('PayPal Financing Api Exception: '.print_r($errors,1));

		$this->errors = is_array($errors) ? $errors : array('' => $errors);
	}
}
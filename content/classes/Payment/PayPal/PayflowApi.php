<?php

class Payment_PayPal_PayflowApi
{
	protected $liveMode = true;
	protected $version = '3.3';

	protected $apiUsername;
	protected $apiVendor;
	protected $apiPartner;
	protected $apiPassword;

	protected $settings;
	protected $buttonSource;

	public function __construct($settings = null, $liveMode = true)
	{
		$this->setLiveMode($liveMode);

		if (!is_null($settings))
		{
			$this->settings = $settings;
		}
		else
		{
			global $settings;
			$this->settings = $settings;
		}
	}

	/**
	 * Gets buttonSource
	 *
	 * @return string
	 */
	public function getButtonSource()
	{
		return $this->buttonSource;
	}
	/**
	 * Sets buttonSource
	 *
	 * @param string $buttonSource
	 */
	public function setButtonSource($buttonSource)
	{
		$this->buttonSource = $buttonSource;
	}

	/**
	 * Gets liveMode
	 *
	 * @return boolean
	 */
	public function getLiveMode()
	{
		return $this->liveMode;
	}
	/**
	 * Sets liveMode
	 *
	 * @param boolean $liveMode
	 */
	public function setLiveMode($liveMode)
	{
		$this->liveMode = $liveMode;
	}

	public function getApiEndpoint()
	{
		if ($this->liveMode)
		{
			return 'https://payflowpro.paypal.com';
		}
		else
		{
			return 'https://pilot-payflowpro.paypal.com';
		}
	}

	/**
	 * Gets apiUsername
	 *
	 * @return string
	 */
	public function getApiUsername()
	{
		if (trim($this->apiUsername) != '')
			return $this->apiUsername;

		return $this->apiVendor;
	}
	/**
	 * Sets apiUsername
	 *
	 * @param string $apiUsername
	 */
	public function setApiUsername($apiUsername)
	{
		$this->apiUsername = $apiUsername;
	}

	/**
	 * Gets apiVendor
	 *
	 * @return string
	 */
	public function getApiVendor()
	{
		return $this->apiVendor;
	}
	/**
	 * Sets apiVendor
	 *
	 * @param string $apiVendor
	 */
	public function setApiVendor($apiVendor)
	{
		$this->apiVendor = $apiVendor;
	}

	/**
	 * Gets apiPartner
	 *
	 * @return string
	 */
	public function getApiPartner()
	{
		return $this->apiPartner;
	}
	/**
	 * Sets apiPartner
	 *
	 * @param string $apiPartner
	 */
	public function setApiPartner($apiPartner)
	{
		$this->apiPartner = $apiPartner;
	}

	/**
	 * Gets apiPassword
	 *
	 * @return string
	 */
	public function getApiPassword()
	{
		return $this->apiPassword;
	}
	/**
	 * Sets apiPassword
	 *
	 * @param string $apiPassword
	 */
	public function setApiPassword($apiPassword)
	{
		$this->apiPassword = $apiPassword;
	}

	/**
	 * Writer message into log
	 * @param string $s 
	 */
	public function log($s, array $values = array())
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			self::cleanData($values);

			file_put_contents(
				"content/cache/log/payflow.txt", 
				date("Y/m/d-H:i:s")." - Payflow\n".(is_array($s) ? array2text($s) : $s)."\n".array2text($values)."\n\n\n",
				FILE_APPEND
			);
		}
	}

	public function getExpressCheckoutUrl($token, $fromCart = false)
	{
		$baseUrl = $this->getLiveMode() ? 'www.paypal.com' : 'www.sandbox.paypal.com';

		return 'https://'.$baseUrl.'/cgi-bin/webscr?cmd=_express-checkout&token='.$token.($fromCart ? '' : '&useraction=commit');
	}

	public function setExpressCheckout(
		$transactionType, $orderTotal, $currencyCode, $returnUrl, $cancelUrl, 
		$noShipping = false, $lines = false, $uxParams = false, $orderAmounts = false, 
		$noShippingAddress = false, $order, $billMeLater = false
	)
	{
//		// Currently, BillMeLater is not implemented for Payflow gateway
//		if ($billMeLater)
//		{
//			return array();
//		}

		$params = array(
			'TRXTYPE' => $transactionType,
			'ACTION' => 'S',
			'AMT' => $_orderTotal = number_format($orderTotal, 2, '.', ''),
			'CURRENCY' => $currencyCode,
			'CANCELURL' => $cancelUrl,
			'RETURNURL' => $returnUrl,
			'TENDER' => 'P',
		);

		if ($noShipping)
		{
			$params['NOSHIPPING'] = '1';
			$params['REQCONFIRMSHIPPING'] = '0';
			if ($transactionType == 'Sale')
				$params['PAYMENTTYPE'] = 'instantonly';
		}

		// Bug at PayPal side, ADDROVERRIDE still allows user to change shipping address, but only on Payflow
		// if ($noShippingAddress && $order !== null && $order->shippingAddress !== null && isset($order->shippingAddress['address1']) && trim($order->shippingAddress['address1']) != '')
		// {
		// 	$params['ADDROVERRIDE'] = 1;
		// 	$params['PAYMENTREQUEST_SHIPTONAME'] = $order->shippingAddress['name'];
		// 	$params['PAYMENTREQUEST_SHIPTOSTREET'] = $order->shippingAddress['address1'];
		// 	$params['PAYMENTREQUEST_SHIPTOSTREET2'] = $order->shippingAddress['address2'];
		// 	$params['PAYMENTREQUEST_SHIPTOCITY'] = $order->shippingAddress['city'];
		// 	$params['PAYMENTREQUEST_SHIPTOSTATE'] = trim($order->shippingAddress['state_abbr']) != '' ? $order->shippingAddress['state_abbr'] : $order->shippingAddress['province'];
		// 	$params['PAYMENTREQUEST_SHIPTOCOUNTRYCODE'] = $order->shippingAddress['country_iso_a2'];
		// 	$params['PAYMENTREQUEST_SHIPTOZIP'] = $order->shippingAddress['zip'];
		// }
		// else 
		if ($noShippingAddress)
		{
			$params['NOSHIPPING'] = '1';
		}

		if (is_array($lines) && $this->settings['paypalec_Send_Line_Item_Details'] == 'Yes')
		{
			$index = 0;
			$itemAmt = 0;
			
			$discountAmt = 0;

			foreach ($lines as $line)
			{
				if ($line['amt'] < 0)
				{
					$discountAmt += (0 - $line['amt']);
				}
				else
				{
					$params['L_NAME'.$index] = substr($line['name'], 0, 127);
					$params['L_DESC'.$index] = substr($line['desc'], 0, 127);
					$params['L_COST'.$index] = number_format($line['amt'], 2, '.', '');
					$params['L_QTY'.$index] = $line['qty'];
					$params['L_TAX'.$index] = number_format($line['tax'], 2, '.', '');

					// No line tax amount, because with discounts, multiple quantity, caused errors with PayPal (off by even 1 cent caused errors)
					// Now we just send total tax amount for the order

					$itemAmt += $line['qty'] * $line['amt'];
					$index++;
				}
			}

			$params['TAXAMT'] = '0.00';
			$params['HANDLINGAMT'] = '0.00';
			$params['SHIPPINGAMT'] = '0.00';

			if ($discountAmt > 0)
			{
				$params['DISCOUNT'] = number_format($discountAmt, 2, '.', '');
			}

			if ($orderAmounts)
			{
				$params['TAXAMT'] = $_taxAmount = number_format(isset($orderAmounts["tax_amount"]) ? $orderAmounts["tax_amount"] : '0.00', 2, '.', '');
				$params['HANDLINGAMT'] = $_handlingAmount = number_format(isset($orderAmounts["handling_amount"]) ? $orderAmounts["handling_amount"] : '0.00', 2, '.', '');
				$params['SHIPPINGAMT'] = $_shippingAmount = number_format(isset($orderAmounts["shipping_amount"]) ? $orderAmounts["shipping_amount"] : '0.00', 2, '.', '');
			}
			
			$params['ITEMAMT'] = number_format($itemAmt, 2, '.', '');
		}

		// Bill Me Later
		if ($billMeLater)
		{
			$params['USERSELECTEDFUNDINGSOURCE'] = 'BML';
			$params['SOLUTIONTYPE'] = 'SOLE';
		}

		if (is_array($uxParams))
		{
			foreach ($uxParams as $key => $value)
			{
				$params[$key] = $value;
			}
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	public function getExpressCheckoutDetails($transactionType, $token)
	{
		$params = array(
			'TRXTYPE' => $transactionType,
			'ACTION' => 'G',
			'TOKEN' => $token,
			'TENDER' => 'P',
		);

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	public function doExpressCheckoutPayment($transactionType, $token, $payerId, $orderTotal, $currencyCode, $notifyUrl = null, array $lines = null, array $orderAmounts = null, $orderNumber = null)
	{
		$params = array(
			'TRXTYPE' => $transactionType,
			'ACTION' => 'D',
			'TOKEN' => $token,
			'TENDER' => 'P',
			'PAYERID' => $payerId,
			'AMT' => number_format($orderTotal, 2, '.', ''),
			'CURRENCY' => $currencyCode,
		);

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		if (is_array($lines) && $this->settings['paypalec_Send_Line_Item_Details'] == 'Yes')
		{
			$index = 0;
			$itemAmt = 0;

			$discountAmt = 0;

			foreach ($lines as $line)
			{
				if ($line['amt'] < 0)
				{
					$discountAmt += (0 - $line['amt']);
				}
				else
				{
					$params['L_NAME'.$index] = substr($line['name'], 0, 127);
					$params['L_DESC'.$index] = substr($line['desc'], 0, 127);
					$params['L_COST'.$index] = number_format($line['amt'], 2, '.', '');
					$params['L_QTY'.$index] = $line['qty'];
					$params['L_TAX'.$index] = number_format($line['tax'], 2, '.', '');

					// No line tax amount, because with discounts, multiple quantity, caused errors with PayPal (off by even 1 cent caused errors)
					// Now we just send total tax amount for the order

					$itemAmt += $line['qty'] * $line['amt'];
					$index++;
				}
			}

			$params['TAXAMT'] = '0.00';
			$params['HANDLINGAMT'] = '0.00';
			$params['SHIPPINGAMT'] = '0.00';
			$params['INVNUM'] = $orderNumber;

			if ($discountAmt > 0)
			{
				$params['DISCOUNT'] = number_format($discountAmt, 2, '.', '');
			}

			if ($orderAmounts)
			{
				$params['TAXAMT'] = $_taxAmount = number_format(isset($orderAmounts["tax_amount"]) ? $orderAmounts["tax_amount"] : '0.00', 2, '.', '');
				$params['HANDLINGAMT'] = $_handlingAmount = number_format(isset($orderAmounts["handling_amount"]) ? $orderAmounts["handling_amount"] : '0.00', 2, '.', '');
				$params['SHIPPINGAMT'] = $_shippingAmount = number_format(isset($orderAmounts["shipping_amount"]) ? $orderAmounts["shipping_amount"] : '0.00', 2, '.', '');
			}

			$params['ITEMAMT'] = number_format($itemAmt, 2, '.', '');
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	public function getSecureToken($transactionType, $orderId, $orderTotal, $currencyCode, $firstName, $lastName, $street, $city, $state, $zip, $country, $email, $phone, $notifyUrl, $returnUrl, $cancelUrl,
		$headerTextColor = '', $borderColor = '', $fieldLabelColor = '', $buttonTextColor = '', $buttonColor = '', $lines = false, $taxAmt = false, $shippingAmt = false,
		$handlingAmt = false, $discountAmt = false, $shipToFirstName = null, $shipToLastName = null, $shipToAddress = null, $shipToCity = null, $shipToState = null, $shipToZip = null, $shipToCountry = null)
	{
		$params = array(
			'TRXTYPE' => $transactionType,
			'INVNUM' => 'INV'.$orderId,
			'AMT' => number_format($orderTotal, 2, '.', ''),
			'CURRENCY' => $currencyCode,
			'PONUM' => $orderId,
			'BILLTOFIRSTNAME' => $firstName,
			'BILLTOLASTNAME' => $lastName,
			'BILLTOSTREET' => $street,
			'BILLTOCITY' => $city,
			'BILLTOSTATE' => $state,
			'BILLTOCOUNTRY' => $country,
			'BILLTOZIP' => $zip,
			'BILLTOEMAIL' => $email,
			'BILLTOPHONENUM' => $phone,
			'CREATESECURETOKEN' => 'Y',
			'RETURNURL' => $returnUrl,
			'URLMETHOD' => 'POST',
			'CANCELURL' => $cancelUrl,
			'TEMPLATE' => 'MINLAYOUT',
			'VERBOSITY' => 'HIGH',
			'SECURETOKENID' => $this->getGuid(),
			'SHIPFROMZIP' => $this->settings['ShippingOriginZip'],
			'SHIPTOFIRSTNAME' => $shipToFirstName,
			'SHIPTOLASTNAME' => $shipToLastName,
			'SHIPTOSTREET' => $shipToAddress,
			'SHIPTOCITY' => $shipToCity,
			'SHIPTOSTATE' => $shipToState,
			'SHIPTOCOUNTRY' => $shipToCountry,
			'SHIPTOZIP' => $shipToZip,
			'SHIPTOEMAIL' => $email,
			'SHIPTOPHONENUM' => $phone,
			'ADDROVERRIDE' => '1',
		);

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		if (!is_null($headerTextColor) && !empty($headerTextColor))
		{
			$params['PAGECOLLAPSETEXTCOLOR'] = $headerTextColor;
		}
		if (!is_null($borderColor) && !empty($borderColor))
		{
			$params['PAGECOLLAPSEBGCOLOR'] = $borderColor;
		}
		if (!is_null($fieldLabelColor) && !empty($fieldLabelColor))
		{
			$params['LABELTEXTCOLOR'] = $fieldLabelColor;
		}
		if (!is_null($buttonTextColor) && !empty($buttonTextColor))
		{
			$params['PAGEBUTTONTEXTCOLOR'] = $buttonTextColor;
		}
		if (!is_null($buttonColor) && !empty($buttonColor))
		{
			$params['PAGEBUTTONBGCOLOR'] = $buttonColor;
		}

		if (is_array($lines) && $this->settings['paypalec_Send_Line_Item_Details'] == 'Yes')
		{
			$index = 0;
			$itemAmt = 0;
			foreach ($lines as $line)
			{
				$params['L_NAME'.$index] = substr($line['name'], 0, 127);
				$params['L_DESC'.$index] = substr($line['desc'], 0, 127);

				$params['L_COST'.$index] = number_format($line['amt'], 2, '.', '');
				$params['L_QTY'.$index] = $line['qty'];

				$itemAmt += $line['qty'] * $line['amt'];

				$index++;
			}

			$params['TAXAMT'] = $taxAmt ? number_format($taxAmt, 2, '.', '') : '0.00';

			$shipping = 0;
			$discount = 0;
			if ($shippingAmt !== false)
				$shipping += $shippingAmt;
			if ($handlingAmt !== false)
				$shipping += $handlingAmt;
			if ($discountAmt !== false)
				$discount += $discountAmt;

			$params['FREIGHTAMT'] = $shipping ? number_format($shipping, 2, '.', '') : '0.00';
			$params['DISCOUNT'] = $discount ? number_format($discount, 2, '.', '') : '0.00';

			$params['ITEMAMT'] = number_format($itemAmt, 2, '.', '');
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->doCall($nvpStr);
	}

	/**
	 * @param $transactionType
	 * @param $orderId
	 * @param $orderTotal
	 * @param $ccNumber
	 * @param $expirationDate
	 * @param $cvv2
	 * @param $firstName
	 * @param $lastName
	 * @param $street
	 * @param $street2
	 * @param $city
	 * @param $state
	 * @param $zip
	 * @param $countryIsoA3
	 * @param bool $cardinalData
	 * @param array $shipTo
	 * @param bool $subtotalAmount
	 * @param bool $taxAmt
	 * @param bool $shippingAmount
	 * @param bool $handlingAmount
	 * @param bool $discountAmount
	 * @param array $items
	 * @return array
	 */
	public function doTransaction($transactionType, $orderId, $orderTotal, $ccNumber, $expirationDate, $cvv2,
		$firstName, $lastName, $street, $street2, $city, $state, $zip, $countryIsoA3, $cardinalData = false,
		$shipTo = array(), $subtotalAmount = false, $taxAmt = false, $shippingAmount = false, $handlingAmount = false,
		$discountAmount = false, $items = array())
	{
		$params = array(
			'TRXTYPE' => $transactionType,
			'TENDER' => 'C',
			'AMT' => number_format($orderTotal, 2, '.', ''),
			'ACCT' => $ccNumber,
			'EXPDATE' => $expirationDate,
			'CVV2' => $cvv2,
			'BILLTOFIRSTNAME' => $firstName,
			'BILLTOLASTNAME' => $lastName,
			'BILLTOSTREET' => $street,
			'BILLTOSTREET2' => $street2,
			'BILLTOSTATE' => $state,
			'BILLTOZIP' => $zip,
			'BILLTOCITY' => $city,
			'BILLTOCOUNTRY' => $countryIsoA3,
			'VERBOSITY' => 'HIGH',
			'INVNUM' => $orderId,
			'PONUM' => $orderId,
			'ITEMAMT' => number_format($orderTotal - $taxAmt, 2, '.', ''), // Must subtract the $taxAmnt since we are already passing a TAXAMT in $params to PayPal else PayPal will add it up again and we will receive a "Error code: 7 Field format error: 10413-The totals of the cart item amounts do not match order amounts."
			'FREIGHTAMT' => number_format($shippingAmount, 2, '.', ''),
			'HANDLINGAMT' => number_format($handlingAmount, 2, '.', ''),
			'DISCOUNT' =>  number_format($discountAmount, 2, '.', ''),
			'TAXAMT' => number_format($taxAmt, 2, '.', ''),
		);

		if (is_array($shipTo) and count($shipTo) > 0)
		{
			if (isset($shipTo['firstName'])) $params['SHIPTOFIRSTNAME'] = substr(trim($shipTo['firstName']), 0, 30);
			if (isset($shipTo['lastName'])) $params['SHIPTOLASTNAME'] = substr(trim($shipTo['lastName']), 0, 30);
			if (isset($shipTo['street'])) $params['SHIPTOSTREET'] = substr(trim($shipTo['street']), 0, 30);
			if (isset($shipTo['city'])) $params['SHIPTOCITY'] = substr(trim($shipTo['city']), 0, 20);
			if (isset($shipTo['state'])) $params['SHIPTOSTATE'] = substr(trim($shipTo['state']), 0, 2);
			if (isset($shipTo['zip']))
			{
				$params['SHIPTOZIP'] = substr(trim($shipTo['zip']), 0, 9);
				$params['SHIPFROMZIP'] = $this->settings['ShippingOriginZip'];
			}
			if (isset($shipTo['countryIsoA3'])) $params['SHIPTOCOUNTRY'] = substr(trim($shipTo['countryIsoA3']), 0, 3);
		}

		if (is_array($items) && count($items) > 0)
		{
			foreach ($items as $key => $value)
			{
				$params[$key] = $value;
			}
		}

		if (is_array($cardinalData))
		{
			foreach ($cardinalData as $key => $value)
			{
				$params[$key] = $value;
			}
		}

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	public function doCapture($transaction_id)
	{
		$params = array(
			'TRXTYPE' => 'D',
			'ORIGID' => $transaction_id,
			'VERBOSITY' => 'HIGH',
		);

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	public function doCredit($transaction_id, $amt)
	{
		$params = array(
			'TRXTYPE' => 'C',
			'ORIGID' => $transaction_id,
			'AMT' => number_format($amt, 2, '.', ''),
			'VERBOSITY' => 'HIGH',
		);

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	public function doVoid($transaction_id)
	{
		$params = array(
			'TRXTYPE' => 'V',
			'ORIGID' => $transaction_id,
			'VERBOSITY' => 'HIGH',
		);

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	public function doInquiry($transaction_id)
	{
		$params = array(
			'TRXTYPE' => 'I',
			'ORIGID' => $transaction_id,
			'VERBOSITY' => 'HIGH',
		);

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		$nvpStr = $this->getNvpStr($params);

		return $this->parseResponse($this->doCall($nvpStr));
	}

	protected function getNvpStr($params)
	{
		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				if (strpos($value, '&') >= 0)
				{
					$nvpStr .= '&'.$key.'['.strlen($value).']='.$value;
				}
				else
				{
					$nvpStr .= '&'.$key.'='.$value;
				}
			}
		}

		return $nvpStr;
	}

	public function doCall($nvpStr)
	{
		$this->nvpReqArray = null;
		$this->lastRequestWasError = false;
		$this->errorMessages = array();

		$nvpHeader = $this->getHeader();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->getApiEndpoint());
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		$this->log("API Endpoint:\n".$this->getApiEndpoint());

		//turning off the server and peer verification(TrustManager Concept).
		//New version has these as TRUE
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		$request_id = base64_encode(md5($nvpStr . date('YmdGis') . "1"));

		$headers_array = array();
		$headers_array[] = "Content-Type: text/namevalue";
		$headers_array[] = 'Host: '.$this->getApiEndpoint();
		$headers_array[] = "X-VPS-Client-Timeout: 45"; //Recommended value is 45
		$headers_array[] = 'X-VPS-REQUEST-ID: '.$request_id;

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);
		curl_setopt($ch, CURLOPT_HEADER, false);

		$nvpStr = $nvpHeader . $nvpStr;

		// check for proxy
		if ($this->settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($ch, CURLOPT_PROXYTYPE, $this->settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($ch, CURLOPT_PROXY, $this->settings["ProxyAddress"].":".$this->settings["ProxyPort"]);
			if ($this->settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->settings["ProxyUsername"].":".$this->settings["ProxyPassword"]);
			}

			curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		}

		$nvpreq = $nvpStr;

		$requestArray = array();
		parse_str($nvpreq, $requestArray);

		$this->log("Request:", $requestArray);

		//setting the nvpreq as POST FIELD to curl
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		//getting response from server
		$response = curl_exec($ch);

		//$this->log("Raw Response:\n".$response);

		//convrting NVPResponse to an Associative Array
		$nvpResArray = explode("&", $response);
		$this->nvpReqArray = explode("&", $nvpreq);

		$this->log("Response:", $nvpResArray);

		if (curl_errno($ch))
		{
			$this->lastRequestWasError = true;
			$this->errorMessages[] = curl_error($ch);

			$this->log("Curl Error: \n".$this->errorMessages[0]);

			return array();
		}
		else
		{
			//closing the curl
			curl_close($ch);
		}

		return $nvpResArray;
	}

	protected function getHeader()
	{
		$nvpHeaderStr = 'USER='.urlencode($this->getApiUsername()).'&VENDOR='.urlencode($this->getApiVendor()).'&PARTNER='.$this->getApiPartner().'&PWD='.$this->getApiPassword();

		return $nvpHeaderStr;
	}

	protected function getGuid()
	{
		//hash out a timestamp and some chars to get a 25 char token to pass in
		$str = date('l jS \of F Y h:i:s A');
		$str = trim($str);
		$str = md5($str);

		return $str;
	}

	public function parseSecureToken($response)
	{
		$secure_token = $response[1];
		$secure_token = substr($secure_token, -25);

		return $secure_token;
	}

	public function parseResponse($response)
	{
		$parsed_response = array();
		foreach ($response as $i => $value) 
		{
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1)
			{
				$parsed_response[$tmpAr[0]] = trim($tmpAr[1]);
			}
		}
		return $parsed_response;
	}

	public function handleCapture($db, $order, $authorization_id, $paypal_settlement_note, $total_amount, $gateway_details)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$result = $this->doCapture($authorization_id);

		if (isset($result['RESULT']) && $result['RESULT'] == 0)
		{
			$new_transaction_id = $result['PNREF'];

			$payment_status = 'Received';
			$db->reset();
			$db->assign("custom3", $total_amount);
			$db->assignStr("payment_status", $payment_status);
			$db->assignStr('security_id', $new_transaction_id);
			
			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
			
			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}
			
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $total_amount, $gateway_details);

			$this->fireOrderEvent($order, $order->getStatus(), $payment_status);

			return false;
		}
		else if (isset($result['RESPMSG']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $total_amount, $gateway_details, false);
			return $result['RESPMSG'];
		}
	}

	public function handleRefund($db, $order, $authorization_id, $paypal_partial_capture, $paypal_charged_amount, $total_amount, $paypal_settlement_note, $gateway_details)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$result = $this->doCredit($authorization_id, $paypal_partial_capture);

		$refund_type = 'Full';

		if (isset($result['RESULT']) && $result['RESULT'] == 0)
		{
			$new_transaction_id = $result['PNREF'];

			$total_charged_amount = ($paypal_charged_amount > 0 ? $paypal_charged_amount : $total_amount) - $paypal_partial_capture;
			
			$db->reset();						
			if ($refund_type == "Full" || $total_charged_amount <= 0)
			{
				$db->assignStr("payment_status", "Refunded");
				$db->assignStr("custom1", "refund");
			}
							
			$db->assign("custom3", $total_charged_amount);
			$db->assignStr('security_id', $new_transaction_id);

			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
			
			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}

			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $paypal_partial_capture, $gateway_details);

			$this->fireOrderEvent($order, $order->getStatus(), 'Refunded');

			return false;
		}
		else if (isset($result['RESPMSG']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $paypal_partial_capture, $gateway_details, false);
			return $result['RESPMSG'];
		}
	}

	public function handleVoid($db, $order, $authorization_id, $paypal_settlement_note, $total_amount, $gateway_details, $order_status = null)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$payment_status = $order->getPaymentStatus();

		$result = $this->doVoid($authorization_id);

		if (isset($result['RESULT']) && $result['RESULT'] == 0)
		{
			$new_transaction_id = $result['PNREF'];

			$payment_status = 'Canceled';

			$db->reset();
			$db->assignStr("custom1", "canceled");
			$db->assignStr("payment_status", $payment_status);
			$db->assignStr('security_id', $new_transaction_id);

			if (!is_null($order_status))
			{
				$db->assignStr("status", $order_status);
			}

			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");

			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}

			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $total_amount, $gateway_details);

			$this->fireOrderEvent($order, !is_null($order_status) ? $order_status : $order->getStatus(), $payment_status);

			return false;
		}
		else if (isset($result['RESPMSG']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $total_amount, $gateway_details, false);
			return $result['RESPMSG'];
		}
	}

	protected function fireOrderEvent($order, $status, $payment_status)
	{
		Events_OrderEvent::dispatchStatusChangeEvent($order, $status, $payment_status);
	}

	protected static function replaceDataKeys()
	{
		return array('ACCT', 'CVV2', 'PWD', 'USER', 'SIGNATURE');
	}

	protected static function cleanData(array &$values)
	{
		foreach (self::replaceDataKeys() as $key)
		{
			$parts = explode('.', $key);

			$s_path = &$values;
			for ($i = 0; $i < count($parts); $i++)
			{
				$part = $parts[$i];
				if (isset($s_path[$part]))
				{
					if (is_array($s_path[$part]))
					{
						$s_path = &$s_path[$part];
					}
					else if ($i + 1 >= count($parts))
					{
						$s_path[$part] = str_repeat('X', strlen($s_path[$part]));
					}
				}
			}
		}
	}
}

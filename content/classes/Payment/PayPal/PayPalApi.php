<?php

class Payment_PayPal_PayPalApi
{
	protected $liveMode = true;
	protected $version = '76';
	protected $apiUsername;
	protected $apiPassword;
	protected $apiSignature;
	protected $useCertificate = false;
	protected $subject;
	protected $authToken;
	protected $authSignature;
	protected $authTimestamp;
	protected $buttonSource;

	protected $nvpReqArray;
	protected $lastRequestWasError = false;
	protected $errorMessages = array();

	protected $settings;

	public function __construct($settings = null, $liveMode = true)
	{
		$this->setLiveMode($liveMode);

		if (!is_null($settings))
		{
			$this->settings = $settings;
		}
		else
		{
			global $settings;
			$this->settings = $settings;
		}
	}

	/**
	 * Gets buttonSource
	 *
	 * @return string
	 */
	public function getButtonSource()
	{
		return $this->buttonSource;
	}
	/**
	 * Sets buttonSource
	 *
	 * @param string $buttonSource
	 */
	public function setButtonSource($buttonSource)
	{
		$this->buttonSource = $buttonSource;
	}

	/**
	* Gets liveMode
	*
	* @return boolean
	*/
	public function getLiveMode()
	{
		return $this->liveMode;
	}
	/**
	* Sets liveMode
	*
	* @param boolean $liveMode
	*/
	public function setLiveMode($liveMode)
	{
		$this->liveMode = $liveMode;
	}

	public function getApiEndpoint()
	{
		if ($this->useCertificate)
		{
			if ($this->liveMode)
			{
				return 'https://api.paypal.com/nvp';
			}
			else
			{
				return 'https://api.sandbox.paypal.com/nvp';
			}
		}
		else
		{
			if ($this->liveMode)
			{
				return 'https://api-3t.paypal.com/nvp';
			}
			else
			{
				return 'https://api-3t.sandbox.paypal.com/nvp';
			}
		}
	}

	/**
	* Gets apiUsername
	*
	* @return string
	*/
	public function getApiUsername()
	{
		return $this->apiUsername;
	}
	/**
	* Sets apiUsername
	*
	* @param string $apiUsername
	*/
	public function setApiUsername($apiUsername)
	{
		$this->apiUsername = $apiUsername;
	}

	/**
	* Gets apiPassword
	*
	* @return string
	*/
	public function getApiPassword()
	{
		return $this->apiPassword;
	}
	/**
	* Sets apiPassword
	*
	* @param string $apiPassword
	*/
	public function setApiPassword($apiPassword)
	{
		$this->apiPassword = $apiPassword;
	}

	/**
	 * Gets useCertificate
	 *
	 * @return boolean
	 */
	public function getUseCertificate()
	{
		return $this->useCertificate;
	}
	/**
	 * Sets useCertificate
	 *
	 * @param boolean $useCertificate
	 */
	public function setUseCertificate($useCertificate)
	{
		$this->useCertificate = $useCertificate;
	}

	/**
	* Gets apiSignature
	*
	* @return string
	*/
	public function getApiSignature()
	{
		return $this->apiSignature;
	}
	/**
	* Sets apiSignature
	*
	* @param string $apiSignature
	*/
	public function setApiSignature($apiSignature)
	{
		$this->apiSignature = $apiSignature;
	}

	/**
	* Gets subject
	*
	* @return string
	*/
	public function getSubject()
	{
		return $this->subject;
	}
	/**
	* Sets subject
	*
	* @param string $subject
	*/
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}

	/**
	* Gets authToken
	*
	* @return string
	*/
	public function getAuthToken()
	{
		return $this->authToken;
	}
	/**
	* Sets authToken
	*
	* @param string $authToken
	*/
	public function setAuthToken($authToken)
	{
		$this->authToken = $authToken;
	}

	/**
	* Gets authSignature
	*
	* @return string
	*/
	public function getAuthSignature()
	{
		return $this->authSignature;
	}
	/**
	* Sets authSignature
	*
	* @param string $authSignature
	*/
	public function setAuthSignature($authSignature)
	{
		$this->authSignature = $authSignature;
	}

	/**
	* Gets authTimestamp
	*
	* @return long
	*/
	public function getAuthTimestamp()
	{
		return $this->authTimestamp;
	}
	/**
	* Sets authTimestamp
	*
	* @param long $authTimestamp
	*/
	public function setAuthTimestamp($authTimestamp)
	{
		$this->authTimestamp = $authTimestamp;
	}

	/**
	 * Gets last request name-value pair array
	 * @return array request array
	 */
	public function getLastRequestArray()
	{
		return $this->nvpReqArray;
	}

	/**
	 * Returns true when the last request resulted in an error
	 * @return boolean true when the last reqeust was an error
	 */
	public function wasLastRequestError()
	{
		return $this->lastRequestWasError;
	}

	/**
	 * Gets the error messages
	 * @return array error messages
	 */
	public function getErrorMessages()
	{
		return $this->errorMessages;
	}

	public function getExpressCheckoutUrl($token, $fromCart = false)
	{
		$baseUrl = $this->getLiveMode() ? 'www.paypal.com' : 'www.sandbox.paypal.com';

		return 'https://'.$baseUrl.'/cgi-bin/webscr?cmd=_express-checkout&token='.$token.($fromCart ? '' : '&useraction=commit');
	}

	public function setExpressCheckout(
		$transactionType, $orderTotal, $currencyCode, $returnUrl, $cancelUrl, 
		$noShipping = false, $lines = false, $uxParams = false, $orderAmounts = false, 
		$noShippingAddress = false, $order, $billMeLater = false
	)
	{
		$params = array(
			'PAYMENTREQUEST_0_PAYMENTACTION' => $transactionType,
			'PAYMENTREQUEST_0_AMT' => $_orderTotal = number_format($orderTotal, 2, '.', ''),
			'PAYMENTREQUEST_0_CURRENCYCODE' => $currencyCode,
			//'NOTETOBUYER' => '',
			'CANCELURL' => $cancelUrl,
			'RETURNURL' => $returnUrl,
		);

		if ($noShipping)
		{
			$params['NOSHIPPING'] = '1';
			if ($transactionType == 'Sale') 
				$params['PAYMENTREQUEST_0_ALLOWEDPAYMENTMETHOD'] = 'InstantPaymentOnly';
			$params['REQCONFIRMSHIPPING'] = '0';
		}
		else if ($noShippingAddress && $order !== null && $order->shippingAddress !== null && isset($order->shippingAddress['address1']) && trim($order->shippingAddress['address1']) != '')
		{
			unset($params['NOSHIPPING']);
			$params['ADDROVERRIDE'] = 1;
			$params['PAYMENTREQUEST_0_SHIPTONAME'] = $order->shippingAddress['name'];
			$params['PAYMENTREQUEST_0_SHIPTOSTREET'] = $order->shippingAddress['address1'];
			$params['PAYMENTREQUEST_0_SHIPTOSTREET2'] = $order->shippingAddress['address2'];
			$params['PAYMENTREQUEST_0_SHIPTOCITY'] = $order->shippingAddress['city'];
			$params['PAYMENTREQUEST_0_SHIPTOSTATE'] = trim($order->shippingAddress['state_abbr']) != '' ? $order->shippingAddress['state_abbr'] : $order->shippingAddress['province'];
			$params['PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE'] = $order->shippingAddress['country_iso_a2'];
			$params['PAYMENTREQUEST_0_SHIPTOZIP'] = $order->shippingAddress['zip'];
		}
		else if ($noShippingAddress)
		{
			$params['NOSHIPPING'] = '1';
		}

		self::log('NoShippingAddress: '.($noShippingAddress?'true':'false'));

		if (!$noShippingAddress)
		{
			$params['TOTALTYPE'] = 'EstimatedTotal';
		}

		if (is_array($lines) && $this->settings['paypalec_Send_Line_Item_Details'] == 'Yes')
		{
			$index = 0;
			$itemAmt = 0;

			$items = array();

			foreach ($lines as $line)
			{
				$items['L_PAYMENTREQUEST_0_NAME'.$index] = substr($line['name'], 0, 127);
				$items['L_PAYMENTREQUEST_0_DESC'.$index] = substr($line['desc'], 0, 127);
				$items['L_PAYMENTREQUEST_0_AMT'.$index] = number_format($line['amt'], 2, '.', '');
				$items['L_PAYMENTREQUEST_0_QTY'.$index] = $line['qty'];
				$items['L_PAYMENTREQUEST_0_TAX'.$index] = number_format($line['tax'], 2, '.', '');

				// No line tax amount, because with discounts, multiple quantity, caused errors with PayPal (off by even 1 cent caused errors)
				// Now we just send total tax amount for the order

				$itemAmt += $line['qty'] * $line['amt'];
				$index++;
			}

			if ($itemAmt > 0) $params = array_merge($params, $items);

			$params['PAYMENTREQUEST_0_TAXAMT'] = '0.00';
			$params['PAYMENTREQUEST_0_HANDLINGAMT'] = '0.00';
			$params['PAYMENTREQUEST_0_SHIPPINGAMT'] = '0.00';

			if ($orderAmounts && $itemAmt > 0)
			{
				$params['PAYMENTREQUEST_0_TAXAMT'] = $_taxAmount = number_format(isset($orderAmounts["tax_amount"]) ? $orderAmounts["tax_amount"] : '0.00', 2, '.', '');
				$params['PAYMENTREQUEST_0_HANDLINGAMT'] = $_handlingAmount = number_format(isset($orderAmounts["handling_amount"]) ? $orderAmounts["handling_amount"] : '0.00', 2, '.', '');
				$params['PAYMENTREQUEST_0_SHIPPINGAMT'] = $_shippingAmount = number_format(isset($orderAmounts["shipping_amount"]) ? $orderAmounts["shipping_amount"] : '0.00', 2, '.', '');
			}
			else if ($itemAmt <= 0)
			{
				$itemAmt = $_orderTotal;
			}

			$params['PAYMENTREQUEST_0_ITEMAMT'] = number_format($itemAmt, 2, '.', '');
		}

		if (is_array($uxParams))
		{
			foreach ($uxParams as $key => $value)
			{
				$params[$key] = ltrim($value,'#');
			}
		}

		// Bill Me Later
		if ($billMeLater)
		{
			$params['USERSELECTEDFUNDINGSOURCE'] = 'BML';
			$params['SOLUTIONTYPE'] = 'SOLE';
		}

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}

		return $this->doCall('SetExpressCheckout', $nvpStr);
	}

	public function getExpressCheckoutDetails($transactionType, $token)
	{
		$params = array(
			'TOKEN' => $token,
		);

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}

		return $this->doCall('GetExpressCheckoutDetails', $nvpStr);
	}

	/**
	 * @param $transactionType
	 * @param $token
	 * @param $payerId
	 * @param $orderTotal
	 * @param $currencyCode
	 * @param null $notifyUrl
	 * @param array|null $lines
	 * @param array|null $orderAmounts
	 * @param null $orderNumber
	 * @return array
	 */
	public function doExpressCheckoutPayment($transactionType, $token, $payerId, $orderTotal, $currencyCode, $notifyUrl = null, array $lines = null, array $orderAmounts = null, $orderNumber = null)
	{
		$params = array(
			'PAYMENTREQUEST_0_PAYMENTACTION' => $transactionType,
			'TOKEN' => $token,
			'PAYERID' => $payerId,
			'PAYMENTREQUEST_0_AMT' => $_orderTotal = number_format($orderTotal, 2, '.', ''),
			'PAYMENTREQUEST_0_CURRENCYCODE' => $currencyCode,
			'NOTIFYURL' => $notifyUrl,
		);


		if (is_array($lines) && $this->settings['paypalec_Send_Line_Item_Details'] == 'Yes')
		{
			$index = 0;
			$itemAmt = 0;

			$items = array();

			foreach ($lines as $line)
			{
				$items['L_PAYMENTREQUEST_0_NAME'.$index] = substr($line['name'], 0, 127);
				$items['L_PAYMENTREQUEST_0_DESC'.$index] = substr($line['desc'], 0, 127);
				$items['L_PAYMENTREQUEST_0_AMT'.$index] = number_format($line['amt'], 2, '.', '');
				$items['L_PAYMENTREQUEST_0_QTY'.$index] = $line['qty'];
				$items['L_PAYMENTREQUEST_0_TAX'.$index] = number_format($line['tax'], 2, '.', '');

				// No line tax amount, because with discounts, multiple quantity, caused errors with PayPal (off by even 1 cent caused errors)
				// Now we just send total tax amount for the order

				$itemAmt += $line['qty'] * $line['amt'];
				$index++;
			}

			if ($itemAmt > 0) $params = array_merge($params, $items);

			$params['PAYMENTREQUEST_0_TAXAMT'] = '0.00';
			$params['PAYMENTREQUEST_0_HANDLINGAMT'] = '0.00';
			$params['PAYMENTREQUEST_0_SHIPPINGAMT'] = '0.00';
			$params['PAYMENTREQUEST_0_INVNUM'] = $orderNumber;

			if ($orderAmounts && $itemAmt > 0)
			{
				$params['PAYMENTREQUEST_0_TAXAMT'] = $_taxAmount = number_format(isset($orderAmounts["tax_amount"]) ? $orderAmounts["tax_amount"] : '0.00', 2, '.', '');
				$params['PAYMENTREQUEST_0_HANDLINGAMT'] = $_handlingAmount = number_format(isset($orderAmounts["handling_amount"]) ? $orderAmounts["handling_amount"] : '0.00', 2, '.', '');
				$params['PAYMENTREQUEST_0_SHIPPINGAMT'] = $_shippingAmount = number_format(isset($orderAmounts["shipping_amount"]) ? $orderAmounts["shipping_amount"] : '0.00', 2, '.', '');
			}
			else if ($itemAmt <= 0)
			{
				$itemAmt = $orderTotal;
			}

			$params['PAYMENTREQUEST_0_ITEMAMT'] = number_format($itemAmt, 2, '.', '');
		}

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}

		return $this->doCall('DoExpressCheckoutPayment', $nvpStr);
	}

	public function doAuthorization($authorizationId, $amt, $currencyCode)
	{
		$params = array(
			'TRANSACTIONID' => $authorizationId,
			'AMT' => number_format($amt, 2, '.', ''),
			'CURRENCYCODE' => $currencyCode,
		);

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}
		return $this->doCall('DoAuthorization', $nvpStr);
	}

	/**
	 * Performs a Capture transaction request
	 * @param  string $authorizationId 
	 * @param  float  $amt             
	 * @param  string $currencyCode    
	 * @param  string $completeType    
	 * @return array response parameters
	 */
	public function doCapture($authorizationId, $amt, $currencyCode, $paypal_settlement_note, $completeType = 'NotComplete')
	{
		$params = array(
			'AUTHORIZATIONID' => $authorizationId,
			'AMT' => number_format($amt, 2, '.', ''),
			'CURRENCYCODE' => $currencyCode,
			'COMPLETETYPE' => $completeType,
			'NOTE' => $paypal_settlement_note,
		);

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}
		return $this->doCall('DoCapture', $nvpStr);
	}

	/**
	 * Performs a Void transaction request
	 * @param  string $authorizationId
	 * @return array response parameters
	 */
	public function doVoid($authorizationId, $note)
	{
		$params = array(
			'AUTHORIZATIONID' => $authorizationId,
			'NOTE' => $note,
		);

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}
		return $this->doCall('DoVoid', $nvpStr);
	}

	/**
	 * Performs a Refund transaction
	 * @param  string $transactionId
	 * @param  string $refundType
	 * @param  float $amt
	 * @param  string $currencyCode
	 * @return array response parameters
	 */
	public function doRefund($transactionId, $refundType = 'Partial', $amt, $currencyCode, $note)
	{
		$params = array(
			'TRANSACTIONID' => $transactionId,
			'REFUNDTYPE' => $refundType,
			'NOTE' => $note,
		);

		if ($refundType == 'Partial')
		{
			$params['AMT'] = number_format($amt, 2, '.', '');
			$params['CURRENCYCODE'] = $currencyCode;
		}

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}
		return $this->doCall('RefundTransaction', $nvpStr);
	}

	/**
	 * Performs a Reauthorization transaction
	 * @param  string $authorizationId
	 * @param  float  $amt
	 * @param  string $currencyCode
	 * @return array response parameters
	 */
	public function doReauthorization($authorizationId, $amt, $currencyCode)
	{
		$params = array(
			'AUTHORIZATIONID' => $authorizationId,
			'AMT' => number_format($amt, 2, '.', ''),
			'CURRENCYCODE' => $currencyCode,
		);

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}
		return $this->doCall('DoReauthorization', $nvpStr);
	}

	/**
	 * Performs a DirectPayment transaction request
	 * @param  string        $paymentAction
	 * @param  string        $id
	 * @param  array         $post_form
	 * @param  array         $settings_vars
	 * @param  array         $common_vars
	 * @param  array|boolean $cardinalData
	 * @return array         response parameters
	 */
	public function doDirectPayment($paymentAction, $id, $post_form, $settings_vars, $common_vars, $cardinalData = false, $notifyUrl = null)
	{
		$paypal_currency = $settings_vars[$id."_Currency_Code"]["value"];

		$order_total = $common_vars["order_total_amount"]["value"];
		$shipping_total = $common_vars["order_shipping_amount"]["value"];
		$handling_total = $common_vars["order_handling_separated"]=="yes" ? $common_vars["order_handling_amount"]["value"] : 0.00;
		$tax_total = $common_vars["order_tax_amount"]["value"];
		$item_total = $order_total - $shipping_total - $tax_total - $handling_total;

		if ($item_total <= 0)
		{
			$item_total = $order_total;
			$shipping_total = 0;
			$tax_total = 0;
			$handling_total = 0;
		}

		if ($common_vars["shipping_country_iso_a2"]["value"]=="US" || $common_vars["shipping_country_iso_a2"]["value"]=="CA")
		{
			$shipping_state = "shipping_state_abbr";
		}
		else
		{
			$shipping_state = "shipping_state";
		}

		if ($common_vars["billing_country_iso_a2"]["value"]=="US" || $common_vars["billing_country_iso_a2"]["value"]=="CA")
		{
			$b_state = "billing_state_abbr";
		}
		else
		{
			$b_state = "billing_state";
		}
		$ipaddress = $_SERVER['REMOTE_ADDR'];

		$params = array(
			'PAYMENTACTION' => $paymentAction,
			'IPADDRESS' => $ipaddress,
			'AMT' => number_format($order_total, 2, '.', ''),
			'CREDITCARDTYPE' => isset($post_form["cc_type"]) ? $post_form["cc_type"] : '',
			'ACCT' => $post_form["cc_number"],
			'EXPDATE' => $post_form["cc_expiration_month"].$post_form["cc_expiration_year"],
			'CVV2' => $post_form["cc_cvv2"],
			'EMAIL' => $common_vars["billing_email"]["value"],
			'FIRSTNAME' => $post_form["cc_first_name"],
			'LASTNAME' => $post_form["cc_last_name"],
			'STREET' => $common_vars["billing_address1"]["value"],
			'STREET2' => $common_vars["billing_address2"]["value"],
			'CITY' => $common_vars["billing_city"]["value"],
			'STATE' => $common_vars[$b_state]["value"],
			'ZIP' => $common_vars["billing_zip"]["value"],
			'COUNTRYCODE' => $common_vars["billing_country_iso_a2"]["value"],
			'CURRENCYCODE' => $paypal_currency,
			'ITEMAMT' => number_format($item_total, 2, '.', ''),
			'SHIPPINGAMT' => number_format($shipping_total, 2, '.', ''),
			'HANDLINGAMT' => number_format($handling_total, 2, '.', ''),
			'TAXAMT' => number_format($tax_total, 2, '.', ''),
			'DESC' => 'Order # '.$common_vars["order_id"]["value"],
			'CUSTOM' => null,
			'INVNUM' => $common_vars["order_id"]["value"],
			'NOTIFYURL' => $notifyUrl,
			'SHIPTONAME' => null,
			'SHIPTOSTREET' => null,
			'SHIPTOSTREET2' => null,
			'SHIPTOCITY' => null,
			'SHIPTOSTATE' => null,
			'SHIPTOZIP' => null,
			'SHIPTOCOUNTRY' => null,
			'SHIPTOPHONENUM' => null,
			'AUTHSTATUS3DS' => null,
			'MPIVENDOR3DS' => null,
			'CAVV' => null,
			'ECI3DS' => null,
			'XID' => null,
		);

		if ($this->getButtonSource() != '')
		{
			$params['BUTTONSOURCE'] = $this->getButtonSource();
		}

		if (strlen(trim($common_vars["shipping_name"]["value"]))>0)
		{
			$params['SHIPTONAME'] = $common_vars["shipping_name"]["value"];
			$params['SHIPTOSTREET'] = $common_vars["shipping_address1"]["value"];
			$params['SHIPTOSTREET2'] = $common_vars["shipping_address2"]["value"];
			$params['SHIPTOCITY'] = $common_vars["shipping_city"]["value"];
			$params['SHIPTOSTATE'] = $common_vars[$shipping_state]["value"];
			$params['SHIPTOZIP'] = $common_vars['shipping_zip']["value"];
			$params['SHIPTOCOUNTRY'] = $common_vars['shipping_country_iso_a2']["value"];
		}

		if ($cardinalData)
		{
			$params['AUTHSTATUS3DS'] = $cardinalData['AuthStatus3ds'];
			$params['MPIVENDOR3DS'] = $cardinalData['MpiVendor3ds'];
			$params['CAVV'] = $cardinalData['Cavv'];
			$params['ECI3DS'] = $cardinalData['Eci3ds'];
			$params['XID'] = $cardinalData['Xid'];
		}

		$nvpStr = '';

		foreach ($params as $key => $value)
		{
			if (!is_null($value))
			{
				$nvpStr .= '&'.$key.'='.urlencode($value);
			}
		}
		return $this->doCall('DoDirectPayment', $nvpStr);
	}

	/**
	 * Perform curl call to api endpoin
	 * @param  string $methodName
	 * @param  array  $nvpStr 
	 * @return array  response parameters
	 */
	public function doCall($methodName, $nvpStr)
	{
		$this->nvpReqArray = null;
		$this->lastRequestWasError = false;
		$this->errorMessages = array();

		$nvpHeader = $this->getHeader();

		self::log("API Endpoint:\n".$this->getApiEndpoint());

		$headerArray = array();
		parse_str($nvpHeader, $headerArray);
		self::log("Request Headers:", $headerArray);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->getApiEndpoint());
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		//turning off the server and peer verification(TrustManager Concept).
		//New version has these as TRUE
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		if ($this->useCertificate)
		{
			$_cert_path = $this->settings["GlobalServerPath"]."/content/engine/payment/paypal/certificate.pem";

			curl_setopt($ch, CURLOPT_SSLCERT, $_cert_path);
			curl_setopt($ch, CURLOPT_CAPATH, $this->settings['GlobalServerPath'].$this->settings['SecuritySslDirectory']);
			curl_setopt($ch, CURLOPT_CAINFO, $this->settings['GlobalServerPath'].$this->settings['SecuritySslDirectory'].'/'.$this->settings['SecuritySslPem']);
		}

		//in case of permission APIs send headers as HTTPheders
		if(!$this->useCertificate && !empty($this->authToken) && !empty($this->authSignature) && !empty($this->authTimestamp))
		{
			$headers_array = array();
			$headers_array[] = "X-PP-AUTHORIZATION: ".$nvpHeader;

			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);
			curl_setopt($ch, CURLOPT_HEADER, false);
		}
		else
		{
			$nvpStr = $nvpHeader . $nvpStr;
		}

		// check for proxy
		if ($this->settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($ch, CURLOPT_PROXYTYPE, $this->settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($ch, CURLOPT_PROXY, $this->settings["ProxyAddress"].":".$this->settings["ProxyPort"]);
			if ($this->settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->settings["ProxyUsername"].":".$this->settings["ProxyPassword"]);
			}

			curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		}

		//check if version is included in $nvpStr else include the version.
		if(strlen(str_replace('VERSION=', '', strtoupper($nvpStr))) == strlen($nvpStr))
		{
			$nvpStr = "&VERSION=" . urlencode($this->version) . $nvpStr;    
		}

		$nvpreq = "METHOD=".urlencode($methodName).$nvpStr;

		$requestArray = array();
		parse_str($nvpreq, $requestArray);

		self::log("Request:", $requestArray);

		//setting the nvpreq as POST FIELD to curl
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		//getting response from server
		$response = curl_exec($ch);

		//convrting NVPResponse to an Associative Array
		$nvpResArray = $this->deformatNVP($response);
		$this->nvpReqArray = $this->deformatNVP($nvpreq);

		self::log("Response:", $nvpResArray);

		if (curl_errno($ch))
		{
			$this->lastRequestWasError = true;
			$this->errorMessages[] = curl_error($ch);

			self::log("Curl Error: \n".$this->errorMessages[0]);

			return array();
		}
		else
		{
			//closing the curl
			curl_close($ch);
		}

		return $nvpResArray;
	}

	/**
	 * Gets the request authorization header
	 * @return string authorization header
	 */
	protected function getHeader()
	{
		$AuthMode = '';
		$nvpHeaderStr = '';

		if (!$this->useCertificate)
		{
			if (!empty($this->apiUsername) && !empty($this->apiPassword) && !empty($this->apiSignature) && !empty($this->subject))
			{
				$AuthMode = 'THIRDPARTY';
			}
			else if (!empty($this->apiUsername) && !empty($this->apiPassword) && !empty($this->apiSignature))
			{
				$AuthMode = '3TOKEN';
			}
			else if (!empty($this->authToken) && !empty($this->authSignature) && !empty($this->authTimestamp))
			{
				$AuthMode = 'PERMISSION';
			}
			else if (!empty($this->subject))
			{
				$AuthMode = 'FIRSTPARTY';
			}

			switch ($AuthMode)
			{
				case '3TOKEN':
					$nvpHeaderStr = '&PWD='.urlencode($this->apiPassword).'&USER='.urlencode($this->apiUsername).'&SIGNATURE='.urlencode($this->apiSignature);
					break;
				case 'FIRSTPARTY':
					$nvpHeaderStr = '&SUBJECT='.urlencode($this->subject);
					break;
				case 'THIRDPARTY':
					$nvpHeaderStr = '&PWD='.urlencode($this->apiPassword).'&USER='.urlencode($this->apiUsername).'&SIGNATURE='.urlencode($this->apiSignature).'&SUBJECT='.urlencode($this->subject);
					break;
				case 'PERMISSION':
					$nvpHeaderStr = $this->formAuthorization();
					break;
			}
		}
		else
		{
			return '&PWD='.urlencode($this->apiPassword).'&USER='.urlencode($this->apiUsername).'&SUBJECT=';
		}

		return $nvpHeaderStr;
	}

	/** This function will take NVPString and convert it to an Associative Array and it will decode the response.
	* It is usefull to search for a particular key and displaying arrays.
	* @nvpstr is NVPString.
	* @nvpArray is Associative Array.
	*/
	protected function deformatNVP($nvpstr)
	{
		$intial=0;
		$nvpArray = array();

		while(strlen($nvpstr))
		{
			//postion of Key
			$keypos= strpos($nvpstr,'=');

			//position of value
			$valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&') : strlen($nvpstr);

			/*getting the Key and Value values and storing in a Associative Array*/
			$keyval = substr($nvpstr, $intial, $keypos);
			$valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);

			//decoding the respose
			$nvpArray[urldecode($keyval)] = urldecode($valval);
			$nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
		}

		return $nvpArray;
	}

	/**
	 * Writer message into log
	 * @param string $s 
	 */
	public static function log($s, array $data = array())
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			self::cleanData($data);

			file_put_contents(
				"content/cache/log/paypal.txt", 
				date("Y/m/d-H:i:s")." - PayPal\n".(is_array($s) ? array2text($s) : $s)."\n".array2text($data)."\n\n\n",
				FILE_APPEND
			);
		}
	}

	public function handleCapture($db, $order, $authorization_id, $paypal_partial_capture, $total_charged_amount, $currencyCode, $paypal_settlement_note, $complete_type, $gateway_details)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$result= $this->DoCapture($authorization_id, $paypal_partial_capture, $currencyCode, $paypal_settlement_note, $complete_type);

		if (isset($result['ACK']) && in_array($result['ACK'], array('Success', 'SuccessWithWarnings', 'SuccessWithWarning')))
		{
			$db->reset();
			$db->assign("custom3", $total_charged_amount);

			$payment_status = 'Partial';
			if ($complete_type == "Complete")
			{
				$payment_status = 'Received';
				$db->assignStr('custom1', 'none');
			}
			$db->assignStr("payment_status", $payment_status);
			
			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");

			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}

			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $paypal_partial_capture, $gateway_details);

			$this->fireOrderEvent($order, $order->getStatus(), $payment_status);

			return false;
		}
		else if (isset($result['L_SHORTMESSAGE0']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $paypal_partial_capture, $gateway_details, false);
			return $result['L_LONGMESSAGE0'];
		}
	}

	public function handleAuthorization($db, $order, $authorization_id, $paypal_partial_capture, $currencyCode, $paypal_settlement_note, $gateway_details)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$result = $this->DoAuthorization($authorization_id, $paypal_partial_capture, $currencyCode);

		if (isset($result['ACK']) && in_array($result['ACK'], array('Success')) && isset($result['TRANSACTIONID']))
		{
			$db->reset();
			$db->assignStr("custom1", "authorization");
			$db->assignStr("security_id", $result['TRANSACTIONID']);
			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");

			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}

			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $paypal_partial_capture, $gateway_details);
			return false;
		}
		else if (isset($result['L_SHORTMESSAGE0']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $paypal_partial_capture, $gateway_details, false);
			return $result['L_LONGMESSAGE0'];
		}
	}

	public function handleReauthorization($db, $order, $authorization_id, $paypal_partial_capture, $currencyCode, $paypal_settlement_note, $gateway_details)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$result = $this->DoReauthorization($authorization_id, $paypal_partial_capture, $currencyCode);

		if (isset($result['ACK']) && in_array($result['ACK'], array('Success')) && isset($result['TRANSACTIONID']))
		{
			$db->reset();
			$db->assignStr("custom1", "authorization");
			$db->assignStr("security_id", $result['TRANSACTIONID']);											
			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");

			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}

			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $paypal_partial_capture, $gateway_details);
			return false;
		}
		else if (isset($result['L_SHORTMESSAGE0']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $paypal_partial_capture, $gateway_details, false);
			return $result['L_LONGMESSAGE0'];
		}
	}

	public function handleRefund($db, $order, $authorization_id, $paypal_partial_capture, $currencyCode, $paypal_settlement_note, $gateway_details, $paypal_charged_amount, $total_amount)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$payment_status = $order->getPaymentStatus();

		if ($paypal_charged_amount == $paypal_partial_capture)
		{
			$refund_type ="Full";
		}
		
		if ($total_amount > $paypal_partial_capture)
		{
			$refund_type ="Partial";
		}

		$result = $this->doRefund($authorization_id, $refund_type, $paypal_partial_capture, $currencyCode, $paypal_settlement_note);

		if (isset($result['ACK']) && in_array($result['ACK'], array('Success')) && isset($result['REFUNDTRANSACTIONID']))
		{
			$total_charged_amount =($paypal_charged_amount>0?$paypal_charged_amount:$total_amount) - $paypal_partial_capture;
			
			$db->reset();
			if ($refund_type == "Full" || $total_charged_amount <= 0)
			{
				$payment_status = 'Refunded';
				$db->assignStr("payment_status", $payment_status);
				$db->assignStr("custom1", "refund");
			}
							
			$db->assign("custom3", $total_charged_amount);
			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");
			
			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}

			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $paypal_partial_capture, $gateway_details);

			$this->fireOrderEvent($order, $order->getStatus(), $payment_status);

			return false;
		}
		else if (isset($result['L_SHORTMESSAGE0']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $paypal_partial_capture, $gateway_details, false);
			return $result['L_LONGMESSAGE0'];
		}
	}

	public function handleVoid($db, $order, $authorization_id, $paypal_settlement_note, $paypal_charged_amount, $paypal_partial_capture, $gateway_details, $order_status = null)
	{
		$oid = $order->getId();
		$uid = $order->getUser()->getId();

		$payment_status = $order->getPaymentStatus();

		$result = $this->doVoid($authorization_id, $paypal_settlement_note);

		if (isset($result['ACK']) && in_array($result['ACK'], array('Success')) && isset($result['AUTHORIZATIONID']))
		{
			$db->reset();
			$db->assignStr("custom1", "canceled");
			if ($paypal_charged_amount > 0)
			{
				$payment_status = 'Received';
				$db->assignStr("payment_status", $payment_status);
			}
			elseif ($paypal_charged_amount == 0)
			{
				$payment_status = 'Canceled';
				$db->assignStr("payment_status", $payment_status);
				if (!is_null($order_status))
				{
					$db->assignStr("status", $order_status);
				}
			}
			$db->update(DB_PREFIX."orders", "WHERE oid='".$oid."'");

			$response = '';
			foreach ($result as $key=>$value)
			{
				$response.=$key." = ".$value[0]."\n";
			}

			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $response, $paypal_partial_capture, $gateway_details);

			$this->fireOrderEvent($order, !is_null($order_status) ? $order_status : $order->getStatus(), $payment_status);

			return false;
		}
		else if (isset($result['L_SHORTMESSAGE0']))
		{
			Payment_PayPal_PayPalApi::createPayPalTransaction($db, $oid, $uid, $paypal_settlement_note, $result, $paypal_partial_capture, $gateway_details, false);
			return $result['L_LONGMESSAGE0'];
		}
	}

	protected function fireOrderEvent($order, $status, $payment_status)
	{
		Events_OrderEvent::dispatchStatusChangeEvent($order, $status, $payment_status);
	}

	/**
	 * Creates paypal transaction record in DB
	 * @param $db
	 * @param $oid
	 * @param $uid
	 * @param $note
	 * @param $response
	 * @param $amount
	 * @param $gateway_details
	 */
	public static function createPayPalTransaction($db, $oid, $uid, $note, $response, $amount, $gateway_details, $wasSuccess = true)
	{
		$response = is_array($response) ? array2text($response) : $response;

		$db->reset();
		$db->assign("oid", $oid);
		$db->assign("uid", $uid);

		$db->assign("completed", "NOW()");
		$db->assign('is_success', $wasSuccess ? '1' : '0');
		$db->assignStr("extra", $note);

		$db->assignStr("payment_type", (is_array($gateway_details) && isset($gateway_details[0]) ? $gateway_details[0] : ''));
		$db->assignStr("payment_gateway", (is_array($gateway_details) && isset($gateway_details[1]) ? $gateway_details[1] : ''));
		$db->assignStr("payment_response", $response);

		$db->assignStr("order_total_amount", $amount);

		$db->assignStr("shipping_method", "");
		$db->assignStr("shipping_submethod", "");
		$db->assignStr("shipping_amount", "");

		$db->assignStr("tax_amount", "");
		$db->insert(DB_PREFIX."payment_transactions");
	}

	public static function verifyIPN($testMode = false)
	{
		global $settings;

		$post_data = 'cmd=_notify-validate';
		
		foreach ($_POST as $var_name => $var_value)
		{
			$post_data .= "&".$var_name."=".urlencode($var_value);
		}

		$post_url = 'https://www.paypal.com/cgi-bin/webscr';
		if ($testMode)
		{
			$post_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		}
		
		//send data back to server
		$c = curl_init();
		
		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		$headers = array();
		$headers[] = 'POST /cgi-bin/webscr HTTP/1.1';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Host: www.paypal.com';
		$headers[] = 'Content-Length: ' . strlen($post_data);

		curl_setopt($c, CURLOPT_URL, trim($post_url));
		curl_setopt($c, CURLOPT_FAILONERROR, 1);
		curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
	
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

		$postDataReq = array();
		parse_str($post_data, $postDataReq);
		self::log('Request:', $postDataReq);

		$buffer = curl_exec($c);

		self::log('Response', array($buffer));

		curl_close($c);
				
		//check verification value
		return trim(strtolower($buffer)) == "verified";
	}

	public static function getExpressCheckoutApi(&$settings, $buttonSource = null)
	{
		global $db;

		$db->query("SELECT * FROM ".DB_PREFIX."payment_methods WHERE active='Yes' AND id like 'paypal%' AND id != 'paypalec' ORDER BY priority");
		$records = $db->getRecords();

		$api = null;

		foreach ($records as $pm)
		{
			switch ($pm['id'])
			{
				case 'paypalpfpro':
				case 'paypalpflink':
				case 'paypaladv':
					$settings['paypalec_Mode'] = $settings[$pm['id'].'_Mode'];
					$settings['paypalec_Currency_Code'] = $settings[$pm['id'].'_Currency_Code'];
					$settings['paypalec_Transaction_Type'] = $settings[$pm['id'].'_Transaction_Type'];

					$liveMode = strtolower($settings[$pm['id'].'_Mode']) != 'test';

					$api = new Payment_PayPal_PayflowApi($settings, $liveMode);

					$api->setApiPartner($settings[$pm['id']."_Partner"]);
					$api->setApiVendor($settings[$pm['id']."_Vendor"]);
					$api->setApiUsername($settings[$pm['id']."_User"]);
					$api->setApiPassword($settings[$pm['id']."_Password"]);
					break;
				
				case 'paypalpro':
					$settings['paypalec_Mode'] = $settings[$pm['id'].'_Mode'];
					$settings['paypalec_Currency_Code'] = $settings[$pm['id'].'_Currency_Code'];
					$settings['paypalec_Transaction_Type'] = $settings[$pm['id'].'_Transaction_Type'];

					$liveMode = strtolower($settings[$pm['id'].'_Mode']) != 'test';

					$api = new Payment_PayPal_PayPalApi($settings, $liveMode);

					$api->setApiUsername($settings[$pm['id']."_Username"]);
					$api->setApiPassword($settings[$pm['id']."_Password"]);

					if (!is_null($settings[$pm['id']."_Signature"]) && !empty($settings[$pm['id']."_Signature"]))
					{
						$api->setApiSignature($settings[$pm['id']."_Signature"]);
					}
					else
					{
						$api->setUseCertificate(true);
					}
					break;
				
				case 'paypalwps':
				case 'paypalipn':
					$settings['paypalec_Mode'] = $settings[$pm['id'].'_Mode'];
					$settings['paypalec_Currency_Code'] = $settings[$pm['id'].'_Currency_Code'];
					$settings['paypalec_Transaction_Type'] = $settings[$pm['id'].'_Transaction_Type'];

					$liveMode = strtolower($settings[$pm['id'].'_Mode']) != 'test';

					$api = new Payment_PayPal_PayPalApi($settings, $liveMode);
					$api->setSubject($settings[$pm['id'].'_business']);
					break;
			}

			if (!is_null($api))
			{
				$api->setButtonSource($buttonSource);

				return $api;
			}
		}

		//Use ec settings / express boarding
		$liveMode = strtolower($settings['paypalec_Mode']) != 'test';
		$api = new Payment_PayPal_PayPalApi($settings, $liveMode);

		if (!is_null($settings["paypalec_Username"]) && !empty($settings["paypalec_Username"]))
		{
			$api->setApiUsername($settings["paypalec_Username"]);
			$api->setApiPassword($settings["paypalec_Password"]);

			if (!is_null($settings["paypalec_Signature"]) && !empty($settings["paypalec_Signature"]))
			{
				$api->setApiSignature($settings["paypalec_Signature"]);
			}
			else
			{
				$api->setUseCertificate(true);
			}
		}
		else
		{
			$api->setSubject($settings["paypalec_Email"]);
			$settings['paypalec_Transaction_Type'] = 'Sale';
		}

		if (!is_null($api))
		{
			$api->setButtonSource($buttonSource);
		}

		return $api;
	}

	protected static function replaceDataKeys()
	{
		return array('ACCT', 'CVV2', 'PWD', 'USER', 'SIGNATURE');
	}

	protected static function cleanData(array &$values)
	{
		foreach (self::replaceDataKeys() as $key)
		{
			$parts = explode('.', $key);

			$s_path = &$values;
			for ($i = 0; $i < count($parts); $i++)
			{
				$part = $parts[$i];
				if (isset($s_path[$part]))
				{
					if (is_array($s_path[$part]))
					{
						$s_path = &$s_path[$part];
					}
					else if ($i + 1 >= count($parts))
					{
						$s_path[$part] = str_repeat('X', strlen($s_path[$part]));
					}
				}
			}
		}
	}

	public static function isPayPalEnabled()
	{
		global $db;

		$paypalEnabledCount = $db->selectOne('SELECT COUNT(*) FROM '.DB_PREFIX."payment_methods WHERE id IN ('paypalec', 'paypalipn', 'paypalpro', 'paypaladv') AND active = 'Yes'");

		$paypalEnabledCount = array_shift($paypalEnabledCount);

		return $paypalEnabledCount > 0;
	}
}
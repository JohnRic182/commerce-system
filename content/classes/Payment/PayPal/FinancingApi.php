<?php

class Payment_PayPal_FinancingApi
{
	public function __construct($liveMode = true)
	{
		$this->liveMode = $liveMode;
	}

	/**
	 * Retrieve a publisherId for paypal financing banners
	 *
	 * @param $emailAddress
	 * @param $payerId
	 * @param $sellerName
	 * @return mixed
	 * @throws Exception
	 * @throws Payment_PayPal_FinancingApiException
	 */
	public function getPublisherId($emailAddress, $payerId, $sellerName)
	{
		$url = $this->getApiEndpoint();
		$query = array(
			't' => base64_encode(LICENSE_NUMBER),
			'email' => $emailAddress,
			'name' => $sellerName,
			'payer_id' => $payerId,
			'l' => time(),
		);
		$data = json_encode($query);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);

		$headers_array = array();
		$headers_array[] = 'CONTENT-TYPE: application/json';
		$headers_array[] = 'ACCEPT: application/json';

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers_array);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$response = curl_exec($ch);

		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if (!curl_errno($ch))
		{
			$responseData = json_decode($response, 1);

			if ($http_status == 201)
			{
				if (isset($responseData['publisherId']))
				{
					return $responseData['publisherId'];
				}
			}
			else
			{
				throw new Payment_PayPal_FinancingApiException(isset($responseData['errors']) ? $responseData['errors'] : 'Unknown error');
			}
		}

		throw new Exception('Unknown error');
	}

	protected function getApiEndpoint()
	{
		if (!$this->liveMode)
		{
			return 'https://account-staging.pinnaclecart.com/paypalbml/';
		}

		return 'https://account.pinnaclecart.com/paypalbml/';
	}
}
<?php
/**
 * Class Payment_DataAccess_PaymentMethodRepository
 */
class Payment_DataAccess_PaymentMethodRepository
{
	/** @var DB $db  */
	protected $db;

	/** @var  DataAccess_SettingsRepository */
	protected $settings;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->db = $db;
		$this->settings = $settings;
	}

	/**
	 * Get all available payment methods
	 *
	 * @param bool $onlyActive
	 * @param ORDER $order
	 *
	 * @return array|null
	 */
	public function getPaymentMethods($onlyActive = false, ORDER $order = null)
	{
		$result = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'payment_methods '.($onlyActive ? ' WHERE active="Yes"' : '').' ORDER BY priority, name');

		if ($result && is_array($result) && count($result) > 0)
		{
			$paymentMethods = array();

			foreach ($result as $paymentMethodData)
			{
				$paymentMethod = new Payment_Method($paymentMethodData);

				if ($order == null || $paymentMethod->isAvailableForOrder($order))
				{
					$paymentMethods[] = $paymentMethod;
				}
			}

			return $paymentMethods;
		}

 		return null;
	}

	/**
	 * Get recurring billing payment method
	 *
	 * @param bool $onlyActive
	 * @return array|null
	 */
	public function getRecurringPaymentMethods($onlyActive = false)
	{
		$result = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'payment_methods WHERE supports_recurring_billing = 1'.($onlyActive ? ' AND active="Yes"' : '').' ORDER BY priority, name');

		if ($result && is_array($result) && count($result) > 0)
		{
			$paymentMethods = array();

			foreach ($result as $paymentMethodData)
			{
				$paymentMethods[$paymentMethodData['id']] = new Payment_Method($paymentMethodData);
			}

			return $paymentMethods;
		}

		return array();
	}

	/**
	 * Get payment methods count
	 *
	 * @param bool $onlyActive
	 *
	 * @return int
	 */
	public function getPaymentMethodsCount($onlyActive = false)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'payment_methods '.($onlyActive ? ' WHERE active="Yes"' : ''));

		if ($result && is_array($result) && count($result) > 0)
		{
			return $result['c'];
		}

		return 0;
	}

	/**
	 * Select payment methods by id
	 *
	 * @param $id
	 * @param bool $onlyActive
	 *
	 * @return Payment_Method|null
	 */
	public function getById($id, $onlyActive = false)
	{
		$result = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'payment_methods WHERE pid='.intval($id).($onlyActive ? ' AND active="Yes"' : ''));

		return $result ? new Payment_Method($result) : null;
	}

	public function getByMethodId($methodid, $onlyActive = false)
	{
		$result = $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'payment_methods WHERE id= "' . ($methodid) . '" ' . ($onlyActive ? ' AND active="Yes"' : ''));

		return $result ? new Payment_Method($result) : null;
	}
}
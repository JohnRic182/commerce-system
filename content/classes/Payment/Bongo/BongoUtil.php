<?php

/**
 * Class Payment_Bongo_BongoUtil
 */
class Payment_Bongo_BongoUtil
{
	/**
	 * Filter shipping method to be used with Bongo
	 *
	 * @param $rates
	 * @param $bongoShippingMethod
	 * @param $msg
	 *
	 * @return mixed
	 */
	public static function filterShippingMethods($rates, $bongoShippingMethod, &$msg)
	{
		$cheapestMethod = null;
		$cheapestMethodRate = INF;

		$defaultMethod = null;

		foreach ($rates['shipments'] as $shipmentId => $shipment)
		{
			foreach ($shipment['shipping_methods'] as $shippingMethod)
			{
				if ($shippingMethod['ssid'] == $bongoShippingMethod)
				{
					$defaultMethod = $shippingMethod;
				}

				if ($shippingMethod['shipping_price'] < $cheapestMethodRate)
				{
					$cheapestMethod = $shippingMethod;
					$cheapestMethodRate = $shippingMethod['shipping_price'];
				}
			}

			if ($bongoShippingMethod == 'auto')
			{
				if (!is_null($cheapestMethod))
				{
					$cheapestMethod['carrier_name'] = $cheapestMethod['method_name'] = $msg['shipping']['international_shipping'];
					$shipment['shipping_methods'] = array($cheapestMethod);
				}
			}
			else
			{
				if (!is_null($defaultMethod))
				{
					$defaultMethod['carrier_name'] = $defaultMethod['method_name'] = $msg['shipping']['international_shipping'];
					$shipment['shipping_methods']['shipping_methods'] = array($defaultMethod);
				}
				else if (!is_null($cheapestMethod))
				{
					$cheapestMethod['carrier_name'] = $cheapestMethod['method_name'] = $msg['shipping']['international_shipping'];
					$shipment['shipping_methods'] = array($cheapestMethod);
				}
			}

			$rates['shipments'][$shipmentId] = $shipment;
		}

		// TODO: determinate what to do when there no shipping methods found

		return $rates;
	}

	/**
	 * Get Bongo facility shipping address
	 *
	 * @param DB $db
	 * @param array $settings
	 * @param array $addressTo
	 * @param $bongoUsed
	 * @return array
	 *
	 * TODO: set address as an object?
	 */
	public static function getBongoShippingAddress(DB $db, array &$settings, array &$addressTo, &$bongoUsed)
	{
		$countryGroupId = $settings['bongocheckout_Shipping_Country_Group'];
		$countryMatch = false;

		// TODO: country groups are deprecated. Must be removed.
		if ($countryGroupId != 'all' && $countryGroupId != '')
		{
			DataAccess_Repository::setDatabaseInstance($db);

			/** @var DataAccess_CountryGroupsRepository $countryGroupsRepository */
			$countryGroupsRepository = DataAccess_Repository::get('CountryGroupsRepository');

			$countryGroup = $countryGroupsRepository->getById($countryGroupId);
			if ($countryGroup)
			{
				$countryGroupCountries = explode(',', $countryGroup['countries']);
				$countryMatch = in_array($addressTo['country_id'], $countryGroupCountries);
			}
		}
		else
		{
			$countryMatch = true;
		}

		if ($countryMatch)
		{
			$bongoCountryData = $db->selectOne('SELECT * FROM '.DB_PREFIX.'countries WHERE iso_a2="US"');
			$bongoStateData = $db->selectOne('SELECT * FROM '.DB_PREFIX.'states WHERE stid='.intval($settings['bongocheckout_State']));

			$bongoUsed = true;

			return array(
				'address_type' => 'Business',
				'name' => $settings['bongocheckout_Company_Name'].', attn. '.$settings['bongocheckout_Attention_To'],
				'address1' => $settings['bongocheckout_Address1'],
				'address2' => $settings['bongocheckout_Address2'],
				'city' => $settings['bongocheckout_City'],
				'state_name' => $bongoStateData['name'],
				'state_code' => $bongoStateData['short_name'],
				'country' => $bongoCountryData['name'],
				'country_id' =>  $bongoCountryData['coid'],
				'country_iso_a2' => $bongoCountryData['iso_a2'],
				'country_iso_a3' => $bongoCountryData['iso_a3'],
				'country_iso_number' => $bongoCountryData['iso_number'],
				'zip' => $settings['bongocheckout_Zip_Code']
			);
		}

		return $addressTo;
	}
}
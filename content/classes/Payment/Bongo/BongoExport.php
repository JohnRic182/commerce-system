<?php

/**
 * Class Payment_Bongo_BongoExport
 */
class Payment_Bongo_BongoExport
{
	protected $db;

	protected $settings;

	protected $fields = array(
		'language',
		'productId',
		'productDescription',
		'url',
		'imageUrl',
		'price',
		'originCountry',
		'hsCode',
		'ECCN',
		'haz',
		'licenseFlag',
		'importFlag',
		'productType',
		'L1',
		'W1',
		'H1',
		'WT1'
	);

	/**
	 * Class constructor
	 */
	public function __construct(&$db, &$settings)
	{
		$this->db = $db;
		$this->settings = $settings;
		require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/content/vendors/nusoap/nusoap.php');
	}

	/**
	 * Get headers for CSV file
	 */
	public function getCsvHeaders()
	{
		//set headers
		$mime_type = "text/csv";
		$filename = 'bongo-products';
		$ext = "csv";

		header('Content-Type: '.$mime_type);
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="'.$filename.'.'.$ext.'"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Disposition: attachment; filename="'.$filename.'.'.$ext.'"');
	}

	/**
	 * Get categories pathes
	 */
	public function getCategories()
	{
		$result = $this->db->query('SELECT cid, parent, name FROM '.DB_PREFIX.'catalog ORDER BY level');

		$categories = array();

		while (($cat = $this->db->moveNext($result)) != false)
		{
			$path = trim($cat['name']);
			
			if (isset($categories[$cat['parent']])) $path = $categories[$cat['parent']].'/'.$path;

			$categories[$cat['cid']] = $path;
		}

		return $categories;
	}

	/**
	 * Cleanup string
	 */
	public function strToCsv($s)
	{
		return '"'.str_replace(array("\t", "\r", "\n", '"'), array("\\t", " ", " ", '""'), $s).'"';
	}

	/**
	 * Render product
	 */
	public function renderCsvProduct($productData, &$categories, ShoppingCartProducts $products)
	{
		$separator = ',';

		$productImage = $products->getProductImage($productData['product_id'], $productData['image_location'], $productData['image_url'], $this->settings['GlobalHttpUrl'].'/');

		echo $line =
			$this->strToCsv('en').$separator. // language
			$this->strToCsv($productData['product_id'].(isset($productData['product_subid']) ? '-'.$productData['product_subid'] : '')).$separator. // product id
			$this->strToCsv($productData['title']).$separator. // productDescription
			$this->strToCsv(UrlUtils::getProductUrl($productData["product_url"], $productData["pid"], $productData["cid"])).$separator. // url
			$this->strToCsv($productImage ? $productImage : '').$separator. // imageUrl
			$this->strToCsv(round($productData['price'], 2)).$separator. // price
			$this->strToCsv('US').$separator. // originCountry
			$this->strToCsv('').$separator. // hsCode
			$this->strToCsv('').$separator. // ECCN
			$this->strToCsv('0').$separator. // haz
			$this->strToCsv('').$separator. // licenseFlag
			$this->strToCsv('').$separator. // importFlag
			$this->strToCsv(isset($categories[$productData['cid']]) ? $categories[$productData['cid']] : '').$separator. // productType
			$this->strToCsv($productData['dimension_length']).$separator. // L1
			$this->strToCsv($productData['dimension_width']).$separator. // W1
			$this->strToCsv($productData['dimension_height']).$separator. // H1
			$this->strToCsv($productData['weight']).$separator. // WT1
			"\n";
	}

	/**
	 * Get products count with subids
	 */
	public function getProductsCountWithSubids($exportAllProducts)
	{
		$result = $this->db->query('
			SELECT COUNT(p.pid) AS c
			FROM '.DB_PREFIX.'products AS p
			LEFT JOIN '.DB_PREFIX.'products_inventory AS p_i ON p.pid=p_i.pid
			WHERE p_i.pi_id IS NULL '.($exportAllProducts ? '' : ' AND p.is_visible="YES"').'
		');
		$col = $this->db->moveNext($result);
		return $col['c'];
	}

	/**
	 * Get products counts with no subids
	 */
	public function getProductsCountWithNoSubids($exportAllProducts)
	{
		$result = $this->db->query('
			SELECT COUNT(p.pid) AS c
			FROM '.DB_PREFIX.'products_inventory AS p_i
			INNER JOIN '.DB_PREFIX.'products AS p ON p_i.pid=p.pid
			'.($exportAllProducts ? '' : 'WHERE p.is_visible="YES"').'
		');
		$col = $this->db->moveNext($result);
		return $col['c'];
	}

	/**
	 * Get products with subids
	 */
	public function getProductsWithSubids($exportAllProducts, $offset = null, $count = null)
	{
		return $this->db->query('
			SELECT p.*, IF (p.url_custom = "", p.url_default, p.url_custom) AS product_url, p_i.product_subid AS product_subid
			FROM '.DB_PREFIX.'products AS p
			LEFT JOIN '.DB_PREFIX.'products_inventory AS p_i ON p.pid=p_i.pid
			WHERE p.product_type = "Tangible" AND  p_i.pi_id IS NULL '.($exportAllProducts ? '' : ' AND p.is_visible="YES"').'
			'.(!is_null($offset) ? ' LIMIT '.intval($offset).','.intval($count) : '').'
		');
	}

	/**
	 * Get products with not sub ids
	 */
	public function getProductsWithNoSubids($exportAllProducts, $offset = null, $count = null)
	{
		return $this->db->query('
			SELECT p.*, IF (p.url_custom = "", p.url_default, p.url_custom) AS product_url
			FROM '.DB_PREFIX.'products_inventory AS p_i
			INNER JOIN '.DB_PREFIX.'products AS p ON p_i.pid=p.pid
			WHERE p.product_type = "Tangible" '.($exportAllProducts ? '' : ' AND p.is_visible="YES" ').'
			'.(!is_null($offset) ? ' LIMIT '.intval($offset).','.intval($count) : '').'
		');
	}

	/**
	 * Get products
	 */
	public function getCsvProducts($exportAllProducts = true)
	{
		$categories = $this->getCategories();

		$products = new ShoppingCartProducts($this->db, $this->settings);

		// select product with sub-ids first
		$result = $this->getProductsWithSubids($exportAllProducts);

		while (($productData = $this->db->moveNext($result)) != false)
		{
			$this->renderCsvProduct($productData, $categories, $products);
		}

		// select product without sub-ids
		$result = $this->getProductsWithNoSubids($exportAllProducts);

		while (($productData = $this->db->moveNext($result)) != false)
		{
			$this->renderCsvProduct($productData, $categories, $products);
		}
	}

	/**
	 * Render CSV file 
	 */
	public function exportCsv($exportAllProducts)
	{
		$this->getCsvHeaders();

		echo implode(',', $this->fields)."\n";

		$this->getCsvProducts($exportAllProducts);
	}

	/**
	 * Synchronization
	 */
	public function synch($result, $categories)
	{
		$items = array();

		$products = new ShoppingCartProducts($this->db, $this->settings);

		while (($productData = $this->db->moveNext($result)) != false)
		{
			$productImage = $products->getProductImage($productData['product_id'], $productData['image_location'], $productData['image_url'], $this->settings['GlobalHttpUrl'].'/');

			$items[] = array(
				'productID' => $productData['product_id'].(isset($productData['product_subid']) ? '-'.$productData['product_subid'] : ''),
				'description' => $productData['title'],
				'url' => UrlUtils::getProductUrl($productData["product_url"], $productData["pid"], $productData["cid"]),
				'imageUrl' => $productImage ? $productImage : '',
				'price' => round($productData['price'], 2),
				'countryOfOrigin' => 'US',
				'hsCode' => '',
				'eccn' => '',
				'hazFlag' => '0',
				'licenseFlag' => '',
				'importFlag' => '',
				'productType' => isset($categories[$productData['cid']]) ? $categories[$productData['cid']] : '',
				'itemInformation' => array(
					array(
						'l' => $productData['dimension_length'],
						'w' => $productData['dimension_width'],
						'h' => $productData['dimension_height'],
						'wt' => $productData['weight']
					)
				)
            );
        }

        $client = new SoapClient("https://api.bongous.com/services/v4?wsdl");

		$request = (object)array(
    		'partnerKey' => $this->settings['bongocheckout_Partner_Key'],
    		'language' => 'en',
    		'items' => $items
    	);

    	$response = $client->connectProductInfo($request);

    	return count($items);
	}
}
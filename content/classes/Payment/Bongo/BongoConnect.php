<?php

/**
 * Class BongoConnect
 */
class Payment_Bongo_BongoConnect
{
	protected $partnerKey;

	protected $lastError = null;
	protected $lastErrorDetails = null;

	/**
	 * Class constructor
	 *
	 * @param $partnerKey
	 */
	public function __construct($partnerKey)
	{
		$this->partnerKey = $partnerKey;
	}

	/**
	 * Set last error
	 *
	 * @param $lastError
	 */
	public function setLastError($lastError)
	{
		$this->lastError = $lastError;
	}

	/**
	 * Get last error
	 */
	public function getLastError()
	{
		return $this->lastError;
	}

	/**
	 * Set last error details
	 *
	 * @param $lastErrorDetails
	 */
	public function setLastErrorDetails($lastErrorDetails)
	{
		$this->lastErrorDetails = $lastErrorDetails;
	}

	/**
	 * Get last error details
	 */
	public function getLastErrorDetails()
	{
		return $this->lastErrorDetails;
	}

	/**
	 * Update order tracking information
	 *
	 * @param $orderItems
	 * @param $bongoOrderId
	 * @param $shippingCarrier
	 * @param $shippingTrackingNumber
	 *
	 * @return bool
	 */
	public function orderTrackingUpdate($orderItems, $bongoOrderId, $shippingCarrier, $shippingTrackingNumber)
	{
		$this->setLastError(null);
		$this->setLastErrorDetails(null);

		try
		{
			$client = new SoapClient('https://api.bongous.com/services/v4?wsdl');

			$shippingCarrier = trim(strtoupper($shippingCarrier));

			$shippingCarriers = array(
				'UPS' => 1,
				'FEDEX' => 2,
				'DHL' => 3,
				'USPS' => 4,
				'EMS' => 5
			);

			$shippingCarrierCode = isset($shippingCarriers[$shippingCarrier]) ? $shippingCarriers[$shippingCarrier] : 6; // all others

			$trackList = array();

			foreach ($orderItems as $orderItem)
			{
				$trackList[] = array(
					'productID' => $orderItem['id'], // include subid if defined
					'trackingNumber' => $shippingTrackingNumber,
					'quantity' => $orderItem['quantity'],
					'carrier' => $shippingCarrierCode
				);
			}

			if (count($trackList) == 0) return false;

			$request = (object)array(
				'partnerKey' => $this->partnerKey,
				'language' => 'en',
				'orderNumber' => $bongoOrderId,
				'trackList' => $trackList
			);

			if (defined('DEVMODE') && DEVMODE)
			{
				@file_put_contents(
					dirname(dirname(dirname(__FILE__))).'/cache/log/bongo.txt',
					date('Y/m/d-H:i:s').' - REQUEST - '.print_r($request, 1)."\n\n\n",
					FILE_APPEND
				);
			}

			$response = $client->ConnectOrderTrackingUpdate($request);

			if (defined('DEVMODE') && DEVMODE)
			{
				@file_put_contents(
					dirname(dirname(dirname(__FILE__))).'/cache/log/bongo.txt',
					date('Y/m/d-H:i:s').' - RESPONSE - '.print_r($response, 1)."\n\n\n",
					FILE_APPEND
				);
			}

			if ($response)
			{
				if (isset($response->error))
				{
					if ($response->error == 0) return true;

					$this->setLastError($response->errorMessage);
					$this->setLastErrorDetails($response->errorMessageDetail);
					return false;
				}
			}
			
			$this->setLastError('Cannot send tracking information to Bongo');
			$this->setLastErrorDetails('Please check your credentials');
			
		}
		catch (Exception $e)
		{
			$this->setLastError($e->getCode());
			$this->setLastErrorDetails($e->getMessage());
		}

		return false;
	}

	/**
	 * Get Bongo landed cost
	 *
	 * @param $orderItems
	 * @param $shipmentDestinationCountry
	 * @param $domesticShippingCost
	 *
	 * @return bool
	 */
	public function getLandedCost($orderItems, $shipmentDestinationCountry, $domesticShippingCost)
	{
		$bongoClient = new SoapClient('https://api.bongous.com/services/v4?wsdl');

		$bongoOrderItems = array();
		$bongoOrderItemsNames = array();

		foreach ($orderItems as $item)
		{
			$bongoOrderItems[] = array(
				'productID' => $productId = $item['product_id'].(trim($item['product_sub_id']) != '' ? '-'.$item['product_sub_id'] : ''),
				'quantity' => $item['quantity'],
				'price' => $item['price'],
			);

			$bongoOrderItemsNames[$productId] = $item['title'];
		}

		$bongoCurrency = isset($_SESSION['current_currency']) ? $_SESSION['current_currency']['code'] : 'USD';

		$request = (object) array(
			'partnerKey' => $this->partnerKey,
			'language' => 'en',
			'shipmentOriginCountry' => 'US',
			'shipmentDestinationCountry' => $shipmentDestinationCountry, //$order->shippingAddress['country_iso_a2'],
			'domesticShippingCost' => $domesticShippingCost, //$order->getShippingAmount(),
			'insuranceFlag' => '0',
			'currency' => $bongoCurrency,
			'currencyConversionRate' => '',
			'privateIndividuals' => '',
			'service' => '1',
			'items' => $bongoOrderItems,
		);

		try
		{
			$response = $bongoClient->ConnectLandedCost($request);

			if ($response)
			{
				$isError = false;

				if (isset($response->error) && $response->error != 0)
				{
					$isError = true;
					$errorMessage =
						'Cannot determinate international shipping cost at this moment.'."\n".
						'Please edit your order, or change your shipping information.';
				}

				if (isset($response->items) && is_array($response->items))
				{
					$errorMessageItems = '';

					foreach ($response->items as $item)
					{
						if (isset($item->errorMessage) && trim($item->errorMessage) != '' && isset($bongoOrderItemsNames[$item->productID]) && strpos(strtolower(trim($item->errorMessage)), 'product is not recognized') !== false)
						{
							$errorMessageItems .= "\n".'- '.$bongoOrderItemsNames[$item->productID];
						}
					}

					if ($errorMessageItems != '')
					{
						$isError = true;
						$errorMessage =
							'One or more products are not available for international shipping.'."\n".
							'Please edit your order, or change your shipping information. '.
							$errorMessageItems;
					}
				}

				if ($isError)
				{
					$this->setLastError($errorMessage);
				}
				else
				{
					return $response;
				}
			}
		}
		catch (Exception $exception)
		{
			error_log(print_r($exception, 1));
		}

		return false;
	}
}

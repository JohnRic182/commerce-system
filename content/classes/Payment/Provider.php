<?php
/**
 * Class Payment_Provider
 *
 * TODO: combine with DataAccess_PaymentMethodRepository ?
 */
class Payment_Provider
{
	/** @var DB $db */
	protected $db;

	/** @var PAYMENT $payment */
	protected $payment;

	/** @var  Payment_DataAccess_PaymentMethodRepository */
	protected $repository;

	/**
	 * Class constructor
	 *
	 * @param Payment_DataAccess_PaymentMethodRepository $paymentMethodsRepository
	 */
	public function __construct(Payment_DataAccess_PaymentMethodRepository $paymentMethodsRepository)
	{
		$this->repository = $paymentMethodsRepository;
	}

	/**
	 * Get payment processor by id
	 *
	 * @param $paymentMethodId
	 *
	 * @return null|PAYMENT_PROCESSOR
	 */
	public function getPaymentProcessorById($paymentMethodId)
	{
		global $payment_processor_class, $payment_processor_id, $payment_processor_name, $payment;

		$paymentMethod = $this->repository->getById($paymentMethodId, true);

		if ($paymentMethod)
		{
			$paymentProcessor = $paymentMethod->getPaymentProcessor();

			return $paymentProcessor;
		}

		return null;
	}

	/**
	 * Get order's payment processor
	 *
	 * @param ORDER $order
	 *
	 * @return null|PAYMENT_PROCESSOR
	 */
	public function getPaymentProcessor(ORDER $order)
	{
		$pp = $this->getPaymentProcessorById($order->getPaymentMethodId());

		if (!is_null($pp))
		{
			$order->getShippingAddress();
			$user = $order->user();

			$pp->getCommonData($user, $order);
		}

		return $pp;
	}

	/**
	 * Get current dir
	 *
	 * @return string
	 */
	protected function contentDirPath()
	{
		return dirname(dirname(dirname(__FILE__)));
	}
}
<?php
/**
 * Class Payment_View_PaymentMethodView
 */
class Payment_View_PaymentMethodView
{
	public $id;
	public $code;
	public $type;
	public $name;
	public $title;
	public $message_payment;
	public $message_thank_you;

	/**
	 * Class constructor
	 *
	 * @param Payment_Method $paymentMethod
	 */
	public function __construct(Payment_Method $paymentMethod)
	{
		$this->id = $paymentMethod->getId();
		$this->code = $paymentMethod->getCode();
		$this->type = $paymentMethod->getType();
		$this->name = $paymentMethod->getName();
		$this->title = $paymentMethod->getTitle();
		$this->message_payment = $paymentMethod->getMessagePayment();
		$this->message_thank_you = $paymentMethod->getMessageThankYou();
	}

	/**
	 * Get many payment methods view model
	 *
	 * @param $paymentMethods
	 *
	 * @return array
	 */
	public static function getPaymentMethods($paymentMethods)
	{
		$result = array();

		/** @var Payment_Method $paymentMethod */
		foreach($paymentMethods as $key => $paymentMethod)
		{
			$result[$key] = new Payment_View_PaymentMethodView($paymentMethod);
		}

		return $result;
	}
}
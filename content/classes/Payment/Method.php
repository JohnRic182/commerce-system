<?php
/**
 * Class Payment_Method
 */
class Payment_Method
{
	const TYPE_CC = 'cc';
	const TYPE_IPN = 'ipn';
	const TYPE_CUSTOM = 'custom';
	const TYPE_CHECK = 'check';

	const SOLUTION_TYPE_ALL_IN_ONE = 'all-in-one';
	const SOLUTION_TYPE_GATEWAY = 'gateway';
	const SOLUTION_TYPE_INSTANT = 'instant';
	const SOLUTION_TYPE_CUSTOM = 'custom';

	/** @var int $id */
	protected $id;

	/** @var bool $isActive */
	protected $isActive;

	/** @var string $type */
	protected $type;

	/** @var int $priority */
	protected $priority;

	/** @var string $code */
	protected $code;

	/** @var string $className */
	protected $className;

	/** @var string $gatewayUrl */
	protected $gatewayUrl;

	/** @var string $gatewayName */
	protected $gatewayName;

	/** @var string $name */
	protected $name;

	/** @var string $title */
	protected $title;

	/** @var bool $ccsEnabled */
	protected $ccsEnabled;

	/** @var string $solutionType */
	protected $solutionType;

	/** @var string $messagePayment */
	protected $messagePayment;

	/** @var string $messageThankYou */
	protected $messageThankYou;

	/** @var string $description */
	protected $description;

	/** @var float $minOrderAmount */
	protected $minOrderAmount;

	/** @var float $maxOrderAmount */
	protected $maxOrderAmount;

	/** @var array|null $countriesInclude */
	protected $countriesInclude = null;

	/** @var array|null $countriesExclude */
	protected $countriesExclude = null;

	/** @var array|null $countriesFilter */
	protected $countriesFilter = null;

	/** @var bool $supportsRecurringBilling */
	protected $supportsRecurringBilling = false;

	/** @var PAYMENT_PROCESSOR $paymentProcessor */
	protected $paymentProcessor = null;

	/** @var bool $enableFraudservice */
	protected $enableFraudservice;

	/**
	 * Class constructor
	 *
	 * @param null $data
	 */
	public function __construct($data = null)
	{
		if (!is_null($data))
		{
			$this->hydrateFromArray($data);
		}
	}

	/**
	 * Return payment processor
	 *
	 * @return PAYMENT_PROCESSOR
	 */
	public function getPaymentProcessor()
	{
		if (is_null($this->paymentProcessor))
		{
			// TODO: remove global dependency here
			global $db;
			$this->paymentProcessor = $this->createPaymentProcessor($db);
		}

		return $this->paymentProcessor;
	}

	/**
	 * Check is this payment method available for order
	 *
	 * @param ORDER $order
	 *
	 * @return bool
	 */
	public function isAvailableForOrder(ORDER $order)
	{
		// TODO: Country group filtering

		if ($this->getMinOrderAmount() > 0 && $order->getTotalAmount() < $this->getMinOrderAmount()) return false;
		if ($this->getMaxOrderAmount() > 0 && $order->getTotalAmount() > $this->getMaxOrderAmount()) return false;

		$countries = $this->getCountriesFilter();

		if (!is_null($countries) && is_array($countries))
		{
			$user = $order->getUser();
			$billingCountry = $user->data['country'];
			if ($billingCountry != 0 && !in_array($billingCountry, $countries)) return false;
		}

		if ($order->getOrderType() == ORDER::ORDER_TYPE_WEB && $order->hasRecurringBillingItems() && !$this->getSupportsRecurringBilling())
		{
			return false;
		}

		return true;
	}

	/**
	 * Get payment processor class instance
	 * TODO: move out of this class
	 *
	 * @param DB $db
	 *
	 * @return PAYMENT_PROCESSOR|null
	 */
	protected function createPaymentProcessor(DB $db)
	{
		$paymentProcessor = null;

		if (($code = $this->getCode()) != '' && ($className = $this->getClassName()) != '')
		{
			if (class_exists($className))
			{
				/** @var PAYMENT_PROCESSOR $paymentProcessor */
				$paymentProcessor = new $className();

				$paymentProcessor->enableLog = defined('DEVMODE') && DEVMODE;
				$paymentProcessor->db_id = $this->getId();

				// global DB
				$paymentProcessor->getSettingsData($db, $this->getGatewayUrl(), $this->getCcsEnabled());

				$paymentProcessor->isTestMode(); // force test mode check to possibly update URLs
			}
		}

		return $paymentProcessor;
	}

	/**
	 * Hydrate from an array
	 *
	 * @param $data
	 */
	public function hydrateFromArray($data)
	{
		$this->setId($data['pid']);
		$this->setIsActive($data['active'] == 'Yes');
		$this->setType($data['type']);
		$this->setPriority($data['priority']);
		$this->setCode($data['id']);
		$this->setClassName($data['class']);
		$this->setGatewayUrl($data['url_to_gateway']);
		$this->setGatewayName($data['gateway_name']);
		$this->setName($data['name']);
		$this->setTitle($data['title']);
		$this->setCcsEnabled($data['enable_ccs'] == '1');
		$this->setEnableFraudservice($data['enable_fraudservice'] == '1');
		$this->setSolutionType($data['solution_type']);
		$this->setMessagePayment($data['message_payment']);
		$this->setMessageThankYou($data['message_thankyou']);
		$this->setDescription($data['description']);
		$this->setMinOrderAmount(floatval($data['min_order_amount']));
		$this->setMaxOrderAmount(floatval($data['max_order_amount']));
		$this->setSupportsRecurringBilling($data['supports_recurring_billing'] == 1);

		$countriesInclude = trim($data['countries_include']);
		$countriesExclude = trim($data['countries_exclude']);
		$countriesFilter = trim($data['countries_exclude']);

		$this->setCountriesInclude($countriesInclude != '' && $countriesInclude != '0' ? explode(',', $countriesInclude) : null);
		$this->setCountriesExclude($countriesExclude != '' && $countriesExclude != '0' ? explode(',', $countriesExclude) : null);
		$this->setCountriesFilter($countriesFilter != '' && $countriesFilter != '0' ? explode(',', $countriesFilter) : null);
	}

	/**
	 * @param boolean $supportsRecurringBilling
	 */
	public function setSupportsRecurringBilling($supportsRecurringBilling)
	{
		$this->supportsRecurringBilling = $supportsRecurringBilling;
	}

	/**
	 * @return boolean
	 */
	public function getSupportsRecurringBilling()
	{
		return $this->supportsRecurringBilling;
	}

	/**
	 * Set ccs enabled
	 *
	 * @param bool $ccsEnabled
	 */
	public function setCcsEnabled($ccsEnabled)
	{
		$this->ccsEnabled = $ccsEnabled;
	}

	/**
	 * Get is ccs enabled
	 *
	 * @return bool
	 */
	public function getCcsEnabled()
	{
		return $this->ccsEnabled;
	}

	/**
	 * Set class name
	 *
	 * @param string $className
	 */
	public function setClassName($className)
	{
		$this->className = $className;
	}

	/**
	 * Get class name
	 *
	 * @return string
	 */
	public function getClassName()
	{
		return $this->className;
	}

	/**
	 * Set code (former id)
	 *
	 * @param string $code
	 */
	public function setCode($code)
	{
		$this->code = $code;
	}

	/**
	 * Get code (former id)
	 *
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * Set countries exclude
	 *
	 * @param array $countriesExclude
	 */
	public function setCountriesExclude($countriesExclude)
	{
		$this->countriesExclude = $countriesExclude;
	}

	/**
	 * Get countries exclude
	 *
	 * @return array
	 */
	public function getCountriesExclude()
	{
		return $this->countriesExclude;
	}

	/**
	 * Set countries filter
	 *
	 * @param array $countriesFilter
	 */
	public function setCountriesFilter($countriesFilter)
	{
		$this->countriesFilter = $countriesFilter;
	}

	/**
	 * Get countries filter
	 *
	 * @return array
	 */
	public function getCountriesFilter()
	{
		return $this->countriesFilter;
	}

	/**
	 * Set countries include
	 *
	 * @param array $countriesInclude
	 */
	public function setCountriesInclude($countriesInclude)
	{
		$this->countriesInclude = $countriesInclude;
	}

	/**
	 * Get countries include
	 *
	 * @return array
	 */
	public function getCountriesInclude()
	{
		return $this->countriesInclude;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * Set gateway name
	 *
	 * @param string $gatewayName
	 */
	public function setGatewayName($gatewayName)
	{
		$this->gatewayName = $gatewayName;
	}

	/**
	 * Get gateway name
	 *
	 * @return string
	 */
	public function getGatewayName()
	{
		return $this->gatewayName;
	}

	/**
	 * Set id
	 *
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set is active
	 *
	 * @param bool $isActive
	 */
	public function setIsActive($isActive)
	{
		$this->isActive = $isActive;
	}

	/**
	 * Get is active
	 *
	 * @return bool
	 */
	public function getIsActive()
	{
		return $this->isActive;
	}

	/**
	 * Set max order amount
	 *
	 * @param float $maxOrderAmount
	 */
	public function setMaxOrderAmount($maxOrderAmount)
	{
		$this->maxOrderAmount = $maxOrderAmount;
	}

	/**
	 * Get max order amount
	 *
	 * @return float
	 */
	public function getMaxOrderAmount()
	{
		return $this->maxOrderAmount;
	}

	/**
	 * Set payment page message
	 *
	 * @param string $messagePayment
	 */
	public function setMessagePayment($messagePayment)
	{
		$this->messagePayment = $messagePayment;
	}

	/**
	 * Get payment message
	 *
	 * @return string
	 */
	public function getMessagePayment()
	{
		return $this->messagePayment;
	}

	/**
	 * Set thank you page message
	 *
	 * @param string $messageThankYou
	 */
	public function setMessageThankYou($messageThankYou)
	{
		$this->messageThankYou = $messageThankYou;
	}

	/**
	 * Get thank you page message
	 *
	 * @return string
	 */
	public function getMessageThankYou()
	{
		return $this->messageThankYou;
	}

	/**
	 * Set min order amount
	 *
	 * @param float $minOrderAmount
	 */
	public function setMinOrderAmount($minOrderAmount)
	{
		$this->minOrderAmount = $minOrderAmount;
	}

	/**
	 * Get min order amount
	 *
	 * @return float
	 */
	public function getMinOrderAmount()
	{
		return $this->minOrderAmount;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set priority
	 *
	 * @param int $priority
	 */
	public function setPriority($priority)
	{
		$this->priority = $priority;
	}

	/**
	 * Get priority
	 *
	 * @return int
	 */
	public function getPriority()
	{
		return $this->priority;
	}

	/**
	 * Set solution type
	 *
	 * @param string $solutionType
	 */
	public function setSolutionType($solutionType)
	{
		$this->solutionType = $solutionType;
	}

	/**
	 * Set solution type
	 *
	 * @return string
	 */
	public function getSolutionType()
	{
		return $this->solutionType;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set title
	 *
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * Set gateway URL
	 *
	 * @param string $gatewayUrl
	 */
	public function setGatewayUrl($gatewayUrl)
	{
		$this->gatewayUrl = $gatewayUrl;
	}

	/**
	 * Get gateway URL
	 *
	 * @return string
	 */
	public function getGatewayUrl()
	{
		return $this->gatewayUrl;
	}

	/**
	 * @return boolean
	 */
	public function isEnableFraudservice()
	{
		return $this->enableFraudservice;
	}

	/**
	 * @param boolean $enableFraudservice
	 */
	public function setEnableFraudservice($enableFraudservice)
	{
		$this->enableFraudservice = $enableFraudservice;
	}


}
<?php

class Payment_Stripe_Connect
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;
	protected $isTest = false;

	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
		$this->isTest = defined('STRIPE_CONNECT_TEST_MODE') && STRIPE_CONNECT_TEST_MODE;
	}

	public function getConnectUrl()
	{
		$license_id = '';

		$licenseParts = explode('-', LICENSE_NUMBER);
		if (count($licenseParts) > 1)
		{
			$license_id = $licenseParts[1];
		}

		$stripeUserParams = array();
		if ($this->settings->get('CompanyAddressLine1') != '')
		{
			$stripeUserParams['stripe_user[street_address]'] = $this->settings->get('CompanyAddressLine1');
		}
		if ($this->settings->get('CompanyCity') != '')
		{
			$stripeUserParams['stripe_user[city]'] = $this->settings->get('CompanyCity');
		}
		if ($this->settings->get('CompanyState') != '')
		{
			$stripeUserParams['stripe_user[state]'] = $this->settings->get('CompanyState');
		}
		if ($this->settings->get('CompanyZip') != '')
		{
			$stripeUserParams['stripe_user[zip]'] = $this->settings->get('CompanyZip');
		}
		if ($this->settings->get('CompanyCountry') != '')
		{
			$stripeUserParams['stripe_user[country]'] = $this->settings->get('CompanyCountry');
		}
		if ($this->settings->get('CompanyPhone') != '')
		{
			$stripeUserParams['stripe_user[phone_number]'] = str_replace(' ', '', str_replace('-', '', $this->settings->get('CompanyPhone')));
		}
		if ($this->settings->get('CompanyName') != '')
		{
			$stripeUserParams['stripe_user[business_name]'] = $this->settings->get('CompanyName');
		}

		global $admin;
		if (isset($admin['name']))
		{
			$nameParts = explode(' ', $admin['name']);
			if (count($nameParts) > 0) $stripeUserParams['stripe_user[first_name]'] = $nameParts[0];
			if (count($nameParts) > 1) $stripeUserParams['stripe_user[last_name]'] = $nameParts[1];
		}
		if (isset($admin['email']))
			$stripeUserParams['stripe_user[email]'] = $admin['email'];

		if (Admin_HostedHelper::isHosted())
		{
			return 'https://connect.stripe.com/oauth/authorize?client_id='.$this->getClientId().'&response_type=code&scope=read_write&state='.$license_id.'&'.http_build_query($stripeUserParams);
		}
		else
		{
			return 'https://connect.stripe.com/oauth/authorize?client_id='.$this->getClientId().'&response_type=code&scope=read_write&state='.urlencode($this->settings->get('AdminHttpsUrl')).'&'.http_build_query($stripeUserParams);
		}
	}

	public function isConnected()
	{
		return $this->settings->get('stripe_ApiSecretKey') != '' && $this->settings->get('stripe_ApiPublishableKey') != '';
	}

	public function connection($publishableKey, $accessToken)
	{
		$this->settings->persist(array(
			'stripe_ApiPublishableKey' => $publishableKey,
			'stripe_ApiSecretKey' => $accessToken,
		));
	}

	private function getClientId()
	{
		if ($this->isTest || (defined('STRIPE_CONNECT_TEST_MODE') && STRIPE_CONNECT_TEST_MODE))
		{
			return 'ca_42DoLTDVWOKY7lznD3pEcjHC4GMPuGAO';
		}

		return 'ca_42DoeKDuxaeOegQ9Euk2CGjFGQ6kYGyo';
	}
}
<?php

/**
 * Class Payment_Stripe_Provision
 */
class Payment_Stripe_Provision
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;
	protected $isTest;

	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
		$this->isTest = defined('STRIPE_CONNECT_TEST_MODE') && STRIPE_CONNECT_TEST_MODE;
	}

	/**
	 * @param $clientId
	 * @param $email
	 * @param $url
	 * @return array|bool|mixed
	 */
	public function provision($admin)
	{
		$nameParts = explode(' ', $admin['name']);
		$firstName = $nameParts[0];
		unset($nameParts[0]);
		$lastName = implode(' ', $nameParts);

		$url = 'https://account.pinnaclecart.com/stripe-connect/provision';
		if ($this->isTest)
		{
			$url = 'https://account-staging.pinnaclecart.com/stripe-connect/provision';
		}

		$c = curl_init($url);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);

		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt(
			$c, CURLOPT_POSTFIELDS,
			http_build_query(array(
				'email' => $admin['email'],
				'url' => $this->settings->get('GlobalHttpUrl'),
				'country' => $this->settings->get('CompanyCountry'),
				'currency' => 'usd',
				'phone_number' => $this->settings->get('CompanyPhone'),
				'physical_product' => 'true',
				'business_name' => $this->settings->get('CompanyName'),
				'first_name' => $firstName,
				'last_name' => $lastName,
				'street_address' => $this->settings->get('CompanyAddressLine1'),
				'city' => $this->settings->get('CompanyCity'),
				'state' => $this->settings->get('CompanyState'),
				'zip' => $this->settings->get('CompanyZip'),
			), '', '&')
		);

		$result = curl_exec($c);

		if ($result && $result != '')
		{
			return @json_decode($result, true);
		}

		return false;
	}
}
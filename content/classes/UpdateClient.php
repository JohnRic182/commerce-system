<?php 
/**
 * @class UpdateClient
 */
class UpdateClient
{
	const updateScriptUrl = 'https://www.pinnaclecart.com/update/get_xml.php';

	var $db;
	var $xml_parser;
	var $fixes_available = false;
	var $fixes = array();
	var $error = false;
	var $new_updates = false;
	var $files = array();
	var $dirs = array();
	
	var $ssl_pem_path = null;
	
	var $path = "content/cache/update/";

	/**
	 * @param $db
	 * @param $xml_parser
	 */
	public function __construct($db, $xml_parser)
	{
		$this->db = $db;
		$this->xml_parser = $xml_parser;
		
		if (is_null($this->ssl_pem_path))
		{
			$this->ssl_pem_path = str_replace('classes', 'ssl', dirname(__FILE__));
		}
		
		return $this;
	}
	
	/**
	 * Check for updates
	 * @param string $server_url
	 * @param string $license_number
	 * @param string $version
	 * @param string $encoder
	 * @return mixed
	 */
	public function checkForUpdates($server_url, $license_number, $version, $encoder)
	{
		global $settings;

		if (function_exists('ioncube_loader_iversion'))
		{
			$liv = ioncube_loader_iversion();
			$encoder_version = sprintf("%d.%d.%d", $liv / 10000, ($liv / 100) % 100, $liv % 100);
		}
		else
		{
			$encoder_version = '';
		}

		$post_data =
			"&action=check_updates" .
			"&license_number=" . $license_number .
			"&version=" . $version .
			"&encoder=" . $encoder .
			"&encoder_version=" . $encoder_version .
			"&php_version=" . phpversion();

		$c = curl_init($server_url);
		
		if ($settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}
			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $this->ssl_pem_path);
		curl_setopt($c, CURLOPT_CAINFO, $this->ssl_pem_path.'/cacert.pem');
		$buffer = curl_exec($c);

		unset($_SESSION["admin_update_fixes"]);
		
		if (curl_errno($c)!=0)
		{
			$this->error = curl_error($c);
			curl_close($c);
			return false;
		}
		else
		{	
			$xml = $this->xml_parser->parse($buffer);
			if (@$xml["update"][0]["fixes_count"][0] > 0)
			{
				$this->fixes_available = true;
				$fixes = array();
				for ($i=0; $i<count($xml["update"][0]["fix"]); $i++)
				{
					$download_url = $xml["update"][0]["fix"][$i]["url"][0];
					$parsed_url = parse_url($download_url);
					$parsed_path = pathinfo($parsed_url["path"]);
					$file_name = $this->path.$parsed_path["basename"];
					$installed = is_file($this->path."installed-".$parsed_path["basename"]);
					if (!$installed)
					{
						$this->new_updates = true;
					}
										
					$fixes[$xml["update"][0]["fix"][$i]["id"][0]] = array(
						"id" => $xml["update"][0]["fix"][$i]["id"][0],
						"name" => $xml["update"][0]["fix"][$i]["name"][0],
						"description" => $xml["update"][0]["fix"][$i]["description"][0],
						"download_url" => $xml["update"][0]["fix"][$i]["url"][0],
						"size" => $xml["update"][0]["fix"][$i]["size"][0],
						"file_name" => $file_name,
						"base_name" => $parsed_path["basename"],
						"installed" => $installed
					);
				}
				$_SESSION["admin_update_fixes"] = $fixes;
				$this->fixes = $fixes;
				curl_close($c);
				return $fixes;
			}
			else
			{
				$this->fixes_available = false;
				curl_close($c);
				return false;
			}
		}
	}
	
	/**
	 * Returns saved fixes
	 * @return mixed
	 */
	public function getFixesFromSession()
	{
		$this->fixes = $_SESSION["admin_update_fixes"];
		return $this->fixes;
	}
	
	
	/**
	 * Returns list of file, recursive
	 * @param $dir
	 */
	public function scanDir($dir)
	{
		$this->dirs[] = $dir;
		$d = dir($dir);
		while (false !== ($entry = $d->read()))
		{
			if ($entry != "." && $entry != "..")
			{
				if (is_dir($dir."/".$entry))
				{
					$this->scanDir($dir."/".$entry);
				}
				elseif (is_file($dir."/".$entry))
				{
					$this->files[] = $dir."/".$entry;
				}
			}
		}
		$d->close();
	}
	
	/**
	 * Execute update code
	 * @return unknown_type
	 */
	public function runCode()
	{
		global $settings;

		$messages = array();
		if (file_exists($this->path."update.php"))
		{
			@include($this->path."update.php");
			@unlink($this->path."update.php");

			@unlink($settings['GlobalServerPath'].'/app_maintenance.htm');

			return $messages;
		}
		return false;
	}
	
	/**
	 * Setup fix
	 * @param $fix_id
	 * @return boolean
	 */
	public function setupFix($fix_id)
	{
		global $settings;

		@copy($settings['GlobalServerPath'].'/content/engine/app_maintenance.htm', $settings['GlobalServerPath'].'/app_maintenance.htm');

		//check is content/cache/update is writable
		if (!is_writable($this->path))
		{
			$this->error = "Cannot process with update. 'content/cache/update' folder is not writable. Please set write permissions.";

			@unlink($settings['GlobalServerPath'].'/app_maintenance.htm');

			return false;
		}
		
		//create folder for extracting
		if (!mkdir("content/cache/update/".$fix_id, 0777, true))
		{
			$this->error = "Cannot process with update. Temporary folder 'content/cache/update/".$fix_id."' is not created.";

			@unlink($settings['GlobalServerPath'].'/app_maintenance.htm');

			return false;
		}
		
		$download_url = $this->fixes[$fix_id]["download_url"];
		$file_name = $this->fixes[$fix_id]["file_name"];

		@set_time_limit(300);

		$c = curl_init($download_url);
		
		if ($settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}
			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}
		
		curl_setopt($c, CURLOPT_HTTPGET, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($c, CURLOPT_HEADER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $this->ssl_pem_path);
		curl_setopt($c, CURLOPT_CAINFO, $this->ssl_pem_path.'/cacert.pem');
		$buffer = curl_exec($c);
		
		if (curl_errno($c) > 0)
		{
			$this->error = curl_error($c);
			curl_close($c);

			@unlink($settings['GlobalServerPath'].'/app_maintenance.htm');

			return false;
		}
		else
		{
			curl_close($c);
			//check headers
			$content_size = $this->fixes[$fix_id]["size"];
			$headers = substr($buffer, 0, strlen($buffer) - $content_size);
			
			$content = substr($buffer, strlen($buffer) - $content_size, $content_size);
			
			//@unlink($file_name);
			$f = @fopen($file_name, "w+");
			if ($f)
			{
				fputs($f, $content, strlen($content));
				fclose($f);
			}
			else
			{
				$this->error = "Cannot write destination file " . $file_name . ". Please check free space and permissions.";

				@unlink($settings['GlobalServerPath'].'/app_maintenance.htm');

				return false;
			}
			
			//extract archive
			$path = $settings["GlobalServerPath"];
			$a = array();
			$files_extracted = false;
			
			$parsed_file_name = pathinfo($file_name);
			
			if (strtolower($parsed_file_name["extension"]) == "xml")
			{
				$f = fopen($file_name, "r");
				if ($f)
				{
					$s = fread($f, filesize($file_name));
					fclose($f);

					$xml = $this->xml_parser->parse($s);

					if (isset($xml["pcupdate:package"][0]["files"][0]))
					{
						foreach ($xml["pcupdate:package"][0]["files"][0]["file"] as $i=>$file)
						{
							$filepath = "content/cache/update/".$fix_id."/".$file["path"][0];
							$parsed_filepath = pathinfo($filepath);
							
							//code below to resolve php 5.0.4 bug
							if ($parsed_filepath["dirname"] != "" && !is_dir($parsed_filepath["dirname"]))
							{
								$a = explode("/", $parsed_filepath["dirname"]);
								$b = "";
								for ($i=0; $i<count($a); $i++)
								{
									$b = $b.($b == ""? "" : "/").$a[$i];
									if (!is_dir($b))
									{
										mkdir($b, 0777, true);
									}
								}
							}
							
							$filesize = $file["size"][0];
							$compressed = $file["compressed"][0];
							$checksum = $file["checksum"][0];
							$checksum = $file["checksum"][0];
							$content = $file["content"][0];
							
							if ($compressed == "1")
							{
								$body = gzinflate(urldecode($content));
							}
							else
							{
								$body = urldecode($content);
							}
							$fsave = fopen($filepath, "w+");
							fputs($fsave, $body, strlen($body));
							fclose($fsave);
						}
					}
					$files_extracted = true;
				}
			}	
			elseif (strpos(strtolower($_SERVER["SERVER_SOFTWARE"]), "win32"))
			{
				//first step - extact tar archive from tar.gz
				$command_line = 
					$path."/content/admin/7zip/7z.exe ".
					"x \"".$path."/".$file_name."\" -aoa -o\"".$path."/content/cache/update\"";
				$result = exec($command_line, $a);
				//second step - extract files from tar archive
				//echo "<hr>";
				$command_line = 
					$path."/content/admin/7zip/7z.exe ".
					"x \"".$path."/".substr($file_name, 0, strlen($file_name)-3)."\" -aoa -o\"".$path."/content/cache/update/".$fix_id."/\"";
				$result = exec($command_line, $a);
				@unlink(substr($file_name, 0, strlen($file_name)-3));
				$files_extracted = true;
				//backup installed file
				//@rename($file_name, "content/cache/update/installed-". $this->fixes[$fix_id]["base_name"]);
			}
			else
			{
				$command_line =
					"tar -xvzf \"".$path."/".$file_name."\" -C\"".$path."/content/cache/update/".$fix_id."/\" --overwrite";
				$result = exec($command_line, $a);
				$files_extracted = true;
			}

			if ($files_extracted)
			{
				$this->dirs = array();
				$this->files = array();
				$this->scanDir("content/cache/update/".$fix_id);
				$l = strlen("content/cache/update/".$fix_id."/");

				$write_errors = "";

				for ($i=0; $i<count($this->files); $i++)
				{
					//check are files writable
					$destination_file_name = substr($this->files[$i], $l, strlen($this->files[$i]) - $l);
					if (file_exists($destination_file_name) && !is_writeable($destination_file_name))
					{
						$write_errors.="<br>".$destination_file_name;
					}
				}

				if ($write_errors != "")
				{
					$this->error = "Cannot update file(s):</b>".$write_errors."<br><b>Please check file(s) permissions";
					for ($i=0; $i<count($this->files); $i++)
					{
						unlink($this->files[$i]);
					}
					unlink($file_name);
					arsort($this->dirs);

					foreach ($this->dirs as $key => $value)
					{
						rmdir($value);
					}

					@unlink($settings['GlobalServerPath'].'/app_maintenance.htm');

					return false;
				}
				else
				{
					for ($i=0; $i<count($this->files); $i++)
					{
						//check are files writable
						$destination_file_name = substr($this->files[$i], $l, strlen($this->files[$i]) - $l);
						//@unlink($destination_file_name);
						$pathinfo = pathinfo($destination_file_name);
						$this->mkdir_recursive($pathinfo['dirname'], 0777, true);
						//rename($this->files[$i], $destination_file_name);

						file_put_contents($destination_file_name, file_get_contents($this->files[$i]));
						@unlink($this->files[$i]);
					}
					@rename($file_name, 'content/cache/update/installed-' . $this->fixes[$fix_id]['base_name']);
					arsort($this->dirs);
					foreach ($this->dirs as $key=>$value)
					{
						rmdir($value);
					}
				}
			}

			return true;
		}
	}
	
	/**
	 * PHP 4.0 compatibility
	 *
	 * @return void
	 * @author Sebastian Nievas
	 */
	protected function mkdir_recursive($dir)
	{
		if ($dir == '') return;

		$new_directories = array();
		while (!is_dir($dir))
		{
			$new_directories[] = $dir;
			$dir = substr($dir, 0, strrpos($dir, '/'));
		}

		foreach (array_reverse($new_directories) as $dir_name)
		{
			@mkdir($dir_name, 0777);
		}
	}
}


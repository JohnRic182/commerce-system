<?php

/**
* 
*/
class WholesaleCentral
{
	protected $username = null;
	protected $password = null;
	protected $endpoint = 'ezfeed.wholesalecentral.com';
	protected $endpointPort = 21;
	
	
	protected $db = null;
	
	const AUTO_FEED_ID = 'f5eX2';
	
	public function __construct($username, $password, DB $db = null, $endpoint = null)
	{
		$this->username = $username;
		$this->password = $password;
		$this->db = $db;
		if (!is_null($endpoint)) $this->endpoint = $endpoint;
	}
	
	public function setPassword($password)
	{
		$this->password = $password;
	}
	
	public function setUsername($username)
	{
		$this->username = $username;
	}
	
	public function setEndpoint($endpoint)
	{
		$this->endpoint = $endpoint;
	}
	
	public function setEndpointPort($port)
	{
		$this->endpointPort = $port;
	}
	
	public function generateDataFeed($product_data, $filename, $auto = false)
	{
		if (($fp = fopen($filename, 'w')) === FALSE) throw new Exception('Could not open '.$filename.' to generate data feed. Please check permissions and try again.');
		
		foreach ($product_data as $data)
		{
			$feedData = '';
			
			$feedData .= 
				substr($this->sanitizeData($data['product_url']), 0, 200)."\t".
				substr($this->sanitizeData($data['product_name']), 0, 100)."\t".
				substr($this->sanitizeData(html_entity_decode($data['product_description'])), 0, 300)."\t".
				substr($this->sanitizeData($data['image_url']), 0, 200)."\t".
				substr($this->sanitizeData($data['product_category']), 0, 50)."\t".
				substr($this->sanitizeData($data['product_price']), 0, 200);
				
			if ($auto) $feedData .= "\t".self::AUTO_FEED_ID;
			$feedData .= "\n";
			
			fwrite($fp, $feedData, strlen($feedData));
		}
		
		return true;
	}
	
	public function sanitizeData($data)
	{
		return preg_replace('/  /',' ', preg_replace('/[\r\n\t]/', ' ', $data));
	}
	
	/**
	 * Sends the data feed to WholesaleCentral.com's server via FTP
	 *
	 * @return void
	 */
	public function 	sendDataFeed($filename)
	{
		if (FALSE === ($connection = @ftp_connect($this->endpoint, $this->endpointPort)))
		{
			throw new Exception('Could not establish FTP connection');
		}
		if (FALSE === ($loginResult = @ftp_login($connection, $this->username, $this->password)))
		{
			throw new Exception('Your EZFeed FTP credentials appear to be invalid. If the problem persists please contact wholesalecentral.com');
		}
		
		ftp_pasv($connection, false);
		
		$remoteFilename = 'remote-wholesalecentral.csv';
		if (!@ftp_put($connection, $remoteFilename, $filename, FTP_ASCII))
		{
			throw new Exception('There was a problem uploading the ezFeed file. Please try again.');
		}
		
		// Close the connection
		ftp_close($connection);
		
		return true;
	}
}

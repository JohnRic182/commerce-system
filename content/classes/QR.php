<?php

require_once dirname(dirname(__FILE__)).'/vendors/qr/qrlib.php';

/**
 * 
 */
class QR 
{
	private $_error_check_level = "L"; 
	private $_default_pixel_size = 4;
	private $_default_margin_size = 2;
	private $_file_cache_path = "content/cache/qr/";
	private $_file_cache_url = "content/cache/qr/";
	private $_settings = null;
	private $_last_rendered_file = false;
	
	/**
	 * Class constructor
	 * @param array $settings 
	 */
	public function __construct(&$settings)
	{
		$this->_settings = $settings;
		//$this->_file_cache_path = dirname(dirname(__FILE__)).'/cache/qr';

		$this->_error_check_level = $settings["qr_error_check"];
		$this->_default_pixel_size = $settings["qr_default_size"];
		return $this;
	}
	
	/**
	 * Set QR code images path
	 * @param string $path 
	 */
	public function setFileCachePath($path)
	{
		$this->_file_cache_path = $path;
	}
	
	/**
	 * Return QR code images path
	 * @return string 
	 */
	public function getFileCachePath()
	{
		return $this->_file_cache_path;
	}
	
	/**
	 * Set QR code images url
	 * @param string $url 
	 */
	public function setFileCacheUrl($url)
	{
		$this->_file_cache_url = $url;
	}
	
	/**
	 * Return QR code images url
	 * @return string 
	 */
	public function getFileCacheUrl()
	{
		return $this->_file_cache_url;
	}
	
	/**
	 * Returns last rendered file path
	 * @return mixed
	 */
	public function getLastRenderedFile()
	{
		return $this->_last_rendered_file;
	}
	
	/**
	 * Generate QR Code
	 * @param char $type // code type prefix: example a=add product to a cart, p=go to product page, c=category page, u=custom
	 * @param string $data // code data
	 * @param int $id // data entry id
	 * @param boolean $force
	 * @param mixed $error_check_level // L,M.H,Q
	 * @param mixed $default_pixel_size // 4..12
	 * @param int $image_type // IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_PNG
	 * @return URL
	 */
	public function render($type, $data, $id, $force = false, $error_check_level = false, $default_pixel_size = false, $image_type = IMAGETYPE_PNG)
	{
		// get file name and path
		$fileName = $this->getFileName($type, $id, $error_check_level, $default_pixel_size, $image_type);
		$fullFileName = $this->getFileCachePath().$fileName;
		// if forced - delete current
		if ($force && is_file($fullFileName)) unlink($fullFileName);
		
		// if file does noe exist - create a new one
		if (!is_file($fullFileName))
		{
			switch ($image_type)
			{
				case IMAGETYPE_PNG : QRcode::png($data, $fullFileName, $error_check_level, $default_pixel_size, $this->_default_margin_size); break;
				case IMAGETYPE_JPEG : QRcode::jpg($data, $fullFileName, $error_check_level, $default_pixel_size, $this->_default_margin_size); break;
				case IMAGETYPE_GIF : QRcode::gif($data, $fullFileName, $error_check_level, $default_pixel_size, $this->_default_margin_size); break;
			}	
		}
		
		$this->_last_rendered_file = $fullFileName;
		
		return $this->getFileUrl(
			$type, $id, 
			$error_check_level ? $error_check_level : $this->_error_check_level, 
			$default_pixel_size ? $default_pixel_size : $this->_default_pixel_size,
			$image_type
		);
	}
	
	/**
	 * Render QR Code file with product URL and returns its URL
	 * @param int $product_id
	 * @param boolean $force
	 * @param char $error_check_level
	 * @param int $default_pixel_size
	 * @param mixed $campaign_id
	 */
	public function renderProductUrlCode($data, $product_id, $force = false, $error_check_level = false, $default_pixel_size = false, $image_type = IMAGETYPE_PNG, $campaign_id = false)
	{
		return $this->render(
			"p", $data, $product_id.($campaign_id ? "-".$campaign_id : ""), $force, 
			$error_check_level ? $error_check_level : $this->_error_check_level, 
			$default_pixel_size ? $default_pixel_size : $this->_default_pixel_size,
			$image_type
		);
	}
	
	/**
	 * Render QR Code file with product-add-to-cart URL and returns its URL
	 * @param int $product_id
	 * @param boolean $force
	 * @param char $error_check_level
	 * @param int $default_pixel_size
	 * @param mixed $campaign_id
	 */
	public function renderProductAddToCartCode($data, $product_id, $force = false, $error_check_level = false, $default_pixel_size = false, $image_type = IMAGETYPE_PNG, $campaign_id = false)
	{
		return $this->render(
			"a", $data, $product_id.($campaign_id ? "-".$campaign_id : ""), $force, 
			$error_check_level ? $error_check_level : $this->_error_check_level, 
			$default_pixel_size ? $default_pixel_size : $this->_default_pixel_size,
			$image_type
		);
	}
	
	/**
	 * Render QR Code file with category URL and returns its URL
	 * @param int $category_id
	 * @param boolean $force
	 * @param char $error_check_level
	 * @param int $default_pixel_size 
	 */
	public function renderCategoryUrlCode($data, $category_id, $force = false, $error_check_level = false, $default_pixel_size = false, $image_type = IMAGETYPE_PNG, $campaign_id = false)
	{
		return $this->render(
			"c", $data, $category_id.($campaign_id ? "-".$campaign_id : ""), $force, 
			$error_check_level ? $error_check_level : $this->_error_check_level, 
			$default_pixel_size ? $default_pixel_size : $this->_default_pixel_size,
			$image_type
		);
	}
	
	/**
	 * Render custom QR Code and return its URL
	 * @param string $data 
	 * @param boolean $force
	 * @param char $error_check_level
	 * @param int $default_pixel_size 
	 */
	public function renderCustomCode($data, $force = false, $error_check_level = false, $default_pixel_size = false, $image_type = IMAGETYPE_PNG)
	{
		return $this->render(
			"u", $data, md5($data), $force, 
			$error_check_level ? $error_check_level : $this->_error_check_level, 
			$default_pixel_size ? $default_pixel_size : $this->_default_pixel_size,
			$image_type
		);
	}
	
	/**
	 * Get QR Code file name
	 * @param type $type
	 * @param type $pid
	 * @param type $error_check_level
	 * @param type $default_pixel_size
	 * @return type 
	 */
	public function getFileName($type, $id, $error_check_level = false, $default_pixel_size = false, $image_type = IMAGETYPE_PNG)
	{
		$types = array(
			IMAGETYPE_PNG => "png",
			IMAGETYPE_JPEG => "jpg",
			IMAGETYPE_GIF => "gif",
		);
		return
			$type."-".
			$id."-".
			($error_check_level ? $error_check_level : $this->_error_check_level)."-".
			($default_pixel_size ? $default_pixel_size : $this->_default_pixel_size).".".$types[$image_type];
	}
	
	/**
	 * Return QR Code file URL
	 * @param type $pid
	 * @param type $error_check_level
	 * @param type $default_pixel_size
	 * @return type 
	 */
	public function getFileUrl($type, $id, $error_check_level = false, $default_pixel_size = false, $image_type = IMAGETYPE_PNG)
	{
		$types = array(
			IMAGETYPE_PNG => "png",
			IMAGETYPE_JPEG => "jpg",
			IMAGETYPE_GIF => "gif",
		);
		return
			$this->getFileCacheUrl().
			$type."-".
			$id."-".
			($error_check_level ? $error_check_level : $this->_error_check_level)."-".
			($default_pixel_size ? $default_pixel_size : $this->_default_pixel_size).".".$types[$image_type];
	}
}
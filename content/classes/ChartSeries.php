<?php

/**
* 
*/
class ChartSeries
{
	protected $title;
	protected $data = array();
	protected $xAxisLabel;
	protected $xAxisFormat = '%d';
	protected $xAxisMode = null;
	protected $yAxisLabel;
	protected $yAxisFormat = '%d';
	protected $yAxisMode = null;
	
	const AXIS_MODE_DEFAULT = 0;
	const AXIS_MODE_TIME = 2;
	
	
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function addData($key, $value, $label = '')
	{
		$this->data[$key] = array(
			'value' => $value,
			'label' => $label
		);
	}
	
	public function getData()
	{
		return $this->data;
	}
	
	public function setXAxisFormat($format)
	{
		$this->xAxisFormat = $format;
	}
	
	public function getXAxisFormat()
	{
		return $this->xAxisFormat;
	}
	
	public function setYAxisFormat($format)
	{
		$this->yAxisFormat = $format;
	}
	
	public function getYAxisFormat()
	{
		return $this->yAxisFormat;
	}
	
	public function setXAxisLabel($label)
	{
		$this->xAxisLabel = $label;
	}
	
	public function getXAxisLabel()
	{
		return $this->xAxisLabel;
	}
	
	public function setYAxisLabel($label)
	{
		$this->yAxisLabel = $label;
	}
	
	public function getYAxisLabel()
	{
		return $this->yAxisLabel;
	}
	
	public function setXAxisMode($mode)
	{
		$this->xAxisMode = ($mode == self::AXIS_MODE_TIME) ? self::AXIS_MODE_TIME : self::AXIS_MODE_DEFAULT;
	}
	
	public function getXAxisMode()
	{
		return $this->xAxisMode;
	}
	
	public function setYAxisMode($mode)
	{
		$this->yAxisMode = ($mode == 'time') ? $mode : null;
	}
	
	public function getYAxisMode()
	{
		return $this->yAxisMode;
	}
}

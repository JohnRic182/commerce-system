<?php

class TaxRates implements TaxRatesInterface
{
	protected $taxRateRepository;
	protected $taxRates = null;

	protected $lastTaxRateCountryId = null;
	protected $lastTaxRateStateId = null;
	protected $lastTaxRateUserLevel = null;

	protected $displayPricesWithTax = false;

	public function __construct(DataAccess_TaxRateRepositoryInterface $taxRateRepository, $settings)
	{
		$this->taxRateRepository = $taxRateRepository;
	}

	/**
	 * Returns an array of Model_TaxRate objects
	 *
	 * @param int $country the country id, or false to use the default tax country
	 * @param int $state the state id, or false to use the default tax state
	 * @param int $userLevel the user level
	 * 
	 * @return array the array of tax rates for the location
	 */
	public function getTaxRates($taxCountryId, $taxStateId, $userLevel)
	{
		// simple caching
		if (!is_null($this->taxRates) && $this->lastTaxRateCountryId == $taxCountryId && $this->lastTaxRateStateId == $taxStateId	&& $this->lastTaxRateUserLevel == $userLevel)
		{
			return $this->taxRates;
		}

		$tax_rates = array('0' => new Model_TaxRate(0));

		$_tax_rates = $this->taxRateRepository->loadTaxRates($taxCountryId, $taxStateId, $userLevel);

		if (count($_tax_rates) > 0)
		{
			foreach ($_tax_rates as $tax_rate)
			{
				if ($tax_rate['display_with_tax'])
				{
					$this->displayPricesWithTax = true;
				}

				$classId = $tax_rate['class_id'];

				if (!array_key_exists($classId, $tax_rates))
				{
					$tax_rates[$classId] = new Model_TaxRate($classId);
				}

				/* @var Model_TaxRate $taxRateObj */
				$taxRateObj = $tax_rates[$classId];
				$taxRateObj->addTaxRate($tax_rate['rate_priority'], $tax_rate['tax_rate'], $tax_rate['rate_description']);
			}
		}

		$this->lastTaxRateCountryId = $taxCountryId;
		$this->lastTaxRateStateId = $taxStateId;
		$this->lastTaxRateUserLevel = $userLevel;

		$this->taxRates = $tax_rates;

		return $this->taxRates;
	}

	/**
	 * Check if the tax class has a tax rate
	 * 
	 * @param int $classId the tax class id
	 * 
	 * @return boolean true when the tax class has a tax rate
	 */
	public function hasTaxRate($classId)
	{
		return !is_null($this->taxRates) && array_key_exists($classId, $this->taxRates);
	}

	/**
	 * Returns the tax amount calculation for the amount
	 * 
	 * @param  int $classId the tax class id
	 * @param  int $amount  the amount
	 * @return float        the calculated tax amount
	 */
	public function calculateTax($classId, $amount)
	{
		if (is_null($this->taxRates))
		{
			return 0;
		}

		if ($this->hasTaxRate($classId))
		{
			$taxRate = $this->taxRates[$classId];

			return $taxRate->calculateTax($amount);
		}

		return 0;
	}

	public function getDisplayPricesWithTax()
	{
		return $this->displayPricesWithTax;
	}

	/**
	 * Returns the tax rate amount
	 * Note: when $fraction is true the value will be in 0..1, when $fraction is false the value will be in 0..100
	 * 
	 * @param  int  $classId     the tax class id
	 * @param  boolean $fraction when true the return value will be between 0...1, when false the return value will be between 0...100
	 * @return float             the tax rate value for the tax class
	 */
	public function getTaxRate($classId, $fraction = true)
	{
		if (is_null($this->taxRates))
		{
			return 0;
		}

		if ($this->hasTaxRate($classId))
		{
			$taxRate = $this->taxRates[$classId];
			return $taxRate->getTaxRate() * ($fraction ? 1 : 100);
		}

		return 0;
	}

	/**
	 * Returns the tax rate description for the tax class
	 * 
	 * @param  int    $classId the tax class id
	 * @return string          the tax rate description
	 */
	public function getTaxRateDescription($classId)
	{
		if (is_null($this->taxRates))
		{
			return '';
		}

		if ($this->hasTaxRate($classId))
		{
			$taxRate = $this->taxRates[$classId];

			return $taxRate->getDescription();
		}

		return '';
	}
}
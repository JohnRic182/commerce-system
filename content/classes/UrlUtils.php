<?php

/**
 * Class UrlUtils
 */
class UrlUtils
{
	/**
	 * @var bool
	 */
	protected static $index;
	protected static $useRewrite;
	protected static $httpUrl;
	protected static $httpsUrl;
	protected static $categoryPaginatorTemplate;
	protected static $urlGlue;

	/**
	 * @param $settings
	 */
	public static function init(&$settings)
	{
		if (!defined('DESIGN_MODE')) define('DESIGN_MODE', false);

		self::$index = $settings['INDEX'];
		self::$useRewrite = $settings['USE_MOD_REWRITE']=='YES';
		self::$httpUrl = $settings['GlobalHttpUrl'];
		self::$httpsUrl = $settings['GlobalHttpsUrl'];
		self::$categoryPaginatorTemplate = $settings['SearchURLCategoryPaginatorTemplate'];
		self::$urlGlue = self::$urlGlue;
	}

	/**
	 * @param $url
	 * @param $pid
	 * @param int $parent
	 * @param bool $force_http
	 * @return string
	 */
	public static function getProductUrl($url, $pid, $parent = 0, $force_http = false)
	{
		if (self::$useRewrite)
		{
			return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/' . $url;
		}
		else
		{
			return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/' . self::$index . '?p=product&amp;id=' . $pid;
		}
	}

	/**
	 * @param $url
	 * @param $cid
	 * @param bool $page
	 * @param bool $force_http
	 * @param string $filtersQueryString
	 * @return string
	 */
	public static function getCatalogUrl($url, $cid, $page = false, $force_http = false, $filtersQueryString = '')
	{
		if (self::$useRewrite)
		{
			if ($page && intval($page) != 1)
			{
				$pageNum = self::$categoryPaginatorTemplate;

				if (($l = strlen($url)) > 0)
				{
					if ($url[$l-1] == '/')
					{
						$url = substr($url, 0, $l-1) . $pageNum . '/';
					}
					else
					{
						$p = pathinfo($url);
						if (isset($p['extension']) && $p['extension'] != '')
						{
							$url = substr($url, 0, $l - strlen($p['extension'])-1) . $pageNum . '.' . $p['extension'];
						}
						else
						{
							$url.=$pageNum;
						}
					}
					$url = str_replace('%PageNumber%', $page, $url);
				}
			}

			return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/' . $url . (!empty($filtersQueryString) ? '?' . $filtersQueryString : '');
		}
		else
		{
			return
				(defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/' . self::$index . '?p=catalog&amp;' .
				(in_array($cid, array('hot', 'new', 'search')) ? ('mode=' . $cid) : ('parent=' . $cid)) . '&amp;pg=' . ($page ? $page : 1) . (!empty($filtersQueryString) ? '&amp;' . $filtersQueryString : '');
		}
	}

	/**
	 * @param $url
	 * @param $mid
	 * @param bool $page
	 * @param bool $force_http
	 * @return string
	 */
	public static function getManufacturerUrl($url, $mid, $page = false, $force_http = false)
	{
		if (self::$useRewrite)
		{
			return
				(defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . "/" . self::$index . "?p=catalog&amp;mode=manufacturer&mid=" . $mid;
		}
		else
		{
			return Seo::getManufacturerSEOUrl($url, $mid, $page, $force_http);
		}
	}

	/**
	 * @param $page_id
	 * @param string $url
	 * @param bool $force_http
	 * @return string
	 */
	public static function getPageUrl($page_id, $url = "", $force_http = false)
	{
		if (self::$useRewrite)
		{
			return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/' . $url;
		}
		else
		{
			return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/' . self::$index . '?p=page&amp;page_id=' . $page_id;
		}
	}

	/**
	 * @param bool $force_http
	 * @param string $page_name
	 * @return string
	 */
	public static function getCommonUrl($force_http = false, $page_name)
	{
		if (self::$useRewrite)
		{
			$page_name = str_replace('_', '', $page_name);
			return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http  ? self::$httpsUrl : self::$httpUrl) . '/' . $page_name;
		}
		else
		{
			return (defined('DESIGN_MODE') && DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/' . self::$index . '?p='. $page_name;
		}
	}

	/**
	 * @param $page
	 * @param bool $force_http
	 * @return string
	 */
	public static function getCanonicalUrl($page, $force_http = false)
	{
		if (!isset($_REQUEST['p']) || $_REQUEST['p'] == 'home' || $_REQUEST['p'] == '')
		{
			return (DESIGN_MODE && !$force_http ? self::$httpsUrl : self::$httpUrl) . '/';
		}

		return self::currentPageURL(true);
	}

	/**
	 * @param bool $encodeParameters
	 * @return string
	 */
	public static function currentPageURL($encodeParameters = false)
	{
		$pageURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
		$pageURL .= $_SERVER['SERVER_PORT'] != '80' ? $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'] : $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

		if ($encodeParameters)
		{
			$pageURL = self::encodeUrlParameters($pageURL, true, true);
		}

		return $pageURL;
	}

	/**
	 * @param $pageUrl
	 * @param bool $useGetParams
	 * @param bool $removeUrlParam
	 * @return string
	 */
	public static function encodeUrlParameters($pageUrl, $useGetParams = true, $removeUrlParam = false)
	{
		$parts = parse_url($pageUrl);

		$url = $parts['scheme'].'://'.$parts['host'];
		$url .= isset($parts['port']) ? ':'.$parts['port'] : '';
		$url .= isset($parts['path']) ? $parts['path'] : '';

		$qs = '';
		if ($useGetParams)
		{
			$getCopy = $_GET;

			if ($removeUrlParam) unset($getCopy['url']);

			$qs = http_build_query($getCopy);
		}
		else if (isset($parts['query']))
		{
			$qsParts = array();
			parse_str($parts['query'], $qsParts);

			$qs = http_build_query($qsParts);
		}

		if (strlen($qs) > 0)
		{
			$url .= '?'.ltrim(rtrim($qs, '&'), '?');
		}

		return $url;
	}

	/**
	 * @return bool
	 */
	public static function getBackUrl()
	{
		global $_SERVER, $_SESSION, $_POST;
		if (isset($_POST["backurl"]) && $_POST["backurl"] != "")
		{
			return $_POST["backurl"];
		}
		elseif (isset($_SESSION["HTTP_REFERER"]) && $_SESSION["HTTP_REFERER"] != "")
		{
			return $_SESSION["HTTP_REFERER"];
		}
		elseif (isset($_SERVER["HTTP_REFERER"]) && $_SERVER["HTTP_REFERER"] != "")
		{
			return $_SERVER["HTTP_REFERER"];
		}
		return false;
	}

	public static function lastSegment($url = '')
	{
		if (!empty($url) && !filter_var($url, FILTER_VALIDATE_URL) === false)
		{
			return basename(parse_url($url, PHP_URL_PATH));
		}
		else
		{
			return '';
		}
	}
}
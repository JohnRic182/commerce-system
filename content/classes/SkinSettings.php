<?php

if (!defined("ENV")) exit();

class SkinSettings
{
	public static $settings;
	public static $db;
	public static $_init = null;
	public static $view = null;
	public static $designmode = null;

	public static function init()
	{
		if (is_null(self::$view))
		{
			global $settings, $db;
			self::$settings = $settings;
			self::$db = $db;
			if (is_null(self::$view)) self::$view = Ddm_View::getInstance();
			if (
				!is_dir('content/cache/skins/'.escapeFileName($settings['DesignSkinName'])) || 
				(isset($_SESSION['admin_auth_id']) && $_SESSION['admin_auth_id'] && isset($_SESSION['admin_design_mode']) && $_SESSION['admin_design_mode'])
				|| (defined("DEVMODE") && DEVMODE)
			)
			{
				view()->assemble(
					'content'.DS.'cache'.DS.'skins'.DS.escapeFileName($settings['DesignSkinName']).DS, 
					null, true
				);
			}

			if (isset($_POST) && array_key_exists('skinName', $_POST)) self::setSkin($_POST['skinName']);
		}
	}
	
	/**
	 *
	 * @return array Response
	 */
	public static function setSkin($skin_name)
	{
		// Make sure the skin exists
		if (is_dir('content'.DS.'skins'.DS.escapeFileName($skin_name)))
		{
			// read in the skin xml file
			$template_xml = simplexml_load_string(file_get_contents('content'.DS.'skins'.DS.escapeFileName($skin_name).DS.'skin.xml'), 'SimpleXMLElement', LIBXML_NOCDATA);

			// Update the current selected skin in settings
			self::$db->reset();
			self::$db->assignStr('value', $skin_name);
			self::$db->update(DB_PREFIX.'settings', "WHERE name='DesignSkinName'");
			self::$settings['DesignSkinName'] = $skin_name;

			// Set the Site Layout Width to the default as defined in skin xml
			$layoutSiteWidth = $template_xml->xpath("/skin/settings/option[@name='LayoutSiteWidth']");
			$layoutSiteWidthValue = (count($layoutSiteWidth) && $layoutSiteWidth !== false)
			? $layoutSiteWidth[0]->attributes()->value
			: self::$settings['LayoutSiteWidthDefault'];

			self::$db->reset();
			self::$db->assignStr('value', $layoutSiteWidthValue);
			self::$db->update(DB_PREFIX.'settings', "WHERE name='LayoutSiteWidth' LIMIT 1");
			self::$settings['LayoutSiteWidth'] = $layoutSiteWidthValue;

			// check if we need to override the current layout settings with skin defaults,
			// otherwise, do nothing
			if (array_key_exists('layoutConfig', $_POST) && $_POST['layoutConfig'] == 'default')
			{
				if (is_null(self::$designmode)) self::$designmode = new Ddm_DesignMode(self::$settings);
				// restore the layout if necessary
				self::$designmode->restoreLayout();
				// restore any skin changes
				self::$designmode->restoreSkin();
			}

			self::$view->assemble('content'.DS.'cache'.DS.'skins'.DS.escapeFileName(self::$settings['DesignSkinName']).DS, array(
				'content'.DS.'engine'.DS.'design'.DS,
				'content'.DS.'skins'.DS.escapeFileName(self::$settings['DesignSkinName']).DS,
				'content'.DS.'skins'.DS.'_custom'.DS.'skin'.DS,
			));

			return true;
		}
	}

	public static function renderAdminView($view, $vars = array())
	{
		foreach ($vars as $key => $value)
		{
			self::$view->assign($key, $value);
		}

		//TODO: Move this to its own class instead of Ddm_View, and put templates under content/admin/skins/default/templates/
		return self::$view->fetch('templates/admin/'.$view.'.html');
	}

	public static function renderSkinBrowserView()
	{
		self::$view->assign('skinsInstallable', is_writable('content'.DS.'skins') ? 'true' : 'false');
		$html = self::$view->fetch('templates/admin/skin-settings/skin-browser.html');

		return $html;
	}

	public static function renderSkinSelectionView()
	{
		// FIXME: TBD - Need to pull from some service, for now we will read
		// filestructure for subdirectories as new skins
		$templates = array();

		if (($fh = opendir('content'.DS.'skins')) !== false)
		{
			$restricted = array('.', '..', '.svn');

			while (($skin_name = readdir($fh)) !== false)
			{
				if (is_dir('content'.DS.'skins'.DS.$skin_name) && file_exists('content'.DS.'skins'.DS.$skin_name.DS.'skin.xml'))
				{
					// Read the XML skin file into the array
					$templates[$skin_name] = simplexml_load_string(file_get_contents('content'.DS.'skins'.DS.$skin_name.DS.'skin.xml'), 'SimpleXMLElement', LIBXML_NOCDATA);
				}
			}

			closedir($fh);
		}

		// For now we read current installed templates by reading the skins directory
		// and parsing the skin.xml file at the skin level root
		$skin_templates = array();
		foreach ($templates as $template_name => $template)
		{

			$activateLink = '<a class="activate" title="'.$template->name.'" href="#">Activate</a>';
			$selectedTemplate = '';
			if ($template->name == self::$settings['DesignSkinName'])
			{
				$activateLink = 'Current Skin';
				$selectedTemplate = 'currentTemplate';
			}

			$path_to_images = 'content/skins/'.$template_name.'/';

			$images = array();
			if (isset($template->images))
			{
				foreach ($template->images->children() as $image)
				{
					$image_data = array('src' => $path_to_images.escapeFileName((String) $image));
					foreach ($image->attributes() as $key => $value)
					{
						$image_data[$key] = $value;
					}
					$images[] = $image_data;
				}
			}

			$skin_templates[] = array(
				'activation_link' => $activateLink,
				'name' => htmlentities($template->name),
				'title' => $template->title,
				'author' => $template->author,
				'thumbnail_image' => $path_to_images.escapeFileName($template->thumbnail),
				'preview_image' => $path_to_images.escapeFileName($template->preview),
				'tag' => strtolower($template->name),
				'selectedTemplate' => $selectedTemplate,
				'images' => $images,
				'rel_group_name' => (count($images)) ? 'rel="group_'.$template->name.'"' : ''
				);
		}

		self::$view->assign('skin_templates', $skin_templates);

		$html = '';
		if (is_null(self::$_init))
		{
			self::$_init = true;

			$html .= self::$view->fetch('templates/admin/skin-settings/init.html');
		}
		$html .= self::$view->fetch('templates/admin/skin-settings/skin-selection.html');

		return $html;
	}
}
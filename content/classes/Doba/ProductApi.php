<?php

/**
 * Doba Product API Class
 */
class Doba_ProductApi extends Doba_Api
{	
	public $lastProductLockedFields = array();
	
	/**
	 * Returns merchants watchlists
	 * @return mixed
	 */
	public function getWatchlists()
	{
		$r = $this->call("<action>getWatchlists</action>");
		
		if ($r && isset($r['response']['watchlists']) && is_array($r['response']['watchlists']) && count($r['response']['watchlists']) > 0)
		{
			if (!isset($r['response']['watchlists']['watchlist'][0]))
			{
				$r['response']['watchlists']['watchlist'] = array($r['response']['watchlists']['watchlist']);
			}

			return $r['response']['watchlists'];
		}
		return false;
	} 
	
	/**
	 * Init doba table
	 */
	public function initDobaTable()
	{
		$this->db()->query("SHOW TABLES LIKE '".DB_PREFIX."doba_cache'");
		if (!$this->db()->moveNext())
		{
			$this->db()->query("CREATE TABLE ".DB_PREFIX."doba_cache(watchlist_id int(11) UNSIGNED NOT NULL DEFAULT 0, item_id int(11) UNSIGNED NOT NULL DEFAULT 0, data text NOT NULL, KEY(watchlist_id), KEY(item_id));");
		}
		else
		{
			$this->db()->query("DELETE FROM ".DB_PREFIX."doba_cache");
		}
	}
	
	/**
	 * Cache items in DB and return only items for browser
	 */
	public function cacheItems($watchlistId, &$products)
	{
		if (!array_key_exists(0, $products)) $products = array($products);
		
		foreach ($products as $product)
		{
			$items = isset($product['items']['item'][0]) ? $product['items']['item'] : array($product['items']['item']);

			foreach ($items as $item)
			{
				$this->db()->reset();
				$this->db()->assignStr("item_id", $item['item_id']);
				$this->db()->assignStr("watchlist_id", $watchlistId);
				$this->db()->assignStr("data", serialize(array("p"=>$product, "i"=>$item)));
				$this->db()->insert(DB_PREFIX."doba_cache");
			}
		}
	}

	/**
	 * Cache items in DB and return only items for browser
	 */
	public function cacheItemsFromFile($watchlist_id, $page = 0)
	{
		try
		{
			$filename = $this->_getWatchlistFileName($watchlist_id, $page);
			$f = fopen($filename, "r");
			$buffer = "";
			$p_start = false;
			$p_finish = false;
			$i = 0;
			while (($s = fread($f, 1024*16)) != false)
			{

				$buffer = $buffer.$s;

				while (true)
				{
					if (($p_start = strpos($buffer, "<product>")) >= 0 && ($p_finish = strpos($buffer, "</product>")) > 0)
					{
						$product = xml2array(simplexml_load_string(substr($buffer, $p_start, $p_finish - $p_start + 10)));
					
						$items = isset($product['items']['item'][0]) ? $product['items']['item'] : array($product['items']['item']);

						foreach ($items as $item)
						{
							$this->db()->reset();
							$this->db()->assignStr("item_id", $item['item_id']);
							$this->db()->assignStr("watchlist_id", $watchlist_id);
							$this->db()->assignStr("data", serialize(array("p"=>$product, "i"=>$item)));
							$this->db()->insert(DB_PREFIX."doba_cache");
						}
			
						$buffer = substr($buffer, $p_finish + 10);
					
						$i++;
					}
					else
					{
						break;
					}
				}
			}
		
			fclose($f);
			
			unlink($filename);
			
			return true;
		}
		catch (Exception $e)
		{
			echo $e->getMessage();
		}
		
		return false;

	}

	/**
	 * Get items from cache
	 *
	 * @param $watchlistId
	 * @param $page
	 * @param $limit
	 * @param bool $short
	 *
	 * @return array|bool
	 */
	public function getCachedItems($watchlistId, $page, $limit, $short = true, $import = false)
	{
		$limitStr = "";
		if (!$import)
		{
			$limitStr = " LIMIT ".((intval($page)-1) * intval($limit)).", ".intval($limit);
		}
		
		$this->db()->query("SELECT data FROM ".DB_PREFIX."doba_cache WHERE watchlist_id=".intval($watchlistId).$limitStr);
		$items = array();
		while ($this->db()->moveNext())
		{
			$c = unserialize($this->db()->col["data"]);

			$product = $c['p'];
			$item = $c['i'];

			$title = isset($product["title"]) ? $product["title"] : '';
			$name = isset($item["name"]) ? $item["name"] : '';
			
			$items[] = 
				$short ?
				array(
					"product_id" => $product["product_id"],
					"item_id" => $item["item_id"],
					"name" => $title != $name && $name != '' ? $title." - ". $name : $title,
					"description" => $product["description"],
					"price" => $item["price"],
					"msrp" => $item["msrp"],
					"ship_cost" => $product["ship_cost"],
					"drop_ship_fee" => isset($product["supplier"]["drop_ship_fee"]) ? $product["supplier"]["drop_ship_fee"] : 0.00,
					"qty_avail" => $item["qty_avail"],
					"stock" => $item["stock"],
					"thumb_url" => isset($product["images"]["image"]["thumb_url"]) ? $product["images"]["image"]["thumb_url"] : ''
				) :
				$c ;
		}
		return count($items) > 0 ? $items : false;
	}

	/**
	 * Get cached items count
	 *
	 * @param  $watchlistId
	 *
	 * @return int
	 */
	public function getCachedItemsCount($watchlistId)
	{
		$this->db()->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."doba_cache WHERE watchlist_id=".intval($watchlistId));
		$r = $this->db()->moveNext();
		return $r ? $r["c"] : 0;
	}

	/**
	 * Read cached data from database
	 *
	 * @param $itemId
	 * @param null $watchlistId
	 *
	 * @return bool|mixed
	 */
	public function getCachedItem($itemId, $watchlistId = null)
	{
		$this->db()->query("SELECT data FROM ".DB_PREFIX."doba_cache WHERE item_id=".intval($itemId).(!is_null($watchlistId) ? ' AND watchlist_id='.intval($watchlistId) : ''));
		if (($c = $this->db()->moveNext()) != false)
		{
			return unserialize($c["data"]);
		}
		return false;
	}

	/**
	 * Get item detail for preview
	 *
	 * @param $itemId
	 * @param null $watchlistId
	 *
	 * @return array
	 */
	public function getItemDetailFromCache($itemId, $watchlistId = null)
	{
		$a = $this->getCachedItem($itemId, $watchlistId);
		
		if ($a)
		{
			$product = $a['p'];

			$item = $a['i'];

			$category = "";
			if (isset($product['categories']) && is_array($product['categories']))
			{
				foreach($product['categories']['category'] as $cat) $category .= ($category == "" ? "" : " / ").$cat['name'];
			}
			else
			{
				$category = 'n/a';
			}
		
			return array(
				"name" => $product['title'] != $item['name'] ? $product['title']." - ".$item['name'] : $product['title'],
				"image_url" => isset($product['images']['image'][0]) ? $product['images']['image'][0]['url'] : $product['images']['image']['url'],
				"image_width" => isset($product['images']['image'][0]) ? $product['images']['image'][0]['image_width'] : $product['images']['image']['image_width'],
				"product_id" => $product['product_id'],
				"product_sku" => $product['product_sku'],
				"brand" => $product['brand'],
				"ship_weight" => $product['ship_weight'],
				"ship_cost" => $product['ship_cost'],
				"supplier_name" => $product['supplier']['name'],
				"supplier_notes" => $product['supplier']['notes'],
				"item_id" => $item['item_id'],
				"item_sku" => $item['item_sku'],
				"item_upc" => $item['upc'],
				"price" => $item['price'],
				"msrp" => $item['msrp'],
				"map" => $item['map'],
				"prepay_price" => $item['prepay_price'],
				"qty_avail" => $item['qty_avail'],
				"stock" => $item['stock'],
				"category" => $category,
				"description" => $product['description']
			);
		}
	}

	/**
	 * Get tmp file name for watchlist files
	 *
	 * @param $watchlist_id
	 * @param int $page
	 *
	 * @return string
	 */
	protected function _getWatchlistFileName($watchlist_id, $page=0)
	{
		return "content/cache/tmp/dw-".escapeFileName($watchlist_id."-".$page).".xml";
	}

	/**
	 * Returns watchlist products
	 *
	 * @param $watchlist_id
	 * @param int $page
	 * @param int $limit
	 *
	 * @return bool
	 */
	public function getWatchlistProducts($watchlist_id, $page=0, $limit = 5000)
	{
		$r = $this->call("<action>getProductDetail</action><page>".intval($page)."</page><limit>".$limit."</limit><watchlists><watchlist>".intval($watchlist_id)."</watchlist></watchlists>");

		if (defined('DEVMODE') && DEVMODE && defined('DOBA_DEBUG') && DOBA_DEBUG)
		{
			file_put_contents(
				dirname(dirname(__FILE__)).'/cache/log/doba-debug.txt',
				date("Y/m/d-H:i:s")." - ".print_r($r['response'],true)."\n\n\n",
				FILE_APPEND
			);
		}
		return $r ? (isset($r['response']['products']['product']) ? $r['response']['products']['product'] : false) : false;
	}

	/**
	 * Saves watchlist into file
	 *
	 * @param $watchlist_id
	 * @param int $page
	 *
	 * @return mixed
	 */
	public function saveWatchlistProducts($watchlist_id, $page=0)
	{
		return $this->call(
			"<action>getProductDetail</action><page>".intval($page)."</page><watchlists><watchlist>".intval($watchlist_id)."</watchlist></watchlists>", 
			$this->_getWatchlistFileName($watchlist_id, $page)
		);
	}
	
	/**
	 * Update item inventory
	 *
	 * @param array $item
	 * @param mixed $price
	 */
	public function updateInventory($item, $price = false)
	{
		//check is product already in database
		$this->db()->query("SELECT pid FROM ".DB_PREFIX."products WHERE product_id='".$this->db()->escape($item['item_id'])."'");
		$prod = $this->db()->moveNext();
		
		//do not add new products, only update stock / price for existing products
		if ($prod)
		{
			//check stock			
			$stock = $item['stock'];
			$qty_avail = $item['qty_avail'];
			$in_stock = $stock == "in-stock" && $qty_avail >= $this->min_stock;
			
			$this->db()->reset();
			
			if ($price)
			{
				$this->db()->assignStr("price", round($price, 2));
			}
			$this->db()->assignStr("inventory_control", "Yes");
			$this->db()->assignStr("inventory_rule", "OutOfStock");
			$this->db()->assignStr("stock", $qty_avail);
			$this->db()->assignStr("is_visible", $in_stock ? "Yes" : "No");
			$this->db()->update(DB_PREFIX."products", "WHERE pid='".intval($prod["pid"])."'");
		}
	}

	/**
	 * Returns default image URL
	 *
	 * @param $product
	 *
	 * @return bool
	 */
	public function getItemDefaultImageUrl($product)
	{
		$image_url = false;
		
		if (isset($product['images']['image']))
		{
			$images = array_key_exists(0, $product['images']['image']) ? $product['images']['image'] : array($product['images']['image']);
			$current = current($images);
			$image_url = $current['url'];
			
			foreach ($images as $image)
			{
				if (isset($image['default']) && !is_object($image['default']) && (string)$image['default']=="1")
				{
					$image_url = $image['url'];
				}
			}
		}
		
		return $image_url;
	}

	/**
	 * Returns array of secondary images URLs
	 *
	 * @param $product
	 *
	 * @return array|bool
	 */
	public function getItemSecondaryImagesUrl($product)
	{
		$images_urls = array();
		//check are images are there
		if (isset($product['images']['image']))
		{
			//find secondary images
			$images = isset($product['images']['image']['url']) ? array($product['images']['image']) : $product['images']['image'];
			foreach ($images as $image)
			{
				if (!isset($image['default']) || (isset($image['default']) && !is_object($image['default']) && (string)$image['default'] != "1"))
				{
					$images_urls[] = $image['url'];
				}
			}
		}
		
		return count($images_urls) ? $images_urls : false;
	}

	/**
	 * Inserts/update doba item
	 *
	 * @param $product
	 * @param $item
	 * @param bool $price
	 * @param bool $image
	 * @param bool $logFile
	 * @return int
	 */
	public function itemToDb($product, $item, $price = false, $image = true, $logFile = false)
	{	
		$status = 0;
		//check category
		if (isset($product['categories']['category']))
		{
			//check stock			
			//$stock = $item['stock'];
			$qty_avail = isset($item['qty_avail']) ? $item['qty_avail'] : 0;
			$in_stock = $qty_avail >= $this->min_stock;
			
			//check is product already in database
			$this->db()->query("SELECT * FROM ".DB_PREFIX."products WHERE product_id='".$this->db()->escape($item['item_id'])."'");
			$prod = $this->db()->moveNext();
			
			//do not add new products if out of stock
			if ($in_stock || $prod)
			{
				$lock_fields = $prod ? explode(",", $prod["lock_fields"]) : array();

				$lock_fields = is_array($lock_fields) ? $lock_fields : array();

				$lock_fields = normalizeProductLockFields($lock_fields);
				
				$this->lastProductLockedFields = $lock_fields;
				
				$this->db()->reset();
				$this->db()->assignStr("product_id", (string)$item['item_id']);
				
				if (!in_array("name", $lock_fields)) $this->db()->assignStr("title", str_replace('\"', '"', stripslashes((string)$product['title'] != (string)$item['name'] ? (string)$product['title']." - ".(string)$item['name'] : (string)$product['title'])));
				
				$description = isset($product['description']) && !is_object($product['description']) ? stripslashes((string)$product['description']) : "";
				if (!in_array("description", $lock_fields)) $this->db()->assignStr("description", str_replace('\"', '"', $description));
				
				$this->db()->assignStr("product_sku", isset($item['item_sku']) && !is_object($item['item_sku']) && (string)$item['item_sku'] != '' ? (string)$item['item_sku'] : ((string)$product['product_sku'] != '' ? (string)$product['product_sku'] : ''));
				$this->db()->assignStr("product_upc", isset($item['upc']) && !is_object($item['upc']) && (string)$item['upc'] != '' ? (string)$item['upc'] : '');
				
				$this->db()->assignStr("weight", isset($item['ship_weight']) && !is_object($item['ship_weight']) && $product['ship_weight'] > 0 ? $product['ship_weight'] : ($item['item_weight'] > 0 ? $item['item_weight'] : '0.00'));
				$this->db()->assignStr("dimension_width", isset($item['ship_width']) && !is_object($item['ship_width']) && $product['ship_width'] > 0 ? $product['ship_width'] : ($item['item_dim1'] > 0 ? $item['item_dim1'] : '0.00'));
				$this->db()->assignStr("dimension_height", isset($item['ship_height']) && !is_object($item['ship_height']) && $product['ship_height'] > 0 ? $product['ship_height'] : ($item['item_dim2'] > 0 ? $item['item_dim2'] : '0.00'));
				$this->db()->assignStr("dimension_length", isset($item['ship_length']) && !is_object($item['ship_length']) && $product['ship_length'] > 0 ? $product['ship_length'] : ($item['item_dim3'] > 0 ? $item['item_dim3'] : '0.00'));
				
				if (!$price)
				{
					$price = isset($item['price']) && isset($item['price']) ? $item['price'] : 0.00;
					$msrp = isset($item['msrp']) && isset($item['msrp']) ? $item['msrp'] : 0.00;
					$msrp = $msrp > 0 ? $msrp : $price;
					
					switch ($this->price_option)
					{
						case "item_price" : $price = $price; break; 
						case "item_price_inc" : $price = $price + ($price / 100) * $this->price_modifier; break;
						case "item_price_dec" : $price = $price - ($price / 100) * $this->price_modifier; break;
						case "msrp_price" : $price = $msrp; break;
						case "msrp_price_inc" : $price = $msrp + ($msrp / 100) * $this->price_modifier; break;
						case "msrp_price_dec" : $price = $msrp - ($msrp / 100) * $this->price_modifier; break;
					}
					
					if ($this->price_option99)
					{
						$price = ceil($price) - 0.01;
					}					
				}

				if ($prod && $prod["price2"] != null)
				{
					if (!in_array("sale-price", $lock_fields))
					{
						$this->db()->assignStr("price", round($price, 2));
					}
				}
				else
				{
					if (!in_array("price", $lock_fields))
					{
						$this->db()->assignStr("price", round($price, 2));
					}
				}
				
				$this->db()->assignStr("inventory_control", "Yes");
				$this->db()->assignStr("inventory_rule", "OutOfStock");
				$this->db()->assignStr("stock", $qty_avail);
				$this->db()->assignStr("is_doba", "Yes");
				$this->db()->assignStr("is_visible", $in_stock ? "Yes" : "No");
				$this->db()->assignStr("free_shipping", $product['ship_cost'] > 0 ? "No" : "Yes");
				
				//images
				if (!in_array("images", $lock_fields))
				{
					if ($image)
					{
						$image_url = $this->getItemDefaultImageUrl($product);
						if ($image_url && $image_url != "")
						{
							$this->db()->assignStr("image_location", "Web");
							$this->db()->assignStr("image_url", $image_url);
						}
					}
				}

				$this->db()->assign("updated", "NOW()");

				//add or update product
				if ($prod)
				{
					//product exists, do update
					$product_id_in_db = intval($prod["pid"]);
					$this->db()->assign("added", "NOW()");
					$this->db()->update(DB_PREFIX."products", "WHERE pid='".$product_id_in_db."'");
					$status = 1;
				}
				else
				{
					//new product, insert
					$this->db()->assign("added", "NOW()");
					$product_id_in_db = intval($this->db()->insert(DB_PREFIX."products"));
					$status = 2;
				}
			
				//check category
				$level = 1;
				$parent = 0;
				//we have to skip very first level as it is not real category
				// we are setting $key to 1 since we can safely assume from DOBA that the first category is always (Catalog) and can be ignored.
				foreach ($product['categories']['category'] as $category)
				{
					$this->db()->query('
						SELECT if (doba_mapped_category_id = 0, cid, doba_mapped_category_id) AS category_id
						FROM ' . DB_PREFIX . 'catalog
						WHERE doba_category_id=' . intval($category['id']) . ' OR key_name = "' . $this->db()->escape('doba_' . $category['id']).'"'
					);
					
					if (($cat = $this->db()->moveNext()) !=  false)
					{
						$parent = $cat['category_id'];
					}
					else
					{
						$this->db()->reset();
						$this->db()->assignStr('parent', $parent);
						$this->db()->assignStr('level', $level);
						$this->db()->assignStr('priority', 5);
						$this->db()->assignStr('is_visible', 'No');
						$this->db()->assignStr('is_doba', '1');
						$this->db()->assignStr('doba_category_id', intval($category['id']));
						$this->db()->assignStr('doba_mapped_category_id', '0');
						$this->db()->assignStr('key_name', 'doba_' . $category['id']);
						$this->db()->assignStr('name', ucfirst($category['name']));
						$parent = $this->db()->insert(DB_PREFIX . 'catalog');
					}
					
					$level++;
				}
				
				//update category
				if (!in_array('category', $lock_fields))
				{
					$this->db()->reset();
					$this->db()->assign('cid', $parent);
					$this->db()->update(DB_PREFIX . 'products', 'WHERE pid=' . $product_id_in_db);
				
					$this->db()->query('DELETE FROM ' . DB_PREFIX . 'products_categories WHERE pid=' . $product_id_in_db . ' AND is_primary=1');

					$this->db()->reset();
					$this->db()->assign('cid', $parent);
					$this->db()->assign('pid', $product_id_in_db);
					$this->db()->assign('is_primary', 1);
					$this->db()->insert(DB_PREFIX . 'products_categories');
				}

				// secondary categories
				if (!in_array('secondary-categories', $lock_fields))
				{
					$this->db()->query('DELETE FROM ' . DB_PREFIX . 'products_categories WHERE pid=' . $product_id_in_db . ' AND is_primary=0');
				}
				
				//shipping
				if ($status == 1)
				{
					$this->db()->query('DELETE FROM ' . DB_PREFIX . 'products_shipping_price WHERE pid=' . $product_id_in_db);
				}
				
				global $dobaShippingMethods;
				
				if (!isset($dobaShippingMethods) || !is_array($dobaShippingMethods))
				{
					$this->db()->query('SELECT * FROM ' . DB_PREFIX . 'shipping_selected WHERE method_id="product_level"');
					
					$dobaShippingMethods = array();
					while (($shippingMethod = $this->db()->moveNext()) != false)
					{
						$dobaShippingMethods[] = $shippingMethod;
					} 
				}
				
				if (count($dobaShippingMethods))
				{
					foreach ($dobaShippingMethods as $shippingMethod)
					{
						$this->db()->reset();
						$this->db()->assignStr('pid', $product_id_in_db);
						$this->db()->assignStr('ssid', $shippingMethod['ssid']);
						$this->db()->assignStr('is_price', 'Yes');
						$this->db()->assignStr('price', $product['ship_cost']);
						$this->db()->insert(DB_PREFIX . 'products_shipping_price');
					}						
				}
			}
		}
		else
		{
			@file_put_contents($logFile, print_r($product, 1)."\n\n", FILE_APPEND);
		}

		return $status;
	}

	/**
	 * Get item image
	 *
	 * @param $image_url
	 * @param $product_id
	 * @param bool $overwrite
	 *
	 * @return bool
	 */
	public function getItemDefaultImage($image_url, $product_id, $overwrite = true)
	{
		try
		{
			$imageFileName = ImageUtility::imageFileName($product_id);

			$imageFile = 'images/products/' . $imageFileName . '.jpg';
			
			if (!is_file($imageFile) || $overwrite)
			{
				$c = curl_init($image_url);
				curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($c, CURLOPT_TIMEOUT, 1800);
				$buffer = curl_exec($c);
				
				if (curl_errno($c) > 0) return false;
				
				if (file_put_contents($imageFile, $buffer))
				{
					if ($this->settings('CatalogThumbType') != 'Manual')
					{
						ImageUtility::generateProductThumb($imageFile, 'images/products/thumbs/' . $imageFileName . '.jpg');
					}

					if ($this->settings('CatalogThumb2Type') != 'Manual')
					{
						ImageUtility::generateProductPreview($imageFile, 'images/products/preview/' . $imageFileName . '.jpg');
					}

					$this->db()->query('UPDATE ' . DB_PREFIX . 'products SET image_location="Local", image_url="" WHERE product_id="' . $this->db()->escape($product_id) . '"');

					return true;
				}
			}
		}
		catch (Exception $e)
		{
			//handle error here
		}
		return false;
	}

	/**
	 * @param $images_urls
	 * @param $item_id
	 * @param bool $overwrite
	 * @return bool
	 */
	public function getItemSecondaryImages($images_urls, $item_id, $overwrite = true)
	{
		try
		{
			$imageFileName = ImageUtility::imageFileName($item_id);

			$this->db()->query('SELECT * FROM ' . DB_PREFIX . 'products WHERE product_id="' . $this->db()->escape($item_id) . '"');
			
			if (($product = $this->db()->moveNext()) != false)
			{
				/**
				 * Remove current secondary products
				 */
				$this->db()->query('DELETE FROM ' . DB_PREFIX . 'products_images WHERE pid="' . $this->db()->escape($product['pid']) . '"');

				for ($i=0; $i<=100; $i++)
				{
					$imageFile = 'images/products/secondary/' . $imageFileName . '-' . $i;

					if (is_file($imageFile . '.jpg')) unlink($imageFile . '.jpg');
					if (is_file($imageFile . '.png')) unlink($imageFile . '.png');
					if (is_file($imageFile . '.gif')) unlink($imageFile . '.gif');

					$imageThumb = 'images/products/secondary/thumbs/' . $imageFileName . '-' . $i . '.jpg';

					if (is_file($imageThumb)) unlink($imageThumb);
				}
				
				if (is_array($images_urls) && count($images_urls) > 0)
				{
					$counter = 0;

					foreach ($images_urls as $image_url)
					{
						$imageFile = 'images/products/secondary/' . $imageFileName . '-' . $counter .'.jpg';

						$c = curl_init($image_url);
						curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
						curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($c, CURLOPT_TIMEOUT, 300);
						$buffer = curl_exec($c);
						
						if (curl_errno($c) == 0)
						{
							if (file_put_contents($imageFile, $buffer))
							{
								ImageUtility::generateProductSecondaryThumb($imageFile, 'images/products/secondary/thumbs/' . $imageFileName . '-' . $counter . '.jpg');
							
								$image_data = getimagesize($imageFile);
								
								//insert image data into database
								$this->db()->reset();
								$this->db()->assignStr('pid', $product['pid']);
								$this->db()->assignStr('is_visible', 1);
								$this->db()->assignStr('width', $image_data[0]);
								$this->db()->assignStr('height', $image_data[1]);
								$this->db()->assignStr('type', 'jpg');
								$this->db()->assignStr('filename', $imageFileName . '-' . $counter);
								$this->db()->assignStr('caption', '');
								$this->db()->assignStr('alt_text', '');
								$this->db()->insert(DB_PREFIX.'products_images');
							}
						}

						$counter++;
					}

					return true;
				}
			}
		}
		catch (Exception $e)
		{
			//handle error here
		}

		return false;
	}
}

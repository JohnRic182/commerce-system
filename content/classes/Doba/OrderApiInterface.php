<?php

/**
 * Interface Doba_OrderApiInterface
 */
interface Doba_OrderApiInterface
{
	/**
	 * @param $shipping_address
	 * @param $items
	 * @return mixed
	 */
	public function getShippingQuote($shipping_address, $items);
}
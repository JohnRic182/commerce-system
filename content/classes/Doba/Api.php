<?php

/**
 * Doba API Class
 */
class Doba_Api
{
	public $test_mode = false;
	public $version = "610";
	protected $username = "";
	protected $password = "";
	public $retailer_id = "";
	public $api_url = "";
	
	public $price_option = "item_price_inc";
	public $price_modifier = 10;
	public $price_option99 = true;
	public $min_stock = 5;
	
	public $last_error = false;
	
	public $response_xml = null;
	public $response_error = null;
	
	protected $_db = null;
	protected $_settings = array();
	
	/**
	 * Class constructor
	 * @param array $settings
	 * @return Doba_Api
	 */
	public function __construct(&$settings, &$db = null)
	{
		$this->test_mode = $settings["doba_mode"] == "Test";
		$this->username = $settings["doba_username"];
		$this->password = $settings["doba_password"];
		$this->retailer_id = $settings["doba_retailer_id"];
		
		$this->api_url = $this->test_mode ? "https://www.sandbox.doba.com/api/20101012/xml_retailer_api.php" : "https://www.doba.com/api/20101012/xml_retailer_api.php";
		
		$this->price_option = $settings["doba_price_option"];
		$this->price_option99 = $settings["doba_price_option99"] == "Yes"; 
		$this->price_modifier = $settings["doba_price_modifier"];
		$this->min_stock = $settings["doba_min_stock"];
		
		$this->_db = $db;
		$this->_settings = $settings;
		
		return $this;
	}
	
	/**
	 * Connect to doba server and return data
	 * @param string $xml
	 * @return mixed
	 */
	public function call($body, $filename = null)
	{
		$isDebug = defined('DEVMODE') && DEVMODE && defined('DOBA_DEBUG') && DOBA_DEBUG;

		$xml = 
			'<dce>'."\n".
				'<request>'."\n".
					'<authentication>'."\n".
						'<username>'.($this->username).'</username>'."\n".
						'<password>'.($this->password).'</password>'."\n".
					'</authentication>'."\n".
					'<retailer_id>'.($this->retailer_id).'</retailer_id>'."\n".
					$body.
				'</request>'."\n".
			'</dce>'."\n";

		if ($isDebug)
		{
			@file_put_contents('content/cache/log/doba-debug.txt', $xml."\n\n", FILE_APPEND);
		}

		$c = curl_init($this->api_url);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($c, CURLOPT_TIMEOUT, 300);
		
		set_time_limit(108000);
		
		if ($filename != null)
		{
			try
			{
				$f = fopen($filename, "w+");
				curl_setopt($c, CURLOPT_FILE, $f);
			}
			catch (Exception $e)
			{
				$this->last_error = $e->getMessage();
				return false;
			}
		}
		else
		{
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		}

		$response = curl_exec($c);

		if ($isDebug)
		{
			@file_put_contents('content/cache/log/doba-debug.txt', $response."\n\n", FILE_APPEND);
		}

		if (curl_errno($c)) 
		{
			$this->last_error = curl_error($c);
			return false;
		}
		
		if ($response && $filename != null)
		{
			fclose($f);
			return true;
		}

		if (trim($response) == "")
		{
			$this->last_error = "Response is empty";
			$this->response_xml = null;
		}
		else
		{
			$this->response_xml = simplexml_load_string($response);
		}
		
		if ($this->response_xml != null)
		{
			if (isset($this->response_xml->response->outcome))
			{
				if ($this->response_xml->response->outcome == "success") return xml2array($this->response_xml);
				if ($this->response_xml->response->outcome == "failure")
				{
					$this->response_error = $this->response_xml->response->error;
					$this->last_error = 
						$this->response_xml->response->error->message." ".
						" (Level ".$this->response_xml->response->error->level.", Code ".
						$this->response_xml->response->error->code.")";
				}
				else
				{
					$this->last_error = "Unknown error. Please try again";
				}
			}
		}
		else
		{
			$this->last_error = "XML parse error";
		}

		if ($isDebug)
		{
			@file_put_contents('content/cache/log/doba-debug.txt', $this->last_error."\n\n", FILE_APPEND);
		}
		
		return false;
	}
	
	/**
	 * Returns database instance
	 * @return unknown
	 */
	public function db()
	{
		return $this->_db;
	}

	/**
	 * @param string $name
	 * @return array
	 */
	public function settings($name = "")
	{
		return $name == "" ? $this->_settings : $this->_settings[$name];
	}
}
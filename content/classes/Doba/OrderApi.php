<?php

/**
 * Doba Order API Class
 */
class Doba_OrderApi extends Doba_Api implements Doba_OrderApiInterface
{
	/**
	 * Order lookup
	 * @param array $shipping_address
	 * @param array $items
	 * @return mixed
	 */
	public function orderLookup($shipping_address, $items)
	{
		$xml = 
			"<action>orderLookup</action>".
			"<shipping_firstname>-</shipping_firstname>".
			"<shipping_lastname>".utf8_encode($shipping_address["name"])."</shipping_lastname>".
			"<shipping_street>".utf8_encode(substr($shipping_address["address1"].($shipping_address["address2"]!=""?(", ".$shipping_address["address2"]):""), 0, 25))."</shipping_street>".
			"<shipping_city>".utf8_encode($shipping_address["city"])."</shipping_city>".
			"<shipping_state>".utf8_encode($shipping_address["state_abbr"])."</shipping_state>".
			"<shipping_postal>".utf8_encode($shipping_address["zip"])."</shipping_postal>".
			"<shipping_country>".utf8_encode($shipping_address["country_iso_a2"])."</shipping_country>".
			"<ip_address>".$_SERVER['REMOTE_ADDR']."</ip_address>".
			"<items>";
		
		foreach ($items as $item)
		{
			$xml.=
				"<item>".
					"<item_id>".utf8_encode($item["item_id"])."</item_id>".
					"<quantity>".utf8_decode($item["quantity"])."</quantity>".
				"</item>";
		}
		
		$xml.=
			"</items>";
					
		$r = $this->call($xml);
		return $r ? $r['response'] : false;
	}

	/**
	 * Get shipping price for current order
	 *
	 * @param $shipping_address
	 * @param $items
	 * @return bool
	 */
	public function getShippingQuote($shipping_address, $items)
	{
		$result = $this->orderLookup($shipping_address, $items);
		if ($result && isset($result['shipping_fees']))
		{
			$a = $result['shipping_fees'] + $result['drop_ship_fees'];
			return $a;
		}
		$this->last_error = $this->last_error != "" ? $this->last_error : 'Error processing shipping data';
		return false;
	}
	
	/**
	 * Creates order
	 * @param array $shipping_address
	 * @param array $items
	 * @return mixed
	 */
	public function createOrder($shipping_address, $items, $order_id)
	{
		$xml = 
			"<action>createOrder</action>".
			"<shipping_firstname>-</shipping_firstname>".
			"<shipping_lastname>".utf8_encode($shipping_address["name"])."</shipping_lastname>".
			"<shipping_street>".utf8_encode($shipping_address["address1"].($shipping_address["address2"]!=""?(", ".$shipping_address["address1"]):""))."</shipping_street>".
			"<shipping_city>".utf8_encode($shipping_address["city"])."</shipping_city>".
			"<shipping_state>".utf8_encode($shipping_address["state_abbr"])."</shipping_state>".
			"<shipping_postal>".utf8_encode($shipping_address["zip"])."</shipping_postal>".
			"<shipping_country>".utf8_encode($shipping_address["country_iso_a2"])."</shipping_country>".
			"<ip_address>".$_SERVER['REMOTE_ADDR']."</ip_address>".
			"<items>";
		
		foreach ($items as $item)
		{
			$xml.=
				"<item>".
					"<item_id>".utf8_encode($item["item_id"])."</item_id>".
					"<quantity>".utf8_decode($item["quantity"])."</quantity>".
				"</item>";
		}
		
		$xml.=
			"</items>".
			"<po_number>".$order_id."</po_number>";

		$r = $this->call($xml);
		
		if ($r && isset($r['response']['order_id']))
		{
			return $r;
		}
		
		return false;
	}

	/**
	 * @param $order_id
	 * @return array|bool
	 */
	public function getOrderDetail($order_id)
	{
		$xml = 
			"<action>getOrderDetail</action>".
			"<order_ids>".
				"<order_id>".$order_id."</order_id>".
        	"</order_ids>";
		
		$r = $this->call($xml);
		
		if ($r)
		{
			$order = $r['response']['orders']['order'];
			$a  = array(
				"order_id" => (string)$order['order_id'],
				"status" => (string)$order['status'],
				"subtotal" => number_format((double)$order['subtotal'], 2),
				"service_fees" => number_format(!isset($order['service_fees']) || $order['service_fees'] == "" ? 0.00 : (double)$order['service_fees'], 2),
				"shipping_fees" => number_format(!isset($order['shipping_fees']) || $order['shipping_fees'] == "" ? 0.00 : (double)$order['shipping_fees'], 2),
				"dropship_fees" => number_format(!isset($order['dropship_fees']) || $order['dropship_fees'] == "" ? 0.00 : (double)$order['dropship_fees'], 2),
				"order_total" => number_format(!isset($order['order_total']) || $order['order_total'] == "" ? 0.00 : (double)$order['order_total'], 2)
			);
			return $a;
		}
		return false;
	}

	/**
	 * Fund order
	 *
	 * @param $order_id
	 * @param $cvv2
	 * @return mixed
	 */
	public function fundOrder($order_id, $cvv2)
	{
		$xml =
			"<action>fundOrder</action>".
			"<order_ids>".
				"<order_id>".$order_id."</order_id>".
        	"</order_ids>".
			"<fund_method>default_payment_account</fund_method>".
			($cvv2 != "" ? "<cvv2>".$cvv2."</cvv2>" : "");   
		
		$r = $this->call($xml);
		
		return $r;
	}
	
	/**
	 * Update order status / callback
	 * @param $order_id
	 * @param $order_data
	 */
	public function updateOrder($order_id, $order_data)
	{
		$order_num = isset($order_data['po_number']) ? $order_data['po_number'] != "" ? $order_data['po_number'] : false : false;
		
		$this->db()->query("SELECT * FROM ".DB_PREFIX."orders WHERE doba_order_id='".$order_id."'".($order_num?" AND order_num='".$order_num."'":""));
		if (($order = $this->db()->moveNext()) != false)
		{
			$oldOrderStatus = $newOrderStatus = $order['status'];
			$oldPaymentStatus = $newPaymentStatus = $order['payment_status'];

			$shipments = is_array($order_data['shipments']['shipment']) ? $order_data['shipments']['shipment'] : array($order_data['shipments']['shipment']);
			
			$tracking = array();
			
			$this->db()->reset();
			$update = false;
			foreach ($shipments as $shipment)
			{
				if (isset($shipment['carrier']) && $shipment['carrier'] != "" && isset($shipment['tracking']) && $shipment['tracking'] != "")
				{
					$tracking[] = array("carrier"=>(string)$shipment['carrier'], "tracking"=>(string)$shipment['tracking']);
				}
			}
			
			if (count($tracking) > 0)
			{
				$this->db()->assignStr("shipping_tracking", serialize($tracking));
				$update = true;
			}
			
			switch ((string)$order_data->status)
			{
				case "Completed" : 
				{
					$this->db()->assignStr("status", $newOrderStatus = "Completed");
					$update = true;
					break;
				}
				case "Cancelled" :
				{
					$this->db()->assignStr("status", $newOrderStatus = "Canceled");
					$update = true;
					break;
				}
			}
			
			if ($update)
			{
				$this->db()->update(DB_PREFIX."orders", "WHERE oid='".$order["oid"]."'");
			}

			/**
			 * Handle on status change event
			 */
			if ($newOrderStatus != $oldOrderStatus || $newPaymentStatus != $oldPaymentStatus)
			{
				require_once('content/engine/engine_user.php');
				require_once('content/engine/engine_order.php');

				$userInstance = USER::getById($db, $settings, $order["uid"]);
				$orderInstance = ORDER::getById($db, $settings, $userInstance, $order["oid"]);
				Events_OrderEvent::dispatchStatusChangeEvent($orderInstance, $oldOrderStatus, $oldPaymentStatus);
			}
		}
	}
}
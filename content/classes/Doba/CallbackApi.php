<?php

/**
 * Doba Product API Class
 */
class Doba_CallbackApi extends Doba_Api
{
	/**
	 * Parses Doba callback request
	 * @param string $xml
	 * @return mixed
	 */
	public function processCallback($xml)
	{
		$xml = xml2array(simplexml_load_string($xml));

		if ($xml && isset($xml['response']['callback_type']))
		{
			$callback_type = $xml['response']['callback_type'];
			
			$db = $this->db();
			$productApi = new Doba_ProductApi($this->_settings, $db);
			$orderApi = new Doba_OrderApi($this->_settings, $db);
			
			switch ($callback_type)
			{
				/**
				 * Order status change
				 */
				case "order_update" :
				{
					$orders = isset($xml['response']['orders']['order'][0]) ? $xml['response']['orders']['order'] : array($xml['response']['orders']['order']);
				
					foreach ($orders as $order)
					{
						$orderApi->updateOrder($order['order_id'], $order);
					}
					break;				
				}
				
				/**
				 * Update items details
				 */
				case "item_detail" :
				{
					$products = isset($xml['response']['products']['product'][0]) ? $xml['response']['products']['product'] : array($xml['response']['products']['product']);
					foreach ($products as $product)
					{
						$items = isset($product['items']['item'][0]) ? $product['items']['item'] : array($product['items']['item']);
						foreach ($items as $item)
						{
						//	$productApi->itemToDb($product, $item, false);
						}
					}
					
					break;		
				}
				
				/**
				 * Update inventory
				 */
				case "inventory_update" :
				{
					$items = isset($xml['response']['items']['item'][0]) ? $xml['response']['items']['item'] : array($xml['response']['items']['item']);
					foreach ($items as $item)
					{
						$productApi->updateInventory($item, false);
					}
					break;		
				}
			}
			
			unset($productApi);
		}
		
		unset($xmlParser);
	} 
}
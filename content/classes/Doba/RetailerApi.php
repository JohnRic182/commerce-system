<?php

/**
 * Doba Retailer API Class
 *
 */
class Doba_RetailerApi extends Doba_Api
{
	// FIXME: ENCODE THIS FILE
	public function __construct($settings, $db)
	{
		parent::__construct($settings, $db);

		// use our credentials here
		$this->username = 'pinnacle';
		$this->password = 'P)O(I*8i9o0p~!@#';

		$this->api_url = ($this->test_mode)
			? "https://www.sandbox.doba.com/api/xml_partner_api.php"
			: "https://www.doba.com/api/xml_partner_api.php";
	}

	/**
	 * Request Partner Permission
	 *
	 * @return mixed
	 */
	public function requestPartnerPermission()
	{
		$xml = "<action>requestPartnerPermission</action>";
		$r = $this->call($xml);

		if ($r && $r['response']['value'] == 1) return true;
		return false;
	}

	/**
	 * Request Partner Permission
	 *
	 * @return mixed Returns an array with status reason
	 */
	public function hasPartnerPermission()
	{
		$xml = "<action>hasPartnerPermission</action>";
		return $this->call($xml);
	}


	/**
	 * Remove Partner Permission
	 *
	 * @return mixed Returns an array with status reason
	 */
	public function removePartnerPermission()
	{
		$xml = "<action>removePartnerPermission</action>";
		return $this->call($xml);
	}
}

<?php

/**
 * Class FraudService_Admin_Controller_FraudMethod
 */
class FraudService_Admin_Controller_FraudMethod extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * List payment methods
	 */
	public function listAction()
	{
		$request    = $this->getRequest();
		$repository = $this->getRepository();
		$adminView  = $this->getView();
		$adminView->assign('FraudMethods', $repository->getFraudMethods());
		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8002');

		$adminView->assign('body', 'templates/pages/fraud-method/methods.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @return FraudService_DataAccess_FraudMethodRepository
	 */
	protected function getRepository()
	{
		return new FraudService_DataAccess_FraudMethodRepository($this->getDb(), $this->getSettings());
	}

	public function activateFormAction()
	{
		$request    = $this->getRequest();
		$repository = $this->getRepository();

		$fraudMethodId = $request->get('fraud_method_id', '');
		$fraudMethod   = $repository->getById($fraudMethodId);

		if ($fraudMethod)
		{
			$editableFormBuilders = $this->getEditableFormBuilders($fraudMethod);
			$adminView            = $this->getView();

			if (count($editableFormBuilders) > 0)
			{
				/** @var core_Form_FormBuilder $formBuilder */
				$formBuilder = $editableFormBuilders[0];
				if(isset($fraudMethod['method_signup_url']))
				{
					$signupGroup = new core_Form_FormBuilder('fraud-method-signupurl', array('label' => 'fraud_methods.sign_up_for_account', 'collapsible' => false));
					$signupGroup->add('signup_url', 'static', array('label' => '<a href="' . $fraudMethod['method_signup_url'] . '" target="_blank">' . 'Click Here to Sign Up ' . '</a>',
																	'value' => ''));
					$formBuilder->add($signupGroup);
				}

				$adminView->assign('form', $formBuilder->getForm());
			}
			else
			{
				if(isset($fraudMethod['method_signup_url']))
				{
					/** @var core_Form_FormBuilder $formBuilder */
					$signupGroup = new core_Form_FormBuilder('fraud-method-signupurl', array('label' => 'fraud_methods.sign_up_for_account', 'collapsible' => false));
					$signupGroup->add('signup_url', 'static', array('label' => '<a href="'.$fraudMethod['method_signup_url'].'">' . 'Click Here to Sign Up ' . '</a>',
																	'value' => ''));
					$adminView->assign('form', $signupGroup->getForm());
				}
				else{
					$adminView->assign('form', false);
				}
			}

			$this->renderJson(array('status' => 1, 'html' => $adminView->fetch('generic-form')));
		}
		else
		{
			$this->renderJson(array('status' => 0, 'message' => 'Cannot find fraud solution by provided ID'));
		}
	}

	protected function getEditableFormBuilders($fraudMethod)
	{
		$groupId = 0;
		/**
		 * Generate settings form from settings table
		 */
		$editableFormBuilders = array();

		$groupLabels = array(
			0 => 'Account Details',
			1 => 'Advanced Settings',
		);

		foreach ($fraudMethod['settings'] as $settingsOption)
		{
			$groupId = $settingsOption['group_id'];

			if (!isset($editableFormBuilders[$groupId]))
			{
				$editableFormBuilders[$groupId] = new core_Form_FormBuilder('editable' . $groupId, array('label' => array_key_exists($groupId, $groupLabels) ? $groupLabels[$groupId] : 'Properties', 'class' => 'ic-payment', 'first' => $groupId == 0));
			}

			/** @var core_Form_FormBuilder $groupFormBuilder */
			$groupFormBuilder = $editableFormBuilders[$groupId];

			switch ($settingsOption['input_type'])
			{
				case 'text' :
				{
					$groupFormBuilder->add('form[' . $settingsOption['name'] . ']', 'text', array('label' => $settingsOption['caption'], 'required' => false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'password' :
				{
					$groupFormBuilder->add('form[' . $settingsOption['name'] . ']', 'password', array('label' => $settingsOption['caption'], 'required' => false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'color' :
				{
					$groupFormBuilder->add('form[' . $settingsOption['name'] . ']', 'color', array('label' => $settingsOption['caption'], 'required' => false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'textarea' :
				{
					$groupFormBuilder->add('form[' . $settingsOption['name'] . ']', 'textarea', array('label' => $settingsOption['caption'], 'required' => false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'label' :
				{
					$groupFormBuilder->add('form[' . $settingsOption['name'] . ']', 'static', array('label' => $settingsOption['caption'], 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'hidden' :
				{
					$groupFormBuilder->add('form[' . $settingsOption['name'] . ']', 'hidden', array('value' => $settingsOption['value']));
					break;
				}
				case 'select' :
				{
					$fieldOptionsArray = explode(',', $settingsOption['options']);
					$fieldOptions      = array();

					foreach ($fieldOptionsArray as $fieldOption)
					{
						if (strpos($settingsOption['name'], 'Currency_Code') !== false)
						{
							$fieldOptions[] = new core_Form_Option(trim($fieldOption), $fieldOption);
						}
						else
						{
							$fieldOptions[] = new core_Form_Option(trim($fieldOption), ucwords(strtolower($fieldOption)));
						}
					}

					$groupFormBuilder->add('form[' . $settingsOption['name'] . ']', 'choice', array('label' => $settingsOption['caption'], 'required' => false, 'value' => $settingsOption['value'], 'options' => $fieldOptions, 'note' => $settingsOption['description']));
					break;
				}
				case 'image' :
				{
					$groupFormBuilder->add($settingsOption['name'], 'file', array('label' => $settingsOption['caption'], 'note' => $settingsOption['description']));
					if ($settingsOption['value'] != '')
					{
						$groupFormBuilder->add(
							'form[preview_' . $settingsOption['name'] . ']', 'imagepreview',
							array('label' => 'Current image', 'value' => $settingsOption['value'], 'note' => 'Click over image to see it real size')
						);
					}
					break;
				}
			}
		}
		if(isset($fraudMethod['snippet']) && $fraudMethod['snippet'])
		{
			$_snippet = $fraudMethod['snippet'];
			/** @var core_Form_FormBuilder $groupFormBuilder */
			if ($editableFormBuilders[1])
			{
				$groupFormBuilder = $editableFormBuilders[1];
				$groupFormBuilder->add('form[snippet_is_visible]', 'checkbox', array(
					'label' => 'Enable/Disable Device Validation?',
					'value' => 'Yes',
					'wrapperClass' => 'clear-both',
					'current_value' => $_snippet['is_visible']
				));
			}
		}

		return $editableFormBuilders;
	}

	/**
	 * Deactivate fraud service method
	 */
	public function deactivateAction()
	{
		$repository    = $this->getRepository();
		$request       = $this->getRequest();
		$fraudMethodId = $request->get('fraud_method_id', '');

		$fraudMethod = $repository->getById($fraudMethodId);

		if (!is_null($fraudMethod))
		{
			$repository->deactivate($fraudMethodId);
			Admin_Flash::setMessage('common.success');
			Admin_Log::log('Fraud solution "' . $fraudMethodId . '": settings have been changed', Admin_Log::LEVEL_IMPORTANT);
		}

		$this->renderJson(array(
			'status' => 1
		));
	}

	/**
	 * Deactivate fraud service method
	 */
	public function activateAction()
	{
		$repository    = $this->getRepository();
		$request       = $this->getRequest();
		$fraudMethodId = $request->get('fraud_method_id');

		$fraudMethod = $repository->getById($fraudMethodId);

		if ($fraudMethod)
		{
			$form = $request->get('form', array());

			$settingsRepository = DataAccess_SettingsRepository::getInstance();

			foreach ($fraudMethod['settings'] as $settingsOption)
			{
				$name = $settingsOption['name'];

				if (isset($form[$name]))
				{
					$value = $form[$name];
					$settingsRepository->save(array($name => $value));
				}
			}
			$repository->activate($fraudMethodId);
			Admin_Flash::setMessage('common.success');
			Admin_Log::log('Fraud method "' . $fraudMethodId . '" settings have been changed', Admin_Log::LEVEL_IMPORTANT);
		}

		$this->redirect('fraud_methods');
	}

	public function indexAction()
	{
		$request = $this->getRequest();

		$repository = $this->getRepository();
		$adminView  = $this->getView();

		$fraudMethodId = $request->get('fraud_method_id', '');
		$fraudMethod   = $repository->getById($fraudMethodId);


		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8003');

		if ($fraudMethod)
		{
			if ($request->isPost())
			{
				if (!Nonce::verify($request->get('nonce'), 'fraud_method_update'))
				{
					Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				}
				else
				{
					if (isset($fraudMethod['settings']))
					{
						$form = $request->get('form', array());

						$settingsRepository = DataAccess_SettingsRepository::getInstance();

						foreach ($fraudMethod['settings'] as $settingsOption)
						{
							$name = $settingsOption['name'];

							if (isset($form[$name]))
							{
								$value = $form[$name];
								$settingsRepository->save(array($name => $value));
							}
						}
					}
					if ($fraudMethod['snippet'])
					{
						$setSnippetState = isset($form['snippet_is_visible'])?true:false;
						if(!$setSnippetState)
						{
							$repository->deactivateSnippetByMethodId($fraudMethodId);
						}
						else{
							$repository->activateSnippetByMethodId($fraudMethodId);
						}
					}
					/**
					 * Reread with changes
					 */
					$fraudMethod = $repository->getById($fraudMethodId);
					Admin_Flash::setMessage('common.success');
				}
				$this->redirect('fraud_method', array('fraud_method_id' => $fraudMethodId));
				return;
			}

			$formBuilders = $this->getEditableFormBuilders($fraudMethod);

			$formBuilder = new core_Form_FormBuilder('general');
			foreach ($formBuilders as $idx => $editableFormBuilder)
			{
				if ($idx < 2)
				{
					if ($idx > 0)
					{
						/** @var core_Form_FormBuilder $editableFormBuilder */
						$data = $editableFormBuilder->getData();
						if ($data == null)
						{
							$data = array();
						}

						$editableFormBuilder->setData(array_merge($data, array('collapsible' => true)));
					}

					$formBuilder->add($editableFormBuilder);
				}
			}

			$adminView = $this->getView();

			$adminView->assign('form', $formBuilder->getForm());

			$formBuilder = new core_Form_FormBuilder('standard');
			$group       = new core_Form_FormBuilder('standard_group', array('label' => 'fraud_methods.common_settings', 'collapsible' => true));

			$fraudProvider = FraudService_Provider_FraudLabs_FraudLabsProvider::getProviderInstanceByProviderId($this->getDb(), $this->getSettings(), $fraudMethod['fid']);
			if (!is_null($fraudProvider))
			{
				$settingsForm = $fraudProvider->getSettingsEditingForm();
				if ($settingsForm)
				{
					$formBuilder->add($settingsForm);
				}
			}

			//$formBuilder->add($group);
			//$group->add('fraud_method[title]', 'text', array('label' => 'fraud_methods.fraud_method_title', 'value' => $fraudMethod['title']));

			/*
			$optionPriorities = array();
			for ($i = 0; $i <=10; $i++)
			{
				$optionPriorities[] = new core_Form_Option($i, $i);
			}
			*/

			$adminView->assign('standardGatewaryForm', $formBuilder->getForm());

			$adminView->assign('fraudMethodId', $fraudMethod['fid']);
			$adminView->assign('fraudMethod', $fraudMethod);

			$adminView->assign('nonce', Nonce::create('fraud_method_update'));

			$adminView->assign('body', 'templates/pages/fraud-method/edit.html');
			$adminView->render('layouts/default');
		}
		else
		{
			$adminView->assign('body', 'templates/pages/fraud-method/not-allowed.html');
			$adminView->render('layouts/default');
		}
	}
}

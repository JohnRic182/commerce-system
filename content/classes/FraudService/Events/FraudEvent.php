<?php

/**
 * Class FraudService_Events_FraudEvent
 */
class FraudService_Events_FraudEvent extends Events_Event
{
	const ON_FRAUD_SERVICE_PAYMENT_METHOD_DATA_SET = 'on-fraud-service-payment-method-data-set';
	const ON_FRAUD_SERVICE_ORDER_CHECK             = 'on-fraud-service-order-check';
	const ON_FRAUD_SERVICE_REVIEW_ACTION           = 'on-fraud-service-review-action';
	const ON_FRAUD_SERVICE_REJECT_ACTION           = 'on-fraud-service-reject-action';
	const ON_FRAUD_SERVICE_APPROVE_ORDER_ACTION    = 'on-fraud-service-approve-order-action';
	const ON_FRAUD_SERVICE_DENY_ORDER_ACTION       = 'on-fraud-service-deny-order-action';

	/*--------------------------------------------------------------------------------------*/
	const FRAUD_SERVICE_PAYMENTS_DATA_IND          = 'FraudServicePaymentMethodData';

	const FRAUD_SERVICE_PAYMENTS_DATA_CVV	   = 'cvv';
	const FRAUD_SERVICE_PAYMENTS_DATA_AVS	   = 'avs';

	const FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM	   = 'cc_number';
	const FRAUD_SERVICE_PAYMENTS_DATA_CARDFNAME	   = 'cc_first_name';
	const FRAUD_SERVICE_PAYMENTS_DATA_CARDLNAME	   = 'cc_last_name';
	const FRAUD_SERVICE_PAYMENTS_DATA_CARDCVV2	   = 'cc_cvv2';
	const FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPMONTH = 'cc_expiration_month';
	const FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPYEAR  = 'cc_expiration_year';
	/*--------------------------------------------------------------------------------------*/

	/**
	 * Set order
	 *
	 * @param ORDER $order
	 *
	 * @return FraudService_Events_FraudEvent
	 */
	public function setOrder(&$order)
	{
		$this->setData('order', $order);
		return $this;
	}

	/**
	 * Returns order
	 *
	 * @return ORDER
	 */
	public function getOrder()
	{
		return $this->getData('order');
	}

	/**
	 * @param $data
	 * @return $this
	 */
	public function setPaymentMethodData($data)
	{
		$this->setData(FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_IND, $data);
		return $this;
	}

	/**
	 * @return null
	 */
	public function getPaymentMethodData()
	{
		return $this->getData(FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_IND);
	}

}

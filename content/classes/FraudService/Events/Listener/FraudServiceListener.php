<?php

/**
 * Class FraudService_Events_Listener_FraudServiceListener
 */
class FraudService_Events_Listener_FraudServiceListener
{
	public static function setPaymentDataForFraudServiceTransaction(FraudService_Events_FraudEvent $event)
	{
		global $db, $settings;

		/** @var ORDER $order */
		$order = $event->getOrder();
		$event->clearErrors();

		$result = array();
		if (!is_null($order))
		{
			$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings));
			$paymentMethod           = $paymentMethodRepository->getById($order->getPaymentMethodId());

			if ($paymentMethod)
			{
				$activeProvider  = FraudService_Provider_FraudServiceProvider::getActiveProviderInstance($db, new DataAccess_SettingsRepository($db, $settings));
				if ($paymentMethod->isEnableFraudservice())
				{
					/* @var FraudService_DataAccess_FraudMethodRepository $fraudServiceRepository */
					$fraudServiceRepository = new FraudService_DataAccess_FraudMethodRepository($db, new DataAccess_SettingsRepository($db, $settings));
					if ($fraudServiceRepository && !is_null($activeProvider))
					{
						$oid = $order->getId();
						$transaction_data  = $fraudServiceRepository->getTransactionDefaults();
						$transaction = $fraudServiceRepository->getFraudTransactionForOrder($oid);
						$paymentData = '';
						if($transaction)
						{
							$transaction_data = $transaction;
						}
						$eventParams = $event->getPaymentMethodData();
						if( is_array($eventParams))
						{
							$paymentData = serialize($eventParams);
						}

						if($paymentData)
						{
							$transaction_data['payment_method_data'] = $paymentData;
							$ftid = $fraudServiceRepository->persistFraudTransaction($oid, $transaction_data);
							$transaction_data['ftid'] = $ftid;
							$result = $transaction_data;
						}
					}
				}
			}
		}
		else
		{
			$event->setError('Fraud Service: wrong Order param');
		}
		$event->setData('EventResult', $result);
	}

	public static function performCheckOrder(FraudService_Events_FraudEvent $event)
	{
		global $db, $settings;

		/** @var ORDER $order */
		$order = $event->getOrder();
		$event->clearErrors();

		$result = FraudService_Provider_FraudLabs_FraudLabsProvider::getCheckOrderDefaultResult();
		if (!is_null($order))
		{
			$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings));
			$paymentMethod           = $paymentMethodRepository->getById($order->getPaymentMethodId());

			if ($paymentMethod)
			{
				if ($paymentMethod->isEnableFraudservice())
				{
					/* @var FraudService_Provider_FraudServiceProvider $fProvider */
					$fProvider = FraudService_Provider_FraudServiceProvider::getActiveProviderInstance($db, new DataAccess_SettingsRepository($db, $settings));
					if ($fProvider)
					{
						$result = $fProvider->checkOrder($order);
					}
				}
			}

			if ($result['status'] == FraudService_Provider_FraudLabs_FraudLabsProvider::RESULT_ERROR)
			{
				$event->setError($result['error']);
			}
		}
		else
		{
			$event->setError('Fraud Service: wrong Order param');
		}
		$event->setData('CheckOrderResult', $result);
	}

	public static function reviewStatusAction(FraudService_Events_FraudEvent $event)
	{
		global $db, $settings;

		$checkOrderResult = $event->getData('CheckOrderResult');

		if ($checkOrderResult)
		{
			/* @var FraudService_Provider_FraudServiceProvider $fProvider */
			$fProvider = FraudService_Provider_FraudServiceProvider::getActiveProviderInstance($db, new DataAccess_SettingsRepository($db, $settings));
			if ($fProvider)
			{
				$result = $fProvider->reviewStatusAction($checkOrderResult);
				if ($result['oldOrderStatus'])
				{
					$event->setData('oldOrderStatus', $result['oldOrderStatus']);
				}

				if ($result['oldPaymentStatus'])
				{
					$event->setData('oldPaymentStatus', $result['oldPaymentStatus']);
				}

				if ($result['newOrderStatus'])
				{
					$event->setData('newOrderStatus', $result['newOrderStatus']);
				}

				if ($result['newPaymentStatus'])
				{
					$event->setData('newPaymentStatus', $result['newPaymentStatus']);
				}
			}
		}
	}

	public static function rejectStatusAction(FraudService_Events_FraudEvent $event)
	{

		global $db, $settings;

		$checkOrderResult = $event->getData('CheckOrderResult');
		if ($checkOrderResult)
		{
			/* @var FraudService_Provider_FraudServiceProvider $fProvider */
			$fProvider = FraudService_Provider_FraudServiceProvider::getActiveProviderInstance($db, new DataAccess_SettingsRepository($db, $settings));
			if ($fProvider)
			{
				$result = $fProvider->rejectStatusAction($checkOrderResult);
				if ($result['oldOrderStatus'])
				{
					$event->setData('oldOrderStatus', $result['oldOrderStatus']);
				}

				if ($result['oldPaymentStatus'])
				{
					$event->setData('oldPaymentStatus', $result['oldPaymentStatus']);
				}

				if ($result['newOrderStatus'])
				{
					$event->setData('newOrderStatus', $result['newOrderStatus']);
				}

				if ($result['newPaymentStatus'])
				{
					$event->setData('newPaymentStatus', $result['newPaymentStatus']);
				}
			}
		}
	}

	public static function approveOrderAction(FraudService_Events_FraudEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();
		$event->clearErrors();
		$result = FraudService_Provider_FraudServiceProvider::getDefaultResult();

		if ($order)
		{
			$settingsRep            = new DataAccess_SettingsRepository($db, $settings);
			$fraudServiceRepository = new FraudService_DataAccess_FraudMethodRepository($db, $settingsRep);
			if ($fraudServiceRepository)
			{
				$fraudTransactions = $fraudServiceRepository->getFraudTransactionForOrder($order->getId());
				if ($fraudTransactions && isset($fraudTransactions['fid']) && $fraudTransactions['fid'])
				{
					/* @var FraudService_Provider_FraudServiceProvider $fProvider */
					$fProvider = FraudService_Provider_FraudServiceProvider::getProviderInstanceByProviderId($db, $settingsRep, $fraudTransactions['fid']);
					if ($fProvider)
					{
						$adminLogId = $event->getData('adminLogId', 0);
						$result = $fProvider->approveOrderAction($order->getId(), $adminLogId);
						if ($result['oldOrderStatus'])
						{
							$event->setData('oldOrderStatus', $result['oldOrderStatus']);
						}

						if ($result['oldPaymentStatus'])
						{
							$event->setData('oldPaymentStatus', $result['oldPaymentStatus']);
						}

						if ($result['newOrderStatus'])
						{
							$event->setData('newOrderStatus', $result['newOrderStatus']);
						}

						if ($result['newPaymentStatus'])
						{
							$event->setData('newPaymentStatus', $result['newPaymentStatus']);
						}
					}
				}

			}
		}

		if ($result['status'] == FraudService_Provider_FraudServiceProvider::RESULT_ERROR)
		{
			$event->setError($result['error']);
		}
	}

	public static function denyOrderAction(FraudService_Events_FraudEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();
		$event->clearErrors();
		$result = FraudService_Provider_FraudServiceProvider::getDefaultResult();

		if ($order)
		{
			$settingsRep            = new DataAccess_SettingsRepository($db, $settings);
			$fraudServiceRepository = new FraudService_DataAccess_FraudMethodRepository($db, $settingsRep);
			if ($fraudServiceRepository)
			{
				$fraudTransactions = $fraudServiceRepository->getFraudTransactionForOrder($order->getId());
				if ($fraudTransactions && isset($fraudTransactions['fid']) && $fraudTransactions['fid'])
				{
					/* @var FraudService_Provider_FraudServiceProvider $fProvider */
					$fProvider = FraudService_Provider_FraudServiceProvider::getProviderInstanceByProviderId($db, $settingsRep, $fraudTransactions['fid']);
					if ($fProvider)
					{
						$adminLogId = $event->getData('adminLogId', 0);
						$result = $fProvider->denyOrderAction($order->getId(), $adminLogId);
						if ($result['oldOrderStatus'])
						{
							$event->setData('oldOrderStatus', $result['oldOrderStatus']);
						}

						if ($result['oldPaymentStatus'])
						{
							$event->setData('oldPaymentStatus', $result['oldPaymentStatus']);
						}

						if ($result['newOrderStatus'])
						{
							$event->setData('newOrderStatus', $result['newOrderStatus']);
						}

						if ($result['newPaymentStatus'])
						{
							$event->setData('newPaymentStatus', $result['newPaymentStatus']);
						}
					}
				}

			}
		}

		if ($result['status'] == FraudService_Provider_FraudServiceProvider::RESULT_ERROR)
		{
			$event->setError($result['error']);
		}
	}
}
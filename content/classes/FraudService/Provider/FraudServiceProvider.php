<?php

/**
 * Class FraudService_Provider_FraudServiceProvider
 */
class FraudService_Provider_FraudServiceProvider
{
	const route       = 'FraudService_Provider_';
	const FORMAT_JSON = 'json';
	const FORMAT_XML  = 'xml';

	const STATUS_APPROVE = 'APPROVE';
	const STATUS_REJECT  = 'REJECT';
	const STATUS_REVIEW  = 'REVIEW';

	const RISK_SCORE_MIN = 1;
	const RISK_SCORE_MAX = 100;
	const RESULT_SUCCESS = 1;
	const RESULT_ERROR   = 0;
	/*  @var FraudService_Provider_FraudServiceProvider $instance */
	static $instance = null;
	/** @var DB */
	protected $db;
	/** @var DataAccess_SettingsRepository $settings */
	protected $settings;
	/** @var FraudService_Model_FraudMethod $fraudMethod */
	protected $fraudMethod = null;
	protected $fraudMethodClassName = '';

	protected $providerRepository;
	protected $licensekey = '';

	protected $checkOrderUrl = '';

	protected $format = 'json';

	protected $_verificationParams;
	protected $_verificationResponse;
	protected $_score;
	protected $_fraudStatus;
	protected $_error;

	protected $logFile = false;
	protected $logFileName = '';
	protected $logFilePath = 'content/cache/log/';

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->db                 = $db;
		$this->settings           = $settings;
		$this->providerRepository = new FraudService_DataAccess_FraudMethodRepository($db, $settings);
		$this->logFileName        = 'fraudLog.txt';
	}

	static function getCheckOrderDefaultResult()
	{
		return array(
			'status' => self::RESULT_SUCCESS,
			'oid' => 0,
			'response' => '',
			'score' => self::RISK_SCORE_MIN,
			'fraud_status' => 'APPROVE',
			'security_id' => '',
			'oldOrderStatus' => '',
			'oldPaymentStatus' => '',
			'newOrderStatus' => '',
			'newPaymentStatus' => '',
			'error' => '');
	}

	static function getDefaultResult()
	{
		return array(
			'status' => self::RESULT_SUCCESS,
			'response' => '',
			'oldOrderStatus' => '',
			'oldPaymentStatus' => '',
			'newOrderStatus' => '',
			'newPaymentStatus' => '',
			'error' => '');
	}

	static function getProviderInstance(DB $db, DataAccess_SettingsRepository $settings, $providerClass)
	{
		self::$instance = null;
		$className      = self::route . $providerClass . '_' . $providerClass . 'Provider';
		if (class_exists($className))
		{
			self::$instance = new $className($db, $settings);
		}
		return (self::$instance) ? self::$instance : null;
	}

	static function getActiveProviderInstance(DB $db, DataAccess_SettingsRepository $settings)
	{
		self::$instance     = null;
		$providerRepository = new FraudService_DataAccess_FraudMethodRepository($db, $settings);
		if ($providerRepository)
		{
			/* @var FraudService_Model_FraudMethod $fraudRec */
			$fraudRec = $providerRepository->getActiveMethod();
			if (!is_null($fraudRec))
			{
				$className = $fraudRec->getClassName();
				if (class_exists($className))
				{
					self::$instance = new $className($db, $settings);
					return (self::$instance) ? self::$instance : null;
				}
				else
				{
					return null;
				}

			}
			return null;
		}
		return null;
	}

	static function getProviderInstanceByProviderId(DB $db, DataAccess_SettingsRepository $settings, $providerId)
	{
		self::$instance     = null;
		$providerRepository = new FraudService_DataAccess_FraudMethodRepository($db, $settings);
		if ($providerRepository)
		{
			/* @var FraudService_Model_FraudMethod $fraudRec */
			$fraudRec = $providerRepository->getById($providerId, false, true);
			if (!is_null($fraudRec))
			{
				$className = $fraudRec->getClassName();
				if (class_exists($className))
				{
					self::$instance = new $className($db, $settings);
					return (self::$instance) ? self::$instance : null;
				}
				else
				{
					return null;
				}

			}
			else
			{
				return null;
			}
		}
		return null;
	}

	public function getSettings()
	{
		return $this->settings;
	}

	/**
	 * Order fraud verification
	 *
	 * @param $params
	 * @param $format // request  type (json, xml .....)
	 *
	 * @return bool
	 */
	public function checkOrder($order)
	{
		return $this->checkOrderDefaultResult;
	}

	public function reviewStatusAction($checkResult)
	{
		return false;
	}

	public function rejectStatusAction($checkResult)
	{
		return false;
	}

	public function approveOrderAction($oid, $admin_log_id = 0)
	{
		return false;
	}

	public function denyOrderAction($oid, $admin_log_id = 0)
	{
		return false;
	}

	/**
	 * @return string
	 */
	public function getLicenseKey()
	{
		return $this->licensekey;
	}

	/**
	 * @param string $licensekey
	 */
	public function setLicenseKey($licensekey)
	{
		$this->licensekey = $licensekey;
	}

	/**
	 * @return string
	 */
	public function getFormat()
	{
		return $this->format;
	}

	/**
	 * @param string $format
	 */
	public function setFormat($format)
	{
		$this->format = $format;
	}

	/**
	 * @return mixed
	 */
	public function getVerificationParams()
	{
		return $this->_verificationParams;
	}

	/**
	 * @param mixed $verificationParams
	 */
	public function setVerificationParams($verificationParams)
	{
		if (is_array($verificationParams))
		{
			$this->_verificationParams = http_build_query($verificationParams);

		}
		else
		{
			$this->_verificationParams = $verificationParams;
		}
	}

	/**
	 * @return mixed
	 */
	public function getVerificationResponse()
	{
		return $this->_verificationResponse;
	}

	/**
	 * @param mixed $verificationResponse
	 */
	public function setVerificationResponse($verificationResponse)
	{
		$this->_verificationResponse = $verificationResponse;
	}

	public function loadProvider($providerClass)
	{
		$className = self::route . $providerClass . '_' . $providerClass . 'Provider';
		$instance  = null;
		if (class_exists($className))
		{
			$instance       = new $className($this->db, $this->settings);
			$this->instance = $instance;
		}
		return ($instance) ? $instance : null;
	}

	public function isScoreImgEnabled()
	{
		return false;
	}

	public function getScoreImgUrl($score = self::RISK_SCORE_MIN)
	{
		return false;
	}

	public function getTransactionDataForm($data)
	{
		return false;
	}

	public function getSettingsEditingForm()
	{
		return false;
	}

	/**
	 * @return string
	 */
	public function getCheckOrderUrl()
	{
		return $this->checkOrderUrl;
	}

	/**
	 * @param string $checkOrderUrl
	 */
	public function setCheckOrderUrl($checkOrderUrl)
	{
		$this->checkOrderUrl = $checkOrderUrl;
	}

	/**
	 * @return mixed
	 */
	public function getScore()
	{
		return $this->_score;
	}

	/**
	 * @param mixed $score
	 */
	public function setScore($score)
	{
		$this->_score = $score;
	}

	/**
	 * @return mixed
	 */
	public function getError()
	{
		return $this->_error;
	}

	/**
	 * @param mixed $error
	 */
	public function setError($error)
	{
		$this->_error = $error;
	}

	/**
	 * @return mixed
	 */
	public function getFraudStatus()
	{
		return $this->_fraudStatus;
	}

	/**
	 * @param mixed $fraudStatus
	 */
	public function setFraudStatus($fraudStatus)
	{
		$this->_fraudStatus = $fraudStatus;
	}

	/**
	 * Write message into log
	 *
	 * @param $action
	 * @param array $values
	 */
	protected function log($action, $values = array())
	{
		if ($this->isLogEnabled())
		{
			if (!is_array($values))
			{
				$values = array($values);
			}

			file_put_contents(
				$this->logFilePath . $this->logFileName,
				date('Y/m/d-H:i:s') . ' - ' . "\n" . (is_array($action) ? array2text($action) : $action) . (count($values) > 0 ? "\n" . array2text($values) : '') . "\n\n\n",
				FILE_APPEND
			);
		}
	}

	public function isLogEnabled()
	{
		return (defined('DEVMODE') && DEVMODE);
	}

	/**
	 * @return FraudService_Model_FraudMethod
	 */
	public function getFraudMethod()
	{
		return $this->fraudMethod;
	}


}
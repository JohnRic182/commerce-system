<?php

/**
 * Class FraudService_Provider_NoFraud_NoFraudProvider
 */
class FraudService_Provider_NoFraud_NoFraudProvider extends FraudService_Provider_FraudServiceProvider
{

	const MYCLASSNAME = 'NoFraud';

	const NOFRAUD_RESULT_PASS   = 'pass';
	const NOFRAUD_RESULT_REVIEW = 'review';
	const NOFRAUD_RESULT_FAIL   = 'fail';
	private $_paramsDefault = array(
		'nf-token' => '',
		'amount' => '0',
		'shippingAmount' => '0',
		'customer' => array(
			'id' => '',
			'email' => ''
		),
		'merchant' => array(
			'name' => '',
			'website' => '',
			'email' => '',
			'productType' => 'Service'
		),
		'order' => array(
			'invoiceNumber' => ''
		),
		'billTo' => array(
			'firstName' => '',
			'lastName' => '',
			'company' => '',
			'address' => '',
			'city' => '',
			'state' => '',
			'zip' => '',
			'country' => '',
			'phoneNumber' => '',
		),
		'shipTo' => array(
			'firstName' => '',
			'lastName' => '',
			'company' => '',
			'address' => '',
			'city' => '',
			'state' => '',
			'zip' => '',
			'country' => '',
		),
		'customerIP' => '',
		'avsResultCode' => '',
		'cvvResultCode' => '0',
		//'referial_code' => '',
		'userFields' => array()
	);

	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		parent::__construct($db, $settings);
		$this->fraudMethodClassName = self::MYCLASSNAME;
		$this->fraudMethod          = $this->providerRepository->getByClassName($this->fraudMethodClassName);
		$this->logFileName          = self::MYCLASSNAME . 'Log.txt';

		if ($this->fraudMethod)
		{
			$methodSettings = $this->fraudMethod->getMethodSettings();

			foreach ($methodSettings as $setting)
			{
				if ($setting['name'] == 'nofraud_ApiKey')
				{
					$this->setLicenseKey($setting['value']);
				}

				if ($setting['name'] == 'nofraud_APIOrderCheckUrl')
				{
					$this->setCheckOrderUrl($setting['value']);
				}

				if ($setting['name'] == 'nofraud_requestType')
				{
					$this->setFormat($setting['value']);
				}
			}
		}
	}

	/**
	 * Order fraud verification
	 *
	 * @param $order
	 * @return bool
	 * @internal param $params
	 */
	public function checkOrder($order)
	{
		$url    = $this->getCheckOrderUrl();

		$oid              = $order->getId();
		$transaction_data = $this->providerRepository->getFraudTransactionForOrder($oid);
		$params = $this->buildQueryParams($order, $transaction_data);
		$_paymentData     = $transaction_data['payment_method_data'];

		$result = $this->sendCheckOrderRequest($url, $params);
		if(isset($params['oid']))
		{
			unset($params['oid']);
		}
		$this->setVerificationParams($params);
		$this->setVerificationResponse($result['response']);
		$this->setFraudStatus($result['fraud_status']);
		$this->setScore($result['score']);
		$this->setError($result['error']);

		$transaction_data                 = $this->providerRepository->getTransactionDefaults();
		$transaction_data['oid']          = $result['oid'];
		$transaction_data['fid']          = $this->fraudMethod->getId();
		$transaction_data['score']        = $this->getScore();
		$transaction_data['request']      = $this->getVerificationParams();
		$transaction_data['response']     = $this->getVerificationResponse();
		$transaction_data['request_type'] = $this->getFormat();
		$transaction_data['fraud_status'] = $this->getFraudStatus();
		$transaction_data['error']        = $this->getError();
		$transaction_data['security_id']  = $result['security_id'];

		$this->log('Call checkOrder: url: ', $url);
		$this->log('Params: ', $params);
		$this->log('Result: ', $transaction_data);

		$data        = unserialize($_paymentData);
		$paymentData = array();
		if ($data)
		{
			if (is_array($data))
			{
				$paymentData                             = $data;
				$paymentData['ip']                       = $_SERVER['REMOTE_ADDR'];
				$cur_card_num = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM];
				if(trim($cur_card_num) != '')
				{
					$paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM] = PaymentProfiles_Model_BillingData::maskCc($cur_card_num);
				}

				$transaction_data['payment_method_data'] = $paymentData;
			}
		}

		$this->providerRepository->persistFraudTransaction($result['oid'], $transaction_data);

		return $result;
	}

	/**
	 * build api request params
	 * @param ORDER $order
	 * @return array
	 */
	private function buildQueryParams($order, $transaction_data = array())
	{
		$result             = $this->_paramsDefault;
		$result['nf-token'] = $this->getLicenseKey();
		$result['format']   = $this->getFormat();
		$settings           = $this->getSettings()->getAll();

		/*  @var ORDER $order */
		if ($order)
		{
			/*  @var USER $user */
			$user                        = $order->user();
			$user_data                   = $user->getUserData();

			$paymentData = array();
			if ($transaction_data && is_array($transaction_data) && isset($transaction_data['payment_method_data']))
			{
				$data = unserialize($transaction_data['payment_method_data']);
				if ($data)
				{
					if (is_array($data))
					{
						$paymentData = $data;
					}
				}
			}

			$result['customer']['id']    = $user_data['uid'];
			$result['customer']['email'] = $user_data['email'];
			$result['customerIP']        = $_SERVER['REMOTE_ADDR'];

			if (trim($settings['CompanyName']) != '')
			{
				$result['merchant']['name'] = $settings['CompanyName'];

				if (trim($settings['CompanyWebsite']))
				{
					$patterns    = array();
					$patterns[0] = '/https:\/\//';
					$patterns[1] = '/http:\/\//';

					$replacements                  = array();
					$replacements[2]               = '';
					$replacements[1]               = '';
					$company                       = preg_replace($patterns, $replacements, trim($settings['CompanyWebsite']));
					$result['merchant']['website'] = $company;
				}
				else
				{
					unset($result['merchant']['website']);
				}

				if (trim($settings['CompanyEmail']))
				{
					$result['merchant']['email'] = $settings['CompanyEmail'];
				}
				else
				{
					unset($result['merchant']['email']);
				}
			}
			else
			{
				unset($result['merchant']);
			}


			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($order->getId());

			$result['oid'] = $order->getId();

			// -----------------------------------------------------------------------
			if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_AVS]))
			{
				$result['avsResultCode'] = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_AVS];
			}
			else
			{
				unset($result['avsResultCode']);
			}

			if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CVV]))
			{
				$code = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CVV];
				$result['cvvResultCode'] = ($code)?$code:'0';
			}
			else
			{
				unset($result['cvvResultCode']);
			}

			if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM]))
			{
				$ccNumber = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM];
				$result['payment']['creditCard']['cardNumber'] = $ccNumber;
				$result['payment']['creditCard']['last4'] = substr($ccNumber, -4);
				if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDCVV2]))
				{
					$result['payment']['creditCard']['cardCode'] = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDCVV2];
				}
				$expDate = '';
				if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPMONTH]))
				{
					$expDate = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPMONTH];
				}
				if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPYEAR]))
				{
					$expDate .= substr($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDEXPYEAR], -2);
				}
				if($expDate)
				{
					$result['payment']['creditCard']['expirationDate'] = $expDate;
				}
			}


			$result['amount'] = number_format($order->getTotalAmount(), 2, ".", "");;
			$result['shippingAmount'] = number_format($order->getShippingAmount(), 2, ".", "");;;

			$result['order']['invoiceNumber'] = $order->getOrderNumber();


			$result['billTo']['lastName']    = $user_data['lname'];
			$result['billTo']['firstName']   = $user_data['fname'];
			$result['billTo']['company']     = $user_data['company'];
			$result['billTo']['address']     = trim($user_data['address1'] . ' ' . $user_data['address2']);
			$result['billTo']['city']        = $user_data['city'];
			$result['billTo']['state']       = ($user_data['state_abbr']) ? $user_data['state_abbr'] : $user_data['province'];
			$result['billTo']['zip']         = $user_data['zip'];
			$result['billTo']['country']     = $user_data['country_iso_a2'];
			$result['billTo']['phoneNumber'] = $user_data['phone'];

			$shippingAddress = $order->getShippingAddress();
			$matches         = array();
			$r               = preg_match('/^([a-zA-Z]+)\s([a-zA-Z]+)/', $shippingAddress['name'], $matches);

			$result['shipTo']['firstName'] = isset($matches[1]) ? $matches[1] : '';
			$result['shipTo']['lastName']  = isset($matches[2]) ? $matches[2] : '';
			$result['shipTo']['company']   = $shippingAddress['company'];
			$result['shipTo']['address']   = trim($shippingAddress['address1'] . ' ' . $shippingAddress['address2']);
			$result['shipTo']['city']      = $shippingAddress['city'];
			$result['shipTo']['state']     = ($shippingAddress['state_abbr']) ? $shippingAddress['state_abbr'] : $shippingAddress['province'];
			$result['shipTo']['zip']       = $shippingAddress['zip'];
			$result['shipTo']['country']   = $shippingAddress['country_iso_a2'];
		}

		return $result;
	}

	/**
	 * @param $url
	 * @param $params
	 * @return array
	 */
	private function sendCheckOrderRequest($url, $params)
	{
		$result          = self::getCheckOrderDefaultResult();
		$result['score'] = '';
		$result['oid']   = $params['oid'];
		unset($params['oid']);

		if (isset($params['format']))
		{
			$format = $params['format'];
		}
		else
		{
			$format = self::FORMAT_JSON;
		}

		try
		{
			unset($params['format']);
			$opts = array();

			$opts[CURLOPT_POST] = true;

			$opts[CURLOPT_POSTFIELDS]     = json_encode($params);
			$opts[CURLOPT_RETURNTRANSFER] = true;
			$opts[CURLOPT_HTTPHEADER]     = array(
				'Content-Type: application/json'
			);
			$opts[CURLOPT_URL]            = $url;
			$settings                     = $this->getSettings()->getAll();
			$opts[CURLOPT_SSL_VERIFYPEER] = 1;
			$opts[CURLOPT_CAPATH]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'];
			$opts[CURLOPT_CAINFO]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'] . '/' . $settings['SecuritySslPem'];

			$c = curl_init();
			curl_setopt_array($c, $opts);
			$buffer = curl_exec($c);
			$info   = curl_getinfo($c);

			if (curl_errno($c) > 0)
			{
				$error_msg          = curl_error($c);
				$result['status']   = self::RESULT_ERROR;
				$result['response'] = $error_msg;
				$result['error']    = $error_msg;
			}
			else
			{
				$result['status']   = self::RESULT_SUCCESS;
				$result['response'] = $buffer;
				$result['format']   = $this::FORMAT_JSON;
				$api_data           = $this->isJson($buffer) ? json_decode($buffer, true) : null;
				if ($api_data)
				{
					if (isset($api_data['Errors']))
					{
						$result['data'] = $api_data;
						$errors         = '';
						foreach ($api_data['Errors'] as $_error)
						{
							$errors .= $_error . ";<br>";
						}
						$result['status'] = self::RESULT_ERROR;
						$result['error']  = $errors;
					}
					else
					{
						$result['security_id']  = $api_data['id'];
						$result['fraud_status'] = $this->getCommonFraudStatus($api_data['decision']);
					}
				}
				else
				{

					$result['data']   = $buffer;
					$result['status'] = self::RESULT_ERROR;
					$result['error']  = $buffer;
				}
			}
		} catch (Exception $e)
		{
			$result['error'] = $e->getMessage();
		}

		return $result;
	}

	/**
	 * @param $string
	 * @return bool
	 */
	private function isJson($string)
	{
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * function should return one of the common statuses :
	 *    FraudService_Provider_FraudServiceProvider::STATUS_APPROVE
	 *    FraudService_Provider_FraudServiceProvider::STATUS_REJECT
	 *    FraudService_Provider_FraudServiceProvider::STATUS_REVIEW
	 * @param $value
	 */
	private function getCommonFraudStatus($value)
	{
		if (strtolower(trim($value)) == $this::NOFRAUD_RESULT_PASS)
		{
			return FraudService_Provider_FraudServiceProvider::STATUS_APPROVE;
		}
		elseif (strtolower(trim($value)) == $this::NOFRAUD_RESULT_REVIEW)
		{
			return FraudService_Provider_FraudServiceProvider::STATUS_REVIEW;
		}
		else
		{
			return FraudService_Provider_FraudServiceProvider::STATUS_REJECT;
		}
	}

	/**
	 * @return bool
	 */
	public function isScoreImgEnabled()
	{
		return false;
	}

	/**
	 * @param $transactionId
	 * @return bool|core_Form_Form
	 */
	public function getTransactionDataForm($transactionId)
	{
		$form        = false;
		$transaction = $this->providerRepository->getFraudTransactionById($transactionId);
		if ($transaction)
		{
			if ($transaction['request_type'] == self::FORMAT_JSON)
			{
				if ($this->isJson($transaction['response']))
				{
					$data = json_decode($transaction['response'], true);
				}
				else
				{
					$data = $transaction['response'];
				}
			}

			if ($data)
			{
				/** @var core_Form_FormBuilder $formBuilder */
				$formBuilder = new core_Form_FormBuilder('fraud-transaction');

				$paramsGroup = new core_Form_FormBuilder('fraud-transaction-params', array('label' => 'fraud_methods.fraud_transaction', 'collapsible' => false));
				$formBuilder->add($paramsGroup);

				if (isset($transaction['admin_log_data']) && is_array($transaction['admin_log_data']))
				{
					$log             = $transaction['admin_log_data'];
					$adminProcessLog = new core_Form_FormBuilder('admin-actions', array('label' => 'Admin Action'));
					$paramsGroup->add($adminProcessLog);

					$adminProcessLog->add('NoFraudpro_admin_date', 'static', array('label' => 'Action Date: ',
																				   'value' => $log['log_date']));

					$adminProcessLog->add('NoFraudpro_admin_name', 'static', array('label' => 'Username: ',
																				   'value' => $log['username']));

					$adminProcessLog->add('NoFraudpro_admin_message', 'static', array('label' => 'Action: ',
																					  'value' => $log['message']));
				}

				if ((trim($transaction['error']) == ''))
				{
					$commonGroup = new core_Form_FormBuilder('common-info', array('label' => 'Common Info'));
					$paramsGroup->add($commonGroup);

					$commonGroup->add('NoFraudpro_status', 'static', array('label' => 'NoFraud Transaction Status ',
																		   'value' => $data['decision']));

					$commonGroup->add('NoFraudpro_transaction_id', 'static', array('label' => 'No Fraud Transaction ID ',
																				   'value' => $data['id']));
				}
				else
				{
					$commonGroup = new core_Form_FormBuilder('billing-info', array('label' => 'Common Info'));
					$paramsGroup->add($commonGroup);

					$commonGroup->add('NoFraudpro_message', 'static', array('label' => '<font style="color: red">' . 'Transaction Error' . '</font>',
																			'value' => '<code>' . $transaction['error'] . '</code>',
					));

				}
				$form = $formBuilder->getForm();
			}
		}

		return $form;

	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getSettingsEditingForm()
	{
		/** @var core_Form_FormBuilder $formBuilder */
		$formBuilder = new core_Form_FormBuilder('fraud-provider-settings');

		$settingsGroup = new core_Form_FormBuilder('fraud-provider-settings-rules', array('label' => 'fraud_methods.statuses_rules', 'collapsible' => true));
		$formBuilder->add($settingsGroup);
		$methodSettings = $this->fraudMethod->getMethodSettings(true);

		$paymentStatusOptions                                 = array();
		$paymentStatusOptions['default']                      = 'No Change';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_REVIEW]   = 'Review';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_PENDING]  = 'Pending';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_RECEIVED] = 'Received';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_DECLINED] = 'Declined';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_CANCELED] = 'Canceled';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_ERROR]    = 'Error';


		$orderStatusOptions                         = array();
		$orderStatusOptions['default']              = 'No Change';
		$orderStatusOptions[ORDER::STATUS_PROCESS]  = 'Process';
		$orderStatusOptions[ORDER::STATUS_CANCELED] = 'Canceled';
		$orderStatusOptions[ORDER::STATUS_FAILED]   = 'Failed';
		$orderStatusOptions[ORDER::STATUS_ABANDON]  = 'Abandon';


		$settingsGroup->add('nofraud_reviewStatus_label', 'static', array('label' => 'fraud_methods.on_review_status_received'));

		$settingsGroup->add('form[nofraud_reviewOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['nofraud_reviewOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[nofraud_reviewPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['nofraud_reviewPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));


		$settingsGroup->add('nofraud_rejectStatus_label', 'static', array('label' => 'fraud_methods.on_reject_status_received'));

		$settingsGroup->add('form[nofraud_rejectOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['nofraud_rejectOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[nofraud_rejectPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['nofraud_rejectPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));


		$settingsGroup->add('nofraud_approveReviewStatus_label', 'static', array('label' => 'fraud_methods.on_order_approve_action'));

		$settingsGroup->add('form[nofraud_approveReviewOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['nofraud_approveReviewOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[nofraud_approveReviewPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['nofraud_approveReviewPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));


		$settingsGroup->add('nofraud_denyReviewStatus_label', 'static', array('label' => 'fraud_methods.on_order_deny_action'));

		$settingsGroup->add('form[nofraud_denyReviewOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['nofraud_denyReviewOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[nofraud_denyReviewPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['nofraud_denyReviewPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));

		return $formBuilder;
	}

	/**
	 * @param $checkResult
	 * @return mixed
	 */
	public function reviewStatusAction($checkResult)
	{
		$oid         = $checkResult['oid'];
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = $checkResult;
		if ($oid && $fid && $transaction)
		{
			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($oid);

			$oldOrderStatus   = $order_data['status'];
			$oldPaymentStatus = $order_data['payment_status'];


			$methodSettings = $this->fraudMethod->getMethodSettings(true);

			$reviewOrderStatus = $methodSettings ['nofraud_reviewOrderStatus']['value'];
			$newOrderStatus    = ($reviewOrderStatus != 'default') ? $reviewOrderStatus : $oldOrderStatus;

			$reviewPaymentStatus = $methodSettings ['nofraud_reviewPaymentStatus']['value'];
			$newPaymentStatus    = ($reviewPaymentStatus != 'default') ? $reviewPaymentStatus : $oldPaymentStatus;

			if ($oldOrderStatus != $newOrderStatus)
			{
				$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
			}

			if ($oldPaymentStatus != $newPaymentStatus)
			{
				$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
			}

			$transaction['order_prev_status']   = $oldOrderStatus;
			$transaction['payment_prev_status'] = $oldPaymentStatus;
			$transaction['order_new_status']    = $newOrderStatus;
			$transaction['payment_new_status']  = $newPaymentStatus;
			$this->providerRepository->persistFraudTransaction($oid, $transaction);
			$result['oldOrderStatus']   = $oldOrderStatus;
			$result['oldPaymentStatus'] = $oldPaymentStatus;
			$result['newOrderStatus']   = $newOrderStatus;
			$result['newPaymentStatus'] = $newPaymentStatus;
		}
		$this->log('Call ', 'reviewStatusAction');
		$this->log('Result: ', $transaction);
		return $result;
	}

	/**
	 * @param $checkResult
	 * @return mixed
	 */
	public function rejectStatusAction($checkResult)
	{
		$oid         = $checkResult['oid'];
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = $checkResult;
		if ($oid && $fid && $transaction)
		{
			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($oid);

			$oldOrderStatus   = $order_data['status'];
			$oldPaymentStatus = $order_data['payment_status'];

			$methodSettings = $this->fraudMethod->getMethodSettings(true);

			$reviewOrderStatus = $methodSettings ['nofraud_rejectOrderStatus']['value'];
			$newOrderStatus    = ($reviewOrderStatus != 'default') ? $reviewOrderStatus : $oldOrderStatus;

			$reviewPaymentStatus = $methodSettings ['nofraud_rejectPaymentStatus']['value'];
			$newPaymentStatus    = ($reviewPaymentStatus != 'default') ? $reviewPaymentStatus : $oldPaymentStatus;

			if ($oldOrderStatus != $newOrderStatus)
			{
				$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
			}

			if ($oldPaymentStatus != $newPaymentStatus)
			{
				$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
			}

			$transaction['order_prev_status']   = $oldOrderStatus;
			$transaction['payment_prev_status'] = $oldPaymentStatus;
			$transaction['order_new_status']    = $newOrderStatus;
			$transaction['payment_new_status']  = $newPaymentStatus;
			$this->providerRepository->persistFraudTransaction($oid, $transaction);

			$result['oldOrderStatus']   = $oldOrderStatus;
			$result['oldPaymentStatus'] = $oldPaymentStatus;
			$result['newOrderStatus']   = $newOrderStatus;
			$result['newPaymentStatus'] = $newPaymentStatus;
		}

		$this->log('Call ', 'rejectStatusAction');
		$this->log('Result: ', $transaction);
		return $result;
	}

	/**
	 * @param $oid
	 * @param int $admin_log_id
	 * @return array
	 */
	public function approveOrderAction($oid, $admin_log_id = 0)
	{
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = self::getDefaultResult();
		if ($oid && $fid && $transaction)
		{

			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($oid);

			$oldOrderStatus   = $order_data['status'];
			$oldPaymentStatus = $order_data['payment_status'];

			$methodSettings = $this->fraudMethod->getMethodSettings(true);

			$approveOrderStatus = $methodSettings ['nofraud_approveReviewOrderStatus']['value'];
			$newOrderStatus     = ($approveOrderStatus != 'default') ? $approveOrderStatus : $oldOrderStatus;

			$approvePaymentStatus = $methodSettings ['nofraud_approveReviewPaymentStatus']['value'];
			$newPaymentStatus     = ($approvePaymentStatus != 'default') ? $approvePaymentStatus : $oldPaymentStatus;

			if ($oldOrderStatus != $newOrderStatus)
			{
				$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
			}

			if ($oldPaymentStatus != $newPaymentStatus)
			{
				$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
			}

			if ($admin_log_id)
			{
				$transaction['admin_log_id'] = $admin_log_id;
				$this->providerRepository->persistFraudTransaction($oid, $transaction);
			}

			$result['oldOrderStatus']   = $oldOrderStatus;
			$result['oldPaymentStatus'] = $oldPaymentStatus;
			$result['newOrderStatus']   = $newOrderStatus;
			$result['newPaymentStatus'] = $newPaymentStatus;
		}

		$this->log('Call ', 'approveOrderAction');
		$this->log('Result: ', $result);
		return $result;
	}

	/**
	 * @param $oid
	 * @param int $admin_log_id
	 * @return array
	 */
	public function denyOrderAction($oid, $admin_log_id = 0)
	{
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = self::getDefaultResult();
		if ($oid && $fid && $transaction)
		{
			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($oid);

			$oldOrderStatus   = $order_data['status'];
			$oldPaymentStatus = $order_data['payment_status'];

			$methodSettings = $this->fraudMethod->getMethodSettings(true);

			$approveOrderStatus = $methodSettings ['nofraud_denyReviewOrderStatus']['value'];
			$newOrderStatus     = ($approveOrderStatus != 'default') ? $approveOrderStatus : $oldOrderStatus;

			$approvePaymentStatus = $methodSettings ['nofraud_denyReviewPaymentStatus']['value'];
			$newPaymentStatus     = ($approvePaymentStatus != 'default') ? $approvePaymentStatus : $oldPaymentStatus;

			if ($oldOrderStatus != $newOrderStatus)
			{
				$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
			}

			if ($oldPaymentStatus != $newPaymentStatus)
			{
				$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
			}

			if ($admin_log_id)
			{
				$transaction['admin_log_id'] = $admin_log_id;
				$this->providerRepository->persistFraudTransaction($oid, $transaction);
			}

			$result['oldOrderStatus']   = $oldOrderStatus;
			$result['oldPaymentStatus'] = $oldPaymentStatus;
			$result['newOrderStatus']   = $newOrderStatus;
			$result['newPaymentStatus'] = $newPaymentStatus;
		}
		$this->log('Call ', 'denyOrderAction');
		$this->log('Result: ', $result);

		return $result;
	}

}
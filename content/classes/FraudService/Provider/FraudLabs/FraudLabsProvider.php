<?php

/**
 * Class FraudService_Provider_FraudLabs_FraudLabsProvider
 */
class FraudService_Provider_FraudLabs_FraudLabsProvider extends FraudService_Provider_FraudServiceProvider
{

	const MYCLASSNAME = 'FraudLabs';

	const FEEDBACK_ORDER_ERROR_INVALID_ID     = 301;
	const FEEDBACK_ORDER_ERROR_READ_ONLY      = 302;
	const FEEDBACK_ORDER_ERROR_INVALID_ACTION = 303;

	private $_scoreImgUrl = 'https://fraudlabspro.hexa-soft.com/images/fraudscore/fraudlabsproscore%score%.png';

	private $feedBackUrl = 'https://api.fraudlabspro.com/v1/order/feedback';

	private $AVSCodes = array(
		'A' => 'Street address matches, but 5-digit and 9-digit postal code do not match.',
		'B' => 'Street address matches, but postal code not verified.',
		'C' => 'Street address and postal code do not match.',
		'D' => 'Street address and postal code match.',
		'E' => 'AVS is invalid or not allowed for this card type.',
		'G' => 'Non-U.S. issuing bank does not participate.',
		'I' => 'Address not verified.',
		'M' => 'Street address and postal code match.',
		'N' => 'Street address and postal code do not match.',
		'P' => 'Postal code matches, but street address not verified.',
		'R' => 'System unavailable.',
		'S' => 'Bank does not support AVS.',
		'U' => 'System unavilable.',
		'W' => 'Street address does not match, but 9-digit postal code matches.',
		'X' => 'Street address and 9-digit postal code match.',
		'Y' => 'Street address and 5-digit postal code match.',
		'Z' => 'Street address does not match, but 5-digit postal code match.',
	);

	private $CVV2Codes = array(
		'I' => 'Invalid',
		'M' => 'CVV2 match',
		'N' => 'CVV2 not match.',
		'P' => 'Not processed.',
		'S' => 'Service not supported or CVV2 not present on the card.',
		'U' => 'Service not available',
	);

	private $_paramsDefault = array('ip' => '',
									'key' => '',
									'format' => 'json',
									'last_name' => '',
									'first_name' => '',
									'bill_city' => '',
									'bill_state' => '',
									'bill_country' => '',
									'bill_zip_code' => '',
									'ship_addr' => '',
									'ship_city' => '',
									'ship_state' => '',
									'ship_country' => '',
									'ship_zip_code' => '',
									'email_domain' => '',
									'user_phone' => '',
									'email' => '',
									'email_hash' => '',
									'username_hash' => '',
									'password_hash' => '',
									'bin_no' => '',
									'bin_bank_name' => '',
									'bin_bank_phone' => '',
									'bin_bank_country' => '',
									'card_hash' => '',
									'avs_result' => '',
									'cvv_result' => '',
									'user_order_id' => '',
									'user_order_memo' => '',
									'amount' => '',
									'quantity' => '',
									'currency' => '',
									'department' => '',
									'payment_mode' => 'creditcard',// Valid values: creditcard | paypal | googlecheckout | bitcoin | cod | moneyorder | wired | bankdeposit | others.
									'session_id' => '',
									'flp_checksum' => '',
									'source' => 'pinnaclecart',
									'source_version' => '1.0.0');

	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		parent::__construct($db, $settings);
		$this->fraudMethodClassName = self::MYCLASSNAME;
		$this->fraudMethod          = $this->providerRepository->getByClassName($this->fraudMethodClassName);
		$this->logFileName          = self::MYCLASSNAME . 'Log.txt';

		if ($this->fraudMethod)
		{
			$methodSettings = $this->fraudMethod->getMethodSettings();

			foreach ($methodSettings as $setting)
			{
				if ($setting['name'] == 'fraudlabs_ApiKey')
				{
					$this->setLicenseKey($setting['value']);
				}

				if ($setting['name'] == 'fraudlabs_APIOrderCheckUrl')
				{
					$this->setCheckOrderUrl($setting['value']);
				}

				if ($setting['name'] == 'fraudlabs_requestType')
				{
					$this->setFormat($setting['value']);
				}
			}
		}
	}

	/**
	 * Order fraud verification
	 *
	 * @param ORDER $order
	 * @return bool
	 * @internal param $params
	 */
	public function checkOrder($order)
	{
		$result = self::getCheckOrderDefaultResult();
		if ($order)
		{
			//$url = 'https://api.fraudlabspro.com/v1/order/screen';
			$url              = $this->getCheckOrderUrl();
			$oid              = $order->getId();
			$transaction_data = $this->providerRepository->getFraudTransactionForOrder($oid);
			$params           = $this->buildQueryParams($order, $transaction_data);
			$_paymentData     = $transaction_data['payment_method_data'];

			$result = $this->sendCheckOrderRequest($url, $params);
			if (isset($params['oid']))
			{
				unset($params['oid']);
			}
			$this->setVerificationParams($params);
			$this->setVerificationResponse($result['response']);
			$this->setFraudStatus($result['fraud_status']);
			$this->setScore($result['score']);
			$this->setError($result['error']);

			$transaction_data                 = $this->providerRepository->getTransactionDefaults();
			$transaction_data['fid']          = $this->fraudMethod->getId();
			$transaction_data['score']        = $this->getScore();
			$transaction_data['request']      = $this->getVerificationParams();
			$transaction_data['response']     = $this->getVerificationResponse();
			$transaction_data['request_type'] = $this->getFormat();
			$transaction_data['fraud_status'] = $this->getFraudStatus();
			$transaction_data['error']        = $this->getError();
			$transaction_data['security_id']  = $result['security_id'];
			$this->log('Call checkOrder: url: ', $url);
			$this->log('Params: ', $params);
			$this->log('Result: ', $transaction_data);

			$data        = unserialize($_paymentData);
			$paymentData = array();
			if ($data)
			{
				if (is_array($data))
				{
					$paymentData                             = $data;
					$paymentData['ip']                       = $_SERVER['REMOTE_ADDR'];
					$cur_card_num = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM];
					if(trim($cur_card_num) != '')
					{
						$paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM] = PaymentProfiles_Model_BillingData::maskCc($cur_card_num);
					}

					$transaction_data['payment_method_data'] = $paymentData;
				}
			}

			$this->providerRepository->persistFraudTransaction($result['oid'], $transaction_data);
		}

		return $result;
	}

	/**
	 * build api request params
	 * @param ORDER $order
	 * @return array
	 */
	private function buildQueryParams($order, $transaction_data = array())
	{
		$result           = $this->_paramsDefault;
		$result['ip']     = $_SERVER['REMOTE_ADDR'];
		$result['key']    = $this->getLicenseKey();
		$result['format'] = $this->getFormat();

		/*  @var ORDER $order */
		if ($order)
		{
			/*  @var USER $user */
			$user      = $order->user();
			$user_data = $user->getUserData();

			$paymentData = array();
			if ($transaction_data && is_array($transaction_data) && isset($transaction_data['payment_method_data']))
			{
				$data = unserialize($transaction_data['payment_method_data']);
				if ($data)
				{
					if (is_array($data))
					{
						$paymentData = $data;
					}
				}
			}

			// -----------------------------------------------------------------------
			$result['last_name']     = $user_data['lname'];
			$result['first_name']    = $user_data['fname'];
			$result['bill_city']     = $user_data['city'];
			$result['bill_state']    = ($user_data['state_abbr']) ? $user_data['state_abbr'] : $user_data['province'];
			$result['bill_country']  = $user_data['country_iso_a2'];
			$result['bill_zip_code'] = $user_data['zip'];

			// -----------------------------------------------------------------------
			$shippingAddress         = $order->getShippingAddress();
			$result['ship_addr']     = $shippingAddress['address1'];
			$result['ship_city']     = $shippingAddress['city'];
			$result['ship_state']    = ($shippingAddress['state_abbr']) ? $shippingAddress['state_abbr'] : $shippingAddress['province'];
			$result['ship_country']  = $shippingAddress['country_iso_a2'];
			$result['ship_zip_code'] = $shippingAddress['zip'];

			// -----------------------------------------------------------------------
			preg_match('/^[a-zA-Z0-9_.+-]+@([a-zA-Z_-]+(.[a-zA-Z]{2,4})+$)/i', $user_data['email'], $matches);
			if (isset($matches[1]))
			{
				$result['email_domain'] = $matches[1];
			}
			$result['user_phone'] = $user_data['phone'];
			$result['email']      = $user_data['email'];
			$result['email_hash'] = $this->fraudlabspro_hash($user_data['email']);

			$userNameHash = '';
			if (!$user->express)
			{
				$userNameHash = $this->fraudlabspro_hash($user_data['login']);
			}
			else
			{
				$userNameHash = $this->fraudlabspro_hash($user_data['login'] . $user_data['uid']);
			}
			$result['username_hash'] = $userNameHash;

			// -----------------------------------------------------------------------
			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($order->getId());
			$result['amount']    = $order->getTotalAmount();

			$items                   = $order->getOrderItems();
			$result['quantity']      = $order->getItemsCount();
			$result['user_order_id'] = $order->getOrderNumber();
			$result['oid']           = $order->getId();

			// -----------------------------------------------------------------------
			if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM]))
			{
				$result['card_hash'] = $this->fraudlabspro_hash($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM]);
				$result['bin_no']    = substr($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CARDNUM], 0, 6);

			}

			if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_AVS]))
			{
				$result['avs_result'] = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_AVS];
			}

			if (isset($paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CVV]))
			{
				$result['cvv_result'] = $paymentData[FraudService_Events_FraudEvent::FRAUD_SERVICE_PAYMENTS_DATA_CVV];
			}

			// -----------------------------------------------------------------------
			$currencies         = new Currencies($this->db);
			$cur                = $currencies->getCurrentCurrency();
			$result['currency'] = ($cur && isset($cur['code'])) ? $cur['code'] : '';

			$result['session_id'] = $order_data['security_id'];

			$flp_checksum           = isset($_COOKIE['flp_checksum']) ? $_COOKIE['flp_checksum'] : '';
			$result['flp_checksum'] = $flp_checksum;
		}

		return $result;
	}

	private function fraudlabspro_hash($s)
	{
		$hash = 'fraudlabspro_' . $s;
		for ($i = 0; $i < 65536; $i++)
		{
			$hash = sha1('fraudlabspro_' . $hash);
		}

		return $hash;
	}

	private function sendCheckOrderRequest($url, $params)
	{
		$result        = self::getCheckOrderDefaultResult();
		$result['oid'] = $params['oid'];

		if (isset($params['format']))
		{
			$format = $params['format'];
		}
		else
		{
			$format = self::FORMAT_JSON;
		}

		try
		{
			$opts  = array();
			$query = '';
			if (count($params) > 0)
			{
				$_params = $params;
				unset($_params['oid']);
				$query = '?' . http_build_query($_params);
			}

			$opts[CURLOPT_RETURNTRANSFER] = true;
			$opts[CURLOPT_URL]            = $url . $query;

			$opts[CURLOPT_VERBOSE]        = false;
			$opts[CURLOPT_HEADER]         = false;
			$settings                     = $this->getSettings()->getAll();
			$opts[CURLOPT_SSL_VERIFYPEER] = 1;
			$opts[CURLOPT_CAPATH]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'];
			$opts[CURLOPT_CAINFO]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'] . '/' . $settings['SecuritySslPem'];

			$c = curl_init();
			curl_setopt_array($c, $opts);
			$buffer = curl_exec($c);
			if (curl_errno($c) > 0)
			{
				$error_msg          = curl_error($c);
				$result['status']   = self::RESULT_ERROR;
				$result['response'] = $error_msg;
				$result['error']    = $error_msg;
			}
			else
			{
				$result['status']   = self::RESULT_SUCCESS;
				$result['response'] = $buffer;
				if ($format == $this::FORMAT_JSON)
				{
					$result['format'] = $this::FORMAT_JSON;
					$api_data         = json_decode($buffer);
					$result['data']   = $api_data;
					$result['score']  = $api_data->fraudlabspro_score;

					$result['fraud_status'] = $this->getCommonFraudStatus($api_data->fraudlabspro_status);
					$result['security_id']  = $api_data->fraudlabspro_id;
					if (intval($api_data->fraudlabspro_error_code))
					{
						$result['status'] = self::RESULT_ERROR;
						$result['error']  = $api_data->fraudlabspro_message;
					}
				}
				elseif ($format == $this::FORMAT_XML)
				{
					$result['format'] = $this::FORMAT_XML;
					/* @var  SimpleXMLElement $api_data */
					$xml                    = simplexml_load_string($buffer, "SimpleXMLElement", LIBXML_NOCDATA);
					$json                   = json_encode($xml);
					$api_data               = json_decode($json, TRUE);
					$result['data']         = $xml;
					$result['score']        = $api_data['fraudlabspro_score'];
					$result['fraud_status'] = $this->getCommonFraudStatus($api_data['fraudlabspro_status']);
					$result['security_id']  = $api_data['fraudlabspro_id'];
					if (intval($api_data['fraudlabspro_error_code']))
					{
						$result['status'] = self::RESULT_ERROR;
						$result['error']  = $api_data['fraudlabspro_message'];
					}
				}
			}
		} catch (Exception $e)
		{
			$result['error'] = $e->getMessage();
		}

		return $result;
	}

	/**
	 * function should return one of the common statuses :
	 *    FraudService_Provider_FraudServiceProvider::STATUS_APPROVE
	 *    FraudService_Provider_FraudServiceProvider::STATUS_REJECT
	 *    FraudService_Provider_FraudServiceProvider::STATUS_REVIEW
	 * @param $value
	 */
	private function getCommonFraudStatus($value)
	{
		if (strtolower(trim($value)) == 'approve')
		{
			return FraudService_Provider_FraudServiceProvider::STATUS_APPROVE;
		}
		elseif (strtolower(trim($value)) == 'review')
		{
			return FraudService_Provider_FraudServiceProvider::STATUS_REVIEW;
		}
		else
		{
			return FraudService_Provider_FraudServiceProvider::STATUS_REJECT;
		}
	}

	public function isScoreImgEnabled()
	{
		return true;
	}

	public function getTransactionDataForm($transactionId)
	{
		$form        = false;
		$transaction = $this->providerRepository->getFraudTransactionById($transactionId);
		if ($transaction)
		{
			$paymentData        = array();
			if (isset($transaction['payment_method_data']))
			{
				$data = unserialize($transaction['payment_method_data']);
				if ($data)
				{
					if (is_array($data))
					{
						$paymentData = $data;
					}
				}
			}
			if ($transaction['request_type'] == self::FORMAT_JSON)
			{
				$data = json_decode($transaction['response'], true);
			}
			elseif ($transaction['request_type'] == self::FORMAT_XML)
			{
				/* @var  SimpleXMLElement $api_data */
				$xml      = simplexml_load_string($transaction['response'], "SimpleXMLElement", LIBXML_NOCDATA);
				$json     = json_encode($xml);
				$api_data = json_decode($json, true);
				$data     = $api_data;
			}

			if ($data)
			{
				/** @var core_Form_FormBuilder $formBuilder */
				$formBuilder = new core_Form_FormBuilder('fraud-transaction');

				$paramsGroup = new core_Form_FormBuilder('fraud-transaction-params', array('label' => 'fraud_methods.fraud_transaction', 'collapsible' => false));
				$formBuilder->add($paramsGroup);

				if (isset($transaction['admin_log_data']) && is_array($transaction['admin_log_data']))
				{
					$log             = $transaction['admin_log_data'];
					$adminProcessLog = new core_Form_FormBuilder('admin-actions', array('label' => 'Admin Action'));
					$paramsGroup->add($adminProcessLog);

					$adminProcessLog->add('fraudlabspro_admin_date', 'static', array('label' => 'Date: ',
						'value' => $log['log_date']));

					$adminProcessLog->add('fraudlabspro_admin_name', 'static', array('label' => 'Username: ',
						'value' => $log['username']));

					$adminProcessLog->add('fraudlabspro_admin_message', 'static', array('label' => 'Action: ',
						'value' => $log['message']));
				}

				if ((trim($transaction['error']) == '') && ($transaction['score'] > 0))
				{
					$commonGroup = new core_Form_FormBuilder('common-info', array('label' => 'Common Info'));
					$paramsGroup->add($commonGroup);

					$commonGroup->add('fraudlabspro_score_img', 'static',
						array(
							'label' => 'FraudLabs Pro Score',
							'value' => '<img src="' . $this->getScoreImgUrl($data['fraudlabspro_score']) . '"/>'
						)
					);

					$commonGroup->add('fraudlabspro_status', 'static', array('label' => 'FraudLabs Pro Status ',
						'value' => $data['fraudlabspro_status'],
						'note' => 'FraudLabs Pro status'));

					$commonGroup->add('fraudlabspro_id', 'static', array('label' => 'Transaction ID ',
						'value' => '<a href="https://www.fraudlabspro.com/merchant/transaction-details/' . $data['fraudlabspro_id'] . '">' . $data['fraudlabspro_id'] . '<a>',
						'note' => 'Unique identifier for a transaction screened by FraudLabs Pro system'));

					$commonGroup->add('fraudlabspro_score', 'static', array('label' => 'FraudLabs Pro Score ',
						'value' => $data['fraudlabspro_score'],
						'note' => 'Risk score, 1 (low risk) - 100 (high risk)'));

					$detailsGroup = new core_Form_FormBuilder('ip-info', array('label' => 'Details'));
					$paramsGroup->add($detailsGroup);

					$_ip = isset($paymentData['ip']) ? $paymentData['ip'] : false;
					$detailsGroup->add('ip_address', 'static', array('label' => 'IP Address',
						'value' => $_ip ? $_ip : '-',
						'note' => 'User IP Address'));

					$ip_location = (trim($data['ip_country']) != '-') ? $data['ip_country'] : '';

					if ($data['ip_country'] && (trim($data['ip_country']) != '-'))
					{
						$countryRepository = new DataAccess_CountriesStatesRepository($this->db);
						if ( $country_rec = $countryRepository->getCountryByIso2Code($data['ip_country']) )
						{
							$ip_location = $country_rec['name'];
						}
					}

					if ($data['ip_region'] && (trim($data['ip_region']) !='-'))
					{
						$ip_location .= ($ip_location != '' ? ', ' : '')  . $data['ip_region'];
					}

					if ($data['ip_city'] && (trim($data['ip_city']) !='-'))
					{
						$ip_location .= ($ip_location != '' ? ', ' : '') . $data['ip_city'];
					}

					if ($ip_location and $_ip)
					{
						$ip_location .= ' [<a href="http://www.geolocation.com/'.$_ip.'">Map</a>]';
					}

					$detailsGroup->add('ip_location', 'static', array('label' => 'IP Location', 'value' => ($ip_location ? $ip_location : '-'),
						'note' => 'Location of the IP address'));

					$detailsGroup->add('ip_netspeed', 'static', array('label' => 'IP Net Speed',
						'value' => $data['ip_netspeed'],
						'note' => 'Connection speed'));

					$detailsGroup->add('ip_isp_name', 'static', array('label' => 'IP ISP Name',
						'value' => $data['ip_isp_name'],
						'note' => 'ISP of the IP address'));

					$detailsGroup->add('is_new_domain_name', 'static', array('label' => 'IP Domain',
						'value' => $data['is_new_domain_name'],
						'note' => 'Domain name of the IP address'
					));

					$detailsGroup->add('ip_usage_type', 'static', array('label' => 'IP Usage Type',
						'value' => $data['ip_usage_type'],
						'note' => 'Usage type of the IP address. E.g, ISP, Commercial, Residential'));

					$detailsGroup->add('ip_timezone', 'static', array('label' => 'IP Time Zone',
						'value' => $data['ip_timezone'],
						'note' => 'Time zone of the IP address'
					));

					$a = '' . $data['distance_in_mile'];
					$detailsGroup->add('distance_in_mile', 'static', array('label' => 'IP Distance', 'value' => $a,
						'note' => 'Distance from IP address to Billing Location'));

					$detailsGroup->add('ip_latitude', 'static', array('label' => 'IP Latitude',
						'value' => $data['ip_latitude'],
						'note' => 'Latitude of the IP address'
					));

					$detailsGroup->add('ip_longitude', 'static', array('label' => 'IP Longitude',
						'value' => $data['ip_longitude'],
						'note' => 'Longitude of the IP address'
					));

					$detailsGroup->add('is_high_risk_country', 'static', array('label' => 'High Risk Country',
						'value' => $data['is_high_risk_country'],
						'note' => 'Whether IP address country is in the latest high risk country list'
					));

					$detailsGroup->add('is_free_email', 'static', array('label' => 'Free Email',
						'value' => $data['is_free_email'],
						'note' => 'Whether e-mail is from free e-mail provider'));

					$detailsGroup->add('is_address_ship_forward', 'static', array('label' => 'Ship Forward',
						'value' => $data['is_address_ship_forward'],
						'note' => 'Whether shipping address is a freight forwarder address'
					));

					$detailsGroup->add('is_proxy_ip_address', 'static', array('label' => 'Using Proxy',
						'value' => $data['is_proxy_ip_address'],
						'note' => 'Whether IP address is from Anonymous Proxy Server'
					));

					$detailsGroup->add('is_bin_found', 'static', array('label' => 'BIN Found',
						'value' => $data['is_bin_found'],
						'note' => 'Whether the BIN information matches our BIN list'
					));

					$detailsGroup->add('is_email_blacklist', 'static', array('label' => 'Email Blacklist',
						'value' => $data['is_email_blacklist'],
						'note' => 'Whether the email address is in our blacklist database'
					));

					$detailsGroup->add('is_credit_card_blacklist', 'static', array('label' => 'Credit Card Blacklist',
						'value' => $data['is_credit_card_blacklist'],
						'note' => 'Whether the credit card is in our blacklist database'
					));

					$detailsGroup->add('fraudlabspro_credits', 'static', array('label' => 'Balance',
						'value' => $data['fraudlabspro_credits'] . ' [<a href="http://www.fraudlabspro.com/pricing?utm_source=module&utm_medium=banner&utm_term=pinnaclecart&utm_campaign=module%20banner">Upgrade</a>]',
						'note' => 'Balance of the credits available after this transaction'));

					$detailsGroup->add('fraudlabspro_message', 'static', array('label' => 'Message',
						'value' => ($data['fraudlabspro_message'] != '' ? $data['fraudlabspro_message'] : '-'),
						'note' => 'FraudLabs Pro error message description'
					));
				}
				else
				{
					$commonGroup = new core_Form_FormBuilder('billing-info', array('label' => 'Common Info'));
					$paramsGroup->add($commonGroup);

					$commonGroup->add('fraudlabspro_error_code', 'static', array('label' => '<font style="color: red">' . 'FraudLabs Pro Transaction Error Code ' . '</font>',
						'value' => $data['fraudlabspro_error_code']));

					$commonGroup->add('fraudlabspro_message', 'static', array('label' => '<font style="color: red">' . 'Message' . '</font>',
						'value' => $data['fraudlabspro_message'],
						'note' => 'FraudLabs Pro error message description'
					));
				}
				$form = $formBuilder->getForm();
			}
		}

		return $form;
	}

	public function getTransactionDataFormLegacyFullDetails($transactionId)
	{
		$form        = false;
		$transaction = $this->providerRepository->getFraudTransactionById($transactionId);
		if ($transaction)
		{
			$paymentData        = array();
			if (isset($transaction['payment_method_data']))
			{
				$data = unserialize($transaction['payment_method_data']);
				if ($data)
				{
					if (is_array($data))
					{
						$paymentData = $data;
					}
				}
			}
			if ($transaction['request_type'] == self::FORMAT_JSON)
			{
				$data = json_decode($transaction['response'], true);
			}
			elseif ($transaction['request_type'] == self::FORMAT_XML)
			{
				/* @var  SimpleXMLElement $api_data */
				$xml      = simplexml_load_string($transaction['response'], "SimpleXMLElement", LIBXML_NOCDATA);
				$json     = json_encode($xml);
				$api_data = json_decode($json, true);
				$data     = $api_data;
			}

			if ($data)
			{
				/** @var core_Form_FormBuilder $formBuilder */
				$formBuilder = new core_Form_FormBuilder('fraud-transaction');

				$paramsGroup = new core_Form_FormBuilder('fraud-transaction-params', array('label' => 'fraud_methods.fraud_transaction', 'collapsible' => false));
				$formBuilder->add($paramsGroup);

				if (isset($transaction['admin_log_data']) && is_array($transaction['admin_log_data']))
				{
					$log             = $transaction['admin_log_data'];
					$adminProcessLog = new core_Form_FormBuilder('admin-actions', array('label' => 'Admin Action'));
					$paramsGroup->add($adminProcessLog);

					$adminProcessLog->add('fraudlabspro_admin_date', 'static', array('label' => 'Date: ',
																					 'value' => $log['log_date']));

					$adminProcessLog->add('fraudlabspro_admin_name', 'static', array('label' => 'Username: ',
																					 'value' => $log['username']));

					$adminProcessLog->add('fraudlabspro_admin_message', 'static', array('label' => 'Action: ',
																						'value' => $log['message']));
				}

				if ((trim($transaction['error']) == '') && ($transaction['score'] > 0))
				{
					$commonGroup = new core_Form_FormBuilder('common-info', array('label' => 'Common Info'));
					$paramsGroup->add($commonGroup);

					$commonGroup->add('fraudlabspro_score_img', 'static',
						array(
							'label' => 'FraudLabs Pro Score',
							'value' => '<img src="' . $this->getScoreImgUrl($data['fraudlabspro_score']) . '"/>'
						)
					);

					$commonGroup->add('fraudlabspro_status', 'static', array('label' => 'FraudLabs Pro Status ',
																			 'value' => $data['fraudlabspro_status'],
																			 'note' => 'FraudLabs Pro status'));

					$commonGroup->add('fraudlabspro_id', 'static', array('label' => 'Transaction ID ',
																		 'value' => '<a href="https://www.fraudlabspro.com/merchant/transaction/' . $data['fraudlabspro_id'] . '">' . $data['fraudlabspro_id'] . '<a>',
																		 'note' => 'Unique identifier for a transaction screened by FraudLabs Pro system'));

					$commonGroup->add('fraudlabspro_score', 'static', array('label' => 'FraudLabs Pro Score ',
																			'value' => $data['fraudlabspro_score'],
																			'note' => 'Risk score, 1 (low risk) - 100 (high risk)'));

					$commonGroup->add('fraudlabspro_distribution', 'static', array('label' => 'Distribution Of The Risk Rate',
																				   'value' => $data['fraudlabspro_distribution'],
																				   'note' => "The distribution of the risk rate range from 1 to 100. Distribution score of 90 means it is at top 10% high score in sample"));

					$commonGroup->add('fraudlabspro_message', 'static', array('label' => 'Message',
																			  'value' => $data['fraudlabspro_message'],
																			  'note' => 'FraudLabs Pro error message description'
					));

					$ipGroup = new core_Form_FormBuilder('ip-info', array('label' => 'User IP Info'));
					$paramsGroup->add($ipGroup);

					$_ip = isset($paymentData['ip']) ? $paymentData['ip'] : false;
					$ipGroup->add('ip_address', 'static', array('label' => 'IP Address',
																   'value' => $_ip ? $_ip : '-',
																   'note' => 'User IP Address'));

					$ip_location = (trim($data['ip_country']) !='-') ? $data['ip_country'] : '';

					if ($data['ip_country'] && (trim($data['ip_country']) !='-'))
					{
						$country_rec = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'countries WHERE iso_a2 LIKE "'.$this->db->escape($data['ip_country']).'"');
						if ($country_rec)
						{
							$ip_location = $country_rec['name'];
						}
					}

					if ($data['ip_region'] && (trim($data['ip_region']) !='-'))
					{
						$ip_location .= ', ' . $data['ip_region'];
					}

					if ($data['ip_city'] && (trim($data['ip_city']) !='-'))
					{
						$ip_location .= ', '.$data['ip_city'];
					}

					if ($ip_location)
					{
						if ($_ip)
						{
							$ip_location .= ' [<a href="http://www.geolocation.com/'.$_ip.'">Map</a>]';
						}
					}

					$ipGroup->add('ip_location', 'static', array('label' => 'IP Location', 'value' => ($ip_location ? $ip_location : '-'),
																 'note' => 'Location of the IP address'));

					$ipGroup->add('is_free_email', 'static', array('label' => 'Free Email',
																   'value' => $data['is_free_email'],
																   'note' => 'Whether e-mail is from free e-mail provider'));

					$ipGroup->add('is_email_blacklist', 'static', array('label' => 'Email Blacklist',
																		'value' => $data['is_email_blacklist'],
																		'note' => 'Whether the email address is in our blacklist database'
					));

					$ipGroup->add('is_new_domain_name', 'static', array('label' => 'IP Domain',
																		'value' => $data['is_new_domain_name'],
																		'note' => 'Domain name of the IP address'
					));

					$ipGroup->add('is_country_match', 'static', array('label' => 'Whether country of IP address matches billing address country?',
																	  'value' => $data['is_country_match']));

					$ipGroup->add('is_high_risk_country', 'static', array('label' => 'High Risk Country',
																		  'value' => $data['is_high_risk_country'],
																		  'note' => 'Whether IP address country is in the latest high risk country list'
					));

					$a = '' . $data['distance_in_mile'];
					$ipGroup->add('distance_in_mile', 'static', array('label' => 'IP Distance', 'value' => $a,
																	  'note' => 'Distance from IP address to Billing Location'));

					$ipGroup->add('ip_isp_name', 'static', array('label' => 'IP ISP Name',
																 'value' => $data['ip_isp_name'],
																 'note' => 'ISP of the IP address'));

					$ipGroup->add('ip_usage_type', 'static', array('label' => 'IP Usage Type',
																   'value' => $data['ip_usage_type'],
																   'note' => 'Usage type of the IP address. E.g, ISP, Commercial, Residential'));

					$ipGroup->add('is_ip_blacklist', 'static', array('label' => 'Whether the IP address is in blacklist database',
																	 'value' => $data['is_ip_blacklist']));

					$ipGroup->add('is_device_blacklist', 'static', array('label' => 'Is Device in Blacklist?',
																		 'value' => $data['is_device_blacklist']));


					$ipGroup->add('ip_latitude', 'static', array('label' => 'IP Latitude',
																 'value' => $data['ip_latitude'],
																 'note' => 'Latitude of the IP address'
					));

					$ipGroup->add('ip_longitude', 'static', array('label' => 'IP Longitude',
																  'value' => $data['ip_longitude'],
																  'note' => 'Longitude of the IP address'
					));

					$ipGroup->add('ip_timezone', 'static', array('label' => 'IP Time Zone',
																 'value' => $data['ip_timezone'],
																 'note' => 'Time zone of the IP address'
					));

					$ipGroup->add('ip_elevation', 'static', array('label' => 'Estimated elevation of the IP address',
																  'value' => $data['ip_elevation']));

					$ipGroup->add('ip_domain', 'static', array('label' => 'Estimated domain name of the IP address',
															   'value' => $data['ip_domain']));

					$ipGroup->add('is_proxy_ip_address', 'static', array('label' => 'Using Proxy',
																		 'value' => $data['is_proxy_ip_address'],
																		 'note' => 'Whether IP address is from Anonymous Proxy Server'
					));

					$ipGroup->add('ip_mobile_mnc', 'static', array('label' => 'Estimated mobile mcc information of the IP address, if it is a mobile network',
																   'value' => $data['ip_mobile_mnc']));

					$ipGroup->add('ip_mobile_mcc', 'static', array('label' => 'Estimated mobile mcc information of the IP address, if it is a mobile network',
																   'value' => $data['ip_mobile_mcc']));

					$ipGroup->add('ip_mobile_brand', 'static', array('label' => 'Estimated mobile brand information of the IP address, if it is a mobile network',
																	 'value' => $data['ip_mobile_brand']));

					$ipGroup->add('ip_netspeed', 'static', array('label' => 'IP Net Speed',
																 'value' => $data['ip_netspeed'],
																 'note' => 'Connection speed'));

					$billingGroup = new core_Form_FormBuilder('billing-info', array('label' => 'Billing Info'));
					$paramsGroup->add($billingGroup);

					$billingGroup->add('is_credit_card_blacklist', 'static', array('label' => 'Credit Card Blacklist',
																				   'value' => $data['is_credit_card_blacklist'],
																				   'note' => 'Whether the credit card is in our blacklist database'
					));

					$billingGroup->add('is_bin_found', 'static', array('label' => 'BIN Found',
																	   'value' => $data['is_bin_found'],
																	   'note' => 'Whether the BIN information matches our BIN list'
					));

					$billingGroup->add('is_bin_country_match', 'static', array('label' => 'Whether the country of issuing bank matches BIN country code given by user',
																			   'value' => $data['is_bin_country_match']));

					$billingGroup->add('is_bin_name_match', 'static', array('label' => 'Whether the name of issuing bank matches BIN bank name given by user',
																			'value' => $data['is_bin_name_match']));

					$billingGroup->add('is_bin_phone_match', 'static', array('label' => 'Whether the customer service phone number matches BIN phone given by user',
																			 'value' => $data['is_bin_phone_match']));

					$billingGroup->add('is_bin_prepaid', 'static', array('label' => 'Whether the credit card is a type of prepaid card',
																		 'value' => $data['is_bin_prepaid']));

					$billingGroup->add('is_address_ship_forward', 'static', array('label' => 'Ship Forward',
																				  'value' => $data['is_address_ship_forward'],
																				  'note' => 'Whether shipping address is a freight forwarder address'
					));

					$billingGroup->add('is_bill_ship_city_match', 'static', array('label' => 'Whether the billing city matches the shipping city',
																				  'value' => $data['is_bill_ship_city_match']));

					$billingGroup->add('is_bill_ship_state_match', 'static', array('label' => 'Whether the billing state matches the shipping state',
																				   'value' => $data['is_bill_ship_state_match']));

					$billingGroup->add('is_bill_ship_country_match', 'static', array('label' => 'Whether the billing country matches the shipping country',
																					 'value' => $data['is_bill_ship_country_match']));

					$billingGroup->add('is_bill_ship_postal_match', 'static', array('label' => 'Whether the billing postal/zip code matches the shipping postal/zip code',
																					'value' => $data['is_bill_ship_postal_match']));
				}
				else
				{
					$commonGroup = new core_Form_FormBuilder('billing-info', array('label' => 'Common Info'));
					$paramsGroup->add($commonGroup);

					$commonGroup->add('fraudlabspro_error_code', 'static', array('label' => '<font style="color: red">' . 'FraudLabs Pro Transaction Error Code ' . '</font>',
																				 'value' => $data['fraudlabspro_error_code']));

					$commonGroup->add('fraudlabspro_message', 'static', array('label' => '<font style="color: red">' . 'Message' . '</font>',
																			  'value' => $data['fraudlabspro_message'],
																			  'note' => 'FraudLabs Pro error message description'
					));

				}
				$form = $formBuilder->getForm();
			}
		}

		return $form;

	}

	/**
	 * @param int $score
	 * @return mixed
	 */
	public function getScoreImgUrl($score = self::RISK_SCORE_MIN)
	{
		if ($score > 0)
		{
			$imgUrl = str_replace('%score%', $score, $this->_scoreImgUrl);
			return $imgUrl;
		}
		else
		{
			return false;
		}
	}

	public function getSettingsEditingForm()
	{
		/** @var core_Form_FormBuilder $formBuilder */
		$formBuilder = new core_Form_FormBuilder('fraud-provider-settings');

		$settingsGroup = new core_Form_FormBuilder('fraud-provider-settings-rules', array('label' => 'fraud_methods.statuses_rules', 'collapsible' => true));
		$formBuilder->add($settingsGroup);
		$methodSettings = $this->fraudMethod->getMethodSettings(true);

		$paymentStatusOptions                                 = array();
		$paymentStatusOptions['default']                      = 'No Change';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_REVIEW]   = 'Review';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_PENDING]  = 'Pending';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_RECEIVED] = 'Received';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_DECLINED] = 'Declined';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_CANCELED] = 'Canceled';
		$paymentStatusOptions[ORDER::PAYMENT_STATUS_ERROR]    = 'Error';

		$orderStatusOptions                         = array();
		$orderStatusOptions['default']              = 'No Change';
		$orderStatusOptions[ORDER::STATUS_PROCESS]  = 'Process';
		$orderStatusOptions[ORDER::STATUS_CANCELED] = 'Canceled';
		$orderStatusOptions[ORDER::STATUS_FAILED]   = 'Failed';
		$orderStatusOptions[ORDER::STATUS_ABANDON]  = 'Abandon';

		$settingsGroup->add('fraudlabs_reviewStatus_label', 'static', array('label' => 'fraud_methods.on_review_status_received'));

		$settingsGroup->add('form[fraudlabs_reviewOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['fraudlabs_reviewOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[fraudlabs_reviewPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['fraudlabs_reviewPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));

		$settingsGroup->add('fraudlabs_rejectStatus_label', 'static', array('label' => 'fraud_methods.on_reject_status_received'));

		$settingsGroup->add('form[fraudlabs_rejectOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['fraudlabs_rejectOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[fraudlabs_rejectPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['fraudlabs_rejectPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));

		$settingsGroup->add('fraudlabs_approveReviewStatus_label', 'static', array('label' => 'fraud_methods.on_order_approve_action'));

		$settingsGroup->add('form[fraudlabs_approveReviewOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['fraudlabs_approveReviewOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[fraudlabs_approveReviewPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['fraudlabs_approveReviewPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));

		$settingsGroup->add('fraudlabs_denyReviewStatus_label', 'static', array('label' => 'fraud_methods.on_order_deny_action'));

		$settingsGroup->add('form[fraudlabs_denyReviewOrderStatus]', 'choice',
			array('label' => 'fraud_methods.change_order_status',
				  'value' => $methodSettings ['fraudlabs_denyReviewOrderStatus']['value'],
				  'options' => $orderStatusOptions,
				  'required' => false,
				  'wrapperClass' => 'clear-both'));

		$settingsGroup->add('form[fraudlabs_denyReviewPaymentStatus]', 'choice',
			array('label' => 'fraud_methods.change_payment_status',
				  'value' => $methodSettings ['fraudlabs_denyReviewPaymentStatus']['value'],
				  'options' => $paymentStatusOptions,
				  'required' => false));

		return $formBuilder;
	}

	public function reviewStatusAction($checkResult)
	{
		$oid         = $checkResult['oid'];
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = $checkResult;
		if ($oid && $fid && $transaction)
		{
			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($oid);

			$oldOrderStatus   = $order_data['status'];
			$oldPaymentStatus = $order_data['payment_status'];

			$methodSettings = $this->fraudMethod->getMethodSettings(true);

			$reviewOrderStatus = $methodSettings ['fraudlabs_reviewOrderStatus']['value'];
			$newOrderStatus    = ($reviewOrderStatus != 'default') ? $reviewOrderStatus : $oldOrderStatus;

			$reviewPaymentStatus = $methodSettings ['fraudlabs_reviewPaymentStatus']['value'];
			$newPaymentStatus    = ($reviewPaymentStatus != 'default') ? $reviewPaymentStatus : $oldPaymentStatus;

			if ($oldOrderStatus != $newOrderStatus)
			{
				$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
			}

			if ($oldPaymentStatus != $newPaymentStatus)
			{
				$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
			}

			$transaction['order_prev_status']   = $oldOrderStatus;
			$transaction['payment_prev_status'] = $oldPaymentStatus;
			$transaction['order_new_status']    = $newOrderStatus;
			$transaction['payment_new_status']  = $newPaymentStatus;
			$this->providerRepository->persistFraudTransaction($oid, $transaction);
			$result['oldOrderStatus']   = $oldOrderStatus;
			$result['oldPaymentStatus'] = $oldPaymentStatus;
			$result['newOrderStatus']   = $newOrderStatus;
			$result['newPaymentStatus'] = $newPaymentStatus;
		}
		$this->log('Call ', 'reviewStatusAction');

		$transaction['payment_method_data'] = '';
		$this->log('Result: ', $transaction);
		return $result;
	}

	public function rejectStatusAction($checkResult)
	{
		$oid         = $checkResult['oid'];
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = $checkResult;
		if ($oid && $fid && $transaction)
		{
			$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
			$order_data          = $orderDataRepository->getOrderData($oid);

			$oldOrderStatus   = $order_data['status'];
			$oldPaymentStatus = $order_data['payment_status'];

			$methodSettings = $this->fraudMethod->getMethodSettings(true);

			$reviewOrderStatus = $methodSettings ['fraudlabs_rejectOrderStatus']['value'];
			$newOrderStatus    = ($reviewOrderStatus != 'default') ? $reviewOrderStatus : $oldOrderStatus;

			$reviewPaymentStatus = $methodSettings ['fraudlabs_rejectPaymentStatus']['value'];
			$newPaymentStatus    = ($reviewPaymentStatus != 'default') ? $reviewPaymentStatus : $oldPaymentStatus;

			if ($oldOrderStatus != $newOrderStatus)
			{
				$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
			}

			if ($oldPaymentStatus != $newPaymentStatus)
			{
				$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
			}

			$transaction['order_prev_status']   = $oldOrderStatus;
			$transaction['payment_prev_status'] = $oldPaymentStatus;
			$transaction['order_new_status']    = $newOrderStatus;
			$transaction['payment_new_status']  = $newPaymentStatus;
			$this->providerRepository->persistFraudTransaction($oid, $transaction);

			$result['oldOrderStatus']   = $oldOrderStatus;
			$result['oldPaymentStatus'] = $oldPaymentStatus;
			$result['newOrderStatus']   = $newOrderStatus;
			$result['newPaymentStatus'] = $newPaymentStatus;
		}

		$this->log('Call ', 'rejectStatusAction');
		$transaction['payment_method_data'] = '';
		$this->log('Result: ', $transaction);
		return $result;
	}

	public function approveOrderAction($oid, $admin_log_id = 0)
	{
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = self::getDefaultResult();
		if ($oid && $fid && $transaction)
		{
			$feedBackParams = $this->buildFeedbackParams($transaction['security_id'], self::STATUS_APPROVE);
			$result         = $this->sendFeedbackRequest($this->feedBackUrl, $feedBackParams);
			if ($result['status'] == self::RESULT_SUCCESS)
			{
				$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
				$order_data          = $orderDataRepository->getOrderData($oid);

				$oldOrderStatus   = $order_data['status'];
				$oldPaymentStatus = $order_data['payment_status'];

				$methodSettings = $this->fraudMethod->getMethodSettings(true);

				$approveOrderStatus = $methodSettings ['fraudlabs_approveReviewOrderStatus']['value'];
				$newOrderStatus     = ($approveOrderStatus != 'default') ? $approveOrderStatus : $oldOrderStatus;

				$approvePaymentStatus = $methodSettings ['fraudlabs_approveReviewPaymentStatus']['value'];
				$newPaymentStatus     = ($approvePaymentStatus != 'default') ? $approvePaymentStatus : $oldPaymentStatus;

				if ($oldOrderStatus != $newOrderStatus)
				{
					$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
				}

				if ($oldPaymentStatus != $newPaymentStatus)
				{
					$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
				}

				if ($admin_log_id)
				{
					$transaction['admin_log_id'] = $admin_log_id;
					$this->providerRepository->persistFraudTransaction($oid, $transaction);
				}

				$result['oldOrderStatus']   = $oldOrderStatus;
				$result['oldPaymentStatus'] = $oldPaymentStatus;
				$result['newOrderStatus']   = $newOrderStatus;
				$result['newPaymentStatus'] = $newPaymentStatus;
			}
		}

		$this->log('Call ', 'approveOrderAction');
		$this->log('Params: ', $feedBackParams);
		$this->log('Result: ', $result);
		return $result;
	}

	private function buildFeedbackParams($transactionId, $action = 'APPROVE')
	{
		$result['id']     = $transactionId;
		$result['key']    = $this->getLicenseKey();
		$result['action'] = $action;
		$result['format'] = $this->getFormat();

		return $result;
	}

	private function sendFeedbackRequest($url, $params)
	{
		$result = self::getDefaultResult();

		if (isset($params['format']))
		{
			$format = $params['format'];
		}
		else
		{
			$format = self::FORMAT_JSON;
		}

		try
		{
			$opts  = array();
			$query = '';
			if (count($params) > 0)
			{
				$query = '?' . http_build_query($params);
			}

			$opts[CURLOPT_RETURNTRANSFER] = true;
			$opts[CURLOPT_URL]            = $url . $query;

			$opts[CURLOPT_VERBOSE]        = false;
			$opts[CURLOPT_HEADER]         = false;
			$settings                     = $this->getSettings()->getAll();
			$opts[CURLOPT_SSL_VERIFYPEER] = 1;
			$opts[CURLOPT_CAPATH]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'];
			$opts[CURLOPT_CAINFO]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'] . '/' . $settings['SecuritySslPem'];

			$c = curl_init();
			curl_setopt_array($c, $opts);
			$buffer = curl_exec($c);
			if (curl_errno($c) > 0)
			{
				$error_msg          = curl_error($c);
				$result['status']   = self::RESULT_ERROR;
				$result['response'] = $error_msg;
				$result['error']    = $error_msg;
			}
			else
			{
				$result['status']   = self::RESULT_SUCCESS;
				$result['response'] = $buffer;
				if ($format == $this::FORMAT_JSON)
				{
					$api_data  = json_decode($buffer);
					$errorCode = intval($api_data->fraudlabspro_error_code);
					if ($errorCode && $errorCode != self::FEEDBACK_ORDER_ERROR_READ_ONLY)
					{
						$result['status'] = self::RESULT_ERROR;
						$result['error']  = $api_data->fraudlabspro_message;
					}
				}
				elseif ($format == $this::FORMAT_XML)
				{
					/* @var  SimpleXMLElement $api_data */
					$xml       = simplexml_load_string($buffer, "SimpleXMLElement", LIBXML_NOCDATA);
					$json      = json_encode($xml);
					$api_data  = json_decode($json, TRUE);
					$errorCode = intval($api_data['fraudlabspro_error_code']);
					if ($errorCode && $errorCode != self::FEEDBACK_ORDER_ERROR_READ_ONLY)
					{
						$result['status'] = self::RESULT_ERROR;
						$result['error']  = $api_data['fraudlabspro_message'];
					}
				}
			}
		} catch (Exception $e)
		{
			$result['error'] = $e->getMessage();
		}

		$this->log('Result: ', $result);

		return $result;
	}

	public function denyOrderAction($oid, $admin_log_id = 0)
	{
		$fid         = $this->fraudMethod->getId();
		$transaction = $this->providerRepository->getFraudTransactionForOrder($oid);
		$result      = self::getDefaultResult();
		if ($oid && $fid && $transaction)
		{
			$feedBackParams = $this->buildFeedbackParams($transaction['security_id'], self::STATUS_REJECT);
			$result         = $this->sendFeedbackRequest($this->feedBackUrl, $feedBackParams);
			if ($result['status'] == self::RESULT_SUCCESS)
			{
				$orderDataRepository = DataAccess_OrderRepository::getInstance($this->db);
				$order_data          = $orderDataRepository->getOrderData($oid);

				$oldOrderStatus   = $order_data['status'];
				$oldPaymentStatus = $order_data['payment_status'];

				$methodSettings = $this->fraudMethod->getMethodSettings(true);

				$approveOrderStatus = $methodSettings ['fraudlabs_denyReviewOrderStatus']['value'];
				$newOrderStatus     = ($approveOrderStatus != 'default') ? $approveOrderStatus : $oldOrderStatus;

				$approvePaymentStatus = $methodSettings ['fraudlabs_denyReviewPaymentStatus']['value'];
				$newPaymentStatus     = ($approvePaymentStatus != 'default') ? $approvePaymentStatus : $oldPaymentStatus;

				if ($oldOrderStatus != $newOrderStatus)
				{
					$orderDataRepository->updateOrderStatus($oid, $newOrderStatus);
				}

				if ($oldPaymentStatus != $newPaymentStatus)
				{
					$orderDataRepository->updateOrderPaymentStatus($oid, $newPaymentStatus);
				}

				if ($admin_log_id)
				{
					$transaction['admin_log_id'] = $admin_log_id;
					$this->providerRepository->persistFraudTransaction($oid, $transaction);
				}

				$result['oldOrderStatus']   = $oldOrderStatus;
				$result['oldPaymentStatus'] = $oldPaymentStatus;
				$result['newOrderStatus']   = $newOrderStatus;
				$result['newPaymentStatus'] = $newPaymentStatus;
			}
		}
		$this->log('Call ', 'denyOrderAction');
		$this->log('Params: ', $feedBackParams);
		$this->log('Result: ', $result);

		return $result;
	}

	private function doGet($url, array $reqParams = array(), array $reqHeaders = array())
	{
		foreach ($reqHeaders as $key => $value)
		{
			$headers[] = $key . ': ' . $value;
		}
		$headers[] = 'Content-Length: ' . strlen('');

		$query = '';
		if (count($reqParams) > 0)
		{
			$query = '?' . http_build_query($reqParams);
		}

		$params                         = array();
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL]            = $url . $query;
		$params[CURLOPT_HTTPHEADER]     = $headers;
		$params[CURLOPT_VERBOSE]        = false;
		$params[CURLOPT_HEADER]         = false;
		$settings                       = $this->getSettings()->getAll();
		$params[CURLOPT_SSL_VERIFYPEER] = 1;
		$params[CURLOPT_CAPATH]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'];
		$params[CURLOPT_CAINFO]         = $settings['GlobalServerPath'] . $settings['SecuritySslDirectory'] . '/' . $settings['SecuritySslPem'];

		$request = '';
		$request .= 'GET ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";
		foreach ($headers as $header)
		{
			$request .= $header . "\r\n";
		}

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		$response = curl_exec($ch);

		@curl_close($ch);

		return $response;
	}

	private function encodeGetParams($arr, $prefix = null)
	{
		if (!is_array($arr))
		{
			return $arr;
		}

		$r = array();
		foreach ($arr as $k => $v)
		{
			if (is_null($v))
			{
				continue;
			}

			if ($prefix && $k && !is_int($k))
			{
				$k = $prefix . '[' . $k . ']';
			}
			else
			{
				if ($prefix)
				{
					$k = $prefix . '[]';
				}
			}

			if (is_array($v))
			{
				$r[] = $this->encodeGetParams($v, $k);
			}
			else
			{
				$r[] = urlencode($k) . '=' . urlencode($v);
			}
		}

		return implode('&', $r);
	}

}
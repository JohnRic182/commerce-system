<?php

/**
 * Class FraudService_Model_FraudMethod
 */
class FraudService_Model_FraudMethod
{
	const route = 'FraudService_Provider_';
	protected $fid = null;
	protected $fields;
	protected $className;
	protected $methodSettings;
	protected $logo;
	protected $isMethodHasSnippet = false;
	protected $isSnippetActive = false;

	/**
	 * Class constructor
	 */
	public function __construct($data)
	{
		$this->fields = $data;
		if (isset($this->fields['fid']) && $this->fields['fid'])
		{
			$this->fid            = $this->fields['fid'];
			$this->className      = self::route . $this->fields['class'] . '_' . $this->fields['class'] . 'Provider';
			$this->methodSettings = (isset($data['settings']) && is_array($data['settings'])) ? $data['settings'] : array();
			$this->logo           = (isset($data['logo'])) ? $data['logo'] : '';
			if ($data['snippet'])
			{
				$this->isMethodHasSnippet = true;
				$snippet = $data['snippet'];
				if ($snippet['is_visible'] == 'Yes')
				{
					$this->isSnippetActive =  true;
				}
			}
		}
	}

	/**
	 * Get method title
	 *
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->fields['title'];
	}


	/**
	 * Get method name
	 *
	 * @return mixed
	 */
	public function getName()
	{
		return $this->fields['name'];
	}

	/**
	 * @return string
	 */
	public function getClassName()
	{
		return $this->className;
	}

	/**
	 * @return mixed
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * @return null
	 */
	public function getId()
	{
		return $this->fid;
	}

	/**
	 * @return array
	 */
	public function getMethodSettings($useNameAsIndex = false)
	{
		if (!$useNameAsIndex)
		{
			return $this->methodSettings;
		}
		else
		{
			$result = array();
			foreach ($this->methodSettings as $k => $v)
			{
				$result[$v['name']] = $v;
			}

			return $result;
		}

	}

	/**
	 * @return string
	 */
	public function getLogo()
	{
		return $this->logo;
	}

	/**
	 * @return boolean
	 */
	public function isIsMethodHasSnippet()
	{
		return $this->isMethodHasSnippet;
	}

	/**
	 * @return boolean
	 */
	public function isIsSnippetActive()
	{
		return $this->isSnippetActive;
	}




}


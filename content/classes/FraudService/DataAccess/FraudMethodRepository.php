<?php

/**
 * Class FraudService_DataAccess_FraudMethodRepository
 */
class FraudService_DataAccess_FraudMethodRepository extends DataAccess_Repository
{

	/** @var  DataAccess_SettingsRepository */
	protected $settings;

	protected $transaction_defaults = array(
		'ftid' => 0,
		'oid' => 0,
		'fid' => 0,
		'completed' => '',
		'fraud_method_name' => '',
		'request_type' => 'json',
		'request' => '',
		'response' => '',
		'score' => 0,
		'order_prev_status' => '',
		'payment_prev_status' => '',
		'order_new_status' => '',
		'payment_new_status' => '',
		'error' => '',
		'fraud_status' => '',
		'security_id' => '',
		'admin_log_id' => 0,
		'payment_method_data' => '',
	);

	// TODO: move this to providers
	protected $methods_logos = array(
		'fraudlabs' => 'images/admin/fraudlabs-logo.png',
		'nofraud' => 'images/admin/nofraud_logo.png',
	);

	protected $methods_signUpUrls = array(
		'fraudlabs' => 'http://www.fraudlabspro.com/pricing?utm_source=module&utm_medium=banner&utm_term=pinnaclecart&utm_campaign=module%20banner',
		'nofraud' => 'https://www.nofraud.com/get_started',
	);

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		if ($db)
		{
			self::setDatabaseInstance($db);
		}
		$this->settings = $settings;
	}

	/**
	 * Get all available fraud methods
	 *
	 * @param bool $onlyActive
	 * @param ORDER $order
	 *
	 * @return array|null
	 */
	public function getFraudMethods($onlyActive = false, $model = false)
	{
		$db     = self::$db;
		$result = $db->selectAll('SELECT * FROM ' . DB_PREFIX . 'fraud_methods ' . ($onlyActive ? ' WHERE active="Yes"' : '') . ' ORDER BY priority, name');

		if ($result && is_array($result) && count($result) > 0)
		{
			$fraudMethods = array();

			foreach ($result as $fraudMethodData)
			{
				$_data = $fraudMethodData;
				if (isset($this->methods_logos[$fraudMethodData['id']]))
				{
					$_data['logo'] = $this->methods_logos[$fraudMethodData['id']];
				}

				if ($model)
				{
					$fraudMethod = new FraudService_Model_FraudMethod($_data);
				}
				else
				{
					$fraudMethod = $_data;
				}
				$fraudMethods[$fraudMethodData['fid']] = $fraudMethod;
			}

			return $fraudMethods;
		}

		return null;
	}


	/**
	 * Get fraud methods count
	 *
	 * @param bool $onlyActive
	 *
	 * @return int
	 */
	public function getFraudMethodsCount($onlyActive = false)
	{
		$db     = self::$db;
		$result = $db->selectOne('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'fraud_methods ' . ($onlyActive ? ' WHERE active="Yes"' : ''));

		if ($result && is_array($result) && count($result) > 0)
		{
			return $result['c'];
		}

		return 0;
	}

	private function getMethodSettings($methodId = '')
	{
		$db             = self::$db;
		$result         = array();
		$groupName      = 'fraud_' . $db->escape($methodId);
		$methodSettings = $this->getData('SELECT * FROM `' . DB_PREFIX . 'settings` WHERE group_name = "' . $groupName . '" ORDER BY `priority`');

		foreach ($methodSettings as $key => $settingsOption)
		{
			$option = $settingsOption;
			if (trim($option['caption']) == '')
			{
				$caption = substr($settingsOption['name'], strlen($result['id']), strlen($settingsOption['name']) - strlen($result['id']));
				$caption = ucwords(strtolower(str_replace('_', ' ', $caption)));
			}
			$result[$key] = $option;
		}

		return $result;
	}

	/**
	 * Select fraud methods by id
	 *
	 * @param $id
	 * @param bool $onlyActive
	 *
	 * @return fraud_MethodFraudService_Model_FraudMethod|null
	 */
	public function getById($id, $onlyActive = false, $model = false)
	{
		$db     = self::$db;
		$result = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_methods WHERE fid=' . intval($id) . ($onlyActive ? ' AND active="Yes"' : ''));

		if ($result)
		{
			$result['settings'] = $this->getMethodSettings($result['id']);
			if (isset($this->methods_signUpUrls[$result['id']]))
			{
				$result['method_signup_url'] = $this->methods_signUpUrls[$result['id']];
			}
			$result['snippet'] = array();
			$snippet = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'snippets WHERE internal_id LIKE "%Fraud_' . $result['class'] . '%"');
			if($snippet)
			{
				$result['snippet'] = $snippet;
			}
		}
		else
		{
			return null;
		}

		if ($model)
		{
			if ($result)
			{
				$result = new FraudService_Model_FraudMethod($result);
			}
			else
				$result = false;
		}
		else
		{
			$result;
		}

		return $result;
	}

	/**
	 * Select fraud methods by class name
	 *
	 * @param $classname
	 *
	 * @return fraud_Method|null
	 */
	public function getByClassName($classname)
	{
		$db     = self::$db;
		$result = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_methods WHERE class = "' . $classname . '"');

		if ($result)
		{
			$result['settings'] = $this->getMethodSettings($result['id']);
			if (isset($this->methods_signUpUrls[$result['id']]))
			{
				$result['method_signup_url'] = $this->methods_signUpUrls[$result['id']];
			}
			$result['snippet'] = array();
			$snippet = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'snippets WHERE internal_id LIKE "%Fraud_' . $result['class'] . '%"');
			if($snippet)
			{
				$result['snippet'] = $snippet;
			}
		}
		return $result ? new FraudService_Model_FraudMethod($result) : null;
	}

	public function deactivate($methodId)
	{
		$db = self::$db;

		$_method = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_methods WHERE fid = ' . intval($methodId));
		if ($_method)
		{
			$db->query('UPDATE ' . DB_PREFIX . 'fraud_methods SET active="No" WHERE fid=' . intval($methodId));
			$db->query('UPDATE ' . DB_PREFIX . 'snippets SET is_visible = "No" WHERE internal_id LIKE "%Fraud_' . $_method['class'] . '%"');
			$this->activateSnippetByMethodId($methodId);
		}

	}

	public function activate($methodId)
	{
		$db      = self::$db;
		$_method = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_methods WHERE fid = ' . intval($methodId));
		if ($_method)
		{
			$db->query('UPDATE ' . DB_PREFIX . 'fraud_methods SET active="No" ');
			$db->query('UPDATE ' . DB_PREFIX . 'fraud_methods SET active="Yes" WHERE fid=' . intval($methodId));
			$this->activateSnippetByMethodId($methodId);
		}
	}

	public function activateSnippetByMethodId($methodId)
	{
		$db      = self::$db;
		$_method = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_methods WHERE fid = ' . intval($methodId));
		if ($_method)
		{
			$db->query('UPDATE ' . DB_PREFIX . 'snippets SET is_visible = "Yes" WHERE internal_id LIKE "%Fraud_' . $_method['class'] . '%"');
		}
	}

	public function deactivateSnippetByMethodId($methodId)
	{
		$db      = self::$db;
		$_method = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_methods WHERE fid = ' . intval($methodId));
		if ($_method)
		{
			$db->query('UPDATE ' . DB_PREFIX . 'snippets SET is_visible = "No" WHERE internal_id LIKE "%Fraud_' . $_method['class'] . '%"');
		}
	}

	/**
	 * Select active fraud method
	 *
	 * @return fraud_Method|null
	 */
	public function getActiveMethod()
	{
		$db     = self::$db;
		$result = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_methods WHERE active="Yes"');
		if ($result)
		{
			$result['settings'] = $this->getMethodSettings($result['id']);
			if (isset($this->methods_signUpUrls[$result['id']]))
			{
				$result['method_signup_url'] = $this->methods_signUpUrls[$result['id']];
			}
			$result['snippet'] = array();
			$snippet = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'snippets WHERE internal_id LIKE "%Fraud_' . $result['class'] . '%"');
			if($snippet)
			{
				$result['snippet'] = $snippet;
			}
		}

		return $result ? new FraudService_Model_FraudMethod($result) : null;
	}

	public function getFraudTransactionForOrder($oid)
	{
		$db     = self::$db;
		$result = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_transactions WHERE oid=' . intval($oid));

		if ($result['admin_log_id'])
		{
			$logRecord = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'admins_logs WHERE id=' . intval($result['admin_log_id']));
			if ($logRecord)
			{
				$result['admin_log_data'] = $logRecord;
			}
		}

		return $result;
	}

	public function getFraudTransactionById($id)
	{
		$db     = self::$db;
		$result = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'fraud_transactions WHERE ftid=' . intval($id));
		if ($result['admin_log_id'])
		{
			$logRecord = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'admins_logs WHERE id=' . intval($result['admin_log_id']));
			if ($logRecord)
			{
				$result['admin_log_data'] = $logRecord;
			}
		}

		return $result;
	}


	public function persistFraudTransaction($oid, $data)
		//($order, $fid, $score = FraudService_Provider_FraudServiceProvider::RISK_SCORE_MIN, $request = '', $response = '', $request_type = 'json', $error = '')
	{
		$_data       = array_merge($this->getTransactionDefaults(), $data);
		$db          = self::$db;
		$fid         = isset($_data['fid']) ? $_data['fid'] : 0;
		$method_data = array();
		if ($fid)
		{
			$fMethod = $this->getById($fid, false, true);
			if ($fMethod)
			{
				$method_data = $fMethod->getFields();
			}
		}


		$transaction = $this->getFraudTransactionForOrder($oid);
		$ftid        = false;
		if ($transaction)
		{
			if (isset($transaction['ftid']))
			{
				$ftid = $transaction['ftid'];
			}

			if (isset($transaction['payment_method_data']))
			{
				if(!$_data['payment_method_data'])
				{
					$_data['payment_method_data'] = $transaction['payment_method_data'];
				}
			}
		}

		$db->reset();
		$db->assignStr('oid', $oid);
		$db->assignStr('fid', $fid);
		$db->assign('completed', 'NOW()');
		$db->assignStr('fraud_method_name', isset($method_data['name']) ? $method_data['name'] : '');
		$db->assignStr('request_type', $_data['request_type']);

		$db->assignStr('request', $_data['request']);
		$db->assignStr('response', $_data['response']);
		$db->assignStr('score', $_data['score']);

		$db->assignStr('fraud_status', $_data['fraud_status']);
		$db->assignStr('security_id', $_data['security_id']);

		$db->assignStr('admin_log_id', $_data['admin_log_id']);

		if(is_array($_data['payment_method_data']))
		{
			$db->assignStr('payment_method_data', serialize($_data['payment_method_data']));
		}
		else
		{
			$db->assignStr('payment_method_data', $_data['payment_method_data']);
		}


		$db->assignStr('order_prev_status', $_data['order_prev_status']);
		$db->assignStr('payment_prev_status', $_data['payment_prev_status']);
		$db->assignStr('order_new_status', $_data['order_new_status']);
		$db->assignStr('payment_new_status', $_data['payment_new_status']);

		if (trim($_data['error']))
		{
			$db->assignStr('error', $_data['error']);
		}

		if ($transaction)
		{
			$db->update(DB_PREFIX . 'fraud_transactions', 'WHERE ftid=' . intval($ftid));
			return $ftid;
		}
		else
		{
			return $db->insert(DB_PREFIX . 'fraud_transactions');
		}
	}

	/**
	 * @return array
	 */
	public function getTransactionDefaults()
	{
		return $this->transaction_defaults;
	}

}
<?php

/**
 * Class FraudService_View_FraudTransactionView
 */
class FraudService_View_FraudTransactionView
{
	public $id;
	public $orderId;
	public $fraudMethodId;
	public $transactionDate;
	public $methodName;
	public $dataType;
	public $score;
	public $data;
	public $parsedData;
	private $fraudProvider;

	/**
	 * Class constructor
	 *
	 * @param $transaction
	 */
	public function __construct($db, $settings, $transaction)
	{
		$this->id              = $transaction['ftid'];
		$this->orderId         = $transaction['oid'];
		$this->fraudMethodId   = $transaction['fid'];
		$this->transactionDate = $transaction['completed'];
		$this->methodName      = $transaction['fraud_method_name'];
		$this->dataType        = $transaction['request_type'];
		$this->score           = $transaction['score'];

		if ($this->dataType == FraudService_Provider_FraudServiceProvider::FORMAT_JSON)
		{
			$this->data = json_decode($transaction['response'], true);
		}
		elseif ($this->dataType == FraudService_Provider_FraudServiceProvider::FORMAT_XML)
		{
			/* @var  SimpleXMLElement $api_data */
			$xml        = simplexml_load_string($transaction['response'], "SimpleXMLElement", LIBXML_NOCDATA);
			$json       = json_encode($xml);
			$api_data   = json_decode($json, true);
			$this->data = $api_data;
		}

		if ($this->data)
		{
			foreach ($this->data as $key => $value)
			{
				$caption                = ucwords(strtolower(str_replace("_", " ", $key)));
				$item                   = array('caption' => $caption, 'value' => $value);
				$this->parsedData[$key] = $item;
			}
		}

		$this->fraudProvider = FraudService_Provider_FraudLabs_FraudLabsProvider::getProviderInstanceByProviderId($db, $settings, $transaction['fid']);
	}

	public function getTransactionDataForm()
	{

		$form = false;

		if (!is_null($this->fraudProvider))
		{
			$form = $this->fraudProvider->getTransactionDataForm($this->id);
		}

		if (!$form)
		{
			$_data = $this->parsedData;
			$form  = $this->getBuilder($_data)->getForm();
		}

		return $form;
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	private function getBuilder($data)
	{
		/** @var core_Form_FormBuilder $formBuilder */
		$formBuilder = new core_Form_FormBuilder('fraud-transaction');

		$paramsGroup = new core_Form_FormBuilder('fraud-transaction-params', array('label' => 'Fraud Service Transaction', 'collapsible' => true));
		$formBuilder->add($paramsGroup);

		if ($data)
		{
			foreach ($data as $key => $item)
			{
				$paramsGroup->add($key, 'text', array('label' => $item['caption'],
													  'value' => $item['value'],
				));
			}
		}

		return $formBuilder;
	}
}
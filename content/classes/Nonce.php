<?php

/**
 * Class Nonce
 */
class Nonce
{
	/**
	 * @param $action
	 * @return string
	 */
	public static function create($action)
	{
		global $auth_ok;

		$uid = 0;
		if ($auth_ok)
		{
			global $admin, $user;

			if ($admin)
			{
				$uid = $admin['aid'];
			}
			else if ($user)
			{
				$uid = $user->id;
			}
		}

		$i = rand(0, 10000);

		$hash = self::hash($i, $uid, $action);

		if (!isset($_SESSION['nonce'])) $_SESSION['nonce'] = array();

		$_SESSION['nonce'][$hash] = true;

		return $hash;
	}

	/**
	 * @param $nonce
	 * @param null $action
	 * @param bool $removeFromSession
	 * @return bool
	 */
	public static function verify($nonce, $action = null, $removeFromSession = true)
	{
		if (isset($_SESSION['nonce'][$nonce]))
		{
			if ($removeFromSession)
			{
				unset($_SESSION['nonce'][$nonce]);
			}

			return true;
		}
		return false;
	}

	/**
	 * @param $fileName
	 */
	public static function preserve($fileName)
	{
		if (isset($_SESSION['nonce']) && count($_SESSION['nonce']))
		{
			@file_put_contents($fileName, serialize(array('t' => time(), 'n' => $_SESSION['nonce'])));
		}
	}

	/**
	 * @param $fileName
	 */
	public static function resurrect($fileName)
	{
		if (is_file($fileName))
		{
			$data = @unserialize(@file_get_contents($fileName));
			if ($data && $data['t'] + 1800 > time())
			{
				$_SESSION['nonce'] = $data['n'];
			}
			@unlink($fileName);
		}
	}

	/**
	 * @param $i
	 * @param $uid
	 * @param $action
	 * @return string
	 */
	public static function hash($i, $uid, $action)
	{
		$salt = self::salt();

		$nonce = hash_hmac('md5', $i.$uid.$action, $salt);

		return $nonce;
	}

	/**
	 * @return null|string
	 */
	public static function salt()
	{
		static $salt = null;

		if (is_null($salt))
		{
			$settings = settings();

			if (isset($settings['SecurityNonceSalt']) && trim($settings['SecurityNonceSalt']) != '')
			{
				$salt = $settings['SecurityNonceSalt'];
			}
			else
			{
				$salt = md5(time().rand(0,10000));
				db()->reset();
				db()->assignStr('value', $salt);
				db()->update(DB_PREFIX.'settings', 'WHERE name = "SecurityNonceSalt" LIMIT 1');
			}
		}

		return $salt;
	}
}
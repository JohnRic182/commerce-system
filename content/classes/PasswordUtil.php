<?php

class PasswordUtil
{
	protected $hasher;

	public function __construct()
	{
		$this->hasher = new PasswordHash(8, false);
	}

	public function generatePasswordSalt()
	{
		if ($this->isBlowfishAvailable())
		{
			$random = $this->hasher->get_random_bytes(16);

			return $this->hasher->gensalt_blowfish($random);
		}
		else if ($this->isExtDESAvailable())
		{
			$random = $this->hasher->get_random_bytes(3);

			return $this->hasher->gensalt_extended($random);
		}

		$random = $this->hasher->get_random_bytes(6);

		return $this->hasher->gensalt_private($random);
	}

	public function hashPassword($password, $salt = false)
	{
		if (!$salt)
		{
			$salt = $this->generatePasswordSalt();
		}

		if ($this->isBlowfishAvailable())
		{
			$hash = crypt($password, $salt);
			if (strlen($hash) == 60)
				return $hash;
		}
		else if ($this->isExtDESAvailable())
		{
			$hash = crypt($password, $salt);
			if (strlen($hash) == 20)
				return $hash;
		}

		$hash = $this->hasher->crypt_private($password, $salt);
		if (strlen($hash) == 34)
			return $hash;

		return '*';
	}

	public function checkPassword($password, $storedHash)
	{
		return $this->hasher->CheckPassword($password, $storedHash);
	}

	protected function isBlowfishAvailable()
	{
		return CRYPT_BLOWFISH == 1;
	}

	protected function isExtDESAvailable()
	{
		return CRYPT_EXT_DES == 1;
	}
}
<?php

/**
* 
*/
class SiteTheme
{
	protected $id;
	protected $name;
	protected $title;
	protected $fullsizeImage;
	protected $thumbnailImage;
	protected $settings = array();
	protected $images = array();
	
	/**
	 * Sets the theme name which is a unique identifier
	 *
	 * @param string $name Unique name for the theme
	 * @throws InvalidArgumentException If $name is not a string
	 * @return void
	 */
	public function setName($name)
	{
		if (!is_string($name)) throw new InvalidArgumentException('Expecting first parameter to be a string');
		$this->name = $name;
	}
	
	/**
	 * Retrieves the theme name
	 *
	 * @return string The theme name
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Sets the theme name which is a unique identifier
	 *
	 * @param array $settings Expectsan associative array of theme settings
	 * @return void
	 */
	public function setSettings(array $settings)
	{
		$this->settings = $settings;
	}
	
	/**
	 * Retrieves the theme settings
	 *
	 * @return string An associative array of theme settings
	 */
	public function getSettings()
	{
		return $this->settings;
	}
	
	/**
	 * Sets the theme title
	 *
	 * @param string $title Title of the theme
	 * @throws InvalidArgumentException If $title is not a string
	 * @return void
	 */
	public function setTitle($title)
	{
		if (!is_string($title)) throw new InvalidArgumentException('Expecting first parameter to be a string');
		$this->title = $title;
	}
	
	/**
	 * Gets the theme title
	 *
	 * @return string The theme title
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * Sets the theme author which is a unique identifier
	 *
	 * @param string $author Unique author for the theme
	 * @throws InvalidArgumentException If $author is not a string
	 * @return void
	 */
	public function setAuthor($author)
	{
		if (!is_string($author)) throw new InvalidArgumentException('Expecting first parameter to be a string');
		$this->author = $author;
	}
	
	/**
	 * Retrieves the theme author
	 *
	 * @return string The theme author
	 */
	public function getAuthor()
	{
		return $this->author;
	}
	
	/**
	 * Add a theme image
	 *
	 * @param string $filename Path to image relative to theme root
	 * @return void
	 */
	public function addImage($filename, $title = '')
	{
		if (!is_string($filename)) throw new InvalidArgumentException('Expecting first parameter to be a string');
		$this->images[$filename] = array('path' => $filename, 'title' => $title);
	}
	
	/**
	 * Sets the theme images
	 *
	 * @param array $images An array of paths to images relative to theme root
	 * @return void
	 */
	public function setImages(array $images)
	{
		$this->images = $images;
	}
	
	/**
	 * Gets the theme images
	 *
	 * @return array An array of theme images
	 */
	public function getImages()
	{
		return $this->images;
	}
	
	public function setThumbnailImage($image)
	{
		$this->thumbnailImage = $image;
	}
	
	public function getThumbnailImage()
	{
		return $this->thumbnailImage;
	}
	
	public function setFullsizeImage($image)
	{
		$this->fullsizeImage = $image;
	}
	
	public function getFullsizeImage()
	{
		return $this->fullsizeImage;
	}
}

<?php

/**
 * Class Config
 */
class Config
{
	public static function loadApplicationConfig()
	{
		if (!file_exists('content/engine/engine_config.php'))
		{
			// Check to see if there's an install folder to redirect to
			if (file_exists('install/index.php') && is_file('install/index.php'))
			{
				header('Location: install/index.php');
				exit;
			}
			else
			{
				$error_message = 'Could not locate the engine config file. Ensure the file is in the content/engine/ folder.';
				error_log($error_message);
				throw new Exception($error_message);
			}
		}
		else
		{
			require_once('content/engine/engine_config.php');
		}
	}
}
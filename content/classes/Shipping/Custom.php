<?php

/**
 * Class Shipping_Custom
 */
class Shipping_Custom extends Shipping_Provider
{
	/** @var DataAccess_ShippingRepositoryInterface  */
	protected $shippingRepository;

	protected $shippingMethodId;
	protected $shippingMethodType;

	protected $items;
	protected $itemsCount;
	protected $itemsPriceBasedPrice;
	protected $weight;

	/**
	 * Shipping provider class constructor
	 * 
	 * @param DataAccess_ShippingRepositoryInterface $shippingRepository
	 */
	public function __construct(DataAccess_ShippingRepositoryInterface $shippingRepository)
	{
		$this->shippingRepository = $shippingRepository;
	}

	/**
	 * Setter for shippingMethodId
	 *
	 * @param int $shippingMethodId value to set
	 *
	 * @return self
	 */
	public function setShippingMethodId($shippingMethodId)
	{
	    $this->shippingMethodId = $shippingMethodId;
	}
	
	/**
	 * Getter for shippingMethodId
	 *
	 * @return int
	 */
	public function getShippingMethodId()
	{
	    return $this->shippingMethodId;
	}
	
	/**
	 * Setter for shippingMethodType
	 *
	 * @param string $shippingMethodType value to set
	 *
	 * @return self
	 */
	public function setShippingMethodType($shippingMethodType)
	{
	    $this->shippingMethodType = $shippingMethodType;
	}
	
	/**
	 * Getter for shippingMethodType
	 *
	 * @return string
	 */
	public function getShippingMethodType()
	{
	    return $this->shippingMethodType;
	}

	/**
	 * @return mixed
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * @param mixed $items
	 */
	public function setItems($items)
	{
		$this->items = $items;
	}

	/**
	 * Setter for weight
	 * Please note - weight units does not matter here as price calculate per weigh unit and formulas will not change for kgs vs lbs
	 * 
	 * @param float $weight value to set
	 *
	 * @return self
	 */
	public function setWeight($weight)
	{
	    $this->weight = $weight;
	}
	
	/**
	 * Getter for weight
	 *
	 * @return float
	 */
	public function getWeight()
	{
	    return $this->weight;
	}
	
	/**
	 * Setter for itemsCount
	 *
	 * @param int $itemsCount value to set
	 *
	 * @return self
	 */
	public function setItemsCount($itemsCount)
	{
	    $this->itemsCount = $itemsCount;
	}
	
	/**
	 * Getter for itemsCount
	 *
	 * @return int
	 */
	public function getItemsCount()
	{
	    return $this->itemsCount;
	}
	
	/**
	 * Setter for price of all item that have
	 *
	 * @param float $itemsPriceBasedPrice value to set
	 *
	 * @return self
	 */
	public function setItemsPriceBasedPrice($itemsPriceBasedPrice)
	{
	    $this->itemsPriceBasedPrice = $itemsPriceBasedPrice;
	}
	
	/**
	 * Getter for itemsPriceBased
	 *
	 * @return float
	 */
	public function getItemsPriceBasedPrice()
	{
	    return $this->itemsPriceBasedPrice;
	}

	/**
	 * Get shipping rate
	 *
	 * @return float
	 */
	public function getShippingRate()
	{
		$rateData = $this->shippingRepository->getCustomRate(
			$this->getShippingMethodId(),
			$this->getShippingMethodType(),
			$this->getItems(),
			$this->getItemsCount(),
			$this->getItemsPriceBasedPrice(),
			$this->getWeight()
		);

		if ($rateData)
		{
			switch ($this->getShippingMethodType())
			{
				case 'base_weight' : return floatval($rateData['base']) + (floatval($rateData['price']) * $this->getWeight());

				case 'weight' : return floatval($rateData['price']) * $this->getWeight();

				case 'flat' : return floatval($rateData['price']) * $this->getItemsCount();

				case 'price' : return $rateData['price_type'] == 'amount' ? floatval($rateData['price']) : (($this->getItemsPriceBasedPrice() / 100) * floatval($rateData['price']));

				case 'product' :
				{
					$isPrice = false;
					$shippingPrice = 0;
					foreach ($this->getItems() as $item)
					{
						foreach ($rateData as $rate)
						{
							if ($item['pid'] == $rate['pid'])
							{
								$isPrice = true;
								$shippingPrice += $rate['price'] * $item['quantity'];
							}
						}
					}

					return $isPrice ? $shippingPrice : false;
				}
			}
		}

		return false;
	}
}
<?php

class Shipping_Endicia
{
	protected $accountId;
	protected $password;
	protected $testMode;
	protected $accountStatusResponse = null;

	public function __construct($accountId, $password, $testMode)
	{
		$this->accountId = $accountId;
		$this->password = $password;
		$this->testMode = $testMode;
		$this->requesterID = 'lddm';
	}
	
	public function getEndiciaAccountPassword()
	{
		return $this->password;
	}

	public function GetEndiciaAccountStatus()
	{
		$gen_response = $this->getAccountStatusResponse();
		$gen_response2 = str_replace('www.envmgr.com/LabelService', 'http://www.envmgr.com/LabelService', $gen_response);
		$values = xml2array($gen_response2);

		return $values;
	}

	public function GetEndiciaAccountBalance()
	{
		$gen_response = $this->getAccountStatusResponse();

		$gen_response2 = str_replace('www.envmgr.com/LabelService', 'http://www.envmgr.com/LabelService', $gen_response);
		$values = xml2array($gen_response2);

		if (isset($values['ErrorMessage']) && strstr(strtolower($values['ErrorMessage']), 'test mode') === false)
		{

			return $values['ErrorMessage'];
		}

		return '$'.$this->getElementValue($gen_response, 'PostageBalance');
	}

	public function AddFundsToEndiciaAccount($p_amount)
	{
		$xml = "recreditRequestXML=<RecreditRequest><RequesterID>".$this->requesterID."</RequesterID><RequestID>".$this->Generate_RequestID()."</RequestID><CertifiedIntermediary><AccountID>".$this->accountId.
		"</AccountID><PassPhrase>".$this->password."</PassPhrase></CertifiedIntermediary><RecreditAmount>".$p_amount."</RecreditAmount></RecreditRequest>";

		$gen_response1 = $this->callEwsLabelService('BuyPostageXML', $xml);	
		$gen_response = str_replace('www.envmgr.com/LabelService', 'http://www.envmgr.com/LabelService', $gen_response1);
		$values = xml2array($gen_response);
		return $values;
	}

	/**
	 * @param Shipping_EndiciaLabelData $data
	 * @return array
	 */
	public function GetEndiciaShippingLabel(Shipping_EndiciaLabelData $data)
	{
		$xml = 'labelRequestXML=<LabelRequest Test="'.$this->testMode.'" LabelType="'.$data->labelType.'"'.
			(($data->labelSubtype == 'Integrated') ? ' LabelSubtype="'.$data->labelSubtype.'"' : ' LabelSize="'.$data->labelSize.'" ImageFormat="'.$data->imageFormat.'"').'>';

		$xml .= 
			'<RequesterID>'.utf8_encode($this->requesterID).'</RequesterID>'.
			'<AccountID>'.utf8_encode($this->accountId).'</AccountID>'.
			'<PassPhrase>'.utf8_encode($this->password).'</PassPhrase>'.
			'<MailClass>'.utf8_encode($data->mailClass).'</MailClass>'.
			'<DateAdvance>'.utf8_encode($data->daysAdvance).'</DateAdvance>'.
		 	'<WeightOz>'.utf8_encode($data->weightOz).'</WeightOz>';

        if ($data->mailPieceShape == "LargeParcel" && in_array($data->mailClass, array('First', 'Express')))
        {

        }
        else
        {
            $xml .= '<MailpieceShape>'.utf8_encode($data->mailPieceShape).'</MailpieceShape>';
        }

        $xml .=
		 	(($data->mailPieceLength > 0 && $data->mailPieceWidth > 0 && $data->mailPieceHeight > 0) ?
			 	'<MailpieceDimensions>'.
			 		'<Length>'.utf8_encode($data->mailPieceLength).'</Length>'.
			 		'<Width>'.utf8_encode($data->mailPieceWidth).'</Width>'.
			 		'<Height>'.utf8_encode($data->mailPieceHeight).'</Height>'.
			 	'</MailpieceDimensions>' : '').
		 	'<Stealth>'.utf8_encode($data->stealth).'</Stealth>'.
		 	'<ValidateAddress>FALSE</ValidateAddress>';

		if ($data->shipToCountryCode == 'US')
		{
			$xml .= '<Services InsuredMail="'.utf8_encode($data->insurance).'" DeliveryConfirmation="'.utf8_encode($data->deliveryConfirmation).'" SignatureConfirmation="'.utf8_encode($data->signatureConfirmation).'" />';
		} 

        if (($data->mailPieceShape == "Parcel" || $data->mailPieceShape == "LargeParcel") && $data->mailClass == "ParcelSelect")
        {
            $xml .=
                '<SortType>'.utf8_encode($data->sortType).'</SortType>'.
                '<EntryFacility>'.utf8_encode($data->entryFacility).'</EntryFacility>'.
                '<POZipCode>'.utf8_encode($data->shipFromZip).'</POZipCode>';
        }

		$xml .= 
			'<Value>'.utf8_encode($data->declaredValue).'</Value>'.
			'<Description>'.utf8_encode($data->packageDescription).'</Description>'.
			'<PartnerCustomerID>'.utf8_encode($this->accountId).'</PartnerCustomerID>'.
			'<RestrictedDelivery>'.utf8_encode($data->restrictedDelivery).'</RestrictedDelivery>'.
			'<NoWeekendDelivery>'.utf8_encode($data->noWeekendDelivery).'</NoWeekendDelivery>'.
		 	'<PartnerTransactionID>'.utf8_encode($this->Generate_RequestID()).'</PartnerTransactionID>'.
		 	'<ToName>'.utf8_encode($data->shipToName).'</ToName>'.
			'<ToCompany>'.utf8_encode($data->shipToCompany).'</ToCompany>'.
		  	'<ToAddress1>'.utf8_encode($data->shipToAddress1).'</ToAddress1>'.
		  	'<ToAddress2>'.utf8_encode($data->shipToAddress2).'</ToAddress2>'.
		  	'<ToCity>'.utf8_encode($data->shipToCity).'</ToCity>'.
		  	'<ToState>'.utf8_encode($data->shipToState).'</ToState>'.
		  	'<ToPostalCode>'.utf8_encode($data->shipToZip).'</ToPostalCode>'.
		   	'<ToZIP4>'.utf8_encode($data->shipToZip4).'</ToZIP4>'.
		   	'<ToDeliveryPoint>00</ToDeliveryPoint>'.
		   	'<ToPhone>'.utf8_encode($data->shipToPhone).'</ToPhone>';

		if ($data->shipToCountryCode != 'US')
		{
			$xml .= '<ToCountry>'.utf8_encode($data->shipToCountryName).'</ToCountry>';
		}

		if ($data->labelSubtype == 'Integrated')
		{
			$xml .= 
				'<IntegratedFormType>Form2976A</IntegratedFormType>'.
				'<CustomsInfo>'.
					'<ContentsType>Merchandise</ContentsType>'.
					'<CustomsItems>'.
						'<CustomsItem>'.
							'<Description>'.utf8_encode($data->shipFromName.' Order').'</Description>'.
							'<Quantity>1</Quantity>'.
							'<Weight>'.$data->weightOz.'</Weight>'.
							'<Value>'.$data->declaredValue.'</Value>'.
							'<CountryofOrigin>US</CountryofOrigin>'.
						'</CustomsItem>'.
					'</CustomsItems>'.
				'</CustomsInfo>';
		}

		$xml .= 
			'<FromCompany>'.utf8_encode($data->shipFromName).'</FromCompany>'.
			'<ReturnAddress1>'.utf8_encode($data->shipFromAddress1).'</ReturnAddress1>'.
			'<ReturnAddress2>'.utf8_encode($data->shipFromAddress2).'</ReturnAddress2>'.
			'<FromCity>'.utf8_encode($data->shipFromCity).'</FromCity>'.
			'<FromState>'.utf8_encode($data->shipFromState).'</FromState>'.
		    '<FromPostalCode>'.utf8_encode($data->shipFromZip).'</FromPostalCode>'.
		    '<FromZIP4>'.utf8_encode($data->shipFromZip4).'</FromZIP4>'.
		    '<FromPhone>'.utf8_encode($data->shipFromPhone).'</FromPhone>';

		if ($data->shipFromCountryCode != 'US')
		{
			$xml .= '<FromCountry>'.utf8_encode($data->shipToCountryName).'</FromCountry>';
		}

		$xml .= '<ResponseOptions PostagePrice="TRUE"/>';

		$xml .= '</LabelRequest>';

		$response = $this->callEwsLabelService('GetPostageLabelXML', $xml);
		$response = str_replace('www.envmgr.com/LabelService', 'http://www.envmgr.com/LabelService', $response);
		$values = xml2array($response);

		return $values;
	}

	/**
	 * @param $pic_number
	 * @param $customs_id
	 * @param $piece_id
	 * @return array
	 */
	public function RequestEndiciaPostageRefund($pic_number, $customs_id, $piece_id)
	{
		$xml = "refundRequestXML=<RefundRequest>\n"
			."<RequesterID>".utf8_encode($this->requesterID)."</RequesterID>\n"
			."<RequestID>".$this->Generate_RequestID()."</RequestID>\n"
			."<CertifiedIntermediary>\n"
			    ."<AccountID>".$this->accountId."</AccountID>\n"
			    ."<PassPhrase>".$this->password."</PassPhrase>\n"
			."</CertifiedIntermediary>\n"
			."<PicNumbers>\n"
			    ."<PicNumber>".utf8_encode($pic_number)."</PicNumber>\n"
			."</PicNumbers>\n"
			."</RefundRequest>";

		$gen_url = "https://labelserver.endicia.com/labelservice/ewslabelservice.asmx?wsdl";

		//$response = $this->requestCurl($gen_url, $xml, array('Content-Type: application/x-www-form-urlencoded'));
		$response = $this->callEwsLabelService('GetRefundXML', $xml);

		$xml = @simplexml_load_string($response);

		$result = array(
			'status' => false,
			'status_message' => '',
		);
		if (isset($xml->Refund->RefundStatus) && $xml->Refund->RefundStatus == 'Approved')
		{
			$result['status'] = true;
			$result['status_message'] = isset($xml->Refund->RefundStatusMessage) ? $xml->Refund->RefundStatusMessage : 'Approved';
		}
		else
		{
			$result['status'] = false;
			$result['status_message'] = isset($xml->Refund->RefundStatusMessage) ? $xml->Refund->RefundStatusMessage : 'Error';	
		}

		return $result;
	}

	/**
	 * @param $new_password
	 * @return array
	 */
	public function ChangeEndiciaAccountPassword($new_password)
	{
		$xml="changePassPhraseRequestXML=<ChangePassPhraseRequest><RequesterID>".$this->requesterID."</RequesterID><RequestID>".$this->Generate_RequestID()."</RequestID><CertifiedIntermediary><AccountID>".$this->accountId.
				"</AccountID><PassPhrase>".$this->password."</PassPhrase></CertifiedIntermediary><NewPassPhrase>".$new_password."</NewPassPhrase></ChangePassPhraseRequest>";
		
		$gen_response = $this->callEwsLabelService('ChangePassPhraseXML', $xml);

		return @xml2array($gen_response);
	}

	/**
	 * @param $imageData
	 * @param $orderId
	 * @param $labelId
	 * @param $trackingNumber
	 * @return string
	 */
	public function SaveEndiciaShippingLabel($imageData, $orderId, $labelId, $trackingNumber)
	{
 		$imageData = str_replace('data:image/gif;base64,', '', $imageData); 
		$imageData = str_replace(' ', '+', $imageData); 
		$labelImage = base64_decode($imageData);
		$fileName = 'images/endicia/label-'.$orderId.'-'.$labelId.'-'.$trackingNumber.'.gif'; 
		$success = file_put_contents($fileName, $labelImage); 
		return $fileName;
	}

	/**
	 * @return int
	 */
	protected function Generate_RequestID()
	{
		$requestid = rand(1000,9999);
		return $requestid;
	}

	/**
	 * @return bool|mixed|null|string
	 */
	protected function getAccountStatusResponse()
	{
		if (is_null($this->accountStatusResponse))
		{
			$xml = "accountStatusRequestXML=<AccountStatusRequest><RequesterID>".$this->requesterID."</RequesterID><RequestID>".$this->Generate_RequestID()."</RequestID><CertifiedIntermediary><AccountID>"
					.$this->accountId."</AccountID><PassPhrase>".$this->password."</PassPhrase></CertifiedIntermediary></AccountStatusRequest>";

			$this->accountStatusResponse = $this->callEwsLabelService('GetAccountStatusXML', $xml);
		}
		return $this->accountStatusResponse;
	}

	/**
	 * @param $xmlStr
	 * @param $elementName
	 * @return string
	 */
	protected function getElementValue($xmlStr, $elementName)
	{
		$a = strpos($xmlStr, "<$elementName>");
		$b = strpos($xmlStr, "</$elementName>");
		$c = $a + strlen("<$elementName>");
		$d = $b;
		$e = $d - $c;

		return substr($xmlStr, $c, $e);
	}

	/**
	 * @param $method
	 * @param $post_xml
	 * @return bool|mixed|string
	 */
	protected function callEwsLabelService($method, $post_xml)
	{
		if ($this->testMode == 'Yes')
			$gen_url = 'https://elstestserver.endicia.com/LabelService/EwsLabelService.asmx/'.$method;
		else
			$gen_url = 'https://labelserver.endicia.com/LabelService/EwsLabelService.asmx/'.$method;

		return $this->requestCurl($gen_url, $post_xml, array('Content-Type: application/x-www-form-urlencoded'));
	}

    /**
     * Curl handled
	 *
     * @param $url
     * @param $rawBody
     * @param $reqHeaders
	 *
     * @return bool|mixed|string
     */
    protected function requestCurl($url, $rawBody, $reqHeaders)
	{
		foreach ($reqHeaders as $key => $value)
		{
			$headers[] = $key . ': ' . $value;
		}
		
		$headers[] = 'Content-Length: ' . strlen($rawBody);

		$params = array();
		$params[CURLOPT_POST] = true;
		$params[CURLOPT_POSTFIELDS] = $rawBody;
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url;
		$params[CURLOPT_HTTPHEADER] = $headers;
		$params[CURLOPT_FOLLOWLOCATION] = true;
		$params[CURLOPT_SSL_VERIFYHOST] = 2;
		$params[CURLOPT_SSL_VERIFYPEER] = 0;
		$params[CURLOPT_VERBOSE] = false;
		$params[CURLOPT_HEADER] = false;

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		$response = curl_exec($ch);

		$this->log("Request:\n".$url."\n".$rawBody);
		$this->log("Response:\n".$response);

		if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404)
		{
			return '';
		}
		
		if (curl_errno($ch))
		{
            $this->log("CURL Error: ".curl_error($ch));
			return false;
		}

		@curl_close($ch);

		return $response;
	}

    /**
     * Log
	 *
     * @param $s
     */
    protected function log($s)
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			file_put_contents(
				"content/cache/log/endicia.txt", 
				date("Y/m/d-H:i:s")." - \n".(is_array($s) ? array2text($s) : $s)."\n\n\n", 
				FILE_APPEND
			);
		}
	}

    /**
     * Get instance
	 *
     * @param $settings
     *
	 * @return Shipping_Endicia
     */
    public static function getInstance(&$settings)
	{
		return new Shipping_Endicia($settings['EndiciaShippingLabelsAccountID'], $settings['EndiciaShippingLabelsPassword'], $settings['EndiciaShippingLabelsTestMode']);
	}
}
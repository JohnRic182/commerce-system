<?php

/**
 * Class Shipping_UPS
 */
class Shipping_UPS extends Shipping_Provider
{
	protected $server = ""; 
	protected $tracking_server = "";
	protected $user = ""; 
	protected $pass = ""; 
	protected $service = ""; 
	protected $dest_zip; 
	protected $orig_zip; 
	protected $pounds; 
	protected $ounces; 
	protected $container = "None"; 
	protected $size = "REGULAR"; 
	protected $machinable; 
	protected $to_country;
	protected $from_country;
	protected $pickupType;
	protected $accessKey;
	protected $weightUnit;
	protected $lengthUnit;
	protected $residential;
	protected $customerClassification;
	
	/**
	 * [setServer description]
	 * @param [type] $server [description]
	 */
	public function setServer($server)
	{
		$this->server = $server;
	}

	/**
	 * [setTrackingServer description]
	 * @param [type] $server [description]
	 */
	public function setTrackingServer($server)
	{
		$this->tracking_server = $server;
	}

	/**
	 * [setAccessKey description]
	 * @param [type] $AccessKey [description]
	 */
	public function setAccessKey($AccessKey)
	{
		$this->accessKey = $AccessKey;
	}

	/**
	 * [setUserName description]
	 * @param [type] $user [description]
	 */
	public function setUserName($user)
	{
		$this->user = $user;		
	}

	/**
	 * [setPass description]
	 * @param [type] $pass [description]
	 */
	public function setPass($pass)
	{
		$this->pass = $pass;
	} 

	/**
	 * [setService description]
	 * @param [type] $service [description]
	 */
	public function setService($service)
	{
		/* Must be: Express, Priority, or Parcel */
		$this->service = $service;
	}

	/**
	 * [setPickupType description]
	 * @param [type] $pickupType [description]
	 */
	public function setPickupType($pickupType)
	{
		$this->pickupType = $pickupType;

		if ($pickupType ==11)
		{
			$this->customerClassification = "<CustomerClassification><Code>04</Code></CustomerClassification>";
		}
		else
		{
			$this->customerClassification = '';
		}
	}

	/**
	 * [setCountry description]
	 * @param [type] $toCountry   [description]
	 * @param [type] $fromCountry [description]
	 */
	public function setCountry($toCountry,$fromCountry)
	{
		$this->to_country = $toCountry;
		$this->from_country = $fromCountry;
	}

	/**
	 * [setDestZip description]
	 * @param [type] $sending_zip [description]
	 */
	public function setDestZip($sending_zip)
	{
		/* Must be 5 digit zip (No extension) */
		$this->dest_zip = $sending_zip;
	}

	/**
	 * [setOrigZip description]
	 * @param [type] $orig_zip [description]
	 */
	public function setOrigZip($orig_zip)
	{
		$this->orig_zip = $orig_zip;
	}

	/**
	 * [setWeight description]
	 * @param [type]  $pounds [description]
	 * @param integer $ounces [description]
	 */
	public function setWeight($pounds, $ounces=0)
	{
		/* Must weight less than 70 lbs. */
		if ($pounds < 1 && $pounds > 0) $pounds = 1;
		$this->pounds = $pounds;
		$this->ounces = $ounces;
	}

	/**
	 * [setWeightUnit description]
	 * @param [type] $weightUnit [description]
	 */
	public function setWeightUnit($weightUnit)
	{
		$this->weightUnit = $weightUnit;
	}

	/**
	 * [setLengthUnit description]
	 * @param [type] $lengthUnit [description]
	 */
	public function setLengthUnit($lengthUnit)
	{
		$this->lengthUnit = $lengthUnit;
	}

	/**
	 * [setContainer description]
	 * @param [type] $cont [description]
	 */
	public function setContainer($cont)
	{
		/* 
		Valid Containers 
		Package Name             Description 
		Express Mail 
		None                For someone using their own package 
		0-1093 Express Mail         Box, 12.25 x 15.5 x 
		0-1094 Express Mail         Tube, 36 x 6 
		EP13A Express Mail         Cardboard Envelope, 12.5 x 9.5 
		EP13C Express Mail         Tyvek Envelope, 12.5 x 15.5 
		EP13F Express Mail         Flat Rate Envelope, 12.5 x 9.5 

		Priority Mail 
		None                For someone using their own package 
		0-1095 Priority Mail        Box, 12.25 x 15.5 x 3 
		0-1096 Priority Mail         Video, 8.25 x 5.25 x 1.5 
		0-1097 Priority Mail         Box, 11.25 x 14 x 2.25 
		0-1098 Priority Mail         Tube, 6 x 38 
		EP14 Priority Mail         Tyvek Envelope, 12.5 x 15.5 
		EP14F Priority Mail         Flat Rate Envelope, 12.5 x 9.5 

		Parcel Post 
		None                For someone using their own package 
		*/ 

		if ($cont=="00")
		{
			$this->container = '<PackagingType><Code>'.urlencode($cont).'</Code></PackagingType>';
		}
		else
		{
			$this->container='<PackagingType><Code>'.urlencode($cont).'</Code></PackagingType>';
		}
	} 

	/**
	 * [setResidential description]
	 * @param [type] $flag [description]
	 */
	public function setResidential($flag)
	{
		$this->residential= ($flag > 0) ? '<ResidentialAddressIndicator/>' : '';
	}

	/**
	 * [setSize description]
	 * @param [type] $size [description]
	 */
	function setSize($size)
	{
		/* Valid Sizes 
		Package Size                Description        Service(s) Available 
		Regular package length plus girth     (84 inches or less)    Parcel Post 
		Priority Mail 
		Express Mail 

		Large package length plus girth        (more than 84 inches but    Parcel Post 
		not more than 108 inches)    Priority Mail 
		Express Mail 

		Oversize package length plus girth   (more than 108 but        Parcel Post 
		not more than 130 inches) 

		*/ 
		$this->size = $size; 
	}

	// public function setMachinable($mach) { 
	/* Required for Parcel Post only, set to True or False */ 
	//      $this->machinable = $mach; 
	//  } 

	/**
	 * Get shipping rate
	 * @return mixed
	 */
	public function getShippingRate()
	{
		global $settings;
		// may need to urlencode xml portion 
		$str = 
			'<?xml version="1.0"?>'.
			'<AccessRequest xml:lang="en-US">'.
				'<AccessLicenseNumber>'.urlencode($this->accessKey).'</AccessLicenseNumber>'.
				'<UserId>'.urlencode($this->user).'</UserId>'.
				'<Password>'.urlencode($this->pass).'</Password>'.
			'</AccessRequest>'.
			'<?xml version="1.0"?>'.
			'<RatingServiceSelectionRequest xml:lang="en-US">'.
				'<Request>'.
					'<TransactionReference>'.
						'<CustomerContext>Bare Bones Rate Request</CustomerContext>'.
						'<XpciVersion>1.0001</XpciVersion>'.
					'</TransactionReference>'.
					'<RequestAction>Rate</RequestAction>'.
					'<RequestOption>Rate</RequestOption>'.
				'</Request>'.
				'<PickupType>'.
					'<Code>'.urlencode($this->pickupType).'</Code>'.
				'</PickupType>'.
				'<Shipment>'.
					'<Shipper>'.
						'<Address>'.
							'<PostalCode>'.urlencode($this->orig_zip).'</PostalCode>'.
							'<CountryCode>'.urlencode($this->from_country).'</CountryCode>'.
						'</Address>'.
					'</Shipper>'.
					'<ShipTo>'.
						'<Address>'.
							'<PostalCode>'.urlencode($this->dest_zip).'</PostalCode>'.
							'<CountryCode>'.urlencode($this->to_country).'</CountryCode>'.
							$this->residential.
						'</Address>'.
					'</ShipTo>'.
					'<ShipFrom>'.
						'<Address>'.
							'<PostalCode>'.urlencode($this->orig_zip).'</PostalCode>'.
							'<CountryCode>'.urlencode($this->from_country).'</CountryCode>'.
						'</Address>'.
					'</ShipFrom>'.
					'<Service>'.
						'<Code>'.urlencode($this->service).'</Code>'.
					'</Service>'.
					'<Package>'.
						$this->container.
						'<PackageWeight>'.
							'<UnitOfMeasurement>'.
								'<Code>'.urlencode($this->weightUnit).'</Code>'.
							'</UnitOfMeasurement>'.
							'<Weight>'.urlencode($this->pounds).'</Weight>'.
						'</PackageWeight>'.
					'</Package>'.
				'</Shipment>'.
				$this->customerClassification.
			'</RatingServiceSelectionRequest>';

			if (defined('DEVMODE') && DEVMODE)
			{
				@file_put_contents(
					dirname(dirname(dirname(__FILE__))).'/cache/log/shipping-ups.txt',
					date("Y/m/d-H:i:s")." - REQUEST:\n".$str."\n\n\n",
					FILE_APPEND
			);
		}

		$buffer = "";
		$c = curl_init();

		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			
			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_URL, $this->server);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_POSTFIELDS, $str);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		set_time_limit(3000);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/');
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

		$buffer = curl_exec($c);

		if (defined('DEVMODE') && DEVMODE)
		{
			@file_put_contents(
				dirname(dirname(dirname(__FILE__))).'/cache/log/shipping-ups.txt',
				date("Y/m/d-H:i:s")." - RESPONSE:\n".$buffer."\n\n\n", 
				FILE_APPEND
			);
		}

		if ($buffer)
		{
			$xmlParser = new XMLParser();

			$data = $xmlParser->parse($buffer);

			if ($data["RatingServiceSelectionResponse"][0]["Response"][0]["ResponseStatusDescription"][0] == "Success")
			{
				$a = $data["RatingServiceSelectionResponse"][0]["RatedShipment"][0];
				$service_code = $a["Service"][0]["Code"][0];

				if ($service_code == $this->service)
				{
					$ship_rate = $a["TotalCharges"][0]["MonetaryValue"][0];
					$currency_code = $a["TotalCharges"][0]["CurrencyCode"][0];

					$amountInDefaultCurrency = $this->getAmountInDefaultCurrency($currency_code, $ship_rate);
					$shippingFee = $this->getShippingFeeAmount($amountInDefaultCurrency);

					return $amountInDefaultCurrency + $shippingFee;
				}
			}
		}

		return false;
	}

	/**
	 * @param $trackingNo
	 * @return array|bool
	 */
	public function getTrackingInfo($trackingNo)
	{
		global $settings;
		$str = 
			'<?xml version="1.0"?>'.
			'<AccessRequest xml:lang="en-US">'.
				'<AccessLicenseNumber>'.urlencode($this->accessKey).'</AccessLicenseNumber>'.
				'<UserId>'.urlencode($this->user).'</UserId>'.
				'<Password>'.urlencode($this->pass).'</Password>'.
			'</AccessRequest>'.
			'<?xml version="1.0"?>'.
			'<TrackRequest xml:lang="en-US">'.
				'<Request>'.
					'<TransactionReference>'.
						'<CustomerContext>Example 2</CustomerContext>'.
						'<XpciVersion>1.0001</XpciVersion>'.
					'</TransactionReference>'.
					'<RequestAction>Track</RequestAction>'.
					'<RequestOption>none</RequestOption>'.
				'</Request>'.
				'<ReferenceNumber><Value>'.urlencode($trackingNo).'</Value></ReferenceNumber>'.
			'</TrackRequest>';

		$buffer = "";
		$c = curl_init();

		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_URL, $this->tracking_server);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_POSTFIELDS, $str);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		set_time_limit(3000);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/');
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);

		$buffer = curl_exec($c);

		if ($buffer)
		{
			$xmlParser = new XMLParser();
			$tracking_data = array();		
			$data = $xmlParser->parse($buffer);

			if ($data["TrackResponse"][0]["Response"][0]["ResponseStatusCode"][0] == 1 and $data["TrackResponse"][0]["Response"][0]["ResponseStatusDescription"][0] == "Success")
			{
				$a = $data["TrackResponse"][0]["Shipment"][0];
				$tracking_data["shipping_method"] = $a["Service"][0]["Description"][0];
				$tracking_data["pickup_date"] = $a["PickupDate"][0];

				$tracking_data["pickup_date"] = date('m-d-Y', strtotime($tracking_data['pickup_date']));

				if (isset($a["ScheduledDeliveryDate"][0]))
				{
					$tracking_data['scheduled_delivery_date'] = date('m-d-Y', strtotime($a["ScheduledDeliveryDate"][0]));
					$tracking_data['scheduled_delivery_time'] = date('H:i:s A', strtotime($a["ScheduledDeliveryTime"][0]));
				}

				$tracking_data["tracking_number"] = $a["Package"][0]["TrackingNumber"][0];

				if (isset($tracking_data["rescheduled_delivery_date"]))
				{
					$tracking_data["rescheduled_delivery_date"] = date('m-d-Y', strtotime($a["Package"][0]["RescheduledDeliveryDate"][0]));
				}

				$tracking_data["status_type_code"] = $a["Package"][0]["Activity"][0]["Status"][0]["StatusType"][0]["Code"][0];
				$tracking_data["status_type_description"] = ucfirst($a["Package"][0]["Activity"][0]["Status"][0]["StatusType"][0]["Description"][0]);   

				$tracking_data["status_code"] = $a["Package"][0]["Activity"][0]["Status"][0]["StatusCode"][0]["Code"][0];

				$tracking_data["activity_location"] = ucfirst($a["Package"][0]["Activity"][0]["ActivityLocation"][0]["Address"][0]["AddressLine1"][0])." ".ucfirst($a["Package"][0]["Activity"][0]["ActivityLocation"][0]["Address"][0]["AddressLine2"][0]). " ".ucfirst($a["Package"][0]["Activity"][0]["ActivityLocation"][0]["Address"][0]["AddressLine3"][0]);

				$tracking_data["activity_location1"] = $a["Package"][0]["Activity"][0]["ActivityLocation"][0]["Address"][0]["City"][0]. " ".$a["Package"][0]["Activity"][0]["ActivityLocation"][0]["Address"][0]["StateProvinceCode"][0]. " ".$a["Package"][0]["Activity"][0]["ActivityLocation"][0]["Address"][0]["PostalCode"][0]. " ".$a["Package"][0]["Activity"][0]["ActivityLocation"][0]["Address"][0]["CountryCode"][0];

				$tracking_data["activity_date"] = date('m-d-Y', strtotime($a["Package"][0]["Activity"][0]["Date"][0]));
				$tracking_data["activity_time"] = date('H:i:s A', strtotime($a["Package"][0]["Activity"][0]["Time"][0]));

				if ($a["Package"][0]["Message"][0])
				{
					$tracking_data["message"] = $tracking_data['status_code'] = $a["Package"][0]["Message"][0]["Description"][0];
				}

				$tracking_data["package_weight_unit"] = $a["Package"][0]["PackageWeight"][0]["UnitOfMeasurement"][0]["Code"][0];
				$tracking_data["package_weight"] = $a["Package"][0]["PackageWeight"][0]["Weight"][0];
				$tracking_data["shipment_weight_unit"] = $a["ShipmentWeight"][0]["UnitOfMeasurement"][0]["Code"][0];
				$tracking_data["shipment_weight"] = $a["ShipmentWeight"][0]["Weight"][0];

				return $tracking_data;
			}
			else
			{ 
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}
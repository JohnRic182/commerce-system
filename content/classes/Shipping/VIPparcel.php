<?php

	class Shipping_VIPparcel extends Shipping_Provider
	{
		// result codes
		const CODE_SUCCESS = 200;
		const CODE_CREATED = 201;
		const CODE_BAD_REQUEST = 400;
		const CODE_PAYMENT_REQUIRED = 402;
		const CODE_FORBIDDEN = 403;

		// label types
		const LT_DOMESTIC = 'domestic';
		const LT_INTERNATIONAL = 'international';

		private $serviceprovider = null;
		private $db = null;
		private $settings = null;
		private $methodData = null;
		private $from = null;
		private $to = null;
		private $weight = null;
		private $itemsCount = null;
		private $itemsPriceBasedPrice = null;
		private $lastRequestID = null;
		private $countryList = null;
		private $testMode = null;

		/**
		 * @param $val
		 * @return float
		 */
		static function weigthInOz($val)
		{
			global $settings;
			if ($settings['LocalizationWeightUnits'] == 'lbs')
			{
				return ceil($val * 16);
			}
			else
			{
				return ceil($val / 35.2739619);
			}
		}

		/**
		 * @param $code
		 * @return bool
		 */
		private function getExternalCountryID($code)
		{		
			$extCountries = $this->getCountryList();
			foreach ($extCountries as $country)
			{
				if ($country['code'] == $code)
				{
					return $country['id'];
				}
			}
			return false;
		}

		/**
		 * @param null $countryList
		 */
		public function setCountryList($countryList)
		{
			$this->countryList = $countryList;
		}

		/**
		 * @return null
		 */
		public function getCountryList()
		{
			return ($this->countryList?$this->countryList: array());
		}


		/**
		 * @param null $lastRequestID
		 */
		public function setLastRequestID($lastRequestID)
		{
			$this->lastRequestID = $lastRequestID;
		}

		/**
		 * @return null
		 */
		public function getLastRequestID()
		{
			return $this->lastRequestID;
		}

		/**
		 * @param null $from
		 */
		public function setFrom($from){
			$this->from = $from;
		}

		/**
		 * @return null
		 */
		public function getFrom(){
			return $this->from;
		}

		/**
		 * @param null $itemsCount
		 */
		public function setItemsCount($itemsCount){
			$this->itemsCount = $itemsCount;
		}

		/**
		 * @return null
		 */
		public function getItemsCount(){
			return $this->itemsCount;
		}

		/**
		 * @param null $itemsPriceBasedPrice
		 */
		public function setItemsPriceBasedPrice($itemsPriceBasedPrice){
			$this->itemsPriceBasedPrice = $itemsPriceBasedPrice;
		}

		/**
		 * @return null
		 */
		public function getItemsPriceBasedPrice(){
			return $this->itemsPriceBasedPrice;
		}

		/**
		 * @param null $weight
		 */
		public function setWeight($weight){
			$this->weight = $weight;
		}

		/**
		 * @return null
		 */
		public function getWeight(){
			return $this->weight;
		}

		/**
		 * @param null $to
		 */
		public function setTo($to){
			$this->to = $to;
		}

		/**
		 * @return null
		 */
		public function getTo(){
			return $this->to;
		}

		public function getLabelType($data = null)
		{
		
			$d = $data;
			if (!$data)
			{
				if ($this->methodData)
				{
					$d = $this->methodData;
				}
			}
			
			if ($d)
			{
				$method_countries = explode(',', $this->methodData['country']);
				if( in_array('1', $method_countries))
				{
					return self::LT_DOMESTIC;
				}
				else
				{
					return self::LT_INTERNATIONAL;
				}
			}
			return false;
		}

		public function __construct($db, $settings, $data = null)
		{
			require_once('content/vendors/VP/Client.php');
			$this->serviceprovider = new VP_Client();
			$this->db = $db;
			$this->settings = $settings;
			$this->methodData = $data;
			if ($this->settings['ShippingVIPparcelTestmode'] == 'yes')
			{
				$this->testMode = TRUE;
			}
			else
			{
				$this->testMode = FALSE;
			}

			$this->serviceprovider->is_test($this->testMode);
		}
		
		public function getParcelLabel($data)
		{
			if ($this->serviceprovider)
			{
				$db = $this->db;
				$settings = $this->settings;
				$client = $this->serviceprovider;
				
				$client->auth_token($settings['ShippingVIPparcelauthToken']);
				$labeltype = $this->getLabelType();
				if (isset($data['labelType']))
				{
					$labeltype = $data['labelType'];
				}
				
				$mailClass = $this->methodData['method_id'];
				if (isset($data['mailClass']))
				{
					$mailClass = $data['mailClass'];
				}
				
				if ($labeltype == self::LT_INTERNATIONAL)
				{
					if (!$this->getCountryList())
					{
						$request = new VP_Request_Location_Country_List();
						$request->set_params(array('authToken' => $settings['ShippingVIPparcelauthToken']));
						$client->request($request);
						$result = $client->execute($settings);
						if ($result)
						{
							$this->setCountryList($result['records']);
						}
					}
				}

				$params = array();
				$params['authToken'] = $settings['ShippingVIPparcelauthToken'];
				$request = new VP_Request_Shipping_Label_Print();
				$request->set_params(array('authToken' => $settings['ShippingVIPparcelauthToken'], 'mailClass' => $mailClass) );
				$request_params = $request->info_params();
				foreach ($request_params as $paramID)
				{
					if (isset($data[$paramID]))
					{
						$params[$paramID] = $data[$paramID];
					}
				}

				if ($params['labelType'] == self::LT_DOMESTIC)
				{

					if (isset($params['recipient']['postalCode']))
					{
						unset($params['recipient']['zip4']);
					}
					
					if (isset($params['recipient']['countryId']))
					{
						unset($params['recipient']['countryId']);
					}

					if (isset($params['recipient']['state']))
					{
						if (isset($params['recipient']['province']))
						{
							unset($params['recipient']['province']);
						}
					}
				}
				else{
					if ($params['recipient']['countryId'] !== 'US')
					{
						if (isset($params['recipient']['state']))
						{
							unset($params['recipient']['state']);
						}

						if (isset($params['recipient']['zip4']))
						{
							unset($params['recipient']['zip4']);
						}
					}
					else
					{
						if (isset($params['recipient']['state']))
						{
							if (isset($params['recipient']['province']))
						{
								unset($params['recipient']['province']);
							}
						}
					}

					if (isset($params['recipient']['countryId']))
					{
						$cid = $this->getExternalCountryID($params['recipient']['countryId']);
						if ($cid)
						{
							$params['recipient']['countryId'] = $cid;
						}
					}

				}

				$request->set_params($params);
				
				$client->request($request); 	// set request object
				$result = $client->execute($settings);
				if ($result)
				{
					if ($result['statusCode'] == self::CODE_CREATED)
					{
						return $result;
					}
					else
					{
						$err_result = array('ErrorMessage' => $result['error'], 'statusCode' => $result['statusCode']);
						return $err_result;
					}
				}
				return false;

			}
			return false;
		}

		/**
		 * Get shipping rate
		 * @return mixed
		 */
		public function getShippingRate()
		{
			if ($this->serviceprovider)
			{
				$db = $this->db;
				$settings = $this->settings;
				$client = $this->serviceprovider;
				
				$client->auth_token($settings['ShippingVIPparcelauthToken']);
				
				$labeltype = $this->getLabelType();

				$international_params = array(
					'authToken' => $settings['ShippingVIPparcelauthToken'],
					'mailClass' => $this->methodData['method_id'],
					'weightOz' => (float) self::weigthInOz($this->weight),
					'senderPostalCode' => $this->from['zip'],
					'countryId' => $this->to['country_iso_number'],
					'labelType' => 'international',
				);


				$domestic_params = array(
					'authToken' => $settings['ShippingVIPparcelauthToken'],
					'mailClass' => $this->methodData['method_id'],
					'weightOz' => (float) self::weigthInOz($this->weight),
					'senderPostalCode' => $this->from['zip'],
					'recipientPostalCode' => $this->to['zip'],
					'labelType' => 'domestic',
				);
				if (trim($settings['ShippingVIPparcelService']) !== '')
				{
					$domestic_params['service'] = $settings['ShippingVIPparcelService'];
				}

				$method_countries = explode(',', $this->methodData['country']);
				$params = array();
				if ($labeltype == self::LT_DOMESTIC)
				{
					$params = $domestic_params;
				}
				else if($labeltype == self::LT_INTERNATIONAL)
				{
					if (!$this->getCountryList())
					{
						$request = new VP_Request_Location_Country_List();
						$request->set_params(array('authToken' => $settings['ShippingVIPparcelauthToken']));
						$client->request($request); 	// set request object
						$result = $client->execute($settings);
						if ($result)
						{
							$this->setCountryList($result['records']);
						}
					}

					$cid = $this->getExternalCountryID($this->to['country_iso_a2']);
					if ($cid)
					{
						$international_params['countryId'] = $cid;
						$params = $international_params;
					}
					else
					{
						return false;
					}
				}

				$request = new VP_Request_Shipping_Label_Calculate();
				$request->set_params($params);

				$client->request($request); 	// set request object
				$result = $client->execute($settings);

				if ($result)
				{
					if ($result['statusCode'] == self::CODE_SUCCESS)
					{
						if ((float) $result['value'] > 0)
						{
							$this->setLastRequestID($result['requestId']);
							$ship_rate = $result['value'];
							$shippingFee = $this->getShippingFeeAmount($ship_rate);
							return $ship_rate + $shippingFee;
						}
					}
				}
				return false;

			}
			return false;
		}

		public function RequestPostageRefund($refundLabels, $reason = '')
		{		
			if ($this->serviceprovider)
			{
				$db = $this->db;
				$settings = $this->settings;
				$client = $this->serviceprovider;
				$client->auth_token($settings['ShippingVIPparcelauthToken']);
				if ($refundLabels)
				{
					$params = array();
					$params['authToken'] = $settings['ShippingVIPparcelauthToken'];
					$params['refundLabels'] = $refundLabels;
					$params['reason'] = $reason;
					$request = new VP_Request_Shipping_Refund_Request();
					$request->set_params(array('authToken' => $settings['ShippingVIPparcelauthToken']));
					$request->set_params($params);
					
					$client->request($request); 	// set request object
					$result = $client->execute($settings);
					if ($result)
					{
						if ($result['statusCode'] == self::CODE_CREATED)
						{
							return array('status' => 1, 'status_message' => 'ok');
						}
						else
						{
							return array('status' => 0, 'status_message' => $result['error']);
						}
					}
				}
				else
				{
					return array('status' => 0, 'status_message' => 'Refund Labels List is empty');
				}

			}
			else
			{
				return array('status' => 0, 'status_message' => 'error');
			}
		}
	}
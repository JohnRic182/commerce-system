<?php

class Shipping_DialAZip
{
	protected $serialNo;
	protected $user;
	protected $password;
	protected $settings;

	protected $lastResponse = null;

	/**
	 * Class constructor
	 */
	public function __construct($serialNo, $user, $password, &$settings = array())
	{
		$this->serialNo = '869001';
		$this->user = '869001';
		$this->password = 'Phoenix85020';
		$this->settings = $settings;
	}

	protected function call($xml)
	{
		$url = 'http://www.dial-a-zip.com/XML-Dial-A-ZIP/DAZService.asmx/MethodZIPValidate?input='.urlencode($xml);

		$c = curl_init();

		if ($this->settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $this->settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $this->settings["ProxyAddress"].":".$this->settings["ProxyPort"]);

			if ($this->settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $this->settings["ProxyUsername"].":".$this->settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($c, CURLOPT_POST, 0);

		$response = curl_exec($c);

		if (curl_errno($c) == 0 && trim($response) != '')
		{
			return xml2array(simplexml_load_string($response));
		}

		return false;
	}

	/**
	 * Get address variant
	 */
	public function getAddressVariants($fullName, $address1, $address2, $city, $stateCode, $zip, $statesData)
	{
		$xml = 
			"<VERIFYADDRESS>\n".
			"\t<COMMAND>ZIPM</COMMAND>\n".
			"\t<SERIALNO>".$this->serialNo."</SERIALNO>\n".
			"\t<PASSWORD>".$this->password."</PASSWORD>\n".
			"\t<USER>".$this->user."</USER>\n".
			"\t<ADDRESS0>".$fullName."</ADDRESS0>\n".
			"\t<ADDRESS1>".$address1."</ADDRESS1>\n".
			"\t<ADDRESS2>".$address2."</ADDRESS2>\n".
			"\t<ADDRESS3>".$city.", ".$stateCode." ".$zip."</ADDRESS3>\n".
			"</VERIFYADDRESS>";

		$result = $this->call($xml);
		
		if ($result && $result['ReturnCode'] == '31')
		{
			if ($result['NumResp'] > 0)
			{
				$addresses = array();

				foreach ($result['AddressList']['Address'] as $address)
				{
					$addresses[] = array(
						'address1' => trim(
							(is_string($address['PLow']) ? intval($address['PLow']).' ' : '').
							(is_string($address['PreDir']) ? $address['PreDir'].' ' : '').
							(is_string($address['Street']) ? $address['Street'].' ' : '').
							(is_string($address['Suff']) ? $address['Suff'].' ' : '').
							(is_string($address['PostDir']) ? $address['PostDir'].' ' : '')
							
						),
						'address2' => trim(
							(is_string($address['Unit']) ? $address['Unit'].' ' : '').
							(is_string($address['SLow']) ? intval($address['SLow']) : '')
						),
						'city' => $address['City'],
						'state_id' => $statesData[strtoupper($address['State'])]['id'],
						'state_name' => $statesData[strtoupper($address['State'])]['name'],
						'zip' => $address['ZIP5'].(is_string($address['LPLUS4']) ? '-'.$address['LPLUS4']: '')
					);
				}

				return $addresses;
			}
		}

		return false;
	}

	/**
	 * Address validation funciton
	 */
	public function validateAddress($fullName, $address1, $address2, $city, $stateCode, $zip)
	{
		$xml = 
			"<VERIFYADDRESS>\n".
			"\t<COMMAND>ZIP1</COMMAND>\n".
			"\t<SERIALNO>".$this->serialNo."</SERIALNO>\n".
			"\t<PASSWORD>".$this->password."</PASSWORD>\n".
			"\t<USER>".$this->user."</USER>\n".
			"\t<ADDRESS0>".$fullName."</ADDRESS0>\n".
			"\t<ADDRESS1>".$address1."</ADDRESS1>\n".
			"\t<ADDRESS2>".$address2."</ADDRESS2>\n".
			"\t<ADDRESS3>".$city.", ".$stateCode." ".$zip."</ADDRESS3>\n".
			"</VERIFYADDRESS>";

		$result = $this->call($xml);
		
		if ($result)
		{
			$this->setLastResponse($result);

			if (isset($result['Status']) && $result['Status'] == 'Invalid user name or password') return 0;

			return intval($result['ReturnCode']);
		}

		return 0;
	}

	/**
	 * Set last response
	 */
	public function setLastResponse($lastResponse)
	{
		$this->lastResponse = $lastResponse;
	}

	/**
	 * Return last result
	 */
	public function getLastResponse()
	{
		return $this->lastResponse;
	}
}
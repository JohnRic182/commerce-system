<?php

/**
 * Class Shipping_Provider
 */
abstract class Shipping_Provider
{
	protected $defaultCurrency = 'USD';
	protected $currencyExchangeRates = array();

	protected $shippingFeeType = "amount";
	protected $shippingFeeValue = 0.00;
	
	/**
	 * Set default currency
	 * @param string $defaultCurrency
	 */
	public function setDefaultCurrency($defaultCurrency)
	{
		$this->defaultCurrency = $defaultCurrency;
	}
	
	/**
	 * Set currency exchange rates
	 * @param array $currencyExchangeRates 
	 */
	public function setCurrencyExchangeRates($currencyExchangeRates)
	{
		$this->currencyExchangeRates = $currencyExchangeRates;
	}
	
	/**
	 * Get amount in defautl currency
	 * @param type $currency
	 * @param type $amount
	 * @return type 
	 */
	protected function getAmountInDefaultCurrency($currency, $amount)
	{
		if ($currency == $this->defaultCurrency) return $amount;
		
		if (isset($this->currencyExchangeRates[$currency]))
		{
			return (float) $amount / $this->currencyExchangeRates[$currency];
		}
		
		// TODO: possibly trow error or return null?
		return $amount;
	}

	/**
	 * [setShippingFeeType description]
	 * @param [type] $shippingFeeType [description]
	 */
	public function setShippingFeeType($shippingFeeType)
	{
		$this->shippingFeeType = $shippingFeeType;
	}

	/**
	 * [setShippingFeeValue description]
	 * @param [type] $shippingFeeValue [description]
	 */
	public function setShippingFeeValue($shippingFeeValue)
	{
		$this->shippingFeeValue = $shippingFeeValue;
	}

	/**
	 * [getShippingFee description]
	 * @param  [type] $amount [description]
	 * @return [type]         [description]
	 */
	protected function getShippingFeeAmount($amount)
	{
		return $this->shippingFeeType == 'amount' ? $this->shippingFeeValue : ($amount * $this->shippingFeeValue) / 100;
	}

	/**
	 * Returns shipping rate
	 */
	public abstract function getShippingRate();

}
<?php

/**
 * Class Shipping_Fedex
 */
class Shipping_Fedex extends Shipping_Provider
{
	public static $freightClasses = array(
		'CLASS_050', 'CLASS_055', 'CLASS_060', 'CLASS_065', 'CLASS_070', 'CLASS_077_5', 'CLASS_085', 'CLASS_092_5', 'CLASS_100', 'CLASS_110',
		'CLASS_125', 'CLASS_150', 'CLASS_175', 'CLASS_200', 'CLASS_250', 'CLASS_300', 'CLASS_400', 'CLASS_500'
	);

	public static $freightPackagingTypes = array(
		'BAG', 'BARREL', 'BASKET', 'BOX', 'BUCKET', 'BUNDLE', 'CARTON', 'CASE', 'CONTAINER', 'CRATE',
		'CYLINDER', 'DRUM', 'ENVELOPE', 'HAMPER', 'OTHER', 'PAIL', 'PALLET', 'PIECE', 'REEL', 'ROLL',
		'SKID', 'TANK', 'TUBE'
	);

	protected $wsdlFilePath = 'content/vendors/fedex/RateService_v10.wsdl';
	protected $apiKey = null;
	protected $apiPassword = null;
	protected $shippingAccount = null;
	protected $billingAccount = null;
	protected $freightAccount = null;
	protected $billingCountryCode = 'US';
	protected $meterNumber = null;
	protected $dropOffType = 'REGULAR_PICKUP'; //
	protected $serviceType = null;
	protected $packagingType = 'YOUR_PACKAGING'; // FFEDEX_BOX, FEDEX_ENVELOPE, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING
	protected $rateRequestType = 'LIST';
	protected $weightUnits = 'LB'; // LB, KG
	protected $totalWeight = null;
	protected $originAddress = null;
	protected $destinationAddress = null;
	protected $preferredCurrency = 'USD';
	protected $rateRule = 'max';
	protected $freightClass = 'CLASS_050';
	protected $freightPackaging = 'PALLET';
	protected $freightPackagingWeight = 0;

	/**
	 * Set wsdl File Path
	 *
	 * @param string $wsdlFilePath
	 */
	public function setWsdlFilePath($wsdlFilePath)
	{
		$this->wsdlFilePath = $wsdlFilePath;
	}

	/**
	 * Get wsdl File Path
	 *
	 * @return string
	 */
	public function getWsdlFilePath()
	{
		return $this->wsdlFilePath;
	}

	/**
	 * Set API Key
	 *
	 * @param string $apiKey
	 */
	public function setApiKey($apiKey)
	{
		$this->apiKey = $apiKey;
	}

	/**
	 * Get API Key
	 *
	 * @return string
	 */
	public function getApiKey()
	{
		return $this->apiKey;
	}

	/**
	 * Set API Password
	 *
	 * @param string $apiPassword
	 */
	public function setApiPassword($apiPassword)
	{
		$this->apiPassword = $apiPassword;
	}

	/**
	 * Get API Password
	 *
	 * @return string
	 */
	public function getApiPassword()
	{
		return $this->apiPassword;
	}

	/**
	 * Set Shipping Account
	 *
	 * @param string $shippingAccount
	 */
	public function setShippingAccount($shippingAccount)
	{
		$this->shippingAccount = $shippingAccount;
	}

	/**
	 * Get Shipping Account
	 *
	 * @return string
	 */
	public function getShippingAccount()
	{
		return $this->shippingAccount;
	}

	/**
	 * Set Billing Account
	 *
	 * @param string $billingAccount
	 */
	public function setBillingAccount($billingAccount)
	{
		$this->billingAccount = $billingAccount;
	}

	/**
	 * Get Billing Account
	 *
	 * @return string
	 */
	public function getBillingAccount()
	{
		return $this->billingAccount;
	}

	/**
	 * Set Freight Account
	 *
	 * @param string $freightAccount
	 */
	public function setFreightAccount($freightAccount)
	{
		$this->freightAccount = $freightAccount;
	}

	/**
	 * Get Freight Account
	 *
	 * @return string
	 */
	public function getFreightAccount()
	{
		return $this->freightAccount;
	}

	/**
	 * Set Billing Country Code
	 *
	 * @param string $billingCountryCode
	 */
	public function setBillingCountryCode($billingCountryCode)
	{
		$this->billingCountryCode = $billingCountryCode;
	}

	/**
	 * Get Billing Country Code
	 *
	 * @return string
	 */
	public function getBillingCountryCode()
	{
		return $this->billingCountryCode;
	}

	/**
	 * Set Meter Number
	 *
	 * @param string $meterNumber
	 */
	public function setMeterNumber($meterNumber)
	{
		$this->meterNumber = $meterNumber;
	}

	/**
	 * Get Meter Number
	 *
	 * @return string
	 */
	public function getMeterNumber()
	{
		return $this->meterNumber;
	}

	/**
	 * Set Drop Off Type
	 *
	 * @param string $dropOffType
	 */
	public function setDropOffType($dropOffType)
	{
		$this->dropOffType = $dropOffType;
	}

	/**
	 * Get Drop Off Type
	 *
	 * @return string
	 */
	public function getDropOffType()
	{
		return $this->dropOffType;
	}

	/**
	 * Set Service Type / Shipping Method
	 *
	 * @param string $serviceType
	 */
	public function setServiceType($serviceType)
	{
		$this->serviceType = $serviceType;
	}

	/**
	 * Get Meter Number
	 *
	 * @return string
	 */
	public function getServiceType()
	{
		return $this->serviceType;
	}

	/**
	 * Set Packaging Type
	 *
	 * @param string $packagingType
	 */
	public function setPackagingType($packagingType)
	{
		$this->packagingType = $packagingType;
	}

	/**
	 * Get Packaging Type
	 *
	 * @return string
	 */
	public function getPackagingType()
	{
		return $this->packagingType;
	}

	/**
	 * Set rate request type
	 *
	 * @param $rateRequestType
	 */
	public function setRateRequestType($rateRequestType)
	{
		$this->rateRequestType = $rateRequestType;
	}

	/**
	 * Get rate request type
	 *
	 * @return string
	 */
	public function getRateRequestType()
	{
		return $this->rateRequestType;
	}

	/**
	 * Set Weight Units
	 *
	 * @param string $weightUnits
	 */
	public function setWeightUnits($weightUnits)
	{
		$this->weightUnits = $weightUnits;
	}

	/**
	 * Get Weight Units
	 *
	 * @return string
	 */
	public function getWeightUnits()
	{
		return $this->weightUnits;
	}

	/**
	 * Set Total Weight
	 *
	 * @param float $totalWeight
	 */
	public function setTotalWeight($totalWeight)
	{
		$this->totalWeight = $totalWeight;
	}

	/**
	 * Get Total Weight
	 *
	 * @return float
	 */
	public function getTotalWeight()
	{
		return $this->totalWeight;
	}

	/**
	 * Set origin address
	 *
	 * @param array $address
	 */
	public function setOriginAddress($address)
	{
		$this->originAddress = $address;
	}

	/**
	 * Set destination address
	 *
	 * @param array $address
	 */
	public function setDestinationAddress($address)
	{
		$this->destinationAddress = $address;
	}

	/**
	 * Set preferred currency
	 *
	 * @param string $preferredCurrency
	 */
	public function setPreferredCurrency($preferredCurrency)
	{
		if ($preferredCurrency == 'TWD') $preferredCurrency = 'NTD';
		$this->preferredCurrency = $preferredCurrency;
	}

	/**
	 * Return preferred currency
	 *
	 * @return string
	 */
	public function getPreferredCurrency()
	{
		return $this->preferredCurrency;
	}

	/**
	 * Set rate rule
	 *
	 * @param string $rateRule - possible values area 'min' and 'max'
	 */
	public function setRateRule($rateRule)
	{
		$this->rateRule =$rateRule;
	}

	/**
	 * Return current rate rule
	 *
	 * @return string
	 */
	public function getRateRule()
	{
		return $this->rateRule;
	}

	/**
	 * Set freight class
	 *
	 * @param $freightClass
	 */
	public function setFreightClass($freightClass)
	{
		$this->freightClass = $freightClass;
	}

	/**
	 * Get freight class
	 *
	 * @return string
	 */
	public function getFreightClass()
	{
		return $this->freightClass;
	}

	/**
	 * Set freight packaging
	 *
	 * @param $freightPackaging
	 */
	public function setFreightPackaging($freightPackaging)
	{
		$this->freightPackaging = $freightPackaging;
	}

	/**
	 * Get freight packaging
	 *
	 * @return string
	 */
	public function getFreightPackaging()
	{
		return $this->freightPackaging;
	}

	/**
	 * Set freight packaging weight
	 *
	 * @param $freightPackagingWeight
	 */
	public function setFreightPackagingWeight($freightPackagingWeight)
	{
		$this->freightPackagingWeight = $freightPackagingWeight;
	}

	/**
	 * Get freight packaging weight
	 *
	 * @return int
	 */
	public function getFreightPackagingWeight()
	{
		return $this->freightPackagingWeight;
	}

	/**
	 * Build basic FedEd request
	 *
	 * @return array
	 */
	protected function buildBasicRequest()
	{
		$request = array(
			'WebAuthenticationDetail' => array(
				'CspCredential' => array(
					'Key' => $this->getApiKey(),
					'Password' => $this->getApiPassword()
				),
				'UserCredential' => array(
					'Key' => $this->getApiKey(),
					'Password' => $this->getApiPassword()
				)
			),
			'ClientDetail' => array(
				'AccountNumber' => $this->getShippingAccount(),
				'MeterNumber' => $this->getMeterNumber()
			),
			'TransactionDetail' => array(
				'CustomerTransactionId' => '*** Rate Request v10 using PHP ***'
			),
			'Version' => array(
				'ServiceId' => 'crs', 'Major' => '10', 'Intermediate' => '0', 'Minor' => '0'
			),
			'ReturnTransitAndCommit' => true,
			'RequestedShipment' => array(
				'ShipTimestamp' => date('c'),
				'DropoffType' => $this->getDropOffType(), // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
				'ServiceType'  => $this->getServiceType(), // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
				'PackagingType' => 'YOUR_PACKAGING', //$this->getPackagingType(),
				'TotalInsuredValue' => array('Amount'=>0, 'Currency'=>$this->getPreferredCurrency()),
				'PreferredCurrency' => $this->getPreferredCurrency(),
				'TotalWeight' => array(
					'Value' => $this->getTotalWeight(),
					'Units' => $this->getWeightUnits()
				),
				'Shipper' => array(
					'AccountNumber' => $this->getShippingAccount(),
					'Address' => array(
						'StreetLines' => array(
							isset($this->originAddress['address1']) ? $this->originAddress['address1'] : '',
							isset($this->originAddress["address2"]) ? $this->originAddress["address2"] : ''
						), // Origin details
						'City' => isset($this->originAddress["city"]) ? $this->originAddress["city"] : '',
						'StateOrProvinceCode' => isset($this->originAddress["state_code"]) ? $this->originAddress["state_code"] : '',
						'PostalCode' => $this->originAddress["zip"],
						'CountryCode' => $this->originAddress["country_iso_a2"]
					)
				),
				'Recipient' => array(
					'Address' => array(
						'StreetLines' => array(
							isset($this->destinationAddress["address1"]) ? $this->destinationAddress["address1"] : '',
							isset($this->destinationAddress["address2"]) ? $this->destinationAddress["address2"] : ''
						), // Destination details
						'City' => isset($this->destinationAddress["city"]) ? $this->destinationAddress["city"] : '',
						'StateOrProvinceCode' => isset($this->destinationAddress["state_code"]) ? $this->destinationAddress["state_code"] : '',
						'PostalCode' => $this->destinationAddress["zip"],
						'CountryCode' => $this->destinationAddress["country_iso_a2"],
						'Residential' => $this->destinationAddress["address_type"] == "Residential" ? true : false
					)
				),
				'ShippingChargesPayment' => array(
					'PaymentType' => 'SENDER',
					'Payor' => array(
						'AccountNumber' => $this->getBillingAccount(),
						'CountryCode' => $this->getBillingCountryCode()
					)
				),
				'RateRequestTypes' => $this->getRateRequestType(), //'LIST' or 'ACCOUNT'
				'PackageCount' => 1,
				'PackageDetail' => 'PACKAGE_SUMMARY', //'INDIVIDUAL_PACKAGES',  //  Or PACKAGE_SUMMARY
			)
		);

		return $request;
	}

	/**
	 * Fedex SOAP client
	 */
	public function getShippingRate()
	{
		/**
		 * Get basic request
		 */
		$request = $this->buildBasicRequest();

		/**
		 * Check for residential - do not return quote for home delivery
		 * when address is business
		 */
		if ($this->getServiceType() == 'GROUND_HOME_DELIVERY' && $this->destinationAddress["address_type"] != "Residential")
		{
			return false;
		}

		/**
		 * Finish request setup based on service type
		 */
		switch ($this->getServiceType())
		{
			/**
			 * Priority
			 */
			case 'FIRST_OVERNIGHT' :
			case 'PRIORITY_OVERNIGHT' :
			case 'STANDARD_OVERNIGHT' :
			case 'FEDEX_2_DAY' :
			case 'FEDEX_2_DAY_AM' :
			case 'FEDEX_EXPRESS_SAVER' :
			case 'FEDEX_GROUND' :
			case 'GROUND_HOME_DELIVERY' :

			/**
			 * International
			 */
			case 'EUROPE_FIRST_INTERNATIONAL_PRIORITY' :
			case 'INTERNATIONAL_ECONOMY' :
			case 'INTERNATIONAL_FIRST' :
			case 'INTERNATIONAL_PRIORITY' :
			case 'INTERNATIONAL_ECONOMY_FREIGHT' :
			case 'INTERNATIONAL_PRIORITY_FREIGHT' :
			{
				$request['RequestedShipment']['RequestedPackageLineItems'] = array(
					'SequenceNumber' => 1,
					'GroupPackageCount' => 1,
					'Weight' => array(
						'Value' => $this->getTotalWeight(),
						'Units' => $this->getWeightUnits()
					),
					'PackageCount' => 1
				);
				break;
			}

			/**
			 * Fedex Freight
			 */
			case 'FEDEX_FIRST_FREIGHT' :
			case 'FEDEX_1_DAY_FREIGHT' :
			case 'FEDEX_2_DAY_FREIGHT' :
			case 'FEDEX_3_DAY_FREIGHT' :
			{
				$request['RequestedShipment']['RequestedPackageLineItems'] = array(
					'SequenceNumber' => 1,
					'GroupPackageCount' => 1,
					'Weight' => array(
						'Value' => $this->getTotalWeight() + $this->getFreightPackagingWeight(),
						'Units' => $this->getWeightUnits()
					),
					'PackageCount' => 1
				);
				break;
			}
			case 'FEDEX_FREIGHT_PRIORITY' :
			case 'FEDEX_FREIGHT_ECONOMY' :
			{
				$request['CarrierCodes'] = 'FXFR';
				$request['VariableOptions'] = 'FREIGHT_GUARANTEE';
				$request['ClientDetail']['ClientProductId'] = 'FXCT';
				$request['ClientDetail']['ClientProductVersion'] = '2608';

				$freightAccount = trim($this->getFreightAccount());
				$freightAccount = $freightAccount != '' ? $freightAccount : $this->getShippingAccount();

				$request['RequestedShipment']['FreightShipmentDetail'] = array(
					'FedExFreightAccountNumber' => $freightAccount,
					'FedExFreightBillingContactAndAddress' => array(
						'Address' => array(
							'StreetLines' => array(
								isset($this->originAddress["address1"]) ? $this->originAddress["address1"] : '',
								isset($this->originAddress["address2"]) ? $this->originAddress["address2"] : ''
							),
							'City' => isset($this->originAddress["city"]) ? $this->originAddress["city"] : '',
							'StateOrProvinceCode' => isset($this->originAddress["state_code"]) ? $this->originAddress["state_code"] : '',
							'PostalCode' => isset($this->originAddress["zip"]) ? $this->originAddress["zip"] : '',
							'CountryCode' => isset($this->originAddress["country_iso_a2"]) ? $this->originAddress["country_iso_a2"] : ''
						)
					),
					'Role' => 'SHIPPER',
					'PaymentType' => 'PREPAID',
					'CollectTermsType' => 'STANDARD',
					'ClientDiscountPercent' => 0,
					'LineItems' => array(
						array(
							'FreightClass' => $this->getFreightClass(),
							'ClassProvidedByCustomer' => false,
							'Packaging' => $this->getFreightPackaging(),
							'Weight' => array(
								'Value' => $this->getTotalWeight() + $this->getFreightPackagingWeight(),
								'Units' => $this->getWeightUnits()
							)
						)
					)
				);

				break;
			}

			/**
			 * Unknown method
			 */
			default :
			{
				return false;
			}
		}

		/**
		 * Send request to a FedEx
		 */
		try
		{
			ini_set("soap.wsdl_cache_enabled", "0");

			if (defined('DEVMODE') && DEVMODE)
			{
				@file_put_contents(
					dirname(dirname(dirname(__FILE__))).'/cache/log/shipping-fedex.txt',
					"\n===============================================================\n".
					"===============================================================\n".
					date("Y/m/d-H:i:s")." - REQUEST -".print_r($request,1)."\n\n\n",
					FILE_APPEND
				);
			}

			$client = new SoapClient($this->getWsdlFilePath(), array('trace' => 1));

			$response = $client->getRates($request);
			if (defined('DEVMODE') && DEVMODE)
			{
				@file_put_contents(
					dirname(dirname(dirname(__FILE__))).'/cache/log/shipping-fedex.txt',
					"\n---------------------------------------------------------------\n".
					"---------------------------------------------------------------\n".
					date("Y/m/d-H:i:s")." - RESPONSE - ".print_r($response,1)."\n\n\n",
					FILE_APPEND
				);
			}

			if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR')
    		{
    			$price = 0;
				$currency = 'USD';

				$RateReplyDetailsNode = $response->RateReplyDetails;
				if (!is_array($RateReplyDetailsNode)) $RateReplyDetailsNode = array($RateReplyDetailsNode);

				foreach ($RateReplyDetailsNode as $RateReplyDetails)
				{
					$RatedShipmentDetailsNode = $RateReplyDetails->RatedShipmentDetails;
					if (!is_array($RatedShipmentDetailsNode)) $RatedShipmentDetailsNode = array($RatedShipmentDetailsNode);

					foreach ($RatedShipmentDetailsNode as $RatedShipmentDetails)
					{
						// TODO: add more specific settings for fedex rates. RateType is not outlined very well in documentation.
						// if ($RatedShipmentDetails->ShipmentRateDetail->RateType ==  'RATED_ACCOUNT_SHIPMENT')

						$_price = (float) $RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount;

						switch ($this->getRateRule())
						{
							case 'min' :
							{
								if ($price == 0 || ($_price < $price && $price > 0))
								{
									$price = $_price; //max price
									$currency = $RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Currency;
								}
								break;
							}
							default :
							case 'max' :
							{
								if ($_price > $price)
								{
									$price = $_price; //max price
									$currency = $RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Currency;
								}
								break;
							}
						}
					}
				}

				$amountInDefaultCurrency = $this->getAmountInDefaultCurrency($currency, $price);
				$shippingFee = $this->getShippingFeeAmount($amountInDefaultCurrency);

				return $amountInDefaultCurrency + $shippingFee;
    		}
    	}
    	catch (SoapFault $exception)
    	{
   			return false;
		}

		return false;
	}
}
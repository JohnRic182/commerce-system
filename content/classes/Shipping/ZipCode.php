<?php

class Shipping_ZipCode extends Shipping_Provider
{
	protected $fromAddress;
	protected $toAddress;
	protected $shippingRepository;
	protected $settings;

	protected $shippingMethodId;
	protected $shippingMethodType;
	protected $weight;
	protected $itemsCount;
	protected $itemsPriceBasedPrice;

	/**
	 * Shipping provider class constructor
	 *
	 * @param DataAccess_ShippingRepositoryInterface $shippingRepository
	 */
	public function __construct(array $fromAddress, array $toAddress, DataAccess_ShippingRepositoryInterface $shippingRepository, array &$settings)
	{
		$this->fromAddress = $fromAddress;
		$this->toAddress = $toAddress;
		$this->shippingRepository = $shippingRepository;
		$this->settings = $settings;
	}

	/**
	 * Setter for shippingMethodId
	 *
	 * @param int $shippingMethodId value to set
	 *
	 * @return self
	 */
	public function setShippingMethodId($shippingMethodId)
	{
		$this->shippingMethodId = $shippingMethodId;
	}

	/**
	 * Getter for shippingMethodId
	 *
	 * @return int
	 */
	public function getShippingMethodId()
	{
		return $this->shippingMethodId;
	}

	/**
	 * Setter for shippingMethodType
	 *
	 * @param string $shippingMethodType value to set
	 *
	 * @return self
	 */
	public function setShippingMethodType($shippingMethodType)
	{
		$this->shippingMethodType = $shippingMethodType;
	}

	/**
	 * Getter for shippingMethodType
	 *
	 * @return string
	 */
	public function getShippingMethodType()
	{
		return $this->shippingMethodType;
	}

	/**
	 * Setter for weight
	 * Please note - weight units does not matter here as price calculate per weigh unit and formulas will not change for kgs vs lbs
	 *
	 * @param float $weight value to set
	 *
	 * @return self
	 */
	public function setWeight($weight)
	{
		$this->weight = $weight;
	}

	/**
	 * Getter for weight
	 *
	 * @return float
	 */
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * Setter for itemsCount
	 *
	 * @param int $itemsCount value to set
	 *
	 * @return self
	 */
	public function setItemsCount($itemsCount)
	{
		$this->itemsCount = $itemsCount;
	}

	/**
	 * Getter for itemsCount
	 *
	 * @return int
	 */
	public function getItemsCount()
	{
		return $this->itemsCount;
	}

	/**
	 * Setter for price of all item that have
	 *
	 * @param float $itemsPriceBasedPrice value to set
	 *
	 * @return self
	 */
	public function setItemsPriceBasedPrice($itemsPriceBasedPrice)
	{
		$this->itemsPriceBasedPrice = $itemsPriceBasedPrice;
	}

	/**
	 * Getter for itemsPriceBased
	 *
	 * @return float
	 */
	public function getItemsPriceBasedPrice()
	{
		return $this->itemsPriceBasedPrice;
	}

	/**
	 * Get shipping rate
	 *
	 * @return float
	 */
	public function getShippingRate()
	{
		global $db;

		$db->query('SELECT *
FROM '.DB_PREFIX.'shipping_zip_methods m
WHERE m.ssid = '.intval($this->shippingMethodId));

		$zip = $this->toAddress['zip'];

		if (($data = $db->moveNext()) !== false)
		{
			$zips = explode(',', $data['zip_codes']);
			if (in_array($zip, $zips))
			{
				return $data['delivery_fee_type'] == 'amount' ?
					floatval($data['delivery_fee']) :
					(($this->getItemsPriceBasedPrice() / 100.0) * floatval($data['delivery_fee']));
			}
		}

		return false;
	}
}
<?php

/**
 * Class Shipping_EndiciaLabelData
 */
class Shipping_EndiciaLabelData
{
	public $shipFromName;
	public $shipFromAddress1;
	public $shipFromAddress2;
	public $shipFromCity;
	public $shipFromState;
	public $shipFromZip;
	public $shipFromZip4;
	public $shipFromPhone;
	public $shipFromCountryName;
	public $shipFromCountryCode;
	public $shipToName;
	public $shipToCompany;
	public $shipToAddress1;
	public $shipToAddress2;
	public $shipToCity;
	public $shipToState;
	public $shipToZip;
	public $shipToZip4;
	public $shipToPhone;
	public $shipToCountryName;
	public $shipToCountryCode;
	public $labelType;
	public $labelSubtype;
	public $labelSize;
	public $mailClass;
	public $daysAdvance;
	public $weightOz;
	public $mailPieceShape;
	public $mailPieceShapeWidth;
	public $mailPieceShapeLength;
	public $mailPieceShapeHeight;
	public $stealth;
	public $insurance; 
	public $signatureConfirmation;
	public $declaredValue;
	public $packageDescription;
	public $restrictedDelivery;
	public $noWeekendDelivery;
	public $imageFormat;
    public $sortType;
    public $entryFacility;
	public $deliveryConfirmation;

	/**
	 * @param $shipFromName
	 * @param $shipFromAddress1
	 * @param $shipFromAddress2
	 * @param $shipFromCity
	 * @param $shipFromState
	 * @param $shipFromZip
	 * @param $shipFromZip4
	 * @param $shipFromPhone
	 * @param $shipFromCountryName
	 * @param $shipFromCountryCode
	 * @param $shipToName
	 * @param $shipToCompany
	 * @param $shipToAddress1
	 * @param $shipToAddress2
	 * @param $shipToCity
	 * @param $shipToState
	 * @param $shipToZip
	 * @param $shipToZip4
	 * @param $shipToPhone
	 * @param $shipToCountryName
	 * @param $shipToCountryCode
	 * @param $labelType
	 * @param $labelSubtype
	 * @param $labelSize
	 * @param $mailClass
	 * @param $daysAdvance
	 * @param $weightOz
	 * @param $mailPieceShape
	 * @param $mailPieceWidth
	 * @param $mailPieceLength
	 * @param $mailPieceHeight
	 * @param $stealth
	 * @param $insurance
	 * @param $signatureConfirmation
	 * @param $declaredValue
	 * @param $packageDescription
	 * @param $restrictedDelivery
	 * @param $noWeekendDelivery
	 * @param $sortType
	 * @param $entryFacility
	 * @param $deliveryConfirmation
	 */
	public function __construct(
		$shipFromName, $shipFromAddress1, $shipFromAddress2, $shipFromCity, $shipFromState, $shipFromZip, $shipFromZip4, $shipFromPhone, $shipFromCountryName, $shipFromCountryCode,
		$shipToName, $shipToCompany, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $shipToZip4, $shipToPhone, $shipToCountryName, $shipToCountryCode,
		$labelType, $labelSubtype, $labelSize, 
		$mailClass, $daysAdvance, $weightOz, $mailPieceShape, $mailPieceWidth, $mailPieceLength, $mailPieceHeight,
		$stealth, $insurance, 
		$signatureConfirmation, $declaredValue, $packageDescription, $restrictedDelivery, $noWeekendDelivery,
        $sortType, $entryFacility, $deliveryConfirmation
	)
	{
		$this->shipFromName = $shipFromName;
		$this->shipFromAddress1 = $shipFromAddress1;
		$this->shipFromAddress2 = $shipFromAddress2;
		$this->shipFromCity = $shipFromCity;
		$this->shipFromState = $shipFromState;
		$this->shipFromZip = $shipFromZip;
		$this->shipFromZip4 = $shipFromZip4;
		$this->shipFromPhone = $this->normalizePhone($shipFromPhone);
		$this->shipFromCountryName = $shipFromCountryName;
		$this->shipFromCountryCode = $shipFromCountryCode;
		$this->shipToName = $shipToName;
		$this->shipToCompany = $shipToCompany;
		$this->shipToAddress1 = $shipToAddress1;
		$this->shipToAddress2 = $shipToAddress2;
		$this->shipToCity = $shipToCity;
		$this->shipToState = $shipToState;
		$this->shipToZip = $shipToZip;
		$this->shipToZip4 = $shipToZip4;
		$this->shipToPhone = $this->normalizePhone($shipToPhone);
		$this->shipToCountryName = $shipToCountryName;
		$this->shipToCountryCode = $shipToCountryCode;
		$this->labelType = $labelType;
		$this->labelSubtype = $labelSubtype;
		$this->labelSize = $labelSize;
		$this->mailClass = $mailClass;
		$this->daysAdvance = $daysAdvance;
		$this->weightOz = $weightOz;
		$this->mailPieceShape = $mailPieceShape;
		$this->mailPieceWidth = number_format($mailPieceWidth, 3, '.', '');
		$this->mailPieceLength = number_format($mailPieceLength, 3, '.', '');
		$this->mailPieceHeight = number_format($mailPieceHeight, 3, '.', '');
		$this->stealth = $stealth;
		$this->insurance = $insurance;
		$this->signatureConfirmation = $signatureConfirmation;
		$this->declaredValue = $declaredValue;
		$this->packageDescription = $packageDescription;
		$this->restrictedDelivery = $restrictedDelivery;
		$this->noWeekendDelivery = $noWeekendDelivery;
		$this->imageFormat = 'GIF';
        $this->sortType = $sortType;
        $this->entryFacility = $entryFacility;
		$this->deliveryConfirmation = $deliveryConfirmation;
	}

	/**
	 * @param $phone
	 * @return mixed|string
	 */
	protected function normalizePhone($phone)
	{
		$phone = preg_replace('/[^0-9]+/', '', $phone);
		if (strlen($phone) > 10)
		{
			if ($phone[0] == "1") $phone = substr($phone, 1);
			$phone = substr($phone, 0, 10);
		}

		return $phone;
	}
}

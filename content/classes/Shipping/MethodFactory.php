<?php

/**
 * Class Shipping_MethodFactory
 */
class Shipping_MethodFactory
{
	protected static $shippingMethods = array();

	/**
	 * Get shipping method by id
	 * 
	 * @param int $shippingMethodId
	 * 
	 * @return Shipping_MethodInterface
	 */
	public static function getMethod($shippingMethodId)
	{
		if (array_key_exists($shippingMethodId, self::$shippingMethods))
		{
			return self::$shippingMethods[$shippingMethodId];
		}

		return new Shipping_Method($shippingMethodId);
	}

	/**
	 * Set shipping method
	 *
	 * @param $shippingMethodId
	 * @param Shipping_MethodInterface $method
	 */
	public static function setMethod($shippingMethodId, Shipping_MethodInterface $method)
	{
		self::$shippingMethods[$shippingMethodId] = $method;
	}
}
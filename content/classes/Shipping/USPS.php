<?php

/**
 * Class Shipping_USPS
 */
class Shipping_USPS
{ 
    var $server = ""; 
    var $user = ""; 
    var $pass = ""; 
    var $service = ""; 
    var $dest_zip; 
    var $orig_zip; 
    var $pounds; 
    var $ounces; 
    var $container = ""; 
    var $size = "REGULAR"; 
    var $machinable; 
	var $country;
	var $country_code;
	var $mailtype;
	var $fc_type = "";
	var $display_mode;

	protected $countriesNames = array(
		"AF" => "afghanistan",
		"AL" => "albania",
		"DZ" => "algeria",
		"AS" => "american samoa, united states",
		"AD" => "Andorra",
		"AO" => "Angola",
		"AI" => "Anguilla",
		"AG" => "Antigua and Barbuda",
		"AR" => "Argentina",
		"AM" => "Armenia",
		"AW" => "Aruba",
		"AU" => "Australia",
		"AT" => "Austria",
		"AZ" => "Azerbaijan",
		"AP" => "Azores (Portugal)",
		"BS" => "Bahamas",
		"BH" => "Bahrain",
		"BD" => "Bangladesh",
		"BB" => "Barbados",
		"BC" => "Barbuda (Antigua and Barbuda)",
		"BY" => "Belarus",
		"BE" => "Belgium",
		"BZ" => "Belize",
		"BJ" => "Benin",
		"BM" => "Bermuda",
		"BT" => "Bhutan",
		"BO" => "Bolivia",
		"BL" => "Bonaire (Netherlands Antilles)",
		"BA" => "Bosnia-Herzegovina",
		"BW" => "Botswana",
		"BR" => "Brazil",
		"VG" => "British Virgin Islands",
		"BN" => "Brunei Darussalam",
		"BG" => "Bulgaria",
		"BF" => "Burkina Faso",
		"MM" => "Burma", 
		"BI" => "Burundi",
		"KH" => "Cambodia",
		"CM" => "Cameroon",
		"CA" => "Canada",
		"CE" => "Canary Islands (Spain)",
		"CV" => "Cape Verde",
		"KY" => "Cayman Islands",
		"CF" => "Central African Republic",
		"TD" => "Chad",
		"NN" => "Channel Islands (Jersey, Guernsey, Alderney, and Sark) (Great Britain and Northern Ireland)",
		"CL" => "Chile",
		"CN" => "China",
		"CX" => "Christmas Island (Australia)",
		"CC" => "Cocos Island (Australia)",
		"CO" => "Colombia",
		"KM" => "Comoros",
		"CD" => "Congo, Democratic Republic of the",
		"CG" => "Congo, Republic of the",
		"CR" => "Costa Rica",
		"CI" => "Cote d’Ivoire",
		"HR" => "Croatia",
		"CU" => "Cuba",
		"CB" => "Curacao (Netherlands Antilles)",
		"CY" => "Cyprus",
		"CZ" => "Czech Republic",
		"DK" => "Denmark",
		"DJ" => "Djibouti",
		"DM" => "Dominica",
		"DO" => "Dominican Republic",
		"TL" => "East Timor (Indonesia)",
		"EC" => "Ecuador",
		"EG" => "Egypt",
		"SV" => "El Salvador",
		"EN" => "England (Great Britain and Northern Ireland)",
		"GQ" => "Equatorial Guinea",
		"ER" => "Eritrea",
		"EE" => "Estonia",
		"ET" => "Ethiopia",
		"FK" => "Falkland Islands",
		"FO" => "Faroe Islands",
		"FJ" => "Fiji",
		"FI" => "Finland",
		"FR" => "France",
		"GF" => "French Guiana",
		"PF" => "French Polynesia",
		"GA" => "Gabon",
		"GM" => "Gambia",
		"GE" => "Georgia, Republic of",
		"DE" => "Germany",
		"GH" => "Ghana",
		"GI" => "Gibraltar",
		"UK" => "Great Britain and Northern Ireland ",
		"GR" => "Greece",
		"GL" => "Greenland",
		"GD" => "Grenada",
		"GP" => "Guadeloupe",
		"GU" => "Guam, United States",
		"GT" => "Guatemala",
		"GN" => "Guinea",
		"GW" => "Guinea–Bissau",
		"GY" => "Guyana",
		"HT" => "Haiti",
		"HN" => "Honduras",
		"HK" => "Hong Kong",
		"HU" => "Hungary",
		"IS" => "Iceland",
		"IN" => "India",
		"ID" => "Indonesia",
		"IR" => "Iran",
		"IQ" => "Iraq",
		"IE" => "Ireland",
		"IL" => "Israel",
		"IT" => "Italy",
		"JM" => "Jamaica",
		"JP" => "Japan",
		"JO" => "Jordan",
		"KZ" => "Kazakhstan",
		"KE" => "Kenya",
		"KI" => "Kiribati",
		"KP" => "Korea, Democratic People’s Republic of (North Korea)",
		"KR" => "Korea, Republic of (South Korea)",
		"KW" => "Kuwait",
		"KG" => "Kyrgyzstan",
		"LA" => "Laos",
		"LV" => "Latvia",
		"LB" => "Lebanon",
		"LS" => "Lesotho",
		"LR" => "Liberia",
		"LY" => "Libya",
		"LI" => "Liechtenstein",
		"LT" => "Lithuania",
		"LU" => "Luxembourg",
		"MO" => "Macao",
		"MK" => "Macedonia, Republic of",
		"MG" => "Madagascar",
		"ME" => "Madeira Islands (Portugal)",
		"MW" => "Malawi",
		"MY" => "Malaysia",
		"MV" => "Maldives",
		"ML" => "Mali",
		"MT" => "Malta",
		"MH" => "Marshall Islands, Republic of the, United States",
		"MQ" => "Martinique",
		"MR" => "Mauritania",
		"MU" => "Mauritius",
		"YT" => "Mayotte (France)",
		"MX" => "Mexico",
		"FM" => "Micronesia, Federated States of, United States",
		"MD" => "Moldova",
		"MC" => "Monaco (France)",
		"MN" => "Mongolia",
		"ME" => "Montenegro",
		"MS" => "Montserrat",
		"MA" => "Morocco",
		"MZ" => "Mozambique ",
		"MM" => "Myanmar (Burma)",
		"NA" => "Namibia",
		"NR" => "Nauru",
		"NP" => "Nepal",
		"NL" => "Netherlands",
		"AN" => "Netherlands Antilles",
		"NV" => "Nevis (Saint Christopher and Nevis)",
		"NC" => "New Caledonia",
		"NZ" => "New Zealand",
		"NI" => "Nicaragua",
		"NE" => "Niger",
		"NG" => "Nigeria",
		"KP" => "North Korea (Korea, Democratic People’s Republic of)",
		"NB" => "Northern Ireland (Great Britain and Northern Ireland)",
		"NO" => "Norway",
		"OM" => "Oman",
		"PK" => "Pakistan",
		"PW" => "Palau, United States",
		"PA" => "Panama",
		"PG" => "Papua New Guinea",
		"PY" => "Paraguay",
		"PE" => "Peru",
		"PH" => "Philippines",
		"PN" => "Pitcairn Island",
		"PL" => "Poland",
		"PT" => "Portugal",
		"PR" => "Puerto Rico, United States",
		"QA" => "Qatar",
		"RE" => "Reunion",
		"RO" => "Romania",
		"RT" => "Rota, Northern Mariana Islands, United States",
		"RU" => "Russia",
		"RW" => "Rwanda",
		"SS" => "Saba (Netherlands Antilles)",
		"SW" => "Saint Christopher and Nevis",
		"SX" => "Saint Croix, US Virgin Islands, United States",
		"EU" => "Saint Eustatius (Netherlands Antilles)",
		"SH" => "Saint Helena",
		"UV" => "Saint John, US Virgin Islands, United States",
		"KN" => "Saint Kitts (Saint Christopher and Nevis)",
		"LC" => "Saint Lucia",
		"MB" => "Saint Maarten (Dutch) (Netherlands Antilles)",
		"PM" => "Saint Pierre and Miquelon",
		"VL" => "Saint Thomas, US Virgin Islands, United States",
		"SP" => "Saipan, Northern Mariana Islands, United States",
		"WS" => "Samoa, American, United States",
		"SM" => "San Marino",
		"ST" => "Sao Tome and Principe",
		"SA" => "Saudi Arabia",
		"SF" => "Scotland (Great Britain and Northern Ireland)",
		"SN" => "Senegal",
		"RS" => "Serbia, Republic of",
		"SC" => "Seychelles",
		"SL" => "Sierra Leone",
		"SG" => "Singapore",
		"SK" => "Slovak Republic (Slovakia)",
		"SI" => "Slovenia",
		"SB" => "Solomon Islands",
		"SO" => "Somali Democratic Republic (Somalia)",
		"SO" => "Somalia",
		"ZA" => "South Africa",
		"GS" => "South Georgia (Falkland Islands)",
		"KR" => "South Korea (Korea, Republic of)",
		"ES" => "Spain",
		"LK" => "Sri Lanka",
		"SD" => "Sudan",
		"SR" => "Suriname",
		"CH" => "Swaziland",
		"SE" => "Sweden",
		"CH" => "Switzerland",
		"SY" => "Syrian Arab Republic (Syria)",
		"TA" => "Tahiti (French Polynesia)",
		"TW" => "Taiwan",
		"TJ" => "Tajikistan",
		"TZ" => "Tanzania",
		"TH" => "Thailand",
		"TI" => "Tinian, Northern Mariana Islands, United States",
		"TG" => "Togo",
		"TO" => "Tonga",
		"TT" => "Trinidad and Tobago",
		"TN" => "Tunisia",
		"TR" => "Turkey",
		"TM" => "Turkmenistan",
		"TC" => "Turks and Caicos Islands",
		"TV" => "Tuvalu",
		"UG" => "Uganda",
		"UA" => "Ukraine",
		"AE" => "United Arab Emirates",
		"GB" => "United Kingdom (Great Britain and Northern Ireland)",
		"UY" => "Uruguay",
		"UZ" => "Uzbekistan",
		"VU" => "Vanuatu",
		"VA" => "Vatican City",
		"VE" => "Venezuela",
		"VN" => "Vietnam",
		"VG" => "Virgin Islands (British)",
		"VI" => "Virgin Islands (US), United States",
		"WK" => "Wales (Great Britain and Northern Ireland)",
		"WF" => "Wallis and Futuna Islands",
		"YA" => "Yap, Micronesia, United States",
		"YE" => "Yemen",
		"ZM" => "Zambia",
		"ZW" => "Zimbabwe"
	);
	
	public function setUSPSSettings()
	{
		global $settings;
		//$usps->setServer($settings["ShippingUSPSUrl"]);
		//$usps->setUserName($settings["ShippingUSPSUserID"]);
		//$usps->setPass($settings["ShippingUSPSUserPassword"]);
		$this->server = $settings["ShippingUSPSUrl"]; 
		$this->user = $settings["ShippingUSPSUserID"];
		$this->pass = $settings["ShippingUSPSUserPassword"]; 
		$this->size = $settings["ShippingUSPSPackageSize"]; 
		//$this->display_mode = $settings["ShippingUSPSMethodDisplayMode"];		
		
	} 
     
    public function setServer($server)
    { 
        $this->server = $server; 
    } 

    public function setUserName($user)
    { 
        $this->user = $user; 
    } 

    public function setPass($pass)
    { 
        $this->pass = $pass; 
    } 

    public function setService($service)
    { 
        /* Must be: Express, Priority, or Parcel */ 
        $this->service = $service; 
    } 
	
	public function setFirstClassMethod($fc_type)
	{
		$this->fc_type = $fc_type;
	}
    
	public function setMailType($mailtype)
	{
		$this->mailtype = $mailtype;
	}

	public function setCountry($country)
	{
		$this->country = $country;
	}
	
	public function setCountryCode($country_code)
	{
		$this->country_code = $country_code;
	}

    public function setDestZip($sending_zip)
    { 
        /* Must be 5 digit zip (No extension) */ 
        $this->dest_zip = $sending_zip; 
    } 

    public function setOrigZip($orig_zip)
    { 
        $this->orig_zip = $orig_zip; 
    } 

    public function setWeight($pounds, $ounces=0)
    { 
        /* Must weight less than 70 lbs. */ 
        $this->pounds = $pounds; 
        $this->ounces = $ounces; 
    } 

    public function setContainer($cont)
    { 
        /* 
        Valid Containers 
                Package Name             Description 
        Express Mail 
                None                For someone using their own package 
                0-1093 Express Mail         Box, 12.25 x 15.5 x 
                0-1094 Express Mail         Tube, 36 x 6 
                EP13A Express Mail         Cardboard Envelope, 12.5 x 9.5 
                EP13C Express Mail         Tyvek Envelope, 12.5 x 15.5 
                EP13F Express Mail         Flat Rate Envelope, 12.5 x 9.5 

        Priority Mail 
                None                For someone using their own package 
                0-1095 Priority Mail        Box, 12.25 x 15.5 x 3 
                0-1096 Priority Mail         Video, 8.25 x 5.25 x 1.5 
                0-1097 Priority Mail         Box, 11.25 x 14 x 2.25 
                0-1098 Priority Mail         Tube, 6 x 38 
                EP14 Priority Mail         Tyvek Envelope, 12.5 x 15.5 
                EP14F Priority Mail         Flat Rate Envelope, 12.5 x 9.5 
         
        Parcel Post 
                None                For someone using their own package 
        */ 

        $this->container = $cont; 
    } 

    public function setSize($size)
    { 
        /* Valid Sizes 
        Package Size                Description        Service(s) Available 
        Regular package length plus girth     (84 inches or less)    Parcel Post 
                                        Priority Mail 
                                        Express Mail 

        Large package length plus girth        (more than 84 inches but    Parcel Post 
                            not more than 108 inches)    Priority Mail 
                                        Express Mail 

        Oversize package length plus girth   (more than 108 but        Parcel Post 
                             not more than 130 inches) 

        */ 
        $this->size = $size; 
    }

	/**
	 * @param $mach
	 */
    public function setMachinable($mach)
    { 
        /* Required for Parcel Post only, set to True or False */ 
        $this->machinable = $mach; 
    }

	/**
	 * Calculate domestic shipping price
	 *
	 * @return bool
	 */
	public function getDomesticPrice()
	{
		global $settings;
		// may need to urlencode xml portion
		$str = $this->server. "?API=RateV4&XML=<RateV4Request%20USERID=\"";
		$str .= urlencode($this->user)."\"%20PASSWORD=\"".urlencode($this->pass)."\"><Package%20ID=\"0\">";

		if ($this->display_mode == 'all')
		{
			$str .= '<Service>All</Service>';
		}
		else
		{
        	$str .= "<Service>".urlencode($this->service)."</Service>";
		}

		if (trim($this->fc_type) != '')
		{
			$str .= "<FirstClassMailType>".urlencode($this->fc_type)."</FirstClassMailType>";
		}

		$str .= "<ZipOrigination>".urlencode($this->orig_zip)."</ZipOrigination>";
		$str .= "<ZipDestination>".urlencode($this->dest_zip)."</ZipDestination>";
		$str .= "<Pounds>".urlencode($this->pounds)."</Pounds><Ounces>".urlencode($this->ounces)."</Ounces>";
		$str .= "<Container>".urlencode($this->container)."</Container><Size>".urlencode($this->size)."</Size>";
		$str .= "<Machinable>".urlencode($this->machinable)."</Machinable></Package></RateV4Request>";

		if (defined('DEVMODE') && DEVMODE)
		{
			@file_put_contents(
				dirname(dirname(__FILE__)).'/cache/log/shipping-usps.txt',
				date("Y/m/d-H:i:s")." - Request:\n".$str."\n\n\n",
				FILE_APPEND
			);
		}

		$maxTrials = 3;

		for ($i=0; $i<$maxTrials; $i++)
		{
			$cu = curl_init($str);

			if ($settings["ProxyAvailable"] == "YES")
			{
				curl_setopt($cu, CURLOPT_VERBOSE, 1);

				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($cu, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}

				curl_setopt($cu, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($cu, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}

				curl_setopt($cu, CURLOPT_TIMEOUT, 120);
			}

			curl_setopt($cu, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($cu, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($cu, CURLOPT_SSL_VERIFYHOST, 0);

			$body = curl_exec($cu);

			if (defined('DEVMODE') && DEVMODE)
			{
				@file_put_contents(
					dirname(dirname(__FILE__)).'/cache/log/shipping-usps.txt',
					date("Y/m/d-H:i:s")." - Response (attempt ".$i."):\n".$body."\n\n\n",
					FILE_APPEND
				);
			}

			if (curl_errno($cu) == 0 && $body && $body != '')
			{
				curl_close($cu);
				break;
			}
			else
			{
				curl_close($cu);
				sleep($i+1);
			}

			if ($i >= $maxTrials-1)
			{
				return false;
			}
		}

		if (!preg_match("/Error/", $body))
		{
			$xmlParser = new XMLParser();
			$data = $xmlParser->parse($body);

			if (isset($data["RateV4Response"][0]["Package"][0]["Postage"][0]))
			{
				$tmp_price = $data["RateV4Response"][0]["Package"][0]["Postage"][0];
				return($tmp_price["Rate"][0]);
			}
		}

		return false;
	}
	
	/**        }
	 * Get international shipping price
	 *
	 * @global type $settings
	 * @global type $_SESSION
	 * @global type $order
	 *
	 * @return type 
	 */
	public function getInternationalPrice()
	{
		global $settings, $_SESSION, $order;
		
		$parts = explode("=", $this->mailtype);
		$mailtype = strtolower(trim($parts[0]));
		$service_id = strtolower(trim($parts[1]));

		$countryNameUpper = strtoupper($this->country);
		$countryCodeUpper = strtoupper($this->country_code);
		$countryName = null;
		
		foreach ($this->countriesNames as $key => $value)
		{
			if ($key == $countryCodeUpper || strtoupper($value) == $countryNameUpper)
			{
				$countryName = $value;
				break;
			}
		}
		
		if (is_null($countryName)) return false;
		
		$sessionKey = "USPS_InternationalCacheResponse".str_replace(' ', '', $parts[0].($order->getSubtotalAmount().'.'.$order->getItemsCount().'.'.$this->pounds.'.'.$this->ounces));

		$xml = '<IntlRateV2Request USERID="'.$this->user.'" PASSWORD="'.$this->pass.'">'.
		'<Revision>2</Revision>'.
		'<Package ID="0">'.
		'<Pounds>'.$this->pounds.'</Pounds><Ounces>'.$this->ounces.'</Ounces>'.
		'<MailType>'.$mailtype.'</MailType>'.
		'<ValueOfContents>'.number_format($order->subtotalAmount, 2, '.', '').'</ValueOfContents>'.
		'<Country>'.$countryName.'</Country>'.
		'<Container>RECTANGULAR</Container>'.
		'<Size>'.strtoupper($this->size).'</Size>';

		// The package size determines the maximum/min width/height
		$xml .= (strtoupper($this->size) == 'REGULAR')
			? "<Width></Width><Length></Length><Height></Height><Girth></Girth>"
			: "<Width>".intval($settings['ShippingUSPSPackageWidth'])."</Width><Length>".intval($settings['ShippingUSPSPackageLength'])."</Length><Height>".intval($settings['ShippingUSPSPackageHeight'])."</Height><Girth>".ceil(2 * (intval($settings['ShippingUSPSPackageWidth']) + intval($settings['ShippingUSPSPackageLength'])))."</Girth>";

		$xml .= '<OriginZip>'.urlencode($this->orig_zip).'</OriginZip>';
		$now = date('c');
		$xml .= '<AcceptanceDateTime>'.$now.'</AcceptanceDateTime>';
		$xml .= '<DestinationPostalCode>'.$this->dest_zip.'</DestinationPostalCode>';

		$xml .= '</Package></IntlRateV2Request>';
		$request = $this->server.'?API=IntlRateV2&XML='.urlencode($xml);

		if (defined('DEVMODE') && DEVMODE)
		{
			@file_put_contents(
				dirname(dirname(__FILE__)).'/cache/log/shipping-usps.txt',
				date("Y/m/d-H:i:s")." - Request:\n".$xml."\n\n\n",
				FILE_APPEND
			);
		}

		$maxTrials = 3;

		for ($i=0; $i<$maxTrials; $i++)
		{
			$cu = curl_init($request);

			if ($settings["ProxyAvailable"] == "YES")
			{
				curl_setopt($cu, CURLOPT_VERBOSE, 1);

				if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
				{
					curl_setopt($cu, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
				}

				curl_setopt($cu, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

				if ($settings["ProxyRequiresAuthorization"] == "YES")
				{
					curl_setopt($cu, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
				}

				curl_setopt($cu, CURLOPT_TIMEOUT, 120);
			}

			curl_setopt($cu, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($cu, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($cu, CURLOPT_SSL_VERIFYHOST, 0);

			$body = curl_exec($cu);

			if (defined('DEVMODE') && DEVMODE)
			{
				@file_put_contents(
					dirname(dirname(__FILE__)).'/cache/log/shipping-usps.txt',
					date("Y/m/d-H:i:s")." - Response (attempt ".$i."):\n".$body."\n\n\n",
					FILE_APPEND
				);
			}

			if (curl_errno($cu) == 0 && $body && $body != '')
			{
				curl_close($cu);
				break;
			}
			else
			{
				curl_close($cu);
				sleep($i+1);
			}

			if ($i >= $maxTrials-1) return false;
		}

		if (!preg_match("/Error/", $body))
		{
			$xmlParser = new XMLParser();
			$data = $xmlParser->parse($body);

			if (isset($data["IntlRateV2Response"][0]["Package"][0]["Service"]))
			{
				$services = $data["IntlRateV2Response"][0]["Package"][0]["Service"];

				for ($i=0; $i<count($services); $i++)
				{
					$service = $services[$i];
					$returned_service_id = $data["IntlRateV2Response"][0]["Package"][0]["Service@"][$i]["ID"];

					if ($returned_service_id == $service_id)
					{
						return $service["Postage"][0];
					}
				}
			}
		}

		return false;

	}
	
	function track($tracking_number)
	{
		global $settings;
		$xml =
			'<TrackRequest%20USERID="'.$this->user.'"%20PASSWORD="'.$this->pass.'">'.
			'<TrackID%20ID="'.$tracking_number.'"></TrackID>'.
			'</TrackRequest>';
		$request = $this->server."?API=TrackV2&XML=".$xml;

		$c = curl_init($request);

		if ($settings["ProxyAvailable"] == "YES")
		{
			//curl_setopt($c, CURLOPT_VERBOSE, 1);
			if(defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5")){
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);

			if($settings["ProxyRequiresAuthorization"] == "YES"){
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($c, CURLOPT_CAPATH, $settings['GlobalServerPath'].$settings['SecuritySslDirectory']);
		curl_setopt($c, CURLOPT_CAINFO, $settings['GlobalServerPath'].$settings['SecuritySslDirectory'].'/'.$settings['SecuritySslPem']);
		$body = curl_exec($c);
		
		if (curl_errno($c) == 0)
		{
			$xml_parser = new XMLParser();
		
			$xml_result = $xml_parser->parse($body);
			
			if (isset($xml_result["TrackResponse"][0]))
			{
				if (isset($xml_result["TrackResponse"][0]["TrackInfo"][0]))
				{
					$result = $xml_result["TrackResponse"][0]["TrackInfo"][0]["TrackSummary"][0];
					
					if (isset($xml_result["TrackResponse"][0]["TrackInfo"][0]["TrackDetail"]))
					{
						for ($i=0; $i<count($xml_result["TrackResponse"][0]["TrackInfo"][0]["TrackDetail"]); $i++)
						{
							$result.="<br/></br>".$xml_result["TrackResponse"][0]["TrackInfo"][0]["TrackDetail"][$i];
						}
					}
					return $result;
				}
			}
		}
		return false;
	}
} 

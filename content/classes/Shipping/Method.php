<?php

/**
 * Class Shipping_Method
 */
class Shipping_Method implements Shipping_MethodInterface
{
	protected $shippingMethodId;

	/**
	 * Class constructor
	 *
	 * @param int $shippingMethodId
	 */
	public function __construct($shippingMethodId)
	{
		$this->shippingMethodId = $shippingMethodId;
	}

	/**
	 * Setter for shippingMethodId
	 *
	 * @param string $shippingMethodId value to set
	 */
	public function setShippingMethodId($shippingMethodId)
	{
		$this->shippingMethodId = $shippingMethodId;
	}
	
	/**
	 * Getter for shippingMethodId
	 *
	 * @return string
	 */
	public function getShippingMethodId()
	{
		return $this->shippingMethodId;
	}
	
	/**
	 * Calculate shipping price
	 * 
	 * @param DB db
	 * @param array $from
	 * @param array $to
	 * @param array $items
	 * @param float $weight
	 * @param int $itemsCount
	 * @param float|array $itemsPriceBasedPrice
	 * @param string $errorMessage
	 * @param string $defaultCurrencyCode
	 * @param array $currencyExchangeRates
	 * 
	 * @return float
	 */
	public function calculate($db, $from, $to, $items, $weight, $itemsCount, $itemsPriceBasedPrice, &$errorMessage, $defaultCurrencyCode = 'USD', $currencyExchangeRates = array())
	{
		return Shipping::getShippingPrice(
			$db, $from, $to, $items,
			$weight, $itemsCount, $itemsPriceBasedPrice, $this->getShippingMethodId(), 
			$errorMessage, $defaultCurrencyCode, $currencyExchangeRates
		);
	}
}
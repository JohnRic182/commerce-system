<?php

/**
 * Class Shipping_CanadaPost
 */
class Shipping_CanadaPost
{
	var $curl_request_timeout = 5;
	var $xml_doc, $xml_result;
	var $server, $merchantId;
	var $destination, $origin;
	var $options;
	var $items;
	var $errors = false;
	var $_settings = null;

	/**
	 * Shipping_CanadaPost constructor
	 * 
	 * @param string $server
	 * @param string $merchantId
	 * @param $settings
	 */
	public function __construct($server = 'http://206.191.4.228:30000/', $merchantId = 'CPC_DEMO_XML', &$settings)
	{
		$this->server = $server;
		$this->merchantId = $merchantId;
		$this->_settings = $settings;
		$this->_initParamaters();
	}

	/**
	 *
	 */
	public function _initParamaters()
	{
		$this->origin = array('postal' => ' ');

		$this->destination = array(
			'country' => ' ',
			'province' => ' ',
			'city' => ' ',
			'postal' => ' '
		);

		$this->options = array(
			'tat' => 0,
			'itemsvalue' => -1
		);

		$this->items = array();
		$this->xml_result = null;
	}

	/**
	 *
	 */
	public function createXMLRequest()
	{
		$xml = '<eparcel>';
		$xml .= '<language>en</language>';
		$xml .= '<ratesAndServicesRequest>';
		$xml .= '<merchantCPCID>' . $this->merchantId . '</merchantCPCID>';
		$xml .= '<fromPostalCode>' . $this->origin['postal'] . '</fromPostalCode>';

		if ($this->options['tat'] != 0) $xml .= '<turnAroundTime>'.$this->options['tat'].'</turnAroundTime>';

		if ($this->options['itemsvalue'] != -1) $xml .= '<itemsPrice>' . $this->options['itemsvalue'] . '</itemsPrice>';

		$xml .= '<lineItems>';

		foreach ($this->items as $item)
		{
			$xml .= '<item>';
			$xml .= '<quantity>' . $item['quantity'] . '</quantity>';
			$xml .= '<weight>' . $item['weight'] . '</weight>';
			$xml .= '<length>' . $item['length'] . '</length>';
			$xml .= '<width>' . $item['width'] . '</width>';
			$xml .= '<height>' . $item['height'] . '</height>';
			$xml .= '<description>' . $item['description'] . '</description>';
			$xml .= '</item>';
		}

		$xml .= '</lineItems>';

		$xml .= '<city>' . $this->destination['city'] . '</city>';
		$xml .= '<provOrState>' . $this->destination['province'] . '</provOrState>';
		$xml .= '<country>' . $this->destination['country'] . '</country>';
		$xml .= '<postalCode>' . $this->destination['postal'] . '</postalCode>';
		$xml .= '</ratesAndServicesRequest>';
		$xml .= '</eparcel>';
		$this->xml_doc = $xml;

	}

	/**
	 * @param $quantity
	 * @param $weight
	 * @param $length
	 * @param $width
	 * @param $height
	 * @param $description
	 */
	public function addItem($quantity, $weight, $length, $width, $height, $description)
	{
		$this->items[] = array(
			'quantity' => $quantity,
			'weight' => $weight,
			'length' => $length,
			'width' => $width,
			'height' => $height,
			'description' => $description
		);
	}

	/**
	 * @param $option
	 * @param $value
	 */
	public function setOption($option, $value)
	{
		if (array_key_exists($option, $this->options));
		$this->options[$option] = $value;
	}

	/**
	 * @param $postal
	 */
	public function setOrigin($postal)
	{
		$this->origin['postal'] = str_replace(" ", "", $postal);
	}

	/**
	 * @param $city
	 * @param $province
	 * @param $country
	 * @param $postal
	 */
	public function setDestination($city, $province, $country, $postal)
	{
		$this->destination['city'] = $city;
		$this->destination['province'] = $province;
		$this->destination['country'] = $country;
		$this->destination['postal'] = str_replace(" ", "", $postal);
	}

	/**
	 * @return bool|mixed
	 */
	public function _callHost( )
	{
		$url = $this->server;

		// build the xml request
		$request = $this->xml_doc;

		// build xml header
		$header[] = 'Content-type: text/xml';
		$header[] = 'Content-length: ' . strlen($request);

		// set curl parameters and send data to host
		$ch = curl_init();

		if (($this->_settings['ProxyAvailable'] == 'YES') != false)
		{
			if (defined('CURLOPT_PROXYTYPE') && defined('CURLPROXY_HTTP') && defined('CURLPROXY_SOCKS5'))
			{
				curl_setopt($ch, CURLOPT_PROXYTYPE, $this->_settings['ProxyType'] == 'HTTP' ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}

			curl_setopt($ch, CURLOPT_PROXY, $this->_settings['ProxyAddress'] . ':' . $this->_settings['ProxyPort']);

			if ($this->_settings['ProxyRequiresAuthorization'] == 'YES')
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->_settings['ProxyUsername'] . ':' . $this->_settings['ProxyPassword']);
			}

			curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		}

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->curl_request_timeout);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_CAPATH, $this->_settings['GlobalServerPath'] . $this->_settings['SecuritySslDirectory']);
		curl_setopt($ch, CURLOPT_CAINFO, $this->_settings['GlobalServerPath'] . $this->_settings['SecuritySslDirectory'] . '/' . $this->_settings['SecuritySslPem']);

		if (defined('DEVMODE') && DEVMODE)
		{
			@file_put_contents(
				dirname(dirname(__FILE__)) . '/cache/log/shipping-canada_post.txt',
				date("Y/m/d-H:i:s") . ' - ' . $request . "\n\n\n",
				FILE_APPEND
			);
		}

		// send data to canada post
		$data = curl_exec($ch);

		if (defined('DEVMODE') && DEVMODE)
		{
			@file_put_contents(
				dirname(dirname(__FILE__)) . '/cache/log/shipping-canada_post.txt',
				date('Y/m/d-H:i:s') . ' - ' . $data . "\n\n\n",
				FILE_APPEND
			);
		}

		if (!curl_errno($ch))
		{
			curl_close($ch);

			return $data;
		}
		else
		{
			//error..
		}

		return false;
	}

	/**
	 * @return array|null
	 */
	public function getQuote()
	{
		if ($resultXML = $this->_callHost())
		{
			$this->xml_result = $this->_parseResult($resultXML);
		}
		else
		{
			$this->xml_result = null;
		}

		return $this->xml_result;
	}

	/**
	 * @param $resultXML
	 * @return array
	 */
	public function _parseResult($resultXML)
	{
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $resultXML, $vals, $index);
		xml_parser_free($xml_parser);

		$params = array();
		$level = array();

		foreach ($vals as $xml_elem)
		{
			if ($xml_elem['type'] == 'open')
			{
				if (array_key_exists('attributes',$xml_elem))
				{
					list($level[$xml_elem['level']], $extra) = array_values($xml_elem['attributes']);
				}
				else
				{
					$level[$xml_elem['level']] = $xml_elem['tag'];
				}
			}

			if ($xml_elem['type'] == 'complete')
			{
				$start_level = 1;
				$php_stmt = '$params';

				while($start_level < $xml_elem['level'])
				{
					$php_stmt .= '[$level['.$start_level.']]';
					$start_level++;
				}

				$php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
				@eval($php_stmt);
			}
		}

		$CPMethod = array();

		if (isset($params['EPARCEL']) && isset($params['EPARCEL']['RATESANDSERVICESRESPONSE']))
		{
			$ratesAndServices = $params['EPARCEL']['RATESANDSERVICESRESPONSE'];

			if ($ratesAndServices['STATUSCODE'] == '1')
			{
				foreach ($ratesAndServices as $key=>$value)
				{
					if (is_numeric($key) && is_array($ratesAndServices[$key]))
					{
						$CPMethod[] = array(
							'id' => (string)$key,
							'rate' => (float)$value['RATE'],
							'delivery' => (string)$value['DELIVERYDATE'],
							'name' => (string)$value['NAME']
						);
					}
				}
			}
		}

		return $CPMethod;
	}
}


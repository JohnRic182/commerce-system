<?php
/**
 * Interface Shipping_MethodInterface
 */
interface Shipping_MethodInterface 
{
	/**
	 * Set shipping method Id
	 * 
	 * @param int $shippingMethodId
	 */
	public function setShippingMethodId($shippingMethodId);

	/**
	 * Return shipping method Id
	 *
	 * @return int
	 */
	public function getShippingMethodId();

	/**
	 * Calculate shipping price
	 * 
	 * @param DB db
	 * @param array $from
	 * @param array $to
	 * @param array $items
	 * @param float $weight
	 * @param int $itemsCount
	 * @param float|array $itemsPriceBasedPrice
	 * @param string $errorMessage
	 * @param string $defaultCurrencyCode
	 * @param array $currencyExchangeRates
	 * 
	 * @return float
	 */
	public function calculate($db, $from, $to, $items, $weight, $itemsCount, $itemsPriceBasedPrice, &$errorMessage, $defaultCurrencyCode = 'USD', $currencyExchangeRates = array());
}
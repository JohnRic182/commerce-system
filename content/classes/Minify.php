<?php

/**
 * Class Minify
 */
class Minify
{
	/**
	 * @param $css
	 * @return mixed
	 */
	public static function minifyCSS($css)
	{
		$css = preg_replace('#\s+#', ' ', $css);
		$css = preg_replace('#/\*.*?\*/#s', '', $css);
		$css = str_replace('; ', ';', $css);
		$css = str_replace(': ', ':', $css);
		$css = str_replace(' {', '{', $css);
		$css = str_replace('{ ', '{', $css);
		$css = str_replace(', ', ',', $css);
		$css = str_replace('} ', '}', $css);
		$css = str_replace(';}', '}', $css);

		$colorCodes = array('#00FFFF', '#F0FFFF', '#F5F5DC', '#FFE4C4', '#000000', '#0000FF', '#A52A2A', '#FF7F50', '#00FFFF', '#FFD700', '#808080', '#008000', '#4B0082', '#FFFFF0', '#F0E68C', '#00FF00', '#FAF0E6', '#800000', '#000080', '#808000', '#FFA500', '#DA70D6', '#CD853F', '#FFC0CB', '#DDA0DD', '#800080', '#FF0000', '#FA8072', '#A0522D', '#C0C0C0', '#FFFAFA', '#D2B48C', '#008080', '#FF6347', '#EE82EE', '#F5DEB3', '#FFFFFF', '#FFFF00');
		$colorNames = array('aqua', 'azure', 'beige', 'bisque', 'black', 'blue', 'brown', 'coral', 'cyan', 'gold', 'gray', 'green', 'indigo', 'ivory', 'khaki', 'lime', 'linen', 'maroon', 'navi', 'olive', 'orange', 'orchid', 'peru', 'pink', 'plum', 'purple', 'red', 'salmon', 'sienna', 'silver', 'snow', 'tan', 'teal', 'tomato', 'violet', 'wheat', 'white', 'yellow');
		$css = str_ireplace($colorCodes, $colorNames, $css);

		$css = str_replace("\r\n", "\n", $css);
		return $css;
	}

	/**
	 * @param $html
	 * @return mixed
	 */
	public static function minifyHTML($html)
	{
		// $html = preg_replace('/<!--([\\s\\S]*?)-->/', '', $html);
		$html = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $html);
		$html = str_ireplace(
			array("\r\n", " >", "\t"),
			array("\n", ">", ""),
			$html
		);
		return $html;
	}
}
<?php

class Timezone
{
	/**
	 * @return string
	 */
	public static function getApplicationDefaultTimezone()
	{
		return date_default_timezone_get();
	}

	/**
	 * Sets the default timezone to use in PHP and MySQL during runtime of the application
	 *
	 * @param DB $db
	 * @param $settings
	 *
	 * @return string
	 *
	 * @throws Exception
	 */
	public static function setApplicationDefaultTimezone(DB $db, $settings)
	{
		if (!(!isset($settings['LocalizationUseSystemTimezone']) || $settings['LocalizationUseSystemTimezone'] == 'Yes'))
		{
			$timezone = $settings['LocalizationDefaultTimezone'];

			// Validate the timezone setting
			if (!is_string($timezone) || ($timezone != '' && !date_default_timezone_set($timezone)))
			{
				throw new Exception('Invalid timezone set');
			}
		}

		// For PHP 5.0 compatibility, otherwise we can use the 'P' date format
		$offset = date('O');
		$offset = substr($offset, 0, 3).':'.substr($offset, 3);

		// Set the mysql timezone offset
		$db->query('SET time_zone="'.$offset.'"');

		return date_default_timezone_get();
	}
}
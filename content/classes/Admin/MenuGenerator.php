<?php
/**
 * Class Admin_MenuGenerator
 */
class Admin_MenuGenerator
{
	protected $adminPrivileges = null;
	protected $currentUrlQuery = null;

	/** @var Admin_Menu */
	protected $currentMenuItem = null;
	protected $currentMenuItemParents = null;
	protected $currentMenuItemChildren = null;
	
	protected $generatedMenu = array();

	/**
	 * Draw menu
	 *
	 * @param Admin_Menu $adminMenu
	 * @param $currentMenuItem
	 * @param $adminMenuHistoryItems
	 * @param $adminPrivileges
	 *
	 * @return array
	 */
	public function generate(Admin_Menu $adminMenu, $currentMenuItem, $adminMenuHistoryItems, $adminPrivileges)
	{
		$this->generatedMenu = array();
		
		$this->adminPrivileges = $adminPrivileges;
		$this->currentMenuItem = $currentMenuItem;;
		
		if (!is_null($this->currentMenuItem))
		{
			$this->currentMenuItemParents = $this->currentMenuItem->getParents();
			$this->currentMenuItemChildren = $this->currentMenuItem->getChildren();
		}
		
		/**
		 * Set menu data
		 */
		$this->generatedMenu['menuId'] = $adminMenu->id;
		$this->generatedMenu['menuClassInner'] = $adminMenu->classInner;
		$this->generatedMenu['menuClassOuter'] = $adminMenu->classOuter;
		$this->generatedMenu['menuAttributes'] = $adminMenu->attributes;
		
		/**
		 * Set history items
		 */
		if (!is_null($adminMenuHistoryItems))
		{
			$this->generatedMenu['historyItems'] = array();
		
			foreach ($adminMenuHistoryItems as $adminMenuHistoryItem)
			{
				if (!is_null($adminMenuHistoryItem->url) && $adminMenuHistoryItem->privilegeMatch($adminPrivileges) && $adminMenuHistoryItem->url)
				{
					$this->generatedMenu['historyItems'][] = $this->setHistoryItem($adminMenuHistoryItem);
				}
				
				if (count($this->generatedMenu['historyItems']) >= 7) break;
			}
		}
		
		/**
		 * Set menu items
		 */
		$topLevelItems = $adminMenu->getChildrenByPrivileges($this->adminPrivileges);

		if (!is_null($topLevelItems))
		{
			$this->generatedMenu['menuItems'] = array();
			$this->generatedMenu['menuItems'][$adminMenu->id] = array(); //top level parent
			
			foreach ($topLevelItems as $menuItem)
			{
				$this->generatedMenu['menuItems'][$adminMenu->id][] = $this->setMenuItem($menuItem, 1);
			}
		}

		return $this->generatedMenu;
	}

	/**
	 * Set menu item
	 *
	 * @param Admin_MenuItem $menuItem
	 * @param $level
	 *
	 * @return Admin_MenuItem
	 */
	protected function setMenuItem(Admin_MenuItem $menuItem, $level)
	{
		$menuItem->level = $level;
		
		$menuItem->isOpen = 
			!is_null($this->currentMenuItem) && 
			(
				(!is_null($this->currentMenuItemParents) && isset($this->currentMenuItemParents[$menuItem->id])) || 
				(!is_null($this->currentMenuItemChildren) && isset($this->currentMenuItemChildren[$menuItem->id]))
			) && 
			($menuItem->level == 1 || (!is_null($this->currentMenuItemParents) && isset($this->currentMenuItemParents[$menuItem->parent_id]) && $menuItem->level - 1 == $this->currentMenuItemParents[$menuItem->parent_id]->level));
		
		$menuItem->isActive = !is_null($this->currentMenuItem) && $menuItem->id == $this->currentMenuItem->id;
		
		$children = $menuItem->getChildrenByPrivileges($this->adminPrivileges);
		
		if (!is_null($children))
		{
			$menuItem->hasChildren = true;
			
			foreach ($children as $child)
			{
				$this->generatedMenu['menuItems'][$menuItem->id][] = $this->setMenuItem($child, $level + 1);
			}
		}
		
		return $menuItem;
	}

	/**
	 * Set history item
	 *
	 * @param Admin_MenuItem $menuItem
	 *
	 * @return Admin_MenuItem
	 */
	protected function setHistoryItem(Admin_MenuItem $menuItem)
	{
		$menuItem->isActive = !is_null($this->currentMenuItem) && $menuItem->id == $this->currentMenuItem->id;
		
		return $menuItem;
	}
}

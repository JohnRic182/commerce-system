<?php

/**
 * Class Admin_MenuProvider
 */
class Admin_MenuProvider
{
	/**
	 * Build menu structure
	 *
	 * @return Admin_Menu
	 */
	public function getMenu()
	{
		/** @var Admin_Menu $adminMenu */
		$adminMenu = new Admin_Menu('leftSideMenu', 'Menu');

		$adminMenu->add(
			//$itemDashboard = new Admin_MenuItem('pi-dashboard', PRIVILEGE_ALL + PRIVILEGE_ORDERS + PRIVILEGE_USERS + PRIVILEGE_RECURRING, 'Dashboard', 'admin.php', null, 'icon-dashboard'),
			$itemSearch = new Admin_MenuItem('pi-search', PRIVILEGE_ALL, trans('menu_text.title_search'), '#', null, 'icon-search', null, null, null, trans('menu_text.desc_search')),
			$itemOrders = new Admin_MenuItem('pi-orders', PRIVILEGE_ALL + PRIVILEGE_ORDERS, trans('menu_text.title_orders'), 'admin.php?p=orders', null, 'icon-orders'),
			$itemCustomers = new Admin_MenuItem('pi-customers', PRIVILEGE_ALL + PRIVILEGE_USERS, trans('menu_text.title_customers'), 'admin.php?p=customers', null, 'icon-customers'),
			$itemProducts = new Admin_MenuItem('pi-products', PRIVILEGE_ALL + PRIVILEGE_PRODUCTS, trans('menu_text.title_products'), 'admin.php?p=products', null, 'icon-products', null, null, null, trans('menu_text.desc_products')),
			$itemReports = new Admin_MenuItem('pi-reports', PRIVILEGE_ALL + PRIVILEGE_STATS, trans('menu_text.title_reports'), 'admin.php?p=reports', null, 'icon-reports'),
			//$itemMarketing = new Admin_MenuItem('pi-marketing', PRIVILEGE_ALL + PRIVILEGE_MARKETING, 'Marketing', 'admin.php?p=marketing', null, 'icon-marketing'),
			$itemMarketing = new Admin_MenuItem('pi-marketing', PRIVILEGE_ALL + PRIVILEGE_MARKETING, trans('menu_text.title_marketing'), 'admin.php?p=promotions', null, 'icon-marketing', null, null, null, trans('menu_text.desc_marketing')),
			$itemContent = new Admin_MenuItem('pi-content', PRIVILEGE_ALL + PRIVILEGE_CONTENT, trans('menu_text.title_content'), 'admin.php?p=pages', null, 'icon-content', null, null, null, trans('menu_text.desc_pages')),
			$itemDesign = new Admin_MenuItem('pi-design', PRIVILEGE_ALL + PRIVILEGE_CONTENT, trans('menu_text.title_design'), 'admin.php?p=design_settings', null, 'icon-design', null, null, null, trans('menu_text.desc_design')),
			$itemApps = new Admin_MenuItem('pi-apps', PRIVILEGE_ALL + PRIVILEGE_CONTENT, trans('menu_text.title_apps'), 'admin.php?p=apps', null, 'icon-apps'),
			$itemSettings = new Admin_MenuItem('pi-settings', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, trans('menu_text.title_settings'), 'admin.php?p=settings', null, 'icon-settings', null, null, null, trans('menu_text.desc_settings')),
			$itemBilling = new Admin_MenuItem('pi-billing', PRIVILEGE_ALL, trans('menu_text.title_my_account'), 'admin.php?p=billing', null, 'icon-billing', null, null, null, trans('menu_text.desc_billing'))
		);

		$itemProducts->add(
			new Admin_MenuItem('si-products', PRIVILEGE_ALL + PRIVILEGE_PRODUCTS, trans('product.products'), 'admin.php?p=products', null, 'icon-products'),
			new Admin_MenuItem('si-categories', PRIVILEGE_ALL + PRIVILEGE_CATEGORIES, trans('categories.categories'), 'admin.php?p=categories', null, 'icon-categories'),
			new Admin_MenuItem('si-manufacturers', PRIVILEGE_ALL + PRIVILEGE_PRODUCTS, trans('manufacturers.manufacturers'), 'admin.php?p=manufacturers', null, 'icon-manufacturers'),
			_ACCESS_PRODUCTS_FILTERS && _CAN_ENABLE_PRODUCTS_FILTERS ? new Admin_MenuItem('si-products-features', PRIVILEGE_ALL + PRIVILEGE_PRODUCTS, trans('products_features.menu_title'), 'admin.php?p=features_groups', null, 'icon-products-groups') : null,
			new Admin_MenuItem('si-products-attributes', PRIVILEGE_ALL + PRIVILEGE_PRODUCTS, trans('product.attribute.global_product_attributes'), 'admin.php?p=products_attributes', null, 'icon-products-groups'),
			new Admin_MenuItem('si-order-forms', PRIVILEGE_ALL + PRIVILEGE_PRODUCTS, trans('order_forms.order_forms'), 'admin.php?p=order_forms', null, 'icon-products-groups')
		);

		$itemReports->add(
			new Admin_MenuItem('si-reports-home', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, trans('menu_text.reports_home'), 'admin.php?p=reports', null, 'icon-home'),
			new Admin_MenuItem('si-reports-orders', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Sales Performance', 'admin.php?p=report&report_type=orders', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-dates', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Sales Performance Summary', 'admin.php?p=report&report_type=dates', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-payment', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Sales by Payment Types', 'admin.php?p=report&report_type=payment', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-customers', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Sales by Customers', 'admin.php?p=report&report_type=customers', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-products', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Products Performance', 'admin.php?p=report&report_type=products', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-promo', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Promo Campaigns', 'admin.php?p=report&report_type=promo', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-qr_campaigns', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'QR Campaigns', 'admin.php?p=report&report_type=qr_campaigns', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-recurring_profiles', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Recurring Profiles', 'admin.php?p=report&report_type=recurring_profiles', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-recurring_orders', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Recurring Orders', 'admin.php?p=report&report_type=recurring_orders', null, 'icon-orders'),
			new Admin_MenuItem('si-reports-recurring_failures', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, 'Recurring Failures', 'admin.php?p=report&report_type=recurring_failures', null, 'icon-orders')
		);

		$itemMarketing->add(
			new Admin_MenuItem('si-promotions', PRIVILEGE_ALL + PRIVILEGE_BUSINESS, trans('menu_text.promotions'), 'admin.php?p=promotions', null, 'icon-promotions'),
			new Admin_MenuItem('si-recommended-products', PRIVILEGE_ALL + PRIVILEGE_BUSINESS, trans('menu_text.recommended_products'), 'admin.php?p=products_families', null, 'icon-recommended-products'),
			new Admin_MenuItem('si-google-tools', PRIVILEGE_ALL + PRIVILEGE_MARKETING, trans('menu_text.google_tools'), 'admin.php?p=google_marketing', null, 'icon-google-tools'),
			new Admin_MenuItem('si-drift-marketing', PRIVILEGE_ALL + PRIVILEGE_MARKETING, trans('menu_text.drift_marketing_campaigns'), 'admin.php?p=drift_marketing', null, 'icon-drift-marketing'),
			new Admin_MenuItem('si-gift-certificates', PRIVILEGE_ALL + PRIVILEGE_MARKETING, trans('menu_text.gift_certificates'), 'admin.php?p=gift_cert', null, 'icon-gift-certificates'),
			new Admin_MenuItem('si-qr-codes', PRIVILEGE_ALL + PRIVILEGE_MARKETING, trans('menu_text.qr_codes'), 'admin.php?p=settings_qr', null, 'icon-qr-codes'),
			new Admin_MenuItem('si-seo', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, trans('menu_text.search_engine_optimization'), 'admin.php?p=settings_search', null, 'icon-seo'),
			new Admin_MenuItem('si-social', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, trans('menu_text.social_sharing'), 'admin.php?p=settings_social', null, 'icon-social'),
			new Admin_MenuItem('si-bestsellers', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, trans('menu_text.bestsellers_settings'), 'admin.php?p=settings_bestsellers', null, 'icon-bestsellers')
		);

		$itemContent->add(
			new Admin_MenuItem('si-pages', PRIVILEGE_ALL + PRIVILEGE_CONTENT, trans('menu_text.pages'), 'admin.php?p=pages', null, 'icon-pages'),
			new Admin_MenuItem('si-testimonials', PRIVILEGE_ALL + PRIVILEGE_MARKETING, trans('menu_text.testimonials'), 'admin.php?p=testimonials', null, 'icon-testimonials'),
			new Admin_MenuItem('si-products-reviews', PRIVILEGE_ALL + PRIVILEGE_PRODUCTS, trans('menu_text.product_reviews'), 'admin.php?p=products_reviews', null, 'icon-products-reviews'),
			new Admin_MenuItem('si-custom-forms', PRIVILEGE_ALL + PRIVILEGE_CONTENT, trans('menu_text.custom_forms'), 'admin.php?p=custom_forms', null, 'icon-pages')
		);

		$itemSettings->add(
			new Admin_MenuItem('si-setup', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, trans('menu_text.setup_your_store'), 'admin.php?p=settings', null, 'icon-setup'),
			new Admin_MenuItem('si-settings-advanced', PRIVILEGE_ALL + PRIVILEGE_GLOBAL, trans('menu_text.advanced_settings'), 'admin.php?p=settings&action=advanced', null, 'icon-settings-advanced')
		);

		$itemDesign->add(
			new Admin_MenuItem('si-theme', PRIVILEGE_ALL + PRIVILEGE_CONTENT, trans('menu_text.change_theme'), 'admin.php?p=design_settings', null, 'icon-theme'),
			new Admin_MenuItem('si-catalog-settings', PRIVILEGE_ALL + PRIVILEGE_CONTENT, 'catalog_settings.catalog_settings', 'admin.php?p=design_catalog', null, 'icon-catalog-settings'),
			new Admin_MenuItem('si-thumbs-generator', PRIVILEGE_ALL + PRIVILEGE_CONTENT, 'thumbs_generator.thumbnail_generator', 'admin.php?p=thumbs_generator', null, 'icon-thumbs-generator')
		);

		if (Admin_HostedHelper::isHosted())
		{
			$itemBilling->add(
				new Admin_MenuItem('si-profile', PRIVILEGE_ALL + PRIVILEGE_BILLING, trans('menu_text.billing'), 'admin.php?p=billing', null, 'icon-billing'),
				new Admin_MenuItem('si-cart-info', PRIVILEGE_ALL, trans('menu_text.cart_information'), 'admin.php?p=billing&mode=cart-information', null, 'icon-billing'),
				new Admin_MenuItem('si-purchase', PRIVILEGE_ALL + PRIVILEGE_BILLING, trans('menu_text.purchase'), 'admin.php?p=billing&mode=purchase', null, 'icon-purchase'),
				new Admin_MenuItem('si-message-center', PRIVILEGE_ALL, trans('menu_text.message_center'), 'admin.php?p=billing&mode=message-center', null, 'icon-message-center'),
				new Admin_MenuItem('si-support', PRIVILEGE_ALL, trans('menu_text.support'), 'admin.php?p=billing&mode=support', null, 'icon-support')
			);
		}
		else
		{
			$itemBilling->add(
				new Admin_MenuItem('si-cart-info', PRIVILEGE_ALL, trans('menu_text.cart_information'), 'admin.php?p=billing&mode=cart-information', null, 'icon-billing'),
				new Admin_MenuItem('si-message-center', PRIVILEGE_ALL, trans('menu_text.message_center'), 'admin.php?p=billing&mode=message-center', null, 'icon-message-center')
			);
			global $label_support_area;
			if (trim($label_support_area) != '') $itemBilling->add(new Admin_MenuItem('si-support', PRIVILEGE_ALL, trans('menu_text.support'), $label_support_area, null, 'icon-support', null, null, null, null, 'target="_blank"'));
		}

		return $adminMenu;
	}
}
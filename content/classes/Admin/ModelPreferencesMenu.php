<?php

define('PREFERENCE_MENU_HISTORY', 'menu-history');

class Admin_ModelPreferencesMenu extends Admin_ModelPreferences
{
	/**
	 * Set menu history items
	 *
	 * @param $menuItems
	 * @param int $numberOfItems
	 */
	public function setHistoryItems($menuItems, $numberOfItems = 7)
	{
		if (!is_null($menuItems) && is_array($menuItems))
		{
			$history = array();
			$c = $numberOfItems;
			foreach ($menuItems as $menuItem)
			{
				$history[$menuItem->id] = $c;
				$c = $c - 1;
			}
		}
		else
		{
			$history = null;
		}
		
		$this->set(PREFERENCE_MENU_HISTORY, $history);
		$this->write();
	}
	
	/**
	 * Get history items form menu
	 */
	public function getHistoryItems()
	{
		return $this->get(PREFERENCE_MENU_HISTORY);
	}
}


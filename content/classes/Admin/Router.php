<?php

/**
 * Class Admin_Router
 */
class Admin_Router
{
	protected $routes;

	/**
	 * Admin_Router constructor.
	 */
	public function __construct()
	{
		$this->registerLegacyRoutes();
	}

	/**
	 * @param $p
	 * @param $controller
	 * @param $actions
	 * @return $this
	 */
	public function registerLegacyRoute($p, $controller, $actions)
	{
		$this->routes[$p] = array(
			'controller' => $controller,
			'actions' => $actions
		);

		return $this;
	}

	/**
	 * @param $p
	 * @param $action
	 * @return array|null
	 */
	public function getRoute($p, $action)
	{
		if (isset($this->routes[$p]))
		{
			$controllerClassName = $this->routes[$p]['controller'];

			if (isset($this->routes[$p]['actions'][$action]))
			{
				$controllerActionName = $this->routes[$p]['actions'][$action];
			}
			else
			{
				reset($this->routes[$p]['actions']);
				$controllerActionName = $this->routes[$p]['actions'][key($this->routes[$p]['actions'])];
			}

			return array(
				'controller' => $controllerClassName,
				'action' => $controllerActionName
			);
		}

		return null;
	}

	/**
	 * Register legacy routes
	 */
	public function registerLegacyRoutes()
	{
		$this
			->registerLegacyRoute('admins', 'Admin_Controller_AdminProfile', array('list' => 'listAction'))
			->registerLegacyRoute('admin', 'Admin_Controller_AdminProfile', array(
				'insert' => 'insertAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
				'securitysetup' => 'updateSecurityProfileAction',
			))
			->registerLegacyRoute('admins_log', 'Admin_Controller_AdminLog', array('list' => 'listAction'))
			->registerLegacyRoute('affiliate', 'Admin_Controller_iDevAffiliate', array('activate-form' => 'activateFormAction'))
			->registerLegacyRoute('api', 'Admin_Controller_ApiSettings', array('update' => 'updateAction'))
			->registerLegacyRoute('apps', 'Admin_Controller_Apps', array(
				'list' => 'listAction',
				'subscribe' => 'subscribeAction',
				'app-center' => 'appCenterAction',
				'activate' => 'activateAction',
				'deactivate' => 'deactivateAction',
				'activate-form' => 'activateFormAction',
			))
			->registerLegacyRoute('app', 'Admin_Controller_Apps', array('index' => 'indexAction'))
			->registerLegacyRoute('avalara', 'Admin_Controller_Avalara', array(
				'activate-form' => 'activateFormAction',
				'tax-codes' => 'taxCodesAction',
				'add-tax-code' => 'addTaxCodeAction',
				'add-avalara-tax-code' => 'addAvalaraTaxCodeAction',
				'update-tax-code' => 'updateTaxCodeAction',
				'delete' => 'deleteAction',
				'update-codes' => 'updateTaxCodesAction',
			))
			->registerLegacyRoute('avalara_tree', 'Admin_Controller_Avalara', array('tree' => 'taxCodesTreeAction'))
			->registerLegacyRoute('backup_restore', 'Admin_Controller_Backup', array(
				'index' => 'indexAction',
				'create-backup' => 'createBackupAction',
				'delete-backup' => 'deleteAction',
				'restore-backup' => 'restoreAction',
				'download-backup' => 'downloadAction',
			))
			->registerLegacyRoute('billing', 'Admin_Controller_Billing', array(
				'profile' => 'profileAction',
				'update-profile' => 'updateProfileAction',
				'update-card' => 'updateCardAction',
				'message-center' => 'messageCenterAction',
				'support' => 'supportAction',
				'purchase' => 'purchaseAction',
				'checkout' => 'checkoutAction',
				'checkout-update' => 'checkoutUpdateAction',
				'checkout-place-order' => 'checkoutPlaceOrderAction',
				'checkout-thank-you' => 'checkoutThankYouAction',
				'request-cancellation' => 'requestCancellationAction',
				'cart-information' => 'cartInformationAction',
				'show-phpinfo' => 'showPhpInfo',
			))
			->registerLegacyRoute('bongo', 'Admin_Controller_BongoCheckout', array(
				'index' => 'indexAction',
				'activate-form' => 'activateFormAction',
				'export' => 'exportAction',
				'sync-start' => 'syncStartAction',
				'sync-batch' => 'syncBatchAction',
			))
			->registerLegacyRoute('bulk_categories', 'Admin_Controller_CategoryImport', array(
				'start' => 'startAction',
				'assign' => 'assignAction',
				'import' => 'importAction',
			))
			->registerLegacyRoute('bulk_global_attributes_products', 'Admin_Controller_GlobalAttributesProductsImport', array(
				'start' => 'startAction',
				'assign' => 'assignAction',
				'import' => 'importAction',
			))
			->registerLegacyRoute('bulk_manufacturers', 'Admin_Controller_ManufacturerImport', array(
				'start' => 'startAction',
				'assign' => 'assignAction',
				'import' => 'importAction',
			))
			->registerLegacyRoute('bulk_products', 'Admin_Controller_ProductImport', array(
				'start' => 'startAction',
				'assign' => 'assignAction',
				'import' => 'importAction',
			))
			->registerLegacyRoute('categories', 'Admin_Controller_Category', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('category', 'Admin_Controller_Category', array(
				'add' => 'addAction',
				'export' => 'exportAction',
				'delete' => 'deleteAction',
				'delete-image' => 'deleteImageAction',
				'update' => 'updateAction',
				'assign-product-feature-group' => 'assignProductFeatureGroupAction',
				'delete-product-feature-group' => 'deleteProductFeatureGroupAction'
			))
			->registerLegacyRoute('checkout', 'Admin_Controller_CheckoutSettings', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('countries', 'Admin_Controller_CountriesStates', array(
				'list' => 'listCountriesAction',
				'add' => 'addCountryAction',
				'update' => 'updateCountryAction',
				'delete' => 'deleteCountriesAction',
				'activation' => 'countryActivationAction',
			))
			->registerLegacyRoute('currencies', 'Admin_Controller_Currency', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('currency', 'Admin_Controller_Currency', array(
				'add' => 'addAction',
				'delete' => 'deleteAction',
				'update' => 'updateAction',
				'update_custom' => 'updateAction',
				'set_default' => 'setDefaultCurrencyAction',
				'update_exchange_rates' => 'updateExchangeRatesAction',
			))
			->registerLegacyRoute('custom_form', 'Admin_Controller_CustomForm', array(
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
			))
			->registerLegacyRoute('custom_form_data', 'Admin_Controller_CustomFormData', array(
				'list' => 'listAction',
				'export' => 'exportAction',
				'delete' => 'deleteAction',
			))
			->registerLegacyRoute('custom_form_field', 'Admin_Controller_CustomField', array(
				'add' => 'addCustomFieldAction',
				'update' => 'updateCustomFieldAction',
				'delete' => 'deleteCustomFieldAction',
			))
			->registerLegacyRoute('custom_forms', 'Admin_Controller_CustomForm', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('customer', 'Admin_Controller_Customers', array(
				'update' => 'updateAction',
				'export_subscribers' => 'exportSubscribersAction',
				'export' => 'exportAction',
				'delete' => 'deleteAction',
				'lockaccount' => 'lockAction',
				'unlockaccount' => 'unlockAction',
			))
			->registerLegacyRoute('customers', 'Admin_Controller_Customers', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('design_catalog', 'Admin_Controller_CatalogSettings', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('doba', 'Admin_Controller_Doba', array(
				'activate-form' => 'activateFormAction',
				'settings' => 'settingsAction',
				'delete-doba-products' => 'deleteDobaProductsAction',
			))
			->registerLegacyRoute('doba_json', 'Admin_Controller_DobaJson', array(
				'savePriceSettings' => 'savePriceSettings',
				'getWatchlists' => 'getWatchlists',
				'getWatchlistProducts' => 'getWatchlistProducts',
				'getItemDetailFromCache' => 'getItemDetailFromCache',
				'updateProductsSet' => 'updateProductsSet',
				'updateImportSettings' => 'updateImportSettings',
				'requestPartnerPermission' => 'requestPartnerPermission',
				'hasPartnerPermission' => 'hasPartnerPermission',
				'removePartnerPermission' => 'removePartnerPermission',
				'sendOrder' => 'sendOrder',
				'getOrderDetail' => 'getOrderDetail',
				'fundOrder' => 'fundOrder',
			))
			->registerLegacyRoute('drift_marketing', 'Admin_Controller_DriftMarketingCampaign', array(
				'list' => 'listAction',
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
			))
			->registerLegacyRoute('email_notification', 'Admin_Controller_NotificationEmails', array(
				'update' => 'editAction',
				'header-footer-edit' => 'headerFooterEditAction',
			))
			->registerLegacyRoute('email_notifications', 'Admin_Controller_NotificationEmails', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('endicia', 'Admin_Controller_Endicia', array(
				'activate-form' => 'activateFormAction',
				'request-refund' => 'requestRefundAction',
				'add-funds' => 'addFundsAction',
				'change-password' => 'changePasswordAction',
				'add-funds-to-account' => 'addFundsToAccountAction',
				'validate-account-info' => 'validateAccountInfoAction',
				'view-label' => 'viewLabelAction'
			))
			->registerLegacyRoute('endicia_generatepostagelabel', 'Admin_Controller_EndiciaGeneratePostageLabel', array(
				'generate' => 'generateAction'
			))
			->registerLegacyRoute('exactor', 'Admin_Controller_Exactor', array(
				'activate-form' => 'activateFormAction',
				'tax-codes' => 'taxCodesAction',
				'add-tax-code' => 'addTaxCodeAction',
				'update-tax-code' => 'updateTaxCodeAction',
				'delete' => 'deleteAction',
				'update-euc' => 'updateEucAction',
			))
			->registerLegacyRoute('exactor_euc_ajax', 'Admin_Controller_Exactor', array(
				'tax-codes-euc' => 'taxCodesEUCAction',
			))
			->registerLegacyRoute('exactor_euc_tree', 'Admin_Controller_Exactor', array(
				'tax-codes-tree' => 'taxCodesTreeAction',
			))
			->registerLegacyRoute('facebook_login', 'Admin_Controller_FacebookLogin', array(
				'activate-form' => 'activateFormAction',
			))
			->registerLegacyRoute('filemanager', 'Admin_Controller_FileManager', array(
				'list' => 'listAction',
				'edit' => 'editAction',
				'preview' => 'previewAction',
				'delete' => 'deleteAction',
				'upload' => 'uploadAction',
				'check-exists' => 'checkExistsAction',
			))
			->registerLegacyRoute('forms_control', 'Admin_Controller_FormsFields', array(
				'customfieldslist' => 'customFieldsListAction',
				'customfieldsadd' => 'customFieldsAddAction',
				'customfieldsupdate' => 'customFieldsEditAction',
				'deletecustomfield' => 'customFieldsDeleteAction',
			))
			->registerLegacyRoute('gift_cert', 'Admin_Controller_GiftCertificate', array(
				'list' => 'listAction',
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
				'settings' => 'settingsAction',
			))
			->registerLegacyRoute('google_marketing', 'Admin_Controller_GoogleTools', array(
				'settings' => 'editSettingsAction',
				'generate' => 'generateSiteMapAction',
			))
			->registerLegacyRoute('home', 'Admin_Controller_Dashboard', array(
				'index' => 'indexAction',
			))
			->registerLegacyRoute('language', 'Admin_Controller_Language', array(
				'add' => 'addAction',
				'delete' => 'deleteAction',
				'update' => 'updateAction',
				'set_default' => 'setDefaultLanguageAction',
			))
			->registerLegacyRoute('languages', 'Admin_Controller_Language', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('mailchimp', 'Admin_Controller_Mailchimp', array(
				'activate-form' => 'activateFormAction',
			))
			->registerLegacyRoute('manufacturers', 'Admin_Controller_Manufacturer', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('manufacturer', 'Admin_Controller_Manufacturer', array(
				'add' => 'addAction',
				'delete' => 'deleteAction',
				'delete-image' => 'deleteImageAction',
				'update' => 'updateAction',
			))
			->registerLegacyRoute('qr_ajax', 'Admin_Controller_QRCode', array(
				'download_qr' => 'downloadAction',
				'generate_custom' => 'generateCustomAction',
				'generate_product_qr' => 'generateProductQRAction',
			))
			->registerLegacyRoute('marketing_qr_ajax', 'Admin_Controller_QRCode', array(
				'download_qr' => 'downloadAction',
				'generate_custom' => 'generateCustomAction',
				'generate_product_qr' => 'generateProductQRAction',
			))
			->registerLegacyRoute('norton_seal', 'Admin_Controller_NortonSeal', array(
				'index' => 'indexAction',
				'activate-form' => 'activateFormAction',
			))
			->registerLegacyRoute('order', 'Admin_Controller_Order', array(
				'update' => 'updateAction',
				'add-note' => 'addNoteAction',
				'update-status' => 'updateStatusAction',
				'process-payment' => 'processPaymentAction',
				'add-fulfillment' => 'addFulfillmentAction',
				'delete' => 'deleteAction',
				'export' => 'exportAction',
				'bulk-update'  => 'bulkUpdateAction',
				'bulk-fulfillment' => 'bulkFulfillmentAction',
				'stored-card' => 'storedCardAction',
				'remove-card' => 'removeCardAction',
				'edit-shipment' => 'editShipmentAction',
				'approve-fraud-review'=> 'approveFraudReviewAction',
				'deny-fraud-review'=> 'denyFraudReviewAction',
			))
			->registerLegacyRoute('order_form', 'Admin_Controller_OrderForm', array(
				'add' => 'addAction',
				'delete' => 'deleteAction',
				'update' => 'updateAction',
				'getProducts' => 'getProductsAction',
				'product-add' => 'addProductAction',
				'product-delete' => 'deleteProductAction',
			))
			->registerLegacyRoute('order_forms', 'Admin_Controller_OrderForm', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('order_invoice', 'Admin_Controller_OrderInvoice', array(
				'index' => 'indexAction',
				'bulk-invoices' => 'bulkIndexAction',
			))
			->registerLegacyRoute('order_notifications', 'Admin_Controller_OrderNotification', array(
				'list' => 'listAction',
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
				'advanced_settings' => 'editAdvancedSettingsAction',
			))
			->registerLegacyRoute('order_stamps', 'Admin_Controller_StampsOrder', array(
				'get-rates' => 'getRatesAction',
				'generate-label' => 'generateLabelAction',
				'cancel-label' => 'cancelLabelAction',
			))
			->registerLegacyRoute('orders', 'Admin_Controller_Order', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('page', 'Admin_Controller_TextPage', array(
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
				'publish' => 'publishAction',
			))
			->registerLegacyRoute('pages', 'Admin_Controller_TextPage', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('payment_cardinal', 'Admin_Controller_CardinalCentinel', array(
				'index' => 'indexAction',
				'activate-form' => 'activateFormAction',
			))
			->registerLegacyRoute('payment_method', 'Admin_Controller_PaymentMethod', array(
				'index' => 'indexAction',
				'activate' => 'activateAction',
				'activate-form' => 'activateFormAction',

				'update-custom' => 'updateCustomPaymentAction',
				'disable' => 'deactivateAction',
				'auto-board' => 'autoBoardAction',
				'auto-board-stripe' => 'autoBoardStripeAction',
			))
			->registerLegacyRoute('payment_methods', 'Admin_Controller_PaymentMethod', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('paypal_bml', 'Admin_Controller_PayPalBillMeLater', array(
				'publisher-form' => 'publisherFormAction',
			))
			->registerLegacyRoute('pikfly', 'Pikfly_Admin_Controller_Pikfly', array(
				'index' => 'indexAction',
				'activate-form' => 'activateFormAction',
				'add-area' => 'addAreaAction',
				'edit-area' => 'editAreaAction',
				'delete-area' => 'deleteAreaAction',
				'settings' => 'settingsAction',
			))
			->registerLegacyRoute('products', 'Admin_Controller_Product', array(
				'list' => 'listAction'
			))
			->registerLegacyRoute('product', 'Admin_Controller_Product', array(
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
				'update-image' => 'updateImageAction',
				'delete-image' => 'deleteImageAction',
				'export' => 'exportAction',
				'add-test-products' => 'addTestProductsAction',
				'copy' => 'copyAction',
				'get-product-fields' => 'getProductFieldsAction',
				'bulk-update' => 'bulkUpdateAction'
			))
			->registerLegacyRoute('product_page_attributes', 'Admin_Controller_ProductAttribute', array(
				'attribute-add' => 'addAttributeAction',
				'attribute-add-global' => 'addGlobalAttributeAction',
				'attribute-update' => 'updateAttributeAction',
				'attribute-delete' => 'deleteAttributeAction',
			))
			->registerLegacyRoute('product_page_variants', 'Admin_Controller_ProductVariant', array(
				'variant-add' => 'addVariantAction',
				'variant-update' => 'updateVariantAction',
				'variant-delete' => 'deleteVariantAction',
			))
			->registerLegacyRoute('product_page_promotions', 'Admin_Controller_ProductPromotion', array(
				'promotion-add' => 'addPromotionAction',
				'promotion-product-lookup' => 'addPromotionProductLookupAction',
				'promotion-update' => 'updatePromotionAction',
				'promotion-delete' => 'deletePromotionAction',
			))
			->registerLegacyRoute('product_page_qd', 'Admin_Controller_ProductQuantityDiscount', array(
				'qd-add' => 'addPromotionAction',
				'qd-update' => 'updatePromotionAction',
				'qd-delete' => 'deletePromotionAction',
			))
			->registerLegacyRoute('products_attribute', 'Admin_Controller_GlobalProductAttributes', array(
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
				'get-products' => 'getProductsAction',
			))
			->registerLegacyRoute('products_attributes', 'Admin_Controller_GlobalProductAttributes', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('products_families', 'Admin_Controller_ProductsFamily', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('products_family', 'Admin_Controller_ProductsFamily', array(
				'add' => 'addAction',
				'update' => 'updateAction',
				'assignProducts' => 'assignProductsAction',
				'getProducts' => 'getProductsAction',
				'getAssignedProducts' => 'getAssignedProductsAction',
				'unassignProducts' => 'unassignProductsAction',
				'delete' => 'deleteAction',
			))
			->registerLegacyRoute('products_review', 'Admin_Controller_ProductReview', array(
				'list' => 'listAction',
				'delete' => 'deleteAction',
				'update' => 'updateAction',
				'update_product_review_status' => 'updateStatusAction',
			))
			->registerLegacyRoute('products_reviews', 'Admin_Controller_ProductReview', array(
				'list' => 'listAction',
				'settings' => 'editAdvancedSettingsAction'
			))
			->registerLegacyRoute('profile', 'Admin_Controller_AdminProfile', array(
				'profile' => 'profileAction',
				'delete-image' => 'deleteImageAction',
				'generatePassword' => 'generatePasswordAction',
			))
			->registerLegacyRoute('profile_security', 'Admin_Controller_AdminProfile', array(
				'security' => 'updateSecurityProfileAction',
			))
			->registerLegacyRoute('promotions', 'Admin_Controller_PromoCode', array(
				'list' => 'listAction',
				'delete' => 'deleteAction',
				'update' => 'updateAction',
				'add' => 'addAction',
				'advanced_settings' => 'editAdvancedSettingsAction',
				'getAssignedProducts' => 'getAssignedProductsAction',
				'getProducts' => 'getProductsAction',
				'assignProducts' => 'assignProductsAction',
				'unassignProducts' => 'unassignProductsAction',
			))
			->registerLegacyRoute('quickbooks', 'Admin_Controller_Quickbooks', array(
				'refresh-license' => 'refreshLicenseAction',
				'update-status' => 'updateStatusAction'
			))
			->registerLegacyRoute('reports', 'Admin_Controller_Reports', array(
				'index' => 'indexAction',
				'summary' => 'summaryAction'
			))
			->registerLegacyRoute('report', 'Admin_Controller_Reports', array(
				'report' => 'reportAction'
			))
			->registerLegacyRoute('search', 'Admin_Controller_Search', array(
				'index' => 'searchAction',
			))
			->registerLegacyRoute('settings', 'Admin_Controller_Settings', array(
				'advanced' => 'advancedSettingsAction',
				'index' => 'indexAction',
			))
			->registerLegacyRoute('settings_bestsellers', 'Admin_Controller_BestsellersSettings', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('settings_ccs', 'Admin_Controller_CreditCardStorage', array(
				'settings' => 'editSettingsAction',
				'generate_certificate' => 'generateAction',
			))
			->registerLegacyRoute('settings_invoice', 'Admin_Controller_PrintableInvoice', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('settings_qr', 'Admin_Controller_QRCode', array(
				'list' => 'listAction',
				'delete' => 'deleteAction',
				'update' => 'updateAction',
				'add' => 'addAction',
				'advanced_settings' => 'editAdvancedSettingsAction',
				'generate' => 'generateAction',
			))
			->registerLegacyRoute('settings_search', 'Admin_Controller_SearchEngineOptimization', array(
				'settings' => 'editSettingsAction',
			))
			->registerLegacyRoute('settings_social', 'Admin_Controller_SocialSharingSettings', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('settings_wholesale', 'Admin_Controller_WholesaleSettings', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('shipping', 'Admin_Controller_Shipping', array(
				'list' => 'listAction',
				'view-method' => 'viewMethodAction',
				'edit-method' => 'editMethodAction',
				'new-method' => 'newMethodAction',
				'update-shipping-method' => 'updateShippingMethodAction',
				'delete' => 'deleteAction',
				'real-time-form' => 'realTimeFormAction',
				'new-real-time-methods' => 'newRealTimeMethodsAction',
				'settings' => 'settingsAction',
				'change-states' => 'changeStatesAction',
				'change-countries' => 'changeCountriesAction',
			))
			->registerLegacyRoute('shopzilla', 'Admin_Controller_Shopzilla', array(
				'index' => 'indexAction',
				'activate-form' => 'activateFormAction',
				'export' => 'exportAction',
			))
			->registerLegacyRoute('speed_test', 'Admin_Controller_SpeedTest', array(
				'speed-test' => 'showSpeedTest',
			))
			->registerLegacyRoute('stamps', 'Admin_Controller_Stamps', array(
				'activate-form' => 'activateFormAction',
				'purchase-postage' => 'purchasePostageAction',
				'scan-forms' => 'scanFormsAction',
				'carrier-pickup' => 'carrierPickupAction',
				'generate-scan-form' => 'generateScanFormAction'
			))
			->registerLegacyRoute('order_stampscom_ajax', 'Admin_Controller_StampsOrder', array(
				'label_view_iframe' => 'labelViewIframeAction',
				'label_track_iframe' => 'labelTrackIframeAction'
			))
			->registerLegacyRoute('states', 'Admin_Controller_CountriesStates', array(
				'list' => 'listStatesByCountryAction',
				'add' => 'addStateAction',
				'update' => 'updateStateAction',
				'delete' => 'deleteStatesAction',
			))
			->registerLegacyRoute('store_info', 'Admin_Controller_StoreAddress', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('store_settings', 'Admin_Controller_StoreSettings', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('taxes', 'Admin_Controller_Taxes', array(
				'list' => 'listAction',
				'settings' => 'editAdvancedSettingsAction',
			))
			->registerLegacyRoute('taxes_class', 'Admin_Controller_Taxes', array(
				'add' => 'addTaxClassAction',
				'update' => 'editTaxClassAction',
				'delete' => 'deleteTaxClassAction',
			))
			->registerLegacyRoute('taxes_classes', 'Admin_Controller_Taxes', array(
				'list' => 'listTaxesClassesAction',
			))
			->registerLegacyRoute('taxes_rate', 'Admin_Controller_Taxes', array(
				'add' => 'addAction',
				'update' => 'editAction',
				'delete' => 'deleteAction',
			))
			->registerLegacyRoute('testimonial', 'Admin_Controller_Testimonial', array(
				'add' => 'addAction',
				'update' => 'editAction',
				'delete' => 'deleteAction',
				'update_testimonial_status' => 'updateStatusAction'
			))
			->registerLegacyRoute('testimonials', 'Admin_Controller_Testimonial', array(
				'list' => 'listAction',
				'settings' => 'editAdvancedSettingsAction',
			))
			->registerLegacyRoute('thumbs_generator', 'Admin_Controller_ThumbnailGenerator', array(
				'index' => 'indexAction',
				'scan' => 'scanAction',
				'generate' => 'generateAction',
				'cleanup-images' => 'cleanupImagesAction',
				'sync-secondary-images' => 'syncSecondaryImagesAction',
			))
			->registerLegacyRoute('user_payment_profiles', 'PaymentProfiles_Admin_Controller_PaymentProfile', array(
				'ajax_add_form' => 'ajaxAddFormAction',
				'ajax_edit_form' => 'ajaxEditFormAction',
				'ajax_persist' => 'ajaxPersistAction',
				'ajax_set_primary' => 'ajaxSetPrimaryAction',
				'ajax_remove' => 'ajaxRemoveAction',
				'ajax_recurring_change_list' => 'ajaxRecurringChangeListAction',
				'ajax_recurring_change_persist' => 'ajaxRecurringChangePersistAction',
			))
			->registerLegacyRoute('vparcel', 'Admin_Controller_VIPparcel', array(
				'request-refund' => 'requestRefundAction',
				'request-generatelabel' => 'generatelabelAction',
				'request-generatepostagelabel' => 'generatePostageLabelAction',
				'request-viewlabel' => 'viewLabelAction'
			))
			->registerLegacyRoute('wholesale_central', 'Admin_Controller_WholesaleCentral', array(
				'activate-form' => 'activateFormAction',
				'sync' => 'syncAction',
				'export' => 'exportAction',
			))
			->registerLegacyRoute('widgets', 'Admin_Controller_Widgets', array(
				'index' => 'indexAction',
				'add' => 'addAction',
				'update' => 'updateAction',
				'delete' => 'deleteAction',
				'generate' => 'generate',
				'settings' => 'settingsAction',
			))
			->registerLegacyRoute('update', 'Admin_Controller_Updates', array(
				'check' => 'indexAction',
				'update' => 'step2Action'
			))
			->registerLegacyRoute('file_validate', 'Admin_Controller_AdminFileValidate', array(
				'checkFileSize' => 'checkFileSizeAction'
			))
			->registerLegacyRoute('security_error', 'Admin_Controller_AdminSecurityError', array(
				'index' => 'indexAction'
			))
			->registerLegacyRoute('paypal_payments_standard', 'Admin_Controller_PayPalPaymentsStandard', array(
				'generate-certificate' => 'generateCertificateAction',
				'download-certificate' => 'downloadCertificateAction'
			))
			->registerLegacyRoute('recurring_profiles', 'RecurringBilling_Admin_Controller_RecurringProfile', array(
				'search' => 'listAction',
				'details' => 'detailsAction',
				'bulk-status' => 'bulkStatusAction',
				'ajax_edit_shipping_address_form' => 'ajaxEditShippingAddressFormAction',
				'ajax_save_shipping_address_form' => 'ajaxSaveShippingAddressFormAction'
			))
			->registerLegacyRoute('fraud_methods', 'FraudService_Admin_Controller_FraudMethod', array(
				'list' => 'listAction',
			))
			->registerLegacyRoute('fraud_method', 'FraudService_Admin_Controller_FraudMethod', array(
				'index' => 'indexAction',
				'activate' => 'activateAction',
				'disable'=> 'deactivateAction',
				'activate-form' => 'activateFormAction',
			))
			->registerLegacyRoute('snippet', 'Admin_Controller_Snippet', array(
					'add' => 'addAction',
					'update' => 'updateAction',
					'delete' => 'deleteAction',
			))
			->registerLegacyRoute('snippets', 'Admin_Controller_Snippet', array(
					'list' => 'listAction',
			))
			->registerLegacyRoute('digital_download', 'Admin_Controller_DigitalDownload', array(
				'update' => 'updateAction',
			))
			->registerLegacyRoute('braintree', 'Admin_Controller_Braintree', array(
				'activate-form' => 'activateFormAction',
				'auth' => 'authAction',
				'auth-connect' => 'authConnectAction',
			))
			->registerLegacyRoute('quick_start_guide', 'Admin_Controller_QuickStartGuide', array(
				'index' => 'indexAction',
				'save-step-one' => 'saveStepOneAction',
				'save-step-two' => 'saveStepTwoAction',
				'save-step-three' => 'saveStepThreeAction',
				'save-taxes' => 'saveTaxesAction',
				'real-time-methods-form' => 'realTimeMethodsFormAction',
				'fetch-payment-method-form' => 'fetchPaymentMethodFormAction',
				'add-flat-rate-method' => 'addFlatRateMethodAction',
				'add-real-time-methods' => 'addRealTimeMethodsAction',
				'activate-payment' => 'activatePaymentAction',
				'deactivate-payment' => 'deactivatePaymentAction',
				'save-image-logo' => 'saveImageLogoAction'
			));

		if (_ACCESS_PRODUCTS_FILTERS && _CAN_ENABLE_PRODUCTS_FILTERS)
		{
			$this->
				registerLegacyRoute('products_features_app', 'Admin_Controller_ProductsFeaturesApp', array(
					'activate-form' => 'activateFormAction'
				))
				->registerLegacyRoute('product_feature', 'Admin_Controller_ProductFeature', array(
					'product-feature-update' => 'productFeatureUpdateAction',
					'product-feature-group-assign' => 'assignProductsFeaturesGroupsAction',
					'product-feature-group-delete' => 'deleteProductsFeaturesGroupsAction'
				))
				->registerLegacyRoute('products_features', 'Admin_Controller_ProductsFeatures', array(
					'list' => 'listAction'
				))
				->registerLegacyRoute('products_feature', 'Admin_Controller_ProductsFeatures', array(
					'add' => 'addAction',
					'update' => 'updateAction',
					'delete' => 'deleteAction'
				))
				->registerLegacyRoute('features_groups', 'Admin_Controller_ProductsFeaturesGroups', array(
					'list' => 'listAction',
				))
				->registerLegacyRoute('feature_groups', 'Admin_Controller_ProductsFeaturesGroups', array(
					'add' => 'addAction',
					'update' => 'updateAction',
					'delete' => 'deleteAction',
					'group-feature-assign' => 'assignGroupFeatureAction',
					'group-feature-delete' => 'deleteGroupFeatureAction',
					'group-feature-add' => 'addGroupFeatureAction'
				));
		}
	}
}

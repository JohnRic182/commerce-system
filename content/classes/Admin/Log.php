<?php
/**
 * Class Admin_Log
 */
class Admin_Log
{
	const LEVEL_NORMAL = 0;
	const LEVEL_WARNING = 1;
	const LEVEL_IMPORTANT = 2;
	const LEVEL_ERROR = 3;
	const LEVEL_CRITICAL = 4;

	/**
	 * Log admin action
	 *
	 * @param string $message
	 * @param int $level
	 * @param bool $force
	 * @param null | string $username
	 */
	public static function log($message, $level = self::LEVEL_NORMAL, $force = false, $username = null)
	{
		global $settings;
		$result = false;

		if ($level > self::LEVEL_WARNING || $force || $settings['SecurityAdminActivyLogging'] == 'YES')
		{
			global $db;

			if (is_null($username))
			{
				global $auth_admin_username;
				$username = $auth_admin_username;
			}

			$db->query('
				INSERT INTO '.DB_PREFIX.'admins_logs (log_date, ip, is_error, username, message)
				VALUES (NOW(), '.sprintf("%u", ip2long($_SERVER['REMOTE_ADDR'])).', '.intval($level).', "'.$db->escape(str_replace(chr(0), '', $username)).'", "'.$db->escape($message).'")'
			);
			$result = $db->insertId();

			// run security audit]
			if ($level > self::LEVEL_WARNING)
			{
				self::audit();
			}
		}
		return $result;
	}

	/**
	 * Do security audit
	 */
	public static function audit()
	{
		global $db, $settings;

		if (intval($settings['SecurityLastAuditEmailTimestamp']) + 3600 < time())
		{
			$auditResult = $db->query('SELECT * FROM '.DB_PREFIX.'admins_logs WHERE DATE_ADD(log_date, INTERVAL 1 HOUR) > NOW() AND is_error > '.self::LEVEL_WARNING);

			//TODO: should we have is int settings??
			if ($db->numRows($auditResult) > 3)
			{
				$auditMessage = 'Suspicious activity detected on "'.$settings['GlobalSiteName']."\" website \n\n";
				while (($logRecord = $db->moveNext($auditResult)) != false)
				{
					$auditMessage .=
						date('m/d/Y H:i:s', strtotime($logRecord['log_date']))."\n".
						'Username: '.htmlspecialchars($logRecord['username'])."\n".
						'IP: '.long2ip($logRecord['ip'])."\n".
						'Action: '.htmlspecialchars($logRecord['message'])."\n\n";
				}

				$db->query('
					SELECT *
					FROM '.DB_PREFIX.'admins
					WHERE
						active="Yes" AND
						(rights LIKE "%all%" OR rights LIKE "%admins%") AND
						(expires=0 OR (expires=1 AND expiration_date > NOW())) AND
						(receive_notifications LIKE "%security%")
				');

				while (($admin = $db->moveNext()) != false)
				{
					$subject = 'WARNING: Security audit from '.$settings['GlobalSiteName'];
					Notifications::sendMail($admin['email'], $subject, $auditMessage, $settings["GlobalNotificationEmail"]);
				}

				$settingsRepository = DataAccess_SettingsRepository::getInstance();
				$settingsRepository->save(array('SecurityLastAuditEmailTimestamp' => time()));
			}
		}
	}
}
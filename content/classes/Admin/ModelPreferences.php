<?php

class Admin_ModelPreferences extends Admin_Model
{
	protected $adminId = 0;
	protected static $preferences = null;
	protected static $changed = false;
	
	/**
	 * Class constructor
	 * @param DB $db
	 * @param int $adminId 
	 */
	public function __construct(DB $db, $adminId)
	{
		parent::__construct($db);
		
		$this->adminId = $adminId;
		
		$this->read();
		
		return $this;
	}

	/**
	 * Read preferences from database
	 *
	 * @return mixed|null
	 */
	public function read()
	{
		self::$preferences = null;
		$this->db()->query('SELECT preferences FROM '.DB_PREFIX.'admins WHERE aid="'.intval($this->adminId).'"');
		
		if (($col = $this->db()->moveNext()) != false)
		{
			$p = trim($col['preferences']);
			self::$preferences = $p == '' ? null : unserialize($p);
		}
	
		return self::$preferences;
	}

	/**
	 * Write preferences to a database
	 *
	 * @param bool $forceWrite
	 * @return bool
	 */
	public function write($forceWrite = false)
	{
		if (self::$changed || $forceWrite)
		{
			$this->db()->query('UPDATE '.DB_PREFIX.'admins SET preferences="'.$this->db()->escape(serialize(self::$preferences)).'" WHERE aid="'.intval($this->adminId).'"');
			self::$changed = false;
			return true;
		}
		
		return false;
	}
	
	/**
	 * Set preferences variable
	 * @param string $key
	 * @param mixed $value 
	 */
	public function set($key, $value)
	{
		self::$preferences[$key] = $value;
		self::$changed = true;
	}
	
	/**
	 * Get preferecens variable
	 * @param string $key
	 * @return mixed
	 */
	public function get($key)
	{
		return is_array(self::$preferences) && isset(self::$preferences[$key]) ? self::$preferences[$key] : null;
	}
}

<?php

class Admin_Tabs
{
	public $id;
	public $activeTab;
	public $tabs = array();

	public function __construct($id, $activeTab = 'main', array $tabs = array())
	{
		$this->id = $id;
		$this->activeTab = $activeTab;

		$this->addTabs($tabs);
	}

	/**
	 * Gets id
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}
	/**
	 * Sets id
	 *
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Gets activeTab
	 *
	 * @return string
	 */
	public function getActiveTab()
	{
		return $this->activeTab;
	}
	/**
	 * Sets activeTab
	 *
	 * @param string $activeTab
	 */
	public function setActiveTab($activeTab)
	{
		$this->activeTab = $activeTab;
	}

	public function addTab($id, $title)
	{
		$this->tabs[$id] = array('id' => $id, 'title' => $title);
	}

	public function addTabs(array $tabs)
	{
		foreach ($tabs as $tab)
		{
			if (isset($tab['id']))
			{
				$this->tabs[$tab['id']] = $tab;
			}
		}
	}

	public function getTabs()
	{
		return $this->tabs;
	}
}
<?php

/**
 * Class Admin_Form_TextPageForm
 */
class Admin_Form_TextPageForm
{
	/**
	 * @param $formData
	 * @param $mode
	 * @param null $id
	 * @param $params
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getBuilder($formData, $mode, $id = null, $params)
	{
		$customFormOptions = isset($params['customFormOptions']) ? $params['customFormOptions'] : array();

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'Page Details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('title', 'text', array('label' => 'text_page.title', 'value' => $formData['title'], 'placeholder' => 'text_page.title', 'required' => true, 'attr' => array('maxlength' => '255')));
		$basicGroup->add('content', 'textarea', array('label' => 'text_page.content', 'value' => htmlspecialchars($formData['content']), 'placeholder' => 'text_page.content', 'wysiwyg' => true));
		$basicGroup->add('is_active', 'checkbox', array('label' => 'text_page.publish_page_to_storefront', 'value' => 'Yes', 'current_value' => $formData['is_active'], 'wrapperClass' => 'clear-both'));
		$basicGroup->add('priority', 'text', array('label' => 'text_page.position', 'value' => $formData['priority'], 'placeholder' => 'text_page.position', 'required' => true, 'note' => 'text_page.position_tooltip'));
		$basicGroup->add('custom_form_id', 'choice', array('label' => 'text_page.custom_form', 'options' => $customFormOptions, 'empty_value' => array(0 => 'text_page.no_custom_form'), 'value' => $formData['custom_form_id']));

		/**
		 * SEO
		 */
		$seoGroup = new core_Form_FormBuilder('seo', array('label' => 'text_page.seo', 'collapsible' => true));

		$formBuilder->add($seoGroup);

		$seoGroup->add('url_custom', 'text', array('label' => 'text_page.url', 'value' => $formData['url_custom'], 'note' => 'text_page.url_tooltip', 'placeholder' => $formData['url_default'], 'attr' => array('class' => 'seo-preview')));
		$seoGroup->add('meta_title', 'text', array('attr' => array('class' => 'seo-preview' , 'maxlength' => 69), 'label' => 'text_page.meta_title', 'value' => $formData['meta_title']));
		$seoGroup->add('meta_description', 'textarea', array('attr' => array('class' => 'seo-preview', 'maxlength' => 156 ), 'label' => 'text_page.meta_description', 'value' => $formData['meta_description']));

		$translator = Framework_Translator::getInstance();

		//  Our search results preview
		$seoGroup->add('pages_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => trim($formData['url_custom']) == '' ? $formData['url_default'] : $formData['url_custom'],
					'isUrlHidden' => 'showing',
					'title' => trim($formData['meta_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['meta_title'],
					'description' => trim($formData['meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['meta_description'],
					'meta_title' => 'meta_title',
					'meta_description' => 'meta_description',
					'meta_url' => 'url_custom',
				)
			)
		);
		
		/**
		 * Menu Location
		 */
		$locationGroup = new core_Form_FormBuilder('menulocation', array('label' => 'text_page.menu_location', 'collapsible' => true));

		$formBuilder->add($locationGroup);
		
		$options = explode(',', $formData['options']);
		$top_value = in_array('top', $options) ? 1 : 0;
		$bottom_value = in_array('bottom', $options) ? 1 : 0;

		$locationGroup->add('options_top', 'checkbox', array('label' => 'text_page.top_menu', 'value' => '1', 'current_value' => $top_value, 'wrapperClass' => 'clear-both'));
		$locationGroup->add('options_bottom', 'checkbox', array('label' => 'text_page.bottom_menu', 'value' => '1', 'current_value' => $bottom_value, 'wrapperClass' => 'clear-both'));

		return $formBuilder;
	}

	/**
	 * Get text page form
	 *
	 * @param $formData
	 * @param $mode
	 * @param null $id
	 * @param $params
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null, $params)
	{
		return $this->getBuilder($formData, $mode, $id, $params)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getTextPageSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[search_str]', 'text', array('label' => 'Search in pages', 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-6'));

		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		return $formBuilder;
	}


	/**
	 * @param $data
	 * @return mixed
	 */
	public function getTextPageSearchForm($data)
	{
		return $this->getTextPageSearchFormBuilder($data)->getForm();

	}
}
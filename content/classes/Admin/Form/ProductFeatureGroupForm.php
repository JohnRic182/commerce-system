<?php

/**
 * Class Admin_Form_ProductFeatureGroupForm
 */
class Admin_Form_ProductFeatureGroupForm
{
	/**
	 * Get products features groups form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'features_groups.feature_group_details'));
		$basicGroup->add(
			'feature_group_name',
			'text',
			array(
				'label' => 'features_groups.feature_group_name',
				'value' => $formData['feature_group_name'],
				'placeholder' => 'features_groups.feature_group_name',
				'required' => true,
				'attr' => array(
					'maxlength' => '128',
					'class' => 'generate-id-source-field'
				)
			)
		);

		$basicGroup->add(
			'feature_group_id',
			'text',
			array(
				'label' => 'features_groups.feature_group_id',
				'value' => $formData['feature_group_id'],
				'placeholder' => 'features_groups.feature_group_id',
				'required' => true,
				'attr' => array('maxlength' => '128', 'class' => 'generated-id-field' . ($mode == 'add' ? ' autogenerate' : ''))
			)
		);

		$basicGroup->add(
			'feature_group_active',
			'checkbox',
			array(
				'label' => 'features_groups.feature_group_active',
				'value' => '1',
				'current_value' => $formData['feature_group_active'],
				'wrapperClass' => 'field-checkbox-space'
			)
		);

		$formBuilder->add($basicGroup);

		/**
		 * Features group
		 */
		if (intval($id) > 0)
		{
			$featureGroup = new core_Form_FormBuilder('group-features', array('label'=>'features_groups.features'));
			$featureGroup->add(
				'features',
				'template',
				array(
					'file' => 'templates/pages/product-feature-group/edit-features.html',
					'value' => json_encode(isset($formData['group_features']) ? $formData['group_features'] : array())
				)
			);
			$formBuilder->add($featureGroup);
		}

		return $formBuilder;
	}

	/**
	 * Get products features groups form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getProductFeatureGroupSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[search_str]', 'text', array('label' => 'Search in Products Features Groups', 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-6'));
		$formBuilder->add('searchParams[feature_group_active]', 'choice', array('label' => 'Feature Group Status', 'value' => $data['feature_group_active'], 'empty_value' => array('any' => 'All'), 'options' => array(
			'No' => 'Inactive',
			'Yes' => 'Active'
		), 'wrapperClass' => 'col-sm-12 col-md-3'));
		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getProductFeatureGroupSearchForm($data)
	{
		return $this->getProductFeatureGroupSearchFormBuilder($data)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_YahooExportForm
 */
class Admin_Form_YahooExportForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('export');

        $formBuilder->add('product_category', 'choice', array('label' => 'apps.yahoo.product_category', 'options' => $data['categories'], 'empty_value' => array('0' => trans('apps.yahoo.all_product_categories'))));

        $maxProductsOptions = array(
            '0' => trans('apps.yahoo.all_products'),
            '10' => '10',
            '50' => '50',
            '100' => '100',
            '150' => '150',
        );
        for ($i = 250; $i <= 5000; $i+=250)
        {
            $maxProductsOptions[$i] = $i;
        }

        $formBuilder->add('product_count', 'choice', array('label' => 'apps.yahoo.max_num', 'options' => $maxProductsOptions));

        $formBuilder->add('feed_type', 'choice', array('label' => 'apps.yahoo.feed_type', 'options' => array(
            'standard' => trans('apps.yahoo.standard'),
            'zip' => trans('apps.yahoo.product_images_zip'),
        )));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
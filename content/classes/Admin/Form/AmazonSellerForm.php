<?php

/**
 * Class Admin_Form_AmazonSellerForm
 */
class Admin_Form_AmazonSellerForm
{
    /**
     * @return core_Form_FormBuilder
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        $formBuilder->add('product_category', 'choice', array('label' => 'apps.amazon.product_category', 'options' => $data['categories'], 'empty_value' => array('0' => 'All Product Categories')));
        $formBuilder->add('product_count', 'choice', array('label' => 'apps.amazon.max_num', 'options' => $data['maxProductsOptions']));

        return $formBuilder;
    }

    /**
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
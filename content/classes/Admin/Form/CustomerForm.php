<?php

//TODO: Missing custom fields
//TODO: Avalara fields
//TODO: Exactor fields
//TODO: tax exempt field

/**
 * Class Admin_Form_CustomerForm
 */
class Admin_Form_CustomerForm
{
	/** @var core_Form_FormBuilder */
	private $billingAddressForm;
	/** @var DB */
	private $db;
	/** @var DataAccess_SettingsRepository */
	private $settings;

	public function __construct(core_Form_FormBuilder $BAForm, DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->billingAddressForm = $BAForm;
		$this->db = $db;
		$this->settings = $settings;
	}

	/**
	 * Get customer details form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$customFields = new CustomFields($this->db);

		$cf_billing = false;
		$cf_account = false;
		$cf_signUp = false;

		if ($id > 0)
		{
			$cf_billing = $customFields->getCustomFieldsValues("billing", $id, 0, 0);
			$cf_account = $customFields->getCustomFieldsValues("account", $id, 0, 0);
			$cf_signUp = $customFields->getCustomFieldsValues("signup", $id, 0, 0);
		}

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label' => 'customers.customer_form.edit_customer'));
		$formBuilder->add($basicGroup);

		$basicGroup->add('fname', 'text', array(
			'label' => 'customers.customer_form.first_name',
			'value' => $formData['fname'],
			'required' => true,
			'attr' => array('maxlength' => '24')
		));

		$basicGroup->add('lname', 'text', array(
			'label' => 'customers.customer_form.last_name',
			'value' => $formData['lname'],
			'required' => true,
			'attr' => array('maxlength' => '36')
		));

		$basicGroup->add('company', 'text', array(
			'label' => 'customers.customer_form.company',
			'value' => $formData['company'],
			'required' => $formData['FormsBillingCompany'] == 'Required',
			'attr' => array('maxlength' => '64')
		));

		$basicGroup->add('phone', 'text', array(
			'label' => 'customers.customer_form.phone',
			'value' => $formData['phone'],
			'required' => $formData['FormsBillingPhone'] == 'Required',
			'attr' => array('maxlength' => '36')
		));

		$basicGroup->add('email', 'text', array(
			'label' => 'customers.customer_form.email',
			'value' => $formData['email'],
			'required' => true,
			'attr' => array('maxlength' => '64')
		));

		$basicGroup->add('receives_marketing', 'checkbox', array(
			'label' => 'customers.customer_form.receives_marketing',
			'value' => 'Yes',
			'current_value' => $formData['receives_marketing']
		));

		if ($cf_signUp && !empty($cf_signUp))
		{
			foreach ($cf_signUp as $field)
			{
				$basicGroup->add($field['field_name'], 'static', array('label' => $field['field_name'], 'value' => $field['value_translated']));
			}
		}

		/**
		 * Account Information
		 */
		$accountInformationGroup = new core_Form_FormBuilder('account_information', array('label' => 'customers.customer_form.account_information', 'collapsible' => true));
		$formBuilder->add($accountInformationGroup);

		$accountInformationGroup->add('login', 'text', array(
			'label' => 'customers.customer_form.username',
			'value' => $formData['login'],
			'required' => true,
			'attr' => array('maxlength' => '24')
		));

		$accountInformationGroup->add('password', 'password', array(
			'label' => 'customers.customer_form.password',
			'value' => '',
			'required' => false,
			'wrapperClass' => 'clear-both',
			'attr' => array('maxlength' => '64'),
			'meter' => true
		));

		$accountInformationGroup->add('password2', 'password', array(
			'label' => 'customers.customer_form.confirm_password',
			'value' => '',
			'required' => false,
			'attr' => array('maxlength' => '64')
		));

		$accountInformationGroup->add('last_update', 'static', array(
			'label' => 'customers.customer_form.last_update',
			'value' => $formData['last_update']
		));

		$accountInformationGroup->add('account_status', 'choice', array(
			'label' => 'customers.customer_form.account_status',
			'value' => $formData['blocked'] == 'Yes' ? 'block' : 'active',
			'options' => array('block' => 'Block', 'active' => 'Active')//($formData['blocked'] == 'Yes' ? 'Blocked' : 'Active')
		));

		if ($this->settings->get('WholesaleDiscountType') !== 'NO')
		{
			$wholesaleLevelOptions = array(
				'0' => 'Standard User',
				'1' => 'Wholesaler Level 1'
			);
			$max = intval($this->settings->get('WholesaleMaxLevels'));
			for ($i = 2; $i <= $max; $i++)
			{
				$wholesaleLevelOptions[strval($i)] = 'Wholesaler Level ' . $i;
			}
			$accountInformationGroup->add('level', 'choice', array('label' => 'customers.customer_form.user_access', 'options' => $wholesaleLevelOptions, 'value' => $formData['level']));
		}

		if ($cf_account && !empty($cf_account))
		{
			foreach ($cf_account as $field)
			{
				$accountInformationGroup->add($field['field_name'], 'static', array('label' => $field['field_name'], 'value' => $field['value_translated']));
			}
		}

		/**
		 * Billing Address
		 */
		if ($this->billingAddressForm)
		{
			$formBuilder->add($this->billingAddressForm);

			if ($cf_billing && !empty($cf_billing))
			{
				foreach ($cf_billing as $field)
				{
					$accountInformationGroup->add($field['field_name'], 'static', array('label' => $field['field_name'], 'value' => $field['value_translated']));
				}
			}
		}

		if ($this->settings->get('TaxExemptionsActive') == 1)
		{
			$additionalInfoGroup = new core_Form_FormBuilder('additional-info', array('label' => 'customers.customer_form.tax_status', 'collapsible' => true));

			if ($this->settings->get('AvalaraEnableTax') == 'Yes')
			{
				$additionalInfoGroup->add('avalara_tax_exempt_number', 'text', array('label' => 'customers.customer_form.tax_avalara', 'value' => $formData['avalara_tax_exempt_number']));
			}
			else if ($this->settings->get('ExactorEnableTax') == 'Yes')
			{
				$additionalInfoGroup->add('exactor_tax_exemption_id', 'text', array('label' => 'customers.customer_form.tax_exactor', 'value' => $formData['exactor_tax_exemption_id']));
			}
			else
			{
				$additionalInfoGroup->add(
					'is_tax_exempt', 'checkbox',
					array('label' => 'customers.customer_form.is_tax_exempt', 'value' => 1, 'current_value' => $formData['is_tax_exempt'])
				);
			}

			$formBuilder->add($additionalInfoGroup);
		}

		// address book form
		$addressBookGroup = new core_Form_FormBuilder('addressbook-form', array('label' => 'customers.customer_form.address_book', 'collapsible' => true));
		$formBuilder->add($addressBookGroup);
		
		if (!empty($formData['shipping_addresses']))
		{
			foreach ($formData['shipping_addresses'] as $k=>$shipping_address)
			{
				$lbl = $k + 1;
				$address = new core_Form_FormBuilder('addressbook_'.$lbl, array('label' => 'Address '.$lbl));
				$addressBookGroup->add($address);
				
				$address->add("address_book[{$k}][usid]", 'hidden',
					array('value' => $shipping_address['usid'], 'required' => true)
				);
				$address->add("address_book[{$k}][address_type]", 'choice', array(
						'label' => 'address.address_type',
						'attr' => array('class' => 'medium'),
						'required' => true,
						'value' => $shipping_address['address_type'],
						'multiple' => false,
						'expanded' => false,
						'options' => array(
							Model_Address::ADDRESS_TYPE_RESIDENTIAL => 'address.address_type_residential',
							Model_Address::ADDRESS_TYPE_BUSINESS => 'address.address_type_business'
						)
					)
				);
				$address->add("address_book[{$k}][name]", 'text',
					array('label'=>'address.name', 'value' => $shipping_address['name'], 'required' => true)
				);
				$address->add("address_book[{$k}][company]",'text',
					array(
						'label'=>'address.company',
						'value' => $shipping_address['company'],
						'required' => $this->settings->get('FormsShippingCompany') == 'Required'
					)
				);
		
				$address->add("address_book[{$k}][address1]",'text',
					array('label'=>'address.address1', 'value' => $shipping_address['address1'], 'required' => true)
				);
		
				$address->add("address_book[{$k}][address2]",'text',
					array(
						'label'=>'address.address2',
						'value' => $shipping_address['address2'],
						'required' => $this->settings->get('FormsShippingAddressLine2') == 'Required'
					)
				);
		
				$address->add("address_book[{$k}][city]",'text',
					array('label'=>'address.city', 'value' => $shipping_address['city'], 'required' => true)
				);
				$address->add(
					"address_book[{$k}][country]",'choice',
					array(
						'label' => 'address.country',
						'value' => $shipping_address['country'],
						'required' => true,
						'options' => array(),
						'attr' => array('data-value' =>$shipping_address['country'], 'class' => 'addressbook-input-address-country')
					)
				);
		
				$address->add(
					"address_book[{$k}][state]",'choice',
					array(
						'label' => 'address.state',
						'value' => $shipping_address['state'],
						'required' => true,
						'multiple' => false,
						'expanded' => false,
						'attr' => array('data-value' => $shipping_address['state'], 'class' => 'addressbook-input-address-state')
					)
				);
		
				$address->add(
					"address_book[{$k}][province]",'text',
					array(
						'label' => 'address.province',
						'value' => $shipping_address['province'],
						'required' => true,
						'multiple' => false,
						'expanded' => false,
						'attr' => array('data-value' => $shipping_address['province'], 'class' => 'addressbook-input-address-province')
					)
				);
		
				$address->add("address_book[{$k}][zip]", 'text',
					array('label'=>'address.zip', 'value' => $shipping_address['zip'], 'required' => true, 'attr' => array('class' => 'short'))
				);
			}
		}

		$paymentProfilesGroup = new core_Form_FormBuilder('payment-profiles', array('label' => 'customers.customer_form.payment_profiles', 'collapsible' => true));
		$formBuilder->add($paymentProfilesGroup);

		$paymentProfilesGroup->add('payment-profiles', 'template', array('file' => 'templates/pages/customers/payment-profiles.html'));


		return $formBuilder;
	}

	/**
	 * Get text page form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}


}

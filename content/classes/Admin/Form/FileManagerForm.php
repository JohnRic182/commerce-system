<?php

/**
 * Class Admin_Form_FileManagerForm
 */
class Admin_Form_FileManagerForm
{
    /**
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder()
    {
        $formBuilder = new core_Form_FormBuilder('upload');
        $formBuilder->add('new_file', 'file', array('label' => 'Choose file'));

        return $formBuilder;
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->getBuilder()->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getFileManageEditBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('file');
        $formBuilder->add('content', 'textarea', array('label' => false, 'value' => $data['content']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getFileManagerEditForm($data)
    {
        return $this->getFileManageEditBuilder($data)->getForm();
    }
}
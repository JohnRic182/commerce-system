<?php
/**
 * Class Admin_Form_TestimonialAdvancedSettingsForm
 */
class Admin_Form_TestimonialAdvancedSettingsForm
{
	/**
	 * Get advanced settings form for testimonials - builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$basicGroup->add('testimonialsSettings[testimonials_Enabled]', 'checkbox', array(
				'label' => 'testimonials.testimonials_enabled',
				'value' => 'Yes',
				'current_value' => $formData['testimonials_Enabled'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['testimonials_Enabled'])
		));

		$basicGroup->add('testimonialsSettings[testimonials_Use_Rating]', 'checkbox', array(
			'label' => 'testimonials.testimonials_use_rating',
			'value' => 'Yes',
			'current_value' => $formData['testimonials_Use_Rating'],
			'attr' => array('data-value' => $formData['testimonials_Use_Rating'])
		));

		$basicGroup->add('testimonialsSettings[testimonials_Per_Page]', 'text', array(
			'label' => 'testimonials.testimonials_per_page',
			'value' => $formData['testimonials_Per_Page'],
			'required' => true,
			'placeholder' => 'testimonials.testimonials_per_page',
			'attr' => array('data-value' => $formData['testimonials_Per_Page'])
		));

		$basicGroup->add('testimonialsSettings[testimonials_Email_Notify]', 'text', array(
				'label' => 'testimonials.testimonials_email_notify',
				'value' => $formData['testimonials_Email_Notify'],
				'label' => 'testimonials.testimonials_email_notify',
				'placeholder' => 'testimonials.testimonials_email_notify',
				'attr' => array('data-value' => $formData['testimonials_Email_Notify'])
		));

		$basicGroup->add('testimonialsSettings[testimonials_Use_Captcha]', 'checkbox', array(
				'label' => 'testimonials.testimonials_use_captcha',
				'value' => 'Yes',
				'current_value' => $formData['testimonials_Use_Captcha'],
				'attr' => array('data-value' => $formData['testimonials_Use_Captcha'])
		));

		return $formBuilder;
	}

	/**
	 * Get advanced testimonials settings form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_ProductPromotionForm
 */
class Admin_Form_ProductPromotionForm
{
	/**
	 * @param null $productId
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($productId)
	{
		$formBuilder = new core_Form_FormBuilder('product-promotion');

		$formBuilder->add('promotion[id]', 'hidden', array('value' => 'new'));

		$formBuilder->add('promotion[pid]', 'hidden', array('value' => $productId));

		$formBuilder->add(
			'promotion[gift_quantity]', 'integer',
			array(
				'label' => 'product.promotions.free_promo_product_quantity', 'value' => '', 'required' => true, 'placeholder' => 'product.promotions.free_promo_product_quantity',
				'tooltip' => 'Enter the quantity of this product a customer must order to receive the item(s) below for free'
			)
		);

		$formBuilder->add(
			'promotion[gift_max_quantity]', 'integer',
			array(
				'label' => 'Free product quantity limit', 'value' => '', 'required' => true, 'placeholder' => 'product.promotions.free_product_quantity_limit',
				'tooltip' => 'Enter the Maximum number of free product allowed per order for this product.'
			)
		);

		$formBuilder->add(
			'promotion[gift_shipping_free]', 'choice',
			array(
				'label' => 'Charge shipping for a free product?', 'wrapperClass' => 'col-xs-12 col-md-12',
				'options' => array(new core_Form_Option('Yes', 'No'), new core_Form_Option('No', 'Yes')),
			)
		);

		return $formBuilder;
	}

	/**
	 * @param null $productId
	 * @return core_Form_Form
	 */
	public function getForm($productId)
	{
		return $this->getFormBuilder($productId)->getForm();
	}
}
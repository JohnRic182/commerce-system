<?php

/**
 * Class Admin_Form_CustomerSearchForm
 */
class Admin_Form_CustomerSearchForm
{
	/** @var core_Form_FormBuilder */
	private $billingAddressForm;
	/** @var DB */
	private $db;
	/** @var DataAccess_SettingsRepository */
	private $settings;

	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->db = $db;
		$this->settings = $settings;
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getCustomerSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');

		$formBuilder->add('searchParams[search_str]', 'text', array('label' => 'customers.search_in', 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-4'));
		$formBuilder->add('searchParams[from_date]', 'text', array('label' => 'customers.search_from', 'value' => $data['from_date'], 'wrapperClass' => 'col-sm-12 col-md-2'));
		$formBuilder->add('searchParams[to_date]', 'text', array('label' => 'customers.search_to', 'value' => $data['to_date'], 'wrapperClass' => 'col-sm-12 col-md-2'));
		$formBuilder->add('searchParams[ExpressCheckoutUser]', 'choice', array('label'=> 'customers.search_show_express', 'wrapperClass' => 'col-sm-12 col-md-2', 'value' => $data['ExpressCheckoutUser'], 'options' => array('0' => 'No', 1 => 'Yes')));

		if ($this->settings->get('WholesaleDiscountType') !== 'NO')
		{
			$wholesaleLevelOptions = array('0' => 'Standard User', '1,2,3' => 'All Wholesalers', '1' => 'Wholesaler Level 1');
			$max = intval($this->settings->get('WholesaleMaxLevels'));
			for ($i = 2; $i <= $max; $i++)
			{
				$wholesaleLevelOptions[strval($i)] = 'Wholesaler Level ' . $i;
			}
			$formBuilder->add('searchParams[wholesale_users]', 'choice', array('label' => 'Wholesaler Customers', 'options' => $wholesaleLevelOptions, 'value' => $data['wholesale_users'], 'wrapperClass' => 'col-sm-12 col-md-2'));
		}

		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getCustomerSearchForm($data)
	{
		return $this->getCustomerSearchFormBuilder($data)->getForm();
	}
}
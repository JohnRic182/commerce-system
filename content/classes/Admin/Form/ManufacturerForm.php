<?php
/**
 * Class Admin_Form_ManufacturerForm
 */
class Admin_Form_ManufacturerForm
{
	/**
	 * @param $formData
	 * @param $mode
	 * @param null $id
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder(
			'basic',
			array('label' => 'manufacturers.manufacturer_details')
		);

		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'manufacturer_name',
			'text',
			array(
				'label' => 'manufacturers.title',
				'value' => $formData['manufacturer_name'],
				'placeholder' => 'manufacturers.title',
				'required' => true,
				'attr' => array(
					'maxlength' => '255',
					'class' => 'generate-id-source-field'
				)
			)
		);

		$attr = array(
			'maxlength' => '255',
			'class' => 'generated-id-field'
		);
		if ($mode == 'add') $attr['class'] =  'generated-id-field autogenerate';

		$basicGroup->add(
			'manufacturer_code',
			'text',
			array(
				'label' => 'manufacturers.manufacturer_id',
				'value' => $formData['manufacturer_code'],
				'placeholder' => 'manufacturers.manufacturer_id',
				'required' => true,
				'attr' => $attr,
				'note' => 'manufacturers.manufacturer_id_tooltip'
			)
		);

		$basicGroup->add(
			'is_visible',
			'checkbox',
			array(
				'label' => 'manufacturers.shown_on_storefront',
				'value' => '1',
				'current_value' => $formData['is_visible']
			)
		);

		$basicGroup->add(
			'image',
			'image',
			array(
				'label' => 'manufacturers.upload_manufacturer_image',
				'value' => (isset($formData['thumb']) && !empty($formData['thumb']) ? $formData['thumb'] : $formData['image']),
				'imageDeleteUrl' => $mode == 'update' && !is_null($id) ? 'admin.php?p=manufacturer&mode=delete-image&id=' . $id : null,
				'wrapperClass' => 'clear-both',
				'attr' => array('accept' => 'image/jpeg, image/png, image/gif')
			)
		);

		/**
		 * SEO
		 */
		$seoGroup = new core_Form_FormBuilder(
			'seo',
			array(
				'label' => 'manufacturers.seo',
				'collapsible' => true
			)
		);

		$formBuilder->add($seoGroup);

		$seoGroup->add(
			'meta_title',
			'text',
			array(
				'attr' => array(
					'class' => 'seo-preview',
					'maxlength' => 69
				),
				'label' => 'manufacturers.meta_title',
				'value' => $formData['meta_title']
			)
		);

		$seoGroup->add(
			'url_custom',
			'text',
			array(
				'label' => 'manufacturers.url',
				'value' => $formData['url_custom'],
				'placeholder' => $formData['url_default'],
				'attr' => array('class' => 'seo-preview'),
				'note' => 'manufacturers.url_tooltip'
			)
		);

		$seoGroup->add(
			'meta_description',
			'textarea',
			array(
				'attr' => array(
					'class' => 'seo-preview',
					'maxlength' => 156
				),
				'label' => 'manufacturers.meta_description',
				'value' => $formData['meta_description']
			)
		);

		//  Our search results preview
		$seoGroup->add(
			'manufacturer_seo_preview',
			'seopreview',
			array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => trim($formData['url_custom']) == '' ? $formData['url_default'] : $formData['url_custom'],
					'isUrlHidden' => 'showing',
					'title' => trim($formData['meta_title']) == '' ? trans('common.add_meta_title') : $formData['meta_title'],
					'description' => trim($formData['meta_description']) == '' ? trans('common.add_meta_description') : $formData['meta_description'],
					'meta_title' => 'meta_title',
					'meta_description' => 'meta_description',
					'meta_url' => 'url_custom',
				)
			)
		);

		return $formBuilder;
	}

	/**
	 * Get manufacturer form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getManufacturerSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add(
			'searchParams[search_str]',
			'text',
			array(
				'label' => 'Search in Manufacturer Name &amp; Code',
				'value' => $data['search_str'],
				'wrapperClass' => 'col-sm-12 col-md-6'
			)
		);

		$formBuilder->add(
			'searchParams[is_visible]',
			'choice',
			array(
				'label' => 'Visibility',
				'value' => $data['visibility'],
				'options' => array(
					'any' => 'All',
					'Yes' => 'Visible',
					'No' => 'Invisible',
				),
				'wrapperClass' => 'col-sm-12 col-md-3'
			)
		);

		$formBuilder->add(
			'orderBy',
			'hidden',
			array('value' => $data['orderBy']
			)
		);

		$formBuilder->add(
			'orderDir',
			'hidden',
			array('value' => $data['orderDir'])
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getManufacturerSearchForm($data)
	{
		return $this->getManufacturerSearchFormBuilder($data)->getForm();
	}
}

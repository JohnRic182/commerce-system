<?php
/**
 * Class Admin_Form_GiftCertificateForm
 */
class Admin_Form_GiftCertificateForm
{
	/**
	 * Get gift certificate form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'marketing.gift_certificate_details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('first_name', 'text', array('label' => 'marketing.first_name', 'value' => $formData['first_name'], 'placeholder' => 'marketing.first_name', 'required' => true, 'attr' => array('maxlength' => '255')));
		$basicGroup->add('last_name', 'text', array('label' => 'marketing.last_name', 'value' => $formData['last_name'], 'placeholder' => 'marketing.last_name', 'required' => true, 'attr' => array('maxlength' => '255')));
		$basicGroup->add('email', 'text', array('label' => 'marketing.email', 'value' => $formData['email'], 'placeholder' => 'marketing.email', 'required' => true, 'attr' => array('maxlength' => '255')));

		if ($mode == Admin_Controller_GiftCertificate::MODE_UPDATE)
		{
			$basicGroup->add('balance', 'money', array('label' => 'marketing.balance', 'value' => $formData['balance'], 'placeholder' => 'marketing.balance', 'required' => true, 'attr' => array('maxlength' => '20')));
		}
		else
		{
			$basicGroup->add('gift_amount', 'money', array('label' => 'marketing.amount', 'value' => $formData['gift_amount'], 'placeholder' => 'marketing.amount', 'required' => true, 'attr' => array('maxlength' => '20')));
		}

		$basicGroup->add('voucher', 'text', array('label' => 'marketing.voucher_number', 'value' => $formData['voucher'], 'placeholder' => 'marketing.voucher_number', 'required' => true, 'attr' => array('maxlength' => '20')));

		if ($mode == Admin_Controller_GiftCertificate::MODE_ADD)
		{
			$basicGroup->add('send_email', 'checkbox', array('label' => 'marketing.notify_customer', 'value' => 'Yes'));
		}

		$basicGroup->add('message', 'textarea', array('label' => 'marketing.message', 'value' => $formData['message']));

		return $formBuilder;
	}

	/**
	 * Get manufacturer form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}

	/**
 * @param $data
 * @return core_Form_FormBuilder
 * @throws Exception
 */
	public function getGiftCertificateSearchBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[balance]', 'choice', array('label' => 'Filter', 'value' => $data['balance'], 'options' => array(
			'all' => 'All',
			'zero' => 'Zero Balance',
			'credit' => 'Outstanding Balance',
		)));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getGiftCertificateSearchForm($data)
	{
		return $this->getGiftCertificateSearchBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getGiftCertificateSettingsBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic');
		$formBuilder->add($basicGroup);

		$basicGroup->add('enable_gift_cert', 'checkbox', array(
			'label' => 'Enable Gift Certificates',
			'value' => 'Yes',
			'current_value' => $data['enable_gift_cert']
		));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getGiftCertificateSettingsForm($data)
	{
		return $this->getGiftCertificateSettingsBuilder($data)->getForm();
	}
}
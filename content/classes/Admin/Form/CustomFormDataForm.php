<?php

/**
 * Class Admin_Form_CustomFormDataForm
 */
class Admin_Form_CustomFormDataForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('search');

        foreach ($data['customFormFields'] as $customFormField) $searchFieldsOptions[$customFormField['field_name']] = $customFormField['field_name'];

	    $formBuilder->add('searchParams[field_name]', 'choice', array('label' => 'custom_form.search_in', 'value' => $data['searchParams']['field_name'], 'wrapperClass' => 'col-sm-6 col-xs-12', 'options' => $searchFieldsOptions));
	    $formBuilder->add('searchParams[search_str]', 'text', array('label' => 'custom_form.search_str', 'value' => $data['searchParams']['search_str'], 'wrapperClass' => 'col-sm-6 col-xs-12'));
	    $formBuilder->add('orderDir', 'hidden', array('value' => $data['searchParams']['orderDir']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
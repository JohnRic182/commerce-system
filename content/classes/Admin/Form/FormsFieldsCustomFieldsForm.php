<?php
/**
 * Class Admin_Form_FormsFieldsCustomFieldsForm
 */
class Admin_Form_FormsFieldsCustomFieldsForm
{
    /**
     * Get text pages form builder
     *
     * @param $formData
     * @param $mode
     * @param $id
     *
     * @return core_Form_FormBuilder
     */
    public function getBuilder($formData, $mode, $id = null)
    {
        global $settings;

        $translator = Framework_Translator::getInstance();

        $formBuilder = new core_Form_FormBuilder('general');

        /**
         * Basic group
         */

        $basicGroup = new core_Form_FormBuilder('basic', array('label'=>'custom_fields.custom_field_details'));

        $formBuilder->add($basicGroup);

        $fieldSections = array();
        foreach($formData['customFieldSectionOptions'] as $k => $v)
        {
            $fieldSections[] = new core_Form_Option($k, $v);
        }
        $basicGroup->add('field_place', 'choice', array('label' => 'custom_fields.section', 'options' =>  $fieldSections, 'required' => true, 'value' => $formData['field_place']));

        $basicGroup->add('field_name', 'text', array('label' => 'custom_fields.field_name', 'placeholder' => 'custom_fields.field_name', 'value' => $formData['field_name'], 'required' => true, 'attr' => array('maxlength' => '250')));
        $basicGroup->add('field_caption', 'text', array('label' => 'custom_fields.field_caption', 'placeholder' => 'custom_fields.field_caption', 'value' => $formData['field_caption'], 'required' => true, 'attr' => array('maxlength' => '250')));

        $fieldTypes = array();
        foreach($formData['customFieldTypeOptions'] as $k => $v)
        {
            $fieldTypes[] = new core_Form_Option($k, $v);
        }
        $basicGroup->add('field_type', 'choice', array('label' => 'custom_fields.field_type', 'options' =>  $fieldTypes, 'required' => true, 'value' => $formData['field_type']));

        $basicGroup->add('is_active', 'checkbox', array('label' => 'custom_fields.active_interrogative', 'value' => '1', 'current_value' => $formData['is_active'], 'wrapperClass' => 'clear-both'));

        $basicGroup->add('is_required', 'checkbox', array('label' => 'custom_fields.required_interrogative', 'value' => '1', 'current_value' => $formData['is_required']));

        $fieldPosition = array();
        foreach($formData['customFieldPositionOptions'] as $k => $v)
        {
            $fieldPosition[] = new core_Form_Option($k, $v);
        }
        $basicGroup->add('priority', 'choice', array('label' => 'custom_fields.position', 'options' =>  $fieldPosition, 'value' => $formData['priority']));

        $basicGroup->add('text_length', 'text', array('label' => 'custom_fields.character_limit', 'placeholder' => 'custom_fields.character_limit', 'value' => $formData['text_length'], 'required' => true));

        $basicGroup->add('options', 'textarea', array('required' => true, 'label' => 'custom_fields.options', 'value' => $formData['options']));

        $basicGroup->add('value_checked', 'text', array('label' => 'custom_fields.checked_value', 'placeholder' => 'custom_fields.checked_value', 'value' => $formData['value_checked'], 'required' => true, 'wrapperClass' => 'clear-both'));
        $basicGroup->add('value_unchecked', 'text', array('label' => 'custom_fields.unchecked_value','placeholder' => 'custom_fields.unchecked_value' , 'value' => $formData['value_unchecked'], 'required' => true));

        return $formBuilder;
    }

    /**
     * Get text page form
     *
     * @param $formData
     * @param $mode
     * @param int|null $id
     *
     * @return core_Form_Form
     */
    public function getForm($formData, $mode, $id = null)
    {
        return $this->getBuilder($formData, $mode, $id)->getForm();
    }
}
<?php

/**
 * Class Admin_Form_WidgetsForm
 */
class Admin_Form_WidgetsForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getWidgetsSettingsBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $basicGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($basicGroup);

        $basicGroup->add('widgets_type', 'choice', array('label' => 'widgets.widgets_type', 'value' => $data->get('widgets_type'), 'options' => array(
            'page' => 'widgets.widgets_type_options.page',
            'cart' => 'widgets.widgets_type_options.cart',
            'checkout' => 'widgets.widgets_type_options.checkout',
        )));

        $basicGroup->add('widgets_image_type', 'choice', array('label' => 'widgets.widgets_image_type', 'value' => $data->get('widgets_image_type'), 'options' => array(
            'product' => 'widgets.widgets_image_type_options.product',
            'thumb' => 'widgets.widgets_image_type_options.thumb',
            'none' => 'widgets.widgets_image_type_options.none',
        )));

        $basicGroup->add('widgets_style', 'choice', array('label' => 'widgets.widgets_style', 'value' => $data->get('widgets_style'), 'options' => array(
            '1' => 'widgets.widgets_style_options.1',
            '2' => 'widgets.widgets_style_options.2',
            '3' => 'widgets.widgets_style_options.3',
        )));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getWidgetsSettingsForm($data)
    {
        return $this->getWidgetsSettingsBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getWidgetsCampaignBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        /**
         * Basic group
         */
        $basicGroup = new core_Form_FormBuilder('basic', array('label' => 'widgets.campaign_details'));

        $formBuilder->add($basicGroup);

        $basicGroup->add('name', 'text', array('label' => 'widgets.name', 'placeholder' => 'widgets.name', 'value' => $data['name'], 'required' => true, 'attr' => array('maxlength' => '100')));

        $basicGroup->add('description', 'text', array('label' => 'widgets.description', 'placeholder' => 'widgets.description', 'value' => $data['description'],  'required' => true, 'attr' => array('maxlength' => '250')));
        
        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getWidgetsCampaignForm($data)
    {
        return $this->getWidgetsCampaignBuilder($data)->getForm();
    }
}
<?php

/**
 * Class Admin_Form_MailchimpForm
 */
class Admin_Form_MailchimpForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $formBuilder->add('mailchimp_username', 'text', array('label' => 'mailchimp.username', 'placeholder' => 'mailchimp.username', 'required' => true, 'value' => $data->get('mailchimp_username')));
        $formBuilder->add('mailchimp_password', 'password', array('label' => 'mailchimp.password', 'placeholder' => 'mailchimp.password', 'required' => true, 'value' => $data->get('mailchimp_password')));
        $formBuilder->add('mailchimp_apikey', 'password', array('label' => 'mailchimp.api_key', 'placeholder' => 'mailchimp.api_key', 'required' => true, 'value' => $data->get('mailchimp_apikey')));

        if ($data->get('mailchimp_enabled') && $data->get('mailchimp_apikey'))
        {
            require_once dirname(dirname(dirname(dirname(__FILE__)))).'/vendors/mailchimp/src/Mailchimp.php';
            $mc = new Mailchimp($data->get('mailchimp_apikey'));

            try {
                $allchimplists = $mc->lists->getList();

                if (isset($allchimplists['data']) && count($allchimplists['data']) > 0)
                {
                    $options = array('' => 'None');
                    foreach ($allchimplists['data'] as $chimplistoption)
                    {
                        $options[$chimplistoption['id']] = $chimplistoption['name'];
                    }
                    $formBuilder->add('mailchimp_newsletter_list', 'choice', array('label' => 'mailchimp.newsletter_list', 'value' => $data->get('mailchimp_newsletter_list'), 'options' => $options));
                }
            }
            catch (Exception $e) {}
        }

        $formBuilder->add('mailchimp_e360', 'checkbox', array('label' => 'mailchimp.ecommerce_360', 'value' => '1', 'current_value' => $data->get('mailchimp_e360')));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getMailchimpActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $formBuilder->add('mailchimp_username', 'text', array('label' => 'mailchimp.username', 'placeholder' => 'mailchimp.username', 'required' => true, 'value' => $data->get('mailchimp_username')));
        $formBuilder->add('mailchimp_password', 'password', array('label' => 'mailchimp.password', 'placeholder' => 'mailchimp.password', 'required' => true, 'value' => $data->get('mailchimp_password')));
        $formBuilder->add('mailchimp_apikey', 'password', array('label' => 'mailchimp.api_key', 'placeholder' => 'mailchimp.api_key', 'required' => true, 'value' => $data->get('mailchimp_apikey')));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getMailchimpActivateForm($data)
    {
        return $this->getMailchimpActivateBuilder($data)->getForm();
    }
}
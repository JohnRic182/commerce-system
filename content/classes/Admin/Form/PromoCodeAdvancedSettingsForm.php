<?php

/**
 * Class Admin_Form_PromoCodeAdvancedSettingsForm
 */
class Admin_Form_PromoCodeAdvancedSettingsForm
{
	/**
	 * Get advanced settings form for promotions - builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$basicGroup->add('promoCodesSettings[DiscountsPromo]', 'checkbox', array(
				'label' => 'marketing.promo_codes_enabled',
				'value' => 'YES',
				'current_value' => $formData['DiscountsPromo'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['DiscountsPromo'])
		));

		return $formBuilder;
	}

	/**
	 * Get advanced promo codes settings form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
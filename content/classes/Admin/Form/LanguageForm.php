<?php
/**
 * Class Admin_Form_TextPageForm
 */
class Admin_Form_LanguageForm
{
	
	/**
	 * Get text pages form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 * @param @existingLanguages
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null, $existingLanguages = null)
	{
		$formBuilder = new core_Form_FormBuilder('general');
		
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'language.language_details'));
		
		$formBuilder->add($basicGroup);
		
		$basicGroup->add('name', 'text', array('label' => 'language.title', 'value' => $formData['name'], 'required' => true, 'attr' => array('maxlength' => '100'), 'note' => 'Enter the language name here. For example: English, French, Spanish, etc.  This title will be used on the storefront when your visitor changes languages.'));
		$basicGroup->add('code', 'text', array('label' => 'language.code', 'value' => $formData['code'], 'required' => true, 'attr' => array('maxlength' => '100')));

		if ($formData['is_default'] == 'Yes')
		{
			$basicGroup->add('is_active', 'static', array('label' => 'language.visible', 'value' => $formData['is_active']));
		}
		else
		{
			$basicGroup->add('is_active', 'checkbox', array('label' => 'language.visible_interrogative', 'value' => 'Yes', 'current_value' => $formData['is_active'], 'wrapperClass' => 'clear-both'));
		}
		
		if ($mode == 'add')
		{
			// form to use existing language as template or upload lang file
			$basicGroup->add('template_lang', 'choice', array(
				'label' => 'language.use_existing_language_as_template', 
				'value' => '', 
				'options' => $existingLanguages, 
				'empty_value' => array('' => trans('language.no_template_upload_file')),
				'note' => 'language.create_copy_of_existing_language'
			));
			
			$basicGroup->add('template_file', 'file', array(
				'label' => 'language.choose_language_file',
				'required' => (empty($formData['template_lang']) ? true : false)
			));
		}
		else
		{
			// display all messages for selected lang, grouped by message category
			foreach ($formData['msg'] as $messageCategory => $messages)
			{
				//print $message_category;
				$visibleMessageCategory = ucfirst(str_replace('_', ' ', $messageCategory));
				$messageGroup = new core_Form_FormBuilder($messageCategory, array('label'=> $visibleMessageCategory, 'collapsible' => true));

				foreach ($messages as $msgId => $msgContent)
				{
					if (!is_array($msgContent))
					{
						$label = isset($formData['template'][$messageCategory][$msgId]) ? $formData['template'][$messageCategory][$msgId] : $msgId;
						$messageGroup->add(
							'msg[' . $messageCategory . '][' . $msgId . ']',
							'textarea',
							array(
								'label' => htmlspecialchars($label),
								'value' => $msgContent
							)
						);
					}
				}

				$formBuilder->add($messageGroup);
			}
		}
		
		return $formBuilder;
	}
	
	/**
	 * Get text page form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 * @param existingLanguages array|null
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null, $existingLanguages = null)
	{
		return $this->getBuilder($formData, $mode, $id, $existingLanguages)->getForm();
	}
}
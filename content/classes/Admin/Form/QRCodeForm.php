<?php
/**
 * Class Admin_Form_QRCodeForm
 */
class Admin_Form_QRCodeForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$request = Framework_Request::createFromGlobals();
	
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'Campaign Details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array('label' => 'marketing.name', 'value' => $formData['name'], 'required' => true, 'attr' => array('maxlength' => '100')));

		$basicGroup->add('description', 'text', array('label' => 'marketing.description', 'value' => $formData['description'], 'attr' => array('maxlength' => '250')));

		return $formBuilder;
	}

	/**
	 * Get campaign form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}
}
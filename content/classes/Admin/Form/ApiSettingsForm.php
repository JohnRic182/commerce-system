<?php

/**
 * Class Admin_Form_ApiSettingsForm
 */
class Admin_Form_ApiSettingsForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        $settingsGroup = new core_Form_FormBuilder('settings', array('label' => 'api.form_title'));
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('plugin_openapi_active', 'checkbox', array('label' => 'api.active', 'value' => 'YES', 'current_value' => $data['plugin_openapi_active']));
        $settingsGroup->add('plugin_openapi_username', 'text', array('label' => 'api.username', 'placeholder' => 'api.username', 'required' => true, 'value' => $data['plugin_openapi_username'], 'wrapperClass' => 'clear-both'));
        $settingsGroup->add('plugin_openapi_password', 'password', array('label' => 'api.password', 'placeholder' => 'api.password', 'required' => true));
        $settingsGroup->add('plugin_openapi_token', 'template', array(
            'file' => 'templates/pages/settings/api-token.html',
	        'required' => true, 'placeholder' => 'api.token',
	        'label' => 'api.token', 'value' => $data['plugin_openapi_token'],
	        'wrapperClass' => 'col-xs-12 col-md-6'));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
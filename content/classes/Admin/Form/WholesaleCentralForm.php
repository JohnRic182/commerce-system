<?php

/**
 * Class Admin_Form_WholesaleCentralForm
 */
class Admin_Form_WholesaleCentralForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getWholesaleCentralActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('WholesaleCentralUsername', 'text', array('label' => 'apps.wholesalecentral.username', 'required' => true, 'value' => $data['WholesaleCentralUsername']));
        $settingsGroup->add('WholesaleCentralPassword', 'password', array('label' => 'apps.wholesalecentral.password', 'required' => true, 'value' => $data['WholesaleCentralPassword']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getWholesaleCentralActivateForm($data)
    {
        return $this->getWholesaleCentralActivateBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('WholesaleCentralUsername', 'text', array('label' => 'apps.wholesalecentral.username', 'placeholder' => 'apps.wholesalecentral.username', 'required' => true, 'value' => $data['WholesaleCentralUsername']));
        $settingsGroup->add('WholesaleCentralPassword', 'password', array('label' => 'apps.wholesalecentral.password', 'placeholder' => 'apps.wholesalecentral.password', 'required' => true, 'value' => $data['WholesaleCentralPassword']));
        $settingsGroup->add('WholesaleCentralEnableCron', 'checkbox', array('label' => 'apps.wholesalecentral.daily_feed', 'value' => 1, 'current_value' => $data['WholesaleCentralEnableCron']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
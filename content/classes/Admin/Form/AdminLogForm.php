<?php

/**
 * Class Admin_Form_AdminLogForm
 */
class Admin_Form_AdminLogForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('search');

        $formBuilder->add('searchParams[from]', 'date', array('label' => 'common.start', 'value' => $data['mysql_from'], 'yearStart' => 2004, 'yearFinish' => date('Y') + 1));
        $formBuilder->add('searchParams[to]', 'date', array('label' => 'common.end', 'value' => $data['mysql_to'], 'yearStart' => 2004, 'yearFinish' => date('Y') + 1));
        $formBuilder->add('searchParams[username]', 'text', array('label' => 'admin.profile.username', 'value' => $data['username']));
        $formBuilder->add('searchParams[level]', 'choice', array('label' => 'admin.logs.level', 'value' => $data['level'], 'options' => array(
            '-1' => 'common.any',
            '0' => 'admin.logs.levels.0',
            '1' => 'admin.logs.levels.1',
            '2' => 'admin.logs.levels.2',
            '3' => 'admin.logs.levels.3',
            '4' => 'admin.logs.levels.4',
        )));

        $formBuilder->add('sort_by', 'choice', array('label' => 'admin.logs.sort_by', 'value' => $data['sort_by'], 'options' => array(
            'date_desc' => 'admin.logs.sort_by_options.date_desc',
            'username' => 'admin.logs.sort_by_options.username',
            'username_desc' => 'admin.logs.sort_by_options.username_desc',
            'ip' => 'admin.logs.sort_by_options.ip',
            'message' => 'admin.logs.sort_by_options.message',
        )));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
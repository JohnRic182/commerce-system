<?php

/**
 * Class Admin_Form_StoreAddressForm
 */
class Admin_Form_StoreAddressForm
{
    private $basicGroup;

    public function __construct()
    {
        $this->basicGroup = new core_Form_FormBuilder('basic');
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        $basicGroup = $this->getBasicGroup();
        $formBuilder->add($basicGroup);

        $basicGroup->add('CompanyName', 'text', array(
            'label' => 'store_information.company_name',
            'value' => $data['CompanyName'], 'placeholder' => 'store_information.company_name',
            'required' => true,
        ));
        $basicGroup->add('CompanyAddressLine1', 'text', array(
            'label' => 'store_information.street_address',
            'value' => $data['CompanyAddressLine1'],
            'required' => true, 'placeholder' => 'store_information.street_address',
        ));
        $basicGroup->add('CompanyAddressLine2', 'text', array(
            'label' => 'store_information.street_address_two',
            'value' => $data['CompanyAddressLine2'],
            'required' => false, 'placeholder' => 'store_information.street_address_two',
        ));
        $basicGroup->add('CompanyCity', 'text', array(
            'label' => 'store_information.city',
            'value' => $data['CompanyCity'],
            'required' => true, 'placeholder' => 'store_information.city',
        ));

        $basicGroup->add('CompanyState', 'choice', array(
            'label' => 'store_information.state',
            'value' => $data['CompanyState'],
            'required' => true, 'placeholder' => 'store_information.state',
            'attr' => array('data-value' => $data['CompanyState'], 'class' => 'input-address-state'),
        ));
        $basicGroup->add('CompanyProvince', 'text', array(
            'label' => 'store_information.province',
            'value' => $data['CompanyState'],
            'required' => true, 'placeholder' => 'store_information.province',
            'wrapperClass' => 'col-xs-12 col-md-6 hidden',
            'attr' => array('class' => 'input-address-province'),
        ));
        $basicGroup->add('CompanyCountry', 'choice', array(
            'label' => 'store_information.country',
            'value' => $data['CompanyCountry'],
            'required' => true, 'placeholder' => 'store_information.country',
            'attr' => array('data-value' => $data['CompanyCountry'], 'class' => 'input-address-country'),
        ));

        $basicGroup->add('CompanyZip', 'text', array(
            'label' => 'store_information.zip_postal',
            'value' => $data['CompanyZip'],
            'required' => true, 'placeholder' => 'store_information.zip_postal',
	        'attr' => array('class' => 'input-address-zip')
        ));
        $basicGroup->add('CompanyPhone', 'text', array(
            'label' => 'store_information.phone',
            'value' => $data['CompanyPhone'],
            'required' => false, 'placeholder' => 'store_information.phone',
        ));
        $basicGroup->add('CompanyFax', 'text', array(
            'label' => 'store_information.fax',
            'value' => $data['CompanyFax'],
            'required' => false, 'placeholder' => 'store_information.fax',
        ));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @return core_Form_FormBuilder
     */
    public function getBasicGroup()
    {
        return $this->basicGroup;
    }
}
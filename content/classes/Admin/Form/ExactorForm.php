<?php

/**
 * Class Admin_Form_ExactorForm
 */
class Admin_Form_ExactorForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsForm = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsForm);

        $settingsForm->add('exactor_merchant_id', 'text', array('label' => 'Account #', 'placeholder' => 'Account #', 'value' => $data['exactor_merchant_id'], 'required' => true));
        $settingsForm->add('exactor_user_id', 'text', array('label' => 'Username', 'placeholder' => 'Username', 'value' => $data['exactor_user_id'], 'required' => true));
        $settingsForm->add('exactor_password', 'password', array('label' => 'Password', 'placeholder' => 'Password', 'value' => $data['exactor_password'], 'required' => true));
        $settingsForm->add('exactor_full_name', 'text', array('label' => 'Company Full Name', 'placeholder' => 'Company Full Name', 'value' => $data['exactor_full_name'], 'required' => true));
        $settingsForm->add('exactor_currency_code', 'text', array('label' => 'Currency Code', 'placeholder' => 'Currency Code', 'value' => $data['exactor_currency_code'], 'required' => true));
        $settingsForm->add(
            'exactor_commit_taxes', 'choice',
            array('label' => 'Commit taxes on completed orders?', 'value' => $data['exactor_commit_taxes'], 'options' => array('0' => 'No', '1' => 'Yes'), 'attr' => array('class' => 'short'), 'note' => 'Yes - Commit taxes to Exactor for tax purposes<br/>No - Just calculate taxes. <strong>Do not</strong> use Exactor for tax reporting')
        );

        $settingsForm->add(
            'exactor_sku_source', 'choice',
            array('label' => 'Source of SKU Information', 'value' => $data['exactor_sku_source'], 'options' => array('SKU' => 'SKU Field', 'PID' => 'Product Id'), 'attr' => array('class' => 'short'))
        );

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getExactorActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('exactor_merchant_id', 'text', array('label' => 'Account #', 'placeholder' => 'Account #', 'required' => true, 'value' => $data['exactor_merchant_id']));
        $settingsGroup->add('exactor_user_id', 'text', array('label' => 'Username', 'placeholder' => 'Username', 'required' => true, 'value' => $data['exactor_user_id']));
        $settingsGroup->add('exactor_password', 'password', array('label' => 'Password', 'placeholder' => 'Password', 'required' => true, 'value' => $data['exactor_password']));
        $settingsGroup->add('exactor_full_name', 'text', array('label' => 'Company Full Name', 'placeholder' => 'Company Full Name', 'required' => true, 'value' => $data['exactor_full_name']));
        $settingsGroup->add('exactor_currency_code', 'text', array('label' => 'Currency Code', 'placeholder' => 'Currency Code', 'required' => true, 'value' => $data['exactor_currency_code']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getExactorActivateForm($data)
    {
        return $this->getExactorActivateBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getExactorTaxCodeBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('tax-code');

        $basicGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($basicGroup);

        $basicGroup->add('tax_code_name', 'text', array('label' => 'Tax Name', 'placeholder' => 'Tax Name', 'required' => true, 'value' => $data['name']));
        $basicGroup->add('tax_code_code', 'text', array('label' => 'Tax Code', 'placeholder' => 'Tax Code', 'required' => true, 'value' => $data['code']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getExactorTaxCodeForm($data)
    {
        return $this->getExactorTaxCodeBuilder($data)->getForm();
    }
}
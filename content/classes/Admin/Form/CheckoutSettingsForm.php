<?php

/**
 * Class Admin_Form_CheckoutSettingsForm
 */
class Admin_Form_CheckoutSettingsForm
{
    /**
     * Holds our basic form
     * @var core_Form_FormBuilder
     */
    private $basicGroup;

    /**
     * Admin_Form_CheckoutSettingsForm constructor.
     */
    public function __construct()
    {
        $this->basicGroup = new core_Form_FormBuilder('basic');
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        $basicGroup = $this->getBasicGroup();
        $formBuilder->add($basicGroup);

        $basicGroup->add('VisitorSeePrice', 'checkbox', array(
            'label' => 'checkout.visitor_see_price',
            'value' => 'NO',
            'current_value' => $data['VisitorSeePrice'],
        ));
        $basicGroup->add('VisitorMayAddItem', 'checkbox', array(
            'label' => 'checkout.visitor_may_add_item',
            'value' => 'YES',
            'current_value' => $data['VisitorMayAddItem'],
        ));
        $basicGroup->add('AllowCreateAccount', 'choice', array(
            'label' => 'checkout.allow_create_account',
            'value' => $data['AllowCreateAccount'],
            'options' => array(
                'Allow user to decide' => 'checkout.allow_create_account_options.allow_guest_checkout',
                'Yes' => 'checkout.allow_create_account_options.yes',
                'No' => 'checkout.allow_create_account_options.no',
            )
        ));
        $basicGroup->add('MinOrderNumber', 'text', array(
            'label' => 'checkout.min_order_number',
            'value' => $data['MinOrderNumber'],
        ));
        $basicGroup->add('AfterProductAddedGoTo', 'choice', array(
            'label' => 'checkout.after_product_added_go_to',
            'value' => $data['AfterProductAddedGoTo'],
            'options' => array(
                'Current Page' => 'checkout.after_product_added_go_to_options.current_page',
                'Cart Page' => 'checkout.after_product_added_go_to_options.cart',
                'Checkout Page' => 'checkout.after_product_added_go_to_options.checkout_page'
            ),
        ));

        $basicGroup->add('AfterContinueShoppingClickGoTo', 'text', array('label' => 'Continue Shopping Link',
                'value' => $data['AfterContinueShoppingClickGoTo'],
                'note' => 'checkout.continueshoppingnote')
        );
        $basicGroup->add('MinOrderSubtotalLevel0', 'text', array('label' => 'Min Order Total', 'value' => $data['MinOrderSubtotalLevel0']));
        $basicGroup->add('OrderShippingAsBillingDefaultOption', 'checkbox', array(
            'label' => 'checkout.order_shipping_as_billing_default_option',
            'value' => 'Yes',
            'current_value' => $data['OrderShippingAsBillingDefaultOption'],
            'wrapperClass' => 'clear-both',
        ));
        $basicGroup->add('ReceivesMarketingDefault', 'checkbox', array(
            'label' => 'checkout.receivesmarketingdefault',
            'value' => 'Yes',
            'current_value' => $data['ReceivesMarketingDefault'],
        ));
        $basicGroup->add('DisplayTermsAndConditionsCheckbox', 'checkbox', array(
            'label' => 'checkout.display_terms_and_conditions_checkbox',
            'value' => 'Yes',
            'current_value' => $data['DisplayTermsAndConditionsCheckbox'],
        ));
        $basicGroup->add('AlwaysDisplayTermsAndConditionsCheckbox', 'checkbox', array(
            'label' => 'checkout.always_display_terms_and_conditions_checkbox',
            'value' => 'Yes',
            'current_value' => $data['AlwaysDisplayTermsAndConditionsCheckbox'],
        ));
        $basicGroup->add('CardTypeMustMatchNumber', 'checkbox', array(
            'label' => 'checkout.card_type_must_match_number',
            'value' => 'Yes',
            'current_value' => $data['CardTypeMustMatchNumber'],
            'note' => 'checkout.card_type_must_match_number_note',
        ));
        $basicGroup->add('ClearCartOnLogout', 'checkbox', array(
            'label' => 'checkout.clear_cart_on_logout',
            'value' => 'Yes',
            'current_value' => $data['ClearCartOnLogout'],
        ));
        $basicGroup->add('EnableWishList', 'checkbox', array(
            'label' => 'checkout.enable_wishlist',
            'value' => 'Yes',
            'current_value' => $data['EnableWishList'],
        ));
        $basicGroup->add('AllowOutOfInventoryOrders', 'checkbox', array(
            'label' => 'checkout.allow_out_of_inventory_orders',
            'value' => 'Yes',
            'current_value' => $data['AllowOutOfInventoryOrders'],
        ));
        $basicGroup->add('InventoryStockUpdateAt', 'choice', array(
            'label' => 'checkout.inventory_stockUpdates_when',
            'value' => $data['InventoryStockUpdateAt'],
            'options' => array(
                'Order Completed' => 'checkout.inventory_stockUpdates_when_options.order_completed',
                'Payment Received' => 'checkout.inventory_stockUpdates_when_options.payment_received',
                'Order Placed' => 'checkout.inventory_stockUpdates_when_options.order_placed',
                'Add To Cart' => 'checkout.inventory_stockUpdates_when_options.add_to_cart',
            ),
            'wrapperClass' => 'clear-both',
        ));

        $basicGroup->add('DisplayStockOnProductPage', 'checkbox', array(
            'label' => 'checkout.display_stock_on_product_page',
            'value' => 'YES',
            'current_value' => $data['DisplayStockOnProductPage'],
        ));
        $basicGroup->add('DisplayStockOnProductPageThreshold', 'text', array(
            'label' => 'checkout.display_stock_on_product_page_threshold',
            'value' => $data['DisplayStockOnProductPageThreshold'],
        ));

        $basicGroup->add('GiftCardActive', 'checkbox', array(
            'label' => 'checkout.gift_card_active',
            'value' => 'YES',
            'current_value' => $data['GiftCardActive'],
            'wrapperClass' => 'clear-both',
        ));

        $basicGroup->add('GiftCardMessageLength', 'text', array(
            'label' => 'checkout.gift_card_message_length',
            'value' => $data['GiftCardMessageLength'],
        ));

        $fieldsGroup = new core_Form_FormBuilder('fields', array('label' => 'Checkout Fields'));
        $formBuilder->add($fieldsGroup);

        $settings_options = array(
            new core_Form_Option('Required', 'form_fields.field_visiblity.required'),
            new core_Form_Option('Notrequired', 'form_fields.field_visiblity.not_required'),
            new core_Form_Option('Invisible', 'form_fields.field_visiblity.hidden'),
        );

        $fieldsGroup->add('FormsBillingCompany', 'choice', array(
            'label' => 'form_fields.FormsBillingCompany',
            'value' => $data['FormsBillingCompany'],
            'multiple' => false,
            'expanded' => false,
            'options' => $settings_options,
        ));

        $fieldsGroup->add('FormsBillingAddressLine2', 'choice', array(
            'label' => 'form_fields.FormsBillingAddressLine2',
            'value' => $data['FormsBillingAddressLine2'],
            'multiple' => false,
            'expanded' => false,
            'options' => $settings_options,
        ));

        $fieldsGroup->add('FormsBillingPhone', 'choice', array(
            'label' => 'form_fields.FormsBillingPhone',
            'value' => $data['FormsBillingPhone'],
            'multiple' => false,
            'expanded' => false,
            'options' => $settings_options,
        ));

        $fieldsGroup->add('FormsShippingCompany', 'choice', array(
            'label' => 'form_fields.FormsShippingCompany',
            'value' => $data['FormsShippingCompany'],
            'multiple' => false,
            'expanded' => false,
            'options' => $settings_options,
        ));

        $fieldsGroup->add('FormsShippingAddressLine2', 'choice', array(
            'label' => 'form_fields.FormsShippingAddressLine2',
            'value' => $data['FormsShippingAddressLine2'],
            'multiple' => false,
            'expanded' => false,
            'options' => $settings_options,
        ));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @return mixed
     */
    public function getBasicGroup()
    {
        return $this->basicGroup;
    }
}
<?php

/**
 * Class Admin_Form_OrderForm
 */
class Admin_Form_OrderForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getOrderSearchBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('search');

        $formBuilder->add('searchParams[order_num]', 'text', array('label' => 'orders.order_id', 'value' => $data['order_num'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2'));
        $formBuilder->add('searchParams[custom_search]', 'text', array('label' => 'orders.custom_search', 'value' => $data['custom_search'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2'));
        $formBuilder->add('searchParams[product_id]', 'text', array('label' => 'orders.product_id', 'value' => $data['product_id'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2'));
        $formBuilder->add('searchParams[status]', 'choice', array('label' => 'orders.order_status', 'value' => $data['status'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2', 'options' => array(
            'any' => 'common.any',
            'Process' => 'orders.status_options.Process',
            'Backorder' => 'orders.status_options.Backorder',
            'Canceled' => 'orders.status_options.Canceled',
            'Completed' => 'orders.status_options.Completed',
            'Failed' => 'orders.status_options.Failed',
            'Abandon' => 'orders.abandoned',
        )));
        $formBuilder->add('searchParams[payment_status]', 'choice', array('label' => 'orders.payment_status', 'value' => $data['payment_status'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2', 'options' => array(
            'any' => 'common.any',
            'Pending' => 'orders.payment_status_options.Pending',
            'Partial' => 'orders.payment_status_options.Partial',
            'Received' => 'orders.payment_status_options.Received',
            'Refunded' => 'orders.payment_status_options.Refunded',
            'Declined' => 'orders.payment_status_options.Declined',
            'Canceled' => 'orders.payment_status_options.Canceled',
            'Error' => 'orders.payment_status_options.Error',
            'Auth Expired' => 'orders.payment_status_options.Auth_Expired',
            'Reversed' => 'orders.payment_status_options.Reversed',
        )));
        $formBuilder->add('searchParams[fulfillment_status]', 'choice', array('label' => 'orders.fulfillment_status', 'value' => $data['fulfillment_status'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2', 'options' => array(
            'any' => 'common.any',
            'pending' => 'common.pending',
            'partial' => 'common.partial',
            'completed' => 'common.completed',
        )));

        $recurringBillingEnabled = isset($data['recurringEnabled']) ? $data['recurringEnabled'] : false;

        if ($recurringBillingEnabled)
        {
            $orderTypeOptions = array(
                'any' => 'Any',
                ORDER::ORDER_TYPE_WEB => 'orders.order_type_options.web',
                ORDER::ORDER_TYPE_RECURRING => 'orders.order_type_options.recurring'
            );

            $formBuilder->add('searchParams[order_type]', 'choice', array('label' => 'orders.order_type', 'value' => $data['order_type'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2', 'options' => $orderTypeOptions));
        }

        $formBuilder->add('searchParams[min_amount]', 'text', array('label' => 'orders.min_amount', 'value' => $data['min_amount'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2'));
        $formBuilder->add('searchParams[max_amount]', 'text', array('label' => 'orders.max_amount', 'value' => $data['max_amount'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2'));

        $formBuilder->add('searchParams[period]', 'hidden', array('label' => 'orders.order_period', 'value' => 'any' /**$data['period']**/, 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2', 'options' => array(
            'any' => 'common.any',
            '24' => 'orders.order_period_options.24',
            '72' => 'orders.order_period_options.72',
            '168' => 'orders.order_period_options.168',
            '336' => 'orders.order_period_options.336',
            '744' => 'orders.order_period_options.744',
        )));

        $formBuilder->add('searchParams[from]', 'text', array('label' => 'Orders Date From', 'value' => $data['from'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2', 'attr' => array('class' => 'order-date-from')));
        $formBuilder->add('searchParams[to]', 'text', array('label' => 'Order Date To', 'value' => $data['to'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2', 'attr' => array('class' => 'order-date-to')));
		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		$formBuilder->add(
		    'searchParams[advanced_search]',
		    'checkbox',
		    array(
			    'label' => 'orders.advanced_search',
			    'current_value'	=> $data['advanced_search'],
			    'wrapperClass' => 'col-xs-12 col-md-12',
		    )
		);

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getOrderSearchForm($data)
    {
        return $this->getOrderSearchBuilder($data)->getForm();
    }

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
    public function getOrderSearchAdvancedBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('search_advanced');

        $visible = isset($data['advanced_search']) && $data['advanced_search'] == 1;

        $formBuilder->add('searchParams[state]', 'text', array('label' => 'orders.state', 'value' => $data['state'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2' . ($visible ? '' : ' hidden')));
        $formBuilder->add('searchParams[zip_code]', 'text', array('label' => 'orders.zip_code', 'value' => $data['zip_code'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2' . ($visible ? '' : ' hidden')));
        $formBuilder->add('searchParams[notes_content]', 'text', array('label' => 'orders.notes_content', 'value' => $data['notes_content'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2' . ($visible ? '' : ' hidden')));
        $formBuilder->add('searchParams[promo_code]', 'text', array('label' => 'orders.promo_code', 'value' => $data['promo_code'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2' . ($visible ? '' : ' hidden')));
        $formBuilder->add('searchParams[product_name]', 'text', array('label' => 'orders.product_name', 'value' => $data['product_name'], 'wrapperClass' => 'col-xs-12 col-md-4 col-lg-2' . ($visible ? '' : ' hidden')));

		return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getOrderSearchAdvancedForm($data)
    {
        return $this->getOrderSearchAdvancedBuilder($data)->getForm();
    }



    /**
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getOrderUpdateStatusBuilder()
    {
        $updateStatusFormBuilder = new core_Form_FormBuilder('update-status');
        $updateStatusFormBuilder->add('status', 'choice', array('label' => 'orders.status', 'options' => array(
            '' => 'orders.keep_current',
            'Process' => 'orders.status_options.Process',
            'Backorder' => 'orders.status_options.Backorder',
            'Canceled' => 'orders.status_options.Canceled',
            'Completed' => 'orders.status_options.Completed',
        )));
        $updateStatusFormBuilder->add('payment_status', 'choice', array('label' => 'orders.payment', 'options' => array(
            '' => 'orders.keep_current',
            'Pending' => 'orders.payment_status_options.Pending',
            'Received' => 'orders.payment_status_options.Received',
            'Declined' => 'orders.payment_status_options.Declined',
            'Canceled' => 'orders.payment_status_options.Canceled',
            'Error' => 'orders.payment_status_options.Error',
        )));

        $updateStatusFormBuilder->add('send_notification_email', 'checkbox', array('label' => 'orders.notify_customer', 'current_value' => true));

        return $updateStatusFormBuilder;
    }

    /**
     * @return mixed
     */
    public function getOrderUpdateStatusForm()
    {
        return $this->getOrderUpdateStatusBuilder()->getForm();
    }

    /**
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getOrderAddOrderNoteBuilder()
    {
        $addOrderNoteFormBuilder = new core_Form_FormBuilder('add_note');
        $addOrderNoteFormBuilder->add('send_email', 'checkbox', array('label' => 'orders.notify_customer', 'value' => 'Yes', 'current_value' => 'Yes'));
        $addOrderNoteFormBuilder->add('title', 'text', array('label' => 'orders.add_note.title'));
        $addOrderNoteFormBuilder->add('note', 'textarea', array('label' => 'orders.add_note.note'));

        return $addOrderNoteFormBuilder;
    }

    /**
     * @return mixed
     */
    public function getOrderAddOrderNoteForm()
    {
        return $this->getOrderAddOrderNoteBuilder()->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getOrderProcessPaymentBuilder($data)
    {
        $processPaymentFormBuilder = new core_Form_FormBuilder('process-payment');

        $statusOptions = array();

        if ($data['showCapture']) $statusOptions['capture'] = trans('orders.transaction_options.capture');
        if ($data['showRefund']) $statusOptions['refund'] =  trans('orders.transaction_options.refund');
        if ($data['showVoid']) $statusOptions['void'] =  trans('orders.transaction_options.void');

        $processPaymentFormBuilder->add('status', 'choice', array('label' => 'orders.payment', 'options' => $statusOptions));

        if ($data['showCapture'])
        {
            $processPaymentFormBuilder->add(
                'capture_amount', 'text',
                array('label' => 'orders.amount_to_capture', 'value' => $data['capturableAmount'], 'wrapperClass' => 'hidden', 'readonly' => !$data['supportsPartialCapture'])
            );
        }

        if ($data['showRefund'])
        {
            $processPaymentFormBuilder->add('refund_amount', 'text', array('label' => 'orders.amount_to_refund', 'value' => $data['refundableAmount'], 'wrapperClass' => 'hidden'));
        }

        return $processPaymentFormBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getOrderProcessPaymentForm($data)
    {
        return $this->getOrderProcessPaymentBuilder($data)->getForm();
    }
}
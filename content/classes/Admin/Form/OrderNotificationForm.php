<?php

/**
 * Class Admin_Form_OrderNotificationForm
 */
class Admin_Form_OrderNotificationForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 * @param $typeOptions
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null, $typeOptions = null)
	{
		$translator = Framework_Translator::getInstance();

		$request = Framework_Request::createFromGlobals();

		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'order_notifications.order_notification_details'));
		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array('label' => 'order_notifications.company_name', 'value' => $formData['name'], 'placeholder' => 'order_notifications.company_name', 'required' => true, 'attr' => array('maxlength' => '100')));
		$basicGroup->add('code', 'text', array('label' => 'order_notifications.location_id', 'value' => $formData['code'], 'placeholder' => 'order_notifications.location_id', 'required' => true, 'attr' => array('maxlength' => '100')));
		$basicGroup->add('contact_name', 'text', array('label' => 'order_notifications.contact_name', 'value' => $formData['contact_name'], 'placeholder' => 'order_notifications.contact_name', 'required' => true, 'attr' => array('maxlength' => '100')));
		$basicGroup->add('notify_email', 'text', array('label' => 'order_notifications.email', 'value' => $formData['notify_email'], 'placeholder' => 'order_notifications.email', 'required' => true, 'attr' => array('maxlength' => '100')));
		
		$opts = array();
		if (is_array($typeOptions))
		{
			foreach ($typeOptions as $key=>$value)
			{
				$opts[] = new core_Form_Option($key, $value);
			}
		}
		$basicGroup->add('location_type', 'choice', array('label' => 'order_notifications.type', 'attr' => array('class' => 'large'), 'required' => true, 'value' => $formData['location_type'], 'multiple' => false, 'expanded' => false, 'options' => $opts));
		$basicGroup->add('is_active', 'checkbox', array('label' => 'order_notifications.enabled', 'value' => 'Yes', 'current_value' => ($formData['is_active'] == 'Yes' || $formData['is_active'] == 1) ? 'Yes' : 'No', 'wrapperClass' => 'clear-both'));
		$basicGroup->add('email_text', 'textarea', array('label' => 'order_notifications.special_instructions', 'wysiwyg' => false, 'required' => false, 'value' => $formData['email_text']));

		return $formBuilder;
	}

	/**
	 * Get order notification form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 * @param array|null $typeOptions
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null, $typeOptions = null)
	{
		return $this->getBuilder($formData, $mode, $id, $typeOptions)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_EbayStoreForm
 */
class Admin_Form_EbayStoreForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('export');

        $formBuilder->add('product_category', 'choice', array('label' => 'apps.ebay.product_category', 'options' => $data['categories'], 'empty_value' => array('0' => trans('apps.ebay.all_product_categories'))));

        $maxProductsOptions = array(
            '0' => trans('apps.ebay.all_products'),
            '10' => '10',
            '50' => '50',
            '100' => '100',
            '150' => '150',
        );
        for ($i = 250; $i <= 5000; $i+=250)
        {
            $maxProductsOptions[$i] = $i;
        }

        $formBuilder->add('product_count', 'choice', array('label' => 'apps.ebay.max_num', 'options' => $maxProductsOptions));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
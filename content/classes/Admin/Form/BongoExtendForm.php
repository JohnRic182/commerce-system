<?php

/**
 * Class Admin_Form_BongoExtendForm
 */
class Admin_Form_BongoExtendForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $formBuilder->add('bongocheckout_ExtendJS', 'textarea', array('label' => 'Bongo Extend Javascript', 'value' => $data->get('bongocheckout_ExtendJS')));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
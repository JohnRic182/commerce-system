<?php

/**
 * Class Admin_Form_QuickStartGuideForm
 */
class Admin_Form_QuickStartGuideForm
{
	private $generalInformationGroup;

	private $contactlnformationGroup;

	private $industrySpecificsGroup;

	private $logoGroup;

	private $socialMediaAccountsGroup;

	private $sharingSettingsGroup;

	private $siteInformationGroup;

	private $seoSettingsGroup;

	private $upsSettingsGroup;

	private $uspsSettingsGroup;

	private $fedExSettingsGroup;

	private $canadaPostSettingsGroup;

	private $vipParcelSettingsGroup;

	private $taxesGroup;

	/**
	 * Admin_Form_QuickStartGuideForm constructor.
	 */
	public function __construct()
	{
		$this->generalInformationGroup = new core_Form_FormBuilder(
			'general-information',
			array(
				'label' => 'quick_start_guide.step_one.general_information',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-12 col-xs-12 clear-both'
			)
		);

		$this->contactlnformationGroup = new core_Form_FormBuilder(
			'contact-information',
			array(
				'label' => 'quick_start_guide.step_one.contact_information',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-12 col-xs-12 clear-both'
			)
		);

		$this->industrySpecificsGroup = new core_Form_FormBuilder(
			'industry-specifics',
			array(
				'label' => 'quick_start_guide.step_one.industry_specifics',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->logoGroup = new core_Form_FormBuilder(
			'logo',
			array(
				'label' => 'quick_start_guide.step_one.logo',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->socialMediaAccountsGroup = new core_Form_FormBuilder(
			'social-media-accounts',
			array(
				'label' => 'quick_start_guide.step_two.social_media_accounts',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-8 col-xs-4 clear-both'
			)
		);

		$this->sharingSettingsGroup = new core_Form_FormBuilder(
			'sharing-settings',
			array(
				'label' => 'quick_start_guide.step_two.sharing_settings',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->siteInformationGroup = new core_Form_FormBuilder(
			'site-information',
			array(
				'label' => 'quick_start_guide.step_three.site_information',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->seoSettingsGroup = new core_Form_FormBuilder(
			'seo-settings',
			array(
				'label' => 'quick_start_guide.step_three.seo_settings',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->upsSettingsGroup = new core_Form_FormBuilder(
			'ups-settings',
			array(
				'label' => 'quick_start_guide.step_four.ups_settings',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->uspsSettingsGroup = new core_Form_FormBuilder(
			'fed-ex-settings',
			array(
				'label' => 'quick_start_guide.step_four.usps_settings',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->fedExSettingsGroup = new core_Form_FormBuilder(
			'fed-ex-settings',
			array(
				'label' => 'quick_start_guide.step_four.fed_ex_settings',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->canadaPostSettingsGroup = new core_Form_FormBuilder(
			'canada-post-settings',
			array(
				'label' => 'quick_start_guide.step_four.canada_post_settings',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->vipParcelSettingsGroup = new core_Form_FormBuilder(
			'vip-parcel-settings',
			array(
				'label' => 'quick_start_guide.step_four.vip_parcel_settings',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);

		$this->taxesGroup = new core_Form_FormBuilder(
			'taxes',
			array(
				'label' => 'quick_start_guide.step_four.taxes',
				'collapsible' => false,
				'wrapperClass' => 'col-sm-4 col-xs-4 clear-both'
			)
		);
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getGeneralInformationFormBuilder($data)
	{
		/**
		 * General Information group
		 */
		$formBuilder = $this->getGeneralInformationGroup();

		$formBuilder->add(
			'qsg_CompanyName',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.company_name',
				'value' => $data['CompanyName'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.company_name',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyAddressLine1',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.street_name',
				'value' => $data['CompanyAddressLine1'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.street_name',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyAddressLine2',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.suite_name',
				'value' => $data['CompanyAddressLine2'],
				'placeholder' => 'quick_start_guide.step_one.suite_name',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyCity',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.city',
				'value' => $data['CompanyCity'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.city',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyCountry',
			'choice',
			array(
				'label' => 'quick_start_guide.step_one.country',
				'value' => $data['CompanyCountry'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.country',
				'attr' => array('data-value' => $data['CompanyCountry'], 'class' => 'input-address-country'),
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyState',
			'choice',
			array(
				'label' => 'quick_start_guide.step_one.state',
				'value' => $data['CompanyState'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.state',
				'attr' => array('data-value' => $data['CompanyState'], 'class' => 'input-address-state'),
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyProvince',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.province',
				'value' => $data['CompanyProvince'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.province',
				'attr' => array('class' => 'input-address-province'),
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyZip',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.zip',
				'value' => $data['CompanyZip'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.zip',
				'note' => 'quick_start_guide.step_one.zip_tooltip',
				'wrapperClass' => 'col-sm-12 col-xs-12',
				'attr' => array('class' => 'input-address-zip')
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getGeneralInformationForm($data)
	{
		return $this->getGeneralInformationFormBuilder($data)->getForm();
	}

	public function getContactlnformationFormBuilder($data)
	{
		/**
		 * Contact Information group
		 */
		$formBuilder = $this->getContactlnformationGroup();

		$formBuilder->add(
			'qsg_CompanyPhone',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.phone',
				'value' => $data['CompanyPhone'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.phone',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyFax',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.fax',
				'value' => $data['CompanyFax'],
				'placeholder' => 'quick_start_guide.step_one.fax',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_CompanyEmail',
			'text',
			array(
				'label' => 'quick_start_guide.step_one.email',
				'value' => $data['CompanyEmail'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.email',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getContactlnformationForm($data)
	{
		return $this->getContactlnformationFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getIndustrySpecificsFormBuilder($data)
	{
		/**
		 * Industry Specifics group
		 */
		$formBuilder = $this->getIndustrySpecificsGroup();

		$companyIndustryOptions = array(
			'' => 'Please Select',
			'Adult Entertainment' => 'Adult Goods',
			'Animal & Pet Care' => 'Animal & Pet Care',
			'Apparel/Clothing' => 'Apparel/Clothing',
			'Arts & Crafts' => 'Arts & Crafts',
			'Automotive' => 'Automotive',
			'Books, Music, Video' => 'Books, Music, Video',
			'Computers & Software' => 'Computers & Software',
			'E-Cig/Vapor/Smoking Accessories' => 'E-Cig/Vapor/Smoking Accessories',
			'Electronics' => 'Electronics',
			'Firearms/Ammo' => 'Firearms/Ammo',
			'Fireworks' => 'Fireworks',
			'Food & Beverage' => 'Food & Beverage',
			'Furniture' => 'Furniture',
			'Garden & Home Improvemen' => 'Garden & Home Improvemen',
			'Health & Beauty' => 'Health & Beauty',
			'Jewelry & Accessories' => 'Jewelry & Accessories',
			'Medical/Pharmaceutical' => 'Medical/Pharmaceutical',
			'Multi/Drop Shipper' => 'Multi/Drop Shipper',
			'Business Supplies' => 'Business Supplies',
			'Services' => 'Services',
			'Sports & Recreation' => 'Sports & Recreation',
			'Toys & Games' => 'Toys & Games',
			'Wedding & Bridal' => 'Wedding & Bridal',
			'Other' => 'Other'
		);

		$formBuilder->add(
			'qsg_CompanyIndustry',
			'choice',
			array(
				'label' => 'quick_start_guide.step_one.industry',
				'value' => $data['CompanyIndustry'],
				'options' => $companyIndustryOptions,
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.industry',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$companyRevenueOptions = array(
			'' => 'Please Select',
			'≤ 10,000' => '≤ ' . $data['symbol_left'] . '10,000' . $data['symbol_right'],
			'0,000 - 49,999' => $data['symbol_left'] . '10,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '49,999' . $data['symbol_right'],
			'50,000 - 99,999' => $data['symbol_left'] . '50,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '99,999' . $data['symbol_right'],
			'100,000 - 249,999' => $data['symbol_left'] . '100,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '249,999' . $data['symbol_right'],
			'250,000 - 499,999' => $data['symbol_left'] . '250,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '499,999' . $data['symbol_right'],
			'500,000 - 999,999' => $data['symbol_left'] . '500,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '999,999' . $data['symbol_right'],
			'1,000,000 - 4,999,999' => $data['symbol_left'] . '1,000,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '4,999,999' . $data['symbol_right'],
			'5,000,000 - 9,999,999' => $data['symbol_left'] . '5,000,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '9,999,999' . $data['symbol_right'],
			'10,000,000 - 24,999,999' => $data['symbol_left'] . '10,000,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '24,999,999' . $data['symbol_right'],
			'25,000,000 - 49,999,999' => $data['symbol_left'] . '25,000,000' . $data['symbol_right'] . ' - ' . $data['symbol_left'] . '49,999,999' . $data['symbol_right'],
			'50,000,000+' => $data['symbol_left'] . '50,000,000'. $data['symbol_right'] . '+'
		);

		$formBuilder->add(
			'qsg_CompanyRevenue',
			'choice',
			array(
				'label' => 'quick_start_guide.step_one.revenue',
				'value' => $data['CompanyRevenue'],
				'options' => $companyRevenueOptions,
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.revenue',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$companyEmployeesOptions = array(
			'' => 'Please Select',
			'1-10'=> '1-10',
			'11-25' => '11-25',
			'26-50' => '26-50',
			'51-100' => '51-100',
			'101-250' => '101-250',
			'251-500' => '251-500',
			'501-1000' => '501-1000',
			'1000+' => '1000+'
		);

		$formBuilder->add(
			'qsg_CompanyEmployees',
			'choice',
			array(
				'label' => 'quick_start_guide.step_one.employees',
				'value' => $data['CompanyEmployees'],
				'options' => $companyEmployeesOptions,
				'required' => true,
				'placeholder' => 'quick_start_guide.step_one.employees',
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getIndustrySpecificsForm($data)
	{
		return $this->getIndustrySpecificsFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getLogoFormBuilder($data)
	{
		/**
		 * Logo group
		 */
		$formBuilder = $this->getLogoGroup();
		$formBuilder->add(
			'qsg_logo',
			'image',
			array(
				'label' => 'quick_start_guide.step_one.upload_company_logo',
				'value' => '',
				'imageDeleteUrl' => null,
				'note' => 'quick_start_guide.step_one.upload_company_logo_tooltip',
				'wrapperClass' => 'col-sm-12 col-xs-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getLogoForm($data)
	{
		return $this->getLogoFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getSocialMediaAccountsFormBuilder($data)
	{
		/**
		 * Social Media Accounts group
		 */
		$formBuilder = $this->getSocialMediaAccountsGroup();

		// Facebook
		$formBuilder->add(
			'qsg_facebookThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/facebook.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12 facebook-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_facebookPageAccount',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.fb_label',
				'value' => $data['facebookPageAccount'],
				'placeholder' => 'quick_start_guide.step_two.fb_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 facebook-input'
			)
		);

		$formBuilder->add(
			'qsg_facebookSignUp',
			'static',
			array(
				'label' => '<a href="https://www.facebook.com/pages/create/?ref_type=registration_form" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 facebook-sign-up social-icons',
			)
		);

		// Twitter
		$formBuilder->add(
			'qsg_twitterThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/twitter.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12  twitter-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_twitterAccount',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.twitter_label',
				'value' => $data['twitterAccount'],
				'placeholder' => 'quick_start_guide.step_two.twitter_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 twitter-input'
			)
		);

		$formBuilder->add(
			'qsg_twitterSignUp',
			'static',
			array(
				'label' => '<a href="https://twitter.com/signup" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 twitter-sign-up social-icons',
			)
		);

		// Pinterest
		$formBuilder->add(
			'qsg_pinterestThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/pinterest.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12 pinterest-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_pinterestUsername',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.pinterest_label',
				'value' => $data['pinterestUsername'],
				'placeholder' => 'quick_start_guide.step_two.pinterest_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 pinterest-input'
			)
		);

		$formBuilder->add(
			'qsg_pinterestSignUp',
			'static',
			array(
				'label' => '<a href="https://www.pinterest.com/join/" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 pinterest-sign-up social-sign-up social-icons',
			)
		);

		// Instagram
		$formBuilder->add(
			'qsg_instagramThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/instagram.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12 instagram-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_instagramAccount',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.instagram_label',
				'value' => $data['instagramAccount'],
				'placeholder' => 'quick_start_guide.step_two.instagram_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 instagram-input social-input'
			)
		);

		$formBuilder->add(
			'qsg_instagramSignUp',
			'static',
			array(
				'label' => '<a href="https://www.instagram.com/accounts/emailsignup/" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 instagram-sign-up social-sign-up social-icons',
			)
		);

		// Youtube
		$formBuilder->add(
			'qsg_youtubeThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/youtube.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12 youtube-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_youtubePageURL',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.youtube_label',
				'value' => $data['youtubePageURL'],
				'placeholder' => 'quick_start_guide.step_two.youtube_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 youtube-input social-input'
			)
		);

		$formBuilder->add(
			'qsg_youtubeSignUp',
			'static',
			array(
				'label' => '<a href="https://www.youtube.com/account" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 youtube-sign-up social-sign-up social-icons',
			)
		);

		// SnapChat
		$formBuilder->add(
			'qsg_snapChatThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/snapchat.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12 snapChat-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_snapChatAccount',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.snapchat_label',
				'value' => $data['snapChatAccount'],
				'placeholder' => 'quick_start_guide.step_two.snapchat_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 snapChat-input social-input'
			)
		);

		$formBuilder->add(
			'qsg_snapChatSignUp',
			'static',
			array(
				'label' => '<a href="https://www.snapchat.com/" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 snapChat-sign-up social-sign-up social-icons',
			)
		);

		// Google Plus
		$formBuilder->add(
			'qsg_googlePlusAccountThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/googleplus.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12 googlePlus-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_googlePlusAccount',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.googleplus_label',
				'value' => $data['googlePlusAccount'],
				'placeholder' => 'quick_start_guide.step_two.googleplus_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 googlePlus-input social-input'
			)
		);

		$formBuilder->add(
			'qsg_googlePlusSignUp',
			'static',
			array(
				'label' => '<a href="https://business.google.com/add?service=plus" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 googlePlus-sign-up social-sign-up social-icons',
			)
		);

		// Blogger
		$formBuilder->add(
			'qsg_bloggerThumbnail',
			'static',
			array(
				'label' => '<img class="img-responsive" src="images/social/blogger.jpg" />',
				'wrapperClass' => 'col-sm-2 col-xs-12 blogger-thumbnail social-icons',
			)
		);

		$formBuilder->add(
			'qsg_bloggerAccount',
			'text',
			array(
				'label' => 'quick_start_guide.step_two.blogger_label',
				'value' => $data['bloggerAccount'],
				'placeholder' => 'quick_start_guide.step_two.blogger_label',
				'wrapperClass' => 'col-sm-10 col-xs-12 blogger-input social-input'
			)
		);

		$formBuilder->add(
			'qsg_bloggerSignUp',
			'static',
			array(
				'label' => '<a href="https://www.blogger.com/go/createyourblog" target="_blank">' . trans('quick_start_guide.step_two.sign_up') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-offset-2 col-sm-10 blogger-sign-up social-sign-up social-icons',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getSocialMediaAccountsForm($data)
	{
		return $this->getSocialMediaAccountsFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getSharingSettingsFormBuilder($data)
	{
		/**
		 * Sharing Settings group
		 */
		$formBuilder = $this->getSharingSettingsGroup();

		$formBuilder->add(
			'qsg_facebookLikeButtonProduct',
			'checkbox',
			array(
				'label' => 'quick_start_guide.step_two.enable_facebook_like_button',
				'value' => 'Yes',
				'current_value' => $data['facebookLikeButtonProduct'],
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_twitterTweetButtonProduct',
			'checkbox',
			array(
				'label' => 'quick_start_guide.step_two.enable_tweet_button',
				'value' => 'Yes',
				'current_value' => $data['twitterTweetButtonProduct'],
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_googlePlusButtonProduct',
			'checkbox',
			array(
				'label' => 'quick_start_guide.step_two.enable_google_plus_button',
				'value' => 'Yes',
				'current_value' => $data['googlePlusButtonProduct'],
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		$formBuilder->add(
			'qsg_pinItButtonProduct',
			'checkbox',
			array(
				'label' => 'quick_start_guide.step_two.enable_pinterest_button',
				'value' => 'Yes',
				'current_value' => $data['pinItButtonProduct'],
				'wrapperClass' => 'col-sm-12 col-xs-12'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getSharingSettingsForm($data)
	{
		return $this->getSharingSettingsFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getSiteInformationFormBuilder($data)
	{
		/**
		 * Site Information group
		 */
		$formBuilder = $this->getSiteInformationGroup();

		$formBuilder->add(
			'qsg_searchSettings[SearchTitle]',
			'text',
			array(
				'label' => 'quick_start_guide.step_three.site_title',
				'value' => $data['SearchTitle'],
				'placeholder' => 'quick_start_guide.step_three.site_title',
				'note' => 'quick_start_guide.step_three.site_title_tooltip',
				'wrapperClass' => 'col-xs-12 col-md-12',
				'attr' => array(
					'data-value' => $data['SearchTitle']
				),
				'required' => true
			)
		);

		$formBuilder->add(
			'qsg_searchSettings[SearchMetaDescription]',
			'textarea',
			array(
				'label' => 'quick_start_guide.step_three.meta_description',
				'value' => $data['SearchMetaDescription'],
				'placeholder' => 'quick_start_guide.step_three.meta_description',
				'note' => 'quick_start_guide.step_three.meta_description_tooltip',
				'wrapperClass' => 'col-sm-12 col-xs-12',
				'attr' => array(
					'data-value' => $data['SearchMetaDescription']
				)
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getSiteInformationForm($data)
	{
		return $this->getSiteInformationFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getSeoSettingsFormBuilder($data)
	{
		/**
		 * SEO Settings group
		 */
		$formBuilder = $this->getSeoSettingsGroup();

		if (AccessManager::checkAccess('FLAT_URL'))
		{
			$formBuilder->add(
				'qsg_searchSettings[USE_MOD_REWRITE]',
				'checkbox',
				array(
					'label' => 'quick_start_guide.step_three.enable_seo_friendly_urls',
					'value' => 'YES',
					'current_value' => $data['USE_MOD_REWRITE'],
					'note' => 'quick_start_guide.step_three.enable_seo_friendly_urls_tooltip',
					'wrapperClass' => 'col-sm-12 col-xs-12 clear-both',
					'attr' => array(
						'data-value' => $data['USE_MOD_REWRITE']
					)
				)
			);
		}
		else
		{
			$formBuilder->add(
				'qsg_modrewrite',
				'static',
				array(
					'label' => 'quick_start_guide.step_three.enable_flat_urls_generation',
					'value' => '<strong>' . trans('quick_start_guide.step_three.enable_flat_urls_generation_tooltip') . '</strong>',
					'wrapperClass' => 'col-sm-12 col-xs-12 clear-both',
				)
			);
		}

		$formBuilder->add(
			'qsg_searchSettings[SearchAutoGenerateLowercase]',
			'checkbox',
			array(
				'label' => 'quick_start_guide.step_three.use_lowercase_letters_in_urls',
				'value' => 'YES',
				'current_value' => $data['SearchAutoGenerateLowercase'],
				'note' => 'quick_start_guide.step_three.use_lowercase_letters_in_urls_tooltip',
				'wrapperClass' => 'col-sm-12 col-xs-12 clear-both',
				'attr' => array(
					'data-value' => $data['SearchAutoGenerateLowercase']
				)
			)
		);

		$formBuilder->add(
			'qsg_searchSettings[SearchAutoGenerate]',
			'checkbox',
			array(
				'label' => 'quick_start_guide.step_three.enable_meta_title_and_description_auto_generation',
				'value' => 'YES',
				'current_value' => $data['SearchAutoGenerate'],
				'note' => 'quick_start_guide.step_three.enable_meta_title_and_description_auto_generation_tooltip',
				'wrapperClass' => 'col-sm-12 col-xs-12 clear-both',
				'attr' => array(
					'data-value' => $data['SearchAutoGenerate']
				)
			)
		);

		$formBuilder->add(
			'qsg_searchSettings[seo_www_preference]',
			'choice',
			array(
				'label' => 'quick_start_guide.step_three.seo_www_preference',
				'value' => $data['seo_www_preference'],
				'wrapperClass' => 'col-sm-12 clear-both',
				'options' => array('default' => 'Do not modify URL\'s', 'www' => 'Force "www" URL\'s', 'nonwww' => 'Force non-"www" URL\'s'),
				'note' => 'marketing.seo.www_nowww'
			)
		);

		$formBuilder->add(
			'qsg_searchSettings[SitemapURL]',
			'text',
			array(
				'label' => 'quick_start_guide.step_three.sitemap_url',
				'value' => $data['sitemap_url'],
				'wrapperClass' => 'col-sm-12 clear-both copy-target',
				'placeholder' => 'quick_start_guide.step_three.sitemap_url',
				'attr' => array('readonly' => 'readonly', 'class' => 'auto-select readonly')
			)
		);

		$formBuilder->add(
			'qsg_copyToClipboard',
			'static',
			array(
				'label' => '<a href="#">' . trans('quick_start_guide.step_three.copy_to_clipboard') . '</a>',
				'wrapperClass' => 'col-xs-12 col-md-12 copy-to-clipboard',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getSeoSettingsForm($data)
	{
		return $this->getSeoSettingsFormBuilder($data)->getForm();
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getCustomPaymentFormBuilder()
	{
		$formBuilder = new core_Form_FormBuilder('custom-payment');

		$formBuilder->add(
			'title',
			'text',
			array(
				'label' => 'quick_start_guide.step_four.title_payment_method',
				'required' => true,
				'wrapperClass' => 'col-xs-12 col-sm-12',
			)
		);

		$formBuilder->add(
			'message_payment',
			'textarea',
			array(
				'label' => 'quick_start_guide.step_four.text_checkout_page',
				'value' => 'We are not accepting credit cards at this time, please complete your order',
				'wrapperClass' => 'col-xs-12 col-sm-12',
			)
		);

		$formBuilder->add(
			'message_thankyou',
			'textarea',
			array(
				'label' => 'quick_start_guide.step_four.thank_you_instructions',
				'value' => 'We will be contacting you shortly to complete your order',
				'wrapperClass' => 'col-xs-12 col-sm-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @return core_Form_Form
	 */
	public function getCustomPaymentForm()
	{
		return $this->getCustomPaymentFormBuilder()->getForm();
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getUpsDescriptionFormBuilder()
	{
		$formBuilder = new core_Form_FormBuilder('ups-description');

		$formBuilder->add(
			'ShippingUpsDescription',
			'description',
			array(
				'value' => trans('shipping.shipping_tax_class_tooltip'),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'
			)
		);

		return $formBuilder;
	}

	/**
	 * @return core_Form_Form
	 */
	public function getUpsDescriptionForm()
	{
		return $this->getUpsDescriptionFormBuilder()->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getUpsSettingsFormBuilder($data)
	{
		/**
		 * UPS Settings group
		 */
		$formBuilder = $this->upsSettingsGroup();

		$formBuilder->add(
			'ShippingUPSAccessKey',
			'text',
			array(
				'label' => 'shipping.ups_xml_access_key',
				'value' => $data['ShippingUPSAccessKey'],
				'placeholder' => 'shipping.ups_xml_access_key',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUPSUserID',
			'text',
			array(
				'label' => 'shipping.ups_user_id',
				'value' => $data['ShippingUPSUserID'],
				'placeholder' => 'shipping.ups_user_id',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUPSUserPassword',
			'text',
			array(
				'label' => 'shipping.ups_password',
				'value' => $data['ShippingUPSUserPassword'],
				'placeholder' => 'shipping.ups_password',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUPSRateChart',
			'choice',
			array(
				'label' => 'shipping.ups_pickup_type',
				'value' => $data['ShippingUPSRateChart'],
				'options' => array(
					'01' => 'shipping.daily_pickup',
					'07' => 'shipping.on_call_air_pickup',
					'06' => 'shipping.one_time_pickup',
					'19' => 'shipping.letter_center',
					'03' => 'shipping.customer_counter',
					'20' => 'shipping.air_service_center',
					'11' => 'shipping.suggested_retail_rates',
				),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'
			)
		);

		$formBuilder->add(
			'ShippingUPSContainer',
			'choice',
			array(
				'label' => 'shipping.ups_package_type',
				'value' => $data['ShippingUPSContainer'],
				'options' => array(
					'00' => 'shipping.your_packaging',
					'01' => 'shipping.ups_letter',
					'03' => 'shipping.ups_tube',
					'04' => 'shipping.ups_pak',
					'21' => 'shipping.ups_express_box',
					'24' => 'shipping.worldwide_25kg_box',
					'25' => 'shipping.worldwide_10kg_box',
				),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUPSUrl',
			'text',
			array(
				'label' => 'shipping.server_url_for_ups_rates',
				'value' => $data['ShippingUPSUrl'],
				'placeholder' => 'shipping.server_url_for_ups_rates',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getUpsSettingsForm($data)
	{
		return $this->getUpsSettingsFormBuilder($data)->getForm();
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getUspsDescriptionFormBuilder()
	{
		$formBuilder = new core_Form_FormBuilder('usps-description');

		$formBuilder->add(
			'ShippingUspsDescription',
			'description',
			array(
				'value' => '<span class="shipping-carrier shipping-carrier-usps shipping-carrier-modal"></span>' . trans('shipping.server_url_for_ups_rates_tooltip') . ' <a target="_blank" href="http://www.usps.com/webtools/">' . trans('shipping.click_here_to_go_to_usps_site') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'
			)
		);

		return $formBuilder;
	}

	/**
	 * @return core_Form_Form
	 */
	public function getUspsDescriptionForm()
	{
		return $this->getUspsDescriptionFormBuilder()->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getUspsSettingsFormBuilder($data)
	{
		/**
		 * USPS Settings group
		 */
		$formBuilder = $this->uspsSettingsGroup();

		$formBuilder->add(
			'ShippingUSPSUserID',
			'text',
			array(
				'label' => 'shipping.usps_tools_user_id',
				'value' => $data['ShippingUSPSUserID'],
				'placeholder' => 'shipping.usps_tools_user_id',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUSPSUrl',
			'text',
			array(
				'label' => 'shipping.server_url_for_usps_rates',
				'value' => $data['ShippingUSPSUrl'],
				'placeholder' => 'shipping.server_url_for_usps_rates',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUSPSPackageSize',
			'choice',
			array(
				'label' => 'shipping.package_size_for_usps',
				'value' => $data['ShippingUSPSPackageSize'],
				'options' => array(
					'REGULAR' => 'shipping.regular',
					'LARGE' => 'shipping.large',
				),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUSPSPackageWidth',
			'text',
			array(
				'label' => 'shipping.international_package_width',
				'value' => $data['ShippingUSPSPackageWidth'],
				'placeholder' => 'shipping.international_package_width',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUSPSPackageLength',
			'text',
			array(
				'label' => 'shipping.international_package_length',
				'value' => $data['ShippingUSPSPackageLength'],
				'placeholder' => 'shipping.international_package_length',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingUSPSPackageHeight',
			'text',
			array(
				'label' => 'shipping.international_package_height',
				'value' => $data['ShippingUSPSPackageHeight'],
				'placeholder' => 'shipping.international_package_height',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getUspsSettingsForm($data)
	{
		return $this->getUspsSettingsFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getFedExSettingsFormBuilder($data)
	{
		/**
		 * FedEx Settings group
		 */
		$formBuilder = $this->fedExSettingsGroup();

		$formBuilder->add(
			'ShippingFedExAccountNumber',
			'text',
			array(
				'label' => 'shipping.fedex_shipping_account_number',
				'value' => $data['ShippingFedExAccountNumber'],
				'placeholder' => 'shipping.fedex_shipping_account_number',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'note' => trans('shipping.your_primary_fedex_account_number'),
			)
		);

		$formBuilder->add(
			'ShippingFedExBillingAccountNumber',
			'text',
			array(
				'label' => 'shipping.fedex_billing_account_number',
				'value' => $data['ShippingFedExBillingAccountNumber'],
				'placeholder' => 'shipping.fedex_billing_account_number',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'note' => trans('shipping.fedex_billing_tooltip'),
			)
		);

		$formBuilder->add(
			'ShippingFedExFreightAccountNumber',
			'text',
			array(
				'label' => 'shipping.alternate_freight_account_number',
				'value' => $data['ShippingFedExFreightAccountNumber'],
				'placeholder' => 'shipping.alternate_freight_account_number',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'note' => trans('shipping.fedex_freight_tooltip'),
			)
		);

		$formBuilder->add(
			'ShippingFedExApiKey',
			'text',
			array(
				'label' => 'shipping.fedex_api_key',
				'value' => $data['ShippingFedExApiKey'],
				'placeholder' => 'shipping.fedex_api_key',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingFedExApiPassword',
			'text',
			array(
				'label' => 'shipping.fedex_api_password',
				'value' => $data['ShippingFedExApiPassword'],
				'placeholder' => 'shipping.fedex_api_password',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingFedExMeter',
			'text',
			array(
				'label' => 'shipping.fedex_meter',
				'value' => $data['ShippingFedExMeter'],
				'placeholder' => 'shipping.fedex_meter',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingFedExRateRequestType',
			'choice', array(
				'label' => 'shipping.fedex_rate_request_type',
				'value' => $data['ShippingFedExRateRequestType'],
				'options' => array(
					'LIST' => trans('shipping.list'),
					'ACCOUNT' => trans('shipping.account')
				),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'note' => trans('shipping.fedex_rate_tooltip')
			)
		);

		$formBuilder->add(
			'ShippingFedExRateRule',
			'choice',
			array(
				'label' => 'shipping.fedex_rate_rule',
				'value' => $data['ShippingFedExRateRule'],
				'options' => array(
					'max' => trans('shipping.return_max_rate'),
					'min' => trans('shipping.return_min_rate')
				),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'note' => trans('shipping.fedex_rate_tooltip')
			)
		);

		$freightClassOptions = array();
		foreach (Shipping_Fedex::$freightClasses as $fedExFreightClass)
		{
			$freightClassOptions[$fedExFreightClass] = $fedExFreightClass;
		}

		$formBuilder->add(
			'ShippingFedExFreightClass',
			'choice',
			array(
				'label' => 'shipping.fedex_freight_class',
				'value' => $data['ShippingFedExFreightClass'],
				'options' => $freightClassOptions,
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$freightPackagingOptions = array();
		foreach (Shipping_Fedex::$freightPackagingTypes as $fedExFreightPackaging)
		{
			$freightPackagingOptions[$fedExFreightPackaging] = $fedExFreightPackaging;
		}

		$formBuilder->add(
			'ShippingFedExFreightPackaging',
			'choice',
			array(
				'label' => 'shipping.fedex_freight_packaging',
				'value' => $data['ShippingFedExFreightPackaging'],
				'options' => $freightPackagingOptions,
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingFedExFreightPackagingWeight',
			'text',
			array(
				'label' => 'shipping.fedex_freight_packaging_weight',
				'value' => $data['ShippingFedExFreightPackagingWeight'],
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'note' => trans('shipping.fedex_freight_packaging_tooltip'
				)
			)
		);

		$formBuilder->add(
			'ShippingFedExDropOffType',
			'choice',
			array(
				'label' => 'shipping.fedex_drop_off_type',
				'value' => $data['ShippingFedExDropOffType'],
				'options' => array(
					'REGULAR_PICKUP' => 'Regular Pickup',
					'DROP_BOX' => 'Drop Box'
				),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getFedExSettingsForm($data)
	{
		return $this->getFedExSettingsFormBuilder($data)->getForm();
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getFedExDescriptionFormBuilder()
	{
		$formBuilder = new core_Form_FormBuilder('fedex-description');

		$formBuilder->add(
			'ShippingFedexDescription',
			'description',
			array(
				'value' => '<span class="shipping-carrier shipping-carrier-fedex shipping-carrier-modal"></span>' . trans('shipping.fedex_tooltip') . ' <a target="_blank" href="http://www.cartmanual.com/url/fedex">' . trans('shipping.fedex_link') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @return core_Form_Form
	 */
	public function getFedExDescriptionForm()
	{
		return $this->getFedExDescriptionFormBuilder()->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getCanadaPostSettingsFormBuilder($data)
	{
		/**
		 * Canada Post Settings group
		 */
		$formBuilder = $this->canadaPostSettingsGroup();

		$formBuilder->add(
			'ShippingCPMerchantID',
			'text',
			array(
				'label' => 'shipping.canada_post_merchant_id',
				'value' => $data['ShippingCPMerchantID'],
				'placeholder' => 'shipping.canada_post_merchant_id',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingCPUrlRating',
			'text',
			array(
				'label' => 'shipping.server_url_for_canada_post_rates',
				'value' => $data['ShippingCPUrlRating'],
				'placeholder' => 'shipping.server_url_for_canada_post_rates',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingCPDeliveryDate',
			'choice',
			array(
				'label' => 'shipping.show_delivery_date_with_method_name',
				'value' => $data['ShippingCPDeliveryDate'],
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'options' => array(
					'yes' => trans('common.yes'),
					'no' => trans('common.no')
				)
			)
		);

		$formBuilder->add(
			'ShippingCPTAT',
			'text',
			array(
				'label' => 'shipping.turn_around_time',
				'value' => $data['ShippingCPTAT'],
				'placeholder' => 'shipping.turn_around_time',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12'
			)
		);

		$formBuilder->add(
			'ShippingCPItemsValue',
			'choice',
				array(
				'label' => 'shipping.send_subtotal_for_insurance_calculation',
				'value' => $data['ShippingCPItemsValue'],
				'options' => array(
					'yes' => trans('common.yes'),
					'no' => trans('common.no')
				),
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getCanadaPostSettingsForm($data)
	{
		return $this->getCanadaPostSettingsFormBuilder($data)->getForm();
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getCanadaPostDescriptionFormBuilder()
	{
		$formBuilder = new core_Form_FormBuilder('canada-post-description');

		$formBuilder->add(
			'ShippingCPDescription',
			'description',
			array(
				'value' => '<span class="shipping-carrier shipping-carrier-canada-post shipping-carrier-modal"></span>' . trans('shipping.ship_using_canada_post') . ' <a target="_blank" href="https://www.canadapost.ca/cpo/mc/business/productsservices/developers/services/gettingstarted.jsf">' . trans('shipping.click_here_to_learn_more') . '</a>',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @return core_Form_Form
	 */
	public function getCanadaPostDescriptionForm()
	{
		return $this->getCanadaPostDescriptionFormBuilder()->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getVipParcelSettingsFormBuilder($data)
	{
		/**
		 * VIP Parcel Settings group
		 */
		$formBuilder = $this->vipParcelSettingsGroup();

		$formBuilder->add(
			'ShippingVIPparcelauthToken',
			'text',
			array(
				'label' => 'shipping.vparcel_authToken',
				'value' => $data['ShippingVIPparcelauthToken'],
				'placeholder' => 'shipping.vparcel_authToken',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'ShippingVIPparcelTestmode',
			'choice',
			array(
				'label' => 'shipping.vparcel_testmode',
				'value' => $data['ShippingVIPparcelTestmode'],
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'options' => array(
					'yes' => trans('common.yes'),
					'no' => trans('common.no'),
				),
			)
		);

		$formBuilder->add(
			'ShippingVIPparcelService',
			'choice',
			array(
				'label' => 'shipping.vparcel_service',
				'value' => $data['ShippingVIPparcelService'],
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'options' => array(
					'' => 'Select...',
					'CertifiedMail' => 'Certified Mail',
					'DeliveryConfirmation' => 'Delivery Confirmation',
					'SignatureConfirmation' => 'Signature Confirmation'
				)
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getVipParcelSettingsForm($data)
	{
		return $this->getVipParcelSettingsFormBuilder($data)->getForm();
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getVipParcelDescriptionFormBuilder()
	{
		$formBuilder = new core_Form_FormBuilder('vip-parcel-description');

		return $formBuilder;
	}

	/**
	 * @return core_Form_Form
	 */
	public function getVipParcelDescriptionForm()
	{
		return $this->getVipParcelDescriptionFormBuilder()->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getTaxesFormBuilder($data)
	{
		/**
		 * Taxes group
		 */
		$formBuilder = $this->getTaxesGroup();

		$formBuilder->add(
			'rate_description',
			'text',
			array(
				'label' => 'quick_start_guide.step_four.give_tax_rate_name',
				'placeholder' => 'quick_start_guide.step_four.give_tax_rate_name',
				'value' => $data['rate_description'],
				'required' => true,
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'attr' => array(
					'maxlength' => '255'
				)
			)
		);

		$classes = array();
		if ($data['taxes_classes'])
		{
			foreach ($data['taxes_classes'] as $k => $v) {
				$classes[] =new core_Form_Option($v['class_id'], $v['class_name']);
			}
		}

		$formBuilder->add(
			'class_id', 'choice',
			array(
				'label' => 'quick_start_guide.step_four.tax_class',
				'value' => $data['class_id'],
				'options' =>  $classes,
				'wrapperClass' => 'hidden col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		$formBuilder->add(
			'coid',
			'choice',
			array(
				'label' => 'quick_start_guide.step_four.country',
				'value' => $data['coid'],
				'required' => true,
				'placeholder' => 'quick_start_guide.step_four.country',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'attr' => array(
					'data-value' => $data['coid'],
					'class' => 'input-address-country all-countries-value'
				),
			)
		);

		$formBuilder->add(
			'stid',
			'choice',
			array(
				'label' => 'quick_start_guide.step_four.state_province',
				'value' => $data['stid'],
				'required' => true,
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
				'attr' => array(
					'data-value' => $data['stid'],
					'class' => 'input-address-state all-states-value'
				),
			)
		);

		$formBuilder->add(
			'tax_rate',
			'numeric',
			array(
				'label' => 'quick_start_guide.step_four.tax_rate_percentage',
				'placeholder' => 'quick_start_guide.step_four.tax_rate_percentage',
				'required' => true,
				'value' => $data['tax_rate'],
				'note' => 'Enter in the tax rate that would be added to the other subtotal if the order is a taxable order. For example, entering in 4.3 would result in a 4.3% of the order subtotal added to the order to accommodate for taxes. Taxes rates vary by location so be sure to check your tax rate. Leave blank if you don\'t know your tax rate.',
				'wrapperClass' => 'col-xs-12 col-sm-12 col-md-12 col-lg-12',
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getTaxesForm($data)
	{
		return $this->getTaxesFormBuilder($data)->getForm();
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getGeneralInformationGroup()
	{
		return $this->generalInformationGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getContactlnformationGroup()
	{
		return $this->contactlnformationGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getIndustrySpecificsGroup()
	{
		return $this->industrySpecificsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getLogoGroup()
	{
		return $this->logoGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getSocialMediaAccountsGroup()
	{
		return $this->socialMediaAccountsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getSharingSettingsGroup()
	{
		return $this->sharingSettingsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getSiteInformationGroup()
	{
		return $this->siteInformationGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getSeoSettingsGroup()
	{
		return $this->seoSettingsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getTaxesGroup()
	{
		return $this->taxesGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function upsSettingsGroup()
	{
		return $this->upsSettingsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function uspsSettingsGroup()
	{
		return $this->uspsSettingsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function fedExSettingsGroup()
	{
		return $this->fedExSettingsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function canadaPostSettingsGroup()
	{
		return $this->canadaPostSettingsGroup;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function vipParcelSettingsGroup()
	{
		return $this->vipParcelSettingsGroup;
	}
}
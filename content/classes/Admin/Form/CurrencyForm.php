<?php
/**
 * Class Admin_Form_CurrencyForm
 */
class Admin_Form_CurrencyForm
{
	/**
	 * @param $formData
	 * @param $mode
	 * @param $currencyType
	 * @param null $id
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $currencyType, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'Currency Details'));

		$formBuilder->add($basicGroup);
		
		if ($mode == 'add' && $currencyType == 'default')
		{
			$currency_list = $formData['default_currency_list'];
		
			$currency_list_form = array();
			foreach ($currency_list as $key=>$val)
			{
				$currency_list_form[] = new core_Form_Option($key,$key . ' - ' . $val[0]);
			}

			$basicGroup->add(
				'currency_list', 'choice',
				array(
					'label' => 'currencies.select_currency', 'value' => $formData['decimal_places'], 'options' => $currency_list_form,
					'note' => 'currencies.notes.select_currency', 'placeholder' => 'currencies.select_currency'
				)
			);
			
			$basicGroup->add('exchange_rate', 'numeric', array('label' => 'currencies.rate', 'placeholder' => 'currencies.rate', 'value' => $formData['exchange_rate'], 'required' => true, 'wrapperClass' => 'clear-both'));
		}
		else
		{
			$basicGroup->add('code', 'text', array('label' => 'currencies.code', 'placeholder' => 'currencies.code', 'value' => $formData['code'], 'required' => true, 'attr' => array('maxlength' => '100')));
			$basicGroup->add('title', 'text', array('label' => 'currencies.title', 'placeholder' => 'currencies.title', 'value' => $formData['title'], 'required' => true, 'attr' => array('maxlength' => '100')));
			$basicGroup->add('exchange_rate', 'text', array('label' => 'currencies.rate', 'placeholder' => 'currencies.rate', 'value' => $formData['exchange_rate'], 'required' => true, 'attr' => array('maxlength' => '255')));

			//decimal_places
			$decimalPlacesoptions = array(
				new core_Form_Option('3', '3'),
				new core_Form_Option('2', '2'),
				new core_Form_Option('1', '1'),
				new core_Form_Option('0', '0'),
			);
				
			$basicGroup->add(
				'decimal_places', 'choice',
				array(
						'label' => 'currencies.decimals', 'value' => $formData['decimal_places'], 'options' => $decimalPlacesoptions,
						'note' => 'currencies.notes.decimals'
				)
			);
			
			$basicGroup->add('symbol_left', 'text', array('label' => 'currencies.symbol_left', 'value' => $formData['symbol_left'], 'attr' => array('maxlength' => '100')));
			
			$basicGroup->add('symbol_right', 'text', array('label' => 'currencies.symbol_right', 'value' => $formData['symbol_right'], 'attr' => array('maxlength' => '100')));
		}
		
		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @param $mode
	 * @param $currencyType
	 * @param null $id
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $currencyType, $id = null)
	{
		return $this->getBuilder($formData, $mode, $currencyType, $id)->getForm();
	}
}
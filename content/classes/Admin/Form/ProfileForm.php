<?php
/**
 * Class Admin_Form_ProfileForm
 */
class Admin_Form_ProfileForm
{
	const MODE_INSERT = 1;
	const MODE_UPDATE = 2;
	const MODE_PROFILE = 3;

	/**
	 * Get admin profile form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param bool $forcePassword
	 * @param bool $forceSecurityQuestion
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $forcePassword = false, $forceSecurityQuestion = false, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$generalFormBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic properties group
		 */
		$basicPropertiesGroup = new core_Form_FormBuilder('basic', array('label' => 'admin.profile.basic_info', 'class'=>'ic-icon', 'first'=>true));
		$generalFormBuilder->add($basicPropertiesGroup);

		$basicPropertiesGroup->add('fname', 'text', array('label' => 'admin.profile.first_name', 'required' => true, 'value' => $formData['fname'], 'placeholder' => 'admin.profile.first_name'));
		$basicPropertiesGroup->add('lname', 'text', array('label' => 'admin.profile.last_name', 'required' => true, 'value' => $formData['lname'], 'placeholder' => 'admin.profile.last_name'));
		$basicPropertiesGroup->add('username', 'text', array('label' => 'admin.profile.username', 'readonly' => $mode != self::MODE_INSERT, 'value' => $formData['username'], 'placeholder' => 'admin.profile.username', 'required' => true));
		$basicPropertiesGroup->add('email', 'text', array('label' => 'admin.profile.email', 'required' => true, 'value' => $formData['email'], 'placeholder' => 'admin.profile.email'));
		$basicPropertiesGroup->add('password', 'password', array('label' => 'admin.profile.password', 'value' => '', 'placeholder' => 'admin.profile.password', 'required' => $mode == self::MODE_INSERT || $forcePassword, 'note' => 'admin.profile.password_hint', 'wrapperClass' => 'col-sm-12 col-lg-6 col-xs-12', 'meter' => true));
		$basicPropertiesGroup->add('password2', 'password', array('label' => 'admin.profile.password2', 'value' => '', 'placeholder' => 'admin.profile.password2', 'required' => $mode == self::MODE_INSERT || $forcePassword, 'confirmationFlag' => true, 'wrapperClass' => 'col-sm-12 col-lg-6 col-xs-12'));

		$notification_options = array(
			new core_Form_Option('invoice', 'admin.profile.notification_options.invoice'),
			new core_Form_Option('outofstock', 'admin.profile.notification_options.outofstock'),
			new core_Form_Option('security', 'admin.profile.notification_options.security_audit'),
			new core_Form_Option('signup', 'admin.profile.notification_options.signup'),
			new core_Form_Option('notactive', 'admin.profile.notification_options.notactive'),
			new core_Form_Option('product_review', 'admin.profile.notification_options.product_review'),
			new core_Form_Option('custom_forms', 'admin.profile.notification_options.custom_forms')
		);

		if (defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING)
		{
			$notification_options[] = new core_Form_Option('recurring', 'admin.profile.notification_options.recurring');
		}

		if ($mode == self::MODE_PROFILE)
		{
			$security_question_options = array(
				new core_Form_Option('', 'admin.profile.security_question_not_selected'),
				new core_Form_Option('1', 'admin.profile.security_question_1'),
				new core_Form_Option('2', 'admin.profile.security_question_2'),
				new core_Form_Option('3', 'admin.profile.security_question_3'),
				new core_Form_Option('4', 'admin.profile.security_question_4'),
				new core_Form_Option('5', 'admin.profile.security_question_5'),
				new core_Form_Option('6', 'admin.profile.security_question_6'),
				new core_Form_Option('7', 'admin.profile.security_question_7'),
			);
			$basicPropertiesGroup->add('security_question_id', 'choice', array('label' => 'admin.profile.security_question', 'attr' => array('class' => 'large'), 'required' => $forceSecurityQuestion, 'value' => $formData['security_question_id'], 'multiple' => false, 'expanded' => false, 'options' => $security_question_options));
			$basicPropertiesGroup->add('security_question_answer', 'text', array('label' => 'admin.profile.security_question_answer', 'attr' => array('class' => 'large'), 'required' => $forceSecurityQuestion, 'value' => ''));

			if (_ACCESS_INTUIT_ANYWHERE)
			{
				if ($formData['openid'] != '')
				{
					$qb_link = $translator->trans('admin.profile.qb_account_linked');
				}
				else
				{
					$qb_link = $translator->trans('admin.profile.qb_account_unlinked');
				}

				$basicPropertiesGroup->add('qb_account', 'static', array('label' => 'admin.profile.qb_account', 'value' => $qb_link, 'note' => 'admin.profile.notes.qb_account'));
			}
		}
		else
		{
			$basicPropertiesGroup->add('force_password_change', 'checkbox', array('label' => 'admin.profile.force_password_change', 'value' => 1, 'current_value' => $formData['force_password_change']));
		}

		if ($mode != self::MODE_PROFILE)
		{
			/**
			 * Account lock properties group
			 */
			$accountLockPropertiesGroup = new core_Form_FormBuilder('account_lock_grp', array('label'=>'admin.profile.account_lock_grp', 'class'=>'ic-icon'));
			$generalFormBuilder->add($accountLockPropertiesGroup);

			if ($formData['active'] == 'Block') $formData['active'] = 'No';
			$accountLockPropertiesGroup->add('active', 'choice', array('label' => 'admin.profile.active', 'options' => array('Yes' => 'admin.profile.active_options.yes', 'No' => 'admin.profile.active_options.no'), 'value' => $formData['active'], 'attr' => array('class' => 'short')));
		}

		$basicPropertiesGroup->add(
			'avatar', 'image',
			array(
				'label' => 'admin.profile.avatar',
				'value' => $formData['avatar'],
				'imageDeleteUrl' =>  ((($mode == self::MODE_UPDATE || $mode == self::MODE_PROFILE) && !is_null($id)) ? 'admin.php?p=profile&mode=delete-image&id='.$id : null),
				'wrapperClass' => 'clear-both',
			)
		);

		$basicPropertiesGroup->add('receive_notification[]', 'choice', array('label'=>'admin.profile.email_notifications', 'value' => $formData['receive_notifications'], 'multiple' => true, 'expanded' => true, 'options' => $notification_options, 'wrapperClass' => 'clear-both'));

		$password_change = 'pwdchange30';

		if (in_array('pwdchange60', $formData['receive_notifications']))
		{
			$password_change = 'pwdchange60';
		}
		else if (in_array('pwdchange90', $formData['receive_notifications']))
		{
			$password_change = 'pwdchange90';
		}
		else if (in_array('pwdchangenever', $formData['receive_notifications']))
		{
			$password_change = 'pwdchangenever';
		}

		$password_change_options = array(
			new core_Form_Option('pwdchange30', 'admin.profile.pwdchange_options.pwdchange30'),
			new core_Form_Option('pwdchange60', 'admin.profile.pwdchange_options.pwdchange60'),
			new core_Form_Option('pwdchange90', 'admin.profile.pwdchange_options.pwdchange90'),
			new core_Form_Option('pwdchangenever', 'admin.profile.pwdchange_options.pwdchangenever')
		);
		$basicPropertiesGroup->add('pwdchange', 'choice', array('label'=>'admin.profile.pwdchange', 'value' => $password_change, 'multiple' => false, 'expanded' => true, 'options' => $password_change_options));

		/**
		 * Expiration properties group
		 */
		$expirationPropertiesGroup = new core_Form_FormBuilder('expiration', array('label'=>'admin.profile.expiration', 'class'=>'ic-icon', 'collapsible' => true));
		$generalFormBuilder->add($expirationPropertiesGroup);

		if ($mode == self::MODE_PROFILE)
		{
			$expirationPropertiesGroup->add('expires', 'static', array('label' => 'admin.profile.expires', 'value' => $formData['expires'] == '0' ? 'No' : 'Yes'));
			if ($formData['expires'] == '1')
			{
				$expirationPropertiesGroup->add('expires_date', 'static', array('label' => 'admin.profile.expires_date', 'value' => date('F j, Y, g:ia', strtotime($formData["expiration_date"]))));
			}
		}
		else
		{
			$expirationPropertiesGroup->add('expires', 'choice', array('label' => 'admin.profile.expires', 'value' => $formData['expires'], 'options' => array('0' => 'common.no', '1' => 'common.yes'), 'attr' => array('class' => 'short')));

			$expirationPropertiesGroup->add('expiration_date', 'datetime', array('label' => 'admin.profile.expiration_date', 'value' => $formData['expiration_date']));
		}

		$privileges_options = array(
			//new core_Form_Option('categories', 'admin.profile.privileges_options.categories'),
			new core_Form_Option('products', 'admin.profile.privileges_options.products'),
			new core_Form_Option('users', 'admin.profile.privileges_options.users'),
			new core_Form_Option('orders', 'admin.profile.privileges_options.orders'),
			//new core_Form_Option('emails', 'admin.profile.privileges_options.emails'),
			//new core_Form_Option('admins', 'admin.profile.privileges_options.admins'),
			new core_Form_Option('global', 'admin.profile.privileges_options.global'),
			//new core_Form_Option('business', 'admin.profile.privileges_options.business'),
			new core_Form_Option('content', 'admin.profile.privileges_options.content'),
			//new core_Form_Option('export', 'admin.profile.privileges_options.export'),
			new core_Form_Option('stats', 'admin.profile.privileges_options.stats'),
			new core_Form_Option('marketing', 'admin.profile.privileges_options.marketing'),
			new core_Form_Option('ccs', 'admin.profile.privileges_options.ccs'),
		);

		if (defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING)
		{
			$privileges_options[] = new core_Form_Option('recurring', 'admin.profile.privileges_options.recurring');
		}

		$privileges_options[] = new core_Form_Option('all', 'admin.profile.privileges_options.all');

		/**
		 * Privileges properties group
		 */
		$privilegesPropertiesGroup = new core_Form_FormBuilder('privileges_group', array('label'=>'admin.profile.privileges_group', 'class'=>'ic-icon'));
		$generalFormBuilder->add($privilegesPropertiesGroup);

		$privilegesPropertiesGroup->add('rights[]', 'choice', array('label' => '', 'value' => $formData['rights'], 'multiple' => true, 'expanded' => true, 'grid' => true, 'readonly' => $mode == self::MODE_PROFILE, 'options' => $privileges_options));

		return $generalFormBuilder;
	}

	/**
	 * Get admin profile form
	 *
	 * @param $formData
	 * @param $mode
	 * @param bool $forcePassword
	 * @param bool $forceSecurityQuestion
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getForm($formData, $mode, $forcePassword = false, $forceSecurityQuestion = false, $id = null)
	{
		return $this->getBuilder($formData, $mode, $forcePassword, $forceSecurityQuestion, $id)->getForm();
	}
}
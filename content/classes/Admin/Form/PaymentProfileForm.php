<?php

/**
 * Class Admin_Form_PaymentProfileForm
 */
class Admin_Form_PaymentProfileForm
{
	protected $PPData = null;
	protected $collapsibleAttr = false;
	protected $current_uid = null;

	/**
	 * Class constructor
	 *
	 * @param PaymentProfiles_DataAccess_PaymentProfileRepository $PPData
	 */
	public function __construct(PaymentProfiles_DataAccess_PaymentProfileRepository $PPData)
	{
		$this->PPData = $PPData;
	}

	/**
	 * @param $val
	 */
	public function setCollapsibleAttr($val)
	{
		$this->collapsibleAttr = $val;
	}

	/**
	 * @param $val
	 */
	public function setCustomer($val)
	{
		$this->current_uid = $val;
	}

	/**
	 * @param PaymentProfiles_DataAccess_PaymentProfileRepository $PPData
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder(PaymentProfiles_DataAccess_PaymentProfileRepository $PPData)
	{
		$formBuilder = new core_Form_FormBuilder('paymentprofiles', array('label' => 'Payment Profiles', 'collapsible' => $this->collapsibleAttr));

		$paymentProfiles = PaymentProfiles_View_PaymentProfileView::getPaymentProfilesView($PPData->getByUserId($this->current_uid));

		if ($paymentProfiles)
		{
			$formBuilder->add('button_profile_add', 'submit', array(
				'label' => '',
				'value' => 'Add payment card',
				'cssClass' => 'btn',
			));

			$ind = 1;

			foreach ($paymentProfiles as $paymentProfile)
			{
				$val = $name = ($paymentProfile->name != '') ? $paymentProfile->name : '';
				$val .= '<span class="payment-profile-primary-label" ' . (!$paymentProfile->isPrimary ? ' style="display:none;" ' : '') . '><i>(primary)</i></span><br>';
				$val .= $paymentProfile->billingData->ccNumber . ' - ';
				$val .= $paymentProfile->billingData->ccExpirationMonth . '/' . $paymentProfile->billingData->ccExpirationYear;
				$val .= "<br>";
				$val .= $paymentProfile->billingData->addressLine1;
				$val .= ($paymentProfile->billingData->addressLine2 != '' ? $paymentProfile->billingData->addressLine2 : '') . '<br>';
				$val .= $paymentProfile->billingData->city . ',' . $paymentProfile->billingData->stateName . ' ' . $paymentProfile->billingData->zip . '<br>';
				$val .= $paymentProfile->billingData->countryName;

				if ($paymentProfile->billingData->phone != '')
				{
					$val .= '<br>Phone: ' . $paymentProfile->billingData->phone;
				}

				$formBuilder->add('profile_' . $ind, 'static', array(
					'label' => '<a href="#" data-target="#modal-paymentprofile" data-toggle="modal" onclick="return EditPaymentProfile(' . $paymentProfile->id . ',' . $this->current_uid . ');"><strong>' . $name . '</strong></a>',
					'value' => $val
				));

				$ind++;
			}
		}
		else
		{
			$formBuilder->add('no_profiles', 'static', array('label' => '', 'value' => 'payment_profiles.no_payment_profiles'));
		}

		return $formBuilder;
	}

	/**
	 *
	 * @return core_Form_Form
	 */
	public function getForm()
	{
		return $this->getBuilder($this->PPData)->getForm();
	}
}
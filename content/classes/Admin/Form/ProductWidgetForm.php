<?php

/**
 * Class Admin_Form_ProductWidgetForm
 */
class Admin_Form_ProductWidgetForm
{
	/**
	 * @param $productId
	 * @param $campaignsOptions
	 * @param $defaultImageType
	 * @param $defaultStyle
	 * @param $defaultType
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($productId, $campaignsOptions, $defaultImageType, $defaultStyle, $defaultType)
	{
		$formBuilder = new core_Form_FormBuilder('widgets', array('label' => 'widgets.widgets_for_this_product', 'class' => 'ic-raster', 'first' => true));
		$formBuilder->add('wg_pid', 'hidden', array('value' => $productId));

		if (count($campaignsOptions) > 0)
		{
			$formBuilder->add('wg_campaign_id', 'choice', array('label' => 'widgets.campaign', 'options' => $campaignsOptions, 'empty_value' => array('0' => 'widgets.no_campaign')));
		}

		$formBuilder->add(
			'widgets_type', 'choice',
			array(
				'label' => 'widgets.product_url_type',
				'value' => $defaultType,
				'options' => array(
					'page' => 'widgets.widgets_type_options.page',
					'cart' => 'widgets.widgets_type_options.cart',
					'checkout' => 'widgets.widgets_type_options.checkout',
				),
			)
		);
		$formBuilder->add(
			'widgets_image_type', 'choice',
			array(
				'label' => 'widgets.product_image_type',
				'value' => $defaultImageType,
				'options' => array(
					'product' => 'widgets.widgets_image_type_options.product',
					'thumb' => 'widgets.widgets_image_type_options.thumb',
					'none' => 'widgets.widgets_image_type_options.none',
				),
			)
		);
		$formBuilder->add(
			'widgets_style', 'choice',
			array(
				'label' => 'widgets.product_style',
				'value' => $defaultStyle,
				'options' => array(
					'1' => 'widgets.widgets_style_options.1',
					'2' => 'widgets.widgets_style_options.2',
					'3' => 'widgets.widgets_style_options.3',
				),
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $productId
	 * @param $campaignsOptions
	 * @param $defaultImageType
	 * @param $defaultStyle
	 * @param $defaultType
	 *
	 * @return core_Form_Form
	 */
	public function getForm($productId, $campaignsOptions, $defaultImageType, $defaultStyle, $defaultType)
	{
		return $this->getFormBuilder($productId, $campaignsOptions, $defaultImageType, $defaultStyle, $defaultType)->getForm();
	}
}
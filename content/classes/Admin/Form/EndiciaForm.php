<?php

/**
 * Class Admin_Form_EndiciaForm
 */
class Admin_Form_EndiciaForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getEndiciaActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('EndiciaShippingLabelsAccountID', 'text', array('label' => 'endicia.account_id', 'required' => true, 'value' => $data['EndiciaShippingLabelsAccountID'], 'placeholder' => 'endicia.account_id'));
        $settingsGroup->add('EndiciaShippingLabelsPassword', 'password', array('label' => 'endicia.passphrase', 'required' => true, 'value' => $data['EndiciaShippingLabelsPassword'], 'placeholder' => 'endicia.passphrase'));
        if (defined('DEVMODE') && DEVMODE)
            $settingsGroup->add('EndiciaShippingLabelsTestMode', 'checkbox', array('label' => 'endicia.test_mode', 'value' => 'Yes', 'current_value' => $data['EndiciaShippingLabelsTestMode']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getEndiciaActivateForm($data)
    {
        return $this->getEndiciaActivateBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('EndiciaShippingLabelsActive', 'hidden', array('value' => $data['EndiciaShippingLabelsActive']));
        $settingsGroup->add('EndiciaShippingLabelsAccountID', 'text', array('label' => 'endicia.account_id', 'required' => true, 'value' => $data['EndiciaShippingLabelsAccountID'], 'placeholder' => 'endicia.account_id'));
        $settingsGroup->add('EndiciaShippingLabelsPassword', 'password', array('label' => 'endicia.passphrase', 'required' => true, 'value' => $data['EndiciaShippingLabelsPassword'], 'placeholder' => 'endicia.passphrase'));
        if (defined('DEVMODE') && DEVMODE)
            $settingsGroup->add('EndiciaShippingLabelsTestMode', 'checkbox', array('label' => 'endicia.test_mode', 'value' => 'Yes', 'current_value' => $data['EndiciaShippingLabelsTestMode']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getEndiciaRefundBuilder()
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('pic_number', 'text', array('label' => 'endicia.pic_number', 'required' => true, 'value' => '', 'placeholder' => 'endicia.pic_number', 'wrapperClass' => 'col-sm-12 col-xs-12'));

        return $formBuilder;
    }

    /**
     * @return mixed
     */
    public function getEndiciaRefundForm()
    {
        return $this->getEndiciaRefundBuilder()->getForm();
    }

    /**
 * @return core_Form_FormBuilder
 * @throws Exception
 */
    public function getEndiciaAddFundsBuilder()
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('addfunds_passphrase', 'password', array('label' => 'endicia.your_passphrase', 'required' => true, 'value' => '', 'placeholder' => 'endicia.your_passphrase'));
        $settingsGroup->add('addfunds_amount', 'text', array('label' => 'endicia.add_amount', 'required' => true, 'value' => '', 'placeholder' => 'endicia.add_amount'));


        return $formBuilder;
    }

    /**
     * @return mixed
     */
    public function getEndiciaAddFundsForm()
    {
        return $this->getEndiciaAddFundsBuilder()->getForm();
    }

    /**
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getEndiciaChangePasswordBuilder()
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('change_existing_passphrase', 'password', array('label' => 'endicia.current_passphrase', 'required' => true, 'value' => '', 'placeholder' => 'endicia.current_passphrase', 'wrapperClass' => 'col-sm-12 col-xs-12'));
        $settingsGroup->add('change_new_passphrase', 'password', array('label' => 'endicia.new_passphrase', 'required' => true, 'value' => '', 'placeholder' => 'endicia.new_passphrase', 'wrapperClass' => 'col-sm-12 col-xs-12 clear-both'));
        $settingsGroup->add('change_new_passphrase2', 'password', array('label' => 'endicia.confirm_new_passphrase', 'required' => true, 'value' => '', 'placeholder' => 'endicia.confirm_new_passphrase', 'wrapperClass' => 'col-sm-12 col-xs-12 clear-both'));

        return $formBuilder;
    }

    /**
     * @return mixed
     */
    public function getEndiciaChangePasswordForm()
    {
        return $this->getEndiciaChangePasswordBuilder()->getForm();
    }
}
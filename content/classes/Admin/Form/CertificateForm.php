<?php

/**
 * Class Admin_Form_CertificateForm
 */
class Admin_Form_CertificateForm
{
	/**
	 * Get certificate form builder
	 *
	 * @param $data
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$basicGroup->add('dn[ccs_username]', 'text', array('label' => 'credit_card_storage.username', 'placeholder' => 'credit_card_storage.username', 'value' => '', 'required' => true));
		$basicGroup->add('dn[ccs_password]', 'password', array('label' => 'credit_card_storage.password', 'placeholder' => 'credit_card_storage.password', 'value' => '', 'required' => true, 'wrapperClass' => 'clear-both'));
		$basicGroup->add('dn[ccs_password2]', 'password', array('label' => 'credit_card_storage.password2', 'placeholder' => 'credit_card_storage.password2', 'value' => '', 'required' => true, 'wrapperClass' => 'clear-both'));

		return $formBuilder;
	}

	/**
	 * Get certificate form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
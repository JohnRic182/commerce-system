<?php

/**
 * Class Admin_Form_FacebookLoginForm
 */
class Admin_Form_FacebookLoginForm
{
    /**
 * @param $data
 * @return core_Form_FormBuilder
 * @throws Exception
 */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsForm = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsForm);

        $settingsForm->add('facebookAppId', 'text', array('label' => 'apps.facebook.app_id', 'placeholder' => 'apps.facebook.app_id', 'required' => true, 'value' => $data['facebookAppId']));
        $settingsForm->add('facebookAppSecret', 'password', array('label' => 'apps.facebook.app_secret', 'placeholder' => 'apps.facebook.app_secret', 'required' => true, 'value' => $data['facebookAppSecret']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

}
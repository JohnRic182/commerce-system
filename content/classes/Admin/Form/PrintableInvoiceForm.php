<?php

/**
 * Class Admin_Form_PrintableInvoiceForm
 */
class Admin_Form_PrintableInvoiceForm
{
    /**
     * Holds our basic form group
     * @var core_Form_FormBuilder
     */
    private $basicGroup;

    /**
     * Admin_Form_AdminLogForm constructor.
     */
    public function __construct()
    {
        $this->basicGroup = new core_Form_FormBuilder('basic');
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        $basicGroup = $this->getBasicGroup();
        $formBuilder->add($basicGroup);

        $basicGroup->add('CompanyWebsite', 'text', array(
            'label' => 'invoice.company_website',
            'value' => $data['CompanyWebsite'],
            'required' => false,
        ));
        $basicGroup->add('CompanyEmail', 'text', array(
            'label' => 'invoice.company_email',
            'value' => $data['CompanyEmail'],
            'required' => false,
        ));
        $basicGroup->add('PrintableInvoiceCompanyLogoAlignment', 'choice', array(
            'label' => 'invoice.logo_alignment',
            'value' => $data['PrintableInvoiceCompanyLogoAlignment'],
            'required' => false,
            'options' => array(
                'NO' => 'invoice.hide',
                'LEFT' => 'invoice.left',
                'CENTER' => 'invoice.center',
                'RIGHT' => 'invoice.right',
            )
        ));
        $basicGroup->add('PrintableInvoiceCompanyName', 'checkbox', array(
            'label' => 'invoice.display_company_name',
            'value' => 'YES',
            'current_value' => $data['PrintableInvoiceCompanyName'],
            'wrapperClass' => 'clear-both',
            'required' => false,
        ));
        $basicGroup->add('PrintableInvoiceCompanyFax', 'checkbox', array(
            'label' => 'invoice.display_company_fax',
            'value' => 'YES',
            'current_value' => $data['PrintableInvoiceCompanyFax'],
            'wrapperClass' => 'clear-both',
            'required' => false,
        ));
        $basicGroup->add('PrintableInvoiceCompanyEmail', 'checkbox', array(
            'label' => 'invoice.display_company_email',
            'value' => 'YES',
            'current_value' => $data['PrintableInvoiceCompanyEmail'],
            'wrapperClass' => 'clear-both',
            'required' => false,
        ));
        $basicGroup->add('PrintableInvoiceCompanyWeb', 'checkbox', array(
            'label' => 'invoice.display_company_website',
            'value' => 'YES',
            'current_value' => $data['PrintableInvoiceCompanyWeb'],
            'wrapperClass' => 'clear-both',
            'required' => false,
        ));
        $basicGroup->add('PrintableInvoiceCompanyAddress', 'checkbox', array(
            'label' => 'invoice.display_company_address',
            'value' => 'YES',
            'current_value' => $data['PrintableInvoiceCompanyAddress'],
            'wrapperClass' => 'clear-both',
            'required' => false,
        ));
        $basicGroup->add('PrintableInvoiceCompanyPhone', 'checkbox', array(
            'label' => 'invoice.display_company_phone',
            'value' => 'YES',
            'current_value' => $data['PrintableInvoiceCompanyPhone'],
            'wrapperClass' => 'clear-both',
            'required' => false,
        ));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @return mixed
     */
    public function getBasicGroup()
    {
        return $this->basicGroup;
    }
}
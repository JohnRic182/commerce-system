<?php
/**
 * Class Admin_Form_ProductReviewForm
 */
class Admin_Form_ProductReviewForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$request = Framework_Request::createFromGlobals();

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'marketing.review_details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('title', 'static', array('label' => 'marketing.product_title', 'value' => $formData['title'], 'attr' => array()));

		$basicGroup->add('review_title', 'static', array('label' => 'marketing.review_title', 'value' => $formData['review_title'], 'attr' => array()));

		$basicGroup->add('rating', 'static', array('label' => 'marketing.rating', 'value' => $formData['rating'], 'attr' => array()));

		$basicGroup->add('reviewer', 'static', array('label' => 'marketing.reviewer', 'value' => $formData['email'], 'attr' => array()));

		$basicGroup->add('review', 'static', array('label' => 'marketing.review', 'value' => htmlspecialchars(nl2br($formData['review_text'])), 'attr' => array()));

		$statusOption = array(
			new core_Form_Option('approved', trans('marketing.approved')),
			new core_Form_Option('pending', trans('marketing.pending')),
			new core_Form_Option('declined', trans('marketing.declined'))
		);

		$basicGroup->add(
			'status', 'choice',
			array(
				'label' => 'marketing.status', 'value' => $formData['status'], 'required' => true, 'options' => $statusOption
			)
		);
				
		return $formBuilder;
	}

	/**
	 * Get product review form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}

	/**
	 * Our product review search form
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getProductSearchReviewBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');

		$formBuilder->add('searchParams[search_str]', 'text', array('label' => trans('product.reviews.search_product_review_title'), 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-6'));
		$formBuilder->add('searchParams[reviewer]', 'text', array('label' => trans('product.reviews.reviews_list_table.review'), 'value' => $data['reviewer'], 'note' => 'Searches the User Email or Last Name', 'wrapperClass' => 'col-sm-12 col-md-3'));
		$formBuilder->add('searchParams[status]', 'choice',	array('label' => trans('product.reviews.reviews_list_table.status'), 'value' => $data['status'], 'options' => array('any' => 'Any', 'approved' => 'Approved', 'pending' => 'Pending'), 'wrapperClass' => 'col-sm-12 col-md-3'));
		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));
		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getProductReviewSearchForm($data)
	{
		return $this->getProductSearchReviewBuilder($data)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_DriftMarketingForm
 */
class Admin_Form_DriftMarketingCampaignForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 * @param $timeframeOptions
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null, $timeframeOptions = null)
	{
		$translator = Framework_Translator::getInstance();

		$request = Framework_Request::createFromGlobals();

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'marketing.campaign_details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array('label' => 'marketing.title', 'value' => $formData['name'], 'placeholder' => 'marketing.title', 'required' => true, 'attr' => array('maxlength' => '45')));
		
		$opts = array();
		if (is_array($timeframeOptions))
		{
			foreach ($timeframeOptions as $key=>$value)
			{
				$opts[] = new core_Form_Option($key, $value);
			}
		}
		$basicGroup->add('timeframe', 'choice', array('label' => 'marketing.send_email_when_cart_is_abandoned', 'attr' => array('class' => 'large'), 'required' => true, 'value' => $formData['timeframe'], 'placeholder' => 'marketing.send_email_when_cart_is_abandoned', 'multiple' => false, 'expanded' => false, 'options' => $opts));

		$basicGroup->add('emailSubject', 'text', array('label' => 'marketing.subject_line_of_email', 'value' => $formData['emailSubject'], 'placeholder' => 'marketing.subject_line_of_email', 'required' => true, 'attr' => array('maxlength' => '50')));

		$basicGroup->add('htmlEmail', 'textarea', array('label' => 'marketing.email_content', 'wysiwyg' => true, 'required' => true, 'value' => $formData['htmlEmail'], 'attr' => array('rows' => '60')));

		return $formBuilder;
	}

	/**
	 * Get campaign form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 * @param array|null $timeframeOptions
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null, $timeframeOptions = null)
	{
		return $this->getBuilder($formData, $mode, $id, $timeframeOptions)->getForm();
	}
}
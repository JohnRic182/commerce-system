<?php
/**
 * Class Admin_Form_ProductsFamilyForm
 */
class Admin_Form_ProductsFamilyForm
{
    /**
     * Get manufacturer form builder
     *
     * @param $formData
     * @param $mode
     * @param $id
     *
     * @return core_Form_FormBuilder
     */
    public function getBuilder($formData, $mode, $id = null)
    {
        $translator = Framework_Translator::getInstance();

        $formBuilder = new core_Form_FormBuilder('general');

        /**
         * Basic group
         */
        $basicGroup = new core_Form_FormBuilder('basic', array('label'=>'marketing.group_details'));

        $formBuilder->add($basicGroup);

        $basicGroup->add('name', 'text', array('label' => 'marketing.title', 'value' => $formData['name'], 'wrapperClass' => 'col-sm-12 col-xs-12', 'placeholder' => 'marketing.title', 'required' => true, 'attr' => array('maxlength' => '100')));
        if($id)
        {
            $basicGroup->add('pf_id', 'hidden', array('label' => '', 'value' => $formData['pf_id']));
            $productsGroup = new core_Form_FormBuilder('products', array('label'=>'marketing.products'));

            $formBuilder->add($productsGroup);

            $category_options = $formData["categories"];
            $productsGroup->add(
                'categories', 'choice',
                array(
                    'label' => 'marketing.categories',
                    'options' => $category_options,
                    'note' => 'marketing.categories_tooltip',
                    'value'=> '',
                    'empty_value'=>array('0'=>trans('marketing.all_categories')),
                    'atrr' => array('id'=>'selectCategories'),
                )
            );

            $selectedProductsGroup = new core_Form_FormBuilder('selected_products', array('label'=>''));
            $selectedProductsGroup->add(
                'products', 'choice',
                array(
                    'label' => 'marketing.products_in_selected_categories', 'value' => '', 'options' => array(),
                    'note' => 'marketing.products_in_selected_categories_tooltip',
                    'multiple' => true,
                    'atrr' => array('id'=>'selectProducts'),
                )
            );
            $selectedProductsGroup->add(
                'selectedProducts', 'choice',
                array(
                    'label' => 'marketing.products_in_this_group', 'value' => '', 'required' => true, 'options' => array(),
                    'multiple' => true,
                    'atrr' => array('id'=>'selectedProducts'),
                )
            );

            $buttonsGroup = new core_Form_FormBuilder('buttons', array('label'=>''));
            $buttonsGroup->add('remove_add', 'static', array(
                'label'=>'',
                'value'=>'
					<button id="btnAddProduct" style="width:100px;">' . trans('common.add') . ' &gt;&gt;</button><br/><br/>
					<button id="btnRemoveProduct" style="width:100px;">&lt;&lt; ' . trans('common.remove') . '</button>'
            ));

            $formBuilder->add($selectedProductsGroup);
            $formBuilder->add($buttonsGroup);
        }


        return $formBuilder;
    }

    /**
     * Get manufacturer form
     *
     * @param $formData
     * @param $mode
     * @param int|null $id
     *
     * @return core_Form_Form
     */
    public function getForm($formData, $mode, $id = null)
    {
        return $this->getBuilder($formData, $mode, $id)->getForm();
    }
}
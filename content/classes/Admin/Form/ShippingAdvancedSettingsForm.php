<?php

/**
 * Class Admin_Form_ShippingAdvancedSettingsForm
 */
class Admin_Form_ShippingAdvancedSettingsForm
{
	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getOriginAddressForm($data)
	{
		$formBuilder = new core_Form_FormBuilder('shipping-form');

		$originAddressGroup = new core_Form_FormBuilder('basic');
		$formBuilder->add($originAddressGroup);

		$originAddressGroup->add('ShippingOriginName', 'text', array('label' => 'shipping.shipping_origin_name', 'value' => $data['ShippingOriginName'], 'required' => true, 'placeholder' => 'shipping.shipping_origin_name'));
		$originAddressGroup->add('ShippingOriginAddressLine1', 'text', array('label' => 'shipping.street_address', 'value' => $data['ShippingOriginAddressLine1'], 'required' => true, 'placeholder' => 'shipping.street_address'));
		$originAddressGroup->add('ShippingOriginAddressLine2', 'text', array('label' => 'shipping.street_address_line2', 'value' => $data['ShippingOriginAddressLine2'], 'placeholder' => 'shipping.street_address_line2'));
		$originAddressGroup->add('ShippingOriginCity', 'text', array('label' => 'shipping.city', 'value' => $data['ShippingOriginCity'], 'required' => true, 'placeholder' => 'shipping.city'));
		$originAddressGroup->add('ShippingOriginCountry', 'choice', array('label' => 'shipping.country', 'value' => $data['ShippingOriginCountry'], 'required' => true, 'attr' => array('data-value' => $data['ShippingOriginCountry'], 'class' => 'input-address-country')));
		$originAddressGroup->add('ShippingOriginState', 'choice', array('label' => 'shipping.state', 'value' => $data['ShippingOriginState'], 'required' => true, 'attr' => array('data-value' => $data['ShippingOriginState'], 'class' => 'input-address-state')));
		$originAddressGroup->add('ShippingOriginProvince', 'text', array('label' => 'shipping.province', 'value' => $data['ShippingOriginProvince'], 'required' => true, 'wrapperClass' => 'col-xs-12 col-sm-6 hidden', 'attr' => array('class' => 'input-address-province')));
		$originAddressGroup->add('ShippingOriginZip', 'text', array('label' => 'shipping.zip', 'attr' => array('class' => 'input-address-zip'), 'value' => $data['ShippingOriginZip'],	'required' => true, 'placeholder' => 'shipping.zip'));
		$originAddressGroup->add('ShippingOriginPhone', 'text', array('label' => 'shipping.phone', 'value' => $data['ShippingOriginPhone'], 'note' => 'Required for some USPS shipping methods. Please enter numbers only, no letters, dashes or any other characters', 'placeholder' => 'shipping.phone'));

		return $formBuilder->getForm();
	}

	/**
	 * @param DB $db
	 * @param $data
	 * @param $forceAddressValidation
	 * @return core_Form_Form
	 */
	public function getShippingSettingsForm(DB $db, $data, $forceAddressValidation)
	{
		$taxService = Tax_ProviderFactory::getTaxService();

		$formBuilder = new core_Form_FormBuilder('shipping-settings-form');

		$handlingGroup = new core_Form_FormBuilder('handling', array('label' => 'shipping.handling_settings', 'collapsible' => true));
		$formBuilder->add($handlingGroup);

		$handlingGroup->add('ShippingHandlingFee', 'text', array('label' => 'shipping.charge_perorder_handling_fee', 'value' => $data['ShippingHandlingFee'], 'placeholder' => 'shipping.charge_perorder_handling_fee', 'required' => true));

		$handlingGroup->add('ShippingHandlingFeeType', 'choice', array(
			'label' => 'shipping.handling_fee_type',
			'value' => $data['ShippingHandlingFeeType'],
			'options' => array(
				'percent' => '%',
				'amount' => trans('shipping.amount')
			)
		));

		$handlingGroup->add('ShippingHandlingFeeWhenFreeShipping', 'text', array('label' => 'shipping.charge_perorder_handling_fee_when_shipping_is_free', 'value' => $data['ShippingHandlingFeeWhenFreeShipping'], 'placeholder' => 'shipping.charge_perorder_handling_fee_when_shipping_is_free', 'required' => true));

		$handlingGroup->add('ShippingHandlingSeparated', 'checkbox', array(
			'label' => 'shipping.show_handling_separately',
			'value' => '1', 'current_value' => $data['ShippingHandlingSeparated'],
			'note' => 'shipping.if_handling_is_not_separated_then_shipping_tax_will_be_applied'
		));

		if ($taxService instanceof Tax_CustomProvider)
		{
			$handlingGroup->add('ShippingHandlingTaxable', 'checkbox', array(
				'label' => 'shipping.is_handling_fee_taxable',
				'value' => '1', 'current_value' => $data['ShippingHandlingTaxable'],
				'wrapperClass' => 'clear-both'
			));

			// TODO: optimize this

			$taxes = new Taxes($db);
			$taxesClasses = $taxes->getTaxClassesList();

			if ($taxesClasses && count($taxesClasses) > 0)
			{
				$taxClassesOptions = array();
				foreach ($taxesClasses as $taxClass) $taxClassesOptions[$taxClass['class_id']] = $taxClass['class_name'];
				$handlingGroup->add('ShippingHandlingTaxClassId', 'choice', array(
					'label' => 'shipping.handling_tax_class',
					'value' => $data['ShippingHandlingTaxClassId'],
					'options' => $taxClassesOptions
				));
			}
			else
			{
				$handlingGroup->add('ShippingHandlingTaxClassId', 'static', array('value' => 'shipping.no_tax_classes'));
			}
		}

		$miscGroup = new core_Form_FormBuilder('misc', array('label' => 'shipping.misc_settings', 'collapsible' => true));
		$formBuilder->add($miscGroup);

		$miscGroup->add('ShippingWithoutMethod', 'checkbox', array(
			'label' => 'shipping.allow_orders_to_be_completed_if_no_shipping_methods_are_available',
			'value' => 'YES', 'current_value' => $data['ShippingWithoutMethod']
		));


		$miscGroup->add('ShippingAllowGetQuote', 'checkbox', array(
			'label' => 'shipping.enable_shipping_quote_feature',
			'value' => 'YES', 'current_value' => $data['ShippingAllowGetQuote'],
			'note' => 'shipping.enable_shipping_quote_feature_tooltip'
		));

		$miscGroup->add('ShippingShowAlternativeOnFree', 'checkbox', array(
			'label' => 'shipping.display_alternative_shipping_methods_when_shipping_is_free',
			'value' => 'YES', 'current_value' => $data['ShippingShowAlternativeOnFree']
		));

		$miscGroup->add('ShippingAllowSeparateAddress', 'checkbox', array(
			'label' => 'shipping.allow_separate_billing_and_shipping_addresses',
			'value' => '1', 'current_value' => $data['ShippingAllowSeparateAddress']
		));

		if ($forceAddressValidation)
		{
			$miscGroup->add('EndiciaUspsAddressValidationDisabled', 'checkbox', array(
				'label' => 'shipping.use_usps_validation_services',
				'value' => '1', 'current_value' => $data['EndiciaUspsAddressValidation'],
				'attr' => array('disabled' => 'disabled')
			));

			$miscGroup->add('EndiciaUspsAddressValidation', 'hidden', array('value' => '1'));
		}
		else
		{
			$miscGroup->add('EndiciaUspsAddressValidation', 'checkbox', array(
				'label' => 'shipping.use_usps_validation_services',
				'value' => '1', 'current_value' => $data['EndiciaUspsAddressValidation']
			));
		}

		if ($taxService instanceof Tax_CustomProvider)
		{
			$miscGroup->add('ShippingTaxable', 'checkbox', array(
				'label' => 'shipping.is_shipping_taxable',
				'value' => '1', 'current_value' => $data['ShippingTaxable'],
				'wrapperClass' => 'clear-both'
			));

			// TODO: optimize this

			$taxes = new Taxes($db);
			$taxesClasses = $taxes->getTaxClassesList();

			if ($taxesClasses && count($taxesClasses) > 0)
			{
				$taxClassesOptions = array();

				foreach ($taxesClasses as $taxClass) $taxClassesOptions[$taxClass['class_id']] = $taxClass['class_name'];

				$miscGroup->add('ShippingTaxClassId', 'choice', array(
					'label' => 'shipping.shipping_tax_class',
					'value' => $data['ShippingTaxClassId'],
					'options' => $taxClassesOptions
				));
			}
			else
			{
				$handlingGroup->add('ShippingHandlingFeeType', 'static', array('value' => 'shipping.no_tax_classes'));
			}
		}

		return $formBuilder->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getUpsSettingsForm($data)
	{
		$formBuilder = new core_Form_FormBuilder('ups-settings-form');

		$settingsGroup = new core_Form_FormBuilder('ups-main');
		$formBuilder->add($settingsGroup);

		$settingsGroup->add('ShippingUpsDescription', 'description', array('value' => trans('shipping.shipping_tax_class_tooltip')));

		$settingsGroup->add('ShippingUPSAccessKey', 'text', array('label' => 'shipping.ups_xml_access_key', 'value' => $data['ShippingUPSAccessKey'], 'placeholder' => 'shipping.ups_xml_access_key', 'required' => true, 'wrapperClass' => 'col-sm-12'));
		$settingsGroup->add('ShippingUPSUserID', 'text', array('label' => 'shipping.ups_user_id', 'value' => $data['ShippingUPSUserID'], 'placeholder' => 'shipping.ups_user_id', 'required' => true));
		$settingsGroup->add('ShippingUPSUserPassword', 'text', array('label' => 'shipping.ups_password', 'value' => $data['ShippingUPSUserPassword'], 'placeholder' => 'shipping.ups_password', 'required' => true));

		$settingsGroup->add('ShippingUPSRateChart', 'choice', array(
			'label' => 'shipping.ups_pickup_type',
			'value' => $data['ShippingUPSRateChart'],
			'options' => array(
				'01' => 'shipping.daily_pickup',
				'07' => 'shipping.on_call_air_pickup',
				'06' => 'shipping.one_time_pickup',
				'19' => 'shipping.letter_center',
				'03' => 'shipping.customer_counter',
				'20' => 'shipping.air_service_center',
				'11' => 'shipping.suggested_retail_rates',
			),
			'wrapperClass' => 'col-sm-12 clear-both'
		));

		$settingsGroup->add('ShippingUPSContainer', 'choice', array(
			'label' => 'shipping.ups_package_type',
			'value' => $data['ShippingUPSContainer'],
			'options' => array(
				'00' => 'shipping.your_packaging',
				'01' => 'shipping.ups_letter',
				'03' => 'shipping.ups_tube',
				'04' => 'shipping.ups_pak',
				'21' => 'shipping.ups_express_box',
				'24' => 'shipping.worldwide_25kg_box',
				'25' => 'shipping.worldwide_10kg_box',
			)
		));

		$settingsGroup->add('ShippingUPSUrl', 'text', array('label' => 'shipping.server_url_for_ups_rates', 'value' => $data['ShippingUPSUrl'], 'placeholder' => 'shipping.server_url_for_ups_rates', 'required' => true, 'wrapperClass' => 'col-sm-12 clear-both'));

		return $formBuilder->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getUspsSettingsForm($data)
	{
		$formBuilder = new core_Form_FormBuilder('usps-settings-form');

		$settingsGroup = new core_Form_FormBuilder('usps-main');
		$formBuilder->add($settingsGroup);

		$settingsGroup->add('ShippingUspsDescription', 'description', array('value' =>
			'<span class="shipping-carrier shipping-carrier-usps shipping-carrier-modal"></span>'.
			trans('shipping.server_url_for_ups_rates_tooltip') .
			' <a target="_blank" href="http://www.usps.com/webtools/">' . trans('shipping.click_here_to_go_to_usps_site') . '</a>'
		));

		$settingsGroup->add('ShippingUSPSUserID', 'text', array('label' => 'shipping.usps_tools_user_id', 'value' => $data['ShippingUSPSUserID'], 'placeholder' => 'shipping.usps_tools_user_id', 'required' => true, 'wrapperClass' => 'col-sm-6'));
		$settingsGroup->add('ShippingUSPSUrl', 'text', array('label' => 'shipping.server_url_for_usps_rates', 'value' => $data['ShippingUSPSUrl'], 'placeholder' => 'shipping.server_url_for_usps_rates', 'required' => true, 'wrapperClass' => 'col-sm-6'));
		$settingsGroup->add('ShippingUSPSPackageSize', 'choice', array(
			'label' => 'shipping.package_size_for_usps',
			'value' => $data['ShippingUSPSPackageSize'],
			'options' => array(
				'REGULAR' => 'shipping.regular',
				'LARGE' => 'shipping.large'
			),
			'wrapperClass' => 'col-sm-12',
		));

		$settingsGroup->add('ShippingUSPSPackageWidth', 'text', array('label' => 'shipping.international_package_width', 'value' => $data['ShippingUSPSPackageWidth'], 'placeholder' => 'shipping.international_package_width'));
		$settingsGroup->add('ShippingUSPSPackageLength', 'text', array('label' => 'shipping.international_package_length', 'value' => $data['ShippingUSPSPackageLength'], 'placeholder' => 'shipping.international_package_length'));
		$settingsGroup->add('ShippingUSPSPackageHeight', 'text', array('label' => 'shipping.international_package_height', 'value' => $data['ShippingUSPSPackageHeight'], 'placeholder' => 'shipping.international_package_height', 'wrapperClass' => 'col-sm-12'));

		return $formBuilder->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getFedExSettingsForm($data)
	{
		$formBuilder = new core_Form_FormBuilder('fedex-settings-form');

		$settingsGroup = new core_Form_FormBuilder('fedex-main');
		$formBuilder->add($settingsGroup);

		$settingsGroup->add('ShippingFedexDescription', 'description', array('value' =>
			'<span class="shipping-carrier shipping-carrier-fedex shipping-carrier-modal"></span>'.
			trans('shipping.fedex_tooltip') .
			' <a target="_blank" href="http://www.cartmanual.com/url/fedex">' . trans('shipping.fedex_link') . '</a>'
		));

		$settingsGroup->add('ShippingFedExAccountNumber', 'text', array('label' => 'shipping.fedex_shipping_account_number', 'value' => $data['ShippingFedExAccountNumber'], 'required' => true, 'placeholder' => 'shipping.fedex_shipping_account_number', 'wrapperClass' => 'col-sm-12', 'note' => trans('shipping.your_primary_fedex_account_number')));
		$settingsGroup->add('ShippingFedExBillingAccountNumber', 'text', array('label' => 'shipping.fedex_billing_account_number', 'value' => $data['ShippingFedExBillingAccountNumber'], 'placeholder' => 'shipping.fedex_billing_account_number', 'note' => trans('shipping.fedex_billing_tooltip')));
		$settingsGroup->add('ShippingFedExFreightAccountNumber', 'text', array('label' => 'shipping.alternate_freight_account_number', 'value' => $data['ShippingFedExFreightAccountNumber'], 'placeholder' => 'shipping.alternate_freight_account_number', 'note' => trans('shipping.fedex_freight_tooltip')));

		$settingsGroup->add('ShippingFedExApiKey', 'text', array('label' => 'shipping.fedex_api_key', 'value' => $data['ShippingFedExApiKey'], 'placeholder' => 'shipping.fedex_api_key', 'required' => true, 'wrapperClass' => 'col-sm-12 clear-both'));
		$settingsGroup->add('ShippingFedExApiPassword', 'text', array('label' => 'shipping.fedex_api_password', 'value' => $data['ShippingFedExApiPassword'], 'placeholder' => 'shipping.fedex_api_password', 'required' => true));
		$settingsGroup->add('ShippingFedExMeter', 'text', array('label' => 'shipping.fedex_meter', 'value' => $data['ShippingFedExMeter'], 'placeholder' => 'shipping.fedex_meter',));

		$settingsGroup->add('ShippingFedExRateRequestType', 'choice', array(
			'label' => 'shipping.fedex_rate_request_type',
			'value' => $data['ShippingFedExRateRequestType'],
			'options' => array(
				'LIST' => trans('shipping.list'),
				'ACCOUNT' => trans('shipping.account')
			),
			'note' => trans('shipping.fedex_rate_tooltip')
		));

		$settingsGroup->add('ShippingFedExRateRule', 'choice', array(
			'label' => 'shipping.fedex_rate_rule',
			'value' => $data['ShippingFedExRateRule'],
			'options' => array(
				'max' => trans('shipping.return_max_rate'),
				'min' => trans('shipping.return_min_rate')
			),
			'note' => trans('shipping.fedex_rate_tooltip')
		));

		$freightClassOptions = array();
		foreach (Shipping_Fedex::$freightClasses as $fedExFreightClass)
		{
			$freightClassOptions[$fedExFreightClass] = $fedExFreightClass;
		}

		$settingsGroup->add('ShippingFedExFreightClass', 'choice', array(
			'label' => 'shipping.fedex_freight_class',
			'value' => $data['ShippingFedExFreightClass'],
			'options' => $freightClassOptions
		));

		$freightPackagingOptions = array();
		foreach (Shipping_Fedex::$freightPackagingTypes as $fedExFreightPackaging)
		{
			$freightPackagingOptions[$fedExFreightPackaging] = $fedExFreightPackaging;
		}

		$settingsGroup->add('ShippingFedExFreightPackaging', 'choice', array(
			'label' => 'shipping.fedex_freight_packaging',
			'value' => $data['ShippingFedExFreightPackaging'],
			'options' => $freightPackagingOptions
		));

		$settingsGroup->add('ShippingFedExFreightPackagingWeight', 'text', array('label' => 'shipping.fedex_freight_packaging_weight', 'value' => $data['ShippingFedExFreightPackagingWeight'], 'note' => trans('shipping.fedex_freight_packaging_tooltip')));
		
		$settingsGroup->add('ShippingFedExDropOffType', 'choice', array(
			'label' => 'shipping.fedex_drop_off_type',
			'value' => $data['ShippingFedExDropOffType'],
			'options' => array('REGULAR_PICKUP' => ' Regular Pickup', 'DROP_BOX' => 'Drop Box')
		));

		return $formBuilder->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getCanadaPostSettingsForm($data)
	{
		$formBuilder = new core_Form_FormBuilder('canada-post-settings-form');

		$settingsGroup = new core_Form_FormBuilder('canada-post-main');
		$formBuilder->add($settingsGroup);

		$settingsGroup->add('ShippingCPDescription', 'description', array('value' =>
			'<span class="shipping-carrier shipping-carrier-canada-post shipping-carrier-modal"></span>'.
			trans('shipping.ship_using_canada_post') .
			' <a target="_blank" href="https://www.canadapost.ca/cpo/mc/business/productsservices/developers/services/gettingstarted.jsf">' . trans('shipping.click_here_to_learn_more') . '</a>'
		));

		$settingsGroup->add('ShippingCPMerchantID', 'text', array('label' => 'shipping.canada_post_merchant_id', 'value' => $data['ShippingCPMerchantID'], 'required' => true, 'placeholder' => 'shipping.canada_post_merchant_id'));
		$settingsGroup->add('ShippingCPUrlRating', 'text', array('label' => 'shipping.server_url_for_canada_post_rates', 'value' => $data['ShippingCPUrlRating'], 'required' => true, 'placeholder' => 'shipping.server_url_for_canada_post_rates'));

		$settingsGroup->add('ShippingCPDeliveryDate', 'choice', array(
			'label' => 'shipping.show_delivery_date_with_method_name',
			'value' => $data['ShippingCPDeliveryDate'],
			'options' => array(
				'yes' => trans('common.yes'),
				'no' => trans('common.no')
			)
		));

		$settingsGroup->add('ShippingCPTAT', 'text', array('label' => 'shipping.turn_around_time', 'value' => $data['ShippingCPTAT'], 'required' => true, 'placeholder' => 'shipping.turn_around_time'));

		$settingsGroup->add('ShippingCPItemsValue', 'choice', array(
			'label' => 'shipping.send_subtotal_for_insurance_calculation',
			'value' => $data['ShippingCPItemsValue'],
			'options' => array(
				'yes' => trans('common.yes'),
				'no' => trans('common.no')
			),
			'wrapperClass' => 'col-sm-12',
		));

		return $formBuilder->getForm();
	}

	public function getVIPparcelSettingsForm($data)
	{
		$formBuilder = new core_Form_FormBuilder('vparcel-settings-form');

		$settingsGroup = new core_Form_FormBuilder('vparcel-main');
		$formBuilder->add($settingsGroup);

		$settingsGroup->add('ShippingVIPparcelauthToken', 'text', array('label' => 'shipping.vparcel_authToken', 'value' => $data['ShippingVIPparcelauthToken'], 'placeholder' => 'shipping.vparcel_authToken', 'required' => true, 'wrapperClass' => 'col-sm-12'));

		$settingsGroup->add('ShippingVIPparcelTestmode', 'choice', array('label' => 'shipping.vparcel_testmode', 'value' => $data['ShippingVIPparcelTestmode'],
			'options' => array(
				'yes' => trans('common.yes'),
				'no' => trans('common.no')
			),
			'required' => true));

		$settingsGroup->add('ShippingVIPparcelService', 'choice', array('label' => 'shipping.vparcel_service', 'value' => $data['ShippingVIPparcelService'],
			'options' => array(
				'' => 'Select...',
				'CertifiedMail' => 'Certified Mail',
				'DeliveryConfirmation' => 'Delivery Confirmation',
				'SignatureConfirmation' => 'Signature Confirmation'
			)
		));

		return $formBuilder->getForm();
	}
}
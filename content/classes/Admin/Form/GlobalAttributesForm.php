<?php

/**
 * Class Admin_Form_GlobalAttributesForm
 */
class Admin_Form_GlobalAttributesForm
{
	/**
	 * Get manufacturer form builder
	 *
	 * @param $formData
	 * @param $categoryOptions
	 * @param $typeOptions
	 * @param $mode
	 * @param null $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $categoryOptions, $typeOptions, $mode, $id = null)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label' => 'Details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array(
			'label' => 'Attribute Name',
			'value' => $formData['name'],
			'required' => true,
			'attr' => array('maxlength' => '255')
		));

		$basicGroup->add('caption', 'text', array(
			'label' => 'Attribute Caption',
			'value' => $formData['caption'],
			'required' => true,
			'attr' => array('maxlength' => '255')
		));

		$basicGroup->add('priority', 'choice',
			array(
				'label' => 'Priority',
				'attr' => array('class'=>'short'),
				'value' => $formData['priority'],
				'options' => array(
					new core_Form_Option('1', '1'),
					new core_Form_Option('2', '2'),
					new core_Form_Option('3', '3'),
					new core_Form_Option('4', '4'),
					new core_Form_Option('5', '5'),
					new core_Form_Option('6', '6'),
					new core_Form_Option('7', '7'),
					new core_Form_Option('8', '8'),
					new core_Form_Option('9', '9'),
					new core_Form_Option('10', '10'),
				)
			)
		);

		$basicGroup->add('is_active', 'checkbox', array('label' => 'Is this attribute active?', 'value'=>'Yes', 'current_value' => $formData['is_active'], 'wrapperClass' => 'field-checkbox-space'));

		$assign_type_options = array(
			new core_Form_Option('categories', 'global_attributes.assign_type_options.categories'),
			new core_Form_Option('products', 'global_attributes.assign_type_options.products'),
		);

		$assign_type = 'categories';
		if (count($formData['products']) > 0)
		{
			$assign_type = 'products';
		}

		$basicGroup->add('assign_type', 'choice', array('label'=>'global_attributes.assign_type', 'value' => $assign_type, 'multiple' => false, 'expanded' => true, 'options' => $assign_type_options));

		$basicGroup->add(
			'categories[]', 'choice',array(
				'label' => 'Categories',
				'empty_value' => array('0' => 'All Products (Global attribute)'),
				'options' => $categoryOptions,
				'multiple' => true,
				'value' => $formData['categories'],
				'wrapperClass' => 'clear-both'
			)
		);

		$basicGroup->add('assign_products_to_global_attribute', 'template', array(
				'file' => 'templates/pages/global-attributes/assign-products.html',
				'value' => array('products' => $formData['products_list']),
		));

		$basicGroup->add(
			'attribute_type', 'choice',array(
				'label' => 'Attribute Type',
				'options' => $typeOptions,
				'value' => $formData['attribute_type'],
				'wrapperClass' => 'clear-both'
			)
		);

		$basicGroup->add('track_inventory', 'checkbox', array('label' => 'Use in variants?', 'value' => '1', 'current_value' => $formData['track_inventory'], 'wrapperClass' => 'field-checkbox-space'));

		$basicGroup->add(
			'options', 'textarea',
			array(
				'wrapperClass' => 'field-full-width',
				'label' => 'Options',
				'note' =>
					'Each new selection must be entered on a new line for it to appear correctly. If the attribute is a price modifier, you need to tell the system to increase or decrease the price (+ or -) between parenthesis ( ) at the end of each attribute. You may use actual amounts or percentages. You can also modify shipping weight on an attribute by entering the increase or decrease after the price and separated by a comma. Examples below.<br><br>'.
					'Small(-25,-0.25) Decrease price by 25, decrease weight by 0.25<br>'.
					'Medium(-10%,+1) Decrease price by 10%, increase weight by 1<br>'.
					'Large(0,+2) Price will remain the same, increase weight 2<br>'.
					'X-Large(+10.00) Increase price by 10',
				'value' => $formData['options']
			)
		);

		$basicGroup->add('text_length', 'text', array('label' => 'Max Length', 'value' => $formData['text_length']));

		$updateModeOption = array(
				new core_Form_Option('just_save_attribute','global_attributes.update_mode.just_save_attribute'),
				new core_Form_Option('bulk_update','global_attributes.update_mode.bulk_update'),
		);
		if ($mode == 'update')
		{
			$updateModeOption[] = new core_Form_Option('re_assign_attribute','global_attributes.update_mode.re_assign_attribute');
			$updateModeOption[] = new core_Form_Option('remove_from_list','global_attributes.update_mode.remove_from_list');
			$updateModeOption[] = new core_Form_Option('delete_attribute','global_attributes.update_mode.delete_attribute');
		}

		$basicGroup->add('update_mode', 'choice',
		array(
			'label' => 'global_attributes.choose_update_mode',
			'value' => 'just_save_attribute',
			'multiple' => false,
			'expanded' => true,
			'wrapperClass' => 'choose_flag',
			'options' => $updateModeOption,
			)
		);

		return $formBuilder;
	}

	/**
	 * Get form
	 *
	 * @param $formData
	 * @param $categoryOptions
	 * @param $typeOptions
	 * @param $mode
	 * @param null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $categoryOptions, $typeOptions, $mode, $id = null)
	{
		return $this->getBuilder($formData, $categoryOptions, $typeOptions, $mode, $id)->getForm();
	}
}
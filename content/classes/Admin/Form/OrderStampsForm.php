<?php

/**
 * Class Admin_Form_OrderStampsForm
 */
class Admin_Form_OrderStampsForm
{
	/**
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder()
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'order_notifications.package_properties'));
		$formBuilder->add($basicGroup);

		$dateOptions = array(
			'0' => trans('order_notifications.today').date(' (m/d)', time()),
			'+1 day' => trans('order_notifications.tomorrow').date(' (m/d)', strtotime('+1 day')),
			'+2 day' => date('D', strtotime('+2 day')).date(' (m/d)', strtotime('+2 day')),
			'+3 day' => date('D', strtotime('+3 day')).date(' (m/d)', strtotime('+3 day')),
		);
		$basicGroup->add('stamps_rate_date', 'choice', array('label' => 'order_notifications.shipping_date', 'options' => $dateOptions));

		$basicGroup->add('stamps_rate_weight', 'lbs_oz', array('label' => 'order_notifications.shipping_weight', 'value' => ''));

		$specials = new core_Form_FormBuilder('specials', array('label' => 'order_notifications.additional_services'));
		$formBuilder->add($specials);

		$specials->add('stamps_insured_value', 'text', array('label' => 'order_notifications.insured_value'));
		$specials->add('stamps_declared_value', 'text', array('label' => 'order_notifications.declared_value'));

		return $formBuilder;
	}

	/**
	 * Get form
	 */
	public function getForm()
	{
		return $this->getBuilder()->getForm();
	}
}
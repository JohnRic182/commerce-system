<?php

/**
 * Class Admin_Form_PromoCodeForm
 */
class Admin_Form_PromoCodeForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null, $currency)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'marketing.promo_code_details'));
		$formBuilder->add($basicGroup);

		$promoTypeOption = array(
			new core_Form_Option('Global', 'marketing.global'),
			new core_Form_Option('Product', 'marketing.product_based'),
			new core_Form_Option('Shipping', 'marketing.shipping')
		);
		$basicGroup->add(
			'promo_type', 'choice',
			array(
				'label' => 'marketing.promo_type', 'value' => $formData['promo_type'], 'required' => true, 'options' => $promoTypeOption
			)
		);
		$basicGroup->add('name', 'text', array('label' => 'marketing.title', 'value' => $formData['name'], 'placeholder' => 'marketing.title', 'required' => true, 'wrapperClass' => 'clear-both', 'attr' => array('maxlength' => '250')));
		$basicGroup->add('promo_code', 'text', array('label' => 'marketing.promo_code', 'value' => $formData['promo_code'], 'placeholder' => 'marketing.promo_code', 'required' => true, 'attr' => array('maxlength' => '250')));
		$basicGroup->add('discount', 'text', array('label' => 'marketing.discount', 'value' => $formData['discount'], 'placeholder' => 'marketing.discount', 'required' => true, 'attr' => array('maxlength' => '250')));


		$discountTypeOption = array(
			new core_Form_Option('amount', ($currency ? $currency["code"] : 'USD')),
			new core_Form_Option('percent', '%')
		);
		$basicGroup->add(
			'discount_type', 'choice',
			array(
				'label' => 'marketing.discount_type', 'value' => $formData['discount_type'], 'required' => true, 'options' => $discountTypeOption
			)
		);

		$basicGroup->add('min_amount', 'text', array('label' => 'marketing.minimum_order_subtotal', 'value' => $formData['min_amount'], 'placeholder' => 'marketing.minimum_order_subtotal', 'required' => true, 'attr' => array('maxlength' => '250')));
		$basicGroup->add(
			'min_amount_type', 'choice',
			array(
				'label' => 'marketing.minimum_order_subtotal_type',
				'value' => $formData['min_amount_type'],
				'required' => true,
				'options' => array(
					new core_Form_Option('order_subtotal', 'marketing.minimum_order_subtotal_type_order'),
					new core_Form_Option('item_subtotal', 'marketing.minimum_order_subtotal_type_item')
				)
			)
		);

		$mins = array();
		for ($i = 0; $i <= 59; $i++)
		{
			$mins[] = new core_Form_Option($i, sprintf('%02d', $i));
		}

		$hours = array();
		$hour_label = '';
		for ($i=0; $i<=23; $i++)
		{
			if ($i == 0)
			{
				$hour_label = '12';
			}
			elseif ($i < 13)
			{
				$hour_label = $i;
			}
			else
			{
				$hour_label = $i - 12;
			}
			$hour_label .= $i >= 12 ? ' pm' : ' am';
			$hours[] = new core_Form_Option($i, $hour_label);
		}

		$startDateGroup = new core_Form_FormBuilder('start_date_group', array('label'=>'marketing.start_date'));
		$startDateGroup->add('start_date', 'text', array('label' => 'marketing.start_date', 'value' => $formData['start_date'], 'placeholder' => 'marketing.start_date', 'required' => true, 'attr' => array('maxlength' => '50')));
		$startDateGroup->add(
				'start_hour', 'choice',
				array(
					'label' => 'marketing.start_time', 'value' => $formData['start_hour'], 'required' => true, 'options' => $hours,
					'wrapperClass' => 'clear-both',
				)
		);
		$startDateGroup->add(
				'start_min', 'choice',
				array(
					'label' => 'marketing.start_min', 'value' => $formData['start_min'], 'required' => true, 'options' => $mins,
				)
		);

		$endDateGroup = new core_Form_FormBuilder('end_date_group', array('label'=>'marketing.end_date'));
		$endDateGroup->add('end_date', 'text', array('label' => 'marketing.end_date', 'value' => $formData['end_date'], 'placeholder' => 'marketing.end_date', 'required' => true, 'attr' => array('maxlength' => '50')));
		$endDateGroup->add(
				'end_hour', 'choice',
				array(
					'label' => 'marketing.end_time', 'value' => $formData['end_hour'], 'required' => true, 'options' => $hours,
					'wrapperClass' => 'clear-both',
				)
		);
		$endDateGroup->add(
				'end_min', 'choice',
				array(
					'label' => 'marketing.end_min', 'value' => $formData['end_min'], 'required' => true, 'options' => $mins,
				)
		);

		$formBuilder->add($startDateGroup);
		$formBuilder->add($endDateGroup);

		if ($mode == 'update')
		{
			$productsGroup = new core_Form_FormBuilder('products', array('label'=>'marketing.assign_products'));
			$category_options = $formData["categories"];
			$productsGroup->add(
					'categories', 'choice',
					array(
						'label' => 'marketing.categories',
						'required' => true,
						'options' => $category_options,
						'note' => 'marketing.categories_tooltip',
						'value'=> $category_options,
						'empty_value'=>array('0'=>'All Categories'),
						'multiple' => true,
						'atrr' => array(),
					)
			);

			$selectedProductsGroup = new core_Form_FormBuilder('selected_products', array('label'=>''));
			$selectedProductsGroup->add(
					'products', 'choice',
					array(
						'label' => 'marketing.products_in_selected_categories', 'value' => '', 'options' => array(),
						'note' => 'marketing.products_in_selected_categories_tooltip',
						'multiple' => true,
						'atrr' => array('id'=>'selectProducts'),
					)
			);
			$selectedProductsGroup->add(
					'selectedProducts', 'choice',
					array(
						'label' => 'marketing.products_in_this_group', 'value' => '', 'required' => true, 'options' => array(),
						'note' => 'marketing.products_in_this_group',
						'multiple' => true,
						'atrr' => array('id'=>'selectedProducts'),
					)
			);

			$buttonsGroup = new core_Form_FormBuilder('buttons', array('label'=>''));
			$buttonsGroup->add('remove_add', 'static', array(
					'label'=>'',
					'value'=>'
					<button id="btnAddProduct" style="width:100px;">' . trans('common.add') . ' &gt;&gt;</button><br/><br/>
					<button id="btnRemoveProduct" style="width:100px;">&lt;&lt; ' . trans('common.remove') . ' </button>'
			));

			$formBuilder->add($productsGroup);
			$formBuilder->add($selectedProductsGroup);
			$formBuilder->add($buttonsGroup);

			$categoryGroup = new core_Form_FormBuilder('categories', array('label'=>'marketing.assign_categories'));

			$category_options = $formData["categories"];
			$categoryGroup->add(
					'promo_categories[]', 'choice',
					array(
						'label' => 'marketing.categories',
						'required' => true,
						'options' => $category_options,
						'note' => 'marketing.categories_tooltip',
						'value'=> $formData["categories_selected"],
						'multiple' => true,
						'atrr' => array(),
					)
			);

			$formBuilder->add($categoryGroup);
			
		}

		return $formBuilder;
	}

	/**
	 * Get promo code form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 * @param $currency
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null, $currency = null)
	{
		return $this->getBuilder($formData, $mode, $id, $currency)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getPromoCodeSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[search_str]', 'text', array('label' => 'Search in promotions', 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-6'));

		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getPromoCodeSearchForm($data)
	{
		return $this->getPromoCodeSearchFormBuilder($data)->getForm();
	}
}
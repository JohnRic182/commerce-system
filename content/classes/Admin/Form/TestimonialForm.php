<?php
/**
 * Class Admin_Form_TestimonialForm
 */
class Admin_Form_TestimonialForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$request = Framework_Request::createFromGlobals();
	
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'testimonials.testimonial_details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array('label' => 'testimonials.name', 'value' => $formData['name'], 'required' => true, 'attr' => array('maxlength' => '100')));

		$basicGroup->add('company', 'text', array('label' => 'testimonials.company', 'value' => $formData['company'], 'attr' => array('maxlength' => '100')));

		$ratingOptions = array();
		
		for ($i = 1; $i <= 10; $i++) {
			$ratingOptions[] = new core_Form_Option($i, $i);
		}

		$basicGroup->add(
			'rating', 'choice',
			array(
				'label' => 'testimonials.rating', 'value' => $formData['rating'], 'options' => $ratingOptions
			)
		);

		$statusOption = array(
			new core_Form_Option('approved', 'testimonials.status_options.approved'),
			new core_Form_Option('pending', 'testimonials.status_options.pending'),
			new core_Form_Option('declined', 'testimonials.status_options.declined')
		);

		$basicGroup->add('testimonial', 'textarea', array('label' => 'testimonials.testimonial', 'required' => true, 'value' => $formData['testimonial']));
		
		$basicGroup->add(
			'status', 'choice',
			array(
				'label' => 'testimonials.status', 'value' => $formData['status'], 'required' => true, 'options' => $statusOption
			)
		);

		return $formBuilder;
	}

	/**
	 * Get testimonial form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getTestimonialSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[search_str]', 'text', array('label' => 'Search in testimonials', 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-6'));

		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getTestimonialSearchForm($data)
	{
		return $this->getTestimonialSearchFormBuilder($data)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_ProductFeatureForm
 */
class Admin_Form_ProductFeatureForm
{
	/**
	 * Get products features form builder
	 *
	 * @param $formData
	 * @param $mode
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'products_features.feature_details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'feature_note', 'description',
			array('value' => trans('products_features.add_feature_notes'))
		);

		$basicGroup->add(
			'feature_name', 'text',
			array(
				'label' => 'products_features.feature_name',
				'value' => $formData['feature_name'],
				'placeholder' => 'products_features.feature_name',
				'required' => true,
				'attr' => array('maxlength' => '128', 'class' => 'generate-id-source-field')
			)
		);

		$basicGroup->add(
			'feature_id',
			'text',
			array(
				'label' => 'products_features.feature_id',
				'value' => $formData['feature_id'],
				'placeholder' => 'products_features.feature_id',
				'required' => true,
				'attr' => array('maxlength' => '128', 'class' => 'generated-id-field' . ($mode == 'add' ? ' autogenerate':''))
			)
		);
		$basicGroup->add(
			'feature_active',
			'checkbox',
			array(
				'label' => 'products_features.feature_active',
				'value' => 1,
				'current_value' => $formData['feature_active'],
				'wrapperClass' => 'field-checkbox-space'
			)
		);

		return $formBuilder;
	}

	/**
	 * Get products features form
	 *
	 * @param $formData
	 * @param $mode
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode)
	{
		return $this->getBuilder($formData, $mode)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getProductFeatureSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[search_str]', 'text', array('label' => 'Search Products Features', 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-6'));
		$formBuilder->add(
			'searchParams[feature_active]',
			'choice',
			array(
				'label' => 'Product Feature Status',
				'value' => $data['feature_active'],
				'empty_value' => array('any' => 'All'),
				'options' => array('No' => 'Inactive', 'Yes' => 'Active'),
				'wrapperClass' => 'col-sm-12 col-md-3'
			)
		);
		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getProductFeatureSearchForm($data)
	{
		return $this->getProductFeatureSearchFormBuilder($data)->getForm();
	}
}

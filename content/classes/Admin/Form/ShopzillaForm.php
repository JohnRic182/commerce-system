<?php

/**
 * Class Admin_Form_ShopzillaForm
 */
class Admin_Form_ShopzillaForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getShopzillaActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('ShopzillaMerchantId', 'text', array('label' => 'apps.shopzilla.merchant_id_number', 'required' => true, 'value' => $data['ShopzillaMerchantId']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getShopzillaActivateForm($data)
    {
        return $this->getShopzillaActivateBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('ShopzillaMerchantId', 'text', array('label' => 'apps.shopzilla.merchant_id_number', 'required' => true, 'value' => $data['ShopzillaMerchantId']));

        $advancedGroup = new core_Form_FormBuilder('group-advanced', array('label' => 'apps.shopzilla.advanced_settings', 'collapsible' => true));
        $formBuilder->add($advancedGroup);

        $advancedGroup->add('ShopzillaROITracker', 'checkbox', array('label' => 'apps.shopzilla.roi_tracker', 'value' => 'YES', 'current_value' => $data['ShopzillaROITracker']));
        $advancedGroup->add('SurveyInvitation', 'checkbox', array('label' => 'apps.shopzilla.bizrate_survey_information', 'value' => 'YES', 'current_value' => $data['SurveyInvitation']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
<?php
/**
 * Class Admin_Form_ProductReviewAdvancedSettingsForm
 */
class Admin_Form_ProductReviewAdvancedSettingsForm
{
	/**
	 * Get advanced settings form for product reviews - builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$basicGroup->add('productReviewSettings[ReviewsEnable]', 'checkbox', array(
				'label' => 'marketing.product_reviews_enabled',
				'value' => '1',
				'current_value' => $formData['ReviewsEnable'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['ReviewsEnable'])
		));

		$basicGroup->add('productReviewSettings[ReviewsAutoApprove]', 'checkbox', array(
				'label' => 'marketing.auto_approve_enabled',
				'value' => '1',
				'current_value' => $formData['ReviewsAutoApprove'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['ReviewsAutoApprove'])
		));

		$ratingOptions = array();
		for ($i = 0; $i <= 5; $i++)
		{
			$ratingOptions[] = new core_Form_Option($i, $i);
		}

		$basicGroup->add('productReviewSettings[ReviewsAutoApproveRating]', 'choice', array(
				'label' => 'marketing.auto_approve_above',
				'options' => $ratingOptions,
				'value' => $formData['ReviewsAutoApproveRating'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['ReviewsAutoApproveRating'])
		));

		$basicGroup->add('productReviewSettings[ReviewsAutoApprovePurchased]', 'checkbox', array(
				'label' => 'marketing.auto_approve_purchased',
				'value' => '1',
				'current_value' => $formData['ReviewsAutoApprovePurchased'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['ReviewsAutoApprovePurchased'])
		));

		return $formBuilder;
	}

	/**
	 * Get advanced products review settings form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_OrderNotificationAdvancedSettingsForm
 */
class Admin_Form_OrderNotificationAdvancedSettingsForm
{
	/**
	 * Get advanced settings form for order notifications - builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$translator = Framework_Translator::getInstance();
		
		$formBuilder = new core_Form_FormBuilder('general');
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$notifyWhenOptions = array();
		foreach ($formData['notifyWhenOptions'] as $key=>$val)
		{
			$notifyWhenOptions[] = new core_Form_Option($key, $val);
		}
		$basicGroup->add('ProductsLocationsSendOption', 'choice', array(
			'label' => 'order_notifications.send_email_notification_when',
			'value' => $formData['ProductsLocationsSendOption'],
			'options' => $notifyWhenOptions,
			'wrapperClass' => 'clear-both'
		));

		return $formBuilder;
	}

	/**
	 * Get advanced settings form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
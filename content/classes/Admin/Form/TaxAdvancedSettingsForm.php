<?php
/**
 * Class Admin_Form_TaxAdvancedSettingsForm
 */
class Admin_Form_TaxAdvancedSettingsForm
{
	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'taxSettings[TaxAddress]', 'choice',
			array(
				'label' => 'tax_settings.tax_address',
				'value' => $formData['TaxAddress'],
				//'expanded' => true, //TODO: Fix radio button list display
				'options' => array(
					'Shipping' => 'tax_settings.tax_address_options.shipping',
					'Billing' => 'tax_settings.tax_address_options.billing',
					'Default' => 'tax_settings.tax_address_options.default',
				)
			)
		);

		$basicGroup->add(
			'taxSettings[TaxDefaultCountry]', 'choice',
			array(
				'label' => 'tax_settings.default_tax_country',
				'value' => $formData['TaxDefaultCountry'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['TaxDefaultCountry'], 'class' => 'input-address-country')
			)
		);

		$basicGroup->add(
			'taxSettings[TaxDefaultState]', 'choice',
			array(
				'label' => 'tax_settings.default_tax_state',
				'value' => $formData['TaxDefaultState'],
				'required' => true,
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['TaxDefaultState'], 'class' => 'input-address-state'),
			)
		);

		$basicGroup->add(
			'taxSettings[TaxExemptionsActive]', 'choice',
			array(
				'label' => 'tax_settings.enable_tax_exemptions',
				'value' => $formData['TaxExemptionsActive'],
				'wrapperClass' => 'clear-both',
				'options' => array(
					'0' => 'tax_settings.enable_tax_exemptions_options.no',
					'1' => 'tax_settings.enable_tax_exemptions_options.yes'),
				'attr' => array('class' => 'short')
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_ProductEditForm
 */
class Admin_Form_ProductEditForm
{
	/**
	 * @return core_Form_FormBuilder
	 * @param array $formData
	 * @param array $categoryList
	 * @param array $taxClassList
	 * @param $currency
	 */
	public function getBuilder($formData, $categoryList, $taxClassList, $currency)
	{
		$editFormBuilder = new core_Form_FormBuilder('edit');

		$editFormBuilder->add('edit[change_price]', 'checkbox', array('label' => 'product.bulk_update_form.change_price', 'current_value' => $formData['is_change_product_price'], 'wrapperClass' => 'col-sm-12 col-xs-12'));

		$priceClass = ($formData['is_change_product_price']) ? '' : 'hidden';

		$editFormBuilder->add('edit[price_change]', 'text', array('label' => 'product.bulk_update_form.price_change', 'value' => $formData['product_price'], 'wrapperClass' => 'col-sm-6 col-xs-12 '.$priceClass));

		$discountTypeOption = array(
			new core_Form_Option('amount', ($currency ? $currency["code"] : 'USD')),
			new core_Form_Option('percent', '%')
		);

		$editFormBuilder->add('edit[currency]', 'choice', array('label' => 'product.bulk_update_form.currency_sign', 'value' => $formData['product_price_discount_type'], 'options' => $discountTypeOption, 'wrapperClass' => 'col-sm-3 col-xs-12 '.$priceClass));
		$editFormBuilder->add('edit[state]', 'choice', array('label' => 'product.bulk_update_form.state', 'value' => $formData['state'], 'options' => array(
			'increase' => 'product.bulk_update_form.increase',
			'decrease' => 'product.bulk_update_form.decrease'
		), 'wrapperClass' => 'col-sm-3 col-xs-12 '.$priceClass));
		$editFormBuilder->add('edit[price_round]', 'checkbox', array('label' => 'product.bulk_update_form.round', 'current_value' => $formData['round_product_price'], 'wrapperClass' => 'col-sm-12 col-xs-12 '.$priceClass));

		$editFormBuilder->add('edit[change_sale_price]', 'choice', array('label' => 'product.bulk_update_form.change_sale_price', 'value' => $formData['is_change_product_sale_price'], 'options' => array(
			'0' => 'product.bulk_update_form.no_change',
			'1' => 'product.bulk_update_form.set_sale_price',
			'2' => 'product.bulk_update_form.remove_sale'
		), 'wrapperClass' => 'col-sm-12 col-xs-12'));

		$priceSaleClass = (intval($formData['is_change_product_sale_price']) == 1) ? '' : 'hidden';

		$editFormBuilder->add('edit[sale_text]', 'description', array('value' => trans('product.bulk_update_form.sale_price_note'), 'wrapperClass' => 'col-xs-12 '.$priceSaleClass));
		$editFormBuilder->add('edit[price_change2]', 'text', array('label' => 'product.bulk_update_form.price_change', 'value' => $formData['product_sale_price'], 'wrapperClass' => 'col-sm-8 col-xs-12 '.$priceSaleClass));

		$editFormBuilder->add('edit[currency2]', 'choice', array('label' => 'Currency Sign', 'value' => $formData['product_sale_price_discount_type'], 'options' => $discountTypeOption, 'wrapperClass' => 'col-sm-4 col-xs-12 '.$priceSaleClass));
		$editFormBuilder->add('edit[price_round2]', 'checkbox', array('label' => 'product.bulk_update_form.round', 'current_value' => $formData['round_product_sale_price'], 'wrapperClass' => 'col-sm-12 col-xs-12 '.$priceSaleClass));

		$editFormBuilder->add('edit[products_category]', 'choice', array('label' => 'product.bulk_update_form.change_category', 'value' => $formData['category'], 'options' => $categoryList));
		$editFormBuilder->add('edit[storefront]', 'choice', array('label' => 'product.bulk_update_form.visibility', 'value' => $formData['visibility'], 'options' => array(
			'0' => 'product.bulk_update_form.do_not_change',
			'1' => 'product.bulk_update_form.show_products',
			'2' => 'product.bulk_update_form.hide_products'
		)));
		$editFormBuilder->add('edit[tax_class]', 'choice', array('label' => 'product.bulk_update_form.change_tax_class', 'value' => $formData['tax_class'], 'options' => $taxClassList));
		$editFormBuilder->add('edit[call_price]', 'choice', array('label' => 'product.bulk_update_form.call_for_price', 'value' => $formData['call_for_price'], 'options' => array(
			'0' => 'product.bulk_update_form.do_not_change',
			'1' => 'product.bulk_update_form.yes',
			'2' => 'product.bulk_update_form.no',
		)));
		$editFormBuilder->add('edit[hot_deal]', 'choice', array('label' => 'product.bulk_update_form.hot_deal', 'value' => $formData['is_hot_deal'], 'options' => array(
			'0' => 'product.bulk_update_form.do_not_change',
			'1' => 'product.bulk_update_form.yes',
			'2' => 'product.bulk_update_form.no',
		)));
		$editFormBuilder->add('edit[homepage]', 'choice', array('label' => 'product.bulk_update_form.is_homepage', 'value' => $formData['is_home_page'], 'options' => array(
			'0' => 'product.bulk_update_form.do_not_change',
			'1' => 'product.bulk_update_form.yes',
			'2' => 'product.bulk_update_form.no',
		)));

		return $editFormBuilder;
	}

	/**
	 * @return mixed
	 * @param array $formData
	 * @param array $categoryList
	 * @param array $taxClassList
	 * @param $currency
	 */
	public function getForm($formData, $categoryList, $taxClassList, $currency)
	{
		// Category List
		$catList = array(
			'no_change' => trans('product.bulk_update_form.do_not_change')
		);
		unset($categoryList['any']);
		foreach($categoryList as $key => $value){
			$catList[$key] = $value;
		}

		// Tax List
		$taxList = array(
			'no_change' => trans('product.bulk_update_form.do_not_change'),
			'no_tax' => trans('product.bulk_update_form.not_taxable')
		);
		foreach($taxClassList as $key => $value){
			$taxList[$key] = $value;
		}

		return $this->getBuilder($formData, $catList, $taxList, $currency)->getForm();
	}

	/**
	 * Returns Bulk Update Form default fields
	 * @return array
	 */
	public function getDefaultFields()
	{
		$updateDefaultFields = array(
			'is_change_product_price' => '0',
			'state' => 'decrease',
			'product_price' => '0',
			'round_product_price' => '0',
			'product_price_discount_type' => 'amount',
			'product_sale_price' => '0',
			'is_change_product_sale_price' => '0',
			'round_product_sale_price' => '0',
			'product_sale_price_discount_type' => 'amount',
			'category' => 'no_change',
			'visibility' => '1',
			'tax_class' => 'no_change',
			'call_for_price' => '0',
			'is_hot_deal' => '0',
			'is_home_page' => '0'
		);
		return $updateDefaultFields;
	}
}
<?php

/**
 * Class Admin_Form_ProductForm
 */
class Admin_Form_ProductForm
{
	protected $settings;
	protected $db;

	/**
	 * @param DataAccess_SettingsRepository $settings
	 * @param DB $db
	 */
	public function __construct(DataAccess_SettingsRepository $settings, DB $db)
	{
		$this->settings = $settings;
		$this->db = $db;
	}

	/**
	 * Get product form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $params
	 * @param null $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $params, $id = null)
	{
		$categoryOptions = isset($params['categoryOptions']) ? $params['categoryOptions'] : array();
		$manufacturerOptions = isset($params['manufacturerOptions']) ? $params['manufacturerOptions'] : array();
		$productLocationOptions = isset($params['productLocationOptions']) ? $params['productLocationOptions'] : array();
		$taxService = isset($params['taxService']) ? $params['taxService'] : array();
		$taxClassOptions = isset($params['taxClassOptions']) ? $params['taxClassOptions'] : array();
		$productShippingMethods =  isset($params['productShippingMethods']) ? $params['productShippingMethods'] : array();

		$primaryImage = isset($params['primaryImage']) ? $params['primaryImage'] : false;
		$secondaryImages = isset($params['secondaryImages']) ? $params['secondaryImages'] : false;
		$productAttributes = isset($params['productAttributes']) ? $params['productAttributes'] : array();
		$productVariants = isset($params['productVariants']) ? $params['productVariants'] : array();
		$productFeaturesGroups = isset($params['productFeaturesGroups']) ? $params['productFeaturesGroups'] : array();
		$productPromotions = isset($params['productPromotions']) ? $params['productPromotions'] : array();
		$productQuantityDiscounts = isset($params['productQuantityDiscounts']) ? $params['productQuantityDiscounts'] : array();
		$productFamilies = isset($params['productFamilies']) ? $params['productFamilies'] : array();

		$settings = $this->settings;

		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		if ($formData instanceof Model_Product)	$formData = $formData->getValues();

		$formBuilder->add($this->getBasicGroup($formData, $mode, $categoryOptions, $primaryImage, $secondaryImages, $id));
		$formBuilder->add($this->getAdvancedGroup(
			$formData, $mode, $categoryOptions, $manufacturerOptions, $productLocationOptions, $taxService, $taxClassOptions,
			$productShippingMethods, $id
		));

		$formBuilder->add($this->getSeoGroup($formData));

		$formBuilder->add($this->getSearchKeywordsGroup($formData));

		if ($mode == 'update')
		{
			$formBuilder->add($this->getAttributesGroup($formData, $productAttributes));
			$formBuilder->add($this->getVariantsGroup($formData, $productVariants));
			if (_ACCESS_PRODUCTS_FILTERS && _CAN_ENABLE_PRODUCTS_FILTERS)
			{
				$formBuilder->add($this->getProductFeaturesGroupsGroup($formData, $productFeaturesGroups));
			}
			$formBuilder->add($this->getPromotionsGroup($formData, $productPromotions));
			$formBuilder->add($this->getQuantityDiscountsGroup($formData, $productQuantityDiscounts));
		}

		if (defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING  && $settings->get('RecurringBillingEnabled') == '1')
		{
			$formBuilder->add($this->getRecurringBillingGroup($formData));
		}

		if ($settings->get('WholesaleDiscountType') == 'PRODUCT')
		{
			$formBuilder->add($this->getWholesaleGroup($formData));
		}

		if (count($productFamilies) > 0 && $this->settings->get('RecommendedProductsCount') > 0)
		{
			$formBuilder->add($this->getRecommendedProductsGroup($productFamilies, $formData['product_families']));
		}

		if (AccessManager::checkAccess('PRODUCT_EXPORT'))
		{
			$exportGroup = $this->getExportGroup($formData);
			if ($exportGroup) $formBuilder->add($exportGroup);
		}

		return $formBuilder;
	}

	/**
	 * Basic product properties
	 *
	 * @param $formData
	 * @param $mode
	 * @param $categoryOptions
	 * @param $primaryImage
	 * @param $secondaryImages
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	protected function getBasicGroup($formData, $mode, $categoryOptions, $primaryImage, $secondaryImages, $id)
	{
		$basicGroup = new core_Form_FormBuilder('group-basic', array('label' => 'product.product_form.product_details'));

		$attr = array('maxlength' => '255', 'class' => 'generated-id-field');
		if ($mode == 'add') $attr['class'] =  'generated-id-field autogenerate';

		$basicGroup->add(
			'title',
			'text',
			array(
				'label' => 'product.product_form.product_name',
				'value' => $formData['title'],
				'placeholder' => 'product.product_form.product_name',
				'required' => true,
				'attr' => array(
					'maxlength' => '255',
					'class' => 'generate-id-source-field'
				)
			)
		);

		$basicGroup->add(
			'product_id',
			'text',
			array(
				'label' => 'product.product_form.product_id',
				'value' => $formData['product_id'],
				'placeholder' => 'product.product_form.product_id',
				'required' => true,
				'attr' => $attr,
				'note' => 'product.product_form.product_id_tooltip'
			)
		);

		if ($formData['price2'] != '')
		{
			$a = $formData['price'];
			$formData['price'] = $formData['price2'];
			$formData['price2'] = $a;
		}

		$basicGroup->add(
			'price',
			'money',
			array(
				'label' => 'product.product_form.price_per_item',
				'value' => $formData['price'],
				'placeholder' => 'product.product_form.price_per_item',
				'required' => true,
				'validators' => 'required|numeric'
			)
		);

		$basicGroup->add(
			'price2',
			'money',
			array(
				'label' => 'product.product_form.sale_price',
				'placeholder' => 'product.product_form.sale_price',
				'value' => $formData['price2'],
				'validators' => 'numeric'
			)
		);

		if (count($categoryOptions) > 0)
		{
			$basicGroup->add(
				'cid',
				'choice',
				array(
					'label' => 'product.product_form.primary_category',
					'value' => $formData['cid'],
					'required' => true,
					'options' => $categoryOptions,
					'empty_value' => array('0' => '-' . trans('product.product_form.please_choose'))
				)
			);
		}
		else
		{
			$basicGroup->add(
				'category_name',
				'text',
				array(
					'label' => 'product.product_form.add_category',
					'value' => '',
					'required' => true,
					'attr' => array('maxlength' => '255'),
					'note' => 'product.product_form.add_category_tooltip'
				)
			);
		}


		$basicGroup->add(
			'is_visible',
			'checkbox',
			array(
				'label' => 'product.product_form.shown_on_storefront',
				'value' => 'Yes',
				'current_value' => $formData['is_visible'],
				'wrapperClass' => 'field-checkbox-space col-xs-12 col-md-6'
			)
		);

		$basicGroup->add(
			'overview',
			'textarea',
			array(
				'label' => 'product.product_form.overview',
				'value' => $formData['overview'],
				'wysiwyg' => false
			)
		);

		$basicGroup->add(
			'description',
			'textarea',
			array(
				'label' => 'product.product_form.description',
				'value' => $formData['description'],
				'wysiwyg' => true
			)
		);

		if ($mode == DataAccess_ProductsRepository::MODE_UPDATE)
		{
			$productImages = $secondaryImages;
			if ($primaryImage) array_unshift($productImages, $primaryImage);
		}
		else
		{
			$productImages = array();
		}

		$basicGroup->add(
			'image_upload',
			'image-multiple',
			array(
				'label' => 'product.product_form.product_images',
				'imageUpdateUrl' => $mode == DataAccess_ProductsRepository::MODE_UPDATE && !is_null($id) ? 'admin.php?p=product&mode=update-image&id=' . $id . '&image_id=' : null,
				'imageDeleteUrl' => $mode == DataAccess_ProductsRepository::MODE_UPDATE && !is_null($id) ? 'admin.php?p=product&mode=delete-image&id=' . $id . '&image_id=' : null,
				'value' => $productImages,
				'allowExternalImage' => true
			)
		);

		return $basicGroup;
	}

	/**
	 * Advanced product properties
	 *
	 * @param $formData
	 * @param $mode
	 * @param $categoryOptions
	 * @param $manufacturerOptions
	 * @param $productLocationOptions
	 * @param $taxService
	 * @param $taxClassOptions
	 * @param $productShippingMethods
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	protected function getAdvancedGroup($formData, $mode, $categoryOptions, $manufacturerOptions, $productLocationOptions, $taxService, $taxClassOptions, $productShippingMethods, $id)
	{
		$advancedGroup = new core_Form_FormBuilder(
			'group-advanced',
			array(
				'label' => 'product.product_form.advanced_settings',
				'collapsible' => true
			)
		);

		$productTypeOptions = array(
			Model_Product::TANGIBLE => 'product.product_form.product_type_tangible',
			Model_Product::VIRTUAL => 'product.product_form.product_type_virtual',
		);

		// check if digital download is enabled or add option for existing digital product
		if ($this->settings->get('EnableDigitalDownload') || $formData['product_type'] == Model_Product::DIGITAL)
		{
			$productTypeOptions[Model_Product::DIGITAL] = 'product.product_form.product_type_digital';
		}

		$advancedGroup->add(
			'product_type',
			'choice',
			array(
				'label' => 'product.product_form.product_type',
				'value' => $formData['product_type'],
				'options' => $productTypeOptions
			)
		);

		$advancedGroup->add(
			'priority',
			'text',
			array(
				'label' => 'product.product_form.order_of_appearance',
				'value' => $formData['priority'],
				'attr' => array('maxlength' => 200),
				'note' => 'product.product_form.order_of_appearance_tooltip'
			)
		);

		/**
		 * Shipping properties
		 */
		$advancedGroup->add(
			'weight',
			'numeric',
			array(
				'label' => trans('product.product_form.item_weight') . ' (' . $this->settings->get('LocalizationWeightUnits') . ')',
				'value' => $formData['weight']
			)
		);

		$advancedGroup->add(
			'dimension',
			'dimension',
			array(
				'label' => 'product.product_form.item_dimensions',
				'widthValue' => $formData['dimension_width'],
				'heightValue' => $formData['dimension_height'],
				'lengthValue' => $formData['dimension_length']
			)
		);

		$advancedGroup->add(
			'free_shipping',
			'checkbox',
			array(
				'label' => 'product.product_form.free_shipping',
				'value' => 'Yes',
				'current_value' => $formData['free_shipping']
			)
		);

		if ($productShippingMethods && is_array($productShippingMethods) && count($productShippingMethods))
		{
			$advancedGroup->add(
				'product_level_shipping',
				'checkbox',
				array(
					'label' => 'product.product_form.product_level_shipping',
					'value' => '1',
					'current_value' => $formData['product_level_shipping'],
					'wrapperClass' => 'clear-both field-product-level-shipping-wrap'
				)
			);

			$advancedGroup->add(
				'product_level_shipping_methods',
				'template',
				array(
					'file' => 'templates/pages/product/edit-shipping-methods.html',
					'value' => array('shippingMethods' => $productShippingMethods)
				)
			);
		}

		if ($this->settings->get('PikflyEnabled') == 'Yes')
		{
			$advancedGroup->add(
				'same_day_delivery',
				'checkbox',
				array(
					'label' => 'Available for same day delivery',
					'value' => '1',
					'current_value' => $formData['same_day_delivery'],
					'wrapperClass' => 'field-checkbox-space'
				)
			);
		}

		if (count($productLocationOptions) > 0)
		{
			$advancedGroup->add(
				'products_location_id',
				'choice',
				array(
					'label' => 'product.product_form.product_location',
					'value' => $formData['products_location_id'],
					'options' => $productLocationOptions, 'empty_value' => array('0' => 'common.na')
				)
			);
		}
		else
		{
			$advancedGroup->add(
				'products_location_id',
				'hidden',
				array('value' => $formData['products_location_id'])
			);
		}

		/**
		 * Digital
		 */
		$advancedGroup->add(
			'digital_product_file',
			'text',
			array(
				'label' => 'product.product_form.enter_path',
				'value' => $formData['digital_product_file'],
				'note' => 'product.product_form.enter_path_tooltip'
			)
		);

		$advancedGroup->add(
			'digital_product_file_upload',
			'file',
			array(
				'label' => 'product.product_form.file_select',
				'note' => 'product.product_form.file_select_tooltip',
				'required' => true,
				'attr' => array('accept' => 'image/jpeg, image/png'),
			)
		);

		/**
		 * Taxes
		 */
		if ($taxService instanceof Tax_CustomProvider)
		{
			$advancedGroup->add(
				'is_taxable',
				'checkbox',
				array(
					'label' => 'product.product_form.is_taxable',
					'value' => 'Yes',
					'current_value' => $formData['is_taxable'],
					'wrapperClass' => 'field-checkbox-space clear-both'
				)
			);

			if (count($taxClassOptions) > 0)
			{
				$advancedGroup->add(
					'tax_class_id',
					'choice',
					array(
						'label' => 'product.product_form.tax_class',
						'value' => $formData['tax_class_id'],
						'options' => $taxClassOptions,
						'empty_value' => array('0' => 'common.na')
					)
				);
			}
			else
			{
				$advancedGroup->add(
					'tax_class_id',
					'hidden',
					array('value' => $formData['tax_class_id'])
				);
			}
		}
		else if ($taxService instanceof Tax_Avalara_AvalaraService)
		{
			$advancedGroup->add(
				'avalara_tax_code',
				'choice',
				array(
					'label' => 'product.product_form.avalara_tax_code',
					'value' => $formData['avalara_tax_code'],
					'options' => $taxClassOptions,
					'wrapperClass' => 'clear-both',
					'empty_value' => array('' => 'common.na')
				)
			);
		}
		else if ($taxService instanceof Tax_Exactor_ExactorService)
		{
			$advancedGroup->add(
				'exactor_euc_code',
				'choice',
				array(
					'label' => 'product.product_form.exactor_tax_code',
					'value' => $formData['exactor_euc_code'],
					'options' => $taxClassOptions,
					'wrapperClass' => 'clear-both',
					'empty_value' => array('' => 'common.na')
				)
			);
		}

		$advancedGroup->add(
			'categories[]', 'choice',
			array(
				'label' => 'product.product_form.secondary_categories',
				'options' => $categoryOptions,
				'wrapperClass' => 'clear-both',
				'multiple' => true,
				'value' => $formData['categories']
			)
		);

		$advancedGroup->add(
			'order',
			'minmaxnumeric',
			array(
				'label' => 'product.product_form.min_max',
				'minValue' => $formData['min_order'],
				'maxValue' => $formData['max_order']
			)
		);

		$advancedGroup->add(
			'call_for_price',
			'checkbox',
			array(
				'label' => 'product.product_form.call_for_price',
				'value' => 'Yes',
				'current_value' => $formData['call_for_price'],
				'wrapperClass' => 'clear-both'
			)
		);

		$advancedGroup->add(
			'is_hotdeal',
			'checkbox',
			array(
				'label' => 'product.product_form.hot_deal',
				'value' => 'Yes',
				'current_value' => $formData['is_hotdeal']
			)
		);

		$advancedGroup->add(
			'is_home',
			'checkbox',
			array(
				'label' => 'product.product_form.show_on_home_page',
				'value' => 'Yes',
				'current_value' => $formData['is_home']
			)
		);

		$advancedGroup->add(
			'is_stealth',
			'checkbox',
			array(
				'label' => 'product.product_form.is_stealth',
				'value' => '1',
				'current_value' => $formData['is_stealth'],
				'note' => 'product.product_form.is_stealth_tooltip'
			)
		);

		if (count($manufacturerOptions) > 0)
		{
			$advancedGroup->add(
				'manufacturer_id',
				'choice',
				array(
					'label' => 'product.product_form.manufacturer',
					'value' => $formData['manufacturer_id'],
					'options' => $manufacturerOptions,
					'empty_value' => array('0' => 'common.na'),
					'wrapperClass' => 'clear-both'
				)
			);
		}
		else
		{
			$advancedGroup->add(
				'manufacturer_id',
				'hidden',
				array('value' => $formData['manufacturer_id'])
			);
		}

		$advancedGroup->add(
			'product_mpn',
			'text',
			array(
				'label' => 'product.product_form.manufacturer_product_number',
				'value' => $formData['product_mpn']
			)
		);

		// $advancedGroup->add('product_upc', 'text', array('label' => 'product.product_form.upc', 'value' => $formData['product_upc']));

		// $advancedGroup->add('image_url', 'text', array('label' => 'Primary Image Url', 'value' => $formData['image_url'], 'wrapperClass' => 'clear-both'));

		// advancedGroup->add('estimated_shipping_price', 'money', array('label' => 'Estimated Shipping Price', 'value' => $formData['estimated_shipping_price'] > -1 ? $formData['estimated_shipping_price'] : null));

		/**
		 * Inventory
		 */
		$advancedGroup->add(
			'product_sku',
			'text',
			array(
				'label' => 'product.product_form.sku',
				'value' => $formData['product_sku'],
				'wrapperClass' => 'clear-both'
			)
		);

		$advancedGroup->add(
			'product_gtin',
			'text',
			array(
				'label' => 'product.product_form.bar_code',
				'value' => $formData['product_gtin']
			)
		);

		$advancedGroup->add(
			'youtube_link',
			'text',
			array(
				'label' => 'product.product_form.youtube_link',
				'value' => $formData['youtube_link'],
				'wrapperClass' => 'clear-both'
			)
		);

		$advancedGroup->add(
			'inventory_control',
			'choice',
			array(
				'label' => 'product.product_form.inventory_tracking',
				'expanded' => true,
				'options' => array(
					new core_Form_Option('No', 'product.product_form.inventory_tracking_no'),
					new core_Form_Option('Yes', 'product.product_form.inventory_tracking_product_level'),
					//new core_Form_Option('AttrRuleInc', 'Track Inventory at attribute level - Track only defined combinations'),
					new core_Form_Option('AttrRuleExc', 'product.product_form.inventory_tracking_variants_level'),
				),
				'value' => $formData['inventory_control'],
				'wrapperClass' => 'clear-both'
			)
		);

		$advancedGroup->add(
			'inventory_rule',
			'choice',
			array(
				'label' => 'product.product_form.out_of_stock_method',
				'value' => $formData['inventory_rule'],
				'options' => array(
					new core_Form_Option('OutOfStock', 'product.product_form.out_of_stock_method_show'),
					new core_Form_Option('Hide', 'product.product_form.out_of_stock_method_hide'),
				),
				'wrapperClass' => 'clear-both'
			)
		);

		$advancedGroup->add(
			'stock',
			'integer',
			array(
				'label' => 'product.product_form.inventory_count',
				'value' => $formData['stock']
			)
		);

		$advancedGroup->add(
			'stock_warning',
			'numeric',
			array(
				'label' => 'product.product_form.inventory_count_notify',
				'value' => $formData['stock_warning']
			)
		);

		$advancedGroup->add(
			'zoom_option',
			'choice',
			array(
				'label' => 'product.image_zooming',
				'value' => $formData['zoom_option'],
				'options' => array(
					new core_Form_Option('global', 'product.product_zoom_options_list.global'),
					new core_Form_Option('none', 'product.product_zoom_options_list.none'),
					new core_Form_Option('zoom', 'product.product_zoom_options_list.zoom'),
					new core_Form_Option('magnify', 'product.product_zoom_options_list.magnify'),
					new core_Form_Option('magicthumb', 'product.product_zoom_options_list.magicthumb'),
					new core_Form_Option('imagelayover', 'product.product_zoom_options_list.imagelayover'),
				),
				'wrapperClass' => 'clear-both'
			)
		);

		return $advancedGroup;
	}

	/**
	 * SEO properties
	 *
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getSeoGroup($formData)
	{
      	$seoGroup = new core_Form_FormBuilder(
      		'group-seo',
	        array(
	        	'label' => 'product.product_form.seo',
		        'collapsible' => true
	        )
        );

		$seoGroup->add(
			'meta_title',
			'text',
			array(
				'label' => 'product.product_form.meta_title',
				'value' => $formData['meta_title'],
				'attr' => array('class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		// TODO: add hint / description
		$seoGroup->add(
			'url_custom',
			'text',
			array(
				'label' => 'product.product_form.product_url',
				'value' => $formData['url_custom'],
				'placeholder' => $formData['url_default'],
				'attr' => array(
					'class' => 'seo-preview'
				)
			)
		);

		$seoGroup->add(
			'meta_description',
			'textarea',
			array(
				'label' => 'product.product_form.meta_description',
				'value' => $formData['meta_description'],
				'attr' => array(
					'class' => 'seo-preview',
					'maxlength' => 156
				)
			)
		);

		$translator = Framework_Translator::getInstance();

		//  Our search results preview
		$seoGroup->add(
			'product_seo_preview',
			'seopreview',
			array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => trim($formData['url_custom']) == '' ? $formData['url_default'] : $formData['url_custom'],
					'isUrlHidden' => 'showing',
					'title' => trim($formData['meta_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['meta_title'],
					'description' => trim($formData['meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['meta_description'],
					'meta_title' => 'meta_title',
					'meta_description' => 'meta_description',
					'meta_url' => 'url_custom',
				)
			)
		);

		return $seoGroup;
	}

	/**
	 * Search Keywords properties
	 *
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getSearchKeywordsGroup($formData)
	{
		$group = new core_Form_FormBuilder(
			'group-search-keywords',
			array(
				'label' => 'product.product_form.search_keywords',
				'collapsible' => true
			)
		);

		$group->add(
			'search_keywords',
			'text',
			array(
				'attr' => array('class' => 'tokenfield'),
				'label' => 'product.product_form.search_keywords',
				'value' => $formData['search_keywords']
			)
		);

		return $group;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getWholesaleGroup($formData)
	{
		$wholesaleGroup = new core_Form_FormBuilder(
			'group-wholesale',
			array(
				'label' => 'product.product_form.wholesaler_discounts',
				'collapsible' => true
			)
		);

		$wholesaleGroup->add(
			'price_level_1',
			'money',
			array(
				'label' => 'product.product_form.level1_price',
				'value' => $formData['price_level_1']
			)
		);

		$maxLevels = intval($this->settings->get('WholesaleMaxLevels'));
		if ($maxLevels > 1)
		{
			$wholesaleGroup->add(
				'price_level_2',
				'money',
				array(
					'label' => 'product.product_form.level2_price',
					'value' => $formData['price_level_2']
				)
			);
		}

		if ($maxLevels > 2)
		{
			$wholesaleGroup->add(
				'price_level_3',
				'money',
				array(
					'label' => 'product.product_form.level3_price',
					'value' => $formData['price_level_3']
				)
			);
		}

		return $wholesaleGroup;
	}

	/**
	 * @param $productFamilies
	 * @param $selectedFamilies
	 * @return core_Form_FormBuilder
	 */
	protected function getRecommendedProductsGroup($productFamilies, $selectedFamilies)
	{
		$recommendedGroup = new core_Form_FormBuilder(
			'group-recommended',
			array(
				'label' => 'product.product_form.recommended',
				'collapsible' => true
			)
		);

		$recommendedGroup->add(
			'recommended',
			'template',
			array(
				'file' => 'templates/pages/product/edit-recommended.html',
				'value' => array(
					'families' => $productFamilies,
					'selected' => $selectedFamilies
				)
			)
		);

		return $recommendedGroup;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getExportGroup($formData)
	{
		$formBuilder = new core_Form_FormBuilder(
			'group-export',
			array(
				'label' => 'product.product_form.data_export_properties',
				'class' => 'ic-gear',
				'collapsible' => true
			)
		);

		$enabledApps = $this->db->selectAll('SELECT `key` FROM '.DB_PREFIX."enabled_apps WHERE `key` IN ('google-merchant', 'amazon', 'yahoo', 'ebay', 'pricegrabber', 'nextag')");

		if (count($enabledApps) < 1) return false;

		$keys = array();
		foreach ($enabledApps as $app)
		{
			$keys[$app['key']] = 1;
		}

		if (isset($keys['google-merchant'])) $formBuilder->add($this->getGoogleBaseGroup($formData));
		if (isset($keys['amazon'])) $formBuilder->add($this->getAmazonGroup($formData));
		if (isset($keys['ebay'])) $formBuilder->add($this->getEbayGroup($formData));
		if (isset($keys['yahoo'])) $formBuilder->add($this->getYahooGroup($formData));
		if (isset($keys['pricegrabber'])) $formBuilder->add($this->getPriceGrabberGroup($formData));
		if (isset($keys['nextag'])) $formBuilder->add($this->getNexTagGroup($formData));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getGoogleBaseGroup($formData)
	{
		$formBuilder = new core_Form_FormBuilder(
			'group-google-base',
			array(
				'label' => 'product.product_form.google_base',
				'class' => 'ic-google',
				'collapsible' => false,
				'first' => true
			)
		);

		$formBuilder->add(
			'google_item_condition',
			'choice',
			array(
				'label' => 'product.product_form.google_base_item_condition',
				'value' => $formData['google_item_condition'],
				'options' => array(
					new core_Form_Option('new', 'product.product_form.google_base_item_condition_new'),
					new core_Form_Option('used', 'product.product_form.google_base_item_condition_used'),
					new core_Form_Option('refurbished', 'product.product_form.google_base_item_condition_refurbished'),
				),
				'tooltip' => 'product.product_form.google_base_item_condition_tooltip'
			)
		);

		$formBuilder->add(
			'google_product_type',
			'text',
			array(
				'label' => 'product.product_form.google_product_category',
				'value' => $formData['google_product_type'],
				'tooltip' => trans('product.product_form.google_product_category_information') . '<a target="_new" href="http://www.google.com/support/merchants/bin/answer.py?answer=160081">' . trans('product.product_form.google_product_category_click_here') . '</a>'
			)
		);

		$formBuilder->add(
			'google_availability',
			'choice',
			array(
				'label' => 'product.product_form.google_availability',
				'value' => $formData['google_availability'],
				'options' => array(
					new core_Form_Option('inventory_control', 'product.product_form.google_availability_inventory_control'),
					new core_Form_Option('in stock', 'product.product_form.google_availability_in_stock'),
					new core_Form_Option('available for order', 'product.product_form.google_availability_available'),
					new core_Form_Option('out of stock', 'product.product_form.google_availability_out_of_stock'),
					new core_Form_Option('preorder', 'product.product_form.google_availability_preorder')
				),
				'tooltip' => trans('product.product_form.google_availability_tooltip1') . ' <a target="_new" href="http://www.google.com/support/merchants/bin/answer.py?answer=160081">' . trans('product.product_form.google_availability_tooltip2') . '</a><br/>' . trans('product.product_form.google_availability_tooltip3'),
			)
		);

		$formBuilder->add(
			'google_online_only',
			'choice',
			array(
				'label' => 'product.product_form.online_only',
				'value' => $formData['google_online_only'],
				'options' => array(
					new core_Form_Option('n', 'common.no'),
					new core_Form_Option('y', 'common.yes'),
				)
			)
		);

		$formBuilder->add(
			'google_gender',
			'choice',
			array(
				'label' => 'product.product_form.gender',
				'value' => $formData['google_gender'],
				'options' => array(
					new core_Form_Option('', ''),
					new core_Form_Option('unisex', 'product.product_form.gender_unisex'),
					new core_Form_Option('male', 'product.product_form.gender_male'),
					new core_Form_Option('female', 'product.product_form.gender_female'),
				)
			)
		);

		$formBuilder->add(
			'google_age_group',
			'choice',
			array(
				'label' => 'product.product_form.age_group',
				'value' => $formData['google_age_group'],
				'options' => array(
					new core_Form_Option('', ''),
					new core_Form_Option('adult', 'product.product_form.age_group_adult'),
					new core_Form_Option('kids', 'product.product_form.age_group_kids'),
				)
			)
		);

		$formBuilder->add(
			'google_color',
			'text',
			array(
				'label' => 'product.product_form.color',
				'value' => $formData['google_color']
			)
		);

		$formBuilder->add(
			'google_size',
			'text',
			array(
				'label' => 'product.product_form.size',
				'value' => $formData['google_size']
			)
		);

		$formBuilder->add(
			'google_material',
			'text',
			array(
				'label' => 'product.product_form.material',
				'value' => $formData['google_material']
			)
		);

		$formBuilder->add(
			'google_pattern',
			'text',
			array(
				'label' => 'product.product_form.pattern',
				'value' => $formData['google_pattern']
			)
		);

		return $formBuilder;

	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getAmazonGroup($formData)
	{
		$formBuilder = new core_Form_FormBuilder(
			'group-amazon',
			array(
				'label' => 'product.product_form.amazon',
				'collapsible' => false
			)
		);

		$formBuilder->add(
			'amazon_id',
			'text',
			array(
				'label' => 'product.product_form.amazon_product_id',
				'value' => $formData['amazon_id'],
				'tooltip' => 'product.product_form.amazon_product_id_tooltip'
			)
		);

		$formBuilder->add(
			'amazon_id_type',
			'choice',
			array(
				'label' => 'product.product_form.amazon_product_id_type',
				'value' => $formData['amazon_id_type'],
				'options' => array(
					new core_Form_Option('1', 'product.product_form.amazon_product_id_type_asin'),
					new core_Form_Option('2', 'product.product_form.amazon_product_id_type_isbn'),
					new core_Form_Option('3', 'product.product_form.amazon_product_id_type_upc'),
					new core_Form_Option('4', 'product.product_form.amazon_product_id_type_ean'),
				),
				'tooltip' => 'product.product_form.amazon_product_id_type_tooltip'
			)
		);

		$formBuilder->add(
			'amazon_item_condition',
			'choice',
			array(
				'label' => 'product.product_form.amazon_item_condition',
				'value' => $formData['amazon_item_condition'],
				'options' => array(
					new core_Form_Option('1','product.product_form.amazon_item_condition1'),
					new core_Form_Option('2','product.product_form.amazon_item_condition2'),
					new core_Form_Option('3','product.product_form.amazon_item_condition3'),
					new core_Form_Option('4','product.product_form.amazon_item_condition4'),
					new core_Form_Option('5','product.product_form.amazon_item_condition5'),
					new core_Form_Option('6','product.product_form.amazon_item_condition6'),
					new core_Form_Option('7','product.product_form.amazon_item_condition7'),
					new core_Form_Option('8','product.product_form.amazon_item_condition8'),
					new core_Form_Option('9','product.product_form.amazon_item_condition9'),
					new core_Form_Option('10','product.product_form.amazon_item_condition10'),
					new core_Form_Option('11','product.product_form.amazon_item_condition11')
				),
				'tooltip' => 'product.product_form.amazon_item_condition_tooltip'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getEbayGroup($formData)
	{
		$formBuilder = new core_Form_FormBuilder(
			'group-ebay',
			array(
				'label' => 'product.product_form.ebay',
				'collapsible' => false
			)
		);

		$formBuilder->add(
			'ebay_cat_id',
			'integer',
			array(
				'label' => 'product.product_form.ebay_category',
				'value' => $formData['ebay_cat_id'],
				'tooltip' => trans('product.product_form.ebay_category_tooltip1') . ' <a href="http://listings.ebay.com/_W0QQloctZShowCatIdsQQsacatZQ2d1QQsalocationZatsQQsocmdZListingCategoryList?loct=2" target="_blank">' . trans('product.product_form.ebay_category_tooltip2') . '</a>'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getYahooGroup($formData)
	{
		$formBuilder = new core_Form_FormBuilder(
			'group-yahoo',
			array(
				'label' => 'product.product_form.yahoo',
				'collapsible' => false
			)
		);

		$formBuilder->add(
			'yahoo_path',
			'text',
			array('label' => 'product.product_form.yahoo_path',
				'value' => $formData['yahoo_path'],
				'tooltip' => 'product.product_form.yahoo_path_tooltip'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getPriceGrabberGroup($formData)
	{
		$formBuilder = new core_Form_FormBuilder(
			'price-grabber',
			array(
				'label' => 'product.product_form.pricegrabber',
				'collapsible' => false
			)
		);

		$formBuilder->add(
			'pricegrabber_category',
			'text',
			array(
				'label' => 'product.product_form.pricegrabber_category',
				'value' => $formData['pricegrabber_category'],
				'tooltip' => trans('product.product_form.pricegrabber_category_tooltip1') . ' <a href="http://www.pricegrabber.com/home_channelindex.php" target="_blank">' . trans('product.product_form.pricegrabber_category_tooltip2') . '</a>'
			)
		);

		$formBuilder->add(
			'pricegrabber_part_number',
			'text',
			array(
				'label' => 'product.product_form.pricegrabber_part_number',
				'value' => $formData['pricegrabber_part_number'],
				'tooltip' => 'product.product_form.pricegrabber_part_number_tooltip'
			)
		);

		$formBuilder->add(
			'pricegrabber_item_condition',
			'choice',
			array(
				'label' => 'product.product_form.pricegrabber_condition',
				'value' => $formData['pricegrabber_item_condition'],
				'options' => array(
					new core_Form_Option('New', 'product.product_form.pricegrabber_condition_new'),
					new core_Form_Option('Like New', 'product.product_form.pricegrabber_condition_like_new'),
					new core_Form_Option('Used', 'product.product_form.pricegrabber_condition_used'),
					new core_Form_Option('Refurbished', 'product.product_form.pricegrabber_condition_refurbished'),
					new core_Form_Option('Generic', 'product.product_form.pricegrabber_condition_generic')
				),
				'tooltip' => 'product.product_form.pricegrabber_condition_tooltip'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getNexTagGroup($formData)
	{
		$formBuilder = new core_Form_FormBuilder(
			'group-nextag',
			array(
				'label' => 'product.product_form.nextag',
				'collapsible' => false
			)
		);

		$formBuilder->add(
			'nextag_category',
			'text',
			array(
				'label' => 'product.product_form.nextag_catalog',
				'value' => $formData['nextag_category'],
				'tooltip' => 'Enter the Nextag category. You may also use your own although Nextag does not recommend it. <a href="http://merchants.nextag.com/serv/main/buyer/BulkCategoryCodes.jsp" target="_blank">See a list of Nextag categories</a>'
			)
		);

		$formBuilder->add(
			'nextag_part_number',
			'text',
			array(
				'label' => 'product.product_form.nextag_part_number',
				'value' => $formData['nextag_part_number'],
				'tooltip' => 'product.product_form.nextag_part_number_tooltip'
			)
		);

		$formBuilder->add(
			'nextag_item_condition', 'choice',
			array(
				'label' => 'product.product_form.nextag_condition',
				'value' => $formData['nextag_item_condition'],
				'options' => array(
					new core_Form_Option('New', 'product.product_form.nextag_condition_new'),
					new core_Form_Option('Open Box', 'product.product_form.nextag_condition_open_box'),
					new core_Form_Option('OEM', 'product.product_form.nextag_condition_oem'),
					new core_Form_Option('Refurbished', 'product.product_form.nextag_condition_refurbished'),
					new core_Form_Option('Pre-Owne', 'product.product_form.nextag_condition_preowne'),
					new core_Form_Option('Like New', 'product.product_form.nextag_condition_like_new'),
					new core_Form_Option('Good', 'product.product_form.nextag_condition_good'),
					new core_Form_Option('Very Good', 'product.product_form.nextag_condition_very_good'),
					new core_Form_Option('Acceptable', 'product.product_form.nextag_condition_acceptable')
				),
				'tooltip' => 'product.product_form.nextag_condition_tooltip'
			)
		);

		return $formBuilder;
	}

	/**
	 * Product attributes
	 *
	 * @param $formData
	 * @param $productAttributes
	 * @return core_Form_FormBuilder
	 */
	protected function getAttributesGroup($formData, $productAttributes)
	{
		$attributesGroup = new core_Form_FormBuilder(
			'group-attributes',
			array(
				'label' => 'product.product_form.attributes',
				'collapsible' => true
			)
		);

		$attributesGroup->add(
			'attributes',
			'template',
			array(
				'file' => 'templates/pages/product/edit-attributes.html',
				'value' => json_encode($productAttributes)
			)
		);

		return $attributesGroup;
	}

	/**
	 * Product variants
	 *
	 * @param $formData
	 * @param $productVariants
	 * @return core_Form_FormBuilder
	 */
	protected function getVariantsGroup($formData, $productVariants)
	{
		$variantsGroup = new core_Form_FormBuilder(
			'group-variants',
			array(
				'label' => 'product.product_form.variants',
				'collapsible' => true
			)
		);

		$variantsGroup->add(
			'variants',
			'template',
			array(
				'file' => 'templates/pages/product/edit-variants.html',
				'value' => json_encode($productVariants)
			)
		);

		return $variantsGroup;
	}

	/**
	 * @param $formData
	 * @param $productFeaturesGroups
	 * @return core_Form_FormBuilder
	 */
	protected function getProductFeaturesGroupsGroup($formData, $productFeaturesGroups)
	{
		$productFeaturesGroup = new core_Form_FormBuilder(
			'group-products-features',
			array(
				'label' => 'product.product_form.features',
				'collapsible' => true
			)
		);

		$productFeaturesGroup->add(
			'products-features-groups',
			'template',
			array(
				'file' => 'templates/pages/product/edit-products-features-groups.html',
				'value' => json_encode($productFeaturesGroups)
			)
		);

		return $productFeaturesGroup;
	}

	/**
	 * Product promotions
	 *
	 * @param $formData
	 * @param $productPromotions
	 *
	 * @return core_Form_FormBuilder
	 */
	protected function getPromotionsGroup($formData, $productPromotions)
	{
		$promoGroup = new core_Form_FormBuilder(
			'group-promotions',
			array(
				'label' => 'product.product_form.product_promotions',
				'collapsible' => true
			)
		);

		$promoGroup->add(
			'gift_quantity',
			'integer',
			array(
				'label' => 'product.product_form.product_promotions_minimum_count',
				'value' => $formData['gift_quantity'],
				'tooltip' => 'product.product_form.product_promotions_minimum_count_tooltip'
			)
		);

		$promoGroup->add(
			'promotions',
			'template',
			array(
				'file' => 'templates/pages/product/edit-promotions.html',
				'value' => json_encode($productPromotions)
			)
		);

		return $promoGroup;
	}

	/**
	 * Quantity Discounts
	 *
	 * @param $formData
	 * @param $productQuantityDiscounts
	 *
	 * @return core_Form_FormBuilder
	 */
	protected function getQuantityDiscountsGroup($formData, $productQuantityDiscounts)
	{
		$qdGroup = new core_Form_FormBuilder(
			'group-qd',
			array(
				'label' => 'product.quantity_discounts.quantity_discounts',
				'collapsible' => true
			)
		);

		$qdGroup->add(
			'quantity-discounts',
			'template',
			array(
				'file' => 'templates/pages/product/edit-quantity-discounts.html',
				'value' => json_encode($productQuantityDiscounts),
				'required' => true,
			)
		);

		return $qdGroup;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	protected function getRecurringBillingGroup($formData)
	{
		$rbd = isset($formData['recurring_billing_data']) && is_array($formData['recurring_billing_data']) ? $formData['recurring_billing_data'] : array();

		$recurringGroup = new core_Form_FormBuilder(
			'group-recurring',
			array(
				'label' => 'product.recurring.recurring_billing',
				'collapsible' => true
			)
		);

		$recurringBillingData = new RecurringBilling_Model_ProductRecurringBillingData($rbd);

		$periodUnitOptions = array(
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_DAY => 'product.recurring.period_unit_options.Day',
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_WEEK => 'product.recurring.period_unit_options.Week',
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_MONTH => 'product.recurring.period_unit_options.Month',
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_YEAR => 'product.recurring.period_unit_options.Year'
		);

		/**
		 * Enable / disable recurring billing
		 */
		$recurringGroup->add(
			'enable_recurring_billing',
			'checkbox',
			array(
				'label' => 'product.recurring.enable_recurring_billing',
				'note' => 'product.recurring.enable_recurring_billing_note',
				'value' => 1,
				'current_value' => $formData['enable_recurring_billing']
			)
		);

		$recurringGroup->add(
			'recurring_billing_data[schedule_description]',
			'text',
			array(
				'label' => 'product.recurring.schedule_description',
				'value' => isset($rbd['schedule_description']) ? $recurringBillingData->getScheduleDescription() : '',
				'note' => 'product.recurring.notes.schedule_description'
			)
		);

		$periodCyclesOptions = array();
		for ($i = 1; $i <= 25; $i++) $periodCyclesOptions[$i] = $i.' time'.($i > 1 ? 's' : '');

		/**
		 * Trial properties
		 */
		$trialPeriodFrequencyAndUnitOptions = array(
			'1-Day' => trans('product.product_form.trial_option_1day'),
			'2-Day' => trans('product.product_form.trial_option_2days'),
			'3-Day' => trans('product.product_form.trial_option_3days'),
			'4-Day' => trans('product.product_form.trial_option_4days'),
			'5-Day' => trans('product.product_form.trial_option_5days'),
			'6-Day' => trans('product.product_form.trial_option_6days'),
			'1-Week' => trans('product.product_form.trial_option_1week'),
			'10-Day' => trans('product.product_form.trial_option_10days'),
			'2-Week' => trans('product.product_form.trial_option_2weeks'),
			'15-Day' => trans('product.product_form.trial_option_15days'),
			'20-D' => trans('product.product_form.trial_option_20days'),
			'3-Week' => trans('product.product_form.trial_option_3weeks'),
			'4-Week' => trans('product.product_form.trial_option_4weeks'),
			'30-Day' => trans('product.product_form.trial_option_30days'),
			'1-Month' => trans('product.product_form.trial_option_1month'),
			'45-Day' => trans('product.product_form.trial_option_45days'),
			'2-Month' => trans('product.product_form.trial_option_2months'),
			'3-Month' => trans('product.product_form.trial_option_3months'),
			'4-Month' => trans('product.product_form.trial_option_4months'),
			'5-Month' => trans('product.product_form.trial_option_5months'),
			'6-Month' => trans('product.product_form.trial_option_6months'),
			'7-Month' => trans('product.product_form.trial_option_7months'),
			'8-Month' => trans('product.product_form.trial_option_8months'),
			'9-Month' => trans('product.product_form.trial_option_9months'),
			'10-Month' => trans('product.product_form.trial_option_10months'),
			'11-Month' => trans('product.product_form.trial_option_11months'),
			'1-Year' => trans('product.product_form.trial_option_1year'),
			'18-Month' => trans('product.product_form.trial_option_18months'),
			'2-Year' => trans('product.product_form.trial_option_2years'),
			'3-Year' => trans('product.product_form.trial_option_3years'),
			'4-Year' => trans('product.product_form.trial_option_4years'),
			'5-Year' => trans('product.product_form.trial_option_5years')
		);

		$recurringGroup->add(
			'recurring_billing_data[trial_enabled]',
			'checkbox',
			array(
				'label' => 'product.recurring.trial_enabled',
				'note' => 'product.recurring.notes.trial_enabled',
				'value' => '1',
				'current_value' => $recurringBillingData->getTrialEnabled()
			)
		);

		$periodUnitOptionsTrial = array();
		$periodUnitOptionsTrial = array_merge($periodUnitOptionsTrial, $periodUnitOptions);

		$recurringGroup->add(
			'recurring_billing_data[trial]',
			'recurring-settings',
			array(
				'label' => 'product.recurring.trial_settings',
				'value' => array(
					'amount' => $recurringBillingData->getTrialAmount(),
					'period_unit' => $recurringBillingData->getTrialPeriodUnit(),
					'period_frequency' => $recurringBillingData->getTrialPeriodFrequency(),
					'period_cycles' => $recurringBillingData->getTrialPeriodCycles()
				),
				'attr' => array('data-recurring-required' => '1'),
				'periodUnitOptions' => $periodUnitOptionsTrial,
				'periodFrequencyAndUnitOptions' => $trialPeriodFrequencyAndUnitOptions,
				'periodCyclesOptions' => $periodCyclesOptions,
				'custom' => $recurringBillingData->getTrialCustom(),
				'trial' => true,
				'wrapperClass' => 'recurring-billing-data-trial-wrapper clear-both',
				'note' => 'product.recurring.notes.trial_period_settings'
			)
		);

		/**
		 * Billing properties
		 */
		$billingPeriodFrequencyAndUnitOptions = array(
			'1-Day' => trans('product.product_form.billing_option_daily'),
			'2-Day' => trans('product.product_form.billing_option_every2days'),
			'3-Day' => trans('product.product_form.billing_option_every3days'),
			'4-Day' => trans('product.product_form.billing_option_every4days'),
			'5-Day' => trans('product.product_form.billing_option_every5days'),
			'6-Day' => trans('product.product_form.billing_option_every6days'),
			'1-Week' => trans('product.product_form.billing_option_weekly'),
			'10-Day' => trans('product.product_form.billing_option_every10days'),
			'2-Week' => trans('product.product_form.billing_option_biweekly'),
			'15-Day' => trans('product.product_form.billing_option_every15days'),
			'20-D' => trans('product.product_form.billing_option_every20days'),
			'3-Week' => trans('product.product_form.billing_option_triweekly'),
			'4-Week' => trans('product.product_form.billing_option_every4weeks'),
			'30-Day' => trans('product.product_form.billing_option_every30days'),
			'1-Month' => trans('product.product_form.billing_option_monthly'),
			'45-Day' => trans('product.product_form.billing_option_every45days'),
			'2-Month' => trans('product.product_form.billing_option_every2months'),
			'3-Month' => trans('product.product_form.billing_option_every3months'),
			'4-Month' => trans('product.product_form.billing_option_every4months'),
			'5-Month' => trans('product.product_form.billing_option_every5months'),
			'6-Month' => trans('product.product_form.billing_option_every6months'),
			'7-Month' => trans('product.product_form.billing_option_every7months'),
			'8-Month' => trans('product.product_form.billing_option_every8months'),
			'9-Month' => trans('product.product_form.billing_option_every9months'),
			'10-Month' => trans('product.product_form.billing_option_every10months'),
			'11-Month' => trans('product.product_form.billing_option_every11months'),
			'1-Year' => trans('product.product_form.billing_option_annually'),
			'18-Month' => trans('product.product_form.billing_option_every18months'),
			'2-Year' => trans('product.product_form.billing_option_every2years'),
			'3-Year' => trans('product.product_form.billing_option_every3years'),
			'4-Year' => trans('product.product_form.billing_option_every4years'),
			'5-Year' => trans('product.product_form.billing_option_every5years')
		);

		$periodCyclesOptions[0] = trans('product.product_form.until_cancelled');
		ksort($periodCyclesOptions);

		$recurringGroup->add(
			'recurring_billing_data[billing]',
			'recurring-settings',
			array(
				'label' => 'product.recurring.billing_settings',
				'value' => array(
					'amount' => $recurringBillingData->getBillingAmount(),
					'period_unit' => $recurringBillingData->getBillingPeriodUnit(),
					'period_frequency' => $recurringBillingData->getBillingPeriodFrequency() > 0 ? $recurringBillingData->getBillingPeriodFrequency() : '',
					'period_cycles' => $recurringBillingData->getBillingPeriodCycles() > 0 ? $recurringBillingData->getBillingPeriodCycles() : ''
				),
				'attr' => array('data-recurring-required' => '1'),
				'periodUnitOptions' => $periodUnitOptions,
				'periodFrequencyAndUnitOptions' => $billingPeriodFrequencyAndUnitOptions,
				'periodCyclesOptions' => $periodCyclesOptions,
				'custom' => $recurringBillingData->getBillingCustom(),
				'trial' => false,
				'wrapperClass' => 'clear-both'
			)
		);

		/**
		 * First payment delay
		 */
		$startDateDelayUnitOptions = array(
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_DAY =>  'product.recurring.delay_unit_options.Day',
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_WEEK => 'product.recurring.delay_unit_options.Week',
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_MONTH => 'product.recurring.delay_unit_options.Month',
			RecurringBilling_Model_ProductRecurringBillingData::UNIT_YEAR => 'product.recurring.delay_unit_options.Year'
		);

		$recurringGroup->add(
			'recurring_billing_data[start_date_delay]',
			'date-delay',
			array(
				'label' => 'product.recurring.start_date_delay',
				'value' => array(
					'delay' => $recurringBillingData->getStartDateDelay(),
					'delay_unit' => $recurringBillingData->getStartDateDelayUnit()
				),
				'delayUnitOptions' => $startDateDelayUnitOptions,
				'note' => 'product.recurring.notes.start_date_delay',
				'wrapperClass' => 'clear-both'
			)
		);

		/**
		 * Max payment failures
		 */
		$recurringGroup->add(
			'recurring_billing_data[max_payment_failures]',
			'choice',
			array(
				'label' => 'product.recurring.max_payment_failures',
				'value' => intval($recurringBillingData->getMaxPaymentFailures()),
				'attr' => array('class' => 'short'),
				'options' => array(1, 2, 3, 4, 5, 6, 7),
				'note' => 'product.recurring.notes.max_payment_failures'
			)
		);

		/**
		 * Auto-complete order settings
		 */
		$autoCompleteOptions = array(
			'auto_complete_initial_order' => 'product.recurring.auto_complete_initial_order',
			'auto_complete_recurring_orders' => 'product.recurring.auto_complete_recurring_orders'
		);

		$autoCompleteOptionsValue = array();
		if ($recurringBillingData->getAutoCompleteInitialOrder()) $autoCompleteOptionsValue['auto_complete_initial_order'] = 'auto_complete_initial_order';
		if ($recurringBillingData->getAutoCompleteRecurringOrders()) $autoCompleteOptionsValue['auto_complete_recurring_orders'] = 'auto_complete_recurring_orders';
		$recurringGroup->add(
			'recurring_billing_data[auto_complete_options][]',
			'choice',
			array(
				'label' => 'product.recurring.auto_complete_orders',
				'options' => $autoCompleteOptions,
				'value' => $autoCompleteOptionsValue,
				'multiple' => true,
				'expanded' => true
			)
		);

		/**
		 * Modification rules
		 */
		$modificationOptions = array(
			'customer_can_suspend' => 'product.recurring.customer_can_suspend',
			'customer_can_cancel' => 'product.recurring.customer_can_cancel',
			'admin_can_adjust_billing' => 'product.recurring.admin_can_adjust_billing',
			'admin_can_adjust_trial' => 'product.recurring.admin_can_adjust_trial',
		);
		$modificationOptionsValue = array();
		if ($recurringBillingData->getCustomerCanSuspend()) $modificationOptionsValue['customer_can_suspend'] = 'customer_can_suspend';
		if ($recurringBillingData->getCustomerCanCancel()) $modificationOptionsValue['customer_can_cancel'] = 'customer_can_cancel';
		if ($recurringBillingData->getAdminCanAdjustBilling()) $modificationOptionsValue['admin_can_adjust_billing'] = 'admin_can_adjust_billing';
		if ($recurringBillingData->getAdminCanAdjustTrial()) $modificationOptionsValue['admin_can_adjust_trial'] = 'admin_can_adjust_trial';

		$recurringGroup->add(
			'recurring_billing_data[modification_rules][]',
			'choice',
			array(
				'label' => 'product.recurring.modification_rules',
				'options' => $modificationOptions,
				'value' => $modificationOptionsValue,
				'multiple' => true,
				'expanded' => true
			)
		);

		/**
		 * Notifications
		 */
		$customerNotificationOptions = array(
			'created' => 'product.recurring.customer_notification_options.created',
			'failure' => 'product.recurring.customer_notification_options.failure',
			'expire' => 'product.recurring.customer_notification_options.expire',
			'one_remaining' => 'product.recurring.customer_notification_options.one_remaining',
			'suspended' => 'product.recurring.customer_notification_options.suspended',
			'reactivated' => 'product.recurring.customer_notification_options.reactivated',
			'canceled' => 'product.recurring.customer_notification_options.canceled',
			'completed' => 'product.recurring.customer_notification_options.completed',
		);

		$recurringGroup->add(
			'recurring_billing_data[customer_notifications][]',
			'choice',
			array(
				'label' => 'product.recurring.customer_notifications',
				'options' => $customerNotificationOptions,
				'value' => $recurringBillingData->getCustomerNotifications(),
				'multiple' => true,
				'expanded' => true,
				'wrapperClass' => 'col-md-6'
			)
		);

		$adminNotificationOptions = array(
			'created' => 'product.recurring.admin_notification_options.created',
			'failure' => 'product.recurring.admin_notification_options.failure',
			'expire' => 'product.recurring.admin_notification_options.expire',
			'one_remaining' => 'product.recurring.admin_notification_options.one_remaining',
			'suspended' => 'product.recurring.admin_notification_options.suspended',
			'reactivated' => 'product.recurring.admin_notification_options.reactivated',
			'canceled' => 'product.recurring.admin_notification_options.canceled',
			'completed' => 'product.recurring.admin_notification_options.completed',
		);

		/** @var core_Form_FormBuilder $formBuilder */
		//$formBuilder = new core_Form_FormBuilder('recurring-admin-notifications', array('label' => 'product.recurring.admin_notifications', 'class' => '', 'collapsible'=>true));
		//$recurringGroup->add($formBuilder);
		$recurringGroup->add(
			'recurring_billing_data[admin_notifications][]',
			'choice',
			array(
				'label' => 'product.recurring.admin_notifications',
				'options' => $adminNotificationOptions,
				'value' => $recurringBillingData->getAdminNotifications(),
				'multiple' => true,
				'expanded' => true,
				'wrapperClass' => 'col-md-6'
			)
		);

		return $recurringGroup;
	}

	/**
	 * @param $formData
	 * @param $mode
	 * @param $params
	 * @param null $id
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $params, $id = null)
	{
		return $this->getBuilder($formData, $mode, $params, $id)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getProductSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add(
			'searchParams[search_str]',
			'text',
			array(
				'label' => trans('product.product_form.product_id_or_title'),
				'value' => $data['search_str'],
				'wrapperClass' => 'col-sm-12 col-md-6'
			)
		);

		$formBuilder->add(
			'searchParams[cid]',
			'choice',
			array(
				'label' => trans('product.product_form.product_category'),
				'value' => $data['cid'],
				'options' => $data['categories'],
				'wrapperClass' => 'col-sm-12 col-md-3'
			)
		);

		$formBuilder->add(
			'searchParams[status]',
			'choice',
			array(
				'label' => trans('product.product_form.product_status'),
				'value' => $data['status'],
				'options' => array(
					'any' => 'Any',
					'Yes' => 'Available',
					'No' => 'Unavailable',
					'OutOfStock' => 'Out of stock',
					'StockWarning' => 'Stock warning',
				),
				'wrapperClass' => 'col-sm-12 col-md-3'
			)
		);

		$formBuilder->add(
			'orderBy',
			'hidden',
			array('value' => $data['orderBy']
			)
		);

		$formBuilder->add(
			'orderDir',
			'hidden',
			array('value' => $data['orderDir']
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getProductSearchForm($data)
	{
		return $this->getProductSearchFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getProductsFeaturesFormBuilder($data)
	{
		$productFeatureFormBuilder = new core_Form_FormBuilder('productFamily');
		if (isset($data['product_features']))
		{
			$productFeatureFormBuilder->add('note', 'static', array('wrapperClass' => 'col-xs-12 col-md-12', 'label' => '', 'value' => 'You are allowed to add an unlimited number features options. Features options allow you to give your customer\'s a better product filter search.'));
			foreach ($data['product_features'] AS $feature => $values)
			{
				foreach ($values AS $key => $value)
				{
					$productFeatureFormBuilder->add(
						'productFeature[' . $feature . ']',
						'text',
						array(
							'attr' => array(
								'data-pfgrid' => $key,
								'data-fgprid' => $data['fgprid'],
								'data-pid' => $data['pid'],
								'data-value' => str_replace('"', "'", json_encode($value)),
								'class' => 'tokenfield'
							),
							'label' => ucfirst($feature),
							'wrapperClass' => 'col-xs-12 col-md-12',
						)
					);
				}
			}
		}
		else
		{
			$productFeatureFormBuilder->add('no_assigned_feature', 'static', array('wrapperClass' => 'col-xs-12 col-md-12', 'label' => '', 'value' => 'Current selected product feature group has no assigned product feature.'));
		}

		return $productFeatureFormBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getProductsFeaturesForm($data)
	{
		return $this->getProductsFeaturesFormBuilder($data)->getForm();
	}
}

<?php

/**
 * Class Admin_Form_StampsForm
 */
class Admin_Form_StampsForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getStampsActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('StampscomUsername', 'text', array('label' => 'stamps.username', 'required' => true, 'value' => $data['StampscomUsername']));
        $settingsGroup->add('StampscomPassword', 'password', array('label' => 'stamps.password', 'required' => true, 'value' => $data['StampscomPassword']));
        if (defined('DEVMODE') && DEVMODE)
            $settingsGroup->add('StampscomTestMode', 'checkbox', array('label' => 'stamps.test_mode', 'value' => 'Yes', 'current_value' => $data['StampscomTestMode']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getStampsActivateForm($data)
    {
        return $this->getStampsActivateBuilder($data)->getForm();
    }
}
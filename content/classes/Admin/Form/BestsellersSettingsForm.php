<?php

/**
 * Class Admin_Form_BestsellersSettingsForm
 */
class Admin_Form_BestsellersSettingsForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        /**
         * Catalog settings
         */
        $bestsellersGroup = new core_Form_FormBuilder('bestsellers', array('label' => 'bestsellers.settings'));
        $formBuilder->add($bestsellersGroup);

        $bestsellersGroup->add('CatalogBestsellersAvailable', 'checkbox', array('label' => 'bestsellers.enable', 'value' => 'YES', 'current_value' => $data['CatalogBestsellersAvailable']));
        $bestsellersGroup->add('CatalogBestsellersCount', 'choice', array('label' => 'bestsellers.max_display', 'value' => $data['CatalogBestsellersCount'], 'wrapperClass' => 'clear-both', 'options' => array(
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
        )));
        $bestsellersGroup->add('CatalogBestsellersPeriod', 'choice', array('label' => 'bestsellers.period', 'value' => $data['CatalogBestsellersPeriod'], 'options' => array(
            'Month' => 'bestsellers.period_values.month',
            '2 Months' => 'bestsellers.period_values.2_months',
            '3 Months' => 'bestsellers.period_values.3_months',
            '6 Months' => 'bestsellers.period_values.6_months',
            'Year' => 'bestsellers.period_values.year',
        )));

        $bestsellersGroup->add('CustomerAlsoBoughtAvailable', 'checkbox', array('label' => 'customers_also_bought.enable', 'value' => 'YES', 'current_value' => $data['CustomerAlsoBoughtAvailable'], 'wrapperClass' => 'clear-both'));
        $bestsellersGroup->add('CustomerAlsoBoughtCount', 'text', array('label' => 'customers_also_bought.max_display', 'value' => $data['CustomerAlsoBoughtCount'], 'wrapperClass' => 'clear-both'));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
<?php

/**
 * Class Admin_Form_SnippetForm
 */
class Admin_Form_SnippetForm
{
	/**
	 * @param $formData
	 * @param $mode
	 * @param null $id
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder(
			'basic',
			array(
				'label'=>'Snippet Details'
			)
		);

		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'name',
			'text',
			array(
				'label' => 'snippet.name',
				'value' => $formData['name'],
				'required' => true,
				'attr' => array(
					'maxlength' => '255',
					'class' => 'generate-id-source-field'
				)
			)
		);

		$attr = array(
			'maxlength' => '255',
			'class' => 'generated-id-field'
		);
		if ($mode == 'add') $attr['class'] =  'generated-id-field autogenerate';

		$basicGroup->add(
			'snippet_id',
			'text',
			array(
				'label' => 'snippet.snippet_id',
				'value' => $formData['snippet_id'],
				'required' => true,
				'attr' => $attr
			)
		);

		$priorityOptions = array();
		for ($i = 1; $i <= 100; $i++)
		{
			$priorityOptions[] = new core_Form_Option($i, $i);
		}

		$basicGroup->add(
			'priority',
			'choice',
			array(
				'label' => 'snippet.priority',
				'value' =>  $formData['priority'],
				'options' => $priorityOptions
			)
		);

		$basicGroup->add(
			'language',
			'choice',
			array(
				'label' => 'snippet.language',
				'value' => $formData['language'],
				'options' => array(
					'javascript' => 'Javascript',
					'css' => 'Css'
				)
			)
		);

		$basicGroup->add(
			'type',
			'choice',
			array(
				'label' => 'snippet.type',
				'value' => $formData['type'],
				'options' => array(
					'include' => 'Include',
					'chunk' => 'Chunk'
				)
			)
		);

		$basicGroup->add(
			'page_types[]',
			'choice',
			array(
				'label' => 'snippet.page_types',
				'required' => true,
				'value' => explode(',', $formData['page_types']),
				'multiple' => true,
				'options' => array(
					'all' => trans('snippet.page_type_options.all'),
					'home' => trans('snippet.page_type_options.home'),
					'catalog' => trans('snippet.page_type_options.catalog'),
					'product' => trans('snippet.page_type_options.product'),
					'one_page_checkout' => trans('snippet.page_type_options.one_page_checkout'),
					'completed' => trans('snippet.page_type_options.completed'),
				)
			)
		);

		$basicGroup->add(
			'is_visible',
			'checkbox',
			array(
				'label' => 'snippet.is_visible',
				'value' => 'Yes',
				'current_value' => $formData['is_visible'],
				'wrapperClass' => 'clear-both'
			)
		);

		$basicGroup->add(
			'content',
			'textarea',
			array(
				'label' => 'snippet.content',
				'value' => htmlspecialchars($formData['content']),
				'required' => true
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @param $mode
	 * @param null $id
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}
}
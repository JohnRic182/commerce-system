<?php

/**
 * Class Admin_Form_BongoCheckoutForm
 */
class Admin_Form_BongoCheckoutForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');
        $settingsGroup = new core_Form_FormBuilder('settings');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('bongocheckout_Shipping_Method', 'choice', array(
            'label' => 'apps.bongo_checkout.domestic_shipping_method',
            'value' => $data['bongocheckout_Shipping_Method'],
            'options' => $data['shippingMethodOptions'],
        ));

        $settingsGroup->add('bongocheckout_Shipping_Country_Group', 'choice', array(
            'label' => 'apps.bongo_checkout.country_group',
            'value' => $data['bongocheckout_Shipping_Country_Group'],
            'options' => $data['shippingCountryGroupOptions'],
        ));

        $addressGroup = new core_Form_FormBuilder('address', array('label' => 'apps.bongo_checkout.bongo_dist_address'));
        $formBuilder->add($addressGroup);

        $addressGroup->add('bongocheckout_Company_Name', 'text', array('label' => 'apps.bongo_checkout.company_name', 'value' => $data['bongocheckout_Company_Name']));
        $addressGroup->add('bongocheckout_Attention_To', 'text', array('label' => 'apps.bongo_checkout.attention_to', 'value' => $data['bongocheckout_Attention_To']));
        $addressGroup->add('bongocheckout_Address1', 'text', array('label' => 'apps.bongo_checkout.address1', 'value' => $data['bongocheckout_Address1']));
        $addressGroup->add('bongocheckout_Address2', 'text', array('label' => 'apps.bongo_checkout.address2', 'value' => $data['bongocheckout_Address2']));
        $addressGroup->add('bongocheckout_City', 'text', array('label' => 'apps.bongo_checkout.city', 'value' => $data['bongocheckout_City']));

        $addressGroup->add('bongocheckout_State', 'choice', array('label' => 'apps.bongo_checkout.state', 'value' => $data['bongocheckout_State'], 'options' => $data['bongoCheckoutStateOptions']));
        $addressGroup->add('bongocheckout_Zip_Code', 'text', array('label' => 'apps.bongo_checkout.zip_code', 'value' => $data['bongocheckout_Zip_Code']));
        $addressGroup->add('bongocheckout_Phone', 'text', array('label' => 'apps.bongo_checkout.phone', 'value' => $data['bongocheckout_Phone']));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBongoActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');
        $formBuilder->add('bongocheckout_Partner_Key', 'text', array('label' => 'apps.bongo_checkout.partner_key', 'value' => $data['bongocheckout_Partner_Key'], 'placeholder' => 'apps.bongo_checkout.partner_key', 'required' => true));
        $formBuilder->add('bongocheckout_Gateway_Url', 'text', array('label' => 'apps.bongo_checkout.gateway_url', 'value' => $data['gateway_url'], 'placeholder' => 'apps.bongo_checkout.gateway_url', 'required' => true));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getBongoActivateForm($data)
    {
        return $this->getBongoActivateBuilder($data)->getForm();
    }
}
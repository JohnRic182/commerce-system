<?php
/**
 * Class Admin_Form_StateForm
 */
class Admin_Form_StateForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$request = Framework_Request::createFromGlobals();
	
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));

		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array('label' => 'states.name', 'value' => $formData['name'], 'placeholder' => 'states.name', 'required' => true, 'attr' => array('maxlength' => '255')));

		$basicGroup->add('short_name', 'text', array('label' => 'states.code', 'value' => $formData['short_name'], 'placeholder' => 'states.code', 'required' => true, 'attr' => array('maxlength' => '100')));
		
		$basicGroup->add('coid', 'hidden', array('label' => '', 'value' => $formData['coid'], 'required' => true, 'attr' => array('maxlength' => '100')));

		return $formBuilder;
	}

	/**
	 * Get country form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}
}
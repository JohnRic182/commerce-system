<?php

/**
 * Class Admin_Form_OrderEndiciaForm
 */
class Admin_Form_OrderEndiciaForm
{
	/**
	 * @param DataAccess_SettingsRepository $settings
	 * @param DataAccess_CountriesStatesRepository $countriesStatesRepository
	 * @param ORDER $order
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder(DataAccess_SettingsRepository $settings, DataAccess_CountriesStatesRepository $countriesStatesRepository, ORDER $order)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'Package Properties'));
		$formBuilder->add($basicGroup);

		if ($order->getPaymentGatewayId() == 'bongocheckout')
		{
			$bongoCountry = $countriesStatesRepository->getCountryById(1);
			$bongoState = $countriesStatesRepository->getStateById($settings->get('bongocheckout_State'));
			$shipToCompany = $settings->get('bongocheckout_Company_Name');
			$shipToName = $settings->get('bongocheckout_Attention_To');
			$shipToType = 'Business';
			$shipToAddress1 = $settings->get('bongocheckout_Address1');
			$shipToAddress2 = $settings->get('bongocheckout_Address2');
			$shipToCity = $settings->get('bongocheckout_City');
			$shipToStateName = $bongoState['name'];
			$shipToStateCode = $bongoState['short_name'];
			$shipToZip = $settings->get('bongocheckout_Zip_Code');
			$shipToCountryName = $bongoCountry['name'];
			$shipToCountryCode = $bongoCountry['iso_a2'];
			$shipToPhone = $settings->get('bongocheckout_Phone');
		}
		else
		{
			$orderShippingAddress = $order->getShippingAddress();
			$userData = $order->getUser()->getUserData();

			$shipToCompany = $orderShippingAddress['company'];
			$shipToName = $orderShippingAddress['name'];
			$shipToType = $orderShippingAddress['address_type'];
			$shipToAddress1 = $orderShippingAddress['address1'];
			$shipToAddress2 = $orderShippingAddress['address2'];
			$shipToCity = $orderShippingAddress['city'];
			$shipToStateName = $orderShippingAddress['province'];
			$shipToStateCode = $orderShippingAddress['state_abbr'];
			$shipToZip = $orderShippingAddress['zip'];
			$shipToCountryName = $orderShippingAddress['country_name'];
			$shipToCountryCode = $orderShippingAddress['country_iso_a2'];
			$shipToPhone = $userData['phone'];
		}

		$shipFromName = $settings->get('CompanyName');
		$shipFromAddress1 = $settings->get('ShippingOriginAddressLine1');
		$shipFromAddress2 = $settings->get('ShippingOriginAddressLine2');
		$shipFromCity = $settings->get('ShippingOriginCity');
		$shipFromState = $settings->get('ShippingOriginState');
		$shipFromProvince = $settings->get('ShippingOriginProvince');
		$shipFromZip = $settings->get('ShippingOriginZip');
		$shipFromCountry = $settings->get('ShippingOriginCountry');
		$shipFromPhone = $settings->get('ShippingOriginPhone');

		if ($shipFromStateData = $countriesStatesRepository->getStateById($shipFromState))
		{
			$shipFromStateName = $shipFromStateData['name'];
			$shipFromStateCode = $shipFromStateData['short_name'];
		}
		else
		{
			$shipFromStateName = $shipFromStateCode = $shipFromProvince;
		}

		if ($shipFromCountryData = $countriesStatesRepository->getCountryById($shipFromCountry))
		{
			$shipFromCountryName = $shipFromCountryData['name'];
			$shipFromCountryCode = $shipFromCountryData['iso_a2'];
		}

		$basicGroup->add('endicia_orderid', 'hidden', array('value' => $order->getId()));
		$basicGroup->add('endicia_mode', 'hidden', array('value' => ''));
        $basicGroup->add('endicia_shiptocompany', 'hidden', array('value' => $shipToCompany));
		$basicGroup->add('endicia_shiptoname', 'hidden', array('value' => $shipToName));
		$basicGroup->add('endicia_shiptoaddress1', 'hidden', array('value' => $shipToAddress1));
		$basicGroup->add('endicia_shiptoaddress2', 'hidden', array('value' => $shipToAddress2));
		$basicGroup->add('endicia_shiptocity', 'hidden', array('value' => $shipToCity));
		$basicGroup->add('endicia_shiptostate', 'hidden', array('value' => $shipToStateCode));
		$basicGroup->add('endicia_shiptozip', 'hidden', array('value' => $shipToZip));
		$basicGroup->add('endicia_shiptocountryname', 'hidden', array('value' => $shipToCountryName));
		$basicGroup->add('endicia_shiptocountrycode', 'hidden', array('value' => $shipToCountryCode));
		$basicGroup->add('endicia_shiptophone', 'hidden', array('value' => $shipToPhone));

		$basicGroup->add('endicia_shipfromname', 'hidden', array('value' => $shipFromName));
		$basicGroup->add('endicia_shipfromaddress1', 'hidden', array('value' => $shipFromAddress1));
		$basicGroup->add('endicia_shipfromaddress2', 'hidden', array('value' => $shipFromAddress2));
		$basicGroup->add('endicia_shipfromcity', 'hidden', array('value' => $shipFromCity));
		$basicGroup->add('endicia_shipfromstate', 'hidden', array('value' => $shipFromStateCode));
		$basicGroup->add('endicia_shipfromzip', 'hidden', array('value' => $shipFromZip));
		$basicGroup->add('endicia_shipfromcountryname', 'hidden', array('value' => $shipFromCountryName));
		$basicGroup->add('endicia_shipfromcountrycode', 'hidden', array('value' => $shipFromCountryCode));
		$basicGroup->add('endicia_shipfromphone', 'hidden', array('value' => $shipFromPhone));
		$basicGroup->add('endicia_nonce', 'hidden', array('value' => Nonce::create('endicia_label')));

		$basicGroup->add(
			'endicia_MailShape', 'choice',
			array(
				'label' => 'Package type',
				'value' => '',
				'options' => array(
					'Choose' => 'Choose a Package Type',
					'Parcel' => 'Parcel',
					'LargeParcel' => 'Large Parcel',
					'Letter' => 'Letter',
					'Card' => 'Card',
					'SmallFlatRateBox' => 'Small Flat Rate Box',
					'MediumFlatRateBox' => 'Medium Flat Rate Box',
					'LargeFlatRateBox' => 'Large Flat Rate Box',
					'Flat' => 'Flat',
					'SmallFlatRateEnvelope' => 'Small Flat Rate Envelope',
					'FlatRateEnvelope' => 'Flat Rate Envelope',
					'FlatRateLegalEnvelope' => 'Flat Rate Legal Envelope',
					'FlatRatePaddedEnvelope' => 'Flat Rate Padded Envelope',
					'FlatRateGiftCardEnvelope' => 'Flat Rate Gift Card Envelope',
					'FlatRateWindowEnvelope' => 'Flat Rate Window Envelope',
					'FlatRateCardboardEnvelope' => 'Flat Rate Cardboard Envelope'
				)
			)
		);

		$basicGroup->add(
			'endicia_MailClass', 'choice',
			array(
				'label' => 'Delivery service',
				'value' => '',
				'options' => array('ChooseFirst' => 'Select a package type above')
			)
		);

		$basicGroup->add(
			'endicia_SortType', 'choice',
			array(
				'label' => 'Sort type',
				'value' => '',
				'options' => array(
					'Presorted' => 'Presorted',
					'Nonpresorted' => 'Non Presorted',
					'BMC' => 'BMC',
					'FiveDigit' => 'Five Digit',
					'MixedBMC' => 'Mixed BMC',
					'SCF' => 'SCF',
					'SinglePiece' => 'Single Piece',
					'ThreeDigit' => 'Three Digit'
				)
			)
		);

		$basicGroup->add(
			'endicia_EntryFacility', 'choice',
			array(
				'label' => 'Entry facility',
				'value' => '',
				'options' => array(
					'Other' => 'Other',
					'DBMC' => 'DBMC',
					'DDU' => 'DDU',
					'DSCF' => 'DSCF',
					'OBMC' => 'OBMC'
				)
			)
		);

		$basicGroup->add('endicia_WeightOz', 'text', array('label' => 'Package weight (ounces)'));
		$basicGroup->add('endicia_DeclaredValue', 'text', array('label' => 'Declared value'));
		$basicGroup->add('endicia_PackageDescription', 'text', array('label' => 'Package description'));
		$basicGroup->add('endicia_PackageDimension', 'dimension', array('label' => 'Item width / length / height (inches)', 'widthValue' => 0, 'heightValue' => 0, 'lengthValue' => 0));

		$basicGroup->add(
			'endicia_DaysAdvance', 'choice',
			array(
				'label' => 'Ship on',
				'value' => '',
				'options' => array(
					'0' => 'Today ',
					'1' => 'Tomorrow '.date('m/d',strtotime('+1 day')),
					'2' => date("D (m/d)",strtotime('+2 day')),
					'3' => date("D (m/d)",strtotime('+3 day')),
					'4' => date("D (m/d)",strtotime('+4 day')),
					'5' => date("D (m/d)",strtotime('+5 day')),
					'6' => date("D (m/d)",strtotime('+6 day')),
					'7' => date("D (m/d)",strtotime('+7 day')),
				)
			)
		);

		$specialGroup = new core_Form_FormBuilder('special', array('label'=>'Special Services requested for this package'));
		$formBuilder->add($specialGroup);

		$specialGroup->add(
			'endicia_Insurance', 'choice',
			array(
				'label' => 'Insurance',
				'value' => '',
				'options' => array(
					'None' => 'None',
					'UspsOnline' => 'USPS Online - *Not allowed for International Mail or When Stealth Mode is on',
					'Endicia' => 'Endicia - *Not included in postage price. It is billed to your Endicia account'
				)
			)
		);

		$specialGroup->add('endicia_DeliveryConfirmation', 'checkbox', array('label' => 'Delivery confirmation', 'value' => '1'));
		$specialGroup->add('endicia_CertifiedMail', 'checkbox', array('label' => 'Available for First-Class and Priority Mail', 'value' => '1', 'note' => 'Available for First-Class and Priority Mail'));
		$specialGroup->add('endicia_RestrictedDelivery', 'checkbox', array('label' => 'Restricted delivery', 'value' => '1'));
		$specialGroup->add('endicia_NoWeekendDelivery', 'checkbox', array('label' => 'No weekend delivery', 'value' => '1', 'note' => 'Delivery attempts will not be made on Saturday'));
		$specialGroup->add('endicia_ReturnReceipt', 'checkbox', array('label' => 'Return receipt', 'value' => '1', 'note' => 'Not supported for International Mail'));
		$specialGroup->add('endicia_AdultSignature', 'checkbox', array('label' => 'Signature confirmation', 'value' => '1'));
		$specialGroup->add('endicia_Stealth', 'checkbox', array('label' => 'Stealth postage mode', 'value' => '1', 'note' => 'Stealth mode cannot be used with COD, USPS Insurance and Card Shape Mailpieces'));

		return $formBuilder;
	}

	/**
	 * @param DataAccess_SettingsRepository $settings
	 * @param DataAccess_CountriesStatesRepository $countriesStatesRepository
	 * @param ORDER $order
	 * @return core_Form_Form
	 */
	public function getForm(DataAccess_SettingsRepository $settings, DataAccess_CountriesStatesRepository $countriesStatesRepository,  ORDER $order)
	{
		return $this->getBuilder($settings, $countriesStatesRepository, $order)->getForm();
	}
}
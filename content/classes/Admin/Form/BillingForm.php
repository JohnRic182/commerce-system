<?php

/**
 * Class Admin_Form_BillingForm
 */
class Admin_Form_BillingForm
{
	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getSupportFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('support');

		$supportGroup = new core_Form_FormBuilder('support-ticket-info', array('label' => 'Please Enter Your Question'));
		$formBuilder->add($supportGroup);

		$supportGroup->add('subject', 'text', array('label' => 'Subject', 'value' => '', 'required' => true));
		$supportGroup->add('message', 'textarea', array('label' => 'Message', 'value' => '', 'required' => true));
		$supportGroup->add('admin', 'checkbox', array('label' => 'Grant access to admin area', 'value' => '1', 'current_value' => '0'));
		return $formBuilder;
	}


	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getSupportForm($data)
	{
		return $this->getSupportFormBuilder($data)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 */
	public function getCheckoutFormBuilder($data)
	{
		$profile = $data['profile'];

		$formBuilder = new core_Form_FormBuilder('billing-checkout');

		/**
		 * Billing info group
		 */
		$billingGroup = new core_Form_FormBuilder('billing-info', array('label' => 'Billing Info'));
		$formBuilder->add($billingGroup);

		$billingGroup->add('profile[firstName]', 'text', array('label' => 'First Name', 'value' => $profile->firstName, 'required' => true));
		$billingGroup->add('profile[lastName]', 'text', array('label' => 'Last Name', 'value' => $profile->lastName, 'required' => true));
		$billingGroup->add('profile[company]', 'text', array('label' => 'Company', 'value' => $profile->company));
		$billingGroup->add('profile[address1]', 'text', array('label' => 'Address Line 1', 'value' => $profile->address1, 'required' => true));
		$billingGroup->add('profile[address2]', 'text', array('label' => 'Address Line 2', 'value' => $profile->address2));
		$billingGroup->add('profile[city]', 'text', array('label' => 'City', 'value' => $profile->city, 'required' => true));

		$country = $profile->country;
		$state = $profile->state;

		$billingGroup->add('profile[state]', 'choice', array('label' => 'State', 'value' => $state,'required' => true, 'attr' => array('data-value' => $state, 'class' => 'input-address-state')));
		$billingGroup->add('profile[province]', 'text', array('label' => 'Province', 'value' => $state, 'required' => true, 'wrapperClass' => 'hidden', 'attr' => array('class' => 'input-address-province')));
		$billingGroup->add('profile[country]', 'choice', array('label' => 'Country', 'value' => $country, 'required' => true, 'attr' => array('data-value' => $country, 'class' => 'input-address-country')));
		$billingGroup->add('profile[zip]', 'text', array('label' => 'Zip / Postal Code', 'value' => $profile->zip, 'required' => true));
		$billingGroup->add('profile[phone]', 'text', array('label' => 'Phone', 'value' => $profile->phone, 'required' => true, 'wrapperClass' => 'clear-both',));
		$billingGroup->add('profile[email]', 'email', array('label' => 'Email', 'value' => $profile->email, 'required' => true));

		/**
		 * Domain group
		 */
		$domainGroup = new core_Form_FormBuilder('domain-info', array('label' => 'Domain Information'));
		$formBuilder->add($domainGroup);
		$domainGroup->add('domain[domain_name]', 'text', array('label' => 'Domain name', 'value' => '', 'required' => true));

		/**
		 * Card group
		 */
		$ccExists = isset($profile->cards) && isset($profile->cards[0]);

		$ccGroup = new core_Form_FormBuilder('cc-info', array('label' => 'Credit Card Info'));
		$formBuilder->add($ccGroup);
		$ccGroup->add('cc[cardNumber]', 'text', array('label' => 'Card Number', 'value' => '', 'required' => true, 'wrapperClass' => $ccExists ? 'hidden' : ''));
		$ccGroup->add('cc[expiration]', 'datemonthyear', array('label' => 'Expiration date', 'value' => '', 'required' => true, 'wrapperClass' => $ccExists ? 'hidden' : ''));
		$ccGroup->add('cc[cvv]', 'text', array('label' => 'CVV', 'value' => '', 'required' => true, 'wrapperClass' => $ccExists ? 'hidden' : ''));

		if ($ccExists)
		{
			$ccGroup->add('cc[ccLink]', 'static', array('label' => '', 'value' => '<a href="#" data-action="checkout-use-stored-card">Use card on file</a>', 'wrapperClass' => 'hidden  clear-both'));
			$ccGroup->add('cc[paymentProfileUsed]', 'hidden', array('value' => 1));
			$ccGroup->add('cc[paymentProfileId]', 'hidden', array('value' => $profile->cards[0]->profile_id));
			$ccGroup->add('cc[paymentProfileView]', 'static', array('label' => '', 'value' => str_repeat('&#8226; &#8226; &#8226; &#8226; &nbsp;', 3).substr($profile->cards[0]->card_number, -4), 'wrapperClass' => 'cc-bg'));
			$ccGroup->add('cc[paymentProfileLink]', 'static', array('label' => '', 'value' => '<a href="#" data-action="checkout-use-new-card">Use different card</a>', 'wrapperClass' => 'clear-both'));
		}

		/**
		 * Promo code
		 */
		$promoCode = new core_Form_FormBuilder('promo-info', array('label' => 'Promo Code'));
		$formBuilder->add($promoCode);
		$promoCode->add('promo[promo_code]', 'text', array('label' => 'If you have a promo code, please enter it here', 'value' => '', 'required' => false));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	public function getCheckoutForm($data)
	{
		return $this->getCheckoutFormBuilder($data)->getForm();
	}
}
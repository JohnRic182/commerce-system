<?php

/**
 * Class Admin_Form_ThumbnailGeneratorForm
 */
class Admin_Form_ThumbnailGeneratorForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('generic');

        $typeGroup = new core_Form_FormBuilder('group_type', array('label' => 'thumbs_generator.group_type'));
        $formBuilder->add($typeGroup);
        $typeGroup->add('generateType', 'choice', array('label' => false,
            'expanded' => true,
            'value' => 'all',
            'options' => array(
                'all' => 'thumbs_generator.generateTypeOptions.all',
                'products' => 'thumbs_generator.generateTypeOptions.products',
                'thumbs' => 'thumbs_generator.generateTypeOptions.thumbs',
                'secondary' => 'thumbs_generator.generateTypeOptions.secondary',
            )));

        $typeGroup->add('generateTarget', 'choice', array('label' => false,
            'expanded' => true,
            'value' => 'new',
            'wrapperClass' => 'clear-both',
            'options' => array(
                'new' => 'thumbs_generator.generateTargetOptions.new',
                'all' => 'thumbs_generator.generateTargetOptions.all',
            )));

        if ($data->get('CatalogOptimizeImages') == 'YES')
        {
            $optimizeGroup = new core_Form_FormBuilder('group_optimize', array('label' => 'thumbs_generator.group_optimize'));
            $formBuilder->add($optimizeGroup);
            $optimizeGroup->add('optimizePrimary', 'choice', array('label' => false,
                'expanded' => true,
                'value' => 'none',
                'wrapperClass' => 'clear-both',
                'options' => array(
                    'none' => 'thumbs_generator.optimizePrimaryOptions.none',
                    'new' => 'thumbs_generator.optimizePrimaryOptions.new',
                    'all' => 'thumbs_generator.optimizePrimaryOptions.all',
                )));
        }
        else
        {
            $typeGroup->add('optimizePrimary', 'hidden', array('value' => 'none'));
        }

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
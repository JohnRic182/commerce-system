<?php

/**
 * Class Admin_Form_SocialSharingSettingsForm
 */
class Admin_Form_SocialSharingSettingsForm
{
	/**
	 * Get advanced settings form for social sharing settings - builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($data)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		$profiles = new core_Form_FormBuilder('profiles', array('label' => 'marketing.social.social_profiles', 'note' => 'marketing.social.social_profiles_tooltip'));
		$formBuilder->add($profiles);

		$profiles->add('facebookPageAccount', 'text', array(
			'label' => 'marketing.social.facebook_page_url',
			'value' => $data['facebookPageAccount'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));
		$profiles->add('twitterAccount', 'text', array(
			'label' => 'marketing.social.twitter_account_url',
			'value' => $data['twitterAccount'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));
		$profiles->add('pinterestUsername', 'text', array(
			'label' => 'marketing.social.pinterest_username',
			'value' => $data['pinterestUsername'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));
		$profiles->add('instagramAccount', 'text', array(
			'label' => 'marketing.social.instagram_account_url',
			'value' => $data['instagramAccount'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));
		$profiles->add('youtubePageURL', 'text', array(
			'label' => 'marketing.social.youtube_account_url',
			'value' => $data['youtubePageURL'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));
		$profiles->add('snapChatAccount', 'text', array(
			'label' => 'marketing.social.snapchat_account_url',
			'value' => $data['snapChatAccount'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));
		$profiles->add('googlePlusAccount', 'text', array(
			'label' => 'marketing.social.googleplus_account_url',
			'value' => $data['googlePlusAccount'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));
		$profiles->add('bloggerAccount', 'text', array(
			'label' => 'marketing.social.blogger_account_url',
			'value' => $data['bloggerAccount'],
			'wrapperClass' => 'col-sm-6 col-xs-12',
		));



		$productsSettings = new core_Form_FormBuilder('products-settings', array('label' => 'marketing.social.social_sharing_for_products'));
		$formBuilder->add($productsSettings);

		$productsSettings->add('facebookLikeButtonProduct', 'checkbox', array(
			'label' => 'marketing.social.enable_facebook_like_button',
			'value' => 'Yes',
			'current_value' => $data['facebookLikeButtonProduct'],
		));

		$productsSettings->add('facebookCommentsProduct', 'checkbox', array(
			'label' => 'marketing.social.enable_facebook_comments',
			'value' => 'Yes',
			'current_value' => $data['facebookCommentsProduct'],
			'wrapperClass' => 'clear-both',
		));

		$productsSettings->add('facebookAppId', 'text', array(
			'label' => 'marketing.social.facebook_application_id',
			'value' => $data['facebookAppId'],
			'wrapperClass' => 'clear-both',
		));

		$productsSettings->add('twitterTweetButtonProduct', 'checkbox', array(
			'label' => 'marketing.social.enable_tweet_button',
			'value' => 'Yes',
			'current_value' => $data['twitterTweetButtonProduct'],
			'wrapperClass' => 'clear-both',
		));

		$productsSettings->add('googlePlusButtonProduct', 'checkbox', array(
			'label' => 'marketing.social.enable_google_plus_button',
			'value' => 'Yes',
			'current_value' => $data['googlePlusButtonProduct'],
			'wrapperClass' => 'clear-both',
		));

		$productsSettings->add('pinItButtonProduct', 'checkbox', array(
				'label' => 'marketing.social.enable_pinterest_button',
				'value' => 'Yes',
				'current_value' => $data['pinItButtonProduct'],
			'wrapperClass' => 'clear-both',
		));

		$productsSettings->add('disallowPinterestPinning', 'checkbox', array(
				'label' => 'marketing.social.block_all_pinterest_sharing',
				'value' => 'Yes',
				'current_value' => $data['disallowPinterestPinning'],
			'wrapperClass' => 'clear-both',
		));

		$productsSettings->add('deliciousButtonProduct', 'checkbox', array(
			'label' => 'marketing.social.enable_delicious_button',
			'value' => 'Yes',
			'current_value' => $data['deliciousButtonProduct'],
			'wrapperClass' => 'clear-both',
		));

		return $formBuilder;
	}

	/**
	 * Get advanced social sharing settings form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($data)
	{
		return $this->getBuilder($data)->getForm();
	}
}
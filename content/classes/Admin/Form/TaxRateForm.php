<?php

/**
 * Class Admin_Form_TaxRateForm
 */
class Admin_Form_TaxRateForm
{
	/** @var DataAccess_SettingsRepository $settings */
	protected $settings;

	/**
	 * Admin_Form_TaxRateForm constructor.
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
	}

	/**
	 * Get text pages form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$settings = $this->settings;

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'taxes.tax_rate_details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('rate_description', 'text', array('label' => 'taxes.title', 'placeholder' => 'taxes.title', 'value' => $formData['rate_description'], 'required' => true, 'attr' => array('maxlength' => '255')));

		$classes = array();
		if ( $formData['taxes_classes'] ){
			foreach ($formData['taxes_classes'] as $k => $v){
				$classes[] =new core_Form_Option($v['class_id'], $v['class_name']);
			}
		}
		
		$basicGroup->add('class_id', 'choice', array('label' => 'taxes.tax_class', 'value' => $formData['class_id'], 'options' =>  $classes));
		
		$basicGroup->add('coid', 'choice', array(
			'label' => 'taxes.country',
			'value' => $formData['coid'],
			'required' => true, 'placeholder' => 'taxes.country',
			'attr' => array('data-value' => $formData['coid'], 'class' => 'input-address-country all-countries-value'),
		));

		$basicGroup->add('stid', 'choice', array(
			'label' => 'taxes.state',
			'value' => $formData['stid'],
			'required' => true,
			'attr' => array('data-value' => $formData['stid'], 'class' => 'input-address-state all-states-value'),
		));
		
		$basicGroup->add('tax_rate', 'numeric', array('label' => 'taxes.tax_rate_percentage', 'placeholder' => 'taxes.tax_rate_percentage', 'required' => true, 'value' => $formData['tax_rate'], 'wrapperClass' => 'clear-both'));
		
	
		$advancedGroup = new core_Form_FormBuilder('advanced', array('label'=>'taxes.advanced_details', 'collapsible' => true));

		$formBuilder->add($advancedGroup);
		
		$advancedGroup->add('display_with_tax', 'checkbox', array('label' => 'taxes.display_prices_with_tax', 'current_value' => $formData['display_with_tax'],  'value' => '1' , 'wrapperClass' => 'clear-both'));

		$rate_priorities = array();
		for ($i=1; $i<=20; $i++){
			$rate_priorities[] =new core_Form_Option($i, $i);
		}
		$advancedGroup->add('rate_priority', 'choice', array('label' => 'taxes.rate_priority', 'options' =>  $rate_priorities, 'value' => $formData['rate_priority']));

		if ($settings->get('WholesaleDiscountType') != 'NO')
		{
			if (trim($formData['user_level']) == '')
			{
				$options = array('0');
			}
			else
			{
				$options = explode(',',   $formData['user_level']);
			}

			$userLevelOptions = array(
				'0' => trans('taxes.simple_users'),
				'1' => trans('taxes.wholesalers_level1'),
			);
			$maxLevels = intval($settings->get('WholesaleMaxLevels'));
			if ($maxLevels > 1) $userLevelOptions['2'] = trans('taxes.wholesalers_level2');
			if ($maxLevels > 2)$userLevelOptions['3'] = trans('taxes.wholesalers_level3');

			$advancedGroup->add('user_level[]', 'choice', array(
				'label' => 'taxes.apply_to',
				'options' => $userLevelOptions,
				'multiple' => true,
				'expanded' => true,
				'value' => $options,
			));
		}

		return $formBuilder;
	}

	/**
	 * Get text page form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}
}
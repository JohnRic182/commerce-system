<?php

/**
 * Class Admin_Form_ProductAttributeForm
 */
class Admin_Form_ProductAttributeForm
{
	/**
	 * @param null $productId
	 * @return core_Form_FormBuilder
	 */
	public function getProductAttributeFormBuilder($productId)
	{
		$formBuilder = new core_Form_FormBuilder('product-attribute');
		$formBuilder->add('attribute[pid]', 'hidden', array('value' => $productId));
		$formBuilder->add('attribute[id]', 'hidden', array('value' => 'new'));
		$formBuilder->add('attribute[name]', 'text', array('label' => 'product.attribute.attributes_table.name', 'required' => true, 'placeholder' => 'product.attribute.attributes_table.name'));
		$formBuilder->add('attribute[caption]', 'text', array('label' => 'product.attribute.attributes_table.caption', 'required' => true, 'placeholder' => 'product.attribute.attributes_table.caption'));
		$formBuilder->add('attribute[priority]', 'choice', array('label' => 'Priority', 'value' => '5', 'attr' => array('class'=>'short'), 'options' => array(
			new core_Form_Option('1', '1'),
			new core_Form_Option('2', '2'),
			new core_Form_Option('3', '3'),
			new core_Form_Option('4', '4'),
			new core_Form_Option('5', '5'),
			new core_Form_Option('6', '6'),
			new core_Form_Option('7', '7'),
			new core_Form_Option('8', '8'),
			new core_Form_Option('9', '9'),
			new core_Form_Option('10', '10'),
		)));
		$formBuilder->add('attribute[is_active]', 'checkbox', array('label' => 'Is this attribute active?', 'value' => 'Yes', 'current_value' => 'Yes', 'wrapperClass' => 'field-checkbox-space'));
		$formBuilder->add('attribute[attribute_type]', 'choice', array('label' => 'Attribute type', 'wrapperClass' => 'clear-both', 'options' => array(
			new core_Form_Option('select', 'Drop-down'),
			new core_Form_Option('radio', 'Radio Buttons'),
			new core_Form_Option('text', 'Text Input'),
			new core_Form_Option('textarea', 'Textarea Input'),
		)));
		$formBuilder->add('attribute[text_length]', 'text', array('label' => 'Text length'));
		$formBuilder->add('attribute[track_inventory]', 'checkbox', array('label' => 'Use in variants?', 'value' => '1', 'current_value' => '1', 'wrapperClass' => 'field-checkbox-space'));
		$formBuilder->add('attribute[attribute_required]', 'checkbox', array('label' => 'Required?', 'value' => 'Yes', 'current_value' => 'Yes', 'wrapperClass' => 'field-checkbox-space'));

		$formBuilder->add(
			'attribute[options]', 'textarea',
			array(
				'label' => 'Options',
				'required' => true,
				'wrapperClass' => 'field-full-width',
				'note' =>
					'Each new selection must be entered on a new line for it to appear correctly. If the attribute is a price modifier, you need to tell the system to increase or decrease the price (+ or -) between parenthesis ( ) at the end of each attribute. You may use actual amounts or percentages. You can also modify shipping weight on an attribute by entering the increase or decrease after the price and separated by a comma. Examples below.<br><br>'.
					'Small(-25,-0.25) Decrease price by 25, decrease weight by 0.25<br>'.
					'Medium(-10%,+1) Decrease price by 10%, increase weight by 1<br>'.
					'Large(0,+2) Price will remain the same, increase weight 2<br>'.
					'X-Large(+10.00) Increase price by 10'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param null $productId
	 * @return core_Form_Form
	 */
	public function getProductAttributeForm($productId)
	{
		return $this->getProductAttributeFormBuilder($productId)->getForm();
	}

	/**
	 * @param $productId
	 * @param $globalAttributes
	 * @return core_Form_FormBuilder
	 */
	public function getGlobalAttributesFormBuilder($productId, $globalAttributes)
	{
		$formBuilder = new core_Form_FormBuilder('product-global-attributes');
		$formBuilder->add('global_attribute_pid', 'hidden', array('value' => $productId));

		foreach ($globalAttributes as $globalAttribute)
		{
			$formBuilder->add('global_attribute['.$globalAttribute['gaid'].']', 'checkbox', array('label' => $globalAttribute['name'], 'value' => $globalAttribute['gaid'], 'current_value' => 1));
		}

		return $formBuilder;
	}

	/**
	 * @param $productId
	 * @param $globalAttributes
	 * @return core_Form_FormBuilder
	 */
	public function getGlobalAttributesForm($productId, $globalAttributes)
	{
		return $this->getGlobalAttributesFormBuilder($productId, $globalAttributes)->getForm();
	}
}
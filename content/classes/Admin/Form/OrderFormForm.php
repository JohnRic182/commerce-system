<?php
/**
 * Class Admin_Form_OrderFormForm
 */
class Admin_Form_OrderFormForm
{
	/**
	 * Get order form form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'order_forms.order_form_details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array('label' => 'order_forms.fields.name', 'value' => $formData['name'], 'placeholder' => 'order_forms.fields.name', 'required' => true, 'attr' => array('maxlength' => '255')));

		$basicGroup->add('active', 'checkbox', array('label' => 'order_forms.fields.active', 'value' => 'Yes', 'current_value' => $formData['active'], 'wrapperClass' => 'field-checkbox-space col-xs-12 col-md-6'));

		$basicGroup->add('can_change_quantities', 'checkbox', array('label' => 'order_forms.fields.can_change_quantities', 'value' => 'Yes', 'current_value' => $formData['can_change_quantities'], 'wrapperClass' => 'field-checkbox-space col-xs-12 col-md-6'));

		$basicGroup->add(
				'thank_you_page', 'choice',
				array(
						'label' => 'order_forms.fields.thank_you_page',
						'value' => trim($formData['thank_you_page_url']) != '' ? 'custom' : 'default',
						'required' => true,
						'multiple' => false,
						'expanded' => true,
						'options' => array(
								new core_Form_Option('default', 'order_forms.fields.default_value'),
								new core_Form_Option('custom', 'order_forms.fields.custom_value'),
						),
						'wrapperClass' => 'clear-both'
				)
		);

		$basicGroup->add('thank_you_page_url', 'text', array('label' => 'order_forms.fields.thank_you_page_url', 'value' => $formData['thank_you_page_url'], 'required' => true, 'attr' => array('maxlength' => '2048')));

		/**
		 * Assigne products
		 */

		if ($mode == 'update')
		{
			$assignerProductsGroup = new core_Form_FormBuilder('group-assigned-products', array('label' => 'order_forms.order_form_products', 'collapsible' => false));

			$formBuilder->add($assignerProductsGroup);

			$assignerProductsGroup->add('assigned-products', 'template', array(
					'file' => 'templates/pages/order-form/edit-products.html',
					'value' => array('products' => $formData['assignedProducts'])
			));
		}

		/**
		 * SEO
		 */
		$seoGroup = new core_Form_FormBuilder('seo', array('label' => 'order_forms.seo', 'collapsible' => false));

		$formBuilder->add($seoGroup);

		$seoGroup->add(
			'url_custom',
			'text',
			array(
				'label' => 'order_forms.fields.url',
				'value' => $formData['url_custom'],
				'placeholder' => $formData['url_default'],
				'attr' => array(
					'maxlength' => '255'
				),
				'note' => 'order_forms.url_tooltip'
			)
		);

		return $formBuilder;
	}

	/**
	 * Get order form form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getOrderFormSearchFormBuilder($data){
		$formBuilder = new core_Form_FormBuilder('search');
		$formBuilder->add('searchParams[name]', 'text', array('label' => trans('order_forms.fields.name'), 'value' => $data['name'], 'wrapperClass' => 'col-sm-12 col-md-3'));
		$formBuilder->add('searchParams[product_name]', 'text', array('label' => trans('order_forms.products.product_name'), 'value' => $data['product_name'], 'wrapperClass' => 'col-sm-12 col-md-3'));
		$formBuilder->add('searchParams[product_id]', 'text', array('label' => trans('order_forms.products.product_id'), 'value' => $data['product_id'], 'wrapperClass' => 'col-sm-12 col-md-3'));
		$formBuilder->add('searchParams[visibility]', 'choice', array('label' => trans('order_forms.status'), 'value' => $data['visibility'], 'options' => array(
			'all' => trans('order_forms.visibility.display_all'),
			'visible' => trans('order_forms.visibility.display_visible'),
			'invisible' => trans('order_forms.visibility.display_invisible'),
		), 'wrapperClass' => 'col-sm-12 col-md-3'));
		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getOrderFormSearchForm($data)
	{
		return $this->getOrderFormSearchFormBuilder($data)->getForm();
	}
}
<?php
/**
 * Class Admin_Form_AddressForm
 */
class Admin_Form_AddressForm
{
	const ADDRESS_MODE_BILLING = 'billing';
	const ADDRESS_MODE_SHIPPING = 'shipping';
	const ADDRESS_MODE_SETTINGS = 'settings';

	/** @var DataAccess_SettingsRepository */
	protected $settings;

    protected $collapsibleAttr = false;

	/**
	 * Class constructor
	 *
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
	}

    /**
     * @param $val
     */
    public function setCollapsibleAttr($val){
        $this->collapsibleAttr = $val;
    }

	/**
	 * Get address form builder
	 *
	 * @param Model_Address $address
	 * @param $mode
	 * @param $title
	 * @param $hideSettings: array. Use this array to hide some fields on form
     *        example: $hideSettings('fname','lname')  - this is mean that fields
     *                "fname", "lname"  have to be invisible on form;
     *
	 * @return core_Form_Form
	 */
	public function getBuilder(Model_Address $address, $mode, $title = '', $hideSettings = array())
	{
		$translator = Framework_Translator::getInstance();

		$arrayName = $mode.'_address';

		$addressFormBuilder = new core_Form_FormBuilder('form-group-address', array('label' => $title, 'class'=>'ic-invoice', 'wrapperClass' => 'form-address', 'collapsible' => $this->collapsibleAttr));

		if ($mode != self::ADDRESS_MODE_SETTINGS)
		{
			$addressFormBuilder->add(
				$arrayName.'[address_type]',
                (!in_array('address_type',$hideSettings)?'choice':'hidden' ),
				array(
					'label' => 'address.address_type',
					'attr' => array('class' => 'medium'),
					'required' => true,
					'value' => $address->getAddressType(),
					'multiple' => false,
					'expanded' => false,
					'options' => array(
						Model_Address::ADDRESS_TYPE_RESIDENTIAL => $translator->trans('address.address_type_residential'),
						Model_Address::ADDRESS_TYPE_BUSINESS => $translator->trans('address.address_type_business')
					)
				)
			);
		}

		$companyNameRequired = false;
		$addressLine2Required = false;

		if ($mode == self::ADDRESS_MODE_BILLING)
		{
			$addressFormBuilder->add($arrayName.'[fname]',
                (!in_array('fname', $hideSettings)?'text':'hidden'),
                array('label'=>'address.first_name', 'value' => $address->getFirstName(), 'required' => true)
			);

            $addressFormBuilder->add($arrayName.'[lname]',
                (!in_array('lname', $hideSettings)?'text':'hidden'),
                array('label'=>'address.last_name', 'value' => $address->getLastName(), 'required' => true)
			);

			$companyNameRequired = $this->settings->get('FormsBillingCompany') == 'Required';
			$addressLine2Required = $this->settings->get('FormsBillingAddressLine2') == 'Required';
		}
		else if ($mode == self::ADDRESS_MODE_SHIPPING || $mode == self::ADDRESS_MODE_SETTINGS)
		{
			$addressFormBuilder->add($arrayName.'[name]',
                (!in_array('name', $hideSettings)?'text':'hidden'),
                array('label'=>'address.name', 'value' => $address->getName(), 'required' => true)
			);

			$companyNameRequired = $this->settings->get('FormsShippingCompany') == 'Required';
			$addressLine2Required = $this->settings->get('FormsShippingAddressLine2') == 'Required';
		}

		$addressFormBuilder->add($arrayName.'[company]',
            (!in_array('company', $hideSettings)?'text':'hidden'),
            array('label'=>'address.company', 'value' => $address->getCompany(), 'required' => $companyNameRequired)
		);

		$addressFormBuilder->add($arrayName.'[address1]',
            (!in_array('address1', $hideSettings)?'text':'hidden'),
            array('label'=>'address.address1', 'value' => $address->getAddressLine1(), 'required' => true)
		);

		$addressFormBuilder->add($arrayName.'[address2]',
            (!in_array('address2', $hideSettings)?'text':'hidden'),
            array('label'=>'address.address2', 'value' => $address->getAddressLine2(), 'required' => $addressLine2Required)
		);

		$addressFormBuilder->add($arrayName.'[city]',
            (!in_array('city', $hideSettings)?'text':'hidden'),
            array('label'=>'address.city', 'value' => $address->getCity(), 'required' => true)
		);

		$addressFormBuilder->add(
			$arrayName.'[country]',
            (!in_array('country', $hideSettings)?'choice':'hidden'),
			array(
				'label' => 'address.country',
				'value' => $address->getCountryId(),
				'required' => true,
				'options' => array(),
				'attr' => array('data-value' => $address->getCountryId(), 'class' => 'input-address-country')
			)
		);

		$addressFormBuilder->add(
			$arrayName.'[state]',
            (!in_array('state',$hideSettings)?'choice':'hidden'),
			array(
				'label' => 'address.state',
				'value' => $address->getStateId(),
				'required' => true,
				'options' => array(),
				'attr' => array('data-value' => $address->getStateId(), 'class' => 'input-address-state')
			)
		);

		$addressFormBuilder->add(
			$arrayName.'[province]',
            (!in_array('province',$hideSettings)?'text':'hidden'),
			array(
				'label' => 'address.province',
				'value' => $address->getProvince(),
				'required' => true,
				'attr' => array('class' => 'input-address-province')
			)
		);

		$addressFormBuilder->add($arrayName.'[zip]',
            (!in_array('zip',$hideSettings)?'text':'hidden'),
            array('label'=>'address.zip', 'value' => $address->getZip(), 'required' => true, 'attr' => array('class' => 'input-address-zip short'))
		);

		return $addressFormBuilder;
	}

	/**
	 * Get address form
	 *
	 * @param Model_Address $address
	 * @param $mode
	 * @param $title
	 *
	 * @return core_Form_Form
	 */
	public function getForm(Model_Address $address, $mode, $title)
	{
		return $this->getBuilder($address, $mode, $title)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_WholesaleSettingsForm
 */
class Admin_Form_WholesaleSettingsForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        /** @var core_Form_FormBuilder $formBuilder */
        $formBuilder = new core_Form_FormBuilder('general');

        /**
         * Catalog settings
         */
        $settingsGroup = new core_Form_FormBuilder('settings', array('label' => 'Wholesale Settings'));
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('WholesaleDiscountType', 'choice', array('label' => 'Wholesaler Discounts', 'value' => $data['WholesaleDiscountType'], 'options' => array(
            'NO' => 'Disable Wholesale',
            'PRODUCT' => 'Discount individual products',
            'GLOBAL' => 'Discount orders',
        )));
        $settingsGroup->add('WholesaleMaxLevels', 'choice', array('label' => 'Wholesaler Discount Levels', 'value' => $data['WholesaleMaxLevels'], 'options' => array(
            '1' => '1',
            '2' => '2',
            '3' => '3',
        )));
        $settingsGroup->add('WholesaleDiscount1', 'text', array('label' => trans('wholesale_settings.wholesale1'), 'placeholder' => trans('wholesale_settings.wholesale1'), 'value' => $data['WholesaleDiscount1'], 'required' => true));
        $settingsGroup->add('MinOrderSubtotalLevel1', 'text', array('label' => trans('wholesale_settings.minorder1'), 'placeholder' => trans('wholesale_settings.minorder1'), 'value' => $data['MinOrderSubtotalLevel1'], 'required' => true));
        $settingsGroup->add('WholesaleDiscount2', 'text', array('label' => trans('wholesale_settings.wholesale2'), 'placeholder' => trans('wholesale_settings.wholesale2'), 'value' => $data['WholesaleDiscount2'], 'required' => true));
        $settingsGroup->add('MinOrderSubtotalLevel2', 'text', array('label' => trans('wholesale_settings.minorder2'), 'placeholder' => trans('wholesale_settings.minorder2'), 'value' => $data['MinOrderSubtotalLevel2'], 'required' => true));
        $settingsGroup->add('WholesaleDiscount3', 'text', array('label' => trans('wholesale_settings.wholesale3'), 'placeholder' => trans('wholesale_settings.wholesale3'), 'value' => $data['WholesaleDiscount3'], 'required' => true));
        $settingsGroup->add('MinOrderSubtotalLevel3', 'text', array('label' => trans('wholesale_settings.minorder3'), 'placeholder' => trans('wholesale_settings.minorder3'), 'value' => $data['MinOrderSubtotalLevel3'], 'required' => true));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
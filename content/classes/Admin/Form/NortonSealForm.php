<?php

/**
 * Class Admin_Form_NortonSealForm
 */
class Admin_Form_NortonSealForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('norton-seal-logo', 'static', array('label' => '', 'value' => '<div id="apps-nav" class="container-fluid"><span class="icon icon-norton-seal"></span></div>'));

        $settingsGroup->add('DisplayNortonShoppingGuarantee', 'checkbox', array('label' => 'apps.norton_seal.display_norton_shopping_guarantee', 'value' => 'YES', 'current_value' => $data['DisplayNortonShoppingGuarantee']));
        $settingsGroup->add('NortonSealStoreNumber', 'text', array('label' => 'apps.norton_seal.store_number', 'value' =>  $data['NortonSealStoreNumber'], 'required' => true));
        $settingsGroup->add('NortonSealGuaranteeHash', 'textarea', array('label' => 'apps.norton_seal.guarantee_hash', 'required' => true, 'value' => $data['NortonSealGuaranteeHash'],
            'note' => 'apps.norton_seal.guarantee_hash_tooltip',
        ));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getNortonSealActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('DisplayNortonShoppingGuarantee', 'checkbox', array('label' => 'apps.norton_seal.display_norton_shopping_guarantee', 'value' => 'YES', 'current_value' => 'YES'));
        $settingsGroup->add('NortonSealStoreNumber', 'text', array('label' => 'apps.norton_seal.store_number', 'value' =>  $data['NortonSealStoreNumber'], 'required' => true));
        $settingsGroup->add('NortonSealGuaranteeHash', 'textarea', array('label' => 'apps.norton_seal.guarantee_hash', 'required' => true, 'value' => $data['NortonSealGuaranteeHash'],
            'note' => 'apps.norton_seal.guarantee_hash_tooltip'
        ));

        $settingsGroup->add('activation-tooltips', 'static', array('label' => '', 'value' => '<script type="text/javascript">forms.ensureTooltips();
		$("#field-NortonSealGuaranteeHash").click(function() { $(".tooltip-popover").popover("hide"); });</script>'));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getNortonSealActivateForm($data)
    {
        return $this->getNortonSealActivateBuilder($data)->getForm();
    }
}
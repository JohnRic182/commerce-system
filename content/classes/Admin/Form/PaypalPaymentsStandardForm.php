<?php

/**
 * Class Admin_Form_PaypalPaymentsStandardForm
 */
class Admin_Form_PaypalPaymentsStandardForm
{
    /**
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder()
    {
        $formBuilder = new core_Form_FormBuilder('gen-cert');

        $formBuilder->add('action', 'hidden', array('value' => 'generate'));
        $formBuilder->add('country', 'text', array('label' => 'Country', 'value' => '', 'required' => true, 'attr' => array('maxlength' => 2)));
        $formBuilder->add('state', 'text', array('label' => 'State / Province Name', 'value' => '', 'required' => true));
        $formBuilder->add('city', 'text', array('label' => 'City / Locality Name', 'value' => '', 'required' => true));
        $formBuilder->add('org', 'text', array('label' => 'Organization', 'value' => '', 'required' => true));
        $formBuilder->add('email', 'text', array('label' => 'Email', 'value' => '', 'required' => true));

        return $formBuilder;
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->getBuilder()->getForm();
    }
}
<?php

/**
 * Class Admin_Form_ProductQRCodeForm
 */
class Admin_Form_ProductQRCodeForm
{
	/**
	 * @param $productId
	 * @param $campaignsOptions
	 * @param $defaultImageType
	 * @param $defaultMedia
	 * @param $defaultErrorCheck
	 * @param $defaultSize
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($productId, $campaignsOptions, $defaultImageType, $defaultMedia, $defaultErrorCheck, $defaultSize)
	{
		$formBuilder = new core_Form_FormBuilder('qr-codes', array('label' => 'marketing.qr_codes_for_this_product', 'class' => 'ic-raster', 'first' => true));
		$formBuilder->add('qr_pid', 'hidden', array('value' => $productId));

		if (count($campaignsOptions) > 0)
		{
			$formBuilder->add('qr_campaign_id', 'choice', array('label' => 'marketing.campaign', 'options' => $campaignsOptions, 'empty_value' => array('0' => 'No campaign')));
		}

		$formBuilder->add(
			'qr_generate_product_url_type', 'choice',
			array(
				'label' => 'marketing.product_url_type',
				'options' => array('product' => 'marketing.product_page', 'cart' => 'Add product to cart')
			)
		);

		$formBuilder->add(
			'qr_generate_image_type', 'choice',
			array(
				'label' => 'marketing.image_type', 'value' => $defaultImageType,
				'options' => array(IMAGETYPE_PNG => 'PNG', IMAGETYPE_JPEG => 'JPG', IMAGETYPE_GIF => 'GIF')
			)
		);

		$formBuilder->add(
			'qr_generate_media', 'choice',
			array(
				'label' => 'marketing.media',
				'value' => $defaultMedia,
				'options' => array(
					'1' => trans('marketing.media_options.web_small'), 
					'2' => trans('marketing.media_options.web_big'),
					'3' => trans('marketing.media_options.print_ad_small'), 
					'4' => trans('marketing.media_options.print_ad_big'),
					'5' => trans('marketing.media_options.parcel_label'), 
					'6' => trans('marketing.media_options.factory_environment'), 
					'0' => trans('marketing.media_options.custom')
				)
			)
		);

		$formBuilder->add(
			'qr_generate_error_check', 'choice',
			array(
				'label' => 'marketing.error_correction_level.error_correction_level',
				'value' => $defaultErrorCheck,
				'options' => array(
					'L' => trans('marketing.error_correction_level.low'), 
					'M' => trans('marketing.error_correction_level.medium'), 
					'Q' => trans('marketing.error_correction_level.good'), 
					'H' => trans('marketing.error_correction_level.the_best')
				),
				'tooltip' => 'marketing.error_correction_level.tooltip'
			)
		);

		$qrSizes = array();
		for ($i = 4; $i <= 12; $i++) $qrSizes[$i] = $i;
		$formBuilder->add('qr_generate_default_size', 'choice', array('label' => 'marketing.pixels_per_qr_code_dot', 'value' => $defaultSize, 'options' => $qrSizes, 'tooltip' => 'marketing.pixels_per_qr_code_dot'));

		return $formBuilder;
	}

	/**
	 * @param $productId
	 * @param $campaignsOptions
	 * @param $defaultImageType
	 * @param $defaultMedia
	 * @param $defaultErrorCheck
	 * @param $defaultSize
	 * @return core_Form_Form
	 */
	public function getForm($productId, $campaignsOptions, $defaultImageType, $defaultMedia, $defaultErrorCheck, $defaultSize)
	{
		return $this->getFormBuilder($productId, $campaignsOptions, $defaultImageType, $defaultMedia, $defaultErrorCheck, $defaultSize)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_StoreSettingsForm
 */
class Admin_Form_StoreSettingsForm
{
    private $basicGroup;

    public function __construct()
    {
        $this->basicGroup = new core_Form_FormBuilder('basic');
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        $basicGroup = $this->getBasicGroup();
        $formBuilder->add($basicGroup);

        $basicGroup->add('GlobalSiteName', 'text', array(
            'label' => 'store_settings.site_title',
            'value' => $data['GlobalSiteName'], 'placeholder' => 'store_settings.site_title',
            'required' => true,
        ));
        $basicGroup->add('GlobalNotificationEmail', 'text', array(
            'label' => 'store_settings.notification_email',
            'value' => $data['GlobalNotificationEmail'],
            'note' => 'store_settings.notes.notification_email',
        ));
        $basicGroup->add('GlobalSupportEmail', 'text', array(
            'label' => 'store_settings.support_email',
            'value' => $data['GlobalSupportEmail'],
            'note' => 'store_settings.notes.support_email',
        ));
        $basicGroup->add('StoreClosed', 'checkbox', array(
            'label' => 'store_settings.storefront_available',
            'value' => 'No',
            'current_value' => $data['StoreClosed'],
            'wrapperClass' => 'clear-both',
        ));
        $basicGroup->add('StoreClosedMessage', 'text', array(
            'label' => 'store_settings.store_closed_message',
            'value' => $data['StoreClosedMessage'],
        ));
        $basicGroup->add('ForceSslRedirection', 'checkbox', array(
            'label' => 'store_settings.force_ssl_redirection',
            'value' => 'Yes',
            'current_value' => $data['ForceSslRedirection'],
            'wrapperClass' => 'clear-both',
        ));

        $emailGroup = new core_Form_FormBuilder('email', array('label' => 'store_settings.email_settings', 'collapsible' => true));
        $formBuilder->add($emailGroup);

        $emailGroup->add('EmailSendmailEngine', 'choice', array(
            'label' => 'store_settings.email_service',
            'value' => $data['EmailSendmailEngine'],
            'options' => array(
                'mail' => 'store_settings.email_service_options.mail',
                'phpmailer' => 'store_settings.email_service_options.phpmailer',
            ),
        ));
        $emailGroup->add('EmailSendmailEolCharacter', 'choice', array(
            'label' => 'store_settings.eol_character',
            'value' => $data['EmailSendmailEolCharacter'],
            'options' => array(
                '\r\n' => '\r\n',
                '\r' => '\r',
                '\n' => '\n',
            ),
        ));
        $emailGroup->add('EmailSMTPSecure', 'choice', array(
            'label' => 'store_settings.smtp_security',
            'value' => $data['EmailSMTPSecure'],
            'options' => array(
                '' => 'common.none',
                'ssl' => 'store_settings.smtp_security_options.ssl',
                'tls' => 'store_settings.smtp_security_options.tls',
            ),
        ));
        $emailGroup->add('EmailSMTPServer', 'text', array(
            'label' => 'store_settings.smtp_server',
            'value' => $data['EmailSMTPServer'],
        ));
        $emailGroup->add('EmailSMTPPort', 'text', array(
            'label' => 'store_settings.smtp_port',
            'value' => $data['EmailSMTPPort'],
        ));
        $emailGroup->add('EmailSMTPLogin', 'text', array(
            'label' => 'store_settings.smtp_username',
            'value' => $data['EmailSMTPLogin'],
        ));
        $emailGroup->add('EmailSMTPPassword', 'password', array(
            'label' => 'store_settings.smtp_password',
            'value' => $data['EmailSMTPPassword'],
        ));

        $securityGroup = new core_Form_FormBuilder('security', array('label' => 'store_settings.security_settings', 'collapsible' => true));
        $formBuilder->add($securityGroup);

        $securityGroup->add('SecurityCookiesPrefix', 'text', array(
            'label' => 'store_settings.user_cookie_name',
            'value' => $data['SecurityCookiesPrefix'],
        ));
        $securityGroup->add('SecurityUserTimeout', 'text', array(
            'label' => 'store_settings.user_cookie_timeout',
            'value' => $data['SecurityUserTimeout'],
        ));
        $securityGroup->add('SecurityAccountBlocking', 'checkbox', array(
            'label' => 'store_settings.security_account_blocking',
            'value' => 'YES',
            'current_value' => $data['SecurityAccountBlocking'],
        ));
        $securityGroup->add('SecurityAccountBlockingAttempts', 'text', array(
            'label' => 'store_settings.security_account_blocking_attempts',
            'value' => $data['SecurityAccountBlockingAttempts'],
        ));
        $securityGroup->add('SecurityAccountBlockingHours', 'text', array(
            'label' => 'store_settings.security_account_blocking_hours',
            'value' => $data['SecurityAccountBlockingHours'],
        ));
        $securityGroup->add('SecurityAdminIpRestriction', 'checkbox', array(
            'label' => 'store_settings.admin_ip_restriction',
            'value' => 'Allow',
            'current_value' => $data['SecurityAdminIpRestriction'],
            'wrapperClass' => 'clear-both',
        ));
        $securityGroup->add('SecurityAdminIps', 'text', array(
            'label' => 'store_settings.admin_ips',
            'value' => $data['SecurityAdminIps'],
        ));
        $securityGroup->add('SecurityAdminTimeOut', 'text', array(
            'label' => 'store_settings.admin_timeout',
            'value' => $data['SecurityAdminTimeOut'],
            'wrapperClass' => 'clear-both',
        ));

        $localizationGroup = new core_Form_FormBuilder('localization', array('label' => 'store_settings.localization_settings', 'collapsible' => true));
        $formBuilder->add($localizationGroup);

        $localizationGroup->add('LocalizationDateTimeFormat', 'text', array(
            'label' => 'store_settings.date_time_format',
            'value' => $data['LocalizationDateTimeFormat'],
        ));
        $localizationGroup->add('LocalizationWeightUnits', 'choice', array(
            'label' => 'store_settings.weight_units',
            'value' => $data['LocalizationWeightUnits'],
            'options' => array(
                'lbs' => 'store_settings.weight_units_options.lbs',
                'kg' => 'store_settings.weight_units_options.kg',
            ),
        ));
        $localizationGroup->add('LocalizationLengthUnits', 'choice', array(
            'label' => 'store_settings.length_units',
            'value' => $data['LocalizationLengthUnits'],
            'options' => array(
                'inches' => 'store_settings.length_units_options.inches',
                'feet' => 'store_settings.length_units_options.feet',
                'cm' => 'store_settings.length_units_options.cm',
            ),
        ));
        $localizationGroup->add('LocalizationCurrencyDecimalSymbol', 'text', array(
            'label' => 'store_settings.decimal_symbol',
            'value' => $data['LocalizationCurrencyDecimalSymbol'],
        ));
        $localizationGroup->add('LocalizationCurrencySeparatingSymbol', 'text', array(
            'label' => 'store_settings.thousands_symbol',
            'value' => $data['LocalizationCurrencySeparatingSymbol'],
        ));
        $localizationGroup->add('LocalizationUseSystemTimezone', 'checkbox', array(
            'label' => 'store_settings.system_timezone',
            'value' => 'Yes',
            'current_value' => $data['LocalizationUseSystemTimezone'],
            'wrapperClass' => 'clear-both',
        ));


        $localizationTimezone = (trim($data['LocalizationDefaultTimezone']) == '') ? APP_SYSTEM_TIMEZONE : $data['LocalizationDefaultTimezone'];

        $tzList = DateTimeZone::listIdentifiers();
        $options = array();
        foreach ($tzList as $tz)
        {
            $options[$tz] = str_replace('_', ' ', $tz);
        }
        $localizationGroup->add('LocalizationDefaultTimezone', 'choice', array(
            'label' => 'store_settings.timezone',
            'value' => $localizationTimezone,
            'options' => $options,
            'wrapperClass' => 'hidden',
        ));

        $captchaGroup = new core_Form_FormBuilder('captcha', array('label' => 'store_settings.captcha_settings', 'collapsible' => true));
        $formBuilder->add($captchaGroup);

        $captchaGroup->add('captchaMethod', 'checkbox', array(
            'label' => 'store_settings.enable_recaptcha',
            'value' => 'reCaptcha',
            'current_value' => $data['captchaMethod'],
            'note' => 'store_settings.notes.recaptcha',
        ));
        $captchaGroup->add('captchaReCaptchaPublicKey', 'text', array(
            'label' => 'store_settings.recaptcha_public_key',
            'value' => $data['captchaReCaptchaPublicKey'],
            'wrapperClass' => 'clear-both',
        ));
        $captchaGroup->add('captchaReCaptchaPrivateKey', 'password', array(
            'label' => 'store_settings.recaptcha_private_key',
            'value' => $data['captchaReCaptchaPrivateKey'],
        ));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    public function getBasicGroup()
    {
        return $this->basicGroup;
    }
}
<?php

/**
 * Class Admin_Form_iDevAffiliateForm
 */
class Admin_Form_iDevAffiliateForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $basicGroup = new core_Form_FormBuilder('group-basic');
        $formBuilder->add($basicGroup);

        $basicGroup->add('AffiliateAreaURL', 'text', array('label' => 'idev.idev_affiliate_installation_url', 'required' => true, 'value' => $data['AffiliateAreaURL'], 'placeholder' => 'idev.idev_affiliate_installation_url', 'wrapperClass' => 'col-xs-12 col-sm-6'));
        $basicGroup->add('AffiliateLinkText', 'text', array('label' => 'idev.idev_affiliate_link_text', 'value' => $data['AffiliateLinkText'], 'placeholder' => 'idev.idev_affiliate_link_text', 'wrapperClass' => 'col-xs-12 col-sm-6'));

        $advancedGroup = new core_Form_FormBuilder('group-advanced', array('label' => 'idev.advanced_settings', 'collapsible' => true, 'wrapperClass' => 'row clear-both'));
        $formBuilder->add($advancedGroup);

        $advancedGroup->add('AffiliateVarAmount', 'text', array('label' => 'idev.variable_for_amount', 'value' => $data['AffiliateVarAmount'], 'placeholder' => 'idev.variable_for_amount'));
        $advancedGroup->add('AffiliateVarOrderId', 'text', array('label' => 'idev.affiliatevarorderid', 'value' => $data['AffiliateVarOrderId'], 'placeholder' => 'idev.affiliatevarorderid'));
        
        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getIDevAffiliateActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');
        $formBuilder->add('AffiliateAreaURL', 'text', array('label' => 'idev.idevaffiliate_installation_url', 'required' => true, 'value' => $data['AffiliateAreaURL'], 'placeholder' => 'idev.idevaffiliate_installation_url', 'wrapperClass' => 'col-xs-12 col-sm-12'));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getIDevAffiliateActivateForm($data)
    {
        return $this->getIDevAffiliateActivateBuilder($data)->getForm();
    }
}
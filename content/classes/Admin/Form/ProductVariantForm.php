<?php

/**
 * Class Admin_Form_ProductVariantForm
 */
class Admin_Form_ProductVariantForm
{
	/**
	 * @param null $productId
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($productId)
	{
		$formBuilder = new core_Form_FormBuilder('product-attribute');
		$formBuilder->add('variant[id]', 'hidden', array('value' => 'new'));
		$formBuilder->add('variant[pid]', 'hidden', array('value' => $productId));
		$formBuilder->add('variant[inventory_control]', 'hidden', array('value' => ''));
		$formBuilder->add('variant[product_subid]', 'text', array('label' => '', 'value' => '', 'placeholder' => 'product.variants.variant_list_table.product_subid', 'required' => true));
		$formBuilder->add('variant[product_sku]', 'text', array('label' => '', 'value' => '', 'placeholder' => 'product.variants.variant_list_table.product_sku', 'required' => false));
		$formBuilder->add('variant[is_active]', 'checkbox', array('label' => 'Is this variant active?', 'value' => '1', 'current_value' => '1', 'wrapperClass' => 'field-checkbox-space'));
		$formBuilder->add('variant[stock]', 'integer', array('label' => 'Number of items in inventory', 'value' => '', 'wrapperClass' => 'clear-both', 'required' => true));
		$formBuilder->add('variant[stock_warning]', 'integer', array('label' => 'Notify when inventory reaches ..', 'value' => '', 'required' => true));

		return $formBuilder;
	}

	/**
	 * @param null $productId
	 * @return core_Form_Form
	 */
	public function getForm($productId)
	{
		return $this->getFormBuilder($productId)->getForm();
	}
}
<?php

/**
 * Class Admin_Form_TaxClassForm
 */
class Admin_Form_TaxClassForm
{
	/**
	 * Get text pages form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'Tax Rate Details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('class_name', 'text', array('label' => 'taxes.title', 'placeholder' => 'taxes.title', 'value' => $formData['class_name'], 'required' => true, 'attr' => array('maxlength' => '128')));
		
		$basicGroup->add('key_name', 'text', array('label' => 'taxes.key_id', 'placeholder' => 'taxes.key_id', 'value' => $formData['key_name'], 'required' => true, 'attr' => array('maxlength' => '255')));
		

		return $formBuilder;
	}

	/**
	 * Get text page form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}
}
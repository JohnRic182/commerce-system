<?php

/**
 * Class Admin_Form_DobaForm
 */
class Admin_Form_DobaForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getDobaActivateBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('doba_retailer_id', 'text', array('label' => 'apps.doba.retailer_id', 'required' => true, 'value' => $data['doba_retailer_id'], 'placeholder' => 'apps.doba.retailer_id'));
        $settingsGroup->add('doba_username', 'text', array('label' => 'apps.doba.username', 'required' => true, 'value' => $data['doba_username'], 'placeholder' => 'apps.doba.username'));
        $settingsGroup->add('doba_password', 'password', array('label' => 'apps.doba.password', 'required' => true, 'value' => $data['doba_password'], 'placeholder' => 'apps.doba.password'));
        $settingsGroup->add('doba_mode', 'choice', array('label' => 'apps.doba.account_status', 'required' => true,
            'options' => array('Live' => trans('apps.doba.live'), 'Test' => trans('apps.doba.test')),
            'value' => $data['doba_mode'],
        ));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getDobaActivateForm($data)
    {
        return $this->getDobaActivateBuilder($data)->getForm();
    }

    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('settings', array('label' => 'apps.doba.sync_options'));
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('doba_price_option', 'template', array('label' => 'apps.doba.pricing_options', 'value' => $data['doba_price_option'], 'file' => 'templates/pages/doba/field-pricing-options.html'));
        $settingsGroup->add('doba_min_stock', 'text', array('label' => 'apps.doba.sync_when', 'value' => $data['doba_min_stock'], 'placeholder' => 'apps.doba.sync_when'));
        $settingsGroup->add('doba_price_option99', 'checkbox', array('label' => 'apps.doba.end_prices_with_99', 'value' => 'Yes', 'current_value' => $data['doba_price_option99']));
        $settingsGroup->add('doba_cron_update', 'checkbox', array('label' => 'apps.doba.auto_update_inventory', 'value' => 'Yes', 'current_value' => $data['doba_cron_update']));
        $settingsGroup->add('doba_images', 'checkbox', array('label' => 'apps.doba.download_images', 'value' => 'Yes', 'current_value' => $data['doba_images']));

        $settingsGroup->add('doba_autosend_orders', 'checkbox', array('label' => 'apps.doba.auto_send_orders', 'value' => 'Yes', 'current_value' => $data['doba_autosend_orders']));

        $settingsGroup->add('doba_images_secondary', 'choice', array('label' => 'apps.doba.download_secondary_images', 'value' => $data['doba_images_secondary'], 'options' => array(
            'No' => trans('apps.doba.no'),
            'New' => trans('apps.doba.only_for_new_products'),
            'All' => trans('apps.doba.download_all_images'),
        )));
        $settingsGroup->add('doba_payment_type', 'choice', array('label' => 'apps.doba.default_payment_method', 'value' => $data['doba_payment_type'], 'options' => array(
            'CC' => trans('apps.doba.credit_card'),
            'Prepaid' => trans('apps.doba.prepaid'),
        )));

        $connectionGroup = new core_Form_FormBuilder('connection', array('label' => 'apps.doba.connection_information', 'collapsible' => true));
        $formBuilder->add($connectionGroup);

        $connectionGroup->add('doba_retailer_id', 'text', array('label' => 'apps.doba.retailer_id', 'required' => true, 'value' => $data['doba_retailer_id'], 'placeholder' => 'apps.doba.retailer_id'));
        $connectionGroup->add('doba_username', 'text', array('label' => 'apps.doba.username', 'required' => true, 'value' => $data['doba_username'], 'placeholder' => 'apps.doba.username'));
        $connectionGroup->add('doba_password', 'password', array('label' => 'apps.doba.password', 'required' => true, 'value' => $data['doba_password'], 'placeholder' => 'apps.doba.password'));
        $connectionGroup->add('doba_mode', 'choice', array('label' => 'apps.doba.account_status', 'required' => true,
            'options' => array('Live' => trans('apps.doba.live'), 'Test' => trans('apps.doba.test')),
            'value' => $data['doba_mode'],
        ));

        $callbackGroup = new core_Form_FormBuilder('callback', array('label' => 'apps.doba.callback_information', 'collapsible' => true));
        $formBuilder->add($callbackGroup);

        $callbackGroup->add('doba_callback_mode', 'choice', array('label' => 'apps.doba.callback_propagation_mode', 'value' => $data['doba_callback_mode'], 'options' => array(
            'None' => trans('apps.doba.none'),
            'Master' => trans('apps.doba.master'),
            'Slave' => trans('apps.doba.slave'),
        ), 'note' => trans('apps.doba.callback_propagation_mode_note')));
        $callbackGroup->add('doba_callback_access_key', 'text', array('label' => 'apps.doba.callback_key', 'value' => $data['doba_callback_access_key'], 'placeholder' => 'apps.doba.callback_key'));
        $callbackGroup->add('doba_callback_urls', 'textarea', array('label' => 'apps.doba.callback_urls', 'value' => $data['doba_callback_urls']));
        
        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
<?php
/**
 * Class Admin_Form_NotificationEmailForm
 */
class Admin_Form_NotificationEmailForm
{
    /**
     * Get manufacturer form builder
     *
     * @param $formData
     * @param $mode
     * @param $id
     *
     * @return core_Form_FormBuilder
     */
    public function getBuilder($formData, $mode, $id = null)
    {
        $formBuilder = new core_Form_FormBuilder('general');

        /**
         * Basic group
         */
        switch ($mode)
		{
            case 'header-footer-edit' :
			{
                $basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));

                $formBuilder->add($basicGroup);

				if(isset($formData['top']))
				{
					$top_data = $formData['top'];
					$basicGroup->add('content[top]', 'textarea', array('label' => 'notification_emails.header_html', 'value' => $top_data['content'], 'wysiwyg' => true));
				}

                if(isset($formData['bottom']))
				{
					$bottom_data = $formData['bottom'];
					$basicGroup->add('content[bottom]', 'textarea', array('label' => 'notification_emails.footer_html', 'value' => $bottom_data['content'], 'wysiwyg' => true));
                }
                break;
            }

            case 'update' :
			{
                $basicGroup = new core_Form_FormBuilder('basic', array('label'=>'notification_emails.edit_email_template'));

                $formBuilder->add($basicGroup);

                $basicGroup->add('content['.$formData['id'].']', 'textarea', array('label' => 'notification_emails.html', 'value' => $formData['content'], 'wysiwyg' => true));
                break;
            }
        }

        return $formBuilder;
    }

    /**
     * Get manufacturer form
     *
     * @param $formData
     * @param $mode
     * @param int|null $id
     *
     * @return core_Form_Form
     */
    public function getForm($formData, $mode, $id = null)
    {
        return $this->getBuilder($formData, $mode, $id)->getForm();
    }
}
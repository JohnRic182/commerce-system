<?php

/**
 * Class Admin_Form_GoogleMerchantForm
 */
class Admin_Form_GoogleMerchantForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('export');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('export_taxes', 'checkbox', array('label' => 'apps.google.export_tax_information', 'value' => '1', 'current_value' => '1'));

        $settingsGroup->add('product_category', 'choice', array('label' => 'apps.google.product_category', 'options' => $data['categories'], 'empty_value' => array('0' => trans('apps.google.all_product_categories'))));

        $maxProductsOptions = array('0' => 'All Products', '10' => '10', '50' => '50', '100' => '100', '150' => '150');
        for ($i = 250; $i <= 5000; $i+=250) $maxProductsOptions[$i] = $i;
        $settingsGroup->add('product_count', 'choice', array('label' => 'apps.google.max_number_of_products', 'options' => $maxProductsOptions));

        $expDaysOptions = array('0' => 'Default');
        for ($i = 1; $i <= 30; $i++) $expDaysOptions[$i] = $i;
        $settingsGroup->add('exp_days', 'choice', array('label' => 'apps.google.feed_expiration_days', 'options' => $expDaysOptions));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
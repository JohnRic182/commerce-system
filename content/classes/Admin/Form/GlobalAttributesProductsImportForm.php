<?php
/**
 * Class Admin_Form_GlobalAttributesProductsImportForm
 */
class Admin_Form_GlobalAttributesProductsImportForm
{
	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getStartFormBuilder($formData)
	{
		// TODO: fix global dependency
		global $label_app_name;

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'global_attributes.import_step1'));

		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('global_attributes.import_step1_description', array('label_app_name' => $label_app_name)))
		);

		$basicGroup->add(
			'fields_separator', 'choice',
			array('label' => 'global_attributes.import_delimiter', 'value' => ',', 'options' => array(','=>'Comma (,)', ';'=>'Semicolon (;)'))
		);

		$basicGroup->add('bulk', 'file', array('label' => 'global_attributes.import_choose_csv'));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getAssignFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'global_attributes.import_step2'));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('global_attributes.import_step2_description'))
		);

		$basicGroup->add('skip_first_line', 'checkbox', array('label' => 'global_attributes.import_skip_first_line', 'value'=>'1', 'current_value' => isset($formData['skip_first_line']) && $formData['skip_first_line'] == '1' ? '1' : '0'));
		$basicGroup->add('linesCount', 'static', array('label' => 'global_attributes.import_total_lines', 'value' => $formData['totalLinesCount']));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getImportFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'global_attributes.import_step3'));
		$formBuilder->add($basicGroup);

		$basicGroup->add('description1', 'description', array('value' => trans('global_attributes.import_data_imported')));
		$basicGroup->add('addedCount', 'static', array('label' => 'global_attributes.import_new_records', 'value' => $formData['addedCount']));
		$basicGroup->add('updatedCount', 'static', array('label' => 'global_attributes.import_updated_records', 'value' => $formData['updatedCount']));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getStartForm($formData)
	{
		return $this->getStartFormBuilder($formData)->getForm();
	}

	/**
	 * @param $formData$formData
	 * @return mixed
	 */
	public function getAssignForm($formData)
	{
		return $this->getAssignFormBuilder($formData)->getForm();
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getImportForm($formData)
	{
		return $this->getImportFormBuilder($formData)->getForm();
	}
}
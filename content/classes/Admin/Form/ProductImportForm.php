<?php
/**
 * Class Admin_Form_CategoryImportForm
 */
class Admin_Form_ProductImportForm
{
	/**
	 * @param $formData
	 * @param $categoryOptions
	 * @return core_Form_FormBuilder
	 */
	public function getStartFormBuilder($formData, $categoryOptions)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'product.import_step1'));

		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('product.import_step1_description'))
		);

		if (count($categoryOptions) > 0)
		{
			$basicGroup->add(
				'cid', 'choice',
				array('label' => 'product.import_select_default_category', 'value' => '0', 'options' => $categoryOptions)
			);
		}
		else
		{
			$basicGroup->add(
				'category_name', 'text',
				array(
					'label' => 'product.product_form.add_category',
					'value' => '', 'required' => true, 'attr' => array('maxlength' => '255'),
					'note' => 'product.product_form.add_category_tooltip'
				)
			);
		}

		$basicGroup->add(
			'fields_separator', 'choice',
			array('label' => 'product.import_delimiter', 'value' => ',', 'options' => array(','=>'Comma (,)', ';'=>'Semicolon (;)'))
		);

		$basicGroup->add(
			'update_rule', 'choice',
			array(
				'label' => 'product.import_remove_products_query', 'value' => ',',
				'options' => array(
					'keep' => 'product.import_keep_products', /* Update existing products & Add new products */
					'clean' => 'product.import_remove_products', /* Remove all existing products & Add new products */
					//'update_only' => 'Update existing products only & Do not add new products',
					//'overwrite' => 'Update existing products, reset fields to default values & Add new products'
				),
				'note' => 'product.import_overwrite_tooltip'
			)
		);

		$basicGroup->add(
			'make_products_visible', 'choice',
			array(
				'label' => 'product.import_show_uploaded_products', 'value' => ',',
				'options' => array(
					'Yes' => 'product.import_show_uploaded_products_yes',
					'No' => 'product.import_show_uploaded_products_no'
				),
				'note' => 'product.import_show_uploaded_products_tooltip'
			)
		);

		$attributesImportLimitOptions = array();
		for ($i = 5; $i <= 100; $i++) $attributesImportLimitOptions[] = new core_Form_Option($i, $i);

		$basicGroup->add(
			'attributes_import_limit', 'choice',
			array(
				'label' => 'product.import_attributes_limit', 'value' => ',',
				'options' => $attributesImportLimitOptions
			)
		);

		$imagesImportLimitOptions = array();
		for ($imageCount = 5; $imageCount <= 100; $imageCount++) $imagesImportLimitOptions[] = new core_Form_Option($imageCount, $imageCount);
		$basicGroup->add(
			'images_import_limit', 'choice',
			array(
				'label' => 'product.import_images_limit', 'value' => ',',
				'options' => $imagesImportLimitOptions
			)
		);

		$basicGroup->add(
			'strip_tags', 'checkbox',
			array(
				'label' => 'product.product_import_strip_tags',
				'note' => 'product.product_import_strip_tags_tooltip',
				'wrapperClass' => 'field-checkbox-space'
			)
		);

		$basicGroup->add(
			'bulk', 'file',
			array(
				'label' => 'product.import_choose_csv',
				'required' => true,
				'attr' => array(
					'accept' => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
				),
				'wrapperClass' => 'col-sm-6 col-xs-12 clear-both'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getAssignFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'product.import_step2'));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('product.import_step2_description'))
		);

		$basicGroup->add('skip_first_line', 'checkbox', array('label' => 'product.import_skip_first_line', 'value'=>'1', 'current_value' => isset($formData['skip_first_line']) && $formData['skip_first_line'] == '1' ? '1' : '0'));
		$basicGroup->add('linesCount', 'static', array('label' => 'product.import_total_lines', 'value' => $formData['totalLinesCount']));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getImportFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'product.import_step3'));
		$formBuilder->add($basicGroup);

		$basicGroup->add('description1', 'description', array('value' => trans('product.import_data_imported')));
		if ($formData['skippedLineCount'] > 0)
		{
			$basicGroup->add('description2', 'description', array('value' => 'There were '.$formData['skippedLineCount'].' lines skipped because one or more required columns were left blank'));
		}
		$basicGroup->add('addedCount', 'static', array('label' => 'product.import_new_records', 'value' => $formData['addedCount']));
		$basicGroup->add('updatedCount', 'static', array('label' => 'product.import_updated_records', 'value' => $formData['updatedCount']));

		if (isset($formData['deletedCount']))
		{
			$basicGroup->add('deletedCount', 'static', array('label' => 'product.import_removed_records', 'value' => $formData['deletedCount']));
		}

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @param $categoryOptions
	 * @return core_Form_Form
	 */
	public function getStartForm($formData, $categoryOptions)
	{
		return $this->getStartFormBuilder($formData, $categoryOptions)->getForm();
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getAssignForm($formData)
	{
		return $this->getAssignFormBuilder($formData)->getForm();
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getImportForm($formData)
	{
		return $this->getImportFormBuilder($formData)->getForm();
	}
}

<?php
/**
 * Class Admin_Form_CountryForm
 */
class Admin_Form_CountryForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $id = null)
	{
		$translator = Framework_Translator::getInstance();

		$request = Framework_Request::createFromGlobals();
	
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'Country Details'));

		$formBuilder->add($basicGroup);

		$basicGroup->add('name', 'text', array('label' => 'countries.name', 'value' => $formData['name'], 'required' => true, 'attr' => array('maxlength' => '100'), 'note' => 'countries.iso_tooltip'));
		if(isset($formData['readonly']))
		{
			$basicGroup->add('iso_a2', 'text', array('label' => 'countries.iso_2', 'value' => $formData['iso_a2'], 'required' => true, 'attr' => array('maxlength' => '2', 'readonly'=>'readonly'), 'note' => 'countries.iso_tooltip'));

			$basicGroup->add('iso_a3', 'text', array('label' => 'countries.iso_3', 'value' => $formData['iso_a3'], 'required' => true, 'attr' => array('maxlength' => '3', 'readonly'=>'readonly'), 'note' => 'countries.iso_tooltip'));

			$basicGroup->add('iso_number', 'text', array('label' => 'countries.iso_number', 'value' => $formData['iso_number'], 'required' => true, 'attr' => array('maxlength' => '4', 'readonly'=>'readonly'), 'note' => 'countries.iso_tooltip'));

			$basicGroup->add('country_priority', 'text', array('label' => 'countries.country_priority', 'value' => $formData['country_priority'], 'required' => true, 'attr' => array('maxlength' => '3', 'readonly'=>'readonly')));
		}
		else
		{
			$basicGroup->add('iso_a2', 'text', array('label' => 'countries.iso_2', 'value' => $formData['iso_a2'], 'required' => true, 'attr' => array('maxlength' => '2'), 'note' => 'countries.iso_tooltip'));

			$basicGroup->add('iso_a3', 'text', array('label' => 'countries.iso_3', 'value' => $formData['iso_a3'], 'required' => true, 'attr' => array('maxlength' => '3'), 'note' => 'countries.iso_tooltip'));

			$basicGroup->add('iso_number', 'text', array('label' => 'countries.iso_number', 'value' => $formData['iso_number'], 'required' => true, 'attr' => array('maxlength' => '4'), 'note' => 'countries.iso_tooltip'));

			$basicGroup->add('country_priority', 'text', array('label' => 'countries.country_priority', 'value' => $formData['country_priority'], 'attr' => array('maxlength' => '3')));
		}

		$attributes = array('data-value' => $formData['available']);
		$zipRequiredAttributes = array('data-value' => $formData['zip_required']);
		$hasProvinces = array('data-value' => $formData['has_provinces']);
		if (intval($formData['coid']) > 0)
		{
			$attributes['id'] = 'field-available-edit';
			$zipRequiredAttributes['id'] = 'field-zip_required-edit';
			$hasProvinces['id'] = 'field-has_provinces-edit';
		}
		$basicGroup->add(
			'available', 'checkbox',
			array(
				'label' => 'countries.active', 'value' => 'Yes', 'required' => true,
				'current_value' => $formData['available'],
				'wrapperClass' => 'clear-both',
				'attr' => $attributes,
			)
		);
		
		$basicGroup->add(
			'zip_required', 'checkbox',
			array(
				'label' => 'countries.zip_required', 'value' => 1, 'required' => true,
				'current_value' => $formData['zip_required'],
				'attr' => $zipRequiredAttributes
			)
		);
		
		$basicGroup->add(
			'has_provinces', 'checkbox',
			array(
				'label' => 'countries.has_states_provinces', 'value' => 1, 'required' => true,
				'current_value' => $formData['has_provinces'],
				'attr' => $hasProvinces
			)
		);

		return $formBuilder;
	}

	/**
	 * Get country form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}
}
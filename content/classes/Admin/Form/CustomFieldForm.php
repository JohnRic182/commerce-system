<?php

/**
 * Class Admin_Form_FormsFieldsCustomFieldsForm
 */
class Admin_Form_CustomFieldForm
{
	/**
	 * Get custom field form builder
	 *
	 * @param $formId
	 *
	 * @return core_Form_FormBuilder
	 *
	 * @throws Exception
	 */
	public function getBuilder($formId)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$formBuilder->add('customField[field_id]', 'hidden', array('value' => 'new'));
		$formBuilder->add('customField[custom_form_id]', 'hidden', array('value' => $formId));
		$formBuilder->add('customField[field_name]', 'text', array('label' => 'custom_fields.field_name', 'placeholder' => 'custom_fields.field_name', 'required' => true, 'attr' => array('maxlength' => '250')));
		$formBuilder->add('customField[field_code]', 'text', array('label' => 'custom_fields.field_code', 'placeholder' => 'custom_fields.field_code', 'required' => true, 'attr' => array('maxlength' => '250')));
		$formBuilder->add('customField[field_caption]', 'text', array('label' => 'custom_fields.field_caption', 'required' => false, 'attr' => array('maxlength' => '250')));
		$formBuilder->add('customField[is_active]', 'checkbox', array('label' => 'custom_fields.active_interrogative', 'value' => '1', 'wrapperClass' => 'clear-both'));
		$formBuilder->add('customField[is_required]', 'checkbox', array('label' => 'custom_fields.required_interrogative', 'value' => '1'));

		$fieldTypes = array(
			new core_Form_Option('text', 'Text Input'),
			new core_Form_Option('select', 'Drop-down'),
			new core_Form_Option('radio', 'Radio Buttons'),
			new core_Form_Option('checkbox', 'Checkbox'),
			new core_Form_Option('textarea', 'Text Area')
		);

		$formBuilder->add('customField[field_type]', 'choice', array('label' => 'custom_fields.field_type', 'options' =>  $fieldTypes, 'required' => true));

		$fieldPriorities = array();
		for ($i=1; $i <= 20; $i++)
		{
			$fieldPriorities[] = new core_Form_Option($i, $i);
		}

		$formBuilder->add('customField[priority]', 'choice', array('label' => 'custom_fields.position', 'options' => $fieldPriorities, 'required' => true));

		$formBuilder->add('customField[text_length]', 'text', array('label' => 'custom_fields.character_limit', 'placeholder' => 'custom_fields.character_limit', 'required' => true));
		$formBuilder->add('customField[options]', 'textarea', array('required' => true, 'label' => 'custom_fields.options'));
		$formBuilder->add('customField[value_checked]', 'text', array('label' => 'custom_fields.checked_value', 'placeholder' => 'custom_fields.checked_value', 'required' => true));
		$formBuilder->add('customField[value_unchecked]', 'text', array('label' => 'custom_fields.unchecked_value', 'placeholder' => 'custom_fields.unchecked_value', 'required' => true));

		return $formBuilder;
	}

	/**
	 * Get custom form field form
	 *
	 * @param $formId
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formId)
	{
		return $this->getBuilder($formId)->getForm();
	}
}
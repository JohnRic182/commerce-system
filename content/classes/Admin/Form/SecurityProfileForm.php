<?php

/**
 * Class Admin_Form_SecurityProfileForm
 */
class Admin_Form_SecurityProfileForm
{
	/**
	 * @param $formData
	 * @param bool $forcePassword
	 * @param bool $forceSecurityQuestion
	 * @return core_Form_Form
	 */
	public function getForm($formData, $forcePassword = false, $forceSecurityQuestion = false)
	{
		$generalFormBuilder = new core_Form_FormBuilder('general');

		if ($forcePassword)
		{
			$generalFormBuilder->add('password', 'password', array('label' => 'admin.profile.password', 'value' => '', 'required' => $forcePassword, 'note' => 'admin.profile.password_hint'));
			$generalFormBuilder->add('password2', 'password', array('label' => 'admin.profile.password2', 'value' => '', 'required' => $forcePassword));
		}

		if ($forceSecurityQuestion)
		{
			$securityQuestionOptions = array(
				new core_Form_Option('', 'admin.profile.security_question_not_selected'),
				new core_Form_Option('1', 'admin.profile.security_question_1'),
				new core_Form_Option('2', 'admin.profile.security_question_2'),
				new core_Form_Option('3', 'admin.profile.security_question_3'),
				new core_Form_Option('4', 'admin.profile.security_question_4'),
				new core_Form_Option('5', 'admin.profile.security_question_5'),
				new core_Form_Option('6', 'admin.profile.security_question_6'),
				new core_Form_Option('7', 'admin.profile.security_question_7'),
			);

			$generalFormBuilder->add(
				'security_question_id', 'choice',
				array(
					'label' => 'admin.profile.security_question',
					'attr' => array('class' => 'large'),
					'required' => $forceSecurityQuestion,
					'value' => $formData['security_question_id'],
					'multiple' => false,
					'expanded' => false,
					'options' => $securityQuestionOptions,
					'wrapperClass' => 'clear-both'
				)
			);

			$generalFormBuilder->add(
				'security_question_answer', 'text',
				array(
					'label' => 'admin.profile.security_question_answer',
					'attr' => array('class' => 'large'),
					'required' => $forceSecurityQuestion,
					'value' => ''
				)
			);
		}

		return $generalFormBuilder->getForm();
	}
}
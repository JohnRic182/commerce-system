<?php

/**
 * Class Admin_Form_CatalogSettingsForm
 */
class Admin_Form_CatalogSettingsForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        /** @var core_Form_FormBuilder $formBuilder */
        $formBuilder = new core_Form_FormBuilder('general');

        /**
         * Catalog settings
         */
        $catalogGroup = new core_Form_FormBuilder(
        	'catalog',
	        array(
	        	'label' => 'catalog_settings.catalog_settings'
	        )
        );

        $formBuilder->add($catalogGroup);

        $catalogGroup->add(
        	'CatalogDefaultSortOrder',
	        'choice', array(
                'label' => 'catalog_settings.catalog_default_sort_order',
                'value' => $data['CatalogDefaultSortOrder'],
                'options' => array(
	                'priority' => 'catalog_settings.catalog_default_sort_order_options.priority',
	                'date' => 'catalog_settings.catalog_default_sort_order_options.date',
	                'name' => 'catalog_settings.catalog_default_sort_order_options.name',
	                'id' => 'catalog_settings.catalog_default_sort_order_options.id',
	                'price_desc' => 'catalog_settings.catalog_default_sort_order_options.price_desc',
	                'price' => 'catalog_settings.catalog_default_sort_order_options.price'
                )
	        )
        );

        $catalogGroup->add(
        	'CatalogItemsOnPage',
	        'integer',
	        array(
	        	'label' => 'catalog_settings.catalog_items_on_page',
		        'placeholder' => 'catalog_settings.catalog_items_on_page',
		        'value' => $data['CatalogItemsOnPage'],
		        'required' => true
	        )
        );

        $catalogGroup->add(
        	'CatalogDisplaySort',
	        'checkbox',
	        array(
	        	'label' => 'catalog_settings.catalog_display_sort',
		        'value' => 'YES',
		        'current_value' => $data['CatalogDisplaySort']
	        )
        );

        $catalogGroup->add(
        	'CatalogDisplayItems',
	        'checkbox',
	        array(
	        	'label' => 'catalog_settings.catalog_display_items',
		        'value' => 'YES',
		        'current_value' => $data['CatalogDisplayItems']
	        )
        );

        $catalogGroup->add(
        	'CatalogShowNested',
	        'checkbox',
	        array(
	        	'label' => 'catalog_settings.catalog_show_nested',
		        'value' => 'YES',
		        'current_value' => $data['CatalogShowNested']
	        )
        );

        $catalogGroup->add(
        	'CatalogOptimizeImages',
	        'checkbox',
	        array(
	        	'label' => 'catalog_settings.catalog_optimize_images',
		        'value' => 'YES',
		        'current_value' => $data['CatalogOptimizeImages']
	        )
        );

	    $catalogGroup->add(
	    	'CatalogHideEmptyCategories',
		    'checkbox',
		    array(
		    	'label' => 'catalog_settings.catalog_hide_empty_categories',
			    'value' => 'YES',
			    'current_value' => $data['CatalogHideEmptyCategories']
		    )
	    );

	    /**
	     * Predictive Search
	     */
	    $predictiveSearchGroup = new core_Form_FormBuilder(
	    	'predictive-search-settings',
		    array(
		    	'label' => 'catalog_settings.predictive_search_settings',
			    'collapsible' => true
		    )
	    );

	    $formBuilder->add($predictiveSearchGroup);

	    $predictiveSearchGroup->add(
	    	'PredictiveSearchEnabled',
		    'checkbox',
		    array(
		    	'label' => 'catalog_settings.predictive_search_enabled',
			    'value' => '1',
			    'current_value' => $data['PredictiveSearchEnabled']
		    )
	    );

	    $options = array();
	    for ($i=5; $i<=15; $i++) $options[$i] = $i;

	    $predictiveSearchGroup->add(
	    	'PredictiveSearchInProductsDescription',
		    'checkbox',
		    array(
		    	'label' => 'catalog_settings.predictive_search_in_products_description',
			    'value' => '1',
			    'current_value' => $data['PredictiveSearchInProductsDescription']
		    )
	    );

	    $predictiveSearchGroup->add(
	    	'PredictiveSearchInCategories',
		    'checkbox',
		    array(
		    	'label' => 'catalog_settings.predictive_search_in_categories',
			    'value' => '1',
			    'current_value' => $data['PredictiveSearchInCategories']
		    )
	    );

	    $predictiveSearchGroup->add(
	    	'PredictiveSearchInManufacturers',
		    'checkbox',
		    array(
		    	'label' => 'catalog_settings.predictive_search_in_manufacturers',
			    'value' => '1',
			    'current_value' => $data['PredictiveSearchInManufacturers']
		    )
	    );

	    $predictiveSearchGroup->add(
	    	'PredictiveSearchResultsCount',
		    'choice',
		    array(
		    	'label' => 'catalog_settings.predictive_search_results_count',
			    'value' => $data['PredictiveSearchResultsCount'],
			    'options' => $options
		    )
	    );
		
        /**
         * Price ranges
         */
        $priceRangesGroup = new core_Form_FormBuilder(
        	'price-ranges-settings',
	        array(
	        	'label' => 'catalog_settings.price_range_filters',
		        'collapsible' => true
	        )
        );

        $formBuilder->add($priceRangesGroup);

        $priceRangesGroup->add(
        	'CatalogUsePriceRanges',
	        'checkbox',
	        array(
	        	'label' => 'catalog_settings.catalog_use_price_ranges',
		        'value' => '1',
		        'current_value' => $data['CatalogUsePriceRanges']
	        )
        );

        $priceRanges = unserialize(base64_decode($data['CatalogPriceRanges']));

        for ($i=1; $i<=10; $i++)
        {
            $priceRangesGroup->add(
            	'price_range_'.$i, 'minmaxnumeric',
	            array(
                    'label' => trans('catalog_settings.price_range', array('idx' => $i)),
	                'minValue' => $priceRanges[$i]['min'],
	                'maxValue' => $priceRanges[$i]['max'],
	                'wrapperClass' => 'clear-both',
	                'required' => true
	            )
            );
        }

        /**
         * Home
         */
        $homeGroup = new core_Form_FormBuilder(
        	'home',
	        array(
	        	'label' => 'catalog_settings.homepage_settings',
		        'collapsible' => true
	        )
        );

        $formBuilder->add($homeGroup);

        $homeGroup->add(
        	'CatalogHomeItemsOnPage',
	        'integer',
	        array(
	        	'label' => 'catalog_settings.catalog_home_items_on_page',
		        'placeholder' => 'catalog_settings.catalog_home_items_on_page',
		        'value' => $data['CatalogHomeItemsOnPage'],
		        'required' => true
	        )
        );

        $homeGroup->add(
        	'CatalogHomeLink',
	        'text',
	        array(
	        	'label' => 'catalog_settings.catalog_home_link',
		        'placeholder' => 'catalog_settings.catalog_home_link',
		        'value' => $data['CatalogHomeLink'],
		        'note' => 'catalog_settings.notes.catalog_home_link',
		        'required' => true
	        )
        );

        /**
         * Product page settings
         */
        $productPageGroup = new core_Form_FormBuilder(
        	'product-page',
	        array(
	        	'label' => 'catalog_settings.product_page_settings',
		        'collapsible' => true
	        )
        );

        $formBuilder->add($productPageGroup);

        $options = array();
        for ($i=1; $i<=24; $i++) $options[$i] = $i;

        $productPageGroup->add(
        	'EnableRecommendedProducts',
	        'checkbox',
	        array(
	            'label' => 'catalog_settings.enable_recommended_products',
	            'value' => 'Yes',
	            'current_value' => $data['RecommendedProductsCount'] > 0 ? 'Yes' : 'No',
	            'wrapperClass' => 'clear-both',
            )
        );

        $productPageGroup->add(
        	'RecommendedProductsCount',
	        'choice',
	        array(
	            'label' => 'catalog_settings.recommended_products_count',
	            'value' => $data['RecommendedProductsCount'] > 0 ? $data['RecommendedProductsCount'] : $data['defaults']['RecommendedProductsCount'],
	            'options' => $options
            )
        );

        $productPageGroup->add(
        	'EnableSiblingProducts',
	        'checkbox',
	        array(
	            'label' => 'catalog_settings.enable_sibling_products',
	            'value' => 'Yes',
	            'current_value' => $data['SiblingProductsCount'] > 0 ? 'Yes' : 'No',
	            'wrapperClass' => 'clear-both',
	        )
        );

        $productPageGroup->add(
        	'SiblingProductsCount',
	        'choice',
	        array(
	            'label' => 'catalog_settings.sibling_products_count',
	            'value' => $data['SiblingProductsCount'] > 0 ? $data['SiblingProductsCount'] : $data['defaults']['SiblingProductsCount'],
	            'options' => $options
	        )
        );

        $productPageGroup->add(
        	'CatalogEmailToFriend',
	        'checkbox',
	        array(
	            'label' => 'catalog_settings.catalog_email_to_friend',
	            'value' => 'HTML',
	            'current_value' => $data['CatalogEmailToFriend'],
	            'wrapperClass' => 'clear-both'
	        )
        );

        /**
         * Category image group
         */
        $categoryImageGroup = new core_Form_FormBuilder(
        	'category-image',
	        array(
	        	'label' => 'catalog_settings.category_image_settings',
		        'collapsible' => true
	        )
        );

        $formBuilder->add($categoryImageGroup);

        // image
        $categoryImageGroup->add(
        	'CatalogCategoryImageSize',
	        'integer',
	        array(
	            'label' => 'catalog_settings.catalog_category_image_size',
	            'value' => $data['CatalogCategoryImageSize'],
	            'note' => trans('catalog_settings.notes.image_size', array('min_size' => '50', 'max_size' => '150')),
	            'placeholder' => 'catalog_settings.catalog_category_image_size',
	            'required' => true
	        )
        );

        $categoryImageGroup->add(
        	'CatalogCategoryImageType',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_category_image_type',
	            'value' => $data['CatalogCategoryImageType'],
	            'options' => array(
	                'Square' => 'catalog_settings.image_type_options.square',
	                'Proportionate' => 'catalog_settings.image_type_options.proportionate',
	                'Manual' => 'catalog_settings.image_type_options.manual'
	            )
	        )
        );

        $categoryImageGroup->add(
        	'CatalogCategoryImageResizeRule',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_category_image_resize_rule',
	            'value' => $data['CatalogCategoryImageResizeRule'],
	            'options' => array(
	                'Resize' => 'catalog_settings.image_resize_rule_options.resize',
	                'Smart' => 'catalog_settings.image_resize_rule_options.smart',
	                'Keep' => 'catalog_settings.image_resize_rule_options.keep'
	            ),
	            'note' => 'catalog_settings.notes.resize_rule'
	        )
        );

        // thumb
        $categoryImageGroup->add(
        	'CatalogCategoryThumbAuto',
	        'checkbox',
	        array(
	            'label' => 'catalog_settings.catalog_category_thumb_auto',
	            'value' => 'YES',
	            'current_value' => $data['CatalogCategoryThumbAuto'],
	            'note' => 'catalog_settings.notes.catalog_category_thumb_auto',
	            'wrapperClass' => 'clear-both'
	        )
        );

        $categoryImageGroup->add(
        	'CatalogCategoryThumbSize',
	        'integer',
	        array(
	            'label' => 'catalog_settings.catalog_category_thumb_size',
	            'value' => $data['CatalogCategoryThumbSize'],
	            'note' => trans('catalog_settings.notes.image_size', array('min_size' => '50', 'max_size' => '150')),
	            'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
		        'placeholder' => 'catalog_settings.catalog_category_thumb_size',
	            'required' => true
	        )
        );

        $categoryImageGroup->add(
        	'CatalogCategoryThumbType',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_category_thumb_type',
	            'value' => $data['CatalogCategoryThumbType'],
	            'options' => array(
	                'Square' => 'catalog_settings.image_type_options.square',
	                'Proportionate' => 'catalog_settings.image_type_options.proportionate',
	                'Manual' => 'catalog_settings.image_type_options.manual'
	            )
	        )
        );

        $categoryImageGroup->add(
        	'CatalogCategoryThumbResizeRule',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_category_thumb_resize_rule',
	            'value' => $data['CatalogCategoryThumbResizeRule'],
	            'options' => array(
	                'Resize' => 'catalog_settings.image_resize_rule_options.resize',
	                'Smart' => 'catalog_settings.image_resize_rule_options.smart',
	                'Keep' => 'catalog_settings.image_resize_rule_options.keep'
	            ),
	            'note' => 'catalog_settings.notes.resize_rule'
	        )
        );

        /**
         * Product image group
         */
        $productImageGroup = new core_Form_FormBuilder(
        	'product-image',
	        array(
	        	'label' => 'catalog_settings.product_image_settings',
		        'collapsible' => true
	        )
        );

        $formBuilder->add($productImageGroup);

        // product page

        $productImageGroup->add(
        	'CatalogThumb2Size',
	        'integer',
	        array(
	            'label' => 'catalog_settings.catalog_thumb2_size',
	            'value' => $data['CatalogThumb2Size'],
	            'note' => trans('catalog_settings.notes.image_size', array('min_size' => '200', 'max_size' => '400')),
	            'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
		        'placeholder' => 'catalog_settings.catalog_thumb2_size',
	            'required' => true
	        )
        );

        $productImageGroup->add(
        	'CatalogThumb2Type',
	        'choice', array(
	            'label' => 'catalog_settings.catalog_thumb2_type',
	            'value' => $data['CatalogThumb2Type'],
	            'options' => array(
	                'Square' => 'catalog_settings.image_type_options.square',
	                'Proportionate' => 'catalog_settings.image_type_options.proportionate',
	                'Manual' => 'catalog_settings.image_type_options.manual'
	            )
	        )
        );

        $productImageGroup->add(
        	'CatalogThumb2ResizeRule',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_thumb2_resize_rule',
	            'value' => $data['CatalogThumb2ResizeRule'],
	            'options' => array(
	                'Resize' => 'catalog_settings.image_resize_rule_options.resize',
	                'Smart' => 'catalog_settings.image_resize_rule_options.smart',
	                'Keep' => 'catalog_settings.image_resize_rule_options.keep'
	            ),
	            'note' => 'catalog_settings.notes.resize_rule'
	        )
        );

        // catalog thumb
        $productImageGroup->add(
        	'CatalogThumbSize',
	        'integer',
	        array(
	            'label' => 'catalog_settings.catalog_thumb_size',
	            'value' => $data['CatalogThumbSize'],
	            'note' => trans('catalog_settings.notes.image_size', array('min_size' => '50', 'max_size' => '150')),
	            'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
		        'placeholder' => 'catalog_settings.catalog_thumb_size',
	            'required' => true
	        )
        );

        $productImageGroup->add(
        	'CatalogThumbType',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_thumb_type',
	            'value' => $data['CatalogThumbType'],
	            'options' => array(
	                'Square' => 'catalog_settings.image_type_options.square',
	                'Proportionate' => 'catalog_settings.image_type_options.proportionate',
	                'Manual' => 'catalog_settings.image_type_options.manual'
	            ),
	            'required' => true
	        )
        );

        $productImageGroup->add(
        	'CatalogThumbResizeRule',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_thumb2_resize_rule',
	            'value' => $data['CatalogThumbResizeRule'],
	            'options' => array(
	                'Resize' => 'catalog_settings.image_resize_rule_options.resize',
	                'Smart' => 'catalog_settings.image_resize_rule_options.smart',
	                'Keep' => 'catalog_settings.image_resize_rule_options.keep'
	            ),
	            'note' => 'catalog_settings.notes.resize_rule'
	        )
        );

	    // product secondary thumb
	    $productImageGroup->add(
	    	'ProductSecondaryThumbSize',
		    'integer',
		    array(
			    'label' => 'catalog_settings.product_secondary_thumb_size',
			    'value' => $data['ProductSecondaryThumbSize'],
			    'note' => trans('catalog_settings.notes.image_size', array('min_size' => '50', 'max_size' => '150')),
			    'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
			    'placeholder' => 'catalog_settings.product_secondary_thumb_size',
			    'required' => true
		    )
	    );

	    $productImageGroup->add(
	    	'ProductSecondaryThumbType',
		    'choice',
		    array(
			    'label' => 'catalog_settings.product_secondary_thumb_type',
			    'value' => $data['ProductSecondaryThumbType'],
			    'options' => array(
				    'Square' => 'catalog_settings.image_type_options.square',
				    'Proportionate' => 'catalog_settings.image_type_options.proportionate',
				    'Manual' => 'catalog_settings.image_type_options.manual'
			    ),
			    'required' => true
		    )
	    );

	    $productImageGroup->add(
	    	'ProductSecondaryThumbResizeRule',
		    'choice',
		    array(
			    'label' => 'catalog_settings.product_secondary_thumb_resize_rule',
			    'value' => $data['ProductSecondaryThumbResizeRule'],
			    'options' => array(
				    'Resize' => 'catalog_settings.image_resize_rule_options.resize',
				    'Smart' => 'catalog_settings.image_resize_rule_options.smart',
				    'Keep' => 'catalog_settings.image_resize_rule_options.keep'
			    ),
			    'note' => 'catalog_settings.notes.resize_rule'
		    )
	    );

        $productImageGroup->add(
        	'ZoomOption',
	        'choice',
	        array(
	            'label' => 'catalog_settings.product_zoom_option',
	            'value' => $data['ZoomOption'],
	            'options' => array(
	                'none' => 'catalog_settings.product_zoom_options_list.none',
	                'zoom' => 'catalog_settings.product_zoom_options_list.zoom',
	                'magnify' => 'catalog_settings.product_zoom_options_list.magnify',
	                'magicthumb' => 'catalog_settings.product_zoom_options_list.magicthumb',
	                'imagelayover' => 'catalog_settings.product_zoom_options_list.imagelayover'
	            ),
	            'wrapperClass' => 'clear-both',
	        )
        );

        $productImageGroup->add(
        	'ZoomOptionSecondary',
	        'choice',
	        array(
	            'label' => 'catalog_settings.product_zoom_option_secondary',
	            'value' => $data['ZoomOptionSecondary'],
	            'options' => array(
	                'none' => 'catalog_settings.product_zoom_options_list_secondary.none',
	                'magicthumb' => 'catalog_settings.product_zoom_options_list_secondary.magicthumb',
	                'imagelayover' => 'catalog_settings.product_zoom_options_list_secondary.imagelayover'
	            ),
	        )
        );

        /**
         * Manufacturer image group
         */
        $manufacturerGroup = new core_Form_FormBuilder(
        	'manufacturer-settings',
	        array(
	        	'label' => 'catalog_settings.manufacturer_image_settings',
		        'collapsible' => true
	        )
        );

        $formBuilder->add($manufacturerGroup);

        $manufacturerGroup->add(
        	'CatalogManufacturerImageDisplay',
	        'checkbox',
	        array(
	            'label' => 'catalog_settings.catalog_manufacturer_image_display',
	            'value' => 'YES',
	            'current_value' => $data['CatalogManufacturerImageDisplay'],
	            'note' => 'If checked then manufacturer image will be displayed on the catalog page.'
	        )
        );

        // image
        $manufacturerGroup->add(
        	'CatalogManufacturerImageSize',
	        'integer',
	        array(
	            'label' => 'catalog_settings.catalog_manufacturer_image_size',
	            'value' => $data['CatalogManufacturerImageSize'],
	            'note' => trans('catalog_settings.notes.image_size', array('min_size' => '50', 'max_size' => '150')),
	            'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
		        'placeholder' => 'catalog_settings.catalog_manufacturer_image_size',
	            'required' => true
	        )
        );

        $manufacturerGroup->add(
        	'CatalogManufacturerImageType',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_manufacturer_image_type',
	            'value' => $data['CatalogManufacturerImageType'],
	            'options' => array(
	                'Square' => 'catalog_settings.image_type_options.square',
	                'Proportionate' => 'catalog_settings.image_type_options.proportionate',
	                'Manual' => 'catalog_settings.image_type_options.manual'
	            )
	        )
        );

        $manufacturerGroup->add(
        	'CatalogManufacturerImageResizeRule',
	        'choice',
	        array(
	            'label' => 'catalog_settings.catalog_manufacturer_image_resize_rule',
	            'value' => $data['CatalogManufacturerImageResizeRule'],
	            'options' => array(
	                'Resize' => 'catalog_settings.image_resize_rule_options.resize',
	                'Smart' => 'catalog_settings.image_resize_rule_options.smart',
	                'Keep' => 'catalog_settings.image_resize_rule_options.keep'
	            ),
	            'note' => 'catalog_settings.notes.resize_rule'
	        )
        );

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
<?php

/**
 * Class Admin_Form_DigitalDownloadForm
 */
class Admin_Form_DigitalDownloadSettingsForm
{
	private $basicGroup;

	/**
	 * @param $data
	 * @return core_Form_FormBuilder
	 * @throws Exception
	 */
	public function getBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('digitalDownload');

		$generalGroup = new core_Form_FormBuilder('general', array('label'=>''));
		$formBuilder->add($generalGroup);

		$generalGroup->add(
			'EnableDigitalDownload', 'checkbox',
			array(
				'label' => 'Enable Digital Downloads?',
				'value' => 1,
				'current_value' => $data['EnableDigitalDownload'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $data['EnableDigitalDownload'])
			)
		);

		$basicGroup = $this->basicGroup = new core_Form_FormBuilder('digitalDownloadLimit', array('label' => 'Digital Downloads Settings'));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'DigitalProductsDownloadLimit', 'text',
			array(
				'label' => 'Number of times digital product can be downloaded',
				'value' =>  $data['DigitalProductsDownloadLimit'],
				'required' => true,
			)
		);



		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getForm($data)
	{
		return $this->getBuilder($data)->getForm();
	}

	/**
	 * Returns our basic group form elements
	 * @return mixed
	 */
	public function getBasicGroup()
	{
		return $this->basicGroup;
	}
}
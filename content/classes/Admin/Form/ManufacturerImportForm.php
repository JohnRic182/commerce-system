<?php
/**
 * Class Admin_Form_ManufacturerForm
 */
class Admin_Form_ManufacturerImportForm
{
	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getStartFormBuilder($formData)
	{
		// TODO: fix global dependency
		global $label_app_name;

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'manufacturers.import_step1'));

		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('manufacturers.import_step1_description', array('label_app_name' => $label_app_name)))
		);

		$basicGroup->add(
			'fields_separator', 'choice',
			array('label' => 'manufacturers.import_delimiter', 'value' => ',', 'options' => array(','=>'Comma (,)', ';'=>'Semicolon (;)'))
		);

		$basicGroup->add(
			'make_manufacturers_visible', 'choice',
			array(
				'label' => 'manufacturers.import_show_uploaded_manufacturers', 'value' => ',', 'options' => array('Yes'=>'manufacturers.import_show_uploaded_manufacturers_yes', 'No'=>'manufacturers.import_show_uploaded_manufacturers_no'),
				'note' => 'manufacturers.import_show_uploaded_manufacturers_tooltip'
			)
		);

		$basicGroup->add(
			'make_manufacturers_visible_type', 'choice',
			array(
				'label' => 'manufacturers.import_change_visibility', 'value' => ',', 'options' => array('inserted'=>'manufacturers.import_inserted_only', 'updated'=>'manufacturers.import_inserted_and_updated'),
				'note' => 'manufacturers.import_apply_choice'
			)
		);

		$basicGroup->add('bulk', 'file',
			array(
				'label' => 'manufacturers.import_choose_csv',
				'required' => true,
				'attr' => array(
					'accept' => '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
				),
				'wrapperClass' => 'col-sm-3 col-xs-12'
			)
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getAssignFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'manufacturers.import_step2'));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('manufacturers.import_step2_description'))
		);

		$basicGroup->add('skip_first_line', 'checkbox', array('label' => 'manufacturers.import_skip_first_line', 'value'=>'1', 'current_value' => isset($formData['skip_first_line']) && $formData['skip_first_line'] == '1' ? '1' : '0'));
		$basicGroup->add('linesCount', 'static', array('label' => 'manufacturers.import_total_lines', 'value' => $formData['totalLinesCount']));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getImportFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'manufacturers.import_step3'));
		$formBuilder->add($basicGroup);

		$basicGroup->add('description1', 'description', array('value' => trans('manufacturers.import_data_imported')));
		$basicGroup->add('addedCount', 'static', array('label' => 'manufacturers.import_new_records', 'value' => $formData['addedCount']));
		$basicGroup->add('updatedCount', 'static', array('label' => 'manufacturers.import_updated_records', 'value' => $formData['updatedCount']));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getStartForm($formData)
	{
		return $this->getStartFormBuilder($formData)->getForm();
	}

	/**
	 * @param $formData$formData
	 * @return mixed
	 */
	public function getAssignForm($formData)
	{
		return $this->getAssignFormBuilder($formData)->getForm();
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getImportForm($formData)
	{
		return $this->getImportFormBuilder($formData)->getForm();
	}
}
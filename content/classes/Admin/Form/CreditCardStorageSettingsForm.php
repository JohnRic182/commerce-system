<?php

/**
 * Class Admin_Form_CreditCardStorageSettingsForm
 */
class Admin_Form_CreditCardStorageSettingsForm
{
	/**
	 * Get CCS form builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'ccsSettings[SecurityCCSActive]', 'checkbox',
			array(
				'label' => 'Enable storage of cardholder data?',
				'value' => '1',
				'current_value' => $formData['SecurityCCSActive'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['SecurityCCSActive'])
			)
		);

		$lengthOptions = array();
		for ($i = 1; $i <= 90; $i++)  $lengthOptions[] = new core_Form_Option($i, $i);

		$basicGroup->add(
			'ccsSettings[SecurityCCSStoredFor]', 'choice',
			array(
				'label' => 'Length of Time to Store Cardholder Data, Days',
				'value' => $formData['SecurityCCSStoredFor'],
				'options' => $lengthOptions,
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['SecurityCCSStoredFor'])
			)
		);

		if ($formData['SecurityCCSCertificate'] == '')
		{
			$secureAccessCredentialsGroup = new core_Form_FormBuilder('secureAccess', array('label' => 'credit_card_storage.generate_certificate'));
			$formBuilder->add($secureAccessCredentialsGroup);

			$secureAccessCredentialsGroup->add(
				'dn[ccs_username]', 'text',
				array(
					'label' => 'credit_card_storage.username',
					'value' => '',
					'required' => true,
					'note' => 'credit_card_storage.username_note',
					'placeholder' => 'credit_card_storage.username'
				)
			);

			$secureAccessCredentialsGroup->add(
				'dn[ccs_password]', 'password',
				array(
					'label' => 'credit_card_storage.password',
					'value' => '',
					'required' => true,
					'wrapperClass' => 'clear-both',
					'note' => 'credit_card_storage.password_note',
					'placeholder' => 'credit_card_storage.password'
				)
			);

			$secureAccessCredentialsGroup->add(
				'dn[ccs_password2]', 'password',
				array(
					'label' => 'credit_card_storage.password2',
					'value' => '',
					'required' => true,
					'wrapperClass' => 'clear-both',
					'placeholder' => 'credit_card_storage.password2'
				)
			);
		}

		return $formBuilder;
	}

	/**
	 * Get CCS form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
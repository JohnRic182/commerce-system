<?php
/**
 * Class Admin_Form_QRCodesAdvancedSettingsForm
 */
class Admin_Form_QRCodeAdvancedSettingsForm
{
	/**
	 * Get advanced settings form for QR Codes - builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$translator = Framework_Translator::getInstance();

		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>''));
		$formBuilder->add($basicGroup);

		$basicGroup->add('qrSettings[qr_active]', 'checkbox', array(
				'label' => 'marketing.enable_qr_codes',
				'value' => 'Yes',
				'current_value' => $formData['qr_active'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['qr_active'])
		));
		
		$imageTypeOptions = array();
		foreach ($formData['imageTypeOptions'] as $key=>$val) {
			$imageTypeOptions[] = new core_Form_Option($key, $val);
		}

		$basicGroup->add('qrSettings[qr_image_type]', 'choice', array(
				'label' => 'marketing.default_image_type',
				'value' => $formData['qr_image_type'],
				'options' => $imageTypeOptions,
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['qr_image_type'])
		));

		$mediaOptions = array();
		foreach ($formData['mediaOptions'] as $key=>$val) {
			$mediaOptions[] = new core_Form_Option($key, $val);
		}
		
		$basicGroup->add('qrSettings[qr_media]', 'choice', array(
				'label' => 'marketing.default_media',
				'value' => $formData['qr_media'],
				'options' => $mediaOptions,
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['qr_media'])
		));

		return $formBuilder;
	}

	/**
	 * Get advanced QR Codes settings form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
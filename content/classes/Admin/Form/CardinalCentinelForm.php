<?php

/**
 * Class Admin_Form_CardinalCentinelForm
*/
class Admin_Form_CardinalCentinelForm
{
    /**
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('cardinalcommerce_URL', 'text', array('label' => 'apps.cardinal_centinel.field_url', 'required' => true, 'value' => $data['cardinalcommerce_URL'], 'placeholder' => 'apps.cardinal_centinel.field_url',));
        $settingsGroup->add('cardinalcommerce_Processor_Id', 'text', array('label' => 'apps.cardinal_centinel.field_processor_id', 'required' => true, 'value' => $data['cardinalcommerce_Processor_Id'], 'placeholder' => 'apps.cardinal_centinel.field_processor_id'));
        $settingsGroup->add('cardinalcommerce_Merchant_Id', 'text', array('label' => 'apps.cardinal_centinel.field_merchant_id', 'required' => true, 'value' => $data['cardinalcommerce_Merchant_Id'], 'placeholder' => 'apps.cardinal_centinel.field_merchant_id'));
        $settingsGroup->add('cardinalcommerce_Password', 'password', array('label' => 'apps.cardinal_centinel.field_password', 'required' => true, 'value' => $data['cardinalcommerce_Password'], 'placeholder' => 'apps.cardinal_centinel.field_password'));

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getForm($data)
    {
        return $this->getBuilder($data)->getForm();
    }
}
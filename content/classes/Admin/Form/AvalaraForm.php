<?php

/**
 * Class Admin_Form_AvalaraForm
 */
class Admin_Form_AvalaraForm
{
    /**
     * Holds our dynamic variables
     * @var $data
     */
    private $data;

    /**
     * Our dynamic getter
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        if (!array_key_exists($key, $this->data))
        {
            $this->data[$key] = null;
            return null;
        }
        else
        {
            return $this->data[$key];
        }
    }

    /**
     * Our dynamic variable setter
     * @param $key
     * @param $value
     */
    public function __set($key,$value)
    {
        $this->data[$key] = $value;
    }

    /**
     * Avalara main form
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getAvalaraFormBuilder($data)
    {
        $formBuilder = new core_Form_FormBuilder('settings');

        $settingsGroup = new core_Form_FormBuilder('basic');
        $formBuilder->add($settingsGroup);

        $settingsGroup->add('AvalaraAccountNumber', 'text', array('label' => 'apps.avalara.account_number', 'required' => true, 'value' => $data['AvalaraAccountNumber'], 'placeholder' => 'apps.avalara.account_number'));
        $settingsGroup->add('AvalaraLicenseKey', 'text', array('label' => 'apps.avalara.license_key', 'required' => true, 'value' => $data['AvalaraLicenseKey'], 'placeholder' => 'apps.avalara.license_key'));
        $settingsGroup->add('AvalaraCompanyCode', 'text', array('label' => 'apps.avalara.company_code', 'required' => true, 'value' => $data['AvalaraCompanyCode'], 'placeholder' => 'apps.avalara.company_code'));
        $settingsGroup->add('AvalaraURL', 'text', array('label' => 'apps.avalara.avalara_webservice_url', 'required' => true, 'value' => $data['AvalaraURL'], 'placeholder' => 'apps.avalara.avalara_webservice_url'));
        if (!$data['isActivateAction']) {
            $settingsGroup->add('AvalaraCurrencyCode', 'text', array('label' => 'apps.avalara.currency_code', 'value' => $data['AvalaraCurrencyCode'], 'placeholder' => 'apps.avalara.currency_code', 'note' => trans('apps.avalara.currency_note')));
        }

        return $formBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getAvalaraForm($data)
    {
        return $this->getAvalaraFormBuilder($data)->getForm();
    }

    /**
     * Our form for avalara tax codes
     * @param $data
     * @return core_Form_FormBuilder
     * @throws Exception
     */
    public function getAvalaraTaxCodeFormBuilder($data)
    {
        $taxCodeFormBuilder = new core_Form_FormBuilder('tax-code');

        $basicGroup = new core_Form_FormBuilder('basic');
        $taxCodeFormBuilder->add($basicGroup);

        $basicGroup->add('tax_code_name', 'text', array('label' => 'apps.avalara.tax_name', 'required' => true, 'value' => $data['name'], 'placeholder' => 'apps.avalara.tax_name'));
        $basicGroup->add('tax_code_code', 'text', array('label' => 'apps.avalara.tax_code', 'required' => true, 'value' => $data['code'], 'placeholder' => 'apps.avalara.tax_code'));

        return $taxCodeFormBuilder;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getAvalaraTaxCodeForm($data)
    {
        return $this->getAvalaraTaxCodeFormBuilder($data)->getForm();
    }
}
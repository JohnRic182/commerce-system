<?php

/**
 * Class Admin_Form_ProductCopyForm
 */
class Admin_Form_ProductCopyForm
{
    /**
     * @return core_Form_FormBuilder
     */
    public function getBuilder($formData)
    {
        $copyFormBuilder = new core_Form_FormBuilder('copy');

		$copyFormBuilder->add('copy[images]', 'checkbox', array('label' => 'Product Images', 'value' => '1', 'current_value' => $formData['images']));
        $copyFormBuilder->add('copy[advance_settings]', 'checkbox', array('label' => 'Advance Settings', 'value' => '1', 'current_value' => $formData['advance_settings']));
        $copyFormBuilder->add('copy[seo]', 'checkbox', array('label' => 'Search Engine Optimization', 'value' => '1', 'current_value' => $formData['seo']));
        $copyFormBuilder->add('copy[search_keywords]', 'checkbox', array('label' => 'Search Keywords', 'value' => '1', 'current_value' => $formData['search_keywords']));
        $copyFormBuilder->add('copy[attributes]', 'checkbox', array('label' => 'Attributes', 'value' => '1', 'current_value' => $formData['attributes']));
        $copyFormBuilder->add('copy[variants]', 'checkbox', array('label' => 'Variants', 'value' => '1', 'current_value' => $formData['variants']));
        $copyFormBuilder->add('copy[product_promotions]', 'checkbox', array('label' => 'Product Promotions', 'value' => '1', 'current_value' => $formData['product_promotions']));
        $copyFormBuilder->add('copy[quantity_discounts]', 'checkbox', array('label' => 'Quantity Discounts', 'value' => '1', 'current_value' => $formData['quantity_discounts']));

        return $copyFormBuilder;
    }

    /**
     * @return mixed
     */
    public function getForm($formData)
    {
        return $this->getBuilder($formData)->getForm();
    }
}
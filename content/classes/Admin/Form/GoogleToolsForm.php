<?php

/**
 * Class Admin_Form_GoogleToolsForm
 */
class Admin_Form_GoogleToolsForm
{
	/**
	 * Get form builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Google Analytics group
		 */
		$googleAnalyticsGroup = new core_Form_FormBuilder('google_analytics', array('label'=>'marketing.google_analytics'));

		$formBuilder->add($googleAnalyticsGroup);

		$googleAnalyticsGroup->add('GoogleAnalyticsActive', 'checkbox',
			array(
				'label' => 'marketing.enable_google_analytics',
				'value' => 'YES',
				'current_value' => $formData['GoogleAnalyticsActive'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['GoogleAnalyticsActive'])
			)
		);

		$googleAnalyticsGroup->add('GoogleAnalytics', 'textarea',
			array(
				'label' => 'marketing.analytics_tracking_code',
				'value' => $formData['GoogleAnalytics'],
				'wrapperClass' => 'clear-both',
				'attr' => array(),
				'note' => 'marketing.analytics_tracking_code_tooltip',
			)
		);

		$googleAnalyticsGroup->add('GoogleConversionActive', 'checkbox',
			array(
				'label' => 'marketing.enable_google_analytics_conversions',
				'value' => 'YES',
				'current_value' => $formData['GoogleConversionActive'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['GoogleConversionActive'])
			)
		);

		$googleAnalyticsGroup->add('GoogleConversionTrackingCode', 'textarea',
			array(
				'label' => 'marketing.conversion_tracking_code',
				'value' => $formData['GoogleConversionTrackingCode'],
				'wrapperClass' => 'clear-both',
				'attr' => array(),
				'note' => 'marketing.conversion_tracking_code_tooltip',
			)
		);

		/**
		 * Google Adwords group
		 */
		$googleAdwordsGroup = new core_Form_FormBuilder('google_adwords', array('label'=>'marketing.google_adwords', 'collapsible' => true));

		$formBuilder->add($googleAdwordsGroup);

		$googleAdwordsGroup->add('GoogleAdwordsConversionActive', 'checkbox',
			array(
				'label' => 'marketing.enable_google_adwords',
				'value' => 'YES',
				'current_value' => $formData['GoogleAdwordsConversionActive'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['GoogleAdwordsConversionActive'])
			)
		);

		$googleAdwordsGroup->add('CoogleAdwordsTrackingCode', 'textarea',
			array(
				'label' => 'marketing.adwords_tracking_code',
				'value' => $formData['CoogleAdwordsTrackingCode'],
				'wrapperClass' => 'clear-both',
				'attr' => array(),
				'note' => 'marketing.adwords_tracking_code_tooltip',
			)
		);

		return $formBuilder;
	}

	/**
	 * Get google tools form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
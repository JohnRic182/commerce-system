<?php

	/**
	 * Class Admin_Form_OrderVparcelForm
	 */
	class Admin_Form_OrderVparcelForm
	{
		protected $shipmentRepository = null;
		protected $vparcel_shipping = null;

		protected $settings = null;
		protected $db = null;

		public function __construct(DB $db, DataAccess_SettingsRepository $settings)
		{
			$this->settings = $settings;
			$this->db = $db;
		}

		/**
		 * @param DataAccess_SettingsRepository $settings
		 * @param DataAccess_CountriesStatesRepository $countriesStatesRepository
		 * @param ORDER $order
		 * @return core_Form_FormBuilder
		 */
		public function getBuilder(DataAccess_CountriesStatesRepository $countriesStatesRepository, ORDER $order)
		{
			$settings = $this->settings;
			$this->shipmentRepository =  new DataAccess_OrderShipmentRepository($this->db, $this->settings);
			$shipments = $this->shipmentRepository->getShipmentsByOrderId($order->getId());
			$cur_shipmentId = false;

			// TODO: handle multiple shipments
			foreach ($shipments as $shid => $shdata)
			{
				if ($shdata['shipping_type'] != 'local')
				{
					$cur_shipmentId = $shid;
					$el = $shdata;
				}
			}

			$shRep = new DataAccess_ShippingRepository($this->db);
			$shipments_data = $shRep->getShippingMethodData($el['shipping_ssid']);
			$this->vparcel_shipping = new Shipping_VIPparcel($this->db, $this->settings->getAll(), $shipments_data);
			$formBuilder = new core_Form_FormBuilder('general');

			/**
			 * Basic group
			 */
			$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'Package Properties'));
			$formBuilder->add($basicGroup);

			$result = $shRep->getOrderItems($order->getId());
			if ($result) foreach ($result as $item)
			{
				$shipmentId = $item['shipment_id'];

				if (!$order->isDobaProduct($item))
				{
					if (!$order->isProductLevelShipping($item))
					{
						if (!isset($shippingWeight[$shipmentId])) $shippingWeight[$shipmentId] = 0;
						if (!isset($shippingItemsCount[$shipmentId])) $shippingItemsCount[$shipmentId] = 0;
						if (!isset($shippingAmountForPriceBased[$shipmentId])) $shippingAmountForPriceBased[$shipmentId] = 0;
						
						$shippingWeight[$shipmentId] = $shippingWeight[$shipmentId] + $item['weight'] * $item['admin_quantity'];
						$shippingItemsCount[$shipmentId] = $shippingItemsCount[$shipmentId] + $item['admin_quantity'];
						$shippingAmountForPriceBased[$shipmentId] += $item['admin_price'] * $item['admin_quantity'];
					}
				}
			}
			$orderShippingAddress = $order->getShippingAddress();
			$userData = $order->getUser()->getUserData();

			$shipToCompany 		= $orderShippingAddress['company'];
			$shipToName 		= $orderShippingAddress['name'];
			$shipToType 		= $orderShippingAddress['address_type'];
			$shipToAddress1 	= $orderShippingAddress['address1'];
			$shipToAddress2 	= $orderShippingAddress['address2'];
			$shipToCity 		= $orderShippingAddress['city'];
			$shipToStateName 	= $orderShippingAddress['province'];
			$shipToStateCode 	= $orderShippingAddress['state_abbr'];
			$shipToZip 		= $orderShippingAddress['zip'];
			$shipToCountryName 	= $orderShippingAddress['country_name'];
			$shipToCountryCode 	= $orderShippingAddress['country_iso_a2'];
			$shipToPhone 		= $userData['phone'];
			$shipToEmail 		= $userData['email'];
			$labelType		= $this->vparcel_shipping->getLabelType();
			$WeightOz 		= (isset($shippingWeight[$cur_shipmentId])?$shippingWeight[$cur_shipmentId]: 0);
			$WeightOz 		= Shipping_VIPparcel::weigthInOz($WeightOz);
			$mailClass		= $shipments_data['method_id'];
			
			$service		= $settings->get('ShippingVIPparcelService');

			$shipFromName = $settings->get('CompanyName');
			$shipFromAddress1 = $settings->get('ShippingOriginAddressLine1');
			$shipFromAddress2 = $settings->get('ShippingOriginAddressLine2');
			$shipFromCity = $settings->get('ShippingOriginCity');
			$shipFromState = $settings->get('ShippingOriginState');
			$shipFromProvince = $settings->get('ShippingOriginProvince');
			$shipFromZip = $settings->get('ShippingOriginZip');
			$shipFromCountry = $settings->get('ShippingOriginCountry');
			$shipFromPhone = $settings->get('ShippingOriginPhone');
			$shipFromEmail = $settings->get('CompanyEmail');

			if ($shipFromStateData = $countriesStatesRepository->getStateById($shipFromState))
			{
				$shipFromStateName = $shipFromStateData['name'];
				$shipFromStateCode = $shipFromStateData['short_name'];
			}
			else
			{
				$shipFromStateName = $shipFromStateCode = $shipFromProvince;
			}

			if ($shipFromCountryData = $countriesStatesRepository->getCountryById($shipFromCountry))
			{
				$shipFromCountryName = $shipFromCountryData['name'];
				$shipFromCountryCode = $shipFromCountryData['iso_a2'];
			}

			$basicGroup->add('vparcel_orderid', 'hidden', array('value' => $order->getId()));
			$basicGroup->add('vparcel_ssid', 'hidden', array('value' => $shipments_data['ssid']));
			$basicGroup->add('vparcel_mode', 'hidden', array('value' => ''));
			$basicGroup->add('vparcel_shiptocompany', 'hidden', array('value' => $shipToCompany));
			$basicGroup->add('vparcel_shiptoname', 'hidden', array('value' => $shipToName));
			$basicGroup->add('vparcel_shiptoaddress1', 'hidden', array('value' => $shipToAddress1));
			$basicGroup->add('vparcel_shiptoaddress2', 'hidden', array('value' => $shipToAddress2));
			$basicGroup->add('vparcel_shiptocity', 'hidden', array('value' => $shipToCity));
			$basicGroup->add('vparcel_shiptostate', 'hidden', array('value' => $shipToStateCode));
			$basicGroup->add('vparcel_shiptozip', 'hidden', array('value' => $shipToZip));
			$basicGroup->add('vparcel_shiptocountryname', 'hidden', array('value' => $shipToCountryName));
			$basicGroup->add('vparcel_shiptocountrycode', 'hidden', array('value' => $shipToCountryCode));
			$basicGroup->add('vparcel_shiptophone', 'hidden', array('value' => $shipToPhone));
			$basicGroup->add('vparcel_shipToEmail', 'hidden', array('value' => $shipToEmail));

			$basicGroup->add('vparcel_shipfromname', 'hidden', array('value' => $shipFromName));
			$basicGroup->add('vparcel_shipfromaddress1', 'hidden', array('value' => $shipFromAddress1));
			$basicGroup->add('vparcel_shipfromaddress2', 'hidden', array('value' => $shipFromAddress2));
			$basicGroup->add('vparcel_shipfromcity', 'hidden', array('value' => $shipFromCity));
			$basicGroup->add('vparcel_shipfromstate', 'hidden', array('value' => $shipFromStateCode));
			$basicGroup->add('vparcel_shipfromzip', 'hidden', array('value' => $shipFromZip));
			$basicGroup->add('vparcel_shipfromcountryname', 'hidden', array('value' => $shipFromCountryName));
			$basicGroup->add('vparcel_shipfromcountrycode', 'hidden', array('value' => $shipFromCountryCode));
			$basicGroup->add('vparcel_shipfromphone', 'hidden', array('value' => $shipFromPhone));
			$basicGroup->add('vparcel_shipFromEmail', 'hidden', array('value' => $shipFromEmail));

			if ($labelType == Shipping_VIPparcel::LT_DOMESTIC)
			{
				$options = array('domestic' => 'Domestic');
			}
			else
			{
				$options = array('international' => 'International');
			}
			$basicGroup->add('vparcel_labelType', 'choice', array('label' => 'Label Type',
				'value' => $labelType,
				'options' => $options,
				'required' => true,
			));

			$vipParcelMethods = $shRep->getActiveShippingMethods('vparcel');
			$mailClassOptions = array();
			foreach ($vipParcelMethods as $method)
			{
				$mailClassOptions[$method['method_id']] = $method['method_name'];
			}
			$basicGroup->add('vparcel_mailClass', 'choice', array('label' => 'Mail Class',
				'value' => $mailClass,
				'options' => $mailClassOptions,
				'required' => true,
			));

			$basicGroup->add('vparcel_WeightOz', 'text', array('label' => 'Package weight (ounces)',
				'value' => $WeightOz,
				'required' => true,
			));

			$basicGroup->add('vparcel_description', 'text', array('label' => 'Description',
				'value' => ''
			));

			$basicGroup->add('vparcel_insuredValue', 'text', array('label' => 'Insured value',
				'value' => ''
			));
			$serviceGroup = new core_Form_FormBuilder('service', array('label'=>''));
			$basicGroup->add($serviceGroup);
			$serviceGroup->add('vparcel_service','choice',
				array(
					'label' => 'Special Services requested for the package',
					'value' => $service,
					'options' => array(
						'' => 'Select...',
						'CertifiedMail' => 'Certified Mail',
						'DeliveryConfirmation' => 'Delivery Confirmation',
						'SignatureConfirmation' => 'Signature Confirmation'
					)
				)
			);
			
			$internationalGroup = new core_Form_FormBuilder('advanced', array('label'=>'Customs Declaration'));
			$formBuilder->add($internationalGroup);

			$internationalGroup->add('vparcel_category','choice',
				array(
					'label' => 'Category of the customs items',
					'value' => 'Merchandise',
					'options' => array(
						'Documents' => 'Documents',
						'Gift' => 'Gift',
						'Merchandise' => 'Merchandise',
						'Other' => 'Other',
						'ReturnedGoods' => 'Returned Goods',
						'Sample' => 'Sample',
						'HumanitarianDonation' => 'Humanitarian Donation',
						'DangerousGoods' => 'Dangerous Goods'
					)
				)
			);

			$internationalGroup->add('vparcel_contentsExplanation', 'text', array('label' => 'Explanation of the customs items<br/>(Required if category is Other)',
				'value' => ''
			));

			$internationalGroup->add('vparcel_eelPfc', 'text', array('label' => 'Exemption or Exclusion Legend(EEL) or a Proof of Filing Citation(PFC)',
				'value' => '', 'required' => true
			));
			$lineItems = array();
			global $msg;
			foreach ($order->lineItems as $orderLineItem)
			{
				$recurringProfile = null;
				$lineView = new Admin_View_OrderLineItemViewModel($orderLineItem, $recurringProfile, $msg);
				$lineItems[] = $lineView;
			}

			if ($lineItems)
			{
				$itemsGroup = new core_Form_FormBuilder('advanced', array('label'=>'Items for shipping'));
				$internationalGroup->add($itemsGroup);
				foreach ($lineItems as $line)
				{
					$itemsGroup->add('vparcel_shpitemid['.$line->id.']', 'checkbox', array('label' => '',
						'value'=>'1',
						'current_value' => '0',
						'attr' => array('class' => 'shippingtems'),
					));

					$itemsGroup->add('vparcel_shpitemn['.$line->id.']', 'static', array('label' => $line->title,
						'value' =>""
					));
				}
			}

			return $formBuilder;
		}

		/**
		 * @param DataAccess_SettingsRepository $settings
		 * @param DataAccess_CountriesStatesRepository $countriesStatesRepository
		 * @param ORDER $order
		 * @return core_Form_Form
		 */
		public function getForm(DataAccess_CountriesStatesRepository $countriesStatesRepository,  ORDER $order)
		{
			return $this->getBuilder($countriesStatesRepository, $order)->getForm();
		}
	}
<?php

/**
 * Class Admin_Form_CategoryForm
 */
class Admin_Form_CategoryForm
{
	/**
	 * Get category form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $params
	 * @param null $id
	 * @param null $avalaraTaxCodes
	 * @param null $exactorTaxCodes
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $params, $id = null, $avalaraTaxCodes = null, $exactorTaxCodes = null)
	{
		$categoryOptions = isset($params['categoryOptions']) ? $params['categoryOptions'] : array();

		$formBuilder = new core_Form_FormBuilder('general');

		$categoryGroup = new core_Form_FormBuilder('basic', array('label' => 'categories.category_details'));
		$formBuilder->add($categoryGroup);

		$categoryGroup->add('mode', 'hidden', array('value' => $mode));
		if ($mode == 'update' && !is_null($id)) $categoryGroup->add('id', 'hidden', array('value' => $id));

		$attr = array('maxlength' => '255', 'class' => 'generated-id-field');
		if ($mode == 'add') $attr['class'] =  'generated-id-field autogenerate';

		$categoryGroup->add('name', 'text', array('label' => 'categories.title', 'value' => $formData['name'],'placeholder' => 'categories.title', 'required' => true, 'attr' => array('maxlength' => '255', 'class' => 'generate-id-source-field')));
		$categoryGroup->add(
			'key_name', 'text', array('label' => 'categories.category_id', 'value' => $formData['key_name'], 'placeholder' => 'categories.category_id', 'required' => true, 'attr' => $attr,
			'note' => 'categories.notes.category_id')
		);

		$categoryGroup->add('parent', 'choice', array('label' => 'categories.parent_category', 'value' => $formData['parent'], 'options' => $categoryOptions, 'empty_value' => array('0' => 'Top Level')));

		if ($formData['is_doba'])
		{
			$categoryGroup->add(
				'doba_mapped_category_id', 'choice',
				array('label' => 'categories.doba_mapped_category', 'value' => $formData['doba_mapped_category_id'], 'options' => $categoryOptions, 'empty_value' => array('0' => 'None'),
				'note' => 'categories.doba_mapped_category_hint')
			);
		}

		$categoryGroup->add('description', 'textarea', array('label' => 'categories.description', 'value' => $formData['description'], 'wysiwyg' => true));
		$categoryGroup->add('is_visible', 'checkbox', array('label' => 'categories.is_visible', 'value'=>'Yes', 'current_value' => $formData['is_visible']));
		$categoryGroup->add('is_stealth', 'checkbox', array('label' => 'categories.is_stealth', 'value'=>'1', 'current_value' => $formData['is_stealth'], 'note' => 'categories.notes.stealth'));
		$categoryGroup->add('list_subcats', 'checkbox', array('label' => 'categories.list_subcats', 'value'=>'Yes', 'current_value' => $formData['list_subcats']));
		$categoryGroup->add('list_subcats_images', 'checkbox', array('label' => 'categories.list_subcats_images', 'value'=>'1', 'current_value' => $formData['list_subcats_images']));
		$categoryGroup->add('priority', 'text', array(
			'label' => 'categories.priority',
			'value' => $formData['priority'],
			'attr' => array('maxlength' => 200),
			'note' => 'categories.notes.priority',
		));
		$categoryGroup->add(
			'image', 'image',
			array(
				'label' => 'categories.image', 'value' => (isset($formData['thumb']) && !empty($formData['thumb']) ? $formData['thumb'] : $formData['image']),
				'imageDeleteUrl' => $mode == 'update' && !is_null($id) ? 'admin.php?p=category&mode=delete-image&id='.$id : null,
				'wrapperClass' => 'clear-both',
				'attr' => array(
					'accept' => 'image/jpeg, image/png, image/gif',
				)
			)
		);

		$seoGroup = new core_Form_FormBuilder('seo', array('label' => 'common.search_engine_optimization', 'collapsible' => true));
		$formBuilder->add($seoGroup);

		$seoGroup->add('meta_title', 'text', array('label' => 'common.meta_title', 'value' => $formData['meta_title'], 'attr' => array('class' => 'seo-preview' , 'maxlength' => 69)));

		$seoGroup->add('url_custom', 'text', array(
			'label' => 'categories.category_url',
			'value' => $formData['url_custom'],
			'required' => false,
			'attr' => array('class' => 'seo-preview'),
			'placeholder' => $formData['url_default'],
			'note' => 'categories.notes.url',
		));

		$seoGroup->add('meta_description', 'textarea', array('label' => 'common.meta_description', 'value' => $formData['meta_description'], 'attr' => array('class' => 'seo-preview', 'maxlength' => 156 )));

		$translator = Framework_Translator::getInstance();

		//  Our search results preview
		$seoGroup->add('category_seo_preview', 'seopreview', array(
			'label' => 'SEO Search Results Preview',
			'seoOptions' => array(
				'url' => trim($formData['url_custom']) == '' ? $formData['url_default'] : $formData['url_custom'],
				'isUrlHidden' => 'showing',
				'title' => trim($formData['meta_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['meta_title'],
				'description' => trim($formData['meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['meta_description'],
				'meta_title' => 'meta_title',
				'meta_description' => 'meta_description',
				'meta_url' => 'url_custom',
			)
		));

		if ($mode == 'update' && _ACCESS_PRODUCTS_FILTERS && _CAN_ENABLE_PRODUCTS_FILTERS)
		{
			/**
			 * Category Product Feature Group
			 */
			$productFeatures = new core_Form_FormBuilder('product_features', array('label' => 'Product Features Groups', 'collapsible' => true));
			$productFeatures->add(
				'products-features-groups',
				'template',
				array(
					'file' => 'templates/pages/category/edit-products-features-groups.html',
					'value' => json_encode(isset($params['categoryProductsFeaturesGroups']) ? $params['categoryProductsFeaturesGroups'] : array())
				)
			);
			$formBuilder->add($productFeatures);
		}

		/**
		 * Tax settings
		 */
		if (($avalaraTaxCodes !== null  && count($avalaraTaxCodes) > 0) || ($exactorTaxCodes !== null && count($exactorTaxCodes) > 0))
		{

			$taxGroup = new core_Form_FormBuilder('tax', array('label' => 'categories.tax_settings', 'collapsible' => true));
			$formBuilder->add($taxGroup);

			if ($avalaraTaxCodes !== null)
			{
				$avalaraTaxCodesOptions = array('0' => 'common.none');
				foreach ($avalaraTaxCodes as $key => $value)
				{
					$avalaraTaxCodesOptions[$key] = $value;
				}

				$taxGroup->add('avalara_tax_code', 'choice', array('label' => 'categories.avalara_tax_code', 'value' => $formData['avalara_tax_code'], 'options' => $avalaraTaxCodesOptions));
			}

			if ($exactorTaxCodes !== null)
			{
				$exactorTaxCodesOptions = array('0' => 'common.none');

				foreach ($exactorTaxCodes as $key => $value)
				{
					$exactorTaxCodesOptions[$key] = $value;
				}

				$taxGroup->add('exactor_euc_code', 'choice', array('label' => 'categories.exactor_tax_code', 'value' => $formData['exactor_euc_code'], 'options' => $exactorTaxCodesOptions));
			}
		}

		return $formBuilder;
	}

	/**
	 * Get category form
	 *
	 * @param $formData
	 * @param $mode
	 * @param $categoryOptions
	 * @param null $id
	 * @param null $avalaraTaxCodes
	 * @param null $exactorTaxCodes
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $categoryOptions, $id = null, $avalaraTaxCodes = null, $exactorTaxCodes = null)
	{
		return $this->getBuilder($formData, $mode, $categoryOptions, $id, $avalaraTaxCodes, $exactorTaxCodes)->getForm();
	}
}

<?php

class Admin_Form_PikflySettingsForm
{
	public function getBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('settings');

		$settingsGroup = new core_Form_FormBuilder('settings', array('label' => 'Settings'));
		$formBuilder->add($settingsGroup);

		$timezoneOptions = array(
			'eastern' => 'pikfly.timezones.eastern',
			'central' => 'pikfly.timezones.central',
			'mountain' => 'pikfly.timezones.mountain',
			'pacific' => 'pikfly.timezones.pacific',
			'alaskan' => 'pikfly.timezones.alaskan',
			'hawaiian' => 'pikfly.timezones.hawaiian',
		);
		$settingsGroup->add('PikflyTimezone', 'choice', array('label' => 'pikfly.local_timezone', 'value' => $formData['PikflyTimezone'], 'options' => $timezoneOptions));
		$settingsGroup->add('PikflyShowAlternativeMethods', 'choice', array('label' => 'pikfly.show_alternate_methods', 'value' => $formData['PikflyShowAlternativeMethods'], 'options' => array(
			'No' => 'common.no',
			'Yes' => 'common.yes',
		), 'note' => 'pikfly.show_alternate_methods_note'));

		return $formBuilder->getForm();
	}
}
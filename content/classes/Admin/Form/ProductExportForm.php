<?php

/**
 * Class Admin_Form_ProductExportForm
 */
class Admin_Form_ProductExportForm
{
	/** @var DataAccess_SettingsRepository  */
	protected $settings;

	/**
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
	}

	/**
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder()
	{
		$settings = $this->settings;

		$exportFormBuilder = new core_Form_FormBuilder('export');
		$exportFormBuilder->add('exportFields[is_visible]', 'checkbox', array('label' => 'Availability', 'value' => 'is_visible'));
		$exportFormBuilder->add('exportFields[product_type]', 'checkbox', array('label' => 'Product type', 'value' => 'product_type'));
		$exportFormBuilder->add('exportFields[category]', 'checkbox', array('label' => 'Categories', 'value' => 'category'));
		$exportFormBuilder->add('exportFields[manufacturer]', 'checkbox', array('label' => 'Manufacturer', 'value' => 'manufacturer'));
		$exportFormBuilder->add('exportFields[tax]', 'checkbox', array('label' => 'Tax settings', 'value' => 'tax'));
		$exportFormBuilder->add('exportFields[location]', 'checkbox', array('label' => 'Location', 'value' => 'location'));
		$exportFormBuilder->add('exportFields[shipping]', 'checkbox', array('label' => 'Shipping settings', 'value' => 'shipping'));
		$exportFormBuilder->add('exportFields[order]', 'checkbox', array('label' => 'Min/max order', 'value' => 'order'));
		$exportFormBuilder->add('exportFields[sale_price]', 'checkbox', array('label' => 'Sale price', 'value' => 'sale_price'));

		if ($settings->get('WholesaleDiscountType') == 'PRODUCT')
		{
			$exportFormBuilder->add('exportFields[wholesale_prices]', 'checkbox', array('label' => 'Wholesale prices', 'value' => 'wholesale_prices'));
		}

		$exportFormBuilder->add('exportFields[call_for_price]', 'checkbox', array('label' => 'Call for price', 'value' => 'call_for_price'));

		$exportFormBuilder->add('exportFields[priority]', 'checkbox', array('label' => 'Priority', 'value' => 'priority'));
		$exportFormBuilder->add('exportFields[rating]', 'checkbox', array('label' => 'Rating', 'value' => 'rating'));
		$exportFormBuilder->add('exportFields[url]', 'checkbox', array('label' => 'URL', 'value' => 'url'));
		$exportFormBuilder->add('exportFields[seo]', 'checkbox', array('label' => 'SEO', 'value' => 'seo'));

		if (AccessManager::checkAccess('INVENTORY'))
		{
			$exportFormBuilder->add('exportFields[inventory]', 'checkbox', array('label' => 'Inventory settings', 'value' => 'inventory'));
		}

		$exportFormBuilder->add('exportFields[digital_product_file]', 'checkbox', array('label' => 'Digital product file', 'value' => 'digital_product_file'));

		$exportFormBuilder->add('exportFields[search_keywords]', 'checkbox', array('label' => 'Search keywords', 'value' => 'search_keywords'));
		$exportFormBuilder->add('exportFields[overview]', 'checkbox', array('label' => 'Overview', 'value' => 'overview'));
		$exportFormBuilder->add('exportFields[description]', 'checkbox', array('label' => 'Description', 'value' => 'description'));
		$exportFormBuilder->add('exportFields[image]', 'checkbox', array('label' => 'Image', 'value' => 'image'));

		$exportFormBuilder->add('exportFields[recurring]', 'checkbox', array('label' => 'Recurring billing properties', 'value' => 'recurring'));
		$exportFormBuilder->add('exportFields[attributes]', 'checkbox', array('label' => 'Attributes', 'value' => 'attributes'));
		$exportFormBuilder->add('exportFields[variants]', 'checkbox', array('label' => 'Variants', 'value' => 'variants'));

		$exportFormBuilder->add('exportFields[marketing]', 'checkbox', array('label' => 'Marketing properties', 'value' => 'marketing'));

		$exportFormBuilder->add('exportFields[product_codes]', 'checkbox', array('label' => 'Product SKU / Barcode', 'value' => 'product_codes'));
		$exportFormBuilder->add('exportFields[google]', 'checkbox', array('label' => 'Google base', 'value' => 'google'));
		$exportFormBuilder->add('exportFields[amazon]', 'checkbox', array('label' => 'Amazon', 'value' => 'amazon'));
		$exportFormBuilder->add('exportFields[ebay]', 'checkbox', array('label' => 'Ebay', 'value' => 'ebay'));
		$exportFormBuilder->add('exportFields[yahoo]', 'checkbox', array('label' => 'Yahoo', 'value' => 'yahoo'));
		$exportFormBuilder->add('exportFields[price_grabber]', 'checkbox', array('label' => 'PriceGrabber', 'value' => 'price_grabber'));
		$exportFormBuilder->add('exportFields[nextag]', 'checkbox', array('label' => 'Nextag', 'value' => 'nextag'));
		$exportFormBuilder->add('exportFields[pikfly]', 'checkbox', array('label' => 'Pikfly', 'value' => 'pikfly'));
		$exportFormBuilder->add('exportFields[added]', 'checkbox', array('label' => 'Date', 'value' => 'added'));
		$exportFormBuilder->add('exportFields[quantity_discounts]', 'checkbox', array('label' => 'Quantity Discounts', 'value' => 'quantity_discounts'));
		$exportFormBuilder->add('exportFields[youtube]', 'checkbox', array('label' => 'YouTube', 'value' => 'youtube'));
		$exportFormBuilder->add('exportFields[product_level_shippings]', 'checkbox', array('label' => 'Product Level Shipping', 'value' => 'product_level_shippings'));

		return $exportFormBuilder;
	}

	/**
	 * @return mixed
	 */
	public function getForm()
	{
		return $this->getBuilder()->getForm();
	}
}
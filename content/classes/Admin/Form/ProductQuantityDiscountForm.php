<?php

/**
 * Class Admin_Form_ProductQuantityDiscountForm
 */
class Admin_Form_ProductQuantityDiscountForm
{
	/**
	 * Get form builder
	 *
	 * @param int $productId
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getFormBuilder($productId)
	{
		$formBuilder = new core_Form_FormBuilder('product-quantity-discount');

		$formBuilder->add('qd[id]', 'hidden', array('value' => 'new'));
		$formBuilder->add('qd[pid]', 'hidden', array('value' => $productId));
		$formBuilder->add('qd[range_min]', 'integer', array('label' => 'marketing.min_range', 'value' => '', 'required' => true, 'placeholder' => 'marketing.min_range'));
		$formBuilder->add('qd[range_max]', 'integer', array('label' => 'marketing.max_range', 'value' => '', 'required' => true, 'placeholder' => 'marketing.max_range'));
		$formBuilder->add('qd[discount]', 'numeric', array('label' => 'marketing.discount', 'value' => '', 'required' => true, 'placeholder' => 'marketing.discount'));

		$formBuilder->add(
			'qd[discount_type]', 'choice',
			array('label' => 'marketing.discount_type', 'options' => array(new core_Form_Option('percent', '%'), new core_Form_Option('amount', 'Amount')))
		);

		$formBuilder->add('qd[apply_to_wholesale]', 'checkbox', array('label' => 'marketing.apply_to_wholesale', 'value' => 'Yes', 'current_value' => 'No'));
		$formBuilder->add('qd[free_shipping]', 'checkbox', array('label' => 'marketing.free_shipping', 'value' => 'Yes', 'current_value' => 'No'));
		$formBuilder->add('qd[is_active]', 'checkbox', array('label' => 'marketing.is_active', 'value' => 'Yes', 'current_value' => 'No'));

		return $formBuilder;
	}

	/**
	 * Get form
	 *
	 * @param int $productId
	 *
	 * @return core_Form_Form
	 */
	public function getForm($productId)
	{
		return $this->getFormBuilder($productId)->getForm();
	}
}
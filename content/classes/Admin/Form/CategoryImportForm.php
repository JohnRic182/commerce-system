<?php
/**
 * Class Admin_Form_CategoryImportForm
 */
class Admin_Form_CategoryImportForm
{
	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getStartFormBuilder($formData)
	{
		global $label_app_name;

		$formBuilder = new core_Form_FormBuilder('general');

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label' => 'categories.import_step1'));

		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('categories.import_step1_description', array('label_app_name' => $label_app_name)))
		);

		$basicGroup->add(
			'fields_separator', 'choice',
			array('label' => 'categories.import_delimiter', 'value' => ',', 'options' => array(
				',' => 'categories.import_delimiter_comma',
				';' => 'categories.import_delimiter_semicolon',
			))
		);

		$basicGroup->add(
			'make_categories_visible', 'choice',
			array(
				'label' => 'categories.import_make_categories_visible', 'value' => ',', 'options' => array(
					'Yes' => 'categories.import_make_categories_visible_yes',
					'No' => 'categories.import_make_categories_visible_no'
			),
				'note' => 'categories.notes.import_make_categories_visible'
			)
		);

		$basicGroup->add(
			'make_categories_visible_type', 'choice',
			array(
				'label' => 'categories.import_make_categories_visible_type', 'value' => ',', 'options' => array(
					'inserted' => 'categories.import_make_categories_visible_type_inserted',
					'updated' => 'categories.import_make_categories_visible_type_updated'
				),
				'note' => 'categories.notes.import_make_categories_visible_type'
			)
		);

		$basicGroup->add(
			'bulk', 'file',
			array('label' => 'common.import_choose_csv_file', 'required' => true, 'wrapperClass' => 'col-sm-3 col-xs-12')
		);

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getAssignFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label' => 'categories.import_step2'));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'description1', 'description',
			array('value' => trans('categories.import_step2_description'))
		);

		$basicGroup->add('skip_first_line', 'checkbox', array('label' => 'common.import_skip_first_line', 'value'=>'1', 'current_value' => isset($formData['skip_first_line']) && $formData['skip_first_line'] == '1' ? '1' : '0'));
		$basicGroup->add('linesCount', 'static', array('label' => 'common.import_total_lines', 'value' => $formData['totalLinesCount']));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return core_Form_FormBuilder
	 */
	public function getImportFormBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label' => 'categories.import_step3'));
		$formBuilder->add($basicGroup);

		$basicGroup->add('description1', 'description', array('value' => trans('categories.import_step3_description')));
		$basicGroup->add('addedCount', 'static', array('label' => 'categories.import_new_records', 'value' => $formData['addedCount']));
		$basicGroup->add('updatedCount', 'static', array('label' => 'categories.import_updated_records', 'value' => $formData['updatedCount']));

		return $formBuilder;
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getStartForm($formData)
	{
		return $this->getStartFormBuilder($formData)->getForm();
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getAssignForm($formData)
	{
		return $this->getAssignFormBuilder($formData)->getForm();
	}

	/**
	 * @param $formData
	 * @return mixed
	 */
	public function getImportForm($formData)
	{
		return $this->getImportFormBuilder($formData)->getForm();
	}
}
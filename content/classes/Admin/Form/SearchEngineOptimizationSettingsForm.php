<?php

/**
 * Class Admin_Form_SearchEngineOptimizationSettingsForm
 */
class Admin_Form_SearchEngineOptimizationSettingsForm
{
	/**
	 * Get settings form for search optimization - builder
	 *
	 * @param $formData
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'SEO Settings'));
		$formBuilder->add($basicGroup);

		$basicGroup->add(
			'searchSettings[SearchTitle]', 'text',
			array(
				'label' => 'marketing.seo.default_site_title',
				'value' => $formData['SearchTitle'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['SearchTitle'])
			)
		);

		$basicGroup->add(
			'searchSettings[SearchMetaDescription]', 'textarea',
			array(
				'label' => 'marketing.seo.default_meta_description',
				'value' => $formData['SearchMetaDescription'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['SearchMetaDescription'])
			)
		);

		if (AccessManager::checkAccess('FLAT_URL'))
		{
			$basicGroup->add('searchSettings[USE_MOD_REWRITE]', 'checkbox', array(
				'label' => 'marketing.seo.enable_seo_friendly_urls',
				'value' => 'YES',
				'current_value' => $formData['USE_MOD_REWRITE'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['USE_MOD_REWRITE'])
			));
		}
		else
		{
			$basicGroup->add(
				'modrewrite', 'static',
				array(
					'label' => 'marketing.seo.enable_flat_urls_generation',
					'value' => '<strong>' . trans('marketing.seo.enable_flat_urls_generation_tooltip') . '</strong>'
				)
			);
		}

		$basicGroup->add(
			'searchSettings[SearchAutoGenerateLowercase]', 'checkbox',
			array(
				'label' => 'marketing.seo.use_lowercase_letters_in_urls',
				'value' => 'YES',
				'current_value' => $formData['SearchAutoGenerateLowercase'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['SearchAutoGenerateLowercase'])
			)
		);


		$basicGroup->add(
			'searchSettings[SearchAutoGenerate]', 'checkbox',
			array(
				'label' => 'marketing.seo.enable_meta_title_and_description_auto_generation',
				'value' => 'YES',
				'current_value' => $formData['SearchAutoGenerate'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['SearchAutoGenerate'])
			)
		);

		$basicGroup->add(
			'searchSettings[seo_www_preference]', 'choice',
			array(
				'label' => 'marketing.seo.seo_www_preference',
				'value' => $formData['seo_www_preference'],
				'wrapperClass' => 'clear-both',
				'options' => array('default' => 'do not modify URL`s', 'www' => 'force "www" URL`s', 'nonwww' => 'force non-"www" URL`s'),
				'note' => 'marketing.seo.www_nowww'
			)
		);

		$basicGroup->add(
			'searchSettings[SearchURLCategoryTemplate]', 'text',
			array(
				'label' => 'marketing.seo.category_url_template',
				'value' => $formData['SearchURLCategoryTemplate'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['SearchURLCategoryTemplate']),
				'note' => 'marketing.seo.category_url_template_note'
			)
		);

		$basicGroup->add(
			'searchSettings[SearchURLCategoryPaginatorTemplate]', 'text',
			array(
				'label' => 'marketing.seo.category_pagination',
				'value' => $formData['SearchURLCategoryPaginatorTemplate'],
				'attr' => array('data-value' => $formData['SearchURLCategoryPaginatorTemplate']),
				'note' => 'marketing.seo.category_pagination_note'
			)
		);

		$basicGroup->add(
			'searchSettings[SearchURLManufacturerTemplate]', 'text',
			array(
				'label' => 'marketing.seo.manufacturer_url_template',
				'value' => $formData['SearchURLManufacturerTemplate'],
				'attr' => array('data-value' => $formData['SearchURLManufacturerTemplate']),
				'note' => 'marketing.seo.manufacturer_url_template_note'
			)
		);

		$basicGroup->add(
			'searchSettings[SearchURLManufacturerPaginatorTemplate]', 'text',
			array(
				'label' => 'marketing.seo.manufacturer_pagination',
				'value' => $formData['SearchURLManufacturerPaginatorTemplate'],
				'attr' => array('data-value' => $formData['SearchURLManufacturerPaginatorTemplate']),
				'note' => 'marketing.seo.manufacturer_pagination_note'
			)
		);

		$basicGroup->add(
			'searchSettings[SearchURLProductTemplate]', 'text',
			array(
				'label' => 'marketing.seo.product_url_template',
				'value' => $formData['SearchURLProductTemplate'],
				'attr' => array('data-value' => $formData['SearchURLProductTemplate']),
				'note' => 'marketing.seo.product_url_template_note'
			)
		);

		$basicGroup->add(
			'searchSettings[SearchURLPageTemplate]', 'text',
			array(
				'label' => 'marketing.seo.page_url_template',
				'value' => $formData['SearchURLPageTemplate'],
				'attr' => array('data-value' => $formData['SearchURLPageTemplate']),
				'note' => 'marketing.seo.page_url_template_note',
			)
		);

		$basicGroup->add(
				'searchSettings[SearchURLOrderFormTemplate]', 'text',
				array(
						'label' => 'marketing.seo.order_form_url_template',
						'value' => $formData['SearchURLOrderFormTemplate'],
						'attr' => array('data-value' => $formData['SearchURLOrderFormTemplate']),
						'note' => 'marketing.seo.order_form_url_template_note',
				)
		);

		$basicGroup->add(
			'searchSettings[SearchHtaccessOverrides]', 'textarea',
			array(
				'label' => '.htaccess ' . trans('marketing.seo.overrides'),
				'value' => $formData['SearchHtaccessOverrides'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['SearchHtaccessOverrides'])
			)
		);

		$basicGroup->add(
			'searchSettings[robots]', 'textarea',
			array(
				'label' => 'robots.txt',
				'value' => $formData['robots'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['robots'])
			)
		);

		/**
		 * Category page
		 */
		$categoryGroup = new core_Form_FormBuilder('category', array('label'=>'marketing.seo.category_page','collapsible' => true));
		$formBuilder->add($categoryGroup);

		$categoryGroup->add(
			'searchSettings[search_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['search_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['search_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$categoryGroup->add(
			'searchSettings[search_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['search_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['search_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		$translator = Framework_Translator::getInstance();

		//  Our search results preview
		$categoryGroup->add('search_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['search_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['search_page_title'],
					'description' => trim($formData['search_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['search_meta_description'],
					'meta_title' => 'searchSettings-search_page_title',
					'meta_description' => 'searchSettings-search_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Testimonials page
		 */
		$testimonialGroup = new core_Form_FormBuilder('testimonial', array('label'=>'marketing.seo.testimonials_page','collapsible' => true));
		$formBuilder->add($testimonialGroup);

		$testimonialGroup->add(
			'searchSettings[testimonials_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['testimonials_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['testimonials_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$testimonialGroup->add(
			'searchSettings[testimonials_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['testimonials_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['testimonials_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$testimonialGroup->add('testimonials_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['testimonials_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['testimonials_page_title'],
					'description' => trim($formData['testimonials_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['testimonials_meta_description'],
					'meta_title' => 'searchSettings-testimonials_page_title',
					'meta_description' => 'searchSettings-testimonials_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Login Page
		 */
		$loginGroup = new core_Form_FormBuilder('login', array('label'=>'marketing.seo.login_page','collapsible' => true));
		$formBuilder->add($loginGroup);

		$loginGroup->add(
			'searchSettings[login_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['login_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['login_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$loginGroup->add(
			'searchSettings[login_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['login_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['login_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$loginGroup->add('login_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['login_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['login_page_title'],
					'description' => trim($formData['login_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['login_meta_description'],
					'meta_title' => 'searchSettings-login_page_title',
					'meta_description' => 'searchSettings-login_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Register Page
		 */
		$registerGroup = new core_Form_FormBuilder('register', array('label'=>'marketing.seo.register_page','collapsible' => true));
		$formBuilder->add($registerGroup);

		$registerGroup->add(
			'searchSettings[register_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['register_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['register_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$registerGroup->add(
			'searchSettings[register_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['register_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['register_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$registerGroup->add('register_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['register_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['register_page_title'],
					'description' => trim($formData['register_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['register_meta_description'],
					'meta_title' => 'searchSettings-register_page_title',
					'meta_description' => 'searchSettings-register_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Sitemap page
		 */
		$siteMapGroup = new core_Form_FormBuilder('site_map', array('label'=>'marketing.seo.sitemap_page','collapsible' => true));
		$formBuilder->add($siteMapGroup);

		$siteMapGroup->add(
			'searchSettings[sitemap_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['sitemap_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['sitemap_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$siteMapGroup->add(
			'searchSettings[sitemap_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['sitemap_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['sitemap_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$siteMapGroup->add('register_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['sitemap_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['sitemap_page_title'],
					'description' => trim($formData['sitemap_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['sitemap_meta_description'],
					'meta_title' => 'searchSettings-sitemap_page_title',
					'meta_description' => 'searchSettings-sitemap_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Not Found Page
		 */
		$notFoundGroup = new core_Form_FormBuilder('not_found', array('label'=>'marketing.seo.not_found_page','collapsible' => true));
		$formBuilder->add($notFoundGroup);

		$notFoundGroup->add(
			'searchSettings[notfound_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['notfound_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['notfound_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$notFoundGroup->add(
			'searchSettings[notfound_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['notfound_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['notfound_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$notFoundGroup->add('notfound_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['notfound_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['notfound_page_title'],
					'description' => trim($formData['notfound_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['notfound_meta_description'],
					'meta_title' => 'searchSettings-notfound_page_title',
					'meta_description' => 'searchSettings-notfound_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Meta Description
		 */
		$cartGroup = new core_Form_FormBuilder('cart', array('label'=>'marketing.seo.cart_page','collapsible' => true));
		$formBuilder->add($cartGroup);

		$cartGroup->add(
			'searchSettings[cart_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['cart_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['cart_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$cartGroup->add(
			'searchSettings[cart_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['cart_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['cart_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$cartGroup->add('cart_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['cart_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['cart_page_title'],
					'description' => trim($formData['cart_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['cart_meta_description'],
					'meta_title' => 'searchSettings-cart_page_title',
					'meta_description' => 'searchSettings-cart_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Subscribe Page
		 */
		$subscribeGroup = new core_Form_FormBuilder('subscribe', array('label'=>'marketing.seo.subscribe_page','collapsible' => true));
		$formBuilder->add($subscribeGroup);

		$subscribeGroup->add(
			'searchSettings[subscribe_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['subscribe_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['subscribe_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$subscribeGroup->add(
			'searchSettings[subscribe_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['subscribe_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['subscribe_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$subscribeGroup->add('subscribe_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['subscribe_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['subscribe_page_title'],
					'description' => trim($formData['subscribe_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['subscribe_meta_description'],
					'meta_title' => 'searchSettings-subscribe_page_title',
					'meta_description' => 'searchSettings-subscribe_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Unsubscribe Page
		 */
		$unsubscribeGroup = new core_Form_FormBuilder('unsubscribe', array('label'=>'marketing.seo.unsubscribe_page','collapsible' => true));
		$formBuilder->add($unsubscribeGroup);
		
		$unsubscribeGroup->add(
			'searchSettings[unsubscribe_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['unsubscribe_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['unsubscribe_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$unsubscribeGroup->add(
			'searchSettings[unsubscribe_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['unsubscribe_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['unsubscribe_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$unsubscribeGroup->add('unsubscribe_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['unsubscribe_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['unsubscribe_page_title'],
					'description' => trim($formData['unsubscribe_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['unsubscribe_meta_description'],
					'meta_title' => 'searchSettings-unsubscribe_page_title',
					'meta_description' => 'searchSettings-unsubscribe_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Password Reset Page
		 */
		$passwordResetGroup = new core_Form_FormBuilder('password_reset', array('label'=>'marketing.seo.password_reset_page','collapsible' => true));
		$formBuilder->add($passwordResetGroup);
		
		$passwordResetGroup->add('searchSettings[reset_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['reset_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['reset_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$passwordResetGroup->add('searchSettings[reset_meta_description]', 'textarea', array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['reset_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['reset_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
		));

		//  Our search results preview
		$passwordResetGroup->add('unsubscribe_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['reset_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['reset_page_title'],
					'description' => trim($formData['reset_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['reset_meta_description'],
					'meta_title' => 'searchSettings-reset_page_title',
					'meta_description' => 'searchSettings-reset_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * International Page
		 */
		$internationalGroup = new core_Form_FormBuilder('international', array('label'=>'marketing.seo.international_page','collapsible' => true));
		$formBuilder->add($internationalGroup);

		$internationalGroup->add(
			'searchSettings[international_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['international_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['international_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$internationalGroup->add('searchSettings[international_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['international_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['international_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$internationalGroup->add('unsubscribe_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['international_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['international_page_title'],
					'description' => trim($formData['international_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['international_meta_description'],
					'meta_title' => 'searchSettings-international_page_title',
					'meta_description' => 'searchSettings-international_meta_description',
					'meta_url' => '',
				)
			)
		);

		/**
		 * Gift Certificate Page
		 */
		$giftCertificateGroup = new core_Form_FormBuilder('gift_certificate', array('label'=>'marketing.seo.gift_certificate_page','collapsible' => true));
		$formBuilder->add($giftCertificateGroup);

		$giftCertificateGroup->add(
			'searchSettings[giftcert_page_title]', 'text',
			array(
				'label' => 'marketing.seo.page_title',
				'value' => $formData['giftcert_page_title'],
				'wrapperClass' => 'col-xs-12 col-md-6 clear-both',
				'attr' => array('data-value' => $formData['giftcert_page_title'], 'class' => 'seo-preview' , 'maxlength' => 69)
			)
		);

		$giftCertificateGroup->add(
			'searchSettings[giftcert_meta_description]', 'textarea',
			array(
				'label' => 'marketing.seo.meta_description',
				'value' => $formData['giftcert_meta_description'],
				'wrapperClass' => 'clear-both',
				'attr' => array('data-value' => $formData['giftcert_meta_description'], 'class' => 'seo-preview' , 'maxlength' => 156)
			)
		);

		//  Our search results preview
		$giftCertificateGroup->add('unsubscribe_seo_preview', 'seopreview', array(
				'label' => 'SEO Search Results Preview',
				'seoOptions' => array(
					'url' => '',
					'isUrlHidden' => 'hidden',
					'title' => trim($formData['giftcert_page_title']) == '' ? $translator->trans('common.add_meta_title') : $formData['giftcert_page_title'],
					'description' => trim($formData['giftcert_meta_description']) == '' ? $translator->trans('common.add_meta_description') : $formData['giftcert_meta_description'],
					'meta_title' => 'searchSettings-giftcert_page_title',
					'meta_description' => 'searchSettings-giftcert_meta_description',
					'meta_url' => '',
				)
			)
		);

		return $formBuilder;
	}

	/**
	 * Get search engine optimization settings form
	 *
	 * @param $formData
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData)
	{
		return $this->getBuilder($formData)->getForm();
	}
}
<?php
/**
 * Class Admin_Form_CustomForm
 */
class Admin_Form_CustomForm
{
	/**
	 * Get custom form builder
	 *
	 * @param $formData
	 * @param $mode
	 * @param $id
	 *
	 * @return core_Form_FormBuilder
	 */
	public function getBuilder($formData, $mode, $params, $id = null)
	{
		$formBuilder = new core_Form_FormBuilder('general');

		$formFields = isset($params['formFields']) ? $params['formFields'] : array();

		/**
		 * Basic group
		 */
		$basicGroup = new core_Form_FormBuilder('basic', array('label'=>'custom_form.form_properties'));
		$formBuilder->add($basicGroup);
		$basicGroup->add('title', 'text', array('label' => 'custom_form.title', 'placeholder' => 'custom_form.title', 'value' => $formData['title'], 'required' => true, 'attr' => array('maxlength' => '250', 'class' => 'generate-id-source-field')));

		$attr = array('maxlength' => '255', 'class' => 'generated-id-field');
		if ($mode == 'add') $attr['class'] =  'generated-id-field autogenerate';

		$basicGroup->add(
			'form_id', 'text',
			array(
				'label' => 'custom_form.form_id', 'value' => $formData['form_id'], 'required' => true, 'attr' => $attr,
				'note' => 'custom_form.form_id_tooltip', 'placeholder' => 'custom_form.form_id'
			)
		);

		$basicGroup->add('is_active', 'checkbox', array('label' => 'custom_form.is_active', 'value' => '1', 'current_value' => $formData['is_active'], 'wrapperClass' => 'field-checkbox-space'));
		$basicGroup->add('is_recaptcha', 'checkbox', array('label' => 'custom_form.is_recaptcha', 'value' => '1', 'current_value' => $formData['is_recaptcha'], 'note' => 'custom_form.is_recaptcha_note', 'wrapperClass' => 'field-checkbox-space'));
		$basicGroup->add('is_title_visible', 'checkbox', array('label' => 'custom_form.is_title_visible', 'value' => '1', 'current_value' => $formData['is_title_visible'], 'wrapperClass' => 'clear-both field-checkbox-space'));
		//$basicGroup->add('save_data', 'checkbox', array('label' => 'custom_form.save_data', 'value' => '1', 'current_value' => $formData['save_data']));

		$basicGroup->add('email_notification', 'checkbox', array('label' => 'custom_form.email_notification', 'value' => '1', 'current_value' => $formData['email_notification'], 'wrapperClass' => 'clear-both field-checkbox-space'));
		$basicGroup->add('email_notification_subject', 'text', array('label' => 'custom_form.email_notification_subject', 'value' => $formData['email_notification_subject'], 'required' => false, 'attr' => array('maxlength' => '250')));

		$basicGroup->add('redirect_on_submit', 'checkbox', array('label' => 'custom_form.redirect_on_submit', 'value' => '1', 'current_value' => $formData['redirect_on_submit'], 'wrapperClass' => 'clear-both field-checkbox-space'));
		$basicGroup->add('redirect_on_submit_url', 'text', array('label' => 'custom_form.redirect_on_submit_url', 'value' => $formData['redirect_on_submit_url'], 'required' => false, 'attr' => array('maxlength' => '250')));

//		$basicGroup->add('columns_count', 'choice', array('label' => 'custom_form.columns_count', 'value' => $formData['columns_count'], 'options' => array(
//			1 => 'custom_form.columns_count_one',
//			2 => 'custom_form.columns_count_two'
//		)));

		/**
		 * Form fields
		 */
		$attributesGroup = new core_Form_FormBuilder('group-fields', array('label' => 'custom_form.form_fields', 'collapsible' => false));
		$formBuilder->add($attributesGroup);

		$attributesGroup->add('fields', 'template', array(
			'file' => 'templates/pages/custom-form/edit-fields.html',
			'value' => array('mode' => $mode, 'fields' => json_encode($formFields))
		));

		/**
		 * Description and thank you message
		 */
		$textsGroup = new core_Form_FormBuilder('group-texts', array('label' => 'custom_form.description_and_thank_you_message', 'collapsible' => true));
		$formBuilder->add($textsGroup);

		$textsGroup->add('description', 'textarea', array('label' => 'custom_form.description', 'value' => $formData['description']));
		$textsGroup->add('thank_you_message', 'textarea', array('label' => 'custom_form.thank_you_message', 'value' => $formData['thank_you_message'], 'note' => 'custom_form.thank_you_message_hint'));

		return $formBuilder;
	}

	/**
	 * Get custom form form
	 *
	 * @param $formData
	 * @param $mode
	 * @param int|null $id
	 *
	 * @return core_Form_Form
	 */
	public function getForm($formData, $mode, $id = null)
	{
		return $this->getBuilder($formData, $mode, $id)->getForm();
	}

	public function getCustomFormSearchFormBuilder($data)
	{
		$formBuilder = new core_Form_FormBuilder('search');

		$formBuilder->add('searchParams[search_str]', 'text', array('label' => 'Search in title & form id', 'value' => $data['search_str'], 'wrapperClass' => 'col-sm-12 col-md-4'));

		$display_options = array(
			new core_Form_Option('all', 'Any'),
			new core_Form_Option('active', 'Active'),
			new core_Form_Option('notactive', 'Not active'),
		);

		$formBuilder->add(
			'searchParams[active]',
			'choice',
			array(
				'label' => 'Status',
				'value' => $data['active'],
				'multiple' => false,
				'expanded' => false,
				'options' => $display_options,
				'wrapperClass' => 'col-sm-12 col-md-4',
			)
		);

		$formBuilder->add('orderBy', 'hidden', array('value' => $data['orderBy']));
		$formBuilder->add('orderDir', 'hidden', array('value' => $data['orderDir']));

		return $formBuilder;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function getCustomFormSearchForm($data)
	{
		return $this->getCustomFormSearchFormBuilder($data)->getForm();
	}
}

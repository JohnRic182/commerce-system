<?php

/**
* Session management for the admin area
*/
class Admin_SessionHandler
{
	protected static $instance = null;

	/**
	 * @param $session_instance
	 */
	public static function setSessionInstance($session_instance)
	{
		if (get_class($session_instance) == 'Session' || get_parent_class($session_instance) == 'Session')
			self::$instance = $session_instance;
	}

	/**
	 * @return null|Session
	 */
	public static function getSessionInstance()
	{
		if (is_null(self::$instance)) self::$instance = Session::getInstance();
		return self::$instance;
	}
	
	/**
	 * Initializes the session as used on the login page
	 *
	 * @param string $settings 
	 * @return void
	 * @author Sebastian Nievas
	 */
	public static function loginInit($settings)
	{
		self::init($settings, null, false);
	}
	
	/**
	 * Initializes the session in the admin area
	 *
	 * @param string $settings 
	 * @param string $session_id 
	 * @return void
	 */
	public static function init($settings, $session_id = null, $doSessionIdCheck = true)
	{
		ini_set('session.gc_maxlifetime', 9000); // set max lifetime to 2.5 hrs

		$sessionInit = self::getSessionInstance();

		self::setSessionSavePath($settings);

		$sessionInit->setCacheLimiter('must-revalidate');
		if (!is_null($session_id)) $sessionInit->setId($session_id);

		$sessionInit->setCookieParams(0, '/', null, self::isSecure(), true);
		$sessionInit->setName('ShoppingCartSessionAdmin');
		$sessionInit->start();

		global $session, $registry;
		$session = new Framework_Session(new Framework_Session_WrapperStorage());
		$session->start();

		$registry->set('session', $session);
	}

	/**
	 * @param $settings
	 */
	public static function designModeInit($settings)
	{
		self::loginInit($settings);
	}

	/**
	 * Sets the session save path
	 *
	 * @param string $settings 
	 * @return void
	 */
	private static function setSessionSavePath($settings)
	{
		$session = self::getSessionInstance();
		$FileUtils = FileUtils::getInstance();

		$session_dir = getSessionSavePath();
		if ($FileUtils->is_dir($session_dir) && $FileUtils->is_writable($session_dir))
		{
			$session->setSavePath($session_dir);
		}
	}

	/**
	 * @return bool|null
	 */
	private static function isSecure()
	{
		$secure = true;

		if (!hasSslUrl())
		{
			$secure = null;
		}

		return $secure;
	}
}

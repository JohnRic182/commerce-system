<?php
/**
 * Class Admin_Paginator
 */
class Admin_Paginator
{
	public $itemsCount = 0;
	public $itemsPerPage = 0;
	public $currentPage = 1;
	public $pagesCount = 0;
	public $pages = array();
	public $sqlOffset = 0;
	public $nextPage = null;
	public $prevPage = null;
	public $itemsFrom = 0;
	public $itemsTo = 0;

	/**
	 * Class constructor
	 *
	 * @param $itemsCount
	 * @param $itemsPerPage
	 * @param $currentPage
	 */
	public function __construct($itemsCount, $itemsPerPage, $currentPage)
	{
		$this->itemsCount = intval($itemsCount);
		$this->itemsPerPage = intval($itemsPerPage);
		$this->currentPage = intval($currentPage);
		$this->pages = array();

		$this->pagesCount = ceil($itemsCount / $itemsPerPage);

		if ($this->currentPage < 1) $this->currentPage = 1;
		if ($this->currentPage > $this->pagesCount) $this->currentPage = $this->pagesCount;

		if ($this->pagesCount > 1 && $this->currentPage > 1) $this->prevPage = $this->currentPage - 1;
		if ($this->pagesCount > 1 && $this->currentPage < $this->pagesCount) $this->nextPage = $this->currentPage + 1;

		$this->itemsFrom = ($this->currentPage - 1) * $this->itemsPerPage + 1;
		$this->itemsTo = ($this->currentPage) * $this->itemsPerPage;
		$this->itemsTo = $this->itemsTo > $this->itemsCount ? $this->itemsCount : $this->itemsTo;

		$this->sqlOffset = ($this->currentPage - 1) * $this->itemsPerPage;

		$this->pages = array();

		for ($i=1; $i<=$this->pagesCount; $i++)
		{
			$this->pages[$i] = array(
				'page' => $i,
				'current' => $i == $this->currentPage
			);
		}
	}
}
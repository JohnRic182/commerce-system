<?php

/**
 * Class Admin_Model
 */
class Admin_Model 
{
	protected $db = null;

	/**
	 * Admin_Model constructor.
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		$this->db = $db;
		return $this;
	}
	
	/**
	 * Returns DB instance
	 * @return DB
	 */
	protected function db()
	{
		return $this->db;
	}
}

<?php

/**
 * Class Admin_Service_GlobalAttributesProductsImport
 */
class Admin_Service_GlobalAttributesProductsImport extends Admin_Service_Import
{
	/** @var array $fields */
	protected $fields = array(
		'__skip' => array(
			'title' => 'Skip',
			'type' => '',
			'required' => false,
			'autoAssign' => array()
		),
		'product_id' => array(
			'title' => 'Product Id',
			'type' => self::TYPE_STRING,
			'required' => true,
			'autoAssign' => array('id', 'productid')
		),
		'attribute_name' => array(
			'title' => 'Attribute name',
			'type' => self::TYPE_STRING,
			'required' => true,
			'autoAssign' => array('attributename', 'name', 'globalattributename')
		),
	);

	/**
	 * Find item by current data
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return array|bool
	 */
	protected function findItem($data, $params = null)
	{
		/** @var DataAccess_BaseRepository $repository */
		$repository = $this->repository;

		return $repository->getAttributeByName($data['attribute_name']); 
	}

	/**
	 * @param $item
	 * @param $data
	 * @param null $params
	 * @return bool
	 */
	protected function canUpdate($item, $data, $params = null)
	{
		if (isset($item['gaid']) && count($data['products']) > 0)
		{
			return true;
		}
		return false;
	}

	/**
	 * Modify data before update
	 *
	 * @param $item
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	protected function beforeUpdate($item, $data, $params = null)
	{
		$item['products'] = is_array($item['products']) ? $item['products'] : array();

		$pid = $this->repository->getPidByProductId($data['product_id']);

		if (isset($item['gaid']) && !in_array($pid, $item['products']))
		{
			$data['products'][] = $pid;
		}

		return $data;
	}

	/**
	 * Can insert
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return bool
	 */
	protected function canInsert($data, $params = null)
	{
		return false;
	}
	
	/**
	 * Import
	 *
	 * @param $fileName
	 * @param $fieldMap
	 * @param $delimiter
	 * @param $skipFirstLine
	 * @param $params
	 *
	 * @return array|bool
	 */
	public function import($fileName, $fieldMap, $delimiter, $skipFirstLine, $params = null)
	{
		/** @var DataAccess_BaseRepository $repository */
		$repository = $this->repository;
	
		$defaults = $repository->getDefaults('add');
	
		try
		{
			$addedCount = 0;
			$updatedCount = 0;
			$counter = 0;

			$f = fopen($fileName, 'r');

			while ($line = fgetcsv($f, 0, $delimiter))
			{
				$counter++;

				if ($counter == 1 && $skipFirstLine) continue;

				$data = $this->mapArray($line, $fieldMap);

				if ($data)
				{
					$item = $this->findItem($data, $params);

					if ($item)
					{
						$data = array_merge($item, $this->beforeUpdate($item, $data, $params));

						if ($this->canUpdate($item, $data, $params))
						{
							if ($repository->importPersist($data))
							{
								$addedCount++;
							}
							$this->afterUpdate($item, $data, $params);
						}
					}
				}
			}
	
			fclose($f);
	
			@unlink($fileName);
	
			return array(
					'addedCount' => $addedCount,
					'updatedCount' => $updatedCount
			);
		}
		catch (Exception $e)
		{
	
		}
	
		return false;
	}
	
}
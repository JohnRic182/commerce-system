<?php

/**
 * Class Admin_Service_CategoryExport
 */
class Admin_Service_CategoryExport extends Admin_Service_Export
{
	/** @var array $fields */
	protected $fields = array(
		'id' => 'Category database id',
		'key_name' => 'Category key',
		'name' => 'Name',
		'parent_key' => 'Parent key',
		'is_visible' => 'Is visible', // Yes / No
		'priority' => 'Priority',
		'url_custom' => 'Custom URL',
		'meta_title' => 'Meta title',
		'meta_description' => 'Meta Description',
		'category_header' => 'Category header',
		'description' => 'Description',
		'description_bottom' => 'Bottom description',
		'exactor_euc_code' => 'Exactor EUC code',
		'avalara_tax_code' => 'Avalara tax code'
	);

	protected $selectResult = null;

	/**
	 * @param array $options
	 * @param array $exportFields
	 */
	public function selectData(array $options = null, array $exportFields = null)
	{
		/** @var DB $db */
		$db = $this->db;

		$defaults = array('mode' => 'all');

		$options = array_merge($defaults, $options);

		if ($options['mode'] == 'byIds')
		{
			if (!isset($options['ids'])) return;

			$ids = is_array($options['ids']) ? $options['ids'] : array($options['ids']);

			foreach ($ids as $key=>$value) $ids[$key] = intval($value);

			if (count($ids) < 1) return;

			$where = 'WHERE c.cid IN ('.implode(',', $ids).')';
		}
		else
		{
			$where = 'WHERE c.key_name <> "gift_cert"';
		}

		$this->selectResult = $db->query(
			'SELECT '.
				'c.cid AS id, '.
				'c.key_name AS key_name, '.
				'c.name AS name, '.
				'p.key_name AS parent_key, '.
				'c.is_visible AS is_visible, '.
				'c.priority AS priority, '.
				'c.url_custom AS url_custom, '.
				'c.meta_title AS meta_title, '.
				'c.meta_description AS meta_description, '.
				'c.category_header AS category_header, '.
				'c.description AS description, '.
				'c.description_bottom AS description_bottom, '.
				'c.exactor_euc_code AS exactor_euc_code, '.
				'c.avalara_tax_code AS avalara_tax_code '.
			'FROM '.DB_PREFIX.'catalog AS c '.
				'LEFT JOIN '.DB_PREFIX.'catalog AS p ON c.parent = p.cid '.$where
		);
	}

	/**
	 * Get data from data source. It must return array ordered in exported fields order
	 * @param array|null $exportFields
	 * @return array|false
	 */
	public function getData(array $exportFields = null)
	{
		/** @var DB $db */
		$db = $this->db;

		return $db->moveNext($this->selectResult);
	}
}
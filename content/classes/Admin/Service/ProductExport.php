<?php

/**
 * Class Admin_Service_ProductExport
 */
class Admin_Service_ProductExport extends Admin_Service_Export
{
	protected $selectResult = null;
	protected $filterFields = false;

	protected $maxAttributeCount = 0;

	protected $maxQuantityDiscountCount = 10;

	protected $recurringBillingDataDefault = array(
		'billing_custom' => '', 'billing_amount' => '',	'billing_period_unit' => '',
		'billing_period_frequency' => '', 'billing_period_cycles' => '',
		'trial_enabled' => '', 'trial_custom' => '', 'trial_amount' => '',
		'trial_period_unit' => '', 'trial_period_frequency' => '', 'trial_period_cycles' => '',
		'start_date_delay' => '', 'schedule_description' => '', 'max_payment_failures' => '',
		'auto_complete_initial_order' => '', 'auto_complete_recurring_orders' => '',
		'customer_can_suspend' => '', 'customer_can_cancel' => '',
		'admin_can_adjust_billing' => '', 'admin_can_adjust_trial' => '',
		'customer_notifications' => array(), 'admin_notifications' => array()
	);

	/**
	 * @param array $exportFields
	 * @return array
	 */
	public function getFieldsTitles(array $exportFields = null)
	{
		$fieldsTitles = array('Product ID', 'Name', 'Price');

		if (in_array('is_visible', $exportFields)) $fieldsTitles[] = 'Available';
		if (in_array('product_type', $exportFields)) $fieldsTitles[] = 'Product type';
		if (in_array('category', $exportFields)) $fieldsTitles[] = 'Categories';

		if (in_array('manufacturer', $exportFields)) { $fieldsTitles[] = 'Manufacturer code'; $fieldsTitles[] = 'Manufacturer name'; }
		if (in_array('tax', $exportFields)) { $fieldsTitles[] = 'Is taxable'; $fieldsTitles[] = 'Tax class name'; $fieldsTitles[] = 'Tax class ID';	}
		if (in_array('location', $exportFields)) { $fieldsTitles[] = 'Location name'; $fieldsTitles[] = 'Location code'; }
		if (in_array('shipping', $exportFields)) { $fieldsTitles[] = 'Free shipping'; $fieldsTitles[] = 'Weight'; $fieldsTitles[] = 'Same day delivery'; }
		if (in_array('order', $exportFields)) { $fieldsTitles[] = 'Min order'; $fieldsTitles[] = 'Max order'; }
		if (in_array('sale_price', $exportFields)) $fieldsTitles[] = 'Sale price';
		if (in_array('wholesale_prices', $exportFields)) { $fieldsTitles[] = 'Price level 1'; $fieldsTitles[] = 'Price level 2'; $fieldsTitles[] = 'Price level 3';	}
		if (in_array('call_for_price', $exportFields)) $fieldsTitles[] = 'Call for price';
		if (in_array('priority', $exportFields)) $fieldsTitles[] = 'Priority';
		if (in_array('rating', $exportFields)) {$fieldsTitles[] = 'Reviews Count'; $fieldsTitles[] = 'Rating'; }
		if (in_array('url', $exportFields)) { $fieldsTitles[] = 'URL default'; $fieldsTitles[] = 'URL custom';}
		if (in_array('seo', $exportFields)) { $fieldsTitles[] = 'Meta title'; $fieldsTitles[] = 'Meta description'; }

		if (AccessManager::checkAccess('INVENTORY') && in_array('inventory', $exportFields))
		{
			$fieldsTitles[] = 'Track inventory'; $fieldsTitles[] = 'Stock'; $fieldsTitles[] = 'Stock warning'; $fieldsTitles[] = 'Inventory rule';
		}

		if (in_array('digital_product_file', $exportFields)) $fieldsTitles[] = 'Digital product file';
		if (in_array('overview', $exportFields)) $fieldsTitles[] = 'Overview';
		if (in_array('description', $exportFields)) $fieldsTitles[] = 'Description';
		if (in_array('search_keywords', $exportFields)) $fieldsTitles[] = 'Search keywords';
		if (in_array('image', $exportFields))
		{
			// Primary Image headers
			$primaryImageIndex = 1;
			$fieldsTitles[] = 'Image URL';
			$fieldsTitles[] = 'Image location';
			$fieldsTitles[] = 'Image alt text';

			// Secondary Image headers
			// Get the maximum number of images per product
			$res = $this->db->selectOne('SELECT COUNT(pid) AS total FROM ' . DB_PREFIX . 'products_images GROUP BY pid ORDER BY total DESC LIMIT 1');
			for($secondaryImageIndex = 1; $secondaryImageIndex <= $res['total']; $secondaryImageIndex++)
			{

				$fieldsTitles[] = 'Image URL ' . ($secondaryImageIndex + $primaryImageIndex);
				$fieldsTitles[] = 'Image location ' . ($secondaryImageIndex + $primaryImageIndex);
				$fieldsTitles[] = 'Image alt text ' . ($secondaryImageIndex + $primaryImageIndex);
			}
		}
		if (in_array('marketing', $exportFields)) { $fieldsTitles[] = 'On home page'; $fieldsTitles[] = 'Is hotdeal'; $fieldsTitles[] = 'Doba product'; }
		if (in_array('product_codes', $exportFields)) { $fieldsTitles[] = 'SKU'; $fieldsTitles[] = 'Barcode'; $fieldsTitles[] = 'MPN'; }

		if (in_array('google', $exportFields))
		{
			$fieldsTitles = array_merge($fieldsTitles, array(
				'Google Base item condition', 'Google Base product category', 'Google Base availability', 'Google Base online only',
				'Google Base gender', 'Google Base age group', 'Google Base color', 'Google Base size', 'Google Base material',
				'Google Base pattern'
			));
		}

		if (in_array('amazon', $exportFields)) { $fieldsTitles[] = 'Amazon item id'; $fieldsTitles[] = 'Amazon item id type'; $fieldsTitles[] = 'Amazon item condition'; }
		if (in_array('ebay', $exportFields)) $fieldsTitles[] = 'Ebay category';
		if (in_array('yahoo', $exportFields)) $fieldsTitles[] = 'Yahoo path';
		if (in_array('price_grabber', $exportFields)){ $fieldsTitles[] = 'PriceGrabber category'; $fieldsTitles[] = 'PriceGrabber part number'; $fieldsTitles[] = 'PriceGrabber item condition'; }
		if (in_array('nextag', $exportFields)) { $fieldsTitles[] = 'Nextag category'; $fieldsTitles[] = 'Nextag part number'; $fieldsTitles[] = 'Nextag item condition';}
		if (in_array('added', $exportFields)) $fieldsTitles[] = 'Date Added';
		if (in_array('recurring', $exportFields))
		{
			$fieldsTitles = array_merge($fieldsTitles, array(
				'Recurring enabled', 'Recurring billing custom', 'Recurring billing amount', 'Recurring billing period unit',
				'Recurring billing period frequency', 'Recurring billing period cycles',
				'Recurring trial enabled', 'Recurring trial custom', 'Recurring trial amount',
				'Recurring trial period unit', 'Recurring trial period frequency', 'Recurring trial period cycles',
				'Recurring start date delay', //'Recurring customer can define start date',	//'Recurring max start date delay',
				'Recurring schedule description', 'Recurring max payment failures',
				'Recurring auto complete initial order', 'Recurring auto complete recurring orders',
				'Recurring customer can suspend', 'Recurring customer can cancel',
				'Recurring admin can adjust billing', 'Recurring admin can adjust trial',
				'Recurring customer notifications',	'Recurring admin notifications'
			));
		}

		if (in_array('quantity_discounts', $exportFields))
		{
			for ($i=1; $i<=$this->maxQuantityDiscountCount; $i++)
			{
				$fieldsTitles[] = 'Quantity Discount Active '.$i;
				$fieldsTitles[] = 'Quantity Discount Range Min '.$i;
				$fieldsTitles[] = 'Quantity Discount Range Max '.$i;
				$fieldsTitles[] = 'Quantity Discount Discount '.$i;
				$fieldsTitles[] = 'Quantity Discount Discount Type '.$i;
				$fieldsTitles[] = 'Quantity Discount Free Shipping '.$i;
				$fieldsTitles[] = 'Quantity Discount Apply To Wholesale '.$i;
			}
		}

		if (in_array('attributes', $exportFields))
		{
			$db = $this->db;
			$db->query('SELECT MAX(attributes_count) AS m FROM '.DB_PREFIX.'products');
			if ($db->moveNext())
			{
				$this->maxAttributeCount = $db->col['m'];
				for ($i=1; $i<=$this->maxAttributeCount; $i++)
				{
					$fieldsTitles[] = 'Attribute type '.$i;
					$fieldsTitles[] = 'Attribute name '.$i;
					$fieldsTitles[] = 'Attribute caption '.$i;
					$fieldsTitles[] = 'Attribute options '.$i;
					$fieldsTitles[] = 'Attribute priority '.$i;
					$fieldsTitles[] = 'Attribute track inventory '.$i;
					$fieldsTitles[] = 'Attribute required '.$i;
				}
			}
		}

		if (in_array('youtube', $exportFields)) { $fieldsTitles[] = 'YouTube Link'; }
		if (in_array('product_level_shippings', $exportFields)) { $fieldsTitles[] = 'Product Level Shipping'; }

		if (AccessManager::checkAccess('INVENTORY') && in_array('variants', $exportFields))
		{
			$fieldsTitles[] = 'Sub ID';
			if (in_array('product_codes', $exportFields)) $fieldsTitles[''] = 'Sub SKU';
			$fieldsTitles[] = 'Stock on attribute level';
			$fieldsTitles[] = 'Sub product attributes';
		}

		return $fieldsTitles;
	}

	/**
	 * @param array $options
	 * @param array $exportFields
	 */
	public function selectData(array $options = null, array $exportFields = null)
	{
		/** @var DB $db */
		$db = $this->db;

		$defaults = array('mode' => 'all');

		$options = array_merge($defaults, $options);

		if ($options['mode'] == 'byIds')
		{
			if (!isset($options['ids'])) return;

			$ids = is_array($options['ids']) ? $options['ids'] : array($options['ids']);

			foreach ($ids as $key=>$value) $ids[$key] = intval($value);

			if (count($ids) < 1) return;

			$where = 'WHERE p.pid IN ('.implode(',', $ids).')';
		}
		else
		{
			$searchParams = $options['searchParams'];
			$productRepository = new DataAccess_ProductsRepository($db);
			$where = $productRepository->getSearchQuery($searchParams);
		}

		$this->selectResult = $db->query('
			SELECT
				'.(in_array('manufacturer', $exportFields) ? 'm.manufacturer_code, m.manufacturer_name,' : '').'
				'.(in_array('tax', $exportFields) ? 't.class_name AS tax_class_name, t.key_name AS tax_class_code,' : '').'
				'.(in_array('location', $exportFields) ? 'l.name AS location_name, l.code AS location_code,' : '').'
				p.*
			FROM '.DB_PREFIX.'products AS p
				'.(in_array('manufacturer', $exportFields) ? 'LEFT JOIN '.DB_PREFIX.'manufacturers AS m ON m.manufacturer_id = p.manufacturer_id' : '').'
				'.(in_array('tax', $exportFields) ? 'LEFT JOIN '.DB_PREFIX.'tax_classes AS t ON t.class_id = p.tax_class_id' : '').'
				'.(in_array('location', $exportFields) ? 'LEFT JOIN '.DB_PREFIX.'products_locations AS l ON l.products_location_id = p.products_location_id' : '').'
		'.$where.' ORDER BY p.product_id');
	}

	/**
	 * Get data from data source. It must return array ordered in exported fields order
	 * @param array|null $exportFields
	 * @return array|false
	 */
	public function getData(array $exportFields = null)
	{
		$this->renderData = true;

		/** @var DB $db */
		$db = $this->db;

		$data = $db->moveNext($this->selectResult);

		if (!$data) return false;

			$fields = array($data['product_id'], $data['title'], !is_null($data['price2']) ? $data['price2'] : $data['price']);

		if (in_array('is_visible', $exportFields)) $fields[] = $data['is_visible'];
		if (in_array('product_type', $exportFields)) $fields[] = $data['product_type'];
		if (in_array('category', $exportFields))
		{
			$db->query('
				SELECT * FROM '.DB_PREFIX.'products_categories
				INNER JOIN '.DB_PREFIX.'catalog ON '.DB_PREFIX.'products_categories.cid = '.DB_PREFIX.'catalog.cid
				WHERE '.DB_PREFIX.'products_categories.pid = "'.intval($data['pid']).'"
				ORDER BY '.DB_PREFIX.'products_categories.is_primary DESC, '.DB_PREFIX.'products_categories.cid
			');

			$categories = array();
			while ($category = $db->moveNext()) $categories[] = $category['key_name'];
			$fields[] = implode('||', $categories);
		}
		if (in_array('manufacturer', $exportFields)) { $fields[] = $data['manufacturer_code']; $fields[] = $data['manufacturer_name']; }
		if (in_array('tax', $exportFields)) { $fields[] = $data['is_taxable']; $fields[] = $data['tax_class_name']; $fields[] = $data['tax_class_code']; }
		if (in_array('location', $exportFields)) { $fields[] = $data['location_name']; $fields[] = $data['location_code']; }
		if (in_array('shipping', $exportFields)) { $fields[] = $data['free_shipping']; $fields[] = $data['weight']; $fields[] = $data['same_day_delivery'] == '1' ? 'Yes' : 'No'; }
		if (in_array('order', $exportFields)) { $fields[] = $data['min_order']; $fields[] = $data['max_order']; }
		if (in_array('sale_price', $exportFields)) $fields[] = !is_null($data['price2']) ? $data['price'] : '';
		if (in_array('wholesale_prices', $exportFields)) { $fields[] = $data['price_level_1'];
			$fields[] = $data['price_level_2'];
			$fields[] = $data['price_level_3']; }
		if (in_array('call_for_price', $exportFields)) $fields[] = $data['call_for_price'];
		if (in_array('priority', $exportFields)) $fields[] = $data['priority'];
		if (in_array('rating', $exportFields)) { $fields[] = $data['reviews_count']; $fields[] = $data['rating']; }
		if (in_array('url', $exportFields)) { $fields[] = $data['url_default']; $fields[] = $data['url_custom']; }
		if (in_array('seo', $exportFields)) { $fields[] = $data['meta_title']; $fields[] = $data['meta_description']; }

		if (AccessManager::checkAccess('INVENTORY') && in_array('inventory', $exportFields))
		{
			$fields[] = $data['inventory_control']; $fields[] = $data['stock']; $fields[] = $data['stock_warning']; $fields[] = $data['inventory_rule'];
		}

		if (in_array('digital_product_file', $exportFields)) $fields[] = $data['digital_product_file'];
		if (in_array('overview', $exportFields)) $fields[] = $data['overview'];
		if (in_array('description', $exportFields)) $fields[] = $data['description'];
		if (in_array('search_keywords', $exportFields)) $fields[] = $data['search_keywords'];
		if (in_array('image', $exportFields))
		{
			// lets get the maximum number of images per product
			$fields[] = $data['image_url'];
			$fields[] = $data['image_location'];
			$fields[] = $data['image_alt_text'];
			$primaryImageIndex = 1;

			// for our secondary images
			$res = $this->db->selectOne('SELECT COUNT(pid) AS total FROM ' . DB_PREFIX . 'products_images GROUP BY pid ORDER BY total DESC LIMIT 1');
			if ($res['total'] > 0)
			{
				$secondaryImages = $this->db->selectAll('SELECT * FROM ' . DB_PREFIX . 'products_images WHERE pid=' . intval($data['pid']) . ' ORDER BY image_priority, iid');
				if ($secondaryImages && is_array($secondaryImages))
				{
					foreach ($secondaryImages AS $key => $image)
					{
						$fields[] = $image['image_url'];
						$fields[] = $image['image_location'];
						$fields[] = $image['alt_text'];
						$primaryImageIndex++;
					}
				}
			}

			// fill in empty spaces, if other images on products is empty
			for ($secondaryImageIndex = $primaryImageIndex; $secondaryImageIndex <= $res['total']; $secondaryImageIndex++)
			{
				$fields[] = ''; $fields[] = ''; $fields[] = '';
			}
		}

		if (in_array('marketing', $exportFields)) { $fields[] = $data['is_home']; $fields[] = $data['is_hotdeal']; $fields[] = 'Doba product'; }
		if (in_array('product_codes', $exportFields)) { $fields[] = $data['product_sku']; $fields[] = $data['product_gtin']; $fields[] = $data['product_mpn']; }

		if (in_array('google', $exportFields))
		{
			$fields = array_merge($fields, array(
				$data['google_item_condition'], $data['google_product_type'], $data['google_availability'], $data['google_online_only'],
				$data['google_gender'], $data['google_age_group'], $data['google_color'], $data['google_size'], $data['google_material'],
				$data['google_pattern']
			));
		}

		if (in_array('amazon', $exportFields)) {
			$fields[] = $data['amazon_id'];
			$fields[] = $data['amazon_id_type'];
			$fields[] = $data['amazon_item_condition'];
		}
		if (in_array('ebay', $exportFields)) $fields[] = $data['ebay_cat_id'];
		if (in_array('yahoo', $exportFields)) $fields[] = $data['yahoo_path'];
		if (in_array('price_grabber', $exportFields)){ $fields[] = $data['pricegrabber_category']; $fields[] = $data['pricegrabber_part_number']; $fields[] = $data['pricegrabber_item_condition']; }
		if (in_array('nextag', $exportFields)) { $fields[] = $data['nextag_category']; $fields[] = $data['nextag_part_number']; $fields[] = $data['nextag_item_condition']; }
		if (in_array('added', $exportFields)) $fields[] = $data['added'];

		if (in_array('recurring', $exportFields))
		{

			if ($data['recurring_billing_data'] != '')
			{
				$rd = @unserialize($data['recurring_billing_data']);
				$rd = is_array($rd) ? array_merge($this->recurringBillingDataDefault, $rd) : $this->recurringBillingDataDefault;
			}
			else
			{
				$rd = $this->recurringBillingDataDefault;
			}

			$fields = array_merge($fields, array(
				$data['enable_recurring_billing'], $rd['billing_custom'], $rd['billing_amount'], $rd['billing_period_unit'],
				$rd['billing_period_frequency'], $rd['billing_period_cycles'],
				$rd['trial_enabled'], $rd['trial_custom'], $rd['trial_amount'],
				$rd['trial_period_unit'], $rd['trial_period_frequency'], $rd['trial_period_cycles'],
				$rd['start_date_delay'], //'Recurring customer can define start date',	//'Recurring max start date delay',
				$rd['schedule_description'], $rd['max_payment_failures'],
				$rd['auto_complete_initial_order'], $rd['auto_complete_recurring_orders'],
				$rd['customer_can_suspend'], $rd['customer_can_cancel'],
				$rd['admin_can_adjust_billing'], $rd['admin_can_adjust_trial'],
				implode('||', $rd['customer_notifications']),	implode('||', $rd['admin_notifications'])
			));
		}

		if (in_array('quantity_discounts', $exportFields))
		{
			$actualQuantityDiscountCount = 0;

			$qdResult =$db->query('SELECT * FROM '.DB_PREFIX.'products_quantity_discounts WHERE pid='.intval($data['pid']));

			while ($qd = $db->moveNext($qdResult))
			{
				$fields[] = $qd['is_active'];
				$fields[] = $qd['range_min'];
				$fields[] = $qd['range_max'];
				$fields[] = $qd['discount'];
				$fields[] = $qd['discount_type'];
				$fields[] = $qd['free_shipping'];
				$fields[] = $qd['apply_to_wholesale'];
				$actualQuantityDiscountCount++;
			}

			// fill in empty spaces
			for ($i = $actualQuantityDiscountCount; $i < $this->maxQuantityDiscountCount; $i++)
			{
				$fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = '';
			}
		}

		if (in_array('attributes', $exportFields))
		{
			$actualAttributeCount = 0;
			
			if ($data['attributes_count'] > 0)
			{
				$attributesResult =$db->query('SELECT * FROM '.DB_PREFIX.'products_attributes WHERE pid='.intval($data['pid']));

				while ($attribute = $db->moveNext($attributesResult))
				{
					$fields[] = $attribute['attribute_type'];
					$fields[] = $attribute['name'];
					$fields[] = $attribute['caption'];
					$fields[] = in_array($attribute['attribute_type'], array('select', 'radio'))?  str_replace("\n", '||', str_replace("\r", "", $attribute['options'])) : $attribute['text_length'];
					$fields[] = $attribute['priority'];
					$fields[] = $attribute['track_inventory'];
					$fields[] = $attribute['attribute_required'];
					$actualAttributeCount++;
				}
			}
			// fill in empty spaces
			for ($i = $actualAttributeCount; $i < $this->maxAttributeCount; $i++)
			{
				$fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = ''; $fields[] = '';
			}
		}

		if (in_array('youtube', $exportFields))
		{
			$fields[] = $data['youtube_link'];
		}

		// product level shipping info
		if (in_array('product_level_shippings', $exportFields))
		{
			$productShippingResult =$db->query('
				SELECT psp.ssid, ss.carrier_name, psp.price FROM ' . DB_PREFIX . 'products_shipping_price psp
				INNER JOIN ' . DB_PREFIX . 'shipping_selected ss ON(psp.ssid = ss.ssid)
				WHERE psp.pid=' . intval($data['pid']) . ' AND ss.method_id ="product"
			');

			$dtaProductLevelShipping = array();
			if ($db->numRows($productShippingResult) > 0)
			{
				while ($productLevelShipping = $db->moveNext($productShippingResult))
				{
					$dtaProductLevelShipping[] = $productLevelShipping['carrier_name'] . ':' . $productLevelShipping['price'];
				}
			}

			$fields[] = implode("||", $dtaProductLevelShipping);
		}

		if (AccessManager::checkAccess('INVENTORY') && in_array('variants', $exportFields))
		{
			$prInvResult = $db->query('SELECT * FROM '.DB_PREFIX.'products_inventory WHERE pid='.intval($data['pid']));
			if ($db->numRows($prInvResult) > 0)
			{
				while (($prInv = $db->moveNext($prInvResult)) != false)
				{
					$_fields = $fields;

					$_fields[] = $prInv['product_subid'];
					if (in_array('product_codes', $exportFields)) $_fields[] = $prInv['product_sku'];
					$_fields[] = $prInv['stock'];
					$_fields[] = implode('||', explode("\n", str_replace("\r", "", $prInv['attributes_list'])));
					$this->render($_fields);
				}

				$this->renderData = false;
			}
			else
			{
				$fields[] = '';
				if (in_array('product_codes', $exportFields)) $fields[] = '';
				$fields[] = '';
				$fields[] = '';
			}
		}

		return $fields;
	}

}
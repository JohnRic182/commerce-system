<?php

/**
 * Class Admin_Service_OrderExport
 */
class Admin_Service_OrderExport extends Admin_Service_Export
{
	/** @var array $fields */
	protected $fields = array(
		'order_date' => 'Order Date',
		'order_num' => 'Order ID',
		'status' => 'Order Status',
		'payment_status' => 'Payment Status',
		'billing_name' => 'Customer Name',
		'billing_company' => 'Company Name',
		'billing_address1' => 'Billing Address 1',
		'billing_address2' => 'Billing Address 2',
		'billing_city' => 'Billing City',
		'billing_state_name' => 'Billing State',
		'billing_zip' => 'Billing Zip/Postal Code',
		'billing_country_name' => 'Billing Country',
		'billing_phone' => 'Phone',
		'billing_email' => 'Email Address',
		'shipping_address_type' => 'Shipping Address Type',
		'shipping_name' => 'Shipping Name',
		'shipping_company' => 'Shipping Company Name',
		'shipping_address1' => 'Shipping Address 1',
		'shipping_address2' => 'Shipping Address 2',
		'shipping_city' => 'Shipping City',
		'shipping_state_name' => 'Shipping State',
		'shipping_zip' => 'Shipping Zip/Postal Code',
		'shipping_country_name' => 'Shipping Country',
		'shipping_cm_name' => 'Shipping Method',
		'custom_fields' => 'Additional Information',
		'subtotal_amount' => 'Subtotal Amount',
		'promo_discount_amount' => 'Discount Amount',
		'tax_amount' => 'Tax Amount',
		'shipping_amount' => 'Shipping Amount',
		'handling_amount' => 'Handling Amount',
		'total_amount' => 'Total Amount',
		'product_id' => 'Product ID',
		'product_sub_id' => 'Product Sub ID',
		'product_title' => 'Product Title',
		'product_quantity' => 'Product Quantity',
		'product_price' => 'Product Price',
		'product_weight' => 'Product Weight',
		'product_attributes' => 'Product Attributes',
		'order_type' => 'Order Type',
		'product_type' => 'Product Type',
		'tracking_number' => 'Tracking Number',
		//'recurring_billing_data' => 'Recurring Billing Data',
		'billing_period_unit' => 'Billing Period Unit',
		'billing_period_frequency' => 'Billing Period Frequency',
		'billing_period_cycles' => 'Billing Period Cycles',
		'billing_amount' => 'Billing Amount',
		'trial_enabled' => 'Trial Enabled',
		'trial_period_unit' => 'Trial Period Unit',
		'trial_period_frequency' => 'Trial Period Frequency',
		'trial_period_cycles' => 'Trial Period Cycles',
		'trial_amount' => 'Trial Amount',
		'start_date' => 'Start Date',
		'payment_profile_id' => 'Payment Profile Id'
	);

	/** @var DataAccess_OrderRepository $repository */
	protected $repository;

	protected $selectResult = null;
	protected $lastOid = 0;
	protected $lastAttributes = '';
	protected $lastCustomFields = array();

	public function __construct(DB $db, DataAccess_OrderRepository $repository)
	{
		parent::__construct($db);
		$this->repository = $repository;

		// prepare custom fields
		$customFields = $db->selectAll('
			SELECT field_id, field_name
			FROM ' . DB_PREFIX . 'custom_fields
			WHERE is_active = 1
			ORDER BY field_place, priority, field_type, field_name
		');

		foreach ($customFields as $customField)
		{
			$this->fields['cf_' . $customField['field_id']] = $customField['field_name'];
		}
	}

	/**
	 * @param array|null $options
	 * @param array|null $exportFields
	 */
	public function selectData(array $options = null, array $exportFields = null)
	{
		$defaults = array('mode' => 'search');
		$options = array_merge($defaults, $options);
		$this->selectResult = $this->repository->getExportResults($options);
	}

	/**
	 * Get data from data source. It must return array ordered in exported fields order
	 * @param array|null $exportFields
	 * @return array
	 */
	public function getData(array $exportFields = null)
	{
		$db = $this->db;

		$ret = $db->moveNext($this->selectResult);

		// force  shipping method value if not set or if free
		if (isset($ret['free_shipping']) && $ret['free_shipping'] == 'Yes')
		{
			$ret['shipping_cm_name'] = 'Free shipping';
		}
		else{
			if (isset($ret['shipping_cm_name']) && empty($ret['shipping_cm_name']))
			{
				$ret['shipping_cm_name'] = 'No shipping method assigned';
			}
		}
		
		if ($ret)
		{
			if ($ret['oid'] != $this->lastOid)
			{
				$options = $ret['product_options'];
				$this->lastAttributes = '';
				$this->lastOid = $ret['oid'];

				if (trim($options) != '')
				{
					$opt_str = explode("\n", $options);
					foreach ($opt_str as $opt)
					{
						$this->lastAttributes .= $opt."||";
					}
				}

				$customFieldsValues = $db->selectAll('
					SELECT cf.field_id, cfv.field_value
					FROM ' . DB_PREFIX . 'custom_fields cf
					LEFT JOIN ' . DB_PREFIX . 'custom_fields_values cfv ON
						cf.field_id = cfv.field_id
						AND (
							cfv.order_id = ' . intval($ret['oid']) . ' OR
							(cfv.profile_id = ' . intval($ret['uid']) . ' AND cfv.order_id = 0)
						)
			  	');

				$this->lastCustomFields = array();

				if ($customFieldsValues)
				{
					foreach ($customFieldsValues as $customFieldValue)
					{
						$this->lastCustomFields['cf_' . $customFieldValue['field_id']] = $customFieldValue['field_value'];
					}
				}
			}

			$ret['product_attributes'] = $this->lastAttributes;

			$ret = array_merge($ret, $this->lastCustomFields);

			// TODO: serialize data that contains information about recurring properties
			if ($ret['recurring_billing_data'])
			{
				$recurringBillingData = unserialize($ret['recurring_billing_data']);
				foreach (array_keys($recurringBillingData) AS $field)
				{
					$this->fields[$field] = ucwords(str_replace("_", " ", $field));
				}
				$ret = array_merge($ret, $recurringBillingData);
			}
		}

		return $ret;
	}
}

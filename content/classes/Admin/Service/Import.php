<?php

/**
 * Class Admin_Service_Import
 */
class Admin_Service_Import
{
	const TYPE_STRING = 'string';
	const TYPE_INTEGER = 'integer';
	const TYPE_NUMBER = 'number';
	const TYPE_BOOLEAN = 'boolean';
	const TYPE_MONEY = 'money';

	/** @var DataAccess_BaseRepository $repository */
	protected $repository;

	/** @var Admin_Service_FileUploader $fileUploaderService */
	protected $fileUploaderService;

	/** @var array $fields */
	protected $fields = array();

	protected $booleanTrue = array('true', 'yes', '1', 'ok', 'on');
	protected $booleanFalse = array('false', 'no', '0', 'not', 'off');

	protected $tmpPath = 'content/cache/tmp';

	/**
	 * @param DataAccess_BaseRepository $repository
	 * @param Admin_Service_FileUploader $fileUploaderService
	 * @param null $params
	 */
	public function __construct(DataAccess_BaseRepository $repository, Admin_Service_FileUploader $fileUploaderService, $params = null)
	{
		$this->repository = $repository;
		$this->fileUploaderService = $fileUploaderService;
	}

	/**
	 * Return fields
	 *
	 * @return array
	 */
	public function getFields()
	{
		return $this->fields;
	}

	/**
	 * Save uploaded file
	 *
	 * @param $fileInfo
	 * @return bool|string
	 */
	public function saveUploadedFile($fileInfo)
	{
		/** @var Admin_Service_FileUploader $fileUploaderService */
		$fileUploaderService = $this->fileUploaderService;

		if ($fileName = $fileUploaderService->moveUploaderFile($fileInfo, $this->tmpPath, md5(uniqid(rand(),1))))
		{
			return $fileName;
		}

		return false;
	}

	/**
	 * @param $fileName
	 * @param $delimiter
	 * @return int
	 */
	public function getAssignOptions($fileName, $delimiter)
	{
		try
		{
			$previewLines = array();
			$max = 0;

			$f = fopen($fileName, 'r');

			$totalLinesCount = 0;

			while ($line = fgetcsv($f, 1048576, $delimiter))
			{
				$max = count($line) > $max ? count($line) : $max;

				if (count($previewLines) < 4)
				{
					for ($i = 0; $i < count($line); $i++)
					{
						$lineValue = $line[$i];
						$line[$i] = strlen($lineValue) > 40 ? substr($lineValue, 0, 40).'...' : $lineValue;
					}

					$previewLines[] = $line;
				}

				$totalLinesCount++;
			}

			fclose($f);

			// auto-assign columns
			$columns = array_fill(0, $max, '');

			if (isset($previewLines[0]) && is_array($previewLines[0]))
			{
				foreach ($previewLines[0] as $key => $value)
				{
					foreach ($this->fields as $fieldName => $field)
					{
						$value = preg_replace("/[^a-z0-9]/", '', strtolower(trim($value)));
						if (in_array($value, $field['autoAssign']))
						{
							$columns[$key] = $fieldName;
						}
					}
				}
			}

			$result = array(
				'totalLinesCount' => $totalLinesCount,
				'previewLinesCount' => count($previewLines),
				'previewLines' => $previewLines,
				'columnsCount' => $max,
				'columns' => $columns
			);

			return $result;
		}
		catch (Exception $e)
		{

		}

		return false;
	}

	/**
	 * Check are all required field are assigned
	 *
	 * @param $fields
	 * @return bool
	 */
	public function validateRequiredFields($fields)
	{
		foreach ($this->fields as $fieldName => $field)
		{
			if ($field['required'] && !in_array($fieldName, $fields))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Map an array
	 *
	 * @param $data
	 * @param $fieldMap
	 *
	 * @return array
	 */
	protected function mapArray(array $data, array $fieldMap)
	{
		$mapped = array();

		for ($i = 0; $i < count($data); $i++)
		{
			if (isset($fieldMap[$i]))
			{
				$fieldName = $fieldMap[$i];

				if (isset($this->fields[$fieldName]))
				{
					$field = $this->fields[$fieldName];
					if ($field['type'] == self::TYPE_BOOLEAN)
					{
						$value = in_array(strtolower(trim($data[$i])), $this->booleanTrue);

						if ($field['subtype'] == 'numeric') $value = $value ? '1' : '0';
						else if ($field['subtype'] == 'yesno') $value = $value ? 'yes' : 'no';
						else if ($field['subtype'] == 'YESNO') $value = $value ? 'YES' : 'NO';
						else if ($field['subtype'] == 'YesNo') $value = $value ? 'Yes' : 'No';

						$mapped[$fieldName] = $value;
					}
					else if ($field['type'] == self::TYPE_INTEGER)
					{
						$mapped[$fieldName] = intval($data[$i]);
					}
					else if ($field['type'] == self::TYPE_MONEY || $field['type'] == self::TYPE_NUMBER)
					{
						$mapped[$fieldName] = trim($data[$i]) == '' ? null : normalizeNumber($data[$i]);
					}
					else
					{
						$mapped[$fieldName] = $this->getUTF8($data[$i]);
					}
				}
			}
		}

		return $mapped;
	}

	/**
	 * Find item by current data
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool
	 */
	protected function findItem($data, $params = null)
	{
		return false;
	}

	/**
	 * Modify data before insert
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	protected function beforeInsert($data, $params = null)
	{
		return $data;
	}

	/**
	 * After insert
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	protected function afterInsert($data, $params = null)
	{
		return $data;
	}

	/**
	 * Can insert
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return bool
	 */
	protected function canInsert($data, $params = null)
	{
		return true;
	}

	/**
	 * @param $item
	 * @param $data
	 * @param null $params
	 * @return mixed
	 */
	protected function beforeUpdate($item, $data, $params = null)
	{
		return $data;
	}

	/**
	 * @param $item
	 * @param $data
	 * @param null $params
	 * @return mixed
	 */
	protected function afterUpdate($item, $data, $params = null)
	{
		return $data;
	}

	/**
	 * @param $item
	 * @param $data
	 * @param null $params
	 * @return bool
	 */
	protected function canUpdate($item, $data, $params = null)
	{
		return true;
	}

	/**
	 * Import
	 *
	 * @param $fileName
	 * @param $fieldMap
	 * @param $delimiter
	 * @param $skipFirstLine
	 * @param $params
	 *
	 * @return array|bool
	 */
	public function import($fileName, $fieldMap, $delimiter, $skipFirstLine, $params = null)
	{
		/** @var DataAccess_BaseRepository $repository */
		$repository = $this->repository;

		$defaults = $repository->getDefaults('add');

		try
		{
			$addedCount = 0;
			$updatedCount = 0;
			$counter = 0;
			$skippedLine = 0;

			$f = fopen($fileName, 'r');

			while ($line = fgetcsv($f, 0, $delimiter))
			{
				$counter++;

				if ($counter == 1 && $skipFirstLine) continue;

				$data = $this->mapArray($line, $fieldMap);

				if ($data)
				{
					$item = $this->findItem($data, $params);

					if ($item && is_array($item))
					{
						$data = array_merge($item, $this->beforeUpdate($item, $data, $params));

						if ($this->canUpdate($item, $data, $params))
						{
							$repository->persist($data);
							$this->afterUpdate($item, $data, $params);
							$updatedCount++;
						}
						else
						{
							$skippedLine++;
						}
					}
					else
					{
						$data = array_merge($defaults, $this->beforeInsert($data, $params));

						if ($this->canInsert($data, $params))
						{
							$data['id'] = $repository->persist($data);
							$this->afterInsert($data, $params);
							$addedCount++;
						}
						else
						{
							$skippedLine++;
						}
					}
				}
			}

			fclose($f);

			@unlink($fileName);

			return array(
				'addedCount' => $addedCount,
				'updatedCount' => $updatedCount,
				'skippedLineCount' => $skippedLine
			);
		}
		catch (Exception $e)
		{

		}

		return false;
	}

	protected function getUTF8($string, $encoding = false)
	{
		mb_detect_order("UTF8, ASCII, auto");
		$enc = mb_detect_encoding($string);
		return mb_convert_encoding($string, "UTF-8", $enc == "" ? "ASCII" : $enc);
	}
}

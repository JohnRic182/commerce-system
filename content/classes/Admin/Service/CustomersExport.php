<?php

/**
 * Class Admin_Service_CustomersExport
 */
class Admin_Service_CustomersExport extends Admin_Service_Export
{
	/** @var array $fields */
	protected $fields = array(
		'fname' => 'First Name',
		'lname' => 'Last Name',
		'company' => 'Company',
		'address1' => 'Address Line 1',
		'address2' => 'Address Line 2',
		'city' => 'City',
		'state_name' => 'State',
		'zip' => 'Zip',
		'country_name' => 'Country',
		'email' => 'Email',
		'phone' => 'Phone',
		'login' => 'Username',
		'receives_marketing' => 'Receives Marketing',
		'custom_fields' => 'Additional Information',
	);

	protected $selectResult = null;

	/**
	 * @param array|null $options
	 * @param array|null $exportFields
	 */
	public function selectData(array $options = null, array $exportFields = null)
	{
		$db = $this->db;

		$defaults = array('mode' => 'all');
		$options = array_merge($defaults, $options);

		$where = "WHERE u.removed='No' ";

		if ($options['mode'] == 'selected')
		{
			if (!isset($options['ids']))
			{
				return;
			}
			$ids = is_array($options['ids']) ? $options['ids'] : array($options['ids']);
			foreach ($ids as $key => $value)
			{
				$ids[$key] = intval($value);
			}
			if (count($ids) < 1)
			{
				return;
			}
			$where .= ' AND u.uid IN (' . implode(',', $ids) . ')';
		}

		$query = "
			SELECT u.*,c.name AS country_name,
				IF(s.name IS NULL, province, s.name) AS state_name
			FROM " . DB_PREFIX . "users u
				INNER JOIN " . DB_PREFIX . "countries c ON u.country = c.coid
				LEFT JOIN " . DB_PREFIX . "states s ON u.state = s.stid
				" . $where . "
			ORDER BY u.lname, u.fname
		";
		$this->selectResult = $db->query($query);
	}

	/**
	 * Get data from data source. It must return array ordered in exported fields order
	 *
	 * @param array|null $exportFields
	 *
	 * @return array|false
	 */
	public function getData(array $exportFields = null)
	{
		$db = $this->db;

		$ret = $db->moveNext($this->selectResult);

		if ($ret)
		{
			$sub_q = "
				SELECT field_value, cf.field_caption
				FROM " . DB_PREFIX . "custom_fields_values cfv
				LEFT JOIN " . DB_PREFIX . "custom_fields cf ON cfv.field_id = cf.field_id
				WHERE profile_id= " . intval($ret['uid']) . " AND order_id = '0' AND address_id = '0' ORDER BY cfv.field_id
			";

			$custom_fields_str = '';
			$customFieldsResults = $db->query($sub_q);
			while (($val = $db->moveNext($customFieldsResults)) !== false)
			{
				$custom_fields_str = $val['field_caption'] . ':  ' . $val['field_value'];
			}
			$ret['custom_fields'] = $custom_fields_str;
		}

		return $ret;
	}
}
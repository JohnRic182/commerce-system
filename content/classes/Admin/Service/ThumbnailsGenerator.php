<?php

/**
 * Class Admin_Service_ThumbnailsGenerator
 */
class Admin_Service_ThumbnailsGenerator
{
	/** @var DB */
	protected $db;

	/** @var Framework_Session */
	protected $session;

	protected $productImagesDir = 'images/products/';
	protected $productThumbsDir = 'images/products/thumbs/';
	protected $productPreviewDir = 'images/products/preview/';
	protected $productSecondaryImagesDir = 'images/products/secondary/';
	protected $productSecondaryImagesThumbsDir = 'images/products/secondary/thumbs/';
	protected $catalogImagesDir = 'images/catalog/';
	protected $catalogThumbsDir = 'images/catalog/thumbs/';
	protected $manufacturerImagesDir = 'images/manufacturers/';
	protected $manufacturerThumbsDir = 'images/manufacturers/thumbs/';

	/** @var Admin_Service_ImageOptimizer */
	protected $imageOptimizer = null;

	/**
	 * Admin_Service_ThumbnailsGenerator constructor.
	 * @param DB $db
	 * @param Framework_Session $session
	 */
	public function __construct(DB $db, Framework_Session $session)
	{
		$this->db = $db;
		$this->session = $session;
	}

	/**
	 * @param $generateType
	 * @param $generateTarget
	 * @param $optimizePrimary
	 * @return int
	 */
	public function scanImages($generateType, $generateTarget, $optimizePrimary)
	{
		$imagesPrimary = $this->scanProductImages($generateType);
		$imagesCatalog = $this->scanCatalogImages($generateType);
		$imagesManufacturer = $this->scanManufacturerImages($generateType);
		$imagesSecondary = $this->scanSecondaryProductImages($generateType);

		$primaryCount = count($imagesPrimary);
		$catalogCount = count($imagesCatalog);
		$manufacturerCount = count($imagesManufacturer);
		$secondaryCount = count($imagesSecondary);

		$totalCount = $primaryCount + $secondaryCount + $catalogCount + $manufacturerCount;

		if ($totalCount > 0)
		{
			$this->session->set('thumb_generator', array(
				'primaryCount' => $primaryCount,
				'secondaryCount' => $secondaryCount,
				'imagesPrimary' => $imagesPrimary,
				'imagesSecondary' => $imagesSecondary,
				'generateType' => $generateType,
				'generateTarget' => $generateTarget,
				'optimizePrimary' => $optimizePrimary,
				'totalCount' => $primaryCount + $secondaryCount,
				'primaryPosition' => 0,
				'secondaryPosition' => 0,
				'catalogCount' => $catalogCount,
				'catalogPosition' => 0,
				'imagesCatalog' => $imagesCatalog,
				'manufacturerCount' => $manufacturerCount,
				'manufacturerPosition' => 0,
				'imagesManufacturer' => $imagesManufacturer,
			));
		}

		return $totalCount;
	}

	/**
	 * @param $generateType
	 * @return array
	 */
	protected function &scanProductImages($generateType)
	{
		$imagesPrimary = array();
		if ($generateType == 'all' || $generateType == 'thumbs' || $generateType == 'products')
		{
			$d = dir($this->productImagesDir);
			while (($entry = $d->read()) !== false)
			{
				$filePath = $this->productImagesDir.$entry;
				if (is_file($filePath))
				{
					$fileInfo = pathinfo(strtolower($filePath));
					if ($fileInfo['extension'] == 'jpg' || $fileInfo['extension'] == 'jpeg' || $fileInfo['extension'] == 'png' || $fileInfo['extension'] == 'gif')
					{
						$imagesPrimary[] = $filePath;
					}
				}
			}
			$d->close();
		}

		return $imagesPrimary;
	}

	/**
	 * @param $generateType
	 * @return array
	 */
	protected function &scanCatalogImages($generateType)
	{
		$imagesCatalog = array();
		if ($generateType == 'all' || $generateType == 'thumbs')
		{
			$d = dir($this->catalogImagesDir);
			while (($entry = $d->read()) !== false)
			{
				$filePath = $this->catalogImagesDir.$entry;
				if (is_file($filePath))
				{
					$fileInfo = pathinfo(strtolower($filePath));
					if ($fileInfo['extension'] == 'jpg' || $fileInfo['extension'] == 'jpeg' || $fileInfo['extension'] == 'png' || $fileInfo['extension'] == 'gif')
					{
						$imagesCatalog[] = $filePath;
					}
				}
			}
			$d->close();
		}

		return $imagesCatalog;
	}

	/**
	 * @param $generateType
	 * @return array
	 */
	protected function &scanManufacturerImages($generateType)
	{
		$imagesManufacturer = array();

		if ($generateType == 'all' || $generateType == 'thumbs')
		{
			$d = dir($this->manufacturerImagesDir);
			while (($entry = $d->read()) !== false)
			{
				$filePath = $this->manufacturerImagesDir.$entry;
				if (is_file($filePath))
				{
					$fileInfo = pathinfo(strtolower($filePath));
					if ($fileInfo['extension'] == 'jpg' || $fileInfo['extension'] == 'jpeg' || $fileInfo['extension'] == 'png' || $fileInfo['extension'] == 'gif')
					{
						$imagesManufacturer[] = $filePath;
					}
				}
			}
			$d->close();
		}

		return $imagesManufacturer;
	}

	/**
	 * @param $generateType
	 * @return array
	 */
	protected function &scanSecondaryProductImages($generateType)
	{
		$imagesSecondary = array();

		if ($generateType == 'all' || $generateType == 'secondary')
		{
			$db = $this->db;

			// Filename should be check if its empty because external image has no filename
			$db->query('SELECT filename, type FROM ' . DB_PREFIX . 'products_images WHERE filename <> ""');
			while (($img = $db->moveNext()) != false)
			{
				$imagesSecondary[] = $this->productSecondaryImagesDir . $img['filename'] . '.' . $img['type'];
			}
		}
		return $imagesSecondary;
	}

	/**
	 * @return array
	 */
	public function generateThumbs()
	{
		$atOnce = rand(10, 25);
		$imageErrors = array();

		// Get data from session

		$sessionData = $this->session->get('thumb_generator');

		$primaryPosition = $sessionData['primaryPosition'];
		$secondaryPosition = $sessionData['secondaryPosition'];
		$catalogPosition = $sessionData['catalogPosition'];
		$manufacturerPosition = $sessionData['manufacturerPosition'];

		$primaryCount = $sessionData['primaryCount'];
		$secondaryCount = $sessionData['secondaryCount'];
		$catalogCount = $sessionData['catalogCount'];
		$manufacturerCount = $sessionData['manufacturerCount'];

		$generateType = $sessionData['generateType'];
		$generateTarget = $sessionData['generateTarget'];
		$optimizePrimary = $sessionData['optimizePrimary'];

		//Check what is already done
		$primaryDone = $primaryPosition >= $primaryCount;
		$secondaryDone = $secondaryPosition >= $secondaryCount;
		$catalogDone = $catalogPosition >= $catalogCount;
		$manufacturerDone = $manufacturerPosition >= $manufacturerCount;

		//Pre-load all products ids from database
		$products = array();

		$this->db->query('SELECT pid, product_id FROM ' . DB_PREFIX . 'products');

		while (($product = $this->db->moveNext()) != false)
		{
			$products[strtolower(trim($product['product_id']))] = $product['pid'];
		}

		$optimizer = $this->getImageOptimizerService();

		/**
		 * Generate product primary images
		 */
		if (!$primaryDone)
		{
			$productsIds = array();

			//Loop images to be processed
			$limit = $primaryPosition + $atOnce;
			$limit = $limit > $primaryCount ? $primaryCount : $limit;

			for ($i = $primaryPosition; $i < $limit ; $i++)
			{
				$primaryImageFileName = $sessionData['imagesPrimary'][$i];

				$pathInfo = pathinfo($primaryImageFileName);

				if (!isset($pathInfo['name'])) $pathInfo['name'] = substr($pathInfo['basename'], 0, strlen($pathInfo['basename']) - strlen($pathInfo['extension']) - 1);

				if (isset($products[$pathInfo['name']]))
				{
					$pid = intval($products[$pathInfo['name']]);

					$addToWhere = true;

					$thumbFileName = $this->productThumbsDir . $pathInfo['name'] . '.' . $pathInfo['extension'];
					$thumbFileExists = is_file($thumbFileName);

					/**
					 * Optimize primary image file when needed
					 */
					if ($optimizePrimary == 'all' || ($optimizePrimary == 'new' && !$thumbFileExists))
					{
						$optimizer->optimize($primaryImageFileName);
					}

					/**
					 * Generate thumb file when needed
					 */
					if (($generateType == 'all' || $generateType == 'thumbs') && ($generateTarget == 'all' || ($generateTarget == 'new' && !$thumbFileExists)))
					{
						if (ImageUtility::generateProductThumb($primaryImageFileName, $thumbFileName))
						{
							$addToWhere = false;
							$productsIds[] = $pid;
						}
						else
						{
							array_push($imageErrors, 'Error generating thumbnail image - ' .$pathInfo['basename']);
						}
					}

					/**
					 * Generate product page image
					 */
					if (($generateType == 'all' || $generateType == 'products'))
					{
						$productPageFileName = $this->productPreviewDir . $pathInfo['name'] . '.' . $pathInfo['extension'];

						if ($generateTarget == 'all' || ($generateTarget == 'new' && !is_file($productPageFileName)))
						{
							if (ImageUtility::generateProductPreview($primaryImageFileName, $productPageFileName))
							{
								if ($addToWhere) $productsIds[] = $pid;
							}
							else
							{
								array_push($imageErrors, 'Error generating product image - ' . $pathInfo['basename']);
							}
						}
					}
				}
			}

			/**
			 * Update products location
			 */
			if (count($productsIds) > 0)
			{
				$this->db->query('UPDATE ' . DB_PREFIX . 'products SET image_location="Local", image_url="" WHERE pid IN (' . implode(',', $productsIds). ')');
			}

			$primaryPosition = $i;

			/**
			 * Check is it time to stop
			 */
			if ($primaryPosition + 1 >= $primaryCount)
			{
				$primaryDone = true;
			}

		}
		unset($products);

		/**
		 * Product secondary images
		 */
		if ($primaryDone && !$secondaryDone)
		{
			$limit = $secondaryPosition + $atOnce;
			$limit = $limit > $secondaryCount ? $secondaryCount : $limit;

			for ($i = $secondaryPosition; $i < $limit; $i++)
			{
				$secondaryImageFileName = $sessionData['imagesSecondary'][$i];

				$pathInfo = pathinfo($secondaryImageFileName);

				if (!isset($pathInfo['name'])) $pathInfo['name'] = substr($pathInfo['basename'], 0, strlen($pathInfo['basename']) - strlen($pathInfo['extension']) - 1);

				$secondaryImageThumbFileName = $this->productSecondaryImagesThumbsDir . $pathInfo['name'] . '.' . $pathInfo['extension'];
				$secondaryImageThumbFileExists = is_file($secondaryImageThumbFileName);

				/**
				 * Optimize secondary image file when needed
				 */
				if ($optimizePrimary == 'all' || ($optimizePrimary == 'new' && !$secondaryImageThumbFileExists))
				{
					$optimizer->optimize($secondaryImageFileName);
				}

				/**
				 * Generate secondary image thumbnail
				 */
				if (($generateType == 'all' || $generateType == 'secondary') && ($generateTarget == 'all' || ($generateTarget == 'new' && !$secondaryImageThumbFileExists)))
				{
					if (!ImageUtility::generateProductSecondaryThumb($secondaryImageFileName, $secondaryImageThumbFileName))
					{
						array_push($imageErrors, 'Error generating secondary image - ' . $pathInfo['basename']);
					}
				}
			}

			$secondaryPosition = $i;

			if ($secondaryPosition + 1 >= $secondaryCount)
			{
				$secondaryDone = true;
			}
		}

		/**
		 * Category images
		 */
		if ($primaryDone && $secondaryDone && !$catalogDone)
		{
			$limit = $catalogPosition + $atOnce;
			$limit = $limit > $catalogCount ? $catalogCount : $limit;

			for ($i = $catalogPosition; $i < $limit; $i++)
			{
				$categoryImageFileName = $sessionData['imagesCatalog'][$i];
				$pathInfo = pathinfo($categoryImageFileName);

				if (!isset($pathInfo['name'])) $pathInfo['name'] = substr($pathInfo['basename'], 0, strlen($pathInfo['basename']) - strlen($pathInfo['extension']) - 1);

				$categoryThumbFileName = $this->catalogThumbsDir . $pathInfo['name'] . '.' . $pathInfo['extension'];
				$categoryThumbFileExists = is_file($categoryThumbFileName);

				/**
				 * Optimize category image file when needed
				 */
				if ($optimizePrimary == 'all' || ($optimizePrimary == 'new' && !$categoryThumbFileExists))
				{
					$optimizer->optimize($categoryImageFileName);
				}

				/**
				 * Generate category image thumbnail
				 */
				if (($generateType == 'all' || $generateType == 'thumbs') && ($generateTarget == 'all' || ($generateTarget == 'new' && !$categoryThumbFileExists)))
				{
					if (!ImageUtility::generateCategoryThumb($categoryImageFileName, $categoryThumbFileName))
					{
						array_push($imageErrors, 'Error generating category thumbnail image - ' . $pathInfo['basename']);
					}
				}
			}

			$catalogPosition = $i;

			if ($catalogPosition + 1 >= $catalogCount)
			{
				$catalogDone = true;
			}
		}

		/**
		 * Manufacturer images
		 */
		if ($primaryDone && $secondaryDone && $catalogDone && !$manufacturerDone)
		{
			$limit = $manufacturerPosition + $atOnce;
			$limit = $limit > $manufacturerCount ? $manufacturerCount : $limit;

			for ($i = $manufacturerPosition; $i < $limit; $i++)
			{
				$manufacturerImageFileName = $sessionData['imagesManufacturer'][$i];
				$pathInfo = pathinfo($manufacturerImageFileName);

				if (!isset($pathInfo['name'])) $pathInfo['name'] = substr($pathInfo['basename'], 0, strlen($pathInfo['basename']) - strlen($pathInfo['extension']) - 1);

				$manufacturerThumbFileName = $this->manufacturerThumbsDir . $pathInfo['name'] . '.' . $pathInfo['extension'];
				$manufacturerThumbFileExists = is_file($manufacturerThumbFileName);

				/**
				 * Optimize manufacturer image file when needed
				 */
				if ($optimizePrimary == 'all' || ($optimizePrimary == 'new' && !$manufacturerThumbFileExists))
				{
					$optimizer->optimize($manufacturerImageFileName);
				}

				//generate thumb image
				if (($generateType == 'all' || $generateType == 'thumbs') && ($generateTarget == 'all' || ($generateTarget == 'new' && !$manufacturerThumbFileExists)))
				{
					if (!ImageUtility::generateManufacturerImage($manufacturerImageFileName, $manufacturerThumbFileName))
					{
						array_push($imageErrors, 'Error generating manufacturer image - ' . $pathInfo['basename']);
					}
				}
			}

			$manufacturerPosition = $i;

			if ($manufacturerPosition + 1 >= $manufacturerCount)
			{
				$manufacturerDone = true;
			}
		}

		if ($primaryDone && $secondaryDone && $catalogDone && $manufacturerDone)
		{
			$this->session->remove('thumb_generator');
		}
		else
		{
			$sessionData = $this->session->get('thumb_generator');

			$sessionData['primaryPosition'] = $primaryPosition;
			$sessionData['secondaryPosition'] = $secondaryPosition;
			$sessionData['catalogPosition'] = $catalogPosition;
			$sessionData['manufacturerPosition'] = $manufacturerPosition;

			$this->session->set('thumb_generator', $sessionData);
		}

		$response = array(
			'done' => $primaryDone && $secondaryDone && $catalogDone && $manufacturerDone,
			'progress' =>  round(100 / ($primaryCount + $secondaryCount + $catalogCount + $manufacturerCount) * ($primaryPosition + $secondaryPosition + $catalogPosition + $manufacturerPosition), 2),
			'progressCount' => $primaryPosition + $secondaryPosition + $catalogPosition + $manufacturerPosition,
			'totalCount' => $primaryCount + $secondaryCount + $catalogCount + $manufacturerCount,
			'rand' => rand(1000, 9999),
			'imageErrors' => $imageErrors
		);

		return $response;
	}

	/**
	 * @return int
	 */
	public function cleanupImages()
	{
		$counter = 0;

		$productIds = array();
		$this->db->query('SELECT product_id FROM ' . DB_PREFIX . 'products');

		while ($row = $this->db->moveNext())
		{
			$fileName = ImageUtility::imageFileName($row['product_id']);
			$productIds[$fileName] = true;
			unset($fileName);
		}

		unset($row);

		$counter += $this->clearProductImages($this->productImagesDir, $productIds);
		$counter += $this->clearProductImages($this->productThumbsDir, $productIds);
		$counter += $this->clearProductImages($this->productPreviewDir, $productIds);
		$counter += $this->clearProductImages($this->productSecondaryImagesDir, $productIds, true);
		$counter += $this->clearProductImages($this->productSecondaryImagesThumbsDir, $productIds, true);
		unset($productIds);

		$catalogIds = array();
		$this->db->query('SELECT cid FROM ' . DB_PREFIX . 'catalog');

		while ($row = $this->db->moveNext())
		{
			$catalogIds[$row['cid']] = true;
		}

		$counter += $this->clearCatalogImages($this->catalogThumbsDir, $catalogIds, 'category');
		unset($catalogIds);

		$manufacturerIds = array();
		$this->db->query('SELECT manufacturer_id FROM ' . DB_PREFIX . 'manufacturers');

		while ($row = $this->db->moveNext())
		{
			$manufacturerIds[$row['manufacturer_id']] = true;
		}

		$counter += $this->clearCatalogImages($this->manufacturerImagesDir, $manufacturerIds);
		$counter += $this->clearCatalogImages($this->manufacturerThumbsDir, $manufacturerIds);

		unset($manufacturerIds);

		return $counter;
	}

	/**
	 * @param $imagesPath
	 * @param $ids
	 * @param bool $trimSequenceNumber
	 * @return int
	 */
	protected function clearProductImages($imagesPath, &$ids, $trimSequenceNumber = false)
	{
		$counter = 0;
		$dir = new DirectoryIterator($imagesPath);

		/** @var DirectoryIterator $fileInfo */
		foreach ($dir as $fileInfo)
		{
			if ($fileInfo->isFile())
			{
				$ext =  strtolower(pathinfo($fileInfo->getFilename(), PATHINFO_EXTENSION));

				if ($ext == 'jpg' || $ext == 'gif' || $ext == 'png')
				{
					$filename = strtolower(pathinfo($fileInfo->getFilename(), PATHINFO_FILENAME));
					if ($trimSequenceNumber)
					{
						$pos = strrpos($filename, '--');
						if ($pos !== false)
						{
							$filename = substr($filename, 0, $pos);
						}
						else
						{
							$pos = strrpos($filename, '-');
							$filename = substr($filename, 0, $pos);
						}
					}

					if (!isset($ids[$filename]))
					{
						@unlink($imagesPath.$fileInfo->getFilename());
						$counter++;
					}
				}
			}
		}

		return $counter;
	}

	/**
	 * @param $imagesPath
	 * @param $ids
	 * @param string $prefix
	 * @return int
	 */
	protected function clearCatalogImages($imagesPath, &$ids, $prefix = '')
	{
		$counter = 0;
		$dir = new DirectoryIterator($imagesPath);

		/** @var DirectoryIterator $fileInfo */
		foreach ($dir as $fileInfo)
		{
			if ($fileInfo->isFile())
			{
				$ext =  strtolower(pathinfo($fileInfo->getFilename(), PATHINFO_EXTENSION));

				if ($ext == 'jpg' || $ext == 'gif' || $ext == 'png')
				{
					$filename = pathinfo($fileInfo->getFilename(), PATHINFO_FILENAME);
					if (trim($prefix) != '')
					{
						$filename = ltrim($filename, $prefix);
					}
					if (!isset($ids[$filename]))
					{
						@unlink($imagesPath.$fileInfo->getFilename());
						$counter++;
					}
				}
			}
		}

		return $counter;
	}

	/**
	 * @return int
	 */
	public function syncSecondaryImages()
	{
		$db = $this->db;

		// get products data
		$db->query('SELECT product_id, pid FROM ' . DB_PREFIX . 'products');
		$productsIds = array();
		while ($db->moveNext()) $productsIds[ImageUtility::imageFileName($db->col['product_id'])] = $db->col['pid'];

		// get current images
		$db->query('SELECT * FROM ' . DB_PREFIX . 'products_images');
		$productsImages = array();
		while ($db->moveNext()) $productsImages[ImageUtility::imageFileName($db->col['filename'])] = $db->col['pid'];

		$counter = 0;

		// scan folder
		$imagesSecondary = array();
		// get images from database
		$d = dir($this->productSecondaryImagesDir);

		while (false !== ($entry = $d->read()))
		{
			$filePath = $this->productSecondaryImagesDir.$entry;

			if (is_file($filePath))
			{
				$fileInfo = pathinfo(strtolower($filePath));
				$imgInfo = getimagesize($filePath);

				//check image type
				if (in_array($imgInfo[2], array(IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_JPEG)))
				{
					$filename = isset($fileInfo['filename']) ? $fileInfo['filename'] : rtrim($fileInfo['basename'], '.'.$fileInfo['extension']);
					if (!isset($productsImages[ImageUtility::imageFileName($filename)]))
					{
						//potentially new file, try to parse and find product_id for it
						$p = strrpos($filename, '-');
						if ($p > 1)
						{
							$_productId = substr($filename, 0, $p);

							if (isset($productsIds[$_productId]))
							{
								$_types = array(IMAGETYPE_GIF => 'gif', IMAGETYPE_PNG => 'png', IMAGETYPE_JPEG => 'jpg');
								
								$db->reset();
								$db->assignStr('pid', $productsIds[$_productId]);
								$db->assignStr('width', $imgInfo[0]);
								$db->assignStr('height', $imgInfo[1]);
								$db->assignStr('type', $_types[$imgInfo[2]]);
								$db->assignStr('filename', $filename);
								$db->assignStr('is_visible', 'Yes');
								$db->insert(DB_PREFIX.'products_images');

								$counter++;
							}
						}
					}
				}
			}
		}

		$d->close();

		return $counter;
	}

	/**
	 * @return DataAccess_SettingsRepository|null
	 */
	protected function getSettings()
	{
		return DataAccess_SettingsRepository::getInstance();
	}

	/**
	 * @return Admin_Service_ImageOptimizer
	 */
	protected function getImageOptimizerService()
	{
		if ($this->imageOptimizer == null)
		{
			$this->imageOptimizer = new Admin_Service_ImageOptimizer();
		}

		return $this->imageOptimizer;
	}
}
<?php

/**
 * Class Admin_Service_Download
 */
class Admin_Service_Download
{
	/** @var string filepath */
	protected $file;
	/** @var array file headers */
	protected $headers;

	/**
	 * Admin_Service_Download constructor.
	 * @param $file
	 * @param array $headers
	 */
	public function __construct($file, $headers = array())
	{
		$this->file = $file;

		$this->headers = array(
			'Content-Description' => 'File Transfer',
			'Content-Type' => 'application/octet-stream',
			'Content-Disposition' => 'attachment; filename="' . basename($this->file) . '"',
			'Expires' => '0',
			'Cache-Control' => 'must-revalidate',
			'Pragma' => 'public',
			'Content-Length' => filesize($this->file),
		);

		$this->headers = array_merge($this->headers, $headers);
	}

	public function run()
	{
		foreach ($this->headers as $key => $header)
		{
			header($key . ': ' . $header);
		}

		$handleFile = fopen($this->file, 'rb');
		$chunksize=(filesize($this->file)/1024);

		set_time_limit(0);
		while(!feof($handleFile))
		{
			echo fgets($handleFile, $chunksize);
			flush();
		}
		fclose($handleFile);
		exit();
	}

	/**
	 * @return bool
	 */
	public function isExist()
	{
		if (file_exists($this->file) && is_file($this->file))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
<?php

/**
 * Class Admin_Service_Export
 */
abstract class Admin_Service_Export
{
	protected $fields = array();
	protected $filterFields = true;

	/** @var DB $db */
	protected $db;

	protected $renderData = true;



	/**
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		$this->db = $db;
	}

	/**
	 * @return array
	 */
	public function getFieldsNames()
	{
		return array_keys($this->fields);
	}

	/**
	 * Render fields titles
	 */
	public function getFieldsTitles(array $exportFields = null)
	{
		$fieldsTitles = $this->fields;

		if (!is_null($exportFields))
		{
			$fieldsTitles = array();

			foreach ($exportFields as $fieldName)
			{
				$fieldsTitles[] = isset($this->fields[$fieldName]) ? $this->fields[$fieldName] : '';
			}
		}

		return $fieldsTitles;
	}

	/**
	 * @param array|null $options
	 * @param array|null $exportFields
	 */
	public abstract function selectData(array $options = null, array $exportFields = null);

	/**
	 * Get data from data source. It must return array ordered in exported fields order
	 * @param array|null $exportFields
	 * @return array|false
	 */
	public abstract function getData(array $exportFields = null);

	/**
	 * Get comma-separated line
	 *
	 * @param $data
	 */
	public function render($data)
	{
		$line = array();

		if ($this->filterFields)
		{
			foreach ($this->fields as $key => $value)
			{
				$line[] = isset($data[$key]) ? $this->escapeDataColumn($data[$key]) : '""';
			}
		}
		else
		{
			foreach ($data as $key => $value)
			{
				$line[$key] = $this->escapeDataColumn($value);
			}
		}

		echo implode(',', $line)."\n";
	}

	protected function escapeDataColumn($value)
	{
		return '"'.str_replace(array("\t", "\r", "\n", "\""), array("\\t", " ", " ", "\"\""), $value).'"';
	}

	/**
	 * @param string $fileName
	 * @param array $options
	 * @param array $exportFields
	 */
	public function export($fileName, array $options = null, array $exportFields = null)
	{
		header('Content-Type: text/csv');
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="'.$fileName.'"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Disposition: attachment; filename="'.$fileName.'"');

		$this->render($this->getFieldsTitles($exportFields));

		// do data select
		$this->selectData($options, $exportFields);

		while ($data = $this->getData($exportFields))
		{
			if ($this->renderData) $this->render($data);
		}

		die();
	}

	/**
	 * @param string $fileName
	 * @param array $options
	 * @param array $exportFields
	 */
	public function exportCsv($fileName, array $options = null, array $exportFields = null)
	{
		header("Pragma: public");
		header('Content-Type: text/csv');
		header('Content-Type: application/force-download');
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="'.$fileName.'"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Disposition: attachment; filename="'.$fileName.'"');

		// do data select
		$this->selectData($options, $exportFields);

		$flag = true;
		while ($data = $this->getData($exportFields))
		{
			if ($flag)
			{
				// render first line with fields names
				$this->render($this->getFieldsTitles($exportFields));
			}

			if ($this->renderData) $this->render($data);
			$flag = false;
		}

		die();
	}
}
<?php

/**
 * Class Admin_Service_Search
 */
class Admin_Service_Search
{
	/** @var DataAccess_OrderRepository  */
	protected $ordersRepository;

	/** @var DataAccess_CustomersRepository  */
	protected $customersRepository;

	/** @var DataAccess_ProductsRepository  */
	protected $productsRepository;

	protected $categoriesRepository;

	/**
	 * @param DataAccess_OrderRepository $ordersRepository
	 * @param DataAccess_CustomersRepository $customersRepository
	 * @param DataAccess_ProductsRepository $productsRepository
	 * @param DataAccess_CategoryRepository $categoriesRepository
	 */
	public function __construct(
		DataAccess_OrderRepository $ordersRepository, DataAccess_CustomersRepository $customersRepository,
		DataAccess_ProductsRepository $productsRepository, DataAccess_CategoryRepository $categoriesRepository
	)
	{
		$this->ordersRepository = $ordersRepository;
		$this->customersRepository = $customersRepository;
		$this->productsRepository = $productsRepository;
		$this->categoriesRepository = $categoriesRepository;
	}

	/**
	 * @param $searchStr
	 * @param array $options
	 * @return array
	 */
	public function search($searchStr, $options = array())
	{
		$optionsDefault = array('searchSite' => false, 'searchOrders' => true, 'searchCustomers' => true,
			'searchProducts' => true, 'searchCategories' => true, 'logic' => 'OR');

		$options = array_merge($optionsDefault, $options);

		$options['logic'] = trim(strtoupper($options['logic']));
		$options['logic'] = in_array($options['logic'], array('AND', 'OR')) ? $options['logic'] : 'OR';

		$result = array('status' => 1, 'count' => 0, 'results' => array());

		if ($searchStr != '')
		{
			//TODO: optimize multiple urlencode below
			$count = 0;
			$results = array();

			/**
			 * Search menu
			 */
			if ($options['searchSite'])
			{
				$siteMap = self::getSiteMap();
				$menuItemsCount = 0;
				$results['admin'] = array('type' => 'group', 'title' => 'Admin Area', 'count' => 0, 'items' => array());

				$siteSearchStr = str_replace(array(' a ', ' the '), array(' ', ' '), $searchStr);
				foreach ($siteMap as $sitePage)
				{
					if (strpos($sitePage['k'], $siteSearchStr) !== false)
					{
						if ($menuItemsCount < 5)
						{
							$results['admin']['items'][] = array('type' => 'item', 'url' => $sitePage['u'], 'title' => $sitePage['t']);
						}

						$menuItemsCount++;
					}
				}

				if ($menuItemsCount > 0)
				{
					$results['admin']['count'] = $menuItemsCount;
					$count += $menuItemsCount;
				}
				else
				{
					unset($results['admin']);
				}
			}

			/**
			 * Search orders
			 */
			if ($options['searchOrders'] && intval($searchStr) > 0)
			{
				$ordersSearchParams = array('order_num' => $searchStr);
				$ordersCount = $this->ordersRepository->getCount($ordersSearchParams, $options['logic']);

				if ($ordersCount)
				{
					$count += $ordersCount;
					$orders = $this->ordersRepository->getList(0, 5, 'title', $ordersSearchParams);

					$results['orders'] = array(
						'type' => 'group',
						'title' => 'Orders',
						'url' => 'admin.php?p=orders&searchParams[order_num]='.urlencode($searchStr).'&searchParams[logic]='.$options['logic'],
						'count' => $ordersCount,
						'items' => array()
					);

					foreach ($orders as $order)
					{
						$results['orders']['items'][] = array(
							'type' => 'item',
							'url' => 'admin.php?p=order&id='.$order['oid'],
							'title' => 'Order #'.$order['order_num']
						);
					}
				}
			}

			/**
			 * Search customers
			 */
			if ($options['searchCustomers'])
			{
				$customersSearchParams = array(
					'fname' => $searchStr, 'lname' => $searchStr, 'email' => $searchStr, 'login' => $searchStr, 'phone' => $searchStr, 'company' => $searchStr
				);

				$customersCount = $this->customersRepository->getCustomersCount($customersSearchParams, $options['logic']);

				if ($customersCount > 0)
				{
					$count += $customersCount;
					$customers = $this->customersRepository->getCustomersList(0, 5, 'lname', $customersSearchParams, $options['logic']);
					$results['customers'] = array(
						'type' => 'group',
						'title' => 'Customers',
						'url' => 'admin.php?p=customers'.
							'&searchParams[fname]='.urlencode($searchStr).'&searchParams[lname]='.urlencode($searchStr).
							'&searchParams[email]='.urlencode($searchStr).'&searchParams[login]='.urlencode($searchStr).
							'&searchParams[phone]='.urlencode($searchStr).'&searchParams[company]='.urlencode($searchStr).
							'&searchParams[logic]='.$options['logic'],
						'count' => $customersCount,
						'items' => array()
					);

					foreach ($customers as $customer)
					{
						$results['customers']['items'][] = array(
							'type' => 'item',
							'url' => 'admin.php?p=customer&mode=update&id='.$customer['uid'],
							'title' => $customer['fname'].' '.$customer['lname'].' / '.$customer['email']
						);
					}
				}
			}

			/**
			 * Search products
			 */
			if ($options['searchProducts'])
			{
				$productsSearchParams = array('product_id' => $searchStr, 'title' => $searchStr);
				$productsCount = $this->productsRepository->getCount($productsSearchParams, $options['logic']);

				if ($productsCount > 0)
				{
					$count += $productsCount;
					$products = $this->productsRepository->getList(0, 5, 'title', $productsSearchParams, 'p.pid, p.product_id, p.title', $options['logic']);

					$results['products'] = array(
						'type' => 'group',
						'title' => 'Products',
						'url' => 'admin.php?p=products&searchParams[product_id]='.urlencode($searchStr).'&searchParams[title]='.urlencode($searchStr).'&searchParams[logic]='.$options['logic'],
						'count' => $productsCount,
						'items' => array()
					);

					foreach ($products as $product)
					{
						$results['products']['items'][] = array(
							'type' => 'item',
							'url' => 'admin.php?p=product&mode=update&id='.$product['pid'],
							'title' => $product['product_id'].' - '.$product['title']
						);
					}
				}
			}

			/**
			 * Search categories
			 */
			if ($options['searchCategories'])
			{
				$categoriesSearchParams = array('key_name' => $searchStr, 'name' => $searchStr);
				$categoriesCount = $this->categoriesRepository->getCount($categoriesSearchParams, $options['logic']);

				if ($categoriesCount > 0)
				{
					$count += $categoriesCount;
					$categories = $this->categoriesRepository->getList(0, 5, 'title', $categoriesSearchParams, 'c.cid, c.key_name, c.name', $options['logic']);

					$results['categories'] = array(
						'type' => 'group',
						'title' => 'Categories',
						'count' => $categoriesCount,
						'items' => array()
					);

					foreach ($categories as $category)
					{
						$results['categories']['items'][] = array(
							'type' => 'item',
							'url' => 'admin.php?p=category&mode=update&id='.$category['cid'],
							'title' => $category['key_name'].' - '.$category['name']
						);
					}
				}
			}

			$result['count'] = $count;
			$result['results'] = $results;
		}

		return $result;
	}

	/**
	 * @return array
	 */
	public static function getSiteMap()
	{
		return array(
			array('u' => '?p=orders', 't' => 'Orders', 'k' => 'orders,manage orders'),
			array('u' => '?p=customers', 't' => 'Customers', 'k' => 'customers,users,clients,manage customers,manage clients,manage users'),
			array('u' => '?p=products', 't' => 'Products', 'k' => 'products,manage products'),
			array('u' => '?p=bulk_products', 't' => 'Import Products', 'k' => 'import products,bulk import products,bulk uploader,products bulk uploader,products loader'),
			array('u' => '?p=product&mode=insert', 't' => 'Add Product', 'k' => 'add product,new product,insert product,create product,new product'),
			array('u' => '?p=categories', 't' => 'Categories', 'k' => 'categories,manage categories'),
			array('u' => '?p=bulk_categories', 't' => 'Import Categories', 'k' => 'import categories,bulk import categories,bulk categories uploader,categories bulk uploader,categories loader'),
			array('u' => '?p=category&mode=insert', 't' => 'Add Category', 'k' => 'add category,new category,insert category,create category,new category'),
			array('u' => '?p=manufacturers', 't' => 'Manufacturers', 'k' => 'manufacturers,manage manufacturers'),
			array('u' => '?p=bulk_manufacturers', 't' => 'Import Manufacturers', 'k' => 'import manufacturers,bulk import manufacturers,bulk manufacturers uploader,manufacturers bulk uploader,manufacturers loader'),
			array('u' => '?p=manufacturer&mode=add', 't' => 'Add Manufacturer', 'k' => 'add manufacturers,new manufacturers,insert manufacturers,create manufacturers,new manufacturers'),
			array('u' => '?p=products_attributes', 't' => 'Global Product Attributes', 'k' => 'global attributes'),
			array('u' => '?p=reports', 't' => 'Reports', 'k' => 'reports,statistics,stats'), // TODO: add all reports by types?
			array('u' => '?p=promotions', 't' => 'Promotions', 'k' => 'promotions,promo codes,promotional codes,discount codes'),
			array('u' => '?p=products_families', 't' => 'Recommended Products', 'k' => 'recommended products,products families'),
			array('u' => '?p=google_marketing', 't' => 'Google Tools', 'k' => 'google tools,google analytics,google adwords,google sitemap,generate sitemap'),
			array('u' => '?p=drift_marketing', 't' => 'Drift Marketing', 'k' => 'drift marketing campaigns'),
			array('u' => '?p=drift_marketing&mode=add', 't' => 'Add Email Campaign', 'k' => 'add email campaign,add drift marketing campaign,create drift marketing campaign'), // TODO: need it here?
			array('u' => '?p=gift_cert', 't' => 'Gift Certificates', 'k' => 'gift certificates,gifts certificates,gift certs'),
			array('u' => '?p=gift_cert&mode=add', 't' => 'Add Gift Certificate', 'k' => 'add gift certificate,new gift certificate,new gift cert,create gift certificate'),
			array('u' => '?p=settings_qr', 't' => 'QR Codes', 'k' => 'qr codes,qr campaigns,add qr campaign,create qr campaign,new qr campaign'),
			array('u' => '?p=settings_search', 't' => 'Search Engine Optimization', 'k' => 'seo,search engine optimization,change seo settings,google,bing,yahoo'),
			array('u' => '?p=settings_social', 't' => 'Social Sharing', 'k' => 'social sharing,socila networks,facebook,twitter,pinterest,google plus,delicious'),
			array('u' => '?p=settings_bestsellers', 't' => 'Bestsellers Settings', 'k' => 'bestseller settings,bestsellers settings'),
			array('u' => '?p=pages', 't' => 'Pages', 'k' => 'pages,text page,text pages'),
			array('u' => '?p=page&mode=add', 't' => 'Add Page', 'k' => 'add page,create page,new page,new text page,add text page,create text page'),
			array('u' => '?p=testimonials', 't' => 'Testimonials', 'k' => 'testimonials,manage testimonials'),
			array('u' => '?p=testimonial&mode=add', 't' => 'Add Testimonial', 'k' => 'add testimonials,new testimonials,create testimonial'),
			array('u' => '?p=products_reviews', 't' => 'Product Reviews', 'k' => 'products reviews,manage products reviews,manage reviews'),
			array('u' => '?p=design_settings', 't' => 'Change Theme', 'k' => 'theme,skin,view,design,change theme'),
			array('u' => '?p=design_catalog', 't' => 'Catalog Settings', 'k' => 'catalog settings,price range filters,home page settings,product page settings,category image settings,product image settings,manufacture image settings'),
			array('u' => '?p=apps&action=app-center', 't' => 'App', 'k' => 'google,stamps.com,avalara,exactor,billmelater,facebook,idevaffiliate,mailchimp,intuit quickbooks,webgility,doba,amazon,yahoo,ebay,goshopping,pricegrabber,nextag,shopzilla,wholesale central,bongo checkout,bongo extend,endicia,recurring billing'),
			array('u' => '?p=settings', 't' => 'Setup Your Store', 'k' => 'settings,store settings'),
			array('u' => '?p=store_info', 't' => 'Store Information', 'k' => 'store information,store address,company address,company name,change address,update address'),
			array('u' => '?p=payment_methods', 't' => 'Payments', 'k' => 'payments,payment methods,authorize.net,ccs,creadit card storage,2checkout,amazon,american express,bongo,chase,first data global gateway,future pay,futurepay,intuit payment services,itransact.com,merchant anywhere,transaction central,netbilling,net billing,payjunction,payleap,paypal express checkout,paypal payflow link,paypal payflow pro,paypal payments advanced,paypal payments pro,paypal payments standard,psigate.com,shift4,stripe,usaepay,virtual merchant,paypal website payments pro hosted solution,paypal website payments standard,protx.com,beanstream,moneris.com,paypal express checkout,paypal payflow link,paypal payflow pro,skrill,worldpay,add custom payment method'),
			array('u' => '?p=shipping', 't' => 'Shipping', 'k' => 'shipping settings,manage shipping,domestic shipping,international shipping,ups,usps,fedex,canadapost,canada post,flat shipping'),
			array('u' => '?p=shipping&mode=settings', 't' => 'Advanced Shipping Settings', 'k' => 'advanced shipping settings,shipping origin address,handling settings,shipping quote,address validation,shipping tax'),
			array('u' => '?p=taxes', 't' => 'Taxes', 'k' => 'taxes settings,tax settings,taxes setup,tax setup,manage taxes,change taxes,update taxes'),
			array('u' => '?p=taxes_rate&mode=add', 't' => 'Add Tax Rate ', 'k' => 'add tax rate,new tax rate,insert tax rate,create tax rate'),
			array('u' => '?p=taxes_classes', 't' => 'Taxes Classes', 'k' => 'manage tax classes,manage taxes classes,add tax class,new tax class,update tax classes,update taxes classes'),
			array('u' => '?p=currencies', 't' => 'Currencies', 'k' => 'currency,currency code,dollar,pound,yen,euro,koruna,zloty,exchange rate'),
			array('u' => '?p=languages', 't' => 'Languages', 'k' => 'language,translation,text changes,change text,internationalization,add a new language,add new language,translate,add language'),
			array('u' => '?p=settings&action=advanced', 't' => 'Advanced Settings', 'k' => 'advanced settings,under the hood,customization,store settings'),
			array('u' => '?p=store_settings', 't' => 'Store Settings', 'k' => 'store settings,email settings,security settings,localization settings,captcha settings,recaptcha settings,cookie settings'),
			array('u' => '?p=checkout', 't' => 'Checkout Settings', 'k' => 'checkout settings,stock,fields settings'),
			array('u' => '?p=order_notifications', 't' => 'Order Notifications', 'k' => 'order notifications settings,add order notification'),
			array('u' => '?p=settings_invoice', 't' => 'Printable Invoice', 'k' => 'printable invoice settings,print settings,printing settings'),
			array('u' => '?p=filemanager', 't' => 'File Manager', 'k' => 'file manager,filemanager,manage files,edit file,delete file,upload file'),
			array('u' => '?p=email_notifications', 't' => 'Notification Emails', 'k' => 'notification emails,edit emails,change emails'),
			array('u' => '?p=admins', 't' => 'Administrators', 'k' => 'administrators,admins,add new administrator,change administrator'),
			array('u' => '?p=custom_forms', 't' => 'Custom Forms', 'k' => 'manage custom forms,add custom forms,new custom form','custom forms'),
			array('u' => '?p=forms_control&mode=customfieldslist', 't' => 'Custom Fields', 'k' => 'manage custom fields,add custom field,new custom field'),
			array('u' => '?p=admins_log', 't' => 'Admin Logs', 'k' => 'admin logs,administrator logs,admin access log'),
			array('u' => '?p=api', 't' => 'API Access', 'k' => 'api access settings,cart api'),
			array('u' => '?p=countries', 't' => 'Countries', 'k' => 'manage countries,country,regions,add region,states,provinces,add country,change country name'),
			array('u' => '?p=settings_ccs', 't' => 'Credit Card Storage ', 'k' => 'credit card storage,store credit cards'),
			array('u' => '?p=settings_wholesale', 't' => 'Wholesale Settings', 'k' => 'wholesale settings,wholesellers'),
			array('u' => '?p=settings&action=advanced', 't' => 'Backup / Restore', 'k' => 'backup,restore,backup database,copy site'),
			array('u' => '?p=profile', 't' => 'Update Your Profile', 'k' => 'update your profile,edit profile,change password,update password,change email,update profile'),
			array('u' => '?p=billing', 't' => 'Billing', 'k' => 'billing and payment,cancel service'),
			array('u' => '?p=billing&mode=cart-information', 't' => 'Cart Information', 'k' => 'cart information,license information,refresh license,update license,cart details'),
			array('u' => '?p=billing&mode=support', 't' => 'Support', 'k' => 'support ticket,help,contact support,submit ticket'),
			array('u' => '?p=thumbs_generator', 't' => 'Thumbnail Generator', 'k' => 'thumbnail generator,cleanup images,remove images'),
			array('u' => '?p=order_forms', 't' => 'Order Forms', 'k' => 'order forms,forms,manage order forms'),
			array('u' => '?p=order_form&mode=add', 't' => 'Add Order Form', 'k' => 'add order form,new order forms,insert order forms,create order forms'),
			array('u' => '?p=snippets', 't' => 'Snippets', 'k' => 'snippet,snippets,css snippet, javascript snippet'),
			//		array('u' => '', 't' => '', 'k' => ''),
			//		array('u' => '', 't' => '', 'k' => ''),
			//		array('u' => '', 't' => '', 'k' => ''),
			//		array('u' => '', 't' => '', 'k' => ''),
			// TODO: finish list
		);
	}
}
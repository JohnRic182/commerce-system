<?php
/**
 * Class Admin_Service_FileUploader
 */
class Admin_Service_FileUploader
{
	/** @var array $imageExtensions */
	protected $imageExtensions = array(
		IMAGETYPE_JPEG => 'jpg',
		IMAGETYPE_GIF => 'gif',
		IMAGETYPE_PNG => 'png'
	);

	/**
	 * @param $path
	 * @param $fileName
	 * @param $extension
	 * @return string
	 */
	public function buildFilePath($path, $fileName, $extension = null)
	{
		if (substr($path, -1) != '/') $path .= '/';
		return $path.$fileName.(!is_null($extension) ? '.'.$extension : '');
	}

	/**
	 * @param $fileUploadInfo
	 * @param $path
	 * @param null $fileName
	 * @return bool|string
	 */
	public function processFileUpload($fileUploadInfo, $path, $fileName = null)
	{
		if (is_null($fileName)) $fileName = $fileUploadInfo['name'];

		$filePath = $this->buildFilePath($path, $fileName);

		if (is_file($filePath) && !unlink($filePath)) return false;

		if (move_uploaded_file($fileUploadInfo['tmp_name'], $filePath))
		{
			return $filePath;
		}

		return false;
	}

	/**
	 * Delete image with the same filename to avoid same filename with different extension (fix on category & manufacturer issue)
	 *
	 * @param $path
	 * @param $fileName
	 * @return bool
	 */
	public function deleteSameFilename($path, $fileName)
	{
		foreach ($this->imageExtensions as $tmpExt)
		{
			$tmpFilePath = $this->buildFilePath($path, $fileName, $tmpExt);
			if (is_file($tmpFilePath) && !unlink($tmpFilePath))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * @param $imageUploadInfo
	 * @param $path
	 * @param null $fileName
	 * @return bool
	 */
	public function processImageUpload($imageUploadInfo, $path, $fileName = null, $deleteFilesWithTheSameName = false)
	{
		if (is_null($fileName)) $fileName = $imageUploadInfo['name'];

		list($width, $height, $type, $attr) = getimagesize($imageUploadInfo['tmp_name']);

		if (in_array($type, array(IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_PNG)))
		{
			$filePath = $this->buildFilePath($path, $fileName, $this->imageExtensions[$type]);

			if (is_file($filePath) && !unlink($filePath)) return false;

			if ($deleteFilesWithTheSameName)
			{
				if (!$this->deleteSameFilename($path, $fileName))
				{
					return false;
				}
			}

			if (move_uploaded_file($imageUploadInfo['tmp_name'], $filePath))
			{
				return $filePath;
			}
		}

		return false;
	}

	/**
	 * Move uploaded file
	 *
	 * @param $fileInfo
	 * @param $path
	 * @param $fileName
	 * @return bool|string
	 */
	public function moveUploaderFile($fileInfo, $path, $fileName)
	{
		$filePath = $this->buildFilePath($path, $fileName);

		if (move_uploaded_file($fileInfo['tmp_name'], $filePath))
		{
			return $filePath;
		}

		return false;
	}
}
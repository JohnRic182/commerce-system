<?php

/**
 * Class Admin_Service_Billing
 */
class Admin_Service_Billing extends Admin_Service_PinnacleConnect
{
	protected $baseUrlProduction = 'https://account.pinnaclecart.com/billing-api/';
	protected $baseUrlTest = 'https://account-staging.pinnaclecart.com/billing-api/';

	/** @var DataAccess_CountriesStatesRepository  */
	protected $countryStateRepository;

	/**
	 * @param DataAccess_SettingsRepository $settings
	 * @param DataAccess_CountriesStatesRepository $countryStateRepository
	 * @param bool $isTest
	 */
	public function __construct(DataAccess_SettingsRepository $settings, DataAccess_CountriesStatesRepository $countryStateRepository, $isTest = false)
	{
		parent::__construct($settings, $isTest);

		$this->countryStateRepository = $countryStateRepository;
	}

	/**
	 * Get account summary
	 *
	 * @return array|mixed
	 */
	public function getAccountSummary()
	{
		$params = array('license_id' => $this->getLicenseId());
		$this->hashParams($params);

		return @json_decode($this->doGet('summary', $params));
	}

	/**
	 * Get profile
	 *
	 * @return array|mixed|stdClass
	 */
	public function getProfile()
	{
		$profile = @json_decode($this->doGet('profile'));
		if (!$profile || !isset($profile->firstName))
		{
			$profile = new stdClass();
			$profile->firstName = '';
			$profile->lastName = '';
			$profile->company = '';
			$profile->address1 = '';
			$profile->address2 = '';
			$profile->city = '';
			$profile->state = '';
			$profile->country = 'US';
			$profile->zip = '';
			$profile->phone = '';
			$profile->email = '';
			$profile->cards = array();
		}

		return $profile;
	}

	/**
	 * @param $data
	 *
	 * @return mixed
	 */
	public function normalizePlaceOrderFormData($data)
	{
		$countryStateRepository = $this->countryStateRepository;

		if (isset($data['profile']['country']))
		{
			$countryData = $countryStateRepository->getCountryByIso2Code($data['profile']['country']);

			if ($countryData)
			{
				if ($countryStateRepository->getStatesCountByCountryId($countryData['coid']) > 0)
				{
					// unset province - deal with state
					unset($data['profile']['province']);

					if (isset($data['profile']['state']))
					{
						$stateData = $countryStateRepository->getStateByCode($countryData['coid'], $data['profile']['state']);
						if (!$stateData)
						{
							$data['profile']['state'] = '';
						}
					}
				}
				else
				{
					$data['profile']['state'] = isset($data['profile']['province']) ? $data['profile']['province'] : '';
				}
			}
			else
			{
				$data['profile']['country'] = '';
			}
		}

		return $data;
	}

	/**
	 * @param $data
	 * @return array|bool
	 */
	public function getPlaceOrderValidationErrors($data)
	{
		$errors = array();

		if (!isset($data['profile']['firstName']) || trim($data['profile']['firstName']) == '') $errors[] = array('field' => 'profile[firstName]', 'message' => 'Please enter first name');
		if (!isset($data['profile']['lastName']) || trim($data['profile']['lastName']) == '') $errors[] = array('field' => 'profile[lastName]', 'message' => 'Please enter last name');
		if (!isset($data['profile']['address1']) || trim($data['profile']['address1']) == '') $errors[] = array('field' => 'profile[address1]', 'message' => 'Please enter address line 1');
		if (!isset($data['profile']['city']) || trim($data['profile']['city']) == '') $errors[] = array('field' => 'profile[city]', 'message' => 'Please enter city name');

		if (!isset($data['profile']['state']) || trim($data['profile']['state']) == '')
		{
			$errors[] = array('field' => 'profile['.(isset($data['profile']['province']) ? 'province' : 'state').']', 'message' => (isset($data['profile']['province']) ? 'province' : 'state') ? 'Please enter province name' : 'Please select state');
		}
		if (!isset($data['profile']['country']) || trim($data['profile']['country']) == '') $errors[] = array('field' => 'profile[country]', 'message' => 'Please choose country');
		if (!isset($data['profile']['zip']) || trim($data['profile']['zip']) == '') $errors[] = array('field' => 'profile[zip]', 'message' => 'Please enter zip / postal code');
		if (!isset($data['profile']['phone']) || trim($data['profile']['phone']) == '') $errors[] = array('field' => 'profile[phone]', 'message' => 'Please enter phone number');
		if (!isset($data['profile']['email']) || trim($data['profile']['email']) == '') $errors[] = array('field' => 'profile[email]', 'message' => 'Please enter email address');

		if (!isset($data['domain']['domain_name']) || trim($data['domain']['domain_name']) == '')
		{
			$errors[] = array('field' => 'domain[domain_name]', 'message' => 'Please enter domain name');
		}
		else
		{
			$matches = array();
			$pregResult = preg_match('/(?=^.{4,255}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}$)/', $data['domain']['domain_name'], $matches);
			if (!$pregResult || count($matches) == 0)
			{
				$errors[] = array('field' => 'domain[domain_name]', 'message' => 'Please enter valid domain name');
			}
		}

		if (isset($data['cc']['paymentProfileUsed']) && $data['cc']['paymentProfileUsed'] == '1')
		{
			if (!isset($data['cc']['paymentProfileId']) || trim($data['cc']['paymentProfileId']) == '')
			{
				$errors[] = array('field' => 'cc[paymentProfileId]', 'message' => 'Payment profile is not selected');
			}
		}
		else
		{
			if (!isset($data['cc']['cardNumber']) || trim($data['cc']['cardNumber']) == '') $errors[] = array('field' => 'cc[cardNumber]', 'message' => 'Credit card number is required');
			if (!isset($data['cc']['cvv']) || trim($data['cc']['cvv']) == '') $errors[] = array('field' => 'cc[cvv]', 'message' => 'CVV Code is required');
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Get purchase form
	 *
	 * @param $product
	 * @param $params
	 *
	 * @return array|mixed
	 */
	public function getPurchaseForm($product, $params = array())
	{
		$params = array_merge(array(
			'product' => $product,
			'license_id' => $this->getLicenseId(),
			'license_number' => LICENSE_NUMBER,
			'hostname' => $this->settings->get('AdminHttpsUrl'),
		), $params);

		$this->hashParams($params);

		return @json_decode($this->doPost('purchase-form', $params));
	}

	/**
	 * Get checkout data
	 *
	 * @param $product
	 * @param array $params
	 * @param $orderSecurityId
	 *
	 * @return array|mixed
	 */
	public function getCheckoutData($product, $params = array(), $orderSecurityId = null)
	{
		$params = array_merge(array(
			'product' => $product,
			'license_id' => $this->getLicenseId(),
			'license_number' => LICENSE_NUMBER,
			'hostname' => $this->settings->get('AdminHttpsUrl')
		), $params);

		if ($orderSecurityId != null) $params['order_security_id'] = $orderSecurityId;

		$this->hashParams($params);

		return @json_decode($this->doPost('checkout-update', $params));
	}

	/**
	 * Place order
	 *
	 * @param $product
	 * @param array $params
	 * @param $orderSecurityId
	 *
	 * @return array|mixed
	 */
	public function placeOrder($product, $params = array(), $orderSecurityId = null)
	{
		$params = array_merge(array(
			'product' => $product,
			'license_id' => $this->getLicenseId(),
			'license_number' => LICENSE_NUMBER,
			'hostname' => $this->settings->get('AdminHttpsUrl'),
		), $params);

		if ($orderSecurityId != null) $params['order_security_id'] = $orderSecurityId;

		if (isset($params['cc']['paymentProfileUsed']) && $params['cc']['paymentProfileUsed'] == '1')
		{
			unset($params['cc']['cardNumber'], $params['cc']['expiration'], $params['cc']['expiration'], $params['cc']['cvv']);
		}
		else
		{
			unset($params['cc']['paymentProfileUsed'], $params['cc']['paymentProfileId']);
		}

		$this->hashParams($params);

		return @json_decode($this->doPost('place-order', $params));
	}

	/**
	 * Subscribe to a service - note - cart will be charged manually
	 *
	 * @param $extraId
	 * @param array $params
	 * @return mixed
	 */
	public function subscribe($extraId, $params = array())
	{
		$params = array_merge(array(
			'extra_id' => $extraId,
			'license_id' => $this->getLicenseId(),
			'license_number' => LICENSE_NUMBER,
			'cart_url' => $this->settings->get('GlobalHttpUrl'),
		), $params);

		$this->hashParams($params);

		return @json_decode($this->doPost('subscribe', $params), true);
	}

	/**
	 * Refresh license
	 */
	public function refreshLicense()
	{
		// TODO: get rid of global dependency here
		global $iono;

		unlink('content/cache/license/license.key');

		if (is_object($iono))
		{
			$iono->iono_keys_call4520(trim(LICENSE_NUMBER), 'b575796b4295', 'content/cache/license/license.key', 604800); //172800 - 2 days

			if ($iono->status == '') $iono->get_status_1980();

			if ($iono->result != 1)
			{
				echo '<script type="text/javascript">document.location="login.php";</script>';
				die();
			}
		}
	}

	/**
	 * Submit cancel
	 *
	 * @param $serviceLength
	 * @param $mainReason
	 *
	 * @return array|mixed
	 */
	public function submitCancellation($serviceLength, $mainReason)
	{
		$params = array(
			'service_length' => $serviceLength,
			'main_reason' => $mainReason,
			'license_id' => $this->getLicenseId(),
			'license_number' => LICENSE_NUMBER,
			'hostname' => $this->settings->get('AdminHttpsUrl'),
		);

		$this->hashParams($params);

		return @json_decode($this->doPost('submit-cancel', $params));
	}

	/**
	 * Update profile
	 *
	 * @param $firstName
	 * @param $lastName
	 * @param $company
	 * @param $address1
	 * @param $address2
	 * @param $city
	 * @param $state
	 * @param $country
	 * @param $zip
	 * @param $phone
	 * @param $email
	 *
	 * @return array|mixed
	 */
	public function updateProfile(
		$firstName, $lastName, $company, $address1, $address2,
		$city, $state, $country, $zip, $phone, $email
	)
	{
		$params = array(
			'firstName' => $firstName,
			'lastName' => $lastName,
			'company' => $company,
			'address1' => $address1,
			'address2' => $address2,
			'city' => $city,
			'state' => $state,
			'country' => $country,
			'zip' => $zip,
			'phone' => $phone,
			'email' => $email,
		);

		if (defined('TRIAL_TOKEN'))
		{
			$params['trial_token'] = TRIAL_TOKEN;
		}

		$this->hashParams($params);

		return @json_decode($this->doPost('update-profile', $params));
	}

	/**
	 * Update card
	 *
	 * @param $ccNum
	 * @param $ccExpMonth
	 * @param $ccExpYear
	 * @param $ccCvv2
	 *
	 * @return array|mixed
	 */
	public function updateCard($ccNum, $ccExpMonth, $ccExpYear, $ccCvv2)
	{
		$params = array(
			'cc_num' => $ccNum,
			'cc_exp_month' => $ccExpMonth,
			'cc_exp_year' => $ccExpYear,
			'cc_cvv2' => $ccCvv2,
		);

		$this->hashParams($params);

		return @json_decode($this->doPost('card', $params));
	}
}
<?php

/**
 * Class Admin_Service_CategoryImport
 */
class Admin_Service_ProductImport extends Admin_Service_Import
{
	/** @var array $fields */
	protected $fields = array(
		'__skip' => array(
			'title' => 'Skip', 'type' => '',
			'required' => false, 'autoAssign' => array()
		),
		'product_id' => array(
			'title' => 'Product ID', 'type' => self::TYPE_STRING,
			'required' => true, 'autoAssign' => array('productid', 'id', 'code', 'productkey', 'productcode')
		),
		'is_visible' => array(
			'title' => 'Available', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'Yes', 'autoAssign' => array('visible', 'isvisible', 'active', 'isactive', 'available', 'isavailable')
		),
		'product_type' => array(
			'title' => 'Product Type', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => 'Tangible', 'autoAssign' => array('type', 'producttype')
		),
		'product_sku' => array(
			'title' => 'SKU (Stock Keeping Unit)', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('productsku', 'sku', 'stockkeepingunit')
		),
//		'product_upc' => array(
//			'title' => 'UPC code', 'type' => self::TYPE_STRING,
//			'required' => false, 'default' => '', 'autoAssign' => array('upc', 'productupc', 'upccode')
//		),
		'product_gtin' => array(
			'title' => 'Barcode', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('gtin', 'globaltradeitemnumber', 'upc', 'productupc', 'upccode', 'barcode')
		),
		'product_mpn' => array(
			'title' => 'MPN', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('mpn', 'manufacturerpartnumber')
		),
		'title' => array(
			'title' => 'Product name', 'type' => self::TYPE_STRING,
			'required' => true, 'autoAssign' => array('producttitle', 'title', 'productname', 'name')
		),
		'price' => array(
			'title' => 'Product price', 'type' => self::TYPE_MONEY,
			'required' => true, 'autoAssign' => array('productprice', 'price', 'itemprice')
		),
		'category' => array(
			'title' => 'Categories', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('parent', 'category', 'parentkey', 'parentcode', 'parentcategoryid', 'parentcategorycode', 'parentcategorykey', 'categorykey', 'categorykeys', 'categories')
		),
		'manufacturer' => array(
			'title' => 'Manufacturer ID', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('manufacturer', 'manufacturerid', 'manufacturerkey', 'manufacturercode')
		),
		'manufacturer_name' => array(
			'title' => 'Manufacturer name', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('manufacturername')
		),
		'location' => array(
			'title' => 'Location code', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('location', 'productlocation', 'itemlocation', 'locationcode')
		),
		'is_locked' => array(
			'title' => 'Is locked', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('locked', 'lock', 'islocked', 'productlocked', 'lockproduct')
		),
		'lock_fields' => array(
			'title' => 'Lock fields', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('locks', 'lockfields', 'readonly')
		),
		'meta_title' => array(
			'title' => 'Meta title', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('metatitle')
		),
		'meta_description' => array(
			'title' => 'Meta description', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('metadescription')
		),
		'url_custom' => array(
			'title' => 'Custom URL', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('url', 'customurl', 'urlcustom', 'customproducturl')
		),
		'priority' => array(
			'title' => 'Priority', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '5', 'autoAssign' => array('priority', 'sortorder', 'productpriority', 'producsortorder', 'sortingorder', 'sortingpriority', 'producsortingorder', 'producsortingpriority')
		),
		'reviews_count' => array(
			'title' => 'Reviews Count', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('reviewscount')
		),
		'rating' => array(
			'title' => 'Rating', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('rating')
		),
		'url_default' => array(
			'title' => 'URL default', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('urldefault')
		),
		'call_for_price' => array(
			'title' => 'Call for price', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('call', 'callforprice')
		),
		'is_taxable' => array(
			'title' => 'Is taxable', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('istaxable', 'taxable')
		),
		'tax_class_name' => array(
			'title' => 'Tax class name', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('taxclassname')
		),
		'tax_class_id' => array(
			'title' => 'Tax class key', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('taxclass', 'taxclassid', 'taxclasskey')
		),
		'location_name' => array(
			'title' => 'Location name', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '','autoAssign' => array('locationname')
		),
		'avalara_tax_code' => array(
			'title' => 'Avalara tax code', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '','autoAssign' => array('avalarataxcode')
		),
		'exactor_euc_code' => array(
			'title' => 'Exactor EUC code', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('exactoreuccode')
		),
		'price2' => array(
			'title' => 'Sale price', 'type' => self::TYPE_MONEY,
			'required' => false, 'default' => '', 'autoAssign' => array('saleprice', 'sale')
		),
		'price_level_1' => array(
			'title' => 'Price level 1', 'type' => self::TYPE_MONEY,
			'required' => false, 'default' => '0.00', 'autoAssign' => array('pricelevel1')
		),
		'price_level_2' => array(
			'title' => 'Price level 2', 'type' => self::TYPE_MONEY,
			'required' => false, 'default' => '0.00', 'autoAssign' => array('pricelevel2')
		),
		'price_level_3' => array(
			'title' => 'Price level 3', 'type' => self::TYPE_MONEY,
			'required' => false, 'default' => '0.00', 'autoAssign' => array('pricelevel3')
		),
		'free_shipping' => array(
			'title' => 'Free shipping', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('freeshipping', 'shippingisfree')
		),
		'weight' => array(
			'title' => 'Unit weight', 'type' => self::TYPE_NUMBER,
			'required' => false, 'default' => '0.00', 'autoAssign' => array('weight', 'productweight', 'itemweight')
		),
		'min_order' => array(
			'title' => 'Min order', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('minorder', 'min')
		),
		'max_order' => array(
			'title' => 'Max order', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('maxorder', 'max')
		),
		'is_home' => array(
			'title' => 'On home page', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('ishome', 'onhomepage', 'showonhomepage')
		),
		'is_hotdeal' => array(
			'title' => 'Is hotdeal', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('ishotdeal', 'hotdeal')
		),
		'is_doba' => array(
			'title' => 'Doba product', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('doba', 'isdoba', 'dobaproduct', 'isdobaproduct')
		),
		'digital_product' => array(
			'title' => 'Is digital product', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('digital', 'isdigital', 'digitalproduct', 'downloadable')
		),
		'digital_product_file' => array(
			'title' => 'Digital product file', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('digitalproductfile', 'digitalfile', 'digitalfilename', 'downloadfile', 'downloadfilename')
		),
		'inventory_control' => array(
			'title' => 'Track inventory', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => 'No', 'autoAssign' => array('inventorycontrol', 'trackinventory'),
			'accessRequired' => 'INVENTORY'
		),
		'stock' => array(
			'title' => 'Stock', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('stock', 'itemsinstock'),
			'accessRequired' => 'INVENTORY'
		),
		'stock_warning' => array(
			'title' => 'Stock warning', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('minstock', 'stockwarning'),
			'accessRequired' => 'INVENTORY'
		),
		'inventory_rule' => array(
			'title' => 'Inventory rule', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => 'Hide', 'autoAssign' => array('inventoryrule'),
			'accessRequired' => 'INVENTORY'
		),
		'overview' => array(
			'title' => 'Overview', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('overview', 'productoverview', 'quickoverview')
		),
		'description' => array(
			'title' => 'Description', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('productdescription', 'description')
		),
		'search_keywords' => array(
			'title' => 'Search Keywords', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('searchkeywords', 'keywords', 'metakeywords')
		),
		'image_url' => array(
			'title' => 'Image URL', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('imageurl', 'image')
		),
		'image_location' => array(
			'title' => 'Image location', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('imagelocation')
		),
		'image_alt_text' => array(
			'title' => 'Image alt text', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('imagealttext', 'imagetext')
		),
		'product_level_shippings' => array(
			'title' => 'Product Level Shipping', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('productlevelshipping')
		),
		'product_sub_id' => array(
			'title' => 'Product sub ID', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('productsubid', 'subid')
		),
		'product_sub_sku' => array(
			'title' => 'Product sub SKU', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('productsubsku', 'subsku')
		),
		'product_sub_id_stock' => array(
			'title' => 'Items in stock on attr. level', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('itemsinstockonattrlevel', 'stockonattributelevel'),
			'accessRequired' => 'INVENTORY'
		),
		'product_sub_options' => array(
			'title' => 'Sub product attributes', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('subproductattributes')
		),
		'google_item_condition' => array(
			'title' => 'Google base item condition', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebaseitemcondition')
		),
		'google_product_type' => array(
			'title' => 'Google base product category', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebaseproductcategory')
		),
		'google_availability' => array(
			'title' => 'Google base availability', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebaseavailability')
		),
		'google_online_only' => array(
			'title' => 'Google base online only', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => 'n', 'autoAssign' => array('googlebaseonlineonly')
		),
		'google_gender' => array(
			'title' => 'Google base gender', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebasegender')
		),
		'google_age_group' => array(
			'title' => 'Google base age group', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebaseagegroup')
		),
		'google_color' => array(
			'title' => 'Google bse color', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebasecolor')
		),
		'google_size' => array(
			'title' => 'Google base size', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebasesize')
		),
		'google_material' => array(
			'title' => 'Google base material', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebasematerial')
		),
		'google_pattern' => array(
			'title' => 'Google base pattern', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('googlebasepattern')
		),
		'amazon_item_id' => array(
			'title' => 'Amazon item id', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('amazonitemid')
		),
		'amazon_item_id_type' => array(
			'title' => 'Amazon item id type', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('amazonitemidtype')
		),
		'amazon_item_condition' => array(
			'title' => 'Amazon item condition', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('amazonitemcondition')
		),
		'ebay_category' => array(
			'title' => 'Ebay category', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('ebaycategory')
		),
		'yahoo_path' => array(
			'title' => 'Yahoo path', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('yahoopath')
		),
		'youtube_link' => array(
			'title' => 'YouTube Link', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('youtubelink')
		),
		'priceGrabber_category' => array(
			'title' => 'PriceGrabber category', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('pricegrabbercategory')
		),
		'pricegrabber_part_number' => array(
			'title' => 'PriceGrabber part number', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('pricegrabberpartnumber')
		),
		'pricegrabber_item_condition' => array(
			'title' => 'PriceGrabber item condition', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('pricegrabberitemcondition')
		),
		'nextag_category' => array(
			'title' => 'Nextag category', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('nextagcategory')
		),
		'nextag_part_number' => array(
			'title' => 'Nextag part number', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('nextagpartnumber')
		),
		'nextag_item_condition' => array(
			'title' => 'Nextag item condition', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('nextagitemcondition')
		),
		'date_added' => array(
			'title' => 'Date Added', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('dateadded')
		),
		'recurring_enabled' => array(
			'title' => 'Recurring enabled', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'YesNo',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringenabled', 'recurring')
		),
		'recurring_billing_custom' => array(
			'title' => 'Recurring billing custom', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringbillingcustom')
		),
		'recurring_billing_period_unit' => array(
			'title' => 'Recurring billing period unit', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringbillingperiodunit')
		),
		'recurring_billing_period_frequency' => array(
			'title' => 'Recurring billing period frequency', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringbillingperiodfrequency')
		),
		'recurring_billing_period_cycles' => array(
			'title' => 'Recurring billing period cycles', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringbillingperiodcycles')
		),
		'recurring_billing_amount' => array(
			'title' => 'Recurring billing amount', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringbillingamount')
		),
		'recurring_trial_enabled' => array(
			'title' => 'Recurring trial enabled', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringtrialenabled')
		),
		'recurring_trial_custom' => array(
			'title' => 'Recurring trial custom', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringtrialcustom')
		),
		'recurring_trial_period_unit' => array(
			'title' => 'Recurring trial period unit', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringtrialperiodunit')
		),
		'recurring_trial_period_frequency' => array(
			'title' => 'Recurring billing period frequency', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringtrialperiodfrequency')
		),
		'recurring_trial_period_cycles' => array(
			'title' => 'Recurring trial period cycles', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringtrialperiodcycles')
		),
		'recurring_trial_amount' => array(
			'title' => 'Recurring trial amount', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringtrialamount')
		),
		'recurring_start_date_delay' => array(
			'title' => 'Recurring start date delay', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('recurringstartdatedelay')
		),
		'recurring_customer_can_define_start_date' => array(
			'title' => 'Recurring customer can define start date', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringcustomercandefinestartdate')
		),
		'recurring_max_start_date_delay' => array(
			'title' => 'Recurring max start date delay', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '0', 'autoAssign' => array('recurringmaxstartdatedelay')
		),
		'recurring_schedule_description' => array(
			'title' => 'Recurring schedule description', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => '', 'autoAssign' => array('recurringscheduledescription')
		),
		'recurring_max_payment_failures' => array(
			'title' => 'Recurring max payment failures', 'type' => self::TYPE_INTEGER,
			'required' => false, 'default' => '7', 'autoAssign' => array('recurringmaxpaymentfailures')
		),
		'recurring_auto_complete_initial_order' => array(
			'title' => 'Recurring auto complete initial order', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringautocompleteinitialorder')
		),
		'recurring_auto_complete_recurring_orders' => array(
			'title' => 'Recurring auto recurring complete orders', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringautocompleterecurringorders')
		),
		'recurring_customer_can_suspend' => array(
			'title' => 'Recurring customer can suspend', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringcustomercansuspend')
		),
		'recurring_customer_can_cancel' => array(
			'title' => 'Recurring customer can cancel', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringcustomercancancel')
		),
		'recurring_admin_can_adjust_billing' => array(
			'title' => 'Recurring admin can adjust billing', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringadmincanadjustbilling')
		),
		'recurring_admin_can_adjust_trial' => array(
			'title' => 'Recurring admin can adjust trial', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringadmincanadjusttrial')
		),
		'recurring_customer_notifications' => array(
			'title' => 'Recurring customer notifications', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringcustomernotifications')
		),
		'recurring_admin_notifications' => array(
			'title' => 'Recurring admin notifications', 'type' => self::TYPE_STRING,
			'required' => false, 'default' => 'No', 'autoAssign' => array('recurringadminnotifications')
		),
		'same_day_delivery' => array(
			'title' => 'Available for same day delivery', 'type' => self::TYPE_BOOLEAN, 'subtype' => 'numeric',
			'required' => false, 'default' => 'No', 'autoAssign' => array('samedaydelivery')
		),
        	'dimension_width' => array(
			'title' => 'Dimension width', 'type' => self::TYPE_NUMBER,
			'required' => false, 'default' => '0.00', 'autoAssign' => array('width', 'dimensionwidth', 'itemwidth')
		),
        	'dimension_height' => array(
			'title' => 'Dimension height', 'type' => self::TYPE_NUMBER,
			'required' => false, 'default' => '0.00', 'autoAssign' => array('height', 'dimensionheight', 'itemheight')
		),
        	'dimension_length' => array(
			'title' => 'Dimension length', 'type' => self::TYPE_NUMBER,
			'required' => false, 'default' => '0.00', 'autoAssign' => array('length', 'dimensionlength', 'itemlength')
		)
	);

	protected $productsCount = 0;
	protected $productsSkipped = 0;

	/** @var DataAccess_CategoryRepository */
	protected $categoryRepository = null;

	/** @var DataAccess_ManufacturerRepository */
	protected $manufacturerRepository = null;

	/** @var DataAccess_ProductLocationRepository  */
	protected $productLocationRepository = null;

	/** @var DataAccess_TaxRepository */
	protected $taxRepository = null;

	/** @var DataAccess_ProductAttributeRepository */
	protected $productAttributeRepository = null;

	/** @var DataAccess_ProductVariantRepository */
	protected $productVariantRepository = null;

	/** @var DataAccess_ProductQuantityDiscountRepository */
	protected $productQuantityDiscount = null;

	/** @var DataAccess_ProductImageRepository */
	protected $productImageRepository = null;

	/** @var DataAccess_ShippingRepository */
	protected $shippingRepository = null;

	protected $updatedProductsIds = array();
	protected $categoriesMap = array();
	protected $manufacturersMap = array();
	protected $productLocationsMap = array();
	protected $taxClassesMap = array();

	protected $updateRule = 'keep';
	protected $makeVisible = false;
	protected $defaultCategoryId = 0;
	protected $importRecurringData = false;
	protected $importAttributes = false;
	protected $importQuantityDiscount = false;
	protected $importImages = false;
	protected $recurringBillingDataDefault = array();
	protected $productTrackAtVariants = array();
	protected $productLevelShipping = array();

	protected $productImagesDir = 'images/products';
	protected $productThumbsDir = 'images/products/thumbs';
	protected $productPreviewDir = 'images/products/preview';
	protected $productSecondaryImagesDir = 'images/products/secondary';
	protected $productSecondaryImagesThumbsDir = 'images/products/secondary/thumbs';

	protected $attributesImportLimit = 5; // Initial minimum value
	protected $imagesImportLimit = 5; // Initial image minimum import value

	/**
	 * Class constructor
	 *
	 * @param DataAccess_BaseRepository $repository
	 * @param Admin_Service_FileUploader $fileUploaderService
	 * @param null $params
	 */
	public function __construct(DataAccess_BaseRepository $repository, Admin_Service_FileUploader $fileUploaderService, $params = null)
	{
		parent::__construct($repository, $fileUploaderService);

		if (!is_null($params) && is_array($params))
		{
			$this->categoryRepository = $params['categoryRepository'];
			$this->manufacturerRepository = $params['manufacturerRepository'];
			$this->productLocationRepository = $params['productLocationRepository'];
			$this->taxRepository = $params['taxRepository'];
			$this->productAttributeRepository = $params['productAttributeRepository'];
			$this->productVariantRepository = $params['productVariantRepository'];
			$this->productQuantityDiscount = $params['productQuantityDiscount'];
			$this->productImageRepository =  $params['productImageRepository'];
			$this->shippingRepository = $params['shippingRepository'];
			$this->attributesImportLimit = isset($params['attributesImportLimit']) && $params['attributesImportLimit'] ? $params['attributesImportLimit'] : $this->attributesImportLimit;
			$this->imagesImportLimit = isset($params['imagesImportLimit']) && $params['imagesImportLimit'] ? $params['imagesImportLimit'] : $this->imagesImportLimit;
		}

		/**
		 * Add attributes params
		 */
		for ($i = 1; $i <= $this->attributesImportLimit; $i++)
		{
			$this->fields['attribute_type_' . $i] = array(
				'title' => 'Attribute type ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('attributetype' . $i)
			);
			$this->fields['attribute_name_' . $i] = array(
				'title' => 'Attribute name ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('attributename' . $i)
			);
			$this->fields['attribute_caption_' . $i] = array(
				'title' => 'Attribute caption ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('attributecaption' . $i)
			);
			$this->fields['attribute_options_' . $i] = array(
				'title' => 'Attribute options ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('attributeoptions' . $i)
			);
			$this->fields['attribute_priority_' . $i] = array(
				'title' => 'Attribute priority ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('attributepriority' . $i)
			);
			$this->fields['attribute_track_' . $i] = array(
				'title' => 'Attribute track inventory ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('attributetrackinventory' . $i)
			);
			$this->fields['attribute_required_' . $i] = array(
				'title' => 'Attribute required ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('attributerequired' . $i)
			);
		}

		/**
		 * Add quantity discount params
		 */
		for ($i = 1; $i <= 10; $i++)
		{
			$this->fields['qd_is_active_' . $i] = array(
				'title' => 'Quantity Discount Active ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('quantitydiscountactive' . $i)
			);
			$this->fields['qd_range_min_' . $i] = array(
				'title' => 'Quantity Discount Range Min ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('quantitydiscountrangemin' . $i)
			);
			$this->fields['qd_range_max_' . $i] = array(
				'title' => 'Quantity Discount Range Max ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('quantitydiscountrangemax' . $i)
			);
			$this->fields['qd_discount_' . $i] = array(
				'title' => 'Quantity Discount Discount ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('quantitydiscountdiscount' . $i)
			);
			$this->fields['qd_discount_type_' . $i] = array(
				'title' => 'Quantity Discount Discount Type ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('quantitydiscountdiscounttype' . $i)
			);
			$this->fields['qd_free_shipping_' . $i] = array(
				'title' => 'Quantity Discount Free Shippping ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('quantitydiscountfreeshippping' . $i)
			);
			$this->fields['qd_apply_to_wholesale_' . $i] = array(
				'title' => 'Quantity Discount Apply To Wholesale ' . $i, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('quantitydiscountapplytowholesale' . $i)
			);
		}

		for ($imageCount = 2; $imageCount <= $this->imagesImportLimit; $imageCount++)
		{
			$this->fields['image_url_'. $imageCount] = array(
				'title' => 'Image URL '. $imageCount, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('imageurl' . $imageCount)
			);

			$this->fields['image_location_'. $imageCount] = array(
				'title' => 'Image location  '. $imageCount, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('imagelocation' . $imageCount)
			);

			$this->fields['image_alt_text_'. $imageCount] = array(
				'title' => 'Image alt text  '. $imageCount, 'type' => self::TYPE_STRING,
				'required' => false, 'default' => '', 'autoAssign' => array('imagealttext' . $imageCount)
			);
		}
	}

	/**
	 * Find item by current data
	 *
	 * @param $data
	 *
	 * @return bool
	 */
	public function findItem($data, $params = null)
	{
		/** @var DataAccess_ProductsRepository $repository */
		$repository = $this->repository;
		return $repository->getProductByProductId($data['product_id'], true);
	}

	/**
	 * Modify data before insert
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	public function beforeInsert($data, $params = null)
	{
		// recurring
		if ($this->importRecurringData)
		{
			// get original products recurring data
			$data['recurring_billing_data'] = $this->recurringBillingDataDefault;
		}

		$data = $this->beforePersist($data, $params);

		// product type
		if (isset($data['digital_product']) && $data['digital_product'] == 'Yes')
		{
			$data['product_type'] = Model_Product::DIGITAL;
		}
		else if ((isset($data['product_type']) && !in_array($data['product_type'], array(Model_Product::DIGITAL, Model_Product::TANGIBLE, Model_Product::VIRTUAL))) || !isset($data['product_type']))
		{
			$data['product_type'] = Model_Product::TANGIBLE;
		}

		// date
		$data['added'] = date('Y-m-d H:i:s');

		return $data;
	}

	/**
	 * Run before update
	 *
	 * @param $item
	 * @param $data
	 * @param null $params
	 *
	 * @return mixed
	 */
	public function beforeUpdate($item, $data, $params = null)
	{
		// recurring
		if ($this->importRecurringData)
		{
			// get original products recurring data
			$recurringBillingData = trim($item['recurring_billing_data']) != '' ? @unserialize($item['recurring_billing_data']) : array();
			$data['recurring_billing_data'] = is_array($recurringBillingData) ? array_merge($this->recurringBillingDataDefault, $recurringBillingData) : $this->recurringBillingDataDefault;
		}

		$data = $this->beforePersist($data, $params);

		$lockFields = explode(',', $item['lock_fields']);
		$lockFields = is_array($lockFields) ? $lockFields : array();
		$lockFields = normalizeProductLockFields($lockFields);

		// mark updated products
		unset($this->updatedProductsIds[strtolower(trim($data['product_id']))]);

		if (!isset($data['categories']))
		{
			$data['cid'] = $item['cid'];
		}
		else
		{
			$data['cid'] = in_array('category', $lockFields) ?  $item['cid'] : intval($data['cid']);
		}

		if (isset($data['title']) && in_array('name', $lockFields)) $data['title'] = $item['title'];
		if (isset($data['price']) && in_array('price', $lockFields)) $data['price'] = $item['price'];
		if (isset($data['price2']) && in_array('sale-price', $lockFields)) $data['price2'] = trim($item['price2']) == '' ? null : $item['price2'];
		if (isset($data['overview']) && in_array('overview', $lockFields)) $data['overview'] = $item['overview'];
		if (isset($data['description']) && in_array('description', $lockFields)) $data['description'] = $item['description'];
		if (isset($data['meta_title']) && in_array('meta-title', $lockFields)) $data['meta_title'] = $item['meta_title'];
		if (isset($data['meta_description']) && in_array('meta-description', $lockFields)) $data['meta_description'] = $item['meta_description'];
		if (isset($data['image_url']) && in_array('images', $lockFields)) $data['image_url'] = $item['image_url'];
		if (isset($data['youtube_link']) && in_array('youtube_link', $lockFields)) $data['youtube_link'] = $item['youtube_link'];

		// if importing price ONLY and not importing sale price, and valid sale price exists for item in the DB, keep sale price and do not overwrite it
		if (isset($data['price']) && !in_array('price', $lockFields) && !array_key_exists('price2', $data) &&
			is_numeric($item['price2']) && floatval($item['price2']) > 0)
		{
			$data['price2'] = $item['price'];
		}

		// product type
		if (isset($data['digital_product']) && $data['digital_product'] == 'Yes')
		{
			$data['product_type'] = Model_Product::DIGITAL;
		}
		else if (isset($data['product_type']) && !in_array($data['product_type'], array(Model_Product::DIGITAL, Model_Product::TANGIBLE, Model_Product::VIRTUAL)))
		{
			$data['product_type'] = $item['product_type'];
		}

		return $data;
	}

	/**
	 * Prepare data before saving
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return mixed|void
	 */
	protected function beforePersist($data, $params = null)
	{
		// Fix / swap prices fields
		if (isset($data['price2']) && !is_numeric($data['price2'])) $data['price2'] = null;
		if (!array_key_exists('price', $data)) $data['price'] = null;

		// categories
		$primaryCategoryId = false;
		$productCategories = array();

		if (isset($data['category']))
		{
			$productCategoryKeys = explode('||', $data['category']);

			for ($i = 0; $i < count($productCategoryKeys); $i++)
			{
				$productCategoryKey = trim($productCategoryKeys[$i]);
				if ($productCategoryKey != '')
				{
					if (array_key_exists($productCategoryKey, $this->categoriesMap))
					{
						$cid = $this->categoriesMap[$productCategoryKey];
						$primaryCategoryId = $primaryCategoryId ? $primaryCategoryId : $cid; //the very first one id primary
						$productCategories[$cid] = $cid;
					}
				}
			}
			$data['categories'] = $productCategories;
		}

		if (!$primaryCategoryId) $primaryCategoryId = $this->defaultCategoryId;
		$data['cid'] = $primaryCategoryId;

		// visibility
		if (!isset($data['is_visible'])) $data['is_visible'] = $this->makeVisible ? 'Yes' : 'No';

		// taxes
		if (isset($data['tax_class_id']))
		{
			$taxClassKey = strtolower($data['tax_class_id']);
			if (trim($data['tax_class_id']) != '' && array_key_exists($taxClassKey, $this->taxClassesMap))
			{
				$data['tax_class_id'] = $this->taxClassesMap[$taxClassKey];
			}
		}

		// manufacturer
		if (isset($data['manufacturer']))
		{
			$manufacturerKey = strtolower($data['manufacturer']);
			if (array_key_exists($manufacturerKey, $this->manufacturersMap))
			{
				$data['manufacturer_id'] = $this->manufacturersMap[$manufacturerKey];
			}
		}

		// product location
		if (isset($data['location']))
		{
			$locationKey = strtolower($data['location']);
			if (array_key_exists($locationKey, $this->productLocationsMap))
			{
				$data['products_location_id'] = $this->productLocationsMap[$locationKey];
			}
		}

		// lock fields
		if (isset($data['lock_fields']))
		{
			$data['lock_fields'] = implode(',', explode('||', $data['lock_fields']));
		}

		// inventory control
		if (isset($data['inventory_control']) && trim($data['inventory_control']) == '') $data['inventory_control'] = 'No';

		// Prepare data for the following:
		// -> Recurring
		// -> Attributes
		// -> Quantity Discount
		// -> Secondary Images
		$recurringBillingData = array();
		$productAttributes = array();
		$productQuantityDiscounts = array();
		$productSecondaryImages = array();

		foreach ($data as $key => $value)
		{
			// Recurring
			if ($this->importRecurringData && substr($key, 0, 10) == 'recurring_')
			{
				if ($key == 'recurring_enabled')
				{
					$data['enable_recurring_billing'] = in_array(strtolower($value), $this->booleanTrue) ? '1' : '0';
				}
				else
				{
					$fieldName = substr($key, 10);

					// We don't need to keep billing_data since we already have data and it will only missed up the whole array after array_merge
					if ($fieldName != 'billing_data')
					{
						$recurringBillingData[$fieldName] = ($fieldName == 'customer_notifications' || $fieldName == 'admin_notifications') ? $data[$fieldName] = explode('||', $value) : $data[$fieldName] = $value;
					}
				}
			}

			// Attributes
			if ($this->importAttributes && substr($key, 0, 15) == 'attribute_name_')
			{
				preg_match_all('/\d+/', $key, $attributeIndexMatches);
				if (count($attributeIndexMatches))
				{
					$productAttributeIndex = end($attributeIndexMatches[0]);

					if (intval($productAttributeIndex))
					{
						if (isset($data['attribute_type_' . $productAttributeIndex]) && trim($data['attribute_type_' . $productAttributeIndex]) != ''
							&& isset($data['attribute_name_' . $productAttributeIndex]) && trim($data['attribute_name_' . $productAttributeIndex]) != ''
							&& isset($data['attribute_caption_' . $productAttributeIndex]) && trim($data['attribute_caption_' . $productAttributeIndex]) != ''
							&& isset($data['attribute_options_' . $productAttributeIndex]) && trim($data['attribute_options_' . $productAttributeIndex]) != ''
						){
							$productAttributes[] = array(
								'attribute_type' => $data['attribute_type_' . $productAttributeIndex],
								'name' => $data['attribute_name_' . $productAttributeIndex],
								'caption' => $data['attribute_caption_' . $productAttributeIndex],
								'text_length' => $data['attribute_options_' . $productAttributeIndex],
								'options' => $data['attribute_options_' . $productAttributeIndex],
								'priority' => isset($data['attribute_priority_' . $productAttributeIndex]) ? $data['attribute_priority_' . $productAttributeIndex] : 0,
								'track_inventory' => isset($data['attribute_track_' . $productAttributeIndex]) ? $data['attribute_track_' . $productAttributeIndex] : 0,
								'attribute_required' => isset($data['attribute_required_' . $productAttributeIndex]) ? $data['attribute_required_' . $productAttributeIndex] : 'No',
							);
						}
					}
				}
			}

			// Quantity Discount
			if ($this->importQuantityDiscount && substr($key, 0, 13) == 'qd_is_active_')
			{
				preg_match_all('/\d+/', $key, $quantityDiscountIndexMatches);
				if (count($quantityDiscountIndexMatches))
				{
					$quantityDiscountIndex = end($quantityDiscountIndexMatches[0]);

					if (intval($quantityDiscountIndex))
					{
						if (isset($data['qd_range_min_' . $quantityDiscountIndex]) && trim($data['qd_range_min_' . $quantityDiscountIndex]) != ''
							&& isset($data['qd_range_max_' . $quantityDiscountIndex]) && trim($data['qd_range_max_' . $quantityDiscountIndex]) != ''
							&& isset($data['qd_discount_' . $quantityDiscountIndex]) && trim($data['qd_discount_' . $quantityDiscountIndex]) != ''
							&& isset($data['qd_discount_type_' . $quantityDiscountIndex]) && trim($data['qd_discount_type_' . $quantityDiscountIndex]) != ''
							&& isset($data['qd_free_shipping_' . $quantityDiscountIndex]) && trim($data['qd_free_shipping_' . $quantityDiscountIndex]) != ''
							&& isset($data['qd_apply_to_wholesale_' . $quantityDiscountIndex]) && trim($data['qd_apply_to_wholesale_' . $quantityDiscountIndex]) != ''
						){
							$productQuantityDiscounts[] = array(
								'is_active' => $data['qd_is_active_' . $quantityDiscountIndex],
								'range_min' => $data['qd_range_min_' . $quantityDiscountIndex],
								'range_max' => $data['qd_range_max_' . $quantityDiscountIndex],
								'discount' => $data['qd_discount_' . $quantityDiscountIndex],
								'discount_type' => $data['qd_discount_type_' . $quantityDiscountIndex],
								'free_shipping' => $data['qd_free_shipping_' . $quantityDiscountIndex],
								'apply_to_wholesale' => $data['qd_apply_to_wholesale_' . $quantityDiscountIndex],
							);
						}
					}
				}
			}

			// Product Secondary Images
			if ($this->importImages && substr($key, 0, 10) == 'image_url_')
			{
				preg_match_all('/\d+/', $key, $productImageIndexMatches);
				if (count($productImageIndexMatches))
				{
					$productImageIndex = end($productImageIndexMatches[0]);

					if (intval($productImageIndex))
					{
						if ($data['image_location_' . $productImageIndex] == 'Local')
						{
							$productSecondaryImages['Local'][] = array(
								'image_url' => $data['image_url_' . $productImageIndex],
								'image_location' => $data['image_location_' . $productImageIndex],
								'image_alt_text' => $data['image_alt_text_' . $productImageIndex]
							);
						}
						else if ($data['image_location_' . $productImageIndex] == 'Web')
						{
							$productSecondaryImages['Web'][] = array(
								'image_url' => $data['image_url_' . $productImageIndex],
								'image_location' => $data['image_location_' . $productImageIndex],
								'image_alt_text' => $data['image_alt_text_' . $productImageIndex]
							);
						}
					}
				}
			}
		}

		$data['recurring_billing_data'] = serialize(array_merge($data['recurring_billing_data'], $recurringBillingData));
		$data['product_attributes'] = $productAttributes;
		$data['product_quantity_discounts'] = $productQuantityDiscounts;
		$data['product_secondary_images'] = $productSecondaryImages;

		if (isset($data['same_day_delivery'])) $data['same_day_delivery'] = in_array(strtolower($data['same_day_delivery']), $this->booleanTrue) ? '1' : '0';

		// Primary Images
		if (isset($data['image_url']) && $data['image_url'] != '')
		{
			$data['image_location'] = 'Web';
		}

		// assign (1) value so that product will be mark as actively set as product level shipping.
		$data['product_level_shipping'] = (isset($data['product_level_shippings']) && trim($data['product_level_shippings']) != '' ? 1 : 0);

		return $data;
	}

	/**
	 * Check do we hanve data to save product
	 *
	 * @param $item
	 * @param $data
	 * @param null $params
	 *
	 * @return bool
	 */
	protected function canPersist($item, $data, $params = null)
	{
		return $data['product_id'] != '' &&  $data['title'] != '' && trim($data['price']) != '' && is_numeric($data['price']) && $data['product_id'] != 'gift_certificate';
	}

	/**
	 * Check is product insertable existing ProductsMap
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool
	 */
	public function canInsert($data, $params = null)
	{
		if (AccessManager::checkAccess('MAX_PRODUCTS') <= $this->productsCount)
		{
			$this->productsSkipped++;
			return false;
		}

		return $this->canPersist(null, $data, $params = null);
	}

	/**
	 * Check is product updatable
	 *
	 * @param $item
	 * @param $data
	 * @param null $params
	 *
	 * @return bool
	 */
	public function canUpdate($item, $data, $params = null)
	{
		return $this->canPersist($item, $data, $params = null);
	}

	/**
	 * Run before inserted
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return mixed|void
	 */
	public function afterInsert($data, $params = null)
	{
		if (isset($data['id']) && $data['id']) $data['pid'] = $data['id'];

		$this->productsCount++;

		$params['__mode'] = DataAccess_ProductsRepository::MODE_ADD;

		$data = $this->afterPersist($data, $params);

		return $data;
	}

	/**
	 * Run after product updated
	 *
	 * @param $item
	 * @param $data
	 * @param null $params
	 *
	 * @return mixed
	 */
	public function afterUpdate($item, $data, $params = null)
	{
		// remove old attributes
		if ($this->importAttributes)
		{
			$this->productAttributeRepository->deleteAttributesByProductId($data['pid']);
		}

		// remove old quantity discount
		if ($this->importQuantityDiscount)
		{
			$this->productQuantityDiscount->deleteQuantityDiscountByProductId($data['pid']);
		}

		$params['__mode'] = DataAccess_ProductsRepository::MODE_UPDATE;

		$data = $this->afterPersist($data, $params);

		return $data;
	}

	/**
	 * Run after product data saved
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return mixed
	 */
	public function afterPersist($data, $params = null)
	{
		/** @var DataAccess_ProductsRepository $productRepository */
		$productRepository = $this->repository;

		// categories
		if (isset($data['categories']))
		{
			$productRepository->persistProductCategories($data['pid'], $params['__mode'], $data['categories'], $data['cid']);
		}

		// attributes
		if ($this->importAttributes && isset($data['product_attributes']) && is_array($data['product_attributes']) && count($data['product_attributes']) > 0)
		{
			$attributeRepository = $this->productAttributeRepository;

			// delete non-existing attributes
			$attributesNames = array();
			foreach ($data['product_attributes'] as $productAttribute) $attributesNames[] = trim($productAttribute['name']);
			$attributeRepository->deleteAttributesByProductIdAndNames($data['pid'], $attributesNames);

			$attributesMap = $attributeRepository->getProductAttributesMap($data['pid']);
			$productAttributeDefault = $attributeRepository->getDefaults();

			foreach ($data['product_attributes'] as $productAttribute)
			{
				if ($productAttribute['attribute_type'] == 'select' || $productAttribute['attribute_type'] == 'radio')
				{
					$productAttribute['options'] = str_replace('||', "\n", $productAttribute['options']);
				}

				$productAttribute = array_merge($productAttributeDefault, $productAttribute);
				$productAttribute['pid'] = $data['pid'];

				if (isset($attributesMap[$productAttribute['name']]))
				{
					$productAttribute['id'] = $productAttribute['paid'] = $attributesMap[$productAttribute['name']];
				}
				else
				{
					unset($productAttribute['paid']);
				}

				$productAttribute['is_active'] = 'Yes';

				$attributeRepository->persist($productAttribute);
			}
		}

		// Secondary Images
		if ($this->importImages && isset($data['product_secondary_images']) && is_array($data['product_secondary_images']) && count($data['product_secondary_images']) > 0)
		{
			$secondaryImageRows = $this->productImageRepository->getSecondaryImages($data['pid']);

			//Check if product has secondary images
			$secondaryImages = array('Local' => array(), 'Web' => array());
			foreach ($secondaryImageRows as $secondaryImageRow)
			{
				if ($secondaryImageRow['location'] == 'Local')
				{
					$secondaryImages['Local'][] = $secondaryImageRow;
				}
				else if ($secondaryImageRow['location'] == 'Web')
				{
					$secondaryImages['Web'][] = $secondaryImageRow;
				}
			}

			// update local images
			foreach ($secondaryImages['Local'] AS $key => $secondaryLocalImage)
			{
				if (isset($data['product_secondary_images']['Local']))
				{
					$secondaryLocalImage['altText'] = $data['product_secondary_images']['Local'][$key]['image_alt_text'];
					$secondaryLocalImage['location'] = $data['product_secondary_images']['Local'][$key]['image_location'];
					$secondaryLocalImage['imageUrl'] = $data['product_secondary_images']['Local'][$key]['image_url'];
					$this->productImageRepository->updateImage($data['pid'], $secondaryLocalImage['image_id'], $secondaryLocalImage);
				}
			}

			// delete all external images
			// so that import of external images of the same source will be fix.
			$this->productImageRepository->deleteWebSecondaryImages($data['pid']);

			// check for primary images
			$existPrimaryImage = (ImageUtility::productHasImage($data['product_id']) || $data['image_url']);
			foreach ($data['product_secondary_images']['Web'] as $importSecondaryImage)
			{
				if ($existPrimaryImage)
				{
					$this->productImageRepository->persistWebSecondaryImage($data['pid'], $importSecondaryImage);
				}
				else
				{
					$this->productImageRepository-> persistWebPrimaryImage($data['pid'], $importSecondaryImage);
					$existPrimaryImage = true;
				}
			}
		}

		//quantity discounts
		if ($this->importQuantityDiscount && isset($data['product_quantity_discounts']) && is_array($data['product_quantity_discounts']) && count($data['product_quantity_discounts']) > 0)
		{
			$quantityDiscountRepository = $this->productQuantityDiscount;
			$productQuantityDiscountDefault = $quantityDiscountRepository->getDefaults();

			foreach ($data['product_quantity_discounts'] as $qd)
			{
				$qd = array_merge($productQuantityDiscountDefault, $qd);
				$qd['pid'] = $data['pid'];
				$quantityDiscountRepository->persist($qd);
			}
		}

		//product level shipping info
		if (isset($data['product_level_shippings']) && trim($data['product_level_shippings']) != '')
		{
			$productRepository->deleteProductLevelShippingByProductId($data['pid']);

			$this->productLevelShipping[$data['product_id']]['pid'] = $data['pid'];
			$this->productLevelShipping[$data['product_id']]['carrier_name'] = $data['product_level_shippings'];
		}

		if (isset($data['product_sub_id']) && trim($data['product_sub_id']) != '')
		{
			$variantRepository = $this->productVariantRepository;

			$productVariant = $variantRepository->getProductVariantBySubId($data['pid'], $data['product_sub_id']);

			if (!$productVariant)
			{
				$productVariant = $variantRepository->getDefaults();
				$productVariant['is_active'] = '1';
				$productVariant['pid'] = $data['pid'];
				$productVariant['product_subid'] = $data['product_sub_id'];
			}

			if (array_key_exists('product_sub_sku', $data))
			{
				$productVariant['product_sku'] = $data['product_sku'];
			}

			if (array_key_exists('product_sub_id_stock', $data))
			{
				$productVariant['stock'] = intval($data['product_sub_id_stock']);
			}

			if (array_key_exists('product_sub_options', $data))
			{
				if (isset($data['product_id']))
				{
					$productVariant['sub_product_attributes'] = $data['product_sub_options'];

					$this->productTrackAtVariants[$data['product_id']]['pid'] = $data['pid'];
					$this->productTrackAtVariants[$data['product_id']]['variant'][] = $productVariant;
				}
			}
		}

		$event = new Events_ProductEvent(Events_ProductEvent::ON_UPDATED);
		$event->setData('product', $data);
		Events_EventHandler::handle($event);

		return $data;
	}

	/**
	 * Import
	 *
	 * @param $fileName
	 * @param $fieldMap
	 * @param $delimiter
	 * @param $skipFirstLine
	 * @param $params
	 *
	 * @return array|bool
	 */
	public function import($fileName, $fieldMap, $delimiter, $skipFirstLine, $params = null)
	{
		$this->updateRule = is_array($params) && isset($params['updateRule']) ? $params['updateRule'] : 'keep';
		$this->makeVisible = is_array($params) && isset($params['makeVisible']) ? $params['makeVisible'] : false;
		$this->defaultCategoryId = is_array($params) && isset($params['defaultCategoryId']) ? $params['defaultCategoryId'] : 0;

		/** @var DataAccess_ProductsRepository $productRepository */
		$productRepository = $this->repository;

		/** @var DataAccess_ProductVariantRepository $variantRepository */
		$variantRepository = $this->productVariantRepository;

		/** @var DataAccess_ProductAttributeRepository $attributeRepository */
		$attributeRepository = $this->productAttributeRepository;

		/** @var DataAccess_ShippingRepository $shippingRepository */
		$shippingRepository = $this->shippingRepository;

		$this->productsCount = $productRepository->getCount();

		// check recurring
		$this->importRecurringData = false;
		$this->importAttributes = false;
		$this->importQuantityDiscount = false;
		$this->importImages = false;

		foreach ($fieldMap as $fieldName)
		{
			if (substr($fieldName, 0, 10) == 'recurring_')
			{
				$this->importRecurringData = true;
				$recurringBillingDataModel = new RecurringBilling_Model_ProductRecurringBillingData();
				$this->recurringBillingDataDefault = $recurringBillingDataModel->toArray();
			}
			else if (substr($fieldName, 0, 10) == 'attribute_')
			{
				$this->importAttributes = true;
			}
			else if (substr($fieldName, 0, 3) == 'qd_')
			{
				$this->importQuantityDiscount = true;
			}

			else if (substr($fieldName, 0, 10) == 'image_url_')
			{
				$this->importImages = true;
			}
		}

		// cache some required data
		$this->updatedProductsIds = $productRepository->getProductsIdsMap();

		if (in_array('category', $fieldMap)) $this->categoriesMap = $this->categoryRepository->getCategoriesIdsMap();
		if (in_array('manufacturer', $fieldMap)) $this->manufacturersMap = $this->manufacturerRepository->getManufacturersIdsMap();
		if (in_array('location', $fieldMap)) $this->productLocationsMap = $this->productLocationRepository->getLocationsIdsMap();
		if (in_array('tax_class_id', $fieldMap)) $this->taxClassesMap = $this->taxRepository->getClassesIdsMap();

		$result = parent::import($fileName, $fieldMap, $delimiter, $skipFirstLine, $params);

		// update product level shipping
		if (is_array($this->productLevelShipping))
		{
			$activeShippingMethods = $shippingRepository->getShippingMethods(true);
			foreach ($activeShippingMethods as $key => $activeShippingMethod)
			{
				unset($activeShippingMethods[$key]);

				// check if product level shipping method
				if ($activeShippingMethod['method_id'] == 'product' && $activeShippingMethod['carrier_id'] == 'custom')
				{
					$key = strtolower(str_replace(' ', '', $activeShippingMethod['carrier_name']));
					$activeShippingMethods[$key] = $activeShippingMethod;
				}
			}

			foreach ($this->productLevelShipping as $productLevelShippingMethod)
			{
				$importProductShippingMethods = array();
				$productShippingCarriers = explode("||", $productLevelShippingMethod['carrier_name']);

				foreach ($productShippingCarriers as $productShippingCarrier)
				{
					list($shippingCarrierName, $shippingCarrierPrice) = explode(":", $productShippingCarrier);

					$key = strtolower(str_replace(' ', '', $shippingCarrierName));
					if (array_key_exists($key, $activeShippingMethods))
					{
						// update product level shipping base on uploaded excel file
						$shippingSelectedId = $activeShippingMethods[$key]['ssid'];
						$importProductShippingMethods[$shippingSelectedId] = array(
							'active' => (floatval($shippingCarrierPrice) > 0 ? 1 : 0),
							'price' => $shippingCarrierPrice
						);
					}
				}
				$productRepository->persistProductShippingMethod($productLevelShippingMethod['pid'], 'update', $importProductShippingMethods);
			}
		}

		// update product variants
		if (is_array($this->productTrackAtVariants))
		{
			foreach ($this->productTrackAtVariants as $productTrackAtVariant)
			{
				if (is_array($productTrackAtVariant['variant']))
				{
					$productVariantIds = array();
					$deleteVariantIds = array();

					// Get all product variants by product ID
					$productVariantList = $variantRepository->getListByProductId($productTrackAtVariant['pid']);

					// Process latest variant data from CSV
					foreach ($productTrackAtVariant['variant'] as $productVariant)
					{
						if (isset($productVariant['pi_id']))
						{
							$productVariantIds[] = $productVariant['pi_id'];
						}

						$productAttributes = $attributeRepository->getListByProductId($productTrackAtVariant['pid']);

						$options = preg_split("/(\|\||\n)/", $productVariant['sub_product_attributes']);
						$attributes = array();

						foreach ($options as $option)
						{
							$pos = strpos($option, ':');
							$attrName = trim(substr($option, 0, $pos));
							$attrValue = trim(substr($option, $pos + 1));

							foreach ($productAttributes as $productAttribute)
							{
								if ($productAttribute['name'] == $attrName)
								{
									$attributes[$productAttribute['paid']] = $attrValue;
								}
							}
						}

						$productVariant['attributes'] = $attributes;
						$variantRepository->persist($productVariant, array('productAttributes' => $productAttributes));
						$productRepository->updateProductVariantsStock($productTrackAtVariant['pid']);
					}

					// Clean product inventory
					foreach ($productVariantList as $currProductVariant)
					{
						if (!in_array($currProductVariant['pi_id'], $productVariantIds))
						{
							$deleteVariantIds[] = $currProductVariant['pi_id'];
						}
					}
					$variantRepository->delete($deleteVariantIds);
				}
			}
		}

		// remove all products that were not updated
		if ($this->updateRule == 'clean')
		{
			$ids = array();
			foreach ($this->updatedProductsIds as $pid) if ($pid) $ids[] = $pid;

			if (count($ids))
			{
				$result['deletedCount'] = count($ids);
				$productRepository->delete($ids, $this->productImagesDir, $this->productThumbsDir, $this->productPreviewDir, $this->productSecondaryImagesDir);
			}
		}

		// update attributes count
		$productRepository->updateProductsAttributesCount();

		return $result;
	}
}

<?php

/**
 * Class Admin_Service_CategoryImport
 */
class Admin_Service_CategoryImport extends Admin_Service_Import
{
	/** @var array $fields */
	protected $fields = array(
		'__skip' => array(
			'title' => 'Skip',
			'type' => '',
			'required' => false,
			'autoAssign' => array()
		),
		'key_name' => array(
			'title' => 'Category key',
			'type' => self::TYPE_STRING,
			'required' => true,
			'autoAssign' => array('key', 'id', 'code', 'categorykey', 'categoryid', 'categorycode')
		),
		'name' => array(
			'title' => 'Category name',
			'type' => self::TYPE_STRING,
			'required' => true,
			'autoAssign' => array('name', 'title', 'categoryname', 'categorytitle')
		),
		'parent' => array(
			'title' => 'Parent category key',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('parent', 'parentid', 'parentkey', 'parentcode', 'parentcategoryid', 'parentcategorycode', 'parentcategorykey')
		),
		'is_visible' => array(
			'title' => 'Is visible',
			'type' => self::TYPE_BOOLEAN,
			'subtype' => 'YesNo',
			'required' => false,
			'default' => 'Yes',
			'autoAssign' => array('visible', 'isvisible', 'active', 'isactive', 'available', 'isavailable')
		),

		'url_custom' => array(
			'title' => 'Custom URL',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('url', 'customurl', 'urlcustom')
		),
		'meta_title' => array(
			'title' => 'Meta title',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('metatitle')
		),
		'meta_description' => array(
			'title' => 'Meta description',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('metadescription')
		),
		'priority' => array(
			'title' => 'Priority',
			'type' => self::TYPE_INTEGER,
			'required' => false,
			'default' => '5',
			'autoAssign' => array('priority', 'sortorder', 'categorypriority', 'categorysortorder', 'sortingorder', 'sortingpriority', 'categorysortingorder', 'categorysortingpriority')
		),
		'category_header' => array(
			'title' => 'Category header',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('header', 'categoryheader')
		),
		'description' => array(
			'title' => 'Description',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('description', 'categorydescription', 'descriptiontop', 'topdescription', 'categorydescriptiontop')
		),
		'description_bottom' => array(
			'title' => 'Bottom description',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('descriptionbottom', 'categorydescriptionbottom', 'bottomdescription', 'categorybottomdescription')
		),
		'exactor_euc_code' => array(
			'title' => 'Exactor Euc Code',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('exactoreuccode', 'exactor', 'exactorcode')
		),
		'avalara_tax_code' => array(
			'title' => 'Avalara Tax Code',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('avalarataxcode', 'avalara', 'avalaracode')
		),
	);

	protected $categoriesKeys = array();

	protected $categoriesMissingParent = array();

	/**
	 * Find item by current data
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return array|bool
	 */
	public function findItem($data, $params = null)
	{
		/** @var DataAccess_CategoryRepository $repository */
		$repository = $this->repository;
		return $repository->getByKeyName($data['key_name']);
	}

	/**
	 * Modify data before insert
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	protected function beforeInsert($data, $params = null)
	{
		$makeVisible = is_array($params) && isset($params['makeVisible']) ? $params['makeVisible'] : false;

		if (isset($data['parent']))
		{
			$parent = strtolower(trim($data['parent']));
			if (isset($this->categoriesKeys[$parent]))
			{
				$data['parent'] =  $this->categoriesKeys[$parent];
			}
			else
			{
				if(strtolower(trim($data['key_name'])) != $parent) $this->categoriesMissingParent[strtolower(trim($data['key_name']))] = $parent;
				$data['parent'] = 0;
			}
		}

		if (!isset($data['is_visible'])) $data['is_visible'] = $makeVisible ? '1' : '0';

		return $data;
	}

	/**
	 * @param $data
	 * @param $params
	 *
	 * @return mixed|void
	 */
	protected function afterInsert($data, $params = null)
	{
		if (isset($data['id']) && $data['id']) $data['cid'] = $data['id'];

		$this->categoriesKeys[strtolower(trim($data['key_name']))] = $data['cid'];

		return $data;
	}

	/**
	 * Modify data before update
	 *
	 * @param $item
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	protected function beforeUpdate($item, $data, $params = null)
	{
		$makeVisible = is_array($params) && isset($params['makeVisible']) ? $params['makeVisible'] : false;
		$makeVisibleExisting = is_array($params) && isset($params['makeVisibleExisting']) ? $params['makeVisibleExisting'] : false;

		if (isset($data['parent']))
		{
			$parent = strtolower(trim($data['parent']));

			if (isset($this->categoriesKeys[$parent]))
			{
				$data['parent'] =  $this->categoriesKeys[$parent];
			}
			else
			{
				$this->categoriesMissingParent[strtolower(trim($data['key_name']))] = $parent;
				$data['parent'] = 0;
			}
		}

		if (!isset($data['is_visible']) && $makeVisibleExisting) $data['is_visible'] = $makeVisible ? '1' : '0';

		return $data;
	}

	protected function afterUpdate($item, $data, $params = null)
	{
		$event = new Events_CategoryEvent(Events_CategoryEvent::ON_UPDATED);
		$event->setData('category', $data);
		Events_EventHandler::handle($event);

		return $data;
	}

	/**
	 * Import
	 *
	 * @param $fileName
	 * @param $fieldMap
	 * @param $delimiter
	 * @param $skipFirstLine
	 * @param null $params
	 *
	 * @return array|bool
	 */
	public function import($fileName, $fieldMap, $delimiter, $skipFirstLine, $params = null)
	{
		/** @var DataAccess_CategoryRepository $repository */
		$repository = $this->repository;

		// get current categories
		$this->categoriesKeys = $repository->getCategoryKeyNames();

		$this->categoriesMissingParent = array();

		$result = parent::import($fileName, $fieldMap, $delimiter, $skipFirstLine, $params);

		/**
		 * Update parent / child relations
		 */
		$parentsChildren = array();
		foreach ($this->categoriesMissingParent as $categoryKey=>$parentKey)
		{
			if (isset($this->categoriesKeys[$parentKey]) && isset($this->categoriesKeys[$categoryKey]))
			{
				$parentId = $this->categoriesKeys[$parentKey];

				if (!isset($parentsChildren[$parentId])) $parentsChildren[$parentId] = array();

				$parentsChildren[$parentId][] = $this->categoriesKeys[$categoryKey];
			}
		}

		foreach ($parentsChildren as $parentId => $categoriesIds)
		{
			$repository->updateCategoriesParent($categoriesIds, $parentId);
		}

		return $result;
	}
}
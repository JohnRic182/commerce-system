<?php

/**
 * Class Admin_Service_SubscribersExport
 */
class Admin_Service_SubscribersExport extends Admin_Service_Export
{
	/** @var array $fields */
	protected $fields = array(
		'fname' => 'First Name',
		'lname' => 'Last Name',
		'email' => 'Email'
	);

	protected $subscribersResult = null;

	/**
	 * @param array|null $options
	 * @param array|null $exportFields
	 *
	 */
	public function selectData(array $options = null, array $exportFields = null)
	{
		$db = $this->db;

		$sql = 'SELECT fname, lname, lower(email) AS email FROM ' . DB_PREFIX . 'users WHERE login != "ExpressCheckoutUser" AND email !="" AND receives_marketing = "Yes" AND removed != "Yes"
				UNION ALL
				SELECT "" as fname, "" as lname, lower(e.email) AS email FROM ' . DB_PREFIX . 'emails e LEFT JOIN ' . DB_PREFIX . 'users u ON e.email = u.email AND u.removed != "Yes" WHERE e.receives_marketing = "YES" and e.dead != "Yes" AND u.uid IS NULL ;';

		$this->subscribersResult = $db->query($sql
			);
	}

	/**
	 * Get data from data source. It must return array ordered in exported fields order
	 *
	 * @param array|null $exportFields
	 *
	 * @return array|false
	 */
	public function getData(array $exportFields = null)
	{
		$db = $this->db;

		return $db->moveNext($this->subscribersResult);
	}
}
<?php

class Admin_Service_MessageCenter
{
	public function getMessages()
	{
		$licenseNumber = base64_encode(LICENSE_NUMBER);
		$isLabeledApp = IS_LABEL;

		$messages = $this->doGet('https://cartmanual.com/messagecenter/web/public/messages', array('t' => $licenseNumber, 'l' => $isLabeledApp));

		return @json_decode($messages);
	}


	protected function doGet($url, array $reqParams = array(), array $reqHeaders = array())
	{
		foreach ($reqHeaders as $key => $value)
		{
			$headers[] = $key . ': ' . $value;
		}
		$headers[] = 'Content-Length: ' . strlen('');

		$query = '';
		if (count($reqParams) > 0)
		{
			$query = '?'.http_build_query($reqParams);
		}

		$params = array();
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url . $query;
		$params[CURLOPT_HTTPHEADER] = $headers;

		// debug
		$params[CURLOPT_VERBOSE] = false;

		// return headers
		$params[CURLOPT_HEADER] = false;

		$request = '';
		$request .= 'GET ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";
		foreach ($headers as $header)
		{
			$request .= $header . "\r\n";
		}

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		$response = curl_exec($ch);

		@curl_close($ch);

		return $response;
	}
}
<?php

/**
 * Class Admin_Service_PinnacleConnect
 */
class Admin_Service_PinnacleConnect
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;
	protected $baseUrl;
	protected $baseUrlProduction = '';
	protected $baseUrlTest = '';
	protected $isTest;

	/**
	 * Class constructor
	 *
	 * @param DataAccess_SettingsRepository $settings
	 * @param bool $isTest
	 */
	public function __construct(DataAccess_SettingsRepository $settings, $isTest = false)
	{
		$this->isTest = $isTest;
		//TODO: This should be refactored out to an abstract method instead of overriding protected fields
		$this->baseUrl = $this->isTest ? $this->baseUrlTest : $this->baseUrlProduction;
		$this->settings = $settings;
	}

	/**
	 * Hash params
	 *
	 * @param array $data
	 */
	protected function hashParams(array &$data)
	{
		ksort($data);
		$message = str_replace('+', '%20', http_build_query($data));
		$hash = base64_encode(hash_hmac('sha256', $message, $this->isTest ? 'SUPER SECRET KEY' : 'L30SD]N}Lj<.h17WQYlqhMM5;!lwzAMK%[U7[)#o{438 G!&(>~=acppBYrj?.~c', true));
		$data['signature'] = $hash;
	}

	/**
	 * Do get
	 *
	 * @param $url
	 * @param array $reqParams
	 * @param array $reqHeaders
	 *Cn
	 * @return mixed
	 */
	protected function doGet($url, array $reqParams = array(), array $reqHeaders = array())
	{
		$url = $this->baseUrl . $url;
		$headers = array('X-Access-Token: '.'Bearer '.$this->settings->get('AccountToken'));

		foreach ($reqHeaders as $key => $value) $headers[] = $key . ': ' . $value;
		$headers[] = 'Content-Length: ' . strlen('');

		$query = '';
		if (count($reqParams) > 0) $query = '?'.http_build_query($reqParams);

		$params = array();
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url . $query;
		$params[CURLOPT_HTTPHEADER] = $headers;
		$params[CURLOPT_VERBOSE] = false;
		$params[CURLOPT_HEADER] = false;

		$request = '';
		$request .= 'GET ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";
		foreach ($headers as $header) $request .= $header . "\r\n";

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		$response = curl_exec($ch);

		@curl_close($ch);

		return $response;
	}

	/**
	 * Do post
	 *
	 * @param $url
	 * @param $body
	 * @param array $reqHeaders
	 *
	 * @return bool|mixed|string
	 */
	protected function doPost($url, $body, array $reqHeaders = array())
	{
		$url = $this->baseUrl . $url;
		$headers = array('X-Access-Token: '.'Bearer '.$this->settings->get('AccountToken'));

		foreach ($reqHeaders as $key => $value) $headers[] = $key . ': ' . $value;

		$params = array();
		$params[CURLOPT_POST] = true;
		$params[CURLOPT_POSTFIELDS] = http_build_query($body);
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url;
		$params[CURLOPT_HTTPHEADER] = $headers;
		$params[CURLOPT_FOLLOWLOCATION] = true;
		$params[CURLOPT_VERBOSE] = false;
		$params[CURLOPT_SSL_VERIFYHOST] = false;
		$params[CURLOPT_SSL_VERIFYPEER] = false;
		$params[CURLOPT_HEADER] = false;

		$request = 'POST ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";

		foreach ($headers as $header) $request .= $header . "\r\n";

		$request .= "\r\n";

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		$response = curl_exec($ch);

		if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404)
		{
			@curl_close($ch);
			return '';
		}

		if (curl_errno($ch))
		{
			@curl_close($ch);
//			if ($errnum)
//				$errnum = curl_errno($ch);
//			if ($errmsg)
//				$errmsg = curl_error($ch);

			return false;
		}

		@curl_close($ch);

		return $response;
	}

	/**
	 * Get licensed id
	 *
	 * @return string
	 */
	protected function getLicenseId()
	{
		$license_id = '';
		$licenseParts = explode('-', LICENSE_NUMBER);
		if (count($licenseParts) > 1)
		{
			$license_id = $licenseParts[1];
		}

		return $license_id;
	}
}
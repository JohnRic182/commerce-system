<?php

/**
 * Class Admin_Service_Support
 */
class Admin_Service_Support extends Admin_Service_PinnacleConnect
{
	protected $baseUrlProduction = 'https://account.pinnaclecart.com/support-api/';
	protected $baseUrlTest = 'https://account-staging.pinnaclecart.com/support-api/';

	/**
	 * @param $data
	 * @return array|bool
	 */
	public function getTicketValidationErrors($data)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['subject']) == '') $errors['subject'] = array('Please enter subject');
			if (trim($data['message']) == '') $errors['message'] = array('Please enter message');
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Submit ticket
	 * @param $data
	 * @return array|mixed
	 */
	public function submitTicket($data)
	{
		if (isset($data['admin']) && $data['admin'] == '1')
		{
			$adminData = $this->setupSupportAdminProfile();
		}
		else
		{
			$adminData['username'] = 'n/a';
			$adminData['password'] = 'n/a';
		}

		$params = array(
			'subject' => $data['subject'],
			'message' => $data['message'],
			'license_id' => $this->getLicenseId(),
			'license_number' => LICENSE_NUMBER,
			'cart_url' => $this->settings->get('GlobalHttpUrl'),
			'admin_url' => $this->settings->get('AdminHttpsUrl').'/admin.php',
			'cart_version' => $this->settings->get('AppVer'),
			'package_type' => $this->settings->get('AppEncoder'),
			'full_name' => $data['full_name'],
			'email' => $data['email'],
			'admin_username' => $adminData['username'],
			'admin_password' => $adminData['password']
		);

		$this->hashParams($params);

		return @json_decode($this->doPost('submit-ticket', $params));
	}

	/**
	 * Setup admin profile
	 */
	protected function setupSupportAdminProfile()
	{
		// TODO fix global dependency
		global $db;
		$adminRepository = new DataAccess_AdminRepository($db, $this->settings);

		$formData['username'] = 'PinnacleCartSupport-'.generatePassword();
		$formData['password'] = generatePassword();
		$formData['name'] = 'Pinnacle Cart';
		$formData['email'] = 'support@pinnaclecart.com';
		$formData['active'] = 'Yes';
		$formData['force_password_change'] = '0';
		$formData['rights'] = 'support,all';
		$receive_notification['password_change'] = 'pwdchange30';
		$formData['receive_notifications'] = '';
		$formData['expires'] = '1';
		$formData['expiration_date'] = date('Y-m-d H:i:s', strtotime('+15 days'));
		$formData['security_question_id'] = '2';
		$formData['security_question_answer'] = generatePassword();

		$passwordHash = getPasswordHash($formData['password']);
		$passwordStrength = pwdStrength($formData['password']);
		$passwordHistory = array();

		$adminRepository->save(0, $formData, $passwordHash, $passwordStrength, $passwordHistory);

		return $formData;
	}
}
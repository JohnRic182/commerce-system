<?php

/**
 * Class Admin_Service_CategoryExport
 */
class Admin_Service_GoogleMerchantExport
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;
	/** @var DB */
	protected $db;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->db = $db;
		$this->settings = $settings;
	}

	/**
	 * Export
	 *
	 * @param $exportTaxes
	 * @param $productCategory
	 * @param $productCount
	 * @param $expDays
	 */
	public function export($exportTaxes, $productCategory, $productCount, $expDays)
	{
		$settings = $this->settings;
		$db = $this->db;

		if ($row = $db->selectOne('SELECT iso_a2 FROM '.DB_PREFIX.'countries WHERE coid='.intval($settings->get('TaxDefaultCountry'))))
		{
			$defaultTaxCountry = $row['iso_a2'];
		}
		else
		{
			$defaultTaxCountry = 'US';
		}

		$expdate = '';
		if ($expDays > 0 && $expDays < 31)
		{
			$expdate = gmdate("Y-m-d", time() + ($expDays * 24 * 60 * 60));
		}

		$store_location = trim($settings->get('CompanyAddressLine1'));
		$store_location .= (trim($settings->get('CompanyAddressLine2')) == '' ? '' : ', '.trim($settings->get('CompanyAddressLine2')));
		$store_location .= (trim($settings->get('CompanyCity')) == '' ? '' : ', '.trim($settings->get('CompanyCity')));
		$store_location .= (trim($settings->get('CompanyState')) == '' ? '' : ', '.trim($settings->get('CompanyState')));
		$store_location .= (trim($settings->get('CompanyZip')) == '' ? '' : ', '.trim($settings->get('CompanyZip')));
		$store_location .= (trim($settings->get('CompanyCountry')) == '' ? '' : ', '.trim($settings->get('CompanyCountry')));

		/**
		 * Preset taxes
		 */
		//create tax tables array
		$alternate_tax_tables = array();

		//non-taxable products
		$alternate_tax_tables[] = '<alternate-tax-table standalone="true" name="tax_exempt"><alternate-tax-rules/></alternate-tax-table>';

		//select all tax classes used in current order
		$db->query("SELECT tc.class_id AS tax_class_id, tc.class_name AS tax_class_name FROM ".DB_PREFIX."tax_classes tc");

		$_tax_classes = $db->getRecords();

		$tax_classes = array();
		foreach ($_tax_classes as $_tax_class)
		{
			//add alternate state table for each tax class
			$tax_xml = '';

			//add rates and zones
			$r = $db->query("
				SELECT
					tr.tax_rate,
					tzr.coid as country_id,
					tzr.stid as state_id,
					c.iso_a2 AS country_code,
					s.short_name AS state_code
				FROM ".DB_PREFIX."tax_zones_regions tzr
					INNER JOIN ".DB_PREFIX."tax_rates tr ON tzr.zone_id = tr.zone_id
					LEFT JOIN ".DB_PREFIX."countries c ON c.coid = tzr.coid
					LEFT JOIN ".DB_PREFIX."states s ON s.stid = tzr.stid
				WHERE tr.class_id='".$_tax_class["tax_class_id"]."' AND tr.user_level LIKE '%0%'
			");

			if ($db->numRows($r) > 0)
			{
				//add new tax rule for each iteration (tax rate)
				while ($db->moveNext($r))
				{
					$_tax_rate = $db->col;
					$tax_xml .= '<g:tax><g:rate>'.myNum($_tax_rate["tax_rate"]).'</g:rate><g:tax_ship>'.($settings->get('ShippingTaxable') ? "y" : "n").'</g:tax_ship>';

					//world-wide tax
					if ($_tax_rate["country_id"] == "0")
					{
						$tax_xml.= '<g:country>'.$defaultTaxCountry.'</g:country><g:region>*</g:region>';
					}
					//country-specific tax
					else
					{
						$tax_xml.= '<g:country>'.$_tax_rate["country_code"].'</g:country>';
						if ($_tax_rate["state_id"] == "0")
						{
							$tax_xml.= '<g:region>*</g:region>';
						}
						else
						{
							$tax_xml.= '<g:region>'.$_tax_rate["state_code"].'</g:region>';
						}
					}
					$tax_xml.= '</g:tax>';

				}
			}
			$tax_classes[$_tax_class["tax_class_id"]] = $tax_xml;
		}

		$mime_type = "text/xml";
		$filename = "google_merchant";
		$ext = "xml";

		header('Content-Type: '.$mime_type);
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="'.$filename.'.'.$ext.'"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Disposition: attachment; filename="'.$filename.'.'.$ext.'"');

		$noImageUrl = DesignElements::getImage('image-no-image-big');

		// initialize $ms array
		$ms = array();

		$db->query("SELECT ".DB_PREFIX."manufacturers.manufacturer_id AS id, ".DB_PREFIX."manufacturers.manufacturer_name FROM ".DB_PREFIX."manufacturers");
		while ($db->moveNext())
		{
			$ms[$db->col["id"]] = $db->col["manufacturer_name"];
		}

		$cid = false;
		if ($productCategory > 0)
		{
			$cid = $productCategory;
		}

//		//TODO: Optimize this?
//		$db->query('SELECT cid, name
//		FROM '.DB_PREFIX.'catalog c
//		'.($cid ? 'WHERE cid = '.$cid : '')
//		);
//		$catalogMap = array();
//		while ($row = $db->moveNext())
//		{
//			$catalogMap[$row['cid']] = $row['name'];
//		}

		echo '<?xml version="1.0" encoding="UTF-8"?>'."\r\n";
		echo '<opml version="1.0">'."\r\n";
		echo '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">'."\r\n";
		echo '<channel>'."\r\n";
		echo '<title>'.$settings->get('GlobalSiteName').'</title>'."\r\n";
		echo '<link>'.$settings->get('GlobalHttpUrl').'</link>'."\r\n";
		echo '<description>'.$settings->get('GlobalSiteName').' Products</description>'."\r\n";

		$where = 'p.is_visible = "Yes" AND p.call_for_price = "No"' . ($cid ? ' AND p.cid = ' . $cid : '') . ' AND ((p.inventory_control IN("Yes", "AttrRuleExc") AND p.stock > 0) OR p.inventory_control = "No")';
		$sql = "SELECT ?
			FROM " . DB_PREFIX . "products p
			LEFT JOIN " . DB_PREFIX . "catalog c ON p.cid = c.cid
			WHERE " . $where . "
			ORDER BY p.title";

		$totalRecordCount = 0;
		if ($productCount > 0)
		{
			$totalRecordCount = $batchSize = $productCount;
		}
		else
		{
			$countSql = str_replace('SELECT ?', 'SELECT COUNT(0) as totalrecords', $sql);
			$db->query($countSql);
			if ($db->moveNext())
			{
				$totalRecordCount = intval($db->col['totalrecords']);
			}
			$batchSize = 10000;
		}

		$batchCount = 0;
		$batchSizeCount = ceil($totalRecordCount / $batchSize);

		while ($batchCount < $batchSizeCount)
		{
			$imageTypes = array( "jpg", "gif", "jpeg", "png");

			$batchSql = 'SELECT pi.filename, pi.type, p.*, c.*, IF (p.url_custom = "", p.url_default, p.url_custom) AS product_url, c.name as cat_name, p.description as product_description
				FROM products p
				LEFT JOIN catalog c ON p.cid = c.cid
				LEFT JOIN products_images pi ON pi.pid = p.pid
				WHERE p.is_visible = "Yes" AND p.call_for_price = "No" AND ((p.inventory_control IN("Yes", "AttrRuleExc") AND p.stock > 0) OR p.inventory_control = "No")
				AND p.pid IN (SELECT * FROM (SELECT pid FROM products LIMIT ' . ($batchCount * $batchSize) . ', ' . $batchSize . ') as t)
				ORDER BY p.title';

			$db->query($batchSql);

			// let's format our result here
			$records = array();
			while ($db->moveNext())
			{
				$records[$db->col["pid"]]['product_url'] = UrlUtils::getProductUrl($db->col["product_url"], $db->col["pid"], $db->col["cid"]);
				$records[$db->col["pid"]]['name'] = $db->col["title"];
				$records[$db->col["pid"]]['pid'] = $db->col["product_id"];
				$records[$db->col["pid"]]['upc'] = $db->col["product_upc"];
				$records[$db->col["pid"]]['gtin'] = $db->col["product_gtin"];
				$records[$db->col["pid"]]['mpn'] = $db->col["product_mpn"];
				$records[$db->col["pid"]]['weight'] = $db->col["weight"];
				$records[$db->col["pid"]]['tax_class_id'] = $db->col["tax_class_id"];
				$records[$db->col["pid"]]['is_taxable'] = $db->col["is_taxable"] == "Yes";
				$records[$db->col["pid"]]['free_shipping'] = $db->col["free_shipping"] == "Yes";

				$records[$db->col["pid"]]['estimated_shipping_price'] = $db->col["estimated_shipping_price"];

				$brand = (array_key_exists($db->col["manufacturer_id"], $ms)) ? $ms[$db->col["manufacturer_id"]] : null;
				$records[$db->col["pid"]]['brand'] = is_null($brand) || empty($brand) ? "Unknown" : $brand;
				$records[$db->col["pid"]]['condition'] = $db->col['google_item_condition'];
				$records[$db->col["pid"]]['product_type'] = $db->col['cat_name'] ? $db->col['cat_name'] : 'Category';

				$records[$db->col["pid"]]['google_product_type'] = $db->col["google_product_type"];

				$records[$db->col["pid"]]['gender'] = $db->col['google_gender'];
				$records[$db->col["pid"]]['age_group'] = $db->col['google_age_group'];
				$records[$db->col["pid"]]['color'] = $db->col['google_color'];
				$records[$db->col["pid"]]['size'] = $db->col['google_size'];
				$records[$db->col["pid"]]['material'] = $db->col['google_material'];
				$records[$db->col["pid"]]['pattern'] = $db->col['google_pattern'];

				$records[$db->col["pid"]]['online_only'] = $db->col['google_online_only'];

				$availability = $db->col['google_availability'];

				if ($availability == '' || $availability == 'inventory_control')
				{
					$records[$db->col["pid"]]['availability'] = 'in stock';
				}

				$description = trim($db->col["product_description"]) == "" ? $db->col["title"] : $db->col["product_description"];
				$description = str_replace("!", ".", $description);
				$records[$db->col["pid"]]['description'] = str_replace("  ", "", $description);

				$records[$db->col["pid"]]['price'] = $db->col["price"];
				$records[$db->col["pid"]]['sale_price'] = $db->col["price2"];

				// primary image
				$image_url = "";
				if ($db->col["image_location"] == "Web" && $db->col["image_url"] != "")
				{
					$records[$db->col["pid"]]['image_url'] = $db->col["image_url"];
				}
				else
				{
					foreach ($imageTypes as $ext)
					{
						$imageFileName = ImageUtility::imageFileName($db->col['product_id']);

						if (file_exists($settings->get('GlobalServerPath') . '/images/products/' . $imageFileName . '.' . $ext))
						{
							$image_url = $this->fr_normalize($settings->get('GlobalHttpUrl') . '/images/products/' . $imageFileName . '.' . $ext); //normalize again
							break;
						}
					}
				}

				$records[$db->col["pid"]]['image_url'] = trim($image_url) == "" ? $this->fr_normalize($settings->get('GlobalHttpUrl').'/'.$noImageUrl) : $image_url;

				// for secondary images
				if (file_exists($settings->get('GlobalServerPath') . '/images/products/secondary/' . $db->col['filename'] . '.' . $db->col['type']))
				{
					$records[$db->col["pid"]]['secondary_images'][] = $this->fr_normalize($settings->get('GlobalHttpUrl') . '/images/products/secondary/' . $db->col['filename'] . '.' . $db->col['type']);
				}

				// If your product doesn't have a GTIN and brand, or MPN and brand
				if (($records[$db->col["pid"]]['brand'] == 'Unknown' && empty($records[$db->col["pid"]]['gtin'])) || ($records[$db->col["pid"]]['brand'] == 'Unknown' && empty($records[$db->col["pid"]]['mpn'])))
				{
					$records[$db->col["pid"]]['identifier_exists'] = 'no';
				}
				else
				{
					$records[$db->col["pid"]]['identifier_exists'] = 'yes';
				}
			}

			foreach ($records AS $key => $record)
			{
				echo "<item>\r\n";
				echo "<title>" . $this->st_normalize($record['name']) . "</title>\r\n";
				echo "<link>" . $this->st_normalize($record['product_url']) . "</link>\r\n";
				echo "<description>" . $this->st_normalize($record['description']) . "</description>\r\n";
				echo "<g:id>" . $this->st_normalize($record['pid']) . "</g:id>\r\n";

				if ($record['identifier_exists'] == 'no') echo "<g:identifier_exists>" . $this->st_normalize($record['identifier_exists']) . "</g:identifier_exists>\r\n";

				echo "<g:upc>" . $this->st_normalize($record['upc']) . "</g:upc>\r\n";
				echo "<g:gtin>" . $this->st_normalize($record['gtin']) . "</g:gtin>\r\n";
				echo "<g:mpn>" . $this->st_normalize($record['mpn']) . "</g:mpn>\r\n";
				echo "<g:image_link>" . $this->st_normalize($record['image_url']) . "</g:image_link>\r\n";

				// our secondary images
				foreach($record['secondary_images'] AS $secondaryImage)
				{
					echo "<g:additional_image_link>" . $this->st_normalize($secondaryImage) . "</g:additional_image_link>\r\n";
				}

				echo "<g:price>" . $this->st_normalize(number_format($record['price'], 2, ".", "")) . "</g:price>\r\n";
				echo "<g:sale_price>" . $this->st_normalize(number_format($record['sale_price'], 2, ".", "")) . "</g:sale_price>\r\n";

				if ($record['weight'] > 0) echo "<g:weight>" . $this->st_normalize($record['weight'].($settings->get('LocalizationWeightUnits') == "kg" ? " kg" : " lb")) . "</g:weight>\r\n";

				//added for google base ver
				echo "<g:brand>" . $this->st_normalize($record['brand']) . "</g:brand>\r\n";
				echo "<g:condition>" . $this->st_normalize($record['condition']) . "</g:condition>\r\n";
				echo "<g:product_type>" . $this->st_normalize($record['product_type']) . "</g:product_type>\r\n";
				echo "<g:google_product_category>" . $this->st_normalize($record['google_product_type']) . "</g:google_product_category>\r\n";
				echo "<g:location>" . $this->st_normalize($store_location) . "</g:location>\r\n";

				if ($expdate != '') echo "<g:expiration_date>" . $this->st_normalize($expdate) . "</g:expiration_date>\r\n";
				if ($record['gender'] != '') echo "<g:gender>" . $this->st_normalize($record['gender']) . "</g:gender>\r\n";
				if ($record['age_group'] != '') echo "<g:age_group>" . $this->st_normalize($record['age_group']) . "</g:age_group>\r\n";
				if ($record['color'] != '') echo "<g:color>" . $this->st_normalize($record['color']) . "</g:color>\r\n";
				if ($record['size'] != '') echo "<g:size>" . $this->st_normalize($record['size']) . "</g:size>\r\n";
				if ($record['material'] != '') echo "<g:material>" . $this->st_normalize($record['material']) . "</g:material>\r\n";
				if ($record['pattern'] != '') echo "<g:pattern>" . $this->st_normalize($record['pattern']) . "</g:pattern>\r\n";
				if ($record['online_only'] == 'y') echo "<g:online_only>" . $this->st_normalize($record['online_only']) . "</g:online_only>\r\n";

				echo "<g:availability>" . $this->st_normalize($record['availability']) . "</g:availability>";

				if ($record['free_shipping'])
				{
					echo "<g:shipping><g:country>" . $defaultTaxCountry . "</g:country><g:region>*</g:region><g:price>0.00</g:price><g:service>Standard</g:service></g:shipping>\r\n";
				}
				else if ($record['estimated_shipping_price'] >= 0)
				{
					echo "<g:shipping><g:country>" . $defaultTaxCountry . "</g:country><g:region>*</g:region><g:price>" . myNum($record['estimated_shipping_price']) . "</g:price><g:service>Standard</g:service></g:shipping>\r\n";
				}

				if ($exportTaxes && $record['is_taxable'])
				{
					if (isset($tax_classes[$record['tax_class_id']]))
					{
						echo $tax_classes[$record['tax_class_id']];
					}
				}
				else if ($exportTaxes)
				{
					echo "<g:tax><g:country>" . $defaultTaxCountry . "</g:country><g:region>*</g:region><g:rate>0.00</g:rate><g:tax_ship>" . ($settings->get('ShippingTaxable')?"y":"n") . "</g:tax_ship></g:tax>\r\n";
				}

				//end added
				echo "</item>\r\n";
			}
			$batchCount++;
		}

		echo '</channel>'."\r\n";
		echo '</rss>'."\r\n";
		echo '</opml>'."\r\n";

		_session_write_close();
		$db->done();

		exit();
	}

	/**
	 * @param $s
	 * @return string
	 */
	private function fr_normalize($s)
	{
		$ss = str_replace("\r", " ", $s);
		$ss = str_replace("\n", " ", $ss);
		$ss = str_replace("\t", " ", $ss);
		$ss = str_replace("\0", " ", $ss);
		$ss = htmlentities(trim(stripslashes($ss)));
		$sss = "";
		$whitespace = false;
		for($i=0; $i<strlen($ss); $i++){
			$sss = $sss.((($ss[$i] == " " && !$whitespace) || ($ss[$i] != " ")) ? $ss[$i] : "");
			$whitespace = $ss[$i] == " ";
		}
		return $sss;
	}

	/**
	 * @param $string
	 * @return string
	 */
	private function st_normalize($string)
	{
		$string_final = strip_tags($string);
		$string_final = htmlspecialchars($string_final);
		return utf8_encode($string_final);
	}
}

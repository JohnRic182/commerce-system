<?php

/**
 * Class Admin_Service_ImageOptimizer
 */
class Admin_Service_ImageOptimizer
{
	/**
	 * @param $fileName
	 * @param int $quality
	 * @return bool
	 */
	public function optimize($fileName, $quality = 75)
	{
		$imageSizeInfo = getimagesize($fileName);

		if ($imageSizeInfo && isset($imageSizeInfo[2]) && $imageSizeInfo[2] == IMAGETYPE_JPEG)
		{
			$image = @imagecreatefromjpeg($fileName);

			if ($image)
			{
				@imageinterlace($image, true);
				@imagejpeg($image, $fileName, $quality);
				@imagedestroy($image);

				return true;
			}
		}

		return false;
	}
}
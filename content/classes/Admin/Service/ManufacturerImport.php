<?php

/**
 * Class Admin_Service_ManufactureImport
 */
class Admin_Service_ManufacturerImport extends Admin_Service_Import
{
	/** @var array $fields */
	protected $fields = array(
		'__skip' => array(
			'title' => 'Skip',
			'type' => '',
			'required' => false,
			'autoAssign' => array()
		),
		'manufacturer_code' => array(
			'title' => 'Manufacturer code',
			'type' => self::TYPE_STRING,
			'required' => true,
			'autoAssign' => array('key', 'id', 'code', 'manufacturerkey', 'manufacturerid', 'manufacturercode')
		),
		'manufacturer_name' => array(
			'title' => 'Manufacturer name',
			'type' => self::TYPE_STRING,
			'required' => true,
			'autoAssign' => array('name', 'title', 'manufacturername', 'manufacturertitle')
		),
		'is_visible' => array(
			'title' => 'Is visible',
			'type' => self::TYPE_BOOLEAN,
			'subtype' => 'numeric',
			'required' => false,
			'default' => 'Yes',
			'autoAssign' => array('visible', 'isvisible', 'active', 'isactive', 'available', 'isavailable')
		),
		'url_custom' => array(
			'title' => 'Custom URL',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('url', 'customurl', 'urlcustom')
		),
		'meta_title' => array(
			'title' => 'Meta title',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('metatitle')
		),
		'meta_description' => array(
			'title' => 'Meta description',
			'type' => self::TYPE_STRING,
			'required' => false,
			'default' => '',
			'autoAssign' => array('metadescription')
		),
	);

	/**
	 * Find item by current data
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return array|bool
	 */
	protected function findItem($data, $params = null)
	{
		/** @var DataAccess_ManufacturerRepository $repository */
		$repository = $this->repository;

		return $repository->getByCode($data['manufacturer_code']);
	}

	/**
	 * Modify data before insert
	 *
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	protected function beforeInsert($data, $params = null)
	{
		$makeVisible = is_array($params) && isset($params['makeVisible']) ? $params['makeVisible'] : false;
		if (!isset($data['is_visible'])) $data['is_visible'] = $makeVisible ? '1' : '0';

		return $data;
	}

	/**
	 * Modify data before update
	 *
	 * @param $item
	 * @param $data
	 * @param $params
	 *
	 * @return mixed
	 */
	protected function beforeUpdate($item, $data, $params = null)
	{
		$makeVisible = is_array($params) && isset($params['makeVisible']) ? $params['makeVisible'] : false;
		$makeVisibleExisting = is_array($params) && isset($params['makeVisibleExisting']) ? $params['makeVisibleExisting'] : false;

		if (!isset($data['is_visible']) && $makeVisibleExisting) $data['is_visible'] = $makeVisible ? '1' : '0';

		return $data;
	}
}
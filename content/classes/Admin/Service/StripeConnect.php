<?php

/**
 * Class Admin_Service_StripeConnect
 */
class Admin_Service_StripeConnect
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;
	protected $baseUrl;
	protected $isTest;

	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->isTest = defined('STRIPE_CONNECT_TEST_MODE') && STRIPE_CONNECT_TEST_MODE;
		$this->baseUrl = defined('STRIPE_CONNECT_TEST_MODE') && STRIPE_CONNECT_TEST_MODE
			? 'https://account-staging.pinnaclecart.com/stripe-connect/'
			: 'https://account.pinnaclecart.com/stripe-connect/';
		$this->settings = $settings;
	}

	public function finishStripeConnect($responseStr = null)
	{
		if ($responseStr === null)
		{
			$license_id = '';
			$licenseParts = explode('-', LICENSE_NUMBER);
			if (count($licenseParts) > 1)
			{
				$license_id = $licenseParts[1];
			}
			$params = array(
				'license_id' => $license_id,
			);
			$this->hashParams($params);

			$responseStr = $this->doGet('token', $params);
		}

		$response = @json_decode($responseStr);

		if (isset($response->access_token) && trim($response->access_token) != '')
		{
			$stripeConnect = new Payment_Stripe_Connect($this->settings);
			$stripeConnect->connection($response->stripe_publishable_key, $response->access_token);
			return true;
		}
		else if (isset($response->status) && $response->status == 0)
		{
			return $response->message;
		}

		return false;
	}

	protected function hashParams(array &$data)
	{
		ksort($data);
		$message = str_replace('+', '%20', http_build_query($data));
		$hash = base64_encode(hash_hmac('sha256', $message, $this->isTest ? 'SUPER SECRET KEY' : 'L30SD]N}Lj<.h17WQYlqhMM5;!lwzAMK%[U7[)#o{438 G!&(>~=acppBYrj?.~c' , true));

		$data['signature'] = $hash;
	}

	protected function doGet($url, array $reqParams = array(), array $reqHeaders = array())
	{
		$url = $this->baseUrl . $url;
		$headers = array(
			'X-Access-Token: '.'Bearer '.$this->settings->get('AccountToken'),
		);
		foreach ($reqHeaders as $key => $value)
		{
			$headers[] = $key . ': ' . $value;
		}
		$headers[] = 'Content-Length: ' . strlen('');

		$query = '';
		if (count($reqParams) > 0)
		{
			$query = '?'.http_build_query($reqParams);
		}

		$params = array();
		$params[CURLOPT_RETURNTRANSFER] = true;
		$params[CURLOPT_URL] = $url . $query;
		$params[CURLOPT_HTTPHEADER] = $headers;

		// debug
		$params[CURLOPT_VERBOSE] = false;

		// return headers
		$params[CURLOPT_HEADER] = false;

		$request = '';
		$request .= 'GET ';
		$request .= $params[CURLOPT_URL] . ' HTTP/1.1' . "\r\n";
		foreach ($headers as $header)
		{
			$request .= $header . "\r\n";
		}

		$ch = curl_init();
		curl_setopt_array($ch, $params);

		$response = curl_exec($ch);

		@curl_close($ch);

		return $response;
	}
}
<?php
/**
 * Class Admin_Service_ProductCopy
 */
class Admin_Service_ProductCopy
{
    /** @var DataAccess_BaseRepository $repository */
    protected $repository;

    /** @var DataAccess_ProductAttributeRepository */
    protected $productAttributeRepository = null;

    /** @var DataAccess_ProductVariantRepository */
    protected $productVariantRepository = null;

    /** @var DataAccess_ProductQuantityDiscountRepository */
    protected $productQuantityDiscount = null;

	/** @var DataAccess_ProductImageRepository */
	protected $productImageRepository = null;

	/** @var string our prefix for copying product */
	public $copyPrefix = 'copy';

    /**
     * Class constructor
     *
     * @param DataAccess_BaseRepository $repository
     * @param null $params
     */
    public function __construct(DataAccess_BaseRepository $repository, $params = null)
    {
        $this->repository = $repository;

        if (!is_null($params) && is_array($params)) {
            $this->productAttributeRepository = $params['productAttributeRepository'];
            $this->productVariantRepository = $params['productVariantRepository'];
            $this->productQuantityDiscount = $params['productQuantityDiscount'];
	        $this->productImageRepository = $params['productImageRepository'];
        }
    }

    /**
     * Execute our copy
     * @param $params
     */
    public function copy($params)
    {
        $request = $params->get('copy');
        $selectedFields = (isset($request['selected-fields'])) ? $request['selected-fields'] : self::getDefaultFields(true);
        $productId = $request['id'];

        // for other product fields
        if(isset($request['advance_settings']) && $request['advance_settings'])
        {
            $selectedFields = array_merge($selectedFields, self::getAdvanceSettingsFields(true));
        }

        if(isset($request['seo']) && $request['seo'])
        {
            $selectedFields = array_merge($selectedFields, self::getSeoFields(true));
        }

        if(isset($request['search_keywords']) && $request['search_keywords'])
        {
            $selectedFields = array_merge($selectedFields, self::getSearchKeywordsFields(true));
        }

        if(isset($request['product_promotions']) && $request['product_promotions'])
        {
            $selectedFields = array_merge($selectedFields, self::getProductPromotionFields(true));
        }

        // execute our copy here
        $newProductId = $this->repository->copyProduct($productId, $selectedFields, $this->copyPrefix);

        // for our related data
	    if(isset($request['images']) && $request['images'])
	    {
		    $this->productImageRepository->copyProductImages($productId, $newProductId, $this->copyPrefix);
	    }

        if(isset($request['attributes']) && $request['attributes'])
        {
            $this->productAttributeRepository->copyProductAttributes($productId, $newProductId);
        }

        if(isset($request['variants']) && $request['variants'])
        {
            $this->productVariantRepository->copyProductVariants($productId, $newProductId);
        }

        if(isset($request['quantity_discounts']) &&  $request['quantity_discounts'])
        {
            $this->productQuantityDiscount->copyProductQuantityDiscounts($productId, $newProductId);
        }
    }

    /**
     * Returns our product default fields
     * @param bool $fieldsOnly
     * @return array
     */
    public static function getDefaultFields($fieldsOnly = false)
    {
        $fields = array(
            array('field' => 'title', 'label' => 'Product Name'),
	        array('field' => 'product_id', 'label' => 'Product ID'),
            array('field' => 'price', 'label' => 'Price Per Item'),
            array('field' => 'price2', 'label' => 'Sale Price'),
            array('field' => 'cid', 'label' => 'Primary Category'),
//            array('field' => 'is_visible', 'label' => 'Visible'),
            array('field' => 'overview', 'label' => 'Overview'),
            array('field' => 'description', 'label' => 'Description'),
        );
        return (!$fieldsOnly) ? $fields : array_column($fields, 'field');
    }

    /**
     * Returns our product advance settings fields
     * @param bool $fieldsOnly
     * @return array
     */
    public static function getAdvanceSettingsFields($fieldsOnly = false)
    {
        $fields = array(
            array('field' => 'product_type', 'label' => 'Product Type'),
	        array('field' => 'digital_product_file', 'label' => 'Enter path to digital product file'),
            array('field' => 'priority', 'label' => 'Order of Appearance'),
            array('field' => 'weight', 'label' => 'Item Weight (lbs)'),
            array('field' => 'dimension_width', 'label' => 'Item Width'),
            array('field' => 'dimension_length', 'label' => 'Item Length'),
            array('field' => 'dimension_height', 'label' => 'Item Height'),
            array('field' => 'free_shipping', 'label' => 'Free Shipping'),
            array('field' => 'is_taxable', 'label' => 'Taxable Product'),
            array('field' => 'tax_class_id', 'label' => 'Select Tax Class'),
            array('field' => 'min_order', 'label' => 'Min Order'),
            array('field' => 'max_order', 'label' => 'Max Order'),
            array('field' => 'call_for_price', 'label' => 'Call for price'),
            array('field' => 'is_hotdeal', 'label' => 'Mark product as a "Hot Deal"'),
            array('field' => 'is_home', 'label' => 'Show product on home page'),
            array('field' => 'manufacturer_id', 'label' => 'Manufacturer'),
            array('field' => 'product_mpn', 'label' => 'Manufacturer Product Number'),
            array('field' => 'product_sku', 'label' => 'SKU (Stock Keeping Unit)'),
            array('field' => 'product_gtin', 'label' => 'Barcode (UPC, GTIN, ISBN)'),
            array('field' => 'inventory_control', 'label' => 'No inventory tracking'),
            array('field' => 'zoom_option', 'label' => 'Image zooming'),
	        array('field' => 'youtube_link', 'label' => 'YouTube Link'),
        );
        return (!$fieldsOnly) ? $fields : array_column($fields, 'field');
    }

    /**
     * Returns our product seo fields
     * @param bool $fieldsOnly
     * @return array
     */
    public static function getSeoFields($fieldsOnly = false)
    {
        $fields = array(
            array('field' => 'meta_title', 'label' => 'Meta Title'),
            array('field' => 'url_custom', 'label' => 'Product URL'),
            array('field' => 'meta_description', 'label' => 'Meta Description'),
        );
        return (!$fieldsOnly) ? $fields : array_column($fields, 'field');
    }

    /**
     * Returns our product search keywords fields
     * @param bool $fieldsOnly
     * @return array
     */
    public static function getSearchKeywordsFields($fieldsOnly = false)
    {
        $fields = array(
            array('field' => 'search_keywords', 'label' => 'Search Keywords'),
        );
        return (!$fieldsOnly) ? $fields : array_column($fields, 'field');
    }

    /**
     * Returns our product promotion fields
     * @param bool $fieldsOnly
     * @return array
     */
    public static function getProductPromotionFields($fieldsOnly = false)
    {
        $fields = array(
            array('field' => 'gift_quantity', 'label' => 'Minimum number of this product (X)'),
        );
        return (!$fieldsOnly) ? $fields : array_column($fields, 'field');
    }
}
<?php

require_once dirname(__FILE__) . '../../../vendors/qr/qrlib.php';

/**
 * Class Admin_Service_QRCodes
 */
class Admin_Service_QRCodes
{
	/** @var DataAccess_SettingsRepository DataAccess_SettingsRepository */
	protected $settings = null;

	protected $errorCheckLevel = 'L';
	protected $defaultPixelSize = 4;

	protected $defaultMarginSize = 2;
	protected $fileCachePath = 'content/cache/qr/';
	protected $fileCacheUrl = 'content/cache/qr/';

	protected $lastRenderedFile = false;

	protected $imageTypes = array(
		IMAGETYPE_PNG => 'png',
		IMAGETYPE_JPEG => 'jpg',
		IMAGETYPE_GIF => 'gif',
	);

	/**
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DataAccess_SettingsRepository $settings)
	{
		$this->settings = $settings;
		$this->errorCheckLevel = $settings->get('qr_error_check');
		$this->defaultPixelSize = $settings->get('qr_default_size');
	}

	/**
	 * Set QR code images path
	 * @param string $path
	 */
	public function setFileCachePath($path)
	{
		$this->fileCachePath = $path;
	}

	/**
	 * Return QR code images path
	 *
	 * @return string
	 */
	public function getFileCachePath()
	{
		return $this->fileCachePath;
	}

	/**
	 * Set QR code images url
	 *
	 * @param string $url
	 */
	public function setFileCacheUrl($url)
	{
		$this->fileCacheUrl = $url;
	}

	/**
	 * Return QR code images url
	 *
	 * @return string
	 */
	public function getFileCacheUrl()
	{
		return $this->fileCacheUrl;
	}

	/**
	 * Returns last rendered file path
	 *
	 * @return mixed
	 */
	public function getLastRenderedFile()
	{
		return $this->lastRenderedFile;
	}

	/**
	 * Generate QR Code
	 *
	 * @param string $type // code type prefix: example a=add product to a cart, p=go to product page, c=category page, u=custom
	 * @param string $data // code data
	 * @param int $id // data entry id
	 * @param boolean $force
	 * @param mixed $errorCheckLevel // L,M.H,Q
	 * @param mixed $defaultPixelSize // 4..12
	 * @param int $imageType // IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_PNG
	 *
	 * @return string
	 */
	public function render($type, $data, $id, $force = false, $errorCheckLevel = false, $defaultPixelSize = false, $imageType = IMAGETYPE_PNG)
	{
		// get file name and path
		$fileName = $this->getFileName($type, $id, $errorCheckLevel, $defaultPixelSize, $imageType);
		$fullFileName = $this->getFileCachePath().$fileName;
		// if forced - delete current
		if ($force && is_file($fullFileName)) unlink($fullFileName);

		// if file does noe exist - create a new one
		if (!is_file($fullFileName))
		{
			switch ($imageType)
			{
				case IMAGETYPE_PNG : QRcode::png($data, $fullFileName, $errorCheckLevel, $defaultPixelSize, $this->defaultMarginSize); break;
				case IMAGETYPE_JPEG : QRcode::jpg($data, $fullFileName, $errorCheckLevel, $defaultPixelSize, $this->defaultMarginSize); break;
				case IMAGETYPE_GIF : QRcode::gif($data, $fullFileName, $errorCheckLevel, $defaultPixelSize, $this->defaultMarginSize); break;
			}
		}

		$this->lastRenderedFile = $fullFileName;

		return $this->getFileUrl(
			$type, $id,
			$errorCheckLevel ? $errorCheckLevel : $this->errorCheckLevel,
			$defaultPixelSize ? $defaultPixelSize : $this->defaultPixelSize,
			$imageType
		);
	}

	/**
	 * Render QR Code file with product URL and returns its URL
	 *
	 * @param $data
	 * @param $product_id
	 * @param bool $force
	 * @param bool $errorCheckLevel
	 * @param bool $defaultPixelSize
	 * @param int $imageType
	 * @param bool $campaignId
	 *
	 * @return string
	 */
	public function renderProductUrlCode($data, $product_id, $force = false, $errorCheckLevel = false, $defaultPixelSize = false, $imageType = IMAGETYPE_PNG, $campaignId = false)
	{
		return $this->render(
			'p', $data, $product_id.($campaignId ? '-'.$campaignId : ''), $force,
			$errorCheckLevel ? $errorCheckLevel : $this->errorCheckLevel,
			$defaultPixelSize ? $defaultPixelSize : $this->defaultPixelSize,
			$imageType
		);
	}

	/**
	 * Render QR Code file with product-add-to-cart URL and returns its URL
	 *
	 * @param $data
	 * @param $product_id
	 * @param bool $force
	 * @param bool $errorCheckLevel
	 * @param bool $defaultPixelSize
	 * @param int $imageType
	 * @param bool $campaignId
	 *
	 * @return string
	 */
	public function renderProductAddToCartCode($data, $product_id, $force = false, $errorCheckLevel = false, $defaultPixelSize = false, $imageType = IMAGETYPE_PNG, $campaignId = false)
	{
		return $this->render(
			'a', $data, $product_id.($campaignId ? '-'.$campaignId : ''), $force,
			$errorCheckLevel ? $errorCheckLevel : $this->errorCheckLevel,
			$defaultPixelSize ? $defaultPixelSize : $this->defaultPixelSize,
			$imageType
		);
	}

	/**
	 * Render QR Code file with category URL and returns its URL
	 *
	 * @param $data
	 * @param $category_id
	 * @param bool $force
	 * @param bool $errorCheckLevel
	 * @param bool $defaultPixelSize
	 * @param int $imageType
	 * @param bool $campaignId
	 *
	 * @return string
	 */
	public function renderCategoryUrlCode($data, $category_id, $force = false, $errorCheckLevel = false, $defaultPixelSize = false, $imageType = IMAGETYPE_PNG, $campaignId = false)
	{
		return $this->render(
			'c', $data, $category_id.($campaignId ? '-'.$campaignId : ''), $force,
			$errorCheckLevel ? $errorCheckLevel : $this->errorCheckLevel,
			$defaultPixelSize ? $defaultPixelSize : $this->defaultPixelSize,
			$imageType
		);
	}

	/**
	 * Render custom QR Code and return its URL
	 *
	 * @param $data
	 * @param bool $force
	 * @param bool $errorCheckLevel
	 * @param bool $defaultPixelSize
	 * @param int $imageType
	 *
	 * @return string
	 */
	public function renderCustomCode($data, $force = false, $errorCheckLevel = false, $defaultPixelSize = false, $imageType = IMAGETYPE_PNG)
	{
		return $this->render(
			'u', $data, md5($data), $force,
			$errorCheckLevel ? $errorCheckLevel : $this->errorCheckLevel,
			$defaultPixelSize ? $defaultPixelSize : $this->defaultPixelSize,
			$imageType
		);
	}

	/**
	 * Get QR Code file name
	 *
	 * @param $type
	 * @param $id
	 * @param bool $errorCheckLevel
	 * @param bool $defaultPixelSize
	 * @param int $imageType
	 *
	 * @return string
	 */
	public function getFileName($type, $id, $errorCheckLevel = false, $defaultPixelSize = false, $imageType = IMAGETYPE_PNG)
	{
		return
			$type.'-'.
			$id.'-'.
			($errorCheckLevel ? $errorCheckLevel : $this->errorCheckLevel).'-'.
			($defaultPixelSize ? $defaultPixelSize : $this->defaultPixelSize).'.'.$this->imageTypes[$imageType];
	}

	/**
	 * Return QR Code file URL
	 *
	 * @param $type
	 * @param $id
	 * @param bool $errorCheckLevel
	 * @param bool $defaultPixelSize
	 * @param int $imageType
	 *
	 * @return string
	 */
	public function getFileUrl($type, $id, $errorCheckLevel = false, $defaultPixelSize = false, $imageType = IMAGETYPE_PNG)
	{
		return
			$this->getFileCacheUrl().
			$type.'-'.
			$id.'-'.
			($errorCheckLevel ? $errorCheckLevel : $this->errorCheckLevel).'-'.
			($defaultPixelSize ? $defaultPixelSize : $this->defaultPixelSize).'.'.$this->imageTypes[$imageType];
	}
}
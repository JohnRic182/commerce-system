<?php

/**
 * Class Admin_ViewController
 */
class Admin_ViewController extends Controller
{
	/** @var Ddm_AdminView */
	protected $view = null;

	/**
	 * @param null $db
	 * @param null $settings
	 * @param null $view_instance
	 */
	public function __construct($db = null, $settings = null, $view_instance = null)
	{
		$view = $view_instance;
		if (is_null($view))
			$view = Ddm_AdminView::getInstance();

		parent::__construct($db, $settings, $view);
	}
}

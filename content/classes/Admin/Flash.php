<?php
/**
 * Class Admin_Flash
 */

// TODO: get rid of this class in future. At this moment it is wrapper for legacy compatibility
class Admin_Flash
{
	const TYPE_SUCCESS = 'success';
	const TYPE_ERROR = 'error';
	const TYPE_INFO = 'info';

	/** @var Model_FlashMessages $flashMessages */
	protected static $flashMessages = null;

	/**
	 * Get flash messages class instance
	 *
	 * @return Model_FlashMessages
	 */
	protected static function getFlashMessages()
	{
		if (is_null(self::$flashMessages))
		{
			global $session;
			if ($session === null)
			{
				$session = new Framework_Session(new Framework_Session_WrapperStorage());
				$session->start();
			}

			self::$flashMessages = new Model_FlashMessages($session, '');
		}

		return self::$flashMessages;
	}

	/**
	 * Set flash message
	 *
	 * @param $message
	 * @param string $status
	 */
	public static function setMessage($message, $status = Admin_Flash::TYPE_SUCCESS)
	{
		self::getFlashMessages()->setMessage(trans($message), $status, null);
	}

	/**
	 * Check does flash message exist
	 *
	 * @return bool
	 */
	public static function hasMessages()
	{
		return self::getFlashMessages()->hasMessages();
	}

	/**
	 * Returns flash message
	 *
	 * @param bool $reset
	 * @return mixed
	 */
	public static function getMessages($reset = true)
	{
		return self::getFlashMessages()->getMessages(null, $reset);
	}
}
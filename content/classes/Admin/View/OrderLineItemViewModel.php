<?php

/**
 * Class Admin_View_OrderLineItemViewModel
 */
class Admin_View_OrderLineItemViewModel
{
	public $id;
	public $productId;
	public $productSubId;
	public $shipmentId;
	public $giftcertid;
	public $title;
	public $options;
	public $price;
	public $quantity;
	public $subtotal;
	public $digitalProductKey = false;
	public $enableRecurringBilling = false;
	public $fulfillmentStatus;
	public $quantityToFulfill;
	public $isTaxable;
	public $isTaxExempt;
	public $taxDescription;
	public $taxRate;

	public $recurringProfileId = false;
	public $recurringProfileStatus = false;
	public $recurringTrialDescription;
	public $recurringBillingDescription;

	public function __construct(Model_LineItem $lineItem, RecurringBilling_Model_RecurringProfile $recurringProfile = null, &$msg)
	{
		$this->id = $lineItem->getId();
		$this->productId = $lineItem->getProductId();
		$this->productSubId = $lineItem->getProductSubId();
		$this->giftcertid = 0;
		$this->shipmentId = $lineItem->getShipmentId();
		$this->title = $lineItem->getTitle();
		$this->options = $lineItem->getOptions();
		if ($lineItem->getProductType() == 'Digital')
		{
			$this->digitalProductKey = $lineItem->getDigitalProductKey();
		}
		$this->price = $lineItem->getFinalPrice();
		$this->quantity = $lineItem->getFinalQuantity();
		$this->subtotal = $lineItem->getSubtotal(false);
		$this->fulfillmentStatus = $lineItem->getFulfillmentStatus();
		$this->quantityToFulfill = $lineItem->getQuantityToFulfill();
		$this->isTaxable = $lineItem->getIsTaxable() == 'Yes';
		$this->isTaxExempt = $lineItem->getTaxExempt() == 1;
		$this->taxDescription = $lineItem->getTaxDescription();
		$this->taxRate = $lineItem->getTaxRate();

		if ($recurringProfile)
		{
			$this->recurringProfileId = $recurringProfile->getProfileId();
			$this->recurringProfileStatus = $recurringProfile->getStatus();
		}

		if ($this->enableRecurringBilling = $lineItem->getEnableRecurringBilling())
		{
			$recurringBillingData = $lineItem->getRecurringBillingData();

			$this->recurringTrialDescription = View_OrderLineItemsViewModel::getRecurringTrialDescription($recurringBillingData, $lineItem->getAdminQuantity(), $msg);
			$this->recurringBillingDescription = View_OrderLineItemsViewModel::getRecurringBillingDescription($recurringBillingData, $lineItem->getAdminQuantity(), $msg);
		}
	}

	public function setGiftCertId($id){
		$this->giftcertid = $id;
	}
}
<?php

class Admin_View_FulfillmentItemViewModel
{
	public $id;
	public $lineItemId;
	public $productId;
	public $productSubId;
	public $title;
	public $options;
	public $quantity;
	public $createdAt;
	public $updatedAt;

	public function __construct(Model_FulfillmentItem $fulfillmentItem, Model_LineItem $lineItem = null)
	{
		$this->id = $fulfillmentItem->getId();
		$this->lineItemId = $fulfillmentItem->getLineItemId();
		$this->quantity = $fulfillmentItem->getQuantity();
		$this->createdAt = $fulfillmentItem->getCreatedAt();
		$this->updatedAt = $fulfillmentItem->getUpdatedAt();

		if ($lineItem)
		{
			$this->productId = $lineItem->getProductId();
			$this->productSubId = $lineItem->getProductSubId();
			$this->title = $lineItem->getTitle();
			$this->options = $lineItem->getOptions();
			//$this->attributes = $lineItem->getAttributes();
		}
	}
}
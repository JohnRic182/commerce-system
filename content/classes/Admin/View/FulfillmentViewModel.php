<?php

class Admin_View_FulfillmentViewModel
{
	public $id;
	public $status;
	public $trackingCompany;
	public $trackingNumber;
	public $createdAt;
	public $updatedAt;
	public $items = array();

	public function __construct(Model_Fulfillment $fulfillment, ORDER $order)
	{
		$this->id = $fulfillment->getId();
		$this->status = $fulfillment->getStatus();
		$this->trackingCompany = $fulfillment->getTrackingCompany();
		$this->trackingNumber = $fulfillment->getTrackingNumber();
		$this->createdAt = $fulfillment->getCreatedAt();
		$this->createdAt = $this->createdAt->format('m/d/Y H:i:s');
		$this->updatedAt = $fulfillment->getUpdatedAt();
		$this->updatedAt = $this->updatedAt->format('m/d/Y H:i:s');

		foreach ($fulfillment->getItems() as $lineItemId => $fulfillmentItem)
		{
			/** @var Model_FulfillmentItem $fulfillmentItem */

			$this->items[$lineItemId] = new Admin_View_FulfillmentItemViewModel($fulfillmentItem, isset($order->lineItems[$lineItemId]) ? $order->lineItems[$lineItemId] : null);
		}
	}

	public static function getFulfillmentViews(array $fulfillments, ORDER $order)
	{
		$ret = array();

		foreach ($fulfillments as $fulfillment)
		{
			$ret[] = new Admin_View_FulfillmentViewModel($fulfillment, $order);
		}

		return $ret;
	}
}
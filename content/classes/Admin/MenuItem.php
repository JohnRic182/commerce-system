<?php

/**
 * Class Admin_MenuItem
 */
class Admin_MenuItem
{
	public $id = null;
	public $parent_id = null;
	public $privileges = null;
	public $title = null;
	public $classInner = null;
	public $classOuter = null;
	public $isActive = false;
	public $isOpen = false;
	public $hasChildren = false;
	public $url = null;
	public $urlTarget = null;
	public $alternativeUrls = null;
	public $html = null;
	public $level = 0;
	public $description = null;
	public $attributes;

	protected $children = array();
	protected static $items = array();
	protected static $cc = 0;

	/**
	 * Class constructor
	 *
	 * @param $id
	 * @param $privileges
	 * @param $title
	 * @param null $url
	 * @param null $alternativeUrls
	 * @param null $classInner
	 * @param null $classOuter
	 * @param null $html
	 * @param null $children
	 * @param null $description
	 * @param string $attributes
	 */
	public function __construct($id, $privileges, $title, $url = null, $alternativeUrls = null, $classInner = null, $classOuter = null, $html = null, $children = null, $description = null, $attributes = null)
	{
		$this->id = $id;
		$this->privileges = $privileges;
		$this->title = trans($title);
		$this->classInner = $classInner;
		$this->classOuter = $classOuter;
		$this->url = $url;
		$this->alternativeUrls = $alternativeUrls;
		$this->html = $html;
		$this->description = trans($description);
		$this->attributes = $attributes;

		if ($children != null)
		{
			$this->add($children);
		}
		
		return $this;
	}

	/**
	 * Add a menu item or items
	 *
	 * Accepts single item, multiple items or array
	 */
	public function add()
	{
		if (func_num_args() > 0)
		{
			foreach (func_get_args() as $param)
			{
				if (!$param) continue;

				if (is_object($param) && ($param instanceof Admin_MenuItem))
				{
					$param->parent_id = $this->id;
					
					self::$items[$param->id] = $param;
					
					$this->children[$param->id] = &self::$items[$param->id];
				}
				else if (is_array($param))
				{
					foreach ($param as $item)
					{
						$this->add($item);
					}
				}
			}
		}
	}
	
	/**
	 * Get item parent notes
	 */
	public function getParents()
	{
		$parents = array();

		if (!is_null($this->parent_id))
		{			
			$item = $this;
			while (!is_null($item) && is_object($item) && isset($item->parent_id) && !is_null($item->parent_id))
			{
				$item = isset(self::$items[$item->parent_id]) ? self::$items[$item->parent_id] : null;
				if (!is_null($item))
				{
					$parents[$item->id] = $item;
				}
			}
		}
		return count($parents) > 0 ? $parents : null;
	}
	
	/**
	 * Returns children
	 */
	public function getChildren()
	{
		return count($this->children) > 0 ? $this->children : null;
	}

	/**
	 * Check privileges required to view menu item
	 *
	 * @param $privileges
	 * @return bool
	 */
	public function privilegeMatch($privileges)
	{
		// check some specific privileges
		if (!is_null($this->privileges) && $this->privileges > 0)
		{
			return ($privileges & $this->privileges) > 0;
		}
		return true;
	}

	/**
	 * Get children by privilege
	 *
	 * @param $adminPrivileges
	 *
	 * @return array|null
	 */
	public function getChildrenByPrivileges($adminPrivileges)
	{
		$children = $this->getChildren();

		if (!is_null($children))
		{
			$result = array();

			/** @var $item Admin_MenuItem */
			foreach ($children as $item)
			{
				if ($item->privilegeMatch($adminPrivileges))
				{
					$result[$item->id] = &self::$items[$item->id];
				}
			}
			
			return count($result) > 0 ? $result : null;
		}
		
		return null;
	}
}
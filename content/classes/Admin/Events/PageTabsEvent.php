<?php
/**
 * Class Admin_Events_PageTabsEvent
 */
class Admin_Events_PageTabsEvent extends Events_Event
{
	protected $tabs;

	/**
	 * Class constructor
	 *
	 * @param $pageName
	 * @param array $tabs
	 */
	public function __construct($pageName, array $tabs = null)
	{
		parent::__construct('admin.'.$pageName.'.page_tabs');

		$this->tabs = is_null($tabs) || !is_array($tabs) ? array() : $tabs;
	}

	/**
	 * Get the tabs
	 *
	 * @return array
	 */
	public function getTabs()
	{
		return $this->tabs;
	}

	/**
	 * Set the tabs
	 *
	 * @param array $tabs
	 */
	public function setTabs(array $tabs)
	{
		$this->tabs = $tabs;
	}

	/**
	 * Add a tab
	 *
	 * @param $id
	 * @param $title
	 */
	public function addTab($id, $title)
	{
		$this->tabs[] = array('id' => $id, 'title' => $title);
	}
}
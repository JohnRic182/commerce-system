<?php
/**
 * Class Admin_Events_PageRenderEvent
 */
class Admin_Events_PageRenderEvent extends Events_Event
{
	public function __construct($pageName)
	{
		parent::__construct('admin.'.$pageName.'.page_render');
	}
}
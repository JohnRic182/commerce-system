<?php

/**
* Intuit Quick Start controller
*/
class Admin_Intuit_QuickStart extends Admin_ViewController
{
	protected $quickbooks_version = null;
	protected $sync_taxes = null;
	/** @var intuit_Services_QBSync */
	protected $intuitSync = null;
	protected $settings = null;

	/**
	 * @param null $db
	 * @param null $settings
	 * @param null $view_instance
	 * @param null $intuitSync
	 */
	public function __construct($db, $settings, $view_instance, $intuitSync = null)
	{
		parent::__construct($db, $settings, $view_instance);

		$this->settings = $settings;
		if ($intuitSync === null)
		{
			$this->intuitSync = intuit_Services_QBSync::create();
		}
		else
		{
			$this->intuitSync = $intuitSync;
		}

		$this->quickbooks_version = $settings['intuit_quickbooks_version'];
		$this->sync_taxes = $settings['intuit_sync_taxes'];
	}
	
	protected function setQuickbooksVersion($value)
	{
		$this->quickbooks_version = $value;
	}
	
	protected function setSyncTaxes($value)
	{
		$this->sync_taxes = $value;
	}
	
	public function getQuickbooksVersion()
	{
		return $this->quickbooks_version;
	}
	
	public function getSyncTaxes()
	{
		return $this->sync_taxes;
	}
	
	protected function saveSettings($incomeAccount, $expenseAccount, $taxableTaxCode, $nonTaxableTaxCode, $intuit_zero_tax_rate_name, $intuit_default_tax_rate_name, $intuit_anywhere_cron_enabled)
	{
		$this->db->reset();
		$this->db->assignStr('value', $incomeAccount);
		$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_income_account"');

		$this->db->reset();
		$this->db->assignStr('value', $expenseAccount);
		$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_expense_account"');

		if ($incomeAccount != '' || $expenseAccount != '')
		{
			if (!is_null($this->intuitSync))
			{
				$expenseAccounts = $this->intuitSync->getExpenseAccounts();
				$incomeAccounts = $this->intuitSync->getIncomeAccounts();

				if ($incomeAccount != '')
				{
					foreach ($incomeAccounts as $account)
					{
						if ($account->Id == $incomeAccount)
						{
							$this->db->reset();
							$this->db->assignStr('value', $account->IdDomain);
							$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_income_account_domain"');
							break;
						}
					}
				}
				else
				{
					$account_id = $this->intuitSync->ensureIncomeAccountExists(getpp().' Sales');
					$this->db->query("UPDATE ".DB_PREFIX."settings SET value='".$this->db->escape($account_id)."' WHERE name='intuit_income_account'");
				}

				foreach ($expenseAccounts as $account)
				{
					if ($account->Id == $expenseAccount)
					{
						$this->db->reset();
						$this->db->assignStr('value', $account->IdDomain);
						$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_expense_account_domain"');
						break;
					}
				}
			}
		}

		$this->db->reset();
		$this->db->assignStr('value', $taxableTaxCode);
		$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_taxable_tax_code"');

		$this->db->reset();
		$this->db->assignStr('value', $nonTaxableTaxCode);
		$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_nontaxable_tax_code"');


		if ($intuit_default_tax_rate_name != '')
		{
			$this->db->reset();
			$this->db->assignStr('value', $intuit_default_tax_rate_name);
			$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_default_tax_rate_name"');
		}
		else
		{
			$rate = null;
			if (!is_null($this->intuitSync))
			{
				$rate = $this->intuitSync->ensureSalesTaxRateExists(getpp().' NON TaxCode Ref');
			}

			if (!is_null($rate))
			{
				$this->db->reset();
				$this->db->assignStr('value', $rate->Name);
				$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_default_tax_rate_name"');
			}
		}

		if ($intuit_zero_tax_rate_name != '')
		{
			$this->db->reset();
			$this->db->assignStr('value', $intuit_zero_tax_rate_name);
			$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_zero_tax_rate_name"');
		}
		else
		{
			$rate = null;
			if (!is_null($this->intuitSync))
			{
				$rate = $this->intuitSync->ensureSalesTaxRateExists(getpp().' TAX TaxCode Ref');
			}

			if (!is_null($rate))
			{
				$this->db->reset();
				$this->db->assignStr('value', $rate->Name);
				$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_zero_tax_rate_name"');
			}
		}

		$this->db->reset();
		$this->db->assignStr('value', $this->quickbooks_version);
		$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_quickbooks_version"');
		
		$this->db->reset();
		$this->db->assignStr('value', $this->sync_taxes);
		$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_sync_taxes"');

		$this->db->reset();
		$this->db->assignStr('value', $intuit_anywhere_cron_enabled ? 'Yes' : 'No');
		$this->db->update(DB_PREFIX.'settings', 'WHERE name = "intuit_anywhere_cron_enabled"');
		
		return 1;
	}
	
	public function action_save()
	{
		$quickbooks_version = (isset($_REQUEST['quickbooks_version']) && $_REQUEST['quickbooks_version'] != '') ? $_REQUEST['quickbooks_version'] : $this->settings['intuit_quickbooks_version'];
		$sync_taxes = (isset($_REQUEST['intuit_sync_taxes']) && ($_REQUEST['intuit_sync_taxes'] == '1')) ? 'Yes' : 'No';

		$intuit_income_account = isset($_REQUEST['intuit_income_account']) ? $_REQUEST['intuit_income_account'] : '';
		$intuit_expense_account = isset($_REQUEST['intuit_expense_account']) ? $_REQUEST['intuit_expense_account'] : '';

		$intuit_taxable_tax_code = isset($_REQUEST['intuit_taxable_tax_code']) ? $_REQUEST['intuit_taxable_tax_code'] : '';
		$intuit_nontaxable_tax_code = isset($_REQUEST['intuit_nontaxable_tax_code']) ? $_REQUEST['intuit_nontaxable_tax_code'] : '';

		$intuit_default_tax_rate_name = isset($_REQUEST['intuit_default_tax_rate_name']) ? $_REQUEST['intuit_default_tax_rate_name'] : '';
		$intuit_zero_tax_rate_name = isset($_REQUEST['intuit_zero_tax_rate_name']) ? $_REQUEST['intuit_zero_tax_rate_name'] : '';

		$intuit_anywhere_cron_enabled = isset($_REQUEST['intuit_anywhere_cron_enabled']) ? $_REQUEST['intuit_anywhere_cron_enabled'] : '';

		$this->setQuickbooksVersion($quickbooks_version);
		$this->setSyncTaxes($sync_taxes);

		$ret = array(
			'status' => $this->saveSettings($intuit_income_account, $intuit_expense_account, $intuit_taxable_tax_code, $intuit_nontaxable_tax_code, $intuit_zero_tax_rate_name, $intuit_default_tax_rate_name, $intuit_anywhere_cron_enabled)
		);

		if ($ret['status'])
		{
			Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
		}

		return $ret;
	}
	
	public function action_render($template = 'pages/quickbooks/modal-quickstart')
	{
		$taxableTaxCodes = array();
		$nonTaxableTaxCodes = array();
		$incomeAccounts = array();
		$expenseAccounts = array();
		$salesTaxRates = array();

		if (!is_null($this->intuitSync))
		{
			$taxableTaxCodes = $this->intuitSync->getTaxableSalesTaxCodes();
			$nonTaxableTaxCodes = $this->intuitSync->getNonTaxableSalesTaxCodes();

			$expenseAccounts = $this->intuitSync->getExpenseAccounts();
			$incomeAccounts = $this->intuitSync->getIncomeAccounts();

			$salesTaxRates = $this->intuitSync->getSalesTaxRates();
		}


		$options = array();
		foreach ($taxableTaxCodes as $taxCode)
		{
			$options[$taxCode->Name] = $taxCode->Name;
		}
		$this->view->assign('taxableTaxCodes', $options);

		$options = array();
		foreach ($nonTaxableTaxCodes as $taxCode)
		{
			$options[$taxCode->Name] = $taxCode->Name;
		}
		$this->view->assign('nonTaxableTaxCodes', $options);

		$options = array('' => '');
		foreach ($incomeAccounts as $account)
		{
			$options[$account->Id] = $account->Name;
		}
		$this->view->assign('incomeAccounts', $options);

		$this->view->assign('defaultIncomeAccountName', getpp() . ' Sales');

		$options = array('' => '');
		foreach ($expenseAccounts as $account)
		{
			$options[$account->Id] = $account->Name;
		}
		$this->view->assign('expenseAccounts', $options);

		$options = array('' => '');
		foreach ($salesTaxRates as $rate)
		{
			$options[$rate->Name] = $rate->Name.' ('.number_format($rate->TaxRate, 2, '.', '').')';
		}
		$this->view->assign('salesTaxRates', $options);

		$this->view->assign('intuit', array(
			'quickbooks_version' => $this->quickbooks_version,
			'sync_taxes' => $this->sync_taxes
		));

		if ($this->settings['intuit_anywhere_connected'] == 'Yes')
		{
			return $this->view->fetch($template);
		}

		return '';
	}
}
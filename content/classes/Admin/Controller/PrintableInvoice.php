<?php

/**
 * Class Admin_Controller_PrintableInvoice
 */
class Admin_Controller_PrintableInvoice extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$defaults = array(
			'PrintInvoiceHeight' => '',
			'PrintableInvoiceCompanyName' => 'NO',
			'PrintableInvoiceCompanyLogoAlignment' => 'LEFT',
			'PrintableInvoiceCompanyFax' => 'NO',
			'PrintableInvoiceCompanyEmail' => 'NO',
			'PrintableInvoiceCompanyWeb' => 'NO',
			'PrintableInvoiceCompanyAddress' => 'NO',
			'PrintableInvoiceCompanyPhone' => 'NO',
			'CompanyWebsite' => '',
			'CompanyEmail' => '',
		);

		$data = $settings->getByKeys(array(
			'PrintInvoiceHeight', 'PrintableInvoiceCompanyName', 'PrintableInvoiceCompanyLogoAlignment',
			'PrintableInvoiceCompanyFax', 'PrintableInvoiceCompanyEmail', 'PrintableInvoiceCompanyWeb', 'PrintableInvoiceCompanyAddress',
			'PrintableInvoiceCompanyPhone', 'CompanyWebsite', 'CompanyEmail',
		));

		$printableInvoiceForm = new Admin_Form_PrintableInvoiceForm();

		$form = $printableInvoiceForm->getForm($data);

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings_invoice');
			}
			else
			{
				$validationErrors = array();
				$formData = array();
				foreach ($data as $key => $temp)
				{
					$formData[$key] = trim($request->request->get($key, array_key_exists($key, $defaults) ? $defaults[$key] : ''));

					/** @var core_Form_Field $field */
					$field = $printableInvoiceForm->getBasicGroup()->get($key);

					if ($field && is_array($field) && $field['options']['required'] && $formData[$key] == '')
					{
						$validationErrors[$key] = array('Please enter '.$field['options']['label']);
					}
				}

				if (count($validationErrors) == 0)
				{
					$settings->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('settings_invoice');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8019');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('settings_update'));

		$adminView->assign('body', 'templates/pages/settings/printable-invoice.html');
		$adminView->render('layouts/default');
	}
}
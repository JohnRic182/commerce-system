<?php

/**
 * Class Admin_Controller_Category
 */
class Admin_Controller_Category extends Admin_Controller
{
	/** @var DataAccess_CategoryRepository $categoryRepository */
	protected $categoryRepository = null;

	/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository */
	protected $productsFeaturesGroupsRepository = null;

	/** @var DataAccess_CategoryProductsFeaturesGroupsRepository $categoryProductsFeaturesGroupsRepository */
	protected $categoryProductsFeaturesGroupsRepository = null;

	/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
	protected $productsFeaturesRepository = null;

	protected $exportService = null;

	protected $imagesPath = 'images/catalog';

	protected $exportFileName = 'categories.csv';

	protected $menuItem = array('primary' => 'products', 'secondary' => 'categories');

	/**
	 * List categories
	 */
	public function listAction()
	{
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3006');

		/** @var DataAccess_CategoryRepository $categoryRepository */
		$categoryRepository = $this->getCategoryRepository();

		/** @var Framework_Request $request */
		//$request = $this->getRequest();

		$adminView->assign('categories', $categoryRepository->getFlattenedList());

		$adminView->assign('delete_nonce', Nonce::create('category_delete'));

		$adminView->assign('body', 'templates/pages/category/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add category
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_CategoryRepository $categoryRepository */
		$categoryRepository = $this->getCategoryRepository();

		/** @var Admin_Form_CategoryForm $categoryForm */
		$categoryForm = new Admin_Form_CategoryForm();

		$defaults = $categoryRepository->getDefaults($mode);

		// get tax options
		$taxService = Tax_ProviderFactory::getTaxService();

		$avalaraTaxCodesOptions = null;
		$exactorTaxCodesOptions = null;

		if ($taxService instanceof Tax_Avalara_AvalaraService || $taxService instanceof Tax_Exactor_ExactorService)
		{
			if ($taxService instanceof Tax_Avalara_AvalaraService)
			{
				$avalaraTaxCodesOptions = $this->getAvalaraTaxCodesOptions();
			}
			else if ($taxService instanceof Tax_Exactor_ExactorService)
			{
				$exactorTaxCodesOptions = $this->getExactorTaxCodesOptions();
			}
		}

		$formDefaults = $defaults;
		$formDefaults['is_visible'] = 'Yes';
		$formDefaults['image'] = '';

		/** @var core_Form_Form $form */
		$form = $categoryForm->getForm(
			$formDefaults,
			$mode,
			array('categoryOptions' => $categoryRepository->getOptionsList()),
			null,
			$avalaraTaxCodesOptions,
			$exactorTaxCodesOptions
		);

		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'category_'.$mode))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('categories');
			}
			else
			{
				if (($validationErrors = $categoryRepository->getValidationErrors($formData, $mode)) == false)
				{
					if (!isset($formDefaults['is_visible'])) $formDefaults['is_visible'] = 'No';
					if (!isset($formDefaults['list_subcats'])) $formDefaults['list_subcats'] = 'No';
					if (!isset($formDefaults['list_subcats_images'])) $formDefaults['list_subcats_images'] = '0';

					if ($formData['url_custom'] != '' && $categoryRepository->hasUrlDuplicate($formData['url_custom'], 0))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$id = $categoryRepository->persist($formData);
					$formData['id'] = $id;

					// TODO: optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), true, false, false, false);

					$this->handleImageUpload($id);

					$event = new Events_CategoryEvent(Events_CategoryEvent::ON_UPDATED);
					$event->setData('category', $formData);
					Events_EventHandler::handle($event);

					Admin_Flash::setMessage('common.success');

					$categoryDetails = $categoryRepository->getCategoryDetails($id);
					Admin_Log::log('Categories: Category added - '.$categoryDetails['name'], Admin_Log::LEVEL_IMPORTANT);
					$this->redirect('category', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3007');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('category_add'));

		$adminView->assign('body', 'templates/pages/category/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CategoryRepository
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_CategoryRepository $categoryRepository */
		$categoryRepository = $this->getCategoryRepository();

		$id = intval($request->get('id'));

		$category = $categoryRepository->getById($id);

		if (!$category)
		{
			Admin_Flash::setMessage('categories.category_not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('categories');
		}

		/** @var Admin_Form_CategoryForm $categoryForm */
		$categoryForm = new Admin_Form_CategoryForm();

		// get tax options
		$taxService = Tax_ProviderFactory::getTaxService();

		$avalaraTaxCodesOptions = null;
		$exactorTaxCodesOptions = null;

		if ($taxService instanceof Tax_Avalara_AvalaraService || $taxService instanceof Tax_Exactor_ExactorService)
		{
			if ($taxService instanceof Tax_Avalara_AvalaraService)
			{
				$avalaraTaxCodesOptions = $this->getAvalaraTaxCodesOptions();
			}
			else if ($taxService instanceof Tax_Exactor_ExactorService)
			{
				$exactorTaxCodesOptions = $this->getExactorTaxCodesOptions();
			}
		}

		/** @var DataAccess_CategoryProductsFeaturesGroupsRepository $categoryProductsFeaturesGroupsRepository */
		$categoryProductsFeaturesGroupsRepository = $this->getCategoryProductsFeaturesGroupsRepository();

		$params = array(
			'categoryOptions' => $categoryRepository->getOptionsList($id),
			'categoryProductsFeaturesGroups' => $categoryProductsFeaturesGroupsRepository->getGroupsByCategoryId($id)
		);

		/** @var core_Form_Form $form */
		$form = $categoryForm->getForm($category, $mode, $params, $id, $avalaraTaxCodesOptions, $exactorTaxCodesOptions);

		if ($request->isPost())
		{
			$formData = array_merge($category, $request->request->all());

			if (!Nonce::verify($formData['nonce'], $mode))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('categories');
			}
			else
			{
				if (($validationErrors = $categoryRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					if (($request->get('is_visible') == null)) $formData['is_visible'] = 'No';
					if (($request->get('is_stealth') == null)) $formData['is_stealth'] = 0;
					if (($request->get('list_subcats') == null)) $formData['list_subcats'] = 'No';
					if (($request->get('list_subcats_images') == null)) $formData['list_subcats_images'] = 0;

					if ($formData['url_custom'] != '' && $categoryRepository->hasUrlDuplicate($formData['url_custom'], $id))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$categoryRepository->persist($formData);

					if (isset($formData['product_features_priorities'])) $this->getCategoryProductsFeaturesGroupsRepository()->updatePriorities($id, $formData['product_features_priorities']);

					if (isset($formData['doba_mapped_category_id']) && intval($formData['doba_mapped_category_id']) > 0)
					{
						$categoryRepository->moveProducts($id, $formData['doba_mapped_category_id']);
					}

					$this->handleImageUpload($id);

					// TODO: optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), true, true, false, false);

					$event = new Events_CategoryEvent(Events_CategoryEvent::ON_UPDATED);
					$event->setData('category', $formData);
					Events_EventHandler::handle($event);

					Admin_Flash::setMessage('common.success');
					$categoryDetails = $categoryRepository->getCategoryDetails($id);
					Admin_Log::log('Categories: Category updated - '.$categoryDetails['name'], Admin_Log::LEVEL_IMPORTANT);
					$this->redirect('category', array('id' => $id, 'mode' => $mode));
				}
				else
				{
					//TODO: Translation
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		$settings = $this->getSettings();

		$categoryUrl = $settings->get('INDEX').'?p=catalog&amp;parent='.$id;

		if ($settings->get('USE_MOD_REWRITE') == 'YES')
		{
			$categoryUrl = $category['url_custom'] != '' ? $category['url_custom'] : $category['url_default'];
		}

		// get available products feature groups
		$adminView->assign('categoryProductsFeaturesGroupsAvailable', json_encode($this->getProductsFeaturesGroupsRepository()->getList(null, null, 'feature_group_name_asc')));

		$adminView->assign('categoryUrl', $categoryUrl);
		$adminView->assign('isDoba', $category['is_doba'] == 1);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3008');
		$adminView->assign('title', $category['name']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('category_update'));
		$adminView->assign('delete_nonce', Nonce::create('category_delete'));

		$adminView->assign('body', 'templates/pages/category/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete image
	 */
	public function deleteImageAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_CategoryRepository $categoryRepository */
		$categoryRepository = $this->getCategoryRepository();

		$id = intval($request->get('id'));

		$category = $categoryRepository->getById($id);

		if (!$category)
		{
			$result = array('status' => 0, 'error' => trans('categories.category_not_found'));
		}
		else
		{
			if (!is_null($image = $categoryRepository->getCategoryImage($id))) @unlink($image);
			if (!is_null($thumb = $categoryRepository->getCategoryThumb($id))) @unlink($thumb);

			$result = array('status' => 1);
		}

		$this->renderJson($result);
	}

	/**
	 * Delete categories
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'category_delete')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('categories');
			return;
		}

		/** @var DataAccess_CategoryRepository $categoryRepository */
		$categoryRepository = $this->getCategoryRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = intval($request->get('id', 0));
			$categoryDetails = $categoryRepository->getCategoryDetails($id);
			if ($id < 1)
			{
				Admin_Flash::setMessage('categories.category_not_found', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($categoryRepository->delete($id))
				{
					Admin_Flash::setMessage('common.success');
					Admin_Log::log('Categories: Category deleted - '.$categoryDetails['name'], Admin_Log::LEVEL_IMPORTANT);
				}
				else
				{
					Admin_Flash::setMessage('categories.category_not_empty', Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			foreach($ids as $id)
			{
				$categoryDetails[] = $categoryRepository->getCategoryDetails($id);
			}

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('categories.no_category_selected', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($categoryRepository->delete($ids))
				{
					Admin_Flash::setMessage('common.success');
					for($i = 0; $i < count($categoryDetails); $i++)
					{
						Admin_Log::log('Categories: Category deleted - '.$categoryDetails[$i]['name'], Admin_Log::LEVEL_IMPORTANT);
					}
				}
				else
				{
					Admin_Flash::setMessage('categories.category_not_empty_multiple', Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else if ($deleteMode == 'all')
		{
			if ($categoryRepository->deleteAll())
			{
				Admin_Flash::setMessage('common.success');
				Admin_Log::log('Categories: All categories deleted', Admin_Log::LEVEL_IMPORTANT);
			}
			else
			{
				Admin_Flash::setMessage('categories.category_not_empty_multiple', Admin_Flash::TYPE_ERROR);
			}
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('categories');
	}

	/**
	 * Assign products features group
	 */
	public function assignProductFeatureGroupAction()
	{
		$request = $this->getRequest();

		$result = array('status' => 1);

		if ($request->isPost())
		{
			$cid = $request->get('cid', 0);

			if ($cid)
			{
				$productsFeaturesGroups = explode(',', $request->get('products_features_groups', ''));
				/** @var DataAccess_CategoryProductsFeaturesGroupsRepository $categoryProductsFeaturesGroupsRepository */
				$categoryProductsFeaturesGroupsRepository = $this->getCategoryProductsFeaturesGroupsRepository();
				$categoryProductsFeaturesGroupsRepository->updateProductsFeaturesGroups($cid, $productsFeaturesGroups);

				$result = array(
					'status' => 1,
					'categoryProductsFeaturesGroupsAssigned' => $categoryProductsFeaturesGroupsRepository->getGroupsByCategoryId($cid)
				);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Delete products features group
	 */
	public function deleteProductFeatureGroupAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$cid = $request->get('cid', 0);
			if ($cid)
			{
				$productFeatureGroupId = $request->get('product_feature_group_id', 0);

				/** @var DataAccess_CategoryProductsFeaturesGroupsRepository $categoryProductsFeaturesGroupsRepository */
				$categoryProductsFeaturesGroupsRepository = $this->getCategoryProductsFeaturesGroupsRepository();
				$categoryProductsFeaturesGroupsRepository->delete($cid, $productFeatureGroupId);

				$result = array(
					'status' => 1,
					'categoryProductsFeaturesGroupsAssigned' => $categoryProductsFeaturesGroupsRepository->getGroupsByCategoryId($cid)
				);
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot delete products features group. Unsupported method.')));
		}

		$this->renderJson($result);
	}


	/**
	 * Export categories
	 */
	public function exportAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$exportMode = $request->get('exportMode');

		$exportService = $this->getCategoryExportService();

		if ($exportMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('categories.no_category_selected_export', Admin_Flash::TYPE_ERROR);
				$this->redirect('categories');
			}
			else
			{
				$exportService->export($this->exportFileName, array('mode'=>'byIds', 'ids' => $ids));
			}
		}
		else if ($exportMode == 'all')
		{
			$exportService->export($this->exportFileName, array('mode'=>'all'));
		}
	}

	/**
	 * Get avalara tax options
	 *
	 * @return array
	 */
	protected function getAvalaraTaxCodesOptions()
	{
		$avalaraRepository = new DataAccess_AvalaraTaxRepository($this->getDb());
		return $avalaraRepository->getTaxCodesOptions();
	}

	/**
	 * Get exactor tax options
	 *
	 * @return mixed
	 */
	protected function getExactorTaxCodesOptions()
	{
		$exactorRepository = new DataAccess_ExactorTaxRepository($this->getDb());
		return $exactorRepository->getTaxCodesOptions();
	}

	/**
	 * Get category repository
	 *
	 * @return DataAccess_CategoryRepository
	 */
	protected function getCategoryRepository()
	{
		if (is_null($this->categoryRepository))
		{
			$this->categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		}

		return $this->categoryRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesGroupsRepository
	 */
	protected function getProductsFeaturesGroupsRepository()
	{
		if ($this->productsFeaturesGroupsRepository == null)
		{
			$this->productsFeaturesGroupsRepository = new DataAccess_ProductsFeaturesGroupsRepository($this->getDb());
		}

		return $this->productsFeaturesGroupsRepository;
	}

	/**
	 * Get category products features groups repository
	 *
	 * @return DataAccess_CategoryProductsFeaturesGroupsRepository
	 */
	protected function getCategoryProductsFeaturesGroupsRepository()
	{
		if ($this->categoryProductsFeaturesGroupsRepository == null)
		{
			$this->categoryProductsFeaturesGroupsRepository = new DataAccess_CategoryProductsFeaturesGroupsRepository($this->getDb());
		}

		return $this->categoryProductsFeaturesGroupsRepository;
	}

	/**
	 * @return Admin_Service_CategoryExport|null
	 */
	protected function getCategoryExportService()
	{
		if ($this->exportService == null)
		{
			$this->exportService = new Admin_Service_CategoryExport($this->getDb());
		}

		return $this->exportService;
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductsFeaturesRepository
	 */
	protected function getProductsFeaturesRepository()
	{
		if ($this->productsFeaturesRepository == null)
		{
			$this->productsFeaturesRepository = new DataAccess_ProductsFeaturesRepository($this->getDb());
		}

 		return $this->productsFeaturesRepository;
 	}

	/**
	 * Get seo URL settings
	 *
	 * @return mixed|string
	 */
	protected function getSeoUrlSettings()
	{
		/** @var DataAccess_SettingsRepository */
		$settings = $this->getSettings();

		return json_encode(array(
			'template' => $settings->get('SearchURLCategoryTemplate'),
			'lowercase' => $settings->get('SearchAutoGenerateLowercase') == 'YES',
			'joiner' => $settings->get('SearchURLJoiner')
		));
	}

	/**
	 * Get Available Features Groups
	 * @param $featureGroups
	 * @return array
	 */
	protected function getAvailableFeaturesGroups($featureGroups)
	{
		$fields = array();

		foreach($featureGroups as $featureGroup)
		{
			if ($featureGroup['feature_group_active'])
			{
				$fieldItem = array(
					'field' => $featureGroup['product_feature_group_id'], 'label' => $featureGroup['feature_group_name']
				);
				array_push($fields, $fieldItem);
			}
		}

		return $fields;
	}

	/**
	 * @param $categoryId
	 */
	protected function handleImageUpload($categoryId)
	{
		$request = $this->request;
		if ($imageUploadInfo = $request->getUploadedFile('image'))
		{
			/** @var Admin_Service_FileUploader $fileUploaderService */
			$fileUploadService = new Admin_Service_FileUploader();

			$imageFile = $fileUploadService->processImageUpload($imageUploadInfo, $this->imagesPath, $categoryId, true);

			if ($imageFile)
			{
				$extension = pathinfo(basename($imageFile), PATHINFO_EXTENSION);
				$settings = $this->getSettings();

				$fileUploadService->deleteSameFilename($this->imagesPath . '/thumbs', $categoryId);

				$imageFileThumb = $this->imagesPath . '/thumbs/' . intval($categoryId) . '.' . $extension;
				ImageUtility::generateCategoryThumb($imageFile, $imageFileThumb);

				// Resize images according to image size define in
				// Design -> Catalog Settings -> Category Image Settings
				ImageUtility::generateCatalogImage($imageFile, $imageFile);

				if ($settings->get('CatalogOptimizeImages') == 'YES')
				{
					$optimizer = new Admin_Service_ImageOptimizer();
					$optimizer->optimize($imageFile);
				}
			}
		}
	}
}
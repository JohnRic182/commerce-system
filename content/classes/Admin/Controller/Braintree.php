<?php

	/**
	 * Class Admin_Controller_Braintree
	 */
	class Admin_Controller_Braintree extends Admin_Controller
	{
		protected $menuItem = array('primary' => 'apps', 'secondary' => '');

		public function indexAction()
		{
			$request = $this->getRequest();
			$settings = $this->getSettings();

			$settingsVars = array('braintree_merchantId',
				'braintree_publicKey',
				'braintree_privateKey',
				'braintree_environment',
				'braintree_transaction_type',);

			if ($request->isPost())
			{
				if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
				{
					Admin_Flash::setMessage('Cannot verify form nonce', Admin_Flash::TYPE_ERROR);
					$this->redirect('app', array('key' => 'braintree'));

					return;
				}

				$settingsData = $request->request->getByKeys($settingsVars);

				$settings->persist($settingsData, $settingsVars);

				Admin_Flash::setMessage('Success!');
				$this->redirect('app', array('key' => 'braintree'));
			}

			$adminView = $this->getView();

			// set menu
			$_accsessToken = $settings->get('braintree_oauth_accessToken', '');
			$adminView->assign('activeMenuItem', $this->menuItem);
			$adminView->assign('helpTag', '9005');

			$adminView->assign('nonce', Nonce::create('update_settings'));

			$formBuilder = new core_Form_FormBuilder('settings');

			if ($_accsessToken != '')
			{
				$formBuilder->add('braintree_merchantId', 'hidden', array('value' => ''));
				$formBuilder->add('braintree_publicKey', 'hidden', array('value' => ''));
				$formBuilder->add('braintree_privateKey', 'hidden', array('value' => ''));
				$formBuilder->add('braintree_environment', 'hidden', array('value' => ''));

				$formBuilder->add('braintree_oauth_clientid', 'hidden', array('value' => $settings->get('braintree_oauth_clientid')));
				$formBuilder->add('braintree_oauth_clientsecret', 'hidden', array('value' => $settings->get('braintree_oauth_clientsecret')));
				$formBuilder->add('braintree_oauth_accessToken', 'hidden', array('value' => $settings->get('braintree_oauth_accessToken')));

			}
			else
			{
				$formBuilder->add('braintree_merchantId', 'text', array('label' => 'braintree.merchantId',
																		'value' => $settings->get('braintree_merchantId')));
				$formBuilder->add('braintree_publicKey', 'text', array('label' => 'braintree.publicKey',
																	   'value' => $settings->get('braintree_publicKey')));
				$formBuilder->add('braintree_privateKey', 'text', array('label' => 'braintree.privateKey',
																		'value' => $settings->get('braintree_privateKey')));
				$formBuilder->add('braintree_environment', 'choice', array('label'   => 'braintree.environment',
																		   'value'   => $settings->get('braintree_environment'),
																		   'attr'    => array('class' => 'short'),
																		   'options' => array(new core_Form_Option(PAYMENT_BRAINTREE::BT_ENVIRONMENT_SANDBOX, 'Sandbox'),
																			   new core_Form_Option(PAYMENT_BRAINTREE::BT_ENVIRONMENT_PRODUCTION, 'Production'),)));

			}

			$formBuilder->add('braintree_transaction_type', 'choice', array('label'   => 'braintree.transaction_type',
																			'value'   => $settings->get('braintree_transaction_type'),
																			'attr'    => array('class' => 'short'),
																			'options' => array(new core_Form_Option(PAYMENT_BRAINTREE::BT_TRANSACTION_SETTLEMENT_SALE, 'Sale'),
																				new core_Form_Option(PAYMENT_BRAINTREE::BT_TRANSACTION_SETTLEMENT_AUTH, 'Auth'),)));

			$formBuilder->add('braintree_EnableVault', 'choice', array('label'   => 'braintree.enableVault',
																	   'value'   => $settings->get('braintree_EnableVault'),
																	   'attr'    => array('class' => 'short'),
																	   'options' => array(new core_Form_Option('No', 'No'),
																		   new core_Form_Option('Yes', 'Yes'),)));

			$db = $this->getDb();
			$peymentrep = new Payment_DataAccess_PaymentMethodRepository($db, $settings);
			$method = $peymentrep->getByMethodId('braintree');
			$provider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, $settings));
			/** @var PAYMENT_BRAINTREE $gateway */

			$gateway = $provider->getPaymentProcessorById($method->getId());
			$plans = array();
			$plans_options = array();
			if ($gateway)
			{
				$plans = $gateway->getRecurringPlans();
				if ($plans)
				{
					/** @var Braintree\Plan $plan */
					foreach ($plans as $plan)
					{
						$plans_options[] = new core_Form_Option($plan->id, $plan->name);
					}
				}
			}

			$form = $formBuilder->getForm();

			$braintree_clientid = $settings->get('braintree_oauth_clientid', '');
			$braintree_clientsecret = $settings->get('braintree_oauth_clientsecret', '');
			$authUrlDAta = $this->generateOAuthUrl($braintree_clientid, $braintree_clientsecret);
			$url = false;
			if ($authUrlDAta)
			{
				if (isset($authUrlDAta['url']) && trim($authUrlDAta['url']) != '')
				{
					$url = $authUrlDAta['url'];
				}
			}
			$adminView->assign('clientid', $braintree_clientid);
			$adminView->assign('clientsecret', $braintree_clientsecret);
			$adminView->assign('authurl', $url);

			$adminView->assign('form', $form);

			$adminView->assign('body', 'templates/pages/braintree/index.html');
			$adminView->render('layouts/default');
		}

		private function activatePaymentMethod($id = 'braintree')
		{
			$db = $this->getDb();
			$paymentRep = new DataAccess_PaymentMethodRepository($db);
			$method = $paymentRep->getPaymentMethodById($id);
			if ($method != null)
			{
				$db->query('UPDATE ' . DB_PREFIX . "payment_methods SET active='Yes' WHERE id='" . $db->escape($id) . "'");
				Admin_Log::log('Payment method ' . $id . ' has been activated', Admin_Log::LEVEL_IMPORTANT);
			}
		}

		private function deactivatePaymentMethod($id = 'braintree')
		{
			$db = $this->getDb();
			$paymentRep = new DataAccess_PaymentMethodRepository($db);
			$paymentRep->deactivate($id);
		}

		public function canActivate()
		{
			return true;
		}

		protected function _generateOAuthUrl($clientid, $clientsecret)
		{
			$result = array('status' => 0);
			require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/vendors/braintree/autoload.php';
			try
			{
				$settings = $this->getSettings();
				$connect_url = $settings->get('GlobalHttpsUrl') . '/admin.php';

				$gateway = new Braintree\Gateway(array('clientId'     => $clientid,
													   'clientSecret' => $clientsecret));

				$url = $gateway->oauth()->connectUrl(array('redirectUri'    => $connect_url,
														   'scope'          => 'read_write',
														   'state'          => 'braintreeauth',
														   'landingPage'    => 'login',
														   'user'           => array('country' => $settings->get('CompanyCountry'),
																					 'email'   => $settings->get('CompanyEmail')),
														   'paymentMethods' => array('credit_card',
															   'paypal')));
				$result['status'] = 1;
				$result['url'] = $url;
			}
			catch (Exception $e)
			{
				$result['error'] = $e->getMessage();
			}

			return $result;
		}

		public function generateOAuthUrl($country = 'USA', $email = 'partnerapiupdates@pinnaclecart.com')
		{
			$result = array('status' => 0);
			try
			{
				$settings = $this->getSettings();
				$c = curl_init();

				$params = array('redirect_url' => $settings->get('GlobalHttpsUrl') . '/admin.php?p=braintree&mode=auth-connect',
								'country'      => $country,
								'email'        => $email);

				curl_setopt($c, CURLOPT_URL, 'https://gateway.pinnaclecart.com/api/braintree/connect-url');
				curl_setopt($c, CURLOPT_VERBOSE, 1);
				curl_setopt($c, CURLOPT_POST, 1);
				curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($params));
				curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

				$buffer = curl_exec($c);
				if (curl_errno($c) > 0)
				{
					$error_msg = curl_error($c);
				}
				else
				{
					$api_data = json_decode($buffer);
					if ($api_data->status_code == 200)
					{
						$result['status'] = 1;
						$result['url'] = $api_data->url;
					}
				}
			}
			catch (Exception $e)
			{
				$result['error'] = $e->getMessage();
			}

			return $result;
		}

		public function _authConnectAction()
		{
			$request = $this->getRequest();
			$settings = $this->getSettings();
			$merchantId = $request->get('merchantId');
			$code = $request->get('code');
			$error = isset($_REQUEST['error']) ? $_REQUEST['error'] : '';

			if ($error)
			{
				Admin_Flash::setMessage($error, Admin_Flash::TYPE_ERROR);
				$this->redirect('apps', array('key' => 'braintree'));
			}

			if ($merchantId && $code)
			{
				require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/vendors/braintree/autoload.php';
				try
				{
					$clientId = $settings->get('braintree_oauth_clientid');
					$clientSecret = $settings->get('braintree_oauth_clientsecret');

					$gateway = new Braintree\Gateway(array('braintree_merchantId'  => '',
														   'braintree_publicKey'   => '',
														   'braintree_privateKey'  => '',
														   'braintree_environment' => '',
														   'clientId'              => $clientId,
														   'clientSecret'          => $clientSecret));

					$result = $gateway->oauth()->createTokenFromCode(array('code' => $code));

					$accessToken = $result->credentials->accessToken;
					$expiresAt = $result->credentials->expiresAt;
					$refreshToken = $result->credentials->refreshToken;

					$settings->persist(array('braintree_oauth_accessToken' => $accessToken,
											 'braintree_oauth_expiresAt'   => $expiresAt));
					$settings->persist(array('braintree_enabled' => 1));
					$appRepository = new DataAccess_AppRepository($this->getDb());
					$appRepository->enableApp('braintree');
					$this->activatePaymentMethod('braintree');

					Admin_Flash::setMessage('Success!');
					$this->redirect('app', array('key' => 'braintree'));

				}
				catch (Exception $e)
				{
					$this->renderJson(array('status'  => 0,
											'message' => $e->getMessage()));
				}
			}

		}

		public function disableNonMerchantAPISettings()
		{
			$db = $this->getDb();
			$db->query('UPDATE ' . DB_PREFIX . "settings
						SET group_name = 'braintree', value = ''
						WHERE name IN ('braintree_merchantId', 'braintree_publicKey', 'braintree_privateKey')");
		}

		public function enableNonMerchantAPISettings()
		{
			$db = $this->getDb();
			$db->query('UPDATE ' . DB_PREFIX . "settings
						SET group_name = 'payment_braintree'
						WHERE name IN ('braintree_merchantId', 'braintree_publicKey', 'braintree_privateKey')");
		}

		public function authConnectAction()
		{
			$request = $this->getRequest();
			$settings = $this->getSettings();
			$merchantId = $request->get('merchantId');
			$code = $request->get('code');
			$error = isset($_REQUEST['error']) ? $_REQUEST['error'] : '';

			if ($error)
			{
				Admin_Flash::setMessage($error, Admin_Flash::TYPE_ERROR);
				$this->redirect('payment_methods');
			}

			if ($merchantId && $code)
			{
				try
				{
					$c = curl_init();

					$params = array('code' => $code);

					curl_setopt($c, CURLOPT_URL, 'https://gateway.pinnaclecart.com/api/braintree/token/from-code');
					curl_setopt($c, CURLOPT_VERBOSE, 1);
					curl_setopt($c, CURLOPT_POST, 1);
					curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($params));
					curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

					$buffer = curl_exec($c);
					if (curl_errno($c) > 0)
					{
						$error_msg = curl_error($c);
					}
					else
					{
						$api_data = json_decode($buffer);
						if ($api_data->status_code == 200)
						{
							$accessToken = $api_data->access_token;
							$expiresAt = $api_data->expires_at->date;
							$refreshToken = $api_data->refresh_token;
						}
					}

					$settings->persist(array('braintree_oauth_accessToken' => $accessToken,
											 'braintree_oauth_expiresAt'   => $expiresAt));
					$this->disableNonMerchantAPISettings();

					$this->activatePaymentMethod('braintree');

					Admin_Flash::setMessage('Success!');
					$this->redirect('payment_methods');

				}
				catch (Exception $e)
				{
					$this->renderJson(array('status'  => 0,
											'message' => $e->getMessage()));
				}
			}

		}
	}
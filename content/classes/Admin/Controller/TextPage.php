<?php

/**
 * Class Admin_Controller_TextPage
 */
class Admin_Controller_TextPage extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/textpage/';

	/** @var DataAccess_TextPageRepository  */
	protected $textPageRepository = null;

	/** @var DataAccess_CustomFormRepository */
	protected $customFormRepository = null;

	protected $menuItem = array('primary' => 'content', 'secondary' => 'pages');

	/** @var array */
	protected $searchParamsDefaults = array('search_str' => '', 'logic' => 'AND');

	/**
	 * List Site Pages
	 */
	public function listAction()
	{
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$textPagesRepository = $this->getTextPageRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$session = $this->getSession();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('textPageSearchParams', array()); else $session->set('textPageSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('textPageSearchOrderBy', 'priority'); else $session->set('textPageSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('textPageSearchOrderDir', 'asc'); else $session->set('textPageSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		$itemsCount = $textPagesRepository->getCount($searchParams, $searchParams['logic']);

		$textPageForm = new Admin_Form_TextPageForm();
		$adminView->assign('searchForm', $textPageForm->getTextPageSearchForm($searchFormData));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('textpages',
				$textPagesRepository->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'p.*', $searchParams['logic']
				)
			);
		}
		else
		{
			$adminView->assign('textpages', null);
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6000');

		$adminView->assign('delete_nonce', Nonce::create('textpage_delete'));
		$adminView->assign('publish_nonce', Nonce::create('textpage_publish'));

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new text page
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TextPageRepository  */
		$textPageRepository = $this->getTextPageRepository();

		/** @var Admin_Form_TextPageForm */
		$textPageForm = new Admin_Form_TextPageForm();

		$defaults = $textPageRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$pageData = $defaults;
		$pageData['is_active'] = 'Yes';
		$form = $textPageForm->getForm($pageData, $mode, null, array('customFormOptions' => $this->getCustomFormRepository()->getOptionsList()));

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('pages');
			}
			else
			{
				if (($validationErrors = $textPageRepository->getValidationErrors($formData, $mode)) == false)
				{
					if ($formData['url_custom'] != '' && $textPageRepository->hasUrlDuplicate($formData['url_custom'], 0))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$id = $textPageRepository->persist($formData);

					// TODO - optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, true, false);

					Admin_Flash::setMessage('common.success');
					$this->redirect('page', array('mode' => self::MODE_UPDATE, 'id' => $id));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6001');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('textpage_add'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update text page
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TextPageRepository */
		$textPageRepository = $this->getTextPageRepository();

		$id = intval($request->get('id'));

		$pageData = $textPageRepository->getById($id);

		if (!$pageData)
		{
			Admin_Flash::setMessage('Cannot find page by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('pages');
		}

		/** @var Admin_Form_TextPageForm */
		$textPageForm = new Admin_Form_TextPageForm();

		$defaults = $textPageRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $textPageForm->getForm($pageData, $mode, $id, array('customFormOptions' => $this->getCustomFormRepository()->getOptionsList()));

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('pages');
			}
			else
			{
				if (($validationErrors = $textPageRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					if ($formData['url_custom'] != '' && $textPageRepository->hasUrlDuplicate($formData['url_custom'], $id))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$textPageRepository->persist($formData);

					// TODO - optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, true, false);

					Admin_Flash::setMessage('common.success');
					$this->redirect('page', array('mode' => self::MODE_UPDATE, 'id' => $id));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}


		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		$settings = $this->getSettings();
		$pageUrl = $settings->get('INDEX') . '?p=page&amp;page_id=' . $pageData['name'];
		
		if ($settings->get('USE_MOD_REWRITE') == 'YES')
		{
			$pageUrl = $pageData['url_custom'] != '' ? $pageData['url_custom'] : $pageData['url_default'];
		}
		
		$adminView->assign('pageUrl', $pageUrl);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6002');
		$adminView->assign('title', $pageData['title']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('textpage_update'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete page
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TextPageRepository */
		$textPageRepository = $this->getTextPageRepository();

		$deleteMode = $request->get('deleteMode');
		
		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('pages');
			return;
		}
		
		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid page id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$textPageRepository->delete(array($id));
				Admin_Flash::setMessage('Site page has been deleted');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one page to delete', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$textPageRepository->delete($ids);
				Admin_Flash::setMessage('Selected pages have been deleted');
			}
		}
		else if ($deleteMode == 'search')
		{
			/** @var Framework_Session $session */
			$session = $this->getSession();
			$searchParams = array();

			$textPageRepository->deleteBySearchParams($searchParams);
			Admin_Flash::setMessage('Select pages have been deleted');
		}

		$this->redirect('pages');
	}

	/**
	 * Bulk hide / show pages
	 */
	public function publishAction()
	{
		$request = $this->getRequest();
		$repository = $this->getTextPageRepository();
		$publishMode = $request->get('publishMode');
		$publishAction = $request->get('publishAction');
		$str = $publishAction == 'publish' ? 'published' : 'unpublished';
		
		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('pages');
			return;
		}
		
		if ($publishMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one page to publish', Admin_Flash::TYPE_ERROR);
				$this->redirect('pages');
			}
			else
			{
				$repository->publishPages($ids, $publishAction);
				Admin_Flash::setMessage('Selected pages have been '.$str);
				$this->redirect('pages');
			}
		}
		else if ($publishMode == 'all')
		{
			$repository->publishPages('all', $publishAction);
			Admin_Flash::setMessage('Select pages have been '.$str);
			$this->redirect('pages');
		}
	}

	/**
	 * @return DataAccess_TextPageRepository
	 */
	protected function getTextPageRepository()
	{
		if ($this->textPageRepository == null)
		{
			$this->textPageRepository = new DataAccess_TextPageRepository($this->getDb(), $this->getSettings()->getAll());
		}

		return $this->textPageRepository;
	}

	/**
	 * @return DataAccess_CustomFormRepository
	 */
	protected function getCustomFormRepository()
	{
		if ($this->customFormRepository == null)
		{
			$this->customFormRepository = new DataAccess_CustomFormRepository($this->getDb());
		}

		return $this->customFormRepository;
	}

	/**
	 * Get seo URL settings
	 *
	 * @return mixed|string
	 */
	protected function getSeoUrlSettings()
	{
		/** @var DataAccess_SettingsRepository */
		$settings = $this->getSettings();

		return json_encode(array(
			'template' => $settings->get('SearchURLPageTemplate'),
			'lowercase' => $settings->get('SearchAutoGenerateLowercase') == 'YES',
			'joiner' => $settings->get('SearchURLJoiner')
		));
	}
}
<?php

/**
 * Class Admin_Controller_ProductVariant
 */
class Admin_Controller_ProductVariant extends Admin_Controller
{
	/** @var DataAccess_ProductVariantRepository  */
	protected $productVariantRepository = null;

	/** @var DataAccess_ProductAttributeRepository */
	protected $productAttributeRepository = null;

	/** @var DataAccess_ProductsRepository */
	protected $productsRepository = null;

	/**
	 * Add variant
	 */
	public function addVariantAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productVariantsRepository = $this->getProductVariantRepository();

			$defaults = $productVariantsRepository->getDefaults();

			$formData = array_merge($defaults, $request->get('variant', array()));
			$formData['required-attributes'] = $this->getProductAttributeRepository()->getRequiredByProductId($formData['pid']);
			$formData['id'] = $formData['pi_id'] = null;

			if (($validationErrors = $productVariantsRepository->getValidationErrors($formData, $mode)) == false)
			{
				$productVariantsRepository->persist($formData, array('productAttributes' => $this->getProductAttributeRepository()->getListByProductId($formData['pid'])));
				$newStock = $this->getProductsRepository()->updateProductInventory($formData['pid'], $formData['inventory_control']);
				$this->touchProduct($formData['pid']);

				$result = array(
					'status' => 1,
					'variants' => $productVariantsRepository->getListByProductId($formData['pid']),
					'productStock' => ($newStock !== false ? $newStock : null)
				);
			}
			else
			{
				$result = array('status' => 0, 'errors' => $validationErrors);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Update variant
	 */
	public function updateVariantAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productVariantsRepository = $this->getProductVariantRepository();

			$formData = array_merge($productVariantsRepository->getDefaults(), $request->get('variant', array()));

			$variantData = $productVariantsRepository->getById($formData['id']);

			if ($variantData)
			{
				if (($validationErrors = $productVariantsRepository->getValidationErrors($formData, $mode)) == false)
				{
					$productVariantsRepository->persist($formData, array('productAttributes' => $this->getProductAttributeRepository()->getListByProductId($formData['pid'])));
					$newStock = $this->getProductsRepository()->updateProductInventory($formData['pid'], $formData['inventory_control']);
					$this->touchProduct($formData['pid']);

					$result = array(
						'status' => 1,
						'variants' => $productVariantsRepository->getListByProductId($formData['pid']),
						'productStock' => ($newStock !== false ? $newStock : null)
					);
				}
				else
				{
					$result = array('status' => 0, 'errors' => $validationErrors);
				}
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find variant by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update variant. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Delete product variant
	 */
	public function deleteVariantAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productVariantsRepository = $this->getProductVariantRepository();

			$formData = array_merge($productVariantsRepository->getDefaults(), $request->get('variant', array()));

			$variantData = $productVariantsRepository->getById($formData['id']);

			if ($variantData)
			{
				$productVariantsRepository->delete(array($formData['id']));
				$newStock = $this->getProductsRepository()->updateProductInventory($formData['pid'], $formData['inventory_control']);
				$this->touchProduct($formData['pid']);

				$result = array(
					'status' => 1,
					'variants' => $productVariantsRepository->getListByProductId($formData['pid']),
					'productStock' => ($newStock !== false ? $newStock : null)
				);
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find variant by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update variant. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductVariantRepository
	 */
	protected function getProductVariantRepository()
	{
		if (is_null($this->productVariantRepository))
		{
			$this->productVariantRepository = new DataAccess_ProductVariantRepository($this->getDb());
		}

		return $this->productVariantRepository;
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductVariantRepository
	 */
	protected function getProductAttributeRepository()
	{
		if (is_null($this->productAttributeRepository))
		{
			$this->productAttributeRepository = new DataAccess_ProductAttributeRepository($this->getDb());
		}

		return $this->productAttributeRepository;
	}

	/**
	 * Get products repository
	 *
	 * @return DataAccess_ProductsRepository
	 */
	protected function getProductsRepository()
	{
		if (is_null($this->productsRepository))
		{
			$this->productsRepository = new DataAccess_ProductsRepository($this->getDb());
		}

		return $this->productsRepository;
	}

	/**
	 * Mark the product as updated
	 *
	 * @param $productDbId
	 */
	protected function touchProduct($productDbId)
	{
		$this->getProductsRepository()->touch($productDbId);

		$productData = $this->getProductsRepository()->getProductById($productDbId, true);

		if ($productData)
		{
			$event = new Events_ProductEvent(Events_ProductEvent::ON_UPDATED);
			$event->setData('product', $productData);
			Events_EventHandler::handle($event);
		}
	}
}
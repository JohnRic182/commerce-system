<?php

/**
 * Class Admin_Controller_BestsellersSettings
 */
class Admin_Controller_BestsellersSettings extends Admin_Controller
{
	/** @var array $menuItem */
	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'bestsellers');

	/**
	 * Update catalog settings
	 */
	public function updateAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_SettingsRepository $settings */
		$settings = $this->getSettings();

		$defaults = array(
			'CatalogBestsellersAvailable' => 'NO',
			'CatalogBestsellersCount' => '',
			'CatalogBestsellersPeriod' => '',
			'CustomerAlsoBoughtAvailable' => 'NO',
			'CustomerAlsoBoughtCount' => '',
		);

		$data = $settings->getByKeys(array_keys($defaults));

		// best seller settings
		$bestsellerSettingsForm = new Admin_Form_BestsellersSettingsForm();

		$form = $bestsellerSettingsForm->getForm($data);

		/**
		 * Process request
		 */
		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings_bestsellers');
			}
			else
			{
				$formData = array_merge($defaults, $request->request->all());

				$settings->persist($formData, array_keys($defaults));

				Admin_Flash::setMessage('common.success');
				$form->bind($formData);
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5014');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('settings_update'));

		$adminView->assign('body', 'templates/pages/settings/bestsellers-settings.html');
		$adminView->render('layouts/default');
	}
}
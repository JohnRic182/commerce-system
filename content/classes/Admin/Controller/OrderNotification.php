<?php

/**
 * Class Admin_Controller_OrderNotification
 */
class Admin_Controller_OrderNotification extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/order-notifications/';
	
	protected $Repository = null;
	
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');
	
	public function __construct(Framework_Request $request, DB $db)
	{
		parent::__construct($request, $db);
	}

	/**
	 * Add new order notification
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$TPForm = new Admin_Form_OrderNotificationForm();

		$defaults = $repository->getDefaults($mode);
		$defaults['is_active'] = 'Yes';

		$form = $TPForm->getForm($defaults, $mode, null, $repository->getTypeOptions());

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'order_notification_add'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('order_notifications');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
				{
					$id = $repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('order_notifications');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8017');
		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('order_notification_add'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * List order notifications
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$rep = $this->getDataRepository();

		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'name');

		$itemsCount = $rep->getCount();
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('notifications', $rep->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy));
		}
		else
		{
			$adminView->assign('notifications', null);
		}

		$adminView->assign('settingsForm', $this->getAdvancedSettingsForm());
		$adminView->assign('settingsNonce', Nonce::create('order_notifications_advanced_settings'));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8016');
		$adminView->assign('delete_nonce', Nonce::create('order_notification_delete'));
		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update order notification
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$id = intval($request->get('id'));

		$notification = $repository->getById($id);

		if (!$notification)
		{
			Admin_Flash::setMessage('Cannot find order notification by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('order_notifications');
		}

		$TPForm = new Admin_Form_OrderNotificationForm();

		$defaults = $repository->getDefaults($mode);
		$form = $TPForm->getForm($notification, $mode, $id, $repository->getTypeOptions());

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'order_notification_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('order_notifications');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('order_notifications', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8018');
		$adminView->assign('title', $notification['name']);
		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('order_notification_update'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete order notification
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$deleteMode = $request->get('deleteMode');
		
		if (!Nonce::verify($request->get('nonce'), 'order_notification_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('order_notifications');
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid order notification id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete(array($id));
				Admin_Flash::setMessage('Order notification has been deleted');
				$this->redirect('order_notifications');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one order notification to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('order_notifications');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('Selected order notifications have been deleted');
				$this->redirect('order_notifications');
			}
		}
		else if ($deleteMode == 'search')
		{
			$repository->deleteBySearchParams();
			Admin_Flash::setMessage('All order notifications have been deleted');
			$this->redirect('order_notifications');
		}
	}

	protected function getAdvancedSettingsForm()
	{
		$repository = $this->getDataRepository();

		$data = $repository->getAdvancedSettingsData();

		$data['notifyWhenOptions'] = $repository->getNotifyWhenOptions();

		$TRForm = new Admin_Form_OrderNotificationAdvancedSettingsForm();

		return $TRForm->getForm($data);
	}

	/**
	 * Edit Advanced Settings for Order Notifications
	 */
	public function editAdvancedSettingsAction()
	{
		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		/**
		 * Handle form post
		*/
		if ($request->isPost()){
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'order_notifications_advanced_settings', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$repository->persistAdvancedSettings($formData);

				$this->renderJson(array(
					'status' => 1,
					'message' => trans('common.success'),
				));
			}
		}
	}

	/**
	 * Get repository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->Repository))
		{
			$this->Repository = new DataAccess_OrderNotificationRepository($this->getDb(), $this->getSettings()->getAll());
		}
		return $this->Repository;
	}
}
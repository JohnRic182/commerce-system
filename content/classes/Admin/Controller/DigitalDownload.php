<?php

/**
 * Class Admin_Controller_DigitalDownload
 */
class Admin_Controller_DigitalDownload extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * Our default values for our form
	 * @var array
	 */
	protected $defaults = [
		'EnableDigitalDownload',
		'DigitalProductsDownloadLimit',
	];

	/**
	 * Our update action for digital download
	 */
	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$data = $settings->getByKeys($this->defaults);

		// Our form
		$storeSettingsForm = new Admin_Form_DigitalDownloadSettingsForm();
		$form = $storeSettingsForm->getForm($data);

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'digital_download'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('digital_download');
			}
			else
			{
				$validationErrors = array();
				$formData = array();
				foreach ($data as $key => $temp)
				{
					$formData[$key] = array_key_exists($key, $request->request->all()) ? $request->request->get($key) : '';

					/** @var core_Form_Field $field */
					$field = $storeSettingsForm->getBasicGroup()->get($key);

					if ($field && is_array($field) && isset($field['options']['required']) && $field['options']['required'] && $formData[$key] == '')
					{
						$validationErrors[$key] = array(trans('common.please_enter', array('field' => $field['options']['label'])));
					}
				}

				if (count($validationErrors) == 0)
				{
					$settings->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('digital_download');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		// Our views
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('digital_download'));

		$adminView->assign('body', 'templates/pages/settings/digital_download.html');
		$adminView->render('layouts/default');
	}
}
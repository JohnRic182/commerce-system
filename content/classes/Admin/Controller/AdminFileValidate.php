<?php

class Admin_Controller_AdminFileValidate extends Admin_Controller
{
    public function checkFileSizeAction()
    {
        if (!defined("ENV")) exit();
        $request = $this->getRequest();
        $postData = $request->request->all();
        $result = array("status" => 0, "message" => "Wrong request");
        $fileSize = $postData['filesize'];
        $uploadMaxFileSizeSetting = ini_get('upload_max_filesize');
        $uploadMaxFileSize = $this->returnBytes($uploadMaxFileSizeSetting);

        if ($fileSize > $uploadMaxFileSize)
        {
            $result = array("status" => 0, "message" => "The file size cannot be bigger than " . $uploadMaxFileSizeSetting);
        }
        else
        {
            $result = array("status" => 1, "message" => "Successful");
        }
            header('Content-Type: application/json');
            $this->renderJson($result);
	}

	/**
	 * @param $val
	 * @return int|string
	 */
    private function returnBytes($val)
	{
		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);
		switch($last)
		{
			case 'g': $val *= 1024;
			case 'm': $val *= 1024;
			case 'k': $val *= 1024;
		}

		return $val;
	}
}
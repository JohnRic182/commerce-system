<?php

class Admin_Controller_YahooExport extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		$data['categories'] = $categoryRepository->getOptionsList();

		$yahooExportForm = new Admin_Form_YahooExportForm();

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9007');

		$adminView->assign('form', $yahooExportForm->getForm($data));

		$adminView->assign('body', 'templates/pages/yahoo-export/index.html');
		$adminView->render('layouts/default');
	}
}
<?php

/**
 * Class Admin_Controller_StampsOrder
 */
class Admin_Controller_StampsOrder extends Admin_Controller
{
	/**
	 * @throws Exception
	 */
	public function getRatesAction()
	{
		$request = $this->getRequest();

		if (!$request->isPost()) return;

		$id = intval($request->request->get('id'));

		$isBongoOrder = false;
		$order_data = false;
		if ($id > 0)
		{
			$orderRepsitory = $this->getOrderRepository();
			$order_data = $orderRepsitory->getOrderDataForAdmin($id);

			if ($order_data)
			{
				$isBongoOrder = $order_data['payment_gateway_id'] == 'bongocheckout';
			}
		}

		if (!$order_data)
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => trans('orders.order_not_found'),
			));
			return;
		}

		$db = $this->getDb();
		$settings = $this->getSettings();
		$regions = new Regions($db);

		$user_data = $db->selectOne('SELECT * FROM '.DB_PREFIX.'users WHERE uid = '.$order_data['uid']);

		if ($isBongoOrder)
		{
			$stamps_state = $regions->getStateById($settings->get('bongocheckout_State'));
			$stamps_country = $regions->getCountryById(1);
			$stamps_rates_address = array(
				'FullName' => $settings->get('bongocheckout_Attention_To'),
				'Address1' => $settings->get('bongocheckout_Address1'),
				'Address2' => $settings->get('bongocheckout_Address2'),
				'City' => $settings->get('bongocheckout_City'),
				'State' =>  $stamps_state['short_name'],
				'Country' => $stamps_country['iso_a2'],
				'ZIPCode' => $settings->get('bongocheckout_Zip_Code'),
				'PhoneNumber' => $settings->get('bongocheckout_Phone')
			);
		}
		else
		{
			$stamps_country = $regions->getCountryById($order_data['shipping_country']);
			$stamps_rates_address = array(
				'FullName' => $order_data['shipping_name'],
				'Address1' => $order_data['shipping_address1'],
				'Address2' => $order_data['shipping_address2'],
				'City' => $order_data['shipping_city'],
				'PhoneNumber' => $user_data['phone'],
				'Country' => $stamps_country['iso_a2']
			);

			// set state data
			// TODO: change to shipments
//			if ($order_data['shipping_carrier_id'] == 'usps_international')
//			{
//				$stamps_rates_address['PostalCode'] = $order_data['shipping_zip'];
//				if (isset($order_data['shipping_province']) && $order_data['shipping_province'] != '')
//				{
//					$stamps_rates_address['Province'] = $order_data['shipping_province'];
//				}
//			}
//			else
			{
				$zipCodeParts = explode('-', trim($order_data['shipping_zip']));
				$stamps_rates_address['ZIPCode'] = substr($zipCodeParts[0], 0, 5);
				if (isset($zipCodeParts[1]) && $zipCodeParts[1] != '')
				{
					$stamps_rates_address['ZIPCodeAddOn'] = substr($zipCodeParts[1], 0, 4);
				}

				$stamps_state = $regions->getStateById($order_data['shipping_state']);
				if ($stamps_state)
				{
					$stamps_rates_address['State'] = $stamps_state['short_name'];
				}
			}
		}

		$stamps = new Stampscom($settings);
		if ($stamps_rates_address == 'US')
		{
			$stamps_address_cleanse_result = $stamps->apiMethodCleanseAddress($stamps_rates_address);

			$stamps_rates_address = $stamps_address_cleanse_result['original'];
			$stamps_cleansed_address = $stamps_address_cleanse_result['cleansed'];

			if (isset($stamps_cleansed_address['Address1']))
			{
				$stamps_rates_address['Address1'] = $stamps_cleansed_address['Address1'];
				$stamps_rates_address['Address2'] = $stamps_cleansed_address['Address2'];
				$stamps_rates_address['City'] = $stamps_cleansed_address['City'];
				$stamps_rates_address['State'] = $stamps_cleansed_address['State'];
				$stamps_rates_address['ZIPCode'] = $stamps_cleansed_address['ZIPCode'];

				if (isset($stamps_cleansed_address['ZIPCodeAddOn']) && $stamps_cleansed_address['ZIPCodeAddOn'] !== '0000')
				{
					$stamps_rates_address['ZIPCodeAddOn'] = $stamps_cleansed_address['ZIPCodeAddOn'];
				}

				if (isset($stamps_cleansed_address['Country']))
				{
					$stamps_rates_address['Country'] = $stamps_cleansed_address['Country'];
				}
			}
		}

		$stamps_weight_lbs = floatval($request->request->get('stamps_rate_weight_lbs'));
		$stamps_weight_oz = floatval($request->request->get('stamps_rate_weight_oz'));

		if ($stamps_weight_lbs == 0 && $stamps_weight_oz == 0)
		{
			$this->renderJson(array(
				'data' => 0,
				'errors' => array(
					array('field' => '#field-lbs-stamps_rate_weight', 'message' => 'Shipping weight is required')
				)
			));
			return;
		}

		$stamps_rate_ship_date = $request->request->get('stamps_rate_date');

		if ($stamps_rate_ship_date != '0')
		{
			$stamps_rate_ship_date = date('Y-m-d', strtotime($stamps_rate_ship_date));
		}
		else
		{
			$stamps_rate_ship_date = date('Y-m-d', time());
		}

		$stamps_get_rates_arguments = array(
			'Rate' => array(
				'ShipDate' => $stamps_rate_ship_date,
				'FromZIPCode' => $settings->get('ShippingOriginZip'),
				'ToZIPCode' => $stamps_rates_address['ZIPCode'],
				'WeightLb' => $stamps_weight_lbs,
				'WeightOz' => $stamps_weight_oz,
				'FromCountry' => 'US',
				'ToCountry' => $stamps_rates_address['Country']
			)
		);

		$stamps_insured_value = floatval($request->request->get('stamps_insured_value'));

		if ($stamps_insured_value > 0)
		{
			$stamps_get_rates_arguments['Rate']['InsuredValue'] = $stamps_insured_value;
		}

		$stamps_declared_value = trim($request->request->get('stamps_declared_value'));
		if (trim($stamps_declared_value) == '')
		{
			$db->query("SELECT SUM(price*quantity) as total_value FROM " . DB_PREFIX . "orders_content WHERE oid=" . $id);

			if ($db->moveNext())
			{
				$stamps_get_rates_arguments['Rate']['DeclaredValue'] = $db->col['total_value'];
			}
		}
		else
		{
			$stamps_get_rates_arguments['Rate']['DeclaredValue'] = floatval($request->request->get('stamps_declared_value'));
		}

		// call the GetRates Stamps.com API method
		$stamps_get_rates_result = $stamps->apiMethodGetRates($stamps_get_rates_arguments);

		if ($stamps_get_rates_result && isset($stamps_get_rates_result['Rates']))
		{
			$stamps_rates = $stamps_get_rates_result['Rates'];

			if (isset($stamps_rates['Rate'])) $stamps_rates = $stamps_rates['Rate'];
			if (!isset($stamps_rates[0])) $stamps_rates = array($stamps_rates);

			$stamps_rates_service_type_names = array(
				'US-XM' => 'USPS Express Mail',
				'US-FC' => 'USPS First Class Mail',
				'US-PM' => 'USPS Priority Mail',
				'US-PP' => 'USPS Parcel Post',
				'US-MM' => 'USPS Media Mail',
				#'US-BPM' => 'USPS BPM',
				#'US-LM' => 'USPS Library',
				'US-EMI' => 'USPS Express Mail International',
				'US-PMI' => 'USPS Priority Mail International',
				'US-FCI' => 'USPS First Class Mail International',
			);

			$stamps_rates_options = array();
			$stamps_rates_add_ons_costs = array();
			$stamps_rates_array = array();

			$stamps_available_add_ons = array(
				'US-A-DC' => 'USPS Tracking',
				'US-A-SC' => 'USPS Signature Confirmation',
				'US-A-INS' => 'USPS Insurance',
				'SC-A-INS' => 'Stamps.com Insurance',
				'US-A-CM' => 'USPS Certified Mail',
				'US-A-RD' => 'USPS Restricted Delivery',// (delivery to specifically named person only)',
				'US-A-NDW' => 'Express Mail item cannot be delivered on Saturdays',
				'US-A-RR' => 'USPS Return Receipt Requested',
				'SC-A-HP' => 'Generate label with Hidden Postage'
			);

			foreach ($stamps_rates as $stamps_rate_id => $stamps_rate)
			{
				// skip some rates
				if ($stamps_rate['ServiceType'] == 'US-PM' || $stamps_rate['ServiceType'] == 'US-XM' || $stamps_rate['ServiceType'] == 'US-FC')
				{
					if ($stamps_rate['PackageType'] == 'Letter' || $stamps_rate['PackageType'] == 'Postcard')
					{
						continue;
					}
				}

				$stamps_rates_array[$stamps_rate_id] = serialize($stamps_rate);

				$stamps_service_type = $stamps_rate['ServiceType'];
				$stamps_service_name =
					(isset($stamps_rates_service_type_names[$stamps_service_type]) ? $stamps_rates_service_type_names[$stamps_service_type] : $stamps_service_type).': '.
					$stamps_rate['PackageType'].($stamps_rate['Amount'] > 0 ? ' ($'.sprintf('%.2f',$stamps_rate['Amount']).')' : '');

				$stamps_rates_options[$stamps_rate_id] = array(
					'name' => $stamps_service_name,
					'type' => $stamps_service_type,
					'addons' => array()
				);

				if (isset($stamps_rate['AddOns']) && isset($stamps_rate['AddOns']['AddOnV2']) && is_array($stamps_rate['AddOns']['AddOnV2']))
				{
					$stamps_rate_add_ons = $stamps_rate['AddOns']['AddOnV2'];
					$stamps_rate_add_ons = isset($stamps_rate_add_ons[0]) ? $stamps_rate_add_ons : array($stamps_rate_add_ons);

					foreach ($stamps_rate_add_ons as $stamps_rate_add_on)
					{
						if (isset($stamps_rate_add_on['AddOnType']))
						{
							$stamps_rate_add_on_type = $stamps_rate_add_on['AddOnType'];

							if (array_key_exists($stamps_rate_add_on_type, $stamps_available_add_ons))
							{
								if (isset($stamps_rate_add_on['RequiresAllOf']['RequiresOneOf']['AddOnTypeV2']))
								{
									$requires = $stamps_rate_add_on['RequiresAllOf']['RequiresOneOf']['AddOnTypeV2'];
									$requires = isset($requires[0]) ? $requires : array($requires);
								}
								else
								{
									$requires = array();
								}

								if (isset($stamps_rate_add_on['ProhibitedWithAnyOf']['AddOnTypeV2']))
								{
									$prohibited = $stamps_rate_add_on['ProhibitedWithAnyOf']['AddOnTypeV2'];
									$prohibited = isset($prohibited[0]) ? $prohibited : array($prohibited);
								}
								else
								{
									$prohibited = array();
								}

								$stamps_rates_options[$stamps_rate_id]['addons'][$stamps_rate_add_on_type] = array(
									'code' => $stamps_rate_add_on_type,
									'name' => $stamps_available_add_ons[$stamps_rate_add_on_type],
									'amount' => isset($stamps_rate_add_on['Amount']) ? $stamps_rate_add_on['Amount'] : 0,
									'requires' => $requires,
									'prohibited' => $prohibited
								);
							}
						}
					}
				}
			}

			$session = $this->getSession();
			$session->set('stamps_rates_array', $stamps_rates_array);
			$session->set('stamps_rates_address', $stamps_rates_address);

			$formBuilder = new core_Form_FormBuilder('general');
			$basic = new core_Form_FormBuilder('basic');
			$formBuilder->add($basic);

			$rateOptions = array();
			foreach ($stamps_rates_options as $rateId => $rateOption)
			{
				$rateOptions[$rateId] = $rateOption['name'];
			}
			$basic->add('shipping_rate', 'choice', array('label' => 'Shipping Rate', 'options' => $rateOptions));

			$addOns = new core_Form_FormBuilder('addons', array('label' => 'Additional Services'));
			$formBuilder->add($addOns);

			if ($stamps_rates_address['Country'] != 'US')
			{
				$international = new core_Form_FormBuilder('international', array('International Customs'));
				$formBuilder->add($international);

				$international->add('ContentType', 'choice', array('label' => 'Content Type', 'options' => array(
					'Other' => 'Other',
					'Commercial Sample' => 'Commercial Sample',
					'Gift' => 'Gift',
					'Document' => 'Document',
				)));
				$international->add('ContentDescription', 'text', array('label' => 'Content Description'));
				$international->add('LicenseNumber', 'text', array('label' => 'License Number'));
				$international->add('CertificateNumber', 'text', array('label' => 'Certificate Number'));
			}

			$adminView = $this->getView();

			$adminView->assign('form', $formBuilder->getForm());

			$this->renderJson(array(
				'status' => 1,
				'options' => $stamps_rates_options,
				'html' => $adminView->fetch('generic-form'),
				'unique_id' => 'order-'.$id.'-'.uniqid(),
			));
		}
		else
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => 'No rates for current options',
			));
		}
	}

	/**
	 *
	 */
	public function generateLabelAction()
	{
		$session = $this->getSession();
		$settings = $this->getSettings();
		$db = $this->getDb();

		$request = $this->getRequest();

		if (!$request->isPost()) return;

		$id = intval($request->request->get('id'));

		$order_data = false;
		if ($id > 0)
		{
			$orderRepsitory = $this->getOrderRepository();
			$order_data = $orderRepsitory->getOrderDataForAdmin($id);
		}

		if (!$order_data)
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => trans('orders.order_not_found'),
			));
			return;
		}

		$stamps_label_olid = 0;
		$stamps_label_url = false;
		$stampsErrorMessage = false;

		$stamps_rate_id = $request->request->get('shipping_rate');
		$shipping_insured_value = floatval($request->request->get('stamps_insured_value'));
		$shipping_rate_date = $request->request->get('stamps_rate_date');
		$shipping_rate_weight_lbs = $request->request->get('stamps_rate_weight_lbs');
		$shipping_rate_weight_oz = $request->request->get('stamps_rate_weight_oz');
		$stamps_label_unique_id = $request->request->get('stamps_label_unique_id');

		if ($shipping_rate_date != '0')
		{
			$shipping_rate_date = date('Y-m-d', strtotime($shipping_rate_date));
		}
		else
		{
			$shipping_rate_date = date('Y-m-d', time());
		}

		$stamps_rate_add_ons = $request->request->getArray('stamps_rate_addons');

		$stamps_rates = $session->get('stamps_rates_array', false);
		$stamps_rates_address = $session->get('stamps_rates_address', false);

		if ($stamps_rate_id && $stamps_rates && isset($stamps_rates[$stamps_rate_id]))
		{
			$stampsErrorMessage = '';

			$stamps_rate = unserialize($stamps_rates[$stamps_rate_id]);

			$stamps_get_label_arguments = array();
			$stamps_get_label_arguments['IntegratorTxID'] = $stamps_label_unique_id; // we use this to ensure the user does not try to create duplicate labels
			//$stamps_get_label_arguments['TrackingNumber'] = '';
			$stamps_get_label_arguments['Rate'] = $stamps_rate;
			$stamps_get_label_arguments_rate_add_ons = array();

			if (isset($stamps_rate_add_ons['confirmation']) && $stamps_rate_add_ons['confirmation'] === '_NONE')
			{
				unset($stamps_rate_add_ons['confirmation']);
			}

			if (isset($stamps_rate_add_ons['insurance']) && $stamps_rate_add_ons['insurance'] === '_NONE')
			{
				unset($stamps_rate_add_ons['insurance']);
			}

			if (isset($stamps_get_label_arguments['Rate']['AddOns']['AddOnV2']))
			{
				if (isset($stamps_get_label_arguments['Rate']['AddOns']['AddOnV2']) && is_array($stamps_get_label_arguments['Rate']['AddOns']['AddOnV2']))
				{
					if (is_array($stamps_rate_add_ons) && !empty($stamps_rate_add_ons))
					{
						foreach ($stamps_get_label_arguments['Rate']['AddOns']['AddOnV2'] as $v)
						{
							foreach ($stamps_rate_add_ons as $stamps_rate_add_on)
							{
								if ($v['AddOnType'] == $stamps_rate_add_on)
								{
									$stamps_get_label_arguments_rate_add_ons[$v['AddOnType']] = 1;
								}
							}
						}
					}
				}

				unset($stamps_get_label_arguments['Rate']['AddOns']);
			}

			$stamps_get_label_arguments['Rate']['AddOns'] = array( 'AddOnV2' => array() );
			foreach ($stamps_get_label_arguments_rate_add_ons as $k=>$v)
			{
				$stamps_get_label_arguments['Rate']['AddOns']['AddOnV2'][] = array('AddOnType'=>$k);
			}

			/**
			 * Get from addresses
			 */
			$stamps_get_label_arguments_from_state = $settings->get('ShippingOriginProvince');

			$db->query('SELECT short_name FROM '.DB_PREFIX.'states WHERE coid='.intval($settings->get('ShippingOriginCountry')).' AND stid='.intval($settings->get('ShippingOriginState')));

			if ($db->moveNext())
			{
				$stamps_get_label_arguments_from_state = $db->col['short_name'];
			}

			$stamps_get_label_arguments['From'] = array();
			$stamps_get_label_arguments['From']['FullName'] = $settings->get('ShippingOriginName');
			$stamps_get_label_arguments['From']['Address1'] = $settings->get('ShippingOriginAddressLine1');

			if (strlen($settings->get('ShippingOriginAddressLine2')))
			{
				$stamps_get_label_arguments['From']['Address2'] = $settings->get('ShippingOriginAddressLine2');
			}

			$stamps_get_label_arguments['From']['City'] = $settings->get('ShippingOriginCity');
			$stamps_get_label_arguments['From']['State'] = $stamps_get_label_arguments_from_state;
			$stamps_get_label_arguments['From']['ZIPCode'] = $settings->get('ShippingOriginZip');
			$stamps_get_label_arguments['From']['PhoneNumber'] = preg_replace("/[^0-9]/","", $settings->get('ShippingOriginPhone'));

			$stamps_get_label_arguments_to_keys = array(
				'FullName', 'NamePrefix', 'FirstName', 'MiddleName', 'LastName',
				'NameSuffix', 'Title', 'Department', 'Company',
				'Address1', 'Address2', 'Address3',
				'City', 'Country', 'PhoneNumber', 'Extension',
			);

			if ($order_data['shipping_carrier_id'] == 'usps_international')
			{
				$stamps_get_label_arguments_to_keys[] = 'Province';
				$stamps_get_label_arguments_to_keys[] = 'PostalCode';
				$stamps_get_label_arguments_to_keys[] = 'Urbanization';
			}
			else
			{
				$stamps_get_label_arguments_to_keys[] = 'State';
				$stamps_get_label_arguments_to_keys[] = 'ZIPCode';
				$stamps_get_label_arguments_to_keys[] = 'ZIPCodeAddOn';
				$stamps_get_label_arguments_to_keys[] = 'CheckDigit';
				$stamps_get_label_arguments_to_keys[] = 'DPB';
			}

			$stamps_get_label_arguments['To'] = array();
			foreach ($stamps_get_label_arguments_to_keys as $k)
			{
				if (array_key_exists($k, $stamps_rates_address))
				{
					$stamps_get_label_arguments['To'][$k] = $stamps_rates_address[$k];
				}
				else
				{
					$stamps_get_label_arguments['To'][$k] = '';
				}
			}

			if (isset($stamps_rates_address['OverrideHash']))
			{
				$stamps_get_label_arguments['To']['OverrideHash'] = $stamps_rates_address['OverrideHash'];
			}

			$stamps_get_label_arguments['SampleOnly'] = false;

			if (preg_match('/^US-(?:EM|PM|FC)I$/', $stamps_get_label_arguments['Rate']['ServiceType']))
			{
				$stamps_get_label_arguments['ImageType'] = 'Pdf';
			}
			else
			{
				$stamps_get_label_arguments['ImageType'] = 'Png';
			}

			if (isset($_REQUEST['stamps_customs_ContentType']))
			{
				$stamps_customs_ContentType = trim($_REQUEST['stamps_customs_ContentType']);
				$stamps_customs_OtherDescribe = trim($_REQUEST['stamps_customs_OtherDescribe']);
				$stamps_customs_LicenseNumber = trim($_REQUEST['stamps_customs_LicenseNumber']);
				$stamps_customs_CertificateNumber = trim($_REQUEST['stamps_customs_CertificateNumber']);

				$stamps_get_label_arguments['Customs'] = array('ContentType' => $stamps_customs_ContentType);

				if ($stamps_customs_ContentType == 'Other') $stamps_get_label_arguments['Customs']['OtherDescribe'] = $stamps_customs_OtherDescribe;
				if ($stamps_customs_LicenseNumber != '') $stamps_get_label_arguments['Customs']['LicenseNumber'] = $stamps_customs_LicenseNumber;
				if ($stamps_customs_CertificateNumber != '') $stamps_get_label_arguments['Customs']['CertificateNumber'] = $stamps_customs_CertificateNumber;

				$stamps_get_label_arguments['Customs']['InvoiceNumber'] = $order_data['order_num'];

				$stamps_get_label_arguments['Customs']['CustomsLines'] = array();
				$lineCount = 0;
				$orderLineItemsForStamps = $db->selectAll('SELECT * FROM '.DB_PREFIX.'orders_content WHERE oid = '.$id);
				foreach ($orderLineItemsForStamps as $orderLineItem)
				{
					// Zero is disallowed for APO/FPO destinations.
					if ($lineCount < 5 && $orderLineItem['admin_price'] > 0)
					{
						$stamps_get_label_arguments['Customs']['CustomsLines']['CustomsLine'][] = array(
							'Description' => substr($orderLineItem['title'], 0, 60),
							'Quantity' => $orderLineItem['admin_quantity'],
							'Value' => round($orderLineItem['admin_quantity'] * $orderLineItem['admin_price'], 2),
							'WeightLb' => round($orderLineItem['admin_quantity'] * $orderLineItem['weight'] * ($settings->get('LocalizationWeightUnits') == 'kg' ? 2.2046226 : 1), 2)
						);
						$lineCount++;
					}
				}
			}

			$stamps_label_olid = 0;

			$db->query("SELECT unique_id FROM ".DB_PREFIX."orders_labels WHERE oid=".$id." AND unique_id='".$db->escape($stamps_label_unique_id)."'");

			if (!strlen($stamps_label_unique_id))
			{
				$stampsErrorMessage = 'There was an error generating a unique ID for this label. Please try again.';
			}
			elseif ($db->moveNext())
			{
				$stampsErrorMessage = 'You have already tried to generate this label.  Please check for the label on the "View Stamps.com Shipping Labels for this Order" tab or try again.';
			}
			else
			{
				$stamps = new Stampscom($settings);
				if ($settings->get('StampscomTestMode') == 'Yes')
				{
					$stamps_get_label_result = $stamps->apiCall('CreateTestIndicium', $stamps_get_label_arguments);
				}
				else
				{
					$stamps_get_label_result = $stamps->apiCall('CreateIndicium', $stamps_get_label_arguments);
				}

				$db->reset();
				$db->assignStr('oid', $id);
				$db->assignStr('unique_id', $stamps_label_unique_id);
				$db->assignStr('type', 'stampscom');
				$db->assign('date_created', 'NOW()');

				$db->assignStr('shipping_date', $shipping_rate_date);
				$db->assignStr('shipping_weight', $shipping_rate_weight_lbs + ($shipping_rate_weight_oz / 16));
				$db->assignStr('shipping_insurance', $shipping_insured_value);
				$db->assignStr('data', serialize($stamps_get_label_result));

				if (isset($stamps_get_label_result['faultcode']))
				{
					$stampsErrorMessage = 'There was an error generating the label. ';
					$db->assignStr('status', 'Failed');

					if ( !($stampscom_tmp_olid=$db->insert(DB_PREFIX."orders_labels")))
					{
						$stampsErrorMessage .= 'Additionally, there was an error updating the label entry database. ';
					}

					if (isset($stamps_get_label_result['faultstring']))
					{
						$stampsErrorMessage .= 'Message: '.$stamps_get_label_result['faultstring'];
					}
				}
				elseif (isset($stamps_get_label_result['URL']))
				{
					$stamps_label_url = $stamps_get_label_result['URL'];
					$db->assignStr('status', 'Created');
					$db->assignStr('url', $stamps_label_url);
					$db->assignStr('shipping_service_type', $stamps_get_label_result['Rate']['ServiceType']);
					$db->assignStr('shipping_package_type', $stamps_get_label_result['Rate']['PackageType']);

					if (isset($stamps_get_label_result['StampsTxID']))
					{
						$db->assignStr('transaction_id', $stamps_get_label_result['StampsTxID']);
					}

					if (isset($stamps_get_label_result['TrackingNumber']))
					{
						$db->assignStr('tracking_number', $stamps_get_label_result['TrackingNumber']);
					}

					if (!($stamps_label_olid = $db->insert(DB_PREFIX."orders_labels")))
					{
						$stampsErrorMessage = 'There was an error updating the labels database';
					}
					else
					{
						if ($settings->get('StampscomTestMode') == 'Yes')
						{
							Admin_Flash::setMessage('Your test label was successfully created');
						}
						else
						{
							Admin_Flash::setMessage('Your label was successfully purchased and generated');
						}
					}
				}
			}
		}
		else
		{
			$stampsErrorMessage = 'No rate found by that id';
		}

		if ($stamps_label_olid > 0 && $stamps_label_url)
		{
			$session->remove('stamps_rates_array');
			$session->remove('stamps_rates_address');
			$data = array(
				'status' => 1,
				'olid' => $stamps_label_olid,
			);
		}
		else
		{
			$data = array(
				'status' => 0,
				'message' => $stampsErrorMessage,
			);
		}

		$this->renderJson($data);
	}

	/**
	 *
	 */
	public function cancelLabelAction()
	{
		$request = $this->getRequest();
		$stamps_label_oid = intval($request->query->get('oid', 0));
		$stamps_label_olid = intval($request->request->get('olid', 0));

		$stamps_cancel_error_msg = '';

		$db = $this->getDb();
		$db->reset();
		$db->query("SELECT oid, olid, url, transaction_id, tracking_number FROM ".DB_PREFIX."orders_labels WHERE oid=".$stamps_label_oid." AND olid=".$stamps_label_olid." AND type='stampscom'");

		if ($row = $db->moveNext())
		{
			if (trim($row['transaction_id']) != '')
			{
				$stamps_cancel_arguments = array('StampsTxID' => $db->col['transaction_id']);

				$stamps = new Stampscom($this->getSettings());
				$stamps_cancel_result = $stamps->apiCall('CancelIndicium', $stamps_cancel_arguments);

				if (isset($stamps_cancel_result['faultcode']))
				{
					$stamps_cancel_error_msg = "An error occurred when trying to cancel the label";

					if (isset($stamps_cancel_result['faultstring']))
					{
						$stamps_cancel_error_msg .= ": ".$stamps_cancel_result['faultstring'];
					}
				}
			}

			if ($stamps_cancel_error_msg == '')
			{
				$db->reset();
				$db->assignStr('status', 'Canceled');
				$db->update(DB_PREFIX."orders_labels","WHERE oid=".intval($stamps_label_oid)." AND olid=".intval($stamps_label_olid)." AND type='stampscom'");
			}
		}
		else
		{
			$stamps_cancel_error_msg = "Cannot find label using provided data";
		}

		if (trim($stamps_cancel_error_msg) == '')
		{
			Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
			$this->renderJson(array(
				'status' => 1,
			));
		}
		else
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => $stamps_cancel_error_msg
			));
		}
	}

	/**
	 * Label view iframe
	 */
	public function labelViewIframeAction()
	{
		$request = $this->getRequest();
		$db = $this->getDb();
		
		$oid = intval($request->get('oid', 0));
		$olid = intval($request->get('olid', 0));
		$imageUrls = $request->get('image_urls', '');

		if ($imageUrls == '')
		{
			$db->reset();
			$db->query('SELECT oid, olid, url FROM ' . DB_PREFIX . 'orders_labels WHERE oid=' . $oid . ' AND olid=' . $olid . ' AND type="stampscom"');
			if ($db->moveNext())
			{
				$imageUrls = trim($db->col['url']);
			}
		}

		$response = '';

		if (strlen($imageUrls))
		{
			$isPdf = strpos($imageUrls, '.pdf?') > 0;

			$imageUrlsArr = preg_split('/\s+/', $imageUrls);

			foreach ($imageUrlsArr as $imageUrl)
			{
				if ($isPdf)
				{
					$response .=
						"<script type=\"text/javascript\"><!--\n" .
						"document.location = '" . $imageUrl . "';\n" .
						"--></script>\n";
				}
				else
				{
					$response .=
						"<style type=\"text/css\">\n" .
							"* {\n" .
							"\tmargin: 0 !important;\n" .
							"\tpadding: 0 !important;\n" .
							"}\n" .
						"</style>\n" .
						"<div style=\"text-align:center\"><img src=\"" . $imageUrl . " class=\"print-label-image\"></div>\n";
				}
			}
		}

		$this->renderRaw($response);
	}

	/**
	 *
	 */
	public function labelTrackIframeAction()
	{
		$request = $this->getRequest();
		$db = $this->getDb();

		$oid = intval($request->get('oid', 0));
		$olid = intval($request->get('olid', 0));

		$db->reset();
		$db->query('
			SELECT oid, olid, url, transaction_id, tracking_number
			FROM ' . DB_PREFIX . 'orders_labels
			WHERE oid=' . $oid . ' AND olid= ' . $olid . ' AND type="stampscom"
		');

		if ($db->moveNext())
		{
			$stampscom_tmp_db = null; # not $db to use read-only

			$settingsRepository = $this->getSettings();
			$stampscomObj = new Stampscom($settingsRepository);

			$arguments = array('StampsTxID' => $db->col['transaction_id']);
			$return = $stampscomObj->apiCall('TrackShipment', $arguments);

			$events = isset($return['TrackingEvents']) ? is_array($return['TrackingEvents']) ? $return['TrackingEvents'] : array() : array();

			if (isset($events['TrackingEvent']))
			{
				$events = isset($events['TrackingEvent'][0]) ? $events['TrackingEvent'] : array($events['TrackingEvent']);
			}

			if (count($events))
			{
				$response =
					"<table id=\"tracking_table\" class=\"admin-list-table\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" .
					"\t<thead>\n" .
					"\t\t<tr>\n" .
					"\t\t\t<th>Date</th>\n" .
					"\t\t\t<th>Event</th>\n" .
					"\t\t\t<th>Location</th>\n" .
					"\t\t</tr>\n" .
					"\t</thead>\n" .
					"\t<tbody>";

				foreach ($events as $event)
				{
					$response .=
						"\t\t<tr>\n" .
						"\t\t\t<td nowrap=\"nowrap\" style=\"font-size:0.75em;\">" . htmlspecialchars($event['Timestamp']) . "</td>\n" .
						"\t\t\t<td nowrap=\"nowrap\" style=\"font-size:0.75em;\">" . htmlspecialchars($event['Event']) . "</td>\n" .
						"\t\t\t<td nowrap=\"nowrap\" style=\"font-size:0.75em;\">\n";

					$locationString = '';
					if ($event['City']) $locationString .= $event['City'];
					if ($event['State']) $locationString .= ($locationString!=''?', ':'') . $event['State'];
					if ($event['Zip']) $locationString .= ($locationString!=''?', ':'') . $event['Zip'];
					if ($event['Country']) $locationString .= ($locationString!=''?', ':'') . $event['Country'];
					$response .= htmlspecialchars($locationString);
					$response .=
						"\t\t\t</td>\n" .
						"\t\t</tr>\n";
				}

				$response .=
					"\t</tbody>\n" .
					"</table>\n";
			}
			else
			{
				$response = "<div>There is no tracking information available at this moment</div>";
			}
		}

		$this->renderRaw($response);
	}

	/**
	 * @return DataAccess_OrderRepository
	 */
	protected function getOrderRepository()
	{
		$db = $this->getDb();
		$settings = $this->getSettings()->getAll();

		return new DataAccess_OrderRepository($db, $settings);
	}
}
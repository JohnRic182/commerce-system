<?php

/**
 * Class Admin_Controller_PromoCode
 */
class Admin_Controller_PromoCode extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/promo-code/';

	protected $repository = null;
	
	protected $categoriesRepository = null;

	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'promotions');

	/** @var array */
	protected $searchParamsDefaults = array('search_str' => '', 'logic' => 'AND');

	/**
	 * Admin_Controller_PromoCode constructor.
	 * @param Framework_Request $request
	 * @param DB $db
	 */
	public function __construct(Framework_Request $request, DB $db)
	{
		parent::__construct($request, $db);
	}

	/**
	 * List Promo codes
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$rep = $this->getDataRepository();

		$request = $this->getRequest();
		$session = $this->getSession();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('productsSearchParams', array()); else $session->set('productsSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('productsSearchOrderBy', 'name'); else $session->set('productsSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('productsSearchOrderDir', 'asc'); else $session->set('productsSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		$itemsCount = $rep->getCount($searchParams, $searchParams['logic']);

		$promoCodeForm = new Admin_Form_PromoCodeForm();
		$adminView->assign('searchForm', $promoCodeForm->getPromoCodeSearchForm($searchFormData));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign(
				'promo_codes',
				$rep->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'p.*', $searchParams['logic']
				)
			);
		}
		else
		{
			$adminView->assign('promo_codes', null);
		}

		$adminView->assign('settingsForm', $this->getSettingsForm());
		$adminView->assign('settingsNonce', Nonce::create('settings_update'));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5000');
		$adminView->assign('delete_nonce', Nonce::create('promo_code_delete'));
		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new promo code
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$TPForm = new Admin_Form_PromoCodeForm();

		$defaults = $repository->getDefaults($mode);

		$currency = $this->getSession()->get('admin_currency');

		$form = $TPForm->getForm($defaults, $mode, null, $currency);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());
			
			if (!Nonce::verify($formData['nonce'], 'promo_code_add'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('promotions');
			}
			else{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
				{
					$id = $repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('promotions&mode=update&id=' . $id);
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5001');
		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('promo_code_add'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update promo code
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();
		$categoriesRepository = $this->getCategoryRepository();

		$id = intval($request->get('id'));

		$promo_code = $repository->getById($id);
		if (!$promo_code)
		{
			Admin_Flash::setMessage('Cannot find promo code by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('promotions&mode=update&id=' . $id);
		}
		
		$categories = $categoriesRepository->getOptionsList();
		if (is_array($categories))
		{
			$promo_code['categories'] = $categories;
			$promo_code['categories_selected'] = $repository->getAssignedCategories($id);
			
		}

		$TPForm = new Admin_Form_PromoCodeForm();

		$defaults = $repository->getDefaults($mode);

		$promo_code = array_merge($defaults, $promo_code);
		
		$form = $TPForm->getForm($promo_code, $mode, $id);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'promo_code_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('promotions');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('promotions', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5002');
		$adminView->assign('title', $promo_code['name']);
		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('promo_code_update'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete promo codes
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		$repository = $this->getDataRepository();
		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce'), 'promo_code_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('promotions');
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid promo code id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete(array($id));
				Admin_Flash::setMessage('Promo code has been deleted');
				$this->redirect('promotions');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one promo code to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('promotions');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('Selected promo codes have been deleted');
				$this->redirect('promotions');
			}
		}
		else if ($deleteMode == 'search')
		{
			$repository->deleteBySearchParams();
			Admin_Flash::setMessage('All promo codes have been deleted');
			$this->redirect('promotions');
		}
	}

	/**
	 * @return core_Form_Form
	 */
	protected function getSettingsForm()
	{
		$repository = $this->getDataRepository();

		$data = $repository->getAdvancedSettingsData();

		$TRForm = new Admin_Form_PromoCodeAdvancedSettingsForm();

		return $TRForm->getForm($data);
	}

	/**
	 * Edit promo codes settings
	 */
	public function editAdvancedSettingsAction()
	{
		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'settings_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$repository->persistPromoCodeSettings($formData);

				$this->renderJson(array(
					'status' => 1,
					'message' => trans('common.success'),
				));
			}
		}
	}

	/**
	 * Get json array of products assigned to promo code
	 */
	public function getAssignedProductsAction()
	{
		$request = $this->getRequest();
		$promo_id = intval($request->get('promo_id'));
		$categories = intval($request->get('categories'));
		$repository = $this->getDataRepository();
		$result = $repository->getAssignedProducts($categories, $promo_id);

		$this->renderJson($result);
	}

	/**
	 * Get all products for selected category
	 */
	public function getProductsAction()
	{
		$request = $this->getRequest();
		$promo_id = intval($request->get('promo_id'));
		$categories = intval($request->get('categories'));
		$repository = $this->getDataRepository();
		$result = $repository->getProducts($categories, $promo_id);

		$this->renderJson($result);
	}

	/**
	 * Assign products to promo code
	 */
	public function assignProductsAction()
	{
		$request = $this->getRequest();
		$promo_id = intval($request->get('promo_id'));
		$products = $request->get('products');
		$promo_categories = $request->get('promo_categories');
		$repository = $this->getDataRepository();
		$repository->persistPromoCategories($promo_id, $promo_categories);
		$result = $repository->assignProducts($products, $promo_id);

		$this->renderJson($result);
	}

	/**
	 * Unassign products from product promo code
	 */
	public function unassignProductsAction()
	{
		$request = $this->getRequest();
		$promo_id = intval($request->get('promo_id'));
		$products = $request->get('products');
		$promo_categories = $request->get('promo_categories');
		$repository = $this->getDataRepository();
		$repository->persistPromoCategories($promo_id, $promo_categories);
		$result = $repository->unassignProducts($products, $promo_id);

		$this->renderJson($result);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_PromoCodeRepository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->repository))
		{
			$settings = $this->getSettings()->getAll();
			$this->repository = new DataAccess_PromoCodeRepository($this->getDb(), $settings);
		}

		return $this->repository;
	}

	/**
	 * @return DataAccess_CategoryRepository|null
	 */
	protected function getCategoryRepository()
	{
		if (is_null($this->categoriesRepository))
		{
			$settings = $this->getSettings()->getAll();
			$this->categoriesRepository = new DataAccess_CategoryRepository($this->getDb(), $settings);
		}

		return $this->categoriesRepository;
	}
}
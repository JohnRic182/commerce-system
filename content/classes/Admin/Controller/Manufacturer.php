<?php

/**
 * Class Admin_Controller_Manufacturer
 */
class Admin_Controller_Manufacturer extends Admin_Controller
{
	/** @var DataAccess_ManufacturerRepository $manufacturerRepository */
	protected $manufacturerRepository = null;

	protected $searchParamsDefaults = array('search_str' => '', 'visibility' => 'all', 'logic' => 'AND');

	protected $acceptedTypes = array(IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_PNG);

	protected $typesExtensions = array(IMAGETYPE_JPEG => 'jpg', IMAGETYPE_GIF => 'gif', IMAGETYPE_PNG => 'png');

	protected $imagesPath = 'images/manufacturers';

	protected $menuItem = array('primary' => 'products', 'secondary' => 'manufacturers');

	/**
	 * List manufacturers
	 */
	public function listAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3012');

		/** @var DataAccess_ManufacturerRepository $manufacturerRepository */
		$manufacturerRepository = $this->getManufacturerRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/**  Prepare search & list parameters   */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('manufacturersSearchParams', array()); else $session->set('manufacturersSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('manufacturersSearchOrderBy', 'manufacturer_name'); else $session->set('manufacturersSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('manufacturersSearchOrderDir', 'asc'); else $session->set('manufacturersSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		/** @var Admin_Form_ManufacturerForm $manufacturerForm */
		$manufacturerForm = new Admin_Form_ManufacturerForm();
		$adminView->assign('searchForm', $manufacturerForm->getManufacturerSearchForm($searchFormData));
		$itemsCount = $manufacturerRepository->getCount($searchParams, $searchParams['logic']);

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('manufacturers',
				$manufacturerRepository->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'*',
					$searchParams['logic']
				)
			);
		}
		else
		{
			$adminView->assign('manufacturers', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('manufacturer_delete'));

		$adminView->assign('body', 'templates/pages/manufacturer/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new manufacturer
	 * Add new manufacturer
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_ManufacturerRepository $manufacturerRepository */
		$manufacturerRepository = $this->getManufacturerRepository();

		/** @var Admin_Form_ManufacturerForm $manufacturerForm */
		$manufacturerForm = new Admin_Form_ManufacturerForm();

		$defaults = $manufacturerRepository->getDefaults($mode);

		$defaults['is_visible'] = 1;
		/** @var core_Form_Form $form */
		$form = $manufacturerForm->getForm($defaults, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!$request->request->has('is_visible')) $formData['is_visible'] = 0;

			if (!Nonce::verify($formData['nonce'], 'manufacturer_add'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('manufacturers');
			}
			else
			{
				if (($validationErrors = $manufacturerRepository->getValidationErrors($formData, $mode)) == false)
				{
					if ($formData['url_custom'] != '' && $manufacturerRepository->hasUrlDuplicate($formData['url_custom'], 0))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$id = $manufacturerRepository->persist($formData);
					$formData['id'] = $id;

					// TODO - optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, false, true);

					if ($imageUploadInfo = $request->getUploadedFile('image'))
					{
						/** @var Admin_Service_FileUploader $fileUploaderService */
						$fileUploadService = new Admin_Service_FileUploader();

						$imageFile = $fileUploadService->processImageUpload($imageUploadInfo, $this->imagesPath, $id);

						if ($imageFile)
						{
							$settings = $this->getSettings();

							$imageFileThumb = $this->imagesPath . '/thumbs/' . intval($id) . '.jpg';
							ImageUtility::generateManufacturerImage($imageFile, $imageFileThumb);
							
							if ($settings->get('CatalogOptimizeImages') == 'YES')
							{
								$optimizer = new Admin_Service_ImageOptimizer();
								$optimizer->optimize($imageFile);
							}

							$formData['image'] = $imageFile;
							// optimize this
							$manufacturerRepository->persist($formData);
						}
					}

					Admin_Flash::setMessage('common.success');
					$this->redirect('manufacturer', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3013');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('manufacturer_add'));

		$adminView->assign('body', 'templates/pages/manufacturer/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update manufacturer
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_ManufacturerRepository $manufacturerRepository */
		$manufacturerRepository = $this->getManufacturerRepository();

		$id = intval($request->get('id'));

		$manufacturer = $manufacturerRepository->getById($id);

		if (!$manufacturer)
		{
			Admin_Flash::setMessage('Cannot find manufacturer by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('manufacturers');
		}

		/** @var Admin_Form_ManufacturerForm $manufacturerForm */
		$manufacturerForm = new Admin_Form_ManufacturerForm();

		$defaults = $manufacturerRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $manufacturerForm->getForm($manufacturer, $mode, $id);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'manufacturer_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('manufacturers');
			}
			else
			{
				if (($validationErrors = $manufacturerRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					if ($imageUploadInfo = $request->getUploadedFile('image'))
					{
						// initialize thumb info before processImageUpload
						// reason: tmp image is deleted inside the call
						$thumbImageInfo = getimagesize($imageUploadInfo['tmp_name']);
						$thumbImageType = $thumbImageInfo[2];

						/** @var Admin_Service_FileUploader $fileUploaderService */
						$fileUploadService = new Admin_Service_FileUploader();

						$imageFile = $fileUploadService->processImageUpload($imageUploadInfo, $this->imagesPath, $id, true);

						if ($imageFile)
						{
							$settings = $this->getSettings();

							if (in_array($thumbImageType, $this->acceptedTypes))
							{
								$fileUploadService->deleteSameFilename($this->imagesPath . '/thumbs', $id);

								$imageFileThumb = $this->imagesPath . '/thumbs/' . intval($id) . '.' . $this->typesExtensions[$thumbImageType];
								ImageUtility::generateManufacturerImage($imageFile, $imageFileThumb);

								if ($settings->get('CatalogOptimizeImages') == 'YES')
								{
									$optimizer = new Admin_Service_ImageOptimizer();
									$optimizer->optimize($imageFile);
									$optimizer->optimize($imageFileThumb);
								}
							}

							$formData['image'] = $imageFile;
						}
					}

					if ($formData['url_custom'] != '' && $manufacturerRepository->hasUrlDuplicate($formData['url_custom'], $id))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$manufacturerRepository->persist($formData);

					// TODO - optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, false, true);

					Admin_Flash::setMessage('common.success');
					$this->redirect('manufacturer', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		$settings = $this->getSettings();
		$manufacturerUrl = $settings->get('INDEX').'?p=catalog&amp;mode=manufacturer&mid='.$id;
		if ($settings->get('USE_MOD_REWRITE') == 'YES')
		{
			$manufacturerUrl = $manufacturer['url_custom'] != '' ? $manufacturer['url_custom'] : $manufacturer['url_default'];
		}
		$adminView->assign('manufacturerUrl', $manufacturerUrl);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3014');
		$adminView->assign('title', $manufacturer['manufacturer_name']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('manufacturer_update'));

		$adminView->assign('body', 'templates/pages/manufacturer/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete image
	 */
	public function deleteImageAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_ManufacturerRepository $manufacturerRepository */
		$manufacturerRepository = $this->getManufacturerRepository();

		$id = intval($request->get('id'));

		$manufacturer = $manufacturerRepository->getById($id);

		if (!$manufacturer)
		{
			$result = array('status' => 0, 'error' => 'Cannot find manufacturer by provided id');
		}
		else
		{
			$imageUrl = $manufacturer['image'];
			@unlink($imageUrl);

			foreach ($this->typesExtensions as $imageType => $imageExtension)
			{
				@unlink($this->imagesPath . '/thumbs/' . intval($id) . '.' . $imageExtension);
			}

			$manufacturer['image'] = '';
			$manufacturerRepository->persist($manufacturer);

			$result = array('status' => 1);
		}

		$this->renderJson($result);
	}

	/**
	 * Delete manufacturers
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'manufacturer_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('manufacturers');
			return;
		}

		/** @var DataAccess_ManufacturerRepository $manufacturerRepository */
		$manufacturerRepository = $this->getManufacturerRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid manufacturer id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$manufacturerRepository->delete($id);
				Admin_Flash::setMessage('Manufacturer has been deleted');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one manufacturer to delete', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$manufacturerRepository->delete($ids);
				Admin_Flash::setMessage('Selected manufacturers have been deleted');
			}
		}
		else if ($deleteMode == 'search')
		{
			/** @var Framework_Session $session */
			$session = $this->getSession();
			$searchParams = $session->get('manufacturersSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());
			$manufacturerRepository->deleteBySearchParams($searchParams);
			Admin_Flash::setMessage('Selected manufacturers have been deleted');
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('manufacturers');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ManufacturerRepository
	 */
	protected function getManufacturerRepository()
	{
		if (is_null($this->manufacturerRepository))
		{
			$this->manufacturerRepository = new DataAccess_ManufacturerRepository($this->getDb());
		}

		return $this->manufacturerRepository;
	}

	/**
	 * Get seo URL settings
	 *
	 * @return mixed|string
	 */
	protected function getSeoUrlSettings()
	{
		/** @var DataAccess_SettingsRepository */
		$settings = $this->getSettings();

		return json_encode(array(
			'template' => $settings->get('SearchURLManufacturerTemplate'),
			'lowercase' => $settings->get('SearchAutoGenerateLowercase') == 'YES',
			'joiner' => $settings->get('SearchURLJoiner')
		));
	}
}

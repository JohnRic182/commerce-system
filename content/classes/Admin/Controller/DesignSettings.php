<?php

/**
 * Class Admin_Controller_DesignSettings
 */
class Admin_Controller_DesignSettings extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/design-settings/';

	protected $menuItem = array('primary' => 'design', 'secondary' => 'theme');

	/**
	* List themes
	*/
	public function listAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '7000');
		
		$selectionData = $this->getSelectionData();

		$adminView->assign('themeCollection', $selectionData['themeCollection']);
		$adminView->assign('selectedTheme', $selectionData['selectedTheme']);
		$adminView->assign('existingThemes', $selectionData['existingThemes']);
		$adminView->assign('themesInstallable', $selectionData['themesInstallable']);

		$settings = $this->getSettings();
		$session = $this->getSession();

		$themeInstalled = $session->get('THEME_INSTALLED', false);
		$session->remove('THEME_INSTALLED');
		$themeHasTestProducts = @file_exists(dirname(dirname(dirname(dirname(__FILE__)))).'/skins/'.$settings->get('DesignSkinName').'/fixtures/test_products.sql');

		$adminView->assign('themeInstalled', $themeInstalled ? 1 : 0);
		$adminView->assign('themeHasTestProducts', $themeHasTestProducts ? 1 : 0);

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	protected function getSelectionData()
	{
		$themesFolderPath = 'content/skins';

		$settings = $this->getSettings();

		/** @var DataAccess_SiteThemeRepository $siteThemesRepository */
		$siteThemesRepository = DataAccess_Repository::get('SiteThemeRepository');
		$themes = $siteThemesRepository->getInstalledThemes($themesFolderPath);

		$themeCollection = array();
		$existingThemes = array();
		$selectedTheme = null;
		foreach ($themes as $themeId => $theme)
		{
			/** @var SiteTheme $theme*/

			$pathToImages = $themesFolderPath.'/'.$themeId.'/';

			$themeHelper = array(
				'name' => htmlentities($theme->getName()),
				'title' => $theme->getTitle(),
				'author' => $theme->getAuthor(),
				'thumbnail_image' => ($theme->getThumbnailImage()) ? $pathToImages.escapeFileName($theme->getThumbnailImage()) : null,
				'preview_image' => ($theme->getFullsizeImage()) ? $pathToImages.escapeFileName($theme->getFullsizeImage()) : null,
				'tag' => strtolower($theme->getName()),
				'images' => $theme->getImages(),
				'rel_group_name' => (count($theme->getImages())) ? 'rel="group_'.$theme->getName().'"' : ''
			);

			$existingThemes[] = $theme->getName();

			if ($theme->getName() == $settings->get('DesignSkinName'))
			{
				$selectedTheme = $themeHelper;
			}
			else
			{
				$themeCollection[$theme->getName()] = $themeHelper;
			}
		}

		$result = array();

		$result['themes'] = $themes;
		$result['themeCollection'] = $themeCollection;
		$result['selectedTheme'] = $selectedTheme;
		$result['existingThemes'] = json_encode($existingThemes);
		$result['themesInstallable'] = is_writable($themesFolderPath) ? 'true' : 'false';
		return $result;
	}

	/**
	 *
	 */
	public function installAndActivateAction()
	{
		$request = $this->getRequest();

		$fromCartDesigner = $request->get('chart') == 'true';

		$settings = $this->getSettings()->getAll();
		$skin_id = $request->get('id');

		if (trim($skin_id) == '')
		{
			if ($fromCartDesigner)
			{
				$this->renderJson(array(
					'result' => 'error',
					'message' => 'Cannot find theme by id'
				));
				return;
			}

			Admin_Flash::setMessage('Cannot find theme by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('design_settings', array('mode' => 'list'));
		}
		
		$file_name = $skin_id.'.zip';
		$result = false;
		if (FileUtils::downloadFile('http://www.cartmanual.com/themes/downloads/'.$file_name, 'content/cache/tmp/'.$file_name))
		{
			$path = $settings["GlobalServerPath"];

			if (strpos(strtolower($request->server->get("SERVER_SOFTWARE")), "win32") or strpos(strtolower("test".$request->server->get("SERVER_SOFTWARE")), "microsoft") != false)
			{
				$command_line = $path."/content/admin/7zip/7z.exe x \"".$path."/content/cache/tmp/".$file_name."\" -aoa -o\"".$path."\content\skins\"";
				$result = exec($command_line, $a);
			}
			else
			{
				$command_line = "unzip -o \"".$path."/content/cache/tmp/".$file_name."\" -d\"".$path."/content/skins/\"";
				$result = exec($command_line, $a);
			}
			$result = true;
		}

		if($result)
		{

			$change_mode = $request->get('change_mode', 'default');
			$restoreLayout = true;
			if ($change_mode == 'leave_changes')
			{
				$restoreLayout = false;
			}

		    $response = $this->activateTheme($skin_id, $restoreLayout);

		    if ($response)
		    {
			    view()->assemble('content'.DS.'cache'.DS.'skins'.DS.escapeFileName($settings['DesignSkinName']).DS, array(
				    'content'.DS.'engine'.DS.'design'.DS,
				    'content'.DS.'skins'.DS.escapeFileName($settings['DesignSkinName']).DS,
				    'content'.DS.'skins'.DS.'_custom'.DS.'skin'.DS,
			    ));
		    }

			if ($fromCartDesigner)
			{
				$this->renderJson(array(
					'result' => 'success'
				));
				return;
			}
			$session = $this->getSession();
			$session->set('THEME_INSTALLED', true);

		    Admin_Flash::setMessage('common.success');
		}
		else
		{
			if ($fromCartDesigner)
			{
				$this->renderJson(array(
					'result' => 'error',
					'message' => 'Cannot download theme file',
				));
				return;
			}

		    Admin_Flash::setMessage('Cannot Download Theme File ', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('design_settings');
	}

	public function activateAction()
	{
		$request = $this->getRequest();
		
		$settings = $this->getSettings()->getAll();
		$skin_id = $request->get('id');
		
		if (trim($skin_id) == '')
		{
			Admin_Flash::setMessage('Cannot find theme by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('design_settings', array('mode' => 'list'));
		}

		$change_mode = $request->get('change_mode', 'default');
		$restoreLayout = true;
		if ($change_mode == 'leave_changes')
		{
			$restoreLayout = false;
		}

		$response = $this->activateTheme($skin_id, $restoreLayout);

		if ($response)
		{
			view()->assemble('content'.DS.'cache'.DS.'skins'.DS.escapeFileName($settings['DesignSkinName']).DS, array(
				'content'.DS.'engine'.DS.'design'.DS,
				'content'.DS.'skins'.DS.escapeFileName($settings['DesignSkinName']).DS,
				'content'.DS.'skins'.DS.'_custom'.DS.'skin'.DS,
			));

			$session = $this->getSession();
			$session->set('THEME_INSTALLED', true);

			Admin_Flash::setMessage('common.success');
		}
		else
		{
			Admin_Flash::setMessage('Cannot activate this theme!', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('design_settings');
	}

	/**
	 * Sets the site theme
	 *
	 * @param string $themeName
	 * @param boolean $restoreLayout Whether the current site theme layout should be restored
	 * @return boolean TRUE if theme successfully set, FALSE otherwise
	 */
	protected function activateTheme($themeName, $restoreLayout = false)
	{
		// Make sure the skin exists
		if (is_dir('content/skins/'.escapeFileName($themeName)))
		{
			$settings = $this->getSettings();
			// read in the skin xml file
			$template_xml = simplexml_load_string(file_get_contents('content/skins/'.escapeFileName($themeName).'/skin.xml'), 'SimpleXMLElement', LIBXML_NOCDATA);
			$this->getSettings()->persist(array('DesignSkinName'=> $themeName));
			$this->settings = $this->getSettings()->getAll();

			// Update the current selected skin in settings

			// Set the Site Layout Width to the default as defined in skin xml
			$layoutSiteWidth = $template_xml->xpath("/skin/settings/option[@name='LayoutSiteWidth']");
			$layoutSiteWidthValue = (count($layoutSiteWidth) && $layoutSiteWidth !== false)
				? $layoutSiteWidth[0]->attributes()->value
				: $settings->get('LayoutSiteWidthDefault');

			$this->getSettings()->persist(array('LayoutSiteWidth'=> $layoutSiteWidthValue));
			$this->settings = $this->getSettings()->getAll();

			// check if we need to override the current layout settings with theme defaults,
			// otherwise, do nothing
			if ((bool) $restoreLayout)
			{
				$designMode = new Ddm_DesignMode($settings->getAll());
				$designMode->restoreLayout();
				$designMode->restoreSkin();
			}

			return true;
		}

		return false;
	}

	public function cartDesignerAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings()->getAll();
		$design_flag = $request->get('design_mode_flag');

		if (trim($design_flag) == '')
		{
			Admin_Flash::setMessage('Cannot Open Cart Designer', Admin_Flash::TYPE_ERROR);
			$this->redirect('design_settings', array('mode' => 'list'));
		}

		$restoreLayout = $request->get('layoutConfig');
		$session = $this->getSession();
		switch (strtolower($design_flag))
		{
			case 'on':
				$session->set('admin_design_mode',true);
				session_write_close();
				header('Location: '.$settings['AdminHttpsUrl']."/".$settings["INDEX"]."?_admc=true&pcsid=".session_id());
				die();
				break;

			case 'Off':
			default:
				// unset admin session
				$session->set('admin_design_mode',false);
				setcookie(
					'_admc',
					'true',
					time() - 60*60*24*365,
					'/',
					isSslMode() ? $settings['GlobalHttpsCookieDomain'] : $settings['GlobalHttpCookieDomain']
				);
				$this->redirect('design_settings', array('mode' => 'list'));
		}
	}

	public function viewSiteAction()
	{
		$url = $this->getSettings()->get('GlobalHttpUrl');
		header('Location: '.$url);
	}

}
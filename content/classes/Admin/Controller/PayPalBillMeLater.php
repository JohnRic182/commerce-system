<?php
/**
 * Class Admin_Controller_PayPalBillMeLater
 */
class Admin_Controller_PayPalBillMeLater extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$request = $this->getRequest();
		$show_popup = $request->get('show_popup', 'no');

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9025');

		$paypalEnabled = Payment_PayPal_PayPalApi::isPayPalEnabled();
		$adminView->assign('paypalEnabled', $paypalEnabled);
		$adminView->assign('show_popup', $show_popup);

		$adminView->assign('body', 'templates/pages/paypal-bml/index.html');
		$adminView->render('layouts/default');
	}

	public function publisherFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('paypal_publisher_id', 'paypal_publisher_email');

		$email = $request->request->get('paypalEmail');

		$financingApi = new Payment_PayPal_FinancingApi(defined('DEV_PAYPALFINANCING') && DEV_PAYPALFINANCING ? false : true);
		try
		{
			$publisherId = $financingApi->getPublisherId($email, '', '');

			$settingsData = array(
				'paypal_publisher_id' => $publisherId,
				'paypal_publisher_email' => $email,
			);

			$settings->persist($settingsData);

			$this->renderJson(array(
				'status' => 1,
			));
		}
		catch (Payment_PayPal_FinancingApiException $ex)
		{
			$message = '';
			foreach ($ex->errors as $error)
			{
				$message .= $error.'<br>';
			}
			$this->renderJson(array(
				'status' => 0,
				'message' => $message,
			));
		}
		catch (Exception $ex)
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => $ex->getMessage(),
			));
		}
	}

	public function activateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('paypalec_Offer_PayPal_BML' => 'Yes'));
	}

	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('paypalec_Offer_PayPal_BML' => 'No'));
	}
}
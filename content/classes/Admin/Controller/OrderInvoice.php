<?php

/**
 * Class Admin_Controller_OrderInvoice
 */
class Admin_Controller_OrderInvoice extends Admin_Controller
{
	/** @var DataAccess_OrderShipmentRepository */
	protected $shipmentRepository = null;

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		$id = intval($request->query->get('id'));

		$orderData = $this->getOrderData($id);

		if (!$orderData)
		{
			Admin_Flash::setMessage('orders_no_order_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$this->renderInvoice($orderData);
	}

	/**
	 * Bulk index action
	 */
	public function bulkIndexAction()
	{
		$request = $this->getRequest();

		$ids = $request->query->get('ids');

		$ids = explode(',', $ids);

		$invoices = '';

		for ($i = 0; $i < count($ids); $i++)
		{
			$id = intval($ids[$i]);
			if ($id < 1) continue;

			$orderData = $this->getOrderData($id);

			if ($orderData)
			{
				$invoices .= $this->renderInvoice($orderData, true, $i + 1 < count($ids));
			}
		}
		$adminView = $this->getView();

		$adminView->assign('invoices', $invoices);
		$adminView->assign('body', 'templates/pages/order/invoice-bulk.html');
		$adminView->render('layouts/print');

	}

	/**
	 * Render invoice
	 *
	 * @param $orderData
	 * @param bool $multiPage
	 *
	 * @param bool $pageBreak
	 */
	protected function renderInvoice($orderData, $multiPage = false, $pageBreak = false)
	{
		$db = $this->getDb();
		$settings = $this->getSettings();
		$settingsArray = $settings->getAll();

		global $msg;
		require($settings->get('GlobalServerPath').'/content/languages/'.escapeFileName($settings->get('ActiveLanguage')).'.php');

		$order = $this->getOrderObject($orderData);

		$settingsValues = $settings->getAll();
		$orderViewModel = new View_OrderLinesViewModel($settingsValues, $msg);
		$orderViewModel->build($order, false, true);

		$orderView = $orderViewModel->asArray();

		$isBongoOrder = $orderData['payment_gateway_id'] == 'bongocheckout';

		$userData = false;
		$cf_billing = false;
		$cf_shipping = false;
		$cf_invoice = false;

		$db->query("
			SELECT ".DB_PREFIX."users.*,
				".DB_PREFIX."countries.name AS country_name
			FROM ".DB_PREFIX."users
			INNER JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users.country
			WHERE
				".DB_PREFIX."users.uid='".$orderData["uid"]."' AND ".DB_PREFIX."users.removed='No'
		");

		if ($db->moveNext())
		{
			$userData = $db->col;

			$customFields = new CustomFields($db);

			$cf_billing = $customFields->getCustomFieldsValues("billing", $userData["uid"], 0, 0);
			$cf_shipping = $customFields->getCustomFieldsValues("shipping", $userData["uid"], $orderData['oid'], 0);
			$cf_invoice = $customFields->getCustomFieldsValues("invoice", $userData["uid"], $orderData['oid'], 0);
		}

		$logo_image = $this->getLogoImage();

		$adminView = $this->getView();

		$shipmentsView = new View_OrderShipmentsViewModel($settingsArray, $msg);
		$adminView->assign('shipments', $shipmentsView->getShipmentsView($this->getShipmentRepository()->getShipmentsByOrderId($orderData['oid'])));

		$adminView->assign('logo_image', $logo_image);

		$adminView->assign('order', $orderData);
		$adminView->assign('user', $userData);
		$adminView->assign('cf_billing', $cf_billing);
		$adminView->assign('cf_shipping', $cf_shipping);
		$adminView->assign('cf_invoice', $cf_invoice);

		$adminView->assign('orderView', $orderView);
		$adminView->assign('isBongoOrder', $isBongoOrder);

		$bongoOrderData = false;

		if (trim($orderData["custom2"]) != '')
		{
			$bongoOrderData = @unserialize($orderData["custom2"]);
		}

		$adminView->assign('bongoOrderData', $bongoOrderData);

		$recurringBillingRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $this->getSettings());
		$lineItems = array();
		$orderLineItems = $order->lineItems;

		/** @var Model_LineItem $orderLineItem */
		foreach ($orderLineItems as $orderLineItem)
		{
			$recurringProfile = null;
			if ($orderLineItem->getEnableRecurringBilling())
			{
				$recurringProfile = $recurringBillingRepository->getByLineItemId($orderLineItem->getId());
			}

			$lineView = new Admin_View_OrderLineItemViewModel($orderLineItem, $recurringProfile, $msg);
			$lineItems[] = $lineView;
		}

		$adminView->assign('lineItems', $lineItems);

		$adminView->assign('page_break', $pageBreak);

		$payment_gateway = false;
		$completed_at = false;

		if ($orderData["payment_is_realtime"]=="Yes")
		{
			$row = $db->selectOne("SELECT *, DATE_FORMAT(completed, '".$settings->get('LocalizationDateTimeFormat')."') AS fcompleted FROM ".DB_PREFIX."payment_transactions WHERE oid=".$orderData['oid']." AND is_success = 1 ORDER BY completed");
			if($row)
			{
				$payment_gateway = $row['payment_gateway'];
				$completed_at = $row['fcompleted'];
			}
		}

		$adminView->assign('payment_gateway', $payment_gateway);
		$adminView->assign('completed_at', $completed_at);

		if ($multiPage)
		{
			return $adminView->fetch('pages/order/invoice');
		}
		else
		{
			$adminView->assign('body', 'templates/pages/order/invoice.html');
			$adminView->render('layouts/print');
		}
	}

	/**
	 * @var null
	 */
	protected $logo_image = null;

	/**
	 * @return bool|null
	 */
	protected function getLogoImage()
	{
		if ($this->logo_image === null)
		{
			$row = $this->getDb()->selectOne("SELECT * FROM ".DB_PREFIX."design_elements WHERE group_id='image' and element_id = 'image-logo'");
			if ($row)
			{
				$this->logo_image = $row['content'];
			}
			else
			{
				$this->logo_image = false;
			}
		}

		return $this->logo_image;
	}

	/**
	 * @param $id
	 * @return array|bool
	 */
	protected function getOrderData($id)
	{
		$db = $this->getDb();
		$settings = $this->getSettings();

		return $db->selectOne('SELECT o.*,
				DATE_FORMAT(o.placed_date, "'.$settings->get('LocalizationDateTimeFormat').'") AS cd,
				DATE_FORMAT(o.status_date, "'.$settings->get('LocalizationDateTimeFormat').'") AS sd,
				IF(DATE_ADD(o.placed_date, INTERVAL 3 DAY) < NOW(), "3", "1") AS auth_exp,
				c.name AS country_name
			FROM '.DB_PREFIX.'orders o
			LEFT JOIN '.DB_PREFIX.'countries c ON o.shipping_country = c.coid
			WHERE
				o.oid='.intval($id).' AND (o.removed="No")'
		);
	}

	/**
	 * @param $orderData
	 * @return ORDER
	 */
	protected function getOrderObject($orderData)
	{
		global $settings;
		$db = $this->getDb();

		$oid = intval($orderData['oid']);

		$user = USER::getById($db, $settings, $orderData['uid']);
		$order = ORDER::getById($db, $settings, $user, $oid);

		$user->getUserData();
		$order->getOrderData();
		$order->getOrderItems(true);

		// have to get VAT status
		$taxProvider = Tax_ProviderFactory::getTaxProvider();
		$taxProvider->initTax($order);
		$settings['DisplayPricesWithTax'] = $taxProvider->getDisplayPricesWithTax() ? 'YES' : 'NO';

		return $order;
	}

	/**
	 * @return DataAccess_OrderShipmentRepository
	 */
	protected function getShipmentRepository()
	{
		if ($this->shipmentRepository == null)
		{
			$this->shipmentRepository = new DataAccess_OrderShipmentRepository($this->getDb(), $this->getSettings());
		}

		return $this->shipmentRepository;
	}
}
<?php

/**
 * Class Admin_Controller_QuickStartGuide
 */
class Admin_Controller_QuickStartGuide extends Admin_Controller
{
	protected $taxRepository = null;
	protected $countryRepository = null;
	protected $shippingRepository = null;
	protected $paymentMethodRepository = null;
	protected $quickStartGuideRepository = null;
	protected $searchEngineOptimizationRepository = null;

	protected $extensions = array('jpg', 'jpeg', 'png', 'gif');

	protected $menuItem = array(
		'primary' => 'quick_start_guide',
		'secondary' => ''
	);

	/**
	 * Index of QSG
	 */
	public function indexAction()
	{
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3006');

		$countryRepository = $this->getCountryRepository();
		$countriesStates = $countryRepository->getCountriesStatesForSettings(true);

		$adminView->assign('nonce', Nonce::create('quick_start_guide'));

		// Step One
		$this->stepOneForm($adminView);

		// Step Two
		$this->stepTwoForm($adminView);

		//Step Three
		$this->stepThreeForm($adminView);

		// Step Four
		$this->stepFourForm($adminView);

		Admin_Log::log('Quick Start Guide has been opened', Admin_Log::LEVEL_IMPORTANT);

		$adminView->assign('companyName', $quickStartGuideRepository->get('CompanyName'));
		$adminView->assign('countriesStates', json_encode($countriesStates));

		$adminView->render('pages/quick-start-guide/index');
	}

	/**
	 * Initialize the stepOneForm
	 * @param $adminView
	 */
	protected function stepOneForm($adminView)
	{
		$db = $this->getDb();
		$settings = $this->getSettings();
		$currencyData = $this->getCurrency();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$quickStartGuideForm = new Admin_Form_QuickStartGuideForm();
		$adminView->assign(
			'generalInformation',
			array('form' => $quickStartGuideForm->getGeneralInformationForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->stepOneFormDataKeys)))
		);

		$adminView->assign(
			'contactlnformation',
			array('form' => $quickStartGuideForm->getContactlnformationForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->stepOneFormDataKeys)))
		);

		$industrySpecifics = array_merge($quickStartGuideRepository->getByKeys($quickStartGuideRepository->stepOneFormDataKeys), $currencyData);
		$adminView->assign(
			'industrySpecifics',
			array('form' => $quickStartGuideForm->getIndustrySpecificsForm($industrySpecifics))
		);

		$adminView->assign(
			'logo',
			array('form' => $quickStartGuideForm->getLogoForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->stepOneFormDataKeys)))
		);

		$adminView->assign('imageLogo', $this->getImageLogo());

		$adminRepository = new DataAccess_AdminRepository($db, $settings);
		$adminId = $this->getCurrentAdminId();
		$adminData = $adminRepository->getAdminById($adminId);
		$adminFirstName =  $adminData['fname'];

		$adminView->assign('adminFirstName', $adminFirstName);
	}

	/**
	 * Initialize the stepTwoForm
	 * @param $adminView
	 */
	protected function stepTwoForm($adminView)
	{
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$quickStartGuideForm = new Admin_Form_QuickStartGuideForm();

		$adminView->assign(
			'socialMediaAccounts',
			array(
				'form' => $quickStartGuideForm->getSocialMediaAccountsForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->stepTwoFormDataKeys)),
			)
		);

		$adminView->assign(
			'sharingSettings',
			array(
				'form' => $quickStartGuideForm->getSharingSettingsForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->stepTwoFormDataKeys)),
			)
		);
	}

	/**
	 * Initialize the stepThreeForm
	 * @param $adminView
	 */
	protected function stepThreeForm($adminView)
	{
		$db = $this->getDb();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$searchEngineOptimizationRepository = $this->getSearchEngineOptimizationRepository();
		$stepThreeFormData = $searchEngineOptimizationRepository->getAdvancedSettingsData();
		$stepThreeFormData['robots'] = $searchEngineOptimizationRepository->getRobots();
		$googleToolsRepository = new DataAccess_GoogleToolsRepository($db, $quickStartGuideRepository);

		if ($googleToolsRepository->generateSiteMap($quickStartGuideRepository->getAll(), false))
		{
			$stepThreeFormData['sitemap_url'] = $quickStartGuideRepository->get('GlobalHttpUrl') . '/sitemap.xml.gz';
		}

		$quickStartGuideForm = new Admin_Form_QuickStartGuideForm();

		$adminView->assign('siteInformation',
			array(
				'form' => $quickStartGuideForm->getSiteInformationForm($stepThreeFormData),
			)
		);

		$adminView->assign(
			'seoSettings',
			array(
				'form' => $quickStartGuideForm->getSeoSettingsForm($stepThreeFormData),
			)
		);
	}

	/**
	 * Initialize the stepFourForm
	 * @param $adminView
	 */
	protected function stepFourForm($adminView)
	{
		$taxRepository = $this->getTaxRepository();
		$countryRepository = $this->getCountryRepository();
		$paymentMethodRepository = $this->getPaymentMethodRepository();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$originAddressData = $quickStartGuideRepository->getByKeys($quickStartGuideRepository->originAddressDataKeys);

		$countryId = intval($originAddressData['ShippingOriginCountry']);
		$originCountry = $countryRepository->getCountryById($countryId);

		$oldestTaxRate = $taxRepository->getRatesList(0, 1, 'rate_id');
		if (!empty($oldestTaxRate))
		{
			$oldestTaxRate = $oldestTaxRate['0'];
			$oldestTaxRateData = $taxRepository->getRateDataById($oldestTaxRate['rate_id']);
			$taxes_zones = $taxRepository->getTaxZonesList();
			$taxes_classes = $taxRepository->getTaxClassesList();
			$oldestTaxRateData['taxes_zones'] = $taxes_zones;
			$oldestTaxRateData['taxes_classes'] = $taxes_classes;
		}
		else
		{
			$oldestTaxRateData = array();
			$oldestTaxRateData['rate_description'] = '';
			$oldestTaxRateData['class_id'] = '';
			$oldestTaxRateData['coid'] = '';
			$oldestTaxRateData['stid'] = '';
			$oldestTaxRateData['tax_rate'] = '';
			$oldestTaxRateData['taxes_zones'] = '';
			$oldestTaxRateData['taxes_classes'] = '';
		}
		
		$quickStartGuideForm = new Admin_Form_QuickStartGuideForm();

		$adminView->assign('originCountry', $originCountry);
		$adminView->assign('currencyData', $this->getCurrency());

		$this->realTimeMethodsFormAction();

		$adminView->assign('paymentMethodsGroups', $paymentMethodRepository->getPaymentMethodsGroups());

		$adminView->assign(
			'customPayment',
			array(
				'form' => $quickStartGuideForm->getCustomPaymentForm(),
			)
		);

		$adminView->assign(
			'taxes',
			array(
				'form' => $quickStartGuideForm->getTaxesForm($oldestTaxRateData),
			)
		);
	}

	/**
	 * Initialize the realTimeMethodsForm
	 */
	public function realTimeMethodsFormAction()
	{
		$adminView = $this->getView();
		$request = $this->getRequest();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$originCountry = $quickStartGuideRepository->get('ShippingOriginCountry');

		$coid = intval($request->get('coid'));
		$carrierId = $request->get('carrier_id');

		$shippingRepository = $this->getShippingRepository();

		if (trim($carrierId) == '')
		{
			$fedexMethods = $shippingRepository->getRealTimeMethods('fedex', $coid, $originCountry);
			$upsMethods = $shippingRepository->getRealTimeMethods('ups', $coid, $originCountry);
			$uspsMethods = $shippingRepository->getRealTimeMethods('usps', $coid, $originCountry);
			$canadaPostMethods = $shippingRepository->getRealTimeMethods('canada_post', $coid, $originCountry);
			$vipParcelsPostMethods = $shippingRepository->getRealTimeMethods('vparcel', $coid, $originCountry);
		}
		else
		{
			$fedexMethods = $carrierId == 'fedex' ? $shippingRepository->getRealTimeMethods('fedex', $coid, $originCountry) : array();
			$upsMethods = $carrierId == 'ups' ? $shippingRepository->getRealTimeMethods('ups', $coid, $originCountry) : array();
			$uspsMethods = $carrierId == 'usps' ? $shippingRepository->getRealTimeMethods('usps', $coid, $originCountry) : array();
			$canadaPostMethods = $carrierId == 'canada_post' ? $shippingRepository->getRealTimeMethods('canada_post', $coid, $originCountry) : array();
			$vipParcelsPostMethods = $carrierId == 'vparcel' ? $shippingRepository->getRealTimeMethods('vparcel', $coid, $originCountry) : array();
		}

		$international = $shippingRepository->getRealTimeMethods($carrierId, 0, $originCountry);
		$adminView->assign('basedMethods',
			array(
				'local' => $coid,
				'international' => !empty($international) ? 0 : null,
			)
		);

		$adminView->assign('fedexBasedMethods', $coid);
		$adminView->assign('uspsBasedMethods', $coid);
		$adminView->assign('canadaPostBasedMethods', $coid);
		$adminView->assign('vipParcelsPostBasedMethods', $coid);

		$adminView->assign('showAll', trim($carrierId) == '');
		$adminView->assign('fedexMethods', $fedexMethods);
		$adminView->assign('upsMethods', $upsMethods);
		$adminView->assign('uspsMethods', $uspsMethods);
		$adminView->assign('canadaPostMethods', $canadaPostMethods);
		$adminView->assign('vparcelMethods', $vipParcelsPostMethods);

		$methods = array_merge($fedexMethods, $upsMethods, $uspsMethods, $canadaPostMethods, $vipParcelsPostMethods);
		$hasRealTime = count($methods) < 1 ? 0 : 1;
		$adminView->assign('hasRealTime', $hasRealTime);

		$quickStartGuideForm = new Admin_Form_QuickStartGuideForm();

		$adminView->assign(
			'upsSettings',
			array(
				'description' => $quickStartGuideForm->getUpsDescriptionForm(),
				'form' => $quickStartGuideForm->getUpsSettingsForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->upsSettingsKeys))
			)
		);

		$adminView->assign(
			'uspsSettings',
			array(
				'description' => $quickStartGuideForm->getUspsDescriptionForm(),
				'form' => $quickStartGuideForm->getUspsSettingsForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->uspsSettingsKeys))
			)
		);

		$adminView->assign(
			'fedExSettings',
			array(
				'description' => $quickStartGuideForm->getFedExDescriptionForm(),
				'form' => $quickStartGuideForm->getFedExSettingsForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->fedExSettingsKeys))
			)
		);

		$adminView->assign(
			'canadaPostSettings',
			array(
				'description' => $quickStartGuideForm->getCanadaPostDescriptionForm(),
				'form' => $quickStartGuideForm->getCanadaPostSettingsForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->canadaPostSettingsKeys))
			)
		);

		$adminView->assign(
			'vipParcelSettings',
			array(
				'description' => $quickStartGuideForm->getVipParcelDescriptionForm(),
				'form' => $quickStartGuideForm->getVipParcelSettingsForm($quickStartGuideRepository->getByKeys($quickStartGuideRepository->vipParcelSettingsKeys))
			)
		);

		if ($request->get('__ajax', false))
		{
			$this->renderJson(array(
				'status' => 1,
				'message' => trans('common.success'),
				'hasRealTime' => $hasRealTime,
				'html' => $adminView->fetch('pages/quick-start-guide/content/step-four/form-real-time-methods'),
			));
		}
	}

	/**
	 * Save or Add Flat Rate method
	 */
	public function addFlatRateMethodAction()
	{
		$request = $this->getRequest();
		$shippingRepository = $this->getShippingRepository();

		$methodData = $request->request->getArray('method');
		$customRate = $this->getCustomRateDefaults();

		$customRate = array_merge($customRate, $methodData);
		$customRate['rates'] = array();

		$customRate['method_name'] = $this->setMethodName($customRate['method_id']);

		$rates = $request->request->getArray('rates');
		$customRate['rates'] = $rates;

		//TODO: Validation
		$isValid = true;
		if ($isValid)
		{
			$shippingRepository->persistShippingMethod($customRate);
			$this->ensureInternationalShippingEnabled($customRate);

			Admin_Log::log('QSG Step 4 : Shipping settings has been updated', Admin_Log::LEVEL_NORMAL);

			$this->renderJson(array('status' => 1));
		}
		else
		{
			//TODO:
			$this->renderJson(array('status' => 0));
		}
	}

	/**
	 * Save or Add Real Time methods
	 */
	public function addRealTimeMethodsAction()
	{
		$request = $this->getRequest();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$shippingRepository = $this->getShippingRepository();

		$methodData = $request->request->getArray('method');
		$based = $request->request->getArray('based');

		$realTimeMethods = array();
		foreach ($based as $key => $base)
		{
			$temp = $shippingRepository->getRealTimeMethods($methodData['carrier_id'], $base);

			foreach ($temp as $value)
			{
				$realTimeMethods[] = $value;
			}
		}

		$methods = $request->request->getArray('methods');
		foreach ($realTimeMethods as $realTimeMethod)
		{
			$methods[] = $realTimeMethod['sdid'];
		}

		foreach ($methods as $realTimeMethod)
		{
			$method = $shippingRepository->getRealTimeMethod($realTimeMethod);
			unset($method['sdid']);
			$method['ssid'] = 0;
			$method['priority'] = 5;
			$method['fee'] = 0;
			$method['fee_type'] = 'amount';
			$method['exclude'] = 'No';
			$method['hidden'] = 'No';
			$method['notes'] = null;
			$method['country'] = $methodData['country'];

			$shippingRepository->persistRealTimeMethod($method);
		}

		$forceAddressValidation = $this->getForceAddressValidation();

		$settingsDefaults = array(
			'settings' => array(
				'ShippingCalcEnabled' => 'YES',
				'ShippingHandlingFee' => '0.00', 'ShippingHandlingFeeType' => 'percent', 'ShippingHandlingFeeWhenFreeShipping' => '0.00',
				'ShippingHandlingSeparated' => '0', 'ShippingHandlingTaxable' => '0', 'ShippingHandlingTaxClassId' => '0',
				'ShippingShowPriceAtProductLevel' => 'NO', // removed
				'ShippingWithoutMethod' => 'NO', 'ShippingShowWeight' => 'NO', 'ShippingAllowGetQuote' => 'NO',
				'ShippingShowAlternativeOnFree' => 'NO', 'ShippingAllowSeparateAddress' => '0',
				'EndiciaUspsAddressValidation' => $forceAddressValidation ? '1' : '0',
				'ShippingTaxable' => '0', 'ShippingTaxClassId' => '0'
			),
			'address' => array(
				'ShippingOriginName' => '', 'ShippingOriginAddressLine1' => '', 'ShippingOriginAddressLine2' => '',
				'ShippingOriginCity' => '', 'ShippingOriginCountry' => '1', 'ShippingOriginState' => '',
				'ShippingOriginProvince' => '', 'ShippingOriginZip' => '', 'ShippingOriginPhone' => ''
			),
		);

		$accepted = array();
		foreach ($settingsDefaults as $settingsGroupKey => $settingsGroup)
		{
			$accepted[$settingsGroupKey] = array_keys($settingsGroup);
		}

		if ($forceAddressValidation)
		{
			$quickStartGuideRepository->set('EndiciaUspsAddressValidation', '1');
			$settingsDefaults['settings']['EndiciaUspsAddressValidation'] = '1';
		}

		$data = array();

		foreach ($accepted as $settingsGroupKey => $settingsGroup)
		{
			$data[$settingsGroupKey] = $quickStartGuideRepository->getByKeys($settingsGroup);
		}

		$carrierId = $methodData['carrier_id'];
		if (trim($carrierId) != '')
		{
			$settingsKeys = array();
			switch ($carrierId)
			{
				case 'ups' :
				{
					$settingsKeys = $quickStartGuideRepository->upsSettingsKeys;
					break;
				}
				case 'usps' :
				{
					$settingsKeys = $quickStartGuideRepository->uspsSettingsKeys;
					break;
				}
				case 'fedex' :
				{
					$settingsKeys = $quickStartGuideRepository->fedExSettingsKeys;
					break;
				}
				case 'canada_post' :
				{
					$settingsKeys = $quickStartGuideRepository->canadaPostSettingsKeys;
					break;
				}
				case 'vparcel' :
				{
					$settingsKeys = $quickStartGuideRepository->vipParcelSettingsKeys;
					break;
				}
			}

			// TODO: Validation

			$settingsData = $request->request->getByKeys($settingsKeys);
			$quickStartGuideRepository->persist($settingsData, $settingsKeys);

			$this->renderJson(array('status' => 1));
		}
	}

	/**
	 * Save QSG Step One data
	 */
	public function saveStepOneAction()
	{
		$db = $this->getDb();
		$request = $this->getRequest();
		$settings = $this->getSettings();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		if ($request->isPost())
		{
			$stepOneData = $request->getRequest()->all();
			parse_str($stepOneData['data'], $parseData);
			$formData['generalInformation'] = array(
				'companyName' => $parseData['qsg_CompanyName'],
				'companyAddressLine1' => $parseData['qsg_CompanyAddressLine1'],
				'companyAddressLine2' => $parseData['qsg_CompanyAddressLine2'],
				'companyCity' => $parseData['qsg_CompanyCity'],
				'companyCountry' => $parseData['qsg_CompanyCountry'],
				'companyState' => $parseData['qsg_CompanyState'],
				'companyZip' => $parseData['qsg_CompanyZip'],
			);

			$formData['contactInformation'] = array(
				'companyPhone' => $parseData['qsg_CompanyPhone'],
				'companyFax' => $parseData['qsg_CompanyFax'],
				'companyEmail' => $parseData['qsg_CompanyEmail'],
			);

			$formData['industrySpecifics'] = array(
				'companyIndustry' => $parseData['qsg_CompanyIndustry'],
				'companyRevenue' => $parseData['qsg_CompanyRevenue'],
				'companyEmployees' => $parseData['qsg_CompanyEmployees'],
			);

			$generalInformationValidationErrors = $quickStartGuideRepository->getGeneralInformationValidationErrors($formData['generalInformation']);
			$contactInformationValidationErrors = $quickStartGuideRepository->getContactInformationValidationErrors($formData['contactInformation']);
			$industrySpecificsValidationErrors = $quickStartGuideRepository->getIndustrySpecificsValidationErrors($formData['industrySpecifics']);
			$addressValidationErrors = $quickStartGuideRepository->validateAddress($formData['generalInformation'], $db);

			if ($generalInformationValidationErrors == false && $contactInformationValidationErrors == false && $industrySpecificsValidationErrors == false && $addressValidationErrors == false)
			{
				/** Save Data */
				$formData['generalInformation']['companyState'] = isset($formData['generalInformation']['companyState']) ? $formData['generalInformation']['companyState'] : $formData['generalInformation']['companyProvince'];
				$shippingOriginData = $this->formatShippingOriginData(array_merge($formData['generalInformation'], $formData['contactInformation']));
				$quickStartGuideRepository->persist($formData['generalInformation']);
				$quickStartGuideRepository->persist($formData['contactInformation']);
				$quickStartGuideRepository->persist($formData['industrySpecifics']);
				$quickStartGuideRepository->persist($shippingOriginData);

				$adminRepository = new DataAccess_AdminRepository($this->getDb(), $settings);
				$adminId = $this->getCurrentAdminId();
				$adminData = $adminRepository->getAdminById($adminId);
				$adminFirstName =  $adminData['fname'];

				$content  = '<h3>Thanks for the insight into your company, ' . $adminFirstName . '</h3>';
				$content .= '<p>Next, let\'s grab your social media accounts.</p>';

				Admin_Log::log('QSG Step 1 : Settings has been updated', Admin_Log::LEVEL_NORMAL);

				$this->renderJson(
					array(
						'status' => 1,
						'content' => $content,
					)
				);
			}
			else
			{

				$tempValidationErrors = array(
					$generalInformationValidationErrors,
					$contactInformationValidationErrors,
					$industrySpecificsValidationErrors,
					$addressValidationErrors
				);

				$validationErrors = array();
				foreach ($tempValidationErrors as $tempValidationError)
				{
					if (is_array($tempValidationError))
					{
						$validationErrors = array_merge($validationErrors, $tempValidationError);
					}
				}

				$this->renderJson(
					array(
						'status' => 0,
						'errors' => $validationErrors,
					)
				);
			}
		}
	}

	/**
	 * Get format of Shipping Origin
	 * @param $generalInformation
	 * @return array
	 */
	public function formatShippingOriginData($generalInformation)
	{
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();
		$settingsData = $quickStartGuideRepository->getAll();

		return array(
			'ShippingOriginAddressLine1' => strlen(trim($settingsData['ShippingOriginAddressLine1'])) ? $settingsData['ShippingOriginAddressLine1'] : $generalInformation['companyAddressLine1'],
			'ShippingOriginAddressLine2' => strlen(trim($settingsData['ShippingOriginAddressLine2'])) ? $settingsData['ShippingOriginAddressLine2'] : $generalInformation['companyAddressLine2'],
			'ShippingOriginCountry' => strlen(trim($settingsData['ShippingOriginCountry'])) ? $settingsData['ShippingOriginCountry'] : $generalInformation['companyCountry'],
			'ShippingOriginState' => strlen(trim($settingsData['ShippingOriginState'])) ? $settingsData['ShippingOriginState'] : isset($generalInformation['companyState']) ? $generalInformation['companyState'] : $settingsData['ShippingOriginState'],
			'ShippingOriginProvince' => strlen(trim($settingsData['ShippingOriginProvince'])) ? $settingsData['ShippingOriginProvince'] : isset($generalInformation['companyProvince']) ? $generalInformation['companyProvince'] : $settingsData['ShippingOriginProvince'],
			'ShippingOriginZip' => strlen(trim($settingsData['ShippingOriginZip'])) ? $settingsData['ShippingOriginZip'] : $generalInformation['companyZip'],
			'ShippingOriginPhone' => strlen(trim($settingsData['ShippingOriginPhone'])) ? $settingsData['ShippingOriginPhone'] : isset($generalInformation['companyPhone']) ? $generalInformation['companyPhone'] : $settingsData['ShippingOriginPhone']
		);
	}

	/**
	 * Save QSG Step Two data
	 */
	public function saveStepTwoAction()
	{
		$request = $this->getRequest();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		if ($request->isPost())
		{
			$stepTwoData = $request->getRequest()->all();
			parse_str($stepTwoData['data'], $parseData);
			$formData['socialMediaAccounts'] = array(
				'facebookPageAccount' => $parseData['qsg_facebookPageAccount'],
				'twitterAccount' => $parseData['qsg_twitterAccount'],
				'pinterestUsername' => $parseData['qsg_pinterestUsername'],
				'instagramAccount' => $parseData['qsg_instagramAccount'],
				'youtubePageURL' => $parseData['qsg_youtubePageURL'],
				'snapChatAccount' => $parseData['qsg_snapChatAccount'],
				'googlePlusAccount' => $parseData['qsg_googlePlusAccount'],
				'bloggerAccount' => $parseData['qsg_bloggerAccount'],
				'facebookPageAccount' => $parseData['qsg_facebookPageAccount'],
			);

			$formData['sharingSettings'] = array(
				'facebookLikeButtonProduct' => isset($parseData['qsg_facebookLikeButtonProduct']) ? $parseData['qsg_facebookLikeButtonProduct'] : '',
				'twitterTweetButtonProduct' => isset($parseData['qsg_twitterTweetButtonProduct']) ? $parseData['qsg_twitterTweetButtonProduct'] : '',
				'googlePlusButtonProduct' => isset($parseData['qsg_googlePlusButtonProduct']) ? $parseData['qsg_googlePlusButtonProduct'] : '',
				'pinItButtonProduct' => isset($parseData['qsg_pinItButtonProduct']) ? $parseData['qsg_pinItButtonProduct'] : '',
			);

			$socialMediaAccountsValidationErrors = $quickStartGuideRepository->getSocialMediaAccountsValidationErrors($formData['socialMediaAccounts']);
			$validationErrors = $socialMediaAccountsValidationErrors; // Use array_merge if more than 1 validation will occur

			if ($validationErrors == false)
			{
				$quickStartGuideRepository->persist($formData['socialMediaAccounts']);
				$quickStartGuideRepository->persist($formData['sharingSettings']);

				$content  = '<h3>Thanks for sharing your social media accounts!</h3>';
				$content .= '<p>Next, let\'s get some SEO set-up tackled.</p>';

				Admin_Log::log('QSG Step 2 : Social Media settings has been updated', Admin_Log::LEVEL_NORMAL);

				$this->renderJson(
					array(
						'status' => 1,
						'content' => $content,
					)
				);
			}
			else
			{
				$this->renderJson(
					array(
						'status' => 0,
						'errors' => $validationErrors,
					)
				);
			}
		}
	}

	/**
	 * Save QSG Step Three data
	 */
	public function saveStepThreeAction()
	{
		$request = $this->getRequest();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();
		$searchEngineOptimizationRepository = $this->getSearchEngineOptimizationRepository();

		if ($request->isPost())
		{
			$data = $searchEngineOptimizationRepository->getAdvancedSettingsData();
			$robots = $searchEngineOptimizationRepository->getRobots();
			if (isset($robots))
			{
				$data['robots'] = $robots;
			}

			$stepThreeData = $request->getRequest()->all();
			parse_str($stepThreeData['data'], $parseData);
			$formData['siteInformation']['searchSettings'] = array(
				'SearchTitle' => $parseData['qsg_searchSettings']['SearchTitle'],
				'SearchMetaDescription' => $parseData['qsg_searchSettings']['SearchMetaDescription'],
			);

			$formData['seoSettings']['searchSettings'] = array(
				'USE_MOD_REWRITE' => isset($parseData['qsg_searchSettings']['USE_MOD_REWRITE']) ? $parseData['qsg_searchSettings']['USE_MOD_REWRITE'] : '',
				'SearchAutoGenerateLowercase' => isset($parseData['qsg_searchSettings']['SearchAutoGenerateLowercase']) ? $parseData['qsg_searchSettings']['SearchAutoGenerateLowercase'] : '',
				'SearchAutoGenerate' => isset($parseData['qsg_searchSettings']['SearchAutoGenerate']) ? $parseData['qsg_searchSettings']['SearchAutoGenerate'] : '',
				'seo_www_preference' => $parseData['qsg_searchSettings']['seo_www_preference'],
				'SitemapURL' => $parseData['qsg_searchSettings']['SitemapURL'],
			);

			$siteInformationValidationErrors = $quickStartGuideRepository->getSiteInformationValidationErrors($formData['siteInformation']);
			$seoSettingsValidationErrors = $quickStartGuideRepository->getSeoSettingsValidationErrors($formData['seoSettings']);

			if ($siteInformationValidationErrors == false && $seoSettingsValidationErrors == false)
			{
				$searchEngineOptimizationRepository->persistSearchOptimizationSettings($formData['siteInformation']);
				$searchEngineOptimizationRepository->persistSearchOptimizationSettings($formData['seoSettings']);

				Admin_Log::log('QSG Step 3 : SEO Settings has been updated', Admin_Log::LEVEL_IMPORTANT);

				$content  = '<h3>You\'ve just made it easier for your customers to find you.</h3>';
				$content .= '<p>Next we\'re going to collect information about your shipping, payments and taxes.</p>';
				$this->renderJson(
					array(
						'status' => 1,
						'content' => $content,
					)
				);
			}
			else
			{
				$tempValidationErrors = array(
					$siteInformationValidationErrors,
					$seoSettingsValidationErrors,
				);

				$validationErrors = array();
				foreach ($tempValidationErrors as $tempValidationError)
				{
					if (is_array($tempValidationError))
					{
						$validationErrors = array_merge($validationErrors, $tempValidationError);
					}
				}

				$this->renderJson(
					array(
						'status' => 0,
						'errors' => $validationErrors,
					)
				);
			}
		}
	}

	/**
	 * Save QSG Taxes data
	 */
	public function saveTaxesAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$taxRepository = $this->getTaxRepository();

			$taxesDefaults = $taxRepository->getRatesDefaults();
			$taxes_zones = $taxRepository->getTaxZonesList();
			$taxes_classes = $taxRepository->getTaxClassesList();

			$taxesDefaults['taxes_zones'] = $taxes_zones;
			$taxesDefaults['taxes_classes'] = $taxes_classes;

			$formData = $request->request->all();
			$formData['taxes'] = array_merge($taxesDefaults, $formData['taxes']);

			$userLevels = $request->request->get('user_level', array());

			if (!is_array($userLevels))
			{
				$userLevels = array();
			}

			if (count($userLevels) == 0)
			{
				$userLevels = array('0', '1', '2', '3');
			}

			$formData['taxes']['user_level'] = $userLevels;

			if (($validationErrors = $taxRepository->getValidationErrors($formData['taxes'])) == false)
			{
				$taxRepository->persistRate($formData['taxes']);

				$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_RATE_ADDED);
				$taxEvent->setData('taxRate', $formData['taxes']);
				Events_EventHandler::handle($taxEvent);

				Admin_Log::log('QSG Step 4 : Taxes settings has been updated', Admin_Log::LEVEL_IMPORTANT);

				$this->renderJson(
					array(
						'status' => 1,
					)
				);
			}
			else
			{
				$this->renderJson(
					array(
						'status' => 0,
						'errors' => $validationErrors,
					)
				);
			}
		}
	}

	/**
	 * Fetch Payment Method Form and render it as JSON
	 */
	public function fetchPaymentMethodFormAction()
	{
		$adminView = $this->getView();
		$request = $this->getRequest();
		$paymentMethodRepository = $this->getPaymentMethodRepository();

		$paymentMethodId = $request->request->get('payment_method_id', '');

		if ($paymentMethodId == 'custom')
		{
			$taxRepository = $this->getTaxRepository();

			$taxesDefaults = $taxRepository->getRatesDefaults();
			$taxes_zones = $taxRepository->getTaxZonesList();
			$taxes_classes = $taxRepository->getTaxClassesList();

			$taxesDefaults['taxes_zones'] = $taxes_zones;
			$taxesDefaults['taxes_classes'] = $taxes_classes;

			$stepFourFormData = $taxesDefaults;

			$quickStartGuideForm = new Admin_Form_QuickStartGuideForm();

			$adminView->assign('form', $quickStartGuideForm->getCustomPaymentForm($stepFourFormData));

			$this->renderJson(
				array(
					'status' => 1,
					'html' => $adminView->fetch('generic-form'),
				)
			);
		}
		else
		{
			$paymentMethod = $paymentMethodRepository->getPaymentMethodById($paymentMethodId);

			if ($paymentMethod != null && $paymentMethodRepository->methodAvailable($paymentMethodId))
			{
				$editableFormBuilders = $this->getEditableFormBuilders($paymentMethod, $paymentMethodId);

				if (count($editableFormBuilders) > 0)
				{
					$formBuilder = $editableFormBuilders[0];
					$adminView->assign('form', $formBuilder->getForm());
				}
				else
				{
					$adminView->assign('form', false);
				}

				$this->renderJson(
					array(
						'status' => 1,
						'html' => $adminView->fetch('generic-form')
					)
				);
			}
			else
			{
				$this->renderJson(
					array(
						'status' => 0,
						'message' => 'Cannot find payment method by provided ID')
				);
			}
		}
	}

	/**
	 * Activate Payment
	 */
	public function activatePaymentAction()
	{
		$db = $this->getDb();
		$request = $this->getRequest();
		$paymentMethodRepository = $this->getPaymentMethodRepository();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		$serializeData = urldecode($request->get('data'));
		parse_str($serializeData, $data);
		$paymentMethodId = $data['payment_method_id'];

		if ($paymentMethodId == 'custom')
		{
			$paymentMethod = array(
				'pid' => 0,
				'name' => '',
				'title' => '',
				'active' => true,
				'priority' => '',
				'message_payment' => '',
				'message_thankyou' => '',
			);
		}
		else
		{
			$paymentMethod = $paymentMethodRepository->getPaymentMethodById($paymentMethodId);
		}

		if ($paymentMethod != null && $paymentMethodRepository->methodAvailable($paymentMethodId))
		{
			if ($paymentMethodId == 'custom')
			{
				$title = trim($data['title']);
				$priority =  (isset($data['priority']) && !empty($data['priority'])) ? $data['priority'] : '5';
				$messagePayment = $data['message_payment'];
				$messageThankYou = $data['message_thankyou'];

				if ($paymentMethod['pid'] == 0)
				{
					$paymentMethod['name'] = str_replace(' ', '_', strtolower($title));
				}
				$paymentMethodRepository->saveCustomMethod($paymentMethod['pid'], $paymentMethod['name'], $title, $messagePayment, $messageThankYou, $paymentMethod['active'], $priority);
			}
			else
			{
				$paypalEnabledPre = Payment_PayPal_PayPalApi::isPayPalEnabled();

				$db->query("UPDATE " . DB_PREFIX . "payment_methods SET active='Yes' WHERE id='" . $db->escape($paymentMethodId) . "'");

				if ($paymentMethodId == 'bongocheckout')
				{
					$quickStartGuideRepository->save(array('ShippingInternational' => 'YES'));
					//update payment_methods table
					$form = $data['form'];
					$db->query("UPDATE " . DB_PREFIX . "payment_methods SET url_to_gateway='" . $form['bongocheckout_Gateway_Url'] . "' WHERE id='" . $db->escape($paymentMethodId) . "'");
				}
				else if ($paymentMethodId == 'firstdata' && $this->isRecurringBillingEnabled())
				{
					$form = $data['form'];
					$form['firstdata_Payment_Profiles'] = 'Yes';
					$request->request->set('form', $form);
				}
				else
				{
					if ($paymentMethodId == 'braintree')
					{
						$quickStartGuideRepository->save(
							array(
								'braintree_oauth_accessToken'  => '',
								'braintree_oauth_expiresAt'    => '',
								'braintree_oauth_clientid'     => '',
								'braintree_oauth_clientsecret' => ''
							)
						);
					}
				}

				$paymentMethod["active"] = "Yes";

				$paypalEnabled = Payment_PayPal_PayPalApi::isPayPalEnabled();
				if (!$paypalEnabledPre && $paypalEnabled)
				{
					// Activate financing banner widgets
					$db->query("UPDATE " . DB_PREFIX . "layout_elements SET is_active = 1 WHERE name LIKE 'element_panels_panel-paypalbml-%'");
				}
				Admin_Log::log('QSG Step 4 : Payment method ' . $paymentMethodId . ' has been activated', Admin_Log::LEVEL_IMPORTANT);

				$this->savePaymentSettings($paymentMethod);
				$this->checkArePaymentProfilesAvaialble();

				/**
				 * Reread with changes
				 */
				$paymentMethod = $paymentMethodRepository->getPaymentMethodById($paymentMethodId);
				$paymentMethodObject = $this->getPaymentMethodObject($paymentMethod);
				$methodConnected = $paymentMethodObject->testConnection();

				if ($methodConnected === true)
				{
					Admin_Flash::setMessage('Connection information successful');
				}
				else if ($methodConnected === false)
				{
					Admin_Flash::setMessage('Connection information not correct', Admin_Flash::TYPE_ERROR);
				}
			}

			if ($request->request->get('fromQS', false))
			{
				$this->redirect('shipping', array('qsg_set_step' => 'shipping'));
			}
			else
			{
				$this->renderJson(
					array(
						'status' => 1,
						'active' => true,
					)
				);
			}
		}
		else
		{
			//TODO: What to do here?
			error_log('Payment method not found: ' . $paymentMethodId);
		}
	}

	/**
	 * Deactivate Payment
	 */
	public function deactivatePaymentAction()
	{
		$request = $this->getRequest();
		$paymentMethodRepository = $this->getPaymentMethodRepository();
		$serializeData = urldecode($request->get('data'));
		parse_str($serializeData, $data);

		$paymentMethodId = $data['payment_method_id'];
		$paymentMethod = $paymentMethodRepository->getPaymentMethodById($paymentMethodId);

		if (!is_null($paymentMethod))
		{
			$paypalEnabledPre = Payment_PayPal_PayPalApi::isPayPalEnabled();

			if ($paymentMethod['type'] == 'custom')
			{
				$paymentMethodRepository->deactivate($paymentMethodId);

				Admin_Flash::setMessage('common.success');

				Admin_Log::log('QSG Step 4 : Payment method "' . $paymentMethodId . '" settings have been changed', Admin_Log::LEVEL_IMPORTANT);
			}
			else if ($paymentMethod['id'] == 'cart_ccs')
			{
				$paymentMethodRepository->deactivate($paymentMethodId);

				$db = $this->getDb();
				$db->query('UPDATE ' . DB_PREFIX . 'settings SET value = "0" WHERE name = "SecurityCCSActive"');

				Admin_Flash::setMessage('common.success');

				Admin_Log::log('QSG Step 4 : Payment method "' . $paymentMethodId . '" settings have been changed', Admin_Log::LEVEL_IMPORTANT);
			}
			else
			{
				$paymentMethodRepository->deactivate($paymentMethodId);
				$paymentMethod["active"] = "Disable";

				Admin_Flash::setMessage('common.success');

				Admin_Log::log('QSG Step 4 : Payment method "' . $paymentMethodId . '" settings have been changed', Admin_Log::LEVEL_IMPORTANT);

				$paypalEnabled = Payment_PayPal_PayPalApi::isPayPalEnabled();
				if ($paypalEnabledPre && !$paypalEnabled)
				{
					$db = $this->getDb();
					// Deactivate financing banner widgets
					$db->query("UPDATE " . DB_PREFIX . "layout_elements SET is_active = 0 WHERE name LIKE 'element_panels_panel-paypalbml-%'");
				}

				if ($this->isRecurringBillingEnabled())
				{
					if ($paymentMethodRepository->hasActiveRecurringProfiles($paymentMethod['pid']))
					{
						Admin_Flash::setMessage('payment_methods.payment_method_has_recurring_profiles', Admin_Flash::TYPE_INFO);
					}
				}
			}

			$this->checkArePaymentProfilesAvaialble();
		}

		$this->renderJson(array(
			'status' => 1,
			'active' => false,
		));
	}

	/**
	 * Get Image Logo location or path
	 * @return string
	 */
	protected function getImageLogo()
	{
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();

		if ($quickStartGuideRepository->get('ForceSslRedirection') == 'No')
		{
			$baseUrl = $quickStartGuideRepository->get('GlobalHttpUrl');
		}
		else
		{
			$baseUrl = $quickStartGuideRepository->get('GlobalHttpsUrl');
		}

		$imageLogo = $baseUrl . '/' . DesignElements::getImage('image-logo');
		foreach($this->extensions as $extension)
		{
			if (file_exists($quickStartGuideRepository->get('GlobalServerPath') . '/content/skins/_custom/skin/images/image-logo.' . $extension))
			{
				$imageLogo = $baseUrl . '/content/skins/_custom/skin/images/image-logo.' . $extension;
			}
		}

		return $imageLogo;
	}

	/**
	 * @param $paymentMethod
	 * @return null
	 */
	protected function getPaymentMethodObject($paymentMethod)
	{
		if ($paymentMethod == null) return null;

		$paymentProcessorClass = $paymentMethod['class'];

		if (class_exists($paymentProcessorClass)) return new $paymentProcessorClass();

		return null;
	}

	/**
	 * Check if Recurring Billing is enabled
	 * @return bool
	 */
	private function isRecurringBillingEnabled()
	{
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();
		return defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING && $quickStartGuideRepository->get('RecurringBillingEnabled') == '1';
	}

	/**
	 * Save Payment Settings
	 * @param $paymentMethod
	 */
	protected function savePaymentSettings($paymentMethod)
	{
		$request = $this->getRequest();
		$quickStartGuideRepository = $this->getQuickStartGuideRepository();
		$paymentMethodRepository = $this->getPaymentMethodRepository();

		$paymentMethodId = $paymentMethod['id'];

		$form = $request->get('form', array());
		if (!is_array($form)) $form = array();

		foreach ($paymentMethod['settings'] as $settingsOption)
		{
			$name = $settingsOption['name'];

			if (isset($form[$name]))
			{
				$value = $form[$name];
				$quickStartGuideRepository->save(array($name => $value));
			}
			else if (isset($_FILES[$name]) && is_uploaded_file($_FILES[$name]['tmp_name']))
			{
				//check image type
				$is = getimagesize($_FILES[$name]['tmp_name']);
				if ($is && in_array($is[2], array(IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_JPEG)))
				{
					$extensions = array(
						IMAGETYPE_GIF => 'gif',
						IMAGETYPE_PNG => 'png',
						IMAGETYPE_JPEG => 'jpg'
					);

					$destination = 'images/payment/' . escapeFileName($name) . '.' . $extensions[$is[2]];

					@unlink('images/payment/' . escapeFileName($name) . '.jpg');
					@unlink('images/payment/' . escapeFileName($name) . '.gif');
					@unlink('images/payment/' . escapeFileName($name) . '.png');

					move_uploaded_file($_FILES[$name]['tmp_name'], $destination);
					chmod($destination, FileUtils::getFilePermissionMode());

					$quickStartGuideRepository->save(array($name => $destination));
				}
			}
		}

		if (in_array($paymentMethodId, array('paypaladv', 'paypalpflink')) && isset($_FILES['paypalec_Logo']) && is_uploaded_file($_FILES['paypalec_Logo']['tmp_name']))
		{
			//check image type
			$is = getimagesize($_FILES['paypalec_Logo']['tmp_name']);
			if ($is && in_array($is[2], array(IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_JPEG)))
			{
				$extensions = array(
					IMAGETYPE_GIF => 'gif',
					IMAGETYPE_PNG => 'png',
					IMAGETYPE_JPEG => 'jpg'
				);

				$destination = 'images/payment/' . escapeFileName('paypalec_Logo') . '.' . $extensions[$is[2]];

				@unlink('images/payment/' . escapeFileName('paypalec_Logo') . '.jpg');
				@unlink('images/payment/' . escapeFileName('paypalec_Logo') . '.gif');
				@unlink('images/payment/' . escapeFileName('paypalec_Logo') . '.png');

				move_uploaded_file($_FILES['paypalec_Logo']['tmp_name'], $destination);
				chmod($destination, FileUtils::getFilePermissionMode());

				$quickStartGuideRepository->save(array('paypalec_Logo' => $destination));
			}
		}

		$paymentMethodData = $request->get('payment_method', array());
		if (is_array($paymentMethodData) && isset($paymentMethodData['title']))
		{
			$paymentMethodRepository->updatePaymentMethod($paymentMethodData, $paymentMethod['pid']);
		}

		/**
		 * Certificate file
		 */
		if (array_key_exists("certificate_file", $_FILES) && file_exists($_FILES["certificate_file"]["tmp_name"]))
		{
			$certificate_path = trim($request->get('certificate_path'));

			if ($certificate_path != '' && is_writable($certificate_path))
			{
				copy($_FILES["certificate_file"]["tmp_name"], $certificate_path);
			}
		}

		$paymentAcceptedCCData = $request->request->get('payment_accepted_cc', array());
		if (is_array($paymentMethod['supported_cards']))
		{
			foreach ($paymentMethod['supported_cards'] as $supportedCard)
			{
				$enabled = $supportedCard['enable'] == 'Yes';
				$cvv2Enabled = $supportedCard['enable_cvv2'] == 'Yes';
				$changed = false;

				if (isset($paymentAcceptedCCData[$supportedCard['ccid']]))
				{
					$newEnabled = isset($paymentAcceptedCCData[$supportedCard['ccid']]['enable']) && $paymentAcceptedCCData[$supportedCard['ccid']]['enable'] == 'Yes';
					$newCvv2Enabled = isset($paymentAcceptedCCData[$supportedCard['ccid']]['enable_cvv2']) && $paymentAcceptedCCData[$supportedCard['ccid']]['enable_cvv2'] == 'Yes';
					if ($newEnabled != $enabled)
					{
						$enabled = $newEnabled;
						$changed = true;
					}
					if ($newCvv2Enabled != $cvv2Enabled)
					{
						$cvv2Enabled = $newCvv2Enabled;
						$changed = true;
					}
				}
				else if ($enabled || $cvv2Enabled)
				{
					$enabled = false;
					$cvv2Enabled = false;
					$changed = true;
				}

				if ($changed)
				{
					$paymentMethodRepository->updateAcceptedCC($paymentMethod['pid'], $supportedCard['ccid'], $enabled, $cvv2Enabled);
				}
			}
		}

		Admin_Log::log('QSG Step 4 : Payment method "' . $paymentMethodId . '" settings have been changed', Admin_Log::LEVEL_IMPORTANT);
	}

	/**
	 * Check if Payment profile is available
	 */
	protected function checkArePaymentProfilesAvaialble()
	{
		$db = $this->getDb();

		$paymentProfilesEnabled = false;

		$db->query("SELECT * FROM " . DB_PREFIX . "payment_methods WHERE active='Yes'");

		while ($db->moveNext())
		{
			$paymentMethodClass = $db->col['class'];

			if (class_exists($paymentMethodClass))
			{
				$gw_temp = new $paymentMethodClass;

				if ($gw_temp->supportsPaymentProfiles())
				{
					$paymentProfilesEnabled = true;
					break;
				}
			}
		}

		$this->getQuickStartGuideRepository()->save(array('paymentProfilesEnabled' => $paymentProfilesEnabled ? 1 : 0));
	}

	/**
	 * @param $paymentMethod
	 * @param $paymentMethodId
	 * @return array
	 */
	protected function getEditableFormBuilders($paymentMethod, $paymentMethodId)
	{
		$groupId = 0;
		/**
		 * Generate settings form from settings table
		 */
		$editableFormBuilders = array();

		$groupLabels = array(
			0 => 'Account Details',
			1 => 'Advanced Settings',
		);

		foreach ($paymentMethod['settings'] as $settingsOption)
		{
			$groupId = $settingsOption['group_id'];

			if (!isset($editableFormBuilders[$groupId]))
			{
				$editableFormBuilders[$groupId] = new core_Form_FormBuilder(
					'editable' . $groupId,
					array(
						'label' => array_key_exists($groupId, $groupLabels) ? $groupLabels[$groupId] : 'Properties',
						'class' => 'ic-payment',
						'first' => $groupId == 0,
						'wrapperClass' => 'col-sm-12 col-xs-12 clear-both',
					)
				);
			}

			/** @var core_Form_FormBuilder $groupFormBuilder */
			$groupFormBuilder = $editableFormBuilders[$groupId];

			switch ($settingsOption['input_type'])
			{
				case 'text' :
				{
					$groupFormBuilder->add(
						'form[' . $settingsOption['name'] . ']',
						'text',
						array(
							'label' => $settingsOption['caption'],
							'required' => false,
							'value' => $settingsOption['value'],
							'note' => $settingsOption['description'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
					break;
				}
				case 'password' :
				{
					$groupFormBuilder->add(
						'form[' . $settingsOption['name'] . ']',
						'password',
						array(
							'label' => $settingsOption['caption'],
							'required' => false,
							'value' => $settingsOption['value'],
							'note' => $settingsOption['description'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
					break;
				}
				case 'color' :
				{
					$groupFormBuilder->add(
						'form[' . $settingsOption['name'] . ']',
						'color',
						array(
							'label' => $settingsOption['caption'],
							'required'=>false,
							'value' => $settingsOption['value'],
							'note' => $settingsOption['description'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
					break;
				}
				case 'textarea' :
				{
					$groupFormBuilder->add(
						'form[' . $settingsOption['name'] . ']',
						'textarea',
						array(
							'label' => $settingsOption['caption'],
							'required' => false,
							'value' => $settingsOption['value'],
							'note' => $settingsOption['description'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
					break;
				}
				case 'label' :
				{
					$groupFormBuilder->add(
						'form[' . $settingsOption['name'] . ']',
						'static',
						array(
							'label' => $settingsOption['caption'],
							'value' => $settingsOption['value'],
							'note' => $settingsOption['description'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
					break;
				}
				case 'hidden' :
				{
					$groupFormBuilder->add(
						'form[' . $settingsOption['name'] . ']',
						'hidden',
						array(
							'value' => $settingsOption['value'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
					break;
				}
				case 'select' :
				{
					$fieldOptionsArray = explode(',', $settingsOption['options']);
					$fieldOptions = array();

					foreach ($fieldOptionsArray as $fieldOption)
					{
						if (strpos($settingsOption['name'], 'Currency_Code') !== false)
						{
							$fieldOptions[] = new core_Form_Option(trim($fieldOption), $fieldOption);
						}
						else
						{
							$fieldOptions[] = new core_Form_Option(trim($fieldOption), ucwords(strtolower($fieldOption)));
						}
					}

					$groupFormBuilder->add(
						'form[' . $settingsOption['name'] . ']',
						'choice',
						array(
							'label' => $settingsOption['caption'],
							'required' => false,
							'value' => $settingsOption['value'],
							'options' => $fieldOptions,
							'note' => $settingsOption['description'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
					break;
				}
				case 'image' :
				{
					$groupFormBuilder->add(
						$settingsOption['name'],
						'file',
						array(
							'label' => $settingsOption['caption'],
							'note' => $settingsOption['description'],
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);

					if ($settingsOption['value'] != '')
					{
						$groupFormBuilder->add(
							'form[preview_' . $settingsOption['name'] . ']',
							'imagepreview',
							array(
								'label' => 'Current image',
								'value' => $settingsOption['value'],
								'note' => 'Click over image to see it real size',
								'wrapperClass' => 'col-sm-12 col-xs-12',
							)
						);
					}
					break;
				}
			}
		}

		if (in_array($paymentMethodId, array('paypaladv', 'paypalpflink')))
		{
			$db = $this->getDb();
			$db->query("SELECT * FROM " . DB_PREFIX . "settings WHERE name ='paypalec_Logo'");
			if ($settingsOption = $db->moveNext())
			{
				$groupFormBuilder = $editableFormBuilders[$groupId];
				$groupFormBuilder->add(
					$settingsOption['name'],
					'file',
					array(
						'label' => $settingsOption['caption'],
						'note' => $settingsOption['description'],
						'wrapperClass' => 'col-sm-12 col-xs-12',
					)
				);

				if ($settingsOption['value'] != '')
				{
					$groupFormBuilder->add(
						'form[preview_' . $settingsOption['name'] . ']',
						'imagepreview',
						array(
							'label' => 'Current image',
							'value' => $settingsOption['value'],
							'note' => 'Click over image to see it real size',
							'wrapperClass' => 'col-sm-12 col-xs-12',
						)
					);
				}
			}
		}

		return $editableFormBuilders;
	}

	/**
	 * @return DataAccess_CountryRepository
	 */
	protected function getCountryRepository()
	{
		if (is_null($this->countryRepository))
		{
			$this->countryRepository = new DataAccess_CountryRepository($this->getDb());
		}

		return $this->countryRepository;
	}

	/**
	 * @return DataAccess_SearchEngineOptimizationRepository|null
	 */
	protected function getSearchEngineOptimizationRepository()
	{
		if (is_null($this->searchEngineOptimizationRepository))
		{
			$this->searchEngineOptimizationRepository = new DataAccess_SearchEngineOptimizationRepository($this->getDb(), $this->getSettings()->getAll());
		}
		return $this->searchEngineOptimizationRepository;
	}

	/**
	 * @return DataAccess_QuickStartGuideRepository|null
	 */
	protected function getQuickStartGuideRepository()
	{
		if (is_null($this->quickStartGuideRepository))
		{
			$this->quickStartGuideRepository = new DataAccess_QuickStartGuideRepository($this->getDb());
		}

		return $this->quickStartGuideRepository;
	}

	/**
	 * @return DataAccess_ShippingRepository
	 */
	protected function getShippingRepository()
	{
		if (is_null($this->shippingRepository))
		{
			$this->shippingRepository = new DataAccess_ShippingRepository($this->getDb());
		}

		return $this->shippingRepository;
	}

	/**
	 * @return DataAccess_TaxRepository|null
	 */
	protected function getTaxRepository()
	{
		if (is_null($this->taxRepository))
		{
			$this->taxRepository = new DataAccess_TaxRepository($this->getDb());
		}

		return $this->taxRepository;
	}

	/**
	 * @return DataAccess_PaymentMethodRepository|null
	 */
	protected function getPaymentMethodRepository()
	{
		if (is_null($this->paymentMethodRepository))
		{
			$this->paymentMethodRepository = new DataAccess_PaymentMethodRepository($this->getDb());
		}

		return $this->paymentMethodRepository;
	}

	/**
	 * @return bool
	 */
	protected function getForceAddressValidation()
	{
		$settings = $this->getSettings();

		return $settings->get('ExactorEnableTax') == 'Yes' || $settings->get('AvalaraEnableTax') == 'Yes';
	}

	/**
	 * Will save the image logo
	 */
	public function saveImageLogoAction()
	{
		$request = $this->getRequest();

		if ($imageUploadInfo = $request->getUploadedFile('logo'))
		{
			$settings = $this->getSettings();

			$imageLogoPath = 'content/skins/_custom/skin/images';
			$imageLogoCachePath = 'content/cache/skins/' . escapeFileName($settings->get('DesignSkinName')) . '/images';

			if (!is_dir($imageLogoPath)) @mkdir($imageLogoPath, FileUtils::getDirectoryPermissionMode(), true);
			if (!is_dir($imageLogoCachePath)) @mkdir($imageLogoCachePath, FileUtils::getDirectoryPermissionMode(), true);

			/** @var Admin_Service_FileUploader $fileUploaderService */
			$fileUploadService = new Admin_Service_FileUploader();
			$imageFile = $fileUploadService->processImageUpload($imageUploadInfo, $imageLogoPath, 'image-logo', true);

			if ($imageFile)
			{
				if ($settings->get('CatalogOptimizeImages') == 'YES')
				{
					$optimizer = new Admin_Service_ImageOptimizer();
					$optimizer->optimize($imageFile);
				}

				$imageFileName = pathinfo($imageFile, PATHINFO_FILENAME);
				$imageCacheFileName = $imageLogoCachePath . '/' . $imageFileName;

				@copy($imageFile, $imageCacheFileName);

				$db = $this->getDb();

				if ($db->selectOne('SELECT * FROM ' . DB_PREFIX . 'design_elements WHERE element_id="image-logo"'))
				{
					$db->reset();
					$db->assignStr('type', 'image');
					$db->assignStr('content', $imageCacheFileName);
					$db->update(DB_PREFIX . 'design_elements',  'WHERE element_id="image-logo"');
				}
				else
				{
					$db->reset();
					$db->assignStr('group_id', 'image');
					$db->assignStr('removable', 'Yes');
					$db->assignStr('element_id', 'image-logo');
					$db->assignStr('name', 'image-logo');
					$db->assignStr('type', 'image');
					$db->assignStr('content', $imageCacheFileName);
					$db->insert(DB_PREFIX . 'design_elements');
				}

				@chmod($imageFileName, FileUtils::getFilePermissionMode());
				@chmod($imageCacheFileName, FileUtils::getFilePermissionMode());
			}
		}

		$this->renderJson(
			array(
				'status' => 1,
				'active' => true,
			)
		);
	}

	/**
	 * @return bool|mixed
	 */
	protected function getCurrency()
	{
		global $admin_currency;

		return $admin_currency;
	}

	/**
	 * @return array
	 */
	protected function getCustomRateDefaults()
	{
		return array(
			'carrier_id' => 'custom',
			'carrier_name' => '',
			'method_id' => 'flat',
			'method_calc_mode' => 'before_discount',
			'method_name' => 'Per item',
			'priority' => 5,
			'country' => 1,
			'state' => 0,
			'weight_min' => 0,
			'weight_max' => 100000,
			'fee' => 0,
			'fee_type' => 'amount',
			'exclude' => 'No',
			'hidden' => 'No',
			'rates' => array(),
		);
	}

	/**
	 * @param $methodId
	 * @return string
	 */
	protected function setMethodName($methodId)
	{
		switch ($methodId)
		{
			case 'flat' :
			{
				$methodName = trans('shipping.per_item');
				break;
			}
			case 'price' :
			{
				$methodName = trans('shipping.order_subtotal');
				break;
			}
			case 'base_weight' :
			{
				$methodName = trans('shipping.weight');
				break;
			}
			case 'product' :
			{
				$methodName = trans('shipping.product_level');
				break;
			}
		}

		return $methodName;
	}

	/**
	 * @param $method
	 */
	protected function ensureInternationalShippingEnabled($method)
	{
		$settings = $this->getSettings();

		if ($method['country'] != $settings->get('ShippingOriginCountry'))
		{
			$settings->persist(array('ShippingInternational' => 'YES'));
		}
	}
}
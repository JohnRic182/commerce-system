<?php

class Admin_Controller_SpeedTest extends Admin_Controller
{
	public function showSpeedTest()
	{
		$adminView = $this->getView();
		$results = array(
			'product' => $this->getProductQuerySpeed(),
			'customer' => $this->getCustomerQuerySpeed(),
			'order' => $this->getOrderQuerySpeed()
		);
		$adminView->assign('results', $results);
		$adminView->assign('body', 'templates/pages/speed-test/index.html');
		$adminView->render('layouts/default');
	}
	
	protected function getProductQuerySpeed()
	{
		// get products data
		$repository = new DataAccess_ProductsRepository($this->getDb());
		$params = array(
			'product_id' => '',
			'sub_id' => '',
			'title' => '',
			'cid' => 'any',
			'status' => 'any',
			'logic' => 'AND');
		$itemsCount = $repository->getCount($params, $params['logic']);
		$start = microtime(true);
		// query all the products
		$result = $repository->getList(null, null, null, $params, 'p.*', $params['logic']);
		$end = microtime(true);
		$milliseconds = $end - $start;
		$seconds = $milliseconds % 60;
		return $this->getResults($result, $itemsCount, $milliseconds, $seconds);
	}
	
	protected function getCustomerQuerySpeed()
	{
		$repository = new DataAccess_CustomersRepository($this->getDb(), $this->getSettings());
		// get customers data
		$params = array(
			'logic' => 'AND');
		$itemsCount = $repository->getCustomersCount($params, $params['logic']);
		$start = microtime(true);
		// query all the customers
		$result = $repository->getCustomersList(null, null, null, $params, $params['logic']);
		$end = microtime(true);
		$milliseconds = $end - $start;
		$seconds = $milliseconds % 60;
		return $this->getResults($result, $itemsCount, $milliseconds, $seconds);
	}
	
	protected function getOrderQuerySpeed()
	{
		$settings = $this->getSettings()->getAll();
        $repository = new DataAccess_OrderRepository($this->getDb(), $settings);
		$params = array(
			'order_num' => '',
			'custom_search' => '',
			'status' => 'Process',
			'fulfillment_status' => '',
			'order_type' => 'Web',
			'payment_status' => 'any',
			'period' => 'any',
			'orderBy' => 'order_num_desc',
			'product_id' => '',
			'min_amount' => '',
			'max_amount' => '',
			'state' => '',
			'zip_code' => '',
			'notes_content' => '',
			'promo_code' => '',
			'product_name' => '',
			'logic' => 'AND',
			'AdvanceSearch' => 'NO',
		);
		$itemsCount = $repository->getCount($params, $params['logic']);
		$start = microtime(true);
		// query all the orders
		$result = $repository->getList(null, null, null, $params, 'o.*', $params['logic']);
		$end = microtime(true);
		$milliseconds = $end - $start;
		$seconds = $milliseconds % 60;
		return $this->getResults($result, $itemsCount, $milliseconds, $seconds);
		
	}
	
	protected function getResults($result, $itemsCount, $milliseconds, $seconds)
	{
		$data = array(
			'result' => $result,
			'count' => $itemsCount <= 1 ? $itemsCount.' row' : $itemsCount.' rows',
			'milliseconds' => number_format($milliseconds,5). " ms",
			'seconds' => $seconds. " sec"
		);
		return $data;
	}
}

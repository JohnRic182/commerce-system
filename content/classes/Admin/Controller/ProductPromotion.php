<?php

/**
 * Class Admin_Controller_ProductPromotion
 */
class Admin_Controller_ProductPromotion extends Admin_Controller
{
	protected $productPromotionRepository = null;
	protected $productRepository = null;

	/**
	 * Product lookup
	 */
	public function addPromotionProductLookupAction()
	{
		$request = $this->getRequest();

		$searchStr = trim($request->get('str', ''));

		$products = $searchStr != '' ? $this->getProductRepository()->lookUp($searchStr) : array();

		$this->renderJson(array('status' => 1, 'products' => $products));
	}

	/**
	 * Add promotion
	 */
	public function addPromotionAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productPromotionsRepository = $this->getProductPromotionRepository();

			$defaults = $productPromotionsRepository->getDefaults();

			$formData = array_merge($defaults, $request->get('promotion', array()));
			$formData['id'] = $formData['product_gift_id'] = null;

			if (($validationErrors = $productPromotionsRepository->getValidationErrors($formData, $mode)) == false)
			{
				$productPromotionsRepository->persist($formData);
				$result = array('status' => 1, 'promotions' => $productPromotionsRepository->getListByProductId($formData['pid']));
			}
			else
			{
				$result = array('status' => 0, 'errors' => $validationErrors);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Update promotion
	 */
	public function updatePromotionAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productPromotionsRepository = $this->getProductPromotionRepository();

			$formData = array_merge($productPromotionsRepository->getDefaults(), $request->get('promotion', array()));

			$promotionData = $productPromotionsRepository->getById($formData['id']);

			if ($promotionData)
			{
				if (($validationErrors = $productPromotionsRepository->getValidationErrors($formData, $mode)) == false)
				{
					$productPromotionsRepository->persist($formData);
					$result = array('status' => 1, 'promotions' => $productPromotionsRepository->getListByProductId($formData['pid']));
				}
				else
				{
					$result = array('status' => 0, 'errors' => $validationErrors);
				}
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find promotion by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update promotion. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Delete product promotion
	 */
	public function deletePromotionAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productPromotionsRepository = $this->getProductPromotionRepository();

			$formData = array_merge($productPromotionsRepository->getDefaults(), $request->get('promotion', array()));

			$promotionData = $productPromotionsRepository->getById($formData['id']);

			if ($promotionData)
			{
				$productPromotionsRepository->delete(array($formData['id']));

				$result = array('status' => 1, 'promotions' => $productPromotionsRepository->getListByProductId($formData['pid']));

			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find promotion by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update promotion. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductPromotionRepository
	 */
	protected function getProductPromotionRepository()
	{
		if (is_null($this->productPromotionRepository))
		{
			$this->productPromotionRepository = new DataAccess_ProductPromotionRepository($this->getDb());
		}

		return $this->productPromotionRepository;
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductsRepository
	 */
	protected function getProductRepository()
	{
		if (is_null($this->productRepository))
		{
			$this->productRepository = new DataAccess_ProductsRepository($this->getDb());
		}

		return $this->productRepository;
	}
}
<?php

/**
 * Class Admin_Controller_NotificationEmails
 */
class Admin_Controller_NotificationEmails extends Admin_Controller
{
	const MODE_HFUPDATE = 'header-footer-edit';
	const TEMPLATES_PATH = 'templates/pages/notification-emails/';

	/** @var DataAccess_NotificationEmailsRepository */
	protected $dataRepository = null;
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * Edit header & footer action
	 */
	public function headerFooterEditAction()
	{
		$mode = self::MODE_HFUPDATE;
		$request = $this->getRequest();
		$repository = $this->getDataRepository();

		$hf_templates = $repository->getHeaderFooterData();

		if (!$hf_templates)
		{
			Admin_Flash::setMessage('Cannot find header & footer emails templates', Admin_Flash::TYPE_ERROR);
			$this->redirect('email_notifications');
		}

		$formBuilder = new Admin_Form_NotificationEmailForm();
		$form = $formBuilder->getForm($hf_templates, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'email_hftemplate_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('email_notifications');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrorsAndPersist($formData)) == false)
				{
					Admin_Flash::setMessage('common.success');
					$this->redirect('email_notification', array(/**'id' => $id, **/'mode' => self::MODE_HFUPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8022');

		$adminView->assign('mode', $mode);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('email_hftemplate_update'));

		$adminView->assign('body', self::TEMPLATES_PATH.'hfedit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Notification emails list
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8021');

		$repository = $this->getDataRepository();
		$request = $this->getRequest();
		$itemsCount = $repository->getCount();
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('notificationEmails', $repository->getList($paginator->sqlOffset, $paginator->itemsPerPage));
		}
		else
		{
			$adminView->assign('notificationEmails', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('notification_email_delete'));

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Edit email action
	 */
	public function editAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$id = $request->get('id');

		$email_template = $repository->getById($id);

		if (!$email_template)
		{
			Admin_Flash::setMessage('Cannot find email template', Admin_Flash::TYPE_ERROR);
			$this->redirect('email_notifications');
		}

		$formBuilder = new Admin_Form_NotificationEmailForm();

		$form = $formBuilder->getForm($email_template, $mode, $id);

		/**
		 * Handle form post$errors
		 */
		if ($request->isPost())
		{
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'email_template_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('email_notifications');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrorsAndPersist($formData)) == false)
				{
					Admin_Flash::setMessage('common.success');
					$this->redirect('email_notification', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8023');
		$adminView->assign('title', $email_template['title']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('email_template_update'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}


	/**
	 * Get repository
	 *
	 * @return DataAccess_NotificationEmailsRepository
	 */
	protected function getDataRepository()
	{
		if ($this->dataRepository === null)
		{
			$this->dataRepository = new DataAccess_NotificationEmailsRepository($this->getDb(), $this->getSettings()->get('DesignSkinName'));
		}

		return $this->dataRepository;
	}

}
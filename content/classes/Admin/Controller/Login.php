<?php

/**
 * Class Admin_Controller_Login
 */
class Admin_Controller_Login extends Framework_Controller
{
	protected $errorMessage = false;
	protected $isError = true;

	public function indexAction()
	{
		global $iono, $label_app_name;
		$request = $this->getRequest();
		$settings = $this->getSettings();
		$db = $this->getDb();

		if (shouldBeSecure())
		{
			$this->redirect();
		}

		/**
		 * Check license
		 */
		$iono->iono_keys_call4520(trim(LICENSE_NUMBER), "b575796b4295", 'content/cache/license/license.key', 604800); //172800 - 2 days

		if ($iono->status == "")
		{
			$iono->get_status_1980();
		}

		/**
		 * If license is valid and was entered by user, save it
		 */
		if ($iono->result == 1)
		{
			$license_error_message = false;
		}
		else
		{
			$license_error_message = $iono->status;
		}

		$action = $request->get('action');

		if ($request->isPost())
		{
			switch ($action)
			{
				case 'reset':
				{
					if ($request->request->get('validate', 0) == 1)
					{
						if (preg_match('/\W/', $request->query->get('callback'))) {
							// if $_GET['callback'] contains a non-word character,
							// this could be an XSS attack.
							header('HTTP/1.1 400 Bad Request');
							exit();
						}

						$username = $request->request->get('username');
						$email = $request->request->get('email');

						$result = $db->selectOne("SELECT security_question_id FROM ".DB_PREFIX."admins WHERE username = '".$db->escape($username)."' AND	email = '".$db->escape($email)."'");
						$response = array(
							'verified' => $result ? true : false,
							'q' => $result && $result['security_question_id'] > 0 ? trans('admin.profile.security_question_'.$result['security_question_id']) : '',
						);

						if ($result)
						{
							Admin_Log::log('Login - account password reset validated', Admin_Log::LEVEL_NORMAL, true, $username);
						}
						else
						{
							Admin_Log::log('Login - account password reset incorrect', Admin_Log::LEVEL_CRITICAL, true, $username);
						}

						$this->renderJson($response);
						return;
					}
					else if ($request->request->get('recovery', 0) == 1)
					{
						$username = $request->request->get('username');
						$email = $request->request->get('email');
						$answer = $request->request->get('answer');

						$admin = $db->selectOne("SELECT *, CONCAT(fname, ' ', lname) as name FROM ".DB_PREFIX."admins WHERE username = '".$db->escape($username)."' AND email = '".$db->escape($email)."'");

						if ($admin && ($admin['security_question_id'] == 0 || checkPasswordHash($answer, $admin['security_question_answer'])))
						{
							$reset_message = 'E-mail notification has been sent to provided email address.';

							$new_password = generatePassword();

							$password_history = array();
							if ($admin["password_history"] != "") $password_history = unserialize(base64_decode($admin["password_history"]));
							$passwordHash = getPasswordHash($new_password);
							$_password_history = $password_history;
							foreach ($_password_history as $key=>$value)
							{
								if($value["p"] == $passwordHash)
								{
									unset($password_history[$key]);
									break;
								}
							}
							$_password_history = array();
							foreach ($password_history as $ph) $_password_history[] = $ph;
							$password_history = $_password_history;
							if(count($password_history) > 3) array_shift($password_history);
							$password_history[] = array("p" => $passwordHash, "s" => pwdStrength($new_password));

							$db->reset();
							$db->assignStr("password", $passwordHash);
							$db->assignStr("password_history", base64_encode(serialize($password_history)));
							$db->assign("password_change_date", "NOW()");
							$db->assign("last_update", "NOW()");
							$db->update(DB_PREFIX."admins", "WHERE aid='".intval($admin["aid"])."'");

							view()->initNotificationsSmarty();
							Notifications::emailAdminNewPassword($admin, $new_password);

							Admin_Log::log('Login - account password reset', Admin_Log::LEVEL_NORMAL, true, $username);
							$this->errorMessage = $reset_message;
							$this->isError = false;
						}
						else
						{
							$this->errorMessage = "Provided username or e-mail is incorrect".(($answer != "") ? ", or security question answer is invalid" : "");
							Admin_Log::log('Login - account password reset incorrect', Admin_Log::LEVEL_CRITICAL, true, $username);
						}
					}
					break;
				}
				case 'login':
				{
					global $admin;

					/**
					 * Check login form
					 */
					$username = $request->request->get('username');
					$password = $request->request->get('password');

					$login_successful = $this->validateLogin($username, $password);

					$request = $this->getRequest();
					$is_ajax = $request->request->get('is_ajax', false);

					if ($is_ajax)
					{
						$session = $this->getSession();
						$session->set('QB_SYNCD', true);

						$result = array(
							'status' => $login_successful ? true : false,
							'error_message' => $this->errorMessage,
							'blocked' => $admin['active'] == 'Block',
						);

						$this->renderJson($result);
						return;
					}
					else if ($login_successful)
					{
						Nonce::resurrect($settings->get('GlobalServerPath') . '/content/cache/sessiondata/nonce_' . md5(strtolower(trim($username))));
						header("Location: ".($settings->get('AdminHttpsUrl')).'/admin.php');

						return;
					}
					break;
				}
			}
		}

		if ($action == 'logout')
		{
			$session = $this->getSession();

			$aid = $session->get('admin_auth_id', false);

			// log event
			$adminData = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'admins WHERE aid = ' . intval($aid));
			Admin_Log::log('Logging out', Admin_Log::LEVEL_NORMAL, true, $adminData['username']);

			Nonce::preserve($settings->get('GlobalServerPath') . '/content/cache/sessiondata/nonce_' . md5(strtolower(trim($adminData['username']))));

			$session->invalidate();
		}
		else if ($action == 'openid')
		{
			require_once dirname(dirname(dirname(dirname(__FILE__)))).'/vendors/lightopenid/openid.php';
			$openid = new LightOpenID($settings->get('AdminHttpsUrl'));

			if (!$openid->mode)
			{
				$openid->identity = 'https://openid.intuit.com/Identity-'.getpp();

				$openid->required = array('contact/email');
				$openid->optional = array('namePerson', 'namePerson/friendly');
				header('Location: ' . $openid->authUrl());
				exit();
			}
			else if ($openid->mode != 'cancel' && $openid->validate())
			{
				$identity = $openid->identity;

				$db->query("
					SELECT *, NOW() AS now, IF (expires=1 AND expiration_date < NOW(), 1, 0) AS expired FROM ".DB_PREFIX."admins WHERE openid_provider='intuit' AND openid='".$db->escape($identity)."'
					");
				if (($admin = $db->moveNext()) != false)
				{
					$username = $admin['username'];

					$login_successful = $this->handleLogin($admin, md5($username.$identity.date("Ymdhms").uniqid(rand(), true)), true);

					if ($login_successful)
					{
						header("Location: ".($settings->get('AdminHttpsUrl')).'/admin.php');
						return;
					}
				}
			}
		}

		$adminView = $this->getView();

		if (IS_LABEL && defined('LABEL_APP_IMAGE') && trim(LABEL_APP_IMAGE) != '')
		{
			$adminView->assign('logo', LABEL_APP_IMAGE);
		}
		else
		{
			$adminView->assign('logo', 'images/admin/logo-light.png');
		}

		$adminView->assign('license_error_message', $license_error_message);

		if ($license_error_message)
		{
			$adminView->assign('body', 'templates/pages/license_error.html');
		}
		else
		{
			$hasIntuitOpenId = false;

			$db->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."admins WHERE openid <> ''");

			if (($row = $db->moveNext()) && $row['c'] != 0)
			{
				$hasIntuitOpenId = true;
			}

			$adminView->assign('hasIntuitOpenId', $hasIntuitOpenId);

			$adminView->assign('is_error', $this->isError);
			$adminView->assign('login_message', $this->errorMessage);
			$adminView->assign('body', 'templates/pages/login.html');
		}

		$adminView->render('layouts/login');
	}

	/**
	 * @param $username
	 * @param $password
	 * @return bool
	 */
	protected function validateLogin($username, $password)
	{
		global $admin;

		$db = $this->getDb();
		Admin_Log::log('Login attempt', Admin_Log::LEVEL_NORMAL, true, $username);

		/**
		 * Try to login
		 */
		$admin = $db->selectOne('
			SELECT *, NOW() AS now, IF (expires=1 AND expiration_date < NOW(), 1, 0) AS expired
			FROM ' . DB_PREFIX . 'admins
			WHERE LCASE(username) = "' . $db->escape(strtolower($username)) . '"
		');

		if ($admin)
		{
			return $this->handleLogin($admin, md5($username . $password . date('Ymdhms') . uniqid(rand(), true)));
		}
		else
		{
			$this->errorMessage = 'Username or password is incorrect';
			Admin_Log::log('Login failed', Admin_Log::LEVEL_CRITICAL, true, $username);

			return false;
		}
	}

	/**
	 * @param $admin
	 * @param $admin_session_id
	 * @param bool $bypassPassword
	 * @return bool
	 */
	protected function handleLogin($admin, $admin_session_id, $bypassPassword = false)
	{
		$login_successful = false;

		$db = $this->getDb();
		$session = $this->getSession();

		$username = $admin['username'];

		/**
		 * username is correct, now check everything else
		 */
		$db->reset();
		$do_update = false;

		/**
		 * check is account already blocked
		 */
		if ($admin['active'] == 'Block')
		{
			$bloked_till = strtotime($admin['last_access']) + 1800;
			$now = strtotime($admin['now']);
			$left = $bloked_till - $now;

			//check is it time to unblock
			if ($left < 0)
			{
				//unblock account
				$admin["active"] = "Yes";
				$admin["login_errors"] = 0;
				$db->assignStr("active", "Yes");
				$db->assignStr("login_errors", "0");
				$do_update = true;
				Admin_Log::log('Login - account unblocked', Admin_Log::LEVEL_CRITICAL, true, $username);
			}
			else
			{
				//30 minutes aren't passed yet
				$this->errorMessage = "Account '".htmlspecialchars($username)."' has been blocked because of too many failed attempts. Please try again in ".(date("i", $left))." minute(s) ".(date("s", $left))." second(s) or contact your account administrator.";
				Admin_Log::log('Login failed - account still blocked', Admin_Log::LEVEL_CRITICAL, true, $username);
			}
		}

		/**
		 * if account active, check password
		 */
		if ($admin["active"] == "Yes")
		{
			//md5 added for backward compatibility
			if ($bypassPassword || $this->checkValidLogin($admin))
			{
				/**
				 * Check for expiration
				 */
				if ($admin['expired'] == 1)
				{
					$this->errorMessage = "Account '".htmlspecialchars($username)."' is expired. Please contact your account administrator.";
					Admin_Log::log('Login failed - account expired', Admin_Log::LEVEL_CRITICAL, true, $username);
				}
				else
				{
					session_regenerate_id(true);

					/**
					 * Set security variables
					 */
					$session->set('admin_session_id', $admin_session_id);
					$session->set('admin_auth_id', $admin['aid']);
					$session->set('admin_auth_time', time());

					/**
					 * Update security session id and database in db, also resel login errors counter
					 */
					$db->assignStr('session_id', $admin_session_id);
					$db->assign('last_access', 'NOW()');
					$db->assign('login_errors', 0);

					if ($bypassPassword)
					{
						Admin_Log::log('Login successful - Intuit OpenId', Admin_Log::LEVEL_NORMAL, true, $username);
					}
					else
					{
						Admin_Log::log('Login successful', Admin_Log::LEVEL_NORMAL, true, $username);
					}

					$login_successful = true;
				}
			}
			else
			{
				Admin_Log::log('Login failed - password incorrect', Admin_Log::LEVEL_CRITICAL, true, $username);

				/**
				 * Password incorrect
				 */
				if ($admin['login_errors'] >= 5)
				{
					Admin_Log::log('Login failed - account blocked after 6 fails', Admin_Log::LEVEL_CRITICAL, true, $username);
					/**
					 * we already have 6 fails, block account
					 */
					$db->assignStr('login_errors', 0);
					$db->assignStr('active', 'Block');
					$this->errorMessage = 'Account "' . htmlspecialchars($username) . '" has been blocked because of too many failed attempts. Please try again in 30 minutes or contact your account administrator.';
				}
				else
				{
					/**
					 * increase number of fails
					 */
					$db->assignStr('login_errors', $admin['login_errors'] + 1);
					$this->errorMessage = 'Username or password is incorrect';
				}
			}
			$db->assign("last_access", "NOW()");
			$do_update = true;
		}

		/**
		 * if account deactivated, inform user about that
		 */
		if ($admin["active"] == "No")
		{
			Admin_Log::log('Login failed - account deactivated', Admin_Log::LEVEL_CRITICAL, true, $username);
			$this->errorMessage = "This account is deactivated. Please contact system administrator.";
		}

		if ($do_update)
		{
			$db->update(DB_PREFIX."admins", "WHERE aid='".intval($admin["aid"])."'");
		}

		if ($login_successful)
		{
			$session->remove('QB_SYNCD');
		}

		return $login_successful;
	}

	/**
	 * @param $admin
	 * @return bool
	 */
	protected function checkValidLogin($admin)
	{
		$request = $this->getRequest();
		$password = $request->request->get('password');

		return checkPasswordHash($password, $admin['password']) || $admin['password'] == sha1($password);
	}

	/**
	 * Clear license
	 */
	public function clearLicenseAction()
	{
		@unlink(dirname(dirname(dirname(dirname(__FILE__)))).'/cache/license/license.key');

		$this->redirect();
	}

	/**
	 * @return Ddm_AdminView
	 */
	public function getView()
	{
		return Ddm_AdminView::getInstance();
	}

	/**
	 * @return string
	 */
	protected function getBasePath()
	{
		$settings = $this->getSettings();
		return $settings->get('AdminHttpsUrl').'/login.php';
	}
}

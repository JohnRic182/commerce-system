<?php
/**
 * Class Admin_Controller_Endicia
 */
class Admin_Controller_Endicia extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/** @var DataAccess_EndiciaRepository */
	protected $repository = null;

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$this->checkCompanyAddressSettings();

		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('EndiciaShippingLabelsActive', 'EndiciaShippingLabelsAccountID', 'EndiciaShippingLabelsTestMode',
		'EndiciaShippingLabelsPassword');

		$settingsData = $settings->getByKeys($settingsVars);

		$endiciaForm = new Admin_Form_EndiciaForm();
		$form = $endiciaForm->getForm($settingsData);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'endicia'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['EndiciaShippingLabelsAccountID']) || trim($settingsData['EndiciaShippingLabelsAccountID']) == '')
			{
				$errors[] = array('field' => 'field-EndiciaShippingLabelsAccountID', 'message' => trans('endicia.account_id_required'));
			}
			if (!isset($settingsData['EndiciaShippingLabelsPassword']) || trim($settingsData['EndiciaShippingLabelsPassword']) == '')
			{
				$errors[] = array('field' => 'field-EndiciaShippingLabelsPassword', 'message' => trans('endicia.passphrase_required'));
			}

			if (!$request->request->has('EndiciaShippingLabelsTestMode'))
			{
				$settingsData['EndiciaShippingLabelsTestMode'] = 'No';
			}

			$endicia = new Shipping_Endicia($settingsData['EndiciaShippingLabelsAccountID'], $settingsData['EndiciaShippingLabelsPassword'], $settingsData['EndiciaShippingLabelsTestMode']);

			$statusResponse = $endicia->GetEndiciaAccountStatus();

			$statusCode = $statusResponse['Status'];
			if ($statusCode != '0')
			{
				if ($statusCode == '2011')
				{
					//force passphrase reset
				}
				else if ($statusCode == '1001' || $statusCode == '12507')
				{
					//invalid account id
					if (empty($settingsData['EndiciaShippingLabelsAccountID']))
					{
						$errors['EndiciaShippingLabelsAccountID'] = array(trans('endicia.account_id_required'));
					}

					if (empty($settingsData['EndiciaShippingLabelsPassword']))
					{
						$errors['EndiciaShippingLabelsPassword'] = array(trans('endicia.passphrase_required'));
					}
				}
				else
				{
					//$errors[] = array($statusCode.': '.$statusResponse['ErrorMessage']);
					Admin_Flash::setMessage($statusCode.': '.$statusResponse['ErrorMessage'], Admin_Flash::TYPE_ERROR);
				}
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'endicia'));
			}
			else
			{
				$form->bind($settingsData);
				$form->bindErrors($errors);
			}
		}

		if (!isset($endicia))
		{
			$endicia = $this->getEndiciaInstance();
		}

		if ($settings->get('EndiciaShippingLabelsActive') == 'Yes')
		{
			$endicia_balance = $endicia->GetEndiciaAccountBalance();
		}
		else
		{
			$endicia_balance = '';
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9016');

		$adminView->assign('nonce', Nonce::create('update_settings'));
		$adminView->assign('form', $form);

		$adminView->assign('refundForm', $this->getRefundForm($settingsData));
		$adminView->assign('refundNonce', Nonce::create('endicia_refund'));

		$adminView->assign('addFundsForm', $this->getAddFundsForm($settingsData));
		$adminView->assign('addNonce', Nonce::create('endicia_add_funds'));

		$adminView->assign('changePassphraseForm', $this->getChangePasswordForm($settingsData));
		$adminView->assign('passphraseNonce', Nonce::create('endicia_change_passphrase'));

		$adminView->assign('currentBalance', $endicia_balance);

		$adminView->assign('body', 'templates/pages/endicia/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Activate form action
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('EndiciaShippingLabelsActive', 'EndiciaShippingLabelsAccountID', 'EndiciaShippingLabelsTestMode',
			'EndiciaShippingLabelsPassword');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['EndiciaShippingLabelsAccountID']) || trim($settingsData['EndiciaShippingLabelsAccountID']) == '')
			{
				$errors[] = array('field' => '.field-EndiciaShippingLabelsAccountID', 'message' => trans('endicia.account_id_required'));
			}
			if (!isset($settingsData['EndiciaShippingLabelsPassword']) || trim($settingsData['EndiciaShippingLabelsPassword']) == '')
			{
				$errors[] = array('field' => '.field-EndiciaShippingLabelsPassword', 'message' => trans('endicia.passphrase_required'));
			}

			if (!$request->request->has('EndiciaShippingLabelsTestMode'))
			{
				$settingsData['EndiciaShippingLabelsTestMode'] = 'No';
			}

			$endicia = new Shipping_Endicia($settingsData['EndiciaShippingLabelsAccountID'], $settingsData['EndiciaShippingLabelsPassword'], $settingsData['EndiciaShippingLabelsTestMode']);

			$statusResponse = $endicia->GetEndiciaAccountStatus();

			$statusCode = $statusResponse['Status'];
			if ($statusCode != '0')
			{
				if ($statusCode == '2011')
				{
					// force passphrase reset
				}
				else if ($statusCode == '1001' || $statusCode == '12507')
				{
					//invalid account id
					if (empty($settingsData['EndiciaShippingLabelsAccountID']))
					{
						$errors[] = array('field' => '.field-EndiciaShippingLabelsAccountID', 'message' => trans('endicia.account_id_required'));
					}

					if (empty($settingsData['EndiciaShippingLabelsPassword']))
					{
						$errors[] = array('field' => '.field-EndiciaShippingLabelsPassword', 'message' => trans('endicia.passphrase_required'));
					}

				}
				else
				{
					$errors[] = array('field' => '', 'message' => $statusCode.': '.$statusResponse['ErrorMessage']);
				}
			}

			if (count($errors) < 1)
			{
				$settingsData['EndiciaShippingLabelsActive'] = 'Yes';

				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('endicia');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$endiciaForm = new Admin_Form_EndiciaForm();
		$form = $endiciaForm->getEndiciaActivateForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('EndiciaShippingLabelsActive' => 'No'));
	}

	public function requestRefundAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'endicia_refund', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			else
			{
				$errors = array();

				$picNumber = trim($request->request->get('pic_number'));

				if ($picNumber == '')
				{
					$errors[] = array('field' => '.field-pic_number', 'message' => trans('endicia.pic_number_required'));
				}

				if (count($errors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $errors,
					));
					return;
				}
				else
				{
					$endicia = $this->getEndiciaInstance();

					$refundStatus = $endicia->RequestEndiciaPostageRefund($picNumber, '', '');
					if ($refundStatus)
					{
						$db = $this->getDb();
						$db->query('DELETE FROM '.DB_PREFIX.'endicia_shippinglabels WHERE pic_number =\''.$db->escape($picNumber).'\'');
					}

					$status = $refundStatus['status'] ? 1 : 0;
					if ($status == 1) Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

					$this->renderJson(array(
						'status' => $status,
						'message' => $status ? '' : trans('endicia.refund_failed').': '.$refundStatus['status_message']
					));
				}
			}
		}
	}

	/**
	 * @param array $settingsData
	 * @return mixed
	 */
	protected function getRefundForm(array $settingsData)
	{
		$endiciaForm = new Admin_Form_EndiciaForm();
		return $endiciaForm->getEndiciaRefundForm();
	}

	/**
	 * Add funds
	 */
	public function addFundsAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'endicia_add_funds', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			else
			{
				$errors = array();

				$passphrase = trim($request->request->get('addfunds_passphrase'));
				$amount = floatval($request->request->get('addfunds_amount'));

				if ($passphrase == '')
				{
					$errors[] = array('field' => '.field-addfunds_passphrase', 'message' => trans('endicia.addfunds_passphrase_required'));
				}
				else if ($passphrase != $settings->get('EndiciaShippingLabelsPassword'))
				{
					$errors[] = array('field' => '.field-addfunds_passphrase', 'message' => trans('endicia.addfunds_invalid_passphrase'));
				}

				if ($amount <= 0)
				{
					$errors[] = array('field' => '.field-addfunds_amount', 'message' => trans('endicia.addfunds_amount_required'));
				}

				if (count($errors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $errors,
					));
					return;
				}
				else
				{
					$endicia = $this->getEndiciaInstance();

					$response = $endicia->AddFundsToEndiciaAccount($amount);

					if (isset($response['Status']) && $response['Status'] == 0)
					{
						$db = $this->getDb();

						$db->reset();
						$db->assignStr('request_id', $response['RequestID']);
						$db->assignStr('serial_number', $response['CertifiedIntermediary']['SerialNumber']);
						$db->assignStr('postage_balance', $response['CertifiedIntermediary']['PostageBalance']);
						$db->assignStr('ascending_balance', $response['CertifiedIntermediary']['AscendingBalance']);
						$db->assignStr('device_id', $response['CertifiedIntermediary']['DeviceID']);
						$db->assignStr('request_status', $response['Status']);
						$db->insert(DB_PREFIX . 'endicia_recreditrequests');

						Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

						$this->renderJson(array(
							'status' => 1,
							'new_balance' => $response['CertifiedIntermediary']['PostageBalance']
						));
					}
					else
					{
						if (isset($response['ErrorMessage']))
						{
							$message = $response['ErrorMessage'];
						}
						else
						{
							$message = trans('encidia.error_addfunds');
						}

						$this->renderJson(array(
							'status' => 0,
							'message' => $message
						));
					}
				}
			}
		}
	}

	protected function getAddFundsForm(array $settingsData)
	{
		$endiciaForm = new Admin_Form_EndiciaForm();
		return $endiciaForm->getEndiciaAddFundsForm();
	}

	public function changePasswordAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'endicia_change_passphrase', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			else
			{
				$errors = array();

				$currentPassphrase = trim($request->request->get('change_existing_passphrase'));
				$newPassphrase = trim($request->request->get('change_new_passphrase'));
				$newPassphrase2 = trim($request->request->get('change_new_passphrase2'));

				if ($currentPassphrase == '')
				{
					$errors[] = array('field' => '.field-change_existing_passphrase', 'message' => trans('endicia.current_passphrase_required'));
				}
				else if ($currentPassphrase != $settings->get('EndiciaShippingLabelsPassword'))
				{
					$errors[] = array('field' => '.field-change_existing_passphrase', 'message' => trans('endicia.addfunds_invalid_passphrase'));
				}

				if ($newPassphrase == '')
				{
					$errors[] = array('field' => '.field-change_existing_passphrase', 'message' => trans('endicia.current_passphrase_required'));
				}

				if ($newPassphrase2 == '')
				{
					$errors[] = array('field' => '.field-change_existing_passphrase', 'message' => trans('endicia.confirm_new_passphrase_required'));
				}
				else if ($newPassphrase != '' && $newPassphrase != $newPassphrase2)
				{
					$errors[] = array('field' => '.field-change_existing_passphrase', 'message' => trans('endicia.confirm_new_passphrase_not_match'));
				}

				if (count($errors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $errors,
					));
					return;
				}
				else
				{
					$endicia = $this->getEndiciaInstance();

					$accountStatus = $endicia->GetEndiciaAccountStatus();
					if ($accountStatus['Status'] == '0' || $accountStatus['Status'] == '2011')
					{
						$passwordChangeStatus = $endicia->ChangeEndiciaAccountPassword($newPassphrase);
						$didPasswordChange = $passwordChangeStatus['Status'] == '0';

						if ($didPasswordChange)
						{
							$settingsData = array(
								'EndiciaShippingLabelsActive' => 'Yes',
								'EndiciaShippingLabelsPassword' => $newPassphrase,
							);

							$settings->persist($settingsData);

							Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
							$this->renderJson(array(
								'status' => 1,
							));
						}
						else
						{
							$statusCode = $passwordChangeStatus['Status'];
							$errorMessage = trans('endicia.passphrase_invalid');
							if ($statusCode == '12101' || $statusCode == '12507')
							{
								$errorMessage = trans('endicia.credentials_invalid');
							}
							else if ($statusCode == '12669')
							{
								$errorMessage = trans('endicia.passphrase_must_not_match_last_4');
							}

							$this->renderJson(array(
								'status' => 0,
								'message' => $errorMessage
							));
						}
					}
					else
					{
						$this->renderJson(array(
							'status' => 0,
							'message' => $accountStatus['ErrorMessage']
						));
					}
				}
			}
		}
	}

	protected function getChangePasswordForm(array $settingsData)
	{
		$endiciaForm = new Admin_Form_EndiciaForm();
		return $endiciaForm->getEndiciaChangePasswordForm();
	}

	protected function getEndiciaInstance()
	{
		$settings = $this->getSettings();
		return new Shipping_Endicia($settings->get('EndiciaShippingLabelsAccountID'), $settings->get('EndiciaShippingLabelsPassword'), $settings->get('EndiciaShippingLabelsTestMode'));
	}

	/**
	 * Check are company settings ready
	 */
	protected function checkCompanyAddressSettings()
	{
		$settings = $this->getSettings();

		if (
			$settings->get('ShippingOriginAddressLine1') == '' || $settings->get('ShippingOriginCity') == 'City' ||
			$settings->get('ShippingOriginState') == '0' || $settings->get('ShippingOriginState') == '' ||
			$settings->get('ShippingOriginZip') == '-' || $settings->get('ShippingOriginZip') == ''
		)
		{
			Admin_Flash::setMessage(
				'To create a label through Endicia, please make sure that you have filled in all required fields under <a href="admin.php?p=shipping&mode=settings">Shipping Origin Address</a>. '.
				'Please note: Endicia may not work with P.O. Box addresses',
				Admin_Flash::TYPE_ERROR
			);
		}
	}
	
	public function addFundsToAccountAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();
		$result = new StdClass();

		$endicia = new Shipping_Endicia($settings->get('EndiciaShippingLabelsAccountID'), $settings->get('EndiciaShippingLabelsPassword'), $settings->get('EndiciaShippingLabelsTestMode'));
	
		$password = $endicia->getEndiciaAccountPassword();
		
		$passconfirm = $request->get('passconfirm', '');
		$amount = $request->get('amount_to_add', 0);

		if (($passconfirm == $password) && ($amount > '0'))
		{
			$result->result = 1;
			$response = $endicia->AddFundsToEndiciaAccount($amount);
			$getPostageStatusCode = $response['Status'];
			$request_id = isset($response['RequestID']) ? $response['RequestID'] : '';
			$certifiedi = $response['CertifiedIntermediary'];
			$serial_number = $certifiedi['SerialNumber'];
			$postage_balance = $certifiedi['PostageBalance'];
			$ascending_balance = $certifiedi['AscendingBalance'];
			$device_id = isset($certifiedi['DeviceID']) ? $certifiedi['DeviceID'] : '';
	
			$repository = $this->getDataRepository();
			$repository->addCreditRequest($request_id, $serial_number, $postage_balance, $ascending_balance, $device_id, $getPostageStatusCode);
	
			if($getPostageStatusCode == '0')
			{
				$result->result = 1;
				$result->newbal = $postage_balance;
				$result->serial = $serial_number;
				$result->requestid = $request_id;
				$result->device = $device_id;
			}
			else
			{
				$result->result = 2;
				$result->message = $response['ErrorMessage'];
			}
		}		
		else
		{
			$result->result = 0;
		}
		$this->renderJson($result);
	}
	
	public function validateAccountInfoAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();
		$result = new StdClass();

		$action = $request->get('action', '');
	
		if ($action == 'passphrase_reset')
		{
			$passwordconfirm = $request->get('changepwpassword', '');
			$accountconfirm = $request->get('EndiciaShippingLabelsAccountID', '');
			$isaccounttest = $request->get('EndiciaShippingLabelsTestMode', '');
	
			$endicia = new Shipping_Endicia($accountconfirm, $passwordconfirm, $isaccounttest);
	
			$accountstatus = $endicia->GetEndiciaAccountStatus();
	
			if ($accountstatus['Status'] == '0' || $accountstatus['Status'] == '2011')
			{
				$result->result = '1';
				$result->extResult = '';
	
				$newpass = $request->get('changepwnewpassword1', '');
				$newpassconfirm = $request->get('changepwnewpassword1', '');
	
				$did_endicia_password_change = false;
	
				if ($newpass == $newpassconfirm)
				{
					$passwordchangestatus = $endicia->ChangeEndiciaAccountPassword($newpass);
					$did_endicia_password_change = $passwordchangestatus['Status'] == '0';
				}
	
				if ($did_endicia_password_change)
				{
					if ($did_endicia_password_change)
					{
						$settings->set('EndiciaShippingLabelsAccountID', $accountconfirm);
						$settings->set('EndiciaShippingLabelsTestMode', $isaccounttest);
						$settings->set('EndiciaShippingLabelsActive', 'Yes');
						$settings->set('EndiciaShippingLabelsPassword', $newpass);
						
						//update settings
						$repository = $this->getDataRepository();
						$repository->updateEndiciaShippingLabelSettings($newpass, $accountconfirm, $isaccounttest);
	
						$result->message = "Your Endicia Account Passphrase has been changed successfully.";
					}
				}
				else
				{
					$result->result = '0';
					$result->extResult = $passwordchangestatus['Status'];
					$result->message = $passwordchangestatus['ErrorMessage'];
				}
			}
			else
			{
				$result->result = '0';
				$result->extResult = $accountstatus['Status'];
			}
		}
		else if ($action == 'update_settings')
		{
			$passwordconfirm = $request->get('EndiciaShippingLabelsPassword', '');
			$accountconfirm = $request->get('EndiciaShippingLabelsAccountID', '');
			$isaccounttest = $request->get('EndiciaShippingLabelsTestMode', '');
	
			$endicia = new Shipping_Endicia($accountconfirm, $passwordconfirm, $isaccounttest);
	
			$accountstatus = $endicia->GetEndiciaAccountStatus();
	
			if ($accountstatus['Status'] == '0')
			{
				$result->result = '1';
				$result->extResult = '';
			}
			else
			{
				$result->result = '0';
				$result->extResult = $accountstatus['Status'];
				$result->message = $accountstatus['ErrorMessage'];
			}
		}
		else
		{
			$endicia3 = new Shipping_Endicia($settings->get('EndiciaShippingLabelsAccountID'), $settings->get('EndiciaShippingLabelsPassword'), $settings->get('EndiciaShippingLabelsTestMode'));
			$password = $endicia3->getEndiciaAccountPassword();
	
			$vaction2 = $request->get('vaction2', '0');
			$vaction3 = $request->get('vaction3', '0');
	
			$passwordconfirm = $request->get('EndiciaShippingLabelsPassword', '');
			$accountconfirm = $request->get('EndiciaShippingLabelsAccountID', '');
			$isaccounttest = $request->get('EndiciaShippingLabelsTestMode', '');
	
			$endicia = new Shipping_Endicia($accountconfirm, $passwordconfirm, $isaccounttest);
	
			if (($passwordconfirm == $password) || ($password  == ''))
			{
				$accountstatus = $endicia->GetEndiciaAccountStatus();
	
				if ($accountstatus['Status'] == '0')
				{
					$result->result = '1';
				}
				else
				{
					$result->result = '0';
				}
			}
			else
			{
				$result->result = '-1';
			}
	
			if ($vaction2 == '1')
			{
				$passwordconfirm = $request->get('addfundspassword', $passwordconfirm);
	
				if ($password == $passwordconfirm)
				{
					$result->result = 1;
				}
				else
				{
					$result->result = $passwordconfirm;
				}
			}
	
			if ($vaction3 == '1')
			{
				$passwordconfirm = $request->get('changepwpassword', $passwordconfirm);
	
				if ($password == $passwordconfirm)
				{
					$result->result = 1;
				}
	
				else
				{
					$result->result = $passwordconfirm;
				}
			}
		}
	
		$this->renderJson($result);	
	}
	
	public function viewLabelAction()
	{
		$request = $this->getRequest();
		$f = $request->get('f', '');
		$repository = $this->getDataRepository();
		if ($repository->getEndiciaShippingLabel($f) && is_file($f))
		{
			header('Content-type: image/gif');
			header("Expires: Mon, 1 Jan 2099 05:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
	
			header('Content-Length: '.filesize($f).' bytes');
			ob_clean();
			flush();
	
			readfile($f);
			die();
		}
		else
		{	
			header("HTTP/1.0 404 Not Found");
			ob_clean();
			flush();
			die();
		}
	}

	/**
	 * @return DataAccess_EndiciaRepository
	 */
	protected function getDataRepository()
	{
		if ($this->repository == null)
		{
			$this->repository = new DataAccess_EndiciaRepository($this->getDb());
		}

		return $this->repository;
	}
}
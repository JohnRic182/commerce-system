<?php

/**
 * Class Admin_Controller_ThumbnailGenerator
 */
class Admin_Controller_ThumbnailGenerator extends Admin_Controller
{
	/** @var Admin_Service_ThumbnailsGenerator $service */
	protected $service;

	protected $menuItem = array('primary' => 'design', 'secondary' => 'thumbs-generator');

	/**
	 * @throws Exception
	 */
	public function indexAction()
	{
		$adminView = $this->getView();
		$settings = $this->getSettings();

		$thumbnailGeneratorForm = new Admin_Form_ThumbnailGeneratorForm();
		$adminView->assign('form', $thumbnailGeneratorForm->getForm($settings));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '7003');

		$adminView->assign('nonce', Nonce::create('thumbs_generator_scan'));

		$adminView->assign('body', 'templates/pages/thumbs-generator/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Scan
	 */
	public function scanAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost())
		{
			$this->redirect('thumbs_generator');
			return;
		}

		if (!Nonce::verify($request->request->get('nonce'), 'Admin_Service_ThumbnailsGeneratorthumbs_generator_scan', false))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('thumbs_generator');
			return;
		}

		$service = $this->getService();

		$generateType = $request->request->get('generateType');
		$generateTarget = $request->request->get('generateTarget');
		$optimizePrimary = $request->request->get('optimizePrimary');
		$totalCount = $service->scanImages($generateType, $generateTarget, $optimizePrimary);

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '7003');

		$adminView->assign('nonce', $request->request->get('nonce'));
		$adminView->assign('generateType', $generateType);
		$adminView->assign('generateTarget', $generateTarget);
		$adminView->assign('optimizePrimary', $optimizePrimary);
		$adminView->assign('totalCount', $totalCount);

		$adminView->assign('body', 'templates/pages/thumbs-generator/scan-results.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Generate
	 */
	public function generateAction()
	{
		$request = $this->getRequest();

		if ($request->get('__ajax', false))
		{
			if (!$request->isPost())
			{
				$this->renderJson(array(
					'status' => 0,
				));
				return;
			}

			if (!Nonce::verify($request->request->get('nonce'), 'thumbs_generator_scan', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}

			$service = $this->getService();
			$result = $service->generateThumbs();

			if (count($result['imageErrors']))
			{
				foreach($result['imageErrors'] as $imageError)
				{
					Admin_Flash::setMessage($imageError, Admin_Flash::TYPE_ERROR);
				}
			}

			if ($result['progress'] > 99.99)
			{
				Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
			}

			$this->renderJson(array(
				'status' => 1,
				'result' => $result,
			));
		}
		else
		{
			Admin_Flash::setMessage('common.no_direct_access_interrupted', Admin_Flash::TYPE_ERROR);
			$this->redirect('thumbs_generator');
		}
	}

	/**
	 * Cleanup
	 */
	public function cleanupImagesAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost())
		{
			$this->renderJson(array(
				'status' => 0,
			));
			return;
		}

		$service = $this->getService();
		$count = $service->cleanupImages();

		Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

		$this->renderJson(array(
			'status' => 1,
			'count' => $count,
		));
	}

	/**
	 * Sync
	 */
	public function syncSecondaryImagesAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost())
		{
			$this->renderJson(array(
				'status' => 0,
			));
			return;
		}

		$service = $this->getService();
		$count = $service->syncSecondaryImages();

		Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

		$this->renderJson(array(
			'status' => 1,
			'count' => $count,
		));
	}

	/**
	 * @return Admin_Service_ThumbnailsGenerator
	 */
	protected function getService()
	{
		if ($this->service == null)
		{
			$this->service = new Admin_Service_ThumbnailsGenerator($this->getDb(), $this->getSession());
		}

		return $this->service;
	}
}
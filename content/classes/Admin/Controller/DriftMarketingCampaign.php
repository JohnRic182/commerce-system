<?php

/**
 * Class Admin_Controller_DriftMarketingCampaign
 */
class Admin_Controller_DriftMarketingCampaign extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/drift-marketing/';

	protected $driftMarketingCampaignRepository = null;

	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'drift-marketing');
	
	/**
	 * Add new campaign
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$repository = $this->getDriftMarketingCampaignRepository();

		$driftMarketingCampaignForm = new Admin_Form_DriftMarketingCampaignForm();

		$defaults = $repository->getDefaults($mode, $this->getSettings()->get('CompanyName'));

		$form = $driftMarketingCampaignForm->getForm($defaults, $mode, null, $repository->getTimeFrameOptions());

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('drift_marketing');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
				{
					$id = $repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('drift_marketing', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5007');
		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('drift_campaign_add'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * List campaigns
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$rep = $this->getDriftMarketingCampaignRepository();

		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'name');

		$itemsCount = $rep->getCount();
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('campaigns', $rep->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy));
		}
		else
		{
			$adminView->assign('campaigns', null);
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5006');

		$adminView->assign('delete_nonce', Nonce::create('campaign_delete'));
		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update campaign
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();
		$repository = $this->getDriftMarketingCampaignRepository();

		$id = intval($request->get('id'));

		$campaign = $repository->getById($id);

		$template = $request->get('template');

		if (!$campaign)
		{
			Admin_Flash::setMessage('Cannot find email campaign by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('drift_marketing');
		}

		$driftMarketingCampaignForm = new Admin_Form_DriftMarketingCampaignForm();

		$defaults = $repository->getDefaults($mode, $this->getSettings()->get('CompanyName'));
		$form = $driftMarketingCampaignForm->getForm($campaign, $mode, $id, $repository->getTimeFrameOptions());

		if (trim($template) == 'restore')
		{
			$form = $driftMarketingCampaignForm->getForm($defaults, $mode, $id, $repository->getTimeFrameOptions());
		}

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('drift_marketing');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('drift_marketing', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5008');
		$adminView->assign('title', $campaign['name']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('campaign_update'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete email campaigns
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		$repository = $this->getDriftMarketingCampaignRepository();

		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('drift_marketing');
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid campaign id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete(array($id));
				Admin_Flash::setMessage('Email campaign has been deleted');
				$this->redirect('drift_marketing');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one email campaign to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('drift_marketing');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('Selected email campaigns have been deleted');
				$this->redirect('drift_marketing');
			}
		}
		else if ($deleteMode == 'search')
		{
			$repository->deleteBySearchParams();
			Admin_Flash::setMessage('All email campaigns have been deleted');
			$this->redirect('drift_marketing');
		}
	}

	/**
	 * Get repository
	 */
	protected function getDriftMarketingCampaignRepository()
	{
		if (is_null($this->driftMarketingCampaignRepository))
		{
			$this->driftMarketingCampaignRepository = new DataAccess_DriftMarketingCampaignRepository($this->getDb());
		}

		return $this->driftMarketingCampaignRepository;
	}
}
<?php

/**
 * Class Admin_Controller_CustomFormData
 */
class Admin_Controller_CustomFormData extends Admin_Controller
{
	/** @var DataAccess_CustomFormRepository $customFormRepository */
	protected $customFormRepository = null;

	/** @var DataAccess_CustomFieldRepository $customFieldRepository */
	protected $customFieldRepository = null;

	protected $searchParamsDefaults = array('field_id' => 'all', 'field_name' => '', 'search_str' => '', 'orderDir' => 'asc', 'exact_match' => 0);

	protected $menuItem = array('primary' => 'content', 'secondary' => 'custom-forms');

	/**
	 * List customForms
	 */
	public function listAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();
		$adminView = $this->getView();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('customFormsSearchParams', array()); else $session->set('customFormsSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('customFormsSearchOrderBy', 'date_created'); else $session->set('customFormsSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('customFormsSearchOrderDir', 'asc'); else $session->set('customFormsSearchOrderDir', $orderDir);

		$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());
		$searchParams['field_name'] = !empty($searchParams['field_name']) ? $searchParams['field_name'] : $orderBy;
		$searchParams['orderDir'] = !empty($orderDir) ? $orderDir : $searchParams['orderDir'];

		$customFormRepository = $this->getCustomFormRepository();
		$customFieldsRepository = $this->getCustomFieldRepository();

		$formId = intval($request->get('form_id'));
		$customFormModel = $customFormRepository->getById($formId);

		if (!$customFormModel)
		{
			Admin_Flash::setMessage('Cannot find custom form by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('custom_forms');
		}

		$customFormFields = $customFieldsRepository->getListByFormId($formId);

		/**
		 * Search form
		 */
		$customFormDataForm = new Admin_Form_CustomFormDataForm();
		$searchFormData = array('customFormFields' => $customFormFields, 'searchParams' => $searchParams);

		$adminView->assign('searchParams', $searchParams);
		$adminView->assign('searchForm', $customFormDataForm->getForm($searchFormData));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3012');

		$adminView->assign('customForm', $customFormModel->toArray());
		$adminView->assign('customFormFields', $customFormFields);

		$itemsCount = $customFormRepository->getFormPostsCount($formId, $searchParams);

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('customFormPosts', $customFormRepository->getCustomFormPostsList($formId, $paginator->sqlOffset, $paginator->itemsPerPage, $searchParams));
		}
		else
		{
			$adminView->assign('customFormData', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('custom_form_data_delete'));

		$adminView->assign('body', 'templates/pages/custom-form/data.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete customForms
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$formId = intval($request->get('form_id'));

		$customFormRepository = $this->getCustomFormRepository();

		$customFormModel = $customFormRepository->getById($formId);

		if (!$customFormModel)
		{
			Admin_Flash::setMessage('Cannot find custom form by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('custom_forms');
		}

		if (!Nonce::verify($request->get('nonce'), 'custom_form_data_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
		}
		else
		{
			$deleteMode = $request->get('deleteMode');

			/**
			 * Do delete
			 */
			if ($deleteMode == 'single')
			{
				$postId = $request->get('post_id', 0);

				if ($postId < 1)
				{
					Admin_Flash::setMessage('Invalid custom form record id', Admin_Flash::TYPE_ERROR);
				}
				else
				{
					$customFormRepository->deleteFormPosts($formId, $postId);
					Admin_Flash::setMessage('Custom form record has been deleted');
				}
			}
			else if ($deleteMode == 'selected')
			{
				$postIds = explode(',', $request->get('post_ids', ''));

				if (count($postIds) == 0)
				{
					Admin_Flash::setMessage('Please select at least one post to delete', Admin_Flash::TYPE_ERROR);
				}
				else
				{
					$customFormRepository->deleteFormPosts($formId, $postIds);
					Admin_Flash::setMessage('Selected form records have been deleted');
				}
			}
			else if ($deleteMode == 'search')
			{
				/** @var Framework_Session $session */
				$session = $this->getSession();
				$searchParams = $session->get('customFormDataSearchParams', array());
				$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

				$customFormRepository->deleteFormPostsBySearchParams($formId, $searchParams);
				Admin_Flash::setMessage('Selected custom form records have been deleted');
			}
			else
			{
				Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
			}
		}

		$this->redirect('custom_form_data', array('form_id' => $formId));
	}

	/**
	 * Export custom form data
	 */
	public function exportAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();

		$formId = intval($request->get('form_id'));
		$customFormRepository = $this->getCustomFormRepository();

		$customFormModel = $customFormRepository->getById($formId);

		if (!$customFormModel)
		{
			Admin_Flash::setMessage('Cannot find custom form by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('custom_forms');
		}

		$customFieldsRepository = $this->getCustomFieldRepository();

		// get sort order
		$orderBy = $request->get('orderBy', null);

		if ($orderBy === null)
		{
			$orderBy = $session->get('customFormDataOrderBy', 'date_desc');
		}
		else
		{
			$session->set('customFormDataOrderBy', $orderBy);
		}

		$postsList = false;

		$exportMode = $request->get('exportMode');

		if ($exportMode == 'selected')
		{
			$postIds = explode(',', $request->get('post_ids', ''));

			if (count($postIds) == 0)
			{
				Admin_Flash::setMessage('Please select at least one post to export', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$postsList = $customFormRepository->getFormPostsList($formId, null, null, array('post_ids' => $postIds), $orderBy);
			}
		}
		else if ($exportMode == 'search')
		{
			/** @var Framework_Session $session */
			$session = $this->getSession();
			$searchParams = $session->get('customFormDataSearchParams', array());

			$postsList = $customFormRepository->getFormPostsList($formId, null, null, $searchParams, $orderBy);
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		if ($postsList)
		{
			$fieldsList = $customFieldsRepository->getListByFormId($formId);

			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=" . $customFormModel->getFormId() . "-form-data.csv");
			header("Pragma: no-cache");
			header("Expires: 0");

			$c = 0;
			foreach ($fieldsList as $fieldData)
			{
				if ($c > 0) echo ','; $c++;
				echo $fieldData['field_name'];
			}
			echo "\n";

			foreach ($postsList as $postRecord)
			{
				$c = 0;
				foreach ($fieldsList as $fieldData)
				{
					if ($c > 0) echo ','; $c++;

					$value = isset($postRecord['fields'][$fieldData['field_id']]) ? $postRecord['fields'][$fieldData['field_id']] : '';
					$value = str_replace(array('"'), array('\"'), $value);

					echo '"' . $value . '"';
				}
				echo "\n";
			}
		}
		else
		{
			$this->redirect('custom_form_data', array('form_id' => $formId));
		}
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CustomFormRepository
	 */
	protected function getCustomFormRepository()
	{
		if (is_null($this->customFormRepository))
		{
			$this->customFormRepository = new DataAccess_CustomFormRepository($this->getDb());
		}

		return $this->customFormRepository;
	}

	/**
	 * Get custom field repository
	 */
	protected function getCustomFieldRepository()
	{
		if (is_null($this->customFieldRepository))
		{
			$this->customFieldRepository = new DataAccess_CustomFieldRepository($this->getDb());
		}

		return $this->customFieldRepository;
	}
}

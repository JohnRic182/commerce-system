<?php

/**
 * Class Admin_Controller_GoogleTools
 */
class Admin_Controller_GoogleTools extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/google-tools/';

	protected $googleToolsRepository = null;

	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'google-tools');

	/**
	 * Edit google tools settings
	 */
	public function editSettingsAction()
	{
		$request = $this->getRequest();
		$repository = $this->getGoogleToolsRepository();
		$googleToolsForm = new Admin_Form_GoogleToolsForm();

		$defaults = $repository->getDefaults();
		$defaultsNames = array_keys($defaults);

		$data = $repository->getSettings($defaultsNames);
		$form = $googleToolsForm->getForm($data);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($request->request->get('nonce'), 'google_tools_settings_edit'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('google_marketing');
			}
			else
			{
				if (($validationErrors = $repository->getValidationSettingsErrors($formData)) == false)
				{
					$repository->persistGoogleToolsSettings($formData);
					$this->getSettings()->save($formData, $defaultsNames);

					Admin_Flash::setMessage('common.success');
					$this->redirect('google_marketing');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5005');
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('google_tools_settings_edit'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Generate site map action
	 */
	public function generateSiteMapAction()
	{
		$request = $this->getRequest();
		$repository = $this->getGoogleToolsRepository();
		$settings = $this->getSettings();
		$googleToolsForm = new Admin_Form_GoogleToolsForm();

		$defaults = $repository->getDefaults();
		$defaultsNames = array_keys($defaults);

		$data = $repository->getSettings($defaultsNames);
		$form = $googleToolsForm->getForm($data);

		if ($repository->generateSiteMap($settings->getAll()))
		{
			Admin_Flash::setMessage(trans('marketing.generate_sitemap_success', array('$sitemap_url' => $settings->get('GlobalHttpUrl').'/sitemap.xml.gz')));
			$this->redirect('google_marketing', array());
		}
		else
		{
			$formData = array_merge($repository->getDefaults(), $request->request->all());
			$form->bind($formData);

			Admin_Flash::setMessage('Failed to generate site map'.$this->formatValidationErrors($repository->getErrors()), Admin_Flash::TYPE_ERROR);
			$this->redirect('google_marketing', array());
		}
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_GoogleToolsRepository
	 */
	protected function getGoogleToolsRepository()
	{
		if (is_null($this->googleToolsRepository))
		{
			$this->googleToolsRepository = new DataAccess_GoogleToolsRepository($this->getDb(), $this->getSettings());
		}

		return $this->googleToolsRepository;
	}
}
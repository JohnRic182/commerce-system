<?php

/**
 * Class Admin_Controller_Testimonial
 */
class Admin_Controller_Testimonial extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/testimonial/';

	/** @var DataAccess_TestimonialRepository  */
	protected $Repository = null;

	protected $menuItem = array('primary' => 'content', 'secondary' => 'testimonials');

	/** @var array */
	protected $searchParamsDefaults = array('search_str' => '', 'logic' => 'AND');

	/**
	 * Add new testimonial
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$TPForm = new Admin_Form_TestimonialForm();

		$defaults = $repository->getDefaults($mode);

		$form = $TPForm->getForm($defaults, $mode);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('testimonials');
			}
			else{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
				{
					$id = $repository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('testimonial', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6004');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('testimonial_add'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * List Testimonials
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$rep = $this->getDataRepository();

		$request = $this->getRequest();
		$session = $this->getSession();
		$settings_data = $rep->getAdvancedSettingsData();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('testimonialsSearchParams', array()); else $session->set('testimonialsSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('testimonialsSearchOrderBy', 'date_created'); else $session->set('testimonialsSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('testimonialsSearchOrderDir', 'desc'); else $session->set('testimonialsSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;



		if ($settings_data['testimonials_Enabled'] == 'Yes')
		{
			/**
			 * Get data from DB
			 */
			$itemsCount = $rep->getCount($searchParams, $searchParams['logic']);

			/**
			 * Show results
			 */
			$testimonialForm = new Admin_Form_TestimonialForm();
			$adminView->assign('searchForm', $testimonialForm->getTestimonialSearchForm($searchFormData));
			$adminView->assign('itemsCount', $itemsCount);

			if ($itemsCount)
			{
				$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
				$adminView->assign('paginator', $paginator);

				$adminView->assign('testimonials',
					$rep->getList(
						$paginator->sqlOffset,
						$paginator->itemsPerPage,
						$orderBy . '_' . $orderDir,
						$searchParams,
						't.*', $searchParams['logic']
					)
				);
			}
			else
			{
				$adminView->assign('testimonials', null);
			}
		}
		else
		{
			$adminView->assign('itemsCount', 0);
			$adminView->assign('testimonials', null, $orderBy);
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6003');

		$adminView->assign('delete_nonce', Nonce::create('currencies_delete'));
		$adminView->assign('update_statuses_nonce', Nonce::create('update_statuses_nonce'));

		$adminView->assign('testimonials_Enabled', $settings_data['testimonials_Enabled']);

		$adminView->assign('settingsForm', $this->getSettingsForm());
		$adminView->assign('settingsNonce', Nonce::create('settings_update'));

		$adminView->assign('averageRating', $rep->getAverageRating());

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update testimonial
	 */
	public function editAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$id = intval($request->get('id'));

		$testimonial = $repository->getById($id);

		if (!$testimonial)
		{
			Admin_Flash::setMessage('testimonials.not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('testimonials');
		}

		$TPForm = new Admin_Form_TestimonialForm();

		$defaults = $repository->getDefaults($mode);

		$form = $TPForm->getForm($testimonial, $mode, $id);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('testimonials');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('testimonial', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6005');
		$adminView->assign('title', $testimonial['name']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('testimonial_update'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	public function updateStatusAction()
	{
		$request = $this->getRequest();
		
		/** @var DataAccess_TestimonialRepository */
		$repository = $this->getDataRepository();
		
		$updateMode = $request->get('updateMode');
		$new_status = $request->get('new_status');

		/*
		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('testimonials');
			return;
		}
		*/

		if ($updateMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('testimonials.no_testimonials_selected', Admin_Flash::TYPE_ERROR);
				$this->redirect('testimonials');
			}
			else
			{
				$repository->updateStatus($ids, $new_status);
				Admin_Flash::setMessage('common.success');
				$this->redirect('testimonials');
			}
		}
		else if ($updateMode == 'search'){
		
			$repository->updateStatus("all", $new_status);
			Admin_Flash::setMessage('common.success');
			$this->redirect('testimonials');

		}
	}

	protected function getSettingsForm()
	{
		$repository = $this->getDataRepository();

		$data = $repository->getAdvancedSettingsData();

		$TRForm = new Admin_Form_TestimonialAdvancedSettingsForm();

		return $TRForm->getForm($data);
	}

	/**
	 * Delete testimonial
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TestimonialRepository */
		$repository = $this->getDataRepository();

		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('testimonials');
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid testimonial id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete(array($id));

				Admin_Flash::setMessage('Testimonial has been deleted');
				$this->redirect('testimonials');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one testimonial to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('testimonials');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('Selected testimonials have been deleted');
				$this->redirect('testimonials');
			}
		}
		else if ($deleteMode == 'search')
		{
			/** @var Framework_Session $session */
			$session = $this->getSession();
			$searchParams = array();

			$repository->deleteBySearchParams($searchParams);
			Admin_Flash::setMessage('Select testimonials have been deleted');
			$this->redirect('testimonials');
		}
	}

	/**
	 * Edit testimonials settings
	 */
	public function editAdvancedSettingsAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();
		$repository = $this->getDataRepository();

		/**
		 * Handle form post
		*/
		if ($request->isPost()){
			$formData = $request->request->all();
	
			if (!Nonce::verify($formData['nonce'], 'settings_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getValidationSettingsErrors($formData, $mode)) == false){
			
					$repository->persistTestimonialsSettings($formData);

					Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_TestimonialRepository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->Repository)){
			$this->Repository = new DataAccess_TestimonialRepository($this->getDb(), $this->getSettings()->getAll());
		}

		return $this->Repository;
	}
}

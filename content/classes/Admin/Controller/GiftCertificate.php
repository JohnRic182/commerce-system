<?php

/**
 * Class Admin_Controller_GiftCertificate
 */
class Admin_Controller_GiftCertificate extends Admin_Controller
{
	protected $giftCertificateRepository = null;
	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'gift-certificates');
	protected $searchParamsDefaults = array('balance' => 'all');

	/**
	 * List action
	 */
	public function listAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();

		$adminView = $this->getView();
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5009');

		$repository = $this->getGiftCertificateRepository();

		$orderBy = null;

		$searchParams = $request->get('searchParams', null);
		if ($searchParams === null)
		{
			$searchParams = $session->get('giftCertSearchParams', array());
		}
		else
		{
			$session->set('giftCertSearchParams', $searchParams);
		}

		$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$giftCertificateForm = new Admin_Form_GiftCertificateForm();
		$searchForm = $giftCertificateForm->getGiftCertificateSearchForm($searchParams);

		$adminView->assign('searchParams', $searchParams);

		$itemsCount = $repository->getCount($searchParams);
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('gift_certs', $repository->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy, $searchParams, $this->getSettings()->get('LocalizationDateTimeFormat')));
		}
		else
		{
			$adminView->assign('gift_certs', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('gift_cert_delete'));

		$adminView->assign('searchForm', $searchForm);

		$adminView->assign('settingsForm', $this->getSettingsForm());
		$adminView->assign('settingsNonce', Nonce::create('settings_update'));

		$adminView->assign('body', 'templates/pages/gift-cert/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add action
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$repository = $this->getGiftCertificateRepository();

		$giftCertForm = new Admin_Form_GiftCertificateForm();

		$defaults = $repository->getDefaults($mode);

		$form = $giftCertForm->getForm($defaults, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'gift_cert_add'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('gift_cert');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
				{
					if (!isset($formData['ordered_date']) || $formData['ordered_date'] === null)
					{
						$formData['ordered_date'] = date("Y-m-d H:i:s");
					}

					$formData['balance'] = $formData['gift_amount'];

					$id = $repository->persist($formData);

					if (isset($formData['send_email']) && $formData['send_email'] == 'Yes')
					{
						$formData['from_name'] = '';

						view()->initNotificationsSmarty();
						Notifications::emailGiftCertificate($formData);
					}

					Admin_Flash::setMessage('common.success');
					$this->redirect('gift_cert', array('mode' => self::MODE_UPDATE, 'id' => $id));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5010');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('gift_cert_add'));

		$adminView->assign('body', 'templates/pages/gift-cert/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update action
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getGiftCertificateRepository();

		$id = intval($request->get('id'));

		$giftCert = $repository->getGiftCertificateById($id);

		if (!$giftCert)
		{
			Admin_Flash::setMessage('Cannot find gift certificate by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('gift_cert');
		}

		$giftCertForm = new Admin_Form_GiftCertificateForm();

		$defaults = $repository->getDefaults($mode);

		$form = $giftCertForm->getForm($giftCert, $mode, $id);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			$formData['id'] = $id;
			unset($formData['gift_amount']);
			unset($formData['ordered_date']);
			unset($formData['created_date']);

			if (!Nonce::verify($formData['nonce'], 'gift_cert_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('gift_cert');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('gift_cert', array('mode' => self::MODE_UPDATE, 'id' => $id));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5011');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('gift_cert_update'));

		$adminView->assign('body', 'templates/pages/gift-cert/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete action
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'gift_cert_delete')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('gift_cert');
			return;
		}

		$repository = $this->getGiftCertificateRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid gift certificate id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete($id);
				Admin_Flash::setMessage('common.success');
				$this->redirect('gift_cert');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one gift certificate to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('gift_cert');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('common.success');
				$this->redirect('gift_cert');
			}
		}
		else if ($deleteMode == 'search')
		{
			/** @var Framework_Session $session */
			$session = $this->getSession();
			$searchParams = $session->get('giftCertSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());
			$repository->deleteBySearchParams($searchParams);
			Admin_Flash::setMessage('common.success');
			$this->redirect('gift_cert');
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);

			$this->redirect('gift_cert');
		}
	}

	protected function getSettingsForm()
	{
		$settings = $this->getSettings();

		$defaults = array('enable_gift_cert' => 'No');
		$data = $settings->getByKeys(array_keys($defaults));
		$giftCertificateForm = new Admin_Form_GiftCertificateForm();

		return $giftCertificateForm->getGiftCertificateSettingsForm($data);
	}

	/**
	 * Settings action
	 */
	public function settingsAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$defaults = array('enable_gift_cert' => 'No');

		$data = $settings->getByKeys(array_keys($defaults));

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'settings_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$formData = array();
				foreach ($data as $key => $temp)
				{
					$formData[$key] = trim($request->request->get($key, array_key_exists($key, $defaults) ? $defaults[$key] : ''));
				}
				if ($formData['enable_gift_cert'] == 'Yes' && $data['enable_gift_cert'] != $formData['enable_gift_cert'])
				{
					$repository = $this->getGiftCertificateRepository();
					$repository->checkGiftCertCategoryAndProduct();
				}
				$settings->persist($formData);
				Admin_Flash::setMessage('common.success');
				$this->renderJson(array(
					'status' => 1
				));
			}
		}
	}

	/**
	 * @return DataAccess_GiftCertificateRepository
	 */
	protected function getGiftCertificateRepository()
	{
		if (is_null($this->giftCertificateRepository))
		{
			$this->giftCertificateRepository = new DataAccess_GiftCertificateRepository($this->getDb(), $this->getSettings());
		}

		return $this->giftCertificateRepository;
	}
}
<?php

/**
 * Class Admin_Controller_StoreSettings
 */
class Admin_Controller_StoreSettings extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$defaults = array(
			'GlobalSiteName' => '',
			'GlobalNotificationEmail' => '',
			'GlobalSupportEmail' => '',
			//'GlobalHttpUrl' => '',
			//'GlobalHttpsUrl' => '',
			//'GlobalHttpCookieDomain' => '',
			//'GlobalHttpsCookieDomain' => '',
			'StoreClosed' => 'Yes',
			'ForceSslRedirection' => 'No',
			//'GlobalServerPath' => '',
			'StoreClosedMessage' => '',
			//'SessionSavePath' => '',
			'SecurityCookiesPrefix' => '',
			'SecurityUserTimeout' => '',
			'SecurityAccountBlocking' => 'NO',
			'SecurityAccountBlockingAttempts' => '',
			'SecurityAccountBlockingHours' => '',
			'SecurityAdminTimeOut' => '',
			'SecurityAdminIpRestriction' => 'Deny',
			'SecurityAdminIps' => '',
			'captchaMethod' => 'None',
			'captchaReCaptchaPublicKey' => '',
			'captchaReCaptchaPrivateKey' => '',
			'EmailSendmailEngine' => '',
			'EmailSendmailEolCharacter' => '',
			'EmailSMTPSecure' => '',
			'EmailSMTPServer' => '',
			'EmailSMTPPort' => '',
			'EmailSMTPLogin' => '',
			'EmailSMTPPassword' => '',
			'LocalizationDateTimeFormat' => '',
			'LocalizationUseSystemTimezone' => 'No',
			'LocalizationDefaultTimezone' => '',
			'LocalizationWeightUnits' => '',
			'LocalizationLengthUnits' => '',
			'LocalizationCurrencyDecimalSymbol' => '',
			'LocalizationCurrencySeparatingSymbol' => '',
		);

		$data = $settings->getByKeys(array_keys($defaults));
		$this->ensureTimezoneSettings();

		$storeSettingsForm = new Admin_Form_StoreSettingsForm();
		$form = $storeSettingsForm->getForm($data);

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('store_settings');
			}
			else
			{
				$validationErrors = array();
				$formData = array();
				foreach ($data as $key => $temp)
				{
					$formData[$key] = trim($request->request->get($key, array_key_exists($key, $defaults) ? $defaults[$key] : ''));

					/** @var core_Form_Field $field */
					$field = $storeSettingsForm->getBasicGroup()->get($key);

					if ($field && is_array($field) && isset($field['options']['required']) && $field['options']['required'] && $formData[$key] == '')
					{
						$validationErrors[$key] = array(trans('common.please_enter', array('field' => trans($field['options']['label']))));
					}

					if (($key == 'GlobalSupportEmail' || $key == 'GlobalNotificationEmail') && !isEmail($formData[$key]) && trim($formData[$key]) != '')
					{
						$validationErrors[$key] = array(trans('common.please_enter_valid_email'));
					}
				}

				if (count($validationErrors) == 0)
				{
					$settings->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('store_settings');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8014');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('settings_update'));

		$adminView->assign('body', 'templates/pages/settings/store-settings.html');
		$adminView->render('layouts/default');
	}

	protected function ensureTimezoneSettings()
	{
		global $db, $settings;

		Timezone::setApplicationDefaultTimezone($db, $settings);
	}
}
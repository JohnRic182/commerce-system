<?php

/**
 * Class Admin_Controller_CatalogSettings
 */
class Admin_Controller_CatalogSettings extends Admin_Controller
{
	/** @var array $menuItem */
	protected $menuItem = array('primary' => 'design', 'secondary' => 'catalog-settings');

	/**
	 * Update catalog settings
	 */
	public function updateAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_SettingsRepository $settings */
		$settings = $this->getSettings();

		$defaults = array(
			'CatalogShowNested' => 'NO',
			'CatalogHideEmptyCategories' => 'NO',
			'CatalogOptimizeImages' => 'NO',
			'CatalogHomeItemsOnPage' => 10,
			'CatalogHomeLink' => 'index.php?p=home',
			'CatalogDefaultSortOrder' => 'priority',
			'CatalogItemsOnPage' => 18,
			'CatalogDisplaySort' => 'NO',
			'CatalogDisplayItems' => 'NO',
			'CatalogCategoryImageSize' => 150,
			'CatalogCategoryImageType' => 'Square',
			'CatalogCategoryImageResizeRule' => 'Smart',
			'CatalogCategoryThumbAuto' => 'NO',
			'CatalogCategoryThumbSize' => 150,
			'CatalogCategoryThumbType' => 'Square',
			'CatalogCategoryThumbResizeRule' => 'Smart',
			'CatalogThumbSize' => 150,
			'CatalogSecondaryThumbSize' => 150,
			'CatalogThumbType' => 'Square',
			'CatalogThumbResizeRule' => 'Smart',
			'CatalogThumb2Size' => 300,
			'CatalogThumb2Type' => 'Square',
			'CatalogThumb2ResizeRule' => 'Smart',
			'ProductSecondaryThumbSize' => 300,
			'ProductSecondaryThumbType' => 'Square',
			'ProductSecondaryThumbResizeRule' => 'Smart',
			'CatalogManufacturerImageDisplay' => 'NO',
			'CatalogManufacturerImageSize' => 150,
			'CatalogManufacturerImageType' => 'Square',
			'CatalogManufacturerImageResizeRule' => 'Smart',
			'RecommendedProductsCount' => 6,
			'SiblingProductsCount' => 6,
			'CatalogEmailToFriend' => 'Disabled',
			'CatalogPriceRanges' => base64_encode(serialize(array(
				1 => array('min' => 0, 'max' => 0), 2 => array('min' => 0, 'max' => 0), 3 => array('min' => 0, 'max' => 0),
				4 => array('min' => 0, 'max' => 0), 5 => array('min' => 0, 'max' => 0), 6 => array('min' => 0, 'max' => 0),
				7 => array('min' => 0, 'max' => 0), 8 => array('min' => 0, 'max' => 0), 9 => array('min' => 0, 'max' => 0),
				10 => array('min' => 0, 'max' => 0),
			))),
			'CatalogUsePriceRanges' => 0,
			'ZoomOption' => 'imagelayover',
			'ZoomOptionSecondary' => 'imagelayover',
			'PredictiveSearchEnabled' => 0,
			'PredictiveSearchResultsCount' => 10,
			'PredictiveSearchInProductsDescription' => 0,
			'PredictiveSearchInCategories' => 0,
			'PredictiveSearchInManufacturers' => 0
		);

		$data = $settings->getByKeys(array_keys($defaults));
		$data['defaults'] = $defaults;

		$catalogSettingsForm = new Admin_Form_CatalogSettingsForm();
		$form = $catalogSettingsForm->getForm($data);

		/**
		 * Process request
		 */
		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'catalog_settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('design_catalog');
			}
			else
			{
				$formData = array_merge($defaults, $request->request->all());

				$priceRanges = array();
				for ($i = 1; $i<= 10; $i++)
				{
					$priceRanges[$i] = array('min' => $formData['min_price_range_'.$i], 'max' => $formData['max_price_range_'.$i]);
				}

				$formData['CatalogPriceRanges'] = base64_encode(serialize($priceRanges));

				if (!$request->request->has('EnableRecommendedProducts')) $formData['RecommendedProductsCount'] = 0;
				if (!$request->request->has('EnableSiblingProducts')) $formData['SiblingProductsCount'] = 0;

				if (($validationErrors = $this->getValidationErrors($formData)) == false)
				{
					$settings->persist($formData, array_keys($defaults));
					Admin_Flash::setMessage('common.success');
					$this->redirect('design_catalog');
				}
				else
				{
					// TODO: find better way to handle this
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '7002');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('catalog_settings_update'));

		$adminView->assign('body', 'templates/pages/settings/catalog-settings.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get validation errors
	 * @param $data
	 * @return array|bool
	 */
	public function getValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			if (isset($data['CatalogItemsOnPage']) && trim($data['CatalogItemsOnPage']) == '')
			{
				$errors['CatalogItemsOnPage'] = array('Products Per Page cannot be empty.');
			}

			if (isset($data['CatalogHomeItemsOnPage']) && trim($data['CatalogHomeItemsOnPage']) == '')
			{
				$errors['CatalogHomeItemsOnPage'] = array('Number Of Products Displayed On Home Page cannot be empty.');
			}

			if (isset($data['CatalogCategoryThumbSize']) && trim($data['CatalogCategoryThumbSize']) == '')
			{
				$errors['CatalogCategoryThumbSize'] = array('Category Thumbnail Image Size cannot be empty.');
			}

			if (isset($data['CatalogCategoryImageSize']) && trim($data['CatalogCategoryImageSize']) == '')
			{
				$errors['CatalogCategoryImageSize'] = array('Category Thumbnail Image Size cannot be empty.');
			}

			if (isset($data['CatalogThumb2Size']) && trim($data['CatalogThumb2Size']) == '')
			{
				$errors['CatalogThumb2Size'] = array('Product Image Size cannot be empty.');
			}

			if (isset($data['CatalogThumbSize']) && trim($data['CatalogThumbSize']) == '')
			{
				$errors['CatalogThumbSize'] = array('Product Thumbnail Image Size cannot be empty.');
			}

			if (isset($data['CatalogManufacturerImageSize']) && trim($data['CatalogManufacturerImageSize']) == '')
			{
				$errors['CatalogManufacturerImageSize'] = array('Manufacturer Image Size cannot be empty.');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}
}

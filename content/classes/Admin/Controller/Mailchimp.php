<?php

class Admin_Controller_Mailchimp extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'mailchimp_username',
			'mailchimp_password', 'mailchimp_apikey',
			'mailchimp_newsletter_list', 'mailchimp_e360',
			'mailchimp_newsletter_list'
		);

		$mailchimpForm = new Admin_Form_MailchimpForm();
		$form = $mailchimpForm->getForm($settings);

		if ($request->isPost())
		{
			$formData = $request->request->all();
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'mailchimp'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);
			if (!$request->request->has('mailchimp_e360'))
			{
				$settingsData['mailchimp_e360'] = 0;
			}

			if (($validationErrors = $this->getValidationErrors($formData)) == false)
			{
				$settings->persist($settingsData, $settingsVars);
				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'mailchimp'));
			}
			else
			{
				// TODO: find better way to handle this
				$form->bind($formData);
				$form->bindErrors($validationErrors);
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9005');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('update_settings'));

		$adminView->assign('body', 'templates/pages/mailchimp/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get validation errors
	 * @param $data
	 * @return array|bool
	 */
	public function getValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			if (isset($data['mailchimp_username']) && trim($data['mailchimp_username']) == '')
			{
				$errors['mailchimp_username'] = array('User Name  is required');
			}

			if (isset($data['mailchimp_password']) && trim($data['mailchimp_password']) == '')
			{
				$errors['mailchimp_password'] = array('Password  is required');
			}

			if (isset($data['mailchimp_apikey']) && trim($data['mailchimp_apikey']) == '')
			{
				$errors['mailchimp_apikey'] = array('API Key is required');
			}

			try {
				require_once dirname(dirname(dirname(dirname(__FILE__)))).'/vendors/mailchimp/src/Mailchimp.php';
				$mc = new Mailchimp($data['mailchimp_apikey']);
				$allchimplists = $mc->lists->getList();
			}
			catch (Exception $e) {
				$errors['mailchimp_username'] = array('Please, check your Mailchimp settings');
				$errors['mailchimp_apikey'] = array($e->getMessage());
			}
		}
		return count($errors) > 0 ? $errors : false;
	}

	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'mailchimp_username',
			'mailchimp_password', 'mailchimp_apikey',
			'mailchimp_newsletter_list', 'mailchimp_e360',
			'mailchimp_newsletter_list'
		);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['mailchimp_username']) || trim($settingsData['mailchimp_username']) == '')
			{
				$errors[] = array('field' => '.field-mailchimp_username', 'message' => 'User Name  is required');
			}
			if (!isset($settingsData['mailchimp_password']) || trim($settingsData['mailchimp_password']) == '')
			{
				$errors[] = array('field' => '.field-mailchimp_password', 'message' => 'Password is required');
			}
			if (!isset($settingsData['mailchimp_apikey']) || trim($settingsData['mailchimp_apikey']) == '')
			{
				$errors[] = array('field' => '.field-mailchimp_apikey', 'message' => 'API Key is required');
			}

			if (count($errors) < 1)
			{
				try {
					require_once dirname(dirname(dirname(dirname(__FILE__)))).'/vendors/mailchimp/src/Mailchimp.php';
					$mc = new Mailchimp($settingsData['mailchimp_apikey']);
					$allchimplists = $mc->lists->getList();
				}
				catch (Exception $e) {
					$errors[] = array('field' => '.field-mailchimp_username', 'message' => ' Please, check your Mailchimp settings');
					$errors[] = array('field' => '.field-mailchimp_apikey', 'message' => $e->getMessage());
					$this->renderJson(array('status' => 0, 'errors' => $errors));
				}

				$settings->persist($settingsData, $settingsVars);
				$settings->persist(array('mailchimp_enabled' => 1));
				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('mailchimp');

				$this->renderJson(array('status' => 1, 'errors' => null));

			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$adminView = $this->getView();

		$mailchimpForm = new Admin_Form_MailchimpForm();

		$form = $mailchimpForm->getMailchimpActivateForm($settings);

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}


	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('mailchimp_enabled' => 0));
	}
}
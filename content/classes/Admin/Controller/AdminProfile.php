<?php

/**
 * Class Admin_Controller_AdminProfile
 */
class Admin_Controller_AdminProfile extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	protected $avatarPath = 'images/admin/avatars';

	/**
	 * Show administrators list
	 */
	public function listAction()
	{
		$request = $this->getRequest();

		$adminView = $this->getView();

		$canAccess = AccessManager::checkAccess('MAX_ADMINS') > 1;

		$repository = $this->getRepository();

		$itemsCount = 0;

		$admins = false;

		if ($canAccess)
		{
			$itemsCount = $repository->getCount(true) - 1; // Always ignore the current admin..

			if ($itemsCount)
			{
				$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
				$adminView->assign('paginator', $paginator);

				$admins = $repository->getList($this->getCurrentAdminId(), $paginator->sqlOffset, $paginator->itemsPerPage);
			}
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '2024');

		$adminView->assign('itemsCount', $itemsCount);
		$adminView->assign('canAccess', $canAccess);
		$adminView->assign('max_admins', AccessManager::checkAccess('MAX_ADMINS'));
		$adminView->assign('admins', $admins);
		$adminView->assign('delete_nonce', Nonce::create('admin_delete'));

		$adminView->assign('body', 'templates/pages/admin/admin/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Security question action
	 *
	 * @return string
	 */
	public function securityProfileAction()
	{
		$repository = $this->getRepository();

		$formData = $repository->getAdminById($this->getCurrentAdminId());

		if ($formData !== false)
		{
			$session = $this->getSession();

			$forceUpdatePassword = $session->get('admin_force_update_password', false);
			$forceUpdateQuestion = $session->get('admin_force_update_security_question', false);

			$adminView = $this->getView();

			$profileForm = new Admin_Form_SecurityProfileForm();
			$adminView->assign('form', $profileForm->getForm($this->prepareFormData($formData), $forceUpdatePassword, $forceUpdateQuestion));
			$adminView->assign('nonce', Nonce::create('security_profile_update'));

			$adminView->assign('admin_id', $this->getCurrentAdminId());
			$adminView->assign('forceUpdatePassword', $forceUpdatePassword);
			$adminView->assign('forceUpdateQuestion', $forceUpdateQuestion);

			$adminView->assign('body', 'templates/pages/admin/profile/security-profile.html');
			$adminView->render('layouts/default');
		}
	}

	/**
	 * Update security profile
	 */
	public function updateSecurityProfileAction()
	{
		$repository = $this->getRepository();

		$request = $this->getRequest();

		$id = $this->getCurrentAdminId();

		if (!$id)
		{
			$id = intval($request->get('id'));
		}

		$formData = $repository->getAdminById($id);

		if ($formData !== false)
		{
			$errorMessages = array();

			$session = $this->getSession();
			$forceUpdatePassword = $session->get('admin_force_update_password', false);
			$forceUpdateQuestion = $session->get('admin_force_update_security_question', false);

			$result = new stdClass();
			$result->status = 0;
			$result->errors = array();

			if ($request->isPost())
			{
				$formData['password'] = trim($request->request->get('password'));
				$password2 = trim($request->request->get('password2'));

				$passwordHash = getPasswordHash($formData['password']);
				$passwordStrength = pwdStrength($formData['password']);
				$passwordHistory = array();

				$formData['security_question_id'] = $request->request->get('security_question_id');
				$formData['security_question_answer'] = trim($request->request->get('security_question_answer'));

				$this->validateSecurityProfile($formData, $password2, $errorMessages, $passwordHash, $passwordStrength, $passwordHistory, $this->getCurrentAdminId(), $forceUpdatePassword, $forceUpdateQuestion);

				if (count($errorMessages) == 0)
				{
					$result->status = 1;

					$passwordHistory = array();

					if ($formData['password'] != '')
					{
						$passwordHistory = unserialize(base64_decode($formData['password_history']));
					}

					$repository->updatePasswordSecurityQuestion(
						$this->getCurrentAdminId(), $formData['password'],
						$passwordHash, $passwordStrength, $passwordHistory,
						$formData['security_question_id'], $formData['security_question_answer']
					);

					Admin_Flash::setMessage('common.success');

					if ($forceUpdatePassword || $forceUpdateQuestion)
					{
						$session->remove('admin_force_update_password');
						$session->remove('admin_force_update_security_question');
						$session->remove('admin_force_update_profile_goto_homepage');
					}
				}
				else
				{
					$result->errors = $errorMessages;
				}
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Create a new admin
	 */
	public function insertAction()
	{
		$session = $this->getSession();
		$request = $this->getRequest();
		$repository = $this->getRepository();

		$adminId = 0;
		$formData = array(
			'avatar' => '',
			'name' => '',
			'fname' => '',
			'lname' => '',
			'email' => '',
			'username' => '',
			'active' => 'Yes',
			'expires' => 0,
			'expiration_date' => date('Y-m-d H:i:s'),
			'rights' => '',
			'password_history' => '',
			'receive_notifications' => array('invoice', 'outofstock', 'signup', 'security', 'product_review', 'custom_forms', 'pwdchange30'),
			'force_password_change' => 1,
		);

		$aid = $session->get('admin_auth_id', false);
		$adminUsername = $this->getAdminUsername($aid);

		$adminsCount = $repository->getCount();
		$canAccess = AccessManager::checkAccess('MAX_ADMINS') > 1 && $adminsCount < AccessManager::checkAccess('MAX_ADMINS');

		if ($canAccess && $request->isPost())
		{
			$errorMessages = array();

			$formData['username'] = trim($request->request->get('username', ''));
			$formData['password'] = trim($request->request->get('password', ''));
			$password2 = $request->request->get('password2', '');

			$formData['fname'] = trim($request->request->get('fname', ''));
			$formData['lname'] = trim($request->request->get('lname', ''));

			$formData['email'] = trim($request->request->get('email', ''));
			$formData['active'] = trim($request->request->get('active', 'No'));
			$formData['force_password_change'] = trim($request->request->get('force_password_change', '0'));

			$rights = $request->request->get('rights', array());
			$formData['rights'] = implode(',', $rights);

			$receive_notification = $request->request->get('receive_notification', array());
			$pwdchange = trim($request->request->get('pwdchange', 'pwdchange30'));
			$receive_notification['password_change'] = $pwdchange;

			$formData['receive_notifications'] = implode(',', $receive_notification);

			$formData['expires'] = intval($request->request->get('expires', 0));
			$expiration_date = $request->request->get('expiration_date', date_parse(date('Y-m-d H:i:s')));
			$exp_year = intval($expiration_date['year']);
			$exp_month = intval($expiration_date['month']);
			$exp_day = intval($expiration_date['day']);
			$exp_hour = intval($expiration_date['hour']);
			$exp_minute = intval($expiration_date['minute']);

			$formData['expiration_date'] = $exp_year . '-' . $exp_month . '-' . $exp_day . ' ' . $exp_hour . ':' . $exp_minute . ':00';

			$passwordHash = getPasswordHash($formData['password']);
			$passwordStrength = pwdStrength($formData['password']);
			$passwordHistory = array();

			$this->validate($formData, $password2, $errorMessages, $passwordHash, $passwordStrength, $passwordHistory, $adminId);

			if (count($errorMessages) == 0)
			{
				if ($adminId = $repository->save(0, $formData, $passwordHash, $passwordStrength, $passwordHistory))
				{
					$formData['id'] = $adminId;
					if ($formData['avatar'] = $this->handleImagesUpload($request, $adminId))
					{
						$repository->save($adminId, $formData, $passwordHash, $passwordStrength, $passwordHistory);
					}

					Admin_Log::log('Successfully added new admin', Admin_Log::LEVEL_NORMAL, true, $adminUsername);
					Admin_Flash::setMessage('common.success');

					$this->redirect('admin', array('mode' => self::MODE_UPDATE, 'aid' => $adminId));
				}
			}

			foreach ($errorMessages as $errorMessage)
			{
				Admin_Log::log('Add new admin failed - '.$errorMessage, Admin_Log::LEVEL_CRITICAL, true, $adminUsername);
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8025');

		/** @var Admin_Form_ProfileForm $profileForm */
		$profileForm = new Admin_Form_ProfileForm();
		$adminView->assign('form', $profileForm->getForm($this->prepareFormData($formData), Admin_Form_ProfileForm::MODE_INSERT, true, false));
		$adminView->assign('nonce', Nonce::create('admin_insert'));
		$adminView->assign('mode', 'insert');
		$adminView->assign('admin_id', $adminId);
		$adminView->assign('canAccess', $canAccess);
		$adminView->assign('max_admins', AccessManager::checkAccess('MAX_ADMINS'));
		$adminView->assign('body', 'templates/pages/admin/admin/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update admin
	 */
	public function updateAction()
	{
		$repository = $this->getRepository();
		$request = $this->getRequest();
		$session = $this->getSession();

		$adminId = intval($request->query->get('aid', 0));
		$aid = $session->get('admin_auth_id', false);

		$formData = false;

		if ($adminId > 0 && $adminId != $this->getCurrentAdminId())
		{
			$formData = $repository->getAdminById($adminId);
		}

		if ($formData !== false)
		{
			if ($request->isPost())
			{
				$errorMessages = array();

				$formData['username'] = trim($request->request->get('username'));
				$formData['password'] = trim($request->request->get('password'));
				$password2 = trim($request->request->get('password2'));

				$formData['fname'] = trim($request->request->get('fname', ''));
				$formData['lname'] = trim($request->request->get('lname', ''));

				$formData['email'] = trim($request->request->get('email'));;
				$formData['active'] = trim($request->request->get('active', 'No'));
				$formData['force_password_change'] = trim($request->request->get('force_password_change', '0'));

				$rights = $request->request->get('rights', array());
				$formData['rights'] = implode(',', $rights);

				$receive_notification = $request->request->get('receive_notification', array());
				$pwdchange = trim($request->request->get('pwdchange', 'pwdchange30'));
				$receive_notification['password_change'] = $pwdchange;

				$formData['receive_notifications'] = implode(',', $receive_notification);

				$formData['expires'] = intval($request->request->get('expires', 0));
				$expiration_date = $request->request->get('expiration_date', date_parse(date('Y-m-d H:i:s')));
				$exp_year = intval($expiration_date['year']);
				$exp_month = intval($expiration_date['month']);
				$exp_day = intval($expiration_date['day']);
				$exp_hour = intval($expiration_date['hour']);
				$exp_minute = intval($expiration_date['minute']);

				$formData['expiration_date'] = $exp_year.'-'.$exp_month.'-'.$exp_day.' '.$exp_hour.':'.$exp_minute.':00';

				$passwordHash = getPasswordHash($formData['password']);
				$passwordStrength = pwdStrength($formData['password']);
				$passwordHistory = array();

				$this->validate($formData, $password2, $errorMessages, $passwordHash, $passwordStrength, $passwordHistory, $adminId);

				if (count($errorMessages) == 0)
				{
					if($avatarImage = $this->handleImagesUpload($request, $adminId))
					{
						$formData['avatar'] = $avatarImage;
					}

					if ($repository->save($adminId, $formData, $passwordHash, $passwordStrength, $passwordHistory))
					{
						Admin_Log::log('Admin updated successfully for admin user '.$this->getAdminUsername($adminId), Admin_Log::LEVEL_NORMAL, true, $this->getAdminUsername($aid));
						Admin_Flash::setMessage('common.success');

						$this->redirect('admin', array('mode' => self::MODE_UPDATE, 'aid' => $adminId));
					}
					else
					{
						Admin_Log::log('Profile update failed', Admin_Log::LEVEL_CRITICAL, true, $formData['username']);
					}
				}

				foreach ($errorMessages as $errorMessage)
				{
					Admin_Log::log('Admin update failed for admin user '.$this->getAdminUsername($adminId) . ' - '.$errorMessage, Admin_Log::LEVEL_CRITICAL, true, $this->getAdminUsername($aid));
				}
			}

			$canAccess = AccessManager::checkAccess('MAX_ADMINS') > 1;

			$adminView = $this->getView();

			// set menu
			$adminView->assign('activeMenuItem', $this->menuItem);
			$adminView->assign('helpTag', '8025');
			$adminView->assign('title', $formData['fname'] . ' ' . $formData['lname']);

			/** @var Admin_Form_ProfileForm $profileForm */
			$profileForm = new Admin_Form_ProfileForm();
			$adminView->assign('form', $profileForm->getForm($this->prepareFormData($formData), Admin_Form_ProfileForm::MODE_UPDATE, false, false, $adminId));
			$adminView->assign('nonce', Nonce::create('admin_update'));
			$adminView->assign('admin_delete_nonce', Nonce::create('admin_delete'));
			$adminView->assign('mode', 'update');
			$adminView->assign('id', $adminId);
			$adminView->assign('canAccess', $canAccess);
			$adminView->assign('max_admins', AccessManager::checkAccess('MAX_ADMINS'));

			$adminView->assign('body', 'templates/pages/admin/admin/edit.html');
			$adminView->render('layouts/default');
		}
		else
		{
			$this->redirect('profile');
		}
	}

	public function deleteImageAction()
	{
		$request = $this->getRequest();
		$adminId = intval($request->get('id'));

		$repository = $this->getRepository();

		$result = array('status' => 0);
		if($repository->deleteImage($this->avatarPath, $adminId))
		{
			$repository->touch($adminId);
			$result = array('status' => 1);
		}
		$this->renderJson($result);
	}

	/**
	 * Delete administrator
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();
		$aid = $session->get('admin_auth_id', false);
		
		$nonce = $request->query->get('nonce', $request->request->get('nonce', ''));
		if (!Nonce::verify($nonce, 'admin_delete'))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('admins');
			return;
		}

		$repository = $this->getRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$adminId = intval($request->query->get('aid', 0));
			$repository->deleteAdmin($adminId, $this->getCurrentAdminId());

			// log event
			Admin_Log::log('Admin user '.$this->getAdminUsername($adminId) . ' deleted', Admin_Log::LEVEL_NORMAL, true, $this->getAdminUsername($aid));
			Admin_Flash::setMessage('common.success');
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('admin.admin.no_admins_selected', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->deleteAdmin($ids, $this->getCurrentAdminId());
				Admin_Log::log('Admin user(s) '. implode(',', $ids) . ' deleted', Admin_Log::LEVEL_NORMAL, true, $this->getAdminUsername($aid));
				Admin_Flash::setMessage('common.success');
				$this->redirect('admins');
			}
		}
		else if ($deleteMode == 'all')
		{
			$repository->deleteAll($this->getCurrentAdminId());
			Admin_Log::log('All admin users successfully deleted', Admin_Log::LEVEL_NORMAL, true, $this->getAdminUsername($aid));
			Admin_Flash::setMessage('common.success');
			$this->redirect('admins');
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('admins');
	}

	/**
	 * Display admin profile form / save changes
	 */
	public function profileAction()
	{
		$repository = $this->getRepository();
		$formData = $repository->getAdminById($this->getCurrentAdminId());

		if ($formData !== false)
		{
			$session = $this->getSession();
			$request = $this->getRequest();

			$forceUpdatePassword = false; // isset($_SESSION['admin_force_update_password']);
			$forceUpdateQuestion = false; //isset($_SESSION['admin_force_update_security_question']);

			if ($request->isPost())
			{
				$errorMessages = array();

				$formData['fname'] = trim($request->request->get('fname'));
				$formData['lname'] = trim($request->request->get('lname'));
				$formData['email'] = trim($request->request->get('email'));
				$formData['password'] = trim($request->request->get('password'));

				$password2 = trim($request->request->get('password2'));
				$receive_notification = $request->request->get('receive_notification', array());
				$pwdchange = trim($request->request->get('pwdchange', 'pwdchange30'));
				$receive_notification['password_change'] = $pwdchange;

				$formData['receive_notifications'] = implode(',', $receive_notification);

				$nonce = trim($request->request->get('nonce'));
				if (!Nonce::verify($nonce, 'profile_update'))
				{
					Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
					$this->redirect('profile');
				}

				$passwordHash = getPasswordHash($formData['password']);
				$passwordStrength = pwdStrength($formData['password']);
				$passwordHistory = array();

				$formData['security_question_id'] = $request->request->get('security_question_id');
				$formData['security_question_answer'] = trim($request->request->get('security_question_answer'));

				$this->validate($formData, $password2, $errorMessages, $passwordHash, $passwordStrength, $passwordHistory, $this->getCurrentAdminId(), $forceUpdatePassword, $forceUpdateQuestion);

				if (count($errorMessages) == 0)
				{
					$passwordHistory = array();

					if ($formData['password'] != '')
					{
						$passwordHistory = unserialize(base64_decode($formData['password_history']));
					}

					if ($forceUpdatePassword || $forceUpdateQuestion)
					{
						$forceUpdatePassword = false;
						$forceUpdateQuestion = false;

						$session->remove('admin_force_update_password');
						$session->remove('admin_force_update_security_question');

						if ($session->has('admin_force_update_profile_goto_homepage'))
						{
							$session->remove('admin_force_update_profile_goto_homepage');
							$this->redirect('home');
						}
					}

					if($avatarImage = $this->handleImagesUpload($request, $this->getCurrentAdminId())){
						$formData['avatar'] = $avatarImage;
					}

					if ($repository->save($this->getCurrentAdminId(), $formData, $passwordHash, $passwordStrength, $passwordHistory))
					{
						Admin_Log::log('Profile successfully updated', Admin_Log::LEVEL_NORMAL, true, $formData['username']);
						Admin_Flash::setMessage('common.success');

						$this->redirect('profile');
					}
					else
					{
						Admin_Log::log('Profile update failed', Admin_Log::LEVEL_CRITICAL, true, $formData['username']);
					}
				}

				foreach ($errorMessages as $errorMessage)
				{
					Admin_Log::log('Profile update failed - ' . $errorMessage, Admin_Log::LEVEL_CRITICAL, true, $formData['username']);
					Admin_Flash::setMessage($errorMessage, Admin_Flash::TYPE_ERROR);
				}
			}

			$adminView = $this->getView();

			// set menu
			$adminView->assign('activeMenuItem', $this->menuItem);

			/** @var Admin_Form_ProfileForm $profileForm */
			$profileForm = new Admin_Form_ProfileForm();
			$adminView->assign('form', $profileForm->getForm($this->prepareFormData($formData), Admin_Form_ProfileForm::MODE_PROFILE, $forceUpdatePassword, $forceUpdateQuestion, $this->getCurrentAdminId()));
			$adminView->assign('nonce', Nonce::create('profile_update'));
			$adminView->assign('body', 'templates/pages/admin/profile/edit.html');
			$adminView->render('layouts/default');
		}
		else
		{
			$this->redirect('');
		}
	}

	/**
	 * Handle avatar image upload
	 *
	 * @param $request
	 * @param $adminId
	 */
	protected function handleImagesUpload($request, $adminId)
	{
		if (!is_dir($this->avatarPath) || !is_writable($this->avatarPath))
		{
			Admin_Flash::setMessage('admin.profile.avatar_dir_permission', Admin_Flash::TYPE_ERROR);
		}
		else
		{
			if ($imageUploadInfo = $request->getUploadedFile('avatar'))
			{
				/** @var Admin_Service_FileUploader $fileUploaderService */
				$fileUploadService = new Admin_Service_FileUploader();

				$imageFile = $fileUploadService->processImageUpload($imageUploadInfo, $this->avatarPath, $adminId);

				if ($imageFile)
				{
					$extension = pathinfo(basename($imageFile), PATHINFO_EXTENSION);
					$settings = $this->getSettings();
					ImageUtility::generateManufacturerImage($imageFile, $this->avatarPath . '/thumbs/' . intval($adminId) . '.' . $extension);

					if ($settings->get('CatalogOptimizeImages') == 'YES')
					{
						$optimizer = new Admin_Service_ImageOptimizer();
						$optimizer->optimize($imageFile);
					}
					return $imageFile;
				}
			}
		}
		return;
	}

	/**
	 *
	 */
	public function unlinkOpenIDAction()
	{
		$db = $this->getDb();

		$db->reset();
		$db->assignStr('openid_provider', '');
		$db->assignStr('openid', '');
		$db->update(DB_PREFIX.'admins', ' WHERE aid = '.$this->getCurrentAdminId());

		$this->redirect('profile');
	}

	/**
	 *
	 */
	public function openIDAction()
	{
		require_once dirname(dirname(dirname(dirname(__FILE__)))).'/vendors/lightopenid/openid.php';
		$settings = $this->getSettings();
		$openid = new LightOpenID($settings->get('AdminHttpsUrl'));

		if (!$openid->mode)
		{
			$openid->identity = 'https://openid.intuit.com/Identity-'.getpp();

			$openid->required = array('contact/email');
			$openid->optional = array('namePerson', 'namePerson/friendly');
			header('Location: ' . $openid->authUrl());
		}
		else if ($openid->mode != 'cancel' && $openid->validate())
		{
			$identity = $openid->identity;

			$db = $this->getDb();
			$db->reset();
			$db->assignStr('openid_provider', 'intuit');
			$db->assignStr('openid', $identity);
			$db->update(DB_PREFIX.'admins', ' WHERE aid = '.$this->getCurrentAdminId());

			$request = $this->getRequest();
			if ($request->query->get('force', false))
			{
				header('Location: admin.php');
			}
			else
			{
				header('Location: admin.php?p=profile');
			}
		}
	}

	/**
	 * Check are there duplicates
	 *
	 * @param $username
	 * @param $adminId
	 *
	 * @return bool
	 */
	protected function hasDuplicateAdmin($username, $adminId)
	{
		$db = $this->getDb();

		$duplicateAdmin = $db->selectOne('SELECT aid FROM '.DB_PREFIX.'admins WHERE username LIKE "'.$db->escape($username).'"  AND aid<>'.intval($adminId));

		return $duplicateAdmin !== false;
	}

	/**
	 * Check area there duplicate passwords
	 *
	 * @param $password
	 * @param $adminId
	 *
	 * @return bool
	 */
	protected function hasDuplicatePassword($password, $adminId)
	{
		$db = $this->getDb();

		$duplicateAdmin = $db->selectOne('SELECT * FROM '.DB_PREFIX.'admins WHERE password = "'.$db->escape(getPasswordHash($password)).'"  AND aid<>'.intval($adminId));

		return $duplicateAdmin !== false;
	}

	/**
	 * Validate security profile
	 *
	 * @param $admin
	 * @param $password2
	 * @param $errorMessages
	 * @param $passwordHash
	 * @param $passwordStrength
	 * @param $passwordHistory
	 * @param $adminId
	 * @param bool $forceChangePassword
	 * @param bool $forceAnswerSecurityQuestion
	 */
	protected function validateSecurityProfile($admin, $password2, &$errorMessages, &$passwordHash, &$passwordStrength, &$passwordHistory, $adminId, $forceChangePassword = false, $forceAnswerSecurityQuestion = false)
	{
		if ($admin['password'] != '' || $forceChangePassword)
		{
			if ($admin['password'] == '' && $forceChangePassword)
			{
				$errorMessages['password'] = 'Please enter new password';
			}

			if ($admin['password_history'] != '')
			{
				$passwordHistory = unserialize(base64_decode($admin['password_history']));
			}

			if ($admin['password'] != $password2)
			{
				$errorMessages['password2'] = 'Password and confirmation are not equal';
			}
			else if ($this->getSettings()->get('SecurityForceCompliantAdminPasswords') == 'YES')
			{
				foreach ($passwordHistory as $history)
				{
					if ($passwordHash == $history['p'])
					{
						$errorMessages['password'] = 'The password you entered has been used before, please enter another password';
					}
				}

				if (count($errorMessages) == 0 && $passwordStrength < 3)
				{
					$errorMessages['password'] = 'Password is not strong enough';
				}
			}
			else if (strlen($admin['password']) < 8)
			{
				$errorMessages['password'] = 'Password is too short';
			}
		}

		if ($this->hasDuplicatePassword($admin['password'], $adminId))
		{
			$errorMessages['password'] = 'The password you entered has been used before, please enter another password';
		}

		if ($forceAnswerSecurityQuestion && (trim($admin['security_question_id']) == '' || trim($admin['security_question_answer']) == ''))
		{
			$errorMessages['security_question_answer'] = 'Please answer a security question';
		}
	}

	/**
	 * Validate admin's data
	 *
	 * @param $admin
	 * @param $password2
	 * @param $errorMessages
	 * @param $passwordHash
	 * @param $passwordStrength
	 * @param $passwordHistory
	 * @param $adminId
	 * @param bool $forceChangePassword
	 * @param bool $forceAnswerSecurityQuestion
	 */
	protected function validate($admin, $password2, &$errorMessages, &$passwordHash, &$passwordStrength, &$passwordHistory, $adminId, $forceChangePassword = false, $forceAnswerSecurityQuestion = false)
	{
		if (isEmail($admin['email']) != true)
		{
			$errorMessages[] = trans('admin.profile.email_invalid');
		}

		$this->validateSecurityProfile($admin, $password2, $errorMessages, $passwordHash, $passwordStrength, $passwordHistory, $adminId, $forceChangePassword, $forceAnswerSecurityQuestion);

		if ($this->hasDuplicateAdmin($admin['username'], $adminId))
		{
			$errorMessages[] = 'Backend administrator with username '.gs($admin['username']).' already exists';
		}
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	protected function getAdminUsername($id)
	{
		$db = $this->getDb();
		$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'admins WHERE aid = '.intval($id));
		return $result['username'];
	}

	/**
	 * Generate Random Password
	 * @return mixed
	 */
	public function generatePasswordAction()
	{
		$this->renderJson(array('password' => generatePassword()));
	}

	/**
	 * Prepare admin's data
	 *
	 * @param $admin
	 */
	protected function prepareFormData($admin)
	{
		if (!is_array($admin['rights']))
		{
			$admin['rights'] = trim($admin['rights']) != '' ? explode(',', $admin['rights']) : array();
		}

		$admin['password_history'] = $admin['password_history'] != '' ? unserialize(base64_decode($admin['password_history'])) : array();

		if (!is_array($admin['receive_notifications']))
		{
			$admin['receive_notifications'] = trim($admin['receive_notifications']) != '' ? explode(',', trim($admin['receive_notifications'])) : array();
		}

		return $admin;
	}

	/**
	 * @return DataAccess_AdminRepository
	 */
	protected function getRepository()
	{
		return new DataAccess_AdminRepository($this->getDb(), $this->getSettings());
	}
}

<?php

/**
 * Class Admin_Controller_CountriesStates
 */
class Admin_Controller_CountriesStates extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/country-state/';
	const FETCH_PATH = 'pages/country-state/';

	const NONCE_ADDCOUNTRIES = 'country_add';
	const NONCE_UPDATECOUNTRY = 'country_update';
	const NONCE_DELETECOUNTRIES = 'countries_delete';
	const NONCE_COUNTRYACTIVATION = 'country_activation';

	const NONCE_ADDSTATES = 'states_add';
	const NONCE_UPDATESTATES = 'state_update';
	const NONCE_DELETESTATES = 'states_delete';

	protected $repository = null;

	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * List countries
	 */
	public function listCountriesAction()
	{
		$adminView = $this->getView();

		$rep = $this->getCountriesStatesRepository();
	
		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'country_priority DESC, name');

		$itemsCount = $rep->getCountriesCount();

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);

			$newrep = $rep->getCountriesList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy);

//			for($i=0; $i<count($newrep); $i++)
//			{
//				$newrep[$i]['disable'] = false;
//				if(in_array($newrep[$i]['name'], $this->excludedCountry()))
//				{
//					$newrep[$i]['disable'] = true;
//				}
//			}

			$adminView->assign('countries', $newrep);
		}
		else
		{
			$adminView->assign('countries', null);
		}

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8033');

		$adminView->assign('delete_nonce', Nonce::create(self::NONCE_DELETECOUNTRIES));
		$adminView->assign('addNonce', Nonce::create(self::NONCE_ADDCOUNTRIES));
		$adminView->assign('updateNonce', Nonce::create(self::NONCE_UPDATECOUNTRY));
		$adminView->assign('activationNonce', Nonce::create(self::NONCE_COUNTRYACTIVATION));
		$adminView->assign('countryForm', $this->getCountryForm());

		$adminView->assign('body', self::TEMPLATES_PATH.'list-countries.html');
		$adminView->render('layouts/default');
	}

	public function excludedCountry()
	{
		return array('United States', 'Canada');
	}

	/**
	 * Add new country
	 */
	public function addCountryAction()
	{
		$request = $this->getRequest();

		$repository = $this->getCountriesStatesRepository();

		$defaults = $repository->getCountryDefaults();
		$defaults['available'] = 'Yes';
		$mode = self::MODE_ADD;

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], self::NONCE_ADDCOUNTRIES, false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getCountryValidationErrors($formData, $mode)) == false)
				{
					$repository->persistCountry($formData);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}
	}

	/**
	 * Update country
	 */
	public function updateCountryAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		/** @var DataAccess_TaxRepository */
		$repository = $this->getCountriesStatesRepository();

		$id = intval($request->get('id'));

		$country = $repository->getCountryById($id);

		$defaults = $repository->getCountryDefaults();

		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!$request->request->has('country_priority'))
			{
				unset($formData['country_priority']);
			}

			if (!Nonce::verify($formData['nonce'], self::NONCE_UPDATECOUNTRY, false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getCountryValidationErrors($formData, $mode, $id)) == false)
				{
					$id = $repository->persistCountry($formData);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}

		if(in_array($country['name'], $this->excludedCountry()))
		{
			$country['readonly'] = true;
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('id', $id);
		$adminView->assign('title', $country['name']);
		$form = $this->getCountryForm($country, $mode);
		$adminView->assign('countryForm', $form);
		$adminView->assign('updateNonce', Nonce::create(self::NONCE_UPDATECOUNTRY));

		$html = $adminView->fetch(self::FETCH_PATH.'modal-edit-country');

		$this->renderJson(array(
			'status' => 1,
			'html' => $html,
		));
	}

	/**
	 * Delete countries
	 */
	public function deleteCountriesAction()
	{
		$request = $this->getRequest();
	
		$repository = $this->getCountriesStatesRepository();
	
		$deleteMode = $request->get('deleteMode');
		$page = $request->get('page', 1);
	
		if (!Nonce::verify($request->get('nonce'), self::NONCE_DELETECOUNTRIES))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('countries', array('page' => $page));
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('categories.country_not_found', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->deleteCountries(array($id));
				Admin_Flash::setMessage('common.success');
				$this->redirect('countries', array('page' => $page));
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('categories.no_country_selected', Admin_Flash::TYPE_ERROR);
				$this->redirect('countries', array('page' => $page));
			}
			else
			{
				$repository->deleteCountries($ids);
				Admin_Flash::setMessage('common.success');
				$this->redirect('countries', array('page' => $page));
			}
		}
		else if ($deleteMode == 'all')
		{
			$repository->deleteCountries('all');
			Admin_Flash::setMessage('common.success');
			$this->redirect('countries');
		}
	}

	/**
	 * List states by country id
	 */
	public function listStatesByCountryAction()
	{
		$adminView = $this->getView();
	
		$repository = $this->getCountriesStatesRepository();

		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'name');
	
		$coid = $request->get('coid');
		$countriespage = $request->get('countriespage', 1);
		$page		 = $request->get('page', 1);

		if (intval($coid) <= 0)
		{
			Admin_Flash::setMessage('countries.country_not_found', Admin_Flash::TYPE_ERROR);
		}

		$cur_country = $repository->getCountryById($coid);

		$itemsCount = $repository->getStatesCountByCountryId($coid);
		$adminView->assign('itemsCount', $itemsCount);
	
		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('states', $repository->getStatesListByCountryId($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy, $coid));
		}
		else
		{
			$adminView->assign('states', null);
		}
	
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8034');
		$adminView->assign('countriespage', $countriespage);
		$adminView->assign('page', $page);
	
		$adminView->assign('delete_nonce', Nonce::create(self::NONCE_DELETESTATES));
		$adminView->assign('addNonce', Nonce::create(self::NONCE_ADDSTATES));
		$adminView->assign('updateNonce', Nonce::create(self::NONCE_UPDATESTATES));
		$adminView->assign('stateForm', $this->getStateForm(null, $coid, self::MODE_ADD));
		$adminView->assign('coid', $coid);
		$adminView->assign('cur_country', $cur_country);

		$adminView->assign('body', self::TEMPLATES_PATH . 'list-states.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new state
	 */
	public function addStateAction()
	{
		$request = $this->getRequest();

		$repository = $this->getCountriesStatesRepository();
		$coid = intval($request->get('coid'));

		$defaults = $repository->getStateDefaults();
		$defaults['coid'] = $coid;

		$mode = self::MODE_ADD;

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], self::NONCE_ADDSTATES, false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getStateValidationErrors($formData, $mode)) == false)
				{
					$repository->persistState($formData);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}
	}
	
	/**
	 * Update state
	 */
	public function updateStateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getCountriesStatesRepository();

		$id = intval($request->get('id'));
		$coid = intval($request->get('coid'));
		$page = $request->get('page', 1);
		$countriespage = $request->get('countriespage', 1);

		$state = $repository->getStateById($id);

		$defaults = $repository->getStateDefaults();
		$defaults['coid'] = $coid;

		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());


			if (!Nonce::verify($formData['nonce'], self::NONCE_UPDATESTATES, false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getStateValidationErrors($formData, $mode, $id)) == false)
				{
					$id = $repository->persistState($formData);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('page', $page);
		$adminView->assign('countriespage', $countriespage);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('title', $state['name']);

		$adminView->assign('coid', $coid);

		$adminView->assign('updateNonce', Nonce::create(self::NONCE_UPDATESTATES));
		$form = $this->getStateForm($state, $coid, $mode);
		$adminView->assign('stateForm', $form);

		$adminView->assign('updateNonce', Nonce::create(self::NONCE_UPDATECOUNTRY));

		$html = $adminView->fetch(self::FETCH_PATH.'modal-edit-state');

		$this->renderJson(array(
			'status' => 1,
			'html' => $html,
		));

	}
	
	/**
	 * Delete states
	 */
	public function deleteStatesAction()
	{	
		$request = $this->getRequest();
	
		$repository = $this->getCountriesStatesRepository();

		$page = $request->get('page', 1);
		$countriespage = $request->get('countriespage', 1);

		$coid = intval($request->get('coid'));

		$deleteMode = $request->get('deleteMode');
	
		if (!Nonce::verify($request->get('nonce'), self::NONCE_DELETESTATES))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
		}
	
		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('state.state_not_found', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($repository->deleteStates(array($id)))
				{
					Admin_Flash::setMessage('common.success');
					$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
				}
				else
				{
					Admin_Flash::setMessage('states.no_state_selected', Admin_Flash::TYPE_ERROR);
					$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
				}
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('states.no_state_selected', Admin_Flash::TYPE_ERROR);
				$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
			}
			else
			{
				if ($repository->deleteStates($ids))
				{
					Admin_Flash::setMessage('common.success');
					$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
				}
				else
				{
					Admin_Flash::setMessage('states.cannot_delete_states', Admin_Flash::TYPE_ERROR);
					$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
				}

			}
		}
		else if ($deleteMode == 'all')
		{
			if ($repository->deleteStates('all', $coid))
			{
				Admin_Flash::setMessage('common.success');
				$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
			}
			else
			{
				Admin_Flash::setMessage('states.cannot_delete_states', Admin_Flash::TYPE_ERROR);
				$this->redirect('states', array('coid'=>$coid, 'page' => $page, 'countriespage' => $countriespage));
			}
		}
	}
	
	/**
	 * Activate / deactivate countries
	 */
	public function countryActivationAction()
	{
		$request = $this->getRequest();
		$repository = $this->getCountriesStatesRepository();
		$activationMode = $request->get('activationMode');
		$page = $request->get('page', 1);
		$mode = $request->get('activationAction');
		
		if (!Nonce::verify($request->get('nonce'), self::NONCE_COUNTRYACTIVATION))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('countries', array('page' => $page));
			return;
		}
		
		if ($activationMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('categories.no_country_selected', Admin_Flash::TYPE_ERROR);
				$this->redirect('countries', array('page' => $page));
			}
			else
			{
				$repository->countriesActivation($ids, $mode);
				Admin_Flash::setMessage('common.success');
				$this->redirect('countries', array('page' => $page));
			}
		}
		else if ($activationMode == 'all')
		{
			$repository->countriesActivation('all', $mode);
			Admin_Flash::setMessage('common.success');
			$this->redirect('countries');
		}
	}
	
	/**
	 * Get repository
	 *
	 * @return DataAccess_CountriesStatesRepository
	 */
	protected function getCountriesStatesRepository()
	{
		if (is_null($this->repository))
		{
			$this->repository = new DataAccess_CountriesStatesRepository($this->getDb());
		}

		return $this->repository;
	}

	/**
	 * Format validation errors
	 *
	 * @param $validationErrors
	 *
	 * @return string
	 */
	protected function formatValidationErrors($validationErrors)
	{
		$list = '';

		foreach ($validationErrors as $errors)
		{
			foreach ($errors as $msg) $list .= '<li>'.$msg.'</li>';
		}

		if ($list != '') $list = '<ul>'.$list.'</ul>';
		return $list;
	}

	/**
	 * @param null $data
	 * @param null $mode
	 * @return core_Form_Form
	 */
	protected function getCountryForm($data = null, $mode = null)
	{

		$repository = $this->getCountriesStatesRepository();
		$form = new Admin_Form_CountryForm();

		if ($data == null)
		{
			$formData = $repository->getCountryDefaults();
			$formData['available'] = 'Yes';
		}
		else
		{
			$formData = $data;
		}

		return $form->getForm($formData, $mode);
	}

	/**
	 * @param null $data
	 * @param null $coid
	 * @param null $mode
	 * @return core_Form_Form
	 */
	protected function getStateForm($data = null, $coid = null, $mode = null)
	{
		$repository = $this->getCountriesStatesRepository();
		$form = new Admin_Form_StateForm();

		if ($data == null)
		{
			$formData = $repository->getStateDefaults();
			$formData['coid'] = $coid;
		}
		else
		{
			$formData = $data;
		}

		return $form->getForm($formData, $mode);
	}
}
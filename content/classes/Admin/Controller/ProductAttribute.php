<?php

/**
 * Class Admin_Controller_ProductAttribute
 */
class Admin_Controller_ProductAttribute extends Admin_Controller
{
	/** @var null */
	protected $productAttributeRepository = null;

	/** @var null */
	protected $productRepository = null;

	/**
	 * Add attribute
	 */
	public function addAttributeAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productAttributesRepository = $this->getProductAttributeRepository();

			$defaults = $productAttributesRepository->getDefaults();

			$formData = array_merge($defaults, $request->get('attribute', array()));
			$formData['id'] = $formData['paid'] = null;

			if (($validationErrors = $productAttributesRepository->getValidationErrors($formData, $mode)) == false)
			{
				$productAttributesRepository->persist($formData);
				$this->getProductRepository()->updateProductAttributesCount($formData['pid']);
				$this->touchProduct($formData['pid']);

				$result = array('status' => 1, 'attributes' => $productAttributesRepository->getListByProductId($formData['pid']));
			}
			else
			{
				$result = array('status' => 0, 'errors' => $validationErrors);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Add global attributes
	 */
	public function addGlobalAttributeAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productId = $request->get('productId', 0);

			$globalAttributes = explode(',', $request->get('globalAttributes', ''));

			$productAttributesRepository = $this->getProductAttributeRepository();

			$productAttributesRepository->addGlobalAttributes($productId, $globalAttributes);
			$this->getProductRepository()->updateProductAttributesCount($productId);
			$this->touchProduct($productId);

			$result = array('status' => 1, 'attributes' => $productAttributesRepository->getListByProductId($productId));
		}

		$this->renderJson($result);
	}

	/**
	 * Update attribute
	 */
	public function updateAttributeAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productAttributesRepository = $this->getProductAttributeRepository();

			$formData = array_merge($productAttributesRepository->getDefaults(), $request->get('attribute', array()));

			$attributeData = $productAttributesRepository->getById($formData['id']);

			if ($attributeData)
			{
				if (($validationErrors = $productAttributesRepository->getValidationErrors($formData, $mode)) == false)
				{
					$productAttributesRepository->persist($formData);
					$this->getProductRepository()->updateProductAttributesCount($formData['pid']);
					$this->touchProduct($formData['pid']);

					$result = array('status' => 1, 'attributes' => $productAttributesRepository->getListByProductId($formData['pid']));
				}
				else
				{
					$result = array('status' => 0, 'errors' => $validationErrors);
				}
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find attribute by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update attribute. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Delete product attribute
	 */
	public function deleteAttributeAction()
	{
		$mode = self::MODE_DELETE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productAttributesRepository = $this->getProductAttributeRepository();

			$formData = array_merge($productAttributesRepository->getDefaults(), $request->get('attribute', array()));

			$attributeData = $productAttributesRepository->getById($formData['id']);

			if ($attributeData)
			{
				$canBeDeleted = $productAttributesRepository->delete(array($formData['id']));
				$this->getProductRepository()->updateProductAttributesCount($formData['pid']);
				$this->touchProduct($formData['pid']);

				if ($canBeDeleted)
				{
					$result = array('status' => 1, 'attributes' => $productAttributesRepository->getListByProductId($formData['pid']));
				}
				else
				{
					$result = array('status' => 0, 'errors' => array('error' => array('Cannot delete attribute when it is used in variants.')));
				}
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find attribute by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update attribute. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Get attributes repository
	 *
	 * @return DataAccess_ProductAttributeRepository
	 */
	protected function getProductAttributeRepository()
	{
		if (is_null($this->productAttributeRepository))
		{
			$this->productAttributeRepository = new DataAccess_ProductAttributeRepository($this->getDb());
		}

		return $this->productAttributeRepository;
	}

	/**
	 * Get product repository
	 */
	protected function getProductRepository()
	{
		if (is_null($this->productRepository))
		{
			$this->productRepository = new DataAccess_ProductsRepository($this->getDb());
		}

		return $this->productRepository;
	}

	/**
	 * Mark the product as updated
	 *
	 * @param $productDbId
	 */
	protected function touchProduct($productDbId)
	{
		$this->getProductRepository()->touch($productDbId);

		$productData = $this->getProductRepository()->getProductById($productDbId, true);

		if ($productData)
		{
			$event = new Events_ProductEvent(Events_ProductEvent::ON_UPDATED);
			$event->setData('product', $productData);
			Events_EventHandler::handle($event);
		}
	}
}
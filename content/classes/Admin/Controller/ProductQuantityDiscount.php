<?php

/**
 * Class Admin_Controller_ProductQuantityDiscount
 */
class Admin_Controller_ProductQuantityDiscount extends Admin_Controller
{
	/** @var DataAccess_ProductQuantityDiscountRepository $productQuantityDiscountRepository */
	protected $productQuantityDiscountRepository = null;

	/**
	 * Add promotion
	 */
	public function addPromotionAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productQuantityDiscountRepository = $this->getProductQuantityDiscountRepository();

			$defaults = $productQuantityDiscountRepository->getDefaults();

			$formData = array_merge($defaults, $request->get('qd', array()));
			$formData['id'] = $formData['qd_id'] = null;

			if (($validationErrors = $productQuantityDiscountRepository->getValidationErrors($formData, $mode)) == false)
			{
				$productQuantityDiscountRepository->persist($formData);
				$result = array('status' => 1, 'discounts' => $productQuantityDiscountRepository->getListByProductId($formData['pid']));
			}
			else
			{
				$result = array('status' => 0, 'errors' => $validationErrors);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Update promotion
	 */
	public function updatePromotionAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productQuantityDiscountRepository = $this->getProductQuantityDiscountRepository();

			$formData = array_merge($productQuantityDiscountRepository->getDefaults(), $request->get('qd', array()));

			$qdData = $productQuantityDiscountRepository->getById($formData['id']);

			if ($qdData)
			{
				if (($validationErrors = $productQuantityDiscountRepository->getValidationErrors($formData, $mode)) == false)
				{
					$productQuantityDiscountRepository->persist($formData);
					$result = array('status' => 1, 'discounts' => $productQuantityDiscountRepository->getListByProductId($formData['pid']));
				}
				else
				{
					$result = array('status' => 0, 'errors' => $validationErrors);
				}
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find quantity discount by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update quantity discount. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Delete product promotion
	 */
	public function deletePromotionAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productQuantityDiscountRepository = $this->getProductQuantityDiscountRepository();

			$formData = array_merge($productQuantityDiscountRepository->getDefaults(), $request->get('qd', array()));

			$promotionData = $productQuantityDiscountRepository->getById($formData['id']);

			if ($promotionData)
			{
				$productQuantityDiscountRepository->delete(array($formData['id']));

				$result = array('status' => 1, 'discounts' => $productQuantityDiscountRepository->getListByProductId($formData['pid']));

			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find quantity discount by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update quantity discount. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductQuantityDiscountRepository
	 */
	protected function getProductQuantityDiscountRepository()
	{
		if (is_null($this->productQuantityDiscountRepository))
		{
			$this->productQuantityDiscountRepository = new DataAccess_ProductQuantityDiscountRepository($this->getDb());
		}

		return $this->productQuantityDiscountRepository;
	}
}
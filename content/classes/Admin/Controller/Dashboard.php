<?php

/**
 * Class Admin_Controller_Dashboard
 */
class Admin_Controller_Dashboard extends Admin_Controller
{
	/**
	 * Index action
	 */
	public function indexAction()
	{
		$adminView = $this->getView();
		$settings = $this->getSettings();
		$settingsValues = $settings->getAll();

		$intuitQuickStart = false;

		if ($this->establishIntuitDirectConnect())
		{
			$doDirectConnect = false;

			global $admin_auth_id;

			$db = $this->getDb();
			$db->query("SELECT COUNT(*) FROM ".DB_PREFIX."admins WHERE aid=".intval($admin_auth_id).' AND openid <> \'\'');
			$row = $db->moveNext();

			if ($row && $row['0'] == 0)
			{
				$connectUrl = $settings->get('GlobalHttpsUrl').'/admin.php?p=profile_openid&chart=true&force=true';
			}
			else
			{
				$connectUrl = false;
				$doDirectConnect = true;
			}

			$adminView->assign('connectUrl', $connectUrl);
			$adminView->assign('doDirectConnect', $doDirectConnect);

			$adminView->assign('body', 'templates/pages/quickbooks/direct-connect.html');
			$adminView->render('layouts/default');
			return;
		}
		else if ($settings->get('intuiNew Orderst_anywhere_connected') == 'Yes' && $settings->get('intuit_income_account') == '')
		{
			$qbSync = intuit_Services_QBSync::create();
			$qs = new Admin_Intuit_QuickStart($this->getDb(), $settingsValues, $adminView, $qbSync);

			$intuitQuickStart = $qs->action_render();
		}

		$adminView->assign('intuit_quick_start', $intuitQuickStart);

		global $auth_rights;

		if (!$intuitQuickStart)
		{
//			$adminRepo
//			$admin = $this->getCurrentAdminId();
//			$autoOpenQSG = isPrivilege($auth_rights, array('all')) && $admin["display_quickstart"] == 1 && $display_quick_start);
		}

		$hasUpdates = false;

		if (isPrivilege($auth_rights, array('all', 'global')))
		{
			$updateClient = new UpdateClient($this->getDb(), new XMLParser());
			$updateScriptUrl = updateClient::updateScriptUrl;
			if (defined('DEVMODE') && DEVMODE && defined('UPDATE_SCRIPT_URL')) $updateScriptUrl = UPDATE_SCRIPT_URL;
			$fixes = $updateClient->checkForUpdates($updateScriptUrl, LICENSE_NUMBER, $settings->get('AppVer'), $settings->get('AppEncoder'));
			$hasUpdates = $fixes && $updateClient->new_updates;
		}

		$adminView->assign('hasUpdates', $hasUpdates);

		$statsRepository = new DataAccess_StatRepository($this->getDb());

		$adminView->assign('ordersStats', $statsRepository->getOrdersMonthData());
		$adminView->assign('customersStats', $statsRepository->getCustomersMonthData());
		$adminView->assign('productsStats', $statsRepository->getProductsData());
		$adminView->assign('testimonialStats', $statsRepository->getTestimonialsData());

		$db = $this->getDb();
		$promoCodesRepository = new DataAccess_PromoCodeRepository($db, $settingsValues);
		$adminView->assign('promoCodes', $promoCodesRepository->getList(0, 5, 'pid DESC'));

		$ordersRepository = new DataAccess_OrderRepository($db, $settingsValues);

		$searchParams = array('status' => array(Order::STATUS_COMPLETED, Order::STATUS_PROCESS));
		$adminView->assign('orders', $ordersRepository->getList(0, 5, null, $searchParams));

		global $auth_rights;
		$adminView->assign('hasOrdersAccess', isPrivilege($auth_rights, array('all', 'orders')));
		$adminView->assign('hasCustomersAccess', isPrivilege($auth_rights, array('all', 'users')));
		$adminView->assign('hasProductsAccess', isPrivilege($auth_rights, array('all', 'products')));
		$adminView->assign('hasMarketingAccess', isPrivilege($auth_rights, array('all', 'marketing')));
		$adminView->assign('hasThemeAccess', isPrivilege($auth_rights, array('all', 'content')));
		$adminView->assign('hasTestimonialAccess', isPrivilege($auth_rights, array('all', 'testimonials')));

		$adminView->assign('helpTag', '0001');

		$adminView->assign('body', 'templates/pages/home/dashboard.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @return bool
	 */
	protected function establishIntuitDirectConnect()
	{
		$settings = $this->getSettings();

		return defined('INTUIT_TRIAL') && INTUIT_TRIAL && $settings->get('intuit_anywhere_connected') != 'Yes';
	}
}
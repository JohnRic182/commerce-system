<?php

/**
 * Class Admin_Controller_Customers
 */
class Admin_Controller_Customers extends Admin_Controller
{
	const MODE_ADD = 'add';
	const MODE_UPDATE = 'update';
	const MODE_LOCK = 'lock';
	const TEMPLATES_PATH = 'templates/pages/customers/';

	/* Export */
	protected $subscribersExportService = null;
	protected $customersExportService = null;
	protected $subscribersExportFileName = 'subscribers.csv';
	protected $customersExportFileName = 'customers.csv';

	/** @var DataAccess_CustomersRepository */
	protected $repository = null;

	/** @var PaymentProfiles_DataAccess_PaymentProfileRepository */
	protected $paymentProfileRepository = null;

	protected $menuItem = array('primary' => 'customers', 'secondary' => '');

	protected $searchParamsDefaults = array('search_str' => '', 'ExpressCheckoutUser' => '0', 'from_date' => '', 'to_date' => '', 'logic' => 'AND', 'wholesale_users' => '');

	/**
	 * List Customers
	 */
	public function listAction()
	{
		$adminView = $this->getView();
		$session = $this->getSession();
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$rep = $this->getDataRepository();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('customerSearchParams', array()); else $session->set('customerSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('customerSearchOrderBy', 'name'); else $session->set('customerSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('customerSearchOrderDir', 'asc'); else $session->set('customerSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		$itemsCount = $rep->getCustomersCount($searchParams, $searchParams['logic']);

		// Our search form
		$customerForm = new Admin_Form_CustomerSearchForm($this->getDb(), $settings);
		$adminView->assign('searchForm', $customerForm->getCustomerSearchForm($searchFormData));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$customers = $rep->getCustomersList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy . '_' . $orderDir, $searchParams, $searchParams['logic']);

			$adminView->assign('customers', $customers);
		}
		else
		{
			$adminView->assign('customers', null);
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '2000');

		$adminView->assign('searchParams', $searchParams);

		$adminView->assign('delete_nonce', Nonce::create('customer_delete'));

		$adminView->assign('body', self::TEMPLATES_PATH . 'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Edit Customer
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$rep = $this->getDataRepository();

		$id = intval($request->get('id'));

		$user = $rep->getById($id);
		$last_update = $user['last_update'];
		
		if (!$user)
		{
			Admin_Flash::setMessage('Cannot find customer by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('customers');
		}

		$paymentProfileRepository = $this->getPaymentProfileRepository();

		/** @var Admin_Form_CustomerForm */
		$settings = $this->getSettings();
		$billingAddressForm = new Admin_Form_AddressForm($settings);
		$billingAddressForm->setCollapsibleAttr(true);
		$hideFields = array('address_type', 'fname', 'lname', 'name', 'company');
		$addressForm = $billingAddressForm->getBuilder(
			$rep->getBillingInfoById($id),
			Admin_Form_AddressForm::ADDRESS_MODE_BILLING,
			'address.billing_address',
			$hideFields
		);

		$customerForm = new Admin_Form_CustomerForm($addressForm, $this->getDb(), $this->getSettings());

		$defaults = $rep->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $customerForm->getForm($user, $mode, $id);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			// check last update
			$formData['last_update'] = $last_update != '0000-00-00 00:00:00' ? $last_update : $formData['last_update'];
			
			if (!Nonce::verify($formData['nonce'], 'user_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('customers');
			}
			else
			{
				if (($validationErrors = $rep->getValidationErrors($formData, $mode, $id)) == false)
				{
					$rep->persist($formData);
					Admin_Flash::setMessage('common.success');
					Admin_Log::log('Customers: Customer updated - '.$rep->getUsername($id), Admin_Log::LEVEL_IMPORTANT);
					$this->redirect('customer', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$billingAddress = $formData['billing_address']; // array
					$formData['billing_address[address1]'] = $billingAddress['address1'];
					$formData['billing_address[address2]'] = $billingAddress['address2'];
					$formData['billing_address[city]'] = $billingAddress['city'];
					$formData['billing_address[state]'] = isset($billingAddress['state']) ? $billingAddress['state'] : '';
					if (isset($billingAddress['province']))
					{
						$formData['billing_address[province]'] = $billingAddress['province'];
					}
					$formData['billing_address[zip]'] = $billingAddress['zip'];
					$formData['billing_address[country]'] = $billingAddress['country'];

					$formData['address_book[0][company]'] = $formData['address_book'][0]['company'];
					$formData['address_book[0][address2]'] = $formData['address_book'][0]['address2'];

					$form->bind($formData);
					$form->bindErrors($validationErrors);
					$list = '';
					foreach ($validationErrors as $errors)
					{
						foreach ($errors as $msg)
						{
							$list .= '<li>' . $msg . '</li>';
						}
					}
					if ($list != '')
					{
						$list = '<ul>' . $list . '</ul>';
					}
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '2001');
		$adminView->assign('title', ($user['fname'] . " " . $user['lname']));

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('customer', $user);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('user_update'));


		$customFields = new CustomFields($this->db);
		$userShippingAddresses = DataAccess_UserRepository::getInstance()->getAllUserShippingAddresses($id);
		foreach ($userShippingAddresses as &$address)
		{
			$address['custom_fields'] = $customFields->GetCustomFieldsValues('shipping', $id, 0, $address['usid']);
		}

		$adminView->assign('userShippingAddresses', $userShippingAddresses);

		$countriesRepository = new DataAccess_CountryRepository($this->db);
		$countriesStates = $countriesRepository->getCountriesStatesForSettings();
		$adminView->assign('countriesStates', json_encode($countriesStates));

		$paymentMethodId = $paymentProfileRepository->getPaymentProfileEnabledPaymentMethodId();
		$paymentProfile = new PaymentProfiles_Model_PaymentProfile();
		$paymentProfile->setPaymentMethodId($paymentMethodId);
		$paymentProfile->setUserId($id);

		$paymentProfileForm = new PaymentProfiles_Admin_Form_PaymentProfileForm($settings);
		$adminView->assign('paymentProfileForm', $paymentProfileForm->getForm($paymentProfile));
		$adminView->assign('paymentProfilePaymentMethodId', $paymentMethodId);
		$adminView->assign('paymentProfiles', PaymentProfiles_View_PaymentProfileView::getPaymentProfilesView($paymentProfileRepository->getByUserId($id)));

		$adminView->assign('body', self::TEMPLATES_PATH . 'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Lock Customer Account
	 */
	public function lockAction()
	{
		$request = $this->getRequest();

		$rep = $this->getDataRepository();

		$id = intval($request->get('id'));
		$user = $rep->getById($id);

		if (!$user)
		{
			Admin_Flash::setMessage('Cannot find customer by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('customers');
		}

		$settings = $this->getSettings();
		$rep->lockAccount($id, $settings->get('SecurityAccountBlockingHours'));

		Admin_Flash::setMessage('Account has been locked!');
		Admin_Log::log('Customers: Customer locked - '.$rep->getUsername($id), Admin_Log::LEVEL_IMPORTANT);
		$this->redirect('customer', array('id' => $id, 'mode' => self::MODE_UPDATE));
	}

	/**
	 * Unlock Customer Account
	 */
	public function unlockAction()
	{
		$request = $this->getRequest();

		$rep = $this->getDataRepository();

		$id = intval($request->get('id'));
		$user = $rep->getById($id);

		if (!$user)
		{
			Admin_Flash::setMessage('Cannot find customer by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('customers');
		}

		$rep->unlockAccount($id);

		Admin_Flash::setMessage('Account has been unlocked!');
		Admin_Log::log('Customers: Customer unlocked - '.$rep->getUsername($id), Admin_Log::LEVEL_IMPORTANT);
		$this->redirect('customer', array('id' => $id, 'mode' => self::MODE_UPDATE));
	}

	/*
	 * delete customer
	*/
	public function deleteAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'customer_delete')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('customers');
			return;
		}

		$rep = $this->getDataRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = intval($request->get('id', 0));
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid customer id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($rep->delete($id))
				{
					Admin_Flash::setMessage('Customer has been deleted');
					Admin_Log::log('Customers: Customer deleted - '.$rep->getUsername($id), Admin_Log::LEVEL_IMPORTANT);
				}
				else
				{
					Admin_Flash::setMessage('Cannot delete selected customer.', Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one customer to delete', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($rep->delete($ids))
				{
					Admin_Flash::setMessage('Selected customers have been deleted');
					foreach($ids as $id){
						Admin_Log::log('Customers: Customer deleted - '.$rep->getUsername($id), Admin_Log::LEVEL_IMPORTANT);
					}
				}
				else
				{
					Admin_Flash::setMessage('Cannot delete one or more customers.', Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else if ($deleteMode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('customersSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			if ($rep->deleteBySearchParams($searchParams))
			{
				Admin_Flash::setMessage('common.success');
			}
			else
			{
				Admin_Flash::setMessage('Cannot delete one or more customers.', Admin_Flash::TYPE_ERROR);
			}
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('customers');
	}

	/**
	 * export subscribers
	 **/
	public function exportSubscribersAction()
	{
		$exportService = $this->getSubscribersExportService();
		$exportService->export($this->subscribersExportFileName);
	}

	public function exportAction()
	{
		$request = $this->getRequest();

		$exportMode = $request->get('exportMode');

		$exportService = $this->getCustomersExportService();

		if ($exportMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one customer to export', Admin_Flash::TYPE_ERROR);
				$this->redirect('customers');
			}
			else
			{
				$exportService->export($this->customersExportFileName, array('mode' => 'selected', 'ids' => $ids));
			}
		}
		else if ($exportMode == 'all')
		{
			$exportService->export($this->customersExportFileName, array('mode' => 'all'));
		}
	}

	/**
	 * @return Admin_Service_SubscribersExport|null
	 */
	protected function getSubscribersExportService()
	{
		if (is_null($this->subscribersExportService))
		{
			$this->subscribersExportService = new Admin_Service_SubscribersExport($this->getDb());
		}

		return $this->subscribersExportService;
	}

	/**
	 * @return Admin_Service_SubscribersExport|null
	 */
	protected function getCustomersExportService()
	{
		if (is_null($this->customersExportService))
		{
			$this->customersExportService = new Admin_Service_CustomersExport($this->getDb());
		}

		return $this->customersExportService;
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CustomersRepository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->repository))
		{
			$this->repository = new DataAccess_CustomersRepository($this->getDb(), $this->getSettings());
		}

		return $this->repository;
	}

	/**
	 * Get payment profile repository
	 *
	 * @return PaymentProfiles_DataAccess_PaymentProfileRepository
	 */
	protected function getPaymentProfileRepository()
	{
		if (is_null($this->paymentProfileRepository))
		{
			$this->paymentProfileRepository = new PaymentProfiles_DataAccess_PaymentProfileRepository($this->getDb());
		}

		return $this->paymentProfileRepository;
	} 

}

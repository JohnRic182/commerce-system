<?php

/**
 * Class Admin_Controller_CheckoutSettings
 */
class Admin_Controller_CheckoutSettings extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$defaults = array(
			'VisitorSeePrice' => 'YES',
			'VisitorMayAddItem' => 'NO',
			'AllowCreateAccount' => '',
			'MinOrderNumber' => '',
			'AfterProductAddedGoTo' => '',
			'AfterContinueShoppingClickGoTo' => '',
			'DisplayTermsAndConditionsCheckbox' => 'No',
			'AlwaysDisplayTermsAndConditionsCheckbox' => 'No',
			'ReceivesMarketingDefault' => 'No',
			'EnableWishList' => 'No',
			'ClearCartOnLogout' => 'No',
			'CardTypeMustMatchNumber' => 'No',
			'InventoryStockUpdateAt' => '',
			'AllowOutOfInventoryOrders' => 'No',
			'OrderShippingAsBillingDefaultOption' => 'No',
			'GiftCardActive' => 'NO',
			'GiftCardMessageLength' => '',
			'MinOrderSubtotalLevel0' => '0',
			'FormsBillingCompany' => 'Notrequired',
			'FormsBillingAddressLine2' => 'Notrequired',
			'FormsBillingPhone' => 'Required',
			'FormsShippingCompany' => 'Notrequired',
			'FormsShippingAddressLine2' => 'Notrequired',
			'DisplayStockOnProductPage'	=> 'NO',
			'DisplayStockOnProductPageThreshold' => '10',
		);

		$data = $settings->getByKeys(array_keys($defaults));

		$checkoutSettingsForm = new Admin_Form_CheckoutSettingsForm();
		$form = $checkoutSettingsForm->getForm($data);

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings');
			}
			else
			{
				$validationErrors = array();
				$formData = array();
				foreach ($data as $key => $temp)
				{
					$formData[$key] = trim($request->request->get($key, array_key_exists($key, $defaults) ? $defaults[$key] : ''));

					/** @var core_Form_Field $field */
					$field = $checkoutSettingsForm->getBasicGroup()->get($key);

					if ($field && is_array($field) && isset($field['options']['required']) && $field['options']['required'] && $formData[$key] == '')
					{
						$validationErrors[$key] = array(trans('common.please_enter', array('field', $field['options']['label'])));
					}
				}

				if (count($validationErrors) == 0)
				{
					$settings->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('checkout');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8051');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('settings_update'));

		$adminView->assign('body', 'templates/pages/settings/checkout-settings.html');
		$adminView->render('layouts/default');
	}
}
<?php

/**
 * Class Admin_Controller_WholesaleSettings
 */
class Admin_Controller_WholesaleSettings extends Admin_Controller
{
	/** @var array $menuItem */
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/** @var DataAccess_TestimonialRepository  */
	protected $Repository = null;

	/**
	 * Update catalog settings
	 */
	public function updateAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_SettingsRepository $settings */
		$settings = $this->getSettings();

		$defaults = array(
			'WholesaleDiscountType' => '',
			'WholesaleMaxLevels' => '',
			'WholesaleDiscount1' => '',
			'WholesaleDiscount2' => '',
			'WholesaleDiscount3' => '',
			'MinOrderSubtotalLevel1' => '',
			'MinOrderSubtotalLevel2' => '',
			'MinOrderSubtotalLevel3' => '',
		);

		$data = $settings->getByKeys(array_keys($defaults));

		$wholesaleSettingsForm = new Admin_Form_WholesaleSettingsForm();
		$form = $wholesaleSettingsForm->getForm($data);

		$repository = $this->getDataRepository();

		/**
		 * Process request
		 */
		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');

			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($nonce, 'settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings', array('group' => 12));
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData)) == false)
				{
					$settings->persist($formData, array_keys($defaults));

					Admin_Flash::setMessage('common.success');
					$form->bind($formData);
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8039');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('settings_update'));

		$adminView->assign('body', 'templates/pages/settings/wholesale-settings.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_WholesaleSettingsRepository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->Repository)){
			$this->Repository = new DataAccess_WholesaleSettingsRepository($this->getDb(), $this->getSettings()->getAll());
		}

		return $this->Repository;
	}
}
<?php

/**
 * Class Admin_Controller_CardinalCentinel
 */
class Admin_Controller_CardinalCentinel extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'cardinalcommerce_URL',
			'cardinalcommerce_Processor_Id',
			'cardinalcommerce_Merchant_Id',
			'cardinalcommerce_Password'
		);

		$form = $this->getSettingsForm($settings->getByKeys($settingsVars));

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'shopzilla'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			if (!($validationErrors = $this->getSettingsErrors($settingsData)))
			{
				$settings->persist($settingsData, $settingsVars);
				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'cardinal-centinel'));
			}
			else
			{
				$form->bind($settingsData);
				$_errors = array();
				foreach ($validationErrors as $error) $_errors[str_replace('.field-', '', $error['field'])][] = $error['message'];
				$form->bindErrors($_errors);
			}
		}

		$adminView = $this->getView();
		$adminView->assign('form', $form);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9031');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$adminView->assign('body', 'templates/pages/cardinal-centinel/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Active form action
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'cardinalcommerce_Enabled',
			'cardinalcommerce_URL',
			'cardinalcommerce_Processor_Id',
			'cardinalcommerce_Merchant_Id',
			'cardinalcommerce_Password'
		);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			if (!($errors = $this->getSettingsErrors($settingsData)))
			{
				$settingsData['cardinalcommerce_Enabled'] = 'Yes';
				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('cardinal-centinel');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$adminView = $this->getView();
		$adminView->assign('form', $form = $this->getSettingsForm($settings->getByKeys($settingsVars)));

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	/**
	 * @param $data
	 * @return core_Form_Form
	 */
	protected function getSettingsForm($data)
	{
		$cardinalCentinelForm = new Admin_Form_CardinalCentinelForm();
		return $cardinalCentinelForm->getForm($data);
	}

	/**
	 * @param $data
	 * @return array|bool
	 */
	protected function getSettingsErrors($data)
	{
		$errors = array();

		if (!isset($data['cardinalcommerce_URL']) || trim($data['cardinalcommerce_URL']) == '')
		{
			$errors[] = array('field' => '.field-cardinalcommerce_URL', 'message' => trans('apps.cardinal_centinel.url_required'));
		}

		if (!isset($data['cardinalcommerce_Processor_Id']) || trim($data['cardinalcommerce_Processor_Id']) == '')
		{
			$errors[] = array('field' => '.field-cardinalcommerce_Processor_Id', 'message' => trans('apps.cardinal_centinel.processor_id_required'));
		}

		if (!isset($data['cardinalcommerce_Merchant_Id']) || trim($data['cardinalcommerce_Merchant_Id']) == '')
		{
			$errors[] = array('field' => '.field-cardinalcommerce_Merchant_Id', 'message' => trans('apps.cardinal_centinel.merchant_id_required'));
		}

		if (!isset($data['cardinalcommerce_Password']) || trim($data['cardinalcommerce_Password']) == '')
		{
			$errors[] = array('field' => '.field-cardinalcommerce_Password', 'message' => trans('apps.cardinal_centinel.password_required'));
		}

		return count($errors) ? $errors : false;
	}

	/**
	 * Deactivate settings
	 */
	public function deactivateAction()
	{
		$this->getSettings()->save(array('cardinalcommerce_Enabled' => 'No'));
	}
}
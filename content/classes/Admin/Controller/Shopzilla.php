<?php

/**
 * Class Admin_Controller_Shopzilla
 */
class Admin_Controller_Shopzilla extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('ShopzillaMerchantId', 'ShopzillaROITracker', 'SurveyInvitation');

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'shopzilla'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			if (!isset($settingsData['ShopzillaROITracker'])) $settingsData['ShopzillaROITracker'] = 'NO';
			if (!isset($settingsData['SurveyInvitation'])) $settingsData['SurveyInvitation'] = 'NO';

			$settings->persist($settingsData, $settingsVars);

			Admin_Flash::setMessage('common.success');
			$this->redirect('app', array('key' => 'shopzilla'));
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$shopzillaForm = new Admin_Form_ShopzillaForm();
		$form = $shopzillaForm->getForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9018');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$adminView->assign('body', 'templates/pages/shopzilla/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Active form action
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('ShopzillaMerchantId');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();

			if (!isset($settingsData['ShopzillaMerchantId']) || trim($settingsData['ShopzillaMerchantId']) == '')
			{
				$errors[] = array('field' => 'field-settings-ShopzillaMerchantId', 'message' => trans('apps.shopzilla.merchant_id_number_required'));
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('shopzilla');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$shopzillaForm = new Admin_Form_ShopzillaForm();
		$form = $shopzillaForm->getShopzillaActivateForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	/**
	 * @param $s
	 * @return string
	 */
	protected function exportNormalize($s)
	{
		return htmlentities(trim(stripslashes(str_replace(array("\r", "\n", "\t", "\0"), array(' ', ' ', ' ', ' '), $s))));
	}

	/**
	 * Export Shopzilla products
	 */
	public function exportAction()
	{
		@set_time_limit(10000);

		$settings = $this->getSettings();

		$db = $this->getDb();

		$noImageUrl = DesignElements::getImage('image-no-image-big');

		/**
		 * Set headers
		 */
		header('Content-Type: text/plain');
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="shopzilla.txt"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Content-Disposition: attachment; filename="shopzilla.txt"');
		header('Pragma: no-cache');

		// TODO: move into repository?

		/**
		 * Select products
		 */
		$productsResult = $db->query('
			SELECT p.*,
				IF(p.url_custom = "", p.url_default, p.url_custom) AS product_url,
				c.name AS cat_name, c.category_path AS category_path,
				m.manufacturer_name AS manufacturer_name
			FROM '.DB_PREFIX.'products AS p
			INNER JOIN '.DB_PREFIX.'catalog AS c ON p.cid = c.cid AND c.is_visible="Yes"
			LEFT OUTER JOIN '.DB_PREFIX.'manufacturers AS m ON m.manufacturer_id = p.manufacturer_id
			WHERE p.is_visible="Yes" AND call_for_price="No"
			ORDER BY p.title
		');

		echo "Category\tMfr\tTitle\tDescription\tLINK\tImage\tSKU\tStock\tCondition\tShip Weight\tShip Cost\tBid\tPromo Des.\tOther\tprice\r\n";

		while ($productData = $db->moveNext($productsResult))
		{
			$productUrl = UrlUtils::getProductUrl($productData['product_url'], $productData['pid'], $productData['cid']);
			$productName = substr($this->exportNormalize($productData['title']), 0, 80);
			$productDescription = substr($this->exportNormalize(strip_tags($productData['description'])), 0, 1000);
			$manufacturerName = trim($productData['manufacturer_name']);
			$shippingPrice = $productData['shipping_price'] > 0 ? $productData['shipping_price'] : '';
			$shippingWeight = $productData['weight'] > 0 ? $productData['weight'] : '';
			$stock = $productData['inventory_control'] != 'No' ? ($productData['stock'] > 0 ? $productData['stock'] : 'Out of Stock') : 'In Stock';
			$condition = '';
			$bid = '';
			$promo = '';
			$other = '';
			$price = $productData['price'];
			$imageUrl = '';

			// TODO: Refactor - use image repo?

			$imageTypes = array('jpg', 'gif', 'jpeg', 'png');

			if ($productData['image_location'] == 'Web' && $productData['image_url'] != '')
			{
				$imageUrl = $db->col['image_url'];
			}
			else
			{
				foreach ($imageTypes as $ext)
				{
					if (file_exists($settings->get('GlobalServerPath').'/images/products/'.escapeFileName(strtolower($productData['product_id'])).'.'.$ext))
					{
						$imageUrl = $this->exportNormalize($settings->get('GlobalHttpUrl').'/images/products/'.escapeFileName(strtolower($productData['product_id'])).'.'.$ext); //normalize again
						break;
					}
					elseif (file_exists($settings->get('GlobalServerPath').'/images/products/'.escapeFileName(strtolower($productData['product_id'])).'.'.$ext))
					{
						$imageUrl = $this->exportNormalize($settings->get('GlobalHttpUrl').'/images/products/'.escapeFileName(strtolower($productData["product_id"])).'.'.$ext);
						break;
					}
				}
			}

			$imageUrl = trim($imageUrl) == '' ? $this->exportNormalize($settings->get('GlobalHttpUrl').'/'.$noImageUrl) : $imageUrl;

			$category = str_replace(array('>', '/'), array(' ', ' > '), $productData['category_path']);

			$productId = $productData['product_id'];

			echo
				$category."\t".
				$manufacturerName."\t".
				$productName."\t".
				$productDescription."\t".
				$productUrl."\t".
				$imageUrl."\t".
				$productId."\t".
				$stock."\t".
				$condition."\t".
				$shippingWeight."\t".
				$shippingPrice."\t".
				$bid."\t".
				$promo."\t".
				$other."\t".
				number_format($price, 2, '.', '').
				"\r\n";
		}

		$this->shutDown();
	}
}
<?php

/**
 * Class Admin_Controller_FormsFields
 */
class Admin_Controller_FormsFields extends Admin_Controller
{
    const MODE_ADD = 'customfieldsadd';
    const MODE_UPDATE = 'customfieldsupdate';
    const TEMPLATES_PATH = 'templates/pages/forms-fields/';

    const NOTICE_ADD_CUSTOM_FIELDS = 'customfields_add';
    const NOTICE_DELETE_CUSTOM_FIELDS = 'customfields_delete';
    const NOTICE_EDIT_CUSTOM_FIELDS = 'customfields_update';

    protected $customFieldRepository = null;
    protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

    /**
     * Custom fields list action
     */
    public function customFieldsListAction()
    {
        $adminView = $this->getView();

        $adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8028');

        $repository = $this->getCustomFieldRepository();

        /** @var Framework_Request $request */
        $request = $this->getRequest();
        $orderBy = $request->get('orderBy', 'field_place, priority');

        $itemsCount = $repository->getCustomFieldsCount();

        $adminView->assign('itemsCount', $itemsCount);

        if ($itemsCount)
        {
            $paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
            $adminView->assign('paginator', $paginator);

            $fieldsList = $repository->getCustomFieldsList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy);
            $TypeOptions = $repository->getCustomFieldTypeOptions();
            $SectionOptions = $repository->getCustomFieldSectionOptions();

            foreach($fieldsList as &$field)
            {
                $field['field_place'] = isset($SectionOptions[$field['field_place']]) ? $SectionOptions[$field['field_place']] : null;
                $field['field_type'] = $TypeOptions[$field['field_type']];
            }

            $adminView->assign('customfields', $fieldsList);
        }
        else
        {
            $adminView->assign('customfields', null);
        }

        $adminView->assign('delete_nonce', Nonce::create(self::NOTICE_DELETE_CUSTOM_FIELDS));

        $adminView->assign('body', self::TEMPLATES_PATH.'customfields/list.html');
        $adminView->render('layouts/default');
    }

    /**
     * Custom fields add action
     */
    public function customFieldsAddAction()
    {
        $mode = self::MODE_ADD;

        $request = $this->getRequest();

        $repository = $this->getCustomFieldRepository();

        $customFieldsForm = new Admin_Form_FormsFieldsCustomFieldsForm();

        $defaults = $repository->getDefaults();
	    $defaults['is_active'] = 1;
        $defaults['customFieldTypeOptions'] = $repository->getCustomFieldTypeOptions();
        $defaults['customFieldSectionOptions'] = $repository->getCustomFieldSectionOptions();
        $defaults['customFieldPositionOptions'] = $repository->getCustomFieldPositionOptions();

        $form = $customFieldsForm->getForm($defaults, $mode);

        /**
         * Handle form post
         */
        if ($request->isPost())
        {
            $formData = array_merge($defaults, $request->request->all());

            if (!Nonce::verify($formData['nonce'], self::NOTICE_ADD_CUSTOM_FIELDS))
            {
                Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
                $this->redirect('forms_control');
            }
            else
            {
                if (($validationErrors = $repository->getValidationErrors($formData)) == false)
                {
                    $id = $repository->persist($formData);

                    Admin_Flash::setMessage('common.success');
                    $this->redirect('forms_control', array('mode' => self::MODE_UPDATE, 'id' => $id));
                }
                else
                {
                    $form->bind($formData);
                    $form->bindErrors($validationErrors);
                }
            }
        }

        /** @var Ddm_AdminView $adminView */
        $adminView = $this->getView();

        // set menu
        $adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8029');

        $adminView->assign('mode', $mode);
        $adminView->assign('id', null);
        $adminView->assign('form', $form);
        $adminView->assign('nonce', Nonce::create(self::NOTICE_ADD_CUSTOM_FIELDS));

        $adminView->assign('body', self::TEMPLATES_PATH.'customfields/edit.html');
        $adminView->render('layouts/default');
    }

    /**
     * Custom fields edit action
     */
    public function customFieldsEditAction()
    {
        $mode = self::MODE_UPDATE;

        $request = $this->getRequest();

        $repository = $this->getCustomFieldRepository();

        $id = intval($request->get('id'));

        $data = $repository->getById($id);

        if (!$data)
        {
            Admin_Flash::setMessage('Cannot find custom field by provided id', Admin_Flash::TYPE_ERROR);
            $this->redirect('forms_control');
        }

        $customFieldsForm = new Admin_Form_FormsFieldsCustomFieldsForm();

        $defaults = $repository->getDefaults();
        $data['customFieldTypeOptions'] = $repository->getCustomFieldTypeOptions();
        $data['customFieldSectionOptions'] = $repository->getCustomFieldSectionOptions();
        $data['customFieldPositionOptions'] = $repository->getCustomFieldPositionOptions();

        $form = $customFieldsForm->getForm($data, $mode);

        /**
         * Handle form post
         */
        if ($request->isPost())
        {
            $formData = array_merge($defaults, $request->request->all());

            if (!Nonce::verify($formData['nonce'], self::NOTICE_EDIT_CUSTOM_FIELDS))
            {
                Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
                $this->redirect('forms_control');
            }
            else
            {
                if (($validationErrors = $repository->getValidationErrors($formData)) == false)
                {
                    $id = $repository->persist($formData);
                    Admin_Flash::setMessage('common.success');
                    $this->redirect('forms_control', array('mode' => self::MODE_UPDATE, 'id' => $id));
                }
                else
                {
                    $form->bind($formData);
                    $form->bindErrors($validationErrors);
                    Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
                }
            }
        }

        $adminView = $this->getView();

        // set menu
        $adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8030');
	    $adminView->assign('title', $data['field_name']);

        $adminView->assign('mode', $mode);
        $adminView->assign('id', $id);
        $adminView->assign('form', $form);
        $adminView->assign('nonce', Nonce::create(self::NOTICE_EDIT_CUSTOM_FIELDS));

        $adminView->assign('body', self::TEMPLATES_PATH.'customfields/edit.html');
        $adminView->render('layouts/default');
    }

    /**
     * Custom fields delete action
     */
    public function customFieldsDeleteAction()
    {
        $request = $this->getRequest();

        $repository = $this->getCustomFieldRepository();

        $deleteMode = $request->get('deleteMode');

        if (!Nonce::verify($request->get('nonce'), self::NOTICE_DELETE_CUSTOM_FIELDS))
        {
            Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
            $this->redirect('forms_control',array('mode' => 'customfieldslist'));
            return;
        }

        if ($deleteMode == 'single')
        {
            $id = $request->get('id', 0);
            
            if ($id < 1)
            {
                Admin_Flash::setMessage('Invalid custom field id', Admin_Flash::TYPE_ERROR);
            }
            else
            {
                $repository->delete($id);
                Admin_Flash::setMessage('common.success');
                $this->redirect('forms_control',array('mode' => 'customfieldslist'));
            }
        }
        else if ($deleteMode == 'selected')
        {
            $ids = explode(',', $request->get('ids', ''));

            if (count($ids) == 0)
            {
                Admin_Flash::setMessage('Please select at least one custom field to delete', Admin_Flash::TYPE_ERROR);
                $this->redirect('forms_control',array('mode' => 'customfieldslist'));
            }
            else
            {
                $repository->delete($ids);
                Admin_Flash::setMessage('Selected custom fields have been deleted');
                $this->redirect('forms_control',array('mode' => 'customfieldslist'));
            }
        }
        else if ($deleteMode == 'all')
        {
	        $repository->delete('all');
            Admin_Flash::setMessage('All custom fields have been deleted');
            $this->redirect('forms_control',array('mode' => 'customfieldslist'));
        }
    }

    /**
     * Get repository
     *
     * @return DataAccess_CustomFieldRepository
     */
    protected function getCustomFieldRepository()
    {
        if (is_null($this->customFieldRepository))
        {
            $this->customFieldRepository = new DataAccess_CustomFieldRepository($this->getDb());
        }

        return $this->customFieldRepository;
    }
}
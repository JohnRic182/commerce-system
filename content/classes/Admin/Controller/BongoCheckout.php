<?php

class Admin_Controller_BongoCheckout extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'bongocheckout_Shipping_Method', 'bongocheckout_Shipping_Country_Group',
			'bongocheckout_Company_Name', 'bongocheckout_Attention_To',
			'bongocheckout_Address1', 'bongocheckout_Address2', 'bongocheckout_City',
			'bongocheckout_State', 'bongocheckout_Zip_Code', 'bongocheckout_Phone',
		);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'bongo-checkout'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			$settings->persist($settingsData, $settingsVars);

			Admin_Flash::setMessage('common.success');
			$this->redirect('app', array('key' => 'bongo-checkout'));
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9013');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$db = $this->getDb();
		$domesticShippingMethods = $db->selectAll('
			SELECT ssid, CASE WHEN carrier_id = "custom" THEN carrier_name ELSE method_name END as method_name
			FROM '.DB_PREFIX.'shipping_selected
			WHERE
				method_id <> "product_level"
				AND
				(country="0" OR country="1" OR country LIKE "1,%" OR country LIKE "%,1,%" OR country LIKE "%,1")
				AND
				(country_exclude IS NULL OR (country_exclude<>"1" AND country_exclude NOT LIKE "1,%" AND country_exclude NOT LIKE "%,1,%" AND country_exclude NOT LIKE "%,1"))
			ORDER BY priority
		');

		$settingsData = $settings->getByKeys($settingsVars);

		$options = array(
			'auto' => 'Auto-select cheapest one',
		);
		foreach ($domesticShippingMethods as $method)
		{
			$options[$method['ssid']] = $method['method_name'];
		}
		$settingsData['shippingMethodOptions'] = $options;

		// TODO: countries groups are deprecated. remove this option?
		$countriesGroups = $db->selectAll('select country_group_id, name from '.DB_PREFIX.'countries_groups where is_available = 1');
		if (count($countriesGroups) > 0)
		{
			$options = array(
				'all' => 'All Countries',
			);
			foreach ($countriesGroups as $countriesGroup)
			{
				$options[$countriesGroup['country_group_id']] = $countriesGroup['name'];
			}
		}
		$settingsData['shippingCountryGroupOptions'] = $options;

		$options = array();
		$countryRepository = new DataAccess_CountryRepository($this->getDb());
		$country = $countryRepository->getCountriesStates(1);
		if (isset($country['states']))
		{
			foreach ($country['states'] as $state)
			{
				$options[$state['id']] = $state['name'];
			}
		}
		$settingsData['bongoCheckoutStateOptions'] = $options;

		$bongoForm = new Admin_Form_BongoCheckoutForm();
		$form = $bongoForm->getForm($settingsData);

		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/bongo-checkout/index.html');
		$adminView->render('layouts/default');
	}

	public function activateFormAction()
	{
		$db = $this->getDb();

		$gateway = $db->selectOne('SELECT * FROM '.DB_PREFIX.'payment_methods WHERE id = "bongocheckout"');
		$gateway_url = $gateway['url_to_gateway'];

		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('bongocheckout_Partner_Key');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = array(
				'bongocheckout_Partner_Key' => $request->request->get('bongocheckout_Partner_Key')
			);

			$gateway_url = $request->request->get('bongocheckout_Gateway_Url');

			if (
				isset($settingsData['bongocheckout_Partner_Key']) && trim($settingsData['bongocheckout_Partner_Key']) != '' &&
				trim($gateway_url) != ''
			)
			{
				$settings->persist($settingsData, $settingsVars);

				$db->query('UPDATE '.DB_PREFIX.'payment_methods SET url_to_gateway = "'.$db->escape($gateway_url).'", active = "Yes" WHERE id = "bongocheckout"');

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('bongo-checkout');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$errors = array();
				if (!isset($settingsData['bongocheckout_Partner_Key']) || trim($settingsData['bongocheckout_Partner_Key']) == '')
				{
					$errors[] = array('field' => '.field-bongocheckout_Partner_Key', 'message' => trans('apps.bongo_checkout.partner_key_required'));
				}
				if (trim($gateway_url) == '')
				{
					$errors[] = array('field' => '.field-bongocheckout_Gateway_Url', 'message' => trans('apps.bongo_checkout.gateway_url_required'));
				}

				$this->renderJson(array(
					'status' => 0,
					'errors' => $errors,
				));
			}
		}

		$data = $settings->getByKeys($settingsVars);
		$data['gateway_url'] = $gateway_url;

		$bongoForm = new Admin_Form_BongoCheckoutForm();
		$form = $bongoForm->getBongoActivateForm($data);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	public function deactivateAction()
	{
		$db = $this->getDb();

		$db->query('UPDATE '.DB_PREFIX.'payment_methods SET active = "Disabled" WHERE id = "bongocheckout"');
	}


	public function exportAction()
	{
		$request = $this->getRequest();

		$bongoExport = $this->getBongoExport();
		$exportAllProducts = $request->get('form-bongo-export-mode') == 'all';

		$bongoExport->exportCsv($exportAllProducts);
	}

	public function syncStartAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();
		$exportAllProducts = $request->get('form-bongo-sync-mode') == 'all';

		$bongoExport = $this->getBongoExport();

		$session->set('bongo_categories', $bongoExport->getCategories());
		$session->set('bongo_count_with_subids', $bongoExport->getProductsCountWithSubids($exportAllProducts));
		$session->set('bongo_count_with_no_subids', $bongoExport->getProductsCountWithNoSubids($exportAllProducts));
		$session->set('bongo_done_with_subids', 0);
		$session->set('bongo_done_with_no_subids', 0);

		$count = $session->get('bongo_count_with_subids') + $session->get('bongo_count_with_no_subids');

		$this->renderJson(array('result' => 1, 'count' => $count));
	}

	public function syncBatchAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();
		$exportAllProducts = $request->get('form-bongo-sync-mode') == 'all';

		$offset = intval($request->get('offset'));

		$bongoExport = $this->getBongoExport();

		if ($session->get('bongo_done_with_subids') < $session->get('bongo_count_with_subids'))
		{
			$result = $bongoExport->getProductsWithSubids($exportAllProducts, $offset, 10);
			$c = $bongoExport->synch($result, $session->get('bongo_categories'));
			$session->set('bongo_done_with_subids', $session->get('bongo_done_with_subids') + $c);
		}
		else if ($session->get('bongo_done_with_no_subids') < $session->get('bongo_count_with_no_subids'))
		{
			$result = $bongoExport->getProductsWithNoSubids($exportAllProducts, $offset - $session->get('bongo_count_with_subids'), 10);
			$c = $bongoExport->synch($result, $session->get('bongo_categories'));
			$session->set('bongo_done_with_no_subids', $session->get('bongo_done_with_no_subids') + $c);
		}

		$processed = $session->get('bongo_done_with_subids') + $session->get('bongo_done_with_no_subids');
		$toProcess = ($session->get('bongo_count_with_subids') + $session->get('bongo_count_with_no_subids')) - $processed;
		if ($toProcess <= 0)
		{
			$session->remove('bongo_categories');
			$session->remove('bongo_count_with_subids');
			$session->remove('bongo_count_with_no_subids');
			$session->remove('bongo_done_with_subids');
			$session->remove('bongo_done_with_no_subids');
		}

		$this->renderJson(array('result' => 1, 'processed'=> $processed, 'toProcess' => $toProcess));
	}

	protected function getBongoExport()
	{
		$db = $this->getDb();
		$settings = $this->getSettings()->getAll();

		return new Payment_Bongo_BongoExport($db, $settings);
	}

	protected function getSettingsForm()
	{
		$settings = $this->getSettings();

		$taxSettingsVars = array('TaxAddress', 'TaxDefaultCountry', 'TaxDefaultState', 'TaxExemptionsActive');

		$data = $settings->getByKeys($taxSettingsVars);

		/** @var Admin_Form_TaxAdvancedSettingsForm */
		$TRForm = new Admin_Form_TaxAdvancedSettingsForm();

		/** @var core_Form_Form $form */
		return $TRForm->getForm($data);
	}
}
<?php
/**
 * Class Admin_Controller_Apps
 */
class Admin_Controller_Apps extends Admin_Controller
{
	/** @var DataAccess_AppRepository $appRepository */
	protected $appRepository = null;

	/** @var Admin_Service_Billing $billingService */
	protected $billingService = null;

	protected $menuItem = array('primary' => 'apps', 'secondary' => '');
	protected $appCenterMenuItem = array('primary' => 'apps', 'secondary' => 'appcenter');
	protected $activeAppsMenuItem = array('primary' => 'apps', 'secondary' => 'active');

	protected $apps = array(
		'google-merchant' => array(
			'title' => 'Google Merchant Center',
			'icon' => 'google-merchant',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/google-merchant/content.html',
			'indexAction' => 'Admin_Controller_GoogleMerchant::indexAction',
			'help' => '9003',
		),
		'norton-seal' => array(
			'title' => 'Norton Shopping Guarantee',
			'icon' => 'norton-seal',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => true,
			'content' => 'templates/pages/norton-seal/content.html',
			'indexAction' => 'Admin_Controller_NortonSeal::indexAction',
			'activateFormUrl' => 'admin.php?p=norton_seal&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_NortonSeal::deactivateAction',
			'help' => '9050',
		),
		'pikfly' => array(
			'title' => 'PikFly Lite',
			'icon' => 'pikfly',
			'category' => 'shipping',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/pikfly/content.html',
			'indexAction' => 'Pikfly_Admin_Controller_Pikfly::indexAction',
			'activateAction' => 'Pikfly_Admin_Controller_Pikfly::activateAction',
			'deactivateAction' => 'Pikfly_Admin_Controller_Pikfly::deactivateAction',
			'help' => ''
		),
		'endicia' => array(
			'title' => 'Endicia',
			'icon' => 'endicia',
			'category' => 'shipping',
			'price' => 'paid',
			'activateForm' => true,
			'content' => 'templates/pages/endicia/content.html',
			'indexAction' => 'Admin_Controller_Endicia::indexAction',
			'activateFormUrl' => 'admin.php?p=endicia&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_Endicia::deactivateAction',
			'help' => '9016',
		),
		'stamps' => array(
			'title' => 'Stamps.com',
			'icon' => 'stamps',
			'category' => 'shipping',
			'price' => 'paid',
			'activateForm' => true,
			'content' => 'templates/pages/stamps/content.html',
			'indexAction' => 'Admin_Controller_Stamps::indexAction',
			'activateFormUrl' => 'admin.php?p=stamps&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_Stamps::deactivateAction',
			'help' => '9027',
		),
		'avalara' => array(
			'title' => 'Avalara',
			'icon' => 'avalara',
			'category' => 'taxes',
			'price' => 'paid',
			'activateForm' => true,
			'content' => 'templates/pages/avalara/content.html',
			'indexAction' => 'Admin_Controller_Avalara::indexAction',
			'activateFormUrl' => 'admin.php?p=avalara&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_Avalara::deactivateAction',
			'help' => '9021',
		),
		'exactor' => array(
			'title' => 'Exactor',
			'icon' => 'exactor',
			'category' => 'taxes',
			'price' => 'paid',
			'activateForm' => true,
			'content' => 'templates/pages/exactor/content.html',
			'indexAction' => 'Admin_Controller_Exactor::indexAction',
			'activateFormUrl' => 'admin.php?p=exactor&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_Exactor::deactivateAction',
			'help' => '9023',
		),
		'paypal-bml' => array(
			'title' => 'PayPal BillMeLater',
			'icon' => 'paypal-bml',
			'category' => 'payments',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/paypal-bml/content.html',
			'indexAction' => 'Admin_Controller_PayPalBillMeLater::indexAction',
			'activateAction' => 'Admin_Controller_PayPalBillMeLater::activateAction',
			'deactivateAction' => 'Admin_Controller_PayPalBillMeLater::deactivateAction',
			'help' => '9025',
		),
		'facebook' => array(
			'title' => 'Facebook Login',
			'icon' => 'facebook',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => true,
			'content' => 'templates/pages/facebook-login/content.html',
			'indexAction' => 'Admin_Controller_FacebookLogin::indexAction',
			'activateFormUrl' => 'admin.php?p=facebook_login&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_FacebookLogin::deactivateAction',
			'help' => '9017',
		),
		'idev' => array(
			'title' => 'iDev Affiliates',
			'icon' => 'idev',
			'category' => 'marketing',
			'price' => 'paid',
			'activateForm' => false,
			'content' => 'templates/pages/idev/content.html',
			'indexAction' => 'Admin_Controller_iDevAffiliate::indexAction',
			'activateFormUrl' => 'admin.php?p=affiliate&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_iDevAffiliate::deactivateAction',
			'help' => '9004',
		),
		'mailchimp' => array(
			'title' => 'Mailchimp',
			'icon' => 'mailchimp',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => true,
			'content' => 'templates/pages/mailchimp/content.html',
			'indexAction' => 'Admin_Controller_Mailchimp::indexAction',
			'activateFormUrl' => 'admin.php?p=mailchimp&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_Mailchimp::deactivateAction',
			'help' => '9005',
		),
		'quickbooks' => array(
			'title' => 'Connect to Quickbooks',
			'icon' => 'quickbooks',
			'category' => 'accounting',
			'price' => 'paid',
			'activateForm' => false,
			'content' => 'templates/pages/quickbooks/content.html',
			'preactivateAction' => 'Admin_Controller_Quickbooks::preActivateAction',
			'deactivateAction' => 'Admin_Controller_Quickbooks::deactivateAction',
			'canAccess' => 'Admin_Controller_Quickbooks::canAccess',
			'canActivate' => 'Admin_Controller_Quickbooks::canActivate',
			'indexAction' => 'Admin_Controller_Quickbooks::indexAction',
			'help' => '9026',
		),
		'webgility' => array(
			'title' => 'Webgility',
			'icon' => 'webgility',
			'category' => 'accounting',
			'price' => 'paid',
			'activateForm' => false,
			'content' => 'templates/pages/webgility/content.html',
			'indexAction' => 'Admin_Controller_Webgility::indexAction',
			'help' => '9015',
		),
		'doba' => array(
			'title' => 'Doba',
			'icon' => 'doba',
			'category' => 'marketing',
			'price' => 'paid',
			'activateForm' => true,
			'content' => 'templates/pages/doba/content.html',
			'activateFormUrl' => 'admin.php?p=doba&mode=activate-form',
			'indexAction' => 'Admin_Controller_Doba::indexAction',
			'deactivateAction' => 'Admin_Controller_Doba::deactivateAction',
			'help' => '9019',
		),
		'amazon' => array(
			'title' => 'Amazon Seller',
			'icon' => 'amazon',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/amazon-seller/content.html',
			'indexAction' => 'Admin_Controller_AmazonSeller::indexAction',
			'help' => '9006',
		),
		'yahoo' => array(
			'title' => 'Yahoo!',
			'icon' => 'yahoo',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/yahoo-export/content.html',
			'indexAction' => 'Admin_Controller_YahooExport::indexAction',
			'help' => '9007',
		),
		'ebay' => array(
			'title' => 'ebay Store',
			'icon' => 'ebay',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/ebay-store/content.html',
			'indexAction' => 'Admin_Controller_EbayStore::indexAction',
			'help' => '9008',
		),
		'goshopping' => array(
			'title' => 'Goshopping',
			'icon' => 'goshopping',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/go-shopping/content.html',
			'indexAction' => 'Admin_Controller_GoShopping::indexAction',
			'help' => '9009',
		),
		'pricegrabber' => array(
			'title' => 'PriceGrabber',
			'icon' => 'pricegrabber',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/price-grabber/content.html',
			'indexAction' => 'Admin_Controller_PriceGrabber::indexAction',
			'help' => '9010',
		),
		'nextag' => array(
			'title' => 'Nextag',
			'icon' => 'nextag',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/nex-tag/content.html',
			'indexAction' => 'Admin_Controller_NexTag::indexAction',
			'help' => '9011',
		),
		'shopzilla' => array(
			'title' => 'Shopzilla',
			'icon' => 'shopzilla',
			'category' => 'marketing',
			'price' => 'paid',
			'activateForm' => false,
			'activateFormUrl' => 'admin.php?p=shopzilla&mode=activate-form',
			'content' => 'templates/pages/shopzilla/content.html',
			'indexAction' => 'Admin_Controller_Shopzilla::indexAction',
			'help' => '9018',
		),
		'wholesale-central' => array(
			'title' => 'Wholesale Central',
			'icon' => 'wholesale-central',
			'category' => 'marketing',
			'price' => 'paid',
			'activateForm' => true,
			'activateFormUrl' => 'admin.php?p=wholesale_central&mode=activate-form',
			'content' => 'templates/pages/wholesale-central/content.html',
			'indexAction' => 'Admin_Controller_WholesaleCentral::indexAction',
			'help' => '9012',
		),
		'bongo-checkout' => array(
			'title' => 'Bongo Checkout',
			'icon' => 'bongo-checkout',
			'category' => 'marketing',
			'price' => 'paid',
			'activateFormUrl' => 'admin.php?p=bongo&mode=activate-form',
			'deactivateAction' => 'Admin_Controller_BongoCheckout::deactivateAction',
			'content' => 'templates/pages/bongo-checkout/content.html',
			'indexAction' => 'Admin_Controller_BongoCheckout::indexAction',
			'help' => '9013',
		),
		'bongo-extend' => array(
			'title' => 'Bongo Extend',
			'icon' => 'bongo-extend',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/bongo-extend/content.html',
			'indexAction' => 'Admin_Controller_BongoExtend::indexAction',
			'help' => '9014',
		),
		'recurring' => array(
			'title' => 'Recurring Billing',
			'icon' => 'recurring',
			'category' => 'payments',
			'price' => 'paid',
			'activateForm' => true,
			'content' => 'templates/pages/recurring-profiles/app-activate.html',
			'preactivateAction' => 'RecurringBilling_Admin_Controller_RecurringProfile::preActivateAction',
			'activateAction' => 'RecurringBilling_Admin_Controller_RecurringProfile::activateAction',
			'deactivateAction' => 'RecurringBilling_Admin_Controller_RecurringProfile::deactivateAction',
			'canAccess' => 'RecurringBilling_Admin_Controller_RecurringProfile::canAccessRecurringBilling',
			'canActivate' => 'RecurringBilling_Admin_Controller_RecurringProfile::canActivateRecurringBilling',
			'indexAction' => 'RecurringBilling_Admin_Controller_RecurringProfile::indexAction',
			'help' => '2002',
		),
		'filtered-menu' => array(
			'title' => 'Filtered Menu',
			'icon' => 'filtered-menu',
			'category' => 'catalog',
			'price' => 'paid',
			'canSubscribe' => true,
			'content' => 'templates/pages/product-feature-app/app-activate.html',
			'activateAction' => 'Admin_Controller_ProductsFeaturesApp::activateAction',
			'activateRedirectUrl' => 'admin.php?p=features_groups',
			'deactivateAction' => 'Admin_Controller_ProductsFeaturesApp::deactivateAction',
			'canAccess' => 'Admin_Controller_ProductsFeaturesApp::canAccessProductsFilters',
			'canActivate' => 'Admin_Controller_ProductsFeaturesApp::canActivateProductsFilters',
			'indexAction' => 'Admin_Controller_ProductsFeaturesApp::indexAction',
			'help' => '2002',
		),
		'cardinal-centinel' => array(
			'title' => 'Cardinal Centinel',
			'icon' => 'cardinal-centinel',
			'category' => 'payments',
			'price' => 'paid',
			'activateForm' => false,
			'activateFormUrl' => 'admin.php?p=payment_cardinal&mode=activate-form',
			'content' => 'templates/pages/cardinal-centinel/content.html',
			'indexAction' => 'Admin_Controller_CardinalCentinel::indexAction',
			'deactivateAction' => 'Admin_Controller_CardinalCentinel::deactivateAction',
			'help' => '',
		),
		'widgets' => array(
			'title' => 'Widgets',
			'icon' => 'widgets',
			'category' => 'marketing',
			'price' => 'free',
			'activateForm' => false,
			'content' => 'templates/pages/widgets/content.html',
			'indexAction' => 'Admin_Controller_Widgets::indexAction',
			'activateAction' => 'Admin_Controller_Widgets::activateAction',
			'deactivateAction' => 'Admin_Controller_Widgets::deactivateAction',
			'help' => '9033',
		),
	);

	public function __construct(Framework_Request $request, DB $db)
	{
		parent::__construct($request, $db);

		if (IS_LABEL) unset($this->apps['quickbooks']);
	}

	/**
	 * Show apps list
	 */
	public function listAction()
	{
		$request = $this->getRequest();

		$categoryFilter = $request->query->get('category', 'any');
		$priceFilter = $request->query->get('price', 'any');

		$activeApps = $this->getActiveApps($categoryFilter, $priceFilter);

		if (count($activeApps) == 0)
		{
			$this->appCenterAction();
		}
		else
		{
			$activeApps = $this->filterApps($activeApps, $categoryFilter, $priceFilter);

			$adminView = $this->getView();

			// set menu
			$adminView->assign('activeMenuItem', $this->activeAppsMenuItem);
			$adminView->assign('helpTag', '9002');

			$adminView->assign('apps', $activeApps);

			$adminView->assign('categoryFilter', $categoryFilter);
			$adminView->assign('priceFilter', $priceFilter);
			$adminView->assign('isActiveApps', true);

			$adminView->assign('body', 'templates/pages/apps/list.html');
			$adminView->render('layouts/default');
		}
	}

	/**
	 * App center action
	 */
	public function appCenterAction()
	{
		$request = $this->getRequest();

		$adminView = $this->getView();

		$categoryFilter = $request->query->get('category', 'any');
		$priceFilter = $request->query->get('price', 'any');

		$activeApps = $this->getActiveApps($categoryFilter, $priceFilter);

		// set menu
		$adminView->assign('activeMenuItem', $this->appCenterMenuItem);
		$adminView->assign('helpTag', '9001');

		$adminView->assign('apps', $this->getAppCenterApps($categoryFilter, $priceFilter));

		$adminView->assign('hasActiveApps', count($activeApps) > 0);

		$adminView->assign('categoryFilter', $categoryFilter);
		$adminView->assign('priceFilter', $priceFilter);

		$adminView->assign('body', 'templates/pages/apps/app-center.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Activate action
	 */
	public function activateAction()
	{
		$request = $this->getRequest();

		$adminView = $this->getView();

		$apps = $this->getAppCenterApps();

		$key = $request->query->get('key', '');

		if (!array_key_exists($key, $apps))
		{
			$this->redirect('apps', array('action' => 'app-center'));
			return;
		}

		$app = $apps[$key];

		$canActivate = $this->getCanActivate($app);

		if (isset($apps[$key]['preactivateAction']) && $apps[$key]['preactivateAction'])
		{
			$this->callAction($app['preactivateAction']);
		}

		if ($canActivate && $request->isPost())
		{
			$this->getAppRepository()->enableApp($key);

			if (isset($apps[$key]['activateAction']) && $apps[$key]['activateAction'])
			{
				$this->callAction($app['activateAction']);
				Admin_Flash::setMessage($apps[$key]['title'].' has been enabled');
			}

			$this->redirect('apps');
			return;
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', $apps[$key]['help']);

		$adminView->assign('canActivate', $canActivate);
		$adminView->assign('appKey', $key);
		$adminView->assign('app', $apps[$key]);

		$adminView->assign('body', 'templates/pages/apps/activate.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Subscribe to app
	 */
	public function subscribeAction()
	{
		$request = $this->getRequest();

		$apps = $this->getAppCenterApps();

		$key = $request->get('app_key', '');

		if (!array_key_exists($key, $apps))
		{
			$result = array('status' => 0, 'error' => 'Cannot find app by id');
		}
		else
		{
			$app = $apps[$key];

			if ($this->getCanAccess($app) && $this->getCanSubscribe($app))
			{
				$billingService = $this->getBillingService();

				$subscribeResult = $billingService->subscribe(isset($app['subscription_id']) ? $app['subscription_id'] : $key);
				if ($subscribeResult && is_array($subscribeResult) && isset($subscribeResult['status']))
				{
					if ($subscribeResult['status'] == 1)
					{
						$billingService->refreshLicense();

						$this->getAppRepository()->enableApp($key);

						$result = array('status' => 1);
						if (isset($app['activateAction']) && $app['activateAction'])
						{
							$this->callAction($app['activateAction']);
							Admin_Flash::setMessage($app['title'].' has been enabled');

						}
						if (isset($app['activateRedirectUrl']) && $app['activateRedirectUrl'])
						{
							$result['redirect_url'] = $app['activateRedirectUrl'];
						}
					}
					else
					{
						$errorMessage = '';
						if ($subscribeResult['errors'])
						{
							foreach ($subscribeResult['errors'] as $error)
							{
								$errorMessage .= ($errorMessage == '' ? '' : '<br>') . $error['message'] ;
							}
						}

						$result = array('status' => 0, 'error' => $errorMessage);
					}
				}
				else
				{
					$result = array('status' => 0, 'error' => 'Cannot connect to billing service. Please try again');
				}
			}
			else
			{
				$result = array('status' => 0, 'error' => 'This feature is not available under your license agreement. Please contact your software vendor.');
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Deactivate app
	 */
	public function deactivateAction()
	{
		$request = $this->getRequest();

		$apps = $this->getActiveApps();

		$key = $request->query->get('key', '');

		if (!array_key_exists($key, $apps))
		{
			$this->redirect('apps');
			return;
		}

		$this->getAppRepository()->disableApp($key);

		if (isset($apps[$key]['deactivateAction']) && $apps[$key]['deactivateAction'])
		{
			$this->callAction($apps[$key]['deactivateAction']);
			Admin_Flash::setMessage($apps[$key]['title'].' has been disabled');
		}

		$this->redirect('apps');
	}

	/**
	 * Activate form action
	 */
	public function activateFormAction()
	{

	}

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		$apps = $this->getActiveApps();

		$key = $request->query->get('key', '');

		if (!array_key_exists($key, $apps))
		{
			$this->redirect('apps');
			return;
		}

		$app = $apps[$key];
		if (array_key_exists('indexAction', $app))
		{
			$this->callAction($app['indexAction']);
		}
		else
		{
			$this->redirect('apps');
			return;
		}
	}

	/**
	 * Get active apps
	 *
	 * @return array
	 */
	protected function getActiveApps() //$categoryFilter = 'any', $priceFilter = 'any')
	{
		// TODO: move into repository

		$apps = $this->apps;

		$activeApps = array();

		$result = $this->getAppRepository()->getEnabledApps();

		$enabledApps = array();

		foreach ($result as $appData) $enabledApps[$appData['key']] = true;

		foreach ($apps as $key => $app)
		{
			if (isset($enabledApps[$key])) $activeApps[$key] = $app;
		}

		return $activeApps;
	}

	/**
	 * Get app center apps
	 *
	 * @param string $categoryFilter
	 * @param string $priceFilter
	 *
	 * @return array
	 */
	protected function getAppCenterApps($categoryFilter = 'any', $priceFilter = 'any')
	{
		$apps = $this->apps;

		foreach ($this->getActiveApps() as $key => $app) unset($apps[$key]);

		$availableApps = array();

		foreach ($apps as $key => $app)
		{
			if ($this->getCanAccess($app)) $availableApps[$key] = $app;
		}

		return $this->filterApps($availableApps, $categoryFilter, $priceFilter);
	}


	/**
	 * Check is app accessible (visible) in app center
	 *
	 * @param $app
	 *
	 * @return bool|mixed
	 */
	protected function getCanAccess($app)
	{
		$canAccess = true;

		if (array_key_exists('canAccess', $app))
		{
			$canAccess = $this->callAction($app['canAccess']);
		}

		return $canAccess;
	}

	/**
	 * Check does app may be activated
	 *
	 * @param $app
	 *
	 * @return bool|mixed
	 */
	protected function getCanActivate($app)
	{
		$canActivate = true;

		if (array_key_exists('canActivate', $app))
		{
			$canActivate = $this->callAction($app['canActivate']);
		}

		return $canActivate;
	}

	/**
	 * @param $app
	 * @return bool
	 */
	protected function getCanSubscribe($app)
	{
		return isset($app['canSubscribe']) ? $app['canSubscribe'] : false;
	}

	/**
	 * Filter apps by category and price
	 *
	 * @param $apps
	 * @param $categoryFilter
	 * @param $priceFilter
	 *
	 * @return array
	 */
	protected function filterApps($apps, $categoryFilter, $priceFilter)
	{
		if (trim($categoryFilter) == '') $categoryFilter = 'any';
		if (trim($priceFilter) == '') $priceFilter = 'any';

		if ($categoryFilter == 'any' && $priceFilter == 'any') return $apps;

		$ret = array();
		foreach ($apps as $key => $app)
		{
			if ($categoryFilter != 'any' && $app['category'] != $categoryFilter) continue;
			if ($priceFilter != 'any' && $app['price'] != $priceFilter) continue;

			$ret[$key] = $app;
		}
		return $ret;
	}

	/**
	 * Execute action
	 *
	 * @param $controllerStr
	 *
	 * @return mixed
	 */
	protected function callAction($controllerStr)
	{
		$parts = explode('::', $controllerStr);
		$controller = $parts[0];
		$params = array($this->getRequest(), $this->getDb());
		$ref = new ReflectionClass($controller);
		$controller = $ref->newInstanceArgs($params);
		return call_user_func(array($controller, $parts[1]));
	}

	/**
	 * @return DataAccess_AppRepository|null
	 */
	protected function getAppRepository()
	{
		if (is_null($this->appRepository))
		{
			$this->appRepository = new DataAccess_AppRepository($this->getDb());
		}

		return $this->appRepository;
	}

	/**
	 * @return Admin_Service_Billing
	 */
	protected function getBillingService()
	{
		if (is_null($this->billingService))
		{
			$this->billingService = new Admin_Service_Billing(
				$this->getSettings(),
				new DataAccess_CountriesStatesRepository($this->getDb()),
				defined('BILLING_TEST_MODE') && BILLING_TEST_MODE
			);
		}

		return $this->billingService;
	}
}

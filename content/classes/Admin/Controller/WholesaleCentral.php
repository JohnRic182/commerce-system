<?php

class Admin_Controller_WholesaleCentral extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function __construct(Framework_Request $request, DB $db)
	{
		parent::__construct($request, $db);
	}

	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'WholesaleCentralUsername', 'WholesaleCentralPassword',
			'WholesaleCentralEnableCron',
		);

		$settingsData = $settings->getByKeys($settingsVars);

		$wholsaleCentralForm = new Admin_Form_WholesaleCentralForm();
		$form = $wholsaleCentralForm->getForm($settingsData);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'wholesale-central'));
				return;
			}
			else
			{
				$errors = array();
				$data = $request->request->all();
				if (is_array($data)) {
					if (trim($data['WholesaleCentralUsername']) == '') {
						$errors['WholesaleCentralUsername'] = array(trans('apps.wholesalecentral.username_required'));
					}

					if (trim($data['WholesaleCentralPassword']) == '') {
						$errors['WholesaleCentralPassword'] = array(trans('apps.wholesalecentral.password_required'));
					}
				}

				if(count($errors)>0)
				{
					$formData['image_upload'] = null;
					$formData['digital_product_file_upload'] = null;
					$form->bind($data);
					$form->bindErrors($errors);
				}
				else
				{
					$settingsData = $request->request->getByKeys($settingsVars);

					$settings->persist($settingsData, $settingsVars);

					Admin_Flash::setMessage('common.success');
					$this->redirect('app', array('key' => 'wholesale-central'));
				}

			}
		}

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9012');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$adminView->assign('body', 'templates/pages/wholesale-central/index.html');
		$adminView->render('layouts/default');
	}

	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('WholesaleCentralUsername', 'WholesaleCentralPassword');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['WholesaleCentralUsername']) || trim($settingsData['WholesaleCentralUsername']) == '')
			{
				$errors[] = array('field' => '.field-WholesaleCentralUsername', 'message' => trans('apps.wholesalecentral.username_required'));
			}
			if (!isset($settingsData['WholesaleCentralPassword']) || trim($settingsData['WholesaleCentralPassword']) == '')
			{
				$errors[] = array('field' => '.field-WholesaleCentralPassword', 'message' => trans('apps.wholesalecentral.password_required'));
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('wholesale-central');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$wholesaleCentralForm = new Admin_Form_WholesaleCentralForm();
		$form = $wholesaleCentralForm->getWholesaleCentralActivateForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	public function exportAction()
	{
		$db = $this->getDb();

		$filename = 'content/cache/tmp/wholesalecentral.txt';
		$settings = $this->getSettings();

		$wholesaleCentralClient = new WholesaleCentral($settings->get('WholesaleCentralUsername'), $settings->get('WholesaleCentralPassword'), $db);

		if ($wholesaleCentralClient->generateDataFeed($this->getProductData(), $filename))
		{
			if (FALSE !== ($fp = fopen($filename, 'r')))
			{
				$download_filename = 'ezfeed-wholesalecentral.txt';

				header('Content-Type: text/plain');

				header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
				header('Content-Disposition: inline; filename="'.$download_filename.'"');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Content-Disposition: attachment; filename="'.$download_filename.'"');
				header('Pragma: no-cache');

				while ($buffer = fread($fp, 2000))
				{
					print $buffer;
				}
				exit;
			}
		}
	}

	public function syncAction()
	{
		$db = $this->getDb();

		$filename = 'content/cache/tmp/wholesalecentral.txt';
		try
		{
			$settings = $this->getSettings();

			$wholesaleCentralClient = new WholesaleCentral($settings->get('WholesaleCentralUsername'), $settings->get('WholesaleCentralPassword'), $db);
			$wholesaleCentralClient->generateDataFeed($this->getProductData(), $filename, true);
			$wholesaleCentralClient->sendDataFeed($filename);

			$db = $this->getDb();
			$db->reset();
			$db->assignStr('value', date('Y-m-d g:i:s'));
			$db->update(DB_PREFIX.'settings', 'WHERE name = "WholesaleCentralLastUpload"');

			$response = array(
				'status' => 1,
				'code' => 1
			);
		}
		catch (Exception $e)
		{
			$response = array(
				'status' => 0,
				'code' => 2,
				'message' => $e->getMessage()
			);
		}

		$this->renderJson($response);
	}

	private function getProductData()
	{
		global $currencies;

		$settings = $this->getSettings();
		$settingsArray = $settings->getAll();

		$storeUrl = $settings->get('GlobalHttpUrl');
		$useOverview = false;

		$currencies->getDefaultCurrency();

		$this->db->query('
			SELECT
				p.pid,
				p.cid,
				p.product_id,
				IF(p.url_custom != "", p.url_custom, p.url_default) product_url,
				p.title,
				p.overview,
				p.description,
				p.image_location,
				p.image_url,
				p.call_for_price,
				p.stock,
				p.price,
				p.inventory_control,
				p.inventory_rule,
				c.name as category_name
			FROM '.DB_PREFIX.'products p
			JOIN '.DB_PREFIX.'catalog c ON p.cid = c.cid
				WHERE p.is_visible = "Yes"
			LIMIT 10000
		');

		$data = array();
		$product = new ShoppingCartProducts($this->db, $settingsArray);

		while ($col = $this->db->moveNext())
		{
			// Figure out whether we should display this product or not based on inventory
			if ($col['inventory_control'] != 'No' && $col['inventory_rule'] == 'Hide')
			{
				if ($col['inventory_control'] == 'Yes' && intval($col['stock']) <= 0)
				{
					continue;
				}
			}

			// Product name, description, and category
			$productName = strip_tags($col['title']);
			$productDescription = strip_tags(($useOverview) ? $col['overview'] : $col['description']);
			$productCategory = $col['category_name'];

			// Generate product URL based on current settings
			if (strtoupper($settings->get('USE_MOD_REWRITE')) == 'YES')
			{
				$productUrl = $storeUrl.'/'.$col['product_url'];
			}
			else
			{
				$productUrl = $storeUrl.'/'.$settings->get('INDEX')."?p=product&id=".$col['pid'];
			}

			// Figure out the price
			if ($settings->get('VisitorSeePrice') == 'NO')
			{
				$productPrice = 'Login to view price';
			}
			else
			{
				$productPrice = ($col['call_for_price'] == 'Yes') ? 'Call for price' : getAdminPrice($col['price']);
			}

			// product image url
			$imageUrl = $product->getProductImage($col['product_id'], $col['image_location'], $col['image_url']);
			if (strtolower($col['image_location']) == 'local')
			{
				$imageUrl = $storeUrl.'/'.$imageUrl;
			}

			$data[] = array(
				'product_url' => $productUrl,
				'product_name' => $productName,
				'product_description' => $productDescription,
				'image_url' => $imageUrl,
				'product_category' => $productCategory,
				'product_price' => $productPrice
			);
		}

		return $data;
	}
}
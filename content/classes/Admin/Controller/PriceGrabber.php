<?php

class Admin_Controller_PriceGrabber extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		$data['categories'] = $categoryRepository->getOptionsList();

		$priceGrabberForm = new Admin_Form_PriceGrabberForm();

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9010');

		$adminView->assign('form', $priceGrabberForm->getForm($data));

		$adminView->assign('body', 'templates/pages/price-grabber/index.html');
		$adminView->render('layouts/default');
	}
}
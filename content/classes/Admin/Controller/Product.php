<?php

/**
 * Class Admin_Controller_Product
 */
class Admin_Controller_Product extends Admin_Controller
{
	/** @var DataAccess_ProductsRepository $productRepository */
	protected $productRepository = null;

	/** @var DataAccess_CategoryRepository */
	protected $categoryRepository = null;

	/** @var DataAccess_ManufacturerRepository */
	protected $manufacturerRepository = null;

	/** @var DataAccess_ProductLocationRepository */
	protected $productLocationRepository = null;

	protected $taxService = null;

	/** @var Admin_Service_FileUploader $fileUploadService */
	protected $fileUploadService = null;

	/** @var DataAccess_ProductImageRepository $productImageRepository */
	protected $productImageRepository = null;

	/** @var DataAccess_ProductShippingMethodRepository $productShippingMethodRepository */
	protected $productShippingMethodRepository = null;

	/** @var DataAccess_ProductAttributeRepository $productAttributeRepository */
	protected $productAttributeRepository = null;

	/** @var DataAccess_GlobalAttributeRepository $globalAttributeRepository  */
	protected $globalAttributeRepository = null;

	/** @var DataAccess_ProductVariantRepository $productVariantRepository */
	protected $productVariantRepository = null;

	/** @var DataAccess_ProductPromotionRepository $productPromotionRepository */
	protected $productPromotionRepository = null;

	/** @var DataAccess_ProductQuantityDiscountRepository $productQuantityDiscountRepository */
	protected $productQuantityDiscountRepository = null;

	/** @var DataAccess_OffsiteCommerceCampaignRepository $offsiteCommerceCampaignRepository  */
	protected $offsiteCommerceCampaignRepository = null;

	/** @var DataAccess_ProductsFamilyRepository $productFamilyRepository  */
	protected $productFamilyRepository = null;

	/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository  */
	protected $productsFeaturesGroupsRepository = null;

	/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
	protected $productsFeaturesRepository = null;

	/** @var DataAccess_ProductsFeaturesValuesRepositoryRepository $productsFeaturesValuesRepository */
	protected $productsFeaturesValuesRepository = null;

	/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesRepository */
	protected $productsFeaturesValuesRelationsRepository = null;

	/** @var Admin_Service_ProductExport $exportService */
	protected $exportService = null;

	/** @var Admin_Service_ProductCopy $copyService */
	protected $copyService = null;

	/** @var array */
	protected $menuItem = array('primary' => 'products', 'secondary' => 'products');

	/** @var array */
	protected $searchParamsDefaults = array('search_str' => '', 'cid' => 'any', 'status' => 'any', 'logic' => 'AND');

	protected $productCopyCheckedDefaults = array('images' => 1, 'advance_settings' => 1, 'seo' => 1, 'search_keywords' => 1, 'attributes' => 1, 'variants' => 1, 'product_promotions' => 1, 'quantity_discounts' => 1);

	/** @var string */
	protected $digitalDownloadsPath = 'content/download';

	// TODO: probably should not be here
	protected $productImagesDir = 'images/products';
	protected $productThumbsDir = 'images/products/thumbs';
	protected $productPreviewDir = 'images/products/preview';
	protected $productSecondaryImagesDir = 'images/products/secondary';
	protected $productSecondaryImagesThumbsDir = 'images/products/secondary/thumbs';

	protected $exportFileName = 'products.csv';

	/**
	 * List products
	 */
	public function listAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();
		$settings = $this->getSettings();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('productsSearchParams', array()); else $session->set('productsSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('productsSearchOrderBy', 'title'); else $session->set('productsSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('productsSearchOrderDir', 'asc'); else $session->set('productsSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['categories'] = $this->getCategoriesOptions(true);
		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		/**
		 * Get data from DB
		 */
		$repository = $this->getProductRepository();
		$itemsCount = $repository->getCount($searchParams, $searchParams['logic']);

		/**
		 * Show results
		 */
		$adminView = $this->getView();
		$productForm = new Admin_Form_ProductForm($this->getSettings(), $this->getDb());
		$adminView->assign('searchForm', $productForm->getProductSearchForm($searchFormData));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign(
				'products',
				$repository->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'p.*', $searchParams['logic']
				)
			);
		}
		else
		{
			$adminView->assign('products', null);
		}

		/**
		 * The rest of features
		 */
		$adminView->assign('delete_nonce', Nonce::create('product_delete'));
		$adminView->assign('export_nonce', Nonce::create('product_export'));

		if ($settings->get('widgets_active') == 'Yes')
		{
			$widgetsForm = new Admin_Form_ProductWidgetForm();
			$adminView->assign(
				'widgetsForm',
				$widgetsForm->getForm(
					false,
					$this->getOffsiteCommerceCampaignRepository()->getCampaignsOptions('wg'),
					$settings->get('widgets_image_type'),
					$settings->get('widgets_style'),
					$settings->get('widgets_type')
				)
			);
		}
		$adminView->assign('widgetsActive', $settings->get('widgets_active') == 'Yes');

		$productCopyOptions = $session->get('productCopyOptions', Admin_Service_ProductCopy::getDefaultFields());
		$productCopyChecked = $session->get('productCopyChecked', $this->productCopyCheckedDefaults);

		$adminView->assign('copyProductFields', json_encode($productCopyOptions));

		$exportForm = new Admin_Form_ProductExportForm($this->getSettings());
		$adminView->assign('exportForm', $exportForm->getForm());

		// product copy
		$copyForm = new Admin_Form_ProductCopyForm();
		$adminView->assign('copyForm', $copyForm->getForm($productCopyChecked));
		$adminView->assign('copy_nonce', Nonce::create('product_copy'));

		// product edit
		$editForm = new Admin_Form_ProductEditForm();
		$taxClassOptions = $this->getTaxClassOptions();

		$currency = $this->getSession()->get('admin_currency');

		$bulkEditFields = $session->get('bulkEditFields', $editForm->getDefaultFields() );
		$adminView->assign('editForm', $editForm->getForm($bulkEditFields, $searchFormData['categories'], $taxClassOptions, $currency));
		$adminView->assign('edit_nonce', Nonce::create('product_edit'));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3000');

		$hasTestProducts = $repository->hasTestProducts();
		$adminView->assign('hasTestProducts', $hasTestProducts);

		$adminView->assign('body', 'templates/pages/product/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add test products
	 */
	public function addTestProductsAction()
	{
		$repository = $this->getProductRepository();
		if ($repository->hasTestProducts())
		{
			$repository->deleteTestProducts($this->productImagesDir, $this->productThumbsDir, $this->productPreviewDir, $this->productSecondaryImagesDir);
		}

		$db = $this->getDb();
		$settings = $this->getSettings();
		$fp = @fopen(dirname(dirname(dirname(dirname(__FILE__)))).'/skins/'.$settings->get('DesignSkinName').'/fixtures/test_products.sql', 'r');

		if ($fp !== false)
		{
			while ($query = fgets($fp, 8096*2))
			{
				if (trim($query) != '')
				{
					if (strstr($query, "INSERT INTO "))
						$query = str_replace("INSERT INTO ", "INSERT INTO ".DB_PREFIX, $query);
					if (strstr($query, "UPDATE "))
						$query = str_replace("UPDATE ", "UPDATE ".DB_PREFIX, $query);
					if (strstr($query," FROM "))
						$query = str_replace(" FROM ", " FROM ".DB_PREFIX, $query);

					$db->query($query);
				}
			}

			copy_dir(dirname(dirname(dirname(dirname(__FILE__)))).'/skins/'.$settings->get('DesignSkinName').'/fixtures/images',
				dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/images/');

			Seo::updateSeoURLs($db, $settings->getAll(), true, true, false, true);

			Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
		}

		$this->renderJson(array('status' => 1));
	}

	/**
	 * Add Product
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();
		$settings = $this->getSettings();
		$productRepository = $this->getProductRepository();
		$productFamilyRepository = $this->getProductFamilyRepository();

		$formDefaults = $defaults = $productRepository->getDefaults();

		$productForm = new Admin_Form_ProductForm($settings, $this->getDb());

		$formDefaults['is_visible'] = 'Yes';

		$form = $productForm->getForm(
			$formDefaults, $mode,
			array(
				'categoryOptions' => $this->getCategoryRepository()->getOptionsList(),
				'manufacturerOptions' => $this->getManufacturerRepository()->getOptionsList(),
				'productLocationOptions' => $this->getProductLocationRepository()->getOptionsList(),
				'productShippingMethods' => $productShippingMethods = $this->getProductShippingMethodRepository()->getProductShippingMethods(),
				'taxService' => $this->getTaxService(),
				'taxClassOptions' => $this->getTaxClassOptions(),
				'productFamilies' => $productFamilyRepository->getList(),
			)
		);

		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->getRequest()->all(), $request->getFiles()->all());

			if (!Nonce::verify($formData['nonce'], 'product_'.$mode))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products');
			}
			else
			{
				$generateCategoryUrls = false;

				if (($validationErrors = $productRepository->getValidationErrors($formData, $mode)) == false)
				{
					if (isset($formData['category_name']) && $formData['cid'] == 0)
					{
						$categoryRepository = $this->getCategoryRepository();
						$category = $categoryRepository->getDefaults(Admin_Controller_Category::MODE_ADD);
						$category['is_visible'] = 'Yes';
						$category['name'] = $formData['category_name'];
						$category['key_name'] = str_replace(' ', '_', strtolower($category['name']));
						$formData['cid'] = $categoryRepository->persist($category);
						$generateCategoryUrls = true;
					}

					// handle recurring billing
					if ($settings->get('RecurringBillingEnabled') == '1')
					{
						$this->handleRecurringBilling($formData);
					}

					if ($formData['url_custom'] != '' && $productRepository->hasUrlDuplicate($formData['url_custom'], 0))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$id = $productRepository->persist($formData);
					$formData['id'] = $id;

					$productRepository->persistProductCategories($id, $mode, $formData['categories'], $formData['cid']);
					$productFamilies = $formData['product_families'];
					if (!is_array($productFamilies)) $productFamilies = explode(',', $formData['product_families']);
					$productRepository->persistProductFamilies($id, $mode, $productFamilies);

					if ($productShippingMethods && is_array($productShippingMethods) && count($productShippingMethods) > 0 && isset($formData['shipping_method']))
					{
						$productRepository->persistProductShippingMethod($id, $mode, $formData['shipping_method']);
					}

					// TODO: optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), $generateCategoryUrls, true, false, false);

					$dirty = false;

					if ($formData['product_type'] == Model_Product::DIGITAL && $this->handleDigitalFileUpload($formData)) $dirty = true;

					// TODO: move on repository level?
					if ($dirty) $productRepository->persist($formData);

					// product images
					$primaryImageAttributes = array(
						'image_location' => $formData['image_location'],
						'image_url' => $formData['image_url'],
						'image_alt_text' => $formData['image_alt_text']
					);
					$this->handleImagesUpload($mode, $id, $formData['product_id'], $this->getRequest()->getFiles()->get('image_upload', array()), $primaryImageAttributes);

					// for our external images
					$primaryImageActive = (ImageUtility::productHasImage($formData['product_id']) ? true : false);
					$this->handleExternalImages($id, $this->getRequest()->get('image_upload_external', array()), $primaryImageActive);

					$event = new Events_ProductEvent(Events_ProductEvent::ON_UPDATED);
					$event->setData('product', $formData);
					Events_EventHandler::handle($event);

					Admin_Flash::setMessage('common.success');
					$productDetails = $productRepository->getProductById($id, true);
					Admin_Log::log('Products: Product added - '.$productDetails['title'], Admin_Log::LEVEL_IMPORTANT);
					$this->redirect('product', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					// TODO: find better way to handle this
					$formData['image_upload'] = null;
					$formData['digital_product_file_upload'] = null;
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', array_merge($this->menuItem, array('secondary' => 'products-add')));
		$adminView->assign('helpTag', '3001');

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());
		$adminView->assign('mode', 'add');
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('product_'.$mode));

		$adminView->assign('body', 'templates/pages/product/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update product
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$productRepository = $this->getProductRepository();

		$id = intval($request->get('id'));

		/** @var Model_Product $product */
		$product = $productRepository->getProductById($id);

		if (!$product)
		{
			Admin_Flash::setMessage('Cannot find product by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('products');
		}

		$productData = $product->toArray();

		$oldProductId = $productData['product_id'];

		$productData['categories'] = $productRepository->getProductCategoriesIdsByProductId($id);
		unset($productData['categories'][$productData['cid']]);

		$productData['product_families'] = $productRepository->getProductFamiliesIdsByProduct($id);

		$settings = $this->getSettings();
		$productForm = new Admin_Form_ProductForm($settings, $this->getDb());
		$params =  array(
			'categoryOptions' => $this->getCategoryRepository()->getOptionsList(),
			'manufacturerOptions' => $this->getManufacturerRepository()->getOptionsList(),
			'productLocationOptions' => $this->getProductLocationRepository()->getOptionsList(),
			'taxService' => $this->getTaxService(),
			'taxClassOptions' => $this->getTaxClassOptions(),
			'primaryImage' => $this->getProductImageRepository()->getPrimaryImage($id, $productData['product_id'], $productData['image_alt_text'], $productData['image_location'], $productData['image_url']),
			'secondaryImages' => $this->getProductImageRepository()->getSecondaryImages($id),
			'productAttributes' => $this->getProductAttributeRepository()->getListByProductId($id),
			'productShippingMethods' => $productShippingMethods = $this->getProductShippingMethodRepository()->getProductShippingMethods($id),
			'productVariants' => $this->getProductVariantRepository()->getListByProductId($id),
			'productFeaturesGroups' => $this->getProductsFeaturesGroups($id),
			'productPromotions' => $this->getProductPromotionRepository()->getListByProductId($id),
			'productQuantityDiscounts' => $this->getProductQuantityDiscountRepository()->getListByProductId($id),
			'productFamilies' => $this->getProductFamilyRepository()->getList(),
		);
		$form = $productForm->getForm($productData, $mode, $params, $id);

		if ($request->isPost())
		{
			$postData = $request->getRequest()->all();
			$formData = array_merge($productData, $postData, $request->getFiles()->all());

			if (!isset($postData['is_visible'])) $formData['is_visible'] = 'No';
			if (!isset($postData['free_shipping'])) $formData['free_shipping'] = 'No';
			if (!isset($postData['product_level_shipping'])) $formData['product_level_shipping'] = '0';
			if (!isset($postData['is_taxable'])) $formData['is_taxable'] = 'No';
			if (!isset($postData['is_stealth'])) $formData['is_stealth'] = '0';
			if (!isset($postData['call_for_price'])) $formData['call_for_price'] = 'No';

			if (!isset($postData['is_hotdeal'])) $formData['is_hotdeal'] = 'No';
			if (!isset($postData['is_home'])) $formData['is_home'] = 'No';
			if (!isset($postData['enable_recurring_billing'])) $formData['enable_recurring_billing'] = '0';
			if (!isset($postData['same_day_delivery'])) $formData['same_day_delivery'] = '0';

			if (!$request->get('categories', false)) $formData['categories'] = array();
			$formData['product_families'] = explode(',', $request->get('product_families', ''));

			if (!Nonce::verify($formData['nonce'], 'product_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products');
			}
			else
			{
				if (($validationErrors = $productRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					if ($formData['product_type'] == Model_Product::DIGITAL)
					{
						$this->handleDigitalFileUpload($formData);
					}

					// handle recurring billing
					if ($settings->get('RecurringBillingEnabled') == '1')
					{
						$this->handleRecurringBilling($formData);
					}

					if ($formData['url_custom'] != '' && $productRepository->hasUrlDuplicate($formData['url_custom'], $id))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$productRepository->persist($formData);
					$productRepository->persistProductCategories($id, $mode, $formData['categories'], $formData['cid']);
					$productFamilies = $formData['product_families'];

					if (!is_array($productFamilies)) $productFamilies = explode(',', $productFamilies);

					$productRepository->persistProductFamilies($id, $mode, $productFamilies);

					if ($productShippingMethods && is_array($productShippingMethods) && count($productShippingMethods) > 0 && isset($formData['shipping_method']))
					{
						$productRepository->persistProductShippingMethod($id, $mode, $formData['shipping_method']);
					}

					$productRepository->updateProductInventory($id, $formData['inventory_control']);

					// handle products features
					if (isset($formData['product_feature']) && is_array($formData['product_feature']))
					{
						$this->updateProductsFeatures($id, $formData['product_feature']);
					}

					// TODO: optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, true, false, false);

					// product images
					$primaryImageAttributes = array(
						'image_location' => $formData['image_location'],
						'image_url' => $formData['image_url'],
						'image_alt_text' => $formData['image_alt_text']
					);
					$this->handleImagesUpload($mode, $id, $formData['product_id'], $this->getRequest()->getFiles()->get('image_upload', array('')), $primaryImageAttributes);

					// for our external images
					$primaryImageActive = (ImageUtility::productHasImage($formData['product_id']) || !empty($productData['image_url']) ? true : false);
					$this->handleExternalImages($id, $this->getRequest()->get('image_upload_external', array()), $primaryImageActive);

					// handle product id change
					if (trim($oldProductId) != trim($formData['product_id']))
					{
						$this->getProductImageRepository()->renameProductImages($id, $oldProductId, $formData['product_id']);
					}

					$event = new Events_ProductEvent(Events_ProductEvent::ON_UPDATED);
					$event->setData('product', $formData);
					Events_EventHandler::handle($event);

					Admin_Flash::setMessage('common.success');
					$productDetails = $productRepository->getProductById($id, true);
					Admin_Log::log('Products: Product updated - '.$productDetails['title'], Admin_Log::LEVEL_IMPORTANT);
					$this->redirect('product', array('id' => $id, 'mode' => $mode));
				}
				else
				{
					// TODO: find better way to handle this
					$formData['image_upload'] = null;
					$formData['digital_product_file_upload'] = null;
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		$adminView->assign('productProductsFeaturesGroupsAvailable', json_encode($this->getProductsFeaturesGroupsRepository()->getList(null, null, 'feature_group_name_asc')));

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3001');
		$adminView->assign('qrCodesActive', $settings->get('qr_active') == 'Yes');
		$adminView->assign('widgetsActive', $settings->get('widgets_active') == 'Yes');
		$adminView->assign('title', $productData['title']);

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		$productUrl = $settings->get('INDEX').'?p=product&amp;id='.$id;
		if ($settings->get('USE_MOD_REWRITE') == 'YES')
		{
			$productUrl = $productData['url_custom'] != '' ? $productData['url_custom'] : $productData['url_default'];
		}
		$adminView->assign('productUrl', $productUrl);

		$adminView->assign('mode', 'update');
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);

		$attributesForm = new Admin_Form_ProductAttributeForm();

		$adminView->assign('productAttributeForm', $attributesForm->getProductAttributeForm($id));

		$globalAttributes = $this->getGlobalAttributeRepository()->getList();

		if ($globalAttributes && count($globalAttributes) > 0)
		{
			$adminView->assign('globalAttributesForm', $attributesForm->getGlobalAttributesForm($id, $globalAttributes));
		}

		$productVariantForm = new Admin_Form_ProductVariantForm();
		$adminView->assign('productVariantForm', $productVariantForm->getForm($id));

		$productPromotionForm = new Admin_Form_ProductPromotionForm();
		$adminView->assign('productPromotionForm', $productPromotionForm->getForm($id));

		$productQuantityDiscountForm = new Admin_Form_ProductQuantityDiscountForm();
		$adminView->assign('productQuantityDiscountForm', $productQuantityDiscountForm->getForm($id));

		$qrCodeForm = new Admin_Form_ProductQrCodeForm();
		$adminView->assign(
			'qrCodeForm',
			$qrCodeForm->getForm(
				$id,
				$this->getOffsiteCommerceCampaignRepository()->getCampaignsOptions('qr'),
				$settings->get('qr_image_type'),
				$settings->get('qr_media'),
				$settings->get('qr_error_check'),
				$settings->get('qr_default_size')
			)
		);

		$widgetsForm = new Admin_Form_ProductWidgetForm();
		$adminView->assign(
			'widgetsForm',
			$widgetsForm->getForm(
				$id,
				$this->getOffsiteCommerceCampaignRepository()->getCampaignsOptions('wg'),
				$settings->get('widgets_image_type'),
				$settings->get('widgets_style'),
				$settings->get('widgets_type')
			)
		);

		$adminView->assign('nonce', Nonce::create('product_'.$mode));

		$adminView->assign('body', 'templates/pages/product/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update image action
	 */
	public function updateImageAction()
	{
		$request = $this->getRequest();

		$productDbId = intval($request->get('id'));
		$imageId = intval($request->get('image_id'));

		$productImageRepository = $this->getProductImageRepository();
		$productImageRepository->updateImage($productDbId, $imageId, $request->getRequest()->all());

		$result = array('status' => 1, 'primaryImage' => false);

		$product = $this->getProductRepository()->getProductById($productDbId);
		$productData = $product->toArray();

		if ($request->get('makePrimary', false) == '1')
		{
			$secondaryImageData = $productImageRepository->getSecondaryImageById($imageId);
			$productImageRepository->makePrimaryImageSecondary($productDbId, $secondaryImageData);

			if ($productImageRepository->makeSecondaryImagePrimary($productDbId, $productData['product_id'], $secondaryImageData))
			{
				// update product array related to image
				// because we successfully change the secondary into primary image
				$productData['image_location'] = $secondaryImageData['location'];
				$productData['image_url'] = $secondaryImageData['url'];
				$productData['image_alt_text'] = $secondaryImageData['altText'];

				$result = array(
					'status' => 1,
					'primaryImage' => $this->getProductImageRepository()->getPrimaryImage($productDbId, $productData['product_id'], $productData['image_alt_text'], $productData['image_location'], $productData['image_url']),
					'secondaryImages' => $this->getProductImageRepository()->getSecondaryImages($productDbId)
				);
			}
		}

		$this->getProductRepository()->touch($productDbId);

		$event = new Events_ProductEvent(Events_ProductEvent::ON_UPDATED);
		$event->setData('product', $productData);
		Events_EventHandler::handle($event);

		$this->renderJson($result);
	}


	/**
	 * Delete image action
	 */
	public function deleteImageAction()
	{
		$request = $this->getRequest();
		$productDbId = intval($request->get('id'));

		$productImageRepository = $this->getProductImageRepository();

		$productImageRepository->deleteImage($productDbId, $request->get('image_id'));
		$this->getProductRepository()->touch($productDbId);

		$productData = $this->getProductRepository()->getProductById($productDbId, true);

		$imageId = 0;
		$primaryImageChanged = 0;
		if ($productData)
		{
			// check if has primary or secondary image then assigned value to imageId
			if (ImageUtility::productHasImage($productData['product_id']) || !empty($productData['image_url']))
			{
				$imageId = $productData['pid'];
			}

			$event = new Events_ProductEvent(Events_ProductEvent::ON_UPDATED);
			$event->setData('product', $productData);
			Events_EventHandler::handle($event);
		}

		if ($request->get('image_id') == 'primary')
		{
			// here we will make the first secondary image to a primary image
			$secondaryImages = $productImageRepository->getSecondaryImages($productDbId);
			if ($secondaryImages)
			{
				$firstSecondaryImage = reset($secondaryImages);
				$imageId = $firstSecondaryImage['image_id'];

				$productImageRepository->makeSecondaryImagePrimary($productDbId, $productData['product_id'], $firstSecondaryImage);
				$productImageRepository->deleteSecondaryImage($firstSecondaryImage['image_id']);

				$primaryImageChanged = 1;
			}
		}

		$this->renderJson(array(
			'status' => 1,
			'imageId' => $imageId,
			'primaryImageChanged' => $primaryImageChanged,
			'editUrl' => 'admin.php?p=product&mode=update-image&id=' . $productDbId . '&image_id=primary',
			'deleteUrl' => 'admin.php?p=product&mode=delete-image&id=' . $productDbId . '&image_id=primary',
		));
	}

	/**
	 * Delete products
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		@set_time_limit(3000);

		if (!Nonce::verify($request->get('nonce', 'product_delete')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('products');
			return;
		}

		$repository = $this->getProductRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = intval($request->get('id', 0));
			$productDetails = $repository->getProductById($id, true);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid product id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($repository->delete(array($id), $this->productImagesDir, $this->productThumbsDir, $this->productPreviewDir, $this->productSecondaryImagesDir))
				{
					Admin_Flash::setMessage('common.success');
					Admin_Log::log('Products: Product deleted - '.$productDetails['title'], Admin_Log::LEVEL_IMPORTANT);
				}
				else
				{
					Admin_Flash::setMessage('Cannot delete selected product.', Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			foreach($ids as $id)
			{
				$productDetails[] = $repository->getProductById($id, true);
			}
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one product to delete', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($repository->delete($ids, $this->productImagesDir, $this->productThumbsDir, $this->productPreviewDir, $this->productSecondaryImagesDir))
				{
					Admin_Flash::setMessage('common.success');
					for($i = 0; $i < count($productDetails); $i++)
					{
						Admin_Log::log('Products: Product deleted - '.$productDetails[$i]['title'], Admin_Log::LEVEL_IMPORTANT);
					}
				}
				else
				{
					Admin_Flash::setMessage('Cannot delete one or more products.', Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else if ($deleteMode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('productsSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			if ($repository->deleteBySearchParams($searchParams, $this->productImagesDir, $this->productThumbsDir, $this->productPreviewDir, $this->productSecondaryImagesDir))
			{
				Admin_Flash::setMessage('common.success');
			}
			else
			{
				Admin_Flash::setMessage('Cannot delete one or more products.', Admin_Flash::TYPE_ERROR);
			}
		}
		else if ($deleteMode == 'test-products')
		{
			if ($repository->deleteTestProducts( $this->productImagesDir, $this->productThumbsDir, $this->productPreviewDir, $this->productSecondaryImagesDir))
			{
				Admin_Flash::setMessage('common.success');
				Admin_Log::log('Products: Deleted test products', Admin_Log::LEVEL_IMPORTANT);
			}
			else
			{
				Admin_Flash::setMessage('Cannot delete one or more products.', Admin_Flash::TYPE_ERROR);
			}
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('products');
	}

	/**
	 * Export products
	 */
	public function exportAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$exportMode = $request->get('exportMode');
		$fields = explode(',', $request->get('fields', ''));
		$exportService = $this->getProductExportService();

		if ($exportMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one product to export', Admin_Flash::TYPE_ERROR);
				$this->redirect('products');
			}
			else
			{
				$exportService->export($this->exportFileName, array('mode'=>'byIds', 'ids' => $ids), $fields);
			}
		}
		else if ($exportMode == 'all')
		{
			$session = $this->getSession();
			$searchParams = $session->get('productsSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());
			$exportService->export($this->exportFileName, array('mode'=>'all', 'searchParams' => $searchParams), $fields);
		}
	}

	/**
	 * @param $productId
	 * @return mixed
	 */
	protected function getProductsFeaturesGroups($productId)
	{
		$groups = $this->getProductsFeaturesGroupsRepository()->getProductsFeaturesGroupsByProductId($productId);

		foreach ($groups as $key => $group)
		{
			$groups[$key]['features'] =
				$this->getProductsFeaturesRepository()->getProductsFeaturesValuesByProductsFeaturesGroupId(
					$group['product_feature_group_id'],
					$productId
				);

			$groups[$key]['options'] =
				$this->getProductsFeaturesRepository()->getProductsFeaturesValuesOptionsByProductsFeaturesGroupId(
					$group['product_feature_group_id']
				);
		}

		return $groups;
	}

	/**
	 * @param $productId
	 * @param $featuresValues
	 * @return mixed
	 */
	protected function updateProductsFeatures($productId, $featuresValues)
	{
		$productsFeaturesRepository = $this->getProductsFeaturesRepository();
		$productsFeaturesGroupsRepository = $this->getProductsFeaturesGroupsRepository();
		$productsFeaturesValuesRepository = $this->getProductsFeaturesValuesRepository();
		$productsFeaturesValuesRelationsRepository = $this->getProductsFeaturesValuesRelationsRepository();

		$groups = $productsFeaturesGroupsRepository->getProductsFeaturesGroupsByProductId($productId);

		foreach ($groups as $key => $group)
		{
			$groupId = $group['product_feature_group_id'];

			$groupFeatures = $productsFeaturesRepository->getProductsFeaturesValuesByProductsFeaturesGroupId($groupId, $productId);

			foreach ($groupFeatures as $feature)
			{
				$featureId = $feature['product_feature_id'];

				$featureOptionValue = trim(isset($featuresValues[$groupId][$featureId]) ? $featuresValues[$groupId][$featureId] : '');

				if ($featureOptionValue == '' && intval($feature['product_feature_option_id']) != 0)
				{
					$productsFeaturesValuesRelationsRepository->delete(array(
						'feature_group_product_relation_id' => $group['feature_group_product_relation_id'],
						'product_feature_group_relation_id' => $feature['product_feature_group_relation_id'],
						'product_feature_option_id' => $feature['product_feature_option_id'],
					));
				}
				else
				{
					// check for changes
					if ($featureOptionValue != $feature['product_feature_option_value'])
					{
						$featureOptionId = intval($productsFeaturesValuesRepository->getIdByValue($featureOptionValue));
						if ($featureOptionId == 0) $featureOptionId = $productsFeaturesValuesRepository->persist(array('product_feature_option_value' => $featureOptionValue));

						if (intval($feature['product_feature_option_id']) != 0)
						{
							$productsFeaturesValuesRelationsRepository->delete(array(
								'feature_group_product_relation_id' => $group['feature_group_product_relation_id'],
								'product_feature_group_relation_id' => $feature['product_feature_group_relation_id'],
								'product_feature_option_id' => $feature['product_feature_option_id'],
							));
						}

						$productsFeaturesValuesRelationsRepository->persist(
							array(
								'feature_group_product_relation_id' => $group['feature_group_product_relation_id'],
								'product_feature_group_relation_id' => $feature['product_feature_group_relation_id'],
								'product_feature_option_id' => $featureOptionId
							)
						);
					}
				}
			}
		}
	}

	/**
	 * @param bool $hasAny
	 * @return array
	 */
	protected function getCategoriesOptions($hasAny = false)
	{
		$categoryRepository = $this->getCategoryRepository();

		$initialValues = null;
		if ($hasAny) $initialValues['any'] = 'Any';

		return $categoryRepository->getOptionsList(null, $initialValues);
	}

	/**
	 * Get product repository
	 *
	 * @return DataAccess_ProductsRepository
	 */
	protected function getProductRepository()
	{
		if (is_null($this->productRepository))
		{
			$this->productRepository = new DataAccess_ProductsRepository($this->getDb());
		}

		return $this->productRepository;
	}

	/**
	 * Get category repository
	 *
	 * @return DataAccess_CategoryRepository
	 */
	protected function getCategoryRepository()
	{
		if (is_null($this->categoryRepository))
		{
			$this->categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		}

		return $this->categoryRepository;
	}

	/**
	 * @return DataAccess_ManufacturerRepository
	 */
	protected function getManufacturerRepository()
	{
		if (is_null($this->manufacturerRepository))
		{
			$this->manufacturerRepository = new DataAccess_ManufacturerRepository($this->getDb());
		}

		return $this->manufacturerRepository;
	}

	/**
	 * @return DataAccess_ManufacturerRepository
	 */
	protected function getProductLocationRepository()
	{
		if (is_null($this->productLocationRepository))
		{
			$this->productLocationRepository = new DataAccess_ProductLocationRepository($this->getDb());
		}

		return $this->productLocationRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesGroupsRepository|null
	 */
	protected function getProductsFeaturesGroupsRepository()
	{
		if (is_null($this->productsFeaturesGroupsRepository))
		{
			$this->productsFeaturesGroupsRepository = new DataAccess_ProductsFeaturesGroupsRepository($this->getDb());
		}

		return $this->productsFeaturesGroupsRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesRepository
	 */
	protected function getProductsFeaturesRepository()
	{
		if (is_null($this->productsFeaturesRepository))
		{
			$this->productsFeaturesRepository = new DataAccess_ProductsFeaturesRepository($this->getDb());
		}

		return $this->productsFeaturesRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesValuesRepository
	 */
	protected function getProductsFeaturesValuesRepository()
	{
		if (is_null($this->productsFeaturesValuesRepository))
		{
			$this->productsFeaturesValuesRepository = new DataAccess_ProductsFeaturesValuesRepository($this->getDb());
		}

		return $this->productsFeaturesValuesRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesValuesRelationsRepository
	 */
	protected function getProductsFeaturesValuesRelationsRepository()
	{
		if (is_null($this->productsFeaturesValuesRelationsRepository))
		{
			$this->productsFeaturesValuesRelationsRepository = new DataAccess_ProductsFeaturesValuesRelationsRepository($this->getDb());
		}

		return $this->productsFeaturesValuesRelationsRepository;
	}

	/**
	 * Get seo URL settings
	 *
	 * @return mixed|string
	 */
	protected function getSeoUrlSettings()
	{
		/** @var DataAccess_SettingsRepository */
		$settings = $this->getSettings();

		return json_encode(array(
			'template' => $settings->get('SearchURLProductTemplate'),
			'lowercase' => $settings->get('SearchAutoGenerateLowercase') == 'YES',
			'joiner' => $settings->get('SearchURLJoiner')
		));
	}

	/**
	 * @return null|Tax_Avalara_AvalaraService|Tax_Exactor_ExactorService|Tax_ProviderInterface
	 */
	protected function getTaxService()
	{
		if (is_null($this->taxService))
		{
			$this->taxService = Tax_ProviderFactory::getTaxService();
		}

		return $this->taxService;
	}

	/**
	 * Get tax classes options
	 *
	 * @return array
	 */
	protected function getTaxClassOptions()
	{
		$db = $this->getDb();

		$taxService = $this->getTaxService();

		$options = array();

		if ($taxService instanceof Tax_CustomProvider)
		{
			$taxRepository = new DataAccess_TaxRepository($db);
			$options = $taxRepository->getTaxClassesOptions();
		}
		else if ($taxService instanceof Tax_Avalara_AvalaraService)
		{
			$taxRepository = new Tax_Avalara_AvalaraRepository($db);
			$options = $taxRepository->getTaxCodesOptions();
		}
		else if ($taxService instanceof Tax_Exactor_ExactorService)
		{
			$taxRepository = new Tax_Exactor_ExactorRepository($db);
			$options = $taxRepository->getTaxCodesOptions();
		}

		return $options;
	}

	/**
	 * @return Admin_Service_FileUploader|null
	 */
	protected function getFileUploadService()
	{
		if (is_null($this->fileUploadService))
		{
			$this->fileUploadService = new Admin_Service_FileUploader();
		}

		return $this->fileUploadService;
	}

	/**
	 * @return DataAccess_ProductImageRepository
	 */
	protected function getProductImageRepository()
	{
		if (is_null($this->productImageRepository))
		{
			$this->productImageRepository = new DataAccess_ProductImageRepository($this->getDb(), $this->getSettings(), $this->getFileUploadService());
		}

		return $this->productImageRepository;
	}

	/**
	 * @return DataAccess_ProductAttributeRepository
	 */
	protected function getProductAttributeRepository()
	{
		if (is_null($this->productAttributeRepository))
		{
			$this->productAttributeRepository = new DataAccess_ProductAttributeRepository($this->getDb());
		}

		return $this->productAttributeRepository;
	}

	/**
	 * @return DataAccess_ProductAttributeRepository
	 */
	protected function getProductShippingMethodRepository()
	{
		if (is_null($this->productShippingMethodRepository))
		{
			$this->productShippingMethodRepository = new DataAccess_ProductShippingMethodRepository($this->getDb());
		}

		return $this->productShippingMethodRepository;
	}

	/**
	 * @return DataAccess_GlobalAttributeRepository
	 */
	protected function getGlobalAttributeRepository()
	{
		if (is_null($this->globalAttributeRepository))
		{
			$this->globalAttributeRepository = new DataAccess_GlobalAttributeRepository(
				$this->getDb(), $this->getProductRepository(), $this->getProductAttributeRepository()
			);
		}

		return $this->globalAttributeRepository;
	}

	/**
	 * @return DataAccess_ProductVariantRepository
	 */
	protected function getProductVariantRepository()
	{
		if (is_null($this->productVariantRepository))
		{
			$this->productVariantRepository = new DataAccess_ProductVariantRepository($this->getDb());
		}

		return $this->productVariantRepository;
	}

	/**
	 * @return DataAccess_ProductPromotionRepository
	 */
	protected function getProductPromotionRepository()
	{
		if (is_null($this->productPromotionRepository))
		{
			$this->productPromotionRepository = new DataAccess_ProductPromotionRepository($this->getDb());
		}

		return $this->productPromotionRepository;
	}

	/**
	 * @return DataAccess_ProductQuantityDiscountRepository
	 */
	protected function getProductQuantityDiscountRepository()
	{
		if (is_null($this->productQuantityDiscountRepository))
		{
			$this->productQuantityDiscountRepository = new DataAccess_ProductQuantityDiscountRepository($this->getDb());
		}

		return $this->productQuantityDiscountRepository;
	}

	/**
	 * @return DataAccess_OffsiteCommerceCampaignRepository
	 */
	protected function getOffsiteCommerceCampaignRepository()
	{
		if (is_null($this->offsiteCommerceCampaignRepository))
		{
			$this->offsiteCommerceCampaignRepository = new DataAccess_OffsiteCommerceCampaignRepository($this->getDb());
		}

		return $this->offsiteCommerceCampaignRepository;
	}

	/**
	 * @return DataAccess_ProductsFamilyRepository
	 */
	protected function getProductFamilyRepository()
	{
		if (is_null($this->productFamilyRepository))
		{
			$this->productFamilyRepository = new DataAccess_ProductsFamilyRepository($this->getDb());
		}

		return $this->productFamilyRepository;
	}

	/**
	 * @param $mode
	 * @param $productDbId
	 * @param $externalImages
	 * @param $primaryImageActive
	 */
	protected function handleExternalImages($productDbId, $externalImages, $primaryImageActive = false)
	{
		if (is_array($externalImages) && count($externalImages) > 0)
		{
			$productImageRepository = $this->getProductImageRepository();
			foreach ($externalImages AS $externalImage)
			{
				// if primary image is not set, then save first row as primary of the external image
				// otherwise all will be save as secondary image into products_images table
				if (!$primaryImageActive)
				{
					$productImageRepository->persistWebPrimaryImage($productDbId, $externalImage);
					$primaryImageActive =  true;
				}
				else
				{
					$productImageRepository->persistWebSecondaryImage($productDbId, $externalImage);
				}
			}
		}
	}

	/**
	 * Handle images upload
	 *
	 * @param $mode
	 * @param $productDbId
	 * @param $productId
	 * @param $imagesFiles
	 */
	protected function handleImagesUpload($mode, $productDbId, $productId, $imagesFiles, $primaryImageAttributes)
	{
		if (isset($imagesFiles['name']) && is_array($imagesFiles['name']) && count($imagesFiles['name']) > 0)
		{
			$_imagesFiles = array();

			foreach ($imagesFiles['name'] as $key => $value)
			{
				$_imagesFiles[] = array(
					'name' => $imagesFiles['name'][$key],
					'type' => $imagesFiles['type'][$key],
					'tmp_name' => $imagesFiles['tmp_name'][$key],
					'error' => $imagesFiles['error'][$key],
					'size' => $imagesFiles['size'][$key]
				);
			}

			$productImageRepository = $this->getProductImageRepository();

			if ($mode == self::MODE_ADD && isset($_imagesFiles[0]))
			{
				$productImageRepository->addPrimaryImage($productDbId, $productId, $_imagesFiles[0], $primaryImageAttributes);
				unset($_imagesFiles[0]);
			}
			else if ($mode == self::MODE_UPDATE)
			{
				if (!$productImageRepository->getPrimaryImage($productDbId, $productId, $primaryImageAttributes['image_alt_text'], $primaryImageAttributes['image_location'], $primaryImageAttributes['image_url']))
				{
					$productImageRepository->addPrimaryImage($productDbId, $productId, $_imagesFiles[0], $primaryImageAttributes);
					unset($_imagesFiles[0]);
				}
			}

			if (count($imagesFiles) > 0) $productImageRepository->addSecondaryImages($mode, $productDbId, $productId, $_imagesFiles);
		}
	}

	/**
	 * Handle digital download file upload
	 *
	 * @param $formData
	 *
	 * @return bool
	 */
	protected function handleDigitalFileUpload(&$formData)
	{
		if ($fileUploadInfo = $this->getRequest()->getUploadedFile('digital_product_file_upload'))
		{
			if ($uploadedFile = $this->getFileUploadService()->processFileUpload($fileUploadInfo, $this->digitalDownloadsPath))
			{
				$formData['digital_product_file'] = pathinfo($uploadedFile, PATHINFO_BASENAME);
				return true;
			}
		}

		return false;
	}

	/**
	 * @param $formData
	 */
	protected function handleRecurringBilling(&$formData)
	{
		if ($formData['enable_recurring_billing'] && $formData['product_type'] == Model_Product::TANGIBLE && $formData['free_shipping'] != 'Yes')
		{
			$formData['free_shipping'] = 'Yes';
			Admin_Flash::setMessage('Please note: Free shipping has been enabled for this product', Admin_Flash::TYPE_INFO);
		}

		if (!isset($formData['recurring_billing_data']) || !is_array($formData['recurring_billing_data'])) return;

		if ($formData['product_type'] == Model_Product::TANGIBLE)
		{
			if (isset($formData['recurring_billing_data']['auto_complete_options']['auto_complete_initial_order']))
			{
				unset($formData['recurring_billing_data']['auto_complete_options']['auto_complete_initial_order']);
			}
			if (isset($formData['recurring_billing_data']['auto_complete_options']['auto_complete_recurring_orders']))
			{
				unset($formData['recurring_billing_data']['auto_complete_options']['auto_complete_recurring_orders']);
			}
		}

		if (!isset($formData['recurring_billing_data']['customer_notifications'])) $formData['recurring_billing_data']['customer_notifications'] = array();
		if (!isset($formData['recurring_billing_data']['admin_notifications'])) $formData['recurring_billing_data']['admin_notifications'] = array();

		/** @var RecurringBilling_Model_ProductRecurringBillingData $recurringBillingData */
		$recurringBillingData = new RecurringBilling_Model_ProductRecurringBillingData();
		$recurringBillingData->hydrateFromForm($formData['recurring_billing_data']);

		$formData['recurring_billing_data'] = serialize($recurringBillingData->toArray());
	}

	/**
	 * @return Admin_Service_ProductExport|null
	 */
	protected function getProductExportService()
	{
		if (is_null($this->exportService))
		{
			$this->exportService = new Admin_Service_ProductExport($this->getDb());
		}

		return $this->exportService;
	}

	/**
	 * Our action for product copy
	 */
	public function copyAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$session = $this->getSession();

		$copyFields = $request->get('copy');

		/**
		 * Update field selected & save to session
		 */
		$productCopyOptions = array();
		foreach(Admin_Service_ProductCopy::getDefaultFields() as $defaultField)
		{
			if (isset($copyFields['selected-fields']) && is_array($copyFields['selected-fields']) && in_array($defaultField['field'], $copyFields['selected-fields']))
			{
				$productCopyOptions[] = $defaultField;
			}
		}
		$session->set('productCopyOptions', $productCopyOptions);

		/**
		 * Update selected checkboxes & save to session
		 */
		$productCopyChecked = array_fill_keys(array_keys($this->productCopyCheckedDefaults), 0);
		foreach ($copyFields as $key => $value)
		{
			if (isset($productCopyChecked[$key]))
			{
				$productCopyChecked[$key] =  $value;
			}
		}
		$session->set('productCopyChecked', $productCopyChecked);

		$copyService = $this->getProductCopyService();
		$copyService->copy($request);

		Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), true, true, false, false);

		// redirect back our users
		$this->redirect('products');
	}

	/**
	 * Returns the product basic fields for copying
	 */
	public function getProductFieldsAction()
	{
		header('Content-Type: application/json');
		echo json_encode(Admin_Service_ProductCopy::getDefaultFields());
	}

	/**
	 * Get copy service
	 *
	 * @return Admin_Service_ProductCopy
	 */
	protected function getProductCopyService()
	{
		if (is_null($this->copyService))
		{
			$db = $this->getDb();

			$this->copyService = new Admin_Service_ProductCopy(
				$this->getProductRepository(),
				array(
					'productAttributeRepository' => new DataAccess_ProductAttributeRepository($db),
					'productVariantRepository' => new DataAccess_ProductVariantRepository($db),
					'productQuantityDiscount' => new DataAccess_ProductQuantityDiscountRepository($db),
					'productImageRepository' => new DataAccess_ProductImageRepository($this->getDb(), $this->getSettings(), $this->getFileUploadService())
				)
			);
		}

		return $this->copyService;
	}

	/**
	 * Update Bulk Products
	 */
	public function bulkUpdateAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();

		$session->remove('bulkEditFields');

		$mode = self::MODE_UPDATE;

		@set_time_limit(3000);

		if (!Nonce::verify($request->get('nonce', 'product_edit'))) {
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('products');
			return;
		}

		$repository = $this->getProductRepository();

		$editMode = $request->get('edit_mode');

		$formData = array(
			'is_change_product_price' => $request->get('is_change_product_price'),
			'state' => $request->get('state'),
			'product_price' => $request->get('product_price'),
			'round_product_price' => $request->get('round_product_price'),
			'product_price_discount_type' => $request->get('product_price_discount_type'),
			'product_sale_price' => $request->get('product_sale_price'),
			'is_change_product_sale_price' => $request->get('is_change_product_sale_price'),
			'round_product_sale_price' => $request->get('round_product_sale_price'),
			'product_sale_price_discount_type' => $request->get('product_sale_price_discount_type'),
			'category' => $request->get('category'),
			'visibility' => $request->get('visibility'),
			'tax_class' => $request->get('tax_class'),
			'call_for_price' => $request->get('call_for_price'),
			'is_hot_deal' => $request->get('is_hot_deal'),
			'is_home_page' => $request->get('is_home_page'),
		);

		if ($editMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			$formData['ids'] = $ids;

			$session->set('bulkEditFields', $formData);

			foreach ($ids as $id)
			{
				$productDetails[] = $repository->getProductById($id, true);
			}

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one product to update', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($repository->edit($ids, $formData))
				{
					if ($formData['category'] != 'no_change')
					{
						$categories = array();
						foreach ($ids as $id)
						{
							$repository->persistProductCategories($id, $mode, $categories, $formData['category']);
						}
					}
					Admin_Flash::setMessage('common.success');
					for($i = 0; $i < count($productDetails); $i++)
					{
						Admin_Log::log('Products: Product updated - '.$productDetails[$i]['title'], Admin_Log::LEVEL_IMPORTANT);
					}
				}
				else
				{
					Admin_Flash::setMessage('Cannot edit one or more products.', Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else if ($editMode == 'search')
		{
			$searchParams = $session->get('productsSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			$session->set('bulkEditFields', $formData);

			$ids = $repository->getList(null, null, 'pid', $searchParams, 'p.pid');

			if ($repository->editBySearchParams($searchParams, $ids, $formData))
			{
				if ($formData['category'] != 'no_change')
				{
					$categories = array();
					foreach ($ids as $id)
					{
						$repository->persistProductCategories($id['pid'], $mode, $categories, $formData['category']);
					}
				}
				Admin_Flash::setMessage('common.success');
			}
			else
			{
				Admin_Flash::setMessage('Cannot edit one or more products.', Admin_Flash::TYPE_ERROR);
			}
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('products');
	}
}
<?php

/**
 * Class Admin_Controller_CustomField
 */
class Admin_Controller_CustomField extends Admin_Controller
{
	/** @var DataAccess_CustomFormRepository */
	protected $customFormRepository = null;

	/** @var DataAccess_CustomFieldRepository */
	protected $customFieldRepository = null;

	/**
	 * Add custom field
	 */
	public function addCustomFieldAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$customFieldsRepository = $this->getCustomFieldRepository();

			$defaults = $customFieldsRepository->getDefaults();

			$formData = array_merge($defaults, $request->get('customField', array()));
			$formData['field_id'] = null;
			$formData['field_place'] = 'form';

			if (($validationErrors = $customFieldsRepository->getValidationErrors($formData, $mode)) == false)
			{
				$customFieldsRepository->persist($formData);
				$result = array('status' => 1, 'customFields' => $customFieldsRepository->getListByFormId($formData['custom_form_id']));
			}
			else
			{
				$result = array('status' => 0, 'errors' => $validationErrors);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Update custom field
	 */
	public function updateCustomFieldAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$customFieldRepository = $this->getCustomFieldRepository();

			$formData = array_merge($customFieldRepository->getDefaults(), $request->get('customField', array()));

			$customFieldData = $customFieldRepository->getById($formData['field_id']);

			if ($customFieldData)
			{
				if (($validationErrors = $customFieldRepository->getValidationErrors($formData, $mode)) == false)
				{
					$formData['field_place'] = 'form';
					$customFieldRepository->persist($formData);
					$result = array('status' => 1, 'customFields' => $customFieldRepository->getListByFormId($formData['custom_form_id']));
				}
				else
				{
					$result = array('status' => 0, 'errors' => $validationErrors);
				}
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find custom field by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update custom field. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Delete custom field
	 */
	public function deleteCustomFieldAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$customFieldRepository = $this->getCustomFieldRepository();

			$formData = array_merge($customFieldRepository->getDefaults(), $request->get('customField', array()));

			$customFieldData = $customFieldRepository->getById($formData['field_id']);

			if ($customFieldData)
			{
				$customFieldRepository->delete(array($formData['field_id']));
				$result = array('status' => 1, 'customFields' => $customFieldRepository->getListByFormId($formData['custom_form_id']));
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find custom field by id')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot update custom field. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * Get custom field repository
	 */
	protected function getCustomFieldRepository()
	{
		if (is_null($this->customFieldRepository))
		{
			$this->customFieldRepository = new DataAccess_CustomFieldRepository($this->getDb());
		}

		return $this->customFieldRepository;
	}
}
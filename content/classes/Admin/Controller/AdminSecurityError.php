<?php

class Admin_Controller_AdminSecurityError extends Admin_Controller
{
    public function indexAction()
    {
        if (!defined("ENV")) exit();
        $adminView = $this->getView();
        $adminView->assign('body', 'templates/pages/security-error.html');
        $adminView->render('layouts/default');
    }
}
<?php

/**
 * Class Admin_Controller_Doba
 */
class Admin_Controller_Doba extends Admin_Controller
{
	/** @var DataAccess_ProductsRepository $productRepository */
	protected $productRepository = null;

	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9019');

		$partnerPermissionStatus = $this->getPartnerPermissionStatus();
		if ($partnerPermissionStatus == 'pending')
		{
			Admin_Flash::setMessage('You must login to your Doba Account and approve the activation so the cart can access your account.', Admin_Flash::TYPE_INFO);
		}

		$adminView->assign('body', 'templates/pages/doba/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @throws Exception
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('doba_password', 'doba_username', 'doba_retailer_id', 'doba_mode');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['doba_retailer_id']) || trim($settingsData['doba_retailer_id']) == '')
			{
				$errors[] = array('field' => '.field-doba_retailer_id', 'message' => trans('apps.doba.retailer_id_req'));
			}
			if (!isset($settingsData['doba_username']) || trim($settingsData['doba_username']) == '')
			{
				$errors[] = array('field' => '.field-doba_username', 'message' => trans('apps.doba.username_req'));
			}
			if (!isset($settingsData['doba_password']) || trim($settingsData['doba_password']) == '')
			{
				$errors[] = array('field' => '.field-doba_password', 'message' => trans('apps.doba.password_req'));
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				$partnerPermissionStatus = $this->getPartnerPermissionStatus();
				if ($partnerPermissionStatus == 'missing request' || $partnerPermissionStatus == 'declined')
				{
					$this->sendPartnerPermissionRequest();
				}

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('doba');

				$countryRepository = new DataAccess_CountriesStatesRepository($this->getDb());
				$usa = $countryRepository->getCountryByIso2Code('US');

				$shippingMethodsRepository = new DataAccess_ShippingMethodRepository($this->getDb());
				$dobaShippingMethod = $shippingMethodsRepository->getActiveMethodByCarrierMethod('doba', 'doba');

				if ($dobaShippingMethod)
				{
					$dobaShippingMethod['hidden'] = 'No';
					$dobaShippingMethod['country'] = $usa['coid'];
				}
				else
				{
					$dobaShippingMethod = array(
						'carrier_id' => 'doba',
						'carrier_name' => 'Shipping',
						'method_id' => 'doba',
						'method_calc_mode' => 'before_discount',
						'method_name' => 'Flat rate',
						'country' => $usa['coid'],
						'country_exclude' => '0',
						'state' => '0',
						'weight_min' => '0.0',
						'weight_max' => '10000.00',
						'hidden' => 'No'
					);
				}

				$shippingMethodsRepository->persist($dobaShippingMethod);

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$dobaForm = new Admin_Form_DobaForm();

		$form = $dobaForm->getDobaActivateForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	/**
	 * Deactivate doba
	 */
	public function deactivateAction()
	{
		$settings = $this->getSettings();

		$settings->persist(array('doba_retailer_id' => '', 'doba_username' => '', 'doba_password' => ''));

		$shippingMethodsRepository = new DataAccess_ShippingMethodRepository($this->getDb());
		$dobaShippingMethod = $shippingMethodsRepository->getActiveMethodByCarrierMethod('doba', 'doba');

		if ($dobaShippingMethod)
		{
			$shippingMethodsRepository->delete($dobaShippingMethod['ssid']);
		}
	}

	/**
	 * @throws Exception
	 */
	public function settingsAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$settingsVars = array('doba_price_option', 'doba_price_option99', 'doba_min_stock',
			'doba_mode', 'doba_username', 'doba_password', 'doba_retailer_id',
			'doba_autosend_orders', 'doba_payment_type', 'doba_cron_update', 'doba_images', 'doba_images_secondary',
			'doba_callback_mode', 'doba_callback_access_key', 'doba_callback_urls', 'doba_price_modifier'
		);

		$settingsDefaults = array(
			'doba_autosend_orders' => 'No', 'doba_price_option99' => 'No',
			'doba_cron_update' => 'No', 'doba_images' => 'No',
		);

		$errors = array();
		if ($request->isPost())
		{
			$formData = array_merge($settingsDefaults, $request->request->getByKeys($settingsVars));

			if (!isset($formData['doba_retailer_id']) || trim($formData['doba_retailer_id']) == '')
			{
				$errors['doba_retailer_id'] = array(trans('apps.doba.retailer_id_req'));
			}
			if (!isset($formData['doba_username']) || trim($formData['doba_username']) == '')
			{
				$errors['doba_username'] = array(trans('apps.doba.username_req'));
			}
			if (!isset($formData['doba_password']) || trim($formData['doba_password']) == '')
			{
				$errors['doba_password'] = array(trans('apps.doba.password_req'));
			}

			if (count($errors) < 1)
			{
				$settings->persist($formData);

				Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
				$this->redirect('doba', array('mode' => 'settings'));
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9020');

		$partnerPermissionStatus = $this->getPartnerPermissionStatus();
		if ($partnerPermissionStatus == 'pending')
		{
			Admin_Flash::setMessage('You must login to your Doba Account and approve the activation so the cart can access your account.', Admin_Flash::TYPE_INFO);
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$dobaForm = new Admin_Form_DobaForm();
		$form = $dobaForm->getForm($settingsData);


		if (count($errors) > 0)
		{
			if (isset($formData)) $form->bind($formData);
			$form->bindErrors($errors);
		}

		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/doba/settings.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @return mixed
	 */
	protected function getPartnerPermissionStatus()
	{
		$dobaRetailerAPI = $this->getDobaRetailerApi();

		$r = $dobaRetailerAPI->hasPartnerPermission();

		return $r['response']['has_permission'];
	}

	/**
	 * @return mixed
	 */
	protected function sendPartnerPermissionRequest()
	{
		$dobaRetailerAPI = $this->getDobaRetailerApi();

		return $dobaRetailerAPI->requestPartnerPermission();
	}

	/**
	 * @return Doba_RetailerApi
	 */
	protected function getDobaRetailerApi()
	{
		global $settings, $db;
		return new Doba_RetailerApi($settings, $db);
	}

	/**
	 * Deletes/Clean up all doba products on our table
	 */
	public function deleteDobaProductsAction()
	{
		$repository = $this->getProductRepository();

		$dobaProducts = $repository->getDobaProducts();

		$ids = array();
		foreach($dobaProducts as $dobaProduct)
		{
			$ids[] = $dobaProduct['pid'];
		}

		if (count($ids) == 0)
		{
			Admin_Flash::setMessage('Currently there are no Doba products imported', Admin_Flash::TYPE_ERROR);
		}
		else
		{
			if ($repository->delete($ids))
			{
				Admin_Flash::setMessage('common.success');
			}
			else
			{
				Admin_Flash::setMessage('Cannot delete one or more products.', Admin_Flash::TYPE_ERROR);
			}
		}

		$this->redirect('app', array('key' => 'doba'));
	}

	/**
	 * Get product repository
	 *
	 * @return DataAccess_ProductsRepository
	 */
	protected function getProductRepository()
	{
		global $db;

		if (is_null($this->productRepository))
		{
			$this->productRepository = new DataAccess_ProductsRepository($db);
		}

		return $this->productRepository;
	}
}
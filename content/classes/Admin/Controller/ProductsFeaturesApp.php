<?php

/**
 * Class Admin_Controller_ProductsFeaturesApp
 */
class Admin_Controller_ProductsFeaturesApp extends Admin_Controller
{
	protected $menuItem = array('primary' => 'products', 'secondary' => '');

	/**
	 * @return bool
	 */
	public function canAccessProductsFilters()
	{
		return defined('_ACCESS_PRODUCTS_FILTERS') && _ACCESS_PRODUCTS_FILTERS && _CAN_PURCHASE_PRODUCTS_FILTERS;
	}

	/**
	 * @return bool
	 */
	public function canActivateProductsFilters()
	{
		return defined('_CAN_ENABLE_PRODUCTS_FILTERS') && _CAN_ENABLE_PRODUCTS_FILTERS;
	}


	/**
	 * Index action
	 */
	public function indexAction()
	{
		$adminView = $this->getView();

		$adminView->assign('helpTag', '');

		$adminView->assign('body', 'templates/pages/product-feature-app/app-index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Activate recurring billing
	 */
	public function activateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('CatalogUseProductFilters' => '1'));
	}

	/**
	 * Deactivate recurring billing
	 */
	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('CatalogUseProductFilters' => '0'));
	}


}
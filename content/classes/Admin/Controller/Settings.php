<?php

/**
 * Class Admin_Controller_Settings
 */
class Admin_Controller_Settings extends Admin_Controller
{
	/**
	 * Settings
	 */
	public function indexAction()
	{
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', array('primary' => 'settings', 'secondary' => 'setup'));
		$adminView->assign('helpTag', '8000');

		$adminView->assign('body', 'templates/pages/settings/setup-dashboard.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Advanced settings
	 */
	public function advancedSettingsAction()
	{
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', array('primary' => 'settings', 'secondary' => 'settings-advanced'));
		$adminView->assign('helpTag', '8013');

		$adminView->assign('access_filemanager', AccessManager::checkAccess('FILEMANAGER'));

		$adminView->assign('body', 'templates/pages/settings/advanced-dashboard.html');
		$adminView->render('layouts/default');
	}
}
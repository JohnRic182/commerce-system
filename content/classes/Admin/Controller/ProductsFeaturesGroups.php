<?php

/**
 * Class Admin_Controller_ProductsFeaturesGroups
 */
class Admin_Controller_ProductsFeaturesGroups extends Admin_Controller
{
	/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
	protected $productsFeaturesRepository = null;

	/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository */
	protected $productsFeaturesGroupsRepository = null;

	/** @var DataAccess_ProductsFeaturesGroupsFeaturesRepository  $productsFeaturesGroupsFeaturesRepository */
	protected $productsFeaturesGroupsFeaturesRepository = null;

	protected $searchParamsDefaults = array('search_str' => '', 'feature_group_active' => 1, 'logic' => 'AND');

	protected $menuItem = array('primary' => 'products', 'secondary' => 'products-features');

	/**
	 * List products features groups
	 */
	public function listAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);

		/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository */
		$productsFeaturesGroupsRepository = $this->getProductsFeaturesGroupsRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** Prepare search & list parameters */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('featuresGroupsSearchParams', array()); else $session->set('featuresGroupsSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('featuresGroupsSearchOrderBy', 'feature_group_name'); else $session->set('featuresGroupsSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('featuresGroupsSearchOrderDir', 'asc'); else $session->set('featuresGroupsSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		/** @var Admin_Form_ProductFeatureGroupForm $featureGroupForm */
		$featureGroupForm = new Admin_Form_ProductFeatureGroupForm();
		$adminView->assign('searchForm', $featureGroupForm->getProductFeatureGroupSearchForm($searchFormData));

		$itemsCount = $productsFeaturesGroupsRepository->getCount($searchParams, $searchParams['logic']);

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('featuresGroups',
				$productsFeaturesGroupsRepository->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'pfg.*, IF(pfg.feature_group_active = 1, "Yes", "No") AS feature_group_status',
					$searchParams['logic']
				)
			);
		}

		$adminView->assign('delete_nonce', Nonce::create('feature_group_delete'));
		$adminView->assign('body', 'templates/pages/product-feature-group/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new products features groups
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository */
		$productsFeaturesGroupsRepository = $this->getProductsFeaturesGroupsRepository();

		/** @var Admin_Form_ProductFeatureGroupForm $featureGroupForm */
		$featureGroupForm = new Admin_Form_ProductFeatureGroupForm();

		$defaults = $productsFeaturesGroupsRepository->getDefaults($mode);

		// set feature group active to true by default
		$defaults['feature_group_active'] = 1;

		/** @var core_Form_Form $form */
		$form = $featureGroupForm->getForm($defaults, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formValues = $request->request->all();

			// check if feature group status is set to active/inactive
			if(!isset($formValues['feature_group_active'])) $formValues['feature_group_active'] = 0;

			$formData = array_merge($defaults, $formValues);
			if (!Nonce::verify($formData['nonce'], 'feature_group_add'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('features_groups');
			}
			else
			{
				if (($validationErrors = $productsFeaturesGroupsRepository->getValidationErrors($formData, $mode)) == false)
				{
					$featureGroupId = $productsFeaturesGroupsRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('feature_groups', array('id' => $featureGroupId, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('feature_group_add'));

		$adminView->assign('body', 'templates/pages/product-feature-group/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update products features groups
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
		$productsFeaturesRepository = $this->getProductsFeaturesRepository();

		/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository */
		$productsFeaturesGroupsRepository = $this->getProductsFeaturesGroupsRepository();

		/** @var DataAccess_ProductsFeaturesGroupsFeaturesRepository $productsFeaturesGroupsFeaturesRepository */
		$productsFeaturesGroupsFeaturesRepository = $this->getProductsFeaturesGroupsFeaturesRepository();

		$featureGroupId = intval($request->get('id'));

		$featureGroupData = $productsFeaturesGroupsRepository->getById($featureGroupId);

		//check if feature group exist
		if (!$featureGroupData)
		{
			Admin_Flash::setMessage('Cannot find products features groups by provided ID', Admin_Flash::TYPE_ERROR);
			$this->redirect('features_groups');
		}

		$featureGroupData['group_features'] = $this->getProductsFeaturesGroupsFeaturesRepository()->getListByFeatureGroupId($featureGroupId);

		/** @var Admin_Form_ProductFeatureGroupForm $featureGroupForm */
		$featureGroupForm = new Admin_Form_ProductFeatureGroupForm();

		$defaults = $productsFeaturesGroupsRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $featureGroupForm->getForm($featureGroupData, $mode, $featureGroupId);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'feature_group_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('features_groups');
			}
			else
			{
				if (($validationErrors = $productsFeaturesGroupsRepository->getValidationErrors($formData, $mode, $featureGroupId)) == false)
				{
					$productsFeaturesGroupsRepository->persist($formData);

					foreach ($formData['priority'] as $productFeatureId => $priorityValue)
					{
						$productsFeaturesGroupsFeaturesRepository->updateFeaturesPriority($formData['id'], $productFeatureId, $priorityValue);
					}

					Admin_Flash::setMessage('common.success');
					$this->redirect('feature_groups', array('id' => $featureGroupId, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('productsFeaturesAvailable', json_encode($this->getProductsFeaturesRepository()->getList(null, null, 'feature_name_asc')));

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('title', $featureGroupData['feature_group_name']);
		$adminView->assign('mode', $mode);
		$adminView->assign('id', $featureGroupId);
		$adminView->assign('form', $form);

		$productFeatureForm = new Admin_Form_ProductFeatureForm();

		$productFeatureDefaults = $productsFeaturesRepository->getDefaults($mode);

		// set feature active to true by default
		$productFeatureDefaults['feature_active'] = 1;
		$adminView->assign('productFeatureForm', $productFeatureForm->getForm($productFeatureDefaults, 'add'));

		$adminView->assign('nonce', Nonce::create('product_feature_update'));
		$adminView->assign('body', 'templates/pages/product-feature-group/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete product features groups
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'feature_group_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('features_groups');
			return;
		}

		/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository */
		$productsFeaturesGroupsRepository = $this->getProductsFeaturesGroupsRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$featureGroupId = $request->get('id', 0);
			if ($featureGroupId == 0)
			{
				Admin_Flash::setMessage('Invalid Product Feature ID', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$productsFeaturesGroupsRepository->delete($featureGroupId);
				Admin_Flash::setMessage('Product feature group has been deleted');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$featureGroupIds = explode(',', $request->get('ids', ''));
			if (count($featureGroupIds) == 0)
			{
				Admin_Flash::setMessage('Please select at least one product feature group to delete', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$productsFeaturesGroupsRepository->delete($featureGroupIds);
				Admin_Flash::setMessage('Selected products features groups have been deleted');
			}
		}
		else if ($deleteMode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('featuresGroupsSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			if ($productsFeaturesGroupsRepository->deleteBySearchParams($searchParams))
			{
				Admin_Flash::setMessage('common.success');
			}
			else
			{
				Admin_Flash::setMessage('Cannot delete one or more products features groups.', Admin_Flash::TYPE_ERROR);
			}
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('features_groups');
	}

	/**
	 * Add new product feature
	 */
	public function addGroupFeatureAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$result = array('status' => 1);

		if ($request->isPost())
		{
			$productsFeaturesRepository = $this->getProductsFeaturesRepository();
			$productsFeaturesGroupsFeaturesRepository = $this->getProductsFeaturesGroupsFeaturesRepository();

			$defaults = $productsFeaturesRepository->getDefaults($mode);
			$formData = array_merge($defaults, $request->request->all());

			$featureGroupId = $request->get('field-product_feature_group_id', 0);

			if (($validationErrors = $productsFeaturesRepository->getValidationErrors($formData, $mode)) == false)
			{
				$productFeatureId = $productsFeaturesRepository->persist($formData);

				$groupFeatureData = array();
				$groupFeatureData['product_feature_group_id'] = $featureGroupId;
				$groupFeatureData['product_feature_id'] = $productFeatureId;
				$productsFeaturesGroupsFeaturesRepository->persist($groupFeatureData);

				$result = array(
					'status' => 1,
					'productsFeaturesAvailable' => $this->getProductsFeaturesRepository()->getList(null, null, 'feature_name_asc'),
					'productsFeaturesAssigned' => $productsFeaturesGroupsFeaturesRepository->getListByFeatureGroupId($featureGroupId)
				);
			}
			else
			{
				$result = array('status' => 0, 'errors' => $validationErrors);
			}
		}
		$this->renderJson($result);
	}

	/**
	 * Add feature to feature group
	 */
	public function assignGroupFeatureAction()
	{
		$request = $this->getRequest();

		$result = array('status' => 1);

		if ($request->isPost())
		{
			$featureGroupId = $request->get('feature_group_id', 0);

			if ($featureGroupId)
			{
				$groupFeatures = explode(',', $request->get('features_assigned', ''));

				$productsFeaturesGroupsFeaturesRepository = $this->getProductsFeaturesGroupsFeaturesRepository();

				$productsFeaturesGroupsFeaturesRepository->updateGroupFeatures($featureGroupId, $groupFeatures);

				$result = array('status' => 1, 'productsFeaturesAssigned' => $productsFeaturesGroupsFeaturesRepository->getListByFeatureGroupId($featureGroupId));
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Delete feature from feature group
	 */
	public function deleteGroupFeatureAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$productsFeaturesGroupsFeaturesRepository = $this->getProductsFeaturesGroupsFeaturesRepository();
			$formData = $request->get('feature', array());

			$featureData = $productsFeaturesGroupsFeaturesRepository->getFeatureGroupFeatureById($formData['product_feature_group_relation_id']);
			if ($featureData)
			{
				$productsFeaturesGroupsFeaturesRepository->delete($formData['product_feature_group_relation_id']);
				$result = array('status' => 1, 'productsFeaturesAssigned' => $productsFeaturesGroupsFeaturesRepository->getListByFeatureGroupId($formData['product_feature_group_id']));
			}
			else
			{
				$result = array('status' => 0, 'errors' => array('error' => array('Cannot find product feature group feature by ID')));
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot delete product feature group features. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 * @return DataAccess_ProductsFeaturesGroupsFeaturesRepository
	 */
	protected function getProductsFeaturesGroupsFeaturesRepository()
	{
		if (is_null($this->productsFeaturesGroupsFeaturesRepository))
		{
			$this->productsFeaturesGroupsFeaturesRepository = new DataAccess_ProductsFeaturesGroupsFeaturesRepository($this->getDb());
		}

		return $this->productsFeaturesGroupsFeaturesRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesGroupsRepository
	 */
	protected function getProductsFeaturesGroupsRepository()
	{
		if (is_null($this->productsFeaturesGroupsRepository))
		{
			$this->productsFeaturesGroupsRepository = new DataAccess_ProductsFeaturesGroupsRepository($this->getDb());
		}
		
		return $this->productsFeaturesGroupsRepository;
	}

	/**
	 * return DataAccess_ProductsFeaturesRepository
	 */
	protected function getProductsFeaturesRepository()
	{
		if (is_null($this->productsFeaturesRepository))
		{
			$this->productsFeaturesRepository = new DataAccess_ProductsFeaturesRepository($this->getDb());
		}

		return $this->productsFeaturesRepository;
	}
}
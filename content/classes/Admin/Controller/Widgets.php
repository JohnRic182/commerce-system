<?php

class Admin_Controller_Widgets extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/** @var DataAccess_OffsiteCampaignRepository */
	protected $repository;

	const CAMPAIGN_TYPE = 'wg';

	public function indexAction()
	{
		$repository = $this->getRepository();

		$adminView = $this->getView();

		$adminView->assign('campaigns', $repository->getList(self::CAMPAIGN_TYPE));
		$adminView->assign('itemsCount', $repository->getCount(self::CAMPAIGN_TYPE));

		$adminView->assign('settingsForm', $this->getSettingsForm());

		$repository = $this->getRepository();
		$defaults = $repository->getDefaults(self::CAMPAIGN_TYPE);
		$adminView->assign('campaignForm', $this->getCampaignForm($defaults));

		$adminView->assign('settings_nonce', Nonce::create('widgets_settings'));
		$adminView->assign('add_nonce', Nonce::create('widgets_campaign_add'));
		$adminView->assign('delete_nonce', Nonce::create('widgets_campaign_delete'));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9033');

		$adminView->assign('body', 'templates/pages/widgets/index.html');
		$adminView->render('layouts/default');
	}

	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$repository = $this->getRepository();

		$defaults = $repository->getDefaults(self::CAMPAIGN_TYPE);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'],'widgets_campaign_add', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
			{
				$repository->persist($formData);

				Admin_Flash::setMessage('common.success');

				$this->renderJson(array(
					'status' => 1
				));
			}
			else
			{
				$this->renderJson(array(
					'status' => 0,
					'errors' => $validationErrors,
				));
			}
		}
	}

	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getRepository();

		$id = intval($request->get('id'));

		$campaign = $repository->getById($id, self::CAMPAIGN_TYPE);

		if (!$campaign)
		{
			Admin_Flash::setMessage('Cannot find campaign by provided id', Admin_Flash::TYPE_ERROR);

			$this->renderJson(array(
				'status' => 0,
				'errors' => '',
			));
			return;
		}

		$defaults = $repository->getDefaults(self::CAMPAIGN_TYPE);

		$form = $this->getCampaignForm($campaign);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'],'widgets_campaign_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
			{
				$repository->persist($formData);

				Admin_Flash::setMessage('common.success');

				$this->renderJson(array(
					'status' => 1
				));
			}
			else
			{
				$this->renderJson(array(
					'status' => 0,
					'errors' => $validationErrors,
				));
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('updateNonce', Nonce::create('widgets_campaign_update'));

		$html = $adminView->fetch('pages/widgets/modal-widgets-edit');

		$this->renderJson(array(
			'status' => 1,
			'html' => $html,
		));
	}

	public function deleteAction()
	{
		$request = $this->getRequest();

		$repository = $this->getRepository();

		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('app', array('key' => 'widgets'));
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('widgets.invalid_campaign_id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete(array($id));

				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'widgets'));
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('widgets.select_at_least_one_campaign', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'widgets'));
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'widgets'));
			}
		}
		else if ($deleteMode == 'search')
		{
			$repository->deleteBySearchParams(self::CAMPAIGN_TYPE);
			Admin_Flash::setMessage('common.success');
			$this->redirect('app', array('key' => 'widgets'));
		}
	}

	public function generate()
	{
		$request = $this->getRequest();
		$pid = intval($request->request->get('product_id'));
		$error = false;

		if ($pid == null)
		{
			$error = "Cannot find product";
		}
		else
		{
			$settings = $this->getSettings();
			$db = $this->getDb();
			$db->query("SELECT * FROM ".DB_PREFIX."products WHERE pid = ".intval($pid));

			if (($product = $db->moveNext()) != false)
			{
				if ($product["image_location"] == "Local")
				{
					$thumb = ImageUtility::productHasThumb($product["product_id"]);
					$thumb2 = ImageUtility::productHasThumb2($product["product_id"]);
				}
				else
				{
					$thumb = $thumb2 = $product["image_url"];
				}

				$thumb = $thumb ? $settings->get("GlobalHttpUrl")."/".$thumb : false;
				$thumb2 = $thumb2 ? $settings->get("GlobalHttpUrl")."/".$thumb2 : false;

				$campaign_id = intval($request->request->get('campaign_id', false));

				$widgets = new Widgets();

				$settingsValues = $settings->getAll();
				$offsite_commerce_campaigns = new OffsiteCommerceCampaigns($db, $settingsValues);

				$w_type = $request->request->get("widget_type");
				$w_itype = '';
				if ($request->request->get("widget_image_type") != 'none')
				{
					$w_itype = $request->request->get("widget_image_type") == "thumb" ? $thumb : $thumb2;
				}

				$this->renderJson(array(
					"status" => 1,
					"widget_html" => $widgets->renderProductStatic(
							$request->request->get("widget_style"),
							$product["title"],
							getAdminPrice($product["price"]),
							$product["overview"],
							$w_type == "cart" ?
								$offsite_commerce_campaigns->getProductAddToCartUrl($pid, $campaign_id, "wg", false) :
								($w_type == 'checkout'?$offsite_commerce_campaigns->getProductAddToCartUrl($pid, $campaign_id, "wg", true):
								$offsite_commerce_campaigns->getProductUrl($pid, $campaign_id, "wg")),
							$w_type == "page" ? "Learn More":"Buy Now",
							$w_itype,
							$product["image_alt_text"]
						)
				));
			}
			else
			{
				$error = "Cannot find product";
			}
		}

		if ($error)
		{
			$this->renderJson(array(
				"status" => 0,
				"message" => $error
			));
		}
	}

	public function activateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('widgets_active' => 'Yes'));
	}

	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('widgets_active' => 'No'));
	}

	public function settingsAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'widgets_settings'))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}

			$data = array(
				'widgets_type' => $request->request->get('widgets_type'),
				'widgets_image_type' => $request->request->get('widgets_image_type'),
				'widgets_style' => $request->request->get('widgets_style'),
			);

			$settings->persist($data);
			Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
		}

		$this->renderJson(array(
			'status' => 1,
		));
	}

	/**
	 * Get our widget advance settings form
	 * @return mixed
	 */
	protected function getSettingsForm()
	{
		$settings = $this->getSettings();
		$widgetsForm = new Admin_Form_WidgetsForm();

		return $widgetsForm->getWidgetsSettingsForm($settings);
	}

	/**
	 * get Campaign Form
	 * @return core_Form_Form
	 */
	protected function getCampaignForm($formData)
	{
		$widgetsForm = new Admin_Form_WidgetsForm();

		return $widgetsForm->getWidgetsCampaignForm($formData);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_OffsiteCampaignRepository
	 */
	protected function getRepository()
	{
		if ($this->repository === null)
		{
			$this->repository = new DataAccess_OffsiteCampaignRepository($this->getDb(), $this->getSettings());
		}

		return $this->repository;
	}
}
<?php

/**
 * Class Admin_Controller_Order
 */
class Admin_Controller_Order extends Admin_Controller
{
	protected $repository = null;

	/** @var DataAccess_UserRepository */
	protected $userRepository = null;

	/** @var DataAccess_OrderShipmentRepository */
	protected $shipmentRepository = null;

	/** @var FraudService_DataAccess_FraudMethodRepository */
	protected $fraudServiceRepository = null;

	protected $menuItem = array('primary' => 'orders', 'secondary' => '');

	protected $exportFileName = 'orders.csv';

	protected $searchParamsDefaults = array(
		'order_num' => '',
		'custom_search' => '',
		'status' => 'any',
		'fulfillment_status' => '',
		'order_type' => ORDER::ORDER_TYPE_WEB,
		'payment_status' => 'any',
		'period' => 'any',
		'product_id' => '',
		'min_amount' => '',
		'max_amount' => '',
		'state' => '',
		'zip_code' => '',
		'notes_content' => '',
		'promo_code' => '',
		'product_name' => '',
		'logic' => 'AND',
		'advanced_search' => '0',
		'from' => '',
		'to' => ''
	);

	/**
	 * List products
	 */
	public function listAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();
		$adminView = $this->getView();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('ordersSearchParams', array()); else $session->set('ordersSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('ordersSearchOrderBy', 'order_num'); else $session->set('ordersSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('ordersSearchOrderDir', 'desc'); else $session->set('ordersSearchOrderDir', $orderDir);

		$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchParams['advanced_search'] = ($searchParams['advanced_search'] == 'on') ? 1 : 0;
		if ($searchParams['advanced_search'] == 0)
		{
			$searchParams['state'] = '';
			$searchParams['zip_code'] = '';
			$searchParams['notes_content'] = '';
			$searchParams['promo_code'] = '';
			$searchParams['product_name'] = '';
		}

		if (isset($searchParams['uid']) && intval($searchParams['uid']) > 0)
		{
			$userRepository = $this->getUserRepository();
			$user = $userRepository->getById(intval($searchParams['uid']));
			if ($user)
			{
				$user->getUserData();
				$adminView->assign('customer', $user->getUserData());
			}
			else
			{
				unset($searchParams['uid']);
				$session->set('ordersSearchParams', $searchParams);
			}
		}

		/**
		 * Recently abandoned / failed orders
		 */
		if (isset($searchParams['abandon']) && strlen($searchParams['abandon']) > 0)
		{
			$searchParams['status'] = array(Order::STATUS_ABANDON, Order::STATUS_FAILED);
		}

		/**
		 * Orders to be fullfilled
		 */
		if (isset($searchParams['fulfilled']) && strlen($searchParams['fulfilled']) > 0)
		{
			$searchParams['payment_status'] = Order::PAYMENT_STATUS_RECEIVED;
			$searchParams['fulfillment_status'] = array(Order::FULFILLMENT_STATUS_PENDING, Order::FULFILLMENT_STATUS_PARTIAL);
		}

		/**
		 * Orders by State & Province
		 */
		if (isset($searchParams['bystate']) && strlen($searchParams['bystate']) > 0)
		{
			$searchParams['status'] = Order::STATUS_COMPLETED;
			$searchParams['payment_status'] = Order::PAYMENT_STATUS_RECEIVED;
		}

		$searchFormData = $searchParams;
		$searchFormData['recurringEnabled'] = defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING && $this->getSettings()->get('RecurringBillingEnabled') == '1';
		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		/**
		 * Get data from DB
		 */
		$repository = $this->getRepository();
		$itemsCount = $repository->getCount($searchParams, $searchParams['logic']);

		/**
		 * Show results
		 */
		$orderForm = new Admin_Form_OrderForm();
		$adminView->assign('searchParams', $searchParams);
		$adminView->assign('searchForm', $orderForm->getOrderSearchForm($searchFormData));
		$adminView->assign('searchFormAdvanced', $orderForm->getOrderSearchAdvancedForm($searchFormData));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign(
				'orders',
				$repository->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'o.*',
					$searchParams['logic']
				)
			);
		}
		else
		{
			$adminView->assign('orders', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('order_delete'));
		$adminView->assign('export_nonce', Nonce::create('order_export'));
		$adminView->assign('update_nonce', Nonce::create('order_bulk_update'));

		$adminView->assign('updateStatusForm', $orderForm->getOrderUpdateStatusForm());

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '1000');

		$adminView->assign('body', 'templates/pages/order/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update order action
	 */
	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		global $msg;
		require($settings->get('GlobalServerPath').'/content/languages/'.escapeFileName($settings->get('ActiveLanguage')).'.php');

		$db = $this->getDb();

		$id = intval($request->get('id'));

		$order_data = $this->getOrderData($id);

		if (!$order_data)
		{
			Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$orderEditable = $order_data["payment_status"] == ORDER::PAYMENT_STATUS_PENDING;

		$order = $this->getOrderObject($order_data);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->get('nonce', 'order_update')))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('orders');
				return;
			}

			$action = $request->get('action', 'update');
			if ($action == 'return-inventory')
			{
				Listener_StockControl::returnToInventory($order);

				Admin_Flash::setMessage('common.success');
				Admin_Log::log('Orders: Order updated - '.$order->getOrderNumber(), Admin_Log::LEVEL_IMPORTANT);
				$this->redirect('order', array('id' => $id));
				return;
			}
			else
			{
				$shipping_error_message = '';
				AdminOrderProvider::editOrder($order_data, $order, $shipping_error_message);

				if (trim($shipping_error_message) != '')
				{
					Admin_Flash::setMessage($shipping_error_message, Admin_Flash::TYPE_ERROR);
				}
				else
				{
					Admin_Flash::setMessage('common.success');
					$this->redirect('order', array('id' => $id));
					return;
				}
			}
		}

		// TODO: move into repository
		$admin_notes = $db->selectAll("
			SELECT a.*,
				DATE_FORMAT(a.created_at , '".$settings->get('LocalizationDateTimeFormat')."') AS note_created_at
			FROM ".DB_PREFIX."admin_notes a
			WHERE
				a.oid=".$id
		);

		// TODO: move into repository
		$transactions = $db->selectAll("SELECT *, DATE_FORMAT(completed, '".$settings->get('LocalizationDateTimeFormat')."') AS co FROM ".DB_PREFIX."payment_transactions WHERE oid = ".$id." ORDER BY completed");

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '1001');

		$settingsArray = $settings->getAll();
		$shipmentsView = new View_OrderShipmentsViewModel($settingsArray, $msg);
		$shipments = $this->getShipmentRepository()->getShipmentsByOrderId($id);
		$adminView->assign('shipments', $shipmentsView->getShipmentsView($shipments));

		$adminView->assign('id', $id);
		$adminView->assign('nonce', Nonce::create('order_update'));
		$adminView->assign('nonce_edit_shipment', Nonce::create('nonce_edit_shipment'));

		$adminView->assign('orderEditable', $orderEditable);
		$adminView->assign('order', $order_data);
		$adminView->assign('shippingRequired', $order->getShippingRequired());

		//order custom  fields
		$customFields = new CustomFields($db);

		$adminView->assign('cf_billing', $customFields->getCustomFieldsValues('billing', $order_data['uid'], 0, 0));
		$adminView->assign('cf_shipping', $customFields->getCustomFieldsValues('shipping', $order_data['uid'], $id, 0));
		$adminView->assign('cf_invoice', $customFields->getCustomFieldsValues('invoice', $order_data['uid'], $id, 0));

		$adminView->assign('admin_notes', $admin_notes);
		$adminView->assign('transactions', $transactions);

		$orderForm = new Admin_Form_OrderForm();

		$adminView->assign('addOrderNoteForm', $orderForm->getOrderAddOrderNoteForm());

		$adminView->assign('updateStatusForm', $orderForm->getOrderUpdateStatusForm());

		$showRefund = false;
		$showVoid = false;
		$showCapture = false;
		$capturableAmount = false;
		$supportsPartialCapture = false;
		$refundableAmount = false;
		$capturedAmount = false;

		if (trim($order_data['payment_method_type']) != '' && $order_data['payment_method_type'] != 'custom')
		{
			$provider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, $settings));
			$gateway = $provider->getPaymentProcessorById($order_data['payment_method_id']);

			if ($gateway)
			{
				$showRefund = $gateway->supportsRefund($order_data);
				$showVoid = $gateway->supportsVoid($order_data);
				$showCapture = $gateway->supportsCapture($order_data);

				$capturableAmount = $gateway->capturableAmount($order_data);
				$supportsPartialCapture = $gateway->supportsPartialCapture($order_data);
				$refundableAmount = $gateway->refundableAmount($order_data);
				$capturedAmount = $gateway->capturedAmount($order_data);
			}
		}
		else if ($order_data['payment_method_name'] == 'Gift Certificate')
		{
			$capturedAmount = $order_data['gift_cert_amount'];
		}

		$adminView->assign('showRefund', $showRefund);
		$adminView->assign('showVoid', $showVoid);
		$adminView->assign('showCapture', $showCapture);
		$adminView->assign('capturableAmount', $capturableAmount);
		$adminView->assign('supportsPartialCapture', $supportsPartialCapture);
		$adminView->assign('refundableAmount', $refundableAmount);
		$adminView->assign('capturedAmount', $capturedAmount);

		$data = array('showRefund' => $showRefund, 'showVoid' => $showVoid, 'showCapture' => $showCapture,
			'capturableAmount' => $capturableAmount, 'supportsPartialCapture' => $supportsPartialCapture,
			'refundableAmount' => $refundableAmount, 'capturedAmount' => $capturedAmount);

		$adminView->assign('processPaymentForm', $orderForm->getOrderProcessPaymentForm($data));

		$fulfillmentRepository = new DataAccess_FulfillmentRepository($db);

		$order->fulfillments = $fulfillmentRepository->getFulfillments($id);
		$fulfillment = Model_Fulfillment::createFulfillmentFromOrder($order);
		$allowPartialFulfillment = false;
		$fulfillmentItems = array();

		if ($fulfillment)
		{
			/** @var Model_FulfillmentItem $fulfillmentItem */
			foreach ($fulfillment->getItems() as $lineItemId => $fulfillmentItem)
			{
				/** @var Model_LineItem $lineItem */
				$lineItem = $order->lineItems[$lineItemId];

				if ($lineItem->getFinalQuantity() > $fulfillmentItem->getQuantity())
				{
					$lineItem->setFulfillmentStatus(trans('orders.fulfillment_options.partial'));
				}
				else
				{
					$lineItem->setFulfillmentStatus(trans('orders.not_fulfilled'));
				}

				$fulfillmentItems[] = array(
					'title' => $lineItem->getTitle(),
					'quantity' => $fulfillmentItem->getQuantity(),
					'lineItemId' => $fulfillmentItem->getLineItemId(),
					'shipmentId' => $fulfillmentItem->getShipmentId()
				);
				if ($fulfillmentItem->getQuantity() > 1 || count($fulfillmentItems) > 1)
				{
					$allowPartialFulfillment = true;
				}
			}
		}

		foreach ($order->lineItems as $lineItem)
		{
			if (!$lineItem->getFulfillmentStatus())
			{
				$lineItem->setFulfillmentStatus(trans('orders.fulfilled'));
			}
		}

		$fulfillmentViews = Admin_View_FulfillmentViewModel::getFulfillmentViews($order->fulfillments, $order);
		$adminView->assign('fulfillments', $fulfillmentViews);
		$adminView->assign('fulfillment', $fulfillment);
		$adminView->assign('fulfillment_items', $fulfillmentItems);

		/*  Fraud Information */
		$fraudTransactions  = $this->getFraudServiceRepository()->getFraudTransactionForOrder($order->getId());
		$fraudScoreImg = false;
		$fraudForm = false;
		$fraudDataView = null;
		$fraudScore = false;
		$fraudStatus = false;
		$fraudscorecaption = 'Fraud Service Score: ';
		$fraudstatuscaption = 'Fraud Service Status: ';
		if ($fraudTransactions)
		{
			$fraudDataView = new FraudService_View_FraudTransactionView($db, $this->getSettings(), $fraudTransactions);
			$fraudProvider = FraudService_Provider_FraudServiceProvider::getProviderInstanceByProviderId($db, $this->getSettings(), $fraudTransactions['fid']);
			if (!is_null($fraudProvider) && (isset($fraudTransactions['score']) && $fraudTransactions['score']))
			{
				$fraudScore = $fraudTransactions['score'];
				$method = $fraudProvider->getFraudMethod();
				if ($method)
				{
					$fraudscorecaption = $method->getTitle().' Score: ';
				}
			}

			if (!is_null($fraudProvider) && (isset($fraudTransactions['fraud_status']) && $fraudTransactions['fraud_status']))
			{
				$method = $fraudProvider->getFraudMethod();
				if ($method)
				{
					$fraudstatuscaption = $method->getTitle().' Status: ';
				}
				if (trim($fraudTransactions['error']) =='')
				{
					$fraudStatus = $fraudTransactions['fraud_status'];
				}
			}

			if ($fraudDataView)
			{
				$fraudForm = $fraudDataView->getTransactionDataForm();
			}

		}
		$adminView->assign('frauddata', ($fraudDataView)?$fraudDataView:false);
		$adminView->assign('frauddataForm', $fraudForm);

		$adminView->assign('fraudstatuscaption', ($fraudStatus)?$fraudstatuscaption:false);
		$adminView->assign('fraudstatus', ($fraudStatus)?$fraudStatus:false);

		$adminView->assign('fraudscorecaption', ($fraudScore)?$fraudscorecaption:false);
		$adminView->assign('fraudscore', ($fraudScore)?$fraudScore:false);
		/*----------------------------------------*/

		$hasInventory = false;
		$hasDobaItems = false;
		foreach ($order->lineItems as $lineItem)
		{
			/** @var Model_LineItem $lineItem */
			if ($lineItem->getQuantityFromStock() > 0)
			{
				$hasInventory = true;
			}
			
			if ($lineItem->getIsDoba() === true || $lineItem->getIsDoba() == 'Yes')
			{
				$hasDobaItems = true;
			}
		}
		$adminView->assign('hasInventory', $hasInventory);
		$adminView->assign('hasDobaItems', $hasDobaItems);

		$recurringBillingRepository = new RecurringBilling_DataAccess_RecurringProfileRepository($db, $this->getSettings());

		$parentRecurringProfile = false;
		if ($order->getOrderType() == ORDER::ORDER_TYPE_RECURRING)
		{
			$recurringProfileData = $recurringBillingRepository->getProfileByRecurringOrderId($id);

			if ($recurringProfileData)
			{
				$parentRecurringProfile = new RecurringBilling_Model_RecurringProfile($recurringProfileData);
			}
		}
		$adminView->assign('parentRecurringProfile', $parentRecurringProfile);

		$lineItems = array();
		$certrepo = new DataAccess_GiftCertificateRepository($db);
		foreach ($order->lineItems as $orderLineItem)
		{
			/** @var Model_LineItem $orderLineItem */
			$recurringProfile = null;
			if ($orderLineItem->getEnableRecurringBilling())
			{
				$recurringProfile = $recurringBillingRepository->getByLineItemId($orderLineItem->getId());
			}

			$lineView = new Admin_View_OrderLineItemViewModel($orderLineItem, $recurringProfile, $msg);
			if ($lineView->productId == DataAccess_GiftCertificateRepository::GS_PRODUCTID){
				$cert = $certrepo->getGiftCertificateByOrderId($order->getId());
				$lineView->setGiftCertId($cert['id']);
			}
			$lineItems[] = $lineView;
		}

		$adminView->assign('lineItems', $lineItems);

		$adminView->assign('isDobaSetup', $settings->get('doba_username') != '' && $settings->get('doba_password') != '');

		$adminView->assign('hasStoredCardData', $order_data['card_data'] != '' && $this->canAccessCCS());

		// TODO: move into repository
		$pc_details = $db->selectOne("
			SELECT o.promo_campaign_id, p.*
			FROM ".DB_PREFIX."orders  o
				LEFT JOIN ".DB_PREFIX."promo_codes p ON o.promo_campaign_id = p.pid
			WHERE o.oid=".$id
		);

		$adminView->assign('pc_details', $pc_details);

		$isBongoOrder = $order_data['payment_gateway_id'] == 'bongocheckout';
		$adminView->assign('isBongoOrder', $isBongoOrder);

		$adminView->assign('bongoOrderId', false);
		$adminView->assign('bongoOrderData', false);
		$bongoAddress = array();
		if ($isBongoOrder)
		{
			$adminView->assign('bongoOrderId', trim($order_data['custom3']));

			if (trim($order_data['custom2']) != '')
			{
				$bongoOrderData = @unserialize($order_data['custom2']);

				if (is_array($bongoOrderData))
				{
					$adminView->assign('bongoOrderData', $bongoOrderData);
				}
			}

			$allowPartialFulfillment = false;

			$regions = new Regions($db);
			$bongoState = $regions->getStateById($settings->get('bongocheckout_State'));
			$bongoAddress['name'] = $settings->get('bongocheckout_Attention_To');
			$bongoAddress['company'] = trim($settings->get('bongocheckout_Company_Name'));
			$bongoAddress['address1'] = trim($settings->get('bongocheckout_Address1'));
			$bongoAddress['address2'] = trim($settings->get('bongocheckout_Address2'));
			$bongoAddress['city'] = trim($settings->get('bongocheckout_City'));
			$bongoAddress['zip'] = trim($settings->get('bongocheckout_Zip_Code'));
			$bongoAddress['province'] = $bongoState ? $bongoState['name'] : '';
			$bongoAddress['country_name'] = 'United States';
			$bongoAddress['phone'] = trim($settings->get('bongocheckout_Phone'));
		}

		$adminView->assign('bongoAddress', $bongoAddress);

		$adminView->assign('allowPartialFulfillment', $allowPartialFulfillment);

		// Endicia
		$endiciaEnabled = $order->getShippingRequired() && $this->getEndiciaEnabled($shipments);
		$adminView->assign('endiciaEnabled', $endiciaEnabled);

		// VIPparcel
		$vparcelEnabled = $order->getShippingRequired() && $this->getVparcelEnabled($shipments);
		$adminView->assign('vparcelEnabled', $vparcelEnabled);		

		if ($endiciaEnabled)
		{
			$adminView->assign('orderEndiciaRefundNonce', Nonce::create('endicia_refund'));
			$orderEndiciaForm = new Admin_Form_OrderEndiciaForm();
			$countriesStatesRepository = new DataAccess_CountriesStatesRepository($this->getDb());
			$endiciaRepository = new DataAccess_EndiciaRepository($this->getDb());
			$adminView->assign('orderEndiciaLabels', $endiciaLabels = $endiciaRepository->getLabelsByOrderId($order->getId()));
			$adminView->assign('orderEndiciaForm', $orderEndiciaForm->getForm($this->getSettings(), $countriesStatesRepository, $order));
		}

		if ($vparcelEnabled)
		{
			$adminView->assign('ordervparcelRefundNonce', Nonce::create('vparcel_refund'));
			$ordervparcelForm = new Admin_Form_OrderVparcelForm($this->getDb(), $this->getSettings());
			$countriesStatesRepository = new DataAccess_CountriesStatesRepository($this->getDb());

			$vparcelRepository = new DataAccess_VparcelShippingLabels($this->getDb(), $this->getSettings()->getAll());
			$adminView->assign('ordervparcelForm', $ordervparcelForm->getForm($countriesStatesRepository, $order));
			$adminView->assign('orderVparcelLabels', $vparcelLabels = $vparcelRepository->getLabelsByOrderId($order->getId()));
		}

		// Stamps
		// TODO: pass shipments?
		$stampsEnabled = $order->getShippingRequired() && $this->getStampsEnabled($shipments);
		$adminView->assign('stampsEnabled', $stampsEnabled);

		if ($stampsEnabled)
		{
			$orderStampsForm = new Admin_Form_OrderStampsForm();
			$adminView->assign('orderStampsForm', $orderStampsForm->getForm());

			$stampsRepository = new DataAccess_StampsRepository($this->getDb());

			$stampsLabels = $stampsRepository->getLabelsByOrderId($id);
			$activeLabels = array();

			foreach ($stampsLabels as &$stampsLabel)
			{
				$stamps_expired = false;
				$stamps_data = @unserialize($stampsLabel['data']);

				if (!is_array($stamps_data))
				{
					$stamps_data = array();
				}

				if (isset($stamps_data['Rate']) && is_array($stamps_data['Rate']) && isset($stamps_data['Rate']['ShipDate']))
				{
					$stamps_expired = true;
					$stamps_ship_date = $stamps_data['Rate']['ShipDate'];
					$stamps_ts_ship_date = strtotime('+1 day', strtotime($stamps_ship_date));
					$stamps_ts_current_date = mktime(0, 0, 0);

					if ($stamps_ts_ship_date >= $stamps_ts_current_date)
					{
						$stamps_expired = false;
					}
				}
				$stampsLabel['expired'] = $stamps_expired;
				if (!$stamps_expired && trim($stampsLabel['tracking_number']) != '') $activeLabels[] = $stampsLabel;
			}
			$adminView->assign('orderStampsLabels', $stampsLabels);
			$adminView->assign('activeStampsLabels', $activeLabels);
		}

		$adminView->assign('body', 'templates/pages/order/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @return bool
	 */
	private function canAccessCCS()
	{
		global $auth_rights;
		return AccessManager::checkAccess('CCS') && isPrivilege(array('ccs', 'all'), $auth_rights);
	}

	/**
	 * Store card action
	 */
	public function storedCardAction()
	{
		global $auth_admin_username;

		$request = $this->getRequest();
		$settings = $this->getSettings();
		$result = array('status' => 0, 'message' => 'Cannon retrieve credit card data');

		$orderId = intval($request->get('id'));

		$orderData = $this->getOrderData($orderId);

		if (!$orderData || !$this->canAccessCCS())
		{
			$result = array('status' => 0, 'message' => 'orders.order_not_found');
		}
		else if ($request->isPost())
		{
			$username = $request->request->get('ccs-username');
			$password = $request->request->get('ccs-password');

			Admin_Log::log('Access to CCS - order #'.$orderData['order_num'], Admin_Log::LEVEL_IMPORTANT, true, $auth_admin_username);

			if ($orderData['card_data'] != '')
			{
				$cardData = unserialize(urldecode($orderData['card_data']));

				$openSSL = new openSSL(4096, OPENSSL_KEYTYPE_RSA, 3650, $settings->get('GlobalServerPath').'/content/ssl/openssl.cnf');

				$decrypted = $openSSL->decrypt($cardData['cr'], $username.$password, base64_decode($settings->get('SecurityCCSPrivateKey')));

				if ($decrypted)
				{
					$result = array('status' => 1, 'data' => unserialize($decrypted));
				}
				else
				{
					$result = array('status' => 0 , 'message' => 'Cannon retrieve credit card data. Please check username & password');
				}
			}
			else
			{
				$result = array('status' => 0 , 'message' => 'This order has no card data stored');
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Remove card action
	 */
	public function removeCardAction()
	{
		$request = $this->getRequest();
		$result = array('status' => 0, 'message' => 'Cannot remove credit card');

		if ($request->isPost())
		{
			$orderId = intval($request->get('id'));
			$orderData = $this->getOrderData($orderId);

			if (!$orderData || !$this->canAccessCCS())
			{
				$result = array('status' => 0, 'message' => 'orders.order_not_found');
			}
			else
			{
				// TODO: move into repository?
				$this->getDb()->query('UPDATE '.DB_PREFIX.'orders SET card_data="" WHERE oid='.$orderId);
				$result = array('status' => 1);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Add note
	 */
	public function addNoteAction()
	{
		$request = $this->getRequest();

		$db = $this->getDb();

		$id = intval($request->get('id'));

		$order_data = $this->getOrderData($id);
		$repository = $this->getRepository();

		if (!$order_data)
		{
			Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		if ($request->isPost())
		{
			$sendEmail = $request->request->get('send_email');
			$title = trim($request->request->get('title'));
			$note = trim($request->request->get('note'));

			$success = false;
			$errors = array();

			if ($title == '') $errors['title'] = array('orders.title_required');
			if ($note == '') $errors['note'] = array('orders.note_required');

			if (count($errors) == 0)
			{
				$order = $this->getOrderObject($order_data);
				$user = $order->getUser();

				if ($sendEmail == 'Yes')
				{
					view()->initNotificationsSmarty();
					Notifications::emailOrderNoteForCustomer($order, $user, $title, $note);
				}

				$db->reset();
				$db->assign('oid', $id);
				$db->assignStr('note_for', 'order');
				$db->assignStr('send_email', $sendEmail);
				$db->assign('created_at', 'NOW()');
				$db->assignStr('note_title', $title);
				$db->assignStr('note_text', $note);
				$db->insert(DB_PREFIX.'admin_notes');

				Admin_Flash::setMessage('common.success');
				Admin_Log::log('Orders: Order added notes - '.$order->getOrderNumber(), Admin_Log::LEVEL_IMPORTANT);
				$success = true;
			}

			$data = array('status' => $success ? 1 : 0, 'errors' => $errors);

			$this->renderJson($data);
			return;
		}

		$this->redirect('order', array('id' => $id));
	}

	/**
	 * Edit shipment
	 */
	public function editShipmentAction()
	{
		$request = $this->getRequest();

		$db = $this->getDb();

		$id = intval($request->get('id'));

		$order_data = $this->getOrderData($id);

		if (!$order_data)
		{
			Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		if ($request->isPost())
		{
			if (!Nonce::verify($request->get('nonce', 'nonce_edit_shipment')))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('order', array('id'=>$id));
				return;
			}

			$tracking_number = $request->request->get('tracking_number');
			$tracking_number = isset($tracking_number) && is_array($tracking_number) ? $tracking_number : array();
			$tracking_company = $request->request->get('tracking_company');
			$tracking_company = isset($tracking_company) && is_array($tracking_company) ? $tracking_company : array();

			$success = false;
			$errors = array();

			$settings = $this->getSettings();
			$order = $this->getOrderObject($order_data);
			$repository = new DataAccess_FulfillmentRepository($db, $settings);
			$fulfillments = $repository->getFulfillments($id);

			if ($fulfillments)
			{
				foreach ($fulfillments as $k => $fulfillment)
				{
					if (isset($tracking_number[$k]) && isset($tracking_company[$k]))
					{
						$fulfillment->setTrackingCompany($tracking_company[$k]);
						$fulfillment->setTrackingNumber($tracking_number[$k]);
						$repository->save($fulfillment);
						$success = true;
					}
				}
			}
			else
			{
				$errors[''] = array('orders.cannot_update_fulfillment');
			}

			$data = array('status' => $success ? 1 : 0, 'errors' => $errors);

			$this->renderJson($data);
			return;
		}

		$this->redirect('order', array('id' => $id));
	}

	/**
	 * Add fulfillment
	 */
	public function addFulfillmentAction()
	{
		$request = $this->getRequest();
		$session = $this->getSession();
		$aid = $session->get('admin_auth_id', false);
		$id = intval($request->get('id'));

		if ($request->isPost())
		{
			$ret = $this->processFulfillmentForOrder($id);

			if (!$ret)
			{
				Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
				$this->redirect('orders');
				return;
			}
			else if (is_array($ret))
			{
				// log event
				Admin_Log::log('Order #'.$id.' fulfilled', Admin_Log::LEVEL_NORMAL, true, $this->getAdminUsername($aid));
				if ($ret['status']) Admin_Flash::setMessage('common.success');

				$this->renderJson($ret);
				return;
			}
		}

		$this->redirect('order', array('id' => $id));
	}

	/**
	 * Bulk fulfillment
	 */
	public function bulkFulfillmentAction()
	{
		$request = $this->getRequest();

		$mode = $request->request->get('form-orders-fulfill-mode');

		$success = false;

		if ($mode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('orders.no_orders_selected_fulfill', Admin_Flash::TYPE_ERROR);
				$this->redirect('orders');
				return;
			}

			foreach ($ids as $id)
			{
				$ret = $this->processFulfillmentForOrder($id, true);

				if ($ret && is_array($ret))
				{
					if ($ret['status']) $success = true;
				}
			}
		}
		else if ($mode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('ordersSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			$start = 0;
			$repository = $this->getRepository();
			do
			{
				$list = $repository->getList($start, 500, 'o.oid', $searchParams, 'o.oid');

				foreach ($list as $o)
				{
					$id = $o['oid'];
					$ret = $this->processFulfillmentForOrder($id, true);
					if ($ret && is_array($ret))
					{
						if ($ret['status'])
						{
							$success = true;
						}
					}
				}

				$start += count($list);

			} while (count($list) > 0);
		}

		if ($success) Admin_Flash::setMessage('common.success');

		$this->renderJson(array('status' => $success, 'errors' => array()));
	}

	/**
	 * @param $id
	 * @param bool $fulfillAll
	 * @return array|bool
	 */
	protected function processFulfillmentForOrder($id, $fulfillAll = false)
	{
		$id = intval($id);

		if ($id < 1) return false;

		// This is here because payment gateways need these to be defined in global
		global $order, $user;

		$request = $this->getRequest();
		$settings = $this->getSettings();

		$db = $this->getDb();

		$order_data = $this->getOrderData($id);

		if ($request->isPost())
		{
			$completeOrder = $request->request->get('complete_order') == 'on';
			$sendEmail = $request->request->get('send_email') == 'on';
			$trackingCompany = $request->request->get('tracking_company');
			$trackingNumber = $request->request->get('tracking_number');
			$quantities = $request->request->get('quantity', array());
			$capturePayment = $request->request->get('capture_payment') == 'on';
			$stampsLabel = intval($request->request->get('stamps_label', 0));
			$endiciaLabel = intval($request->request->get('endicia_label', 0));
			$vparcelLabel = intval($request->request->get('vparcel_label', 0));

			if (!is_array($quantities))
			{
				$quantities = array();
			}

			$order = $this->getOrderObject($order_data);

			$fulfillment = Model_Fulfillment::createFulfillmentFromOrder($order);
			if ($fulfillment == null)
			{
				return array('status' => true, 'errors' => array());
			}

			$fulfillment->setStatus(Model_Fulfillment::STATUS_COMPLETED);

			if ($vparcelLabel > 0)
			{
				$label = $db->selectOne("
					SELECT tracking_number
					FROM ".DB_PREFIX."vparcel_shippinglabels WHERE order_id=".intval($id)." AND slid = ".intval($vparcelLabel)
				);

				if ($label && trim($label['tracking_number']) != '')
				{
					$fulfillment->setTrackingCompany('USPS');
					$fulfillment->setTrackingNumber($label['tracking_number']);
				}
			}
			else if ($stampsLabel > 0)
			{
				$label = $db->selectOne("
					SELECT tracking_number
					FROM ".DB_PREFIX."orders_labels WHERE oid=".intval($id)." AND olid = ".intval($stampsLabel)." AND type='stampscom'"
				);

				if ($label && trim($label['tracking_number']) != '')
				{
					$fulfillment->setTrackingCompany('USPS');
					$fulfillment->setTrackingNumber($label['tracking_number']);
				}
			}
			else if ($endiciaLabel > 0)
			{
				$label = $db->selectOne("
					SELECT tracking_number
					FROM ".DB_PREFIX."endicia_shippinglabels
					WHERE order_id=".intval($id)." AND slid = ".intval($endiciaLabel)
				);

				if ($label && trim($label['tracking_number']) != '')
				{
					$fulfillment->setTrackingCompany('USPS');
					$fulfillment->setTrackingNumber($label['tracking_number']);
				}
			}
			else
			{
				$fulfillment->setTrackingCompany($trackingCompany);
				$fulfillment->setTrackingNumber($trackingNumber);
			}

			$items = $fulfillment->getItems();
			$validItems = array();

			foreach ($items as $lineItemId => $fulfillmentItem)
			{
				$quantity = false;

				/** @var Model_FulfillmentItem $fulfillmentItem */
				if ($fulfillAll)
				{
					$quantity = $fulfillmentItem->getQuantity();
				}
				else if (isset($quantities[$lineItemId]) && intval($quantities[$lineItemId]) > 0)
				{
					$quantity = intval($quantities[$lineItemId]);
				}

				if ($quantity)
				{
					if (isset($order->lineItems[$lineItemId]))
					{
						/** @var Model_LineItem $lineItem */
						$lineItem = $order->lineItems[$lineItemId];

						if ($lineItem->getFulfilledQuantity() + $quantity > $lineItem->getFinalQuantity())
						{
							$quantity = $lineItem->getFinalQuantity() - $lineItem->getFulfilledQuantity();
						}

						$lineItem->setFulfilledQuantity($lineItem->getFulfilledQuantity() + $quantity);
						$lineItem->setFulfillmentStatus(
							$lineItem->getFulfilledQuantity() == $lineItem->getFinalQuantity() ?
								ORDER::FULFILLMENT_STATUS_COMPLETED :
								ORDER::FULFILLMENT_STATUS_PARTIAL
						);

						$fulfillmentItem->setQuantity($quantity);
						$validItems[$lineItemId] = $fulfillmentItem;
					}
				}
			}

			$fulfillment->setItems($validItems);

			$success = false;
			$errors = array();
			$errorMessage = false;

			if (count($validItems) < 1)
			{
				$errors['quantity'] = array(trans('orders.cannot_create_empty_fulfillment'));
			}
			else
			{
				$success = true;

				view()->initNotificationsSmarty();

				$paymentStatus = '';

				if ($capturePayment && trim($order_data['payment_method_type']) != '' && $order->getPaymentStatus() == ORDER::PAYMENT_STATUS_PENDING)
				{
					if ($order_data['payment_method_type'] == 'custom')
					{
						$paymentStatus = ORDER::PAYMENT_STATUS_RECEIVED;
					}
					else
					{
						$provider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, $settings));
						$gateway = $provider->getPaymentProcessorById($order_data['payment_method_id']);

						if ($gateway)
						{
							$user = $order->getUser();
							$capturableAmount = $gateway->capturableAmount($order_data);
							$captureAmount = floatval($request->request->get('capture_amount', $capturableAmount));

							if ($captureAmount > 0)
							{
								$gateway->capture($order_data, $captureAmount);

								Admin_Log::log('Order #'.$order_data['order_num'].' - funds captured ('.$captureAmount.')', Admin_Log::LEVEL_IMPORTANT);
								$success = !$gateway->is_error;
								if (!$success)
								{
									$errorMessage = $gateway->error_message;
								}
								else if ($capturableAmount - $captureAmount <= 0)
								{
									$paymentStatus = ORDER::PAYMENT_STATUS_RECEIVED;
								}
							}
							else
							{
								if ($capturableAmount > 0)
								{
									$success = false;
									$errorMessage = 'Amount to Capture must be greater than 0';
								}
								else
								{
									$success = true;
									$paymentStatus = ORDER::PAYMENT_STATUS_RECEIVED;
								}
							}
						}
					}
				}

				if ($success)
				{
					if ($sendEmail)
					{
						Notifications::emailOrderShipped($db, $order, $fulfillment);
					}

					$repository = new DataAccess_FulfillmentRepository($db, $settings);
					$repository->save($fulfillment);

					$fulfilledStatus = ORDER::FULFILLMENT_STATUS_COMPLETED;

					foreach ($order->lineItems as $lineItem)
					{
						if ($lineItem->getFulfillmentStatus() != ORDER::FULFILLMENT_STATUS_COMPLETED)
						{
							$fulfilledStatus = ORDER::FULFILLMENT_STATUS_PARTIAL;
							break;
						}
					}

					$order->setFulfillmentStatus($fulfilledStatus);

					$orderStatus = '';

					if ($completeOrder && $fulfilledStatus == ORDER::FULFILLMENT_STATUS_COMPLETED)
					{
						$orderStatus = ORDER::STATUS_COMPLETED;
					}

					if ($orderStatus != '' || $paymentStatus != '')
					{
						$this->updateOrderStatus($id, $orderStatus, $paymentStatus, $sendEmail, $order_data, $order);
					}

					$event = new Events_FulfillmentEvent(Events_FulfillmentEvent::ON_COMPLETED);
					$event->setOrder($order);
					$event->setFulfillment($fulfillment);
					Events_EventHandler::handle($event);

					$sendBongoTracking = $request->request->get('send_bongo_tracking') == 'on';

					if ($sendBongoTracking)
					{
						if ($order_data['payment_gateway_id'] == 'bongocheckout' && trim($order_data['custom2']) != '')
						{
							$bongoOrderData = @unserialize($order_data['custom2']);

							if (is_array($bongoOrderData) && isset($bongoOrderData['trackingLink']) && trim($bongoOrderData['trackingLink']) != '')
							{
								$result = $db->query('SELECT product_id, product_sub_id, admin_quantity FROM '.DB_PREFIX.'orders_content WHERE oid='.$id);
								$bongoOrderItems = array();

								while (($productData = $db->moveNext($result)) != false)
								{
									$bongoOrderItems[] = array(
										'id' => $productData['product_id'].(isset($productData['product_sub_id']) && trim($productData['product_sub_id']) != '' ? '-'.$productData['product_sub_id'] : ''),
										'quantity' => $productData['admin_quantity']
									);
								}

								$bongoConnect = new Payment_Bongo_BongoConnect($settings->get('bongocheckout_Partner_Key'));

								$bongoOrderTrackingUpdateResult = $bongoConnect->orderTrackingUpdate(
									$bongoOrderItems, $bongoOrderData['bongoOrderId'],
									$trackingCompany, $trackingNumber
								);

								if (!$bongoOrderTrackingUpdateResult)
								{
									$bongoError = $bongoConnect->getLastError().'<br/>'.$bongoConnect->getLastErrorDetails();
									Admin_Flash::setMessage($bongoError, Admin_Flash::TYPE_ERROR);
								}
							}
						}
					}

					$order->persist(true);
				}
			}

			$shipmentRepository = $this->getShipmentRepository();
			$shipmentRepository->updateStatus($id);

			return array(
				'status' => $success ? 1 : 0,
				'errors' => $errors,
				'message' => $errorMessage
			);
		}

		return false;
	}

	/**
	 * Delete products
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'order_delete')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$repository = $this->getRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = intval($request->get('id', 0));

			if ($id < 1)
			{
				Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($repository->delete(array($id)))
				{
					Admin_Flash::setMessage('common.success');
					Admin_Log::log('Orders: Order deleted - '.$repository->getOrderNumber($id), Admin_Log::LEVEL_IMPORTANT);
				}
				else
				{
					Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
				}

				$this->redirect('orders');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('orders.no_orders_selected_delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('orders');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('common.success');

				foreach($ids as $id)
				{
					Admin_Log::log('Orders: Order deleted - '.$repository->getOrderNumber($id), Admin_Log::LEVEL_IMPORTANT);
				}

				$this->redirect('orders');
			}
		}
		else if ($deleteMode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('ordersSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			$repository->deleteBySearchParams($searchParams);
			Admin_Flash::setMessage('common.success');

			$this->redirect('orders');
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);

			$this->redirect('orders');
		}
	}

	/**
	 * Bulk update orders
	 */
	public function updateStatusAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'update_order')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$id = intval($request->get('id'));

		$orderStatus = $request->request->get('status');
		$paymentStatus = $request->request->get('payment_status');
		$sendNotificationEmail = $request->request->get('send_notification_email') == 'on';

		if (trim($orderStatus) == '' && trim($paymentStatus) == '')
		{
			$this->redirect('order', array('id' => $id));
			return;
		}

		$this->updateOrderStatus($id, $orderStatus, $paymentStatus, $sendNotificationEmail);
		$repository = $this->getRepository();
		Admin_Flash::setMessage('common.success');
		Admin_Log::log('Orders: Order updated status - ' . $repository->getOrderNumber($id), Admin_Log::LEVEL_IMPORTANT);
		$this->redirect('order', array('id' => $id));
	}

	/**
	 * Bulk update orders
	 */
	public function processPaymentAction()
	{
		$db = $this->getDb();
		$settings = $this->getSettings();

		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'update_order')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$id = intval($request->get('id'));

		$order_data = $this->getOrderData($id);

		if (!$order_data)
		{
			Admin_Flash::setMessage('order.order_not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
		}

		if (trim($order_data['payment_method_type']) != '' && $order_data['payment_method_type'] != 'custom')
		{
			$provider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, $settings));
			$gateway = $provider->getPaymentProcessorById($order_data['payment_method_id']);

			if ($gateway)
			{
				global $order, $user;
				$order = $this->getOrderObject($order_data);
				$user = $order->getUser();

				view()->initNotificationsSmarty();

				$success = false;

				$status = $request->request->get('status', '');
				switch ($status)
				{
					case 'capture':
					{
						$captureAmount = floatval($request->request->get('capture_amount', 0));
						if ($captureAmount > 0)
						{
							$gateway->capture($order_data, $captureAmount);
							Admin_Log::log('Order #'.$order_data['order_num'].' - funds captured ('.$captureAmount.')', Admin_Log::LEVEL_IMPORTANT);
							$success = !$gateway->is_error;
							$message = $success ? 'common.success' : $gateway->error_message;
						}
						else
						{
							$message = 'orders.invalid_capture_amount';
						}
						break;
					}
					case 'refund':
					{
						$refundAmount = floatval($request->request->get('refund_amount', 0));
						if ($refundAmount > 0)
						{
							$gateway->refund($order_data, $refundAmount);

							Admin_Log::log('Order #'.$order_data['order_num'].' - funds refunded ('.$refundAmount.')', Admin_Log::LEVEL_IMPORTANT);
							$success = !$gateway->is_error;
							$message = $success ? 'common.success' : $gateway->error_message;
						}
						else
						{
							$message = 'orders.invalid_refund_amount';
						}
						break;
					}
					case 'void':
					{
						$gateway->void($order_data);
						Admin_Log::log('Order #'.$order_data['order_num'].' - transaction void', Admin_Log::LEVEL_IMPORTANT);
						$success = !$gateway->is_error;
						$message = $success ? 'common.success' : $gateway->error_message;
						break;
					}
					default:
					{
						$success = false;
						$message = 'Invalid status';
					}
				}

				Admin_Flash::setMessage($message, $success ? Admin_Flash::TYPE_SUCCESS : Admin_Flash::TYPE_ERROR);
			}
		}

		$this->redirect('order', array('id' => $id));
	}

	/**
	 * Bulk update orders
	 */
	public function bulkUpdateAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'order_bulk_update')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$repository = $this->getRepository();

		$updateMode = $request->get('updateMode');

		$orderStatus = $request->request->get('status');
		$paymentStatus = $request->request->get('payment_status');
		$sendNotificationEmail = $request->request->get('send_notification_email') == 'on';

		if (trim($orderStatus) == '' && trim($paymentStatus) == '')
		{
			$this->redirect('orders');
			return;
		}

		if ($updateMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('orders.no_orders_selected_update', Admin_Flash::TYPE_ERROR);
				$this->redirect('orders');
			}
			else
			{
				foreach ($ids as $oid)
				{
					$this->updateOrderStatus($oid, $orderStatus, $paymentStatus, $sendNotificationEmail);
				}

				Admin_Flash::setMessage('common.success');

				$this->redirect('orders');
			}
		}
		else if ($updateMode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('ordersSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			$list = null;
			$start = 0;
			do
			{
				$list = $repository->getList($start, 500, 'oid', $searchParams, 'o.oid');
				if (count($list) > 0)
				{
					foreach ($list as $v)
					{
						$oids[] = $v['oid'];
						$this->updateOrderStatus(intval($v['oid']), $orderStatus, $paymentStatus, $sendNotificationEmail);
					}
					$start += count($list);
				}
			} while ($list !== null && count($list) > 0);

			Admin_Flash::setMessage('common.success');

			$this->redirect('orders');
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);

			$this->redirect('orders');
		}
	}

	/**
	 * Export categories
	 */
	public function exportAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$session = $this->getSession();
		$aid = $session->get('admin_auth_id', false);
		$exportMode = $request->get('exportMode');

		$exportService = $this->getExportService();

		if ($exportMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('orders.no_orders_selected_export', Admin_Flash::TYPE_ERROR);
				$this->redirect('orders');
			}
			else
			{
				$exportService->export($this->exportFileName, array('mode' => 'selected', 'ids' => $ids));
				// log event
				Admin_Log::log('Orders exported: order id(s) '. implode(',', $ids), Admin_Log::LEVEL_NORMAL, true, $this->getAdminUsername($aid));
			}
		}
		else if ($exportMode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('ordersSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			$exportService->export($this->exportFileName, array('mode' => 'search', 'searchParams' => $searchParams));
		}
	}

	public function approveFraudReviewAction()
	{
		$request = $this->getRequest();
		$id = intval($request->get('id'));

		$order_data = $this->getOrderData($id);

		if (!$order_data)
		{
			Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$logID = Admin_Log::log('Orders - Fraud Action: APPROVE ', Admin_Log::LEVEL_IMPORTANT);

		$order = $this->getOrderObject($order_data);
		$event = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_APPROVE_ORDER_ACTION);

		$event->setData('adminLogId', $logID);
		$event->setOrder($order);
		Events_EventHandler::handle($event);

		if ($event->getErrors())
		{
			$errors = $event->getErrors();
			$error_message = $errors[0];
			Admin_Flash::setMessage($error_message, Admin_Flash::TYPE_ERROR);
			$this->redirect('order', array('id' => $id));
		}
		else
		{
			Admin_Flash::setMessage('common.success');

			$oldOrderStatus = $event->getData('oldOrderStatus');
			$oldPaymentStatus = $event->getData('oldPaymentStatus');
			$newOrderStatus = $event->getData('newOrderStatus');
			$newPaymentStatus = $event->getData('newPaymentStatus');

			if($oldOrderStatus && $oldPaymentStatus && $newOrderStatus && $newPaymentStatus)
			{
				/**
				 * Handle on status change event
				 */
				$statusEvent = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);

				$order_num = $order->getOrderNumber();
				$statusEvent->setOrder($order)
					->setData('oldOrderStatus', $oldOrderStatus)
					->setData('oldPaymentStatus', $oldPaymentStatus)
					->setData('newOrderStatus', $newOrderStatus)
					->setData('newPaymentStatus', $newPaymentStatus);

				$orderStatusChanged =  ($oldOrderStatus != $newOrderStatus);
				$paymentStatusChanged =  ($oldPaymentStatus != $newPaymentStatus);

				Events_EventHandler::handle($statusEvent);

				Admin_Log::log(
					'Order #'.$order_num.' - '.
					($orderStatusChanged ? ' status changed from '.$oldOrderStatus.' to '.$newOrderStatus : '').
					($paymentStatusChanged ? ($orderStatusChanged ? ', ' : '').' payment status changed from '.$oldPaymentStatus.' to '.$newPaymentStatus : ''),
					Admin_Log::LEVEL_IMPORTANT
				);
			}
			$this->redirect('order', array('id' => $id));
		}

		return;
	}

	public function denyFraudReviewAction()
	{
		$request = $this->getRequest();
		$id = intval($request->get('id'));

		$order_data = $this->getOrderData($id);

		if (!$order_data)
		{
			Admin_Flash::setMessage('orders.order_not_found', Admin_Flash::TYPE_ERROR);
			$this->redirect('orders');
			return;
		}

		$logID = Admin_Log::log('Orders - Fraud Action: Deny ', Admin_Log::LEVEL_IMPORTANT);
		$order = $this->getOrderObject($order_data);
		$event = new FraudService_Events_FraudEvent(FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_DENY_ORDER_ACTION);

		$event->setData('adminLogId', $logID);
		$event->setOrder($order);
		Events_EventHandler::handle($event);

		if ($event->getErrors())
		{
			$errors = $event->getErrors();
			$error_message = $errors[0];
			Admin_Flash::setMessage($error_message, Admin_Flash::TYPE_ERROR);
			$this->redirect('order', array('id' => $id));
		}
		else
		{
			Admin_Flash::setMessage('common.success');

			$oldOrderStatus = $event->getData('oldOrderStatus');
			$oldPaymentStatus = $event->getData('oldPaymentStatus');
			$newOrderStatus = $event->getData('newOrderStatus');
			$newPaymentStatus = $event->getData('newPaymentStatus');

			if($oldOrderStatus && $oldPaymentStatus && $newOrderStatus && $newPaymentStatus)
			{
				/**
				 * Handle on status change event
				 */
				$statusEvent = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);

				$order_num = $order->getOrderNumber();
				$statusEvent->setOrder($order)
					->setData('oldOrderStatus', $oldOrderStatus)
					->setData('oldPaymentStatus', $oldPaymentStatus)
					->setData('newOrderStatus', $newOrderStatus)
					->setData('newPaymentStatus', $newPaymentStatus);

				$orderStatusChanged =  ($oldOrderStatus != $newOrderStatus);
				$paymentStatusChanged =  ($oldPaymentStatus != $newPaymentStatus);

				Events_EventHandler::handle($statusEvent);

				Admin_Log::log(
					'Order #'.$order_num.' - '.
					($orderStatusChanged ? ' status changed from '.$oldOrderStatus.' to '.$newOrderStatus : '').
					($paymentStatusChanged ? ($orderStatusChanged ? ', ' : '').' payment status changed from '.$oldPaymentStatus.' to '.$newPaymentStatus : ''),
					Admin_Log::LEVEL_IMPORTANT
				);
			}
			$this->redirect('order', array('id' => $id));
		}

		return;
	}

	/**
	 * @param $id
	 * @return array|bool
	 */
	protected function getOrderData($id)
	{
		global $settings;
		$db = $this->getDb();

		$orderRepository = new DataAccess_OrderRepository($db, $settings);

		return $orderRepository->getOrderDataForAdmin($id);
	}

	/**
	 * @param $order_data
	 * @return ORDER
	 */
	protected function getOrderObject($order_data)
	{
		global $settings;
		$db = $this->getDb();

		$oid = intval($order_data['oid']);

		$user = USER::getById($db, $settings, $order_data['uid']);
		$order = ORDER::getById($db, $settings, $user, $oid);

		$user->getUserData();
		$order->getOrderData();
		$order->getOrderItems(true);

		// have to get VAT status
		$taxProvider = Tax_ProviderFactory::getTaxProvider();
		$taxProvider->initTax($order);
		$settings['DisplayPricesWithTax'] = $taxProvider->getDisplayPricesWithTax() ? 'YES' : 'NO';

		return $order;
	}

	/**
	 * @param $oid
	 * @param $orderStatus
	 * @param $paymentStatus
	 * @param $sendNotificationEmail
	 * @param array $order_data
	 * @param ORDER $order
	 */
	protected function updateOrderStatus($oid, $orderStatus, $paymentStatus, $sendNotificationEmail, array $order_data = null, ORDER $order = null)
	{
		$db = $this->getDb();
		$oid = intval($oid);

		if ($order_data === null)
		{
			$order_data = $db->selectOne('SELECT * FROM '.DB_PREFIX.'orders WHERE oid ='.$oid);
			if ($order_data)
			{
				$order = $this->getOrderObject($order_data);
			}
		}

		//reset status vars
		$order_num = $order_data["order_num"];
		$placed_date = $order_data['placed_date'];

		if ($orderStatus == ORDER::STATUS_COMPLETED && $order_data['status'] != ORDER::STATUS_COMPLETED)
		{
			if ($sendNotificationEmail)
			{
				$this->sendOrderNotificationEmail($db, $order, $order_data);
			}
		}

		$this->updateOrderStatusDatabase($oid, $orderStatus, $paymentStatus, $order, $placed_date);

		$orderStatusChanged = $orderStatus != '' && $order_data['status'] != $orderStatus;
		$paymentStatusChanged = $paymentStatus != '' && $order_data['payment_status'] != $paymentStatus;

		if ($orderStatusChanged || $paymentStatusChanged)
		{
			/**
			 * Handle on status change event
			 */
			$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);

			$event
				->setOrder($order)
				->setData('oldOrderStatus', $order_data['status'])
				->setData('oldPaymentStatus', $order_data['payment_status'])
				->setData('newOrderStatus', $orderStatus)
				->setData('newPaymentStatus', $paymentStatus);

			Events_EventHandler::handle($event);

			Admin_Log::log(
				'Order #'.$order_num.' - '.
				($orderStatusChanged ? ' status changed from '.$order_data['status'].' to '.$orderStatus : '').
				($paymentStatusChanged ? ($orderStatusChanged ? ', ' : '').' payment status changed from '.$order_data['payment_status'].' to '.$paymentStatus : ''),
				Admin_Log::LEVEL_IMPORTANT
			);
		}
	}

	/**
	 * @param $oid
	 * @param $orderStatus
	 * @param $paymentStatus
	 * @param $order
	 * @param null $placedDate
	 * @param null $shippingTrackingNumber
	 * @param null $shippingTrackingNumberType
	 */
	protected function updateOrderStatusDatabase($oid, $orderStatus, $paymentStatus, $order, $placedDate = null, $shippingTrackingNumber = null, $shippingTrackingNumberType = null)
	{
		$db = $this->getDb();

		/**
		 * Update orders status
		 */
		$shouldUpdate = false;
		$db->reset();
		if ($orderStatus != '' || $paymentStatus != '')
		{
			$shouldUpdate = true;
			if ($orderStatus != '')
			{
				$order->status = $orderStatus;

				$db->assignStr('status', $orderStatus);

				if ($paymentStatus == ORDER::STATUS_COMPLETED && !is_null($placedDate) && $placedDate == '0000-00-00 00:00:00')
				{
					$db->assign('placed_date', 'NOW()');
				}
			}

			if ($paymentStatus != '')
			{
				$order->payment_status = $paymentStatus;
				$db->assignStr('payment_status', $paymentStatus);
			}
		}

		if (!is_null($shippingTrackingNumber) && !is_null($shippingTrackingNumberType))
		{
			$shouldUpdate = true;
			$db->assignStr('shipping_tracking_number', $shippingTrackingNumber);
			$db->assignStr('shipping_tracking_number_type', $shippingTrackingNumberType);
		}

		if ($shouldUpdate)
		{
			$db->assign('status_date', 'NOW()');
			$db->update(DB_PREFIX.'orders', 'WHERE oid='.$oid);

			Admin_Log::log('Order #'.$order->order_num.' - status changed', Admin_Log::LEVEL_IMPORTANT);
		}
	}

	/**
	 * @param $db
	 * @param ORDER $order
	 * @param $order_data
	 */
	protected function sendOrderNotificationEmail($db, ORDER $order, $order_data)
	{
		view()->initNotificationsSmarty();
		// *** GIFT CERTIFICATE
		view()->assign('payment_method_name', 'Gift Certificate');
		view()->assign('gift_cert_amount', $order_data['gift_cert_amount']);
		// *** GIFT CERTIFICATE
		Notifications::emailOrderCompleted($db, $order, $order_data['oid']);
	}

	/**
	 * @return DataAccess_OrderRepository
	 */
	protected function getRepository()
	{
		if ($this->repository == null)
		{
			// TODO: change to use setting repository
			$settings = $this->getSettings()->getAll();
			$this->repository = new DataAccess_OrderRepository($this->getDb(), $settings);
		}

		return $this->repository;
	}

	/**
	 * @return DataAccess_UserRepository|null
	 */
	protected function getUserRepository()
	{
		if ($this->userRepository == null)
		{
			// TODO: change to use setting repository
			$settings = $this->getSettings()->getAll();
			$this->userRepository = new DataAccess_UserRepository($this->getDb(), $settings);
		}

		return $this->userRepository;
	}

	/**
	 * @return DataAccess_OrderShipmentRepository
	 */
	protected function getShipmentRepository()
	{
		if ($this->shipmentRepository == null)
		{
			$this->shipmentRepository = new DataAccess_OrderShipmentRepository($this->getDb(), $this->getSettings());
		}

		return $this->shipmentRepository;
	}

	/**
	 * @return FraudService_DataAccess_FraudMethodRepository
	 */
	public function getFraudServiceRepository()
	{
		if ($this->fraudServiceRepository == null)
		{
			$this->fraudServiceRepository = new FraudService_DataAccess_FraudMethodRepository($this->getDb(), $this->getSettings());
		}

		return $this->fraudServiceRepository;
	}



	/**
	 * @return Admin_Service_OrderExport
	 */
	protected function getExportService()
	{
		return new Admin_Service_OrderExport($this->getDb(), $this->getRepository());
	}

	/**
	 * Check is endicia enabled
	 *
	 * @param $shipments
	 *
	 * @return bool
	 */
	protected function getEndiciaEnabled($shipments)
	{
		if (AccessManager::checkAccess('ENDICIA') && $this->getSettings()->get('EndiciaShippingLabelsActive') == 'Yes')
		{
			$applicableCarrierFound = false;
			foreach ($shipments as $shipment)
			{
				if (in_array(strtolower($shipment['shipping_carrier_id']), array('custom', 'usps', 'usps_domestic', 'usps_international')))
				{
					$applicableCarrierFound = true;
				}
			}

			return $applicableCarrierFound;
		}

		return false;
	}

	protected function getVparcelEnabled($shipments)
	{
		if (trim($this->getSettings()->get('ShippingVIPparcelauthToken')) !== '')
		{
			$applicableCarrierFound = false;
			foreach ($shipments as $shipment)
			{
				if (in_array(strtolower($shipment['shipping_carrier_id']), array('vparcel')))
				{
					$applicableCarrierFound = true;
				}
			}

			return $applicableCarrierFound;
		}

		return false;
	}

	/**
	 * Check are stamps enables
	 *
	 * @param $shipments
	 *
	 * @return bool
	 */
	protected function getStampsEnabled($shipments)
	{
		if ($this->getSettings()->get('StampscomActive') == 'Yes')
		{
			$applicableCarrierFound = false;
			foreach ($shipments as $shipment)
			{
				if (in_array(strtolower($shipment['shipping_carrier_id']), array('custom', 'usps', 'usps_domestic', 'usps_international')))
				{
					$applicableCarrierFound = true;
				}
			}

			return $applicableCarrierFound;
		}

		return false;
	}
	
	protected function getAdminUsername($id)
	{
		$db = $this->getDb();
		$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'admins WHERE aid = '.intval($id));
		return $result['username'];
	}
}
<?php

/**
 * Class Admin_Controller_Shipping
 */
class Admin_Controller_Shipping extends Admin_Controller
{
	const MODE_ADD = 'add';
	const MODE_UPDATE = 'update';

	/** @var $shippingMethodRepository DataAccess_ShippingMethodRepository  */
	protected $shippingMethodRepository = null;

	/** @var array $menuItem */
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'setup');

	protected $upsSettingsKeys = array(
		'ShippingUPSAccessKey', 'ShippingUPSUserID', 'ShippingUPSUserPassword',
		'ShippingUPSRateChart', 'ShippingUPSContainer', 'ShippingUPSUrl', 'ShippingUPSTrackingUrl'
	);

	protected $uspsSettingsKeys = array(
		'ShippingUSPSUserID', 'ShippingUSPSUrl', 'ShippingUSPSTrackingUrl',
		'ShippingUSPSLabelingUrl', 'ShippingUSPSPackageSize', 'ShippingUSPSPackageWidth',
		'ShippingUSPSPackageLength', 'ShippingUSPSPackageHeight'
	);

	protected $fedexSettingsKeys = array(
		'ShippingFedExAccountNumber', 'ShippingFedExBillingAccountNumber', 'ShippingFedExFreightAccountNumber',
		'ShippingFedExApiKey', 'ShippingFedExApiPassword', 'ShippingFedExMeter',
		'ShippingFedExRateRule', 'ShippingFedExRateRequestType', 'ShippingFedExFreightClass',
		'ShippingFedExFreightPackaging', 'ShippingFedExFreightPackagingWeight','ShippingFedExDropOffType'
	);

	protected $canadaPostSettingsKeys = array(
		'ShippingCPMerchantID', 'ShippingCPUrlRating', 'ShippingCPDeliveryDate',
		'ShippingCPItemsValue', 'ShippingCPTAT', 'ShippingUSPSPackageWidth',
		'ShippingUSPSPackageLength', 'ShippingUSPSPackageHeight'
	);

	protected $VIPparcelSettingsKeys = array(
		'ShippingVIPparcelauthToken', 'ShippingVIPparcelUrl', 'ShippingVIPparcelTestmode', 'ShippingVIPparcelService'
	);

	/**
	 * List action
	 */
	public function listAction()
	{
		$settings = $this->getSettings();

		$adminView = $this->getView();
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8004');

		$repository = $this->getRepository();

		$activeMethods = $repository->getShippingMethods(true);

		$countryRepository = $this->getCountryRepository();

		$countriesCarriersMethods = array();

		foreach ($activeMethods as &$activeMethod)
		{
			if ($activeMethod['method_id'] == 'product_level') continue; // Skip product level methods

			$this->parseMethod($activeMethod, $countryRepository);

			$key = $activeMethod['carrier_key'];
			if (!isset($countriesCarriersMethods[$key]))
			{
				$countryName = 'International';

				if ($key != 'all')
				{
					$country = $countryRepository->getCountryById(intval($key), true);
					$countryName = $country ? $country['name'] : 'Unknown';
				}

				$countriesCarriersMethods[$key] = array(
					'flat' => array(),
					'real_time' => array(),
					'name' => $countryName,
				);
			}

			if ($activeMethod['carrier_id'] == 'custom')
			{
				$countriesCarriersMethods[$key]['flat'][] = $activeMethod;
			}
			else
			{
				$carrierKey = $activeMethod['carrier_id'];

				if (substr($carrierKey, 0, 4) == 'usps')
				{
					$carrierKey = 'usps';
				}
				else if (substr($carrierKey, 0, 5) == 'fedex')
				{
					$carrierKey = 'fedex';
				}
				else if (substr($carrierKey, 0, 7) == 'vparcel')
				{
					$carrierKey = 'vparcel';
				}

				$countriesCarriersMethods[$key][$carrierKey][] = $activeMethod;
			}
		}

		$adminView->assign('upsConfigured', trim($settings->get('ShippingUPSAccessKey')) != '' && trim($settings->get('ShippingUPSUserID')) != '');
		$adminView->assign('uspsConfigured', trim($settings->get('ShippingUSPSUserID')) != '' && trim($settings->get('ShippingUSPSUrl')) != '');
		$adminView->assign('canadaPostConfigured', trim($settings->get('ShippingCPMerchantID')) != '' && trim($settings->get('ShippingCPMerchantID')) != 'CPC_DEMO_XML');
		$adminView->assign('fedexConfigured', trim($settings->get('ShippingFedExAccountNumber')) != '' && trim($settings->get('ShippingFedExApiKey')) != '' && trim($settings->get('ShippingFedExApiPassword')) != '');
		$adminView->assign('vparcelConfigured', trim($settings->get('ShippingVIPparcelauthToken')) != '');

		$form = new Admin_Form_ShippingAdvancedSettingsForm();
		$adminView->assign('upsSettingsForm', $form->getUpsSettingsForm($settings->getByKeys($this->upsSettingsKeys)));
		$adminView->assign('uspsSettingsForm', $form->getUspsSettingsForm($settings->getByKeys($this->uspsSettingsKeys)));
		$adminView->assign('fedexSettingsForm', $form->getFedExSettingsForm($settings->getByKeys($this->fedexSettingsKeys)));
		$adminView->assign('canadaPostSettingsForm', $form->getCanadaPostSettingsForm($settings->getByKeys($this->canadaPostSettingsKeys)));
		$adminView->assign('vparcelSettingsForm', $form->getVIPparcelSettingsForm($settings->getByKeys($this->VIPparcelSettingsKeys)));

		$adminView->assign('settingsNonce', Nonce::create('shipping_settings'));
		$adminView->assign('currency', $this->getCurrency());

		$adminView->assign('countriesMethods', $countriesCarriersMethods);

		/**
		 * Get shipping address for qsg
		 * TODO: move into separated function
		 */
		$originAddressData = $settings->getByKeys(array(
			'ShippingOriginName', 'ShippingOriginAddressLine1', 'ShippingOriginAddressLine2',
			'ShippingOriginCity', 'ShippingOriginCountry', 'ShippingOriginState', 'ShippingOriginProvince',
			'ShippingOriginZip', 'ShippingOriginPhone'
		));

		$countryId = intval($originAddressData['ShippingOriginCountry']);
		$stateId = intval($originAddressData['ShippingOriginState']);

		$originAddressData['ShippingOriginCountryName'] = 'n/a';
		$originAddressData['ShippingOriginStateName'] = 'n/a';

		$countryRepository = new DataAccess_CountryRepository($this->getDb());
		$countriesStates = $countryRepository->getCountriesStatesForSettings();

		foreach ($countriesStates as $country)
		{
			if ($country['id'] == $countryId)
			{
				$originAddressData['ShippingOriginCountryName'] = $country['name'];
				if ($country['states'] && isset($country['states'][$stateId]))
				{
					$originAddressData['ShippingOriginStateName'] = $country['states'][$stateId]['name'];
				}
				else
				{
					$originAddressData['ShippingOriginStateName'] = $originAddressData['ShippingOriginProvince'];
				}

				break;
			}
		}

		$originCountry = $countryRepository->getCountryById($countryId);
		$adminView->assign('originCountry', $originCountry);

		$adminView->assign('shippingAddressData', $originAddressData);
		$adminView->assign('delete_nonce', Nonce::create('shipping_method_delete'));
		$adminView->assign('add_nonce', Nonce::create('shipping_method_add'));

		$adminView->assign('body', 'templates/pages/shipping/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete shipping method action
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce', 'shipping_method_delete')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('shipping');
			return;
		}

		$repository = $this->getRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = intval($request->get('id', 0));
			if ($id < 1)
			{
				Admin_Flash::setMessage('shipping.method_not_found', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if ($repository->deleteMethod($id))
				{
					Admin_Flash::setMessage('common.success');
				}

				$this->redirect('shipping');
			}
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);

			$this->redirect('shipping');
		}
	}

	/**
	 * View method action
	 */
	public function viewMethodAction()
	{
		$this->renderPartial('view');
	}

	/**
	 * Edit method action
	 */
	public function editMethodAction()
	{
		$this->renderPartial('edit');
	}

	/**
	 * Create a new method action
	 */
	public function newMethodAction()
	{
		$request = $this->getRequest();

		$formData = $request->request->getArray('method');
		$customRate = $this->getCustomRateDefaults();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'shipping_method_add', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$customRate = array_merge($customRate, $formData);
				$customRate['rates'] = array();

				$this->setMethodName($customRate);

				$rates = $request->request->getArray('rates');
				$customRate['rates'] = $rates;

				$repository = $this->getRepository();

				//TODO: Validation
				$isValid = true;
				if ($isValid)
				{
					$repository->persistShippingMethod($customRate);

					$this->ensureInternationalShippingEnabled($customRate);

					$this->renderJson(array('status' => 1));
				}
				else
				{
					//TODO:
					$this->renderJson(array('status' => 0));
				}
			}
		}
		else
		{
			$customRate['ssid'] = -1;

			$this->renderPartial('edit', $customRate);
		}
	}

	/**
	 * @param $method
	 */
	protected function ensureInternationalShippingEnabled($method)
	{
		$settings = $this->getSettings();

		if ($method['country'] != $settings->get('ShippingOriginCountry'))
		{
			$settings->persist(array('ShippingInternational' => 'YES'));
		}
	}

	/**
	 * @param $rate
	 */
	protected function setMethodName(&$rate)
	{
		switch ($rate['method_id'])
		{
			case 'flat' :
			{
				$rate['method_name'] = trans('shipping.per_item');
				break;
			}
			case 'price' :
			{
				$rate['method_name'] = trans('shipping.order_subtotal');
				break;
			}
			case 'base_weight' :
			{
				$rate['method_name'] = trans('shipping.weight');
				break;
			}
			case 'product' :
			{
				$rate['method_name'] = trans('shipping.product_level');
				break;
			}
		}
	}

	/**
	 *
	 */
	public function newRealTimeMethodsAction()
	{
		$request = $this->getRequest();

		$repository = $this->getRepository();

		$methodData = $request->request->getArray('method');
		$methods = $request->request->getArray('methods');

		foreach ($methods as $realTimeMethod)
		{
			$method = $repository->getRealTimeMethod($realTimeMethod);
			unset($method['sdid']);
			$method['ssid'] = 0;
			$method['priority'] = 5;
			$method['fee'] = 0;
			$method['fee_type'] = 'amount';
			$method['exclude'] = 'No';
			$method['hidden'] = 'No';
			$method['notes'] = null;
			$method['country'] = $methodData['country'];

			$repository->persistRealTimeMethod($method);
		}

		$this->renderJson(array('status' => 1));
	}

	/**
	 *
	 */
	public function updateShippingMethodAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$id = $request->get('id');

			$repository = $this->getRepository();

			$rate = $repository->getShippingMethod($id);

			if (!$rate)
			{
				$this->renderJson(array('status' => 0));
			}
			else
			{
				//TODO: Validate
				$formData = $request->request->getArray('method');
				$rate = array_merge($rate, $formData);

				$this->setMethodName($rate);

				if (!isset($formData['hidden']))
				{
					$rate['hidden'] = 'Yes';
				}

				$rates = $request->request->getArray('rates');

				$indexesToRemove = array();
				foreach ($rates as $idx => $customRate)
				{
					if (!isset($customRate['base'])) $customRate['base'] = 0;
					if (!isset($customRate['price'])) $customRate['price'] = 0;

					if (trim($customRate['rate_min']) === '' && trim($customRate['price']) === '' && trim($customRate['base']) === '')
					{
						$indexesToRemove[] = $idx;
					}
				}
				foreach ($indexesToRemove as $idx)
				{
					unset($rates[$idx]);
				}

				$rate['rates'] = $rates;

				$isValid = true;
				if ($isValid)
				{
					$repository->persistShippingMethod($rate);

					$rate = $this->getShippingMethod($id);

					$template = 'pages/shipping/elements/element-flat-rate-';
					if ($rate['carrier_id'] != 'custom')
					{
						$template = 'pages/shipping/elements/element-real-time-';
					}

					$this->renderJson(array(
						'status' => 1,
						'message' => trans('common.success'),
						'html' => $this->fetchPartial($template.'view', $rate),
					));
				}
				else
				{
					//TODO:
				}
			}
		}
	}

	/**
	 * Get real time form
	 */
	public function realTimeFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();
		$originCountry = $settings->get('ShippingOriginCountry');

		$coid = intval($request->get('coid'));
		$carrierId = $request->get('carrier_id');

		$adminView = $this->getView();

		$repository = $this->getRepository();

		if (trim($carrierId) == '')
		{
			$fedexMethods = $repository->getRealTimeMethods('fedex', $coid, $originCountry);
			$upsMethods = $repository->getRealTimeMethods('ups', $coid, $originCountry);
			$uspsMethods = $repository->getRealTimeMethods('usps', $coid, $originCountry);
			$canadaPostMethods = $repository->getRealTimeMethods('canada_post', $coid, $originCountry);
			$vipParcelsPostMethods = $repository->getRealTimeMethods('vparcel', $coid, $originCountry);
		}
		else
		{
			$fedexMethods = $carrierId == 'fedex' ? $repository->getRealTimeMethods('fedex', $coid, $originCountry) : array();
			$upsMethods = $carrierId == 'ups' ? $repository->getRealTimeMethods('ups', $coid, $originCountry) : array();
			$uspsMethods = $carrierId == 'usps' ? $repository->getRealTimeMethods('usps', $coid, $originCountry) : array();
			$canadaPostMethods = $carrierId == 'canada_post' ? $repository->getRealTimeMethods('canada_post', $coid, $originCountry) : array();
			$vipParcelsPostMethods = $carrierId == 'vparcel' ? $repository->getRealTimeMethods('vparcel', $coid, $originCountry) : array();
		}

		$adminView->assign('showAll', trim($carrierId) == '');
		$adminView->assign('fedexMethods', $fedexMethods);
		$adminView->assign('upsMethods', $upsMethods);
		$adminView->assign('uspsMethods', $uspsMethods);
		$adminView->assign('canadaPostMethods', $canadaPostMethods);
		$adminView->assign('vparcelMethods', $vipParcelsPostMethods);

        $methods = array_merge($fedexMethods, $upsMethods, $uspsMethods, $canadaPostMethods, $vipParcelsPostMethods);
        $hasRealTime = count($methods) < 1 ? 0 : 1;
        $adminView->assign('hasRealTime', $hasRealTime);

		$this->renderJson(array(
			'status' => 1,
			'message' => trans('common.success'),
			'html' => $adminView->fetch('pages/shipping/form-real-time-methods'),
		));
	}

	/**
	 *
	 */
	public function settingsAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$forceAddressValidation = $this->getForceAddressValidation();

		$settingsDefaults = array(
			'settings' => array(
				'ShippingCalcEnabled' => 'YES',
				'ShippingHandlingFee' => '0.00', 'ShippingHandlingFeeType' => 'percent', 'ShippingHandlingFeeWhenFreeShipping' => '0.00',
				'ShippingHandlingSeparated' => '0', 'ShippingHandlingTaxable' => '0', 'ShippingHandlingTaxClassId' => '0',
				'ShippingShowPriceAtProductLevel' => 'NO', // removed
				'ShippingWithoutMethod' => 'NO', 'ShippingShowWeight' => 'NO', 'ShippingAllowGetQuote' => 'NO',
				'ShippingShowAlternativeOnFree' => 'NO', 'ShippingAllowSeparateAddress' => '0',
				'EndiciaUspsAddressValidation' => $forceAddressValidation ? '1' : '0',
				'ShippingTaxable' => '0', 'ShippingTaxClassId' => '0'
			),
			'address' => array(
				'ShippingOriginName' => '', 'ShippingOriginAddressLine1' => '', 'ShippingOriginAddressLine2' => '',
				'ShippingOriginCity' => '', 'ShippingOriginCountry' => '1', 'ShippingOriginState' => '',
				'ShippingOriginProvince' => '', 'ShippingOriginZip' => '', 'ShippingOriginPhone' => ''
			),
		);

		// get accepted settings arrays
		$accepted = array();
		foreach ($settingsDefaults as $settingsGroupKey => $settingsGroup)
		{
			$accepted[$settingsGroupKey] = array_keys($settingsGroup);
		}

		if ($forceAddressValidation)
		{
			$settings->set('EndiciaUspsAddressValidation', '1');
			$settingsDefaults['settings']['EndiciaUspsAddressValidation'] = '1';
		}

		$data = array();

		foreach ($accepted as $settingsGroupKey => $settingsGroup)
		{
			$data[$settingsGroupKey] = $settings->getByKeys($settingsGroup);
		}

		$form = false;

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'shipping_settings'))
			{
				Admin_Flash::setMessage('Invalid nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('shipping');
				return;
			}

			$carrierId = $request->request->get('carrier_id');

			if (trim($carrierId) != '')
			{
				$settingsKeys = array();
				switch ($carrierId)
				{
					case 'ups' :
					{
						$settingsKeys = $this->upsSettingsKeys;
						break;
					}
					case 'usps' :
					{
						$settingsKeys = $this->uspsSettingsKeys;
						break;
					}
					case 'fedex' :
					{
						$settingsKeys = $this->fedexSettingsKeys;
						break;
					}
					case 'canada-post' :
					{
						$settingsKeys = $this->canadaPostSettingsKeys;
						break;
					}
					case 'vparcel' :
					{
						$settingsKeys = $this->VIPparcelSettingsKeys;
						break;
					}
				}

				//TODO: Validation

				$settingsData = $request->request->getByKeys($settingsKeys);
				$settings->persist($settingsData, $settingsKeys);

				$this->redirect('shipping');
				return;
			}
			else
			{
				$action = $request->request->get('action');

				$mode = 'settings';
				$errors = array();
				if ($action == 'save-address')
				{
					$mode = 'address';

					$settingsData = array_merge($settingsDefaults[$mode], $request->request->all());
					//TODO: Validation
				}
				else
				{
					$settingsData = array_merge($settingsDefaults[$mode], $request->request->all());

					if (isset($settingsData['ShippingHandlingFee']) && (floatval($settingsData['ShippingHandlingFee']) < 0 || !is_numeric($settingsData['ShippingHandlingFee'])))
					{
						$errors['ShippingHandlingFee'] = array(trans('shipping.errors.shipping_handling_fee'));
					}

					if (isset($settingsData['ShippingHandlingFeeWhenFreeShipping']) && (floatval($settingsData['ShippingHandlingFeeWhenFreeShipping']) < 0 || !is_numeric($settingsData['ShippingHandlingFeeWhenFreeShipping'])))
					{
						$errors['ShippingHandlingFeeWhenFreeShipping'] = array(trans('shipping.errors.shipping_handling_fee_when_free_shipping'));
					}
				}

				if (count($errors) == 0)
				{
					$settings->persist($settingsData);

					Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

					$this->redirect('shipping', array('mode' => 'settings'));
					return;
				}
				else
				{
					$formBuilder = new Admin_Form_ShippingAdvancedSettingsForm();
					$form = $formBuilder->getShippingSettingsForm($this->getDb(), $settingsData, $forceAddressValidation);

					$form->bindErrors($errors);
				}
			}
		}

		$countryRepository = new DataAccess_CountryRepository($this->getDb());

		$countriesStates = $countryRepository->getCountriesStatesForSettings();

		if (!isset($formBuilder) || !$formBuilder)
		{
			$formBuilder = new Admin_Form_ShippingAdvancedSettingsForm();
			$form = $formBuilder->getShippingSettingsForm($this->getDb(), $data['settings'], $forceAddressValidation);
		}

		$adminView = $this->getView();

		$adminView->assign('shippingSettingsForm', $form);
		$adminView->assign('countriesStates', json_encode($countriesStates));
		$adminView->assign('shippingAddressForm', $formBuilder->getOriginAddressForm($data['address']));

		$countryId = intval($data['address']['ShippingOriginCountry']);
		$stateId = intval($data['address']['ShippingOriginState']);

		$data['address']['ShippingOriginCountryName'] = 'n/a';
		$data['address']['ShippingOriginStateName'] = 'n/a';

		foreach ($countriesStates as $country)
		{
			if ($country['id'] == $countryId)
			{
				$data['address']['ShippingOriginCountryName'] = $country['name'];
				if ($country['states'] && isset($country['states'][$stateId]))
				{
					$data['address']['ShippingOriginStateName'] = $country['states'][$stateId]['name'];
				}
				else
				{
					$data['address']['ShippingOriginStateName'] = $data['address']['ShippingOriginProvince'];
				}

				break;
			}
		}

		$adminView->assign('shippingAddressData', $data['address']);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8005');

		$adminView->assign('settingsNonce', Nonce::create('shipping_settings'));

		$adminView->assign('body', 'templates/pages/shipping/settings.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Change state action
	 */
	public function changeStatesAction()
	{
		$request = $this->getRequest();

		$ssid = $request->get('ssid');

		$repository = $this->getRepository();

		$method = $this->getShippingMethod($ssid);
		$countriesRepository = $this->getCountryRepository();
		$statesRepository = $this->getStatesRepository();

		if ($request->isPost())
		{
			if (!$method)
			{
				$this->redirect('shipping');
				return;
			}

			$states = $request->request->getArray('states');

			if (count($states) == 1)
			{
				$state = $states[0];
				if ($state == 'continental')
				{
					$states = $statesRepository->getStatesByCountryId(null, trim($method['country'], ','));
					$stateStr = '';

					foreach ($states as $state)
					{
						if ($state)
						{
							if ($state['short_name'] != 'AK' && $state['short_name'] != 'HI' && $state['short_name'] != 'PR')
							{
								$stateStr .= $state['stid'].',';
							}
						}
					}

					if (trim($stateStr) == '')
					{
						$stateStr = '0';
					}
				}
				else
				{
					$stateStr = $state;
				}
			}
			else
			{
				$stateStr = '';
				foreach ($states as $state)
				{
					if ($state == 'continental')
					{
						$states = $statesRepository->getStatesByCountryId(null, trim($method['country'], ','));
						$stateStr = '';
						foreach ($states as $state2)
						{
							if ($state2)
							{
								if ($state2['short_name'] != 'AK' && $state2['short_name'] != 'HI' && $state2['short_name'] != 'PR')
								{
									$stateStr .= $state2['stid'].',';
								}
							}
						}

						if (trim($stateStr) == '')
						{
							$stateStr = '0';
						}
					}
					else
					{
						$stateStr .= intval($state).',';
					}
				}
			}

			$method['state'] = $stateStr;

			$repository->persistShippingMethod($method, false);

			Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

			$this->redirect('shipping');
			return;
		}

		$adminView = $this->getView();

		$country = $countriesRepository->getCountriesStates(trim($method['country'], ','));

		$adminView->assign('states', $country && isset($country['states']) ? $country['states'] : array());
		$adminView->assign('selectedStates', explode(',', $method['state']));

		$adminView->assign('methodItem', $method);

		if ($request->get('__ajax', false))
		{
			$this->renderJson(array('status' => 1, 'html' => $adminView->fetch('pages/shipping/form-change-states')));
		}
		else
		{
			$adminView->render('pages/shipping/form-change-states');
		}
	}

	/**
	 * Change countries action
	 */
	public function changeCountriesAction()
	{
		$request = $this->getRequest();

		$ssid = $request->get('ssid');

		$repository = $this->getRepository();
		$method = $repository->getShippingMethod($ssid);
		$countriesRepository = $this->getCountryRepository();

		$settings = $this->getSettings();

		$shippingOriginCountry = $settings->get('ShippingOriginCountry');

		if ($request->isPost())
		{
			if (!$method)
			{
				$this->redirect('shipping');
				return;
			}

			$countries = $request->request->getArray('countries');

			if (count($countries) == 1)
			{
				$countryStr = $countries[0];
			}
			else
			{
				$countryStr = '';
				foreach ($countries as $country)
				{
					$countryStr .= intval($country).',';
				}
			}

			$method['country'] = $countryStr;

			$repository->persistShippingMethod($method, false);

			Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

			$this->redirect('shipping');
			return;
		}

		$adminView = $this->getView();

		$countries = $countriesRepository->getCountriesStates();

		$temp = array();
		foreach ($countries as $country)
		{
			if ($country['id'] != $shippingOriginCountry && $country['id'] != 1 && $country['id'] != 2)
			{
				$temp[] = $country;
			}
		}
		$adminView->assign('countries', $temp);
		$adminView->assign('selectedCountries', explode(',', $method['country']));
		$adminView->assign('methodItem', $method);

		if ($request->get('__ajax', false))
		{
			$this->renderJson(array('status' => 1, 'html' => $adminView->fetch('pages/shipping/form-change-countries')));
		}
		else
		{
			$adminView->render('pages/shipping/form-change-countries');
		}
	}

	/**
	 * @return bool
	 */
	protected function getForceAddressValidation()
	{
		$settings = $this->getSettings();

		return $settings->get('ExactorEnableTax') == 'Yes' || $settings->get('AvalaraEnableTax') == 'Yes';
	}

	/**
	 * @return array
	 */
	protected function getCustomRateDefaults()
	{
		return array(
			'carrier_id' => 'custom',
			'carrier_name' => '',
			'method_id' => 'flat',
			'method_calc_mode' => 'before_discount',
			'method_name' => 'Per item',
			'priority' => 5,
			'country' => 1,
			'state' => 0,
			'weight_min' => 0,
			'weight_max' => 100000,
			'fee' => 0,
			'fee_type' => 'amount',
			'exclude' => 'No',
			'hidden' => 'No',
			'rates' => array(),
		);
	}

	/**
	 * @param $view
	 * @param null $rate
	 */
	protected function renderPartial($view, $rate = null)
	{
		$request = $this->getRequest();

		if (!$rate)
		{
			$id = $request->get('id');

			$rate = $this->getShippingMethod($id);

			if ($request->query->has('change-type'))
			{
				$type = $request->query->get('change-type');
				$rate['method_id'] = $type;
				$rate['method_name'] = '';

				$this->setMethodName($rate);
				$rate['rates'] = array();
				$rate['ratesCount'] = 0;
			}
		}

		if (!$rate)
		{
			$this->renderJson(array('status' => 0));
		}
		else
		{
			$template = 'pages/shipping/elements/element-flat-rate-';
			if ($rate['carrier_id'] != 'custom')
			{
				$template = 'pages/shipping/elements/element-real-time-';
			}

			$this->renderJson(array('status' => 1, 'html' => $this->fetchPartial($template.$view, $rate)));
		}
	}

	/**
	 * @param $id
	 * @return array|bool|null
	 */
	protected function &getShippingMethod($id)
	{
		$repository = $this->getRepository();

		$method = $repository->getShippingMethod($id);
		if ($method)
		{
			$this->parseMethod($method, $this->getCountryRepository());
		}

		return $method;
	}

	/**
	 * @param $method
	 * @param DataAccess_CountryRepository $countryRepository
	 */
	protected function parseMethod(&$method, DataAccess_CountryRepository $countryRepository)
	{
		$countries = explode(',', trim($method['country'], ','));
		if (count($countries) == 1)
		{
			// Only targeted to 1 country
			$key = $countries[0];

			if ($key == '0') $key = 'all';
		}
		else
		{
			$key = 'all';
		}

		$method['carrier_key'] = $key;
		$method['uses_states'] = $key != 'all';
		$this->parseStatesNames($method, $countryRepository);
	}

	/**
	 * @param $method
	 * @param DataAccess_CountryRepository $countryRepository
	 */
	protected function parseStatesNames(&$method, DataAccess_CountryRepository $countryRepository)
	{
		$statesNames = '';

		if ($method['uses_states'])
		{
			$countryId = intval($method['country']);
			$continentalUSStatesCount = 0;
			$hasNonContinentalUS = false;

			$states = explode(',', trim($method['state'], ','));
			foreach ($states as $stateId)
			{
				if ($stateId > 0)
				{
					$state = $countryRepository->getStateById($stateId, true);
					if ($state)
					{
						if ($state['short_name'] == 'HI' || $state['short_name'] == 'AK' || $state['short_name'] == 'PR')
						{
							$hasNonContinentalUS = true;
						}
						else
						{
							$continentalUSStatesCount++;
						}

						$statesNames .= ', '.$state['short_name'];
					}
				}
			}

			if ($countryId == 1 && !$hasNonContinentalUS && ($continentalUSStatesCount == 48 || $continentalUSStatesCount == 49))
			{
				$statesNames = trans('shipping.continental_us');
				$method['state'] = 'continental';
			}
		}
		else
		{
			$countries = explode(',', trim($method['country'], ','));
			foreach ($countries as $countryId)
			{
				if ($countryId > 0)
				{
					$country = $countryRepository->getCountryById($countryId, true);
					if ($country)
					{
						$statesNames .= ', '.$country['iso_a2'];
					}
				}
			}
		}

		$method['states_names'] = trim($statesNames) != '' ? trim(ltrim($statesNames, ',')) : 'All';
	}

	/**
	 * @param $view
	 * @param $rate
	 * @return mixed|string|void
	 */
	protected function fetchPartial($view, $rate)
	{
		$adminView = $this->getView();
		$adminView->assign('methodItem', $rate);
		$adminView->assign('rowSpan', $rate['ratesCount']);

		if ($view == 'pages/shipping/elements/element-flat-rate-edit')
		{
			$adminView->assign('rowSpan', $rate['ratesCount'] + 1);
		}
		$adminView->assign('currency', $this->getCurrency());
		$adminView->assign('priceType', 'amount');

		if (isset($rate['rates'][0]))
		{
			$adminView->assign('priceType', $rate['rates'][0]['price_type']);
		}

		return $adminView->fetch($view);
	}

	/**
	 * @return DataAccess_ShippingRepository
	 */
	protected function getRepository()
	{
		return new DataAccess_ShippingRepository($this->getDb());
	}

	/**
	 * @return DataAccess_CountryRepository
	 */
	protected function getCountryRepository()
	{
		return new DataAccess_CountryRepository($this->getDb());
	}

	/**
	 * @return DataAccess_StateRepository
	 */
	protected function getStatesRepository()
	{
		return new DataAccess_StateRepository($this->getDb());
	}

	/**
	 * @return bool
	 */
	protected function getCurrency()
	{
		global $admin_currency;

		return $admin_currency;
	}
}
<?php

/**
 * Class Admin_Controller_Endicia_GeneratePostAgeLabel
 */
class Admin_Controller_EndiciaGeneratePostageLabel extends Admin_Controller
{
    protected $repository = null;
    
    public function generateAction()
    {
        $request = $this->getRequest();
        $postData = $request->request->all();
        $settings = $this->getSettings();
        $nonce = isset($postData["nonce"]) ? $postData["nonce"] : "";
        $orderId = intval($postData['orderid']);
    
        if (isset($postData['mode']))
        {
    
            $orderId = $postData['orderid'];
            $shipFromName = str_replace('&','and', $postData['shipfromname']);
            $shipFromAddress1 = $postData['shipfromaddress1'];
            $shipFromAddress2 = $postData['shipfromaddress2'];
            $shipFromCity = $postData['shipfromcity'];
            $shipFromState = $postData['shipfromstate'];
            $shipFromZip = trim($postData['shipfromzip']);
            $shipFromZip4 = '';
            $shipFromPhone = $postData['shipfromphone'];
            $shipFromCountryName = $postData['shipfromcountryname'];
            $shipFromCountryCode = $postData['shipfromcountrycode'];
    
            $p = strpos($shipFromZip, '-');
            if ($shipFromCountryCode == 'US' && $p !== false)
            {
                $shipFromZip4 = substr($shipFromZip, $p + 1);
                $shipFromZip = substr($shipFromZip, 0, $p);
            } 
    
            $shipToName = str_replace('&','and', $postData['shiptoname']);
            $shipToCompany = str_replace('&','and', $postData['shiptocompany']);
            $shipToAddress1 = $postData['shiptoaddress1'];
            $shipToAddress2 = $postData['shiptoaddress2'];
            $shipToCity = $postData['shiptocity'];
            $shipToState = $postData['shiptostate'];
            $shipToZip = trim($postData['shiptozip']);
            $shipToZip4 = '';
            $shipToPhone = $postData['shiptophone'];
            $shipToCountryName = $postData['shiptocountryname'];
            $shipToCountryCode = $postData['shiptocountrycode'];
    
            $p = strpos($shipToZip, '-');
            if ($shipToCountryCode == 'US' && $p !== false)
            {
                $shipToZip4 = substr($shipToZip, $p + 1);
                $shipToZip = substr($shipToZip, 0, $p);
            }
    
            $daysAdvance = $postData['DaysAdvance'];
            $mailClass = $postData['MailClass'];
            $mailPieceShape = $postData['MailShape'];
            $sortType = $postData['SortType'];
            $entryFacility = $postData['EntryFacility'];
            $weightOz = floatval($postData['WeightOz']);
            $declaredValue = floatval($postData['DeclaredValue']);
            $packageDescription = $postData['PackageDescription'];
            $width = floatval($postData['Width']);
            $length = floatval($postData['Length']);
            $height = floatval($postData['Height']);
            
            $insurance = $postData['Insurance']; if ($insurance == 'None') {$insurance = 'OFF';}
            $deliveryConfirmation = isset($postData['DeliveryConfirmation']) ? 'ON' : 'OFF';
            $certifiedMail = isset($postData['CertifiedMail']) ? 'ON' : 'OFF';
            $restrictedDelivery = isset($postData['RestrictedDelivery']) ? 'ON' : 'OFF';
            $noWeekendDelivery = isset($postData['NoWeekendDelivery']) ? 'TRUE' : 'FALSE';
            $returnReceipt = isset($postData['ReturnReceipt']) ? 'ON' : 'OFF';
            $signatureConfirmation = isset($postData['AdultSignature']) ? 'ON' : 'OFF';
            $stealth = isset($postData['Stealth']) ? 'TRUE' : 'FALSE';
    
            $labelSize = '4x6';
            $labelType = 'Default';
            $labelSubtype = 'None';
    
            if ($mailPieceShape == 'Card' || $mailPieceShape == 'Letter')
            {
                $labelType='DestinationConfirm';
                $labelSize = '6x4';
            }
    
            if ($certifiedMail == 'ON')
            {
                $labelType = 'CertifiedMail';
                $labelSize = '4x6';
            }
            
            if ($shipToCountryCode != 'US')
            {
                $labelType = 'International';
                $labelSize = '4x6c';
            }
    
            if ($shipToCountryCode != 'US')
            {
                $labelSize="4x6c";
            }
    
            $endicia = new Shipping_Endicia($settings->get('EndiciaShippingLabelsAccountID'), $settings->get('EndiciaShippingLabelsPassword'), $settings->get('EndiciaShippingLabelsTestMode'));
    
            $data = new Shipping_EndiciaLabelData(
                $shipFromName, $shipFromAddress1, $shipFromAddress2, $shipFromCity, $shipFromState, $shipFromZip, $shipFromZip4, $shipFromPhone, $shipFromCountryName, $shipFromCountryCode,
                $shipToName, $shipToCompany, $shipToAddress1, $shipToAddress2, $shipToCity, $shipToState, $shipToZip, $shipToZip4, $shipToPhone, $shipToCountryName, $shipToCountryCode,
                $labelType, $labelSubtype, $labelSize, 
                $mailClass, $daysAdvance, $weightOz, $mailPieceShape, $width, $length, $height,
                $stealth, $insurance, 
                $signatureConfirmation, $declaredValue, $packageDescription, $restrictedDelivery, $noWeekendDelivery,
                $sortType, $entryFacility, $deliveryConfirmation
            );
    
            $response = $endicia->GetEndiciaShippingLabel($data);
    
            $isGetLabelStatusCode = $response['Status'];
    
            $status = 200;
            $ret = array();
    
            if ($isGetLabelStatusCode == '0')
            {
                $encodedLabelImage = isset($response['Label']['Image']) ?  $response['Label']['Image'] : $response['Base64LabelImage'];
                $trackingNumber = $response['TrackingNumber'];
                $finalPostage = $response['FinalPostage'];
    
                // save endicia shipping label
                $repository = $this->getDataRepository();
                $labelId = $repository->addShippingShippingLabel($orderId, $trackingNumber, $finalPostage, $response);
    
                $labelUrl = $endicia->SaveEndiciaShippingLabel($encodedLabelImage, $orderId, $labelId, $trackingNumber);
    
                // update shipping labels and orders
                $repository->updateShippingLabelsAndOrders($labelUrl, $labelId, $trackingNumber, $orderId);

                $ret = array(
                    'result' => 1,
                    'error' => '',
                    'trackingNumber' => $trackingNumber,
                    'labelUrl' => $labelUrl
                );
            }
            else
            {
                if (isset($response['ErrorMessage']))
                {
                    $ret = array(
                        'result' => 0,
                        'error' => $response['ErrorMessage'],
                        'trackingNumber' => '',
                        'labelUrl' => ''
                    );
                }
            }
    
            $this->renderJson($ret);
        }
    }
    
    protected function getDataRepository()
	{
		if (is_null($this->repository))
		{
			$this->repository = new DataAccess_EndiciaRepository($this->getDb());
		}

		return $this->repository;
	}
}

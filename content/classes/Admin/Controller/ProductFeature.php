<?php

/**
 * Class Admin_Controller_ProductFeature
 */
class Admin_Controller_ProductFeature extends Admin_Controller
{
	/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
	protected $productsFeaturesRepository = null;

	/** @var DataAccess_ProductsFeaturesGroupsRepository $productsFeaturesGroupsRepository */
	protected $productsFeaturesGroupsRepository = null;

	/** @var DataAccess_ProductsFeaturesValuesRepository $productsFeaturesValuesRepository */
	protected $productsFeaturesValuesRepository = null;

	/** @var DataAccess_ProductsFeaturesValuesRelationsRepository $productsFeaturesValuesRelationsRepository */
	protected $productsFeaturesValuesRelationsRepository = null;

	/** @var DataAccess_ProductsFeaturesGroupsProductsRelationsRepository $productsFeaturesGroupsProductsRelationsRepository */
	protected $productsFeaturesGroupsProductsRelationsRepository = null;

	/**
	 * Assign features groups with a product
	 */
	public function assignProductsFeaturesGroupsAction()
	{
		$request = $this->getRequest();

		$result = array('status' => 1);

		if ($request->isPost())
		{
			$pid = $request->get('pid', 0);
			if ($pid)
			{
				$productsFeaturesGroups = explode(',', $request->get('products_features_groups', ''));

				/** @var DataAccess_ProductsFeaturesGroupsProductsRelationsRepository $productsFeaturesGroupsProductsRelationsRepository */
				$productsFeaturesGroupsProductsRelationsRepository = $this->getProductsFeaturesGroupsProductsRelationsRepository();
				$productsFeaturesGroupsProductsRelationsRepository->updateProductsFeaturesGroups($pid, $productsFeaturesGroups);

				$result = array(
					'status' => 1,
					'productProductsFeaturesGroupsAssigned' => $this->getProductsFeaturesGroups($pid)
				);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Unassign product features group from a product
	 */
	public function deleteProductsFeaturesGroupsAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$pid = $request->get('pid', 0);

			if ($pid)
			{
				$productFeatureGroupId = $request->get('product_feature_group_id', 0);

				/** @var DataAccess_ProductsFeaturesGroupsProductsRelationsRepository $productsFeaturesGroupsProductsRelationsRepository */
				$productsFeaturesGroupsProductsRelationsRepository = $this->getProductsFeaturesGroupsProductsRelationsRepository();
				$productsFeaturesGroupsProductsRelationsRepository->delete($pid, $productFeatureGroupId);

				$result = array(
					'status' => 1,
					'productProductsFeaturesGroupsAssigned' => $this->getProductsFeaturesGroups($pid)
				);
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => array('error' => array('Cannot delete products features group. Unsupported method.')));
		}

		$this->renderJson($result);
	}

	/**
	 *
	 */
	public function productFeatureUpdateAction()
	{
		$settings = $this->getSettings();

		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$productForm = new Admin_Form_ProductForm($settings, $this->getDb());
		$data = $this->getProductsFeaturesRepository()->getProductsFeaturesValuesByProductsFeaturesGroupId($request->get('id'), $request->get('pid'), $request->get('fgprid'));

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('productsFeatures', $productForm->getProductsFeaturesForm($data));
		$adminView->assign('nonce', Nonce::create('product_feature'));
		$html = $adminView->fetch('pages/product/modal-product-feature');

		$this->renderJson(array(
			'status' => 1,
			'html' => $html,
		));
	}

	/**
	 * @param $productId
	 * @return mixed
	 */
	protected function getProductsFeaturesGroups($productId)
	{
		$families = $this->getProductsFeaturesGroupsRepository()->getProductsFeaturesGroupsByProductId($productId);

		foreach ($families as $key => $family)
		{
			$families[$key]['features'] = $this->getProductsFeaturesRepository()->getProductsFeaturesValuesByProductsFeaturesGroupId($family['product_feature_group_id'], $productId);
			$families[$key]['options'] = $this->getProductsFeaturesRepository()->getProductsFeaturesValuesOptionsByProductsFeaturesGroupId($family['product_feature_group_id']);
		}

		return $families;
	}

	/**
	 * @return DataAccess_ProductsFeaturesRepository
	 */
	protected function getProductsFeaturesRepository()
	{
		if (is_null($this->productsFeaturesRepository))
		{
			$this->productsFeaturesRepository = new DataAccess_ProductsFeaturesRepository($this->getDb());
		}

		return $this->productsFeaturesRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesGroupsRepository|null
	 */
	protected function getProductsFeaturesGroupsRepository()
	{
		if (is_null($this->productsFeaturesGroupsRepository))
		{
			$this->productsFeaturesGroupsRepository = new DataAccess_ProductsFeaturesGroupsRepository($this->getDb());
		}

		return $this->productsFeaturesGroupsRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesValuesRepository
	 */
	protected function getProductsFeaturesValuesRepository()
	{
		if (is_null($this->productsFeaturesValuesRepository))
		{
			$this->productsFeaturesValuesRepository = new DataAccess_ProductsFeaturesValuesRepository($this->getDb());
		}

		return $this->productsFeaturesValuesRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesValuesRelationsRepository
	 */
	protected function getProductsFeaturesValuesRelationsRepository()
	{
		if (is_null($this->productsFeaturesValuesRelationsRepository))
		{
			$this->productsFeaturesValuesRelationsRepository = new DataAccess_ProductsFeaturesValuesRelationsRepository($this->getDb());
		}

		return $this->productsFeaturesValuesRelationsRepository;
	}

	/**
	 * @return DataAccess_ProductsFeaturesGroupsProductsRelationsRepository
	 */
	protected function getProductsFeaturesGroupsProductsRelationsRepository()
	{
		if (is_null($this->productsFeaturesGroupsProductsRelationsRepository))
		{
			$this->productsFeaturesGroupsProductsRelationsRepository = new DataAccess_ProductsFeaturesGroupsProductsRelationsRepository($this->getDb());
		}

		return $this->productsFeaturesGroupsProductsRelationsRepository;
	}
}
<?php

/**
 * Class Admin_Controller_GoShopping
 */
class Admin_Controller_GoShopping extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		$data['categories'] = $categoryRepository->getOptionsList();

		$goShoppingForm = new Admin_Form_GoShoppingForm();

		$adminView = $this->getView();
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9009');
		$adminView->assign('form', $goShoppingForm->getForm($data));
		$adminView->assign('body', 'templates/pages/go-shopping/index.html');
		$adminView->render('layouts/default');
	}
}
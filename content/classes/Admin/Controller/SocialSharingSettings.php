<?php

/**
 * Class Admin_Controller_SocialSharingSettings
 */
class Admin_Controller_SocialSharingSettings extends Admin_Controller
{
	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'social');

	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$data = $settings->getByKeys(array(
			'facebookLikeButtonProduct', 'facebookCommentsProduct', 'facebookAppId', 'facebookPageAccount',
			'twitterAccount','twitterTweetButtonProduct','googlePlusButtonProduct','deliciousButtonProduct',
			'pinItButtonProduct','pinterestUsername','disallowPinterestPinning','youtubePageURL','snapChatAccount',
			'googlePlusAccount','bloggerAccount','instagramAccount','instagramButtonProduct', 'youtubeButtonProduct',
			'snapChatButtonProduct', 'bloggerButtonProduct'
		));

		$TPForm = new Admin_Form_SocialSharingSettingsForm();
		$form = $TPForm->getForm($data);

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'social_settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings');
			}
			else
			{
				$validationErrors = array();
				$formData = array();
				foreach ($data as $key => $temp)
				{					
					$formData[$key] = trim($request->request->get($key, ''));

					if ($key == 'facebookPageAccount' && $formData[$key] != '' && !validate($formData[$key], VALIDATE_URL)) 
					{
						$validationErrors[$key] = array('Facebook Page URL must be valid URL');
					}
					if ($key == 'twitterAccount' && $formData[$key] != '' && !validate($formData[$key], VALIDATE_URL))
					{
						$validationErrors[$key] = array('Twitter Account URL must be valid URL');
					}
				}

				if (count($validationErrors) == 0)
				{
					$settings->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('settings_social');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '7001');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('social_settings_update'));
		$adminView->assign('body', 'templates/pages/settings/social-sharing.html');
		$adminView->render('layouts/default');
	}
}
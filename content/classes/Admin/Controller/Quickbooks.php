<?php

/**
 * Class Admin_Controller_Quickbooks
 */
class Admin_Controller_Quickbooks extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9026');

		$db = $this->getDb();
		$productsStatus = $db->selectOne('
			SELECT COUNT(*) as `count`, max(last_sync_date) as last_sync_date
			FROM ".DB_PREFIX."intuit_sync s
			INNER JOIN ".DB_PREFIX."products p ON s.id = p.product_id AND s.type = "item"
		');

		$customersStatus = $db->selectOne('
			SELECT COUNT(*) as `count`, max(last_sync_date) as last_sync_date
			FROM ".DB_PREFIX."intuit_sync s
			INNER JOIN ".DB_PREFIX."users u ON s.id = u.uid AND s.type = "user"
		');

		$ordersStatus = $db->selectOne('
			SELECT COUNT(*) as `count`, max(last_sync_date) as last_sync_date
			FROM ".DB_PREFIX."intuit_sync s
			INNER JOIN ".DB_PREFIX."orders o ON s.id = o.oid AND s.type = "order"
		');

		$adminView->assign('productsStatus', $productsStatus);
		$adminView->assign('customersStatus', $customersStatus);
		$adminView->assign('ordersStatus', $ordersStatus);

		$settingsData = $this->getSettings()->getAll();
		$quickStart = new Admin_Intuit_QuickStart($db, $settingsData, $adminView);

		$adminView->assign('modalSettings', $quickStart->action_render('pages/quickbooks/modal-settings'));
		$adminView->assign('body', 'templates/pages/quickbooks/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @return bool
	 */
	public function canAccess()
	{
		return defined('_ACCESS_INTUIT_ANYWHERE') && _ACCESS_INTUIT_ANYWHERE;
	}

	/**
	 * @return bool
	 */
	public function canActivate()
	{
		return $this->canAccess() && defined('_CAN_ENABLE_INTUIT_ANYWHERE') && _CAN_ENABLE_INTUIT_ANYWHERE;
	}

	/**
	 * Pre-activate action
	 */
	public function preActivateAction()
	{
		$settings = $this->getSettings();
		$adminView = Ddm_AdminView::getInstance();

		$data = array(
			'l' => LICENSE_NUMBER,
			'u' => $settings->get('GlobalHttpsUrl') . '/admin.php?p=quickbooks&mode=refresh-license'
		);

		$upgradeUrl = false;

		if (!$this->canActivate())
		{
			$upgradeUrl = 'https://account.' . getp() . '.com/account/purchase-upgrade/' . urlencode(base64_encode(serialize($data))) . '.html';
		}

		$adminView->assign('upgradeUrl', $upgradeUrl);

		global $menu_proxy, $grant_url;

		require_once dirname(dirname(dirname(dirname(__FILE__)))).'/vendors/intuit/oauth/config.php';

		$adminView->assign('menu_proxy', $menu_proxy);
		$adminView->assign('grant_url', $grant_url);
	}

	/**
	 * Deactivate action
	 */
	public function deactivateAction()
	{
		$settings = $this->getSettings();

		$wasIntuitAAEnabled = $settings->get('intuit_anywhere_enabled') == 'Yes';
		$settings->persist(array('intuit_anywhere_enabled' => 'No'));

		if ($wasIntuitAAEnabled)
		{
			$intuitSync = intuit_Services_QBSync::create();
			if ($intuitSync !== null) $intuitSync->disconnect();
			intuit_ServiceLayer::disconnect();
		}
	}

	/**
	 * Refresh license
	 */
	public function refreshLicenseAction()
	{
		@unlink('content/cache/license/license.key');
		$this->redirect('admin.php?p=app&key=quickbooks');
	}

	/**
	 * Update status
	 */
	public function updateStatusAction()
	{
		$request = $this->getRequest();
		$action = $request->get('action');

		if ($action == 'disable-quickbooks')
		{
			$this->deactivateAction();
		}

		$this->redirect('');
	}
}
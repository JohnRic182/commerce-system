<?php

/**
 * Class Admin_Controller_GlobalAttributesProductsImport
 */
class Admin_Controller_GlobalAttributesProductsImport extends Admin_Controller
{
	/** @var DataAccess_GlobalAttributeRepository $globalAttrubutesRepository */
	protected $globalAttrubutesRepository = null;
	
	/** @var Admin_Service_GlobalAttributesProductsImport $globalAttributesImportService */
	protected $globalAttributesImportService = null;

	/** @var array $defaults */
	protected $defaults = array(
		'fields_separator' => ',',
		'make_manufacturers_visible' => 'Yes',
		'make_manufacturers_visible_type' => 'Yes'
	);

	protected $menuItem = array('primary' => 'products', 'secondary' => 'products-attributes');

	/**
	 * Display manufacturers CSV upload form
	 */
	public function startAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Admin_Form_GlobalAttributesProductsImportForm $globalAttributesImportForm */
		$globalAttributesImportForm = new Admin_Form_GlobalAttributesProductsImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $globalAttributesImportForm->getStartForm(array());

		if ($request->isPost())
		{
			$formData = array_merge($this->defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_attributes');
			}
			else
			{
				/** @var Admin_Service_GlobalAttributesProductsImport $importService */
				$importService = $this->getGlobalAttributesProductsImportService();

				$errors = array();

				if ($fileInfo = $request->getUploadedFile('bulk'))
				{
					if ($uploadedFile = $importService->saveUploadedFile($fileInfo))
					{
						/** @var Framework_Session $session */
						$session = $this->getSession();

						$session->set('global-attributes-products-import-settings', array(
							'file' => $uploadedFile,
							'fields_separator' => $formData['fields_separator'],
						));

						$this->redirect('bulk_global_attributes_products', array('action' => 'assign'));
					}
					else
					{
						$errors['bulk'][] = 'Cannot save uploaded file. Please check free space and permissions';
					}
				}
				else
				{
					$errors['bulk'][] = 'Please select file for uploading';
				}

				$form->bind($formData);
				$form->bindErrors($errors);
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3015');

		$adminView->assign('page', 'bulk_global_attributes_products');
		$adminView->assign('pageCancel', 'products_attributes');
		$adminView->assign('pageTitle', 'Global Attributes Products Import');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('global_attributes_products_import_start'));

		$adminView->assign('body', 'templates/pages/import/start.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Display fields assignment form
	 */
	public function assignAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = $session->get('global-attributes-products-import-settings', $this->defaults);

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('Cannot find uploaded file', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_attributes');
		}

		/** @var array $formData */
		$formData = array_merge($importSettings, $this->defaults);

		/** @var Admin_Service_GlobalAttributesProductsImport $importService */
		$importService = $this->getGlobalAttributesProductsImportService();

		/** @var array $assignOptions */
		$assignOptions = $importService->getAssignOptions($uploadedFile, $formData['fields_separator']);

		$formData = array_merge($formData, $assignOptions);
		$formData['skip_first_line'] = count($assignOptions['columns']) > 1 ? '1' : '0';

		/** @var Admin_Form_GlobalAttributesProductsImportForm $globalAttributesImportForm */
		$globalAttributesImportForm = new Admin_Form_GlobalAttributesProductsImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $globalAttributesImportForm->getAssignForm($formData);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('fields', $fields = $importService->getFields());
		$adminView->assign('fieldsJson', json_encode($fields));
		$adminView->assign('assignOptions', $assignOptions);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3016');

		$adminView->assign('page', 'bulk_global_attributes_products');
		$adminView->assign('pageCancel', 'products_attributes');
		$adminView->assign('pageTitle', 'Global Attributes Products Import');
		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/import/assign.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Do import and finish
	 */
	public function importAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = $session->get('global-attributes-products-import-settings', $this->defaults);

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('Cannot find uploaded file', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_attributes');
		}

		/** @var array $formData */
		$formData = $request->request->all();

		/** @var Admin_Service_GlobalAttributesProductsImport $importService */
		$importService = $this->getGlobalAttributesProductsImportService();

		$result = $importService->import(
			$uploadedFile, $formData['assign_fields'],
			$importSettings['fields_separator'],
			isset($formData['skip_first_line']) && $formData['skip_first_line'] == 1,
			array()
		);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3017');

		$adminView->assign('page', 'bulk_global_attributes_products');
		$adminView->assign('pageCancel', 'products_attributes');
		$adminView->assign('pageTitle', 'Global Attributes Products Import');

		if ($result)
		{
			/** @var Admin_Form_GlobalAttributesProductsImportForm $globalAttributesImportForm */
			$globalAttributesImportForm = new Admin_Form_GlobalAttributesProductsImportForm();

			/** @var core_Form_Form $formBuilder */
			$form = $globalAttributesImportForm->getImportForm($result);

			$adminView->assign('form', $form);
		}
		else
		{
			Admin_Flash::setMessage('Cannot import data. Something went wrong.', Admin_Flash::TYPE_ERROR);
		}

		$adminView->assign('body', 'templates/pages/import/import.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_GlobalAttributeRepository
	 */
	protected function getGlobalAttributesRepository()
	{
		if (is_null($this->globalAttrubutesRepository))
		{
			$this->globalAttrubutesRepository = new DataAccess_GlobalAttributeRepository($this->getDb(),
					new DataAccess_ProductsRepository($this->getDb()), new DataAccess_ProductAttributeRepository($this->getDb())
				);
		}

		return $this->globalAttrubutesRepository;
	}

	/**
	 * Get import service
	 *
	 * @return Admin_Service_ManufacturerImport
	 */
	protected function getGlobalAttributesProductsImportService()
	{
		if (is_null($this->globalAttributesImportService))
		{
			$this->globalAttributesImportService = new Admin_Service_GlobalAttributesProductsImport(
				$this->getGlobalAttributesRepository(),
				new Admin_Service_FileUploader()
			);
		}

		return $this->globalAttributesImportService;
	}
}
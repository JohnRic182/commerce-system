<?php

class Admin_Controller_NexTag extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		$data['categories'] = $categoryRepository->getOptionsList();

		$nexTagForm = new Admin_Form_NexTagForm();

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9011');

		$adminView->assign('form', $nexTagForm->getForm($data));

		$adminView->assign('body', 'templates/pages/nex-tag/index.html');
		$adminView->render('layouts/default');
	}
}
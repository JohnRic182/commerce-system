<?php

/**
 * Class Admin_Controller_ProductImport
 */
class Admin_Controller_ProductImport extends Admin_Controller
{
	/** @var DataAccess_ProductsRepository $productRepository */
	protected $productRepository = null;

	/** @var DataAccess_CategoryRepository $productRepository */
	protected $categoryRepository = null;

	/** @var Admin_Service_ProductImport $productImportService */
	protected $productImportService = null;

	/** @var array $defaults */
	protected $defaults = array(
		'fields_separator' => ',',
		'make_products_visible' => 'Yes',
		'make_products_visible_type' => 'Yes',
		'attributes_import_limit' => 10,
		'images_import_limit' => 10
	);

	protected $menuItem = array('primary' => 'products', 'secondary' => 'products');

	/**
	 * Display products CSV upload form
	 */
	public function startAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$session = $this->getSession();

		/** @var Admin_Form_ProductImportForm $productImportForm */
		$productImportForm = new Admin_Form_ProductImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $productImportForm->getStartForm(array(), $this->getCategoryRepository()->getOptionsList());

		if ($request->isPost())
		{
			$formData = array_merge($this->defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products');
			}
			else
			{
				$errors = array();

				if (isset($formData['category_name']) && trim($formData['category_name']) == '')
				{
					$errors['category_name'] = array(trans('product.import_select_default_category_empty'));
				}
				else
				{
					if ($fileInfo = $request->getUploadedFile('bulk'))
					{
						/** @var Admin_Service_ProductImport $importService */
						$importService = $this->getProductImportService($formData['attributes_import_limit'], $formData['images_import_limit']);

						if ($uploadedFile = $importService->saveUploadedFile($fileInfo))
						{
							/** @var Framework_Session $session */
							$session = $this->getSession();

							// check content to strip invalid html tags
							if (isset($formData['strip_tags']) && $formData['strip_tags'] == 'on')
							{
								$content = file_get_contents($uploadedFile);
								$resultTags = $this->checkIfInvalidHtmlTagsExist($content);

								if ($resultTags !== false)
								{
									$tags = implode(', ', $resultTags);
									$errors['bulk'] = array('Found an invalid html tags in the .csv file content <strong>('.$tags.')</strong>. It can cause security issues, break UI views and data malformed. Do you want to continue and strip those invalid tag(s)? Click <a style="text-decoration:underline;" href="admin.php?p=bulk_products&action=assign"><strong>Continue</strong></a> or <a style="text-decoration:underline;" href="admin.php?p=products"><strong>Cancel</strong></a>');
								}
							}

							//session data
							$session->set('product-import-settings', array(
								'file' => $uploadedFile,
								'cid' => (isset($formData['cid']) ? $formData['cid'] : 0),
								'category_name' => (isset($formData['category_name']) ? $formData['category_name'] : ''),
								'fields_separator' => $formData['fields_separator'],
								'update_rule' => $formData['update_rule'],
								'make_products_visible' => $formData['make_products_visible'],
								'strip_tags' => (isset($formData['strip_tags']) && $formData['strip_tags'] == 'on') ? $formData['strip_tags'] : 'off',
								'invalid_tags' => $resultTags,
								'attributes_import_limit' => $formData['attributes_import_limit'],
								'images_import_limit' => $formData['images_import_limit']
							));

							if (empty($errors)) $this->redirect('bulk_products', array('action' => 'assign'));
						}
						else
						{
							$errors['bulk'] = array(trans('product.import_bulk_upload_permissions'));
						}
					}
					else
					{
						$errors['bulk'] = array(trans('product.import_bulk_upload_empty'));
					}
				}

				$form->bind($formData);
				$form->bindErrors($errors);
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3003');

		$adminView->assign('page', 'bulk_products');
		$adminView->assign('pageCancel', 'products');
		$adminView->assign('pageTitle', 'Import Products');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('product_import_start'));

		$adminView->assign('body', 'templates/pages/import/start.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Display fields assignment form
	 */
	public function assignAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = array_merge($this->defaults, $session->get('product-import-settings', $this->defaults));

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('Cannot find uploaded file', Admin_Flash::TYPE_ERROR);
			$this->redirect('products');
		}
		
		// strip invalid html tags if the option is enabled
		if (isset($importSettings['strip_tags']) && $importSettings['strip_tags'] == 'on' && $importSettings['invalid_tags'] !== false)
		{
			$content = file_get_contents($uploadedFile);
			file_put_contents($uploadedFile, $this->stripInvalidHtmlTags($content, $importSettings['invalid_tags']));
		}

		/** @var array $formData */
		$formData = array_merge($this->defaults, $importSettings);

		/** @var Admin_Service_ProductImport $importService */
		$importService = $this->getProductImportService($importSettings['attributes_import_limit'], $importSettings['images_import_limit']);

		/** @var array $assignOptions */
		$assignOptions = $importService->getAssignOptions($uploadedFile, $formData['fields_separator']);

		$formData = array_merge($formData, $assignOptions);

		$formData['skip_first_line'] = count($assignOptions['columns']) > 1 ? '1' : '0';

		/** @var Admin_Form_ProductImportForm $productImportForm */
		$productImportForm = new Admin_Form_ProductImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $productImportForm->getAssignForm($formData);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('fields', $fields = $importService->getFields());
		$adminView->assign('fieldsJson', json_encode($fields));
		$adminView->assign('assignOptions', $assignOptions);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3004');

		$adminView->assign('page', 'bulk_products');
		$adminView->assign('pageCancel', 'products');
		$adminView->assign('pageTitle', 'Import Products');

		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/import/assign.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Do import and finish
	 */
	public function importAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = array_merge($this->defaults, $session->get('product-import-settings', $this->defaults));

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('Cannot find uploaded file', Admin_Flash::TYPE_ERROR);
			$this->redirect('products');
		}

		/** @var array $formData */
		$formData = $request->request->all();

		if ( isset($importSettings['category_name']) && !empty($importSettings['category_name']) )
		{
			$categoryRepository = $this->getCategoryRepository();
			$category = $categoryRepository->getDefaults(Admin_Controller_Category::MODE_ADD);
			$category['is_visible'] = 'Yes';
			$category['name'] = $importSettings['category_name'];
			$category['key_name'] = str_replace(' ', '_', strtolower($category['name']));
			$importSettings['cid'] = $categoryRepository->persist($category);
		}

		/** @var Admin_Service_ProductImport $importService */
		$importService = $this->getProductImportService($importSettings['attributes_import_limit'], $importSettings['images_import_limit']);

		$result = $importService->import(
			$uploadedFile, $formData['assign_fields'],
			$importSettings['fields_separator'],
			isset($formData['skip_first_line']) && $formData['skip_first_line'] == 1,
			array(
				'updateRule' => $importSettings['update_rule'],
				'makeVisible' => $importSettings['make_products_visible'] == 'Yes',
				'defaultCategoryId' => $importSettings['cid']
			)
		);

		Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), true, true, false, false);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3005');

		$adminView->assign('page', 'bulk_products');
		$adminView->assign('pageCancel', 'products');
		$adminView->assign('pageTitle', 'Import Products');

		if ($result)
		{
			/** @var Admin_Form_ProductImportForm $productImportForm */
			$productImportForm = new Admin_Form_ProductImportForm();

			/** @var core_Form_Form $formBuilder */
			$form = $productImportForm->getImportForm($result);

			$adminView->assign('form', $form);
		}
		else
		{
			Admin_Flash::setMessage('Cannot import data. Something went wrong.', Admin_Flash::TYPE_ERROR);
		}

		$adminView->assign('body', 'templates/pages/import/import.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CategoryRepository
	 */
	protected function getProductRepository()
	{
		if (is_null($this->productRepository))
		{
			$this->productRepository = new DataAccess_ProductsRepository($this->getDb());
		}

		return $this->productRepository;
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CategoryRepository
	 */
	protected function getCategoryRepository()
	{
		if (is_null($this->categoryRepository))
		{
			$this->categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		}

		return $this->categoryRepository;
	}

	/**
	 * Get import service
	 *
	 * @param null $attributesImportLimit
	 *
	 * @return Admin_Service_ProductImport
	 */
	protected function getProductImportService($attributesImportLimit = 0, $imagesImportLimit = 0)
	{
		if (is_null($this->productImportService))
		{
			$db = $this->getDb();

			$this->productImportService = new Admin_Service_ProductImport(
				$this->getProductRepository(),
				new Admin_Service_FileUploader(),
				array(
					'categoryRepository' => new DataAccess_CategoryRepository($db),
					'manufacturerRepository' => new DataAccess_ManufacturerRepository($db),
					'productLocationRepository' => new DataAccess_ProductLocationRepository($db),
					'taxRepository' => new DataAccess_TaxRepository($db),
					'productAttributeRepository' => new DataAccess_ProductAttributeRepository($db),
					'productVariantRepository' => new DataAccess_ProductVariantRepository($db),
					'productQuantityDiscount' => new DataAccess_ProductQuantityDiscountRepository($db),
					'productImageRepository' => new DataAccess_ProductImageRepository($db, $this->getSettings(), new Admin_Service_FileUploader()),
					'shippingRepository' => new DataAccess_ShippingRepository($db),
					'attributesImportLimit' => $attributesImportLimit,
					'imagesImportLimit' => $imagesImportLimit
				)
			);
		}

		return $this->productImportService;
	}

	/**
	 * @param $content
	 * @return array|bool
	 */
	protected function checkIfInvalidHtmlTagsExist($content)
	{
		$tagsFound = array();
		$invalidTags = array('form', 'script', 'object', 'head', 'style', 'embed', 'applet', 'noframes', 'noscript', 'noembed');

		foreach ($invalidTags as $k => $value)
		{
			$regex = '/<' . $value . '[^>](.*?)\<\/' . $value . '\>/';
			$match = preg_match($regex, $content);
			if ($match == 1) $tagsFound[] = $value;
		}

		return count($tagsFound) > 0 ? $tagsFound : false;
	}

	/**
	 * @param $string
	 * @param $tags
	 * @return bool|mixed|string
	 */
	protected function stripInvalidHtmlTags($string, $tags)
	{
		if (!is_array($tags)) return false;

		$result = '';

		foreach ($tags as $k => $value)
		{
			$regex = "/<{$value}[^>](.*?)\<\/{$value}\>/";
			if (preg_match($regex, $string))
			{
				$result = preg_replace($regex, '', $string);
				$string = $result;
			}
		}

		return $result;
	}
}

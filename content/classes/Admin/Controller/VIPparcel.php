<?php
	/**
	 * Class Admin_Controller_VIPparcel
	 */
	class Admin_Controller_VIPparcel extends Admin_Controller
	{
		protected $menuItem = array('primary' => '', 'secondary' => '');
		protected  $shippingrepository = null;
		protected  $orderrepository = null;
		protected  $shipping_vipparcel = null;
		protected  $vipparcelLabelsRepository = null;

		protected  $db = null;
		protected  $settings = null;


		public function __construct(Framework_Request $request, $db, $settings){
			parent::__construct($request, $db);
			$this->db 			=  $db;
			$this->settings 	= $settings;
		}

		private function _getOrderObject($order_data){
			$db = $this->db;
			$settings = $this->settings;

			$oid = intval($order_data['oid']);

			$user = USER::getById($db, $settings, $order_data['uid']);
			$order = ORDER::getById($db, $settings, $user, $oid);

			$user->getUserData();
			$order->getOrderData();
			$order->getOrderItems(true);

			// have to get VAT status
			$taxProvider = Tax_ProviderFactory::getTaxProvider();
			$taxProvider->initTax($order);
			$settings['DisplayPricesWithTax'] = $taxProvider->getDisplayPricesWithTax() ? 'YES' : 'NO';

			return $order;
		}

		public function generatelabelAction()
		{
			$request = $this->getRequest();
			
			$db = $this->db;
			$settings = $this->settings;
			
			$nonce = $request->request->get("nonce",'');
			$orderId = intval($request->request->get('orderid'));
			$vparcel_ssid = $request->request->get('ssid', false);
			$parcelitems = $request->request->get('shpitemid', array());
			$parcelitems = array_keys($parcelitems, '1');

			if ($vparcel_ssid)
			{			
				$shRep = $this->getShippingRepository();
				$shipments_data = $shRep->getShippingMethodData($vparcel_ssid);
				$vparcel_shipping = $this->getShippingVipparcel($db, $settings, $shipments_data);

				//  SENDER
				// ----------------------------------------------------------------------------------------
				$sender = array();
				$shipFromZip = trim($request->request->get('shipfromzip',''));
				$shipFromZip4 = '';
				$shipFromCountryCode = $request->request->get('shipfromcountrycode','');
				$p = strpos($shipFromZip, '-');
				if ($shipFromCountryCode == 'US' && $p !== false)
				{
					$shipFromZip4 = substr($shipFromZip, $p + 1);
					$shipFromZip = substr($shipFromZip, 0, $p);
				}

				$sender['streetAddress'] = htmlentities($request->request->get('shipfromaddress1').' '. $request->request->get('shipfromaddress2'));
				$sender['city'] = htmlentities($request->request->get('shipfromcity'));
				$sender['firstName'] = htmlentities($request->request->get('shipfromname'));
				$sender['lastName'] = '';
				$sender['phone'] = preg_replace('![^\d]*!','',$request->request->get('shipfromphone'));
				$sender['postalCode'] = $shipFromZip;
				$sender['zip4'] = $shipFromZip4;
				$sender['state'] = $request->request->get('shipfromstate');
				$sender['email'] = $request->request->get('shipFromEmail');
				$sender['company'] = htmlentities($request->request->get('shipfromname'));

				// RECIPIENT
				// ----------------------------------------------------------------------------------------
				$recipient = array();

				$shipToAddress1 = htmlentities($request->request->get('shiptoaddress1'));
				$shipToAddress2 = htmlentities($request->request->get('shiptoaddress2'));
				$shipToCountryCode = $request->request->get('shiptocountrycode');
				$shipToZip = trim($request->request->get('shiptozip'));
				$shipToZip4 = '';
				$p = strpos($shipToZip, '-');
				if ($shipToCountryCode == 'US' && $p !== false)
				{
					$shipToZip4 = substr($shipToZip, $p + 1);
					$shipToZip = substr($shipToZip, 0, $p);
				}
				$recipient['countryId'] = $shipToCountryCode;
				$recipient['city'] = $request->request->get('shiptocity');
				$recipient['firstName'] = htmlentities($request->request->get('shiptoname'));
				$recipient['lastName'] = '';
				$recipient['postalCode'] = $shipToZip;
				$recipient['zip4'] = $shipToZip4;
				$recipient['streetAddress'] = htmlentities($shipToAddress1.' '.$shipToAddress2);
				$recipient['state'] = $request->request->get('shiptostate');
				$recipient['province'] = $request->request->get('shiptostate');
				$recipient['phone'] = $request->request->get('shiptophone');
				$recipient['email'] = $request->request->get('shipToEmail');
				$recipient['company'] = htmlentities($request->request->get('shiptocompany'));
				//---------------------------------------------------------------------------------------------------

				// customsInfo (International Only)
				// ----------------------------------------------------------------------------------------
				$customsInfo = array();
				$customsInfo['eelPfc'] = $request->request->get('eelPfc');
				$customsInfo['contentsExplanation'] = $request->request->get('contentsExplanation');
				$customsInfo['category'] = $request->request->get('category');


				// customsItem (International Only)
				// ----------------------------------------------------------------------------------------
				$orderRepository = $this->getOrderRepository();
				$odata =  $orderRepository->getOrderDataForAdmin($orderId);
				$order = $this->_getOrderObject($odata);
				$items = $order->getOrderItems();

				$customsItem = array();
				foreach ($items as $k => $v)
				{
					if ( in_array($v['ocid'], $parcelitems) )
					{
						$item['quantity'] = $v['quantity'];
						$item['value'] = $v['product_price'];
						$item['weightOz'] = floatval( Shipping_VIPparcel::weigthInOz($v['weight']));
						$item['description'] = (trim($v['title']) !== '')?htmlentities(substr($v['title'],0,50)):htmlentities(substr($v['description'],0,50));
						$customsItem[] = $item;
					}
				}

				//---------------------------------------------------------------------------------------------------
				$labeldata = array();
				$labeldata['mailClass'] 	= $request->request->get('mailClass', $shipments_data['method_id']);
				$labeldata['weightOz'] 		= floatval($request->request->get('WeightOz'));
				$labeldata['insuredValue'] 	= floatval($request->request->get('insuredValue'));
				$labeldata['labelType'] 	= $request->request->get('labelType');
				$labeldata['description'] 	= htmlentities($request->request->get('description'));
				$labeldata['service'] 		= $request->request->get('service');
				if (trim($labeldata['service']) == '') unset($labeldata['service']);
				$labeldata['sender'] 		= $sender;
				$labeldata['recipient'] 	= $recipient;
				$labeldata['customsInfo'] 	= $customsInfo;
				$labeldata['customsItem'] 	= $customsItem;

				$response = $vparcel_shipping->getParcelLabel($labeldata);
				$isGetLabelStatusCode = $response['statusCode'];
				$status = 200;

				$ret = array();
				if ($isGetLabelStatusCode == Shipping_VIPparcel::CODE_CREATED)
				{
					$encodedLabelImage = isset($response['images']['0']) ?  $response['images']['0'] : false;
					$trackingNumber = $response['trackNumber'];
					$label_amount = $response['value'];

					$recorddata = array();
					$recorddata['tracking_number'] = $trackingNumber;
					$recorddata['label_amount'] = $label_amount;
					$recorddata['pic_number'] = isset($response['id']) ? $response['id'] : $response['CustomsNumber'];
					$recorddata['order_id']	= $orderId;
					$vparcel_data_repository = $this->getvparcelLabelsRepository();

					$labelId = $vparcel_data_repository->persist($recorddata);
					$labelUrl = $vparcel_data_repository->SaveShippingLabel($encodedLabelImage, $orderId, $labelId, $trackingNumber);

					$recorddata = array();
					$recorddata['id'] = $labelId;
					$recorddata['label_url'] =  $labelUrl;
					$labelId = $vparcel_data_repository->persist($recorddata);

					$db->reset();
					$db->assignStr('shipping_tracking_number', $trackingNumber);
					$db->assignStr('shipping_tracking_number_type', "USPS");
					$db->update(DB_PREFIX.'orders', 'WHERE oid='.intval($orderId));

					$ret = array(
						'result' => 1,
						'error' => '',
						'trackingNumber' => $trackingNumber,
						'labelUrl' => $labelUrl
					);
				}
				else
				{
					if (isset($response['ErrorMessage']))
					{
						$ret = array(
							'result' => 0,
							'error' => $response['ErrorMessage'],
							'trackingNumber' => '',
							'labelUrl' => ''
						);
					}
				}
			}
			header('HTTP/1.0 '.$status);
			header('Content-Type: application/json');
			echo json_encode($ret);
			die();
		}

		public function requestRefundAction()
		{
			$request = $this->getRequest();
			
			if ($request->isPost())
			{
				if (!Nonce::verify($request->request->get('nonce'), 'vparcel_refund', false))
				{
					$this->renderJson(array(
						'status' => 0,
						'message' => trans('common.invalid_nonce'),
					));
					return;
				}
				else
				{
					$errors = array();

					$picNumber = trim($request->request->get('pic_number'));
					$reason = trim($request->request->get('refund_reason',''));
					
					if ($picNumber == '')
					{
						$errors[] = array('field' => '.field-pic_number', 'message' => trans('vparcel.pic_number_required'));
					}

					if (count($errors) > 0)
					{
						$this->renderJson(array(
							'status' => 0,
							'errors' => $errors,
						));
						return;
					}
					else
					{
						$vparcel_shipping = $this->getShippingVipparcel($this->db, $this->settings, null);
						$labelsRepository = $this->getvparcelLabelsRepository();
						
						$numbers = array($picNumber);
						$refundStatus = $vparcel_shipping->RequestPostageRefund($numbers, $reason);
						$status = $refundStatus['status'] ? 1 : 0;
						if ($status)
						{
							$labelsRepository->deleteByPictNumber($picNumber);
						}
						if ($status == 1) Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

						$this->renderJson(array(
							'status' => $status,
							'message' => $status ? '' : trans('vparcel.refund_failed').': '.$refundStatus['status_message']
						));
					}
				}
			}
		}
		
		public function generatePostageLabelAction()
		{
			$db = $this->db;
			$settings = $this->settings;
			$request = $this->getRequest();
			$postData = $request->request->all();
			$nonce	= isset($postData['nonce']) ? $postData['nonce'] : '';
			$orderId = intval($postData['orderid']);
			$vparcel_ssid = isset($postData['ssid']) ? intval($postData['ssid']) : false;
			$parcelitems = isset($postData['shpitemid']) ? $postData['shpitemid'] : array();
			$parcelitems = array_keys($parcelitems, '1');
		
			if ($vparcel_ssid)
			{
			
				$shRep = new DataAccess_ShippingRepository($db);
				$shipments_data = $shRep->getShippingMethodData($vparcel_ssid);
				$vparcel_shipping = new Shipping_VIPparcel($db, $settings, $shipments_data);
		
				//  SENDER
				// ----------------------------------------------------------------------------------------
				$sender = array();
				$shipFromZip = trim($postData['shipfromzip']);
				$shipFromZip4 = '';
				$shipFromCountryCode = $postData['shipfromcountrycode'];
				$p = strpos($shipFromZip, '-');
				if ($shipFromCountryCode == 'US' && $p !== false)
				{
					$shipFromZip4 = substr($shipFromZip, $p + 1);
					$shipFromZip = substr($shipFromZip, 0, $p);
				}
				
				$sender['streetAddress'] = $postData['shipfromaddress1'].' '. $postData['shipfromaddress2'];
				$sender['city'] = $postData['shipfromcity'];
				$sender['firstName'] = $postData['shipfromname'];
				$sender['lastName'] = '';
				$sender['phone'] =     preg_replace('![^\d]*!','',$postData['shipfromphone']);
				$sender['postalCode'] = $shipFromZip;
				$sender['zip4'] = $shipFromZip4;
				$sender['state'] = $postData['shipfromstate'];
				$sender['email'] = $postData['shipFromEmail'];
				$sender['company'] = $postData['shipfromname'];
		
		
				// RECIPIENT
				// ----------------------------------------------------------------------------------------
				$recipient = array();
		
				$shipToAddress1 = $postData['shiptoaddress1'];
				$shipToAddress2 = $postData['shiptoaddress2'];
				$shipToCountryCode = $postData['shiptocountrycode'];
				$shipToZip = trim($postData['shiptozip']);
				$shipToZip4 = '';
				$p = strpos($shipToZip, '-');
				if ($shipToCountryCode == 'US' && $p !== false)
				{
					$shipToZip4 = substr($shipToZip, $p + 1);
					$shipToZip = substr($shipToZip, 0, $p);
				}
				$recipient['countryId'] = $shipToCountryCode;
				$recipient['city'] = $postData['shiptocity'];
				$recipient['firstName'] = $postData['shiptoname'];
				$recipient['lastName'] = '';
				$recipient['postalCode'] = $shipToZip;
				$recipient['zip4'] = $shipToZip4;
				$recipient['streetAddress'] = $shipToAddress1.' '.$shipToAddress2;
				$recipient['state'] = $postData['shiptostate'];
				$recipient['province'] = $postData['shiptostate'];
				$recipient['phone'] = $postData['shiptophone'];
				$recipient['email'] = $postData['shipToEmail'];
				$recipient['company'] = $postData['shiptocompany'];
				//---------------------------------------------------------------------------------------------------
		
				// customsInfo (International Only)
				// ----------------------------------------------------------------------------------------
				$customsInfo = array();
				$customsInfo['eelPfc'] = $postData['eelPfc'];
				$customsInfo['contentsExplanation'] = $postData['contentsExplanation'];
				$customsInfo['category'] = $postData['category'];
		
		
				// customsItem (International Only)
				// ----------------------------------------------------------------------------------------
				$orderRepository = new DataAccess_OrderRepository($db, $settings);
				$odata =  $orderRepository->getOrderDataForAdmin($orderId);
				$order = $this->_getOrderObject($odata);
				$items = $order->getOrderItems();
		
				$customsItem = array();
				$label_amount = 0;
				foreach ($items as $k => $v)
				{
					if ( in_array($v['ocid'], $parcelitems) )
					{
						$item['quantity'] = $v['quantity'];
						$item['value'] = $v['product_price'];
						$item['weightOz'] = floatval( Shipping_VIPparcel::weigthInOz($v['weight']));
						$item['description'] = (trim($v['title']) !== '')?substr($v['title'],0,50):substr($v['description'],0,50);
		
						$customsItem[] = $item;
						$label_amount = $label_amount + ($v['quantity'] * $v['price']);
					}
				}
		
				//---------------------------------------------------------------------------------------------------
				$labeldata = array();
				$labeldata['mailClass'] 	= (isset($postData['mailClass']) ? $postData['mailClass'] : $shipments_data['method_id']);
				$labeldata['weightOz'] 		= floatval($postData['WeightOz']);
				$labeldata['insuredValue'] 	= floatval($postData['insuredValue']);
				$labeldata['labelType'] 	= $postData['labelType'];
				$labeldata['description'] 	= $postData['description'];
				$labeldata['service'] 		= $postData['service'];
				$labeldata['sender'] 		= $sender;
				$labeldata['recipient'] 	= $recipient;
				$labeldata['customsInfo'] 	= $customsInfo;
				$labeldata['customsItem'] 	= $customsItem;
		
				$response = $vparcel_shipping->getParcelLabel($labeldata);
				$isGetLabelStatusCode = $response['statusCode'];
				$status = 200;
		
				$ret = array();
				if ($isGetLabelStatusCode == Shipping_VIPparcel::CODE_CREATED)
				{
					$encodedLabelImage = isset($response['images']['0']) ?  $response['images']['0'] : false;
					$trackingNumber = $response['trackNumber'];
		
					$recorddata = array();
					$recorddata['tracking_number'] = $trackingNumber;
					$recorddata['label_amount'] = $label_amount;
					$recorddata['pic_number'] = isset($response['requestId']) ? $response['requestId'] : $response['CustomsNumber'];
					$recorddata['order_id'] = $orderId;
					$vparcel_data_repository = new DataAccess_VparcelShippingLabels($db, $settings);
		
					$labelId = $vparcel_data_repository->persist($recorddata);
					$labelUrl = $vparcel_data_repository->SaveShippingLabel($encodedLabelImage, $orderId, $labelId, $trackingNumber);
		
					$recorddata = array();
					$recorddata['id'] = $labelId;
					$recorddata['label_url'] =  $labelUrl;
					$labelId = $vparcel_data_repository->persist($recorddata);
					
					$db->reset();
					$db->assignStr('shipping_tracking_number', $trackingNumber);
					$db->assignStr('shipping_tracking_number_type', 'USPS');
					$db->update(DB_PREFIX.'orders', 'WHERE oid='.intval($orderId));
		
					$ret = array(
						'result' => 1,
						'error' => '',
						'trackingNumber' => $trackingNumber,
						'labelUrl' => $labelUrl
					);
				}
				else
				{
					if (isset($response['ErrorMessage']))
					{
						$ret = array(
							'result' => 0,
							'error' => $response['ErrorMessage'],
							'trackingNumber' => '',
							'labelUrl' => ''
						);
					}
				}
			}
			header('HTTP/1.0 '.$status);
			header('Content-Type: application/json');
			$this->renderJson($ret);
		}
		
		public function viewLabelAction()
		{
			$db = $this->db;
			$request = $this->getRequest();
			$postData = $request->request->all();
			$f = isset($postData['f']) ? $postData['f'] : '';
			if ($db->selectOne('SELECT * FROM '.DB_PREFIX.'vparcel_shippinglabels WHERE label_url="'.$db->escape($f).'"') && is_file($f))
			{
				header('Content-type: image/png');
				header('Expires: Mon, 1 Jan 2099 05:00:00 GMT');
				header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
				header('Cache-Control: no-store, no-cache, must-revalidate');
				header('Cache-Control: post-check=0, pre-check=0', false);
				header('Pragma: no-cache');
		
				header('Content-Length: '.filesize($f).' bytes');
				ob_clean();
				flush();
		
				readfile($f);
				die();
			}
			else
			{
				header('HTTP/1.0 404 Not Found');
				ob_clean();
				flush();
				die();
			}
		}

		protected function getShippingRepository()
		{
			if (is_null($this->shippingrepository))
			{
				$this->shippingrepository = new DataAccess_ShippingRepository($this->db);
			}

			return $this->shippingrepository;
		}

		protected function getShippingVipparcel($db, $settings, $shipments_data)
		{
			if (is_null($this->shipping_vipparcel))
			{
				$this->shipping_vipparcel = new Shipping_VIPparcel($db, $settings, $shipments_data);
			}

			return $this->shipping_vipparcel;
		}

		protected function getOrderRepository()
		{
			if (is_null($this->orderrepository))
			{
				$this->orderrepository = new DataAccess_OrderRepository($this->db, $this->settings);
			}

			return $this->orderrepository;
		}

		protected function getvparcelLabelsRepository()
		{
			if (is_null($this->vipparcelLabelsRepository))
			{
				$this->vipparcelLabelsRepository = new DataAccess_VparcelShippingLabels($this->db, $this->settings);
			}

			return $this->vipparcelLabelsRepository;
		}

		/**
		 * Check are company settings ready
		 */
		protected function checkCompanyAddressSettings()
		{
			$settings = $this->getSettings();

			if (
				$settings->get('ShippingOriginAddressLine1') == '' || $settings->get('ShippingOriginCity') == 'City' ||
				$settings->get('ShippingOriginState') == '0' || $settings->get('ShippingOriginState') == '' ||
				$settings->get('ShippingOriginZip') == '-' || $settings->get('ShippingOriginZip') == ''
			)
			{
				Admin_Flash::setMessage(
					'To create a label through VIPparcel, please make sure that you have filled in all required fields under <a href="admin.php?p=shipping&mode=settings">Shipping Origin Address</a>. '.
					'Please note: VIPparcel may not work with P.O. Box addresses',
					Admin_Flash::TYPE_ERROR
				);
			}
		}
	}
<?php

/**
 * Class Admin_Controller_Updates
 */
class Admin_Controller_Updates extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/updates/';

	// TODO: refactor / move updateClient into service

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$db = $this->getDb();
		$settings = $this->getSettings();

		$xmlParser = new XMLParser();
		$updateClient = new UpdateClient($db, $xmlParser);

		$fixes = $updateClient->checkForUpdates(updateClient::updateScriptUrl, LICENSE_NUMBER, $settings->get('AppVer'), $settings->get('AppEncoder'));

		$new_updates = false;

		if (is_array($fixes))
		{
			foreach ($fixes as &$fix)
			{
				if (!$fix["installed"])
				{
					$new_updates = true;
				}
				$fix['size'] = number_format($fix['size'] / 1024, 2);
			}
		}

		$adminView = $this->getView();

		$adminView->assign('fixes', $fixes);
		$adminView->assign('new_updates', $new_updates);
		$adminView->assign('helpTag', '8036');

		$adminView->assign('body', self::TEMPLATES_PATH.'index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update action
	 */
	public function step2Action()
	{
		$request = $this->getRequest();
		$db = $this->getDb();

		if ($request->isPost())
		{
			global $fixes, $fix_id;

			$xmlParser = new XMLParser();
			$updateClient = new UpdateClient($db, $xmlParser);
			$fixes = $updateClient->getFixesFromSession();
			$fix = $request->request->getArray('fix');

			if (count($fix) > 0)
			{
				foreach ($fix as $fix_id => $value)
				{
					if ($updateClient->setupFix($fix_id))
					{
						$updateClient->runCode();
						Admin_Flash::setMessage(trans('updates.update_installed', array('update_name' => $fixes[$fix_id]['name'])), Admin_Flash::TYPE_SUCCESS);
					}
					else
					{
						Admin_Flash::setMessage(trans('updates.update_install_error', array('update_name' => $fixes[$fix_id]['name'], 'err_message' => $updateClient->error)), Admin_Flash::TYPE_ERROR);
					}
				}
			}
			else
			{
				Admin_Flash::setMessage('updates.no_updates_selected', Admin_Flash::TYPE_ERROR);
			}
		}

		$this->redirect('update');
	}
}
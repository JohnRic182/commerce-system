<?php

/**
 * Class Admin_Controller_PaymentMethod
 */
class Admin_Controller_PaymentMethod extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'setup');
	protected $countriesRepository = null;


	/**
	 * List payment methods
	 */
	public function listAction()
	{
		$request = $this->getRequest();
		$repository = $this->getRepository();
		$adminView = $this->getView();
		$adminView->assign('paymentMethodsGroups', $repository->getPaymentMethodsGroups());
		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8002');
		$paymentMethods = $repository->getActivePaymentMethods(true);
		$temp = array();
		$stripeDisplayed = false;
		$stripeEnabled = false;
		$stripeProvisionData = $this->getSettings()->get('stripe_ProvisioningData');
		$stripeProvisionData = $stripeProvisionData == '' ? false : @unserialize($stripeProvisionData);
		$stripeActive = 'No';

		$paypalExpressDisplayed = false;
		$paypalExpressEnabled = false;
		$paypalExpressActive = 'No';

		$braintreeDisplayed = false;
		$braintreeEnabled = false;
		$braintreeActive = 'No';

		foreach ($paymentMethods as $paymentMethod)
		{
			if ($paymentMethod['id'] == 'stripe')
			{
				$stripeDisplayed = true;
				$stripeEnabled = $paymentMethod['active'] == 'Yes';
				$stripeActive = $paymentMethod['active'];
			}
			else if ($paymentMethod['id'] == 'paypalec')
			{
				$paypalExpressDisplayed = true;
				$paypalExpressEnabled = $paymentMethod['active'] == 'Yes';
				$paypalExpressActive = $paymentMethod['active'];
			}
			else if ($paymentMethod['id'] == 'braintree')
			{
				$braintreeDisplayed = true;
				$braintreeEnabled = $paymentMethod['active'] == 'Yes';
				$braintreeActive = $paymentMethod['active'];
			}
			else
			{
				$temp[] = $paymentMethod;
			}
		}

		$paymentMethods = $temp;


		$settings = $this->getSettings();
		$stripeConnect = new Payment_Stripe_Connect($settings);
		if ($stripeDisplayed && !$stripeConnect->isConnected() && $request->query->has('_refreshStripe'))
		{
			if ($request->query->get('_refreshStripe') == '2')
			{
				$stripeConnectService = new Admin_Service_StripeConnect($settings);
				$result = $stripeConnectService->finishStripeConnect(base64_decode($request->query->get('x')));
				if ($result === true)
				{
					$this->db->query('UPDATE '.DB_PREFIX.'payment_methods SET active = "Yes" WHERE id = "stripe"');
					$this->redirect('payment_methods');
					return;
				}
				if ($result !== true && $result !== false)
				{
					Admin_Flash::setMessage($result, Admin_Flash::TYPE_ERROR);
				}
			}
			else
			{
				$stripeConnectService = new Admin_Service_StripeConnect($settings);
				$result = $stripeConnectService->finishStripeConnect();
				if ($result !== true && $result !== false)
				{
					Admin_Flash::setMessage($result, Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView->assign('paymentMethods', $paymentMethods);
		$adminView->assign('stripeDisplayed', $stripeDisplayed);
		$adminView->assign('stripeEnabled', $stripeEnabled);
		$adminView->assign('stripeConnected', $stripeEnabled && $stripeConnect->isConnected());
		$adminView->assign('stripeConnectUrl', $stripeConnect->getConnectUrl());
		$adminView->assign('stripeActive', $stripeActive);
		$stipeActivateUrl = false;

		if ($stripeProvisionData && is_array($stripeProvisionData) && isset($stripeProvisionData['stripe_user_id']))
		{
			$clientId = 'ca_42DoeKDuxaeOegQ9Euk2CGjFGQ6kYGyo';
			if (defined('STRIPE_CONNECT_TEST_MODE') && STRIPE_CONNECT_TEST_MODE)
			{
				$clientId = 'ca_42DoLTDVWOKY7lznD3pEcjHC4GMPuGAO';
			}
			$licenseParts = explode('-', LICENSE_NUMBER);
			$stipeActivateUrl = 'https://connect.stripe.com/account/activate?client_id='.$clientId.'&user_id='.$stripeProvisionData['stripe_user_id'].'&state='.$licenseParts[1];
		}

		$adminView->assign('stripeActivateUrl', $stipeActivateUrl);

		$btController = new Admin_Controller_Braintree($this->getRequest(), $this->getDb());
		$countryRepository = $this->getCountryRepository();
		$companyCountry = $settings->get('CompanyCountry');
		$countryRec = $countryRepository->getCountryByIso2Code($companyCountry);
		$country = 'USA';
		if ($countryRec)
		{
			if (isset($countryRec['iso_a3']))
			{
				$country = $countryRec['iso_a3'];
			}
		}
		$email = $settings->get('CompanyEmail', '');
		$bt_authUrl = '';
		if (!$braintreeEnabled)
		{
			$btController->enableNonMerchantAPISettings();
			$bt_authUrlDAta = $btController->generateOAuthUrl($country, $email);
			if ($bt_authUrlDAta)
			{
				if (isset($bt_authUrlDAta['url']))
				{
					$bt_authUrl = $bt_authUrlDAta['url'];
				}
			}
		}

		$adminView->assign('braintreeConnectUrl', $bt_authUrl);
		$adminView->assign('btConnected', $braintreeEnabled);

		$adminView->assign('paypalExpressDisplayed', $paypalExpressDisplayed);
		$adminView->assign('paypalExpressEnabled', $paypalExpressEnabled);
		$adminView->assign('paypalExpressActive', $paypalExpressActive);

		$adminView->assign('noPaymentMethods', !$paypalExpressDisplayed && !$stripeDisplayed && count($paymentMethods) == 0);

		$adminView->assign('delete_nonce', Nonce::create('payment_method_delete'));

		$adminView->assign('body', 'templates/pages/payments/methods.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get activation form
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();
		$repository = $this->getRepository();

		$paymentMethodId = $request->query->get('payment_method_id', '');
		if ($paymentMethodId == 'custom')
		{

			$formBuilder = new core_Form_FormBuilder('custom');
			$formBuilder->add('title', 'text', array('label' => 'payment_methods.title', 'required' => true));
			$formBuilder->add('message_payment', 'textarea', array('label' => 'payment_methods.checkout_instructions'));
			$formBuilder->add('message_thankyou', 'textarea', array('label' => 'payment_methods.thank_you_instructions'));
			$optionYesNo = array();
			$optionYesNo [] = new core_Form_Option('1', 'Yes');
			$optionYesNo [] = new core_Form_Option('0', 'No');
			$formBuilder->add('enable_fraudservice', 'choice', array('label' => 'payment_methods.enable_fraudservice', 'value' => '1', 'options' => $optionYesNo));

			$adminView = $this->getView();
			$adminView->assign('form', $formBuilder->getForm());

			$this->renderJson(array('status' => 1, 'html' => $adminView->fetch('generic-form')));
		}
		else
		{
			$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);

			if ($paymentMethod != null && $repository->methodAvailable($paymentMethodId))
			{
				$editableFormBuilders = $this->getEditableFormBuilders($paymentMethod, $paymentMethodId);
				$adminView = $this->getView();

				if (count($editableFormBuilders) > 0)
				{
					/** @var core_Form_FormBuilder $formBuilder */
					$formBuilder = $editableFormBuilders[0];

					$adminView->assign('form', $formBuilder->getForm());
				}
				else
				{
					$adminView->assign('form', false);
				}

				$this->renderJson(array('status' => 1, 'html' => $adminView->fetch('generic-form')));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'message' => 'Cannot find payment method by provided ID'));
			}
		}
	}

	/**
	 * Update custom payment method
	 */
	public function updateCustomPaymentAction()
	{
		$request = $this->getRequest();
		$repository = $this->getRepository();

		$paymentMethodId = $request->get('payment_method_id', '');
		if ($paymentMethodId == 'custom')
		{
			$paymentMethod = array(
				'pid' => 0,
				'name' => '',
				'title' => '',
				'active' => true,
				'priority' => '',
				'message_payment' => '',
				'message_thankyou' => '',
				'enable_fraudservice' => 1,
			);
		}
		else
		{
			$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);
		}

		if ($paymentMethod != null && $repository->methodAvailable($paymentMethodId))
		{
			if ($request->isPost())
			{
				$title = trim($request->request->get('title', ''));
				$priority =  $request->request->get('priority', '5');
				$messagePayment = $request->request->get('message_payment', '');
				$messageThankYou = $request->request->get('message_thankyou', '');
				$enableFraudService = $request->request->get('enable_fraudservice', '0');

				$success = false;
				$errors = array();
				if ($title == '')
				{
					$errors['title'] = array(trans('payment_methods.title_required'));
				}
				else
				{
					if ($paymentMethod['pid'] == 0)
					{
						$paymentMethod['name'] = str_replace(' ', '_', strtolower($title));
					}
					$repository->saveCustomMethod($paymentMethod['pid'], $paymentMethod['name'], $title, $messagePayment, $messageThankYou, $paymentMethod['active'], $priority, $enableFraudService);
					Admin_Flash::setMessage('common.success');

					$success = true;
				}

				$this->renderJson(array('status' => $success ? 1 : 0, 'errors' => $errors));
			}
			else
			{
				$adminView = $this->getView();

				$formBuilder = new core_Form_FormBuilder('custom');
				$formBuilder->add('title', 'text', array('label' => 'payment_methods.title', 'required' => true, 'value' => $paymentMethod['title']));

				$optionPriorities = array();
				for ($i = 0; $i <=10; $i++)
				{
					$optionPriorities[] = new core_Form_Option($i, $i);
				}
				$formBuilder->add('priority', 'choice', array('label' => 'payment_methods.priority', 'value' => $paymentMethod['priority'], 'options' => $optionPriorities));
				$optionYesNo = array();
				$optionYesNo [] = new core_Form_Option('1', 'Yes');
				$optionYesNo [] = new core_Form_Option('0', 'No');
				$formBuilder->add('enable_fraudservice', 'choice', array('label' => 'payment_methods.enable_fraudservice', 'value' => $paymentMethod['enable_fraudservice'], 'options' => $optionYesNo));

				$formBuilder->add('message_payment', 'textarea', array('label' => 'payment_methods.checkout_instructions', 'value' => $paymentMethod['message_payment']));
				$formBuilder->add('message_thankyou', 'textarea', array('label' => 'payment_methods.thank_you_instructions', 'value' => $paymentMethod['message_thankyou']));

				$adminView->assign('form', $formBuilder->getForm());
				$this->renderJson(array('status' => 1, 'html' => $adminView->fetch('generic-form')));
			}

			$this->checkArePaymentProfilesAvaialble();
		}
		else
		{
			$this->renderJson(array('status' => 0, 'message' => 'Cannot find payment method by provided ID'));
		}
	}

	/**
	 * Activate payment method
	 */
	public function activateAction()
	{
		$repository = $this->getRepository();

		$db = $this->getDb();
		$request = $this->getRequest();

		$paymentMethodId = $request->get('payment_method_id', '');
		$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);

		if ($paymentMethod != null)
		{
			$paypalEnabledPre = Payment_PayPal_PayPalApi::isPayPalEnabled();

			$db->query("UPDATE ".DB_PREFIX."payment_methods SET active='Yes' WHERE id='".$db->escape($paymentMethodId)."'");

			$settingsRepository = DataAccess_SettingsRepository::getInstance();
			if ($paymentMethodId == 'bongocheckout')
			{
				$settingsRepository->save(array('ShippingInternational' => 'YES'));
				//update payment_methods table
				$form = $request->request->get('form');
				$db->query("UPDATE ".DB_PREFIX."payment_methods SET url_to_gateway='".$form['bongocheckout_Gateway_Url']."' WHERE id='".$db->escape($paymentMethodId)."'");
			}
			else if ($paymentMethodId == 'firstdata' && $this->isRecurringBillingEnabled())
			{
				$form = $request->request->get('form');
				$form['firstdata_Payment_Profiles'] = 'Yes';
				$request->request->set('form', $form);
			}
			else
			{
				if ($paymentMethodId == 'braintree')
				{
					$settingsRepository->save(array('braintree_oauth_accessToken'  => '',
													'braintree_oauth_expiresAt'    => '',
													'braintree_oauth_clientid'     => '',
													'braintree_oauth_clientsecret' => ''));
				}
			}

			$paymentMethod["active"] = "Yes";

			$paypalEnabled = Payment_PayPal_PayPalApi::isPayPalEnabled();
			if (!$paypalEnabledPre && $paypalEnabled)
			{
				// Activate financing banner widgets
				$db->query("UPDATE ".DB_PREFIX."layout_elements SET is_active = 1 WHERE name LIKE 'element_panels_panel-paypalbml-%'");
			}
			Admin_Log::log('Payment method '.$paymentMethodId.' has been activated', Admin_Log::LEVEL_IMPORTANT);

			$this->saveSettings($paymentMethod);

			$this->checkArePaymentProfilesAvaialble();

			/**
			 * Reread with changes
			 */
			$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);
			$paymentMethodObject = $this->getPaymentMethodObject($paymentMethod);
			$methodConnected = $paymentMethodObject->testConnection();

			if ($methodConnected === true)
			{
				Admin_Flash::setMessage('Connection information successful');
			}
			else if ($methodConnected === false)
			{
				Admin_Flash::setMessage('Connection information not correct', Admin_Flash::TYPE_ERROR);
			}

			if ($request->request->get('fromQS', false))
			{
				$this->redirect('shipping', array('qsg_set_step' => 'shipping'));
			}
			else
			{
				Admin_Flash::setMessage('common.success');

				$this->redirect('payment_methods');
			}
		}
		else
		{
			//TODO: What to do here?
			error_log('Payment method not found: '.$paymentMethodId);
		}
	}

	/**
	 *
	 */
	public function autoBoardStripeAction()
	{
		$repository = $this->getRepository();

		$db = $this->getDb();
		$request = $this->getRequest();

		$paymentMethodId = 'stripe';

		$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);

		$error = false;
		if ($paymentMethod != null && $paymentMethod['active'] != 'Yes')
		{
			$email = $request->request->get('stripe_email');

			global $admin;
			$admin['email'] = $email;
			$provision = new Payment_Stripe_Provision($this->getSettings());
			$result = $provision->provision($admin);

			if ($result)
			{
				if (isset($result['stripe_publishable_key']) && isset($result['access_token']))
				{
					$db->query("UPDATE ".DB_PREFIX."payment_methods SET active='Yes' WHERE id='".$db->escape($paymentMethodId)."'");

					$paymentMethod["active"] = "Yes";

					$settings = $this->getSettings();
					$settings->save(array(
						'stripe_ApiPublishableKey' => $result['stripe_publishable_key'],
						'stripe_ApiSecretKey' => $result['access_token'],
						'stripe_ProvisioningData' => serialize($result),
					));

					Admin_Log::log('Payment method '.$paymentMethodId.' has been activated', Admin_Log::LEVEL_IMPORTANT);
				}
				else if (isset($result['error']['message']))
				{
					$error = $result['error']['message'];
					if ($error == 'An account with this email address already exists')
					{
						$error = 'stripe.account_exists';
					}
				}
			}
			else
			{
				//TODO: Unable to connect to provisioning!?!
			}
		}

		$this->checkArePaymentProfilesAvaialble();

		if ($error)
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => $error,
			));
		}
		else
		{
			$this->renderJson(array(
				'status' => 1,
				'activeCount' => count($repository->getActivePaymentMethods()),
			));
			return;
		}
	}

	/**
	 *
	 */
	public function autoBoardAction()
	{
		$repository = $this->getRepository();

		$db = $this->getDb();
		$request = $this->getRequest();

		$paymentMethods = $request->request->getArray('payment_method');

		$paymentMethodsStatus = array(
			'paypalec' => isset($paymentMethods['paypalec']),
			'stripe' => isset($paymentMethods['stripe']),
		);

		$error = false;
		foreach ($paymentMethodsStatus as $paymentMethodId => $active)
		{
			$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);

			if ($paymentMethod != null)
			{
				if (!$active)
				{
					$db->query("UPDATE ".DB_PREFIX."payment_methods SET active='Disabled' WHERE id='".$db->escape($paymentMethodId)."'");

					$paymentMethod["active"] = "No";

					if ($paymentMethodId == 'paypalec')
					{
						$db->query("UPDATE ".DB_PREFIX."layout_elements SET is_active = 0 WHERE name LIKE 'element_panels_panel-paypalbml-%'");
					}

					Admin_Log::log('Payment method '.$paymentMethodId.' settings have been changed', Admin_Log::LEVEL_IMPORTANT);
				}
				else
				{
					global $admin;

					if ($paymentMethodId == 'stripe' && $paymentMethod['active'] != 'Yes')
					{
						$provision = new Payment_Stripe_Provision($this->getSettings());
						$result = $provision->provision($admin);

						if ($result)
						{
							if (isset($result['stripe_publishable_key']) && isset($result['access_token']))
							{
								$db->query("UPDATE ".DB_PREFIX."payment_methods SET active='Yes' WHERE id='".$db->escape($paymentMethodId)."'");

								$paymentMethod["active"] = "Yes";

								$settings = $this->getSettings();
								$settings->save(array(
									'stripe_ApiPublishableKey' => $result['stripe_publishable_key'],
									'stripe_ApiSecretKey' => $result['access_token'],
									'stripe_ProvisioningData' => serialize($result),
								));

								Admin_Log::log('Payment method '.$paymentMethodId.' has been activated', Admin_Log::LEVEL_IMPORTANT);
							}
							else if (isset($result['error']['message']))
							{
								$error = $result['error']['message'];
								if ($error == 'An account with this email address already exists')
								{
									$error = 'stripe.account_exists';
								}
							}
						}
						else
						{
							//TODO: Unable to connect to provisioning!?!
						}
					}
					else if ($paymentMethodId == 'paypalec' && $paymentMethod['active'] != 'Yes')
					{
						$db->query("UPDATE ".DB_PREFIX."payment_methods SET active='Yes' WHERE id='".$db->escape($paymentMethodId)."'");

						$paymentMethod["active"] = "Yes";

						$settings = $this->getSettings();
						$settings->save(array('paypalec_Email' => $admin['email']));

						// Activate financing banner widgets
						$db->query("UPDATE ".DB_PREFIX."layout_elements SET is_active = 1 WHERE name LIKE 'element_panels_panel-paypalbml-%'");
						Admin_Log::log('Payment method '.$paymentMethodId.' has been activated', Admin_Log::LEVEL_IMPORTANT);
					}
				}
			}
		}

		$this->checkArePaymentProfilesAvaialble();

		if ($error)
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => $error,
			));
		}
		else
		{
			$this->renderJson(array(
				'status' => 1,
				'activeCount' => count($repository->getActivePaymentMethods()),
			));
			return;
		}
	}

	/**
	 * @return bool
	 */
	private function isRecurringBillingEnabled()
	{
		$settings = $this->getSettings();
		return defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING && $settings->get('RecurringBillingEnabled') == '1';
	}

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		$repository = $this->getRepository();
		$adminView = $this->getView();

		$paymentMethodId = $request->query->get('payment_method_id', '');
		$paymentMethod = $repository->getPaymentMethodById($paymentMethodId, true);

		$paymentMethodObject = $this->getPaymentMethodObject($paymentMethod);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8003');

		if ($paymentMethod != null && $paymentMethodObject != null)
		{
			if ($repository->methodAvailable($paymentMethodId))
			{
				$formBuilder = new core_Form_FormBuilder('standard');
				$group = new core_Form_FormBuilder('standard_group', array('label' => 'payment_methods.presentation_settings', 'collapsible' => true));
				$formBuilder->add($group);
				$group->add('payment_method[title]', 'text', array('label' => 'payment_methods.payment_method_title', 'placeholder' => 'payment_methods.payment_method_title', 'required' => true, 'value' => $paymentMethod['title']));
				$optionPriorities = array();
				for ($i = 0; $i <=10; $i++)
				{
					$optionPriorities[] = new core_Form_Option($i, $i);
				}
				$group->add('payment_method[priority]', 'choice', array('label' => 'payment_methods.priority', 'value' => $paymentMethod['priority'], 'options'=> $optionPriorities));
				$group->add('payment_method[message_payment]', 'text', array('label' => 'payment_methods.checkout_instructions', 'value' => $paymentMethod['message_payment']));
				$group->add('payment_method[message_thankyou]', 'text', array('label' => 'payment_methods.thank_you_instructions', 'value' => $paymentMethod['message_thankyou']));

				$standardGatewaryForm = $formBuilder->getForm();

				if ($request->isPost())
				{
					if (!Nonce::verify($request->request->get('nonce'), 'payment_method_update'))
					{
						Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
					}
					else
					{
						$presentation_settings = $request->request->get('payment_method');
						$formData['payment_method[title]'] = $presentation_settings['title'];
						$errors = array();
						if (isset($presentation_settings['title']) && trim($presentation_settings['title']) == '')
						{
							$errors['payment_method[title]'] = array('Payment Method Title is required');
						}

						$hasErrors = count($errors) > 0 ? true : false;
						if (!$hasErrors)
						{
							$this->saveSettings($paymentMethod);
						}
						else
						{
							$standardGatewaryForm->bind($formData);
							$standardGatewaryForm->bindErrors($errors);
						}

						/**
						 * Reread with changes
						 */
						$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);

						$paymentMethodObject = $this->getPaymentMethodObject($paymentMethod);
						$methodConnected = $paymentMethodObject->testConnection();

						if ($methodConnected === true)
						{
							Admin_Flash::setMessage('Connection information successful');
							$this->redirect('payment_method', array('payment_method_id' => $paymentMethodId));
							return;
						}
						else if ($methodConnected === false)
						{
							Admin_Flash::setMessage('Connection information not correct', Admin_Flash::TYPE_ERROR);
						}
						if (!$hasErrors)
						{
							Admin_Flash::setMessage('common.success');
						}
					}
					if (!$hasErrors)
					{
						$this->redirect('payment_methods');
						return;
					}
				}

				$formBuilders = $this->getEditableFormBuilders($paymentMethod, $paymentMethodId);

				$formBuilder = new core_Form_FormBuilder('general');
				foreach ($formBuilders as $idx => $editableFormBuilder)
				{
					if ($idx < 2)
					{
						if ($idx > 0)
						{
							/** @var core_Form_FormBuilder $editableFormBuilder */
							$data = $editableFormBuilder->getData();
							if ($data == null)
							{
								$data = array();
							}

							$editableFormBuilder->setData(array_merge($data, array('collapsible' => true)));
						}

						$formBuilder->add($editableFormBuilder);
					}
				}

				$adminView = $this->getView();

				$adminView->assign('form', $formBuilder->getForm());

				if (array_key_exists(2, $formBuilders))
				{
					/** @var core_Form_FormBuilder $customFormBuilder */
					$customFormBuilder = $formBuilders[2];

					$adminView->assign('customForm', $customFormBuilder->getForm());
				}
				else
				{
					$adminView->assign('customForm', false);
				}

				$adminView->assign('standardGatewaryForm', $standardGatewaryForm);

				$adminView->assign('paymentMethodId', $paymentMethod['id']);
				$adminView->assign('paymentMethod', $paymentMethod);
				$adminView->assign('braintreeConnectUrl', '');
				$adminView->assign('btConnected', false);

				$settings = $this->getSettings();
				/**
				 * Payment Server Settings
				 */
				$ccs = !is_null($paymentMethodObject) && $paymentMethodObject->support_ccs && $settings->get('SecurityCCSActive') == '1' && (AccessManager::checkAccess('CCS'));

				//$advancedOtherFormBuilder = null;
				//if ($paymentMethod['type'] != 'custom' || $ccs)
				//{
					$otherSettingsFormBuilder = new core_Form_FormBuilder('other-settings');
					$advancedOtherFormBuilder = new core_Form_FormBuilder('advanced-other', array('label' => 'payment_methods.other_settings', 'collapsible' => true));
					$otherSettingsFormBuilder->add($advancedOtherFormBuilder);

					// CCS
					if ($ccs)
					{
						$ccsOptions = array(new core_Form_Option('1', 'Yes'), new core_Form_Option('0', 'No'));

						$advancedOtherFormBuilder->add('payment_method[enable_ccs]', 'choice', array(
							'label' => 'payment_methods.enable_ccs',
							'options' => $ccsOptions, 'value' => $paymentMethod['enable_ccs'],
							'attr' => array('class' => 'short'),
							'note' => 'payment_methods.notes.enable_ccs'
						));
					}

					// URL
					if ($paymentMethod['type'] != 'custom' && $paymentMethodObject->allow_change_gateway_url)
					{
						$advancedOtherFormBuilder->add('payment_method[url_to_gateway]', 'text',
							array(
								'label' => 'payment_methods.url_to_gateway', 'value' => $paymentMethod["url_to_gateway"],
								'note' => 'payment_methods.notes.url_to_gateway',
							)
						);
					}

					// Certificate
					if (!is_null($paymentMethodObject) && is_object($paymentMethodObject) && $paymentMethodObject->is_certificate)
					{
						$cfile = 'content/engine/payment/'.$paymentMethodObject->certificate_path;
						$cdir = dirname($cfile);

						if (!is_dir($cdir) || !is_writable($cdir))
						{
							$advancedOtherFormBuilder->add('certificate_file_error', 'static',
								array(
									'label' => ($paymentMethodObject->certificate_optional ? 'payment_methods.certificate_file_optional' : 'payment_methods.certificate_file'),
									'value' => trans('payment_methods.certificate_dir_not_writable', array('cdir' => $cdir)),
								)
							);
						}
						elseif (file_exists($cfile) && !is_writable($cfile))
						{
							$advancedOtherFormBuilder->add('certificate_file_error', 'static',
								array(
									'label' => 'Certificate File',
									'value' => trans('payment_methods.certificate_file_not_writable', array('cfile' => $cfile)),
								)
							);
						}
						else
						{
							$note = $paymentMethodObject->certificate_optional ? 'payment_methods.notes.certificate_file_optional' : 'payment_methods.notes.certificate_file';

							$advancedOtherFormBuilder->add('certificate_file', 'file',
								array(
									'label' => 'payment_methods.certificate_file',
									'note' => $note
								)
							);
							$advancedOtherFormBuilder->add('certificate_path', 'hidden', array('value' => $cfile));
						}

						if (file_exists($cfile))
						{
							$advancedOtherFormBuilder->add('certificate_file_current', 'static',
								array(
									'label' => 'payment_methods.current_certificate',
									'value' => $cfile,
									'note' => trans('payment_methods.notes.current_certificate', array('file_size' => filesize($cfile), 'change_time' => date("F j, Y, g:i a", filemtime($cfile)))),
								)
							);
						}
					}
//				}

				if ($advancedOtherFormBuilder != null && $advancedOtherFormBuilder->childCount() > 0)
				{
					$adminView->assign('otherSettingsForm', $otherSettingsFormBuilder->getForm());
				}
				else
				{
					$adminView->assign('otherSettingsForm', false);
				}

				$adminView->assign('supportsCC', $paymentMethod["type"] == "cc" && !is_null($paymentMethodObject) && $paymentMethodObject->support_accepted_cc);

				$adminView->assign('nonce', Nonce::create('payment_method_update'));

				$adminView->assign('body', 'templates/pages/payments/edit.html');
				$adminView->render('layouts/default');
			}
			else
			{
				$adminView->assign('body', 'templates/pages/payments/not-allowed.html');
				$adminView->render('layouts/default');
			}
		}
		else
		{
			//TODO:Not found message
			$this->redirect('payment_methods');
		}
	}

	/**
	 * @param $paymentMethod
	 */
	protected function saveSettings($paymentMethod)
	{
		$repository = $this->getRepository();
		$settingsRepository = $this->getSettings();

		$request = $this->getRequest();

		$paymentMethodId = $paymentMethod['id'];

		$form = $request->get('form', array());
		if (!is_array($form)) $form = array();

		foreach ($paymentMethod['settings'] as $settingsOption)
		{
			$name = $settingsOption['name'];

			if (isset($form[$name]))
			{
				$value = $form[$name];
				$settingsRepository->save(array($name => $value));
			}
			else if (isset($_FILES[$name]) && is_uploaded_file($_FILES[$name]['tmp_name']))
			{
				//check image type
				$is = getimagesize($_FILES[$name]['tmp_name']);
				if ($is && in_array($is[2], array(IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_JPEG)))
				{
					$extensions = array(
						IMAGETYPE_GIF => 'gif',
						IMAGETYPE_PNG => 'png',
						IMAGETYPE_JPEG => 'jpg'
					);

					$destination = 'images/payment/'.escapeFileName($name).'.'.$extensions[$is[2]];

					@unlink('images/payment/'.escapeFileName($name).'.jpg');
					@unlink('images/payment/'.escapeFileName($name).'.gif');
					@unlink('images/payment/'.escapeFileName($name).'.png');

					move_uploaded_file($_FILES[$name]['tmp_name'], $destination);
					chmod($destination, FileUtils::getFilePermissionMode());

					$settingsRepository->save(array($name => $destination));
				}
			}
		}

		if (in_array($paymentMethodId, array('paypaladv', 'paypalpflink')) && isset($_FILES['paypalec_Logo']) && is_uploaded_file($_FILES['paypalec_Logo']['tmp_name']))
		{
			//check image type
			$is = getimagesize($_FILES['paypalec_Logo']['tmp_name']);
			if ($is && in_array($is[2], array(IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_JPEG)))
			{
				$extensions = array(
					IMAGETYPE_GIF => 'gif',
					IMAGETYPE_PNG => 'png',
					IMAGETYPE_JPEG => 'jpg'
				);

				$destination = 'images/payment/'.escapeFileName('paypalec_Logo').'.'.$extensions[$is[2]];

				@unlink('images/payment/'.escapeFileName('paypalec_Logo').'.jpg');
				@unlink('images/payment/'.escapeFileName('paypalec_Logo').'.gif');
				@unlink('images/payment/'.escapeFileName('paypalec_Logo').'.png');

				move_uploaded_file($_FILES['paypalec_Logo']['tmp_name'], $destination);
				chmod($destination, FileUtils::getFilePermissionMode());

				$settingsRepository->save(array('paypalec_Logo' => $destination));
			}
		}

		$paymentMethodData = $request->get('payment_method', array());
		if (is_array($paymentMethodData) && isset($paymentMethodData['title']))
		{
			if (!isset($paymentMethodData['enable_fraudservice']))
			{
				$paymentMethodData['enable_fraudservice'] = 0;
			}
			$repository->updatePaymentMethod($paymentMethodData, $paymentMethod['pid']);
		}

		/**
		 * Certificate file
		 */
		if (array_key_exists("certificate_file", $_FILES) && file_exists($_FILES["certificate_file"]["tmp_name"]))
		{
			$certificate_path = trim($request->get('certificate_path'));

			if ($certificate_path != '' && is_writable($certificate_path))
			{
				copy($_FILES["certificate_file"]["tmp_name"], $certificate_path);
			}
		}

		$paymentAcceptedCCData = $request->request->get('payment_accepted_cc', array());
		if (is_array($paymentMethod['supported_cards']))
		{
			//foreach ($paymentAcceptedCCData as $ccid => $ccData)
			//{
			//	$repository->updateAcceptedCC($paymentMethod['pid'], $ccid, $ccData);
			//}
			foreach ($paymentMethod['supported_cards'] as $supportedCard)
			{
				$enabled = $supportedCard['enable'] == 'Yes';
				$cvv2Enabled = $supportedCard['enable_cvv2'] == 'Yes';
				$changed = false;

				if (isset($paymentAcceptedCCData[$supportedCard['ccid']]))
				{
					$newEnabled = isset($paymentAcceptedCCData[$supportedCard['ccid']]['enable']) && $paymentAcceptedCCData[$supportedCard['ccid']]['enable'] == 'Yes';
					$newCvv2Enabled = isset($paymentAcceptedCCData[$supportedCard['ccid']]['enable_cvv2']) && $paymentAcceptedCCData[$supportedCard['ccid']]['enable_cvv2'] == 'Yes';
					if ($newEnabled != $enabled)
					{
						$enabled = $newEnabled;
						$changed = true;
					}
					if ($newCvv2Enabled != $cvv2Enabled)
					{
						$cvv2Enabled = $newCvv2Enabled;
						$changed = true;
					}
				}
				else if ($enabled || $cvv2Enabled)
				{
					$enabled = false;
					$cvv2Enabled = false;
					$changed = true;
				}

				if ($changed)
				{
					$repository->updateAcceptedCC($paymentMethod['pid'], $supportedCard['ccid'], $enabled, $cvv2Enabled);
				}
			}
		}

		Admin_Log::log('Payment method "'.$paymentMethodId.'" settings have been changed', Admin_Log::LEVEL_IMPORTANT);
	}

	/**
	 * Deactivate payment method
	 */
	public function deactivateAction()
	{
		$repository = $this->getRepository();
		$request = $this->getRequest();
		$paymentMethodId = $request->query->get('payment_method_id');

		if (!Nonce::verify($request->query->get('nonce'), 'payment_method_delete'))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('payment_methods');
			return;
		}

		$paymentMethod = $repository->getPaymentMethodById($paymentMethodId);

		if (!is_null($paymentMethod))
		{
			$paypalEnabledPre = Payment_PayPal_PayPalApi::isPayPalEnabled();

			if ($paymentMethod['type'] == 'custom')
			{
				$repository->deactivate($paymentMethodId);

				Admin_Flash::setMessage('common.success');

				Admin_Log::log('Payment method "'.$paymentMethodId.'" settings have been changed', Admin_Log::LEVEL_IMPORTANT);
			}
			else if ($paymentMethod['id'] == 'cart_ccs')
			{
				$repository->deactivate($paymentMethodId);

				$db = $this->getDb();
				$db->query('UPDATE '.DB_PREFIX.'settings SET value = "0" WHERE name = "SecurityCCSActive"');

				Admin_Flash::setMessage('common.success');

				Admin_Log::log('Payment method "'.$paymentMethodId.'" settings have been changed', Admin_Log::LEVEL_IMPORTANT);
			}
			else
			{
				$repository->deactivate($paymentMethodId);
				$paymentMethod["active"] = "Disable";

				Admin_Flash::setMessage('common.success');

				Admin_Log::log('Payment method "'.$paymentMethodId.'" settings have been changed', Admin_Log::LEVEL_IMPORTANT);

				$paypalEnabled = Payment_PayPal_PayPalApi::isPayPalEnabled();
				if ($paypalEnabledPre && !$paypalEnabled)
				{
					$db = $this->getDb();
					// Deactivate financing banner widgets
					$db->query("UPDATE ".DB_PREFIX."layout_elements SET is_active = 0 WHERE name LIKE 'element_panels_panel-paypalbml-%'");
				}

				if ($this->isRecurringBillingEnabled())
				{
					if ($repository->hasActiveRecurringProfiles($paymentMethod['pid']))
					{
						Admin_Flash::setMessage('payment_methods.payment_method_has_recurring_profiles', Admin_Flash::TYPE_INFO);
					}
				}
			}

			$this->checkArePaymentProfilesAvaialble();
		}

		$this->renderJson(array(
			'status' => 1
		));
	}

	/**
	 *
	 */
	protected function checkArePaymentProfilesAvaialble()
	{
		$db = $this->db;

		$paymentProfilesEnabled = false;

		$db->query("SELECT * FROM ".DB_PREFIX."payment_methods WHERE active='Yes'");

		while ($db->moveNext())
		{
			$paymentMethodClass = $db->col['class'];

			if (class_exists($paymentMethodClass))
			{
				$gw_temp = new $paymentMethodClass;

				if ($gw_temp->supportsPaymentProfiles())
				{
					$paymentProfilesEnabled = true;
					break;
				}
			}
		}

		$this->getSettings()->save(array('paymentProfilesEnabled' => $paymentProfilesEnabled ? 1 : 0));
	}

	/**
	 * @param $paymentMethod
	 *
	 * @return PAYMENT_PROCESSOR|null
	 */
	protected function getPaymentMethodObject($paymentMethod)
	{
		if ($paymentMethod == null) return null;

		$paymentProcessorClass = $paymentMethod['class'];

		if (class_exists($paymentProcessorClass)) return new $paymentProcessorClass();

		return null;
	}

	/**
	 * @return array
	 */
	protected function getEditableFormBuilders($paymentMethod, $paymentMethodId)
	{
		$groupId = 0;
		/**
		 * Generate settings form from settings table
		 */
		$editableFormBuilders = array();

		$groupLabels = array(
			0 => 'Account Details',
			1 => 'Advanced Settings',
		);

		foreach ($paymentMethod['settings'] as $settingsOption)
		{
			$groupId = $settingsOption['group_id'];

			if (!isset($editableFormBuilders[$groupId]))
			{
				$editableFormBuilders[$groupId] = new core_Form_FormBuilder('editable'.$groupId, array('label' => array_key_exists($groupId, $groupLabels) ? $groupLabels[$groupId] : 'Properties', 'class' => 'ic-payment', 'first' => $groupId==0));
			}

			/** @var core_Form_FormBuilder $groupFormBuilder */
			$groupFormBuilder = $editableFormBuilders[$groupId];

			switch ($settingsOption['input_type'])
			{
				case 'text' :
				{
					$groupFormBuilder->add('form['.$settingsOption['name'].']', 'text', array('label' => $settingsOption['caption'], 'required'=>false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'password' :
				{
					$groupFormBuilder->add('form['.$settingsOption['name'].']', 'password', array('label' => $settingsOption['caption'], 'required'=>false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'color' :
				{
					$groupFormBuilder->add('form['.$settingsOption['name'].']', 'color', array('label' => $settingsOption['caption'], 'required'=>false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'textarea' :
				{
					$groupFormBuilder->add('form['.$settingsOption['name'].']', 'textarea', array('label' => $settingsOption['caption'], 'required'=>false, 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'label' :
				{
					$groupFormBuilder->add('form['.$settingsOption['name'].']', 'static', array('label' => $settingsOption['caption'], 'value' => $settingsOption['value'], 'note' => $settingsOption['description']));
					break;
				}
				case 'hidden' :
				{
					$groupFormBuilder->add('form['.$settingsOption['name'].']', 'hidden', array('value' => $settingsOption['value']));
					break;
				}
				case 'select' :
				{
					$fieldOptionsArray = explode(',', $settingsOption['options']);
					$fieldOptions = array();

					foreach ($fieldOptionsArray as $fieldOption)
					{
						if (strpos($settingsOption['name'], 'Currency_Code') !== false)
						{
							$fieldOptions[] = new core_Form_Option(trim($fieldOption), $fieldOption);
						}
						else
						{
							$fieldOptions[] = new core_Form_Option(trim($fieldOption), ucwords(strtolower($fieldOption)));
						}
					}

					$groupFormBuilder->add('form['.$settingsOption['name'].']', 'choice', array('label' => $settingsOption['caption'], 'required'=>false, 'value' => $settingsOption['value'], 'options' => $fieldOptions, 'note' => $settingsOption['description']));
					break;
				}
				case 'image' :
				{
					$groupFormBuilder->add($settingsOption['name'], 'file', array('label' => $settingsOption['caption'], 'note' => $settingsOption['description']));
					if ($settingsOption['value'] != '')
					{
						$groupFormBuilder->add(
							'form[preview_'.$settingsOption['name'].']', 'imagepreview',
							array('label' => 'Current image', 'value' => $settingsOption['value'], 'note' => 'Click over image to see it real size')
						);
					}
					break;
				}
			}
		}
		/** @var core_Form_FormBuilder $groupFormBuilder */
		if (isset($editableFormBuilders[1]))
		{
			$inst = FraudService_Provider_FraudServiceProvider::getActiveProviderInstance($this->getDb(), $this->getSettings());
			if(!is_null($inst))
			{
				$groupFormBuilder = $editableFormBuilders[1];
				$groupFormBuilder->add('payment_method[enable_fraudservice]', 'checkbox', array('label' => 'payment_methods.enable_fraudservice', 'value' => '1','current_value' => $paymentMethod['enable_fraudservice']));
			}
			else
			{
				$groupFormBuilder = $editableFormBuilders[1];
				$groupFormBuilder->add('payment_method[enable_fraudservice]', 'hidden', array('label' => '', 'value' => $paymentMethod['enable_fraudservice']));
			}
		}

		if (in_array($paymentMethodId, array('paypaladv', 'paypalpflink')))
		{
			$db = $this->getDb();
			$db->query('SELECT * FROM '.DB_PREFIX.'settings WHERE name =\'paypalec_Logo\'');
			if ($settingsOption = $db->moveNext())
			{
				$groupFormBuilder = $editableFormBuilders[$groupId];
				$groupFormBuilder->add($settingsOption['name'], 'file', array('label' => $settingsOption['caption'], 'note' => $settingsOption['description']));
				if ($settingsOption['value'] != '')
				{
					$groupFormBuilder->add(
						'form[preview_'.$settingsOption['name'].']', 'imagepreview',
						array('label' => 'Current image', 'value' => $settingsOption['value'], 'note' => 'Click over image to see it real size')
					);
				}
			}
		}

		return $editableFormBuilders;
	}

	/**
	 * @return DataAccess_PaymentMethodRepository
	 */
	protected function getRepository()
	{
		return  new DataAccess_PaymentMethodRepository($this->getDb());
	}

	protected function getCountryRepository()
	{
		if ($this->countriesRepository == null)
		{
			$this->countriesRepository = new DataAccess_CountryRepository($this->getDb());
		}

		return $this->countriesRepository;
	}
}

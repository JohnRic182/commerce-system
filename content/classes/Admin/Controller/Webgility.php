<?php

class Admin_Controller_Webgility extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9015');

		$adminView->assign('body', 'templates/pages/webgility/index.html');
		$adminView->render('layouts/default');
	}
}
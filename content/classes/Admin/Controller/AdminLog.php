<?php
/**
 * Class Admin_Controller_AdminLog
 */
class Admin_Controller_AdminLog extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * Show administrators activity log list
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$request = $this->request;
		$session = $this->getSession();

		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null)
		{
			$searchParams = $session->get('adminLogsSearchParams', array());
		}
		else
		{
			$session->set('adminLogsSearchParams', $searchParams);
		}

		$from = array_key_exists('from', $searchParams) ? $searchParams['from'] : null;
		$from = is_array($from) ? $from : array();

		$from_y = intval(isset($from['year']) ? $from['year'] : date('Y'));
		$from_m = intval(isset($from['month']) ? $from['month'] : date('n'));
		$from_d = intval(isset($from['day']) ? $from['day'] : 1);

		$to = array_key_exists('to', $searchParams) ? $searchParams['to'] : null;
		$to = is_array($to) ? $to : array();

		$to_y = intval(isset($to['year']) ? $to['year'] : date('Y'));
		$to_m = intval(isset($to['month']) ? $to['month'] : date('n'));
		$to_d = intval(isset($to['day']) ? $to['day'] : date('t', mktime(1, 1, 1, $to_m, 1, $to_y)));

		$sort_by = $request->query->get('sort_by', 'date_desc');
		$sort_by = in_array($sort_by, array('date', 'date_desc', 'username', 'username_desc', 'ip', 'message')) ? $sort_by : 'date_desc';
		$username = trim(array_key_exists('username', $searchParams) ? $searchParams['username'] : null);
		$level = intval(array_key_exists('level', $searchParams) ? $searchParams['level'] : -1);

		$mysql_from = $from_y.'-'.$from_m.'-'.$from_d.' 00:00:01';
		$mysql_to = $to_y.'-'.$to_m.'-'.$to_d.' 00:00:01';

		$reportDate = date('F j, Y', mktime(1, 1, 1, $from_m, $from_d, $from_y))." - ".
			date('F j, Y', mktime(1, 1, 1, $to_m, $to_d, $to_y));

		// admin log
		$adminLogForm = new Admin_Form_AdminLogForm();
		$data = array('mysql_from' => $mysql_from,
					'mysql_to' => $mysql_to,
					'username' => $username,
					'level' => $level,
					'sort_by' => $sort_by);

		$adminView->assign('searchForm', $adminLogForm->getForm($data));
		$adminView->assign('copy_nonce', Nonce::create('product_copy'));

		$adminView->assign('report_date', $reportDate);

		$searchParams = array(
			'mysql_from' => $mysql_from,
			'mysql_to' => $mysql_to,
			'username' => $username,
			'level' => $level,
		);

		$repository = $this->getAdminLogRepository();

		$itemsCount = $repository->getCount($searchParams);
		$adminView->assign('itemsCount', $itemsCount);

		$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
		$adminView->assign('paginator', $paginator);

		$list = null;
		if ($itemsCount)
		{
			$list = $repository->getList($paginator->sqlOffset, $paginator->itemsPerPage, $sort_by, $searchParams);
		}

		$adminView->assign('logs', $list);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8031');

		$adminView->assign('body', 'templates/pages/admin/logs/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @var DataAccess_AdminLogRepository
	 */
	protected $repository;

	/**
	 * Get repository
	 *
	 * @return DataAccess_AdminLogRepository
	 */
	protected function getAdminLogRepository()
	{
		if ($this->repository === null)
		{
			$this->repository = new DataAccess_AdminLogRepository($this->getDb(), $this->getSettings());
		}

		return $this->repository;
	}
}
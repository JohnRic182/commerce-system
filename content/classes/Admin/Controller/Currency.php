<?php

/**
 * Class Admin_Controller_Currency
 */
class Admin_Controller_Currency extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/currency/';

	/** @var DataAccess_CurrencyRepository  */
	protected $currencyRepository = null;

	public $currenciesList = array(
		'USD'=>array('US dollar', 2, '$', ''),
		'EUR'=>array('Euro', 2, '&euro;', ''),
		'JPY'=>array('Japanese yen', 0, '&yen;', ''),
		'CYP'=>array('Cyprus pound', 2, '', '&pound;'),
		'CZK'=>array('Czech koruna', 2, '', 'Kr'),
		'DKK'=>array('Danish krone', 2, '', 'Kr'),
		'EEK'=>array('Estonian kroon', 2, '', 'Kr'),
		'GBP'=>array('Pound sterling', 2, '', '&pound;'),
		'HUF'=>array('Hungarian forint', 2, '', 'Fr'),
		'LTL'=>array('Lithuanian litas', 2, '', 'Li'),
		'LVL'=>array('Latvian lats', 2, '', 'LVL'),
		'MTL'=>array('Maltese lira', 2, '', 'MTL'),
		'PLN'=>array('Polish zloty', 2, '', 'Zl'),
		'SEK'=>array('Swedish krona', 2, '', 'Kr'),
		'SIT'=>array('Slovenian tolar', 2, '', 'SIT'),
		'SKK'=>array('Slovak koruna', 2, '', 'Kr'),
		'CHF'=>array('Swiss franc', 2, '', 'Fr'),
		'ISK'=>array('Icelandic krona', 2, '', 'Kr'),
		'NOK'=>array('Norwegian krone', 2, '', 'Kr'),
		'BGN'=>array('Bulgarian lev', 2, '', 'BGN'),
		'HRK'=>array('Croatian kuna', 2, '', 'HRK'),
		'RON'=>array('New Romanian leu', 2, '', 'RON'),
		'RUB'=>array('Russian rouble', 2, '', 'Rub'),
		'TRY'=>array('New Turkish lira', 2, '', '&#20A4;'),
		'AUD'=>array('Australian dollar', 2, '$', ''),
		'CAD'=>array('Canadian dollar', 2, '$', ''),
		'CNY'=>array('Chinese yuan renminbi', 2, '', '&#5143;'),
		'HKD'=>array('Hong Kong dollar', 2, '', '&#5713;'),
		'IDR'=>array('Indonesian rupiah', 2, '', '&#20A8;'),
		'KRW'=>array('South Korean won', 2, '', '&#20A9;'),
		'MYR'=>array('Malaysian ringgit', 2, '', 'MYR'),
		'NZD'=>array('New Zealand dollar', 2, '', ''),
		'PHP'=>array('Philippine peso', 2, '', '&#20B1;'),
		'SGD'=>array('Singapore dollar', 2, '$', ''),
		'THB'=>array('Thai baht', 2, '', '&#3647;'),
		'ZAR'=>array('South African rand', 2, '', 'R')
	);

	protected $menuItem = array('primary' => 'settings', 'secondary' => 'setup');

	/**
	 * Add new currency
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$currencyRepository = $this->getCurrencyRepository();

		$defaults = $currencyRepository->getDefaults($mode);

		$currencyType = $request->get('currency_type');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if ($formData['mode'] == 'add' && $currencyType == 'default')
			{
				$code = $formData['currency_list'];
				$formData['code'] = $code;
				$formData['title'] = $this->currenciesList[$code][0];
				$formData['decimal_places'] = $this->currenciesList[$code][1];
				$formData['symbol_left'] = $this->currenciesList[$code][2];
				$formData['symbol_right'] = $this->currenciesList[$code][3];
			}

			if (!Nonce::verify($formData['nonce'], 'currency_add', false))
			{
				$this->renderJson(array(
						'status' => 0,
						'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $currencyRepository->getValidationErrors($formData, $mode)) == false)
				{
					$currencyRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					
					$this->renderJson(array(
							'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
							'status' => 0,
							'errors' => $validationErrors,
					));
				}
			}
		}
	}

	/**
	 * List Currencies
	 */
	public function listAction()
	{
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$currencyRepository = $this->getCurrencyRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'title');

		$itemsCount = $currencyRepository->getCount();
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('currencies', $currencyRepository->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy));
		}
		else
		{
			$adminView->assign('currencies', null);
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8010');

		$adminView->assign('currencyCustomForm', $this->getCurrencyCustomForm());
		$adminView->assign('currencyDefaultForm', $this->getCurrencyDefaultForm());

		$adminView->assign('addNonce', Nonce::create('currency_add'));
		$adminView->assign('updateNonce', Nonce::create('currency_update'));

		$adminView->assign('delete_nonce', Nonce::create('currencies_delete'));

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * get Currency Custom Form
	 * @return core_Form_Form
	 */
	protected function getCurrencyCustomForm()
	{
		$mode = self::MODE_ADD;

		$currencyRepository = $this->getCurrencyRepository();

		/** @var Admin_Form_CurrencyForm */
		$currencyForm = new Admin_Form_CurrencyForm();

		$defaults = $currencyRepository->getDefaults($mode);

		return $currencyForm->getForm($defaults, $mode, 'custom');
	}

	/**
	 * get Currency Default Form
	 * @return core_Form_Form
	 */
	protected function getCurrencyDefaultForm()
	{
		$mode = self::MODE_ADD;

		$currencyRepository = $this->getCurrencyRepository();

		/** @var Admin_Form_CurrencyForm */
		$currencyForm = new Admin_Form_CurrencyForm();

		$defaults = $currencyRepository->getDefaults($mode);

		$defaults['default_currency_list'] = $this->currenciesList;

		$existingCodes =  $currencyRepository->getExistsCurrenciesCodes();

		if ($existingCodes)
		{
			foreach ($existingCodes as $v)
			{
				$code  = $v['code'];
				if (isset($defaults['default_currency_list'][$code]))
				{
					unset($defaults['default_currency_list'][$code]);
				}
			}
		}

		return $currencyForm->getForm($defaults, $mode, 'default');
	}

	/**
	 * Update currency
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;
		$session = $this->getSession();
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TextPageRepository */
		$currencyRepository = $this->getCurrencyRepository();

		$id = intval($request->get('id'));

		$currency = $currencyRepository->getById($id);

		if (!$currency)
		{
			$this->renderJson(array(
					'status' => 0,
					'message' => trans('currencies.not_found'),
			));
		}

		/** @var Admin_Form_TextPageForm */
		$currencyForm = new Admin_Form_CurrencyForm();

		/** @var core_Form_Form $form */
		$form = $currencyForm->getForm($currency, $mode, 'custom', $id);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$defaults = $currencyRepository->getDefaults($mode);
			$formData = array_merge($defaults, $request->request->all());

		if (!Nonce::verify($formData['nonce'], 'currency_update', false))
			{
				$this->renderJson(array(
						'status' => 0,
						'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $currencyRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$currencyRepository->persist($formData);
					
					// update admin currency session data
					$currencies = new Currencies($this->db);
					$session->set('admin_currency',$currencies->getDefaultCurrency());
					
					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
							'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
							'status' => 0,
							'errors' => $validationErrors,
					));
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('id', $id);
		$adminView->assign('title', $currency['code']);

		$adminView->assign('currencyForm', $form);

		$adminView->assign('updateNonce', Nonce::create('currency_update'));

		$html = $adminView->fetch('pages/currency/modal-edit');

		$this->renderJson(array('status' => 1, 'html' => $html));
	}
	
	/**
	 * Delete currencies
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('currencies');
			return;
		}

		/** @var DataAccess_TextPageRepository */
		$currencyRepository = $this->getCurrencyRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('currencies.not_found', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$currencyRepository->delete(array($id));

				Admin_Flash::setMessage('common.success');
				$this->redirect('currencies');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('currencies.no_currency_selected', Admin_Flash::TYPE_ERROR);
				$this->redirect('currencies');
			}
			else
			{
				$currencyRepository->delete($ids);
				Admin_Flash::setMessage('common.success');
				$this->redirect('currencies');
			}
		}
		else if ($deleteMode == 'search')
		{
			$currencyRepository->deleteBySearchParams();
			Admin_Flash::setMessage('common.success');
			$this->redirect('currencies');
		}
	}

	/**
	 * Update exchange rates
	 */
	public function updateExchangeRatesAction()
	{
		// TODO: remove this dependency - move required functionality into classes/Admin/Service/CurrencyService
		$currencies = new Currencies($this->db);

		$request = $this->getRequest();
		$ids = $request->get('ids');

		// TODO: check what is going on here - updateExchangeRates does not have parameters
		$currencies->updateExchangeRates($ids);

		Admin_Flash::setMessage('common.success');
		$this->redirect('currencies');
	}

	/**
	 * Set default currency action
	 */
	public function setDefaultCurrencyAction()
	{
		$request = $this->getRequest();

		$currencyRepository = $this->getCurrencyRepository();
		$currencyRepository->setDefaultById(intval($request->get('id')));

		// TODO: remove this dependency - move required functionality into classes/Admin/Service/CurrencyService

		$currencies = new Currencies($this->db);
		$currencies->updateExchangeRates();
		$currenciesList = $currencies->getCurrencies();
		$this->renderJson($currenciesList);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CurrencyRepository
	 */
	protected function getCurrencyRepository()
	{
		if (is_null($this->currencyRepository))
		{
			$this->currencyRepository = new DataAccess_CurrencyRepository($this->getDb());
		}

		return $this->currencyRepository;
	}
}
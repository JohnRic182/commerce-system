<?php

class Admin_Controller_iDevAffiliate extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'AffiliateAreaURL',
			'AffiliateLinkText',
			'AffiliateVarAmount',
			'AffiliateVarOrderId',
		);

		$settingsData = $settings->getByKeys($settingsVars);

		$iDevAffiliateForm = new Admin_Form_iDevAffiliateForm();
		$form = $iDevAffiliateForm->getForm($settingsData);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'idev'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['AffiliateAreaURL']) || trim($settingsData['AffiliateAreaURL']) == '')
			{
				$errors['AffiliateAreaURL'] = 'iDevAffiliate Installation URL is required';
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'idev'));
			}
			else
			{
				$form->bind($settingsData);
				$form->bindErrors($errors);
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9004');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/idev/index.html');
		$adminView->render('layouts/default');
	}

	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('AffiliateAreaURL', 'AffiliateActive');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = array('AffiliateAreaURL' => $request->request->get('AffiliateAreaURL', ''));

			if (trim($settingsData['AffiliateAreaURL']) != '')
			{
				$settingsData['AffiliateActive'] = 'YES';
				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('idev');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array(
					'status' => 0,
					'errors' => array(
						array('field' => '.field-AffiliateAreaURL', 'message' => 'iDevAffiliate Installation URL is required'),
					),
				));
			}
		}

		$data = $settings->getByKeys($settingsVars);

		$iDevAffiliateForm = new Admin_Form_iDevAffiliateForm();

		$form = $iDevAffiliateForm->getIDevAffiliateActivateForm($data);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	public function deactivateAction()
	{
		$db = $this->getDb();

		$db->query('UPDATE '.DB_PREFIX.'settings SET value = "NO" WHERE name = "AffiliateActive"');
	}

//	public function activateAction()
//	{
//		$db = $this->getDb();
//
//		$db->query('UPDATE '.DB_PREFIX.'settings SET value = "YES" WHERE name = "AffiliateActive"');
//	}
}
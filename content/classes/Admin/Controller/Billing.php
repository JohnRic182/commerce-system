<?php

/**
 * Class Admin_Controller_Billing
 */
class Admin_Controller_Billing extends Admin_Controller
{
	const ORDER_ID_SESSION_VAR = 'billingApiOrderSecurityId';
	const ORDER_PLACE_RESULT_SESSION_VAR = 'placeOrderResult';

	protected $currentAdminId = null;
	protected $billingService = null;
	protected $supportService = null;
	protected $countriesRepository = null;
	protected $adminRepository = null;

	protected $menuItemSiteStats = array('primary' => 'billing', 'secondary' => 'site-stats');
	protected $menuItemProfile = array('primary' => 'billing', 'secondary' => 'profile');
	protected $menuItemMessageCenter = array('primary' => 'billing', 'secondary' => 'message-center');

	protected $billingProfileDefaults = array(
		'firstName' => '', 'lastName' => '', 'company' => '',
		'address1' => '', 'address2' => '', 'city' => '',
		'state' => '', 'country' => '', 'zip' => '',
		'phone' => '', 'email' => '',
	);

	/**
	 * Show cart information
	 */
	public function cartInformationAction()
	{
		$request = $this->getRequest();
		if ($request->get('refresh-license', false))
		{
			$this->getBillingService()->refreshLicense();
			Admin_Flash::setMessage('License information has been updated', Admin_Flash::TYPE_SUCCESS);
			$this->redirect('billing', array('mode' => 'cart-information'));
		}

		$adminView = $this->getView();
		$settings = $this->getSettings();

		$formBuilder = new core_Form_FormBuilder('general');
		$basicGroup = new core_Form_FormBuilder('basic');
		$formBuilder->add($basicGroup);

		$phpinfo = '<a href="#" " data-toggle="modal" data-target="#modal-phpinfo"><span style="color: blue;">Show more information</span></a>';

		$basicGroup->add('cart_info_app_ver', 'static', array('label' => 'Application Version', 'value' => $settings->get('AppVer')));
		$basicGroup->add('cart_info_encoder', 'static', array('label' => 'Encoder Used', 'value' => substr(trim(strtolower($settings->get('AppEncoder'))), 0, 7) == 'ioncube' ? 'IonCube' : 'Zend'));
		$basicGroup->add('cart_info_license_key', 'static', array('label' => 'License Key', 'value' => LICENSE_NUMBER));
		$basicGroup->add('cart_info_products_limit', 'static', array('label' => 'Products Limit', 'value' => (''.AccessManager::checkAccess('MAX_PRODUCTS') == 'INF') ? 'Unlimited' : AccessManager::checkAccess('MAX_PRODUCTS')));
		$basicGroup->add('cart_info_admin_limit', 'static', array('label' => 'Admins Limit', 'value' => (''.AccessManager::checkAccess('MAX_ADMINS') == 'INF') ? 'Unlimited' : AccessManager::checkAccess('MAX_ADMINS')));
		$basicGroup->add('cart_info_server_name', 'static', array('label' => 'Server Name', 'value' => $_SERVER['SERVER_NAME']));
		$basicGroup->add('cart_info_server_ip', 'static', array('label' => 'Server IP Address', 'value' => $_SERVER['SERVER_ADDR']));
		$basicGroup->add('cart_info_server_software', 'static', array('label' => 'Server Software', 'value' => $_SERVER['SERVER_SOFTWARE']));
		$basicGroup->add('cart_info_phpinfo', 'static', array('label' => 'More Details', 'value' => $phpinfo));

		$adminView->assign('phpinfo', $this->showPhpInfo());
		$adminView->assign('form', $formBuilder->getForm());
		$adminView->assign('activeMenuItem', array_merge($this->menuItemProfile, array('secondary' => 'cart-information')));
		$adminView->assign('helpTag', '0000');
		$adminView->assign('body', 'templates/pages/billing/cart-information.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Request cancellation
	 */
	public function requestCancellationAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$billingService = $this->getBillingService();

			if (!Nonce::verify($request->request->get('nonce'), 'cancel_nonce', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce')
				));
				return;
			}
			else
			{
				$serviceLength = $request->request->get('service_length');
				$mainReason = $request->request->get('main_reason');
				$errors = array();

				if (trim($serviceLength) == '')
				{
					$errors[] = array('field' => '#service_length', 'message' => 'Please select service length');
				}

				if (trim($mainReason) == '')
				{
					$errors[] = array('field' => '#cancel-main-reason', 'message' => 'Please enter reason for canceling the service');
				}

				if (count($errors) == 0)
				{
					$response = $billingService->submitCancellation($serviceLength, $mainReason);

					$this->renderJson($response);
					return;
				}
				else
				{
					$this->renderJson(array('status' => 0, 'errors' => $errors));
					return;
				}
			}
		}

		$this->renderJson(array(
			'status' => 0,
			'message' => 'Invalid request',
		));
	}

	/**
	 * Profile action
	 */
	public function profileAction()
	{
		$adminView = $this->getView();

		$billingService = $this->getBillingService();
		$profile = $billingService->getProfile();

		$countryRepository = $this->getCountryRepository();

		$countriesStates = $countryRepository->getCountriesStatesForSettings(true, 'state_code');

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItemProfile);
		$adminView->assign('helpTag', '0000');

		$summary = $billingService->getAccountSummary();
		$adminView->assign('summary', $summary);

		$planName = false;
		$hasDiskUsage = false;
		$diskUsage = false;
		$diskLimit = false;
		$diskPercentage = false;
		$hasBandwidth = false;
		$bandwidthUsage = false;
		$bandwidthLimit = false;
		$bandwidthPercentage = false;

		if ($summary && isset($summary->status) && $summary->status  == 1)
		{
			$planName = $summary->name;
			$hasDiskUsage = $summary->disk_limit > 0;
			$diskUsage = $this->parseSize($summary->disk_usage);
			$diskLimit = $this->parseSize($summary->disk_limit);
			$diskPercentage = ceil(($summary->disk_limit > 0 ? $summary->disk_usage / $summary->disk_limit : 0) * 100);

			$hasBandwidth = $summary->traffic_limit > 0;
			$bandwidthUsage = $this->parseSize($summary->traffic);
			$bandwidthLimit = $this->parseSize($summary->traffic_limit);
			$bandwidthPercentage = ceil(($summary->traffic_limit > 0 ? $summary->traffic / $summary->traffic_limit : 0) * 100);
		}

		$adminView->assign('planName', $planName);
		$adminView->assign('hasDiskUsage', $hasDiskUsage);
		$adminView->assign('diskUsage', $diskUsage);
		$adminView->assign('diskLimit', $diskLimit);
		$adminView->assign('diskPercentage', $diskPercentage);
		$adminView->assign('hasBandwidth', $hasBandwidth);
		$adminView->assign('bandwidthUsage', $bandwidthUsage);
		$adminView->assign('bandwidthLimit', $bandwidthLimit);
		$adminView->assign('bandwidthPercentage', $bandwidthPercentage);

		if (count($profile->cards) > 0)
		{
			$profile->card = $profile->cards[0];
		}

		$adminView->assign('profile', $profile);

		$formBuilder = new core_Form_FormBuilder('general');
		$basicGroup = new core_Form_FormBuilder('basic');
		$formBuilder->add($basicGroup);

		$basicGroup->add('profile[firstName]', 'text', array('label' => 'First Name', 'value' => $profile->firstName, 'required' => true));
		$basicGroup->add('profile[lastName]', 'text', array('label' => 'Last Name', 'value' => $profile->lastName, 'required' => true));
		$basicGroup->add('profile[company]', 'text', array('label' => 'Company', 'value' => $profile->company));
		$basicGroup->add('profile[address1]', 'text', array('label' => 'Address Line 1', 'value' => $profile->address1, 'required' => true));
		$basicGroup->add('profile[address2]', 'text', array('label' => 'Address Line 2', 'value' => $profile->address2));
		$basicGroup->add('profile[city]', 'text', array('label' => 'City', 'value' => $profile->city, 'required' => true));

		$country = ($profile->country != '' ? $profile->country : 'US');
		$state = $profile->state;

		$basicGroup->add('profile[state]', 'choice', array(
			'label' => 'State',
			'value' => $state,
			'required' => true,
			'attr' => array('data-value' => $state, 'class' => 'input-address-state'),
		));
		$basicGroup->add('profile[province]', 'text', array(
			'label' => 'Province',
			'value' => $state,
			'required' => true,
			'wrapperClass' => 'hidden',
			'attr' => array('class' => 'input-address-province'),
		));
		$basicGroup->add('profile[country]', 'choice', array(
			'label' => 'Country',
			'value' => $country,
			'required' => true,
			'attr' => array('data-value' => $country, 'class' => 'input-address-country'),
		));

		$basicGroup->add('profile[zip]', 'text', array('label' => 'Zip / Postal Code', 'value' => $profile->zip, 'required' => true));

		$basicGroup->add('profile[phone]', 'text', array('label' => 'Phone', 'value' => $profile->phone, 'required' => true, 'wrapperClass' => 'clear-both'));
		$basicGroup->add('profile[email]', 'email', array('label' => 'Email', 'value' => $profile->email, 'required' => true));

		$adminView->assign('profileForm', $formBuilder->getForm());
		$adminView->assign('update_nonce', Nonce::create('billing_profile_update'));

		$year = date('Y');
		$years = array($year);

		for ($i = 1; $i <= 10; $i++) $years[] = $year + $i;

		$adminView->assign('years', $years);
		$adminView->assign('countriesStates', json_encode($countriesStates));
		$adminView->assign('cancel_nonce', Nonce::create('cancel_nonce'));
		$adminView->assign('body', 'templates/pages/billing/profile.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update profile
	 */
	public function updateProfileAction()
	{
		$billingService = $this->getBillingService();
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$profile = $request->request->getArray('profile', $this->billingProfileDefaults);

			$response = $billingService->updateProfile(
				$profile['firstName'], $profile['lastName'], $profile['company'],
				$profile['address1'], $profile['address2'], $profile['city'],
				$profile['state'], $profile['country'],	$profile['zip'],
				$profile['phone'], $profile['email']
			);

			if (isset($response->status) and ($response->status == 1))
			{
				Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
			}

			$this->renderJson($response);
		}
	}

	/**
	 * Update card action
	 */
	public function updateCardAction()
	{
		$billingService = $this->getBillingService();
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$card = $request->request->getArray('card', array('cc_num' => '', 'cc_exp_month' => '', 'cc_exp_year' => '', 'cc_cvv2' => ''));

			// TODO: Validation

			$response = $billingService->updateCard($card['cc_num'], $card['cc_exp_month'], $card['cc_exp_year'], $card['cc_cvv2']);

			if ($response->status == 1)
			{
				Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
			}

			$this->renderJson($response);
		}
	}

	/**
	 * Get message center action
	 */
	public function messageCenterAction()
	{
		$messageService = new Admin_Service_MessageCenter();

		$messages = $messageService->getMessages();

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItemMessageCenter);

		$adminView->assign('messages', $messages);
		$adminView->assign('messages_encoded', json_encode($messages));

		$adminView->assign('body', 'templates/pages/billing/message-center.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Purchase
	 */
	public function purchaseAction()
	{
		$billingService = $this->getBillingService();
		$adminView = $this->getView();

		$result = $billingService->getPurchaseForm('trial');

		if ($result && isset($result->html))
		{
			$adminView->assign('html', $result->html);
			$adminView->assign('helpTag', '0003');
			$adminView->assign('body', 'templates/pages/billing/purchase.html');
			$adminView->render('layouts/default');
		}
		else
		{
			Admin_Flash::setMessage('Cannot connect to billing service. Please try again later or contact our support', Admin_Flash::TYPE_ERROR);
			$this->redirect('billing');
		}
	}

	/**
	 * Billing action
	 */
	public function checkoutAction()
	{
		$request = $this->getRequest();
		$billingService = $this->getBillingService();
		$adminView = $this->getView();

		$params = $request->get('params', '');
		parse_str($params, $paramsArray);
		$result = $billingService->getPurchaseForm('trial', is_array($paramsArray) ? $paramsArray : array());

		if ($result && isset($result->html))
		{
			$adminView->assign('html', $result->html);

			$billingForm = new Admin_Form_BillingForm();
			$profile = $billingService->getProfile();
			$adminView->assign('form', $billingForm->getCheckoutForm(array('profile' => $profile)));

			$countryRepository = $this->getCountryRepository();
			$countriesStates = $countryRepository->getCountriesStatesForSettings(true, 'state_code');
			$adminView->assign('countriesStates', json_encode($countriesStates));
		}

		$adminView->assign('helpTag', '0003');
		$adminView->assign('body', 'templates/pages/billing/checkout.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update billing prices
	 */
	public function checkoutUpdateAction()
	{
		$request = $this->getRequest();
		$billingService = $this->getBillingService();
		$session = $this->getSession();

		$result = $billingService->getCheckoutData('trial', $request->request->all(), $session->get(self::ORDER_ID_SESSION_VAR, null));

		if ($result && isset($result->orderSecurityId))
		{
			$session->set(self::ORDER_ID_SESSION_VAR, $result->orderSecurityId);
		}

		$this->renderJson($result);
	}

	/**
	 * Update billing prices
	 */
	public function checkoutPlaceOrderAction()
	{
		$request = $this->getRequest();
		$billingService = $this->getBillingService();
		$session = $this->getSession();
		$formData = $request->request->all();

		$formData = $billingService->normalizePlaceOrderFormData($formData);

		if (($validationErrors = $billingService->getPlaceOrderValidationErrors($formData)) == false)
		{
			$result = $billingService->placeOrder('trial', $formData, $session->get(self::ORDER_ID_SESSION_VAR, null));

			if ($result && isset($result->status))
			{
				if ($result->status)
				{
					$session->remove(self::ORDER_ID_SESSION_VAR);

					$session->set(self::ORDER_PLACE_RESULT_SESSION_VAR, $result);
					$billingService->refreshLicense();

					if (defined('TRIAL_MODE') && TRIAL_MODE)
					{
						$engineConfig = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/engine/engine_config.php');
						$engineConfig = str_replace("define('TRIAL_MODE', true);", '', $engineConfig);
						$engineConfig = str_replace('define("TRIAL_MODE", true);', '', $engineConfig);
						if (defined('TRIAL_TOKEN'))
						{
							$engineConfig = str_replace("define('TRIAL_TOKEN', '".TRIAL_TOKEN."');", '', $engineConfig);
							$engineConfig = str_replace('define("TRIAL_TOKEN", "'.TRIAL_TOKEN.'");', '', $engineConfig);
						}
						@file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/engine/engine_config.php', $engineConfig);

						@FileUtils::removeDirectory(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/trial/');
					}
				}
				else
				{
					if (isset($result->orderSecurityId))
					{
						$session->set(self::ORDER_ID_SESSION_VAR, $result->orderSecurityId);
					}
				}
			}
			else
			{
				$result = array('status' => 0, 'message' => 'Cannot process order at this moment');
			}
		}
		else
		{
			$result = array('status' => 0, 'errors' => $validationErrors);
		}

		$this->renderJson($result);
	}

	/**
	 * Checkout thank you action
	 */
	public function checkoutThankYouAction()
	{
		$session = $this->getSession();
		$result = $session->get(self::ORDER_PLACE_RESULT_SESSION_VAR, false);
		$session->remove(self::ORDER_PLACE_RESULT_SESSION_VAR);

		if ($result)
		{
			$adminView = $this->getView();
			$adminView->assign('placeOrderResult', $result);
			$adminView->assign('body', 'templates/pages/billing/thank-you.html');
			$adminView->render('layouts/default');
		}
		else
		{
			$this->redirect('home');
		}
	}

	/**
	 * Support action
	 */
	public function supportAction()
	{
		$request = $this->getRequest();
		$adminView = $this->getView();

		$billingForm = new Admin_Form_BillingForm();
		$ticketForm = $billingForm->getSupportForm(array());

		if ($request->isPost())
		{
			$formData = array_merge(array('subject' => '', 'message' => '', 'nonce' => ''), $request->getRequest()->all());

			if (!Nonce::verify($formData['nonce'], 'support_ticket'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('billing');
			}
			else
			{
				$supportService = $this->getSupportService();

				if (($validationErrors = $supportService->getTicketValidationErrors($formData)) == false)
				{
					$currentAdmin = $this->getAdminRepository()->getAdminById($this->getCurrentAdminId());

					$formData['full_name'] = $currentAdmin['name'];
					$formData['email'] = $currentAdmin['email'];

					$result = $supportService->submitTicket($formData);
					if ($result && is_object($result))
					{
						if ($result->status)
						{
							$adminView->assign('ticketData', $result->ticket);
							$adminView->assign('ticketSent', true);
							$adminView->assign('ticketSubject', $formData['subject']);
							$adminView->assign('ticketMessage', $formData['message']);
						}
						else
						{
							Admin_Flash::setMessage(
								isset($result->message) ? $result->message : 'Cannot submit ticket at this moment. Please try again later.',
								Admin_Flash::TYPE_ERROR
							);
						}
					}
					else
					{
						Admin_Flash::setMessage(
							'Cannot submit ticket at this moment. Please try again later.',
							Admin_Flash::TYPE_ERROR
						);
					}
				}
				else
				{
					$ticketForm->bind($formData);
					$ticketForm->bindErrors($validationErrors);
					Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView->assign('activeMenuItem', array_merge($this->menuItemProfile, array('secondary' => 'support')));
		$adminView->assign('helpTag', '0002');
		$adminView->assign('form', $ticketForm);
		$adminView->assign('nonce', Nonce::create('support_ticket'));
		$adminView->assign('body', 'templates/pages/billing/support.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @param $size
	 * @return string
	 */
	protected function parseSize($size)
	{
		if ($size >= 1024 * 1024 * 1024 * 1024)
		{
			//TB
			return ceil($size / (1024 * 1024 * 1024 * 1024)).' TB';
		}
		else if ($size >= 1024 * 1024 * 1024)
		{
			//GB
			return ceil($size / (1024 * 1024 * 1024)).' GB';
		}
		else if ($size >= 1024 * 1024)
		{
			//MB
			return ceil($size / (1024 * 1024)).' MB';
		}
		else if ($size >= 1024)
		{
			//KB
			return ceil($size / 1024).' KB';
		}
		else
		{
			//B
			return ceil($size).' B';
		}
	}

	/**
	 * @return Admin_Service_Billing|null
	 */
	protected function getBillingService()
	{
		if (is_null($this->billingService))
		{
			$this->billingService = new Admin_Service_Billing(
				$this->getSettings(),
				new DataAccess_CountriesStatesRepository($this->getDb()),
				defined('BILLING_TEST_MODE') && BILLING_TEST_MODE
			);
		}

		return $this->billingService;
	}

	/**
	 * @return Admin_Service_Support
	 */
	protected function getSupportService()
	{
		if (is_null($this->supportService))
		{
			$this->supportService = new Admin_Service_Support($this->getSettings(), defined('BILLING_TEST_MODE') && BILLING_TEST_MODE);
		}

		return $this->supportService;
	}

	/**
	 * @return DataAccess_CountryRepository
	 */
	protected function getCountryRepository()
	{
		if ($this->countriesRepository == null)
		{
			$this->countriesRepository = new DataAccess_CountryRepository($this->getDb());
		}

		return $this->countriesRepository;
	}

	/**
	 * @return int
	 */
	public function getCurrentAdminId()
	{
		return $this->currentAdminId;
	}

	/**
	 * @param int $currentAdminId
	 */
	public function setCurrentAdminId($currentAdminId)
	{
		$this->currentAdminId = $currentAdminId;
	}

	/**
	 * @return DataAccess_AdminRepository
	 */
	protected function getAdminRepository()
	{
		if ($this->adminRepository == null)
		{
			$this->adminRepository = new DataAccess_AdminRepository($this->getDb(), $this->getSettings());
		}

		return $this->adminRepository;
	}

	/**
	 *  Show php info
	 */
	public function showPhpInfo()
	{
		ob_start();
		phpinfo();
		$pinfo = ob_get_contents();
		ob_end_clean();

		return preg_replace( '%^.*<body>(.*)</body>.*$%ms', '$1' , $pinfo);
	}
}
<?php

/**
 * Class Admin_Controller_StoreAddress
 */
class Admin_Controller_StoreAddress extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'setup');

	/**
	 * Update action
	 */
	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$countryRepository = $this->getCountryRepository();

		$countriesStates = $countryRepository->getCountriesStatesForSettings(true);

		$data = $settings->getByKeys(array(
			'CompanyName', 'CompanyAddressLine1', 'CompanyAddressLine2',
			'CompanyCity', 'CompanyState', 'CompanyZip', 'CompanyCountry',
			'CompanyPhone', 'CompanyFax',
		));

		$storeAddressForm = new Admin_Form_StoreAddressForm();
		$form = $storeAddressForm->getForm($data);

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('store_info');
			}
			else
			{
				$validationErrors = array();
				$formData = array();
				foreach ($data as $key => $temp)
				{
					if ($key == 'CompanyProvince' || $key == 'CompanyState') continue;

					$formData[$key] = trim($request->request->get($key, ''));

					/** @var core_Form_Field $field */
					$field = $storeAddressForm->getBasicGroup()->get($key);

					if ($field && is_array($field) && $field['options']['required'] && $formData[$key] == '')
					{
						$validationErrors[$key] = array(trans('common.please_enter') . ' ' . trans($field['options']['label']));
					}
				}

				$country = null;
				foreach ($countriesStates as $c)
				{
					if ($c['id'] == $formData['CompanyCountry'])
					{
						$country = $c;
						break;
					}
				}

				if ($country && isset($country['states']) && is_array($country['states']) && count($country['states']) > 0)
				{
					$formData['CompanyState'] = trim($request->request->get('CompanyState', ''));
					if ($formData['CompanyState'] == '')
					{
						$validationErrors['CompanyState'] = array('Please enter State');
					}
				}
				else
				{
					$formData['CompanyState'] = trim($request->request->get('CompanyProvince', ''));
					if ($formData['CompanyState'] == '')
					{
						$validationErrors['CompanyState'] = array('Please enter Province');
					}
				}

				if (count($validationErrors) == 0)
				{
					$settings->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('store_info');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8001');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('settings_update'));

		$adminView->assign('countriesStates', json_encode($countriesStates));

		$adminView->assign('body', 'templates/pages/settings/store-address.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @return DataAccess_CountryRepository
	 */
	protected function getCountryRepository()
	{
		$db = $this->getDb();

		return new DataAccess_CountryRepository($db);
	}
}
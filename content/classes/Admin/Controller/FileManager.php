<?php

/**
 * Class Admin_Controller_FileManager
 */
class Admin_Controller_FileManager extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	protected $blacklist = array(
		'./content/engine/engine_config.php',
	);

	/**
	 * Show administrators activity log list
	 */
	public function listAction()
	{
		if (!AccessManager::checkAccess('FILEMANAGER'))
		{
			$this->accessDeniedAction();
			return;
		}

		$request = $this->getRequest();

		$folder = $this->getFolder($request);

		$folders = array();
		$files = array();

		$this->getFolderData($folder, $folders, $files);
		Admin_Log::log('File Manager: directory viewed - '.$folder, Admin_Log::LEVEL_IMPORTANT);

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8020');

		$adminView->assign('currentFolder', $folder);
		$adminView->assign('parentFolder', $folder == '/' ? '' : dirname($folder));

		$adminView->assign('parentFolderParts', $this->getParentFolderParts($folder));

		$adminView->assign('folders', $folders);
		$adminView->assign('files', $files);

		$fileManagerForm = new Admin_Form_FileManagerForm();

		$form = $fileManagerForm->getForm();
		$adminView->assign('uploadForm', $form);

		$adminView->assign('delete_nonce', Nonce::create('file_manager_delete'));
		$adminView->assign('upload_nonce', Nonce::create('file_manager_upload'));

		$adminView->assign('body', 'templates/pages/file-manager/list.html');
		$adminView->render('layouts/default');
	}

	public function editAction()
	{
		if (!AccessManager::checkAccess('FILEMANAGER'))
		{
			$this->accessDeniedAction();
			return;
		}

		$request = $this->getRequest();

		$folder = $this->getFolder($request);
		$file = $this->getFile($request);

		$currentFile = '.'.$folder.$file;

		if (in_array($currentFile, $this->blacklist))
		{
			$this->redirect('filemanager');
			return;
		}

		if ($request->isPost())
		{
			$content = $request->request->get('content');
			$nonce = $request->request->get('nonce');

			if (!Nonce::verify($nonce, 'file_manager_edit'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('filemanager');
			}
			else
			{
				if (file_put_contents($currentFile, $content))
				{
					Admin_Log::log('File Manager: file modified - '.$currentFile, Admin_Log::LEVEL_IMPORTANT);
					Admin_Flash::setMessage('common.success');
				}
				else
				{
					Admin_Flash::setMessage('Can\'t save file. Please check file permissions and free space', Admin_Flash::TYPE_ERROR);
				}
			}
		}

		Admin_Log::log('File Manager: file opened for editing - '.$currentFile, Admin_Log::LEVEL_IMPORTANT);

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('currentFolder', $folder);
		$adminView->assign('parentFolder', $folder == '/' ? '' : dirname($folder));

		$adminView->assign('currentFile', $file);

		$data['content'] = file_get_contents($currentFile);

		$fileManagerForm = new Admin_Form_FileManagerForm();

		$form = $fileManagerForm->getFileManagerEditForm($data);

		$adminView->assign('form', $form);

		$adminView->assign('nonce', Nonce::create('file_manager_edit'));

		$adminView->assign('parentFolderParts', $this->getParentFolderParts($folder));

		$adminView->assign('body', 'templates/pages/file-manager/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Upload action
	 */
	public function uploadAction()
	{
		if (!AccessManager::checkAccess('FILEMANAGER'))
		{
			$this->accessDeniedAction();
			return;
		}

		$request = $this->getRequest();

		$folder = $this->getFolder($request);

		if ($request->isPost())
		{
			$newFile = $request->getUploadedFile('new_file');
			if ($newFile && is_array($newFile) && array_key_exists('name', $newFile))
			{
				$file = $newFile['name'];

				$currentFile = '.'.$folder.$file;

				if (!in_array($currentFile, $this->blacklist))
				{
					if (@copy($newFile['tmp_name'], $currentFile))
					{
						chmod($currentFile, FileUtils::getFilePermissionMode());
						Admin_Log::log('File Manager: New file uploaded - '.$currentFile, Admin_Log::LEVEL_IMPORTANT);

						Admin_Flash::setMessage('common.success');
					}
					else
					{
						Admin_Flash::setMessage('Can\'t upload file.  Please check free space and permissions.');
					}
				}
				else
				{
					Admin_Flash::setMessage('Can\'t upload file.  Please check free space and permissions.');
					Admin_Log::log('File Manager: Attempted to replace file from a black list - '.$currentFile, Admin_Log::LEVEL_IMPORTANT);
				}
			}
		}
	}

	/**
	 *
	 */
	public function checkExistsAction()
	{
		if (!AccessManager::checkAccess('FILEMANAGER'))
		{
			$this->accessDeniedAction();
			return;
		}

		$request = $this->getRequest();

		$folder = $this->getFolder($request);
		$file = $this->getFile($request);

		$exists = false;
		if ($file != '')
		{
			$currentFile = '.'.$folder.$file;

			$exists = file_exists($currentFile);
		}
		else
		{
			$exists = true;
		}

		header('Content-Type: application/json');
		echo json_encode($exists);
	}

	/**
	 * Preview action
	 */
	public function previewAction()
	{
		if (!AccessManager::checkAccess('FILEMANAGER'))
		{
			$this->accessDeniedAction();
			return;
		}

		$request = $this->getRequest();

		$folder = $this->getFolder($request);
		$file = $this->getFile($request);

		$currentFile = '.'.$folder.$file;

		if (in_array($currentFile, $this->blacklist) || $file == '')
		{
			$this->redirect('filemanager', array('folder' => $folder));
			return;
		}

		$size = getimagesize($currentFile);

		Admin_Log::log('File Manager: file opened for viewing - '.$currentFile, Admin_Log::LEVEL_IMPORTANT);

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('currentFolder', $folder);
		$adminView->assign('parentFolder', $folder == '/' ? '' : dirname($folder));

		$adminView->assign('currentFile', $file);
		$adminView->assign('width', isset($size[0]) ? $size[0] : false);
		$adminView->assign('height', isset($size[1]) ? $size[1] : false);
		$fileSize = @filesize($currentFile);
		if ($fileSize)
		{
			$fileSize = number_format($fileSize/1024, 2, ".", ",");
		}
		$adminView->assign('fileSize', $fileSize);

		$adminView->assign('parentFolderParts', $this->getParentFolderParts($folder));

		$adminView->assign('body', 'templates/pages/file-manager/preview.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete action
	 */
	public function deleteAction()
	{
		if (!AccessManager::checkAccess('FILEMANAGER'))
		{
			$this->accessDeniedAction();
			return;
		}

		$request = $this->getRequest();

		$folder = $this->getFolder($request);
		$file = $this->getFile($request);

		$currentFile = '.'.$folder.$file;

		$nonce = $request->get('nonce');
		if (!Nonce::verify($nonce, 'file_manager_delete'))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('filemanager', array('folder' => $folder));
		}
		else
		{
			if (in_array($currentFile, $this->blacklist))
			{
				Admin_Log::log('File Manager: Attempted to delete file from a black list - '.$currentFile, Admin_Log::LEVEL_IMPORTANT);
				Admin_Flash::setMessage('Can\'t remove file.  Please check file permissions.');
				$this->redirect('filemanager', array('folder' => $folder));
				return;
			}

			if (@unlink($currentFile))
			{
				Admin_Flash::setMessage('common.success');
				Admin_Log::log('File Manager: File deleted - '.$currentFile, Admin_Log::LEVEL_IMPORTANT);
				$this->redirect('filemanager', array('folder' => $folder));
			}
			else
			{
				Admin_Flash::setMessage('Can\'t remove file.  Please check file permissions.');
				$this->redirect('filemanager', array('folder' => $folder));
			}
		}
	}

	public function accessDeniedAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('body', 'templates/pages/file-manager/access-denied.html');
		$adminView->render('layouts/default');
	}

	protected function getParentFolderParts($folder)
	{
		$temp = trim($folder, '/');
		if ($temp == '.' || $temp == '')
		{
			return array();
		}
		else
		{
			$parts = explode('/', $temp);
			return $parts;
		}
	}

	protected function getFolderData($folder, array &$folders, array &$files)
	{
		$dir = @dir('.'.$folder);
		if (!$dir) {
			$folder = '/';
			$dir = dir('.'.$folder);
		}
		if ($dir)
		{
			while (($entry = $dir->read()) !== false)
			{
				$filePath = '.'.$folder.$entry;
				switch (@filetype($filePath))
				{
					case 'dir':
					{
						if ($entry != '.' && $entry != '..') {
							if (!in_array($filePath, $this->blacklist)) {
								$folders[$entry] = array(
									'name' => $entry,
									'created_at' => date("Y/m/d, g:i a",filectime($filePath)),
									'permissions' => substr(sprintf('%o', fileperms($filePath)), -4),
								);
							}
						}
						break;
					}
					case 'link':
					case 'file':
					{
						if (!in_array($filePath, $this->blacklist)) {
							$isImage = false;
							$ext = strtolower($this->getFileExt($filePath));

							switch ($ext)
							{
								case 'bmp':
								case 'jpg':
								case 'jpeg':
								case 'gif':
								case 'png':
									$isImage = true;
							}

							$files[$entry] = array(
								'name' => $entry,
								'created_at' => date("Y/m/d, g:i a",filectime($filePath)),
								'size' => number_format(filesize($filePath)/1024, 2, ".", ","),
								'permissions' => substr(sprintf('%o', fileperms($filePath)), -4),
								'isImage' => $isImage,
							);
						}
						break;
					}
				}
			}

			asort($folders);
			asort($files);
		}
	}

	protected function getFolder(Framework_Request $request)
	{
		$folder = trim($request->query->get('folder', ''));

		$folder = str_replace('\\', '/', $folder);
		$parts = explode('/', $folder);

		// Directory traversal is prohibited
		if (in_array('..', $parts)) $folder = '';

		if (trim($folder) == '') $folder = '/';

		$path_prefix = '.';

		//check folder
		$real_folder = str_replace("\\", "/", realpath($path_prefix.$folder));

		$cart_folder = realpath('.');

		if ($cart_folder == "")
		{
			$cart_folder = isset($_SERVER["SCRIPT_FILENAME"])?str_replace("\\\\", "/", dirname($_SERVER["SCRIPT_FILENAME"])):'';
		}
		if ($cart_folder == "")
		{
			$cart_folder = isset($_SERVER["DOCUMENT_ROOT"])?str_replace("\\\\", "/", dirname($_SERVER["DOCUMENT_ROOT"])):'';
		}

		$cart_folder = str_replace("\\", "/", $cart_folder);
		if (substr($real_folder, 0, strlen($cart_folder)) != $cart_folder)
		{
			$folder = "/";
		}

		return rtrim($folder, '/').'/';
	}

	protected function getFile(Framework_Request $request)
	{
		$file = trim($request->query->get('file', ''));

		$folder = str_replace('\\', '/', $file);
		$parts = explode('/', $file);

		// Directory traversal is prohibited
		if (in_array('..', $parts)) $file = '';

		if (trim($file) == '') $file = '';

		return $file;
	}

	protected function getFileExt($file_name)
	{
		$ar = explode('.', $file_name);
		return strtolower(array_pop($ar));
	}
}
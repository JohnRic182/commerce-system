<?php

/**
 * Class Admin_Controller_CustomForm
 */
class Admin_Controller_CustomForm extends Admin_Controller
{
	/** @var DataAccess_CustomFormRepository $customFormRepository */
	protected $customFormRepository = null;

	/** @var DataAccess_CustomFieldRepository $customFieldRepository */
	protected $customFieldRepository = null;

	protected $searchParamsDefaults = array('search_str' => '', 'active' => 'all', 'title' => '', 'form_id' => '');

	protected $menuItem = array('primary' => 'content', 'secondary' => 'custom-forms');

	/**
	 * List customForms
	 */
	public function listAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();
		/** @var Framework_Session $session */
		$session = $this->getSession();
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('customFormsSearchParams', array()); else $session->set('customFormsSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('customFormsSearchOrderBy', 'title'); else $session->set('customFormsSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('customFormsSearchOrderDir', 'asc'); else $session->set('customFormsSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());
		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		/**
		 * Get data from DB
		 */
		$customFormRepository = $this->getCustomFormRepository();
		$itemsCount = $customFormRepository->getCount($searchParams);

		/**
		 * Show results
		 */
		$customFormForm = new Admin_Form_CustomForm();
		$adminView->assign('searchForm', $customFormForm->getCustomFormSearchForm($searchFormData));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign(
				'customForms',
				$customFormRepository->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams
				)
			);
		}
		else
		{
			$adminView->assign('customForms', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('custom_form_delete'));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6008');

		$adminView->assign('body', 'templates/pages/custom-form/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new customForm
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		/** @var DataAccess_CustomFormRepository $customFormRepository */
		$customFormRepository = $this->getCustomFormRepository();

		$customFormModel = $customFormRepository->create();
		$itemsCount = $customFormRepository->getCount();

		$defaults = $customFormModel->toArray();

		$validationErrors = false;
		$formValues = null;

		$handle = $request->getRequest('handle');
		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$postData = $request->getRequest()->all();

			$postData['is_active'] = 0; // form with no fields cannot be active
			$postData['is_title_visible'] = isset($postData['is_title_visible']) && $postData['is_title_visible'] == '1' ? 1 : 0;
			$postData['is_recaptcha'] = isset($postData['is_recaptcha']) && $postData['is_recaptcha'] == '1' ? 1 : 0;
			$postData['save_data'] = 1;
			$postData['email_notification'] = isset($postData['email_notification']) && $postData['email_notification'] == '1' ? 1 : 0;
			$postData['redirect_on_submit'] = isset($postData['redirect_on_submit']) && $postData['redirect_on_submit'] == '1' ? 1 : 0;

			$formData = array_merge($defaults, $postData);

			if (!Nonce::verify($formData['nonce'], 'custom_form_add'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('custom_forms');
			}
			else
			{
				if (($validationErrors = $customFormRepository->getValidationErrors($formData, $mode)) == false)
				{
					$customFormModel->hydrate($formData);

					$handle = $customFormRepository->persist($customFormModel);
					$handle = ($handle === true) ? 'update' : 'insert';

					Admin_Flash::setMessage('common.success');
					Admin_Flash::setMessage('custom_form.new_custom_form_added');
					$this->redirect('custom_form', array('id' => $customFormModel->getId(), 'mode' => self::MODE_UPDATE, 'handle' => $handle));
				}
				else
				{
					$formValues = $formData;
				}
			}
		}
		else
		{
			$formValues = $defaults;
			$formValues['is_visible'] = 'Yes';
			$formValues['image'] = '';
		}

		/** @var Admin_Form_CustomForm $customFormForm */
		$customFormForm = new Admin_Form_CustomForm();

		/** @var core_Form_Form $form */
		$form = $customFormForm->getForm($formValues, $mode);
		if ($validationErrors) $form->bindErrors($validationErrors);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3013');

		$adminView->assign('handle', $handle);
		$adminView->assign('itemsCount', $itemsCount);
		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('custom_form_add'));

		$adminView->assign('body', 'templates/pages/custom-form/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update customForm
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		/** @var DataAccess_CustomFormRepository $customFormRepository */
		$customFormRepository = $this->getCustomFormRepository();

		$id = intval($request->get('id'));
		$handle = $request->get('handle', 'update');

		$customFormModel = $customFormRepository->getById($id);
		$itemsCount = $customFormRepository->getCount();

		if (!$customFormModel)
		{
			Admin_Flash::setMessage('Cannot find customForm by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('custom_forms');
		}

		$formFields = $this->getCustomFieldRepository()->getListByFormId($id);

		$defaults = $customFormModel->toArray();

		$validationErrors = false;

		if ($request->isPost())
		{
			$postData = $request->request->all();

			$postData['is_active'] = isset($postData['is_active']) && $postData['is_active'] == '1' ? 1 : 0;
			if ($postData['is_active'] == 1 && (!is_array($formFields) || count($formFields) == 0))
			{
				Admin_Flash::setMessage('custom_form.form_no_fields', Admin_Flash::TYPE_ERROR);
				$postData['is_active'] = 0;
			}

			$postData['is_title_visible'] = isset($postData['is_title_visible']) && $postData['is_title_visible'] == '1' ? 1 : 0;
			$postData['is_recaptcha'] = isset($postData['is_recaptcha']) && $postData['is_recaptcha'] == '1' ? 1 : 0;
			$postData['save_data'] = 1;
			$postData['email_notification'] = isset($postData['email_notification']) && $postData['email_notification'] == '1' ? 1 : 0;
			$postData['redirect_on_submit'] = isset($postData['redirect_on_submit']) && $postData['redirect_on_submit'] == '1' ? 1 : 0;

			$formData = array_merge($defaults, $postData);

			if (!Nonce::verify($formData['nonce'], 'custom_form_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('custom_forms');
			}
			else
			{
				if (($validationErrors = $customFormRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$customFormModel->hydrate($formData);

					$handle = $customFormRepository->persist($customFormModel);
					$handle = ($handle === true) ? 'update' : 'insert';

					// TODO - optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, false, true);

					Admin_Flash::setMessage('common.success');
					$this->redirect('custom_form', array('id' => $id, 'mode' => self::MODE_UPDATE, 'handle' => $handle));
				}
				else
				{
					$formValues = $formData;
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}
		else
		{
			$formValues = $defaults;
		}

		$customFormForm = new Admin_Form_CustomForm();
		$customFieldForm = new Admin_Form_CustomFieldForm();

		$form = $customFormForm->getForm($formValues, $mode, array('formFields' => $formFields), $id);
		if ($validationErrors) $form->bindErrors($validationErrors);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3014');
		$adminView->assign('title', $defaults['title']);

		$adminView->assign('handle', $handle);
		$adminView->assign('itemsCount', $itemsCount);
		$adminView->assign('mode', $mode);
		$adminView->assign('id', $customFormModel->getId());
		$adminView->assign('form', $form);
		$adminView->assign('customFieldForm', $customFieldForm->getForm($id));
		$adminView->assign('nonce', Nonce::create('custom_form_update'));

		$adminView->assign('body', 'templates/pages/custom-form/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete customForms
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'custom_form_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
		}
		else
		{
			/** @var DataAccess_CustomFormRepository $customFormRepository */
			$customFormRepository = $this->getCustomFormRepository();

			$deleteMode = $request->get('deleteMode');

			if ($deleteMode == 'single')
			{
				$id = $request->get('id', 0);
				if ($id < 1)
				{
					Admin_Flash::setMessage('Invalid customForm id', Admin_Flash::TYPE_ERROR);
				}
				else
				{
					$customFormRepository->delete($id);
					Admin_Flash::setMessage('Custom form has been deleted');
				}
			}
			else if ($deleteMode == 'selected')
			{
				$ids = explode(',', $request->get('ids', ''));
				if (count($ids) == 0)
				{
					Admin_Flash::setMessage('Please select at least one form to delete', Admin_Flash::TYPE_ERROR);
				}
				else
				{
					$customFormRepository->delete($ids);
					Admin_Flash::setMessage('Selected form has been deleted');
				}
			}
			else if ($deleteMode == 'search')
			{
				/** @var Framework_Session $session */
				$session = $this->getSession();
				$searchParams = $session->get('customFormsSearchParams', array());
				$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());
				$customFormRepository->deleteBySearchParams($searchParams);
				Admin_Flash::setMessage('Selected customForms have been deleted');
			}
			else
			{
				Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
			}
		}

		$this->redirect('custom_forms');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CustomFormRepository
	 */
	protected function getCustomFormRepository()
	{
		if (is_null($this->customFormRepository))
		{
			$this->customFormRepository = new DataAccess_CustomFormRepository($this->getDb());
		}

		return $this->customFormRepository;
	}

	/**
	 * Get custom field repository
	 */
	protected function getCustomFieldRepository()
	{
		if (is_null($this->customFieldRepository))
		{
			$this->customFieldRepository = new DataAccess_CustomFieldRepository($this->getDb());
		}

		return $this->customFieldRepository;
	}
}

<?php
/**
 * Class Admin_Controller_Exactor
 */
class Admin_Controller_Exactor extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$this->checkCompanyAddressSettings();

		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('exactor_merchant_id', 'exactor_user_id', 'exactor_password',
			'exactor_full_name', 'exactor_currency_code', 'exactor_commit_taxes', 'exactor_sku_source',
		);

		$settingsData = $settings->getByKeys($settingsVars);
		$exactorForm = new Admin_Form_ExactorForm();
		$form = $exactorForm->getForm($settingsData);

		if ($request->isPost())
		{
			$formData = $request->request->all();
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'exactor'));
				return;
			}

			if (($validationErrors = $this->getValidationErrors($formData)) == false) {
				$settingsData = $request->request->getByKeys($settingsVars);
				$settings->persist($settingsData, $settingsVars);
				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'exactor'));
			}
			else
			{
				// TODO: find better way to handle this
				$form->bind($formData);
				$form->bindErrors($validationErrors);
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9023');

		$adminView->assign('nonce', Nonce::create('update_settings'));
		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/exactor/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get validation errors
	 * @param $data
	 * @return array|bool
	 */
	public function getValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			if (isset($data['exactor_merchant_id']) && trim($data['exactor_merchant_id']) == '')
			{
				$errors['exactor_merchant_id'] = array('Account # is required.');
			}

			if (isset($data['exactor_user_id']) && trim($data['exactor_user_id']) == '')
			{
				$errors['exactor_user_id'] = array('Username is required.');
			}

			if (isset($data['exactor_password']) && trim($data['exactor_password']) == '')
			{
				$errors['exactor_password'] = array('Password is required.');
			}

			if (isset($data['exactor_full_name']) && trim($data['exactor_full_name']) == '')
			{
				$errors['exactor_full_name'] = array('Compant Full Name is required.');
			}

			if (isset($data['exactor_currency_code']) && trim($data['exactor_currency_code']) == '')
			{
				$errors['exactor_currency_code'] = array('Currecny Code is required.');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('ExactorEnableTax', 'exactor_merchant_id', 'exactor_user_id', 'exactor_password', 'exactor_full_name', 'exactor_currency_code');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['exactor_merchant_id']) || trim($settingsData['exactor_merchant_id']) == '')
			{
				$errors[] = array('field' => '.field-exactor_merchant_id', 'message' => 'Account # is required');
			}
			if (!isset($settingsData['exactor_user_id']) || trim($settingsData['exactor_user_id']) == '')
			{
				$errors[] = array('field' => '.field-exactor_user_id', 'message' => 'Username is required');
			}
			if (!isset($settingsData['exactor_password']) || trim($settingsData['exactor_password']) == '')
			{
				$errors[] = array('field' => '.field-exactor_password', 'message' => 'Password is required');
			}
			if (!isset($settingsData['exactor_full_name']) || trim($settingsData['exactor_full_name']) == '')
			{
				$errors[] = array('field' => '.field-exactor_full_name', 'message' => 'Company Name is required');
			}
			if (!isset($settingsData['exactor_currency_code']) || trim($settingsData['exactor_currency_code']) == '')
			{
				$errors[] = array('field' => '.field-exactor_currency_code', 'message' => 'Currency Code is required');
			}

			if ($settings->get('AvalaraEnableTax') == 'Yes')
			{
				$errors[] = array('field' => '', 'message' => 'Avalara Tax Service is enabled.  Please disable before enabling the Exactor Tax Service.');
			}

			if (count($errors) < 1)
			{
				$settingsData['ExactorEnableTax'] = 'Yes';
				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('exactor');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$exactorForm = new Admin_Form_ExactorForm();
		$form = $exactorForm->getExactorActivateForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('ExactorEnableTax' => 'No'));
	}

	/**
	 * Check are company settings ready
	 */
	protected function checkCompanyAddressSettings()
	{
		$settings = $this->getSettings();

		if (
			$settings->get('CompanyName') == 'Company Name' || $settings->get('CompanyName') == '' ||
			$settings->get('CompanyAddressLine1') == 'Address Line 1' || $settings->get('CompanyAddressLine1') == '' ||
			$settings->get('CompanyState') == 'State' || $settings->get('CompanyState') == '' ||
			$settings->get('CompanyZip') == '-' || $settings->get('CompanyZip') == ''
		)
		{
			Admin_Flash::setMessage(
				'To calculate taxes using Exactor, please make sure you have updated your <a href="admin.php?p=settings&group=1">Company Information</a> under Global Cart Settings. '.
				'Please note: Exactor requires a valid address',
				Admin_Flash::TYPE_ERROR
			);
		}
	}

	/**
	 * Manage tax codes
	 */
	public function taxCodesAction()
	{
		$adminView = $this->getView();

		$exactorForm = new Admin_Form_ExactorForm();
		$data = array('name' => '', 'code' => '');
		$adminView->assign('taxCodeForm', $exactorForm->getExactorTaxCodeForm($data));

		$adminView->assign('addNonce', Nonce::create('tax_code_add'));
		$adminView->assign('updateNonce', Nonce::create('tax_code_update'));

		$db = $this->getDb();

		$taxCodes = $db->selectAll('SELECT * FROM '.DB_PREFIX.'exactor_tax_codes');
		$adminView->assign('tax_codes', $taxCodes);

		$adminView->assign('itemsCount', count($taxCodes));

		$adminView->assign('update_euc_nonce', Nonce::create('tax_update_exactor_eucs'));
		$adminView->assign('delete_nonce', Nonce::create('tax_code_delete'));
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9024');

		$adminView->assign('body', 'templates/pages/exactor/tax-codes.html');
		$adminView->render('layouts/default');
	}

	public function addTaxCodeAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'tax_code_add', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$taxName = $request->request->get('tax_code_name');
				$taxCode = $request->request->get('tax_code_code');

				$validationErrors = array();
				if (trim($taxCode) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_code', 'message' => 'Please enter Tax Code');
				}
				if (trim($taxName) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_name', 'message' => 'Please enter Tax Name');
				}

				if (count($validationErrors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
				else
				{
					$db = $this->getDb();
					$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'exactor_tax_codes WHERE code = \''.$this->db->escape($taxCode).'\'');

					if (!$result)
					{
						$db->reset();
						$db->assignStr('code', $taxCode);
						$db->assignStr('name', $taxName);
						$db->insert(DB_PREFIX.'exactor_tax_codes');

						Admin_Flash::setMessage('common.success');

						$this->renderJson(array(
							'status' => 1
						));
					}
					else
					{
						$this->renderJson(array(
							'status' => 0,
							'message' => 'Tax Code already exists',
						));
					}
				}
			}
		}
	}

	public function updateTaxCodeAction()
	{
		$request = $this->getRequest();

		$id = intval($request->get('id'));

		$db = $this->getDb();
		$taxCode = $db->selectOne('SELECT * FROM '.DB_PREFIX.'exactor_tax_codes WHERE id = '.$id);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'tax_code_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$taxName = $request->request->get('tax_code_name');
				$taxCode = $request->request->get('tax_code_code');

				$validationErrors = array();
				if (trim($taxCode) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_code', 'message' => 'Please enter Tax Code');
				}
				if (trim($taxName) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_name', 'message' => 'Please enter Tax Name');
				}

				if (count($validationErrors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
				else
				{
					$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'exactor_tax_codes WHERE id <> '.$id.' AND code = \''.$this->db->escape($taxCode).'\'');

					if (!$result)
					{
						$db->reset();
						$db->assignStr('code', $taxCode);
						$db->assignStr('name', $taxName);
						$db->update(DB_PREFIX.'exactor_tax_codes', 'WHERE id = '.$id);

						Admin_Flash::setMessage('common.success');

						$this->renderJson(array(
							'status' => 1
						));
					}
					else
					{
						$this->renderJson(array(
							'message' => 'Tax Code already exists',
						));
					}
				}
			}
		}

		$adminView = $this->getView();

		$exactorForm = new Admin_Form_ExactorForm();
		$adminView->assign('form', $exactorForm->getExactorTaxCodeForm($taxCode));

		$adminView->render('generic-form');
	}

	public function deleteAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'tax_code_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('exactor', array('mode' => 'tax-codes'));
			return;
		}

		$db = $this->getDb();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid tax code id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$this->removeTaxCode($id);

				Admin_Flash::setMessage('Tax Code has been deleted');
				$this->redirect('exactor', array('mode' => 'tax-codes'));
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one tax code to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('exactor', array('mode' => 'tax-codes'));
			}
			else
			{
				foreach ($ids as $id)
				{
					$this->removeTaxCode($id);
				}

				Admin_Flash::setMessage('Selected tax code have been deleted');
				$this->redirect('exactor', array('mode' => 'tax-codes'));
			}
		}
		else if ($deleteMode == 'search')
		{
			$db->query('DELETE FROM '.DB_PREFIX.'exactor_tax_codes');

			$db->query("UPDATE ".DB_PREFIX."products SET exactor_euc_code = ''  WHERE exactor_euc_code != ''");
			$db->query("UPDATE ".DB_PREFIX."catalog SET exactor_euc_code = ''  WHERE exactor_euc_code != ''");

			Admin_Flash::setMessage('Selected tax codes have been deleted');
			$this->redirect('exactor', array('mode' => 'tax-codes'));
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);

			$this->redirect('exactor', array('mode' => 'tax-codes'));
		}
	}

	protected function removeTaxCode($id)
	{
		$id = intval($id);
		if ($id > 0)
		{
			$db = $this->getDb();
			$code = $db->selectOne('SELECT code FROM '.DB_PREFIX.'exactor_tax_codes WHERE id = '.$id);

			if ($code)
			{
				$code = array_shift($code);

				$db->query('DELETE FROM '.DB_PREFIX.'exactor_tax_codes WHERE id = '.$id);

				$db->query("UPDATE ".DB_PREFIX."products SET exactor_euc_code = ''  WHERE exactor_euc_code = '".$db->escape($code)."'");
				$db->query("UPDATE ".DB_PREFIX."catalog SET exactor_euc_code = ''  WHERE exactor_euc_code = '".$db->escape($code)."'");
			}
		}
	}

	/**
	 * Add eus code
	 */
	public function addEucCodeAction()
	{
		$errors = array();
		$submitSuccess = false;
		$eucCodeUpdatesSuccess = false;
		$lastActiveTab = 'tax-codes';

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$eucCode = $request->get('euc_code');
			$eucName = $request->get('euc_name');

			if (trim($eucCode) != '' && trim($eucName) != '')
			{
				$ret = $this->addTaxCode($eucCode, $eucName);
			}
		}

		$this->renderExactorSettingsPage($errors, $lastActiveTab);
	}

	/**
	 * Add tax code
	 *
	 * @param $taxCode
	 * @param $taxName
	 * @return bool|string
	 */
	protected function addTaxCode($taxCode, $taxName)
	{
		// TODO: move this function into repository

		$taxCode = trim($taxCode);
		$taxName = trim($taxName);

		$result = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'exactor_tax_codes WHERE code = \''.$this->db->escape($taxCode).'\'');

		if (!$result)
		{
			$this->db->reset();
			$this->db->assignStr('name', $taxName);
			$this->db->assignStr('code', $taxCode);
			$this->db->insert(DB_PREFIX.'exactor_tax_codes');

			return true;
		}
		else
		{
			return 'Tax Code already exists';
		}
	}

	/**
	 * Update EUC action
	 */
	public function updateEucAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'tax_update_exactor_eucs'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('exactor', array('mode' => 'tax-codes'));
		}
		else
		{
			Tax_Exactor_ExactorEUCHelper::updateEUCCodes();

			Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

			$this->redirect('exactor', array('mode' => 'tax-codes'));
		}
	}

	public function taxCodesTreeAction()
	{
		$repository = new Tax_Exactor_ExactorRepository($this->getDb());
		$tree = $repository->getTaxCodeCategoriesTree();

		$render = new Tax_Exactor_TaxCodeCategoryNodeRenderer();
		$render->render($tree);
	}

	public function taxCodesEUCAction()
	{
		$repository = new Tax_Exactor_ExactorRepository($this->getDb());
		$products = $repository->getEUCProductsServices();

		$this->renderJson($products);
	}
}
<?php

/**
 * Class Admin_Controller_Search
 */
class Admin_Controller_Search extends  Admin_Controller
{
	/**
	 * Search for something
	 */
	public function searchAction()
	{
		$db = $this->getDb();

		$request = $this->getRequest();

		$searchStr = trim($request->get('str', ''));

		$settingsArray = $this->getSettings()->getAll();

		$searchService = new Admin_Service_Search(
			new DataAccess_OrderRepository($db, $settingsArray),
			new DataAccess_CustomersRepository($db, $this->getSettings()),
			new DataAccess_ProductsRepository($db),
			new DataAccess_CategoryRepository($db)
		);

		$result = $searchService->search($searchStr);

		$this->renderJson($result);
	}
}
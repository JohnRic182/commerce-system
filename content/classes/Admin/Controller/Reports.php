<?php

/**
 * Class Admin_Controller_Reports
 */
class Admin_Controller_Reports extends Admin_Controller
{
	protected $menuItem = array('primary' => 'reports', 'secondary' => 'reports');

	/** @var DataAccess_StatRepository $statsRepository */
	protected $statsRepository = null;

	/**
	 * @var array
	 */
	public $reportTypes = array(
		'orders', 'dates', 'payment', 'customers',
		'products', 'promo', 'qr_campaigns',
		'recurring_profiles', 'recurring_orders', 'recurring_failures'
	);

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$view = $this->getView();

		$view->assign('pageTitle', trans('reporting.reporting'));
		$view->assign('helpTag', '4000');
		$view->assign('reports', $this->getReportsTypes());
		$view->assign('activeMenuItem', $this->menuItem);

		$view->assign('body', 'templates/pages/reports/dashboard.html');
		$view->render('layouts/default');
	}

	public function salesAction()
	{
		$view = $this->getView();
		$view->assign('pageTitle', trans('reporting.reporting'));
		$view->assign('helpTag', '4000');
		$view->assign('reports', $this->getReportsTypes());
		$view->assign('activeMenuItem', $this->menuItem);

		$view->assign('body', 'templates/pages/reports/sales.html');
		$view->render('layouts/default');
	}

	/**
	 * Return reports types
	 *
	 * @return array
	 */
	protected function getReportsTypes()
	{
		return $this->reportTypes;
	}

	/**
	 * Generate report
	 */
	public function reportAction()
	{
		$request = $this->getRequest();

		$reportType = $request->get('report_type');

		$rangeType = $request->get('range_type', 'month');

		$reportType = in_array($reportType, $this->reportTypes) ? $reportType : '';

		if ($reportType == '')
		{
			Admin_Flash::setMessage(trans('reporting.report_not_found'), Admin_Flash::TYPE_ERROR);
			$this->redirect('reports');
		}
		else
		{
			/**
			 * Get dates
			 */
			$dateFrom = $this->prepareDateFrom($request->get('from', null));
			$dateTo = $this->prepareDateTo($request->get('to', null));

			/**
			 * Params
			 */
			$params = array(
				'report_type' => $reportType,
				'export' => $request->get('export', '0') == '1',
				'export_type' => $request->get('export_type', 'csv'),
				'export_file' => $request->get('export_file', '0') == '1',
				'filter' => $request->get('filter', array())
			);

			$db = $this->getDb();
			$settings = $this->getSettings();

			$report = null;

			switch ($reportType)
			{
				case 'orders': $report = new Admin_Reports_Report_SalesPerformance($db, $settings); break;
				case 'dates':  $report = new Admin_Reports_Report_SalesPerformanceSummary($db, $settings); break;
				case 'payment': $report = new Admin_Reports_Report_SalesByPaymentTypes($db, $settings); break;
				case 'customers': $report = new Admin_Reports_Report_SalesByCustomer($db, $settings); break;

				case 'products': $report = new Admin_Reports_Report_ProductPerformance($db, $settings); break;
				case 'tax': $report = new Admin_Reports_Report_Taxes($db, $settings); break;
				case 'promo': $report = new Admin_Reports_Report_PromoCampaigns($db, $settings); break;
				case 'qr_campaigns': $report = new Admin_Reports_Report_QRCampaigns($db, $settings); break;

//				case 'shift4': $report = new Admin_Reports_Report_Shift4($db, $settings); break;
//				case 'shift4_failures': $report = new Admin_Reports_Report_Shift4Failures($db, $settings); break;
				case 'recurring_profiles': $report = new Admin_Reports_Report_RecurringProfiles($db, $settings); break;
				case 'recurring_orders': $report = new Admin_Reports_Report_RecurringOrders($db, $settings); break;
				case 'recurring_failures': $report = new Admin_Reports_Report_RecurringFailures($db, $settings); break;
			}

			$reportResult = $report->prepare($reportType, 'reporting.reports.' . $reportType, $dateFrom, $dateTo, $params);

			if ($reportResult != null)
			{
				if ($params['export'])
				{
					switch ($params['export_type'])
					{
						case 'csv': $this->exportCsv($reportResult); break;
						case 'xml': $this->exportXml($reportResult); break;
						case 'json': $this->exportJson($reportResult); break;
					}
				}
				else
				{
					$this->renderReport($reportResult, $rangeType);
				}
			}
		}
	}

	/**
	 * Get summary data
	 */
	public function summaryAction()
	{
		$request = $this->getRequest();

		$summaryTypes = $request->get('summary_type', array());
		if (!is_array($summaryTypes)) $summaryTypes = explode(',', $summaryTypes);

		$result = array();

		if (is_array($summaryTypes) && count($summaryTypes))
		{
			$statsRepository = $this->getStatsRepository();

			/**
			 * Get dates
			 */
			$dateFrom = new DateTime($request->get('from', 'now'));
			$dateTo = new DateTime($request->get('to', 'now'));
			$rangeType = $request->get('range_type', 'custom');

			$diff = $dateFrom->diff($dateTo);

			$dateFromPrev = clone $dateFrom;
			$dateFromPrev->sub($diff);

			$dateToPrev = clone $dateFrom;
			$dateToPrev->modify('-1 day');

			$result = array();
			foreach ($summaryTypes as $summaryType)
			{
				$value = null;

				switch ($summaryType)
				{
					case 'orders_this_period': $value = $statsRepository->getPlacedOrdersCountPerPeriod($dateFrom, $dateTo); break;
					case 'orders_prev_period': $value = $statsRepository->getPlacedOrdersCountPerPeriod($dateFromPrev, $dateToPrev); break;
					case 'revenue_this_period': $value = $statsRepository->getTotalRevenuePerPeriod($dateFrom, $dateTo); break;
					case 'revenue_prev_period': $value = $statsRepository->getTotalRevenuePerPeriod($dateFromPrev, $dateToPrev); break;
					case 'conversions_this_period':
					{
						$placedCount = $statsRepository->getPlacedOrdersCountPerPeriod($dateFrom, $dateTo);
						$attemptsCount = $statsRepository->getCheckoutAttemptsCountPerPeriod($dateFrom, $dateTo);
						$value = round(($attemptsCount > 0 ? ($placedCount / $attemptsCount) : 0) * 100, 2);
						break;
					}
					case 'conversions_prev_period':
					{
						$placedCount = $statsRepository->getPlacedOrdersCountPerPeriod($dateFromPrev, $dateToPrev);
						$attemptsCount = $statsRepository->getCheckoutAttemptsCountPerPeriod($dateFromPrev, $dateToPrev);
						$value = round(($attemptsCount > 0 ? ($placedCount / $attemptsCount) : 0) * 100, 2);
						break;
					}
				}

				$result[$summaryType] = $value;
			}
		}

		$this->renderJson($result);
	}

	/**
	 * @param $date
	 * @return DateTime
	 */
	protected function prepareDateFrom($date)
	{
		if (is_array($date) || $date == null)
		{
			$dateY = date('Y');
			$dateM = date('n');
			$dateD = 1;

			if (isset($date['year'])) $dateY = intval($date['year']);
			if (isset($date['month'])) $dateM = intval($date['month']);
			if (isset($date['day'])) $dateD = intval($date['day']);

			return new DateTime($dateY . '-' . $dateM . '-' . $dateD . ' 00:00:00');
		}

		return new DateTime($date . ' 00:00:00');
	}

	/**
	 * @param $date
	 * @return DateTime
	 */
	protected function prepareDateTo($date)
	{
		if (is_array($date) || $date == null)
		{
			$dateY = date('Y');
			$dateM = date('n');
			$dateD = date('t', mktime(1, 1, 1, $dateM, 1, $dateY));

			if (isset($date['year'])) $dateY = intval($date['year']);
			if (isset($date['month'])) $dateM = intval($date['month']);
			if (isset($date['day'])) $dateD = intval($date['day']);

			return new DateTime($dateY . '-' . $dateM . '-' . $dateD . ' 00:00:00');
		}

		return new DateTime($date . ' 23:59:59');
	}

	/**
	 * @param Admin_Reports_Result $result
	 */
	protected function exportCsv(Admin_Reports_Result $result)
	{
		$fileName = $result->type . '-' . $result->dateFrom->format('Y-m-d') . '-' . $result->dateTo->format('Y-m-d') . '.csv';

		header('Content-Type: text/csv');
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		if ($result->params['export_file']) header('Content-Disposition: inline; filename="' . $fileName . '"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		if ($result->params['export_file']) header('Content-Disposition: attachment; filename="' . $fileName . '"');

		/**
		 * Header
		 */
		$headerLine = array();
		foreach ($result->fields as $field)
		{
			$headerLine[] =  $this->escapeCsvDataColumn(trans($field['title']));
		}
		echo implode(',', $headerLine) . "\n";

		/**
		 * Data
		 */

		foreach ($result->dataRaw as $data)
		{
			$dataLine = array();

			foreach ($result->fields as $fielId => $field)
			{
				$dataLine[] = $this->escapeCsvDataColumn($data[$fielId]);
			}
			echo implode(',', $dataLine) . "\n";
		}

		die();
	}

	/**
	 * @param $value
	 * @return string
	 */
	protected function escapeCsvDataColumn($value)
	{
		return '"' . str_replace(array("\t", "\r", "\n", "\""), array("\\t", " ", " ", "\"\""), $value) . '"';
	}

	/**
	 * @param Admin_Reports_Result $result
	 */
	protected function exportXml(Admin_Reports_Result $result)
	{
		$fileName = $result->type . '-' . $result->dateFrom->format('Y-m-d') . '-' . $result->dateTo->format('Y-m-d') . '.xml';

		header('Content-Type: text/xml');
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		if ($result->params['export_file']) header('Content-Disposition: inline; filename="' . $fileName . '"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		if ($result->params['export_file']) header('Content-Disposition: attachment; filename="' . $fileName . '"');

		echo '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>' . "\n";

		echo "<report>\n" .
			"\t<title>" . trans($result->title) . ' - ' . $result->dateFrom->format('F j, Y') . ' - ' . $result->dateTo->format('F j, Y') . "</title>\n" .
			"\t<range>\n" .
			"\t\t<from>" . $result->dateFrom->format('Y-m-d') . "</from>\n" .
			"\t\t<to>" . $result->dateTo->format('Y-m-d') . "</to>\n" .
			"\t</range>\n" .
			"\t<fields>\n";

		foreach ($result->fields as $fieldKey => $field)
		{
			echo
				"\t\t<field>" .
				"\t\t\t<id>" . $this->escapeXmlDataColumn($fieldKey) . "</id>\n" .
				"\t\t\t<title>" . $this->escapeXmlDataColumn(trans($field['title'])) . "</title>\n" .
				"\t\t</field>";

		}
		echo "\t</fields>\n";

		echo
			"\t<data>\n".
			"\t\t<" . $result->itemGroupName. ">\n";

		foreach ($result->dataRaw as $line)
		{
			echo "\t\t\t<" . $result->itemName . ">\n";

			foreach ($result->fields as $fieldKey => $field)
			{
				echo "\t\t\t<" . $fieldKey . ">" . $this->escapeXmlDataColumn($line[$fieldKey]) . "</" . $fieldKey . ">\n";
			}

			echo "\t\t\t</" . $result->itemName . ">\n";
		}

		echo
			"\t\t</" . $result->itemGroupName. ">\n" .
			"\t</data>\n";

		echo '</report>';

		die();
	}

	/**
	 * @param $value
	 * @return string
	 */
	protected function escapeXmlDataColumn($value)
	{
		return htmlspecialchars($value);
	}

	/**
	 * @param Admin_Reports_Result $result
	 */
	protected function exportJson(Admin_Reports_Result $result)
	{
		$fileName = $result->type . '-' . $result->dateFrom->format('Y-m-d') . '-' . $result->dateTo->format('Y-m-d') . '.json';

		header('Content-Type: application/json');
		header('Expires: '.gmdate('D, d M Y H:i:s').' GMT');
		if ($result->params['export_file']) header('Content-Disposition: inline; filename="' . $fileName . '"');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		if ($result->params['export_file']) header('Content-Disposition: attachment; filename="' . $fileName . '"');

		$fields = array();
		foreach ($result->fields as $fieldKey => $field)
		{
			$fields[$fieldKey] = trans($field['title']);
		}

		echo json_encode(
			array(
				'title' => trans($result->title) . ' - ' . $result->dateFrom->format('F j, Y') . ' - ' . $result->dateTo->format('F j, Y'),
				'title_shorten' => trans($result->title) . ' - ' . $result->dateFrom->format('M j, Y') . ' - ' . $result->dateTo->format('M j, Y'),
				'title_xsmall' => trans($result->title) . ' - ' . $result->dateFrom->format('m-d-Y') . ' - ' . $result->dateTo->format('m-d-Y'),
				'range' => array(
					'from' => $result->dateFrom->format('Y-m-d'),
					'to' => $result->dateTo->format('Y-m-d'),
				),
				'fields' => $fields,
				'data' => array($result->itemGroupName => $result->dataRaw)
			)
		);
		die();
	}

	/**
	 * @param Admin_Reports_Result $result
	 * @param $rangeType
	 */
	protected function renderReport(Admin_Reports_Result $result, $rangeType)
	{
		$view = $this->getView();

		$view->assign('report', $result);
		$view->assign('report_dates', $result->dateFrom->format('F j, Y') . ' - ' . $result->dateTo->format('F j, Y'));

		$view->assign('reportType', $result->type);
		$view->assign('reportRangeType', $rangeType);
		$view->assign('reportDateFrom', $result->dateFrom->format('Y-m-d'));
		$view->assign('reportDateTo', $result->dateTo->format('Y-m-d'));

		$view->assign('activeMenuItem', array('primary' => 'reports', 'secondary' => 'reports'));
		$view->assign('helpTag', '4000'); // TODO: assign correct help tag
		$view->assign('body', 'templates/pages/reports/report.html');
		$view->render('layouts/default');
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @return core_Form_Form
	 * @throws Exception
	 */
	protected function getSearchForm(DateTime $dateFrom, DateTime $dateTo)
	{
		$searchFormBuilder = new core_Form_FormBuilder('search');
		$searchFormBuilder->add('from', 'text', array('label' => 'From', 'value' => $dateFrom->format('Y-m-d'), 'wrapperClass' => 'col-sm-12 col-md-2'));
		$searchFormBuilder->add('to', 'text', array('label' => 'To', 'value' => $dateTo->format('Y-m-d'), 'wrapperClass' => 'col-sm-12 col-md-2'));

		$searchForm = $searchFormBuilder->getForm();

		return $searchForm;
	}

	/**
	 * @return DataAccess_StatRepository
	 */
	protected function getStatsRepository()
	{
		if ($this->statsRepository == null)
		{
			$this->statsRepository = new DataAccess_StatRepository($this->getDb());
		}

		return $this->statsRepository;
	}
}
<?php

/**
 * Class Admin_Controller_GoogleMerchant
 */
class Admin_Controller_GoogleMerchant extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * List action
	 */
	public function indexAction()
	{
		$request = $this->getRequest();
		$action = $request->query->get('action');

		if ($request->isPost() && $action == 'export')
		{
			$exportService = new Admin_Service_GoogleMerchantExport($this->getDb(), $this->getSettings());
			$exportService->export(
				$request->request->get('export_taxes', '0'),
				intval($request->request->get('product_category', 'all')),
				intval($request->request->get('product_count', 'all')),
				intval($request->request->get('exp_days', 'default'))
			);
		}


		$categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		$data['categories'] = $categoryRepository->getOptionsList();

		$googleMerchantForm = new Admin_Form_GoogleMerchantForm();

		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9003');

		$adminView->assign('form', $googleMerchantForm->getForm($data));
		$adminView->assign('body', 'templates/pages/google-merchant/index.html');
		$adminView->render('layouts/default');
	}
}
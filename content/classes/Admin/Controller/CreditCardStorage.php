<?php

/**
 * Class Admin_Controller_CreditCardStorage
 */
class Admin_Controller_CreditCardStorage extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/credit-card-storage/';

	protected $creditCardStorageRepository = null;

	protected $settingsKeys = array('SecurityCCSActive', 'SecurityCCSStoredFor', 'SecurityCCSCertificate', 'SecurityCCSPrivateKey');

	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * Edit credit card storage settings
	 */
	public function editSettingsAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$settingsValues = $settings->getByKeys($this->settingsKeys);

		$creditCardStorageSettingsForm = new Admin_Form_CreditCardStorageSettingsForm();
		$settingsForm = $creditCardStorageSettingsForm->getForm($settingsValues);

		$certForm = new Admin_Form_CertificateForm();
		$certificateForm = $certForm->getForm($settingsValues);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'ccs_settings_edit'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings_ccs');
			}
			else
			{
				$isValid = true;
				$ccsUsername = '';
				$ccsPassword = '';

				if (isset($formData['ccsSettings']['SecurityCCSActive']) && $formData['ccsSettings']['SecurityCCSActive'] == '1')
				{
					if ($request->request->has('dn'))
					{
						global $admin;
						Admin_Log::log('Attempt to generate CCS certificate', Admin_Log::LEVEL_IMPORTANT, true, $admin['username']);

						$dn = $request->request->get('dn');
						$ccsUsername = $dn['ccs_username'];
						$ccsPassword = $dn['ccs_password'];
						$ccsPassword2 = $dn['ccs_password2'];

						if (($errorMessage = $this->validateCertParams($ccsUsername, $ccsPassword, $ccsPassword2)) != '')
						{
							Admin_Flash::setMessage($errorMessage, Admin_Flash::TYPE_ERROR);
							$isValid = false;
						}
					}
				}

				if ($isValid)
				{
					if ($ccsUsername != '' && $ccsPassword != '')
					{
						$result = $this->generateCertificate($ccsUsername, $ccsPassword);

						if (!$result)
						{
							Admin_Flash::setMessage('Error occurred while generating CA certificate.', Admin_Flash::TYPE_ERROR);
							$isValid = false;
						}
					}

					if ($isValid)
					{
						$ccsSettings = $formData['ccsSettings'];
						$ccsSettings['SecurityCCSActive'] = isset($ccsSettings["SecurityCCSActive"]) ? $ccsSettings["SecurityCCSActive"]: '0';
						$settings->save($ccsSettings, $this->settingsKeys);

						$activeStatus = isset($formData['ccsSettings']['SecurityCCSActive']) && $formData['ccsSettings']['SecurityCCSActive'] == '1' ? 'Yes' : 'No';

						// TODO: why do we enable css payment method here?
						$db = $this->getDb();
						$db->query('UPDATE '.DB_PREFIX.'payment_methods SET active = "'.$activeStatus.'" WHERE id = "cart_ccs"');

						Admin_Flash::setMessage('common.success');
						$this->redirect('settings_ccs');
					}
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8035');

		$adminView->assign('form', $settingsForm);
		$adminView->assign('certificateForm', $certificateForm);
		$adminView->assign('nonce', Nonce::create('ccs_settings_edit'));
		$adminView->assign('nonce_generate', Nonce::create('ccs_generate_certificate'));
		$adminView->assign('canAccess', AccessManager::checkAccess('CCS'));
		$adminView->assign('canRegenerate', $settingsValues['SecurityCCSCertificate'] != '');
		$adminView->assign('body', self::TEMPLATES_PATH.'edit-settings.html');

		$adminView->render('layouts/default');
	}

	/**
	 * Generate Certificate
	 */
	public function generateAction()
	{
		$request = $this->getRequest();

		$result = array();

		if (AccessManager::checkAccess('CCS'))
		{
			if (!Nonce::verify($request->get('nonce_generate'), 'ccs_generate_certificate', false))
			{
				$result['status'] = 'Error';
				$result['errors'] = trans('common.invalid_nonce');

				$this->renderJson($result);
			}

			if (!($temp_dn = $request->get('dn')))
			{
				$result['status'] = 'Error';
				$result['errors'] = 'Required parameter missing';

				$this->renderJson($result);
			}

			$ccsUsername = $temp_dn['ccs_username'];
			$ccsPassword = $temp_dn['ccs_password'];
			$ccsPassword2 = $temp_dn['ccs_password2'];

			$errorMessage = $this->validateCertParams($ccsUsername, $ccsPassword, $ccsPassword2);

			if ($errorMessage == '')
			{
				if ($this->generateCertificate($ccsUsername, $ccsPassword))
				{
					$result['status'] = 'Success';
					$result['errors'] = '';
				}
				else
				{
					$errorMessage = 'Error occurred while generating CA certificate.';
				}
			}

			if ($errorMessage != '')
			{
				$result['status'] = 'Error';
				$result['errors'] = $errorMessage;
			}
		}
		else 
		{
			$result['status'] = 'Error';
			$result['errors'] = trans('common.access_denied');
		}

		$this->renderJson($result);
	}

	/**
	 * Validate certificate params
	 *
	 * @param $ccs_username
	 * @param $ccs_password
	 * @param $ccs_password2
	 *
	 * @return string
	 */
	protected function validateCertParams($ccs_username, $ccs_password, $ccs_password2)
	{
		$repository = $this->getCreditCardStorageRepository();
		return $repository->validateCertParams($ccs_username, $ccs_password, $ccs_password2);
	}

	/**
	 * Generate certificate
	 *
	 * @param $ccsUsername
	 * @param $ccsPassword
	 *
	 * @return bool
	 */
	protected function generateCertificate($ccsUsername, $ccsPassword)
	{
		$settings = $this->getSettings();

		$companyName = $settings->get('CompanyName');

		if (trim($companyName) == '')
		{
			$companyName = str_replace(array('http://', 'https://'), array('', ''), $settings->get('GlobalHttpsUrl'));
		}

		$dn = array(
			'countryName' => 'US',
			'stateOrProvinceName' => 'Arizona',
			'localityName' => 'Phoenix',
			'organizationName' => 'PC Cart',
			'organizationalUnitName' => $companyName.' Unit',
			'commonName' => $companyName,
			'emailAddress' => $settings->get('CompanyEmail')
		);

		$openSSL = new openSSL(4096, OPENSSL_KEYTYPE_RSA, 3650, $settings->get('GlobalServerPath').'/content/ssl/openssl.cnf');

		if (!$openSSL->generate($dn, $ccsUsername.$ccsPassword))
		{
			return false;
		}
		else
		{
			global $admin;
			$repository = $this->getCreditCardStorageRepository();

			Admin_Log::log('CCS certificate updated', Admin_Log::LEVEL_IMPORTANT, true, $admin['username']);
			$repository->updateCCSkeys($openSSL->csr_pem, $openSSL->private_key_pem);
			$settings->set('SecurityCCSCertificate', base64_encode($openSSL->csr_pem));
			$settings->set('SecurityCCSPrivateKey', base64_encode($openSSL->private_key_pem));

			return true;
		}
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CreditCardStorageRepository
	 */
	protected function getCreditCardStorageRepository()
	{
		if ($this->creditCardStorageRepository == null)
		{
			$this->creditCardStorageRepository = new DataAccess_CreditCardStorageRepository($this->getDb());
		}

		return $this->creditCardStorageRepository;
	}
}
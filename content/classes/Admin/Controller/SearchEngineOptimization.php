<?php

/**
 * Class Admin_Controller_SearchOptimization
 */
class Admin_Controller_SearchEngineOptimization extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/search-engine-optimization/';

	protected $Repository = null;

	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'seo');

	/**
	 * Generate .htaccess
	 */
	public function generateHtaccessAction()
	{
		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$data = $repository->getAdvancedSettingsData();

		if (!Nonce::verify($request->get('nonce_generate_htaccess'), 'generate_htaccess'))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('settings_search');
		}

		try
		{
			Seo::generateHtaccessFile(($data['USE_MOD_REWRITE'] == 'YES' ? 'On' : 'Off'));
			Admin_Flash::setMessage('Success! .htaccess file has been generated');
		}
		catch (Exception $e)
		{
			Admin_Flash::setMessage('Please note: Error writing ".htaccess" file. Please check file permissions.', Admin_Flash::TYPE_ERROR);
		}
		$this->redirect('settings_search');
	}

	/**
	 * Regenerate Urls
	 * @param $data
	 */
	public function regenerateUrlsAction($data)
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce_regenerate_urls'), 'regenerate_urls'))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('settings_search');
		}

		/**
		 * Generate flat URLs
		 */
		Seo::updateSeoURLs($this->db, $this->getSettings()->getAll(), $data['updateCategories'], $data['updateProducts'], $data['updatePages'], $data['updateManufacturers']);

		/**
		 * Generate unique URL's
		 */
		$unique_count = Seo::generateUniqueUrls();
		$note_message = ($unique_count > 0)
			? 'There was '.$unique_count.' duplicate URL(s) to process.'
			: 'There were '.$unique_count.' duplicate URLs to process.';
		Admin_Flash::setMessage('Success! Search engine settings have been updated. ' . $note_message);
		$this->redirect('settings_search');
	}

	/**
	 * Edit search optimization settings
	 */
	public function editSettingsAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$data = $repository->getAdvancedSettingsData();

		if (!$data)
		{
			Admin_Flash::setMessage('Cannot find global settings', Admin_Flash::TYPE_ERROR);
			$this->redirect('settings_search');
		}

		$robots = $repository->getRobots();
		if (isset($robots))
		{
			$data['robots'] = $robots;
		}

		$TRForm = new Admin_Form_SearchEngineOptimizationSettingsForm();

		$form = $TRForm->getForm($data);
		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'settings_edit'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings_search');
			}
			else
			{
				if (($validationErrors = $repository->getValidationSettingsErrors($formData, $mode)) == false)
				{
					$repository->persistSearchOptimizationSettings($formData);

					if (isset($formData['searchSettings']['SearchAutoGenerateLowercase']))
					{
						$toLower = $data['SearchAutoGenerateLowercase'] == $formData['searchSettings']['SearchAutoGenerateLowercase'] ? false : true;
					}
					else
					{
						$toLower = $data['SearchAutoGenerateLowercase'] == "NO" ? false : true;
					}
					$check['updateManufacturers'] = ($data['SearchURLManufacturerTemplate'] == $formData['searchSettings']['SearchURLManufacturerTemplate']) ?  $toLower : true;
					$check['updateCategories'] = ($data['SearchURLCategoryTemplate'] == $formData['searchSettings']['SearchURLCategoryTemplate']) ? $toLower : true;
					$check['updateProducts'] = ($data['SearchURLProductTemplate'] == $formData['searchSettings']['SearchURLProductTemplate']) ? $toLower : true;
					$check['updatePages'] = ($data['SearchURLPageTemplate'] == $formData['searchSettings']['SearchURLPageTemplate']) ? $toLower : true;

					if ($formData['generate_htaccess_action'] == 'Yes')
					{
						Admin_Log::log('SEO: .htaccess generated', Admin_Log::LEVEL_IMPORTANT);
						$this->generateHtaccessAction();
					}
					if ($formData['regenerate_urls_action'] == 'Yes' || (0 < count(array_filter($check, function($value) { return $value !== false; }))))
					{
						Admin_Log::log('SEO: URLs regenerated', Admin_Log::LEVEL_IMPORTANT);
						$this->regenerateUrlsAction($check);
					}

					Admin_Flash::setMessage('common.success');
					Admin_Log::log('SEO: Settings search updated', Admin_Log::LEVEL_IMPORTANT);
					$this->redirect('settings_search', array());
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5013');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('settings_edit'));
		$adminView->assign('nonce_generate_htaccess', Nonce::create('generate_htaccess'));
		$adminView->assign('nonce_regenerate_urls', Nonce::create('regenerate_urls'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit-settings.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_SearchEngineOptimizationRepository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->Repository))
		{
			$this->Repository = new DataAccess_SearchEngineOptimizationRepository($this->getDb(), $this->getSettings()->getAll());
		}
		return $this->Repository;
	}
}
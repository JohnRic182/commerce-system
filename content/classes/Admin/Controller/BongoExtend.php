<?php

class Admin_Controller_BongoExtend extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'bongo-extend'));
				return;
			}

			$extendJS = $request->request->get('bongocheckout_ExtendJS');

			$settings->persist(array('bongocheckout_ExtendJS' => $extendJS));

			Admin_Flash::setMessage('common.success');
			$this->redirect('app', array('key' => 'bongo-extend'));
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9014');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$bongoExtendForm = new Admin_Form_BongoExtendForm();
		$form = $bongoExtendForm->getForm($settings);

		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/bongo-extend/index.html');
		$adminView->render('layouts/default');
	}
}
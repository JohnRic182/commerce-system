<?php

/**
 * Class Admin_Controller_GlobalProductAttributes
 */
class Admin_Controller_GlobalProductAttributes extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/global-attributes/';

	const NONCE_GLOBAL_ATTRIBUTES_DELETE = 'globalattributes_delete';
	const NONCE_GLOBAL_ATTRIBUTES_UPDATE = 'globalattributes_update';
	const NONCE_GLOBAL_ATTRIBUTES_ADD = 'globalattributes_add';

	/** @var DataAccess_GlobalAttributeRepository  */
	protected $globalProductAttributesRepository = null;

	/** @var DataAccess_CategoryRepository  */
	protected $categoryRepository = null;

	/** @var DataAccess_ProductsRepository */
	protected $productsRepository = null;

	protected $menuItem = array('primary' => 'products', 'secondary' => 'products-attributes');

	/**
	 * List action
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);

		$gpaRepository = $this->getGlobalProductAttributesRepository();

		$request = $this->getRequest();

		$orderBy = $request->get('orderBy', 'name');

		$itemsCount = $gpaRepository->getCount();

		$adminView->assign('itemsCount', $itemsCount);
 
		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$attributesList = $gpaRepository->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy);
			$TypeOptions =  $gpaRepository->getAttributeTypesList();

			foreach($attributesList as &$attr)
			{
				$attr['attribute_type'] = $TypeOptions[$attr['attribute_type']];
			}

			$adminView->assign('globalattributes', $attributesList);
		}
		else
		{
			$adminView->assign('globalattributes', null);
		}

		$adminView->assign('delete_nonce', Nonce::create(self::NONCE_GLOBAL_ATTRIBUTES_DELETE));
		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add action
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$gpaRepository = $this->getGlobalProductAttributesRepository();

		$gpaForm = new Admin_Form_GlobalAttributesForm();

		$defaults = $gpaRepository->getDefaults();
		$defaults['is_active'] = 'Yes';

		$form = $gpaForm->getForm($defaults, $this->getCategoryRepository()->getOptionsList(), $gpaRepository->getAttributeTypesList(), $mode, null);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$current_categories = $request->request->get('categories');
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], self::NONCE_GLOBAL_ATTRIBUTES_ADD))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_attributes');
			}
			else
			{
				if (($validationErrors = $gpaRepository->getValidationErrors($formData, $mode)) == false)
				{
					$formData['products'] = explode(",",$formData['products_assigned']);
					$id = $gpaRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('products_attribute', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create(self::NONCE_GLOBAL_ATTRIBUTES_ADD));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update action
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$gpaRepository = $this->getGlobalProductAttributesRepository();

		$id = intval($request->get('id'));

		$attribute = $gpaRepository->getById($id);

		if (!$attribute)
		{
			Admin_Flash::setMessage('Cannot find attribute by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_attributes');
		}

		$gpaForm = new Admin_Form_GlobalAttributesForm();

		$defaults = $gpaRepository->getDefaults();
		$form = $gpaForm->getForm($attribute, $this->getCategoryRepository()->getOptionsList(), $gpaRepository->getAttributeTypesList(), $mode, $id);

		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], self::NONCE_GLOBAL_ATTRIBUTES_UPDATE))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_attributes');
			}
			else
			{
				if (($validationErrors = $gpaRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$formData['products'] = explode(",",$formData['products_assigned']);
					$result = $gpaRepository->persist($formData);

					if ($result == 'removed')
					{
						Admin_Flash::setMessage('Global attribute has been successfully removed!');
						$this->redirect('products_attributes');
					}
					else
					{
						Admin_Flash::setMessage('common.success');
						$this->redirect('products_attribute', array('id' => $id, 'mode' => self::MODE_UPDATE));
					}
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('title', $attribute['name']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create(self::NONCE_GLOBAL_ATTRIBUTES_UPDATE));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete action
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		$gpaRepository = $this->getGlobalProductAttributesRepository();

		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce'), self::NONCE_GLOBAL_ATTRIBUTES_DELETE))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_attributes');
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);

			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid attribute id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$gpaRepository->delete(array($id));
				Admin_Flash::setMessage('common.success');
				$this->redirect('products_attributes');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one attribute to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_attributes');
			}
			else
			{
				$gpaRepository->delete($ids);
				Admin_Flash::setMessage('Selected attributes have been deleted');
				$this->redirect('products_attributes');
			}
		}
		else if ($deleteMode == 'all')
		{
			$gpaRepository->delete('all');
			Admin_Flash::setMessage('All attributes have been deleted');
			$this->redirect('products_attributes');
		}
	}

	/**
	 * Products auto sugges
	 */
	public function getProductsAction()
	{
		$request = $this->getRequest();

		$searchStr = trim($request->get('str', ''));

		$products = $searchStr != '' ? $this->getProductRepository()->lookUp($searchStr, '') : array();

		$this->renderJson(array('status' => 1, 'products' => $products));
	}

	/**
	 * @return DataAccess_CategoryRepository
	 */
	protected function getCategoryRepository()
	{
		if ($this->categoryRepository === null)
		{
			$this->categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		}

		return $this->categoryRepository;
	}

	/**
	 * @return DataAccess_ProductsRepository
	 */
	protected function getProductRepository()
	{
		if ($this->productsRepository === null)
		{
			$this->productsRepository = new DataAccess_ProductsRepository($this->getDb());
		}

		return $this->productsRepository;
	}

	/**
	 * @return DataAccess_GlobalAttributeRepository
	 */
	protected function getGlobalProductAttributesRepository()
	{
		if ($this->globalProductAttributesRepository === null)
		{
			$db = $this->getDb();
			$this->globalProductAttributesRepository = new DataAccess_GlobalAttributeRepository(
				$db, new DataAccess_ProductsRepository($db), new DataAccess_ProductAttributeRepository($db)
			);
		}

		return $this->globalProductAttributesRepository;
	}
}
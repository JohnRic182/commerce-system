<?php
/**
 * Class Admin_Controller_Avalara
 */
class Admin_Controller_Avalara extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$this->checkCompanyAddressSettings();

		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array(
			'AvalaraEnableTax', 'AvalaraAccountNumber', 'AvalaraLicenseKey',
			'AvalaraCompanyCode', 'AvalaraCurrencyCode', 'AvalaraURL',
		);

		$settingsData = $settings->getByKeys($settingsVars);
		$settingsData['isActivateAction'] = false;

		// avalara form
		$avalaraForm = new Admin_Form_AvalaraForm();
		$form = $avalaraForm->getAvalaraForm($settingsData);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'avalara'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['AvalaraAccountNumber']) || trim($settingsData['AvalaraAccountNumber']) == '')
			{
				$errors['AvalaraAccountNumber'] = array(trans('apps.avalara.account_number_req'));
			}

			if (!isset($settingsData['AvalaraLicenseKey']) || trim($settingsData['AvalaraLicenseKey']) == '')
			{
				$errors['AvalaraLicenseKey'] = array(trans('apps.avalara.license_key_req'));
			}

			if (!isset($settingsData['AvalaraCompanyCode']) || trim($settingsData['AvalaraCompanyCode']) == '')
			{
				$errors['AvalaraCompanyCode'] = array(trans('apps.avalara.currency_code_req'));
			}

			if (!isset($settingsData['AvalaraURL']) || trim($settingsData['AvalaraURL']) == '')
			{
				$errors['AvalaraURL'] = array(trans('apps.avalara.avalara_webservice_url_req'));
			}

			if ($settings->get('ExactorEnableTax') == 'Yes')
			{
				$errors['ExactorEnableTax'] = array(trans('apps.avalara.exactor_enabled'));
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'avalara'));
			}
			else
			{
				$form->bind($settingsData);
				$form->bindErrors($errors);
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9021');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/avalara/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Activate form action
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('AvalaraEnableTax', 'AvalaraAccountNumber', 'AvalaraLicenseKey', 'AvalaraCompanyCode', 'AvalaraURL');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['AvalaraAccountNumber']) || trim($settingsData['AvalaraAccountNumber']) == '')
			{
				$errors[] = array('field' => '.field-AvalaraAccountNumber', 'message' => trans('apps.avalara.account_number_req'));
			}
			if (!isset($settingsData['AvalaraLicenseKey']) || trim($settingsData['AvalaraLicenseKey']) == '')
			{
				$errors[] = array('field' => '.field-AvalaraLicenseKey', 'message' => trans('apps.avalara.license_key_req'));
			}
			if (!isset($settingsData['AvalaraCompanyCode']) || trim($settingsData['AvalaraCompanyCode']) == '')
			{
				$errors[] = array('field' => '.field-AvalaraCompanyCode', 'message' => trans('apps.avalara.currency_code_req'));
			}
			if (!isset($settingsData['AvalaraURL']) || trim($settingsData['AvalaraURL']) == '')
			{
				$errors[] = array('field' => '.field-AvalaraURL', 'message' => trans('apps.avalara.avalara_webservice_url_req'));
			}

			if ($settings->get('ExactorEnableTax') == 'Yes')
			{
				$errors[] = array('field' => '', 'message' => trans('apps.avalara.exactor_enabled'));
			}

			if (count($errors) < 1)
			{
				$settingsData['AvalaraEnableTax'] = 'Yes';

				//TODO: Test connection and display error message if not enabled

				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('avalara');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		// avalara form
		$avalaraForm = new Admin_Form_AvalaraForm();
		$settingsData['isActivateAction'] = true;

		$adminView = $this->getView();

		$adminView->assign('form', $avalaraForm->getAvalaraForm($settingsData));

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('AvalaraEnableTax' => 'No'));
	}

	/**
	 * Check are company settings ready
	 */
	protected function checkCompanyAddressSettings()
	{
		$settings = $this->getSettings();

		if (
			$settings->get('CompanyName') == 'Company Name' || $settings->get('CompanyName') == '' ||
			$settings->get('CompanyAddressLine1') == 'Address Line 1' || $settings->get('CompanyAddressLine1') == '' ||
			$settings->get('CompanyState') == 'State' || $settings->get('CompanyState') == '' ||
			$settings->get('CompanyZip') == '-' || $settings->get('CompanyZip') == ''
		)
		{
			Admin_Flash::setMessage(
				'To calculate taxes using Avalara, please make sure you have updated your <a href="admin.php?p=settings&group=1">Company Information</a> under Global Cart Settings. '.
				'Please note: Avalara requires a valid address',
				Admin_Flash::TYPE_ERROR
			);
		}
	}

	/**
	 * Manage tax codes
	 */
	public function taxCodesAction()
	{
		$adminView = $this->getView();

		// avalara form
		$avalaraForm = new Admin_Form_AvalaraForm();
		$data = array('name' => '', 'code' => '');

		$adminView->assign('taxCodeForm', $avalaraForm->getAvalaraTaxCodeForm($data));

		$adminView->assign('addNonce', Nonce::create('tax_code_add'));
		$adminView->assign('updateNonce', Nonce::create('tax_code_update'));

		$db = $this->getDb();

		$taxCodes = $db->selectAll('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes');
		$adminView->assign('tax_codes', $taxCodes);

		$adminView->assign('itemsCount', count($taxCodes));

		$adminView->assign('hasAvalaraTaxCodes', Tax_Avalara_AvalaraTaxCodeHelper::getTaxCodesCount() > 0);
		$adminView->assign('update_codes_nonce', Nonce::create('tax_update_tax_codes'));
		$adminView->assign('delete_nonce', Nonce::create('tax_code_delete'));
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9022');

		$adminView->assign('body', 'templates/pages/avalara/tax-codes.html');
		$adminView->render('layouts/default');
	}

	public function addTaxCodeAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'tax_code_add', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('apps.avalara.cannot_verify'),
				));
			}
			else
			{
				$taxName = $request->request->get('tax_code_name');
				$taxCode = $request->request->get('tax_code_code');

				$validationErrors = array();
				if (trim($taxCode) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_code', 'message' => trans('apps.avalara.please_enter_tax_code'));
				}
				if (trim($taxName) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_name', 'message' => trans('apps.avalara.please_enter_tax_name'));
				}

				if (count($validationErrors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
				else
				{
					$db = $this->getDb();
					$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes WHERE code = \''.$this->db->escape($taxCode).'\'');

					if (!$result)
					{
						$db->reset();
						$db->assignStr('code', $taxCode);
						$db->assignStr('name', $taxName);
						$db->insert(DB_PREFIX.'avalara_tax_codes');

						Admin_Flash::setMessage('common.success');

						$this->renderJson(array(
							'status' => 1
						));
					}
					else
					{
						$this->renderJson(array(
							'message' => trans('apps.avalara.tax_code_exists'),
						));
					}
				}
			}
		}
	}

	public function updateTaxCodeAction()
	{
		$request = $this->getRequest();

		$id = intval($request->get('id'));

		$db = $this->getDb();
		$taxCode = $db->selectOne('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes WHERE id = '.$id);

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'tax_code_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('apps.avalara.cannot_verify'),
				));
			}
			else
			{
				$taxName = $request->request->get('tax_code_name');
				$taxCode = $request->request->get('tax_code_code');

				$validationErrors = array();
				if (trim($taxCode) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_code', 'message' => trans('apps.avalara.please_enter_tax_code'));
				}
				if (trim($taxName) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_name', 'message' => trans('apps.avalara.please_enter_tax_name'));
				}

				if (count($validationErrors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
				else
				{
					$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes WHERE id <> '.$id.' AND code = \''.$this->db->escape($taxCode).'\'');

					if (!$result)
					{
						$db->reset();
						$db->assignStr('code', $taxCode);
						$db->assignStr('name', $taxName);
						$db->update(DB_PREFIX.'avalara_tax_codes', 'WHERE id = '.$id);

						Admin_Flash::setMessage('common.success');

						$this->renderJson(array(
							'status' => 1
						));
					}
					else
					{
						$this->renderJson(array(
							'message' => trans('apps.avalara.tax_code_exists'),
						));
					}
				}
			}
		}

		$adminView = $this->getView();

		// avalara form
		$avalaraForm = new Admin_Form_AvalaraForm();

		$adminView->assign('form', $avalaraForm->getAvalaraTaxCodeForm($taxCode));

		$adminView->render('generic-form');
	}

	public function addAvalaraTaxCodeAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'tax_code_add', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('apps.avalara.cannot_verify'),
				));
			}
			else
			{
				$taxCode = $request->request->get('tax_code_code');

				$validationErrors = array();
				if (trim($taxCode) == '')
				{
					$validationErrors[] = array('field' => 'tax_code_code', 'message' => trans('apps.avalara.please_enter_tax_code'));
				}

				if (count($validationErrors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
				else
				{
					$db = $this->getDb();
					$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes WHERE code = \''.$this->db->escape($taxCode).'\'');

					if (!$result)
					{

						$result = $db->selectOne('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes_categories WHERE tax_code = "'.$db->escape($taxCode).'"');
						if (!$result)
						{
							$this->renderJson(array(
								'status' => 0,
								'message' => trans('apps.avalara.not_found'),
							));
						}
						else
						{
							$taxName = $result['description'];

							$db->reset();
							$db->assignStr('code', $taxCode);
							$db->assignStr('name', $taxName);
							$db->insert(DB_PREFIX.'avalara_tax_codes');

							Admin_Flash::setMessage('common.success');

							$this->renderJson(array(
								'status' => 1
							));
						}
					}
					else
					{
						$this->renderJson(array(
							'status' => 0,
							'message' => trans('apps.avalara.tax_code_exists'),
						));
					}
				}
			}
		}
	}

	public function deleteAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'tax_code_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('avalara', array('mode' => 'tax-codes'));
			return;
		}

		$db = $this->getDb();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid tax code id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$this->removeTaxCode($id);

				Admin_Flash::setMessage('Tax Code has been deleted');
				$this->redirect('avalara', array('mode' => 'tax-codes'));
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one tax code to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('avalara', array('mode' => 'tax-codes'));
			}
			else
			{
				foreach ($ids as $id)
				{
					$this->removeTaxCode($id);
				}

				Admin_Flash::setMessage('Selected tax code have been deleted');
				$this->redirect('avalara', array('mode' => 'tax-codes'));
			}
		}
		else if ($deleteMode == 'search')
		{
			$db->query('DELETE FROM '.DB_PREFIX.'avalara_tax_codes');

			$db->query("UPDATE ".DB_PREFIX."products SET avalara_tax_code = ''  WHERE avalara_tax_code != ''");
			$db->query("UPDATE ".DB_PREFIX."catalog SET avalara_tax_code = ''  WHERE avalara_tax_code != ''");

			Admin_Flash::setMessage('Selected tax codes have been deleted');
			$this->redirect('avalara', array('mode' => 'tax-codes'));
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);

			$this->redirect('avalara', array('mode' => 'tax-codes'));
		}
	}

	protected function removeTaxCode($id)
	{
		$id = intval($id);
		if ($id > 0)
		{
			$db = $this->getDb();
			$code = $db->selectOne('SELECT code FROM '.DB_PREFIX.'avalara_tax_codes WHERE id = '.$id);

			if ($code)
			{
				$code = array_shift($code);

				$db->query('DELETE FROM '.DB_PREFIX.'avalara_tax_codes WHERE id = '.$id);

				$db->query("UPDATE ".DB_PREFIX."products SET avalara_tax_code = ''  WHERE avalara_tax_code = '".$db->escape($code)."'");
				$db->query("UPDATE ".DB_PREFIX."catalog SET avalara_tax_code = ''  WHERE avalara_tax_code = '".$db->escape($code)."'");
			}
		}
	}

	/**
	 * Update tax codes action
	 */
	public function updateTaxCodesAction()
	{
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'tax_update_tax_codes', false))
		{
			$this->renderJson(array(
				'status' => 0,
				'message' => trans('apps.avalara.cannot_verify'),
			));
		}
		else
		{
			$avalaraUsername = trim($request->request->get('avalaraUsername'));
			$avalaraPassword = trim($request->request->get('avalaraPassword'));

			if (Tax_Avalara_AvalaraTaxCodeHelper::updateTaxCodes($avalaraUsername, $avalaraPassword))
			{
				Admin_Flash::setMessage('common.success');
				$this->renderJson(array(
					'status' => 1,
				));
			}
			else
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('apps.avalara.cannot_update'),
				));
			}
		}
	}

	public function taxCodesTreeAction()
	{
		$repository = new Tax_Avalara_AvalaraRepository($this->getDb());
		$tree = $repository->getTaxCodeCategoriesTree();

		$render = new Tax_Avalara_TaxCodeCategoryNodeRenderer();
		$render->render($tree);
	}
}
<?php
/**
 * Class Admin_Controller_Stamps
 */
class Admin_Controller_Stamps extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$this->checkCompanyAddressSettings();

		$request = $this->getRequest();

		$settings = $this->getSettings();

		$stamps = new Stampscom($this->getSettings());

		//Ensure that stamps account information is loaded prior to handling post request
		$stamps->refreshAccountInformation();

		$settingsVars = array('StampscomActive', 'StampscomUsername', 'StampscomTestMode', 'StampscomPassword', 'StampscomSCANFormEnable');

		$settingsData = $settings->getByKeys($settingsVars);
		$formBuilder = new core_Form_FormBuilder('settings');

		$settingsGroup = new core_Form_FormBuilder('basic');
		$formBuilder->add($settingsGroup);

		$settingsGroup->add('StampscomUsername', 'text', array('label' => 'stamps.username', 'placeholder' => 'stamps.username', 'required' => true, 'value' => $settingsData['StampscomUsername']));
		$settingsGroup->add('StampscomPassword', 'password', array('label' => 'stamps.password', 'placeholder' => 'stamps.password', 'required' => true, 'value' => $settingsData['StampscomPassword']));
		if (defined('DEVMODE') && DEVMODE)
			$settingsGroup->add('StampscomTestMode', 'checkbox', array('label' => 'stamps.test_mode', 'value' => 'Yes', 'current_value' => $settingsData['StampscomTestMode']));

		if ($stamps->canGenerateScanForms())
			$settingsGroup->add('StampscomSCANFormEnable', 'checkbox', array('label' => 'stamps.scan_form_enable', 'value' => 'Yes', 'current_value' => $settingsData['StampscomSCANFormEnable']));

		$form = $formBuilder->getForm();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'stamps'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['StampscomUsername']) || trim($settingsData['StampscomUsername']) == '')
			{
				$errors[] = array('field' => 'field-StampscomUsername', 'message' => trans('stamps.username_required'));
			}
			if (!isset($settingsData['StampscomPassword']) || trim($settingsData['StampscomPassword']) == '')
			{
				$errors[] = array('field' => 'field-StampscomPassword', 'message' => trans('stamps.password_required'));
			}

			if (!$request->request->has('StampscomTestMode'))
			{
				$settingsData['StampscomTestMode'] = 'No';
			}
			if (!$request->request->has('StampscomSCANFormEnable'))
			{
				$settingsData['StampscomSCANFormEnable'] = 'No';
			}

			if (count($errors) < 1)
			{
				$settings->set('StampscomActive', 'Yes');
				$settings->set('StampscomUsername', $settingsData['StampscomUsername']);
				$settings->set('StampscomPassword', $settingsData['StampscomPassword']);
				$settings->set('StampscomTestMode', $settingsData['StampscomTestMode']);

				$response = $stamps->apiMethodAuthenticateUser();
				if (!($response && !isset($response['faultcode']) && isset($response['LoginBannerText'])))
				{
					$errors[] = array('field' => '', 'message' => trans('stamps.connection_error'));
				}
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'stamps'));
			}
			else
			{
				$form->bind($settingsData);
				$form->bindErrors($errors);
				Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($errors), Admin_Flash::TYPE_ERROR);
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9027');

		$adminView->assign('nonce', Nonce::create('update_settings'));
		$adminView->assign('form', $form);


		$postage_purchase_status = false;
		$session = $this->getSession();
		if ($session->has('STAMPS_POSTAGE_TRANSACTION_ID'))
		{
			$stamps_arguments = array('TransactionID' => $session->get('STAMPS_POSTAGE_TRANSACTION_ID'));
			$stamps_return = $stamps->apiMethodGetPurchaseStatus($stamps_arguments);

			$postage_purchase_status = 'Processing';
			if ($stamps_return && isset($stamps_return['PurchaseStatus']))
			{
				$postage_purchase_status = $stamps_return['PurchaseStatus'];

				if ($postage_purchase_status != 'Processing')
				{
					$session->remove('STAMPS_POSTAGE_TRANSACTION_ID');
				}
			}
		}
		$adminView->assign('purchasePostageStatus', $postage_purchase_status);

		$adminView->assign('canPurchasePostage', $stamps->canPurchasePostage());
		$adminView->assign('addFundsForm', $this->getAddFundsForm($settingsData));
		$adminView->assign('addNonce', Nonce::create('stamps_purchase_postage'));

		$adminView->assign('canCarrierPickup', $stamps->canCarrierPickup());
		$adminView->assign('canGenerateScanForms', $stamps->canGenerateScanForms());

		$adminView->assign('currentBalance', $stamps->getPostageBalance());

		$adminView->assign('body', 'templates/pages/stamps/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Activate action
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('StampscomActive', 'StampscomUsername', 'StampscomTestMode', 'StampscomPassword');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['StampscomUsername']) || trim($settingsData['StampscomUsername']) == '')
			{
				$errors[] = array('field' => '.field-StampscomUsername', 'message' => trans('stamps.username_required'));
			}

			if (!isset($settingsData['StampscomPassword']) || trim($settingsData['StampscomPassword']) == '')
			{
				$errors[] = array('field' => '.field-StampscomPassword', 'message' => trans('stamps.password_required'));
			}

			if (!$request->request->has('StampscomTestMode'))
			{
				$settingsData['StampscomTestMode'] = 'No';
			}

			if (count($errors) < 1)
			{
				$settings->set('StampscomActive', 'Yes');
				$settings->set('StampscomUsername', $settingsData['StampscomUsername']);
				$settings->set('StampscomPassword', $settingsData['StampscomPassword']);
				$settings->set('StampscomTestMode', $settingsData['StampscomTestMode']);

				$stamps = new Stampscom($this->getSettings());

				$response = $stamps->apiMethodAuthenticateUser();
				if (!($response && !isset($response['faultcode']) && isset($response['LoginBannerText'])))
				{
					$errors[] = array('field' => '', 'message' => trans('stamps.connection_error'));
				}
			}

			if (count($errors) < 1)
			{
				$settingsData['StampscomActive'] = 'Yes';

				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('stamps');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$stampsForm = new Admin_Form_StampsForm();
		$form = $stampsForm->getStampsActivateForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	/**
	 * Deactivate action
	 */
	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('StampscomActive' => 'No'));
	}

	/**
	 * Scan forms action
	 */
	public function scanFormsAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$scanShipmentDate = false;
			if ($request->query->has('search_date'))
			{
				$scanShipmentDate = date_parse($request->query->get('search_date', date('Y-m-d')));
			}

			if (!$scanShipmentDate || !is_array($scanShipmentDate))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('stamps.invalid_date'),
				));
				return;
			}

			$scanShipmentDateYear = intval($scanShipmentDate['year']);
			$scanShipmentDateMonth = intval($scanShipmentDate['month']);
			$scanShipmentDateDay = intval($scanShipmentDate['day']);

			$scanShipmentDate =
				$scanShipmentDateYear.'-'.
				($scanShipmentDateMonth < 10 ? '0' : '').$scanShipmentDateMonth.'-'.
				($scanShipmentDateDay < 10 ? '0' : '').$scanShipmentDateDay;

			$settings = $this->getSettings();
			$db = $this->getDb();
			$db->query('SELECT short_name FROM '.DB_PREFIX.'states WHERE coid='.intval($settings->get('ShippingOriginCountry')).' AND stid='.intval($settings->get('ShippingOriginState')));

			if ($db->moveNext())
			{
				$stamps_tmp_arguments_state = $db->col['short_name'];
			}
			else
			{
				$stamps_tmp_arguments_state = $settings->get('ShippingOriginProvince');
			}

			$stamps_tmp_arguments['ShipDate'] = $scanShipmentDate;

			$stamps_tmp_arguments['FromAddress'] = array(
				'name_' => $settings->get('ShippingOriginName'),
				'address_1_' => $settings->get('ShippingOriginAddressLine1'),
				'address_2_' => $settings->get('ShippingOriginAddressLine2'),
				'city_' => $settings->get('ShippingOriginCity'),
				'state_' => $stamps_tmp_arguments_state,
				'zip_code_' => $settings->get('ShippingOriginZip'),
			);
			$stamps_tmp_arguments['ImageType'] = 'Png';
			$stamps_tmp_arguments['PrintInstructions'] = 'true';

			$stamps = new Stampscom($settings);
			$stamps_return = $stamps->apiCall('CreateScanForm', $stamps_tmp_arguments);

			if (isset($stamps_return['faultcode']))
			{
				$stamps_msg_scanform = 'Error generating SCAN form';

				if (isset($stamps_return['faultstring']))
				{
					$stamps_msg_scanform .= ": ".$stamps_return['faultstring'];
				}

				$this->renderJson(array(
					'status' => 0,
					'message' => $stamps_msg_scanform,
				));
			}
			elseif (isset($stamps_return['Url']))
			{
				$stampsRepository = $this->getStampsRepository();

				$stamps_scanform_urls = explode(' ', $stamps_return['Url']);

				$stampsRepository->saveScanForm(array(
					'date' => $scanShipmentDate,
					'code' => $stamps_return['ScanFormId'],
					'urls' => $stamps_scanform_urls
				));

				$this->renderJson(array(
					'status' => 1,
					'urls' => $stamps_scanform_urls,
				));
			}

			return;
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9029');

		$scanSearchDate = false;
		if ($request->query->has('search_date'))
		{
			$scanSearchDate = date_parse($request->query->get('search_date', date('Y-m-d')));
		}

		if (!$scanSearchDate && !is_array($scanSearchDate))
		{
			$scanSearchDate = date_parse(date('Y-m-d'));
		}
		$scanSearchDateYear = intval($scanSearchDate['year']);
		$scanSearchDateMonth = intval($scanSearchDate['month']);
		$scanSearchDateDay = intval($scanSearchDate['day']);

		$scanSearchDate =
			$scanSearchDateYear.'-'.
			($scanSearchDateMonth < 10 ? '0' : '').$scanSearchDateMonth.'-'.
			($scanSearchDateDay < 10 ? '0' : '').$scanSearchDateDay;

		$stampsRepository = $this->getStampsRepository();

		$currentYear = date('Y') + 1;
		$startYear = $currentYear - 11;
		$scanFormSearchBuilder = new core_Form_FormBuilder('scan-search');
		$scanFormSearchDateGroup = new core_Form_FormBuilder('scan-search-basic');
		$scanFormSearchBuilder->add($scanFormSearchDateGroup);
		$scanFormSearchDateGroup->add('search_date', 'date', array('label' => 'stamps.search_date', 'value' => isset($scanSearchDate) ? $scanSearchDate : '', 'yearStart' => $startYear, 'yearFinish' => $currentYear));

		$adminView->assign('scanFormSearch', $scanFormSearchBuilder->getForm());

		$scanFormGenerateBuilder = new core_Form_FormBuilder('scan-generate');
		$scanFormGenerateDateGroup = new core_Form_FormBuilder('scan-generate-basic');
		$scanFormGenerateBuilder->add($scanFormGenerateDateGroup);
		$scanFormGenerateDateGroup->add('shipment_date', 'date', array('label' => 'stamps.shipment_date', 'value' => isset($scanShipmentDate) ? $scanShipmentDate : ''));

		$adminView->assign('generateScanForm', $scanFormGenerateBuilder->getForm());

		$adminView->assign('scanFormsDate', date('M d, Y', mktime(0, 0, 0, $scanSearchDateMonth, $scanSearchDateDay, $scanSearchDateYear)));
		$adminView->assign('scanForms', $stampsRepository->getScanFormsByDate($scanSearchDate));

		$adminView->assign('body', 'templates/pages/stamps/scan-forms.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Carrier pickup action
	 */
	public function carrierPickupAction()
	{
		global $admin;
		$request = $this->getRequest();

		$settings = $this->getSettings();
		$db = $this->getDb();

		$pickupDate = $request->request->get('pickup_date', date_parse(date('Y-m-d')));

		if (!$pickupDate && !is_array($pickupDate))
		{
			$pickupDate = date_parse(date('Y-m-d'));
		}
		$pickupDateYear = intval($pickupDate['year']);
		$pickupDateMonth = intval($pickupDate['month']);
		$pickupDateDay = intval($pickupDate['day']);
		$pickupDate = $pickupDateYear.'-'.$pickupDateMonth.'-'.$pickupDateDay;

		$numberOfExpressMailPieces = $request->get('pickup_express_mail_pieces', 0);
		$numberOfPriorityMailPieces = $request->get('pickup_priority_mail_pieces', 0);
		$numberOfInternationalPieces = $request->get('pickup_international_mail_pieces', 0);
		$numberOfOtherPieces = $request->get('pickup_other_mail_pieces', 0);
		$totalWeightOfPackagesLbs = ceil($request->get('pickup_total_weight', 0));

		$pickupFirstName = $request->get('pickup_first_name', '');
		$pickupLastName = $request->get('pickup_last_name', '');
		$pickupCompany = $request->get('pickup_company', $settings->get('ShippingOriginName'));
		$pickupAddress = $request->get('pickup_address', $settings->get('ShippingOriginAddressLine1'));
		$pickupSuite = $request->get('pickup_suite', $settings->get('ShippingOriginAddressLine2'));
		$pickupCity = $request->get('pickup_city', $settings->get('ShippingOriginCity'));
		$pickupState = $request->get('pickup_state', '');
		$pickupZip = $request->get('pickup_zip', $settings->get('ShippingOriginZip'));
		$pickupZip4 = $request->get('pickup_zi4', '');
		$pickupPhone = preg_replace('/[^0-9]/', '', $request->get('pickup_phone', $settings->get('ShippingOriginPhone')));

		if (strlen($pickupPhone) > 10)
		{
			if ($pickupPhone[0] == '1') $pickupPhone = substr($pickupPhone, 1);
			$pickupPhone = substr($pickupPhone, 0, 10);
		}

		$pickupPhoneExt = $request->get('pickup_phone_ext', '');
		$pickupLocation = $request->get('pickup_location', 'Office');
		$pickupInstructions = $request->get('pickup_instructions', '');

		if ($pickupFirstName == '' && $pickupLastName == '')
		{
			$a = explode(' ', trim($admin['name']));
			if (count($a) > 0)
			{
				$pickupFirstName = $a[0]; unset($a[0]);
				$pickupLastName = implode(' ', $a);
			}
		}

		if ($request->isPost())
		{
			$action = $request->request->get('action', 'save');

			if ($action == 'pre-populate')
			{
				$shipmentsForPickup = $db->selectAll($q = 'SELECT * FROM '.DB_PREFIX.'orders_labels WHERE type="stampscom" AND shipping_date="'.$db->escape($pickupDate).'" AND status="Created" AND tracking_number <> ""');

				if ($shipmentsForPickup)
				{
					foreach ($shipmentsForPickup as $shipment)
					{
						switch ($shipment['shipping_service_type'])
						{
							// express mail
							case 'US-XM' : $numberOfExpressMailPieces++; break;

							// priority mail
							case 'US-PM' : $numberOfPriorityMailPieces++; break;

							// international mail
							case 'US-EMI' :
							case 'US-PMI' :
							case 'US-FCI' : $numberOfInternationalPieces++; break;

							// other mail pieces
							default : $numberOfOtherPieces++; break;
						}

						$totalWeightOfPackagesLbs += $shipment['shipping_weight'];
					}

					$totalWeightOfPackagesLbs = ceil($totalWeightOfPackagesLbs);
				}
				else
				{
					Admin_Flash::setMessage('stamps.settings.pickup.no_results_for_date', Admin_Flash::TYPE_INFO);
				}
			}
			else
			{
				$arguments = array(
					'FirstName' => $pickupFirstName,
					'LastName' => $pickupLastName,
					'Company' => $pickupCompany,
					'Address' => $pickupAddress,
					'SuiteOrApt' => $pickupSuite,
					'City' => $pickupCity,
					'State' => $pickupState,
					'ZIP' => $pickupZip,
					'ZIP4' => $pickupZip4,
					'PhoneNumber' => $pickupPhone,
					'PhoneExt' => $pickupPhoneExt,
					'NumberOfExpressMailPieces' => $numberOfExpressMailPieces,
					'NumberOfPriorityMailPieces' => $numberOfPriorityMailPieces,
					'NumberOfInternationalPieces' => $numberOfInternationalPieces,
					'NumberOfOtherPieces' => $numberOfOtherPieces,
					'TotalWeightOfPackagesLbs' => ceil($totalWeightOfPackagesLbs),
					'PackageLocation' => $pickupLocation,
					'SpecialInstruction' => $pickupInstructions
				);

				$stamps = new Stampscom($settings);
				$result = $stamps->apiMethodCarrierPickup($arguments);

				if ($result)
				{
					if (isset($result['faultstring']) && trim($result['faultstring']) != '')
					{
						$this->renderJson(array(
							'status' => 0,
							'message' => $result['faultstring'],
						));
						return;
					}
					else if (isset($result['ErrorMsg']) && trim($result['ErrorMsg']) != '')
					{
						$this->renderJson(array(
							'status' => 0,
							'message' => $result['faultstring'],
						));
						return;
					}
					else
					{
						Admin_Flash::setMessage(
							'stamps.settings.pickup.request_sent'.
							isset($result['ConfirmationNumber']) && trim($result['ConfirmationNumber']) != '' ? $result['ConfirmationNumber'] : 'n/a'
						);
						$this->renderJson(array(
							'status' => 1,
						));
						return;
					}
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'message' => trans('stamps.settings.pickup.error_cannot_get_data'),
					));

					return;
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9028');

		$pickupFormBuilder = new core_Form_FormBuilder('pickup');

		$pickupOptionsGroup = new core_Form_FormBuilder('pickup_options', array('label' => 'stamps.settings.pickup.title_options', 'class'=>'ic-truck', 'first' => true));
		$pickupFormBuilder->add($pickupOptionsGroup);

		$currentYear = date('Y') + 2;
		$startYear = $currentYear - 11;
		$pickupOptionsGroup->add('pickup_date', 'date', array('label' => 'stamps.settings.pickup.pickup_date', 'value' => $pickupDate, 'note' => 'stamps.settings.pickup.pickup_date_note', 'yearStart' => $startYear, 'yearFinish' => $currentYear));

		$pickupOptionsGroup->add('pickup_express_mail_pieces', 'text', array('label' => 'stamps.settings.pickup.express_mail_pieces', 'value' => $numberOfExpressMailPieces, 'attr' => array('class' => 'tiny mail-piece')));
		$pickupOptionsGroup->add('pickup_priority_mail_pieces', 'text', array('label' => 'stamps.settings.pickup.priority_mail_pieces', 'value' => $numberOfPriorityMailPieces, 'attr' => array('class' => 'tiny mail-piece')));
		$pickupOptionsGroup->add('pickup_international_mail_pieces', 'text', array('label' => 'stamps.settings.pickup.international_mail_pieces', 'value' => $numberOfInternationalPieces, 'attr' => array('class' => 'tiny mail-piece')));
		$pickupOptionsGroup->add(
			'pickup_other_mail_pieces', 'text',
			array('label' => 'stamps.settings.pickup.other_mail_pieces', 'note'=>'stamps.settings.pickup.other_mail_pieces_note', 'value' => $numberOfOtherPieces, 'attr' => array('class' => 'tiny mail-piece'))
		);
		$pickupOptionsGroup->add(
			'pickup_total_weight', 'text',
			array('label' => 'stamps.settings.pickup.total_weight', 'required' => true, 'note'=>'stamps.settings.pickup.total_weight_note', 'value' => $totalWeightOfPackagesLbs, 'attr' => array('class' => 'tiny'))
		);

		$pickupAddressGroup = new core_Form_FormBuilder('pickup_address', array('label' => 'stamps.settings.pickup.title_address', 'class'=>'ic-location'));
		$pickupFormBuilder->add($pickupAddressGroup);

		$pickupAddressGroup->add('pickup_first_name', 'text', array('label' => 'stamps.settings.pickup.first_name', 'value' => $pickupFirstName, 'required' => true, 'attr' => array('maxlength' => 50)));
		$pickupAddressGroup->add('pickup_last_name', 'text', array('label' => 'stamps.settings.pickup.last_name', 'value' => $pickupLastName, 'required' => true, 'attr' => array('maxlength' => 50)));
		$pickupAddressGroup->add('pickup_company', 'text', array('label' => 'stamps.settings.pickup.company', 'value' => $pickupCompany, 'attr' => array('maxlength' => 50)));
		$pickupAddressGroup->add('pickup_address', 'text', array('label' => 'stamps.settings.pickup.address', 'value' => $pickupAddress, 'required' => true, 'attr' => array('maxlength' => 50)));
		$pickupAddressGroup->add('pickup_suite', 'text', array('label' => 'stamps.settings.pickup.suite', 'value' => $pickupSuite, 'attr' => array('maxlength' => 10, 'class' => 'short')));
		$pickupAddressGroup->add('pickup_city', 'text', array('label' => 'stamps.settings.pickup.city', 'value' => $pickupCity, 'required' => true, 'attr' => array('maxlength' => 30)));

		$usStates = $db->selectAll('SELECT stid, name, short_name FROM '.DB_PREFIX.'states WHERE coid=1 ORDER BY name');
		$statesOptions = array();
		foreach ($usStates as $usState)
		{
			$statesOptions[] = new core_Form_Option($usState['short_name'], $usState['name']);
			if ($pickupState == '' && $usState['stid'] == $settings->get('ShippingOriginState'))
			{
				$pickupState = $usState['short_name'];
			}
		}

		$pickupAddressGroup->add('pickup_state', 'choice', array('label' => 'stamps.settings.pickup.state', 'value' => $pickupState, 'options' => $statesOptions, 'required' => true));
		$pickupAddressGroup->add('pickup_zip', 'text', array('label' => 'stamps.settings.pickup.zip', 'value' => $pickupZip, 'required' => true, 'attr' => array('class' => 'short', 'maxlength' => 5)));
		$pickupAddressGroup->add('pickup_zip4', 'text', array('label' => 'stamps.settings.pickup.zip4', 'value' => $pickupZip4, 'attr' => array('class' => 'short', 'maxlength' => 4)));
		$pickupAddressGroup->add('pickup_phone', 'text', array('label' => 'stamps.settings.pickup.phone', 'value' => $pickupPhone, 'required' => true, 'attr' => array('class' => 'short', 'maxlength' => 10), 'note' => 'stamps.settings.pickup.phone_note'));
		$pickupAddressGroup->add('pickup_phone_ext', 'text', array('label' => 'stamps.settings.pickup.phone_ext', 'value' => $pickupPhoneExt, 'attr' => array('class' => 'short', 'maxlength' => 6)));
		$pickupAddressGroup->add('pickup_location', 'choice', array(
			'label' => 'stamps.settings.pickup.pickup_location',
			'value' => $pickupLocation,
			'options' => array(
				new core_Form_Option('FrontDoor', 'stamps.settings.pickup.pickup_location_options.FrontDoor'),
				new core_Form_Option('BackDoor', 'stamps.settings.pickup.pickup_location_options.BackDoor'),
				new core_Form_Option('SideDoor', 'stamps.settings.pickup.pickup_location_options.SideDoor'),
				new core_Form_Option('KnockOnDoorOrRingBell', 'stamps.settings.pickup.pickup_location_options.KnockOnDoorOrRingBell'),
				new core_Form_Option('MailRoom', 'stamps.settings.pickup.pickup_location_options.MailRoom'),
				new core_Form_Option('Office', 'stamps.settings.pickup.pickup_location_options.Office'),
				new core_Form_Option('Reception', 'stamps.settings.pickup.pickup_location_options.Reception'),
				new core_Form_Option('InOrAtMailbox', 'stamps.settings.pickup.pickup_location_options.InOrAtMailbox'),
				new core_Form_Option('Other', 'stamps.settings.pickup.pickup_location_options.Other')
			),
			'note' => 'stamps.settings.pickup.pickup_location_options_note',
			'attr' => array('class' => 'large')
		));

		$pickupAddressGroup->add('pickup_instructions', 'textarea', array('label' => 'stamps.settings.pickup.instructions', 'value' => $pickupInstructions));

		$adminView->assign('form', $pickupFormBuilder->getForm());

		$adminView->assign('nonce', Nonce::create('carrier_pickup_nonce'));

		$adminView->assign('body', 'templates/pages/stamps/carrier-pickup.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Purchase postage action
	 */
	public function purchasePostageAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'stamps_purchase_postage', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			else
			{
				$errors = array();

				$amount = floatval($request->request->get('addfunds_amount'));

				if ($amount <= 0)
				{
					$errors[] = array('field' => '.field-addfunds_amount', 'message' => trans('stamps.amount_required'));
				}

				if (count($errors) > 0)
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $errors,
					));
					return;
				}
				else
				{
					$stamps = new Stampscom($this->getSettings());

					if (!$stamps->canPurchasePostage())
					{
						$errors[] = array('field' => '', 'message' => trans('stamps.cannot_purchase_postage'));

						$this->renderJson(array(
							'status' => 0,
							'errors' => $errors,
						));
						return;
					}
					else
					{
						$stamps_arguments = array( 'PurchaseAmount' => $amount, 'ControlTotal' => $stamps->getPostageControlTotal());
						$stamps_return = $stamps->apiMethodPurchasePostage($stamps_arguments);

						$message = false;
						if (!$stamps_return)
						{
							$message = trans('stamps.error_connecting');
						}
						elseif (isset($stamps_return['faultcode']))
						{
							if ($stamps_return['faultcode'] === 'soap:Server' && isset($stamps_return['detail']) && isset($stamps_return['detail']['sdcerror']) && isset($stamps_return['detail']['sdcerror']['!code']) && intval($stamps_return['detail']['sdcerror']['!code']) === 450116)
							{
								$message = 'Stamps.com: Your current postage balance does not match that of Stamps.com. This can be caused by something that changes your postage balance, such as another postage purchase or generating a label. Please reload this page to update the local balance and try your purchase again';
							}
							elseif (isset($stamps_return['faultstring']))
							{
								$message = $stamps_return['faultstring'];
							}
						}

						if (!$message)
						{
							$postage_purchase_transactionid = 0;
							if (isset($stamps_return['TransactionID']))
							{
								$postage_purchase_transactionid = $stamps_return['TransactionID'];
							}
							$postage_purchase_status = 'Processing';

							if (!isset($stamps_return['PurchaseStatus']) || $stamps_return['PurchaseStatus'] === 'Rejected')
							{
								if (isset($stamps_return['RejectionReason']))
								{
									$message = gs($stamps_return['RejectionReason']);
									$this->renderJson(array(
										'status' => 0,
										'message' => $message,
									));
									return;
								}
							}

							$stamps_arguments = array('TransactionID' => $postage_purchase_transactionid);
							$stamps_return = $stamps->apiMethodGetPurchaseStatus($stamps_arguments);

							if ($stamps_return && isset($stamps_return['PurchaseStatus']))
							{
								$postage_purchase_status = $stamps_return['PurchaseStatus'];
							}

							if ($postage_purchase_status == 'Success')
							{
								Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
							}
							else
							{
								$session = $this->getSession();
								$session->set('STAMPS_POSTAGE_TRANSACTION_ID', $postage_purchase_transactionid);
							}

							$this->renderJson(array(
								'status' => 1,
							));
						}
						else
						{
							$this->renderJson(array(
								'status' => 0,
								'message' => $message,
							));
							return;
						}
					}
				}
			}
		}
	}

	/**
	 * Generate scan form action
	 */
	public function generateScanFormAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$scanShipmentDate = $request->request->get('shipment_date', date_parse(date('Y-m-d H:i:s')));
		$scanShipmentDateYear = intval($scanShipmentDate['year']);
		$scanShipmentDateMonth = intval($scanShipmentDate['month']);
		$scanShipmentDateDay = intval($scanShipmentDate['day']);

		$scanShipmentDate =
			$scanShipmentDateYear.'-'.
			($scanShipmentDateMonth < 10 ? '0' : '').$scanShipmentDateMonth.'-'.
			($scanShipmentDateDay < 10 ? '0' : '').$scanShipmentDateDay;

		$db = $this->getDb();
		$db->query('SELECT short_name FROM '.DB_PREFIX.'states WHERE coid='.intval($settings->get('ShippingOriginCountry')).' AND stid='.intval($settings->get('ShippingOriginState')));

		if ($db->moveNext())
		{
			$stamps_tmp_arguments_state = $db->col['short_name'];
		}
		else
		{
			$stamps_tmp_arguments_state = $settings->get('ShippingOriginProvince');
		}

		$stamps_tmp_arguments['ShipDate'] = $scanShipmentDate;

		$stamps_tmp_arguments['FromAddress'] = array(
			'name_' => $settings->get('ShippingOriginName'),
			'address_1_' => $settings->get('ShippingOriginAddressLine1'),
			'address_2_' => $settings->get('ShippingOriginAddressLine2'),
			'city_' => $settings->get('ShippingOriginCity'),
			'state_' => $stamps_tmp_arguments_state,
			'zip_code_' => $settings->get('ShippingOriginZip'),
		);
		$stamps_tmp_arguments['ImageType'] = 'Png';
		$stamps_tmp_arguments['PrintInstructions'] = 'true';

		$stamps = new Stampscom($settings);
		$stamps_return = $stamps->apiCall('CreateScanForm', $stamps_tmp_arguments);

		if (isset($stamps_return['faultcode']))
		{
			$stamps_msg_scanform = 'Error generating SCAN form';

			if (isset($stamps_return['faultstring']))
			{
				$stamps_msg_scanform .= ": ".$stamps_return['faultstring'];
			}

			$this->renderJson(array(
				'status' => 0,
				'message' => $stamps_msg_scanform,
			));
		}
		elseif (isset($stamps_return['Url']))
		{
			/** @var DataAccess_StampsRepository $stampsRepository */
			$stampsRepository = DataAccess_Repository::get('StampsRepository');

			$stamps_scanform_urls = explode(' ', $stamps_return['Url']);

			$stampsRepository->saveScanForm(array(
				'date' => $scanShipmentDate,
				'code' => $stamps_return['ScanFormId'],
				'urls' => $stamps_scanform_urls
			));

			$this->renderJson(array(
				'status' => 1,
				'scanform_urls' => $stamps_scanform_urls,
			));
		}
	}

	/**
	 * @param array $settingsData
	 * @return core_Form_Form
	 */
	protected function getAddFundsForm(array $settingsData)
	{
		$formBuilder = new core_Form_FormBuilder('settings');

		$settingsGroup = new core_Form_FormBuilder('basic');
		$formBuilder->add($settingsGroup);

		$settingsGroup->add('addfunds_amount', 'text', array('label' => 'stamps.amount', 'required' => true, 'value' => ''));

		return $formBuilder->getForm();
	}

	/**
	 * Check are company settings ready
	 */
	protected function checkCompanyAddressSettings()
	{
		$settings = $this->getSettings();

		if (
			$settings->get('ShippingOriginAddressLine1') == '' || $settings->get('ShippingOriginCity') == 'City' ||
			$settings->get('ShippingOriginState') == '0' || $settings->get('ShippingOriginState') == '' ||
			$settings->get('ShippingOriginZip') == '-' || $settings->get('ShippingOriginZip') == ''
		)
		{
			Admin_Flash::setMessage(
				'To create a label through Stamps.com, please make sure that you have filled in all required fields under <a href="admin.php?p=shipping&mode=settings">Shipping Origin Address</a>. '.
				'Please note: Stamps.com may not work with P.O. Box addresses',
				Admin_Flash::TYPE_ERROR
			);
		}
	}

	/**
	 * @return DataAccess_StampsRepository
	 */
	protected function getStampsRepository()
	{
		return new DataAccess_StampsRepository($this->getDb());
	}
}
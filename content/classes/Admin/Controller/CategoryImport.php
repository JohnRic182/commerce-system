<?php

/**
 * Class Admin_Controller_CategoryImport
 */
class Admin_Controller_CategoryImport extends Admin_Controller
{
	/** @var DataAccess_CategoryRepository $categoryRepository */
	protected $categoryRepository = null;

	/** @var Admin_Service_CategoryImport $categoryImportService */
	protected $categoryImportService = null;

	/** @var array $defaults */
	protected $defaults = array(
		'fields_separator' => ',',
		'make_categories_visible' => 'Yes',
		'make_categories_visible_type' => 'Yes'
	);

	protected $menuItem = array('primary' => 'products', 'secondary' => 'categories');

	/**
	 * Display categories CSV upload form
	 */
	public function startAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Admin_Form_CategoryImportForm $categoryImportForm */
		$categoryImportForm = new Admin_Form_CategoryImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $categoryImportForm->getStartForm(array());

		if ($request->isPost())
		{
			$formData = array_merge($this->defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('categories');
			}
			else
			{
				/** @var Admin_Service_CategoryImport $importService */
				$importService = $this->getCategoryImportService();

				$errors = array();

				if ($fileInfo = $request->getUploadedFile('bulk'))
				{
					if ($uploadedFile = $importService->saveUploadedFile($fileInfo))
					{
						/** @var Framework_Session $session */
						$session = $this->getSession();

						$session->set('category-import-settings', array(
							'file' => $uploadedFile,
							'fields_separator' => $formData['fields_separator'],
							'make_categories_visible' => $formData['make_categories_visible'],
							'make_categories_visible_type' => $formData['make_categories_visible_type']
						));

						$this->redirect('bulk_categories', array('action' => 'assign'));
					}
					else
					{
						$errors['bulk'][] = trans('categories.import_cannot_save_file');
					}
				}
				else
				{
					$errors['bulk'][] = trans('categories.no_file_selected');
				}

				$form->bind($formData);
				$form->bindErrors($errors);
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3009');

		$adminView->assign('page', 'bulk_categories');
		$adminView->assign('pageCancel', 'categories');
		$adminView->assign('pageTitle', trans('categories.import_categories'));

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('category_import_start'));

		$adminView->assign('body', 'templates/pages/import/start.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Display fields assignment form
	 */
	public function assignAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = $session->get('category-import-settings', $this->defaults);

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('categories.cannot_find_uploaded_file', Admin_Flash::TYPE_ERROR);
			$this->redirect('categories');
		}

		/** @var array $formData */
		$formData = array_merge($importSettings, $this->defaults);

		/** @var Admin_Service_CategoryImport $importService */
		$importService = $this->getCategoryImportService();

		/** @var array $assignOptions */
		$assignOptions = $importService->getAssignOptions($uploadedFile, $formData['fields_separator']);

		$formData = array_merge($formData, $assignOptions);

		$formData['skip_first_line'] = count($assignOptions['columns']) > 1 ? '1' : '0';

		/** @var Admin_Form_CategoryImportForm $categoryImportForm */
		$categoryImportForm = new Admin_Form_CategoryImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $categoryImportForm->getAssignForm($formData);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('fields', $fields = $importService->getFields());
		$adminView->assign('fieldsJson', json_encode($fields));
		$adminView->assign('assignOptions', $assignOptions);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3010');

		$adminView->assign('page', 'bulk_categories');
		$adminView->assign('pageCancel', 'categories');
		$adminView->assign('pageTitle', trans('categories.import_categories'));

		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/import/assign.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Do import and finish
	 */
	public function importAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = $session->get('category-import-settings', $this->defaults);

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('categories.cannot_find_uploaded_file', Admin_Flash::TYPE_ERROR);
			$this->redirect('categories');
		}

		/** @var array $formData */
		$formData = $request->request->all();

		/** @var Admin_Service_CategoryImport $importService */
		$importService = $this->getCategoryImportService();

		$result = $importService->import(
			$uploadedFile, $formData['assign_fields'],
			$importSettings['fields_separator'],
			isset($formData['skip_first_line']) && $formData['skip_first_line'] == 1,
			array(
				'makeVisible' => $importSettings['make_categories_visible'] == 'Yes',
				'makeVisibleExisting' => $importSettings['make_categories_visible_type'] != 'inserted'
			)
		);

		Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), true, false, false, false);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3011');

		$adminView->assign('page', 'bulk_categories');
		$adminView->assign('pageCancel', 'categories');
		$adminView->assign('pageTitle', trans('categories.import_categories'));

		if ($result)
		{
			/** @var Admin_Form_CategoryImportForm $categoryImportForm */
			$categoryImportForm = new Admin_Form_CategoryImportForm();

			/** @var core_Form_Form $formBuilder */
			$form = $categoryImportForm->getImportForm($result);

			$adminView->assign('form', $form);
		}
		else
		{
			Admin_Flash::setMessage('categories.import_error', Admin_Flash::TYPE_ERROR);
		}

		$adminView->assign('body', 'templates/pages/import/import.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_CategoryRepository
	 */
	protected function getCategoryRepository()
	{
		if (is_null($this->categoryRepository))
		{
			$this->categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		}

		return $this->categoryRepository;
	}

	/**
	 * Get import service
	 *
	 * @return Admin_Service_ManufacturerImport
	 */
	protected function getCategoryImportService()
	{
		if (is_null($this->categoryImportService))
		{
			$this->categoryImportService = new Admin_Service_CategoryImport(
				$this->getCategoryRepository(),
				new Admin_Service_FileUploader()
			);
		}

		return $this->categoryImportService;
	}
}
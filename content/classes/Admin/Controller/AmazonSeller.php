<?php

class Admin_Controller_AmazonSeller extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		$categories = $categoryRepository->getOptionsList();

		$maxProductsOptions = array(
			'0' => 'All Products',
			'10' => '10',
			'50' => '50',
			'100' => '100',
			'150' => '150',
		);
		for ($i = 250; $i <= 5000; $i+=250)
		{
			$maxProductsOptions[$i] = $i;
		}

		// amazon seller from
		$amazonSellerForm = new Admin_Form_AmazonSellerForm();
		$data = array('categories' => $categories, 'maxProductsOptions' => $maxProductsOptions);

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9006');

		$adminView->assign('form', $amazonSellerForm->getForm($data));

		$adminView->assign('body', 'templates/pages/amazon-seller/index.html');
		$adminView->render('layouts/default');
	}
}
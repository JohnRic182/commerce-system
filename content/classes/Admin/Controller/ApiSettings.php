<?php

/**
 * Class Admin_Controller_ApiSettings
 */
class Admin_Controller_ApiSettings extends Admin_Controller
{
	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	public function updateAction()
	{
		$request = $this->getRequest();
		$settings = $this->getSettings();

		$db = $this->getDb();
		if (!$settings->has('plugin_openapi_active'))
		{
			$db->reset();
			$db->assignStr("visible", "No");
			$db->assignStr("input_type", "hidden");
			$db->assignStr("group_name", "plugin_api");
			$db->assignStr("name", "plugin_openapi_active");
			$db->assignStr("value", "NO");
			$db->assignStr("options", "YES,NO");
			$db->insert(DB_PREFIX."settings");

			$settings->set('plugin_openapi_active', 'NO');
		}
		if (!$settings->has('plugin_openapi_username'))
		{
			$db->reset();
			$db->assignStr("visible", "No");
			$db->assignStr("input_type", "hidden");
			$db->assignStr("group_name", "plugin_api");
			$db->assignStr("name", "plugin_openapi_username");
			$db->assignStr("value", "");
			$db->insert(DB_PREFIX."settings");

			$settings->set('plugin_openapi_username', '');
		}
		if (!$settings->has('plugin_openapi_password'))
		{
			$db->reset();
			$db->assignStr("visible", "No");
			$db->assignStr("input_type", "hidden");
			$db->assignStr("group_name", "plugin_api");
			$db->assignStr("name", "plugin_openapi_password");
			$db->assignStr("value", "");
			$db->insert(DB_PREFIX."settings");

			$settings->set('plugin_openapi_password', '');
		}
		if (!$settings->has('plugin_openapi_token'))
		{
			$db->reset();
			$db->assignStr("visible", "No");
			$db->assignStr("input_type", "hidden");
			$db->assignStr("group_name", "plugin_api");
			$db->assignStr("name", "plugin_openapi_token");
			$db->assignStr("value", "");
			$db->insert(DB_PREFIX."settings");

			$settings->set('plugin_openapi_token', '');
		}

		$defaults = array(
			'plugin_openapi_active' => 'NO',
			'plugin_openapi_username' => '',
			'plugin_openapi_password' => '',
			'plugin_openapi_token' => '',
		);

		$data = $settings->getByKeys(array_keys($defaults));

		// admin log
		$apiSettingsForm = new Admin_Form_ApiSettingsForm();

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'settings_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings');
			}
			else
			{
				$formData = array();
				foreach ($data as $key => $temp)
				{
					$formData[$key] = trim($request->request->get($key, array_key_exists($key, $defaults) ? $defaults[$key] : ''));
				}
				if ($formData['plugin_openapi_password'] == '')
				{
					unset($formData['plugin_openapi_password']);
				}
				else
				{
					$formData['plugin_openapi_password'] = getPasswordHash($formData['plugin_openapi_password']);
				}

				$settings->persist($formData);
				Admin_Flash::setMessage('common.success');
				$this->redirect('api');
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8032');

		$adminView->assign('form', $apiSettingsForm->getForm($data));
		$adminView->assign('nonce', Nonce::create('settings_update'));

		$adminView->assign('body', 'templates/pages/settings/api.html');
		$adminView->render('layouts/default');
	}

	protected function ensureTimezoneSettings()
	{
		global $db, $settings;

		Timezone::setApplicationDefaultTimezone($db, $settings);
	}
}
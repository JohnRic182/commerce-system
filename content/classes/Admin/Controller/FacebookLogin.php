<?php
/**
 * Class Admin_Controller_FacebookLogin
 */
class Admin_Controller_FacebookLogin extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('facebookLogin', 'facebookAppId', 'facebookAppSecret');

		$settingsData = $settings->getByKeys($settingsVars);
		$facebookLoginForm = new Admin_Form_FacebookLoginForm();
		$form = $facebookLoginForm->getForm($settingsData);

		if ($request->isPost())
		{
			$formData = $request->request->all();
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'facebook'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);

			if (($validationErrors = $this->getValidationErrors($formData)) == false)
			{
				$settings->persist($settingsData, $settingsVars);
				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'facebook'));
			}
			else
			{
				// TODO: find better way to handle this
				$form->bind($formData);
				$form->bindErrors($validationErrors);
			}
		}



		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9017');

		$adminView->assign('nonce', Nonce::create('update_settings'));
		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/facebook-login/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get validation errors
	 * @param $data
	 * @return array|bool
	 */
	public function getValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			if (isset($data['facebookAppId']) && trim($data['facebookAppId']) == '')
			{
				$errors['facebookAppId'] = array('Facebook App Id is required.');
			}

			if (isset($data['facebookAppSecret']) && trim($data['facebookAppSecret']) == '')
			{
				$errors['facebookAppSecret'] = array('Facebook App Secret is required.');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('facebookLogin', 'facebookAppId', 'facebookAppSecret');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			$errors = array();
			if (!isset($settingsData['facebookAppId']) || trim($settingsData['facebookAppId']) == '')
			{
				$errors[] = array('field' => '.field-facebookAppId', 'message' => trans('apps.facebook.app_id_required'));
			}
			if (!isset($settingsData['facebookAppSecret']) || trim($settingsData['facebookAppSecret']) == '')
			{
				$errors[] = array('field' => '.field-facebookAppSecret', 'message' => trans('apps.facebook.app_secret_required'));
			}

			if (count($errors) < 1)
			{
				$settingsData['facebookLogin'] = 'Yes';
				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('facebook');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$facebookLoginForm = new Admin_Form_FacebookLoginForm();

		$form = $facebookLoginForm->getForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('facebookLogin' => 'No'));
	}
}
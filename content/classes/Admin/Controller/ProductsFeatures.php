<?php

/**
 * Class Admin_Controller_ProductsFeatures
 */
class  Admin_Controller_ProductsFeatures extends Admin_Controller
{
	/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
	protected $productsFeaturesRepository = null;

	protected $searchParamsDefaults = array('search_str' => '', 'feature_active' => 1, 'logic' => 'AND');

	protected $menuItem = array('primary' => 'products', 'secondary' => 'products-features');

	/**
	 * List products features
	 */
	public function listAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);

		/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
		$productsFeaturesRepository = $this->getProductsFeaturesRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** Prepare search & list parameters */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('productsFeaturesSearchParams', array()); else $session->set('productsFeaturesSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('productsFeaturesSearchOrderBy', 'feature_name'); else $session->set('productsFeaturesSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('productsFeaturesSearchOrderDir', 'asc'); else $session->set('productsFeaturesSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		/** @var Admin_Form_ProductFeatureForm $featureProductForm */
		$featureProductForm = new Admin_Form_ProductFeatureForm();
		$adminView->assign('searchForm', $featureProductForm->getProductFeatureSearchForm($searchFormData));

		$itemsCount = $productsFeaturesRepository->getCount($searchParams, $searchParams['logic']);

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('productsFeatures',
				$productsFeaturesRepository->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'pf.*, IF(pf.feature_active = 1, "Yes", "No") AS feature_status',
					$searchParams['logic']
				)
			);
		}

		$adminView->assign('delete_nonce', Nonce::create('product_feature_delete'));

		$adminView->assign('body', 'templates/pages/product-feature/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new products features
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
		$productsFeaturesRepository = $this->getProductsFeaturesRepository();

		/** @var Admin_Form_ProductFeatureForm $featureProductForm */
		$featureProductForm = new Admin_Form_ProductFeatureForm();

		$defaults = $productsFeaturesRepository->getDefaults($mode);

		// set feature active to true by default
		$defaults['feature_active'] = 1;

		/** @var core_Form_Form $form */
		$form = $featureProductForm->getForm($defaults, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formValues = $request->request->all();

			// check if feature status is set to active/inactive
			if (!isset($formValues['feature_active'])) $formValues['feature_active'] = 0;

			$formData = array_merge($defaults, $formValues);

			if (!Nonce::verify($formData['nonce'], 'product_feature_' . $mode))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_features');
			}
			else
			{
				if (($validationErrors = $productsFeaturesRepository->getValidationErrors($formData, $mode)) == false)
				{
					$productFeatureId = $productsFeaturesRepository->persist($formData);
					$formData['id'] = $productFeatureId;

					Admin_Flash::setMessage('common.success');
					$this->redirect('products_feature', array('id' => $productFeatureId, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('product_feature_add'));

		$adminView->assign('body', 'templates/pages/product-feature/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update products features
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
		$productsFeaturesRepository = $this->getProductsFeaturesRepository();

		$productFeatureId = intval($request->get('id'));
		$productFeatureData = $productsFeaturesRepository->getById($productFeatureId);

		if (!$productFeatureData)
		{
			Admin_Flash::setMessage('Cannot find products feature by provided ID', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_features');
		}

		/** @var Admin_Form_ProductFeatureForm $featureProductForm */
		$featureProductForm = new Admin_Form_ProductFeatureForm();

		$defaults = $productsFeaturesRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $featureProductForm->getForm($productFeatureData, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'product_feature_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_features');
			}
			else
			{
				if (($validationErrors = $productsFeaturesRepository->getValidationErrors($formData, $mode, $productFeatureId)) == false)
				{
					$productsFeaturesRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('products_feature', array('id' => $productFeatureId, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('title', $productFeatureData['feature_name']);
		$adminView->assign('mode', $mode);
		$adminView->assign('id', $productFeatureId);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('product_feature_update'));

		$adminView->assign('body', 'templates/pages/product-feature/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete product features
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'product_feature_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_features');
			return;
		}

		/** @var DataAccess_ProductsFeaturesRepository $productsFeaturesRepository */
		$productsFeaturesRepository = $this->getProductsFeaturesRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$productFeatureId = $request->get('id', 0);
			if ($productFeatureId == 0)
			{
				Admin_Flash::setMessage('Invalid Product Feature ID', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$productsFeaturesRepository->delete($productFeatureId);
				Admin_Flash::setMessage('Product feature has been deleted');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$productFeatureIds = explode(',', $request->get('ids', ''));
			if (count($productFeatureIds) == 0)
			{
				Admin_Flash::setMessage('Please select at least one product feature to delete', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$productsFeaturesRepository->delete($productFeatureIds);
				Admin_Flash::setMessage('Selected product features have been deleted');
			}
		}
		else if ($deleteMode == 'search')
		{
			$session = $this->getSession();
			$searchParams = $session->get('productsFeaturesSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

			if ($productsFeaturesRepository->deleteBySearchParams($searchParams, $searchParams['logic']))
			{
				Admin_Flash::setMessage('common.success');
			}
			else
			{
				Admin_Flash::setMessage('Cannot delete one or more products features.', Admin_Flash::TYPE_ERROR);
			}
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);
		}

		$this->redirect('products_features');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductsFeaturesRepository
	 */
	protected function getProductsFeaturesRepository()
	{
		if ($this->productsFeaturesRepository == null)
		{
			$this->productsFeaturesRepository = new DataAccess_ProductsFeaturesRepository($this->getDb());
		}

		return $this->productsFeaturesRepository;
	}
}

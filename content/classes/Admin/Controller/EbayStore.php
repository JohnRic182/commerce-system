<?php

class Admin_Controller_EbayStore extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	public function indexAction()
	{
		$categoryRepository = new DataAccess_CategoryRepository($this->getDb());
		$data['categories'] = $categoryRepository->getOptionsList();
		$ebayStoreForm = new Admin_Form_EbayStoreForm();

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9008');

		$adminView->assign('form', $ebayStoreForm->getForm($data));

		$adminView->assign('body', 'templates/pages/ebay-store/index.html');
		$adminView->render('layouts/default');
	}
}
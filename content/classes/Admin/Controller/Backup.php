<?php

/**
 * Class Admin_Controller_Backup
 */
class Admin_Controller_Backup extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/backup/';

	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$backups = array();

		$backupsPath = $this->getBackupsPath();
		$d = dir($backupsPath);
		while (($entry = $d->read()) !== false)
		{
			if ($entry != '.' && $entry != '..' && $entry != 'readme.txt' && is_file($backupsPath.$entry))
			{
				$backups[] = array(
					'file_name' => $entry,
					'size' => number_format(filesize($backupsPath.$entry) / 1024 / 1024, 2, '.', ''),
					'created' => date('F j, Y g:i a', filectime($backupsPath.$entry)),
				);
			}
		}
		$d->close();

		$adminView = $this->getView();

		$adminView->assign('backups', $backups);
		$adminView->assign('backup_nonce', Nonce::create('create_backup'));
		$adminView->assign('restore_backup_nonce', Nonce::create('restore_backup'));
		$adminView->assign('download_backup_nonce', Nonce::create('download_backup'));
		$adminView->assign('delete_nonce', Nonce::create('delete_backup'));

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8038');

		$adminView->assign('body', self::TEMPLATES_PATH.'index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * @return string
	 */
	protected function getBackupsPath()
	{
		return dirname(dirname(dirname(dirname(__FILE__)))).'/backups/';
	}

	/**
	 * @return string
	 */
	protected function getRootPath()
	{
		return dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/';
	}

	/**
	 * Create backup action
	 */
	public function createBackupAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');

			if (!Nonce::verify($nonce, 'create_backup', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			else
			{
				set_time_limit(1000000);
				$settings = $this->getSettings();
				$db = $this->getDb();

				$options = $request->request->getArray('options');

				$backup_engine = isset($options['backup_engine']);
				$backup_skins = isset($options['backup_skins']);
				$backup_cache = isset($options['backup_cache']);
				$backup_images = isset($options['backup_images']);
				$backup_download = isset($options['backup_download']);
				$backup_database = isset($options['backup_database']);

				$rootPath = $this->getRootPath();
				$backup_name = $settings->get('AppVer').'-'.date('Ymd-His').
					($backup_engine ? '-files' : '').
					($backup_skins ? '-skins' : '').
					($backup_cache ? '-cache' : '').
					($backup_images ? '-img' : '').
					($backup_download ? '-dwnld' : '').
					($backup_database ? '-db' : '');

				$dump_created = false;

				if ($backup_database)
				{
					$database_file = $rootPath.'content/backups/database-dump.sql';
					@unlink($database_file);
					$f = @fopen($database_file, "w+");
					$fe = "'";
					$add_drop_table = "Yes";
					$complete_insert = "No";

					global $label_app_name;
					fputs($f, "#\r\n# `".DB_NAME."` database DUMP\r\n# Generated at ".date("D M j G:i:s T Y")."\r\n# by ".$label_app_name."backend software\r\n#\r\n");

					$tables = array();
					$db->query("SHOW TABLES");
					while (($table = $db->moveNext()) != false)
					{
						$table_name = $table["Tables_in_".DB_NAME];
						if (DB_PREFIX !="" && strstr($table_name, DB_PREFIX))
						{
							$pos = strpos($table_name, DB_PREFIX);
							if ($pos == 0)
							{
								$tables[count($tables)] = $table_name;
							}
						}
						elseif (DB_PREFIX == "")
						{
							$tables[count($tables)] = $table_name;
						}
					}

					$replace = array("'" => "\'", '"' => '\"',"\\" => "\\\\", "\n" => "\\n", "\r" => "\\r");

					for ($i = 0; $i < count($tables); $i++)
					{
						$db->query("SHOW CREATE TABLE `" . $tables[$i] . "`");

						$result = $db->moveNext();

						fputs($f, "\r\n#\r\n# Table structure for table `" . $tables[$i] . "`\r\n#\r\n\r\n");

						if ($add_drop_table == "Yes")
						{
							fputs($f, "DROP TABLE IF EXISTS `" . $tables[$i] . "`;\r\n\r\n");
						}

						fputs($f, str_replace("\n", " ", $result["Create Table"]).";\r\n\r\n");


						$db->query("SHOW COLUMNS FROM `" . $tables[$i] . "`");
						$fields = array();
						while(($field = $db->moveNext()) != false) $fields[] = $field["Field"];

						$db->query("SELECT * FROM `" . $tables[$i] . "`");

						if ($complete_insert == "No")
						{
							while (($record = $db->moveNext()) != false)
							{
								fputs($f, "INSERT INTO `" . $tables[$i] . "` VALUES(");

								for ($j=0; $j<count($fields); $j++)
								{
									fputs($f, ($j > 0 ? "," : "") . stripslashes($fe) . (strtr(stripslashes($record[$fields[$j]]), $replace)) . stripslashes($fe));
								}
								fputs($f, ");\r\n");
							}
						}
						else
						{
							fputs($f, "INSERT INTO `" . $tables[$i] . "` VALUES\r\n");
							$c = 0;

							while (($record = $db->moveNext()) != false)
							{
								fputs($f, "\t(");
								for ($j=0; $j<count($fields); $j++)
								{
									fputs($f, ($j > 0 ? "," : "") . stripslashes($fe) . (strtr(stripslashes($record[$fields[$j]]), $replace)) . stripslashes($fe));
								}

								$c++;
								fputs($f, ")" . ($c < $db->numRows() ? "," : ";") . "\r\n");
							}
						}
					}
					fclose($f);
					$dump_created = true;
				}

				if (strpos(strtolower($_SERVER["SERVER_SOFTWARE"]), "win32") or strpos(strtolower("test" . $_SERVER["SERVER_SOFTWARE"]), "microsoft") != false)
				{
					$command_line =
						//$path."/content/backups/zip/PKZIP25.EXE -add ".$backup_name." ";
						'"'.$rootPath.'content/admin/7zip/7z.exe" '.
						"a -tzip \"".$rootPath."content/backups/".$backup_name.".zip\" ".
						"-r ";
					if (isset($backup_engine))
					{
						$command_line.=
							"\"" . $rootPath . "*content/admin/*\" ".
							"\"" . $rootPath . "*content/classes/*\" ".
							"\"" . $rootPath . "*content/engine/*\" ".
							"\"" . $rootPath . "*content/languages/*\" ".
							"\"" . $rootPath . "*content/public/*\" ".
							"\"" . $rootPath . "*content/smarty_plugins/*\" ".
							"\"" . $rootPath . "*content/ssl/*\" ".
							"\"" . $rootPath . "*content/vendors/*\" ".
							"\"" . $rootPath . ".htaccess\" ".
							"\"" . $rootPath . "admin.php\" ".
							"\"" . $rootPath . "download.php\" ".
							"\"" . $rootPath . "index.php\" ".
							"\"" . $rootPath . "login.php\" ".
							"\"" . $rootPath . "robots.txt\" ".
							(is_file("sitemap.xml") ? "\"" . $rootPath . "sitemap.xml\" " : "").
							(is_file("sitemap.xml.gz") ? "\"" . $rootPath . "sitemap.xml.gz\" " : "");

						if (is_dir($rootPath . 'ecc'))
						{
							$command_line .= "\"" . $rootPath . "*ecc/*\" ";
						}
					}
					if (isset($backup_skins)) $command_line .= "\"" . $rootPath . "*content/skins/*\" ";
					if (isset($backup_cache)) $command_line .= "\"" . $rootPath . "*content/cache/*\" ";
					if (isset($backup_images)) $command_line .= "\"" . $rootPath . "*images/*\" ";
					if (isset($backup_download)) $command_line .= "\"" . $rootPath . "*content/download/*\" ";
					if (isset($backup_database) && $dump_created) $command_line .= "\"" . $rootPath . "*content/backups/database-dump.sql\" ";

					$a = array();
					$result = exec($command_line, $a);
					@unlink($rootPath."content/backups/database-dump.sql");
				}
				else
				{
					$command_line = "tar -cf - -C\"" . $rootPath . "\" ";

					if ($backup_engine)
					{
						$command_line .=
							"content/admin/ ".
							"content/classes/ ".
							"content/engine/ ".
							"content/languages/ ".
							"content/public/ ".
							"content/smarty_plugins/ ".
							"content/ssl/ ".
							"content/vendors/ ".
							".htaccess ".
							"admin.php ".
							"download.php ".
							"index.php ".
							"login.php ".
							"robots.txt ".
							(is_file("sitemap.xml") ? "sitemap.xml " :"").
							(is_file("sitemap.xml.gz") ? "sitemap.xml.gz " :"");

						if (is_dir($rootPath.'ecc'))
						{
							$command_line .= $rootPath."ecc/ ";
						}
					}

					if ($backup_skins) $command_line .= $rootPath . "content/skins/ ";
					if ($backup_cache) $command_line .= $rootPath . "content/cache/ ";
					if ($backup_images) $command_line .= $rootPath . "images/ ";
					if ($backup_download) $command_line .= $rootPath . "content/download/ ";
					if ($backup_database && $dump_created) $command_line .= $rootPath."content/backups/database-dump.sql";

					$command_line .= " | gzip -c > \"" . $rootPath . "content/backups/" . $backup_name . ".tar.gz\"";
					$a = array();
					$result = exec($command_line, $a);

					@unlink($rootPath.'content/backups/database-dump.sql');
				}

				Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
			}
		}

		$this->renderJson(array(
			'status' => 1
		));
	}

	/**
	 * Restore action
	 */
	public function restoreAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'restore_backup', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			else
			{
				set_time_limit(1000000);
				$db = $this->getDb();
				$rootPath = $this->getRootPath();

				$fileName = $request->request->get('file_name');

				//TODO: Check for file?
				if (is_file($rootPath.'content/backups/'.$fileName))
				{
					if (strpos(strtolower($_SERVER["SERVER_SOFTWARE"]), "win32") or strpos(strtolower("test".$_SERVER["SERVER_SOFTWARE"]), "microsoft") != false)
					{
						$command_line =
							$rootPath."content/admin/7zip/7z.exe ".
							"x \"".$rootPath."content/backups/".$fileName."\" -aoa -o\"".$rootPath."\"";
						$result = exec($command_line, $a);
					}
					else
					{
						$command_line =
							"tar -xvzf \"".$rootPath."content/backups/".$fileName."\" -C\"".$rootPath."\" --overwrite";
						$result = exec($command_line, $a);
					}

					if (is_file($rootPath.'content/backups/database-dump.sql'))
					{
						$f = @fopen($rootPath.'content/backups/database-dump.sql', 'r');
						if ($f)
						{
							$buffer = '';
							while ($s = fgets($f))
							{
								$buffer .= $s;
							}
							fclose($f);
							$lines = explode("\n", $buffer);
							for ($i = 0; $i < count($lines); $i++)
							{
								$line = trim($lines[$i]);
								if ($line != "" && $line[0] != "#")
								{
									$db->query($line);
								}
								if ($i % 25  == 0)
								{
									sleep(1);
								}
							}
						}
						@unlink($rootPath.'content/backups/database-dump.sql');
					}
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'message' => trans('backup.backup_file_not_found')
					));
					return;
				}
			}
		}

		$this->renderJson(array(
			'status' => 1
		));
	}

	public function downloadAction()
	{
		$request = $this->getRequest();

		$nonce = $request->get('nonce');
		if (!Nonce::verify($nonce, 'download_backup', false))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('backup_restore');
		}
		else
		{
			$file = $backupsPath = $this->getBackupsPath() . $request->get('file_name');

			$download = new Admin_Service_Download($file);

			if ($download->isExist() == true)
			{
				$download->run();
			}
			else
			{
				Admin_Flash::setMessage('common.file_not_exist', Admin_Flash::TYPE_ERROR);
				$this->redirect('backup_restore');
			}
		}
	}

	public function deleteAction()
	{
		$request = $this->getRequest();

		if ($request->isPost())
		{
			$nonce = $request->request->get('nonce');
			if (!Nonce::verify($nonce, 'delete_backup', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
				return;
			}
			else
			{
				$fileName = $request->request->get('file_name');

				if (strpos($fileName, '/') === false && strpos($fileName, "\\") === false)
				{
					@unlink($this->getRootPath().'content/backups/'.$fileName);
				}
			}
		}

		$this->renderJson(array(
			'status' => 1
		));
	}
}
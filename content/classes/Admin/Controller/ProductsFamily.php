<?php

/**
 * Class Admin_Controller_ProductsFamily
 */
class Admin_Controller_ProductsFamily extends Admin_Controller{

	const TEMPLATES_PATH = 'templates/pages/product-family/';

	protected $dataRepository = null;

	protected $categoriesRepository = null;

	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'recommended-products');


	/**
	 * List action
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5003');

		$productsFamilyRepository = $this->getDataRepository();

		$request = $this->getRequest();

		$itemsCount = $productsFamilyRepository->getCount();

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('productsfamilies', $productsFamilyRepository->getList($paginator->sqlOffset, $paginator->itemsPerPage));
		}
		else
		{
			$adminView->assign('productsfamilies', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('productsfamily_delete'));


		$productsFamilyForm = new Admin_Form_ProductsFamilyForm();

		$defaults = $productsFamilyRepository->getDefaults(self::MODE_ADD);

		$mode = self::MODE_ADD;
		/** @var core_Form_Form $form */
		$form = $productsFamilyForm->getForm($defaults, $mode);

		$adminView->assign('addForm', $form);
		$adminView->assign('mode', $mode);
		$adminView->assign('addNonce', Nonce::create('productsfamily_add'));

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 *
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$productsFamilyRepository = $this->getDataRepository();

		$defaults = $productsFamilyRepository->getDefaults(self::MODE_ADD);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'productsfamily_add', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $productsFamilyRepository->getValidationErrors($formData, $mode)) == false)
				{
					$id = $productsFamilyRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->renderJson(array('status' => 1, 	'id' => $id));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}

	}

	/**
	 * Update action
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$productsFamilyRepository = $this->getDataRepository();
		$categoriesRepository = $this->getCategoryRepository();

		$id = intval($request->get('id'));

		$familyGroup = $productsFamilyRepository->getById($id);

		if (!$familyGroup)
		{
			Admin_Flash::setMessage('Cannot find products group provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_families');
		}

		$categories = $categoriesRepository->getOptionsList();
		if($categories)
		{
			$familyGroup['categories'] = $categories;
		}

		$productsFamilyForm = new Admin_Form_ProductsFamilyForm();

		$defaults = $productsFamilyRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $productsFamilyForm->getForm($familyGroup, $mode, $id);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'productsfamily_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_families');
			}
			else
			{
				if (($validationErrors = $productsFamilyRepository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$productsFamilyRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('products_family', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5004');
		$adminView->assign('title', $familyGroup['name']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('productsfamily_update'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete action
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		$productsFamilyRepository = $this->getDataRepository();

		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_families');
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid recomended proudcts group id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if (($validationErrors = $productsFamilyRepository->getDeleteValidationErrors(array($id))) == false)
				{
					$productsFamilyRepository->delete(array($id));

					Admin_Flash::setMessage('Sucess');
					$this->redirect('products_families');
				}
				else
				{
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
					$this->redirect('products_families');
				}
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one group to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_families');
			}
			else
			{
				if (($validationErrors = $productsFamilyRepository->getDeleteValidationErrors($ids)) == false)
				{
					$productsFamilyRepository->delete($ids);
					Admin_Flash::setMessage('Selected product groups have been deleted');
					$this->redirect('products_families');
				}
				else
				{
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
					$this->redirect('products_families');
				}
			}
		}
		else if ($deleteMode == 'search')
		{
			if (($validationErrors = $productsFamilyRepository->getDeleteValidationErrors('all')) == false)
			{
				$productsFamilyRepository->deleteAll();
				Admin_Flash::setMessage('All recomended product groups have been deleted');
				$this->redirect('products_families');
			}
			else
			{
				Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				$this->redirect('products_families');
			}
		}
	}

	/**
	 * Get assigned products action
	 */
	public function getAssignedProductsAction()
	{
		$request = $this->getRequest();
		$pf_id = intval($request->get('pf_id'));
		$categories = intval($request->get('categories'));

		$productsFamilyRepository = $this->getDataRepository();
		$result = $productsFamilyRepository->getAssignedProducts($pf_id, $categories);

		$this->renderJson($result);
	}

	/**
	 * Get products action
	 */
	public function getProductsAction()
	{
		$request = $this->getRequest();
		$pf_id = intval($request->get('pf_id'));
		$categories = intval($request->get('categories'));

		$productsFamilyRepository = $this->getDataRepository();
		$result = $productsFamilyRepository->getProducts($pf_id, $categories);

		$this->renderJson($result);
	}

	/**
	 * Assign products action
	 */
	public function assignProductsAction()
	{

		$request = $this->getRequest();

		$pf_id = intval($request->get('pf_id'));

		$productsFamilyRepository = $this->getDataRepository();
		$products = $request->get('products');


		$result = $productsFamilyRepository->assignProducts($pf_id, $products);

		$this->renderJson($result);
	}

	/**
	 * Unassign products action
	 */
	public function unassignProductsAction()
	{
		$request = $this->getRequest();

		$pf_id = intval($request->get('pf_id'));

		$productsFamilyRepository = $this->getDataRepository();
		$products = $request->get('products');


		$result = $productsFamilyRepository->unassignProducts($pf_id, $products);

		$this->renderJson($result);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductsFamilyRepository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->dataRepository))
		{
			$this->dataRepository = new DataAccess_ProductsFamilyRepository($this->getDb(), $this->getSettings()->getAll());
		}

		return $this->dataRepository;
	}


	/**
	 * @return DataAccess_CategoryRepository|null
	 */
	protected function getCategoryRepository()
	{
		if (is_null($this->categoriesRepository))
		{
			$this->categoriesRepository = new DataAccess_CategoryRepository($this->getDb(), $this->getSettings()->getAll());
		}

		return $this->categoriesRepository;
	}
}
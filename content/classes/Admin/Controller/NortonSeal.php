<?php

/**
 * Class Admin_Controller_NortonSeal
 */
class Admin_Controller_NortonSeal extends Admin_Controller
{
	protected $menuItem = array('primary' => 'apps', 'secondary' => '');

	/**
	 * Index action
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('DisplayNortonShoppingGuarantee','NortonSealGuaranteeHash','NortonSealStoreNumber');

		if ($request->isPost())
		{
			if (!Nonce::verify($request->request->get('nonce'), 'update_settings'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('app', array('key' => 'norton-seal'));
				return;
			}

			$settingsData = $request->request->getByKeys($settingsVars);
			if (!isset($settingsData['DisplayNortonShoppingGuarantee'])) $settingsData['DisplayNortonShoppingGuarantee'] = 'NO';
			
			$errors = array();
			if (!isset($settingsData['NortonSealGuaranteeHash']) || trim($settingsData['NortonSealGuaranteeHash']) == '')
			{
				$errors[] = array(trans('apps.norton_seal.guarantee_hash_required'));
				Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($errors), Admin_Flash::TYPE_ERROR);
			}
			elseif(!preg_match('/^(.+)\%3d\%3d$/i', $settingsData['NortonSealGuaranteeHash']))
			{
				$errors[] = array(trans('apps.norton_seal.guarantee_hash_not_valid'));
				Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($errors), Admin_Flash::TYPE_ERROR);
			}
			if (!isset($settingsData['NortonSealStoreNumber']) || trim($settingsData['NortonSealStoreNumber']) == '')
			{
				$errors[] = array(trans('apps.norton_seal.store_number_required'));
				Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($errors), Admin_Flash::TYPE_ERROR);
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);
				Admin_Flash::setMessage('common.success');
				$this->redirect('app', array('key' => 'norton-seal'));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$nortonSealForm = new Admin_Form_NortonSealForm();
		$form = $nortonSealForm->getForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '9050');

		$adminView->assign('nonce', Nonce::create('update_settings'));

		$adminView->assign('body', 'templates/pages/norton-seal/index.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Active form action
	 */
	public function activateFormAction()
	{
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$settingsVars = array('DisplayNortonShoppingGuarantee', 'NortonSealGuaranteeHash','NortonSealStoreNumber');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$settingsData = $request->request->getByKeys($settingsVars);

			if (!isset($settingsData['DisplayNortonShoppingGuarantee'])) $settingsData['DisplayNortonShoppingGuarantee'] = 'NO';

			$errors = array();

			if (!isset($settingsData['NortonSealGuaranteeHash']) || trim($settingsData['NortonSealGuaranteeHash']) == '')
			{
				$errors[] = array('field' => 'field-settings-NortonSealGuaranteeHash', 'message' => trans('apps.norton_seal.guarantee_hash_required'));
			}
			elseif(!preg_match('/^(.+)\%3d\%3d$/i', $settingsData['NortonSealGuaranteeHash']))
			{
				$errors[] = array('field' => 'field-settings-NortonSealGuaranteeHash', 'message' => trans('apps.norton_seal.guarantee_hash_not_valid'));
			}
			if (!isset($settingsData['NortonSealStoreNumber']) || trim($settingsData['NortonSealStoreNumber']) == '')
			{
				$errors[] = array('field' => 'field-settings-NortonSealStoreNumber', 'message' => trans('apps.norton_seal.store_number_required'));
			}

			if (count($errors) < 1)
			{
				$settings->persist($settingsData, $settingsVars);

				$appRepository = new DataAccess_AppRepository($this->getDb());
				$appRepository->enableApp('norton-seal');

				$this->renderJson(array('status' => 1, 'errors' => null));
			}
			else
			{
				$this->renderJson(array('status' => 0, 'errors' => $errors));
			}
		}

		$settingsData = $settings->getByKeys($settingsVars);

		$nortonSealForm = new Admin_Form_NortonSealForm();
		$form = $nortonSealForm->getNortonSealActivateForm($settingsData);

		$adminView = $this->getView();

		$adminView->assign('form', $form);

		$this->renderJson(array(
			'status' => 1,
			'html' => $adminView->fetch('generic-form'),
		));
	}

	/**
	 * Deactivate action
	 */
	public function deactivateAction()
	{
		$settings = $this->getSettings();
		$settings->persist(array('DisplayNortonShoppingGuarantee' => 'NO'));
	}
}
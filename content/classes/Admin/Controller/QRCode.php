<?php

/**
 * Class Admin_Controller_QRCode
 */
class Admin_Controller_QRCode extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/qr-code/';

	protected $repository = null;

	const CAMPAIGN_TYPE = 'qr';

	protected $menuItem = array('primary' => 'marketing', 'secondary' => 'qr-codes');

	/**
	 * List campaigns
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		$rep = $this->getRepository();

		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'name');

		$itemsCount = $rep->getCount(self::CAMPAIGN_TYPE);
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('campaigns', $rep->getList(self::CAMPAIGN_TYPE, $paginator->sqlOffset, $paginator->itemsPerPage, $orderBy));
		}
		else
		{
			$adminView->assign('settings_qr', null);
		}

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '5012');

		$adminView->assign('delete_nonce', Nonce::create('campaign_delete'));
		$adminView->assign('update_statuses_nonce', Nonce::create('update_statuses_nonce'));

		$adminView->assign('settingsNonce', Nonce::create('settings_update'));

		$adminView->assign('qrCodeForm', $this->getAddQrCodeForm());
		$adminView->assign('settingsForm', $this->getSettingsForm());

		$adminView->assign('generateForm', $this->getGenerateForm());

		$adminView->assign('addNonce', Nonce::create('qr_code_add'));

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new campaign
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;
		$request = $this->getRequest();
		$repository = $this->getRepository();
		$defaults = $repository->getDefaults(self::CAMPAIGN_TYPE);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'],'qr_code_add', false))
			{
				$this->renderJson(array(
						'status' => 0,
						'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
				{
					$repository->persist($formData);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}
	}

	/**
	 * get Add QR Code Form
	 * @return core_Form_Form
	 */
	protected function getAddQrCodeForm()
	{
		$mode = self::MODE_ADD;

		$repository = $this->getRepository();

		/** @var Admin_Form_QRCodeForm */
		$TRForm = new Admin_Form_QRCodeForm();

		$defaults = $repository->getDefaults(self::CAMPAIGN_TYPE);

		return $TRForm->getForm($defaults, $mode);
	}

	/**
	 * get Settings Form
	 * @return core_Form_Form
	 */
	protected function getSettingsForm()
	{
		$data = $this->getSettingsData();

		$data['imageTypeOptions'] = $this->getImageTypeOptions();
		$data['mediaOptions'] = $this->getMediaOptions();

		/** @var Admin_Form_QRCodeAdvancedSettingsForm */
		$TRForm = new Admin_Form_QRCodeAdvancedSettingsForm();

		/** @var core_Form_Form $form */
		return $TRForm->getForm($data);
	}

	/**
	 * @return array
	 */
	protected function &getSettingsData()
	{
		$settings = $this->getSettings();
		$data = $settings->getByKeys(array(
			'qr_active',
			'qr_image_type',
			'qr_media',
			'GlobalHttpUrl',
		));
		return $data;
	}

	/**
	 * get Generate Form
	 * @return core_Form_Form
	 */
	protected function getGenerateForm()
	{
		$data = $this->getSettingsData();

		$data['imageTypeOptions'] = $this->getImageTypeOptions();
		$data['mediaOptions'] = $this->getMediaOptions();

		/** @var Admin_Form_QRCodeGenerateForm */
		$TRForm = new Admin_Form_QRCodeGenerateForm();

		/** @var core_Form_Form $form */
		return $TRForm->getForm($data);
	}

	/**
	 * Update campaign
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getRepository();

		$id = intval($request->get('id'));

		$campaign = $repository->getById($id, self::CAMPAIGN_TYPE);

		if (!$campaign)
		{
			Admin_Flash::setMessage('marketing.qr_cannot_find_campaign', Admin_Flash::TYPE_ERROR);

			$this->renderJson(array(
				'status' => 0,
				'errors' => '',
			));
		}

		$TPForm = new Admin_Form_QRCodeForm();

		$defaults = $repository->getDefaults(self::CAMPAIGN_TYPE);

		$form = $TPForm->getForm($campaign, $mode, $id);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'qr_code_edit', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('id', $id);
		$adminView->assign('qrCodeForm', $form);
		$adminView->assign('updateNonce', Nonce::create('qr_code_edit'));

		$html = $adminView->fetch('pages/qr-code/modal-edit');

		$this->renderJson(array(
			'status' => 1,
			'html' => $html,
		));
	}

	/**
	 * Delete campaign groups
	 */
	public function deleteAction()
	{
	
		$request = $this->getRequest();
	
		$repository = $this->getRepository();
	
		$deleteMode = $request->get('deleteMode');
	
		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('settings_qr');
			return;
		}
	
		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('marketing.invalid_campaign_id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete(array($id));
	
				Admin_Flash::setMessage('common.success');
				$this->redirect('settings_qr');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('marketing.qr_select_at_least_one_campaign', Admin_Flash::TYPE_ERROR);
				$this->redirect('settings_qr');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('common.success');
				$this->redirect('settings_qr');
			}
		}
		else if ($deleteMode == 'search')
		{
			$repository->deleteBySearchParams(self::CAMPAIGN_TYPE);
			Admin_Flash::setMessage('common.success');
			$this->redirect('settings_qr');
		}
	}
	
	/**
	 * Edit QR Codes settings
	 */
	public function editAdvancedSettingsAction()
	{
		$request = $this->getRequest();

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = $request->request->getArray('qrSettings');

			if (!Nonce::verify($request->request->get('nonce'), 'settings_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$settings = $this->getSettings();

				if (!isset($formData['qr_active'])) $formData['qr_active'] = 'No';

				$settings->persist($formData, array('qr_active', 'qr_image_type', 'qr_media'));

				Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);

				$this->renderJson(array(
					'status' => 1,
					'errors' => null,
				));
			}
		}
	}

	/**
	 * Generate QR Code image
	 */
	public function generateAction()
	{
		$request = $this->getRequest();

		$mode = $request->get('mode');

		$data = $this->getSettingsData();

		$data['imageTypeOptions'] = $this->getImageTypeOptions();
		$data['mediaOptions'] = $this->getMediaOptions();

		$TRForm = new Admin_Form_QRCodeGenerateForm();

		$form = $TRForm->getForm($data);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('generate_image'));
		$adminView->assign('mode', $mode);

		$adminView->assign('body', self::TEMPLATES_PATH.'generate.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Download QR Code
	 */
	public function downloadAction()
	{
		$request = $this->getRequest();
		$path = $request->get('path');

		if ($path && is_file($path))
		{
			$is = getimagesize($path);

			if (in_array($is[2], array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF)))
			{
				//TODO: Shouldn't this check that the image path is in the qr codes directory?
				$path_info = pathinfo($path);
				header("Content-Type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$path_info["basename"]);
				readfile($path);
			}
		}
	}

	/**
	 * Generate QR Code based on custom text
	 */
	public function generateCustomAction()
	{
		$request = $this->getRequest();

		$text = trim($request->get('text'));

		$image_type = $request->get("image_type", IMAGETYPE_PNG);
		$error_check_level = $request->get('error_check', 0);
		$default_pixel_size = $request->get('default_size', 0);

		$settings = $this->getSettings();

		$settingsValues = $settings->getAll();
		$qrCodes = new QR($settingsValues);
		$url = $qrCodes->renderCustomCode($text, true, $error_check_level, $default_pixel_size, $image_type);

		$path = $qrCodes->getLastRenderedFile();
		$is = getimagesize($path);
		$result = array(
			'url' => $url,
			'path' => $path,
			'width' => $is[0],
			'height' => $is[1],
		);
		$this->renderJson($result);
	}

	/**
	 * Generate QR Code for a product
	 */
	public function generateProductQRAction()
	{
		$request = $this->getRequest();

		$product_id = trim($request->get('product_id'));
		$campaign_id = trim($request->get('campaign_id'));
		$campaign_id = ($campaign_id == '' || $campaign_id == 0) ? false : $campaign_id;
		$url_type = trim($request->get('url_type'));

		$image_type = $request->get("image_type", IMAGETYPE_PNG);
		$error_check_level = $request->get('error_check', 0);
		$default_pixel_size = $request->get('default_size', 0);

		$settings = $this->getSettings();

		$settingsValues = $settings->getAll();
		$qrCodes = new QR($settingsValues);

		$offsite_commerce_campaigns = new OffsiteCommerceCampaigns($this->getDb(), $settingsValues);

		if ($url_type == 'product')
		{
			$link_url = $offsite_commerce_campaigns->getProductUrl($product_id, $campaign_id, "qr");
			$url = $qrCodes->renderProductUrlCode($link_url, $product_id, true, $error_check_level, $default_pixel_size, $image_type, $campaign_id);

		}
		else
		{
			$link_url = $offsite_commerce_campaigns->getProductAddToCartUrl($product_id, $campaign_id, "qr");
			$url = $qrCodes->renderProductAddToCartCode($link_url, $product_id, true, $error_check_level, $default_pixel_size, $image_type, $campaign_id);
		}

		$path = $qrCodes->getLastRenderedFile();
		$is = getimagesize($path);
		$result = array(
			'url' => $url,
			'link_url' => $link_url,
			'path' => $path,
			'width' => $is[0],
			'height' => $is[1],
		);
		$this->renderJson($result);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_OffsiteCampaignRepository
	 */
	protected function getRepository()
	{
		if ($this->repository === null)
		{
			$this->repository = new DataAccess_OffsiteCampaignRepository($this->getDb(), $this->getSettings());
		}

		return $this->repository;
	}

	/**
	 * get Image Type Options array
	 */
	protected function getImageTypeOptions()
	{
		return array (
			'3' => 'PNG',
			'2' => 'JPG',
			'1' => 'GIF'
		);
	}

	/**
	 * get Media Options array
	 */
	protected function getMediaOptions()
	{
		return array (
			'1' => trans('marketing.qr_media_options.1'),
			'2' => trans('marketing.qr_media_options.2'),
			'3' => trans('marketing.qr_media_options.3'),
			'4' => trans('marketing.qr_media_options.4'),
			'5' => trans('marketing.qr_media_options.5'),
			'6' => trans('marketing.qr_media_options.6'),
		);
	}
}
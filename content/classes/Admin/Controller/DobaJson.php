<?php

/**
 * Class Admin_Controller_DobaJson
 */
class Admin_Controller_DobaJson extends Admin_Controller
{
	/** @var Doba_ProductApi $dobaProductApi **/
	protected $dobaProductApi = null;

	/** @var Doba_OrderApi $dobaOrderApi **/
	protected $dobaOrderApi = null;

	/** @var Doba_RetailerApi $dobaRetailerApi */
	protected $dobaRetailerApi = null;
	
	protected $defaultResponse = array('status' => 0, 'message' => 'Wrong request', 'data' => false);

	/**
	 * Save price settings
	 */
	public function savePriceSettings()
	{
		$db = $this->getDb();
		$request = $this->getRequest();

		$s = array(
			'doba_price_option' => $request->get('price_option'),
			'doba_price_option99' => $request->get('price_option99'),
			'doba_price_modifier' => $request->get('price_modifier'),
		);

		foreach ($s as $key=>$value)
		{
			$db->query('UPDATE ' . DB_PREFIX . 'settings SET value="'. $db->escape($value) . '" WHERE name="' . $db->escape($key) . '"');
		}

		$result = array('status' => 1);

		$this->renderJson($result);
	}

	/**
	 * Get watch lists
	 */
	public function getWatchlists()
	{
		$dobaProductApi = $this->getDobaProductApi();

		$dobaProductApi->initDobaTable();

		if (($watchLists = $dobaProductApi->getWatchlists()) != false)
		{
			$result = array('status' => 1, 'count' => count($watchLists['watchlist']), 'data' => $watchLists);
		}
		else
		{
			$result = array('status' => 0, 'message' => 'You do not have watchlists yet or something wrong with a server', 'data' => false);
		}

		$this->renderJson($result);
	}

	/**
	 * Get watchlist products from doba and cache in database
	 */
	public function getWatchlistProducts()
	{
		$dobaProductApi = $this->getDobaProductApi();
		$request = $this->getRequest();

		$id = intval($request->get('id', 0));
		$page = intval($request->get('page', 1));
		$limit = intval($request->get('limit', 20));
		$refresh = intval($request->get('refresh', 0)) == 1;

		if ($page == 1 && $refresh)
		{
			$finish = false;
			$wlPage = 1;
			
			while (!$finish)
			{
				if (($pr = $dobaProductApi->getWatchlistProducts($id, $wlPage, $limit)) != false)
				{
					$dobaProductApi->cacheItems($id, $pr);
					$wlPage++;
					unset($pr);
				}
				else
				{
					$finish = true;
				}
			}
		}

		$items_count = $dobaProductApi->getCachedItemsCount($id);
		$items = $dobaProductApi->getCachedItems($id, $page, $limit);

		$result = array('status' => 1, 'items' => $items, 'items_count' => $items_count);

		$this->renderJson($result);
	}

	/**
	 * Get items details from cache
	 */
	public function getItemDetailFromCache()
	{
		$dobaProductApi = $this->getDobaProductApi();
		$request = $this->getRequest();

		$itemId = intval($request->get('item_id'), 0);
		$watchlistId = $request->get('watchlist_id', null);
		$watchlistId = $watchlistId != null ? intval($watchlistId) : null;

		if (($item = $dobaProductApi->getItemDetailFromCache($itemId, $watchlistId)) != false)
		{
			$result = array('status' => 1, 'item' => $item);
			unset($pr);
		}

		$this->renderJson($result);
	}

	/**
	 * Update product (synk with cart database)
	 */
	public function  updateProductsSet()
	{
		$db = $this->getDb();
		$dobaProductApi = $this->getDobaProductApi();

		$settings = $this->getSettings();

		$request = $this->getRequest();

		$id = intval($request->get('id', 0));
		$page = intval($request->get('page', 0));
		$limit = floatval($request->get('limit', 0));
		$import = intval($request->get('import', 0)) == 1 ? true : false;

		if ($page == 1)
		{
			$db->query('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'doba_cache WHERE watchlist_id=' . $id);
			
			$c = $db->moveNext();
			
			if ($c['c'] == 0)
			{
				if ($dobaProductApi->saveWatchlistProducts($id))
				{
					$dobaProductApi->cacheItemsFromFile($id);
				}
			}
		}

		$logFile = 'content/cache/log/doba-skipped-products-' . escapeFileName($id) . '-' . date('Ymd') . '.txt';

		if (($items = $dobaProductApi->getCachedItems($id, $page, $limit, false, $import)) != false)
		{
			foreach ($items as $item)
			{
				$a = $dobaProductApi->itemToDb($item['p'], $item['i'], false, true, $logFile);

				/**
				 * Do images here too
				 */
				if (!in_array('images', $dobaProductApi->lastProductLockedFields))
				{
					if ($settings->get('doba_images') != 'No')
					{
						$defaultImageURL = $dobaProductApi->getItemDefaultImageUrl($item['p']);
						$dobaProductApi->getItemDefaultImage($defaultImageURL, $item['i']['item_id'], $settings->get('doba_images') == 'Yes');
						if ($settings->get('doba_images_secondary') == 'Yes' || $settings->get('doba_images_secondary') == 'All')
						{
							$secondaryImages = $dobaProductApi->getItemSecondaryImagesUrl($item['p']);

							if ($secondaryImages)
							{
								$dobaProductApi->getItemSecondaryImages(
									$secondaryImages,
									$item['i']['item_id'],
									$settings->get('doba_images') == 'Yes'
								);
							}
						}
					}
					else
					{
						$imageFileName = ImageUtility::imageFileName($item['i']['item_id']) . '.jpg';
						$imageFile = 'images/products/' . $imageFileName;
						$previewFile = 'images/products/preview/' . $imageFileName;
						$thumbFile = 'images/products/thumbs/' . $imageFileName;
						
						if (is_file($imageFile)) @unlink($imageFile);
						if (is_file($previewFile)) @unlink($previewFile);
						if (is_file($thumbFile)) @unlink($thumbFile);
					}
				}
			}

			Seo::updateSeoURLs($db, $settings->getAll(), true, true, false, true);

			$result = array('status' => 1,  'ignored' => $a == 0 ? 1 : 0, 'added' => $a == 2 ? 1 : 0, 'updated' => $a == 1 ? 1 : 0);
		}
		else
		{
			$result = $this->defaultResponse;
		}

		return $this->renderJson($result);
	}

	/**
	 * Update import settings
	 */
	public function updateImportSettings()
	{
		$db = $this->getDb();
		$request = $this->getRequest();
		
		$data = $request->get('data', array());
		
		foreach ($data as $key => $value)
		{
			$db->query('UPDATE ' . DB_PREFIX . 'settings SET value="' . $db->escape($value) . '" WHERE name="' . $db->escape($key) . '"');
			$settings[$key] = $value;
		}

		$result = array('status' => 1);

		$this->renderJson($result);
	}

	/**
	 * Request Partner Permission
	 */
	public function requestPartnerPermission()
	{
		$dobaRetailerApi = $this->getDobaRetailerApi();

		if ($dobaRetailerApi->requestPartnerPermission())
		{
			$result = array('status' => 1);
		}
		else
		{
			$result = $this->defaultResponse;
		}

		$this->renderJson($result);
	}

	/**
	 * Has Partner Permission
	 */
	public function hasPartnerPermission()
	{
		$dobaRetailerApi = $this->getDobaRetailerApi();

		if ($r = $dobaRetailerApi->hasPartnerPermission())
		{
			$result = array('status' => 1, 'message' => $r['response']['has_permission']);
		}
		else
		{
			$result = $this->defaultResponse;
		}

		$this->renderJson($result);
	}

	/**
	 * Remove Partner Permission
	 */
	public function removePartnerPermission()
	{
		$dobaRetailerApi = $this->getDobaRetailerApi();

		if ($dobaRetailerApi->removePartnerPermission())
		{
			$result = array('status' => 1);
		}
		else
		{
			$result = $this->defaultResponse;
		}

		$this->renderJson($result);
	}

	/**
	 * Send order to doba
	 */
	public function sendOrder()
	{
		$db = $this->getDb();
		$dobaOrderApi = $this->getDobaOrderApi();
		$request = $this->getRequest();

		$result = $this->defaultResponse;

		$oid = intval($request->get('oid', 0));

		$db->query('SELECT * FROM ' . DB_PREFIX . 'orders_content WHERE oid=' . $oid . ' AND is_doba="Yes"');

		$dobaItems = array();

		while ($item = $db->moveNext())
		{
			$dobaItems[] = array('item_id' => $item['product_id'], 'quantity' => $item['admin_quantity']);
		}

		if (count($dobaItems) > 0)
		{
			$db->query('
				SELECT
					o.order_num AS order_num,
					o.shipping_address_type AS address_type,
					o.shipping_name AS name,
					o.shipping_address1 AS address1,
					o.shipping_address2 AS address2,
					o.shipping_city AS city,
					o.shipping_zip AS zip,
					c.iso_a2 AS country_iso_a2,
					s.short_name AS state_abbr
				FROM ' . DB_PREFIX . 'orders o
				LEFT JOIN ' . DB_PREFIX . 'countries c ON c.coid = o.shipping_country
				LEFT JOIN ' . DB_PREFIX . 'states s ON s.states.stid = o.shipping_state
				WHERE o.oid=' . $oid
			);

			if ($db->moveNext())
			{
				$data = $db->col;
				$dobaOrderResponse = $dobaOrderApi->createOrder($data, $dobaItems, $data['order_num']);

				if ($dobaOrderResponse)
				{
					$db->query('UPDATE ' . DB_PREFIX . 'orders SET doba_order_sent=1, doba_order_id=' . $db->escape($dobaOrderResponse['response']['order_id']) . ' WHERE oid=' . $oid);

					$result = array('status' => 1, 'doba_order_id' => trim($dobaOrderResponse['response']['order_id']));
				}
				else
				{
					$result = array('status' => 0, 'message' => $dobaOrderApi->last_error, 'data' => false);
				}
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Get order details
	 */
	public function getOrderDetail()
	{
		$dobaOrderApi = $this->getDobaOrderApi();
		$result = $this->defaultResponse;
		$request = $this->getRequest();

		$dobaOrderId = $request->get('doba_order_id', false);

		if ($dobaOrderId)
		{
			$dobaOrderResponse = $dobaOrderApi->getOrderDetail($dobaOrderId);
			if ($dobaOrderResponse)
			{
				$result = array('status' => 1, 'order' => $dobaOrderResponse);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * Fund order
	 */
	public function fundOrder()
	{
		$db = $this->getDb();
		$dobaOrderApi = $this->getDobaOrderApi();
		$request = $this->getRequest();
		$result = $this->defaultResponse;

		$dobaOrderId = $request->get('doba_order_id', false);
		$cvv2 = $request->get('cvv2', '');

		if ($dobaOrderId)
		{
			$dobaOrderResponse = $dobaOrderApi->fundOrder($dobaOrderId, $cvv2);
			if ($dobaOrderResponse)
			{
				$db->query('UPDATE ' . DB_PREFIX . 'orders SET doba_order_funded=1 WHERE doba_order_id="' . $db->escape($dobaOrderId) . '"');
				$result = array('status' => 1);
			}
			else
			{
				$result = array('status' => 0, 'message' => $dobaOrderApi->last_error);
			}
		}

		$this->renderJson($result);
	}

	/**
	 * @return Doba_ProductApi
	 */
	protected function getDobaProductApi()
	{
		if ($this->dobaProductApi == null)
		{
			$settings = $this->getSettings()->getAll();
			$db = $this->getDb();

			$this->dobaProductApi = new Doba_ProductApi($settings, $db);
		}

		return $this->dobaProductApi;
	}

	/**
	 * @return Doba_OrderApi
	 */
	protected function getDobaOrderApi()
	{
		if ($this->dobaOrderApi != null)
		{
			$settings = $this->getSettings()->getAll();
			$db = $this->getDb();

			$this->dobaOrderApi = new Doba_OrderApi($settings, $db);
		}

		return $this->dobaOrderApi;
	}

	/**
	 * @return Doba_ProductApi|Doba_RetailerApi
	 */
	protected function getDobaRetailerApi()
	{
		if ($this->dobaProductApi != null)
		{
			$settings = $this->getSettings()->getAll();
			$db = $this->getDb();

			$this->dobaProductApi = new Doba_RetailerApi($settings, $db);
		}

		return $this->dobaProductApi;
	}
}

<?php

/**
 * Class Admin_Controller_Snippet
 */
class Admin_Controller_Snippet extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/snippet/';

	/** @var DataAccess_SnippetRepository  */
	protected $snippetRepository = null;

	protected $menuItem = array('primary' => 'settings', 'secondary' => 'settings-advanced');

	/**
	 * List Site Snippets
	 */
	public function listAction()
	{
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6000');

		$snippetRepository = $this->getSnippetRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'priority');

		// get search params
		$searchParams = array();
		$adminView->assign('searchParams', $searchParams);

		$itemsCount = $snippetRepository->getCount($searchParams);
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('snippets', $snippetRepository->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy, $searchParams));
		}
		else
		{
			$adminView->assign('snippets', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('snippet_delete'));

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new snippet
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_SnippetRepository  */
		$snippetRepository = $this->getSnippetRepository();

		/** @var Admin_Form_SnippetForm */
		$snippetForm = new Admin_Form_SnippetForm();

		$defaults = $snippetRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$snippetData = $defaults;
		$snippetData['is_visible'] = 'Yes';
		$snippetData['page_types'] = 'all';
		$form = $snippetForm->getForm($snippetData, $mode, null);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			$formData['page_types'] = implode(',' ,$formData['page_types']);

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('snippets');
			}
			else
			{
				if (($validationErrors = $snippetRepository->getValidationErrors($formData, $mode)) == false)
				{
					$id = $snippetRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('snippet', array('mode' => self::MODE_UPDATE, 'id' => $id));
				}
				else
				{
					if ($formData['page_types'] == '')
					{
						$formData['page_types'] = array();
					}

					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6001');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('snippet_add'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update snippet
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_SnippetRepository */
		$snippetRepository = $this->getSnippetRepository();

		$id = intval($request->get('id'));

		$snippetData = $snippetRepository->getById($id);

		if (!$snippetData)
		{
			Admin_Flash::setMessage('Cannot find snippet by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('snippets');
		}

		/** @var Admin_Form_SnippetForm */
		$snippetForm = new Admin_Form_SnippetForm();

		$defaults = $snippetRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $snippetForm->getForm($snippetData, $mode, $id);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			$formData['page_types'] = implode(',' ,$formData['page_types']);

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('snippets');
			}
			else
			{
				if (($validationErrors = $snippetRepository->getValidationErrors($formData, $mode, $id)) == false)
				{

					$snippetRepository->persist($formData);

					Admin_Flash::setMessage('common.success');
					$this->redirect('snippet', array('mode' => self::MODE_UPDATE, 'id' => $id));
				}
				else
				{
					if ($formData['page_types'] == '')
					{
						$formData['page_types'] = array();
					}
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}


		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6002');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('snippet_update'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete snippet
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_SnippetRepository */
		$snippetRepository = $this->getSnippetRepository();

		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('snippets');
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid snippet id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$snippetRepository->delete(array($id));
				Admin_Flash::setMessage('Site snippet has been deleted');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one snippet to delete', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$snippetRepository->delete($ids);
				Admin_Flash::setMessage('Selected snippets have been deleted');
			}
		}
		else if  ($deleteMode == 'all')
		{
			$snippetRepository->deleteAll();
			Admin_Flash::setMessage('All snippets have been deleted');
		}

		$this->redirect('snippets');
	}

	/**
	 * @return DataAccess_SnippetRepository
	 */
	protected function getSnippetRepository()
	{
		if ($this->snippetRepository == null)
		{
			$this->snippetRepository = new DataAccess_SnippetRepository($this->getDb(), $this->getSettings()->getAll());
		}

		return $this->snippetRepository;
	}
}
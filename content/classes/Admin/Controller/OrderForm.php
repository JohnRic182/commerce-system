<?php

/**
 * Class Admin_Controller_OrderForm
 */
class Admin_Controller_OrderForm extends Admin_Controller
{
	/** @var DataAccess_OrderFormRepository $orderFormRepository */
	protected $orderFormRepository = null;

	/** @var DataAccess_ProductsRepository $productRepository */
	protected $productRepository = null;

	/** @var DataAccess_ProductAttributeRepository */
	protected $productAttributeRepository = null;

	protected $searchParamsDefaults = array('visibility' => 'all', 'name' => '', 'product_name' => '', 'product_id' => '');

	protected $menuItem = array('primary' => 'products', 'secondary' => 'order-forms');

	/**
	 * List order forms
	 */
	public function listAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3018');

		/** @var DataAccess_OrderFormRepository $orderFormRepository */
		$orderFormRepository = $this->getOrderFormRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$orderBy = $request->get('orderBy', null);

		// get search params
		$searchParams = $request->get('searchParams', null);
		if (is_null($searchParams))
		{
			$searchParams = $session->get('orderFormSearchParams', array());
		}
		else
		{
			$session->set('orderFormSearchParams', $searchParams);
		}

		if ($orderBy === null)
		{
			$orderBy = $session->get('orderFormSearchOrderBy', 'name');
		}
		else
		{
			$session->set('orderFormSearchOrderBy', $orderBy);
		}

		$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$adminView->assign('searchParams', $searchParams);

		$itemsCount = $orderFormRepository->getCount($searchParams);

		$orderFormForm = new Admin_Form_OrderFormForm();
		$adminView->assign('searchForm', $orderFormForm->getOrderFormSearchForm($searchParams));
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('orderForms', $orderFormRepository->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy, $searchParams));
		}
		else
		{
			$adminView->assign('orderForms', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('order_form_delete'));

		$adminView->assign('body', 'templates/pages/order-form/list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new order form
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_OrderFormRepository $orderFormRepository */
		$orderFormRepository = $this->getOrderFormRepository();

		/** @var Admin_Form_OrderFormForm $orderFormForm */
		$orderFormForm = new Admin_Form_OrderFormForm();

		$defaults = $orderFormRepository->getDefaults($mode);

		$defaults['active'] = 'Yes';
		$defaults['assignedProducts'] = array();
		/** @var core_Form_Form $form */
		$form = $orderFormForm->getForm($defaults, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!$request->request->has('is_visible')) $formData['is_visible'] = 0;

			if (!Nonce::verify($formData['nonce'], 'order_form_add'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('order_forms');
			}
			else
			{
				if (($validationErrors = $orderFormRepository->getValidationErrors($formData, $mode)) == false)
				{

					if ($formData['url_custom'] != '' && $orderFormRepository->hasUrlDuplicate($formData['url_custom'], 0))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$id = $orderFormRepository->persist($formData);
					$formData['id'] = $id;

					// TODO - optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, false, false, true);

					Admin_Flash::setMessage('common.success');
					$this->redirect('order_form', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3025');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('order_form_add'));

		$adminView->assign('body', 'templates/pages/order-form/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update order form
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_OrderFormRepository $orderFormRepository */
		$orderFormRepository = $this->getOrderFormRepository();

		$id = intval($request->get('id'));

		$orderForm = $orderFormRepository->getById($id);

		if (!$orderForm)
		{
			Admin_Flash::setMessage('Cannot find order form by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('order_forms');
		}

		$orderForm['assignedProducts'] = $orderFormRepository->getFormDetailsProducts($id);

		/** @var Admin_Form_OrderFormForm $orderFormForm */
		$orderFormForm = new Admin_Form_OrderFormForm();

		$defaults = $orderFormRepository->getDefaults($mode);

		/** @var core_Form_Form $form */
		$form = $orderFormForm->getForm($orderForm, $mode, $id);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'order_form_update'))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('order_forms');
			}
			else
			{
				if (($validationErrors = $orderFormRepository->getValidationErrors($formData, $mode, $id)) == false)
				{

					if ($formData['url_custom'] != '' && $orderFormRepository->hasUrlDuplicate($formData['url_custom'], $id))
					{
						Admin_Flash::setMessage('Custom URL already exist', Admin_Flash::TYPE_ERROR);
						$formData['url_custom'] = '';
					}

					$formData['active'] = isset($formData['active']) && $formData['active'] == 'Yes' ? 'Yes' : 'No';
					$orderFormRepository->persist($formData);

					$quantities = isset($formData['quantity']) ? $formData['quantity'] : array();
					$quantities = is_array($quantities) ? $quantities : array();

					if (count($quantities) > 0)
					{
						$orderFormRepository->updateProductsQuantities($quantities);
					}

					// TODO - optimize this
					Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, false, false, true);

					Admin_Flash::setMessage('common.success');
					$this->redirect('order_form', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// assign settings for URL auto generation
		$adminView->assign('seoUrlSettings', $this->getSeoUrlSettings());

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3026');
		$adminView->assign('title', $orderForm['name']);
		$adminView->assign('orderFormUrl', $orderForm['url']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('order_form_update'));

		$adminView->assign('delete_nonce', Nonce::create('order_form_delete'));

		$adminView->assign('body', 'templates/pages/order-form/edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete order forms
	 */
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		if (!Nonce::verify($request->get('nonce'), 'order_form_delete'))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('order_forms');
			return;
		}

		/** @var DataAccess_OrderFormRepository $orderFormRepository */
		$orderFormRepository = $this->getOrderFormRepository();

		$deleteMode = $request->get('deleteMode');

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid order form id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$orderFormRepository->delete($id);
				Admin_Flash::setMessage('Order form has been deleted');
				$this->redirect('order_forms');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one order form to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('order_forms');
			}
			else
			{
				$orderFormRepository->delete($ids);
				Admin_Flash::setMessage('Selected order forms have been deleted');
				$this->redirect('order_forms');
			}
		}
		else if ($deleteMode == 'search')
		{
			/** @var Framework_Session $session */
			$session = $this->getSession();
			$searchParams = $session->get('orderFormsSearchParams', array());
			$searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());
			$orderFormRepository->deleteBySearchParams($searchParams);
			Admin_Flash::setMessage('Selected order forms have been deleted');
			$this->redirect('order_forms');
		}
		else
		{
			Admin_Flash::setMessage('Invalid mode.', Admin_Flash::TYPE_ERROR);

			$this->redirect('order_forms');
		}
	}

	/**
	 * Products auto suggest
	 */
	public function getProductsAction()
	{
		$request = $this->getRequest();

		$searchStr = trim($request->get('term', ''));

		$products = $searchStr != '' ? $this->getProductRepository()->lookUpForOrderForm($searchStr, '') : array();

		$this->renderJson($products);
	}

	public function addProductAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		if ($request->isPost())
		{
			$data = array();
			$data['pid'] = $request->get('pid', 0);
			$data['ofid'] = $request->get('ofid', 0);
			$data['quantity'] = 1;
			$data['attributes'] = $request->get('attributes', array());

			$data['productAttributes'] = $this->getProductAttributeRepository()->getListByProductId($data['pid']);

			$orderFormRepository = $this->getOrderFormRepository();

			if ($orderFormRepository->getOneOrderFormProduct($data))
			{
				$result = array('status' => 0, 'error' => trans('order_forms.error_adding_product_dublicate'));
			}
			else
			{
				if (($validationErrors = $orderFormRepository->getValidationErrorsProduct($data, $mode)) == false)
				{
					$ofpid = $orderFormRepository->persistProduct($data, array('productAttributes' => $this->getProductAttributeRepository()->getListByProductId($data['pid'])));

					$result = array(
							'status' => 1,
							'product' => $orderFormRepository->getOneOrderFormProductById($ofpid)
					);
				}
				else
				{
					$result = array('status' => 0, 'error' => $validationErrors);
				}
			}
		}

		$this->renderJson($result);
	}

	public function deleteProductAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		if ($request->isPost())
		{

			$ofpid = $request->get('ofpid', 0);

			$orderFormRepository = $this->getOrderFormRepository();

			$orderFormProduct = $orderFormRepository->getOneOrderFormProductById($ofpid);

			if ($orderFormProduct)
			{
				$orderFormRepository->deleteProduct($ofpid);

				$result = array(
						'status' => 1
				);
			}
			else
			{
				$result = array('status' => 0, 'error' => trans('order_forms.error_deleting_product'));
			}

		}

		$this->renderJson($result);
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_OrderFormRepository
	 */
	protected function getOrderFormRepository()
	{
		if (is_null($this->orderFormRepository))
		{
			$this->orderFormRepository = new DataAccess_OrderFormRepository($this->getDb());
		}

		return $this->orderFormRepository;
	}

	/**
	 * Get product repository
	 */
	protected  function getProductRepository()
	{
		if (is_null($this->productRepository))
		{
			$this->productRepository = new DataAccess_ProductsRepository($this->getDb());
		}

		return $this->productRepository;
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductVariantRepository
	 */
	protected function getProductAttributeRepository()
	{
		if (is_null($this->productAttributeRepository))
		{
			$this->productAttributeRepository = new DataAccess_ProductAttributeRepository($this->getDb());
		}

		return $this->productAttributeRepository;
	}

	/**
	 * Get seo URL settings
	 *
	 * @return mixed|string
	 */
	protected function getSeoUrlSettings()
	{
		/** @var DataAccess_SettingsRepository */
		$settings = $this->getSettings();

		return json_encode(array(
			'template' => $settings->get('SearchURLOrderFormTemplate'),
			'lowercase' => $settings->get('SearchAutoGenerateLowercase') == 'YES',
			'joiner' => $settings->get('SearchURLJoiner')
		));
	}
}

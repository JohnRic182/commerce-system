<?php
/**
 * Class Admin_Controller_PayPalPaymentsStandard
 */
class Admin_Controller_PayPalPaymentsStandard extends Admin_Controller
{
    private $foafLocation = 'paypal';

    /**
     *  Generate our certificate
     */
    public function generateCertificateAction()
    {
        $request = $this->getRequest();
        $adminView = $this->getView();

        $paypalStandardForm = new Admin_Form_PaypalPaymentsStandardForm();

        if ($request->isPost())
        {
            require_once 'content/engine/payment/paypal/gencsr.php';

            $country = $request->get('country');
            $state =  $request->get('state');
            $city =  $request->get('city');
            $org =  $request->get('org');
            $email =  $request->get('email');

            if ($data = createIdentityCertificate($country, $state, $city, $org, $org, $org, $email, $this->foafLocation))
            {
                file_put_contents('content/engine/payment/paypal/'.$this->foafLocation.'.cer', $data['cer']);
                file_put_contents('content/engine/payment/paypal/'.$this->foafLocation.'.pem', $data['pem']);
                file_put_contents('content/engine/payment/paypal/'.$this->foafLocation.'_signature.txt', $data['signature']);

                $this->downloadCertificate();
            }
        }

        $adminView->assign('form', $paypalStandardForm->getForm());
        $adminView->assign('nonce', Nonce::create('payment_standard'));
        $adminView->render('/pages/paypal-standard/gen-cert');
    }

    /**
     * Download our certificate
     */
    public function downloadCertificateAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->downloadCertificate();
        }
    }

    /**
     * Render downloadable certificate
     */
    public function downloadCertificate()
    {
        header("Pragma: public");
        header("Expires: 0"); // set expiration time
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        // browser must download file from server instead of cache

        // force download dialog
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // use the Content-Disposition header to supply a recommended filename and
        // force the browser to display the save dialog.
        header("Content-Disposition: attachment; filename=$this->foafLocation.cer;");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize('content/engine/payment/paypal/'.$this->foafLocation.'.cer'));

        @set_time_limit(360000000);

        $f = fopen('content/engine/payment/paypal/'.$this->foafLocation.'.cer', "r");
        while ($s = fread($f, 1024)) echo $s;
        fclose($f);

        exit();
    }
}
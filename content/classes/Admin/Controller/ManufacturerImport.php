<?php

/**
 * Class Admin_Controller_ManufacturerBulk
 */
class Admin_Controller_ManufacturerImport extends Admin_Controller
{
	/** @var DataAccess_ManufacturerRepository $manufacturerRepository */
	protected $manufacturerRepository = null;

	/** @var Admin_Service_ManufacturerImport $manufacturerImportService */
	protected $manufacturerImportService = null;

	/** @var array $defaults */
	protected $defaults = array(
		'fields_separator' => ',',
		'make_manufacturers_visible' => 'Yes',
		'make_manufacturers_visible_type' => 'Yes'
	);

	protected $menuItem = array('primary' => 'products', 'secondary' => 'manufacturers');

	/**
	 * Display manufacturers CSV upload form
	 */
	public function startAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Admin_Form_ManufacturerImportForm $manufacturerImportForm */
		$manufacturerImportForm = new Admin_Form_ManufacturerImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $manufacturerImportForm->getStartForm(array());

		if ($request->isPost())
		{
			$formData = array_merge($this->defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('manufacturers');
			}
			else
			{
				/** @var Admin_Service_ManufacturerImport $importService */
				$importService = $this->getManufacturerImportService();

				$errors = array();

				if ($fileInfo = $request->getUploadedFile('bulk'))
				{
					if ($uploadedFile = $importService->saveUploadedFile($fileInfo))
					{
						/** @var Framework_Session $session */
						$session = $this->getSession();

						$session->set('manufacturer-import-settings', array(
							'file' => $uploadedFile,
							'fields_separator' => $formData['fields_separator'],
							'make_manufacturers_visible' => $formData['make_manufacturers_visible'],
							'make_manufacturers_visible_type' => $formData['make_manufacturers_visible_type']
						));

						$this->redirect('bulk_manufacturers', array('action' => 'assign'));
					}
					else
					{
						$errors['bulk'][] = 'Cannot save uploaded file. Please check free space and permissions';
					}
				}
				else
				{
					$errors['bulk'][] = 'Please select file for uploading';
				}

				$form->bind($formData);
				$form->bindErrors($errors);
				Admin_Flash::setMessage(trans('common.fix_errors').$this->formatValidationErrors($errors), Admin_Flash::TYPE_ERROR);
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3015');

		$adminView->assign('page', 'bulk_manufacturers');
		$adminView->assign('pageCancel', 'manufacturers');
		$adminView->assign('pageTitle', 'Import Manufacturers');

		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('manufacturer_import_start'));

		$adminView->assign('body', 'templates/pages/import/start.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Display fields assignment form
	 */
	public function assignAction()
	{
		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = $session->get('manufacturer-import-settings', $this->defaults);

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('Cannot find uploaded file', Admin_Flash::TYPE_ERROR);
			$this->redirect('manufacturers');
		}

		/** @var array $formData */
		$formData = array_merge($importSettings, $this->defaults);

		/** @var Admin_Service_ManufacturerImport $importService */
		$importService = $this->getManufacturerImportService();

		/** @var array $assignOptions */
		$assignOptions = $importService->getAssignOptions($uploadedFile, $formData['fields_separator']);

		$formData = array_merge($formData, $assignOptions);
		$formData['skip_first_line'] = count($assignOptions['columns']) > 1 ? '1' : '0';

		/** @var Admin_Form_ManufacturerImportForm $manufacturerImportForm */
		$manufacturerImportForm = new Admin_Form_ManufacturerImportForm();

		/** @var core_Form_Form $formBuilder */
		$form = $manufacturerImportForm->getAssignForm($formData);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('fields', $fields = $importService->getFields());
		$adminView->assign('fieldsJson', json_encode($fields));
		$adminView->assign('assignOptions', $assignOptions);

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3016');

		$adminView->assign('page', 'bulk_manufacturers');
		$adminView->assign('pageCancel', 'manufacturers');
		$adminView->assign('pageTitle', 'Import Manufacturers');
		$adminView->assign('form', $form);

		$adminView->assign('body', 'templates/pages/import/assign.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Do import and finish
	 */
	public function importAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var Framework_Session $session */
		$session = $this->getSession();

		/** @var array $importSettings */
		$importSettings = $session->get('manufacturer-import-settings', $this->defaults);

		/** @var string $uploadedFile */
		$uploadedFile = is_array($importSettings) && isset($importSettings['file']) && is_file($importSettings['file']) ? $importSettings['file'] : false;

		if (!$uploadedFile)
		{
			Admin_Flash::setMessage('Cannot find uploaded file', Admin_Flash::TYPE_ERROR);
			$this->redirect('manufacturers');
		}

		/** @var array $formData */
		$formData = $request->request->all();

		/** @var Admin_Service_ManufacturerImport $importService */
		$importService = $this->getManufacturerImportService();

		$result = $importService->import(
			$uploadedFile, $formData['assign_fields'],
			$importSettings['fields_separator'],
			isset($formData['skip_first_line']) && $formData['skip_first_line'] == 1,
			array(
				'makeVisible' => $importSettings['make_manufacturers_visible'] == 'Yes',
				'makeVisibleExisting' => $importSettings['make_manufacturers_visible_type'] != 'inserted'
			)
		);

		Seo::updateSeoURLs($this->getDb(), $this->getSettings()->getAll(), false, false, false, true);

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '3017');

		$adminView->assign('page', 'bulk_manufacturers');
		$adminView->assign('pageCancel', 'manufacturers');
		$adminView->assign('pageTitle', 'Import Manufacturers');

		if ($result)
		{
			/** @var Admin_Form_ManufacturerImportForm $manufacturerImportForm */
			$manufacturerImportForm = new Admin_Form_ManufacturerImportForm();

			/** @var core_Form_Form $formBuilder */
			$form = $manufacturerImportForm->getImportForm($result);

			$adminView->assign('form', $form);
		}
		else
		{
			Admin_Flash::setMessage('Cannot import data. Something went wrong.', Admin_Flash::TYPE_ERROR);
		}

		$adminView->assign('body', 'templates/pages/import/import.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_ManufacturerRepository
	 */
	protected function getManufacturerRepository()
	{
		if (is_null($this->manufacturerRepository))
		{
			$this->manufacturerRepository = new DataAccess_ManufacturerRepository($this->getDb());
		}

		return $this->manufacturerRepository;
	}

	/**
	 * Get import service
	 *
	 * @return Admin_Service_ManufacturerImport
	 */
	protected function getManufacturerImportService()
	{
		if (is_null($this->manufacturerImportService))
		{
			$this->manufacturerImportService = new Admin_Service_ManufacturerImport(
				$this->getManufacturerRepository(),
				new Admin_Service_FileUploader()
			);
		}

		return $this->manufacturerImportService;
	}
}
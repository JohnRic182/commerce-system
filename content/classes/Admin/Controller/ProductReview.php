<?php

/**
 * Class Admin_Controller_ProductReview
 */
class Admin_Controller_ProductReview extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/product-review/';

	protected $Repository = null;

	protected $searchParamsDefaults = array('search_str' => '','product_id' => '', 'title' => '', 'keyword' => '', 'reviewer'=>'' ,'status' => 'any', 'logic' => 'AND');

	protected $menuItem = array('primary' => 'content', 'secondary' => 'products-reviews');

	/**
	 * List reviews
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6006');

		$rep = $this->getDataRepository();

		$request = $this->getRequest();
		$session = $this->getSession();

		/**
		 * Prepare search & list params
		 */
		$orderBy = $request->get('orderBy', null);
		$orderDir = $request->get('orderDir', null);
		$searchParams = $request->get('searchParams', null);

		if ($searchParams === null) $searchParams = $session->get('productsReviewSearchParams', array()); else $session->set('productsReviewSearchParams', $searchParams);
		if ($orderBy === null) $orderBy = $session->get('productsReviewSearchOrderBy', 'title'); else $session->set('productsReviewSearchOrderBy', $orderBy);
		if ($orderDir === null) $orderDir = $session->get('productsReviewSearchOrderDir', 'asc'); else $session->set('productsReviewSearchOrderDir', $orderDir);

		$searchFormData = $searchParams = array_merge($this->searchParamsDefaults, is_array($searchParams) ? $searchParams : array());

		$searchFormData['orderBy'] = $orderBy;
		$searchFormData['orderDir'] = $orderDir;

		$productReviewForm = new Admin_Form_ProductReviewForm();

		$adminView->assign('hasSearchParams', $request->query->has('searchParams') || $request->request->has('searchParams'));
		$adminView->assign('searchParams', $searchParams);
		$adminView->assign('searchForm', $productReviewForm->getProductReviewSearchForm($searchFormData));

		$itemsCount = $rep->getCount($searchParams, $searchParams['logic']);
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('products_reviews',
				$rep->getList(
					$paginator->sqlOffset,
					$paginator->itemsPerPage,
					$orderBy . '_' . $orderDir,
					$searchParams,
					'pr.id AS review_id, pr.pid, p.product_id, pr.user_id, pr.review_title, pr.review_text, pr.rating, pr.status',
					$searchParams['logic']
				)
			);
		}
		else
		{
			$adminView->assign('products_reviews', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('products_reviews_delete'));
		$adminView->assign('update_statuses_nonce', Nonce::create('update_statuses_nonce'));

		$adminView->assign('settingsNonce', Nonce::create('settings_update'));
		$adminView->assign('settingsForm', $this->getSettingsForm());

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update product review
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$id = intval($request->get('id'));

		$product_review = $repository->getById($id);

		if (!$product_review)
		{
			Admin_Flash::setMessage('Cannot find product review by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_reviews');
		}

		$TPForm = new Admin_Form_ProductReviewForm();

		$defaults = $repository->getDefaults($mode);

		$form = $TPForm->getForm($product_review, $mode, $id);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_reviews');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					$this->redirect('products_reviews');
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '6007');

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('product_review_update'));
		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	public function updateStatusAction()
	{
		$request = $this->getRequest();

		$repository = $this->getDataRepository();

		$updateMode = $request->get('updateMode');
		$new_status = $request->get('new_status');

		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('products_reviews');
			return;
		}

		if ($updateMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0){
				Admin_Flash::setMessage('Select at least one product review to update', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_reviews');
			}
			else
			{
				$repository->updateStatus($ids, $new_status);
				Admin_Flash::setMessage('Selected product reviews have been updated');
				$this->redirect('products_reviews');
			}
		}
		else if ($updateMode == 'search')
		{
			$repository->updateStatus("all", $new_status);
			Admin_Flash::setMessage('Product reviews have been updated');
			$this->redirect('products_reviews');
		}
	}

	protected function getSettingsForm()
	{
		$repository = $this->getDataRepository();

		$data = $repository->getAdvancedSettingsData();

		$TRForm = new Admin_Form_ProductReviewAdvancedSettingsForm();

		return $TRForm->getForm($data);
	}

	/**
	 * Edit products review settings
	 */
	public function editAdvancedSettingsAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();
		$repository = $this->getDataRepository();

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'settings_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{

				if (($validationErrors = $repository->getValidationSettingsErrors($formData, $mode)) == false)
				{
					$repository->persistProductReviewSettings($formData);

					Admin_Flash::setMessage('common.success', Admin_Flash::TYPE_SUCCESS);
					$this->renderJson(array(
						'status' => 1
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}
	}
	
	public function deleteAction()
	{
		
		$request = $this->getRequest();
		$repository = $this->getDataRepository();
		$id = $request->get('id');
		if ($id != null)
		{
			$repository->deleteReviews($id);
			Admin_Flash::setMessage('Selected product review have been deleted');
			$this->redirect('products_reviews');
		}
		else
		{
			$deleteMode = $request->get('deleteMode');
			if (!Nonce::verify($request->get('nonce')))
			{
				Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('products_reviews');
				return;
			}
	
			if ($deleteMode == 'selected')
			{
				$ids = explode(',', $request->get('ids', ''));
				if (count($ids) == 0){
					Admin_Flash::setMessage('Select at least one product review to delete', Admin_Flash::TYPE_ERROR);
					$this->redirect('products_reviews');
				}
				else
				{
					$repository->deleteReviews($ids);
					Admin_Flash::setMessage('Selected product reviews have been deleted');
					$this->redirect('products_reviews');
				}
			}
			else if ($deleteMode == 'search')
			{
				$repository->deleteReviews();
				Admin_Flash::setMessage('Product reviews have been deleted');
				$this->redirect('products_reviews');
			}
		}
	}
	
	/**
	 * Get repository
	 *
	 * @return DataAccess_ProductReviewRepository
	 */
	protected function getDataRepository()
	{
		if (is_null($this->Repository))
		{
			$this->Repository = new DataAccess_ProductReviewRepository($this->getDb(), $this->getSettings()->getAll());
		}
		return $this->Repository;
	}
}
<?php

/**
 * Class Admin_Controller_Language
 */
class Admin_Controller_Language extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/language/';

	/** @var DataAccess_LanguageRepository  */
	protected $languageRepository = null;

	protected $menuItem = array('primary' => 'settings', 'secondary' => 'setup');

	/**
	 * List languages
	 */
	public function listAction()
	{
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8011');

		$repository = $this->getLanguageRepository();

		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'name');

		// get search params
		$searchParams = array();
		$adminView->assign('searchParams', $searchParams);

		$itemsCount = $repository->getCount($searchParams);
		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$adminView->assign('languages', $repository->getList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy, $searchParams));
		}
		else
		{
			$adminView->assign('languages', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('language_delete'));
		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new language
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		$request = $this->getRequest();

		$repository = $this->getLanguageRepository();

		$langForm = new Admin_Form_LanguageForm();

		$defaults = $repository->getDefaults($mode);

		$form = $langForm->getForm($defaults, $mode, null, $repository->getExistingLanguages());

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('languages');
			}
			else
			{
				$formData['template_file'] = $request->getUploadedFile('template_file');

				if (isset($formData['template_file']['tmp_name'])) $this->cleanLanguageFile($formData['template_file']['tmp_name']);

				if (($validationErrors = $repository->getValidationErrors($formData, $mode)) == false)
				{
					$id = $repository->persist($formData);

					if ($id)
					{
						Admin_Flash::setMessage('common.success');
						$this->redirect('language', array('id' => $id, 'mode' => self::MODE_UPDATE));
					}
					else
					{
						$form->bind($formData);
						$persistErrors = $repository->getLastPersistErrors();
						Admin_Flash::setMessage('Error saving language'.$this->formatValidationErrors($persistErrors), Admin_Flash::TYPE_ERROR);
					}
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('language_add'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Update text page
	 */
	public function updateAction()
	{
		$mode = self::MODE_UPDATE;

		$request = $this->getRequest();

		$repository = $this->getLanguageRepository();

		$id = intval($request->get('id'));

		$language = $repository->getById($id);

		if (!$language)
		{
			Admin_Flash::setMessage('Cannot find language by id', Admin_Flash::TYPE_ERROR);
			$this->redirect('languages');
		}

		$langForm = new Admin_Form_LanguageForm();

		$defaults = $repository->getDefaults($mode);

		$form = $langForm->getForm($language, $mode, $id);

		/**
		 * Handle form post
		*/
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			$formData['is_active'] = $request->request->get('is_active', 'No');

			if ($language['is_default'] == 'Yes') unset($formData['is_active']);
			if (!$request->request->has('is_default')) unset($formData['is_default']);

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('languages');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData, $mode, $id)) == false)
				{
					$repository->persist($formData);
					Admin_Flash::setMessage('common.success');
					sleep(1);
					$this->redirect('language', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8012');
		$adminView->assign('title', $language['name']);

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('language_update'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Delete languages
	 */
	public function deleteAction()
	{
		$request = $this->getRequest();

		$repository = $this->getLanguageRepository();

		$deleteMode = $request->get('deleteMode');

		if (!Nonce::verify($request->get('nonce'))){
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('languages');
			return;
		}

		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);
			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid language id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->delete(array($id));

				Admin_Flash::setMessage('Language has been deleted');
				$this->redirect('languages');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));
			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one language to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('languages');
			}
			else
			{
				$repository->delete($ids);
				Admin_Flash::setMessage('Selected languages have been deleted');
				$this->redirect('languages');
			}
		}
		else if ($deleteMode == 'search')
		{
			$repository->deleteBySearchParams();
			Admin_Flash::setMessage('All languages but default have been deleted');
			$this->redirect('languages');
		}
	}

	/**
	 * Set default language action
	 */
	public function setDefaultLanguageAction()
	{
		$repository = $this->getLanguageRepository();

		$request = $this->getRequest();
		$id = intval($request->get('id'));

		$repository->setDefaultLanguage($id);

		$this->renderJson(array('lang_made_visible' => $request->get('id')));
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_LanguageRepository
	 */
	protected function getLanguageRepository()
	{
		if (is_null($this->languageRepository))
		{
			$this->languageRepository = new DataAccess_LanguageRepository($this->getDb());
		}
		
		return $this->languageRepository;
	}

	/**
	 * @param $file
	 */
	public function cleanLanguageFile($file)
	{
		$resultFile = str_replace("\r\n", "\n", file_get_contents($file));
		file_put_contents($file, $resultFile);
	}
}
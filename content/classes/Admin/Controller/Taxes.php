<?php

/**
 * Class Admin_Controller_Taxes
 */
class Admin_Controller_Taxes extends Admin_Controller
{
	const TEMPLATES_PATH = 'templates/pages/taxes/';

	/** @var DataAccess_TaxRepository  */
	protected $taxRepository = null;
	protected $countryRepository = null;

	protected $menuItem = array('primary' => 'settings', 'secondary' => 'setup');

	/**
	 * List Site Taxes
	 */
	public function listAction()
	{
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8006');
		
		$taxRepository = $this->getTaxRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'rate_priority');

		// get search params
		$searchParams = array();
		$adminView->assign('searchParams', $searchParams);

		$itemsCount = $taxRepository->getRatesCount();

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);
			$rates = $taxRepository->getRatesList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy);

			$adminView->assign('taxrates', $rates);
		}
		else
		{
			$adminView->assign('taxrates', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('textpage_delete'));

		$adminView->assign('settingsForm', $this->getSettingsForm());
		$adminView->assign('settingsNonce', Nonce::create('settings_update'));

		$adminView->assign('countriesStates', json_encode($this->getCountryRepository()->getCountriesStates()));

		$adminView->assign('body', self::TEMPLATES_PATH.'list.html');
		$adminView->render('layouts/default');
	}

	/**
	 * Add new tax rate
	 */
	public function addAction()
	{
		$mode = self::MODE_ADD;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TaxRepository  */
		$repository = $this->getTaxRepository();

		/** @var Admin_Form_TaxRateForm */
		$TRForm = new Admin_Form_TaxRateForm($this->getSettings());

		$defaults = $repository->getRatesDefaults();

		/** @var core_Form_Form $form */

		$taxes_zones = $repository->getTaxZonesList();
		$taxes_classes = $repository->getTaxClassesList();

		$defaults['taxes_zones'] = $taxes_zones;
		$defaults['taxes_classes'] = $taxes_classes;

		$form = $TRForm->getForm($defaults, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			$userLevels = $request->request->get('user_level', array());

			if (!is_array($userLevels))
			{
				$userLevels = array();
			}

			if (count($userLevels) == 0)
			{
				$userLevels = array('0', '1', '2', '3');
			}

			$formData['user_level'] = $userLevels;

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('taxes');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData)) == false)
				{
					$id = $repository->persistRate($formData);

					$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_RATE_ADDED);
					$taxEvent->setData('taxRate', $formData);
					Events_EventHandler::handle($taxEvent);

					Admin_Flash::setMessage('common.success');
					$this->redirect('taxes_rate', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8007');
		$adminView->assign('countriesStates', json_encode($this->getCountryRepository()->getCountriesStates()));

		$adminView->assign('mode', $mode);
		$adminView->assign('id', null);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('tax_rate_add'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}
	
	/**
	 * Edit tax rate
	 */
	public function editAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TaxRepository  */
		$repository = $this->getTaxRepository();

		$id = intval($request->get('id'));

		$data = $repository->getRateDataById($id);

		if (!$data)
		{
			Admin_Flash::setMessage('Cannot find tax rate by provided id', Admin_Flash::TYPE_ERROR);
			$this->redirect('taxes');
		}
		else
		{
			$taxes_zones = $repository->getTaxZonesList();
			$taxes_classes = $repository->getTaxClassesList();
			$data['taxes_zones'] = $taxes_zones;
			$data['taxes_classes'] = $taxes_classes;
			$zone_id = $data['zone_id'];
		}

		/** @var Admin_Form_TaxRateForm */
		$TRForm = new Admin_Form_TaxRateForm($this->getSettings());

		$defaults = $repository->getRatesDefaults();

		/** @var core_Form_Form $form */

		$taxes_zones = $repository->getTaxZonesList();
		$taxes_classes = $repository->getTaxClassesList();

		$defaults['taxes_zones'] = $taxes_zones;
		$defaults['taxes_classes'] = $taxes_classes;

		$form = $TRForm->getForm($data, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			$userLevels = $request->request->get('user_level', array());

			if (!is_array($userLevels))
			{
				$userLevels = array();
			}

			if (count($userLevels) == 0)
			{
				$userLevels = array('0', '1', '2', '3');
			}

			$formData['user_level'] = $userLevels;

			if (!Nonce::verify($formData['nonce']))
			{
				Admin_Flash::setMessage('common.invalid_nonce', Admin_Flash::TYPE_ERROR);
				$this->redirect('taxes');
			}
			else
			{
				if (($validationErrors = $repository->getValidationErrors($formData)) == false)
				{
					$id = $repository->persistRate($formData);

					$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_RATE_UPDATED);
					$taxEvent->setData('taxRate', $formData);
					Events_EventHandler::handle($taxEvent);

					Admin_Flash::setMessage('common.success');
					$this->redirect('taxes_rate', array('id' => $id, 'mode' => self::MODE_UPDATE));
				}
				else
				{
					$form->bind($formData);
					$form->bindErrors($validationErrors);
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8008');
		$adminView->assign('title', $data['rate_description']);
		$adminView->assign('countriesStates', json_encode($this->getCountryRepository()->getCountriesStates()));

		$adminView->assign('mode', $mode);
		$adminView->assign('id', $id);
		$adminView->assign('zone_id', $zone_id);
		$adminView->assign('form', $form);
		$adminView->assign('nonce', Nonce::create('tax_rate_edit'));

		$adminView->assign('body', self::TEMPLATES_PATH.'edit.html');
		$adminView->render('layouts/default');
	}

   /**
	* Remove tax rate
	*/
	public function deleteAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TextPageRepository */
		$repository = $this->getTaxRepository();

		$deleteMode = $request->get('deleteMode');
		
		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('taxes');
			return;
		}
		
		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);

			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid tax rate id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				$repository->deleteRate(array($id));

				$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_RATE_DELETED);
				$taxEvent->setData('taxRateIds', array($id));
				Events_EventHandler::handle($taxEvent);

				Admin_Flash::setMessage('common.success');
				$this->redirect('taxes');
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one tax rate to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('taxes');
			}
			else
			{
				$repository->deleteRate($ids);

				$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_RATE_DELETED);
				$taxEvent->setData('taxRateIds', $ids);
				Events_EventHandler::handle($taxEvent);

				Admin_Flash::setMessage('common.success');
				$this->redirect('taxes');
			}
		}
		else if ($deleteMode == 'all')
		{
			$repository->deleteAllRates();

			$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_RATE_DELETED);
			$taxEvent->setData('taxRateIds', array());
			Events_EventHandler::handle($taxEvent);

			Admin_Flash::setMessage('common.success');
			$this->redirect('taxes');
		}
	}

	/**
	 * List tax classes
	 */
	public function listTaxesClassesAction()
	{
		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		// set menu
		$adminView->assign('activeMenuItem', $this->menuItem);
		$adminView->assign('helpTag', '8009');

		$repository = $this->getTaxRepository();

		/** @var Framework_Request $request */
		$request = $this->getRequest();
		$orderBy = $request->get('orderBy', 'class_name');

		$itemsCount = $repository->getClassesCount();

		$adminView->assign('itemsCount', $itemsCount);

		if ($itemsCount)
		{
			$paginator = new Admin_Paginator($itemsCount, 25, $request->get('page', 1));
			$adminView->assign('paginator', $paginator);

			$classes = $repository->getClassesList($paginator->sqlOffset, $paginator->itemsPerPage, $orderBy);
			$adminView->assign('taxclasses', $classes);
		}
		else
		{
			$adminView->assign('taxclasses', null);
		}

		$adminView->assign('delete_nonce', Nonce::create('taxclasses_delete'));

		$adminView->assign('addNonce', Nonce::create('tax_class_add'));
		$adminView->assign('taxClassForm', $this->getAddTaxClassForm());

		$adminView->assign('body', self::TEMPLATES_PATH.'classes/list.html');
		$adminView->render('layouts/default');
	}

	protected function getAddTaxClassForm()
	{
		$mode = self::MODE_ADD;

		/** @var DataAccess_TaxRepository  */
		$repository = $this->getTaxRepository();

		/** @var Admin_Form_TaxClassForm */
		$TRForm = new Admin_Form_TaxClassForm();

		$defaults = $repository->getClassesDefaults();

		return $TRForm->getForm($defaults, $mode);
	}

	/**
	 * Add new tax rate class
	 */
	public function addTaxClassAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TaxRepository  */
		$repository = $this->getTaxRepository();

		$defaults = $repository->getClassesDefaults();

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'tax_class_add', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getTaxClassesValidationErrors($formData)) == false)
				{
					$repository->persistClass($formData);

					$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_CLASS_ADDED);
					$taxEvent->setData('taxClass', $formData);
					Events_EventHandler::handle($taxEvent);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1,
						'message' => trans('common.success'),
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}
	}

	/**
	 * Edit tax rate class
	 */
	public function editTaxClassAction()
	{
		$mode = self::MODE_UPDATE;

		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TaxRepository  */
		$repository = $this->getTaxRepository();
		
		$id = intval($request->get('id'));

		$data = $repository->getTaxClassDataById($id);

		/** @var Admin_Form_TaxClassForm */
		$TRForm = new Admin_Form_TaxClassForm();

		$defaults = $repository->getClassesDefaults();

		/** @var core_Form_Form $form */
		$form = $TRForm->getForm($data, $mode);

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = array_merge($defaults, $request->request->all());

			if (!Nonce::verify($formData['nonce'], 'tax_class_edit', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				if (($validationErrors = $repository->getTaxClassesValidationErrors($formData)) == false)
				{
					$repository->persistClass($formData);

					$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_CLASS_UPDATED);
					$taxEvent->setData('taxClass', $formData);
					Events_EventHandler::handle($taxEvent);

					Admin_Flash::setMessage('common.success');

					$this->renderJson(array(
						'status' => 1,
						'message' => trans('common.success'),
					));
				}
				else
				{
					$this->renderJson(array(
						'status' => 0,
						'errors' => $validationErrors,
					));
				}
			}
		}

		/** @var Ddm_AdminView $adminView */
		$adminView = $this->getView();

		$adminView->assign('id', $id);
		$adminView->assign('taxClassForm', $form);
		$adminView->assign('updateNonce', Nonce::create('tax_class_edit'));
		$adminView->assign('title', $data['class_name']);

		$html = $adminView->fetch('pages/taxes/classes/modal-edit');

		$this->renderJson(array(
			'status' => 1,
			'html' => $html,
		));
	}

	/**
	 * Remove tax class
	 */
	public function deleteTaxClassAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		/** @var DataAccess_TextPageRepository */
		$repository = $this->getTaxRepository();

		$deleteMode = $request->get('deleteMode');
		
		if (!Nonce::verify($request->get('nonce')))
		{
			Admin_Flash::setMessage('Cannot verify nonce', Admin_Flash::TYPE_ERROR);
			$this->redirect('taxes_classes');
			return;
		}
		
		if ($deleteMode == 'single')
		{
			$id = $request->get('id', 0);

			if ($id < 1)
			{
				Admin_Flash::setMessage('Invalid tax tax class id', Admin_Flash::TYPE_ERROR);
			}
			else
			{
				if (($validationErrors = $repository->getDeleteClassesValidationErrors(array($id))) == false)
				{
					$repository->deleteClass(array($id));

					$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_CLASS_DELETED);
					$taxEvent->setData('taxClassIds', array($id));
					Events_EventHandler::handle($taxEvent);

					Admin_Flash::setMessage('common.success');
					$this->redirect('taxes_classes');
				}
				else
				{
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
					$this->redirect('taxes_classes');
				}
			}
		}
		else if ($deleteMode == 'selected')
		{
			$ids = explode(',', $request->get('ids', ''));

			if (count($ids) == 0)
			{
				Admin_Flash::setMessage('Please select at least one tax class to delete', Admin_Flash::TYPE_ERROR);
				$this->redirect('taxes_classes');
			}
			else
			{
				if (($validationErrors = $repository->getDeleteClassesValidationErrors($ids)) == false)
				{
					$repository->deleteClass($ids);

					$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_CLASS_DELETED);
					$taxEvent->setData('taxClassIds', $ids);
					Events_EventHandler::handle($taxEvent);

					Admin_Flash::setMessage('common.success');
					$this->redirect('taxes_classes');
				}
				else
				{
					Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
					$this->redirect('taxes_classes');
				}
			}
		}
		else if ($deleteMode == 'all')
		{
			if (($validationErrors = $repository->getDeleteClassesValidationErrors('all')) == false)
			{
				$repository->deleteAllClasses();

				$taxEvent = new Events_TaxEvent(Events_TaxEvent::ON_TAX_CLASS_DELETED);
				$taxEvent->setData('taxClassIds', array());
				Events_EventHandler::handle($taxEvent);

				Admin_Flash::setMessage('common.success');
				$this->redirect('taxes_classes');
			}
			else
			{
				Admin_Flash::setMessage('Please fix errors'.$this->formatValidationErrors($validationErrors), Admin_Flash::TYPE_ERROR);
				$this->redirect('taxes_classes');
			}
		}
	}

	protected function getSettingsForm()
	{
		$settings = $this->getSettings();

		$taxSettingsVars = array('TaxAddress', 'TaxDefaultCountry', 'TaxDefaultState', 'TaxExemptionsActive');

		$data = $settings->getByKeys($taxSettingsVars);

		/** @var Admin_Form_TaxAdvancedSettingsForm */
		$TRForm = new Admin_Form_TaxAdvancedSettingsForm();

		/** @var core_Form_Form $form */
		return $TRForm->getForm($data);
	}

	/**
	 * Edit tax rate settings
	 */
	public function editAdvancedSettingsAction()
	{
		/** @var Framework_Request $request */
		$request = $this->getRequest();

		$settings = $this->getSettings();

		$taxSettingsVars = array('TaxAddress', 'TaxDefaultCountry', 'TaxDefaultState', 'TaxExemptionsActive');

		/**
		 * Handle form post
		 */
		if ($request->isPost())
		{
			$formData = $request->request->all();

			if (!Nonce::verify($formData['nonce'], 'settings_update', false))
			{
				$this->renderJson(array(
					'status' => 0,
					'message' => trans('common.invalid_nonce'),
				));
			}
			else
			{
				$taxSettings = isset($formData['taxSettings']) ? is_array($formData['taxSettings']) ? $formData['taxSettings'] : array() : array();
				$settings->persist($taxSettings, $taxSettingsVars);

				$this->renderJson(array(
					'status' => 1,
					'message' => trans('common.success'),
					'errors' => null,
				));
			}
		}
	}

	/**
	 * Get repository
	 *
	 * @return DataAccess_TaxRepository
	 */
	protected function getTaxRepository()
	{
		if (is_null($this->taxRepository))
		{
			$this->taxRepository = new DataAccess_TaxRepository($this->getDb());
		}

		return $this->taxRepository;
	}

	/**
	 * Get country repository
	 *
	 * @return DataAccess_CountryRepository
	 */
	protected function getCountryRepository()
	{
		if (is_null($this->countryRepository))
		{
			$this->countryRepository = new DataAccess_CountryRepository($this->getDb());
		}

		return $this->countryRepository;
	}
}
<?php

class Admin_Menu extends Admin_MenuItem
{
	/**
	 * Class constructor
	 *
	 * @param $id
	 * @param $title
	 * @param null $classInner
	 * @param null $classOuter
	 */
	public function __construct($id, $title, $classInner = null, $classOuter = null)
	{
		parent::__construct($id, null, $title, null, null, $classInner, $classOuter);
	}
	
	/**
	 * Get all items
	 * @return array
	 */
	public function getAll()
	{
		return self::$items;
	}

	/**
	 * Find menu item by id
	 *
	 * @param $id
	 * @return null
	 */
	public function getItemById($id)
	{
		return isset(self::$items[$id]) ? self::$items[$id] : null;
	}

	/**
	 * Find menu item by URL
	 *
	 * @param $matchUrl
	 * @return null
	 */
	public function getItemByUrl($matchUrl)
	{
		$matchedItemMatchesCount = 0;
		$matchedItem = null;
		
		//parse matched URL
		
		$matchUrlParsed = parse_url($matchUrl);
		
		if (!isset($matchUrlParsed['query'])) return null;
		
		$matchUrlQueryParts = explode('&', $matchUrlParsed['query']);
		$matchUrlQuery = array();

		foreach ($matchUrlQueryParts as $matchUrlQueryPart)
		{
			if ($matchUrlQueryPart == '') continue;
			
			$matchUrlQueryPartPieces = explode('=', $matchUrlQueryPart);
			
			if (isset($matchUrlQueryPartPieces[0]) && isset($matchUrlQueryPartPieces[1]))
			{
				$matchUrlQuery[$matchUrlQueryPartPieces[0]] = $matchUrlQueryPartPieces[1];
			}
		}
		
		if (count($matchUrlQuery) == 0) return null;
		
		$pluginMatch = false;
		
		//try to find item	
		foreach (self::$items as $item)
		{
			$urlMatch = false;
			$pageMatch = false;
			$modeMatch = false;

			if (!is_null($item->url) || !is_null($item->alternativeUrls))
			{
				//get all item's urls
				$itemUrls = is_null($item->alternativeUrls) ? array() : $item->alternativeUrls;
				if (!is_null($item->url)) $itemUrls[] = $item->url;

				//match URLs
				foreach ($itemUrls as $itemUrl)
				{
					/**
					 * Check exact URL match
					 */
					if ($itemUrl == $matchUrl) return $item;
					
					/**
					 * Check by URL parts
					 */
					$itemUrlParsed = parse_url($itemUrl);
					$itemUrlQuery = null;

					if (!isset($itemUrlParsed['query'])) continue;
					
					$itemUrlQueryParts = explode('&', $itemUrlParsed['query']);
					$itemUrlQuery = array();
					
					foreach ($itemUrlQueryParts as $itemUrlQueryPart)
					{
						$itemUrlQueryPartPieces = explode('=', $itemUrlQueryPart);
						$itemUrlQuery[$itemUrlQueryPartPieces[0]] = $itemUrlQueryPartPieces[1];
					}

					$matchesCount = 0;
					foreach ($matchUrlQuery as $mParamName => $mParamValue)
					{
						foreach ($itemUrlQuery as $iParamName => $iParamValue)
						{
							if ($mParamName == $iParamName && $mParamValue == $iParamValue)
							{
								$matchesCount++;
								if ($mParamName == 'p') $pageMatch = true;
							}
						}
					}
					
					$modeMatch = (isset($matchUrlQuery['mode']) && isset($itemUrlQuery['mode']) && $matchUrlQuery['mode'] == $itemUrlQuery['mode']) || !isset($matchUrlQuery['mode']);

					// exclusion for plugins
					if (isset($matchUrlQuery['p']) && $matchUrlQuery['p'] == 'plugin')
					{
						if (isset($itemUrlQuery['p']) && $itemUrlQuery['p'] == 'plugins' && is_null($matchedItem))
						{
							$matchedItem = $item;
						}
						else if(isset($itemUrlQuery['p']) && $itemUrlQuery['p'] == 'plugin' && isset($itemUrlQuery['plugin_id']) && isset($matchUrlQuery['plugin_id']) && $itemUrlQuery['plugin_id'] == $matchUrlQuery['plugin_id'])
						{
							$matchedItem = $item;
							$pluginMatch = true;
						}
					}
					else
					{
						if ($pageMatch && $modeMatch && $matchesCount > $matchedItemMatchesCount)
						{
							$matchedItemMatchesCount = $matchesCount;
							$matchedItem = $item;
						}
					}
				}
			}
			if ($pluginMatch) break;
		}
		return $matchedItem;
	}
}
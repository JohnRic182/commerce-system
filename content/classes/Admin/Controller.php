<?php

/**
 * Class Admin_Controller
 */
class Admin_Controller extends Framework_Controller
{
	protected $currentAdminId;

	// standard actions
	const MODE_ADD = 'add';
	const MODE_UPDATE = 'update';
	const MODE_DELETE = 'delete';

	protected $accessRequired = PRIVILEGE_ALL;

	protected $quickStartGuiderService = null;

	/**
	 * @return Ddm_AdminView
	 */
	public function getView()
	{
		global $auth_rights;

		$view = Ddm_AdminView::getInstance();

		$isHosted = Admin_HostedHelper::isHosted();
		$settings = $this->getSettings();

		if ($settings->get('QuickStartCompleted') != '1')
		{
			$view->assign('qsgAvailable', true);
			$settings->save(array('QuickStartCompleted' => '1'));
		}

		if (defined('TRIAL_MODE') && TRIAL_MODE)
		{
			global $label_affiliate_tracking_url;

			if ($label_affiliate_tracking_url && $label_affiliate_tracking_url != '')
			{
				$view->assign('qsg_affiliate_tracking_iframe', true);
				$view->assign('qsg_affiliate_tracking_url', $label_affiliate_tracking_url);
			}
		}

		$view->assign('isHosted', $isHosted);
		$view->assign('accessBilling', isPrivilege($auth_rights, array('all', 'billing')));
		$view->assign('trialExpiration', defined('LICENSE_EXPIRATION_TEXT') ? LICENSE_EXPIRATION_TEXT : '');
		$view->assign('isTrial', defined('TRIAL_MODE') && TRIAL_MODE);
		$view->assign('devMode', defined('DEVMODE') ? DEVMODE : false);

		$currency = getCurrentAdminCurrency();
		$currency['symbol_decimal'] = $settings->get('LocalizationCurrencyDecimalSymbol');
		$currency['symbol_separating'] = $settings->get('LocalizationCurrencySeparatingSymbol');

		$view->assign('currency', json_encode($currency));

		return $view;
	}

	/**
	 * @return string
	 */
	protected function getBasePath()
	{
		return 'admin.php';
	}

	/**
	 * Get a registered service
	 *
	 * @param $key
	 *
	 * @return null|object
	 */
	protected function get($key)
	{
		global $registry;

		return $registry->get($key);
	}

	/**
	 * @param $validationErrors
	 * @return string
	 */
	protected function formatValidationErrors($validationErrors)
	{
		$list = '';

		foreach ($validationErrors as $errors)
		{
			foreach ($errors as $msg) $list .= '<li>'.$msg.'</li>';
		}

		if ($list != '') $list = '<ul>'.$list.'</ul>';

		return $list;
	}

	/**
	 * Set current admin id
	 *
	 * @param int $currentAdminId
	 */
	public function setCurrentAdminId($currentAdminId)
	{
		$this->currentAdminId = $currentAdminId;
	}

	/**
	 * Get current admin id
	 *
	 * @return int
	 */
	public function getCurrentAdminId()
	{
		return intval($this->currentAdminId);
	}
}
<?php

/**
 * Class Admin_Reports_Report_OrdersCountPerPeriod
 */
class Admin_Reports_Report_OrdersCountPerPeriod extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'base_summary';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'base_summary';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('orders_this_period', 'Orders this period', Admin_Reports_Field::TYPE_NUMERIC),
				new Admin_Reports_Field('orders_prev_period', 'Orders previous period', Admin_Reports_Field::TYPE_NUMERIC),
				new Admin_Reports_Field('revenue_this_period', 'Revenue this period', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('revenue_prev_period', 'Revenue previous period', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('conversions_this_period', 'Conversion this period', Admin_Reports_Field::TYPE_NUMERIC),
				new Admin_Reports_Field('conversions_prev_period', 'Conversion previous period', Admin_Reports_Field::TYPE_NUMERIC)
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				COUNT(*) AS orders_this_period,
				SUM(o.total_amount - o.gift_cert_amount) AS revenue_this_period
			FROM ' . DB_PREFIX . 'orders o
			WHERE o.removed = "No"
				AND o.status = "Completed"
				AND o.payment_status = "Received"
				AND DATE(o.placed_date)	BETWEEN "' . $dateFrom . '" AND "' . $dateTo . '"
		';
	}

	/**
	 * @param Admin_Reports_Result $result
	 * @param $type
	 * @param $title
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param $params
	 */
	protected function prepareData(Admin_Reports_Result $result, $type, $title, DateTime $dateFrom, DateTime $dateTo, $params)
	{
		$fields = $this->getFields();

		$db = $this->db;

		$rangeType = isset($params['range_type']) ? $params['range_type'] : 'custom';

		$result->count = 1;

		$data = $db->selectOne($this->getQuery());

		$result->dataRaw[0] = $data;
		$result->dataDisplay[0] = array();

		/** @var $field Admin_Reports_Field */
		foreach ($fields as $field)
		{
			$fieldId = $field->getId();
			$result->dataRaw[0][$fieldId] = $data[$fieldId];
			$result->dataDisplay[0][$fieldId] = $this->getDisplayValue($field->getType(), $data[$fieldId]);
		}

		$this->calculateSummary($result, $result->dataRaw[0]);

		/** @var Admin_Reports_Field $field */
		foreach ($fields as $field)
		{
			$fieldId = $field->getId();
			$result->summaryDisplay[$fieldId] =
				in_array($field->getType(), array(Admin_Reports_Field::TYPE_CURRENCY, Admin_Reports_Field::TYPE_NUMERIC)) ?
					$this->getDisplayValue($field->getType(), $result->summaryRaw[$fieldId]) : '';
		}
	}
}
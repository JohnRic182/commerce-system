<?php

/**
 * Class Admin_Reports_Report_Taxes
 */
class Admin_Reports_Report_Taxes extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'orders';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'order';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('order_id', 'reporting.table_labels.order_id', Admin_Reports_Field::TYPE_NUMERIC),
				new Admin_Reports_Field('order_tax_amount', 'reporting.table_labels.tax', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('order_total_amount', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				o.order_num AS order_id,
				o.tax_amount AS order_tax_amount,
				o.total_amount - o.gift_cert_amount AS order_total_amount
			FROM ' . DB_PREFIX . 'orders o
			WHERE
				o.removed = "No" AND
				o.status_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.status_date <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				o.status = "Completed" AND
				o.payment_status = "Received"
			ORDER BY
				o.order_num
		';
	}
}
<?php

/**
 * Class Admin_Reports_Report_SalesByPaymentTypes
 */
class Admin_Reports_Report_SalesByPaymentTypes extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'payment_types';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'payment_type';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('payment_method_name', 'reporting.table_labels.payment_type', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('orders_count', 'reporting.table_labels.orders', Admin_Reports_Field::TYPE_NUMERIC, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_subtotal_amount', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_discount_amount', 'reporting.table_labels.discount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_tax_amount', 'reporting.table_labels.tax', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_shipping_amount', 'reporting.table_labels.shipping', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_gift_cert_amount', 'reporting.table_labels.gift_certificate', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_total_amount', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				IF(pm.name = "custom_payment", pm.title, pm.name) AS payment_method_name,
				COUNT(o.oid) AS orders_count,
				SUM(o.subtotal_amount) AS orders_subtotal_amount,
				SUM(o.tax_amount) AS orders_tax_amount,
				SUM(o.discount_amount + o.promo_discount_amount) AS orders_discount_amount,
				SUM(o.shipping_amount + IF(o.handling_separated="1", o.handling_amount, 0)) AS orders_shipping_amount,
				SUM(o.gift_cert_amount) AS orders_gift_cert_amount,
				SUM(o.total_amount - o.gift_cert_amount) AS orders_total_amount
			FROM ' . DB_PREFIX . 'orders o
			LEFT JOIN ' . DB_PREFIX . 'payment_methods pm ON o.payment_method_id = pm.pid
			WHERE
				o.removed = "No" AND
				o.placed_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.placed_date <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				o.status = "Completed" AND
				o.payment_status = "Received"
			GROUP BY o.payment_method_id
			ORDER BY orders_total_amount DESC
		';
	}
}
<?php

/**
 * Class Admin_Reports_Report_PromoCampaigns
 */
class Admin_Reports_Report_PromoCampaigns extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'promo_campaigns';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'promo_campaign';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('campaign_name', 'reporting.table_labels.campaign_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('campaign_date_start', 'reporting.table_labels.date_start', Admin_Reports_Field::TYPE_DATETIME),
				new Admin_Reports_Field('campaign_date_stop', 'reporting.table_labels.date_end', Admin_Reports_Field::TYPE_DATETIME),

				new Admin_Reports_Field('orders_count', 'reporting.table_labels.orders', Admin_Reports_Field::TYPE_NUMERIC, 'text-right'),

				new Admin_Reports_Field('orders_subtotal_amount', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_discount_amount', 'reporting.table_labels.discount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_shipping_amount', 'reporting.table_labels.shipping', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_tax_amount', 'reporting.table_labels.tax', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_gift_cert_amount', 'reporting.table_labels.gift_certificate', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_total_amount', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				pc.name AS campaign_name,
				pc.date_start AS campaign_date_start,
				pc.date_stop AS campaign_date_stop,
				SUM(o.subtotal_amount) AS orders_subtotal_amount,
				SUM(o.discount_amount + o.promo_discount_amount) AS orders_discount_amount,
				SUM(o.shipping_amount + IF(o.handling_separated="1", o.handling_amount, 0)) AS orders_shipping_amount,
				SUM(o.tax_amount) AS orders_tax_amount,
				SUM(o.gift_cert_amount) AS orders_gift_cert_amount,
				SUM(o.total_amount - o.gift_cert_amount) AS orders_total_amount,
				COUNT(o.oid) AS orders_count
			FROM ' . DB_PREFIX . 'orders o
			INNER JOIN ' . DB_PREFIX . 'promo_codes pc ON pc.pid = o.promo_campaign_id
			WHERE
				o.removed = "No" AND
				o.placed_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.placed_date <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				o.status = "Completed" AND
				o.payment_status = "Received"
			GROUP BY o.promo_campaign_id
			ORDER BY orders_total_amount DESC
		';
	}
}
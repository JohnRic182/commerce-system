<?php

/**
 * Class Admin_Reports_Report_Shift4
 */
class Admin_Reports_Report_Shift4 extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'orders';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'order';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('customer_id', 'reporting.table_labels.id', Admin_Reports_Field::TYPE_ID),
				new Admin_Reports_Field('customer_name', 'reporting.table_labels.name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('orders_count', 'reporting.table_labels.orders', Admin_Reports_Field::TYPE_NUMERIC, 'col-sm-1'),
				new Admin_Reports_Field('orders_subtotal', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_discount', 'reporting.table_labels.discount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_tax', 'reporting.table_labels.tax', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_shipping', 'reporting.table_labels.shipping', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_gift_certificate', 'reporting.table_labels.gift_certificate', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_total', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				t.extra,
				t.completed AS completed,
				t.payment_response,
				t.order_total_amount as processed_amount,
				o.total_amount as order_total_amount,
				CONCAT(CONCAT(u.fname, " "), u.lname) as customer_name,
				o.order_num
			FROM ' . DB_PREFIX . 'orders o
			INNER JOIN ' . DB_PREFIX . 'payment_transactions t ON o.oid = t.oid
			LEFT OUTER JOIN ' . DB_PREFIX . 'users u ON o.uid = u.uid
			WHERE
				o.payment_gateway_id = "shift4i4go" AND
				t.completed >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				t.completed <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				t.extra <> "" AND
				payment_response <> "timeout"
		';
	}
}
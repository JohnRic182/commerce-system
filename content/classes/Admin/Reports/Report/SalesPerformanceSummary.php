<?php

/**
 * Class Admin_Reports_Report_SalesPerformanceSummary
 */
class Admin_Reports_Report_SalesPerformanceSummary extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'days';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'day';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('report_date', 'reporting.table_labels.create_date', Admin_Reports_Field::TYPE_DATE),
				new Admin_Reports_Field('orders_count', 'reporting.table_labels.orders', Admin_Reports_Field::TYPE_NUMERIC, 'col-sm-1'),
				new Admin_Reports_Field('items_count', 'reporting.table_labels.items_sold', Admin_Reports_Field::TYPE_NUMERIC, 'col-sm-1'),
				new Admin_Reports_Field('orders_subtotal_amount', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_discount_amount', 'reporting.table_labels.discount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_tax_amount', 'reporting.table_labels.tax', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_shipping_amount', 'reporting.table_labels.shipping', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_gift_cert_amount', 'reporting.table_labels.gift_certificate', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_total_amount', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		$orderStatus = isset($params['filter']['order_status']) ? $params['filter']['order_status'] : null;

		$where = $orderStatus == 'any' ? 'status NOT IN ("New", "Abandon")' : 'status="Completed" AND	payment_status="Received"';

		// date should be CAST
		return '
			SELECT
				DATE_FORMAT(o.placed_date, "%Y-%m-%d") AS report_date,
				COUNT(o.oid) AS orders_count,
				SUM((SELECT SUM(oc.quantity) FROM ' . DB_PREFIX . 'orders_content oc WHERE oc.oid=o.oid)) AS items_count,
				SUM(o.subtotal_amount) AS orders_subtotal_amount,
				SUM(o.discount_amount + o.promo_discount_amount) AS orders_discount_amount,
				SUM(o.shipping_amount + IF(o.handling_separated = "1", o.handling_amount, 0)) AS orders_shipping_amount,
				SUM(o.tax_amount) AS orders_tax_amount,
				SUM(o.gift_cert_amount) AS orders_gift_cert_amount,
				SUM(o.total_amount - gift_cert_amount) AS orders_total_amount
			FROM ' . DB_PREFIX . 'orders o
			WHERE
				o.removed="No" AND
				DATE(o.placed_date) >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				DATE(o.placed_date) <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				' . $where . '
			GROUP BY report_date
			ORDER BY o.placed_date
		';
	}
}
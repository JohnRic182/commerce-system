<?php

/**
 * Class Admin_Reports_Report_SalesPerformance
 */
class Admin_Reports_Report_SalesPerformance extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'orders';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'order';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('order_id', 'reporting.table_labels.id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('customer_name', 'reporting.table_labels.customer', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('order_date', 'reporting.table_labels.order_date', Admin_Reports_Field::TYPE_DATETIME),
				new Admin_Reports_Field('order_subtotal_amount', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('order_discount_amount', 'reporting.table_labels.discount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('order_tax_amount', 'reporting.table_labels.tax', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('order_shipping_amount', 'reporting.table_labels.shipping', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('order_gift_cert_amount', 'reporting.table_labels.gift_certificate', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('order_total_amount', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				o.order_num AS order_id,
				CONCAT(u.fname, " ",  u.lname) AS customer_name,
				o.placed_date AS order_date,
		  		o.subtotal_amount AS order_subtotal_amount,
		  		o.discount_amount + o.promo_discount_amount AS order_discount_amount,
		  		o.tax_amount AS order_tax_amount,
		  		o.shipping_amount + IF(o.handling_separated = "1", o.handling_amount, 0) AS order_shipping_amount,
		  		o.gift_cert_amount AS order_gift_cert_amount,
				o.total_amount - o.gift_cert_amount AS order_total_amount
			FROM ' . DB_PREFIX . 'orders o
			INNER JOIN ' . DB_PREFIX . 'users u ON u.uid = o.uid AND u.removed="No"
			WHERE
				o.removed = "No" AND
				o.placed_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.placed_date <= "' . $this->mySqlDateFormat($dateTo)  . '" AND
				o.status = "Completed" AND
				o.payment_status = "Received"
			ORDER BY
				o.order_num
		';
	}
}
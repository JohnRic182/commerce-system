<?php

/**
 * Class Admin_Reports_Report_RecurringOrders
 */
class Admin_Reports_Report_RecurringOrders extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'recurring_orders';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'recurring_order';
	}

	/**
	 * Get fields
	 *
	 * @param $params
	 * @return array|mixed
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('order_id', 'reporting.table_labels.order_id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('order_date', 'reporting.table_labels.order_date', Admin_Reports_Field::TYPE_DATE),
				new Admin_Reports_Field('profile_id', 'reporting.table_labels.profile_id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('profile_status', 'reporting.table_labels.profile_status', Admin_Reports_Field::TYPE_TEXT, 'col-sm-1'),
				new Admin_Reports_Field('date_next_billing', 'reporting.table_labels.next_billing_date', Admin_Reports_Field::TYPE_DATE),

				new Admin_Reports_Field('customer_name', 'reporting.table_labels.customer_name', Admin_Reports_Field::TYPE_TEXT),

				new Admin_Reports_Field('product_name', 'reporting.table_labels.product_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('items_quantity', 'reporting.table_labels.quantity', Admin_Reports_Field::TYPE_NUMERIC, 'text-right col-sm-1'),
				new Admin_Reports_Field('subtotal_amount', 'reporting.table_labels.subtotal_amount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('tax_amount', 'reporting.table_labels.tax_amount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('shipping_amount', 'reporting.table_labels.shipping_amount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('total_amount', 'reporting.table_labels.total_amount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				o.order_num AS order_id,
				o.placed_date AS order_date,
				o.subtotal_amount AS subtotal_amount,
				o.tax_amount AS tax_amount,
				o.shipping_amount AS shipping_amount,
				o.total_amount AS total_amount,
				rp.recurring_profile_id AS profile_id,
				CONCAT(u.fname, " ", u.lname) AS customer_name,
				oc.title AS product_name,
				rp.items_quantity AS items_quantity,
				rp.date_next_billing AS date_next_billing,
				rp.status AS profile_status
			FROM ' . DB_PREFIX . 'orders AS o
			INNER JOIN ' . DB_PREFIX . 'users AS u ON u.uid = o.uid AND u.removed = "No"
			INNER JOIN ' . DB_PREFIX . 'recurring_profiles_orders AS rpo ON rpo.order_id = o.oid
			INNER JOIN ' . DB_PREFIX . 'recurring_profiles AS rp ON rp.recurring_profile_id = rpo.recurring_profile_id
			INNER JOIN ' . DB_PREFIX . 'orders_content AS oc ON rp.initial_order_line_item_id = oc.ocid
			WHERE
				o.removed = "No" AND
				o.order_type = "Recurring" AND
				o.status_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.status_date <= "' . $this->mySqlDateFormat($dateTo). '" AND
				o.status = "Completed" AND
				o.payment_status = "Received"
			ORDER BY
				o.order_num
		';
	}
}
<?php

/**
 * Class Admin_Reports_Report_Shift4Failures
 */
class Admin_Reports_Report_Shift4Failures extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'failures';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'failure';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('customer_id', 'reporting.table_labels.id', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('customer_name', 'reporting.table_labels.name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('orders_count', 'reporting.table_labels.orders', Admin_Reports_Field::TYPE_NUMERIC),
				new Admin_Reports_Field('orders_subtotal', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right'),
				new Admin_Reports_Field('orders_discount', 'reporting.table_labels.discount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right'),
				new Admin_Reports_Field('orders_tax', 'reporting.table_labels.tax', Admin_Reports_Field::TYPE_CURRENCY, 'text-right'),
				new Admin_Reports_Field('orders_shipping', 'reporting.table_labels.shipping', Admin_Reports_Field::TYPE_CURRENCY, 'text-right'),
				new Admin_Reports_Field('orders_gift_certificate', 'reporting.table_labels.gift_certificate', Admin_Reports_Field::TYPE_CURRENCY, 'text-right'),
				new Admin_Reports_Field('orders_total', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				u.uid AS customer_id,
				CONCAT(u.fname, " ", u.lname) AS customer_name,
				COUNT(oid) AS orders_count,
				SUM(subtotal_amount) AS orders_subtotal,
				SUM(discount_amount + promo_discount_amount) AS orders_discount,
				SUM(tax_amount) AS orders_tax,
				SUM(shipping_amount + IF(handling_separated = "1", handling_amount, 0)) AS orders_shipping,
				SUM(gift_cert_amount) AS orders_gift_certificate,
				SUM(total_amount - gift_cert_amount) AS orders_total
			FROM ' . DB_PREFIX . 'orders o
			INNER JOIN ' . DB_PREFIX . 'users u ON u.uid = o.uid AND u.removed = "No"
			WHERE
				o.removed = "No" AND
				o.placed_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.placed_date <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				o.status = "Completed" AND
				o.payment_status = "Received"
			GROUP BY o.uid
			ORDER BY orders_total DESC
		';
	}
}
<?php
/**
 * Class Admin_Reports_Report_ProductPerformance
 */
class Admin_Reports_Report_ProductPerformance extends Admin_Reports_Report
{
	public $triggerPage = 'products';
	public $filterFields = array(
		'product_attributes' => 'product_attributes',
		'product_url' => 'product_url'
	);

	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'products';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'product';
	}

	protected function getProductAttributeValue($product_options)
	{
		$product_attributes = array_map('trim', explode("\n", $product_options));
		return implode("|", $product_attributes);
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('product_db_id', 'reporting.table_labels.id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('product_id', 'reporting.table_labels.product_id', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_name', 'reporting.table_labels.product_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_url', 'reporting.table_labels.product_url', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_attributes', 'reporting.table_labels.product_attributes', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_sub_id', 'reporting.table_labels.product_sub_id', Admin_Reports_Field::TYPE_ID),
				new Admin_Reports_Field('product_price', 'reporting.table_labels.product_price', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('product_sale_price', 'reporting.table_labels.product_sale_price', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('product_items_sold', 'reporting.table_labels.items_sold', Admin_Reports_Field::TYPE_NUMERIC, 'text-right col-sm-1'),
				new Admin_Reports_Field('product_subtotal_amount', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				oc.pid AS product_db_id,
				p.product_id AS product_id,
				oc.title AS product_name,
				p.url_default AS product_url,
				p.url_custom AS custom_url,
				IF(oc.options IS NULL OR oc.options = "", "n/a", oc.options) AS product_attributes,
				oc.product_sub_id,
				p.price AS product_price,
				p.price2 AS product_sale_price,								
				SUM(oc.admin_quantity) AS product_items_sold,
				SUM(oc.admin_price * oc.admin_quantity) AS product_subtotal_amount
			FROM ' . DB_PREFIX . 'orders_content oc
			INNER JOIN ' . DB_PREFIX . 'orders o ON o.oid = oc.oid
			LEFT JOIN ' . DB_PREFIX . 'products p ON p.pid = oc.pid
			WHERE
				o.removed = "No" AND
				o.placed_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.placed_date <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				o.status = "Completed" AND
				o.payment_status = "Received"
			GROUP BY oc.pid, oc.options
			ORDER BY product_items_sold desc, oc.title DESC
		';
	}
}
<?php

/**
 * Class Admin_Reports_Report_RecurringProfiles
 */
class Admin_Reports_Report_RecurringProfiles extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'recurring_profiles';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'recurring_profile';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('recurring_profile_id', 'reporting.table_labels.profile_id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('subscriber_name', 'reporting.table_labels.customer_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_name', 'reporting.table_labels.product_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('date_next_billing', 'reporting.table_labels.next_billing_date', Admin_Reports_Field::TYPE_DATE),

				new Admin_Reports_Field('status', 'reporting.table_labels.status', Admin_Reports_Field::TYPE_TEXT, 'col-sm-1'),
				new Admin_Reports_Field('trial_enabled', 'reporting.table_labels.has_trial', Admin_Reports_Field::TYPE_TEXT, 'col-sm-1'),
				new Admin_Reports_Field('trial_subtotal_amount', 'reporting.table_labels.subtotal_trial', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
				new Admin_Reports_Field('billing_subtotal_amount', 'reporting.table_labels.subtotal', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				rp.recurring_profile_id,
				CONCAT(u.fname, " ", u.lname) AS subscriber_name,
				oc.title AS product_name,
				rp.date_next_billing AS date_next_billing,
				rp.status AS status,
				IF (rp.trial_enabled = 1, "Yes", "No") AS trial_enabled,
				IF (rp.trial_enabled = 1, rp.trial_amount * rp.items_quantity, NULL) AS trial_subtotal_amount,
				rp.billing_amount * rp.items_quantity AS billing_subtotal_amount
			FROM ' . DB_PREFIX . 'recurring_profiles AS rp
			INNER JOIN ' . DB_PREFIX . 'users AS u ON u.uid = rp.user_id
			INNER JOIN ' . DB_PREFIX . 'orders AS o ON o.oid = rp.initial_order_id
			INNER JOIN ' . DB_PREFIX . 'orders_content AS oc ON oc.ocid = rp.initial_order_line_item_id
			WHERE
				rp.date_created >= "' . $this->mySqlDateFormat($dateFrom). '" AND
				rp.date_created <= "' . $this->mySqlDateFormat($dateTo). '"
			ORDER BY recurring_profile_id
		';
	}
}
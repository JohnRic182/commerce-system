<?php

/**
 * Class Admin_Reports_Report_QRCampaigns
 */
class Admin_Reports_Report_QRCampaigns extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'qr_campaigns';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'qr_campaign';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('campaign_id', 'reporting.table_labels.id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('campaign_name', 'reporting.table_labels.campaign_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('orders_count', 'reporting.table_labels.orders', Admin_Reports_Field::TYPE_NUMERIC, 'text-right col-sm-1'),
				new Admin_Reports_Field('orders_total_amount', 'reporting.table_labels.total', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1')
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				o.offsite_campaign_id AS campaign_id,
				oc.name AS campaign_name,
				count(*) AS orders_count,
				SUM(o.total_amount - o.gift_cert_amount) AS orders_total_amount
			FROM ' . DB_PREFIX . 'orders AS o
			INNER JOIN ' . DB_PREFIX . 'offsite_campaigns oc ON o.offsite_campaign_id = oc.campaign_id
			WHERE
				o.placed_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.placed_date <= "' . $this->mySqlDateFormat($dateTo) . '" AND
				o.status = "Completed" AND
				o.payment_status = "Received" AND
				o.removed = "No" AND
				oc.type = "qr"
			GROUP BY o.offsite_campaign_id
		';
	}
}
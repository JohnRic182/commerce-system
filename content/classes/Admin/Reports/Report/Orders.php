<?php
/**
 * Class Admin_Reports_Report_Orders
 */
class Admin_Reports_Report_Orders extends Admin_Reports_Report
{
	public $triggerPage = 'orders';
	public $filterFields = array(
		'custom_fields' =>  'custom_fields',
		'product_attributes' => 'product_attributes',
		'shipping_cm_name' => 'shipping_cm_name'
	);

	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'orders';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'order';
	}

	protected function getCustomFieldValue($orderId = 0, $userId = 0, $db)
	{
		// prepare custom fields
		$customFieldsValues = $db->selectAll('
			SELECT cf.field_id, cfv.field_value
			FROM ' . DB_PREFIX . 'custom_fields cf
			LEFT JOIN ' . DB_PREFIX . 'custom_fields_values cfv ON
				cf.field_id = cfv.field_id
				AND (
					cfv.order_id = ' . intval($orderId) . ' OR
					(cfv.profile_id = ' . intval($userId) . ' AND cfv.order_id = 0)
				)
	    ');

		$customOrderFields = array();
		if ($customFieldsValues)
		{
			foreach ($customFieldsValues as $customFieldValue)
			{
				$customOrderFields['cf_' . $customFieldValue['field_id']] = $customFieldValue['field_value'];
			}
		}
		return implode("|", $customOrderFields);
	}

	protected function getProductAttributeValue($product_options)
	{
		$product_attributes = explode("\n", $product_options);
		return implode("|", $product_attributes);
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('order_date', 'reporting.table_labels.order_date', Admin_Reports_Field::TYPE_DATE),
				new Admin_Reports_Field('order_num' , 'reporting.table_labels.order_id', Admin_Reports_Field::TYPE_ID),
				new Admin_Reports_Field('status', 'reporting.table_labels.status', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('payment_status', 'reporting.table_labels.payment_status', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_name', 'reporting.table_labels.customer_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_company', 'reporting.table_labels.company_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_address1', 'reporting.table_labels.billing_address1', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_address2', 'reporting.table_labels.billing_address2', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_city', 'reporting.table_labels.billing_city', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_state_name', 'reporting.table_labels.billing_state_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_zip', 'reporting.table_labels.billing_zip', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_country_name', 'reporting.table_labels.billing_country_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_phone', 'reporting.table_labels.billing_phone',  Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('billing_email', 'reporting.table_labels.email_address', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_address_type', 'reporting.table_labels.shipping_address_type', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_name', 'reporting.table_labels.shipping_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_company', 'reporting.table_labels.shipping_company', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_address1', 'reporting.table_labels.shipping_address1', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_address2', 'reporting.table_labels.shipping_address2', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_city', 'reporting.table_labels.shipping_city', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_state_name', 'reporting.table_labels.shipping_state_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_zip', 'reporting.table_labels.shipping_zip', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_country_name', 'reporting.table_labels.shipping_country_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('shipping_cm_name', 'reporting.table_labels.shipping_cm_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('custom_fields','reporting.table_labels.custom_fields', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('subtotal_amount', 'reporting.table_labels.subtotal_amount', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('promo_discount_amount', 'reporting.table_labels.promo_discount_amount', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('tax_amount', 'reporting.table_labels.tax_amount', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('shipping_amount', 'reporting.table_labels.shipping_amount', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('handling_amount', 'reporting.table_labels.handling_amount', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('total_amount', 'reporting.table_labels.total_amount', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('product_id', 'reporting.table_labels.product_id', Admin_Reports_Field::TYPE_ID),
				new Admin_Reports_Field('product_sub_id', 'reporting.table_labels.product_sub_id', Admin_Reports_Field::TYPE_ID),
				new Admin_Reports_Field('product_title', 'reporting.table_labels.product_title', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_quantity', 'reporting.table_labels.product_quantity', Admin_Reports_Field::TYPE_NUMERIC),
				new Admin_Reports_Field('product_price', 'reporting.table_labels.product_price', Admin_Reports_Field::TYPE_CURRENCY),
				new Admin_Reports_Field('product_weight', 'reporting.table_labels.product_weight', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_attributes', 'reporting.table_labels.product_attributes', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('order_type', 'reporting.table_labels.order_type', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_type', 'reporting.table_labels.product_type', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('tracking_number', 'reporting.table_labels.tracking_number', Admin_Reports_Field::TYPE_TEXT),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT o.*,
				DATE_FORMAT(o.placed_date, "%M %e, %Y") AS order_date,
				CONCAT(u.fname, " ", u.lname) AS billing_name,
				u.address1 AS billing_address1,
				u.address2 AS billing_address2,
				u.company AS billing_company,
				u.city AS billing_city,
				IF(u.country = 1 OR u.country = 2, s1.name, u.province) AS billing_state_name,
				u.zip AS billing_zip,
				c1.name AS billing_country_name,
				u.phone AS billing_phone,
				u.email AS billing_email,
				IF(u.country = 1 OR u.country = 2, s2.name, u.province) AS shipping_state_name,
				IF(o.shipping_country = 1 OR o.shipping_country = 2, s2.name, o.shipping_province) AS shipping_state_name,
				c2.name AS shipping_country_name,
				oc.product_id,
				oc.product_sub_id,
				oc.title as product_title,
				oc.quantity as product_quantity,
				oc.price as product_price,
				oc.weight as product_weight,
				oc.options as product_options,
				oc.product_type,
				oc.recurring_billing_data,
				oc.free_shipping,
				of.tracking_number,
				os.*
			FROM orders_content oc
				INNER JOIN orders o ON oc.oid = o.oid
				INNER JOIN users u ON o.uid = u.uid AND u.removed = "No"
				INNER JOIN orders_shipments os ON o.oid = os.oid
				LEFT JOIN orders_fulfillment of ON o.oid = of.oid
				LEFT JOIN countries c1 ON u.country = c1.coid
				LEFT JOIN countries c2 ON o.shipping_country = c2.coid
				LEFT JOIN states AS s1 ON u.state = s1.stid
				LEFT JOIN states AS s2 ON o.shipping_state = s2.stid
			 WHERE o.removed = "No"  AND			 	
			 	o.placed_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
				o.placed_date <= "' . $this->mySqlDateFormat($dateTo) . '" AND 
				o.status = "Completed" AND 
				o.payment_status = "Received" 
			ORDER BY o.order_num, oc.pid
		';
	}
}
<?php

/**
 * Class Admin_Reports_Report_RecurringFailures
 */
class Admin_Reports_Report_RecurringFailures extends Admin_Reports_Report
{
	/**
	 * @return string
	 */
	protected function getItemGroupName()
	{
		return 'recurring_failures';
	}

	/**
	 * @return string
	 */
	protected function getItemName()
	{
		return 'recurring_failure';
	}

	/**
	 * @param null $params
	 * @return array
	 */
	public function getFields($params = null)
	{
		if (empty($this->fields))
		{
			$this->fields = array(
				new Admin_Reports_Field('order_id', 'reporting.table_labels.order_id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('order_date', 'reporting.table_labels.order_date', Admin_Reports_Field::TYPE_DATE),
				new Admin_Reports_Field('profile_id', 'reporting.table_labels.profile_id', Admin_Reports_Field::TYPE_ID, 'col-sm-1'),
				new Admin_Reports_Field('profile_status', 'reporting.table_labels.profile_status', Admin_Reports_Field::TYPE_TEXT, 'col-sm-1'),

				new Admin_Reports_Field('transaction_attempts', 'reporting.table_labels.billing_attempts', Admin_Reports_Field::TYPE_NUMERIC, 'text-right col-sm-1'),
				new Admin_Reports_Field('last_transaction_date', 'reporting.table_labels.last_billing_date', Admin_Reports_Field::TYPE_DATE),
				new Admin_Reports_Field('date_next_billing', 'reporting.table_labels.next_billing_date', Admin_Reports_Field::TYPE_DATE),

				new Admin_Reports_Field('customer_name', 'reporting.table_labels.customer_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('customer_email', 'reporting.table_labels.customer_email', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('product_name', 'reporting.table_labels.product_name', Admin_Reports_Field::TYPE_TEXT),
				new Admin_Reports_Field('items_quantity', 'reporting.table_labels.quantity', Admin_Reports_Field::TYPE_NUMERIC, 'text-right col-sm-1'),
				new Admin_Reports_Field('total_amount', 'reporting.table_labels.total_amount', Admin_Reports_Field::TYPE_CURRENCY, 'text-right col-sm-1'),
			);
		}

		return $this->fields;
	}

	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return string
	 */
	public function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null)
	{
		return '
			SELECT
				o.order_num AS order_id,
				o.status_date AS order_date,
				o.total_amount AS total_amount,
				rp.recurring_profile_id AS profile_id,
				CONCAT(u.fname, " ", u.lname) AS customer_name,
				u.email AS customer_email,
				oc.title AS product_name,
				rp.items_quantity AS items_quantity,
				rp.date_next_billing AS date_next_billing,
				rp.status AS profile_status,
				trans.last_transaction_date AS last_transaction_date,
				trans.attempts AS transaction_attempts
			FROM ' . DB_PREFIX . 'orders AS o
			INNER JOIN ' . DB_PREFIX . 'users AS u ON u.uid = o.uid AND u.removed="No"
			INNER JOIN ' . DB_PREFIX . 'recurring_profiles_orders AS rpo ON rpo.order_id = o.oid
			INNER JOIN ' . DB_PREFIX . 'recurring_profiles AS rp ON rp.recurring_profile_id = rpo.recurring_profile_id
			INNER JOIN ' . DB_PREFIX . 'orders_content AS oc ON rp.initial_order_line_item_id = oc.ocid
			INNER JOIN (
				SELECT o2.oid, MAX(t.completed) AS last_transaction_date, COUNT(*) AS attempts
				FROM '.DB_PREFIX.'orders o2
					INNER JOIN ' . DB_PREFIX . 'payment_transactions AS t ON o2.oid = t.oid AND t.is_success = 0
					LEFT JOIN ' . DB_PREFIX . 'payment_transactions AS ts ON o2.oid = ts.oid AND ts.is_success = 1
				WHERE ts.oid IS NULL AND
					o2.removed = "No" AND
					o2.order_type = "Recurring" AND
					o2.status_date >= "' . $this->mySqlDateFormat($dateFrom) . '" AND
					o2.status_date <= "' . $this->mySqlDateFormat($dateTo)  . '" AND
					o2.payment_status <> "Received"
				GROUP BY o2.oid
			) trans ON o.oid = trans.oid
			ORDER BY o.order_num
		';
	}
}
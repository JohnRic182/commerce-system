<?php

/**
 * Class Admin_Reports_Result
 */
class Admin_Reports_Result
{
	public $type;
	public $title;
	public $params = array();

	/** @var DateTime */
	public $dateFrom;

	/** @var DateTime */
	public $dateTo;

	public $fields = array();

	public $count = 0;
	public $dataRaw = array();
	public $dataDisplay = array();
	public $summaryRaw = array();
	public $summaryDisplay = array();

	public $itemGroupName = '';
	public $itemName = '';

	/**
	 * Result constructor.
	 * @param $type
	 * @param $title
	 * @param $itemGroupName
	 * @param $itemName
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param $params
	 */
	public function __construct($type, $title, $itemGroupName, $itemName, DateTime $dateFrom, DateTime $dateTo, $params)
	{
		$this->type = $type;
		$this->title = $title;
		$this->itemGroupName = $itemGroupName;
		$this->itemName = $itemName;

		$this->dateFrom = $dateFrom;
		$this->dateTo = $dateTo;

		$this->params = $params;
	}
}
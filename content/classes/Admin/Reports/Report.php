<?php

/**
 * Class Admin_Reports_Report
 */
abstract class Admin_Reports_Report
{
	/** @var DB $db */
	protected $db;

	/** @var DataAccess_SettingsRepository $settings */
	protected $settings;

	/** @var array $fields */
	protected $fields = array();

	/** @var array $summary */
	protected $summary = array();

	/** @var mixed $dbResult */
	protected $dbResult = null;

	protected $dataFormat = '';

	protected $dateTimeFormat = '';

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->dateFormat = mysqlDateFormatToPhp('%M %e, %Y');
		$this->dateTimeFormat = mysqlDateFormatToPhp($settings->get('LocalizationDateTimeFormat'));
	}

	/**
	 * @return mixed
	 */
	protected abstract function getItemGroupName();

	/**
	 * @return mixed
	 */
	protected abstract function getItemName();

	/**
	 * Get field names
	 *
	 * @param $params
	 * @return mixed
	 */
	protected abstract function getFields($params = null);


	/**
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param null $params
	 * @return mixed
	 */
	protected abstract function getQuery(DateTime $dateFrom, DateTime $dateTo, $params = null);

	/**
	 * @param $type
	 * @param $title
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param $params
	 * @return Admin_Reports_Result
	 */
	public function prepare($type, $title, DateTime $dateFrom, DateTime $dateTo, $params)
	{
		$result = new Admin_Reports_Result($type, $title, $this->getItemGroupName(), $this->getItemName(), $dateFrom, $dateTo, $params);

		$this->prepareFields($result, $type, $title, $dateFrom, $dateTo, $params);

		$this->prepareData($result, $type, $title, $dateFrom, $dateTo, $params);

		return $result;
	}

	/**
	 * @param Admin_Reports_Result $result
	 * @param $type
	 * @param $title
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param $params
	 */
	protected function prepareFields(Admin_Reports_Result $result, $type, $title, DateTime $dateFrom, DateTime $dateTo, $params)
	{
		$fields = $this->getFields();

		/** @var Admin_Reports_Field $field */
		foreach ($fields as $field)
		{
			$fieldId = $field->getId();

			$result->fields[$fieldId] = array(
				'class' => $field->getCssClass(),
				'title' => $field->getName()
			);

			$result->summaryRaw[$fieldId] =
			$result->summaryDisplay[$fieldId] =
				in_array($field->getType(), array(Admin_Reports_Field::TYPE_CURRENCY, Admin_Reports_Field::TYPE_NUMERIC)) ? 0 : '';
		}
	}

	/**
	 * @param Admin_Reports_Result $result
	 * @param $type
	 * @param $title
	 * @param DateTime $dateFrom
	 * @param DateTime $dateTo
	 * @param $params
	 */
	protected function prepareData(Admin_Reports_Result $result, $type, $title, DateTime $dateFrom, DateTime $dateTo, $params)
	{
		$fields = $this->getFields();

		$db = $this->db;

		$this->dbResult = $db->query($this->getQuery($dateFrom, $dateTo, $params));

		$result->count = $db->numRows($this->dbResult);

		if ($this->dbResult)
		{
			while ($data = $this->fetch())
			{
				$c = count($result->dataRaw);

				$result->dataRaw[$c] = array();
				$result->dataDisplay[$c] = array();

				/** @var $field Admin_Reports_Field */
				foreach ($fields as $field)
				{
					$fieldId = $field->getId();
					$result->dataRaw[$c][$fieldId] = $data[$fieldId];
					$result->dataDisplay[$c][$fieldId] = $this->getDisplayValue($field->getType(), $data[$fieldId]);
				}

				$this->calculateSummary($result, $result->dataRaw[$c]);
			}

			/** @var Admin_Reports_Field $field */
			foreach ($fields as $field)
			{
				$fieldId = $field->getId();
				$result->summaryDisplay[$fieldId] =
					in_array($field->getType(), array(Admin_Reports_Field::TYPE_CURRENCY, Admin_Reports_Field::TYPE_NUMERIC)) ?
						$this->getDisplayValue($field->getType(), $result->summaryRaw[$fieldId]) : '';
			}
		}
	}

	/**
	 * Fetch results
	 *
	 * @return array
	 */
	protected function fetch()
	{
		$data = $this->db->moveNext($this->dbResult);

		return $data;
	}

	/**
	 * Calculate summary
	 *
	 * @param Admin_Reports_Result $result
	 * @param $data
	 */
	protected function calculateSummary(Admin_Reports_Result $result, $data)
	{
		/** @var Admin_Reports_Field $field */
		foreach ($this->getFields() as $field)
		{
			if (in_array($field->getType(), array(Admin_Reports_Field::TYPE_CURRENCY, Admin_Reports_Field::TYPE_NUMERIC)))
			{
				$fieldId = $field->getId();
				$result->summaryRaw[$fieldId] += $data[$fieldId];
			}
		}
	}

	/**
	 * @param $fieldType
	 * @param $value
	 * @return string
	 */
	protected function getDisplayValue($fieldType, $value)
	{
		switch ($fieldType)
		{
			case Admin_Reports_Field::TYPE_ID:
			case Admin_Reports_Field::TYPE_NUMERIC:
			case Admin_Reports_Field::TYPE_TEXT: return htmlspecialchars($value); break;
			case Admin_Reports_Field::TYPE_CURRENCY: return getAdminPrice($value); break;
			case Admin_Reports_Field::TYPE_DATE: return date($this->dateFormat, strtotime($value)); break;
			case Admin_Reports_Field::TYPE_DATETIME: return date($this->dateTimeFormat, strtotime($value)); break;
		}

		return $value;
	}

	/**
	 * @param DateTime $date
	 * @return string
	 */
	protected function mySqlDateFormat(DateTime $date)
	{
		return $date->format('Y-m-d H:i:s');
	}
}
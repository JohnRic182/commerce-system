<?php

/**
 * Class Admin_Reports_Field
 */
class Admin_Reports_Field
{
	const TYPE_ID = 'id';
	const TYPE_TEXT = 'text';
	const TYPE_NUMERIC = 'numeric';
	const TYPE_CURRENCY = 'currency';
	const TYPE_DATE = 'date';
	const TYPE_DATETIME = 'datetime';

	/** @var string $id */
	protected $id;

	/** @var string $name */
	protected $name;

	/** @var string $class */
	protected $cssClass;

	/** @var string $type */
	protected $type;

	/**
	 * Class constructor
	 *
	 * @param $id
	 * @param $name
	 * @param string $type
	 * @param string $cssClass
	 */
	public function __construct($id, $name, $type = self::TYPE_TEXT, $cssClass = '')
	{
		$this->setId($id);
		$this->setName($name);
		$this->setType($type);
		$this->setCssClass($cssClass);
	}

	/**
	 * Set CSS class name
	 *
	 * @param string $cssClass
	 */
	public function setCssClass($cssClass)
	{
		$this->cssClass = $cssClass;
	}

	/**
	 * Get CSS class name
	 *
	 * @return string
	 */
	public function getCssClass()
	{
		return $this->cssClass;
	}

	/**
	 * Set id
	 *
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Get id
	 *
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set field type
	 *
	 * @param $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * Get field type
	 *
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}
}
<?php

class Admin_HostedHelper
{
	public static function isHosted()
	{
		global $settings, $iono;

		return $iono->hosted_by_ddm && trim($settings['AccountToken']) != '';
	}
}
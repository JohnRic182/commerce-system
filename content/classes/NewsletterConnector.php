<?php
/**
 * @class NewsletterConnector
 */
class NewsletterConnector
{
	/**
	 * Subscribe to newsletter
	 * @param string $email - valid email address
	 * @return boolean - true if email added to emails table, false otherwise
	 */
	static function subscribe($email, $suppressDb = false)
	{
		global $settings, $db, $user;

		$ret = false;
		if (!$suppressDb)
		{
			$db->query("SELECT * FROM ".DB_PREFIX."emails WHERE email LIKE BINARY '".$db->escape($email)."'");
			if (!$db->moveNext())
			{
				$db->query("INSERT INTO ".DB_PREFIX."emails (added, email, receives_marketing) VALUES(NOW(), '".$db->escape($email)."', 'Yes')");
				$ret = true;
				if ($user->auth_ok)
				{
					$db->reset();
					$db->assignStr('receives_marketing', 'Yes');
					$db->update(DB_PREFIX.'users', "WHERE uid='$user->id'");
				}
			}
		}

		if (defined('NEWSLETTER_SUB_TEST') && NEWSLETTER_SUB_TEST)
		{
			error_log('Newsletter Subscribe - email: '.$email);
		}
		else
		{
			//check if mailchimp is enabled on this cart,
			//if so, then subscribe the user to mailchimp newsletter
			$chimpenabled = $settings["mailchimp_enabled"];
			$chimpkey = $settings["mailchimp_apikey"];
			$chimplist = $settings["mailchimp_newsletter_list"];
		
			if ($chimpenabled && $chimpkey && $chimplist)
			{
				require_once dirname(dirname(__FILE__))."/vendors/mailchimp/src/Mailchimp.php";

				$mc = new Mailchimp($chimpkey);
				$mc->lists->subscribe($chimplist, array('email' => $email), array(), 'html', true, true);
			}
		}

		return $ret;
	}
	
	/**
	 * Unsubscribe to newsletter
	 * @param string $cancel_email - valid email address
	 * @return boolean - true if email added to emails table, false otherwise
	 */
	static function unsubscribe($cancel_email)
	{
		global $settings, $db;

		$status_changed = false;
		$db->query("SELECT * FROM ".DB_PREFIX."emails WHERE email LIKE BINARY '".$db->escape($cancel_email)."'");

		if ($db->moveNext())
		{
			$db->query("DELETE FROM ".DB_PREFIX."emails WHERE eid='".intval($db->col["eid"])."'");
			$status_changed = true;
		}

		$db->query("SELECT uid, email FROM ".DB_PREFIX."users WHERE email LIKE BINARY '".$db->escape($cancel_email)."' AND receives_marketing='Yes'");

		if ($db->moveNext())
		{
			$db->query("UPDATE ".DB_PREFIX."users SET receives_marketing='No', last_update = NOW() WHERE email LIKE BINARY '".$db->escape($cancel_email)."' AND receives_marketing='Yes'");
			$status_changed = true;
		}

		if (defined('NEWSLETTER_SUB_TEST') && NEWSLETTER_SUB_TEST)
		{
			error_log('Marketing unsubscribe - email: '.$cancel_email);
		}
		else
		{
			//check if mailchimp is enabled on this cart,
			//if so, then subscribe the user to mailchimp newsletter
			$chimpenabled = $settings["mailchimp_enabled"];
			$chimpkey = $settings["mailchimp_apikey"];
			$chimplist = $settings["mailchimp_newsletter_list"];

			if ($chimpenabled && $chimpkey && $chimplist)
			{

				require_once dirname(dirname(__FILE__))."/vendors/mailchimp/src/Mailchimp.php";

				$mc = new Mailchimp($chimpkey);
				$mc->lists->unsubscribe($chimplist, array('email' => $cancel_email));
			}
		}

		return $status_changed;
	}
}
<?php

/**
 * Shipping Repository
 */
interface DataAccess_ShippingRepositoryInterface
{
    public function getOrderItems($orderId);

    public function getOrderItemsWithShippingPrice($orderId, $productLevelMethodId);

    public function getCountryById($countryId);

    public function getOrderShippingAddress($orderId);

    public function getShippingOriginAddress($orderId, &$settings);

    public function updateOrderItemShippingPrice($orderItemId, $shippingPrice);

    public function getCustomRate($shippingMethodId, $shippingMethodType, $items, $itemsCount, $itemsPriceBasedPrice, $weight);
}
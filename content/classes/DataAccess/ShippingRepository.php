<?php

/**
 * Countries Repository
 */

class DataAccess_ShippingRepository implements DataAccess_ShippingRepositoryInterface
{
	/** @var DB $db */
	protected $db;

	public function __construct(DB $db = null)
	{
		if (is_null($db))
		{
			global $db;
		}

		$this->db = $db;
	}

	//TODO: Wrong place for these methods orders, orders_contents, countries, where should they go?
	/**
	 * @param int $orderId
	 * @return array
	 */
	public function getOrderItems($orderId)
	{
		return $this->db->selectAll("
			SELECT
				".DB_PREFIX."orders_content.*,
				'No' AS product_is_shipping_price,
				'0.00' AS product_shipping_price
			FROM ".DB_PREFIX."orders_content
			WHERE ".DB_PREFIX."orders_content.oid='".intval($orderId)."'
		");
	}

	/**
	 * Get order items with shipping price
	 *
	 * @param int $orderId
	 * @param int $productLevelMethodId
	 *
	 * @return array
	 */
	public function getOrderItemsWithShippingPrice($orderId, $productLevelMethodId)
	{
		return $this->db->selectAll("
			SELECT
				".DB_PREFIX."orders_content.*,
				if (".DB_PREFIX."products_shipping_price.is_price IS NULL, 'No', ".DB_PREFIX."products_shipping_price.is_price) AS product_is_shipping_price,
				if (".DB_PREFIX."products_shipping_price.price IS NULL, 0.00, ".DB_PREFIX."products_shipping_price.price) AS product_shipping_price
			FROM ".DB_PREFIX."orders_content
			LEFT JOIN ".DB_PREFIX."products_shipping_price ON 
			".DB_PREFIX."products_shipping_price.pid = ".DB_PREFIX."orders_content.pid AND 
			".DB_PREFIX."products_shipping_price.ssid='".intval($productLevelMethodId)."'
			WHERE ".DB_PREFIX."orders_content.oid='".intval($orderId)."'
		");
	}

	/**
	 * Get country by id
	 *
	 * @param int $countryId
	 *
	 * @return array|false
	 */
	public function getCountryById($countryId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'countries WHERE coid='.intval($countryId));
	}

	/**
	 * Get state by country / state id
	 *
	 * @param $countryId
	 * @param $stateId
	 *
	 * @return array|false
	 */
	public function getStateById($countryId, $stateId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'states WHERE coid='.intval($countryId).' AND stid='.intval($stateId));
	}

	/**
	 * Get shipping origin address
	 *
	 * @param $orderId
	 * @param $settings
	 *
	 * @return array
	 */
	public function getShippingOriginAddress($orderId, &$settings)
	{
		$originAddress = array(
			'address1' => $settings['ShippingOriginAddressLine1'],
			'address2' => $settings['ShippingOriginAddressLine2'],
			'city' => $settings['ShippingOriginCity'],
			'zip' => $settings['ShippingOriginCountry'] == '1' ? substr(trim($settings['ShippingOriginZip']), 0, 5) : trim($settings['ShippingOriginZip'])
		);

		$countryData = $this->getCountryById($settings['ShippingOriginCountry']);
		if ($countryData)
		{
			$originAddress['country_id'] = $countryData['coid'];
			$originAddress['country'] = $countryData['name'];
			$originAddress['country_iso_a2'] = $countryData['iso_a2'];
			$originAddress['country_iso_a3'] = $countryData['iso_a3'];
			$originAddress['country_iso_number'] = $countryData['iso_number'];
		}
		else
		{
			$originAddress['country_id'] = 1;
			$originAddress['country'] = 'United States';
			$originAddress['country_iso_a2'] = 'US';
			$originAddress['country_iso_a3'] = 'USA';
			$originAddress['country_iso_number'] = '840';
		}

		$stateData = $this->getStateById($originAddress['country_id'], $settings['ShippingOriginState']);
		if ($stateData)
		{
			$originAddress['state_id'] = $stateData['stid'];
			$originAddress['state_name'] = $stateData['name'];
			$originAddress['state_code'] = $stateData['short_name'];
		}
		else
		{
			$originAddress['state_id'] = 0;
			$originAddress['state_name'] = $originAddress['state_code'] = $settings['ShippingOriginProvince'];
		}

		return $originAddress;
	}

	/**
	 * Get order shipping address
	 *
	 * @param int $orderId
	 *
	 * @return array|false
	 */
	public function getOrderShippingAddress($orderId)
	{
		return $this->db->selectOne('
			SELECT
				o.shipping_address_type, o.shipping_name, o.shipping_address1, o.shipping_address2,
				o.shipping_city, o.shipping_province, o.shipping_country, o.shipping_zip,
				c.coid AS country_id, c.name AS country_name, c.iso_a2 AS country_iso_a2,
				c.iso_a3 AS country_iso_a3,	c.iso_number AS country_iso_number,
				s.stid AS state_id, s.name AS state_name,
				s.short_name AS state_abbr, s.short_name AS state_code
			FROM '.DB_PREFIX.'orders AS o
			LEFT JOIN '.DB_PREFIX.'countries AS c ON c.coid = o.shipping_country
			LEFT JOIN '.DB_PREFIX.'states AS s ON s.stid = o.shipping_state
			WHERE oid='.intval($orderId)
		);
	}

	/**
	 * Update order items shipping price
	 *
	 * @param int $orderItemId
	 * @param float $shippingPrice
	 */
	public function updateOrderItemShippingPrice($orderItemId, $shippingPrice)
	{
		$db = $this->db;
		$db->reset();
		$db->assignStr('is_shipping_price', 'Yes');
		$db->assignStr('shipping_price', $shippingPrice);
		$db->update(DB_PREFIX.'orders_content', 'WHERE ocid='.intval($orderItemId));
	}

    /**
     * Returns shipping method data
     * 
     * @param int $shippingMethodId
     * 
     * @return array|false
     */
    public function getShippingMethodData($shippingMethodId)
    {
        return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'shipping_selected WHERE ssid='.intval($shippingMethodId));
    }

	/**
	 * Get custom shipping rates
     * 
	 * @param int $shippingMethodId
	 * @param string $shippingMethodType
	 * @param array $items
	 * @param int $itemsCount
	 * @param float $itemsPriceBasedPrice
	 * @param float $weight
     * 
	 * @return array|false
	 */
	public function getCustomRate($shippingMethodId, $shippingMethodType, $items, $itemsCount, $itemsPriceBasedPrice, $weight)
    {
		if ($shippingMethodType == 'product')
		{
			$ids = array();
			foreach ($items as $item) $ids[] = intval($item['pid']);
			if ($ids){
				return $this->db->selectAll('
				SELECT *
				FROM ' . DB_PREFIX . 'products_shipping_price
				WHERE
					ssid = ' . intval($shippingMethodId) . '
					AND pid IN (' . implode(',', $ids) . ')
					AND is_price="Yes"
			');
			}
			else{
				return array();
			}

		}
		else
		{
			return $this->db->selectOne('
				SELECT *
				FROM ' . DB_PREFIX . 'shipping_custom_rates
				WHERE
					ssid = ' . intval($shippingMethodId) . '
					AND rate_min <= ' . ($shippingMethodType == 'flat' ? intval($itemsCount) : floatval($shippingMethodType == 'price' ? $itemsPriceBasedPrice : $weight)) . '
					AND (rate_max = 0 OR rate_max >= ' . ($shippingMethodType == 'flat' ? intval($itemsCount) : floatval($shippingMethodType == 'price' ? $itemsPriceBasedPrice : $weight)) . ')
				ORDER BY rate_min LIMIT 1
			');
		}
    }

	/**
	 * Make fake address for shipping quotes
	 *
	 * @param int $countryId
	 * @param int $stateId
	 * @param string $zip
	 *
	 * @return array
	 */
	public function makeShippingQuotesAddress($countryId, $stateId, $zip)
	{
		$address = $this->db->selectOne('
			SELECT
				"Residential" AS address_type,
				"John Doe" AS name,
				"Some Company, LLC" AS company,
				"123 Some Street" AS address1,
				"" AS address2,
				"Bestcity" AS city,
				c.coid AS country_id,
				c.name AS country,
				c.name AS country_name,
				c.iso_a2 AS country_iso_a2,
				c.iso_a3 AS country_iso_a3,
				c.iso_number AS country_iso_number,
				s.short_name AS state_abbr,
				s.short_name AS state_code,
				s.name AS state,
				s.name AS province,
				s.stid AS state_id
			FROM '.DB_PREFIX.'countries AS c
			LEFT JOIN '.DB_PREFIX.'states AS s ON c.coid=s.coid AND s.stid='.intval($stateId).'
			WHERE c.coid='.intval($countryId)
		);

		if (!$address)
		{
			$address = array('country_id' => $countryId, 'state_id' => $stateId);
		}

		$address['shipping_zip'] = $zip;
		$address['zip'] = $zip;

		return $address;
	}

	/**
	 * Select active shipping methods
	 *
	 * @return array
	 */
	public function getActiveShippingMethods($carrier_id = false)
	{
		return $this->db->selectAll('
			SELECT *
			FROM '.DB_PREFIX.'shipping_selected
			WHERE '.DB_PREFIX.'shipping_selected.hidden="No"'.($carrier_id !== false ? ' AND carrier_id = "'.$this->db->escape($carrier_id).'"' : '').'
			ORDER BY priority, carrier_name
		');
	}

	/**
	 * Select shipping methods
	 *
	 * @param bool $withRates
	 *
	 * @return array
	 */
	public function getShippingMethods($withRates = false)
	{
		$methods = $this->db->selectAll('
			SELECT *
			FROM '.DB_PREFIX.'shipping_selected
			ORDER BY priority, carrier_name
		');

		$methodsById = array();
		foreach ($methods as &$method)
		{
			if ($method['carrier_id'] == 'custom')
			{
				$method['rates'] = array();
				$methodsById[$method['ssid']] = &$method;
			}
		}

		$rates = $this->db->selectAll('
			SELECT r.*
			FROM '.DB_PREFIX.'shipping_custom_rates r
			INNER JOIN '.DB_PREFIX.'shipping_selected s ON r.ssid = s.ssid
			ORDER BY r.ssid, r.rate_min
		');

		foreach ($rates as $rate)
		{
			if (isset($methodsById[$rate['ssid']]))
			{
				$methodsById[$rate['ssid']]['rates'][] = $rate;
			}
		}

		foreach ($methods as &$method)
		{
			$method['ratesCount'] = isset($method['rates']) ? count($method['rates']) : 0;
		}

		return $methods;
	}

	/**
	 * @param $carrier_id
	 * @param int $destinationCountry
	 * @param int $originCountry
	 * @return mixed
	 */
	public function getRealTimeMethods($carrier_id, $destinationCountry = 0, $originCountry = 0)
	{
		$destinationCountry = intval($destinationCountry);

		$methods = $this->db->selectAll('
			SELECT *
			FROM '.DB_PREFIX.'shipping_default s
			WHERE s.carrier_id LIKE "'.$this->db->escape($carrier_id).'%"
				AND (
					country = "'.$destinationCountry.'" OR country LIKE "'.$destinationCountry.',%"
					OR country LIKE "%,'.$destinationCountry.',%" OR country LIKE "%,'.$destinationCountry.'"
				)
			ORDER BY method_name
		');

		return $methods;
	}

	/**
	 * @param $sdid
	 * @return array|bool|null
	 */
	public function getRealTimeMethod($sdid)
	{
		$sdid = intval($sdid);
		if ($sdid < 1) return null;

		return $this->db->selectOne('
			SELECT *
			FROM '.DB_PREFIX.'shipping_default s
			WHERE s.sdid = '.$sdid
		);
	}

	/**
	 * @param $ssid
	 * @return bool
	 */
	public function deleteMethod($ssid)
	{
		$ssid = intval($ssid);

		if ($ssid > 0)
		{
			$this->db->query('DELETE FROM '.DB_PREFIX.'shipping_custom_rates WHERE ssid = '.$ssid);
			$this->db->query("DELETE FROM ".DB_PREFIX."products_shipping_price WHERE ssid = ".$ssid);
			$this->db->query('DELETE FROM '.DB_PREFIX.'shipping_selected WHERE ssid = '.$ssid);
			return true;
		}

		return false;
	}

	/**
	 * @param $ssid
	 * @return array|bool
	 */
	public function getShippingMethod($ssid)
	{
		$ssid = intval($ssid);

		if ($ssid < 1) return null;

		$method = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'shipping_selected WHERE ssid = '.$ssid);

		if ($method)
		{
			$rates = $this->getRatesForCustomMethod($ssid);

			$method['rates'] = $rates;
			$method['ratesCount'] = count($rates);
		}

		return $method;
	}

	/**
	 * @param $ssid
	 * @return array|mixed
	 */
	protected function getRatesForCustomMethod($ssid)
	{
		return $this->db->selectAll('
			SELECT r.*
			FROM '.DB_PREFIX.'shipping_custom_rates r
			WHERE r.ssid = '.$ssid.'
			ORDER BY r.ssid, r.rate_min
		');
	}

	/**
	 * @param $data
	 *
	 * @return int
	 */
	public function persistShippingMethod(&$data, $updateRates = true)
	{
		$db = $this->db;

		$data['id'] = intval(isset($data['id']) ? $data['id'] : (isset($data['ssid']) ? $data['ssid'] : 0));

		$db->reset();
		if (isset($data['carrier_id'])) $db->assignStr('carrier_id', $data['carrier_id']);
		if (isset($data['carrier_name'])) $db->assignStr('carrier_name', $data['carrier_name']);
		if (isset($data['method_id'])) $db->assignStr('method_id', $data['method_id']);
		if (isset($data['method_calc_mode'])) $db->assignStr('method_calc_mode', $data['method_calc_mode']);
		if (isset($data['method_name'])) $db->assignStr('method_name', $data['method_name']);
		if (isset($data['priority'])) $db->assignStr('priority', $data['priority']);
		if (isset($data['country'])) $db->assignStr('country', $data['country']);
		if (isset($data['country_exclude'])) $db->assignStr('country_exclude', $data['country_exclude']);
		if (isset($data['state'])) $db->assignStr('state', $data['state']);
		if (isset($data['weight_min'])) $db->assignStr('weight_min', $data['weight_min']);
		if (isset($data['weight_max'])) $db->assignStr('weight_max', $data['weight_max']);
		if (isset($data['fee'])) $db->assignStr('fee', $data['fee']);
		if (isset($data['fee_type'])) $db->assignStr('fee_type', $data['fee_type']);
		if (isset($data['exclude'])) $db->assignStr('exclude', $data['exclude']);
		if (isset($data['hidden'])) $db->assignStr('hidden', $data['hidden']);
		if (isset($data['notes'])) $db->assignStr('notes', $data['notes']);

		if ($data['id'] > 0)
		{
			$db->update(DB_PREFIX.'shipping_selected',  'WHERE ssid='.intval($data['id']));
		}
		else
		{
			$res = $db->insert(DB_PREFIX.'shipping_selected');
			$data['id'] = intval($res);
		}

		if ($updateRates && isset($data['carrier_id']) && $data['carrier_id'] == 'custom')
		{
			$existingRates = $this->getRatesForCustomMethod($data['id']);

			$updatedRates = array();
			foreach ($data['rates'] as $rate)
			{
				$this->persistCustomRate($rate, $data['id']);
				if ($rate['srid'] > 0)
				{
					$updatedRates[$rate['srid']] = true;
				}
			}

			// Cleanup removed rows
			foreach ($existingRates as $rate)
			{
				if (!isset($updatedRates[$rate['srid']]))
				{
					$db->query('DELETE FROM '.DB_PREFIX.'shipping_custom_rates WHERE srid = '.intval($rate['srid']).' AND ssid='.intval($rate['ssid']));
				}
			}
		}
		return intval($data['id']);
	}

	/**
	 * @param $data
	 *
	 * @return int
	 */
	public function persistRealTimeMethod(&$data, $params = null)
	{
		$db = $this->db;

		$data['id'] = intval(isset($data['id']) ? $data['id'] : (isset($data['ssid']) ? $data['ssid'] : 0));

		$db->reset();
		if (isset($data['carrier_id'])) $db->assignStr('carrier_id', $data['carrier_id']);
		if (isset($data['carrier_name'])) $db->assignStr('carrier_name', $data['carrier_name']);
		if (isset($data['method_id'])) $db->assignStr('method_id', $data['method_id']);
		if (isset($data['method_calc_mode'])) $db->assignStr('method_calc_mode', $data['method_calc_mode']);
		if (isset($data['method_name'])) $db->assignStr('method_name', $data['method_name']);
		if (isset($data['priority'])) $db->assignStr('priority', $data['priority']);
		if (isset($data['country'])) $db->assignStr('country', $data['country']);
		if (isset($data['country_exclude'])) $db->assignStr('country_exclude', $data['country_exclude']);
		if (isset($data['state'])) $db->assignStr('state', $data['state']);
		if (isset($data['weight_min'])) $db->assignStr('weight_min', $data['weight_min']);
		if (isset($data['weight_max'])) $db->assignStr('weight_max', $data['weight_max']);
		if (isset($data['fee'])) $db->assignStr('fee', $data['fee']);
		if (isset($data['fee_type'])) $db->assignStr('fee_type', $data['fee_type']);
		if (isset($data['exclude'])) $db->assignStr('exclude', $data['exclude']);
		if (isset($data['hidden'])) $db->assignStr('hidden', $data['hidden']);
		if (isset($data['notes'])) $db->assignStr('notes', $data['notes']);

		if ($data['id'] > 0)
		{
			$db->update(DB_PREFIX.'shipping_selected',  'WHERE ssid='.$data['id']);
		}
		else
		{
			$res = $db->insert(DB_PREFIX.'shipping_selected');
			$data['id'] = intval($res);
		}

		return intval($data['id']);
	}

	/**
	 * @param $data
	 * @param $ssid
	 */
	protected function persistCustomRate(&$data, $ssid)
	{
		if (!isset($data['srid']))
		{
			$data['srid'] = 0;
		}

		$db = $this->db;
		$db->reset();

		if (isset($data['rate_min'])) $db->assignStr('rate_min', $data['rate_min']);
		if (isset($data['rate_max'])) $db->assignStr('rate_max', $data['rate_max']);
		if (isset($data['base'])) $db->assignStr('base', $data['base']);
		if (isset($data['price'])) $db->assignStr('price', $data['price']);
		if (isset($data['price_type'])) $db->assignStr('price_type', $data['price_type']);

		$ssid = intval($ssid);
		if (intval($data['srid']) > 0)
		{
			$db->update(DB_PREFIX.'shipping_custom_rates', 'WHERE srid = '.intval($data['srid']).' AND ssid='.$ssid);
		}
		else
		{
			$db->assign('ssid', $ssid);
			$data['srid'] = intval($db->insert(DB_PREFIX.'shipping_custom_rates'));
		}
	}
}
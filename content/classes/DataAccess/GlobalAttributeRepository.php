<?php

/**
 * Class DataAccess_GlobalAttributeRepository
 */
class DataAccess_GlobalAttributeRepository extends DataAccess_BaseRepository
{
	/** @var DataAccess_ProductsRepository */
	protected $productRepository = null;

	/** @var DataAccess_ProductAttributeRepository $productAttributeRepository */
	protected $productAttributeRepository = null;

	/**
	 * @param DB $db
	 * @param DataAccess_ProductsRepository $productRepository
	 * @param DataAccess_ProductAttributeRepository $productAttributeRepository
	 */
	public function __construct(DB $db, DataAccess_ProductsRepository $productRepository, DataAccess_ProductAttributeRepository $productAttributeRepository)
	{
		parent::__construct($db);

		$this->productRepository = $productRepository;
		$this->productAttributeRepository = $productAttributeRepository;
	}

	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'attribute_type' => 'select',
			'is_global' => 'Yes',
			'is_active' => 'No',
			'priority' => 5,
			'name' => '',
			'caption' => '',
			'text_length' => 20,
			'options' => '',
			'categories' => array(),
			'products' => array(),
			'products_list' => array(),
			'update_mode' => 'assign',
			'track_inventory' => 0,
			'update_mode' => 'just_save_attribute',
		);
	}

	/**
	 * @return array
	 */
	public function getAttributeTypesList()
	{
		return array(
			'select' => 'Drop-down',
			'radio' => 'Radio buttons',
			'text' => 'Text input',
			'textarea' => 'Textarea input'
		);
	}

	/**
	 * Get attributes count
	 *
	 * @return int
	 */
	public function getCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'global_attributes');
		return intval($result['c']);
	}

	/**
	 * Get attributes list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';

		return $this->db->selectAll(
			'SELECT * FROM '.DB_PREFIX.'global_attributes ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Get attribute by id
	 *
	 * @param $id
	 *
	 * @return array|bool
	 */
	public function getById($id)
	{
		$db = $this->db;

		$attribute = $db->selectOne('SELECT * FROM '.DB_PREFIX.'global_attributes WHERE gaid='.intval($id));

		if ($attribute)
		{
			if ($attribute['is_global'] == 'Yes')
			{
				$categories = array(0);
			}
			else
			{
				$categories = array();
				$db->query('SELECT * FROM '.DB_PREFIX.'catalog_attributes WHERE gaid='.intval($id));

				while ($db->moveNext())
				{
					$categories[] = $db->col['cid'];
				}
			}

			$attribute['categories'] = $categories;

			if ($attribute['is_global'] == 'Yes')
			{
				$products = array(0);
				$products_list = array(0);
			}
			else
			{
				$products = array();
				$products_list = array();
				$db->query('SELECT
								gap.*, p.product_id, p.title
							FROM '.DB_PREFIX.'global_attributes_products gap
								INNER JOIN '.DB_PREFIX.'products p ON p.pid = gap.pid AND is_visible = "Yes"
							WHERE
								gap.gaid='.intval($id));

				while ($db->moveNext())
				{
					$products[] = $db->col['pid'];
					$products_list[] = array('pid' => $db->col['pid'], 'title' => $db->col['product_id'] . '-' . $db->col['title']);
				}
			}

			$attribute['products_list'] = $products_list;
			$attribute['products'] = $products;
		}

		return $attribute;
	}

	/**
	 * @param $formData
	 * @param $mode
	 * @param null $id
	 * @return array|bool
	 */
	public function getValidationErrors($formData, $mode, $id = null)
	{
		$errors = array();
		if (is_array($formData))
		{
			if (trim($formData['name']) == '')
			{
				$errors['name'] = array('Please enter attribute name');
			}
			else
			{
				$db = $this->db;
				if ($db->selectOne('SELECT * FROM '.DB_PREFIX."global_attributes WHERE name='".$db->escape($formData['name'] )."'".(!is_null($id) ? ' AND gaid <> '.intval($id) : '')))
				{
					$errors['name'] = array('Attribute name must be unique within attributes');
				}
			}

			if (trim($formData['caption']) == '') $errors['caption'] = array('Please enter attribute caption');
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		$update_mode = isset($data['update_mode']) ? $data['update_mode'] : 'just_save_attribute';

		$removed = false;

		switch ($update_mode)
		{
			case 'just_save_attribute':
			case 'bulk_update':
			case 're_assign_attribute':
			{
				$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['gaid']) ? $data['gaid'] : null);

				$data['categories'] = is_array($data['categories']) ? $data['categories'] : array();
				$data['products'] = is_array($data['products']) ? $data['products'] : array();

				if (isset($data['assign_type']) && $data['assign_type'] == 'categories')
				{
					$data['products'] = array();
				}
				if (isset($data['assign_type']) && $data['assign_type'] == 'products')
				{
					$data['categories'] = array();
				}

				$isGlobalAttribute = in_array(0, $data['categories']);
				$attributeType = $data['attribute_type'];
				$isActive = isset($data['is_active']) && $data['is_active'] == 'Yes';

				$db->reset();
				$db->assignStr('is_global', $isGlobalAttribute ? 'Yes' : 'No');
				$db->assignStr('is_active', $isActive ? 'Yes' : 'No');
				$db->assignStr('priority', $data['priority']);
				$db->assignStr('name', $data['name']);
				$db->assignStr('caption', $data['caption']);
				$db->assignStr('track_inventory', $data['track_inventory']);

				$db->assignStr('attribute_type', $attributeType);
				$db->assignStr('text_length', $attributeType == 'text' || $attributeType == 'textarea' ? intval($data['text_length']) : 0);
				$db->assignStr('options', $attributeType == 'select' || $attributeType == 'radio' ? preg_replace_callback('/(?:\d+)(\.[0-9]+)|(\.[0-9]+)/', array($this, 'formatNumberModifier'), trim($data['options'])) : '');

				if (!is_null($data['id']) && $data['id'] && $data['id'] != '')
				{
					$db->update(DB_PREFIX.'global_attributes', 'WHERE gaid='.intval($data['id']));
					$newAttribute = false;
				}
				else
				{
					$data['id'] = $db->insert(DB_PREFIX.'global_attributes');
					$newAttribute = true;
				}

				// update category attributes
				/**
				 * Save categories
				 */
				$categories = array();
				$gaid = intval($data['id']);

				if (!$newAttribute)
				{
					$db->query('DELETE FROM '.DB_PREFIX.'global_attributes_products WHERE gaid='.$gaid);
					$db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes WHERE gaid='.$gaid);
				}

				if (!$isGlobalAttribute)
				{
					foreach ($data['categories'] as $cid)
					{
						$categories[] = $cid = intval($cid);
						$db->query('INSERT INTO '.DB_PREFIX.'catalog_attributes VALUES('.$cid.', '.$gaid.')');
					}
				}

				/**
				 * Save products
				 */
				$products = array();

				if (!$newAttribute && count($data['products']) > 0)
				{
					$db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes WHERE gaid='.$gaid);
					$db->query('DELETE FROM '.DB_PREFIX.'global_attributes_products WHERE gaid='. $gaid);
				}

				if (count($data['products']) > 0)
				{
					foreach ($data['products'] as $pid)
					{
						$products[] = $pid = intval($pid);
						$db->query('INSERT INTO '.DB_PREFIX.'global_attributes_products VALUES('.$gaid.', '.$pid.')');
					}
				}

				switch ($update_mode)
				{
					case 'bulk_update':
					{
						$attribute = array_merge($this->productAttributeRepository->getDefaults(), $data);
						$attribute['gaid'] = $gaid;
						$attribute['id'] = null;
						$attribute['is_active'] = $isActive ? 'Yes' : 'No';
						$attribute['track_inventory'] = $data['track_inventory'];

						unset($attribute['categories'], $attribute['nonce'], $attribute['mode'], $attribute['update_mode']);

						/**
						 * Assign attributes with products
						 */
						if ($isGlobalAttribute || count($categories) > 0)
						{
							if (!$newAttribute)
							{
								$db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE gaid='.$gaid);
							}

							$productsResult = $db->query(
									'SELECT pid FROM '.DB_PREFIX.'products'.
									($isGlobalAttribute ? '' : ' WHERE cid IN ('.implode(',', $categories).')')
							);

							if ($db->numRows($productsResult) > 0)
							{
								while (($product = $db->moveNext($productsResult)) != false)
								{
									$attribute['pid'] = $product['pid'];
									$this->productAttributeRepository->persist($attribute);
								}
							}
						}

						/**
						 * Assign attributes with products
						 */

						if (count($products) > 0)
						{
							if (!$newAttribute)
							{
								$db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE gaid='.$gaid);
							}

							$productsResult = $db->query('SELECT pid FROM '.DB_PREFIX.'products WHERE pid IN ('.implode(',', $products).')');

							if ($db->numRows($productsResult) > 0)
							{
								while (($product = $db->moveNext($productsResult)) != false)
								{
									$attribute['pid'] = $product['pid'];
									$this->productAttributeRepository->persist($attribute);
								}
							}
						}

						$this->productRepository->updateProductsAttributesCount();

						break;
					}
					case 're_assign_attribute':
					{
						$attribute = array_merge($this->productAttributeRepository->getDefaults(), $data);
						$attribute['gaid'] = $gaid;
						$attribute['id'] = null;
						$attribute['is_active'] = $isActive ? 'Yes' : 'No';
						$attribute['track_inventory'] = $data['track_inventory'];

						unset($attribute['categories'], $attribute['nonce'], $attribute['mode'], $attribute['update_mode']);

						/**
						 * Assign attributes with products
						 */
						if ($isGlobalAttribute || count($categories) > 0)
						{

							$productsToReAssign = $db->selectAll(
									'SELECT pid FROM '.DB_PREFIX.'products'.
									($isGlobalAttribute ? '' : ' WHERE cid IN ('.implode(',', $categories).')')
							);

							if ($db->numRows($productsResult) > 0)
							{
								foreach ($productsToReAssign as $product)
								{
									$db->query('SELECT count(*) AS c FROM '.DB_PREFIX.'products_attributes WHERE pid = '.$product['pid'].' AND gaid ='.$gaid);
									$db->moveNext();
									if ($db->col['c'] == 0)
									{
										$attribute['pid'] = $product['pid'];
										$this->productAttributeRepository->persist($attribute);
									}
								}
							}
						}

						/**
						 * Assign attributes with products
						 */
						if (count($products) > 0)
						{
							$productsToReAssign = $db->selectAll('SELECT pid FROM '.DB_PREFIX.'products WHERE pid IN ('.implode(',', $products).')');

							if ($db->numRows($productsResult) > 0)
							{
								foreach ($productsToReAssign as $product)
								{
									$db->query('SELECT count(*) AS c FROM '.DB_PREFIX.'products_attributes WHERE pid = '.$product['pid'].' AND gaid ='.$gaid);
									$db->moveNext();
									if ($db->col['c'] == 0)
									{
										$attribute['pid'] = $product['pid'];
										$this->productAttributeRepository->persist($attribute);
									}
								}
							}
						}

						$this->productRepository->updateProductsAttributesCount();

						break;
					}
				}
				break;

			}
			case 'remove_from_list':
			{
				$this->removeFromList(array($data['id']));
				$removed = true;
				break;
			}
			case 'delete_attribute':
			{
				$this->delete(array($data['id']));
				$removed = true;
				break;
			}
		}

		if ($removed)
		{
			return 'removed';
		}
		else
		{
			return $data['id'];
		}
	}

	
	/**
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function importPersist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['gaid']) ? $data['gaid'] : null);

		$data['products'] = is_array($data['products']) ? $data['products'] : array();
		$isActive = isset($data['is_active']) && $data['is_active'] == 'Yes';

		//assign global attribute to products
		$products = array();
		$gaid = $data['gaid'];

		if (count($data['products']) > 0)
		{
			foreach ($data['products'] as $pid)
			{
				$pid = intval($pid);
				$db->query('SELECT count(*) AS c FROM '.DB_PREFIX.'products_attributes WHERE pid = '.$pid.' AND gaid ='.$gaid);
				$db->moveNext();

				if ($db->col['c'] == 0)
				{	
					$products[] = $pid;
					$pid = intval($pid);
					$db->query('SELECT count(*) AS c FROM '.DB_PREFIX.'global_attributes_products WHERE pid = '.$pid.' AND gaid ='.$gaid);
					if ($db->col['c'] == 0)
					{	
						$db->query('INSERT INTO '.DB_PREFIX.'global_attributes_products VALUES('.$gaid.', '.$pid.')');
					}
				}
			}
		}

		$updated = false;

		/**
		 * Assign attributes with products
		 */
		if (count($products) > 0)
		{
			$productsResult = $db->query('SELECT pid FROM '.DB_PREFIX.'products WHERE pid IN ('.implode(',', $products).')');

			if ($db->numRows($productsResult) > 0)
			{
				$updated = true;

				$attribute = array_merge($this->productAttributeRepository->getDefaults(), $data);
				$attribute['gaid'] = $gaid;
				$attribute['id'] = null;
				$attribute['is_active'] = $isActive ? 'Yes' : 'No';
				$attribute['track_inventory'] = $data['track_inventory'];

				unset($attribute['products'], $attribute['nonce'], $attribute['mode'], $attribute['update_mode']);

				while (($product = $db->moveNext($productsResult)) != false)
				{
					$attribute['pid'] = $product['pid'];
					$this->productAttributeRepository->persist($attribute);
				}
			}

			/**
			 * move catalog attribute to product attributes
			 */
			$db->query('SELECT * FROM '.DB_PREFIX.'catalog_attributes WHERE gaid=' . $gaid);
			$categories = array();
			while($db->moveNext())
			{
				$categories[] = $db->col['cid'];
			}
			if (count($categories) > 0)
			{
				$db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes WHERE gaid='.$gaid);

				$productsToReAssign = $db->selectAll('SELECT pid FROM '.DB_PREFIX.'products WHERE cid IN ('.implode(',', $categories).')');
				foreach ($productsToReAssign as $product)
				{
					$db->query('SELECT count(*) AS c FROM '.DB_PREFIX.'products_attributes WHERE pid = '.$product['pid'].' AND gaid ='.$gaid);
					$db->moveNext();
					if ($db->col['c'] == 0)
					{
						$attribute['pid'] = $product['pid'];
						$this->productAttributeRepository->persist($attribute);
					}
					$db->query('SELECT count(*) AS c FROM '.DB_PREFIX.'global_attributes_products WHERE pid = '.$product['pid'].' AND gaid ='.$gaid);
					$db->moveNext();
					if ($db->col['c'] == 0)
					{
						$db->query('INSERT INTO '.DB_PREFIX.'global_attributes_products VALUES('.$gaid.', '.$product['pid'].')');
					}
				}
			}
		}

		$this->productRepository->updateProductsAttributesCount();

		return $updated;
	}
	
	/**
	 * @param $data
	 */
	public function delete($data)
	{
		if (is_array($data))
		{
			$ids = $data;

			foreach ($ids as $key=>$value) $ids[$key] = intval($value);

			if (count($ids) < 1) return;

			$this->db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE gaid IN ('.implode(',', $ids).')');
			$this->db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes WHERE gaid IN ('.implode(',', $ids).')');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes_products WHERE gaid IN ('.implode(',', $ids).')');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes WHERE gaid IN ('.implode(',', $ids).')');
		}
		else if ($data == 'all')
		{
			$this->db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE gaid>0');
			$this->db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes_products');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes');
		}

		$this->productRepository->updateProductsAttributesCount();
	}

	public function removeFromList($data)
	{
		if (is_array($data))
		{
			$ids = $data;

			foreach ($ids as $key=>$value) $ids[$key] = intval($value);

			if (count($ids) < 1) return;
			$this->db->query('UPDATE '.DB_PREFIX.'products_attributes SET gaid = 0 WHERE gaid IN ('.implode(',', $ids).')');
			$this->db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes WHERE gaid IN ('.implode(',', $ids).')');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes_products WHERE gaid IN ('.implode(',', $ids).')');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes WHERE gaid IN ('.implode(',', $ids).')');
		}
		else if ($data == 'all')
		{
			$this->db->query('UPDATE '.DB_PREFIX.'products_attributes SET gaid = 0 WHERE gaid>0');
			$this->db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes_products');
			$this->db->query('DELETE FROM '.DB_PREFIX.'global_attributes');
		}

		$this->productRepository->updateProductsAttributesCount();
	}

	/**
	 * * get Attribute Name
	 * @param $name
	 * @return array
	 */
	public function getAttributeByName($name)
	{
		$db = $this->db;

		$attribute = $db->selectOne('SELECT * FROM '.DB_PREFIX.'global_attributes WHERE name="'.$db->escape(trim($name)).'"');

		if (isset($attribute['gaid']))
		{
			return $this->getById($attribute['gaid']);
		}
		else
		{
			return false;
		}
	}

	/**
	 * @param $product_id
	 * @return integer
	 */
	public function getPidByProductId($product_id)
	{
		$productData = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products WHERE product_id="'.$this->db->escape(trim($product_id)) .'"');

		return isset($productData['pid']) ? $productData['pid'] : false;
	}

	/**
	 * Formats the number of our price modifier
	 * @param $matches
	 * @return string
	 */
	public function formatNumberModifier($matches)
	{
		return number_format($matches[0], 2);
	}
}
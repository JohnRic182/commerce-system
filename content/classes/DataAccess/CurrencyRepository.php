<?php
/**
 * Class DataAccess_CurrencyRepository
 */
class DataAccess_CurrencyRepository extends DataAccess_BaseRepository
{
	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'currency_id' => '',
			'is_default' => 'No',
			'title' => '',
			'code' => '',
			'symbol_left' => '',
			'symbol_right' => '',
			'decimal_places' => '2',
			'exchange_rate' => '1',
		);		
	}

	/**
	 * Get currency by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne(
			'SELECT * FROM '.DB_PREFIX.'currencies WHERE currency_id='.intval($id)
		);
	}
	
	/**
	 * Get currencies codes
	 * @return array
	 */
	public function getExistsCurrenciesCodes()
	{
		return $this->db->selectAll(
			'SELECT code FROM '.DB_PREFIX.'currencies '
		);
	}

	

	/**
	 * Get currencies count
	 *
	 * @return mixed
	 */
	public function getCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'currencies');

		return intval($result['c']);
	}

	/**
	 * Get currencies list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null)
	{
		
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'title';
				
		return $this->db->selectAll(
				'SELECT * FROM '.DB_PREFIX.'currencies  ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{			

			if (trim($data['title']) == '') $errors['title'] = array('Please enter title');

			if (trim($data['exchange_rate']) == '')
			{
				$errors['exchange_rate'] = array('Please enter exchange rate');
			}
			else {
				if ( floatval($data['exchange_rate']) <= 0)
				{
					$errors['exchange_rate'] = array('Exchange rate must be > 0');
				}
			}	

			if (trim($data['code']) == '') 
			{
				$errors['code'] = array('Please enter code');
			}
			else
			{
				$db = $this->db;
				if ($db->selectOne('SELECT * FROM '.DB_PREFIX.'currencies WHERE code="'.$db->escape($data['code'] ).'"'.(!is_null($id) ? ' AND currency_id <> '.intval($id) : '')))
				{
					$errors['code'] = array('Currency code must be unique within currencies');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist currency
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['currency_id']) ? $data['currency_id'] : null);

		$db->reset();
		$db->assignStr('title', $data['title']);
		$db->assignStr('code', $data['code']);
		$db->assignStr('symbol_left', $data['symbol_left']);
		$db->assignStr('symbol_right', $data['symbol_right']);
		$db->assign('decimal_places', $data['decimal_places']);
		$db->assignStr('exchange_rate', floatval($data['exchange_rate']));

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != ''){
			$db->update(DB_PREFIX.'currencies',  'WHERE currency_id='.intval($data['id']));
			return intval($data['id']);
		}
		else{
			return $db->insert(DB_PREFIX.'currencies');
		}
	}

	/**
	 * Delete manufacturers by ids
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		// delete currencies
		$this->db->query('DELETE FROM '.DB_PREFIX.'currencies WHERE currency_id IN('.implode(',', $ids).') AND is_default !="Yes"');
	}
	
	/**
	 * Set selected currency as default
	 * @param $id
	 */
		
	public function setDefaultById($id) {
		
		$this->db->reset();
		$this->db->assignStr("is_default", "Yes");
		$this->db->update(DB_PREFIX."currencies", "WHERE currency_id ='" . $id . "'");
		
		$this->db->reset();
		$this->db->assignStr("is_default", "No");
		$this->db->update(DB_PREFIX."currencies", "WHERE currency_id !='" . $id . "'");
	}
	
	
	/**
	 * Delete currencies by search results
	 *
	 * @param $searchParams
	 */
	public function deleteBySearchParams()
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'currencies WHERE is_default != "Yes"');
	}
}
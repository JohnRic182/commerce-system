<?php

/**
 * Class DataAccess_Repository
 */
class DataAccess_Repository
{
	/**
	 * @var DB
	 */
	protected static $db = null;
	
	private static $_instances = array();
	private static $_data_cache = array();
	
	/**
	 *
	 * @param DB $instance 
	 */
	public static function setDatabaseInstance(DB $instance)
	{
		self::$db = $instance;
	}
	
	/**
	 *
	 * @param string $class_name
	 * @return type 
	 */
	public static function get($class_name)
	{
		$class_name = 'DataAccess_'.$class_name;
		
		if (!isset(self::$_instances[$class_name]))
		{
			self::$_instances[$class_name] = new $class_name;
		}
		
		return self::$_instances[$class_name];
	}

	/**
	 * @param $query
	 * @param bool $cache
	 * @return array
	 */
	protected function getData($query, $cache = false)
	{
		if ($cache)
		{
			$cache_hash = md5(str_replace(array(' ',"\n", "\t"),'', $query));
			
			if (isset(self::$_data_cache[$cache_hash]))
			{
				return self::$_data_cache[$cache_hash];
			}
		}
		
		$result = self::$db->query($query);
		
		$data = array();
		
		while (self::$db->moveNext($result))
		{
			$data[] = self::$db->col;
		}
		
		if ($cache)
		{
			self::$_data_cache[$cache_hash] = $data;
		}
		
		return $data;
	}

	/**
	 * @param $query
	 * @param bool $cache
	 * @return null
	 */
	protected function getOne($query, $cache = false)
	{
		if ($cache)
		{
			$cache_hash = md5(str_replace(array(' ',"\n", "\t"),'', $query));
			
			if (isset(self::$_data_cache[$cache_hash]))
			{
				return self::$_data_cache[$cache_hash];
			}
		}
		
		$result = self::$db->query($query);
		
		$one = null;
		
		if (self::$db->moveNext($result))
		{
			$one = self::$db->col;
		}
		
		if ($cache)
		{
			self::$_data_cache[$cache_hash] = $one;
		}
		
		return $one;
	}

	/**
	 * Check access
	 * @param type $key
	 * @return type 
	 */
	protected function checkAccess($key)
	{
		return AccessManager::checkAccess($key); // done
	}
}



<?php

/**
 * Countries Repository
 */
class DataAccess_CountryRepository extends DataAccess_Repository
{
	private $statesRepository = null;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 */
	public function __construct(DB $db = null)
	{
		if ($db)
		{
			self::setDatabaseInstance($db);
			$this->statesRepository  = new DataAccess_StateRepository($db);
		}
	}

	/**
	 * @param $data
	 * @param null $params
	 * @return int
	 */
	public function persist($data, $params = null)
	{
		$db = self::$db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['coid']) ? $data['coid'] : null);

		$db->reset();
		$db->assignStr('name', trim($data["name"]));
		$db->assignStr('available', isset($data['available']) && $data['available'] != '' ? 'Yes' : 'No');

		if (isset($data['country_priority'])) $db->assignStr('country_priority', $data['country_priority']);

		$db->assignStr('iso_a2', $data['iso_a2']);
		$db->assignStr('iso_a3', $data['iso_a3']);
		$db->assignStr('iso_number', $data['iso_number']);
		$db->assignStr('zip_required', $data['zip_required']);
		$db->assignStr('has_provinces', $data['has_provinces']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'countries', 'WHERE coid='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			return $db->insert(DB_PREFIX.'countries');
		}
	}

	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'coid' => '',
			'available' => '',
			'iso_a2' => '',
			'iso_a3' => '',
			'iso_number' => '',
			'name' => '',
			'country_priority' => 0,
			'zip_required' => 0,
			'has_provinces' => 0
		);
	}

	/**
	 * @return int
	 */
	public function getCount()
	{
		$db = self::$db;
		$result = $db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'countries');

		return intval($result['c']);
	}

	/**
	 * Get countries list
	 *
	 * @param bool $cache
	 * @param null $orderBy
	 *
	 * @return array
	 */
	public function getCountries($cache = true, $orderBy = null)
	{
		$orderbystr = (is_null($orderBy)?'country_priority DESC, name': $orderBy);
		return $this->getData('SELECT * FROM '.DB_PREFIX.'countries ORDER BY '.$orderbystr, $cache);
	}

	/**
	 * Get country by id
	 *
	 * @param $countryId
	 * @param bool $cache
	 *
	 * @return type
	 */
	public function getCountryById($countryId, $cache = false)
	{
		return $this->getOne('SELECT * FROM '.DB_PREFIX.'countries WHERE coid='. intval($countryId));
	}

	/**
	 * Get country by ISO A2 Code
	 *
	 * @param $code
	 *
	 * @return null
	 */
	public function getCountryByIso2Code($code)
	{
		return $this->getOne('SELECT * FROM '.DB_PREFIX.'countries WHERE iso_a2 LIKE "'.self::$db->escape($code).'"');
	}

	/**
	 * @param $countryId
	 * @return bool|mixed
	 */
	public function deleteCountryById($countryId)
	{
		$db = self::$db;
		if (intval($countryId)< 1) return;

		if ($this->statesRepository->deleteStatesByCountryID($countryId))
		{
			return $db->query('DELETE FROM '.DB_PREFIX.'countries WHERE coid NOT IN(1, 2) AND coid='. intval($countryId));
		}

		return false;
	}

	/**
	 * @return bool|mixed
	 */
	public function deleteAllCountries()
	{
		if ($this->statesRepository->deleteAllStates())
		{
			$db = self::$db;
			return $db->query('DELETE FROM '.DB_PREFIX.'countries where coid NOT IN(1, 2) ');
		}

		return false;
	}

	/**
	 * Get state by id
	 *
	 * @param $stateId
	 * @param bool $cache
	 *
	 * @return array|bool
	 */
	public function getStateById($stateId, $cache = false)
	{
		return $this->statesRepository->getById(intval($stateId), $cache);
	}

	/**
	 * Get countries and states
	 *
	 * @param bool $coid
	 * @return array
	 */
	public function getCountriesStates($coid = false)
	{
		$db = self::$db;

		$result = $db->query('
			SELECT c.coid AS country_id, c.name AS country_name, s.stid AS state_id, s.name AS state_name, c.zip_required, c.has_provinces
			FROM '.DB_PREFIX.'countries AS c
				LEFT JOIN '.DB_PREFIX.'states AS s ON c.coid = s.coid
			'.($coid ? 'WHERE c.coid = '.intval($coid) : '').'
			ORDER BY c.country_priority DESC, c.name, s.name
		');

		$countries = array();
		$countriesMap = array();

		while (($record = $db->moveNext($result)) != false)
		{
			$countryId = $record['country_id'];

			if (!isset($countriesMap[$countryId]))
			{
				$countries[] = array('id' => $countryId, 'name' => $record['country_name'], 'zip_required' => $record['zip_required'], 'has_provinces' => $record['has_provinces'], 'states' => false);
				$countriesMap[$countryId] = count($countries) - 1;
			}

			$countryPosition = $countriesMap[$countryId];

			if ($record['state_id'] != '')
			{
				if (!is_array($countries[$countryPosition]['states'])) $countries[$countryPosition]['states'] = array();

				$stateId = $record['state_id'];

				$countries[$countryPosition]['states'][$stateId] = array('id' => $stateId, 'name' => $record['state_name']);
			}
		}

		if ($coid && isset($countriesMap[$coid]))
		{
			return $countries[$countriesMap[$coid]];
		}

		return $countries;
	}

	public function getCountriesStatesForSettings($returnIsAsText = false, $stateIdParam = 'state_name')
	{
		$db = self::$db;
	
		$result = $db->query('
			SELECT c.coid AS coid, c.iso_a2 AS country_id, c.name AS country_name, s.stid AS state_id, s.short_name AS state_code, s.name AS state_name, c.zip_required, c.has_provinces
			FROM '.DB_PREFIX.'countries AS c
			LEFT JOIN '.DB_PREFIX.'states AS s ON c.coid = s.coid
			ORDER BY c.country_priority DESC, c.name, s.name
		');

		$countries = array();
		$countriesMap = array();

		while (($record = $db->moveNext($result)) != false)
		{
			$countryId = $returnIsAsText ? $record['country_id'] : $record['coid'];

			if (!isset($countriesMap[$countryId]))
			{
				$countries[] = array('id' => $countryId, 'name' => $record['country_name'], 'zip_required' => $record['zip_required'], 'has_provinces' => $record['has_provinces'], 'states' => false);
				$countriesMap[$countryId] = count($countries) - 1;
			}

			$countryPosition = $countriesMap[$countryId];

			if ($record['state_id'] != '')
			{
				if (!is_array($countries[$countryPosition]['states'])) $countries[$countryPosition]['states'] = array();

				$stateId = $record['state_id'];

				$countries[$countryPosition]['states'][$stateId] = array('id' => $record[$returnIsAsText ? $stateIdParam : 'state_id'], 'name' => $record['state_name'], 'code' => $record['state_code']);
			}
		}

		return $countries;
	}
	
	public function countriesActivation($data)
	{
		$db = self::$db;
		if (isset($data['mode']) && $data['mode'] == 'all')
		{
			return $db->query('UPDATE ' . DB_PREFIX . 'countries SET available="' . $data['available'] . '"');
		}
		else
		{
			return $db->query('UPDATE ' . DB_PREFIX . 'countries SET available="' . $data['available'] . '" WHERE coid=' . intval($data['id']));
		}
	}
}

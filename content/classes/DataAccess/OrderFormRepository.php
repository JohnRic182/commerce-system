<?php
/**
 * Class DataAccess_OrderFormRepository
 */
class DataAccess_OrderFormRepository extends DataAccess_BaseRepository
{
	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'name' => '',
			'active' => 'No',
			'can_change_quantities' => 'No',
			'thank_you_page_url' => '',
			'url_default' => '',
			'url_custom' => '',
		);
	}

	/**
	 * Get order form by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne(
			'SELECT *, IF(url_custom="", url_default, url_custom) AS url FROM '.DB_PREFIX.'order_forms WHERE ofid='.intval($id)
		);
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams)
	{
		$where = array();

		if (isset($searchParams['visibility']))
		{
			switch ($searchParams['visibility'])
			{
				case 'visible' : $where[] = 'of.active="Yes"'; break;
				case 'invisible' : $where[] = 'of.active="No"'; break;
			}
		}

		if (isset($searchParams['product_id']) && trim($searchParams['product_id']) != '')
		{
			$where[] = 'of.ofid IN (SELECT ofp.ofid FROM '.DB_PREFIX.'order_forms_products ofp INNER JOIN '.DB_PREFIX.'products p WHERE p.pid = ofp.pid AND p.product_id like "%'.$this->db->escape(trim($searchParams['product_id'])).'%")';
		}

		if (isset($searchParams['product_name']) && trim($searchParams['product_name']) != '')
		{
			$where[] = 'of.ofid IN (SELECT ofp.ofid FROM '.DB_PREFIX.'order_forms_products ofp INNER JOIN '.DB_PREFIX.'products p WHERE p.pid = ofp.pid AND p.title like "%'.$this->db->escape(trim($searchParams['product_name'])).'%")';
		}

		if (isset($searchParams['name']) && $searchParams['name'] != '') $where[] = 'of.name LIKE "%'.$this->db->escape($searchParams['name']).'%"';

		return count($where) > 0 ? ' WHERE '.implode(' AND ', $where).' ' : '';
	}

	/**
	 * Get order forms count
	 *
	 * @param array|null $searchParams
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null)
	{
		$result = $this->db->selectOne('SELECT COUNT(of.ofid) AS c FROM '.DB_PREFIX.'order_forms of'.$this->getSearchQuery($searchParams));

		return intval($result['c']);
	}

	/**
	 * Get order forms list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'of.name';

		return $this->db->selectAll(
			'SELECT of.*, IF(of.url_custom="", of.url_default, of.url_custom) AS url,
				(SELECT COUNT(ofp.ofpid) FROM '.DB_PREFIX.'order_forms_products ofp WHERE of.ofid = ofp.ofid) AS count_products
				 FROM '.DB_PREFIX.'order_forms of '.$this->getSearchQuery($searchParams).' ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Get proucts list by order form id
	 *
	 * @param int $ofid
	 *
	 * @return mixed
	 */
	public function getFormProducts($ofid)
	{

		$db = $this->db;
		$result = false;

		if ($ofid)
		{
			$_result = $db->selectAll("SELECT of_products.*, p.product_id, p.title FROM ".DB_PREFIX."order_forms_products as of_products
			 						JOIN ".DB_PREFIX."products as p on of_products.pid = p.pid
									WHERE ofid = '".intval($ofid)."'");
			if ($_result)
			{
				foreach($_result as $k => $p)
				{
					$options_js = (trim($p['attributes_list_prepared']) != '')?json_decode($p['attributes_list_prepared'], true):array();
					$options = array();
					foreach ($options_js as $key => $val)
					{
						$options[$key] = urldecode($val);
					}
					$item = $p;
					$item['options'] = $options;
					$result[$k] = $item;
				}
			}
		}

		return $result;
	}

	public function getFormDetailsProducts($ofid)
	{
		return $this->db->selectAll("
						SELECT
							ofp.*, p.*
						FROM ".DB_PREFIX."order_forms_products as ofp
			 			INNER JOIN ".DB_PREFIX."products as p on ofp.pid = p.pid
						WHERE ofp.ofid = '".intval($ofid)."'");
	}

	public function getOneOrderFormProduct($data)
	{

		$ofid = $data['ofid'];
		$pid = $data['pid'];
		$attributes = $data['attributes'];

		$productAttributes = isset($data['productAttributes']) ? $data['productAttributes'] : array();

		$attributes_cond = '';
		if (isset($data['attributes']) && is_array($data['attributes']) && count($data['attributes']) > 0)
		{
			$attributesList = '';

			foreach ($productAttributes as $productAttribute)
			{
				if ($productAttribute['is_active'] == 'Yes' && in_array($productAttribute['attribute_type'], array('radio', 'select')))
				{
					$attributesList = $attributesList.($attributesList == '' ? '' : "\n").$productAttribute['name'].': '.htmlspecialchars_decode($data['attributes'][$productAttribute['paid']]);
				}
			}

			$attributes_cond  = " AND ofp.attributes_list =  '" . $this->db->escape($attributesList) . "'";
		}

		return $this->db->selectOne("
						SELECT
							ofp.*, p.*
						FROM ".DB_PREFIX."order_forms_products as ofp
			 			INNER JOIN ".DB_PREFIX."products as p on ofp.pid = p.pid
						WHERE ofp.ofid = '".intval($ofid)."' AND p.pid = '" . intval($pid) . "'" . $attributes_cond
				);
	}

	public function getOneOrderFormProductById($ofpid)
	{

		return $this->db->selectOne("
			SELECT ofp.*, p.*
			FROM ".DB_PREFIX."order_forms_products as ofp
				INNER JOIN ".DB_PREFIX."products as p on ofp.pid = p.pid
			WHERE ofpid = '" . $ofpid . "'
		");
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '')
			{
				$errors['name'] = array('Please enter form name');
			}
			if ($data['thank_you_page'] == 'custom' && trim($data['thank_you_page_url']) == '')
			{
				$errors['thank_you_page_url'] = array('Please enter thank you page url');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Get validation errors product
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrorsProduct($data, $mode, $id = null)
	{
		$error = array();
		if (is_array($data))
		{
			if (intval($data['pid']) == 0 || intval($data['ofid']) == 0) $error = 'Product can not be added. Please refresh the page and try again';
		}

		return $error != '' ? $error : false;
	}

	/**
	 * Persist order form
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['ofid']) ? $data['ofid'] : null);

		$db->assignStr('active', isset($data['active']) && $data['active'] == 'Yes' ? 'Yes' : 'No');
		$db->assignStr('can_change_quantities', isset($data['can_change_quantities']) && $data['can_change_quantities'] == 'Yes' ? 'Yes' : 'No');
		$db->assignStr('name', $data['name']);

		if ($data['thank_you_page'] == 'default')
		{
			$db->assignStr('thank_you_page_url', '');
		}
		else 
		{
			$db->assignStr('thank_you_page_url', $data['thank_you_page_url']);
		}

		$db->assignStr('url_default', $data['url_default']);
		$db->assignStr('url_custom', $data['url_custom']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'order_forms',  'WHERE ofid='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'order_forms');
		}
	}

	public function updateProductsQuantities($quantities)
	{
		$db = $this->db;

		foreach ($quantities as $key => $val)
		{
			$db->reset();
			$db->assignStr('quantity', intval($val));
			$db->update(DB_PREFIX.'order_forms_products', 'WHERE ofpid ='.intval($key));
		}
	}

	/**
	 * add product
	*/
	/**
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persistProduct($data, $params = null)
	{
		$productAttributes = is_array($params) && isset($params['productAttributes']) ? $params['productAttributes'] : array();
		$db = $this->db;

		$db->reset();

		if (isset($data['attributes']) && is_array($data['attributes']) && count($data['attributes']) > 0)
		{
			$attributesList = '';
			$attributesClean = '';
			$attributesPrepared = array();

			foreach ($productAttributes as $productAttribute)
			{
				if ($productAttribute['is_active'] == 'Yes' && in_array($productAttribute['attribute_type'], array('radio', 'select')))
				{
					$attributesClean = $attributesClean.($attributesClean == '' ? '' : "\n").$productAttribute['paid'].': '.htmlspecialchars_decode($data['attributes'][$productAttribute['paid']]);
					$attributesList = $attributesList.($attributesList == '' ? '' : "\n").$productAttribute['name'].': '.htmlspecialchars_decode($data['attributes'][$productAttribute['paid']]);
					$attributesPrepared[$productAttribute['paid']] = htmlspecialchars_decode($data['attributes'][$productAttribute['paid']]);
				}
			}
			$db->assignStr('attributes_list_clean', $attributesClean);
			$db->assignStr('attributes_list', $attributesList);
			$db->assignStr('attributes_list_prepared', json_encode($attributesPrepared));
		}

		$db->assignStr('pid', $data['pid']);
		$db->assignStr('ofid', $data['ofid']);
		$db->assignStr('quantity', intval($data['quantity']));

		return $db->insert(DB_PREFIX.'order_forms_products');
	}

	public function deleteProduct($ofpid)
	{
		$db = $this->db;
		$db->query('DELETE FROM '.DB_PREFIX.'order_forms_products WHERE ofpid =' . intval($ofpid));
	}

	/**
	 * Delete order forms by ids
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		// delete order form & reset products
		$this->db->query('DELETE FROM '.DB_PREFIX.'order_forms WHERE ofid IN('.implode(',', $ids).')');
		$this->db->query('DELETE FROM '.DB_PREFIX.'order_forms_products WHERE ofid IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * Delete order forms in search results
	 *
	 * @param $searchParams
	 *
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams)
	{
		$searchQuery = $this->getSearchQuery($searchParams);

		// delete order forms & reset products
		$this->db->query('DELETE FROM '.DB_PREFIX.'order_forms'.$searchQuery);
		$this->db->query('DELETE FROM '.DB_PREFIX.'order_forms_products'.$searchQuery);

		return true;
	}


	/**
	 * @param $url
	 * @param $ofid
	 * @return int
	 */
	public function hasUrlDuplicate($url, $ofid)
	{
		$escapedUrl = $this->db->escape($url);
		$db = $this->db;

		$result = $db->query('
			SELECT url_custom FROM '.DB_PREFIX.'order_forms
			WHERE (url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'")
				AND ofid != ' . intval($ofid) . '

			UNION

			SELECT url_custom FROM '.DB_PREFIX.'manufacturers
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'products
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'catalog
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'pages
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"
		');

		return $db->numRows($result);
	}
}
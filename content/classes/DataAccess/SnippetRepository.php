<?php

/**
 * Class DataAccess_SnippetRepository
 */
class DataAccess_SnippetRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $mode
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'is_visible' => 'No',
			'snippet_id' => '',
			'name' => '',
			'priority' => '5',
			'language' => 'javascript',
			'type' => 'chunk',
			'page_types' => array(),
			'content' => ''
		);
	}

	/**
	 * @param $id
	 * @return array|bool
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM ' . DB_PREFIX. 'snippets WHERE TRIM(internal_id) = "" AND sid=' . intval($id));
	}

	/**
	 * @param $snippet_id
	 * @return array|bool
	 */
	public function getBySnippetId($snippet_id)
	{
		return $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'snippets WHERE snippet_id="' . $this->db->escape($snippet_id) . '"');
	}

	/**
	 * @param null $searchParams
	 * @return int
	 */
	public function getCount($searchParams = null)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'snippets WHERE TRIM(internal_id) = ""');

		return intval($result['c']);
	}

	/**
	 * Get pages list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'priority';

		return $this->db->selectAll('
			SELECT *
			FROM ' . DB_PREFIX . 'snippets
			WHERE TRIM(internal_id) = ""
			ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '') $errors['name'] = array('Please enter name');

			if (trim($data['page_types']) == '') $errors['page_types'] = array('Please select page type');

			if (trim($data['snippet_id']) == '')
			{
				$errors['snippet_id'] = array('Please enter snippet id');
			}
			else
			{
				$db = $this->db;
				if ($db->selectOne('SELECT * FROM '.DB_PREFIX.'snippets WHERE snippet_id="'.$db->escape($data['snippet_id'] ).'"'.($mode == 'update' && !is_null($id) ? ' AND sid <> '.intval($id) : '')))
				{
					$errors['snippet_id'] = array('Snippet id must be unique within snippets');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist page
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pid']) ? $data['pid'] : null);

		$db->reset();

		$db->assignStr('is_visible', isset($data['is_visible']) && $data['is_visible'] == 'Yes' ? 'Yes' : 'No');
		$db->assignStr('snippet_id', $data['snippet_id']);

		$db->assignStr('name', isset($data['name']) ? $data['name'] : '');
		$db->assignStr('priority', isset($data['priority'])? intval($data['priority']) : '0');

		$db->assignStr('language', $data['language']);
		$db->assignStr('type', $data['type']);
		$db->assignStr('page_types', $data['page_types']);

		$db->assignStr('content', isset($data['content']) ? $data['content'] : '');

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX . 'snippets',  'WHERE sid=' . intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			$res = $db->insert(DB_PREFIX . 'snippets');
			return $res;
		}
	}

	/**
	 * Delete snippet by ids
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		// delete snippets
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'snippets WHERE TRIM(internal_id) = "" AND sid IN(' . implode(',', $ids) . ')');
	}

	/**
	 * Delete all snippets
	 *
	 * @param $ids
	 */
	public function deleteAll()
	{
		// delete all snippets
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'snippets WHERE TRIM(internal_id) = ""');
	}
}
<?php

/**
 * Class DataAccess_ProductsFamilyRepository
 */
class DataAccess_ProductsFamilyRepository extends DataAccess_BaseRepository
{

	/**
	 * @param $pf_id
	 * @param array $products
	 * @return array
	 */
	public function assignProducts($pf_id, $products= array())
	{
		$db = $this->db;

		if ($pf_id)
		{
			$products = trim($products);
			$products = explode(',', $products);

			foreach ($products as $pid)
			{
				if ($pid > 0)
				{
					$db->reset();
					$db->assign('pf_id', $pf_id);
					$db->assign('pid', $pid);
					$db->insert(DB_PREFIX.'products_families_content');
				}
			}
			return array('result' => 1);
		}
		else
		{
			return array('result' => 0);
		}
	}

	/**
	 * @param $pf_id
	 * @param array $products
	 * @return array
	 */
	public function unassignProducts($pf_id, $products = array())
	{
		$db = $this->db;

		if ($pf_id)
		{
			$products = trim($products);
			$products = explode(',', $products);

			foreach ($products as $pid)
			{
				if ($pid > 0)
				{
					$db->query('DELETE FROM '.DB_PREFIX.'products_families_content WHERE pf_id='.intval($pf_id).' and pid='.intval($pid));
				}
			}
			return array('result' => 1);
		}
		else
		{
			return array('result' => 0);
		}
	}

	/**
	 * @param $pf_id
	 * @param array $categories
	 * @return array
	 */
	public function getAssignedProducts($pf_id, $categories = array())
	{
		$db = $this->db;

		$db->query('
			SELECT p.pid, p.title
			FROM '.DB_PREFIX.'products p
				INNER JOIN '.DB_PREFIX.'products_families_content pfc ON p.pid = pfc.pid AND pfc.pf_id = '.intval($pf_id).'
			ORDER BY p.title
		');

		return array(
			'count' => $db->numRows(),
			'products' => $db->getRecords()
		);
	}

	/**
	 * @param $pf_id
	 * @param array $categories
	 * @return array
	 */
	public function getProducts($pf_id, $categories = array())
	{
		$categories = trim($categories) == '' ? '0' : $categories;
		$categories = preg_match("/^([0-9\,])+$/", $categories) ? $categories : '0';
 
		$where = ($categories == '0') ? (' 1=1 ') : (' p.cid IN ('.$categories.') ');
		$this->db->query('
			SELECT p.pid, p.tax_class_id, p.title
			FROM '.DB_PREFIX.'products p
			LEFT JOIN '.DB_PREFIX.'products_families_content pfc ON p.pid=pfc.pid AND pfc.pf_id='.intval($pf_id).'
			WHERE pfc.pid IS NULL AND p.product_id <> "gift_certificate"
				AND '.$where.'
			ORDER BY p.title
		');

		return array(
			'count' => $this->db->numRows(),
			'products' => $this->db->getRecords()
		);
	}

	/**
	 * @param $id
	 * @return array|bool
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products_families WHERE pf_id='.intval($id));
	}

	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array('name' => '');
	}

	/**
	 *
	 */
	public function deleteAll()
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_families_content ');
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_families ');
	}

	/**
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		$this->db->query('DELETE FROM '.DB_PREFIX.'products_families_content WHERE pf_id IN('.implode(',', $ids).')');
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_families WHERE pf_id IN('.implode(',', $ids).')');
	}

	/**
	 * @param $data
	 * @param $mode
	 * @param null $id
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['name']) == '') $errors['name'] = array('Please enter title');
		}

		return count($errors) > 0 ? $errors : false;

	}

	/**
	 * @param $data
	 * @return array|bool
	 */
	public function getDeleteValidationErrors($data)
	{
		return false;
	}

	/**
	 * @return int
	 */
	public function getCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'products_families');

		return intval($result['c']);
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';

		return $this->db->selectAll(
			'SELECT * FROM '.DB_PREFIX.'products_families ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pf_id']) ? $data['pf_id'] : null);
	 
		$db->reset();
		$db->assignStr('name', $data['name']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products_families', 'WHERE pf_id='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'products_families');
		}
	}
} 
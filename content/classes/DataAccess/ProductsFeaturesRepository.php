<?php

/**
 * Class DataAccess_ProductsFeaturesRepository
 */
class DataAccess_ProductsFeaturesRepository extends DataAccess_BaseRepository
{
	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'feature_name' => '',
			'feature_id' => '',
			'feature_active' => 0
		);
	}

	/**
	 * Get product features by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'products_features WHERE product_feature_id=' . intval($id));
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 * @param $logic
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams = null, $logic = 'AND')
	{
		if ($searchParams == null) return '';

		$where = array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		if (isset($searchParams['feature_active']) && $searchParams['feature_active'] <> 'any')
		{
			if ($searchParams['feature_active'] == 'No') $where[] = 'feature_active = 0';
			else if ($searchParams['feature_active'] == 'Yes') $where[] = 'feature_active = 1';
		}

		if (isset($searchParams['search_str']))
		{
			$searchStr = trim($searchParams['search_str']);

			if ($searchStr != '')
			{
				$searchStr = $this->db->escape($searchStr);
				$where[] = '(pf.feature_name like "%' . $searchStr . '%" OR pf.feature_id like "%' . $searchStr . '%")';
			}
		}

		return count($where) > 0 ? ' WHERE ' . implode( ' ' . $logic . ' ', $where) . ' ' : '';
	}

	/**
	 * Get product features count
	 *
	 * @param array|null $searchParams
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS product_feature_count  FROM ' . DB_PREFIX . 'products_features pf' . $this->getSearchQuery($searchParams, $logic));

		return intval($result['product_feature_count']);
	}

	/**
	 * Get product features list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = '*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'feature_name';

		switch($orderBy)
		{
			case 'feature_name_asc'  : $orderBy = 'feature_name, feature_id'; break;
			case 'feature_name_desc' : $orderBy = 'feature_name DESC, feature_id DESC'; break;
			case 'feature_id_asc'  : $orderBy = 'feature_id'; break;
			case 'feature_id_desc' : $orderBy = 'feature_id DESC'; break;
			case 'feature_active_asc' : $orderBy = 'feature_active'; break;
			case 'feature_active_desc' : $orderBy = 'feature_active DESC'; break;
		}

		return $this->db->selectAll('
			SELECT ' . $columns . '
			FROM ' . DB_PREFIX . 'products_features pf' . $this->getSearchQuery($searchParams, $logic) . '
			ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['feature_name']) == '') $errors['feature_name'] = array('Please enter Feature Name');
			if (trim($data['feature_id']) == '') $errors['feature_id'] = array('Please enter Feature ID');

			if (count($errors) == 0)
			{
				$db = $this->db;
				$result = $db->selectOne('SELECT product_feature_id FROM ' . DB_PREFIX . 'products_features WHERE feature_id="' . $db->escape($data['feature_id']) . '"');

				if ($result)
				{
					if (intval($id) <= 0 || (intval($id) > 0 && $result['product_feature_id'] <> intval($id)))
					{
						$errors['feature_id'] = array('Product Feature ID must be unique within products features');
					}
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist product features
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		$db->reset();
		$db->assignStr('feature_name', $data['feature_name']);
		$db->assignStr('feature_id', $data['feature_id']);
		$db->assignStr('feature_active', intval($data['feature_active']));

		if (isset($data['id']) && intval($data['id']) > 0)
		{
			$db->update(DB_PREFIX . 'products_features', 'WHERE product_feature_id=' . intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'products_features');
		}
	}

	/**
	 * Delete product feature by ids
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		if (count($ids) == 0) return false;

		/** @var DB $db */
		$db = $this->db;
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features_groups_relations WHERE product_feature_id IN(' . implode(',', $ids) . ')');
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features WHERE product_feature_id IN(' . implode(',', $ids) . ')');

		return true;
	}

	/**
	 * Delete products features in search results
	 *
	 * @param $searchParams
	 * @param $logic
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams, $logic)
	{
		$ids = array();

		$db = $this->db;

		$result = $db->query('SELECT product_feature_id FROM ' . DB_PREFIX . 'products_features pf' . $this->getSearchQuery($searchParams, $logic));
		if ($db->numRows($result) < 1) return false;

		while ($productFeature = $db->moveNext($result))
		{
			$ids[] = $productFeature['product_feature_id'];
		}

		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features_groups_relations WHERE product_feature_id IN(' . implode(',', $ids) . ')');
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features WHERE product_feature_id IN(' . implode(',', $ids) . ')');

		return true;
	}

	/**
	 * @param $productFeatureGroupsId
	 * @param $productId
	 * @return array
	 */
	public function getProductsFeaturesValuesByProductsFeaturesGroupId($productFeatureGroupsId, $productId)
	{
		return $this->db->selectAll('
			SELECT
				pfgpr.pid,
				pfgpr.product_feature_group_id,
				pfgr.product_feature_group_relation_id,
				pf.product_feature_id,
				pf.feature_id,
				pf.feature_name,
				pfvr.product_feature_option_id,
				pfv.product_feature_option_value
			FROM ' . DB_PREFIX . 'products_features_groups_products_relations pfgpr
			INNER JOIN ' . DB_PREFIX . 'products_features_groups_relations pfgr ON (pfgr.product_feature_group_id = pfgpr.product_feature_group_id)
			INNER JOIN ' . DB_PREFIX . 'products_features pf ON (pfgr.product_feature_id = pf.product_feature_id)
			LEFT JOIN ' . DB_PREFIX . 'products_features_values_relations pfvr ON (pfgpr.feature_group_product_relation_id = pfvr.feature_group_product_relation_id AND pfgr.product_feature_group_relation_id = pfvr.product_feature_group_relation_id)
			LEFT JOIN ' . DB_PREFIX . 'products_features_values pfv ON (pfvr.product_feature_option_id = pfv.product_feature_option_id)
			WHERE pfgpr.product_feature_group_id = ' . intval($productFeatureGroupsId) . ' AND pfgpr.pid = ' . intval($productId) . '
			ORDER BY pfgr.priority, pf.feature_name, pf.feature_id
		');
	}

	/**
	 * @param $productFeatureGroupId
	 * @return mixed
	 */
	public function getProductsFeaturesValuesOptionsByProductsFeaturesGroupId($productFeatureGroupId)
	{
		return $this->db->selectAll('
			SELECT
				pfgr.product_feature_group_id,
				pfgr.product_feature_id,
				pfv.product_feature_option_id,
				pfv.product_feature_option_value
			FROM ' . DB_PREFIX . 'products_features_groups_relations pfgr
			INNER JOIN ' . DB_PREFIX . 'products_features_values_relations pfvr ON pfgr.product_feature_group_relation_id = pfvr.product_feature_group_relation_id
			INNER JOIN ' . DB_PREFIX . 'products_features_values pfv ON pfvr.product_feature_option_id = pfv.product_feature_option_id
			WHERE pfgr.product_feature_group_id = ' . intval($productFeatureGroupId) . '
			GROUP BY pfv.product_feature_option_id
			ORDER BY pfv.product_feature_option_value
		');
	}
}
<?php

/**
 * Class DataAccess_AppRepository
 */
class DataAccess_AppRepository extends DataAccess_BaseRepository
{
	/**
	 * Check is app already enabled
	 *
	 * @param $appKey
	 *
	 * @return array|bool
	 */
	public function getEnabledApp($appKey)
	{
		$db = $this->db;

		return $db->selectOne('SELECT * FROM '.DB_PREFIX.'enabled_apps WHERE `key`="'.$db->escape($appKey).'"');
	}

	/**
	 * Get enables apps
	 *
	 * @return mixed
	 */
	public function getEnabledApps()
	{
		return $this->db->selectAll('SELECT `key` FROM '.DB_PREFIX.'enabled_apps');
	}

	/**
	 * Enable app
	 *
	 * @param $appKey
	 */
	public function enableApp($appKey)
	{
		$db = $this->db;

		if (!$this->getEnabledApp($appKey))
		{
			$this->db->query('INSERT INTO '.DB_PREFIX.'enabled_apps (`key`) VALUES ("'.$db->escape($appKey).'")');
		}
	}

	/**
	 * Disable app
	 *
	 * @param $appKey
	 */
	public function disableApp($appKey)
	{
		$db = $this->db;

		$db->query('DELETE FROM '.DB_PREFIX.'enabled_apps WHERE `key`="'.$db->escape($appKey).'"');
	}
}
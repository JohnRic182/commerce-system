<?php

/**
 * Class DataAccess_ProductImageRepository
 */
class DataAccess_ProductImageRepository extends DataAccess_BaseRepository
{
	/** @var DataAccess_SettingsRepository */
	protected $settings = null;

	/** @var Admin_Service_FileUploader $fileUploadService */
	protected $fileUploadService = null;

	protected $extensions = array('jpg', 'png', 'gif');
	protected $acceptedTypes = array(IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_PNG);
	protected $typesExtensions = array(IMAGETYPE_JPEG => 'jpg', IMAGETYPE_GIF => 'gif', IMAGETYPE_PNG => 'png');

	protected $productImagesDir = "images/products";
	protected $productThumbsDir = "images/products/thumbs";
	protected $productPreviewDir = "images/products/preview";
	protected $productSecondaryImagesDir = "images/products/secondary";
	protected $productSecondaryImagesThumbsDir = "images/products/secondary/thumbs";

	protected $errors = array();

	/**
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 * @param Admin_Service_FileUploader $fileUploadService
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings, Admin_Service_FileUploader $fileUploadService)
	{
		parent::__construct($db);
		$this->settings = $settings;
		$this->fileUploadService = $fileUploadService;
	}

	/**
	 * @param $mode
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'pid' => 0,
			'is_visible' => 'Yes',
			'image_priority' => 0,
			'width' => 0,
			'height' => 0,
			'filename' => '',
			'type' => '',
			'caption' => '',
			'alt_text' => ''
		);
	}

	/**
	 * @param array $pids
	 * @return array
	 */
	public function getByProductIds(array $pids)
	{
		if (count($pids) == 0) return array();

		$products = $this->db->selectAll('SELECT pid, product_id, image_alt_text FROM '.DB_PREFIX.'products WHERE pid IN ('.implode(',', $pids).')');
		$imagesByProductIds = array();

		foreach ($products as &$product)
		{
			$image = $this->getPrimaryImage($product['pid'], $product['product_id'], $product['image_alt_text']);
			if ($image)
			{
				$imagesByProductIds[$product['pid']] = array($image);
			}
		}

		$result = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'products_images WHERE pid IN ('.implode(',', $pids).') ORDER BY pid, image_priority DESC, iid');

		foreach ($result as &$imageData)
		{
			$image = $this->getSecondaryImage($imageData);
			if ($image)
			{
				if (!isset($imagesByProductIds[$imageData['pid']]))
				{
					$imagesByProductIds[$imageData['pid']] = array();
				}
				$imagesByProductIds[$imageData['pid']][] = $image;
			}
		}

		return array_values($imagesByProductIds);
	}

	/**
	 * @param $pid
	 * @return array
	 */
	public function getByProductId($pid)
	{
		$product = $this->db->selectOne('SELECT pid, product_id, image_alt_text FROM '.DB_PREFIX.'products WHERE pid = '.intval($pid));
		if (!$product) return array();

		$images = array();

		$image = $this->getPrimaryImage($product['pid'], $product['product_id'], $product['image_alt_text']);
		if ($image) $images[] = $image;

		$result = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'products_images WHERE pid = '.intval($pid).' ORDER BY pid, image_priority DESC, iid');

		foreach ($result as &$imageData)
		{
			$image = $this->getSecondaryImage($imageData);
			if ($image)
			{
				$images[] = $image;
			}
		}

		return $images;
	}

	/**
	 * Add primary product image
	 *
	 * @param $productDbId
	 * @param $productId
	 * @param $imageFile
	 * @param array $imageAttributes
	 * @return bool
	 */
	public function addPrimaryImage($productDbId, $productId, $imageFile, $imageAttributes = array())
	{
		$db = $this->db;

		if ($imageAttributes['image_location'] == 'Web')
		{
			$this->persistWebPrimaryImage($productDbId, $imageAttributes);
			return true;
		}
		else
		{
			if (!isset($imageFile['error']) || $imageFile['error'] != UPLOAD_ERR_OK)
			{
				$this->errors[] = array('Can\'t upload product primary image. Please check free disk space and permissions');
				return false;
			}

			$imageInfo = getimagesize($imageFile['tmp_name']);
			$imageType = $imageInfo[2];

			if (in_array($imageType, $this->acceptedTypes))
			{
				$imageFileName = ImageUtility::imageFileName($productId);

				foreach ($this->extensions as $extension)
				{
					@unlink($this->productImagesDir . '/' . $imageFileName . '.' . $extension);
					@unlink($this->productThumbsDir . '/' . $imageFileName . '.' . $extension);
					@unlink($this->productPreviewDir . '/' . $imageFileName . '.' . $extension);
				}

				$imageExtension = $this->typesExtensions[$imageType];

				$imageFilePath = $this->productImagesDir . '/' . $imageFileName . '.' . $imageExtension;
				@rename($imageFile['tmp_name'], $imageFilePath);

				if ($imageFilePath)
				{
					$optimizer = new Admin_Service_ImageOptimizer();

					if ($this->settings->get('CatalogOptimizeImages') == 'YES')
					{
						$optimizer->optimize($imageFilePath);
					}

					@chmod($imageFilePath, FileUtils::getFilePermissionMode());

					$settings = $this->settings;

					if ($settings->get('CatalogThumbType') != 'Manual')
					{
						$fileName = $this->productThumbsDir . '/' . $imageFileName . '.' . $imageExtension;

						ImageUtility::generateProductThumb($imageFilePath, $fileName);
					}

					if ($settings->get('CatalogThumb2Type') != 'Manual')
					{
						$fileName = $this->productPreviewDir . '/' . $imageFileName . '.' . $imageExtension;

						ImageUtility::generateProductPreview($imageFilePath, $fileName);
					}

					$db->reset();
					$db->assignStr('image_url', "");
					$db->assignStr('image_location', "Local");
					$db->assignStr('image_alt_text', isset($imageAttributes['image_alt_text']) ? $imageAttributes['image_alt_text'] : '');
					$db->update(DB_PREFIX . 'products', 'WHERE pid=' . intval($productDbId));

					return true;
				}
				else
				{
					$this->errors[] = array('Can\'t upload product primary image. Please check free disk space and permissions');
				}
			}
			else
			{
				$this->errors[] = array('Can\'t upload product primary image. Accepted image types are JPG, PNG and GIF');
			}
		}
		return false;
	}

	/**
	 * Add secondary images
	 *
	 * @param $mode
	 * @param $productDbId
	 * @param $productId
	 * @param array $imagesFiles
	 *
	 * @return bool
	 */
	public function addSecondaryImages($mode, $productDbId, $productId, array $imagesFiles)
	{
		$db = $this->db;

		$imageFileName = ImageUtility::imageFileName($productId);

		$uploadError = false;
		$imageTypeError = false;
		$maxPriority = 1;

		$currentFileNames = array();
		if ($mode == 'update')
		{
			$result = $db->query('SELECT filename, image_priority FROM '.DB_PREFIX.'products_images WHERE pid='.intval($productDbId));
			while ($imageData = $db->moveNext($result))
			{
				$currentFileNames[] = $imageData['filename'];
				if ($imageData['image_priority'] > $maxPriority)
				{
					$maxPriority = $imageData['image_priority'];
				}
			}
		}

		foreach ($imagesFiles as $imageFile)
		{
			if (!isset($imageFile['error']) || $imageFile['error'] != UPLOAD_ERR_OK)
			{
				$uploadError = true;
				continue;
			}

			$imageInfo = getimagesize($imageFile['tmp_name']);
			$imageType = $imageInfo[2];

			if (in_array($imageType, $this->acceptedTypes))
			{
				for ($imageNum=1; $imageNum<=10000; $imageNum++)
				{
					if (!in_array($imageFileName.'-'.$imageNum, $currentFileNames)) break;
				}

				$currentFileNames[] = $imageFileName.'-'.$imageNum;

				$imageExtension = $this->typesExtensions[$imageType];

				if ($imageFilePath = $this->fileUploadService->moveUploaderFile($imageFile, $this->productSecondaryImagesDir, $imageFileName.'-'.$imageNum.'.'.$imageExtension))
				{
					@chmod($imageFilePath, FileUtils::getFilePermissionMode());

					$db->reset();

					$db->assign('pid', intval($productDbId));
					$db->assignStr('is_visible', 'Yes');
					$db->assign('width', intval($imageInfo[0]));
					$db->assign('height', intval($imageInfo[1]));
					$db->assignStr('type', $imageExtension);
					$db->assignStr('filename', $imageFileName.'-'.$imageNum);
					$db->assignStr('caption', '');
					$db->assignStr('alt_text', '');
					$db->assign('image_priority', $maxPriority);

					$db->insert(DB_PREFIX.'products_images');

					$maxPriority++;

					$imageFilePathSecondaryThumb = $this->productSecondaryImagesThumbsDir . '/' . $imageFileName . '-' . $imageNum . '.' . $imageExtension;

					ImageUtility::generateProductSecondaryThumb($imageFilePath, $imageFilePathSecondaryThumb);

					if ($this->settings->get('CatalogOptimizeImages') == 'YES')
					{
						$optimizer = new Admin_Service_ImageOptimizer();
						$optimizer->optimize($imageFilePath);
						// $optimizer->optimize($imageFilePathSecondaryThumb);
					}
				}
				else
				{
					$uploadError = true;
				}
			}
			else
			{
				$imageTypeError = true;
			}
		}

		if ($uploadError) $this->errors[] = array('Can\'t upload one or more product secondary image. Please check free disk space and permissions');
		if ($imageTypeError) $this->errors[] = array('Can\'t upload one or more product secondary image. Accepted image types are JPG, PNG and GIF');
	}

	/**
	 * Get the next image filename
	 * @param $productDbId
	 * @param $productId
	 * @return int
	 */
	function getNextSecondaryImageName($productDbId, $productId)
	{
		$db = $this->db;
		$result = $db->query('SELECT filename FROM ' . DB_PREFIX . 'products_images WHERE pid=' . intval($productDbId));

		$currentFileNames = array();
		while ($imageData = $db->moveNext($result))
		{
			$currentFileNames[] = $imageData['filename'];
		}

		$imageFileName = ImageUtility::imageFileName($productId);
		for ($imageNumber = 1; $imageNumber <= 10000; $imageNumber++)
		{
			if (!in_array($imageFileName . '-' . $imageNumber, $currentFileNames)) break;
		}
		return $imageFileName . '-' . $imageNumber;
	}

	/**
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @param $productDbId
	 * @param $productId
	 * @param string $altText
	 * @param string $imageLocation
	 * @param string $imageUrl
	 * @return array|bool
	 */
	public function getPrimaryImage($productDbId, $productId, $altText = '', $imageLocation = 'Local', $imageUrl = '')
	{
		if ($imageLocation == 'Local' && $productImageFile = ImageUtility::productHasImage($productId))
		{
			if ($imageInfo = getimagesize($productImageFile))
			{
				return array(
					'image_id' => 'primary',
					'pid' => $productDbId,
					'primary' => true,
					'imageFileName' => $productImageFile,
					'imageFileSize' => filesize($productImageFile),
					'width' => isset($imageInfo[0]) ? $imageInfo[0] : false,
					'height' => isset($imageInfo[1]) ? $imageInfo[1] : false,
					'imageThumbFileName' => ImageUtility::productHasThumb($productId),
					'imagePreviewFileName' => ImageUtility::productHasThumb2($productId),
					'caption' => $altText,
					'altText' => $altText,
					'priority' => 0,
					'imageUrl' => $this->settings->get('GlobalHttpUrl').'/'.$productImageFile,
					'location' => 'Local',
				);
			}
		}
		else if ($imageLocation == 'Web' && $imageUrl != '')
		{
			return array(
				'image_id' => 'primary',
				'pid' => $productDbId,
				'primary' => true,
				'imageFileName' => $imageUrl,
				'imageFileSize' => false,
				'width' => false,
				'height' => false,
				'imageThumbFileName' => false,
				'imagePreviewFileName' => false,
				'caption' => $altText,
				'altText' => $altText,
				'imageUrl' => $imageUrl,
				'location' => 'Web'
			);
		}

		return false;
	}

	/**
	 * @param $productDbId
	 * @return array
	 */
	public function getSecondaryImages($productDbId)
	{
		$db = $this->db;

		$result = $db->query('SELECT * FROM ' . DB_PREFIX . 'products_images WHERE pid=' . intval($productDbId) . ' ORDER BY image_priority, iid');

		$images = array();
		while ($imageData = $db->moveNext($result))
		{
			$imageDataRow = $this->getSecondaryImage($imageData);
			if (!is_null($imageDataRow))
			{
				$images[] = $imageDataRow;
			}
		}

		return $images;
	}

	/**
	 * @param $imageData
	 * @return array|null
	 */
	protected function &getSecondaryImage(&$imageData)
	{
		$image = null;
		$productImageFile = $this->productSecondaryImagesDir . '/' . $imageData['filename'] . '.' . $imageData['type'];

		$productImageThumb = $this->productSecondaryImagesThumbsDir . '/' . $imageData['filename'] . '.' . $imageData['type'];

		if (is_file($productImageFile) && $imageData['image_location'] == 'Local')
		{
			if ($imageInfo = getimagesize($productImageFile))
			{

				$image  = array(
					'image_id' => $imageData['iid'],
					'pid' => $imageData['pid'],
					'primary' => false,
					'imageFileName' => $productImageFile,
					'imageFileSize' => filesize($productImageFile),
					'width' => isset($imageInfo[0]) ? $imageInfo[0] : false,
					'height' => isset($imageInfo[1]) ? $imageInfo[1] : false,
					'imageThumbFileName' => is_file($productImageThumb) ? $productImageThumb : false,
					'imagePreviewFileName' => false,
					'caption' => $imageData['caption'],
					'altText' => $imageData['alt_text'],
					'priority' => $imageData['image_priority'],
					'url' => $this->settings->get('GlobalHttpUrl').'/'.$productImageFile,
					'location' => $imageData['image_location'],
					'filename' => $imageData['filename'],
					'type' => $imageData['type']

				);
			}
		}
		else if ($imageData['image_location'] == 'Web')
		{
			$image  = array(
				'image_id' => $imageData['iid'],
				'pid' => $imageData['pid'],
				'primary' => false,
				'imageFileName' => $imageData['image_url'],
				'imageFileSize' => false,
				'width' => false,
				'height' => false,
				'imageThumbFileName' => false,
				'imagePreviewFileName' => false,
				'caption' => $imageData['caption'],
				'altText' => $imageData['alt_text'],
				'priority' => $imageData['image_priority'],
				'url' => $imageData['image_url'],
				'location' => $imageData['image_location'],
				'filename' => $imageData['filename'],
				'type' => $imageData['type']
			);
		}

		return $image;
	}

	/**
	 * Get secondary image data
	 *
	 * @param int $secondaryImageId
	 * @return array|bool|null
	 */
	public function getSecondaryImageById($secondaryImageId = 0)
	{
		$db = $this->db;
		$result = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'products_images WHERE iid=' . intval($secondaryImageId));
		if ($result)
		{
			return $this->getSecondaryImage($result);
		}
		return false;
	}

	/**
	 * @param $productDbId
	 * @param $imageId
	 * @param $imageData
	 * @return bool
	 */
	public function updateImage($productDbId, $imageId, $imageData)
	{
		$db = $this->db;

		$productData = $db->selectOne('SELECT product_id FROM ' . DB_PREFIX . 'products WHERE pid=' . intval($productDbId));

		if ($productData)
		{
			if ($imageId == 'primary')
			{
				$db->reset();
				$db->assignStr('image_alt_text', isset($imageData['altText']) ? $imageData['altText'] : '');
				$db->update(DB_PREFIX . 'products', 'WHERE pid=' . intval($productDbId));
				return true;
			}
			else
			{
				$db->reset();
				$db->assignStr('alt_text', isset($imageData['altText']) ? $imageData['altText'] : '');
				$db->assignStr('caption', isset($imageData['caption']) ? $imageData['caption'] : '');
				$db->assignStr('image_priority', isset($imageData['priority']) ? intval($imageData['priority']) : 0);

				if (isset($imageData['image_location']) && !empty($imageData['image_location']))
				{
					$db->assignStr('image_location', $imageData['location']);
				}

				if (isset($imageData['imageUrl']) && !empty($imageData['imageUrl']))
				{
					$db->assignStr('image_url', $imageData['imageUrl']);
				}

				$db->update(DB_PREFIX . 'products_images', 'WHERE iid=' . intval($imageId));
				return true;
			}
		}

		return false;
	}

	/**
	 * @param $productDbId
	 * @param $imageId
	 * @return bool
	 */
	public function deleteImage($productDbId, $imageId)
	{
		$db = $this->db;

		$productData = $db->selectOne('SELECT product_id FROM ' . DB_PREFIX . 'products WHERE pid=' . intval($productDbId));

		if ($productData)
		{
			$imageFileName = ImageUtility::imageFileName($productData['product_id']);

			if ($imageId == 'primary')
			{
				foreach ($this->extensions as $extension)
				{
					@unlink($this->productImagesDir . '/' . $imageFileName . '.' . $extension);
					@unlink($this->productThumbsDir . '/' . $imageFileName . '.' . $extension);
					@unlink($this->productPreviewDir . '/' . $imageFileName . '.' . $extension);
				}

				$db->reset();
				$db->assignStr('image_location', 'Local');
				$db->assignStr('image_url', "");
				$db->assignStr('image_alt_text', "");
				$db->update(DB_PREFIX . 'products', 'WHERE pid=' . intval($productDbId));

				return true;
			}
			else
			{
				$imageData = $productData = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'products_images WHERE pid=' . intval($productDbId) . ' AND iid=' . intval($imageId));

				if ($imageData)
				{
					@unlink($this->productSecondaryImagesDir . '/' . $imageData['filename'] . '.' . $imageData['type']);
					@unlink($this->productSecondaryImagesThumbsDir . '/' . $imageData['filename'] . '.' . $imageData['type']);

					$this->deleteSecondaryImage($imageId);
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Delete secondary image
	 *
	 * @param $imageId
	 */
	public function deleteSecondaryImage($imageId)
	{
		$db = $this->db;
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_images WHERE iid=' . intval($imageId));
	}

	/**
	 * @param $productDbId
	 * @param $oldProductId
	 * @param $newProductId
	 */
	public function renameProductImages($productDbId, $oldProductId, $newProductId)
	{
		$db = $this->db;

		$oldImageFileName = ImageUtility::imageFileName($oldProductId);
		$newImageFileName = ImageUtility::imageFileName($newProductId);

		/**
		 * Primary image
		 */
		foreach ($this->extensions as $extension)
		{
			// remove possibly existing images
			@unlink($newImageFile = $this->productImagesDir.'/'.$newImageFileName.'.'.$extension);
			@unlink($newThumbFile = $this->productThumbsDir.'/'.$newImageFileName.'.'.$extension);
			@unlink($newPreviewFile = $this->productPreviewDir.'/'.$newImageFileName.'.'.$extension);

			// rename images
			@rename($this->productImagesDir.'/'.$oldImageFileName.'.'.$extension, $newImageFile);
			@rename($this->productThumbsDir.'/'.$oldImageFileName.'.'.$extension, $newThumbFile);
			@rename($this->productPreviewDir.'/'.$oldImageFileName.'.'.$extension, $newImageFile);
		}

		/**
		 * Secondary images
		 */
		$counter = 1;
		$result = $db->query('SELECT iid, filename, type FROM '.DB_PREFIX.'products_images WHERE pid='.intval($productDbId).' ORDER BY image_priority DESC, iid');
		while ($imageData = $db->moveNext($result))
		{
			$imageId = $imageData['iid'];
			$filename = $imageData['filename'];
			$extension = $imageData['type'];

			@unlink($newImageFile = $this->productSecondaryImagesDir.'/'.$newImageFileName.'-'.$counter.'.'.$extension);
			@unlink($newThumbFile = $this->productSecondaryImagesThumbsDir.'/'.$newImageFileName.'-'.$counter.'.'.$extension);

			@rename($this->productSecondaryImagesDir.'/'.$filename.'.'.$extension, $newImageFile);
			@rename($this->productSecondaryImagesThumbsDir.'/'.$filename.'.'.$extension, $newThumbFile);

			$db->query('UPDATE '.DB_PREFIX.'products_images SET filename="'.$db->escape($newImageFileName.'-'.$counter).'" WHERE pid='.intval($productDbId).' AND iid='.intval($imageId));

			$counter++;
		}
	}

	/**
	 * Change primary image into secondary image
	 *
	 * @param $productDbId
	 * @param $secondaryImageData
	 * @return bool
	 */
	public function makePrimaryImageSecondary($productDbId, $secondaryImageData)
	{
		$db = $this->db;

		$productData = $db->selectOne('SELECT product_id, image_location, image_url, image_alt_text FROM ' . DB_PREFIX . 'products WHERE pid=' . intval($productDbId));
		if ($productData)
		{
			$productId = $productData['product_id'];

			// set primary as secondary image
			$primaryImage = $this->getPrimaryImage($productDbId, $productId, $productData['image_alt_text'], $productData['image_location'], $productData['image_url']);

			if ($primaryImage)
			{
				if ($primaryImage['location'] == 'Local')
				{
					// store image file
					$tmpFileName = uniqid('tpm-image-', rand(0, 10000));
					@rename($primaryImage['imageFileName'], $tmpFileName);

					$imageInfo = getimagesize($tmpFileName);
					$imageType = $imageInfo[2];

					if (in_array($imageType, $this->acceptedTypes))
					{
						$imageExtension = $this->typesExtensions[$imageType];
						$secondaryImageFile = $this->getNextSecondaryImageName($productDbId, $productId);

						$imageFilePath = $this->productSecondaryImagesDir . '/' . $secondaryImageFile . '.' . $imageExtension;
						@rename($tmpFileName, $imageFilePath);
						@chmod($imageFilePath, FileUtils::getFilePermissionMode());

						ImageUtility::generateProductSecondaryThumb($imageFilePath, $this->productSecondaryImagesThumbsDir . '/' . $secondaryImageFile . '.' . $imageExtension);

						$db->reset();
						$db->assignStr('width', intval($imageInfo[0]));
						$db->assignStr('height', intval($imageInfo[1]));
						$db->assignStr('type', $imageExtension);
						$db->assignStr('filename', $secondaryImageFile);
						$db->assignStr('caption', $productData['image_alt_text']);
						$db->assignStr('alt_text', $productData['image_alt_text']);
						$db->assignStr('image_url', "");
						$db->assignStr('image_location', 'Local');
						$db->update(DB_PREFIX . 'products_images', 'WHERE pid=' . intval($productDbId) . ' AND iid=' . intval($secondaryImageData['image_id']));

						return true;
					}
				}
				else if ($primaryImage['location'] == 'Web')
				{
					$db->reset();
					$db->assignStr('width', 0);
					$db->assignStr('height', 0);
					$db->assignStr('type', "");
					$db->assignStr('filename', "");
					$db->assignStr('caption', $productData['image_alt_text']);
					$db->assignStr('alt_text', $productData['image_alt_text']);
					$db->assignStr('image_url', $productData['image_url']);
					$db->assignStr('image_location', 'Web');
					$db->update(DB_PREFIX . 'products_images', 'WHERE pid=' . intval($productDbId) . ' AND iid=' . intval($secondaryImageData['image_id']));

					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Change secondary image into primary
	 *
	 * @param $productDbId
	 * @param $productId
	 * @param $secondaryImageData
	 * @return bool
	 */
	public function makeSecondaryImagePrimary($productDbId, $productId, $secondaryImageData)
	{
		if ($secondaryImageData)
		{
			$this->addPrimaryImage(
				$productDbId,
				$productId,
				array(
					'error' => UPLOAD_ERR_OK,
					'tmp_name' => $this->productSecondaryImagesDir . '/' . $secondaryImageData['filename'] . '.' . $secondaryImageData['type']
				),
				array(
					'image_alt_text' => $secondaryImageData['altText'],
					'image_location' => $secondaryImageData['location'],
					'image_url' => $secondaryImageData['url']
				)
			);
			return true;
		}
		return false;
	}

	/**
	 * @param $productId
	 * @param $newProductId
	 * @param $copyPrefix
	 */
	public function copyProductImages($productId, $newProductId, $copyPrefix='copy')
	{
		/**
		 * Set our primary image
		 */
		$db = $this->db;
		$productData = $db->selectOne('SELECT product_id FROM '.DB_PREFIX.'products WHERE pid='.intval($productId));

		$productData['product_id'];
		$copiedProductId = $copyPrefix . '-' . $productData['product_id'] . '-' . $newProductId;

		foreach ($this->extensions as $extension)
		{
			@copy($this->productImagesDir . '/' . $productData['product_id'] . '.' . $extension, $this->productImagesDir . '/' . $copiedProductId . '.' . $extension);
			@copy($this->productThumbsDir . '/' . $productData['product_id'] . '.' . $extension, $this->productThumbsDir . '/' . $copiedProductId . '.' . $extension);
			@copy($this->productPreviewDir . '/' . $productData['product_id'] . '.' . $extension, $this->productPreviewDir . '/' . $copiedProductId . '.' . $extension);
		}

		/**
		 * Secondary images
		 */
		$counter = 1;
		$result = $db->query('SELECT * FROM '.DB_PREFIX.'products_images WHERE pid='.intval($productId).' ORDER BY image_priority DESC, iid');
		while ($imageData = $db->moveNext($result))
		{
			$filename = $imageData['filename'];
			$newFilename = $copyPrefix . '-' . $imageData['filename'] . '-' . $newProductId . '-' . $counter;
			$extension = $imageData['type'];

			@copy($this->productSecondaryImagesDir . '/' . $filename . '.' . $extension, $this->productSecondaryImagesDir . '/' . $newFilename . '.' . $extension);
			@copy($this->productSecondaryImagesThumbsDir . '/' . $filename . '.' . $extension, $this->productSecondaryImagesThumbsDir . '/' . $newFilename . '.' . $extension);

			$sql = 'INSERT INTO ' . DB_PREFIX . 'products_images (pid, filename,' . implode(", ", self::getProductImagesFields(true)) . ') 
				VALUES (' . $newProductId . ', "' . $newFilename . '", "' . $imageData["is_visible"] . '", ' . $imageData["image_priority"] . ', 
				' . $imageData["width"] . ', ' . $imageData["height"] . ', "' . $extension . '", "' . $imageData["caption"] . '", "' . $imageData["alt_text"] . '")';

			$db->query($sql);
			$counter++;
		}
	}

	/**
	 * Returns our product images fields
	 * @param bool $fieldsOnly
	 * @return array
	 */
	public static function getProductImagesFields($fieldsOnly = false)
	{
		$fields = array(
			array('field' => 'is_visible', 'label' => 'Is Visible'),
			array('field' => 'image_priority', 'label' => 'Image Priority'),
			array('field' => 'width', 'label' => 'Width'),
			array('field' => 'height', 'label' => 'Height'),
			array('field' => 'type', 'label' => 'Type'),
			array('field' => 'caption', 'label' => 'Caption'),
			array('field' => 'alt_text', 'label' => 'Alternate Text'),
		);

		return (!$fieldsOnly) ? $fields : array_column($fields, 'field');
	}

	/**
	 * Save external secondary image
	 *
	 * @param $pid
	 * @param $secondaryImage
	 */
	public function persistWebSecondaryImage($pid, $secondaryImage)
	{
		$db = $this->db;

		$db->reset();
		$db->assign('pid', intval($pid));
		$db->assignStr('is_visible', 'Yes');
		$db->assignStr('image_url', $secondaryImage['image_url']);
		$db->assignStr('alt_text', $secondaryImage['image_alt_text']);
		$db->assignStr('caption', isset($secondaryImage['image_caption']) ? $secondaryImage['image_caption'] : '');
		$db->assignStr('image_priority',isset($secondaryImage['image_priority']) ? $secondaryImage['image_priority'] : 0);
		$db->assignStr('image_location', 'Web');

		$db->insert(DB_PREFIX . 'products_images');
	}

	/**
	 * Save external primary image
	 *
	 * @param $pid
	 * @param $primaryImage
	 */
	public function persistWebPrimaryImage($pid, $primaryImage)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('image_location', "Web");
		$db->assignStr('image_alt_text', isset($primaryImage['image_alt_text']) ? $primaryImage['image_alt_text'] : '');
		$db->assignStr('image_url', isset($primaryImage['image_url']) ? $primaryImage['image_url'] : '');

		$db->update(DB_PREFIX . 'products', 'WHERE pid=' . intval($pid));
	}

	/**
	 * Delete all external secondary images
	 *
	 * @param $pid
	 */
	public function deleteWebSecondaryImages($pid)
	{
		$db = $this->db;
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_images WHERE image_location = "Web" AND pid=' . intval($pid));
	}
}

<?php

/**
 * Class DataAccess_ProductsFeaturesGroupsProductsRelationsRepository
 */
class DataAccess_ProductsFeaturesGroupsProductsRelationsRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $allFilters
	 * @param $narrowFilters
	 * @param $cid
	 * @return array
	 */
	public function getProductsByFiltersAndCategory($allFilters, $narrowFilters, $cid)
	{
		// First Solution
		$records = $this->db->selectAll('
			SELECT pfgpr.feature_group_product_relation_id, pfgpr.product_feature_group_id, pf.product_feature_id, pfgpr.pid, cfg.cid, pfvr.product_feature_option_id
			FROM ' . DB_PREFIX . 'categories_features_groups AS cfg
			INNER JOIN ' . DB_PREFIX . 'products_features_groups_relations AS pfgr ON cfg.product_feature_group_id = pfgr.product_feature_group_id
			INNER JOIN ' . DB_PREFIX . 'products_features AS pf ON pfgr.product_feature_id = pf.product_feature_id
			INNER JOIN ' . DB_PREFIX . 'products_features_values_relations AS pfvr ON pfgr.product_feature_group_relation_id = pfvr.product_feature_group_relation_id
			INNER JOIN ' . DB_PREFIX . 'products_features_groups_products_relations AS pfgpr ON pfvr.feature_group_product_relation_id = pfgpr.feature_group_product_relation_id
			WHERE cfg.cid = ' . $cid . ' AND pfvr.product_feature_option_id IN (' . implode(',', $allFilters) . ')'
		);

		$filterProducts = array();
		$productsWithFilters = array();
		foreach ($records as $key => $record)
		{
			$productsWithFilters[$record['pid']][$record['product_feature_id']][] = $record['product_feature_option_id'];
		}

		foreach ($productsWithFilters as $pid => $productWithFilters)
		{
			if (count(array_intersect_key($productWithFilters, $narrowFilters)) == count($narrowFilters))
			{
				$validFilters = true;
				foreach ($narrowFilters as $productFeatureId => $filters)
				{
					if (count(array_intersect($productWithFilters[$productFeatureId], $filters)) == 0)
					{
						$validFilters = false;
						break;
					}
				}

				if ($validFilters)
				{
					$filterProducts[] = $pid;
				}
			}
		}

		return $filterProducts;
	}

	/**
	 * Persist
	 *
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		$db->reset();
		$db->assignStr('product_feature_group_id', $data['product_feature_group_id']);
		$db->assignStr('pid', $data['pid']);
		return $db->insert(DB_PREFIX . 'products_features_groups_products_relations');
	}

	/**
	 * @param $pid
	 * @param $productsFeaturesGroups
	 */
	public function updateProductsFeaturesGroups($pid, $productsFeaturesGroups)
	{
		$db = $this->db;

		// get current ones
		$currentFamilies = array();
		$result = $db->query('SELECT product_feature_group_id FROM ' . DB_PREFIX . 'products_features_groups_products_relations WHERE pid=' . intval($pid));
		while ($c = $db->moveNext($result)) $currentFamilies[] = $c['product_feature_group_id'];

		// insert new ones
		$updatedFamilies = array();
		foreach ($productsFeaturesGroups as $productFeatureGroupId)
		{
			$productFeatureGroupId = intval($productFeatureGroupId);

			if (!in_array($productFeatureGroupId, $currentFamilies))
			{
				$this->persist(array('pid' => $pid, 'product_feature_group_id' => $productFeatureGroupId));
			}

			$updatedFamilies[] = $productFeatureGroupId;
		}

		// remove families that aren't reassigned
		$deletedFamilies = array();
		foreach ($currentFamilies as $currentFamilyId)
		{
			if (!in_array($currentFamilyId, $updatedFamilies))
			{
				$deletedFamilies[] = $currentFamilyId;
			}
		}

		if (count($deletedFamilies) > 0)
		{
			$this->delete($pid, $deletedFamilies);
		}
	}

	/**
	 * Delete currently assigned product feature group in product
	 * @param $pid
	 * @params $productFeatureGroupId
	 * @return bool
	 */
	public function delete($pid, $productFeatureGroupId)
	{
		$db = $this->db;

		$ids = is_array($productFeatureGroupId) ? $productFeatureGroupId : array($productFeatureGroupId);

		if (count($ids) == 0) return false;

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features_values_relations 
			WHERE feature_group_product_relation_id
			IN(
				SELECT feature_group_product_relation_id
				FROM ' . DB_PREFIX . 'products_features_groups_products_relations
				WHERE pid = ' . intval($pid) . ' AND product_feature_group_id IN(' . implode(',', $ids) . ')
			)'
		);

		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features_groups_products_relations WHERE pid = ' . intval($pid) . ' AND product_feature_group_id IN(' . implode(',', $ids). ')');

		return true;
	}
}
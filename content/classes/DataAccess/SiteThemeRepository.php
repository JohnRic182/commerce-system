<?php

/**
* DataAccess_SiteThemeRepository
*/
class DataAccess_SiteThemeRepository extends DataAccess_Repository
{
	/**
	 * undocumented function
	 *
	 * @param string $themesFolderPath 
	 * @return array An array of SiteTheme instances
	 */
	public function getInstalledThemes($themesFolderPath = null)
	{
		static $themesCollection = null;
		
		if (is_null($themesFolderPath))
		{
			$themesFolderPath = 'content'.DS.'skins';
		}
		
		if (is_null($themesCollection))
		{
			$themesCollection = array();
			
			
			if (($fh = opendir($themesFolderPath)) !== false)
			{
				$restricted = array('.', '..', '.svn');

				while (($theme_name = readdir($fh)) !== false)
				{
					if (is_dir($themesFolderPath.DS.$theme_name) && file_exists($themesFolderPath.DS.$theme_name.DS.'skin.xml'))
					{
						// Read the XML skin file into the array
						$xml = simplexml_load_string(file_get_contents($themesFolderPath.DS.$theme_name.DS.'skin.xml'), 'SimpleXMLElement', LIBXML_NOCDATA);
						
						if (!array_key_exists((string) $xml->name, $themesCollection))
						{
							$theme = new SiteTheme();
							$theme->setName((string) $xml->name);
							$theme->setTitle((string) $xml->title);
							$theme->setAuthor((string) $xml->author);
							$theme->setThumbnailImage((string) $xml->thumbnail);
							$theme->setFullsizeImage((string) $xml->preview);

							if ($xml->images && $xml->images->children())
							{
								foreach ($xml->images->children() as $image)
								{
									$attributes = $image->attributes();
									$title = (isset($attributes['title'])) ? (string) $attributes['title'] : '';
									$theme->addImage((string) $image, $title);
								}
							}

							$themesCollection[$theme->getName()] = $theme;
						}
					}
				}

				closedir($fh);
			}
		}
		
		return $themesCollection;
	}
}

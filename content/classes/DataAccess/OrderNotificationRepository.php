<?php

/**
 * Class DataAccess_OrderNotificationCampaingRepository
 */
class DataAccess_OrderNotificationRepository extends DataAccess_BaseRepository
{
	protected $settings;
	
	public function __construct(DB $db, $settings)
	{
		if ( $settings )
		{
			$this->settings = $settings;
		}
		parent::__construct($db);
	}

	/**
	 * Get defaults
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'name' => '',
			'code' => '',
			'contact_name' => '',
			'notify_email' => '',
			'location_type' => 'drop-shipper',
			'is_active' => 'No',
			'email_text' => ''
		);
	}

	/**
	 * Get order notification by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne(
			"SELECT * FROM ".DB_PREFIX."products_locations WHERE products_location_id=".intval($id)
		);
	}

	/**
	 * Get order notifications count
	 *
	 * @return mixed
	 */
	public function getCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX."products_locations");
		return intval($result['c']);
	}

	/**
	 * Get order notifications list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';
		return $this->db->selectAll("
			SELECT *
			FROM ".DB_PREFIX."products_locations
			ORDER BY ".$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '')
			{
				$errors['name'] = array('Name is empty');
			}
			if (trim($data['code']) == '')
			{
				$errors['code'] = array('Location ID is empty');
			} else {
				// check that location code is unique
				if (
					($mode == 'add' and $this->db->selectOne("SELECT 1 FROM ".DB_PREFIX."products_locations WHERE code = '" . $this->db->escape(trim($data['code'])) . "'"))
					or
					($mode == 'update' and $this->db->selectOne("SELECT 1 FROM ".DB_PREFIX."products_locations WHERE code = '" . $this->db->escape(trim($data['code'])) . "' AND products_location_id <> " . intval($id)))
				)
				{
					$errors['code'] = array('Location ID must be unique');
				}
			}
			if (trim($data['contact_name']) == '')
			{
				$errors['contact_name'] = array('Contact Name is empty');
			}
			if (trim($data['notify_email']) == '')
			{
				$errors['notify_email'] = array('Email is empty');
			} elseif (!isEmail(trim($data['notify_email']))) {
				$errors['notify_email'] = array('Invalid email address');
			}
		}
		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist order notification
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['location_id']) ? $data['location_id'] : null);

		$db->reset();
		$db->assignStr('name', trim($data['name']));
		$db->assignStr('code', trim($data['code']));
		$db->assignStr('contact_name', trim($data['contact_name']));
		$db->assignStr('notify_email', trim($data['notify_email']));
		$db->assignStr('location_type', trim($data['location_type']));
		$db->assign('is_active', isset($data['is_active']) && $data['is_active'] == 'Yes' ? 1 : 0);
		$db->assignStr('email_text', trim($data['email_text']));
		
		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products_locations', 'WHERE products_location_id='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			return $db->insert(DB_PREFIX.'products_locations');
		}
	}

	/**
	 * Delete order notification(s) by id
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);
		foreach ($ids as $key=>$value) $ids[$key] = intval($value);
		if (count($ids) < 1) return;
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_locations WHERE products_location_id IN('.implode(',', $ids).')');
	}

	/**
	 * Delete all order notifications
	 *
	 */
	public function deleteBySearchParams()
	{
		$this->db->query('DELETE FROM '.DB_PREFIX. 'products_locations');
	}

	/**
	 * get options for the "Type" drop-down
	 */
	public function getTypeOptions()
	{
		$options = array();
		$db_field_desc = $this->db->selectOne("SHOW COLUMNS FROM ". DB_PREFIX."products_locations WHERE Field = 'location_type'" );
		if ($db_field_desc)
		{
			if (preg_match('/^enum\((.*)\)$/', $db_field_desc['Type'], $m))
			{
				foreach( explode(',', $m[1]) as $value)
				{
					$type = trim($value, "'");
					$options[$type] = ucfirst($type);
				}
			}
		}
		return $options;
	}

	/**
	 * Get Settings
	 *
	 * @return array
	 */
	public function getAdvancedSettingsData()
	{
		return $this->settings;
	}

	/**
	 * Get validation errors for advanced settings form
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationSettingsErrors($data, $mode, $id = null)
	{
		return false;
	}

	/**
	 * Get options for the "Notify When" drop-down on advanced settings form
	 *
	*/
	function getNotifyWhenOptions()
	{
		return array(
			'admin' => 'Order status set to Completed',
			'user' => 'Payment is Received'
		);
	}

	/**
	 * Persist Advanced Settings for Order Notifications
	 *
	 * @param $data
	 */
	public function persistAdvancedSettings($settings)
	{
		if (is_array($settings) && count($settings) > 0)
		{
			$settingsRepository = new DataAccess_SettingsRepository($this->db, $this->settings);
			$settingsRepository->save($settings);
			return true;
		}
		return false;
	}

}
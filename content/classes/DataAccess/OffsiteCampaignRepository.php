<?php

class DataAccess_OffsiteCampaignRepository extends DataAccess_BaseRepository
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;

	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		if ( $settings )
		{
			$this->settings = $settings;
		}
		parent::__construct($db);
	}

	/**
	 * Get defaults
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($type)
	{
		return array(
			'name' => '',
			'description' => '',
			'type' => $type,
		);
	}

	/**
	 * Get campaign by id
	 *
	 * @param int $id
	 * @param string $type
	 *
	 * @return mixed
	 */
	public function getById($id, $type)
	{
		return $this->db->selectOne(
			"SELECT * FROM ".DB_PREFIX."offsite_campaigns WHERE type='".$this->db->escape($type)."' AND campaign_id=".intval($id)
		);
	}

	/**
	 * Get campaigns count
	 *
	 * @param string $type
	 *
	 * @return mixed
	 */
	public function getCount($type)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX."offsite_campaigns WHERE type='".$this->db->escape($type)."'");
		return intval($result['c']);
	}

	/**
	 * Get campaigns list
	 *
	 * @param string $type
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getList($type, $start = null, $limit = null, $orderBy = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';
		return $this->db->selectAll("
			SELECT *, DATE_FORMAT(created, '" .$this->settings->get('LocalizationDateTimeFormat'). "') AS created
			FROM ".DB_PREFIX."offsite_campaigns
			WHERE type='".$this->db->escape($type)."' ORDER BY ".$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '')
			{
				$errors['name'] = array(trans('apps.widget.required_name'));
			}

			if (trim($data['description']) == '')
			{
				$errors['description'] = array(trans('apps.widget.required_description'));
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist campaigns
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['campaign_id']) ? $data['campaign_id'] : null);

		$db->reset();
		$db->assignStr('name', trim($data['name']));
		$db->assignStr('description', trim($data['description']));

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != ''){
			$db->update(DB_PREFIX.'offsite_campaigns', 'WHERE campaign_id='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			$this->db->assignStr('type', trim($data['type']));
			$this->db->assign('created', 'NOW()');
			return $db->insert(DB_PREFIX.'offsite_campaigns');
		}
	}

	/**
	 * Delete campaign(s) by id
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		$this->db->query('DELETE FROM '.DB_PREFIX.'offsite_campaigns WHERE campaign_id IN('.implode(',', $ids).')');
	}


	/**
	 * Delete campaigns by search results
	 *
	 * @param $type
	 */
	public function deleteBySearchParams($type)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX. "offsite_campaigns WHERE type = '".$this->db->escape($type)."'");
	}
}
<?php
/**
 * Class DataAccess_ProductsFeaturesGroupsRepository
 */
class DataAccess_ProductsFeaturesGroupsRepository extends DataAccess_BaseRepository
{
	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'feature_group_name' => '',
			'feature_group_id' => '',
			'feature_group_active' => 0
		);
	}

	/**
	 * Get product features by id
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'products_features_groups WHERE product_feature_group_id="' . intval($id) . '"');
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 * @param string $logic
	 *  @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$where = array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		if (isset($searchParams['feature_group_active']) && $searchParams['feature_group_active'] <> 'any')
		{
			if ($searchParams['feature_group_active'] == "No") $where[] = 'pfg.feature_group_active = 0';
			if ($searchParams['feature_group_active'] == "Yes") $where[] = 'pfg.feature_group_active = 1';

		}

		$searchStr = trim($searchParams['search_str']);
		if (isset($searchParams['search_str']) && $searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$where[] = '(pfg.feature_group_name like "%' . $searchStr . '%" OR pfg.feature_group_id like "%' . $searchStr . '%")';
		}

		return count($where) > 0 ? ' WHERE ' . implode( ' ' . $logic . ' ', $where) . ' ' : '';
	}

	/**
	 * Get products features groups count
	 *
	 * @param array|null $searchParams
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS feature_group_count  FROM ' . DB_PREFIX . 'products_features_groups pfg' . $this->getSearchQuery($searchParams, $logic));

		return intval($result['feature_group_count']);
	}

	/**
	 * Get products features groups list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = '*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'feature_group_name';

		switch($orderBy)
		{
			case 'feature_group_name_asc'  : $orderBy = 'feature_group_name'; break;
			case 'feature_group_name_desc' : $orderBy = 'feature_group_name DESC'; break;
			case 'feature_group_id_asc'  : $orderBy = 'feature_group_id'; break;
			case 'feature_group_id_desc' : $orderBy = 'feature_group_id DESC'; break;
			case 'feature_group_active_asc'  : $orderBy = 'feature_group_active'; break;
			case 'feature_group_active_desc'  : $orderBy = 'feature_group_active DESC'; break;
		}

		return $this->db->selectAll('
			SELECT ' . $columns . '
			FROM ' . DB_PREFIX . 'products_features_groups pfg' . $this->getSearchQuery($searchParams, $logic) . '
			ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			$db = $this->db;
			$result = $db->selectOne('SELECT product_feature_group_id FROM ' . DB_PREFIX . 'products_features_groups WHERE feature_group_id="' . $db->escape($data['feature_group_id']) . '"');

			if ($result)
			{
				if (intval($id) <= 0 || (intval($id) > 0 && $result['product_feature_group_id'] <> intval($id)))
				{
					$errors['feature_group_id'] = array('Feature Group ID must be unique within products features groups');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist products features groups
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		$db->reset();
		$db->assignStr('feature_group_name', $data['feature_group_name']);
		$db->assignStr('feature_group_id', $data['feature_group_id']);
		$db->assignStr('feature_group_active', intval($data['feature_group_active']));

		if (isset($data['id']) && intval($data['id']) > 0)
		{
			$db->update(DB_PREFIX . 'products_features_groups', 'WHERE product_feature_group_id=' . intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX . 'products_features_groups');
		}
	}

	/**
	 * Delete products features groups by ids
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		if (count($ids) == 0) return false;

		/** @var DB $db */
		$db = $this->db;

		// Delete group feature related data in products_features_values_relations
		$db->query('
			DELETE FROM ' . DB_PREFIX . 'products_features_values_relations 
			WHERE feature_group_product_relation_id
			IN(
				SELECT feature_group_product_relation_id
				FROM ' . DB_PREFIX . 'products_features_groups_products_relations
				WHERE product_feature_group_id IN(' . implode(',', $ids) . ')
			)'
		);

		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features_groups_products_relations WHERE product_feature_group_id IN(' . implode(',', $ids) . ')');
		$db->query('DELETE FROM ' . DB_PREFIX . 'categories_features_groups WHERE product_feature_group_id IN(' . implode(',', $ids) . ')');
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features_groups_relations WHERE product_feature_group_id IN(' . implode(',', $ids) . ')');
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_features_groups WHERE product_feature_group_id IN(' . implode(',', $ids) . ')');

		return true;
	}

	/**
	 * Delete products features groups in search results
	 *
	 * @param $searchParams
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams)
	{
		$ids = array();

		$db = $this->db;

		$result = $db->query('SELECT product_feature_group_id FROM ' . DB_PREFIX . 'products_features_groups pfg' . $this->getSearchQuery($searchParams));
		if ($db->numRows($result) < 1) return false;

		while ($productFeatureGroup = $db->moveNext($result))
		{
			$ids[] = $productFeatureGroup['product_feature_group_id'];
		}

		return $this->delete($ids);
	}

	/**
	 * @param $productId
	 * @return mixed
	 */
	public function getProductsFeaturesGroupsByProductId($productId)
	{
		return $this->db->selectAll('
			SELECT *
			FROM ' . DB_PREFIX . 'products_features_groups pfg
			INNER JOIN ' . DB_PREFIX . 'products_features_groups_products_relations pfgpr ON (pfgpr.product_feature_group_id = pfg.product_feature_group_id)
			WHERE pfgpr.pid = ' . intval($productId) . '
			ORDER BY pfg.feature_group_name, pfg.feature_group_id
		');
	}
}
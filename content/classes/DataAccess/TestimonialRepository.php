<?php
/**
 * Class DataAccess_TestimonialRepository
 */
class DataAccess_TestimonialRepository extends DataAccess_BaseRepository
{
	protected $settings = array();
		
	public function __construct(DB $db, $settings){	
		if ( $settings ){
			$this->settings = $settings;
		}
		parent::__construct($db);
	}
	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'id' => '',
			'name' => '',
			'company' => '',
			'rating' => '5',
			'testimonial' => '',
			'status' => 'pending',
		);
	}

	/**
	 * Get testimonial by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'testimonials WHERE id='.intval($id));
	}

	/**
	 * Get testimonials count
	 *
	 * @param null $searchParams
	 * @param string $logic
	 * @return mixed
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('
			SELECT COUNT(*) AS cnt
			FROM '.DB_PREFIX.'testimonials t
			'.$this->getSearchQuery($searchParams, $logic)
		);

		return intval($result['cnt']);
	}

	/**
	 * Get testimonials list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 't.*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';

		switch ($orderBy)
		{
			/**
			 * New order by params
			 */
			case 'name_asc' : $orderBy = 't.name'; break;
			case 'name_desc' : $orderBy = 't.name DESC'; break;
			case 'company_asc' : $orderBy = 't.company'; break;
			case 'company_desc' : $orderBy = 't.company DESC'; break;
			case 'rating_asc' : $orderBy = 't.rating'; break;
			case 'rating_desc' : $orderBy = 't.rating DESC'; break;
			case 'testimonial_asc' : $orderBy = 't.testimonial'; break;
			case 'testimonial_desc' : $orderBy = 't.testimonial DESC'; break;
			case 'date_created_asc' : $orderBy = 't.date_created'; break;
			case 'date_created_desc' : $orderBy = 't.date_created DESC'; break;
			case 'status_asc' : $orderBy = 't.status'; break;
			case 'status_desc' : $orderBy = 't.status DESC'; break;

			default: $orderBy = 't.name';	break;
		}

		return $this->db->selectAll(
				'SELECT ' . $columns . ',
				DATE_FORMAT(t.date_created, "' . $this->settings["LocalizationDateTimeFormat"] . '") AS date_created_formatted
				FROM '.DB_PREFIX.'testimonials AS t 
				' . $this->getSearchQuery($searchParams, $logic) . '
				ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '') {
				$errors['name'] = array('Please enter name');
			}
		
			if (trim($data['testimonial']) == '') {
				$errors['testimonial'] = array('Please enter testimonial');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationSettingsErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (!empty($data['testimonialsSettings']['testimonials_Email_Notify']) && !isEmail(trim($data['testimonialsSettings']['testimonials_Email_Notify'])))
			{
				$errors[] = array('field' => '#field-testimonialsSettings-testimonials_Email_Notify', 'message' => trans('testimonials.no_notification_email'));
			}

			if (!preg_match("/^\d+$/", trim($data['testimonialsSettings']['testimonials_Per_Page'])))
			{
				$errors[] = array('field' => '#field-testimonialsSettings-testimonials_Per_Page', 'message' => trans('testimonials.no_testimonials_per_page'));
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist testimonials
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
	
		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['id']) ? $data['id'] : null);

		$db->reset();
		$db->assignStr('name', $data['name']);
		$db->assignStr('company', $data['company']);
		$db->assign('rating', $data['rating']);
		$db->assignStr('testimonial', $data['testimonial']);
		$db->assignStr('status', $data['status']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != ''){
			$db->update(DB_PREFIX.'testimonials', 'WHERE id='.intval($data['id']));
			return intval($data['id']);
		}
		else{
			$this->db->assign("date_created", "NOW()");
			return $db->insert(DB_PREFIX.'testimonials');
		}
	}

	/**
	 * Delete testimonials by ids
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		// delete testimonials
		$this->db->query('DELETE FROM '.DB_PREFIX.'testimonials WHERE id IN('.implode(',', $ids).')');
	}

	/**
	 * Delete testimonials by search results
	 *
	 * @param $searchParams
	 */
	public function deleteBySearchParams($searchParams)
	{
		$searchQuery = $this->getSearchQuery($searchParams);

		$ids = array();

		$result = $this->db->query('SELECT id FROM ' . DB_PREFIX . 'testimonials' . $searchQuery);

		if ($this->db->numRows($result) < 1) return;

		while ($testimonial = $this->db->moveNext($result)){
			$ids[] = $testimonial['id'];
		}

		// delete pages
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'testimonials' . $searchQuery);
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 * @param string $logic
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$searchParams = is_array($searchParams) ? $searchParams : array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$where = array();
		$searchStr = trim($searchParams['search_str']);

		if (isset($searchParams['search_str']) && $searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$where[] =
				'((t.name like "%' . $searchStr . '%")' .
				'OR (t.company like "%' . $searchStr . '%")' .
				'OR (t.rating like "%' . $searchStr . '%")' .
				'OR (t.status like "%' . $searchStr . '%"))';
		}

		return (count($where) > 0 ? 'WHERE (' . implode(' '  . $logic . ' ', $where) . ') ' : '') . ' ';
	}

	/**
	 * Update testimonial status (all or selected)
	 * @param $id
	 */	
	public function updateStatus($ids, $new_status) {
		if (preg_match("/^(pending|approved|declined)/", $new_status)) {
			if ($ids == 'all') {
				$where = " 1=1 ";
			}
			else {
				$ids = is_array($ids) ? $ids : array($ids);
				foreach ($ids as $key=>$value) $ids[$key] = intval($value);
				if (count($ids) < 1) return;
				$where = " id IN(".implode(',', $ids).") ";
			}
			$this->db->reset();
			$this->db->assignStr('status', $new_status);
			$this->db->update(DB_PREFIX."testimonials", "WHERE " . $where);
		}
	}

	/**
	 * Get Tax Rate  Settings
	 *
	 * @return array
	 */
	public function getAdvancedSettingsData(){
		return $this->settings;
	}

	/**
	 * Persis Tax Tax Settings
	 *
	 * @param $data
	 */
	public function persistTestimonialsSettings($data)
	{
		if ( isset($data['testimonialsSettings']) ){
			$testimonialsSettings = $data['testimonialsSettings'];

			$testimonialsSettings["testimonials_Enabled"] = isset($testimonialsSettings["testimonials_Enabled"]) ? $testimonialsSettings["testimonials_Enabled"]: "No";
			$testimonialsSettings["testimonials_Use_Rating"] = isset($testimonialsSettings["testimonials_Use_Rating"]) ? $testimonialsSettings["testimonials_Use_Rating"]: "No";
			$testimonialsSettings["testimonials_Use_Captcha"] = isset($testimonialsSettings["testimonials_Use_Captcha"]) ? $testimonialsSettings["testimonials_Use_Captcha"]: "No";

			if (is_array($testimonialsSettings) && count($testimonialsSettings) > 0)
			{
				$settingsRepository = new DataAccess_SettingsRepository($this->db, $this->settings);
				$settingsRepository->save($testimonialsSettings);
				return true;
			}
		}
		return false;
	}

	/**
	 * Get Average Rating Score
	 *
	 * @return int
	 */
	public function getAverageRating()
	{
		$result = $this->db->selectOne("SELECT AVG(rating) AS `rating` FROM ".DB_PREFIX."testimonials WHERE status = 'approved' AND rating > 0");

		return intval($result['rating']);
	}
}
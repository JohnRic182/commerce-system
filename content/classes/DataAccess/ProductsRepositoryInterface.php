<?php
/**
 * Interface DataAccess_ProductsRepositoryInterface
 */
interface DataAccess_ProductsRepositoryInterface
{
	public function getProductByProductId($productId);

	public function getProductsInventory($pid, $inventoryList);

	public function getProductsAttributes($pid);

	public function getInventoryStock($inventoryId);

	public function getProductsQuantityDiscounts($pid, $quantity);
}
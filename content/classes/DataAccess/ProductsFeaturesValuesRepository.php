<?php

/**
 * Class DataAccess_ProductsFeaturesValuesRepository
 */
class DataAccess_ProductsFeaturesValuesRepository extends DataAccess_BaseRepository
{
	public function getDefaults()
	{
		return array(
			'product_feature_option_id' => '',
			'product_feature_option_value' => ''
		);
	}

	/**
	 * @param $searchParams
	 * @param string $logic
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$where = array();
		$searchParams = is_array($searchParams) ? $searchParams : array();
		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		if (isset($searchParams['product_feature_option_value']) && trim($searchParams['product_feature_option_value']) != '')
		{
			$where[] = 'pfv.product_feature_option_value like "%' . $this->db->escape(trim($searchParams['product_feature_option_value'])) . '%"';
		}

		if (isset($searchParams['product_feature_option_id']) && trim($searchParams['product_feature_option_id']) != '')
		{
			$where[] = 'pfv.product_feature_option_id like "%' . $this->db->escape(trim($searchParams['product_feature_option_id'])) . '%"';
		}

		return count($where) > 0 ? ' WHERE ' . implode( ' ' . $logic . ' ', $where) . ' ' : '';
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = '*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'product_feature_option_value, product_feature_option_id';

		switch($orderBy)
		{
			case 'product_feature_option_value_asc' : $orderBy = 'product_feature_option_value'; break;
			case 'product_feature_option_value_desc' : $orderBy = 'product_feature_option_value DESC'; break;
			case 'product_feature_option_id_asc' : $orderBy = 'product_feature_option_id'; break;
			case 'product_feature_option_id_desc' : $orderBy = 'product_feature_option_id DESC'; break;
		}

		return $this->db->selectAll('
			SELECT ' . $columns . '
            FROM ' . DB_PREFIX . 'products_features_values AS pfv ' . $this->getSearchQuery($searchParams, $logic) . '
            ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * @param array $categoriesFeaturesGroups
	 * @param array $narrowFilters
	 * @return array
	 */
	public function getProductsFeaturesValues($categoriesFeaturesGroups = array(), $narrowFilters = array())
	{
		$sortedProductsFeatures = array();
		if (!empty($categoriesFeaturesGroups))
		{
			$productFeatureGroupRelationIds = array();
			foreach ($categoriesFeaturesGroups as $key => $categoriesFeaturesGroup)
			{
				$productFeatureGroupRelationIds[] = $this->db->escape(trim($categoriesFeaturesGroup['product_feature_group_relation_id']));
			}

			$sortedProductsFeatures = $this->db->selectAll(
				'SELECT DISTINCT pf.product_feature_id ,pf.feature_name, pf.feature_id ' .
				'FROM ' . DB_PREFIX . 'categories_features_groups AS cfg ' .
				'INNER JOIN ' . DB_PREFIX . 'products_features_groups_relations AS pfgr ON cfg.product_feature_group_id = pfgr.product_feature_group_id ' .
				'INNER JOIN ' . DB_PREFIX . 'products_features AS pf ON pfgr.product_feature_id = pf.product_feature_id ' .
				'INNER JOIN ' . DB_PREFIX . 'products_features_groups AS pfg ON pfgr.product_feature_group_id = pfg.product_feature_group_id ' .
				'WHERE pfgr.product_feature_group_relation_id IN (' . implode(',', $productFeatureGroupRelationIds) . ') ' .
				'AND pfg.feature_group_active = 1 AND pf.feature_active = 1 ' .
				'ORDER BY cfg.priority, pfgr.priority'
			);

			$productsFeaturesValues = $this->db->selectAll(
				'SELECT DISTINCT pfv.product_feature_option_id, pfv.product_feature_option_value, pf.product_feature_id, pf.feature_id, pf.feature_name ' .
				'FROM ' . DB_PREFIX . 'products_features_values AS pfv ' .
				'INNER JOIN ' . DB_PREFIX . 'products_features_values_relations AS pfvr ON pfv.product_feature_option_id = pfvr.product_feature_option_id ' .
				'INNER JOIN ' . DB_PREFIX . 'products_features_groups_relations AS pfgr ON pfvr.product_feature_group_relation_id = pfgr.product_feature_group_relation_id ' .
				'INNER JOIN ' . DB_PREFIX . 'products_features AS pf ON pfgr.product_feature_id = pf.product_feature_id ' .
				'WHERE pfvr.product_feature_group_relation_id IN (' . implode(',', $productFeatureGroupRelationIds) . ') AND pf.feature_active = 1 ' .
				'ORDER BY LENGTH(product_feature_option_value), product_feature_option_value'
			);

			foreach($sortedProductsFeatures as $sortedProductFeaturesKey => $sortedProductFeatures)
			{
				foreach ($productsFeaturesValues as $productsFeaturesValueKey => $productsFeaturesValue)
				{
					// Get checked product filter option
					if (!empty($narrowFilters))
					{
						foreach ($narrowFilters as $productFeatureId => $narrowFilter)
						{
							if ($productsFeaturesValue['product_feature_id'] == $productFeatureId && in_array($productsFeaturesValue['product_feature_option_id'], $narrowFilter))
							{
								$productsFeaturesValue['check'] = true;
								break;
							}
							else
							{
								$productsFeaturesValue['check'] = false;
							}
						}
					}

					if ($sortedProductFeatures['product_feature_id'] == $productsFeaturesValue['product_feature_id'])
					{
						unset($productsFeaturesValue['feature_id'], $productsFeaturesValue['feature_name']);
						$sortedProductsFeatures[$sortedProductFeaturesKey]['options'][] = $productsFeaturesValue;
						unset($productsFeaturesValues[$productsFeaturesValueKey]);
					}
				}
			}
		}

		return $sortedProductsFeatures;
	}

	/**
	 * Persist
	 *
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('product_feature_option_value', trim(isset($data['product_feature_option_value']) ? $data['product_feature_option_value'] : (isset($data['new_value']) ? $data['new_value'] : '')));

		return $db->insert(DB_PREFIX . 'products_features_values');
	}

	/**
	 * @param $value
	 * @return null
	 */
	public function getIdByValue($value)
	{
		$db = $this->db;
		$data = $db->selectOne('SELECT product_feature_option_id FROM ' . DB_PREFIX . 'products_features_values WHERE product_feature_option_value LIKE BINARY "' . $db->escape($value). '"');
		return $data && isset($data['product_feature_option_id']) ? $data['product_feature_option_id'] : null;
	}
}
<?php
/**
 * Class DataAccess_GoogleToolsRepository
 */
class DataAccess_GoogleToolsRepository extends DataAccess_BaseRepository
{
	const CONVERSION_CODE_FILE = 'templates/pages/google/conversion_code.html';
	const ADWORDS_CODE_FILE = 'templates/pages/google/adwords_conversion.html';
	const SITEMAP_LOCATION = 'sitemap.xml.gz';

	private $errors = array();

	protected $settings;

	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		parent::__construct($db);

		$this->settings = $settings;
	}

	/**
	 * Get default settings
	 *
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'GoogleAdwordsConversion' => '',
			'GoogleAdwordsConversionActive' => 'NO',
			'GoogleAnalytics' => '',
			'GoogleAnalyticsActive' => 'NO',
			'GoogleConversionActive' => 'NO'
		);
	}

	/**
	 * Get settings
	 */
	public function getSettings($keys)
	{
		$data = $this->settings->getByKeys($keys);

		$data['GoogleConversionTrackingCode'] = readTemplateFile(self::CONVERSION_CODE_FILE);
		$data['CoogleAdwordsTrackingCode'] = readTemplateFile(self::ADWORDS_CODE_FILE);
		return $data;
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationSettingsErrors($data, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (!isset($data['GoogleConversionTrackingCode'])) $errors['GoogleConversionTrackingCode'] = array('Conversion Tracking Code is missing');
			if (!isset($data['CoogleAdwordsTrackingCode'])) $errors['CoogleAdwordsTrackingCode'] = array('Adwords Tracking Code is missing');
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param $data
	 * @param null $params
	 *
	 * @return bool
	 */
	public function persistGoogleToolsSettings($data, $params = null)
	{
		if (is_array($data) && count($data) > 0)
		{
			$this->settings->persist($data, array_keys($this->getDefaults()));

			if (writeTemplateFile(self::CONVERSION_CODE_FILE, $data['GoogleConversionTrackingCode']))
			{
				@chmod(self::CONVERSION_CODE_FILE, FileUtils::getFilePermissionMode());
			}

			if (writeTemplateFile(self::ADWORDS_CODE_FILE, $data['CoogleAdwordsTrackingCode']))
			{
				@chmod(self::ADWORDS_CODE_FILE, FileUtils::getFilePermissionMode());
			}

			return true;
		}
		return false;
	}

	/**
	 * Generate site map, gzip and save in cart's root directory
	 *
	 * @param $settingsArray
	 * @return bool
	 */
	public function generateSiteMap($settingsArray, $download = true)
	{
		$this->errors = array();

		//TODO: This needs refactored out of plugins
		require_once 'content/admin/plugins/google_site_maps/plugin.google_site_maps.php';
		$plugin = new plugin_google_site_maps();
		$params = array('step' => 2);
		$params = array('download' => $download);

		//TODO: review and optimize
		ob_start();
		$plugin->runPlugin($this->db, $settingsArray, $params);
		$sitemap = ob_get_contents();
		ob_end_clean();

		$gzdata = gzencode($sitemap, 9);
		if (!($fp = @fopen(self::SITEMAP_LOCATION, 'w')))
		{
			$this->errors = array('Cannot open ' . self::SITEMAP_LOCATION);
			return false;
		}

		if (!(@fwrite($fp, $gzdata)))
		{
			$this->errors = array('Cannot write to ' . self::SITEMAP_LOCATION);
			return false;
		}

		if ($fp)
		{
			fclose($fp);
		}

		return true;
	}
	
	/*
	 * Get last error, to use with generateSiteMap()
	*/
	public function getErrors() 
	{
		return $this->errors;
	}
}
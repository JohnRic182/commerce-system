<?php
/**
 * Class DataAccess_AdminRepository
 */
class DataAccess_AdminRepository extends DataAccess_BaseRepository
{
	/** @var DataAccess_SettingsRepository */
	protected $settings;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param DataAccess_SettingsRepository $settings
	 */
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		parent::__construct($db);
		$this->settings = $settings;
	}

	/**
	 * Get admins list
	 *
	 * @param $currentAdminId
	 * @param null $start
	 * @param null $limit
	 *
	 * @return mixed
	 */
	public function getList($currentAdminId, $start = null, $limit = null)
	{
		$dateFormat =  $this->settings->get('LocalizationDateTimeFormat');

		return $this->db->selectAll('SELECT aid, username, CONCAT(fname, " ", lname) AS name, active, DATE_FORMAT(last_access, "'.$dateFormat.'") AS last_access FROM '.DB_PREFIX.'admins WHERE aid <> '.intval($currentAdminId).' ORDER BY name'.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : ''));
	}

	/**
	 * Get admins count
	 *
	 * @param bool $includeSupportAdmins
	 *
	 * @return mixed
	 */
	public function getCount($includeSupportAdmins = false)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'admins'.(!$includeSupportAdmins ? ' WHERE rights NOT LIKE "%support%"' : ''));

		return $result['c'];
	}

	/**
	 * Get admin by id
	 *
	 * @param $aid
	 * @return array|bool
	 */
	public function getAdminById($aid)
	{
		return $this->db->selectOne('SELECT *, CONCAT(fname, " ", lname) AS name FROM ' . DB_PREFIX . 'admins WHERE aid=' . intval($aid));
	}

	/**
	 * @param $aid
	 * @param $password
	 * @param $passwordHash
	 * @param $passwordStrength
	 * @param $passwordHistory
	 * @param $securityQuestionId
	 * @param $securityQuestionAnswer
	 */
	public function updatePasswordSecurityQuestion($aid, $password, $passwordHash, $passwordStrength, &$passwordHistory, $securityQuestionId, $securityQuestionAnswer)
	{
		$db = $this->db;

		$db->reset();
		if ($password != '')
		{
			$this->updatePasswordHistory($passwordHistory, $passwordHash, $passwordStrength);

			$db->assignStr('password', $passwordHash);
			$db->assignStr('password_history', base64_encode(serialize($passwordHistory)));
			$db->assign('last_update', 'NOW()');
			$db->assign('password_change_date', 'NOW()');
			$db->assign('force_password_change', 0);
		}

		// update security question
		if ($securityQuestionId != '' && $securityQuestionAnswer != '')
		{
			$db->assignStr('security_question_id', $securityQuestionId);
			$db->assignStr('security_question_answer', getPasswordHash($securityQuestionAnswer));
		}

		$db->update(DB_PREFIX.'admins', 'WHERE aid='.intval($aid));
	}

	/**
	 * @param $adminId
	 * @param $formData
	 * @param $passwordHash
	 * @param $passwordStrength
	 * @param $passwordHistory
	 * @param bool $isProfile
	 * @return int
	 */
	public function save($adminId, $formData, $passwordHash, $passwordStrength, &$passwordHistory, $isProfile = false)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('avatar', $formData['avatar']);
		$db->assignStr('fname', $formData['fname']);
		$db->assignStr('lname', $formData['lname']);
		$db->assignStr('email', $formData['email']);
		$db->assignStr('receive_notifications', $formData['receive_notifications']);

		if (!$isProfile)
		{
			$db->assignStr('username', $formData['username']);
			$db->assignStr('active', $formData['active']);
			$db->assignStr('rights', $formData['rights']);
			$db->assignStr('expires', $formData['expires']);
			$db->assignStr('expiration_date', $formData['expiration_date']);
			$db->assign('force_password_change', $formData['force_password_change'] == 1 ? 1 : 0);
		}

		if ($adminId == 0)
		{
			$db->assign('last_access', 'NOW()');
			$db->assign('last_update', 'NOW()');
			$db->assign('password_change_date', 'NOW()');
			$db->assignStr('password', $passwordHash);
			$db->assignStr('password_history', base64_encode(serialize(array(array('p' => $passwordHash, 's' => $passwordStrength)))));

			return $db->insert(DB_PREFIX.'admins');
		}
		else
		{
			/**
			 * Update password if it is not empty
			 */
			if ($formData['password'] != '')
			{
				// log event
				Admin_Log::log('Password successfully change', Admin_Log::LEVEL_NORMAL, true, $formData['username']);

				$this->updatePasswordHistory($passwordHistory, $passwordHash, $passwordStrength);

				$db->assignStr('password', $passwordHash);
				$db->assignStr('password_history', base64_encode(serialize($passwordHistory)));
				$db->assign('last_update', 'NOW()');
				$db->assign('password_change_date', 'NOW()');
			}

			// update security question
			if ($formData['security_question_id'] != '' && $formData['security_question_answer'] != '')
			{
				// log event
				Admin_Log::log('Security question successfully updated', Admin_Log::LEVEL_NORMAL, true, $formData['username']);

				$db->assignStr('security_question_id', $formData['security_question_id']);
				$db->assignStr('security_question_answer', getPasswordHash($formData['security_question_answer']));
			}

			$db->update(DB_PREFIX.'admins', 'WHERE aid='.intval($adminId));

			return $adminId;
		}
	}

	/**
	 * @param $avatarDir
	 * @param $adminId
	 *
	 * @return bool
	 */
	public function deleteImage($avatarDir, $adminId)
	{
		$fileUtils = FileUtils::getInstance();

		//TODO: Is this always jpg?
		if (file_exists($avatarDir . '/' . $adminId . '.jpg'))
		{
			return $fileUtils->unlink($avatarDir . '/' . $adminId . '.jpg');
		}
		return;
	}

	/**
	 * @param $adminId
	 */
	public function touch($adminId)
	{
		$this->db->query('UPDATE '.DB_PREFIX.'admins SET avatar=NULL, last_update=NOW() WHERE aid='.intval($adminId));
	}

	/**
	 * @param $adminIds
	 * @param null $currentAdminId
	 * @return bool
	 */
	public function deleteAdmin($adminIds, $currentAdminId = null)
	{
		$adminIds = is_array($adminIds) ? $adminIds : array($adminIds);

		if (count($adminIds) == 0) return false;

		$currentAdminId = intval($currentAdminId);
		foreach ($adminIds as $adminId)
		{
			$adminId = intval($adminId);

			if ($adminId == 0 || $adminId == $currentAdminId) continue;

			$this->db->query('DELETE FROM '.DB_PREFIX.'admins WHERE aid = '.$adminId);
		}

		return true;
	}

	/**
	 * @param null $currentAdminId
	 * @return bool
	 */
	public function deleteAll($currentAdminId = null)
	{
		$currentAdminId = intval($currentAdminId);

		$list = null;

		$success = true;
		do
		{
			$list = $this->getList($currentAdminId, 0, 500);

			if (count($list) > 0)
			{
				$adminIds = array();
				foreach ($list as $v)
				{
					$adminIds[] = $v['aid'];
				}

				$result = $this->deleteAdmin($adminIds, $currentAdminId);

				if (!$result) $success = false;
			}
		} while ($list !== null && count($list) > 0);

		return $success;
	}

	/**
	 * Update password history
	 *
	 * @param $passwordHistory
	 * @param $passwordHash
	 * @param $passwordStrength
	 */
	protected function updatePasswordHistory(&$passwordHistory, $passwordHash, $passwordStrength)
	{
		/**
		 * If current password in array, remove it
		 */
		foreach ($passwordHistory as $key=>$value)
		{
			if ($value['p'] == $passwordHash)
			{
				unset($passwordHistory[$key]);
				break;
			}
		}

		/**
		 * and rearrange array keys (like 0,1,3,4 to 0,1,2,3)
		 * @var array
		 */
		$_password_history = array();
		foreach ($passwordHistory as $ph)
		{
			$_password_history[] = $ph;
		}
		$passwordHistory = $_password_history;

		/**
		 * if history already has 4 or more (why??) password - remove last one
		 */
		if (count($passwordHistory) > 3) array_shift($passwordHistory);

		/**
		 * finally add current password
		 */
		$passwordHistory[] = array(
			'p' => $passwordHash,
			's' => $passwordStrength
		);
	}
}

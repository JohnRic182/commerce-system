<?php
/**
 * Class DataAccess_AdminLogRepository
 */
class DataAccess_AdminLogRepository extends DataAccess_BaseRepository
{
	/**
	 * @var DataAccess_SettingsRepository
	 */
	protected $settings;

	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		$this->db = $db;
		$this->settings = $settings;
	}

	/**
	 * Get logs count
	 *
	 * @param array|null $searchParams
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'admins_logs'.$this->getSearchQuery($searchParams));

		return intval($result['c']);
	}

	/**
	 * Get logs list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'manufacturer_name';

		return $this->db->selectAll(
			'SELECT *, log_date as date, DATE_FORMAT(log_date, "'.$this->settings->get('LocalizationDateTimeFormat').'") AS log_date_formatted
			FROM '.DB_PREFIX.'admins_logs '.$this->getSearchQuery($searchParams).' ORDER BY '.str_replace('_', ' ', $orderBy).(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	protected function getSearchQuery($searchParams = null)
	{
		$where = array();

		if (isset($searchParams['mysql_from']) && $searchParams['mysql_from'] != '') $where[] = 'log_date >= \''.$searchParams['mysql_from'].'\'';
		if (isset($searchParams['mysql_to']) && $searchParams['mysql_to'] != '') $where[] = 'log_date <= \''.$searchParams['mysql_to'].'\'';
		if (isset($searchParams['username']) && $searchParams['username'] != '') $where[] = 'username like \''.$this->db->escape($searchParams['username']).'\'';
		if (isset($searchParams['level']) && $searchParams['level'] >= 0) $where[] = 'is_error = '.intval($searchParams['level']);

		return count($where) > 0 ? ' WHERE '.implode(' AND ', $where).' ' : '';
	}
}
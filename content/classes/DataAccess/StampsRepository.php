<?php

/**
 * Stamps.com Repository
 */

class DataAccess_StampsRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $date
	 * @return mixed
	 */
	public function getScanFormsByDate($date)
	{
		$db = $this->db;

		$scanForms = $db->selectAll('SELECT * FROM '.DB_PREFIX.'shipping_scan_forms WHERE scan_form_date="'.$db->escape($date).'" AND scan_form_source="stamps"');

		if ($scanForms)
		{
			foreach ($scanForms as $key => $scanForm)
			{
				$scanForms[$key]['scan_form_url'] = unserialize($scanForms[$key]['scan_form_url']);
			}

			return $scanForms;
		}

		return false;
	}

	/**
	 * @param $scanFormData
	 */
	public function saveScanForm($scanFormData)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('scan_form_source', 'stamps');
		$db->assignStr('scan_form_date', $scanFormData['date']);
		$db->assignStr('scan_form_code', $scanFormData['code']);
		$db->assignStr('scan_form_url', serialize($scanFormData['urls']));
		$db->insert(DB_PREFIX.'shipping_scan_forms');
	}

	/**
	 * @param $orderId
	 * @return mixed
	 */
	public function getLabelsByOrderId($orderId)
	{
		global $settings;

		return $this->db->selectAll('
			SELECT oid, olid, status, url, transaction_id, tracking_number, data,
			DATE_FORMAT(date_created, "'.$settings['LocalizationDateTimeFormat'].'") as date_created
			FROM '.DB_PREFIX.'orders_labels WHERE oid='.intval($orderId).' AND type="stampscom"
			ORDER BY olid desc
		');
	}
}
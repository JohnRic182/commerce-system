<?php
/**
 * Class DataAccess_GiftCertificateRepository
 */
class DataAccess_GiftCertificateRepository extends DataAccess_BaseRepository
{
	const GS_PRODUCTID = 'gift_certificate';
	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'oid' => 0,
			'first_name' => '',
			'last_name' => '',
			'phone' => '',
			'email' => '',
			'gift_amount' => '',
			'from_name' => '',
			'message' => '',
			'voucher' => $mode == Admin_Controller_GiftCertificate::MODE_ADD ? $this->generateVoucher() : '',
			'email_gift' => 0,
			'balance' => 0,
			'ordered_date' => null,
//			'is_active' => 0,
		);
	}

	/**
	 * Get gift certificates count
	 *
	 * @param array|null $searchParams
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'gift_cert'.$this->getSearchQuery($searchParams));

		return intval($result['c']);
	}

	/**
	 * Get gift certificates list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $dateFormat
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $dateFormat = '%m/%d/%Y - %r')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'created_date';

		return $this->db->selectAll(
			'SELECT *, DATE_FORMAT(created_date, "'.$dateFormat.'") as created_date_formatted FROM '.DB_PREFIX.'gift_cert'.$this->getSearchQuery($searchParams).' ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams)
	{
		$where = array();

		$where[] = 'ordered_date IS NOT NULL';

		if (isset($searchParams['balance']))
		{
			switch (strtolower($searchParams['balance']))
			{
				case 'credit': $where[] = 'balance > 0'; break;
				case 'zero': $where[] = 'balance = 0'; break;
				default: break;
			}
		}

		return count($where) > 0 ? ' WHERE '.implode(' AND ', $where).' ' : '';
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['first_name']) == '') $errors['first_name'] = array('Please enter first name');
			if (trim($data['last_name']) == '') $errors['last_name'] = array('Please enter last name');
			if (trim($data['email']) == '') $errors['email'] = array('Please enter email');
			if (trim($data['voucher']) == '') $errors['voucher'] = array('Please enter voucher number');
			if (!isEmail($data['email'])) $errors['email'] = array('Please enter a valid email');

			if ($mode == Admin_Controller_GiftCertificate::MODE_ADD)
			{
				if (trim($data['gift_amount']) == '')
				{
					$errors['gift_amount'] = array('Please enter amount');
				}
				else
				{
					$giftAmount = normalizeNumber($data['gift_amount']);
					if ($giftAmount <= 0)
					{
						$errors['gift_amount'] = array('Please enter a positive amount');
					}
				}
			}

			if ($mode == Admin_Controller_GiftCertificate::MODE_UPDATE)
			{
				if (!is_numeric($data['balance']))
				{
					$errors['balance'] = array('Please enter a valid number');
				}
				$balance = normalizeNumber($data['balance']);
				if (trim($data['balance']) == '' || $balance < 0 )
				{
					$errors['balance'] = array('Please enter a positive balance');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist gift certificate
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = intval(isset($data['id']) ? $data['id'] : 0);

		$db->reset();
		if (isset($data['oid'])) $db->assign('oid', intval($data['oid']));
		if (isset($data['first_name'])) $db->assignStr('first_name', $data['first_name']);
		if (isset($data['last_name'])) $db->assignStr('last_name', $data['last_name']);
		if (isset($data['email'])) $db->assignStr('email', $data['email']);
		if (isset($data['message'])) $db->assignStr('message', $data['message']);
		if (isset($data['gift_amount'])) $db->assignStr('gift_amount', $data['gift_amount']);
		if (isset($data['phone'])) $db->assignStr('phone', $data['phone']);
		if (isset($data['from_name'])) $db->assignStr('from_name', $data['from_name']);
		if (isset($data['email_gift'])) $db->assignStr('email_gift', $data['email_gift']);
		if (isset($data['voucher'])) $db->assignStr('voucher', $data['voucher']);
		if (isset($data['balance'])) $db->assignStr('balance', $data['balance']);
		if (isset($data['ordered_date'])) $db->assignStr('ordered_date', $data['ordered_date']);

		$db->assign('modified_date', 'now()');

		if ($data['id'] > 0)
		{
			if (isset($data['balance'])) $db->assign('balance', $data['balance']);

			$db->update(DB_PREFIX.'gift_cert',  'WHERE id='.intval($data['id']));
			return true;
		}
		else
		{
			$db->assign('created_date', 'now()');

			if (isset($data['gift_amount'])) $db->assign('balance', $data['gift_amount']);

			return $db->insert(DB_PREFIX.'gift_cert');
		}
	}

	/**
	 * Delete gift certificates by ids
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		$this->db->query('DELETE FROM '.DB_PREFIX.'gift_cert WHERE id IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * Delete gift certificates in search results
	 *
	 * @param $searchParams
	 *
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams)
	{
		$searchQuery = $this->getSearchQuery($searchParams);

		$this->db->query('DELETE FROM '.DB_PREFIX.'gift_cert'.$searchQuery);

		return true;
	}

	/**
	 * @param $firstName
	 * @param $lastName
	 * @param $voucher
	 * @return array|bool
	 */
	public function getGiftCertificate($firstName, $lastName, $voucher)
	{
		$db = $this->db;
		return $db->selectOne('SELECT * FROM '.DB_PREFIX.'gift_cert WHERE first_name = "'.$db->escape($firstName).'" AND last_name = "'.$db->escape($lastName).'" AND voucher = "'.$db->escape($voucher).'"');
	}

	/**
	 * @param $id
	 * @return array|bool|null
	 */
	public function getGiftCertificateById($id)
	{
		$id = intval($id);

		if ($id < 1) return null;

		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'gift_cert WHERE id = '.$id);
	}

	/**
	 * @param $oid
	 * @return array|bool|null
	 */
	public function getGiftCertificateByOrderId($oid)
	{
		if (intval($oid) < 1) return null;

		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'gift_cert WHERE oid = '.intval($oid));
	}

	/**
	 * @param $gift_data
	 */
	public function setGiftCertificateOrdered($gift_data)
	{
		$id = isset($gift_data['id']) ? intval($gift_data['id']) : 0;

		if ($id < 1) return;

		$this->db->query('UPDATE '.DB_PREFIX.'gift_cert SET ordered_date = NOW() WHERE id = '.$id);
	}

	/**
	 * Get the details of the gift certificate applied to the order
	 *
	 * @param $oid
	 *
	 * @return array|bool
	 */
	public function getOrderGiftCert($oid)
	{
		$oid = intval($oid);

		if ($oid < 1) return false;

		$order = $this->db->selectOne('SELECT gift_cert_first_name, gift_cert_last_name, gift_cert_voucher, gift_cert_amount, "0" as gift_cert_valid FROM '.DB_PREFIX.'orders WHERE oid = '.$oid);

		if ($order['gift_cert_first_name'] != '' && $order['gift_cert_last_name'] != '' && $order['gift_cert_voucher'] != '')
		{
			$order['gift_cert_valid'] = '1';
		}

		return $order;
	}

	/**
	 * Deduct the amount from the gift certificate
	 *
	 * @param $id
	 * @param $amount
	 *
	 * @return bool
	 */
	public function deductAmount($id, $amount)
	{
		$amount = floatval($amount);
		$id = intval($id);

		if ($amount <= 0 || $id < 1) return false;

		$this->db->query('UPDATE '.DB_PREFIX.'gift_cert SET balance = balance - '.$amount.' WHERE id = '.$id.' AND balance >= '.$amount);

		return $this->db->numRowsAffected() > 0;
	}

	/**
	 * Add a gift certificate to an order
	 *
	 * @param $oid
	 * @param $values
	 *
	 * @return bool
	 */
	public function addGiftCertificate($oid, $values)
	{
		$db = $this->db;

		//TODO: Is this the best place for this?
		if ($product = $db->selectOne('SELECT * FROM '.DB_PREFIX.'products WHERE product_id="gift_certificate" AND is_visible="Yes"'))
		{
			$giftCertData = $this->getGiftCertificateByOrderId($oid);
			if ($giftCertData)
			{
				$lineItemData = $db->selectOne('SELECT * FROM '.DB_PREFIX.'orders_content WHERE oid='.intval($oid).' AND product_id="gift_certificate"');
			}

			/**
			 * Insert / update order content
			 */
			$giftAmount = $values['gift_amount'];

			$db->reset();
			$db->assign('oid', $oid);
			$db->assign('pid', $product['pid']);
			$db->assignStr('price', $giftAmount);
			$db->assignStr('admin_price', $giftAmount);
			$db->assignStr('price_withtax', $giftAmount);
			$db->assign('quantity', '1');
			$db->assign('admin_quantity', '1');
			$db->assignStr('is_taxable', 'No');
			$db->assignStr('free_shipping', 'Yes');
			$db->assignStr('product_id', $product['product_id']);
			$db->assignStr('title', $product['title']);
			$db->assignStr('product_type', 'Virtual');

			if ($giftCertData && $lineItemData)
			{
				$lineItemId = $lineItemData['ocid'];
				$values['id'] = $giftCertData['id'];
				$db->update(DB_PREFIX.'orders_content', 'WHERE ocid='.intval($lineItemId));

			}
			else
			{
				$lineItemId = $db->insert(DB_PREFIX.'orders_content');
			}

			/**
			 * Persist gift certificate itself
			 */
			$voucher = $this->generateVoucher();
			$values['voucher'] = $voucher;
			$this->persist($values);

			return $lineItemId;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Update the gift certificate data on an order
	 *
	 * @param $oid
	 * @param $first_name
	 * @param $last_name
	 * @param $voucher
	 *
	 * @return bool
	 */
	public function updateOrderGiftCertificate($oid, $first_name, $last_name, $voucher)
	{
		if (intval($oid) < 1)
		{
			return false;
		}
		else
		{
			$this->db->reset();
			$this->db->assignStr("gift_cert_first_name", $first_name);
			$this->db->assignStr("gift_cert_last_name", $last_name);
			$this->db->assignStr("gift_cert_voucher", $voucher);

			$this->db->update(DB_PREFIX."orders", "WHERE oid = ".$this->db->escape($oid));

			return true;
		}
	}

	/**
	 * Check gift certificate category & product
	 */
	public function checkGiftCertCategoryAndProduct()
	{
		$changed = false;
		$this->db->query('SELECT * FROM '. DB_PREFIX .'catalog WHERE key_name ="gift_cert"');

		if ($this->db->moveNext())
		{
			$cid = $this->db->col['cid'];
		}
		else
		{
			$this->db->reset();
			$this->db->assign('parent', '0');
			$this->db->assign('level', '1');
			$this->db->assign('priority', '5');
			$this->db->assignStr('is_visible', 'No');
			$this->db->assignStr('key_name', 'gift_cert');
			$this->db->assignStr('name', 'gift_cert');
			$cid = $this->db->insert(DB_PREFIX.'catalog');
			$changed = true;
		}

		$this->db->query('SELECT * FROM '. DB_PREFIX .'products WHERE product_id ="gift_certificate"');
		if ($this->db->moveNext())
		{
			$pid = $this->db->col['pid'];
			if ($cid != $this->db->col['cid'])
			{
				$this->db->reset();
				$this->db->assign('cid', $cid);
				$this->db->update(DB_PREFIX.'products', 'WHERE pid = "' . $pid . '"');
				$changed = true;
			}
		}
		else
		{
			$this->db->reset();
			$this->db->assign('cid', $cid);
			$this->db->assign('added', 'NOW()');
			$this->db->assignStr('product_id', 'gift_certificate');
			$this->db->assignStr('title', 'Gift Certificate');
			$this->db->insert(DB_PREFIX.'products');
			$changed = true;
		}
		return $changed;
	}

	/**
	 * Update the gift certificate amount applied to an order
	 *
	 * @param int $oid
	 * @param float $amount
	 */
	public function updateOrderGiftCertAmountApplied($oid, $amount)
	{
		$this->db->reset();
		$this->db->assign("gift_cert_amount", floatval($amount));
		$this->db->update(DB_PREFIX."orders", "WHERE oid=".intval($oid));
	}

	/**
	 * Generate a voucher number
	 *
	 * @return string
	 */
	public function generateVoucher()
	{
		$voucher = '';
		for ($i=0; $i < 15; $i++) $voucher .= rand(0, 9);
		return $voucher;
	}
}
<?php

/**
 * Class DataAccess_CustomFormRepository
 */
class DataAccess_CustomFormRepository extends DataAccess_BaseRepository
{
	/**
	 * @param null $data
	 * @return Model_CustomForm
	 */
	public function create($data = null)
	{
		return new Model_CustomForm($data);
	}

	/**
	 * Get custom form by id
	 *
	 * @param $id
	 * @param bool $asArray
	 *
	 * @return array|bool|Model_CustomForm|null
	 */
	public function getById($id, $asArray = false)
	{
		$customFormData = $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'custom_forms WHERE id=' . intval($id));

		return $customFormData ? ($asArray ? $customFormData : $this->create($customFormData)) : null;
	}

	/**
	 * Get custom form by id
	 *
	 * @param $code
	 * @param $asArray
	 *
	 * @return array|bool|Model_CustomForm|null
	 */
	public function getByFormId($code, $asArray)
	{
		$customFormData = $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'custom_forms WHERE form_id="' . $this->db->escape($code) . '"');

		return $customFormData ? ($asArray ? $customFormData : $this->create($customFormData)) : null;
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams)
	{
		$where = array();

		$searchStr = trim($searchParams['search_str']);

		if (isset($searchParams['search_str']) && $searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$where[] =
				'((title like "%' . $searchStr . '%")' .
				'OR (form_id like "%' . $searchStr . '%"))';
		}

		if (isset($searchParams['active']))
		{
			switch ($searchParams['active'])
			{
				case 'active' : $where[] = 'is_active=1'; break;
				case 'not_active' :
				case 'notactive' : $where[] = 'is_active=0'; break;
			}
		}

		if (isset($searchParams['title']) && $searchParams['title'] != '') $where[] = 'title LIKE "%' . $this->db->escape($searchParams['title']) . '%"';
		if (isset($searchParams['form_id']) && $searchParams['form_id'] != '') $where[] = 'form_id  LIKE "%' . $this->db->escape($searchParams['form_id']) . '%"';

		return count($where) > 0 ? ' WHERE '.implode(' AND ', $where).' ' : '';
	}

	/**
	 * Get custom forms count
	 *
	 * @param array|null $searchParams
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'custom_forms' . $this->getSearchQuery($searchParams));

		return intval($result['c']);
	}

	/**
	 * Get custom forms list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'title';

		switch ($orderBy)
		{
			case 'title_asc':
				$orderBy = 'title';
				break;
			case 'title_desc':
				$orderBy = 'title DESC';
				break;
			case 'custom_form_id_asc':
				$orderBy = 'form_id, title';
				break;
			case 'custom_form_id_desc':
				$orderBy = 'form_id DESC, title DESC';
				break;
			case 'is_active_asc':
				$orderBy = 'is_active, form_id, title';
				break;
			case 'is_active_desc':
				$orderBy = 'is_active DESC, form_id DESC, title DESC';
				break;
			default:
				$orderBy = 'title';
				break;
		}

		return $this->db->selectAll('
			SELECT * 
			FROM ' . DB_PREFIX . 'custom_forms ' . $this->getSearchQuery($searchParams).' 
			ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Get custom forms options list
	 *
	 * @return array
	 */
	public function getOptionsList()
	{
		$list = $this->getList();
		$options = array();
		foreach ($list as $customForm) $options[$customForm['id']] = $customForm['title'];
		return $options;
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['title']) == '') $errors['title'] = array('Please enter title');

			if (trim($data['form_id']) == '')
			{
				$errors['form_id'] = array('Please enter form id');
			}
			else
			{
				$db = $this->db;
				if ($db->selectOne('SELECT * FROM ' . DB_PREFIX . 'custom_forms WHERE form_id="' . $db->escape($data['form_id'] ) . '"' . (!is_null($id) ? ' AND id <> '.intval($id) : '')))
				{
					$errors['form_id'] = array('Form id must be unique within custom forms');
				}
			}

			if ($data['redirect_on_submit'] == 1 && trim($data['redirect_on_submit_url']) == '')
			{
				$errors['redirect_on_submit_url'] = array('Please enter redirect URL');
			}

		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist custom form
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$customFormModel = is_array($data) ? new Model_CustomForm($data) : $data;

		$db->assignStr('is_active', $customFormModel->getIsActive() ? '1' : '0');
		$db->assignStr('is_title_visible', $customFormModel->getIsTitleVisible() ? '1' : '0');
		$db->assignStr('is_recaptcha', $customFormModel->getIsRecaptcha() ? '1' : '0');
		$db->assignStr('columns_count', 1);
		$db->assignStr('save_data', 1);
		$db->assignStr('email_notification', $customFormModel->getEmailNotification() ? '1' : '0');
		$db->assignStr('redirect_on_submit', $customFormModel->getRedirectOnSubmit() ? '1' : '0');

		$db->assignStr('form_id', $customFormModel->getFormId());
		$db->assignStr('title', $customFormModel->getTitle());
		$db->assignStr('email_notification_subject', $customFormModel->getEmailNotificationSubject());
		$db->assignStr('redirect_on_submit_url', $customFormModel->getRedirectOnSubmitUrl());
		$db->assignStr('description', $customFormModel->getDescription());
		$db->assignStr('thank_you_message', $customFormModel->getThankYouMessage());

		$id = $customFormModel->getId();

		if ($id != null && $id > 0)
		{
			$db->update(DB_PREFIX . 'custom_forms',  'WHERE id=' . intval($id));
			return true;
		}
		else
		{
			$id = $db->insert(DB_PREFIX . 'custom_forms');
			$customFormModel->setId($id);

			return $id;
		}
	}

	/**
	 * Delete custom forms by ids
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		// delete manufacturers & reset products
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'custom_forms WHERE id IN(' . implode(',', $ids) . ')');

		// TODO: delete from custom fields as well

		return true;
	}

	/**
	 * Delete custom forms in search results
	 *
	 * @param $searchParams
	 *
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams)
	{
		$searchQuery = $this->getSearchQuery($searchParams);

		$this->db->query('DELETE FROM ' . DB_PREFIX . 'custom_forms ' . $searchQuery);

		// TODO: delete from custom fields as well

		return true;
	}

	/**
	 * @param $formId
	 * @param null $searchParams
	 * @return mixed
	 */
	public function getFormPostsCount($formId, $searchParams = null)
	{
		if (isset($searchParams['field_id']) && array_key_exists('search_str', $searchParams) && $searchParams['search_str'] !== '')
		{
			$exactMatch = isset($searchParams['exact_match']) && $searchParams['exact_match'] ? '' : '%';

			$r = $this->db->selectOne('
				SELECT COUNT(*) c
				FROM ' . DB_PREFIX . 'custom_fields_values cfv,
					 ' . DB_PREFIX . 'custom_fields cf
				WHERE cfv.custom_form_id=' . intval($formId) . ' 
				AND cfv.field_id = cf.field_id
				AND ' . ($searchParams['field_id'] != 'all' ? (' cfv.field_id = ' . intval($searchParams['field_id']) . ' AND ') : '') . ' 
				' . ($searchParams['field_name'] != '' ? (' cf.field_name = "' . $this->db->escape($searchParams['field_name']) . '" AND ') : '') . '
				cfv.field_value LIKE "' . $exactMatch . $this->db->escape($searchParams['search_str']). $exactMatch . '"
			');
		}
		else
		{
			$r = $this->db->selectOne('SELECT COUNT(*) c FROM ' . DB_PREFIX . 'custom_forms_posts WHERE custom_form_id=' . intval($formId));
		}

		return $r['c'];
	}

	/**
	 * @param $formId
	 * @param null $start
	 * @param null $limit
	 * @param null $searchParams
	 * @param null $orderBy
	 * @return array|bool
	 */
	public function getFormPostsList($formId, $start = null, $limit = null, $searchParams = null, $orderBy = null)
	{
		$formId = intval($formId);

		/**
		 * Get fields
		 */
		$postsFieldsRecords = $this->db->selectAll('
			SELECT *
			FROM ' . DB_PREFIX . 'custom_fields
			WHERE custom_form_id= ' . $formId . ' AND field_place = "form"
		');

		if (!$postsFieldsRecords || (is_array($postsFieldsRecords) && count($postsFieldsRecords) == 0)) return false;

		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		$orderBy = in_array($orderBy, array('date', 'date_desc')) ? $orderBy : 'date_desc';

		/**
		 * Get records first
		 */
		if ($searchParams != null && isset($searchParams['field_id']) && array_key_exists('search_str', $searchParams) && $searchParams['search_str'] !== '')
		{
			$exactMatch = isset($searchParams['exact_match']) && $searchParams['exact_match'] ? '' : '%';

			$postsRecords = $this->db->selectAll('
				SELECT cfp.*
				FROM ' . DB_PREFIX . 'custom_fields_values cfv
				INNER JOIN ' . DB_PREFIX . 'custom_forms_posts cfp ON cfv.custom_form_post_id = cfp.id
				WHERE
					cfv.custom_form_id=' . intval($formId) . ' AND
					' . ($searchParams['field_id'] != 'all' ? (' cfv.field_id = ' . intval($searchParams['field_id']) . ' AND') : '') . '
					cfv.field_value LIKE "' . $exactMatch . $this->db->escape($searchParams['search_str']). $exactMatch . '"
				GROUP BY cfp.id
				ORDER BY cfp.date_created ' . ($orderBy == 'date_desc' ? ' DESC ' : '') .
				(!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
			);
		}
		else
		{
			$postIds = array();

			if ($searchParams != null && isset($searchParams['post_ids']) && count($searchParams['post_ids']) > 0)
			{
				foreach ($searchParams['post_ids'] as $postId) $postIds[] = intval($postId);
			}

			$postsRecords = $this->db->selectAll('
				SELECT *
				FROM ' . DB_PREFIX . 'custom_forms_posts
				WHERE custom_form_id = ' . $formId . '
				' . (count($postIds) > 0 ? ' AND id IN (' . implode(',', $postIds). ')' : '') . '
				ORDER BY date_created ' . ($orderBy == 'date_desc' ? ' DESC ' : '') .
				(!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
			);
		}

		if (!$postsRecords || (is_array($postsRecords) && count($postsRecords) == 0)) return false;

		/**
		 * Parse posts and fields
		 */
		$posts = array();
		foreach ($postsRecords as $postsRecord)
		{
			$postRecordId = intval($postsRecord['id']);
			$posts[$postRecordId] = $postsRecord;
			$posts[$postRecordId]['fields'] = array();
		}

		$fields = array();
		foreach ($postsFieldsRecords as $postsFieldsRecord)
		{
			$postsFieldsRecordId = intval($postsFieldsRecord['field_id']);
			$fields[$postsFieldsRecordId] = $postsFieldsRecord;
		}

		/**
		 * Get data
		 */
		$fieldsValues = $this->db->selectAll('
			SELECT field_id, custom_form_post_id, field_value
			FROM ' . DB_PREFIX . 'custom_fields_values
			WHERE custom_form_id = ' . $formId . ' AND custom_form_post_id in (' . implode(',', array_keys($posts)). ')
		');

		foreach ($fieldsValues as $fieldsValue)
		{
			$fieldId = $fieldsValue['field_id'];

			if (isset($fields[$fieldId]))
			{
				$posts[$fieldsValue['custom_form_post_id']]['fields'][$fieldId] = $fieldsValue['field_value'];
			}
		}

		return $posts;
	}

	/**
	 * @param $formId
	 * @param null $start
	 * @param null $limit
	 * @param null $searchParams
	 * @return array
	 */
	public function getCustomFormPostsList($formId, $start = null, $limit = null, $searchParams = null)
	{
		$formId = intval($formId);
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		if (($searchParams != null && isset($searchParams['field_name'])) || (array_key_exists('search_str', $searchParams) && $searchParams['search_str'] !== ''))
		{
			$exactMatch = isset($searchParams['exact_match']) && $searchParams['exact_match'] ? '' : '%';
			$orderDir = $this->db->escape($searchParams['orderDir']);
			$fieldName = $this->db->escape($searchParams['field_name']);
			$searchStr = $this->db->escape($searchParams['search_str']);

			// Get the custom_forms_posts with the field to be sorted

			// By Date Created (Default)
			if ($fieldName == 'date_created')
			{
				$customFormsPosts = $this->db->selectAll("
					SELECT DISTINCT c_fields_values.custom_form_id AS custom_forms_id,
					                c_forms_posts.id AS custom_forms_posts_id,
					                c_forms_posts.date_created AS date_created
					FROM " . DB_PREFIX . "custom_forms AS c_forms
					LEFT JOIN " . DB_PREFIX . "custom_forms_posts AS c_forms_posts ON (c_forms.id = c_forms_posts.custom_form_id)
					LEFT JOIN " . DB_PREFIX . "custom_fields AS c_fields ON (c_fields.custom_form_id = c_forms_posts.custom_form_id)
					LEFT JOIN " . DB_PREFIX . "custom_fields_values AS c_fields_values ON (c_fields_values.field_id = c_fields.field_id)
					WHERE c_forms.id = $formId 
					AND c_fields_values.custom_form_id IS NOT NULL
					AND c_forms_posts.id IS NOT NULL 
					AND c_forms_posts.date_created IS NOT NULL
					ORDER BY date_created $orderDir" .
					(!is_null($start) && !is_null($limit) ? (" LIMIT " . $start . ", " . $limit) : "")
				);
			}
			// By Field Name
			else
			{
				// With Field Value search
				if (!empty($searchStr))
				{
					$customFormsPosts = $this->db->selectAll("
						SELECT DISTINCT c_fields_values.custom_form_id AS custom_forms_id,
						                c_fields_values.custom_form_post_id AS custom_forms_posts_id,
						                c_fields_values.field_value AS field_value
						FROM " . DB_PREFIX . "custom_forms AS c_forms
						LEFT JOIN " . DB_PREFIX . "custom_forms_posts AS c_forms_posts ON (c_forms.id = c_forms_posts.custom_form_id)
						LEFT JOIN " . DB_PREFIX . "custom_fields AS c_fields ON (c_fields.custom_form_id = c_forms_posts.custom_form_id)
						LEFT JOIN " . DB_PREFIX . "custom_fields_values AS c_fields_values ON (c_fields_values.field_id = c_fields.field_id)
						WHERE c_forms.id =  $formId
						AND c_fields.field_name = '" . $fieldName . "'
						AND c_fields_values.field_value LIKE  '" . $exactMatch . $searchStr . $exactMatch . "' 
						AND c_fields_values.custom_form_id IS NOT NULL
						AND c_fields_values.custom_form_post_id IS NOT NULL
						AND c_fields_values.field_value IS NOT NULL
						ORDER BY c_fields_values.field_value $orderDir" .
						(!is_null($start) && !is_null($limit) ? (" LIMIT " . $start . ", " . $limit) : "")
					);
				}
				// Without Field Value search
				else
				{
					$customFormsPosts = $this->db->selectAll("
						SELECT DISTINCT c_fields_values.custom_form_id AS custom_forms_id,
						                c_fields_values.custom_form_post_id AS custom_forms_posts_id,
						                c_fields_values.field_value AS field_value
						FROM " . DB_PREFIX . "custom_forms AS c_forms
						LEFT JOIN " . DB_PREFIX . "custom_forms_posts AS c_forms_posts ON (c_forms.id = c_forms_posts.custom_form_id)
						LEFT JOIN " . DB_PREFIX . "custom_fields AS c_fields ON (c_fields.custom_form_id = c_forms_posts.custom_form_id)
						LEFT JOIN " . DB_PREFIX . "custom_fields_values AS c_fields_values ON (c_fields_values.field_id = c_fields.field_id)
						WHERE c_forms.id =  $formId
						AND c_fields.field_name = '" . $fieldName . "'
						AND c_fields_values.custom_form_id IS NOT NULL
						AND c_fields_values.custom_form_post_id IS NOT NULL
						AND c_fields_values.field_value IS NOT NULL
						ORDER BY c_fields_values.field_value $orderDir" .
						(!is_null($start) && !is_null($limit) ? (" LIMIT " . $start . ", " . $limit) : "")
					);
				}
			}
		}
		else
		{
			// By Date Created (Default)
			$customFormsPosts = $this->db->selectAll("
				SELECT DISTINCT c_fields_values.custom_form_id AS custom_forms_id,
				                c_forms_posts.id AS custom_forms_posts_id,
				                c_forms_posts.date_created AS date_created
				FROM " . DB_PREFIX . "custom_forms AS c_forms
				LEFT JOIN " . DB_PREFIX . "custom_forms_posts AS c_forms_posts ON (c_forms.id = c_forms_posts.custom_form_id)
				LEFT JOIN " . DB_PREFIX . "custom_fields AS c_fields ON (c_fields.custom_form_id = c_forms_posts.custom_form_id)
				LEFT JOIN " . DB_PREFIX . "custom_fields_values AS c_fields_values ON (c_fields_values.field_id = c_fields.field_id)
				WHERE c_forms.id = $formId 
				AND c_fields_values.custom_form_id IS NOT NULL
				AND c_forms_posts.id IS NOT NULL 
				AND c_forms_posts.date_created IS NOT NULL
				ORDER BY date_created DESC" .
				(!is_null($start) && !is_null($limit) ? (" LIMIT " . $start . ", " . $limit) : "")
			);
		}

		// The result above will be loop and feed to the c_fields_values.custom_form_post_id = $custom_forms_posts_id and id = $custom_forms_posts_id

		$customFormsPostsData = array();
		if (!empty($customFormsPosts))
		{
			foreach ($customFormsPosts as $key => $customFormPost)
			{
				$customFormsId = $customFormPost['custom_forms_id'];
				$customFormsPostsId = $customFormPost['custom_forms_posts_id'];

				// Get the date_created and user_id
				$customFormsPostsAdditionalData = $this->db->selectOne("
					SELECT *
					FROM " . DB_PREFIX . "custom_forms_posts
					WHERE id = $customFormsPostsId;
				");

				$customFormsPostsUserId = $customFormsPostsAdditionalData['user_id'];
				$customFormsPostDateCreated = $customFormsPostsAdditionalData['date_created'];

				// Get the fields
				$customFields = $this->db->selectAll("
					SELECT c_fields.field_id,
						   c_fields.field_name,
						   c_fields_values.field_value
					FROM " . DB_PREFIX . "custom_fields_values AS c_fields_values
					LEFT JOIN " . DB_PREFIX . "custom_fields AS c_fields ON (c_fields_values.field_id = c_fields.field_id)
					WHERE c_fields_values.custom_form_post_id = $customFormsPostsId
					ORDER BY c_fields_values.field_id;
				");

				// Restructure data
				$count = count($customFields);
				foreach ($customFields as $index => $customField)
				{
					$customFormsPostsData[$customFormsPostsId]['fields'][$customField['field_id']] = $customField['field_value'];
					if ($index == $count - 1)
					{
						$customFormsPostsData[$customFormsPostsId]['id'] = $customFormsPostsId;
						$customFormsPostsData[$customFormsPostsId]['custom_form_id'] = $customFormsId;
						$customFormsPostsData[$customFormsPostsId]['user_id'] = $customFormsPostsUserId;
						$customFormsPostsData[$customFormsPostsId]['date_created'] = $customFormsPostDateCreated;
					}
				}
			}
		}

		return $customFormsPostsData;
	}

	/**
	 * Delete by ids
	 *
	 * @param $formId
	 * @param $postIds
	 */
	public function deleteFormPosts($formId, $postIds)
	{
		$postIds = is_array($postIds) ? $postIds : array($postIds);

		foreach ($postIds as $key => $value) $postIds[$key] = intval($value);

		$db = $this->db;
		$db->query('DELETE FROM ' . DB_PREFIX .'custom_fields_values WHERE custom_form_id=' . intval($formId) . ' AND custom_form_post_id IN (' . implode(',', $postIds) . ')');
		$db->query('DELETE FROM ' . DB_PREFIX .'custom_forms_posts WHERE id IN (' . implode(',', $postIds) . ') AND custom_form_id=' . intval($formId));
	}

	/**
	 * @param $formId
	 * @param $searchParams
	 */
	public function deleteFormPostsBySearchParams($formId, $searchParams)
	{
		if ($searchParams != null && isset($searchParams['field_id']) && array_key_exists('search_str', $searchParams) && $searchParams['search_str'] !== '')
		{
			$exactMatch = isset($searchParams['exact_match']) && $searchParams['exact_match'] ? '' : '%';

			$postsRecords = $this->db->selectAll('
				SELECT cfp.id
				FROM ' . DB_PREFIX . 'custom_fields_values cfv
				INNER JOIN ' . DB_PREFIX . 'custom_forms_posts cfp ON cfv.custom_form_post_id = cfp.id
				WHERE
					cfv.custom_form_id=' . intval($formId) . ' AND
					' . ($searchParams['field_id'] != 'all' ? ' cfv.field_id = ' . intval($searchParams['field_id']) . ' AND ' : '') . '
					cfv.field_value LIKE "' . $exactMatch . $this->db->escape($searchParams['search_str']). $exactMatch . '"
				GROUP BY cfp.id
			');

			if ($postsRecords)
			{
				$postsIds =  array();
				foreach ($postsRecords as $key => $value) $postsIds[] = $value['id'];
				$this->deleteFormPosts($formId, $postsIds);
			}
		}
	}
}
<?php

/**
 * Class DataAccess_CustomersRepository
 */
class DataAccess_CustomersRepository extends DataAccess_BaseRepository
{
	/** @var DataAccess_SettingsRepository|null  */
	protected $settings = null;

	/**
	* @param DB $db
	* @param $settings
	*/
	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		parent::__construct($db);
		$this->settings = $settings;
	}

	/**
	* @param $mode
	* @return array
	*/
	public function getDefaults($mode)
	{
		return array(
			'created_date' => '0000-00-00 00:00:00',
			'session_id' =>'',
			'session_date' => '0000-00-00 00:00:00',
			'blocked' => 'No',
			'lock' => '',
			'block_date' => '0000-00-00 00:00:00',
			'removed' => 'No',
			'receives_marketing' => 'No',
			'level' => 0,
			'is_tax_exempt' => 0,
			'fname' => '',
			'lname' => '',
			'login' => '',
			'password' => '',
			'billing_address[address1]' => '',
			'billing_address[address2]' => '',
			'billing_address[city]' => '',
			'billing_address[state]' => 3,
			'billing_address[province]' => '',
			'billing_address[zip]' => '',
			'billing_address[country]' => '',
			'billing_address' => array(
				'address1' => '',
				'address2' => '',
				'city' => '',
				'state' => 3,
				'province' => '',
				'zip' => '',
				'country' => 1,
				'company' => '',
			),
			'email' => '',
			'phone' => '',
			'last_update' => '0000-00-00 00:00:00',
			'facebook_user' => 0,
			'facebook_id' => '',
			'avalara_tax_exempt_number' => '',
			'exactor_tax_exemption_id' => '',
			'ExpressCheckoutUser' => 0,
			'FormsBillingCompany' => $this->settings->get('FormsBillingCompany'),
			'FormsBillingPhone' => $this->settings->get('FormsBillingPhone'),
			'FormsBillingAddressLine2' => $this->settings->get('FormsBillingAddressLine2')
		);
	}

	/**
	 * @param null $searchParams
	 * @param string $logic
	 * @return int
	 */
	public function getCustomersCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'users ' . $this->getSearchQuery($searchParams, $logic));
		return intval($result['c']);
	}

	/**
	 * @param $id
	 * @return bool|Model_Address
	 */
	public function getBillingInfoById($id)
	{
		$data = false;
		if ($id)
		{
			$data = $this->db->selectOne("
				SELECT
					".DB_PREFIX."users.fname,
					".DB_PREFIX."users.lname,
					CONCAT(".DB_PREFIX."users.fname, ' ', ".DB_PREFIX."users.lname) AS name,
					".DB_PREFIX."users.company,
					".DB_PREFIX."users.address1,
					".DB_PREFIX."users.address2,
					".DB_PREFIX."users.city,
					".DB_PREFIX."users.state,
					".DB_PREFIX."users.state AS state_id,
					".DB_PREFIX."users.province,
					".DB_PREFIX."users.country,
					".DB_PREFIX."users.country AS country_id,
					".DB_PREFIX."users.zip,
					".DB_PREFIX."users.phone,
					".DB_PREFIX."users.email,
					".DB_PREFIX."countries.name AS country_name,
					".DB_PREFIX."countries.iso_a2 AS country_iso_a2,
					".DB_PREFIX."countries.iso_a3 AS country_iso_a3,
					".DB_PREFIX."countries.iso_number AS country_iso_number,
					".DB_PREFIX."states.name AS state_name,
					".DB_PREFIX."states.short_name AS state_abbr
				FROM ".DB_PREFIX."users
				LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users.country
				LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."users.state
				WHERE ".DB_PREFIX."users.uid='".intval($id)."' AND removed='No'
			");
		}
		if ($data)
		{
			return $address = new Model_Address($data);
		}
		else
		{
			return false;
		}
	}

	/**
	 * @param $id
	 * @return bool|Model_Address
	 */
	public function getShippingAddressesByCustomerId($id)
	{
		$this->db->query("
			SELECT
				".DB_PREFIX."users_shipping.*,
				".DB_PREFIX."countries.name AS country_name,
				".DB_PREFIX."states.name AS state_name
			FROM ".DB_PREFIX."users_shipping
			LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users_shipping.country
			LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."users_shipping.state
			WHERE uid='".intval($id)."'
			ORDER BY is_primary
		");
		if($this->db->numRows() > 0)
		{
			return $address = new Model_Address($this->db->getRecords());
		}
		else{
			return false;
		}
	}

	/**
	* @param $id
	* @return array|bool
	*/
	public function getById($id)
	{
		$result = $this->db->selectOne('
			SELECT u.*,
				u.state,
				u.state AS state_id,
				u.country AS country_id,
				c.name AS country_name,
				c.iso_a2 AS country_iso_a2,
				c.iso_a3 AS country_iso_a3,
				c.iso_number AS country_iso_number,
				s.name AS state_name,
				s.short_name AS state_abbr,
				IF(u.block_date != "0000-00-00 00:00:00", "Yes", "No") AS blocked
			FROM '.DB_PREFIX.'users u
				LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = u.country
				LEFT JOIN '.DB_PREFIX.'states s ON s.stid = u.state
			WHERE u.uid = '.intval($id)
		);

		if ($result)
		{
			$result['FormsBillingCompany']= $this->settings->get('FormsBillingCompany');
			$result['FormsBillingPhone'] = $this->settings->get('FormsBillingPhone');

			$result['shipping_addresses'] = $this->db->selectAll('
				SELECT u.*,
					u.state,
					u.state AS state_id,
					u.country AS country_id,
					c.name AS country_name,
					c.iso_a2 AS country_iso_a2,
					c.iso_a3 AS country_iso_a3,
					c.iso_number AS country_iso_number,
					s.name AS state_name,
					s.short_name AS state_abbr
				FROM '.DB_PREFIX.'users_shipping u
					LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = u.country
					LEFT JOIN '.DB_PREFIX.'states s ON s.stid = u.state
				WHERE u.uid = '.intval($id));
		}

		return $result;
	}

	/**
	 * @param array $ids
	 * @return array|mixed
	 */
	public function getByIds(array $ids)
	{
		if (count($ids) == 0) return array();

		$users = $this->db->selectAll('
			SELECT u.*,
				u.state,
				u.state AS state_id,
				u.country AS country_id,
				c.name AS country_name,
				c.iso_a2 AS country_iso_a2,
				c.iso_a3 AS country_iso_a3,
				c.iso_number AS country_iso_number,
				s.name AS state_name,
				s.short_name AS state_abbr,
				IF(NOW() < u.block_date, "Yes", "No") AS blocked
			FROM '.DB_PREFIX.'users u
				LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = u.country
				LEFT JOIN '.DB_PREFIX.'states s ON s.stid = u.state
			WHERE u.uid IN ('.implode(',', $ids).')
		');

		foreach ($users as &$user)
		{
			$user['FormsBillingCompany']= $this->settings->get('FormsBillingCompany');
			$user['FormsBillingPhone'] = $this->settings->get('FormsBillingPhone');
			$user['shipping_addresses'] = array();
		}

		//TODO: Load shipping addresses?
//		if ($result)
//		{
//			$result['FormsBillingCompany']= $this->settings->get('FormsBillingCompany');
//			$result['FormsBillingPhone'] = $this->settings->get('FormsBillingPhone');
//
//			$result['shipping_addresses'] = $this->db->selectAll('SELECT u.*,
//	u.state,
//	u.state AS state_id,
//	u.country AS country_id,
//	c.name AS country_name,
//	c.iso_a2 AS country_iso_a2,
//	c.iso_a3 AS country_iso_a3,
//	c.iso_number AS country_iso_number,
//	s.name AS state_name,
//	s.short_name AS state_abbr
//FROM '.DB_PREFIX.'users_shipping u
//	LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = u.country
//	LEFT JOIN '.DB_PREFIX.'states s ON s.stid = u.state
//WHERE u.uid = '.intval($id));
//		}

		return $users;
	}

	/**
	 * @param $ids
	 * @return mixed
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);
		foreach ($ids as $key=>$value) $ids[$key] = intval($value);
		if (count($ids) < 1) return;

		return $this->db->query('UPDATE '.DB_PREFIX.'users SET `removed` = "Yes" WHERE uid IN('.implode(',', $ids).')');
	}

	/**
	 * @return mixed
	 */
	public function deleteAll()
	{
		return $this->db->query('UPDATE '.DB_PREFIX.'users SET `removed` = "Yes"');
	}

	/**
	 * @param $searchParams
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams)
	{
		$records = null;
		$start = 0;

		$success = true;
		do
		{
			$records = $this->getCustomersList($start, 500, 'lname, fname', $searchParams);

			if (count($records) > 0)
			{
				$uids = array();
				foreach ($records as $record)
				{
					$uids[] = $record['uid'];
				}
				$result = $this->delete($uids);

				if (!$result) $success = false;
			}

		} while ($records !== null && count($records) > 0);

		return $success;
	}

	/**
	 * @param $data
	 * @param $mode
	 * @param null $id
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['fname']) == '')
			{
				$errors['fname'] = array('Please enter first name');
			}

			if ($data['lname'] == '') {
				$errors['lname'] = array('Please enter last name');
			}

			if (trim($data['company']) == '' && $this->settings->get('FormsBillingCompany') == 'Required')
			{
				$errors['company'] = array('Please enter company');
			}

			if (isset($data['address_book']))
			{
				if (trim($data['address_book'][0]['company']) == '' && $this->settings->get('FormsShippingCompany') == 'Required') {
					$errors['address_book[0][company]'] = array('Please enter company');
				}

				if (trim($data['address_book'][0]['address2']) == '' && $this->settings->get('FormsShippingAddressLine2') == 'Required') {
					$errors['address_book[0][address2]'] = array('Please enter Address line 2');
				}

				if (trim($data['address_book'][0]['address1']) == '') {
					$errors['address_book[0][address1]'] = array('Please enter Address line 1');
				}
			}

			if ($data['phone'] == '' && $this->settings->get('FormsBillingPhone') == 'Required') {
				$errors['phone'] = array('Please enter phone');
			}

			if ($data['email'] == '') {
				$errors['email'] = array('Please enter email');
			}

			if($this->hasDuplicateUsername($data['login'], $data['id']))
			{
				$errors['login'] = array('Username already exist');
			}

			if(strlen($data['login']) > 0 && strlen($data['login']) < 4)
			{
				$errors['login'] = array('Username must be at least 4 characters');
			}

			if ($data['password'] != '')
			{
				if ($data['password'] != $data['password2'])
				{
					$errors['password2'] = array('Please check password confirmation');
				}

				if(strlen($data['password']) > 0 && strlen($data['password']) < 6)
				{
					$errors['password'] = array('Password must be at least 6 characters');
				}
			}

			$billingAddress = $data['billing_address']; // array

			if ($billingAddress['address1'] == '') {
				$errors['billing_address[address1]'] = array('Please enter address');
			}

			if ($data['FormsBillingAddressLine2'] ==  'Required')
			{
				if ($billingAddress['address2'] == '') {
					$errors['billing_address[address2]'] = array('Please enter address');
				}
			}

			if ($billingAddress['city'] == '')
			{
				$errors['billing_address[city]'] = array('Please enter City');
			}

			if ($billingAddress['zip'] == '')
			{
				$errors['billing_address[zip]'] = array('Please enter postal code');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Get customers list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param $logic
	 *
	 * @return mixed
	 */
	public function getCustomersList($start = null, $limit = null, $orderBy = null, $searchParams = null, $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy) || empty($orderBy)) $orderBy = 'lname, fname';

		switch ($orderBy)
		{
			/**
			 * New order by params
			 */
			case 'name_asc' : $orderBy = 'lname, fname'; break;
			case 'name_desc' : $orderBy = 'lname DESC, fname DESC'; break;
			case 'username_asc' : $orderBy = 'login'; break;
			case 'username_desc' : $orderBy = 'login DESC'; break;
			case 'email_asc' : $orderBy = 'email'; break;
			case 'email_desc' : $orderBy = 'email DESC'; break;
			case 'company_asc' : $orderBy = 'company'; break;
			case 'company_desc' : $orderBy = 'company DESC'; break;
			case 'phone_asc' : $orderBy = 'email'; break;
			case 'phone_desc' : $orderBy = 'email DESC'; break;

			default: $orderBy = 'lname, fname';	break;
		}

		$customers = $this->db->selectAll('
			SELECT u.*,
				u.state,
				u.state AS state_id,
				u.country AS country_id,
				c.name AS country_name,
				c.iso_a2 AS country_iso_a2,
				c.iso_a3 AS country_iso_a3,
				c.iso_number AS country_iso_number,
				s.name AS state_name,
				s.short_name AS state_abbr,
				IF(NOW() < u.block_date, "Yes", "No") AS blocked
			FROM '.DB_PREFIX.'users u
			LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = u.country
			LEFT JOIN '.DB_PREFIX.'states s ON s.stid = u.state
			' . $this->getSearchQuery($searchParams, $logic) . '
			ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);

		$customersByIds = array();
		if (count($customers) > 0)
		{
			foreach ($customers as &$customer)
			{
				$customer['shipping_addresses'] = array();
				$customersByIds[$customer['uid']] = $customer;
			}

			$customerAddresses = $this->db->selectAll('
				SELECT u.*,
					u.state,
					u.state AS state_id,
					u.country AS country_id,
					c.name AS country_name,
					c.iso_a2 AS country_iso_a2,
					c.iso_a3 AS country_iso_a3,
					c.iso_number AS country_iso_number,
					s.name AS state_name,
					s.short_name AS state_abbr
				FROM '.DB_PREFIX.'users_shipping u
				LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = u.country
				LEFT JOIN '.DB_PREFIX.'states s ON s.stid = u.state
				WHERE u.uid IN ('.implode(',', array_keys($customersByIds)).')
				ORDER BY u.uid
			');

			foreach ($customerAddresses as &$customerAddress)
			{
				if (array_key_exists($customerAddress['uid'], $customersByIds))
				{
					$customersByIds[$customerAddress['uid']]['shipping_addresses'][] = $customerAddress;
				}
			}
		}

		return array_values($customersByIds);
	}

	/**
	 * @param $data
	 * @param null $persist
	 * @return bool|null
	 */
	public function persist($data, $persist = null)
	{
		$db = $this->db;

		if ($data['billing_address']['state'] > 0)
		{
			$stateData = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'states WHERE coid=' . intval($data['billing_address']['country']) . ' AND stid=' . intval($data['billing_address']['state']));
			if ($stateData)
			{
				$data['billing_address']['province'] = $stateData['name'];
			}
		}


		if (isset($data['address_book']) && !empty($data['address_book']))
		{
			// update users shipping addreses
			$this->updateUserAddressBook($db, $data['address_book']);
		}

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['uid']) ? $data['uid'] : null);

		$db->reset();

		if ($data['lock'] == 'Yes' || (isset($data['account_status']) && $data['account_status'] == 'block')) $db->assign('block_date', 'NOW()');
		elseif (isset($data['account_status']) && $data['account_status'] == 'active') $this->unlockAccount($data['id']);

		$db->assignStr('fname', $data['fname']);
		$db->assignStr('lname', $data['lname']);

		$db->assignStr('email', $data['email']);
		$db->assignStr('phone', $data['phone']);

		if (isset($data['company'])) $db->assignStr('company', $data['company']);
		$db->assignStr('receives_marketing', $data['receives_marketing']);

		$billingAddress = $data['billing_address']; // array
		$db->assignStr('address1', $billingAddress['address1']);
		$db->assignStr('address2', $billingAddress['address2']);
		$db->assignStr('city', $billingAddress['city']);
		$db->assign('state', $billingAddress['state']);
		$db->assignStr('province', $billingAddress['province']);
		$db->assignStr('zip', $billingAddress['zip']);
		$db->assign('country', $billingAddress['country']);

		$db->assign('level', $data['level']);
		if (isset($data['is_tax_exempt'])) $db->assignStr('is_tax_exempt', $data['is_tax_exempt']);
		if (isset($data['avalara_tax_exempt_number'])) $db->assignStr('avalara_tax_exempt_number', $data['avalara_tax_exempt_number']);
		if (isset($data['exactor_tax_exemption_id'])) $db->assignStr('exactor_tax_exemption_id', $data['exactor_tax_exemption_id']);

		$db->assign('last_update', 'NOW()');
		$db->assignStr('login', $data['login']);

		if (trim($data['password']) != '') $db->assignStr('password', getPasswordHash($data['password']));

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'users',  'WHERE uid='.intval($data['id']));
			return $data['id'];
		}
		else
		{
			$db->assign('created_date', 'now()');
			if (isset($data['login'])) $db->assignStr('login', $data['login']);
			if (isset($data['password'])) $db->assignStr('password', $data['password']);

			$db->insert(DB_PREFIX.'users');
			return $db->insertId();
		}
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 * @param $logic
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$whereStr = ' WHERE removed = "No" ';

		if (!isset($searchParams['ExpressCheckoutUser']) || trim($searchParams['ExpressCheckoutUser']) == '0') $whereStr .= ' AND login <> "ExpressCheckoutUser"';

		$where = array();

		$searchStr = (isset($searchParams['search_str']) ? $searchParams['search_str'] : '');
		$searchStr = trim($searchStr);

		if ($searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$where[] =
				'((CONCAT(fname, " ", lname) like "%' . $searchStr . '%")' .
				'OR (email like "%' . $searchStr . '%")' .
				'OR (login like "%' . $searchStr . '%")' .
				'OR (company like "%' . $searchStr . '%")' .
				'OR (phone like "%' . $searchStr . '%"))';
		}

		if (isset($searchParams['from_date']) && $searchParams['from_date'] != '') $where[] = 'created_date  >= "'.$this->db->escape($searchParams['from_date']).'"';
		if (isset($searchParams['to_date']) && $searchParams['to_date'] != '') $where[] = 'created_date  <= "'.$this->db->escape($searchParams['to_date']).'"';
		if (isset($searchParams['wholesale_users']) && $searchParams['wholesale_users'] != '')
		{
			if ($searchParams['wholesale_users'] == '1,2,3')
			{
				$where[] = 'level > 0';
			}
			else
			{
				$where[] = 'level = ' . intval($searchParams['wholesale_users']);
			}
		}

		return $whereStr.(count($where) > 0 ? ' AND ('.implode(' '.$logic.' ', $where).') ' : '') . ' ';
	}

	/**
	 * @param $uid
	 * @param $securityAccountBlockingHours
	 */
	public function lockAccount($uid, $securityAccountBlockingHours)
	{
		$this->db->query('UPDATE '.DB_PREFIX.'users SET block_date = DATE_ADD(NOW(), INTERVAL '.intval($securityAccountBlockingHours).' HOUR) WHERE uid = '.intval($uid));
	}

	/**
	 * @param $uid
	 */
	public function unlockAccount($uid)
	{
		$this->db->query('UPDATE '.DB_PREFIX.'users SET block_date = "0000-00-00 00:00:00" WHERE uid = '.intval($uid));
	}

	/**
	 * @param $username
	 * @param $uid
	 * @return array|bool
	 */

	public function hasDuplicateUsername($username, $uid)
	{
		$result = $this->db->selectOne('SELECT login FROM '.DB_PREFIX.'users WHERE login = "' . $this->db->escape($username) . '" AND uid <> ' . intval($uid) . ' AND removed <> "YES"' );

		return $result;
	}

	/**
	 * @param $uid
	 */
	public function getUsername($uid)
	{
		$result = $this->db->selectOne('SELECT login FROM '.DB_PREFIX.'users WHERE uid = '. intval($uid));

		return $result['login'];
	}

	/**
	 * @param array $users
	 * @return array|null
	 */
	public function getUsersId($users = array())
	{
		if (!empty($users))
		{
			$usersId = array();
			foreach($users as $key => $user)
			{
				$usersId[] = $user['uid'];
			}

			return $usersId;
		}
		else
		{
			return null;
		}
	}

	/**
	 * @param $db
	 * @param $data
	 */
	protected function updateUserAddressBook($db, $data)
	{
		$db->reset();
		foreach ($data as $address)
		{
			$db->assignStr("address_type", $address["address_type"]);
			$db->assignStr("name", $address["name"]);
			$db->assignStr("company", $address["company"]);
			$db->assignStr("address1", $address["address1"]);
			$db->assignStr("address2", $address["address2"]);
			$db->assignStr("city", $address["city"]);
			$db->assignStr("state", (isset($address["state"]) ? $address["state"] : 0));
			$db->assignStr("province", (isset($address["province"]) ? $address["province"] : ''));
			$db->assignStr("zip", $address["zip"]);
			$db->assignStr("country", $address["country"]);
			$db->update(DB_PREFIX."users_shipping", "WHERE usid='".intval($address['usid'])."'");
		}
	}
}

<?php

/**
 *  Class DataAccess_ReportsCustomersRepository
 */
class DataAccess_ReportsCustomersRepository
{
	/** @var DB */
	protected $db;

	/**
	 * DataAccess_ReportsCustomersRepository constructor.
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		$this->db = $db;
	}

	/**
	 * @param array $dateRange
	 * @return float
	 */
	public function getSimpleCustomerLifetimeValue($dateRange = array())
	{
		// Reference to Customer Lifetime Value: https://blog.kissmetrics.com/how-to-calculate-lifetime-value/?wide=1
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		// Get the average customer expenditures per order at the given time period where orders are fulfilled
		$queryCustomersExpendituresPerOrder = $this->db->selectOne("
			SELECT COALESCE(SUM(o.total_amount), 0) AS total_expenditures, COALESCE(COUNT(DISTINCT o.uid), 0) AS count_users, COALESCE(SUM(o.total_amount) / COUNT(DISTINCT o.uid), 0) AS average_customers_expenditures_per_order
			FROM " . DB_PREFIX . "orders AS o
			INNER JOIN " . DB_PREFIX . "users AS u ON u.uid = o.uid
			WHERE o.uid != 0 AND (o.status = 'Completed' AND o.payment_status = 'Received') AND u.login != 'ExpressCheckoutUser'
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
		");
		$averageCustomersExpendituresPerOrder = $queryCustomersExpendituresPerOrder['average_customers_expenditures_per_order'];

		// Get the average numbers of orders at the given time period where orders are fulfilled
		$queryNumbersOfOrders = $this->db->selectOne("
			SELECT COALESCE(COUNT(o.oid), 0) AS number_of_orders, COALESCE(COUNT(DISTINCT o.uid), 0) AS count_users, COALESCE(COUNT(o.oid) / COUNT(DISTINCT o.uid), 0) AS average_numbers_of_orders
			FROM " . DB_PREFIX . "orders AS o
			INNER JOIN " . DB_PREFIX . "users AS u ON u.uid = o.uid
			WHERE o.uid != 0 AND (o.status = 'Completed' AND o.payment_status = 'Received') AND u.login != 'ExpressCheckoutUser'
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
		");
		$averageNumbersOfOrders = $queryNumbersOfOrders['average_numbers_of_orders'];

		// Get the average across (n) customers value at the given time period. (a = $averageCustomersExpendituresPerOrder * $averageNumbersOfOrders)
		$averageCustomersValue = $averageCustomersExpendituresPerOrder * $averageNumbersOfOrders;

		// Get the average customer lifespan. During their time as a customer
		// Note: I don't use YEAR in TIMESTAMPDIFF since it will return 0 even if user already spent months as a customer
		$queryCustomersLifespan = $this->db->selectOne("
			SELECT COALESCE(SUM(TIMESTAMPDIFF(DAY, u.created_date, CURDATE())), 0) AS days_as_customer, COALESCE(COUNT(DISTINCT o.uid), 0) AS count_users, COALESCE(SUM(TIMESTAMPDIFF(DAY, u.created_date, CURDATE())) / COUNT(DISTINCT o.uid), 0) AS average_days_as_customers
			FROM " . DB_PREFIX . "orders AS o
			INNER JOIN " . DB_PREFIX . "users AS u ON u.uid = o.uid
			WHERE o.uid != 0 AND (o.status = 'Completed' AND o.payment_status = 'Received') AND u.login != 'ExpressCheckoutUser'
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
		");
		$averageDaysAsCustomers = $queryCustomersLifespan['average_days_as_customers'];

		// Convert days to years
		// Sample:
		// 700 days, 30 days in a month.
		// 700/30 = 23.33 months. 12 months in a year.
		// 23.33/12 = 1.944444... take the integer value (int)1.944444 to get 1 for the year.
		// 23.33%12= 11.33... months. You can cast it to an int as well to get 11
		// Result: 1.11 (1 year, 11 months)
		$months = $averageDaysAsCustomers / 30;
		$getYears = floor($months / 12);
		$getMonths = floor($months % 12);
		$averageYearsAsCustomers = $getYears . '.' . $getMonths;

		$simpleCustomerLifetimeValue = round((52 * $averageCustomersValue) * $averageYearsAsCustomers, 2);

		return $simpleCustomerLifetimeValue;
	}

	/**
	 * @param array $dateRange
	 * @return int
	 */
	public function getTotalNewCustomers($dateRange = array())
	{
		// Total new customers should be new express checkout users that have ordered plus users that have registered in the time period as per Mike's instruction
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$totalNewCustomers = $this->db->selectOne("
			SELECT COUNT(u.uid) AS count
			FROM " . DB_PREFIX . "users AS u
			INNER JOIN " . DB_PREFIX . "orders AS o ON u.uid = o.uid
			WHERE u.login != 'ExpressCheckoutUser'
			AND (o.status = 'Completed' AND o.payment_status = 'Received')
			AND DATE(u.created_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
		");

		return $totalNewCustomers['count'];
	}

	/**
	 * @param array $dateRange
	 * @return int
	 */
	public function getNewRegisteredCustomers($dateRange = array())
	{
		// Should just be new registered users for the time period as per Mike's instruction
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$newRegisteredCustomers = $this->db->selectOne("
			SELECT COUNT(u.uid) AS count
			FROM " . DB_PREFIX . "users AS u
			WHERE u.login != 'ExpressCheckoutUser'
			AND DATE(u.created_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
		");

		return $newRegisteredCustomers['count'];
	}

	/**
	 * @param int $offset
	 * @param int $limit
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getTopCustomers($offset = 0, $limit = 10, $dateRange = array())
	{
		// This should be the top 10 customers for the time period. If the user is a registered user, all of their orders should be combined into one value for that time period. This should be a list view, show their name
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$topCustomers = $this->db->selectAll("
			SELECT u.uid, u.fname, u.lname, ROUND(SUM(o.total_amount), 2) AS total_order_amount
			FROM " . DB_PREFIX . "users AS u
			INNER JOIN " . DB_PREFIX . "orders o ON u.uid = o.uid
			WHERE (u.login != 'ExpressCheckoutUser')
			AND (o.uid != 0 AND o.order_num != 0) AND (o.status = 'Completed' AND o.payment_status = 'Received')
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
			GROUP BY u.uid
			ORDER BY total_order_amount DESC
			LIMIT $offset, $limit
		");

		return $topCustomers;
	}

	/**
	 * @param int $offset
	 * @param int $limit
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getTopWholesalers($offset = 0, $limit = 10, $dateRange = array())
	{
		// Top wholesalers - Display top ten list of wholesale users for time period, show their name
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$topCustomers = $this->db->selectAll("
			SELECT u.uid, u.fname, u.lname, ROUND(SUM(o.total_amount), 2) AS total_order_amount
			FROM " . DB_PREFIX . "users u
			INNER JOIN " . DB_PREFIX . "orders o ON u.uid = o.uid
			WHERE u.login != 'ExpressCheckoutUser' 
			AND (o.status = 'Completed' AND o.payment_status = 'Received')
			AND u.level > 0 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
			GROUP BY u.uid
			ORDER BY total_order_amount DESC
			LIMIT $offset, $limit
		");

		return $topCustomers;
	}
}
<?php

/**
 * Class DataAccess_TaxRepository
 */
class DataAccess_TaxRepository extends DataAccess_BaseRepository
{
	/* Class Taxes  */
	protected $rates = null;

	/**
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		parent::__construct($db);
		$this->rates = new Taxes($db);
	}

	/**
	 * Get Tax Rate defaults
	 *
	 * @return array
	 */
	public function getRatesDefaults()
	{
		return array(
			'class_id' => 0,
			'zone_id' => 0,
			'stid' => 0,
			'coid' => 0,
			'rate_priority' => 0,
			'tax_rate' => 0.00000,
			'user_level' => '0',
			'rate_description' => '',
			'display_with_tax' => 0,
		);
	}
	
    /**
	* Get Tax Rate Class defaults
	*
	* @return array
	*/	
	public function getClassesDefaults()
	{
		return array(
			'class_name' => '',
			'class_description' => '',
			'key_name' => ''
		);
	}

	/**
	 * Get tax rates count
	 *
	 * @return mixed
	 */
	public function getRatesCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'tax_rates');

		return intval($result['c']);
	}

	/**
	 * Get Tax Rates list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getRatesList($start = null, $limit = null, $orderBy = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'rate_priority';

		return $this->db->selectAll('
			SELECT
				tax_rates.*,
				tax_classes.class_name,
				tax_classes.key_name,
				tax_zones.zone_name,
				tax_zones.display_with_tax
			FROM '.DB_PREFIX.'tax_rates AS tax_rates
			LEFT JOIN '.DB_PREFIX.'tax_classes AS tax_classes ON tax_classes.class_id = tax_rates.class_id
			LEFT JOIN '.DB_PREFIX.'tax_zones AS tax_zones ON tax_zones.zone_id = tax_rates.zone_id
			ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

   	/**
	 * @param $id
	 * @return bool
	 */
	public function getRateDataById($id)
	{
		$taxRate = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'tax_rates WHERE rate_id='.intval($id));

		if ($taxRate)
		{
			$taxRegion = $this->rates->getTaxZoneRegions($taxRate['zone_id']);
			
			if ($taxRegion)
			{
				$taxRate['zr_id'] = $taxRegion[0]['zr_id'];
				$taxRate['coid'] = $taxRegion[0]['coid'];
				$taxRate['stid'] = $taxRegion[0]['stid'];
			}
			
			$taxZone = $this->rates->getTaxZoneById($taxRate['zone_id']);

			if ($taxZone)
			{
				$taxRate['display_with_tax'] = $taxZone['display_with_tax'];
			}
			
		}

		return $taxRate;
	}

	/**
	 * Get Tax Rate Class data by class_id
	 *
	 * @param $id
	 * @return bool
	 */
	public function getTaxClassDataById($id)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'tax_classes WHERE class_id='.intval($id));
	}

	/**
	 * Get Tax Zones list
	 *
	 * @return mixed
	 */
	public function getTaxZonesList()
	{
		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'tax_zones ORDER BY zone_name');
	}

	/**
	 * Get Tax Rates Classes list
	 *
	 * @return mixed
	 */
	public function getTaxClassesList()
	{
		$result = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'tax_classes ORDER BY class_name');
		return $result;
	
	}

	/**
	 * @return array
	 */
	public function getTaxClassesOptions()
	{
		$list = $this->getTaxClassesList();
		$options = array();

		foreach ($list as $taxClass)
		{
			$options[$taxClass['class_id']] = $taxClass['class_name'];
		}

		return $options;
	}
		
	/**
	 * Delete tax rate by ids
	 *
	 * @param $ids
	 */
	public function deleteRate($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;
		
		foreach ($ids as $v)
		{
			$rate = $this->rates->getRateById($v);

			// TODO: why do we delete zone here?
			if ($rate['zone_id'])
			{
				$this->rates->deleteZone($rate['zone_id']);
			}

			$this->rates->deleteRate($v);
		}
	}
	
	
   /**
	* Delete all tax rates
	*
	*/
	public function deleteAllRates()
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'tax_zones');
		$this->db->query('DELETE FROM '.DB_PREFIX.'tax_zones_regions');
		$this->db->query('DELETE FROM '.DB_PREFIX.'tax_rates');
	}

   /**
	* Get tax rates count
	*
	* @return mixed
	*/

	public function getClassesCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'tax_classes');

		return intval($result['c']);
	}

	/**
	 * Get Tax Rates Classes list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getClassesList($start = null, $limit = null, $orderBy = null)
	{
		$tax_classes = array();
		
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'class_name';
		
		$tax_classes = $this->db->selectAll("SELECT ".DB_PREFIX."tax_classes.* FROM ".DB_PREFIX."tax_classes".
						' ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
		
 		return $tax_classes;
	}
	
	/**
	 * Delete tax class by ids
	 *
	 * @param $ids
	 */
	public function deleteClass($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		$this->db->query('DELETE FROM '.DB_PREFIX.'tax_classes WHERE class_id IN ('.implode(',', $ids).')');
	}

	/**
	 * Delete all tax classes
	 */
	public function deleteAllClasses()
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'tax_classes');
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['rate_description']) == '') $errors['rate_description'] = array('Please enter title');
			
			if ($data['tax_rate'] <= 0) $errors['tax_rate'] = array('Tax percent has to be > 0');
		}

		return count($errors) > 0 ? $errors : false;
	}
	
	
   /**
	* Get validation errors for tax rates classes
	*
	* @param $data
	* @param null $id
	*
	* @return array|bool
	*/
	public function getTaxClassesValidationErrors($data, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['class_name']) == '') $errors[] = array('field' => 'class_name', 'message' => 'Please enter title');
			
			if (trim($data['key_name']) == '')
			{
				$errors[] = array('field' => 'key_name', 'message' => 'Please enter Key / ID');
			}
			else
			{
				$db = $this->db;

				if ($db->selectOne('SELECT * FROM '.DB_PREFIX.'tax_classes WHERE key_name="'.$db->escape($data['key_name']).'"'))
				{
					if ($data['id'] !== $db->col['class_id'])
					{
						$errors[] = array('field' => 'key_name', 'message' => 'Class key must be unique within tax classes');
					}
				}	
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Get validation errors for tax rates classes delete action
	 *
	 * @param $data
	 * @return array|bool
	 */
	public function getDeleteClassesValidationErrors($data)
	{
		$db = $this->db;

		$errors = array();
		if (is_array($data))
		{
			foreach ($data as $k => $v)
			{
				if ($db->selectOne('SELECT count(pid) as c FROM '.DB_PREFIX.'products WHERE tax_class_id="'.intval($v).'"'))
				{
					if ($db->col['c'] > 0)
					{
						$errors['list_action'] = array('You can`t remove this tax class(es). '.$db->col['c'].' products linked with it!');
					}
				}
			}
			
			if (count($errors) == 0)
			{
				foreach ($data as $k => $v)
				{
					if ($db->selectOne('SELECT count(rate_id) as c FROM '.DB_PREFIX.'tax_rates WHERE class_id="'.intval($v).'"'))
					{
						if ($db->col['c'] > 0)
						{
							$errors['list_action'] = array('You can`t remove this tax class(es). '.$db->col['c'].' tax rate(s) linked with it!');
						}
					}
				}
			}
		}
		else if ($data == 'all')
		{
			$db = $this->db;
			if ($db->selectOne('SELECT count(pid) as c FROM '.DB_PREFIX.'products WHERE tax_class_id <> 0'))
			{
				if ( $db->col['c'] > 0)
				{
					$errors['list_action'] = array('You can`t remove all tax classes. '.$db->col['c'].' products linked with some of them!');
				}
			}
			
			if (count($errors) == 0)
			{
				if ($db->selectOne('SELECT count(rate_id) as c FROM '.DB_PREFIX.'tax_rates WHERE class_id <> 0'))
				{
					if ($db->col['c'] > 0)
					{
						$errors['list_action'] = array('You can`t remove all tax classes. '.$db->col['c'].' tax rate(s) linked with some of them!');
					}
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}



	/**
	 * Persis Tax Tax Rate
	 *
	 * @param $data
	 *
	 * @return bool|int
	 */
	public function persistRate($data)
	{
		$db = $this->db;
		
		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pid']) ? $data['pid'] : null);

		$db->reset();
		
		$country_data = $this->rates->getCountryById($data["coid"]);
		$state_data = $this->rates->getStateById($data["stid"]);

		$zone_name = ($country_data?$country_data["name"]:"All Countries")." - ".($state_data?$state_data["name"]:"All States");

		$display_with_tax = isset($data['display_with_tax']) && $data['display_with_tax'] ? 1 : 0;

		if ($data['id'])
		{
			$db->reset();
			$db->assignStr("zone_name", $zone_name);
			$db->assignStr('display_with_tax', $display_with_tax);
			$db->update(DB_PREFIX."tax_zones", "WHERE zone_id='".intval($data['zone_id'])."'");
			$db->reset();
			$db->assignStr("coid", intval($data["coid"]));
			$db->assignStr("stid", intval($data["stid"]));
			$db->update(DB_PREFIX."tax_zones_regions", "WHERE zone_id='".intval($data['zone_id'])."'");
		}
		else
		{
			$zone_data = array(
				"zone_name" => $zone_name,
				"zone_description" => "Zone for ".$zone_name,
				'display_with_tax' => $display_with_tax,
			);
			$data["zone_id"] = $added_zone_id = $this->rates->addZone($zone_data);

			$this->rates->addZoneRegion($added_zone_id, $data["coid"], $data["stid"]);
		}

//
//		if ( isset($data['user_level'][0])){
//			$data['user_level'][0] = '0';
//		}
//
		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$this->rates->saveRate($data['id'], $data);
			return intval($data['id']);
		}
		else
		{
			$rate_id = $this->rates->addRate($data);
			return $rate_id;
		}
	}
	
	/**
	 * Persis Tax Tax Rates Class
	 *
	 * @param $data
	 *
	 * @return bool|int
	 */
	public function persistClass($data)
	{

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pid']) ? $data['pid'] : null);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$save_data = array(
				'class_name' => array($data['id'] => $data['class_name']),
				'key_name' => array($data['id'] => $data['key_name'])
			);

			$this->rates->saveClassesData($save_data);

			return intval($data['id']);
		}
		else
		{
			$rate_id = $this->rates->addClass($data);
			return $rate_id;
		}
	}

	/**
	 * @return array
	 */
	public function getClassesIdsMap()
	{
		$db = $this->db;

		$tax_class_keys_hash = array();
		$db->query('SELECT class_id, key_name FROM '.DB_PREFIX.'tax_classes WHERE key_name <> ""');

		while ($db->moveNext())
		{
			$ck = explode(',', $db->col['key_name']);
			for ($i=0; $i<count($ck); $i++)
			{
				if (trim($ck[$i]) != '') $tax_class_keys_hash[strtolower(trim($ck[$i]))] = $db->col['class_id'];
			}
		}

		return $tax_class_keys_hash;
	}
}
<?php
/**
 *  Class DataAccess_ReportsOrdersRepository
 */
class DataAccess_ReportsOrdersRepository
{
	/** @var DB */
	protected $db;

	public function __construct(DB $db)
	{
		$this->db = $db;
	}

	/**
	 * Get order by date range
	 *
	 * @param $dateRange
	 * @return array
	 */
	public function getOrderByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$stat = array();

		$stat['total_order_count'] = 0;
		$d = $this->db->selectOne("SELECT COUNT(*) AS total_order_count FROM " . DB_PREFIX . "orders o 
			WHERE o.removed = 'No' 
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received' 
			AND DATE(o.placed_date) 
			BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");

		if (intval($d['total_order_count'])) {
			$stat['total_order_count'] = $d['total_order_count'];
		}
		return $stat;
	}

	/**
	 * Get order average amount by date range
	 *
	 * @param array $dateRange
	 * @return array
	 */
	public function getAverageOrderByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$stat = array();

		$stat['average_order_amount'] = 0;
		$d = $this->db->selectOne("SELECT COALESCE(AVG(o.total_amount - o.gift_cert_amount), 0) AS average_order_amount 
			FROM " . DB_PREFIX . "orders o 
			WHERE o.removed = 'No' 
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received' 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");

		if (floatval($d['average_order_amount']) > 0)
		{
			$stat['average_order_amount'] = $d['average_order_amount'];
		}
		return $stat;
	}

	/**
	 * Get checkout abandon by date range
	 *
	 * @param array $dateRange
	 * @return array
	 */
	public function getCheckoutAbandonByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$stat = array();
		$d = $this->db->selectOne("SELECT COUNT(*) AS total_order_range FROM " . DB_PREFIX . "orders o 
			WHERE o.removed = 'No' 
			AND o.status != 'New' 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");
		$stat['total_order_range'] = $d['total_order_range'];

		$d = $this->db->selectOne("SELECT COUNT(*) AS total_order_abandon FROM " . DB_PREFIX . "orders o 
			WHERE o.removed = 'No'
			AND o.status ='Failed'
			OR (o.status ='Abandon' AND o.placed_date <= DATE_SUB(NOW(), INTERVAL 24 HOUR)) 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");
		$stat['total_order_abandon'] = $d['total_order_abandon'];
		return $stat;
}

	/**
	 * Get conversion by date range
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getConversionByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$stat = array();
		$d = $this->db->selectOne("SELECT COUNT(*) AS total_order_conv FROM " . DB_PREFIX . "orders o 
			WHERE o.removed = 'No' 
			AND o.status != 'New' 
			AND DATE(o.placed_date) 
			BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");
		$stat['total_order_conv'] = $d['total_order_conv'];

		$d = $this->db->selectOne("SELECT COUNT(*) AS placed_order_conv FROM " . DB_PREFIX . "orders o
			WHERE o.removed = 'No'
			AND o.status NOT IN ('Failed', 'Abandon', 'New') 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");
		$stat['placed_order_conv'] = $d['placed_order_conv'];

		return $stat;
	}

	/**
	 * Get order by payment gateway by date
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getOrderByPaymentGatewayByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		return $this->db->selectAll("SELECT o.payment_method_id, pm.gateway_name, COUNT(1) AS gateway_order_count 
			FROM " . DB_PREFIX . "orders o
			RIGHT JOIN " . DB_PREFIX . "payment_methods pm ON(o.payment_method_id = pm.pid)
			WHERE o.removed = 'No' 
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received' 
			AND o.payment_method_id > 0
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "' 
			GROUP BY (o.payment_method_id)");
	}

	/**
	 * Get order on channel by date
	 *
	 * @param bool $onSite
	 * @param array $dateRange
	 * @return array
	 */
	public function getOrderByChannelByDate($onSite = true, $dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$stat = array();
		$d = $this->db->selectOne("SELECT COUNT(*) AS channel_order_count FROM " . DB_PREFIX . "orders o 
			WHERE o.removed = 'No'
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received' " . ($onSite ? "AND o.offsite_campaign_id <= 0" : "AND o.offsite_campaign_id > 0") . "
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");
		$stat['channel_order_count'] = $d['channel_order_count'];

		return $stat;
	}

	/**
	 * Get order list by status and date range
	 *
	 * @param array $params
	 * @param array $dateRange
	 * @param int $limit
	 * @return mixed
	 */
	public function getOrderListByStatusByDate($params = array(), $dateRange = array(), $limit = 0)
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		// orders need to be fulfill
		if (isset($params['fulfill']) && $params['fulfill'])
		{
			$whereStr = ' AND o.payment_status ="Received" AND o.fulfillment_status IN("pending", "partial")';
		}

		// orders abandon or failed
		if (isset($params['abandon']) && $params['abandon'])
		{
			$whereStr = ' AND o.status IN("Abandon", "Failed")';
		}

		return $this->db->selectAll("SELECT o.oid, o.order_num, o.status, o.fulfillment_status, o.total_amount - o.gift_cert_amount AS total_amount_without_gift_cert, o.shipping_city, st.name AS state_name, o.shipping_province, o.shipping_zip, co.iso_a2 AS country_iso2 
			FROM " . DB_PREFIX . "orders o
			INNER JOIN " . DB_PREFIX . "users u ON o.uid = u.uid AND u.removed = 'No'
			LEFT JOIN " . DB_PREFIX . "countries co ON (o.shipping_country = co.coid)
			LEFT JOIN " . DB_PREFIX . "states st ON (o.shipping_state = st.stid)
			WHERE o.order_num > 0 
			AND o.removed = 'No' {$whereStr} 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
			ORDER BY st.name ASC, o.shipping_province ASC LIMIT " . $limit);
	}

	/**
	 * Get order by country by date
	 *
	 * @param array $dateRange
	 * @return array
	 */
	public function getOrderListByCountryByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$dataResults = array();
		$resultRows = $this->db->selectAll("SELECT co.iso_a3, COUNT(*) AS order_count 
			FROM " . DB_PREFIX . "orders o
			LEFT JOIN " . DB_PREFIX . "countries co ON (o.shipping_country = co.coid)			
			WHERE NOT ISNULL(co.iso_a3) 
			AND o.removed = 'No' 
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received' 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "' GROUP BY co.iso_a3");
		foreach($resultRows as $resultRow)
		{
			$dataResults[$resultRow['iso_a3']] = $resultRow['order_count'];
		}

		return $dataResults;
	}

	/**
	 * Get order list by state/province then by date range
	 *
	 * @param array $dateRange
	 * @param int $limit
	 * @return mixed
	 */
	public function getOrderListByStateProvinceByDate($dateRange = array(), $limit = 0)
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		return $this->db->selectAll("SELECT COUNT(*) AS total_order_count, 
				SUM(o.total_amount - o.gift_cert_amount) AS total_amount_without_gift_cert, 
				o.shipping_state,
				COALESCE(st.name, o.shipping_province) AS state_province,				  
				o.shipping_zip, 
				co.iso_a3 AS country_iso3				 
			FROM " . DB_PREFIX . "orders o
			LEFT JOIN " . DB_PREFIX . "countries co ON (o.shipping_country = co.coid)
			LEFT JOIN " . DB_PREFIX . "states st ON (o.shipping_state = st.stid)
			WHERE o.order_num > 0 
			AND o.removed = 'No' 
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received'
			AND (o.shipping_state > 0 OR o.shipping_province <> '')			
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
			GROUP BY o.shipping_state, o.shipping_province, o.shipping_zip 
			ORDER BY state_province ASC LIMIT " . $limit);
	}

	/**
	 * Get total revenue
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getRevenueByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$d = $this->db->selectOne("SELECT COALESCE(SUM(o.total_amount - o.gift_cert_amount), 0) AS c FROM " . DB_PREFIX . "orders o 
			WHERE o.removed = 'No' 
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received' 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");
		return $d['c'];
	}

	/**
	 * Get product count by date range
	 *
	 * @param array $dateRange
	 * @return array
	 */
	public function getProductByDate($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$stat = array();
		$d = $this->db->selectOne("SELECT COALESCE(SUM(oc.quantity), 0) AS order_product_count FROM " . DB_PREFIX . "orders o
		LEFT JOIN  " . DB_PREFIX . "orders_content oc ON (o.oid = oc.oid)
		WHERE o.removed = 'No' 
		AND o.status = 'Completed' 
		AND o.payment_status = 'Received' 
		AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'");
		$stat['order_product_count'] = 0;
		if (intval($d['order_product_count']) > 0)
		{
			$stat['order_product_count'] = $d['order_product_count'];
		}
		return $stat;
	}
}
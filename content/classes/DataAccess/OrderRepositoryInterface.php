<?php

interface DataAccess_OrderRepositoryInterface
{
	public function getLastActiveOrders($dateFormat = '%m/%d/%Y - %r', $count = 7);

	public function resetLineItemShippingPrices($orderId);

	public function resetLineItemDiscounts($orderId);

	public function updateLineItemDiscount($orderId, $lineId, $discountAmount, $discountAmountWithTax, $promoDiscountAmount, $promoDiscountAmountWithTax);

	public function findExistingLineItemForProduct(ORDER $order, $productId, $options);

	public function getOrderItemsProducts($orderId, &$lineItemIds);

	public function deleteLineItems($orderId, &$lineItemIds);

	public function deleteAllLineItems($orderId);

	public function persistLineItem(ORDER $order, Model_LineItem $lineItemObj, $updateAttributes = false);

	public function getNextOrderNumber($orderId, $minOrderNumber);

	public function getShippingAddress($orderId);

	public function getLineItems($orderId);

	/**
	 * @param $orderLineItemId
	 *
	 * @return Model_LineItem
	 */
	public function getLineItemById($orderLineItemId);

	public function getOrderData($orderId);

	public function persistOrderData(ORDER $order);

	public function updateLineItemQuantityPricing($orderId, $lineItemId, $finalPrice, $priceWithTax, $freeShipping);

	public function getProductsGiftsData($orderId);

	/**
	 * Delete orders by ids
	 *
	 * @param $ids
	 * @return bool
	 */
	public function delete($ids);
}
<?php

class DataAccess_TaxRateRepository implements DataAccess_TaxRateRepositoryInterface
{
	protected $db;

	public function __construct(&$db)
	{
		$this->db = $db;
	}

	public function loadTaxRates($coid, $stid, $userLevel)
	{
		return $this->db->selectAll("
			SELECT ".DB_PREFIX."tax_rates.*, ".DB_PREFIX."tax_zones.display_with_tax
			FROM ".DB_PREFIX."tax_rates
			INNER JOIN ".DB_PREFIX."tax_zones ON (".DB_PREFIX."tax_rates.zone_id = ".DB_PREFIX."tax_zones.zone_id)
			INNER JOIN ".DB_PREFIX."tax_zones_regions ON (".DB_PREFIX."tax_zones_regions.zone_id = ".DB_PREFIX."tax_rates.zone_id)
			WHERE 
				(((".DB_PREFIX."tax_zones_regions.coid = '0') AND (".DB_PREFIX."tax_zones_regions.stid = '0')) OR
				((".DB_PREFIX."tax_zones_regions.coid = '".$coid."') AND (".DB_PREFIX."tax_zones_regions.stid = '0' OR ".DB_PREFIX."tax_zones_regions.stid = '".$stid."')))
				AND user_level  LIKE '%".intval($userLevel)."%'
			GROUP BY ".DB_PREFIX."tax_rates.rate_id
			ORDER BY ".DB_PREFIX."tax_rates.class_id, ".DB_PREFIX."tax_rates.rate_priority
		");
	}
}

interface DataAccess_TaxRateRepositoryInterface
{
	public function loadTaxRates($coid, $stid, $userLevel);
}
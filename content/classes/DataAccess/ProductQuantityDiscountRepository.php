<?php

/**
 * Class DataAccess_ProductQuantityDiscountRepository
 */
class DataAccess_ProductQuantityDiscountRepository extends DataAccess_BaseRepository
{
	/**
	 * Get defaults
	 *
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'id' => null,
			'qdt_id' => null,
			'pid' => '',
			'is_active' => 'No',
			'range_min' => '',
			'range_max' => '',
			'discount' => '',
			'discount_type' => 'percent',
			'free_shipping' => 'No',
			'apply_to_wholesale' => 'No'
		);
	}

	/**
	 * Get quantity discounts by product
	 *
	 * @param $productId
	 *
	 * @return mixed
	 */
	public function getListByProductId($productId)
	{
		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'products_quantity_discounts WHERE pid='.intval($productId).' ORDER BY range_min, range_max');
	}

	/**
	 * Get quantity discount by id
	 *
	 * @param $qdId
	 *
	 * @return array|bool
	 */
	public function getById($qdId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products_quantity_discounts WHERE qd_id='.intval($qdId));
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode)
	{
		$errors = array();
		if (is_array($data))
		{
			if (!isset($data['pid']) || intval($data['pid']) == 0)
			{
				$errors['pid'] = array('Cannot find product id');
			}

			if (!isset($data['range_min']) || trim($data['range_min']) == '')
			{
				$errors['range_min'] = array('Please enter min range');
			}

			if (!isset($data['range_max']) || trim($data['range_max']) == '')
			{
				$errors['range_max'] = array('Please enter max range');
			}

			if ((isset($data['range_min']) || trim($data['range_min']) != '') && (isset($data['range_max']) || trim($data['range_max']) != ''))
			{
				if (intval($data['range_min']) > intval($data['range_max']))
				{
					$errors['range_min'] = array('Min range should not be higher than max range');
				}
			}

			if (!isset($data['discount']) || trim($data['discount']) == '')
			{
				$errors['discount'] = array('Please enter discount');
			}
			elseif (floatval($data['discount']) == 0)
			{
				$errors['discount'] = array('Please enter valid discount');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Save changes
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['qd_id']) ? $data['qd_id'] : null);

		$db->reset();

		$db->assignStr('pid', $data['pid']);
		$db->assignStr('is_active', $data['is_active']);
		$db->assignStr('range_min', intval($data['range_min']));
		$db->assignStr('range_max', intval($data['range_max']));
		$db->assignStr('discount', floatval($data['discount']));
		$db->assignStr('discount_type', $data['discount_type']);
		$db->assignStr('free_shipping', $data['free_shipping']);
		$db->assignStr('apply_to_wholesale', $data['apply_to_wholesale']);

		if (!is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products_quantity_discounts', 'WHERE qd_id='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'products_quantity_discounts');
		}
	}

	/**
	 * Delete product quantity discounts
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		$this->db->query('DELETE FROM '.DB_PREFIX.'products_quantity_discounts WHERE qd_id IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * @param $pid
	 */
	public function deleteQuantityDiscountByProductId($pid)
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_quantity_discounts WHERE pid='.intval($pid));
	}

	/**
	 * Copy product quantity discounts
	 * @param $productId
	 * @param $newProductId
	 */
	public function copyProductQuantityDiscounts($productId, $newProductId)
	{
		$db = $this->db;
		$sql = 'INSERT INTO ' . DB_PREFIX . 'products_quantity_discounts (pid,' . implode(", ", self::getQuantityDiscountFields()) . ')
			SELECT
				'. $newProductId . ',
				' . implode(", ", self::getQuantityDiscountFields()) . '
			FROM ' . DB_PREFIX . 'products_quantity_discounts WHERE pid = '. $productId;
		$db->query($sql);
	}

	/**
	 * Product quantity discount fields
	 * @return array
	 */
	public static function getQuantityDiscountFields()
	{
		return array('is_active', 'range_min', 'range_max', 'discount',
			'discount_type', 'free_shipping', 'apply_to_wholesale');
	}
}
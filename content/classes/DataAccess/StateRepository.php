<?php

/**
 * Class DataAccess_StateRepository
 */
class DataAccess_StateRepository extends DataAccess_BaseRepository
{
	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'stid' => '',
			'coid' => '',
			'name' => '',
			'short_name' => ''
		);
	}

	/**
	 * @param null $coid
	 * @return int
	 */
	public function getCountByCountryId($coid = null)
	{
		$where = '1 = 1';
		if (intval($coid) > 0) $where = 'coid = ' . intval($coid);
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'states WHERE '.$where);

		return intval($result['c']);
	}

	/**
	 * @param null $orderBy
	 * @param null $coid
	 * @return mixed
	 */
	public function getStatesByCountryId($orderBy = null, $coid = null)
	{
		if (is_null($orderBy)) $orderBy = 'name';
		$where = '1 = 1';

		if (intval($coid) > 0) $where = 'coid = ' . intval($coid);

		return $this->db->selectAll(
			'SELECT * FROM '.DB_PREFIX.'states WHERE '.$where.' ORDER BY '.$orderBy
		);
	}

	protected $statesCache = array();

	/**
	 * Get state by id
	 *
	 * @param $id
	 * @param bool $cache
	 *
	 * @return array|bool
	 */
	public function getById($id, $cache = false)
	{
		if ($cache && isset($this->statesCache[$id])) return $this->statesCache[$id];

		$state = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'states WHERE stid='.intval($id));

		if ($cache) $this->statesCache[$id] = $state;

		return $state;
	}

	/**
	 * Get state by code
	 *
	 * @param $countryId
	 * @param $code
	 *
	 * @return array|bool
	 */
	public function getByCode($countryId, $code)
	{
		$db = $this->db;
		return $db->selectOne('SELECT * FROM '.DB_PREFIX.'states WHERE coid='.intval($countryId).' AND short_name LIKE "'.$db->escape($code).'"');
	}

	/**
	 * Get state by name
	 *
	 * @param $countryId
	 * @param $name
	 *
	 * @return array|bool
	 */
	public function getByName($countryId, $name)
	{
		$db = $this->db;
		return $db->selectOne('SELECT * FROM '.DB_PREFIX.'states WHERE coid='.intval($countryId).' AND name LIKE "'.$db->escape($name).'"');
	}

	/**
	 * Save data
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['stid']) ? $data['stid'] : null);

		$db->reset();
		$db->assign('coid', intval($data['coid']));
		$db->assignStr('name', $data["name"]);
		$db->assignStr('short_name', $data['short_name']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'states', 'WHERE stid='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			return $db->insert(DB_PREFIX.'states');
		}
	}

	/**
	 * @param $ids
	 */
	public function deleteByIds($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		$this->db->query('DELETE FROM '.DB_PREFIX.'states WHERE stid IN('.implode(',', $ids).')');
	}

	/**
	 * @param $coid
	 * @return bool
	 */
	public function deleteStatesByCountryID($coid)
	{
		if (intval($coid) < 1) return false;

		$this->db->query('DELETE FROM '.DB_PREFIX.'states WHERE coid NOT IN(1, 2) AND coid = '.intval($coid));
 
		return true;
	}

	/**
	 * @return bool
	 */
	public function deleteAllStates()
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'states');

		return true;
	}
}
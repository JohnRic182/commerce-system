<?php

/**
 * Class DataAccess_EndiciaRepository
 */
class DataAccess_EndiciaRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $orderId
	 * @return mixed
	 */
	public function getLabelsByOrderId($orderId)
	{
		global $settings;

		return $this->db->selectAll('SELECT *,
			DATE_FORMAT(date_created, "'.$settings['LocalizationDateTimeFormat'].'") as date_created
			FROM '.DB_PREFIX.'endicia_shippinglabels
			WHERE order_id='.intval($orderId).'
			ORDER BY slid DESC
		');
	}
	
	public function addShippingShippingLabel($orderId, $trackingNumber, $finalPostage, $response)
	{
		$this->db->reset();
		$this->db->assignStr('tracking_number', $trackingNumber);
		$this->db->assignStr('label_amount', $finalPostage);
		$this->db->assignStr('pic_number', isset($response['PIC']) ? $response['PIC'] : $response['CustomsNumber']);
		$this->db->assignStr('order_id', $orderId);
		$this->db->assign('date_created', 'now()');
		$labelId = $this->db->insert(DB_PREFIX.'endicia_shippinglabels');
		return $labelId;
	}
	
	public function updateShippingLabelsAndOrders($labelUrl, $labelId, $trackingNumber, $orderId)
	{
		$this->db->reset();
		$this->db->assignStr('label_url', $labelUrl);
		$this->db->update(DB_PREFIX.'endicia_shippinglabels', 'WHERE slid='.intval($labelId));

		$this->db->reset();
		$this->db->assignStr('shipping_tracking_number', $trackingNumber);
		$this->db->assignStr('shipping_tracking_number_type', "USPS");
		$this->db->update(DB_PREFIX.'orders', 'WHERE oid='.intval($orderId));
	}
	
	public function addCreditRequest($request_id, $serial_number, $postage_balance, $ascending_balance, $device_id, $getPostageStatusCode)
	{
		$this->db->reset();
		$this->db->assignStr('request_id', $request_id);
		$this->db->assignStr('serial_number', $serial_number);
		$this->db->assignStr('postage_balance', $postage_balance);
		$this->db->assignStr('ascending_balance', $ascending_balance);
		$this->db->assignStr('device_id', $device_id);
		$this->db->assignStr('request_status', $getPostageStatusCode);
		$this->db->insert(DB_PREFIX.'endicia_recreditrequests');
	}
	
	public function updateEndiciaShippingLabelSettings($newpass, $accountconfirm, $isaccounttest)
	{
		$this->db->query("UPDATE ".DB_PREFIX."settings SET value='".$this->db->escape($newpass)."' WHERE name='EndiciaShippingLabelsPassword'");
		$this->db->query("UPDATE ".DB_PREFIX."settings SET value='".$this->db->escape($accountconfirm)."' WHERE name='EndiciaShippingLabelsAccountID'");
		$this->db->query("UPDATE ".DB_PREFIX."settings SET value='".$this->db->escape($isaccounttest)."' WHERE name='EndiciaShippingLabelsTestMode'");
		$this->db->query("UPDATE ".DB_PREFIX."settings SET value='Yes' WHERE name='EndiciaShippingLabelsActive'");
	}
	
	public function getEndiciaShippingLabel($f)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'endicia_shippinglabels WHERE label_url="'.$this->db->escape($f).'"');
	}
}
<?php

/**
 * Class DataAccess_ProductsFeaturesValuesRelationsRepository
 */
class DataAccess_ProductsFeaturesValuesRelationsRepository extends DataAccess_BaseRepository
{
	/**
	 * Persist
	 *
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('product_feature_group_relation_id', $data['product_feature_group_relation_id']);
		$db->assignStr('feature_group_product_relation_id', $data['feature_group_product_relation_id']);
		$db->assignStr('product_feature_option_id', $data['product_feature_option_id']);

		return $db->insert(DB_PREFIX . 'products_features_values_relations');
	}

	/**
	 * Delete currently assigned product feature values in product feature
	 * @return bool
	 */
	public function delete($data)
	{
		/** @var DB $db */
		$db = $this->db;
		$db->query('
			DELETE FROM ' . DB_PREFIX . 'products_features_values_relations
			WHERE
				feature_group_product_relation_id = ' . intval($data['feature_group_product_relation_id']) . '
				AND product_feature_group_relation_id = ' . intval($data['product_feature_group_relation_id']) . '
				AND product_feature_option_id = ' .  intval($data['product_feature_option_id'])
		);
		return true;
	}
}
<?php
/**
 * 
 */
class DataAccess_UserRepository
{
	private static $instance;

	protected $db;
	protected $settings;

	public function __construct(DB $db, &$settings)
	{
		$this->db = $db;
		$this->settings = $settings;
	}

	public static function getInstance()
	{
		if (is_null(self::$instance))
		{
			global $settings, $db;
			self::$instance = new DataAccess_UserRepository($db, $settings);
		}

		return self::$instance;
	}

	/**
	 * Get user by id
	 *
	 * @param $userId
	 *
	 * @return USER
	 */
	public function getById($userId)
	{
		return USER::getById($this->db, $this->settings, $userId);
	}

	/**
	 * @param $userId
	 * @param bool $cache
	 * @return array
	 */
	public function getAllUserShippingAddresses($userId, $cache = true)
	{
		$query = '
			SELECT
				u.*,
				c.name AS country_name,
				IF(s.name IS NULL, u.province, s.name) AS province
			FROM '.DB_PREFIX.'users_shipping u
			LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = u.country
			LEFT JOIN '.DB_PREFIX.'states s ON s.stid = u.state
			WHERE uid='.intval($userId).' ORDER BY is_primary
		';

		$result = $this->db->query($query);

		$data = array();

		while ($this->db->moveNext($result))
		{
			$data[] = $this->db->col;
		}

		return $data;
	}

	/**
	 *
	 * @param type $count
	 * @return type 
	 */
	public function getLastActiveUsers($dateFormat = '%m/%d/%Y - %r', $count = 7)
	{
		$query = "
			SELECT *, DATE_FORMAT(session_date, '".$dateFormat."') AS la 
			FROM ".DB_PREFIX."users 
			WHERE login <> 'ExpressCheckoutUser' AND removed='No' 
			ORDER BY ".DB_PREFIX."users.uid DESC 
			LIMIT ".(intval($count))."
		";

		$result = $this->db->query($query);

		$data = array();

		while ($this->db->moveNext($result))
		{
			$data[] = $this->db->col;
		}

		return $data;
	}
}

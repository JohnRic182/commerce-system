<?php
/**
 * Class DataAccess_WholesaleSettingsRepository
 */
class DataAccess_WholesaleSettingsRepository extends DataAccess_BaseRepository
{
	protected $settings = array();

	public function __construct(DB $db, $settings)
	{
		if ( $settings )
		{
			$this->settings = $settings;
		}
		parent::__construct($db);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode = null, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{

			if (trim($data['WholesaleDiscount1']) == '')
			{
				$errors['WholesaleDiscount1'] = array(trans('wholesale_settings.wholesale_discount1_required'));
			}
			else
			{
				if (!is_numeric($data['WholesaleDiscount1']))
				{
					$errors['WholesaleDiscount1'] = array('Please enter a valid number');
				}
			}

			if (trim($data['WholesaleDiscount2']) == '')
			{
				$errors['WholesaleDiscount2'] = array(trans('wholesale_settings.wholesale_discount2_required'));
			}
			else
			{
				if (!is_numeric($data['WholesaleDiscount2']))
				{
					$errors['WholesaleDiscount2'] = array('Please enter a valid number');
				}
			}

			if (trim($data['WholesaleDiscount3']) == '')
			{
				$errors['WholesaleDiscount3'] = array(trans('wholesale_settings.wholesale_discount3_required'));
			}
			else
			{
				if (!is_numeric($data['WholesaleDiscount3']))
				{
					$errors['WholesaleDiscount3'] = array('Please enter a valid number');
				}
			}

			if (trim($data['MinOrderSubtotalLevel1']) == '')
			{
				$errors['MinOrderSubtotalLevel1'] = array(trans('wholesale_settings.min_order1_required'));
			}
			else
			{
				if (!is_numeric($data['MinOrderSubtotalLevel1']))
				{
					$errors['MinOrderSubtotalLevel1'] = array('Please enter a valid number');
				}
			}

			if (trim($data['MinOrderSubtotalLevel2']) == '')
			{
				$errors['MinOrderSubtotalLevel2'] = array(trans('wholesale_settings.min_order2_required'));
			}
			else
			{
				if (!is_numeric($data['MinOrderSubtotalLevel2']))
				{
					$errors['MinOrderSubtotalLevel2'] = array('Please enter a valid number');
				}
			}

			if (trim($data['MinOrderSubtotalLevel3']) == '')
			{
				$errors['MinOrderSubtotalLevel3'] = array(trans('wholesale_settings.min_order3_required'));
			}
			else
			{
				if (!is_numeric($data['MinOrderSubtotalLevel3']))
				{
					$errors['MinOrderSubtotalLevel3'] = array('Please enter a valid number');
				}
			}

		}

		return count($errors) > 0 ? $errors : false;
	}


}
<?php
/**
 * Class DataAccess_AvalaraTaxRepository
 */
class DataAccess_AvalaraTaxRepository extends DataAccess_BaseRepository
{
	/**
	 * @return array
	 */
	public function getTaxCodesOptions()
	{
		$options = array();
		$taxCodes = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes');

		foreach ($taxCodes as $taxCode)
		{
			$options[$taxCode['code']] = $taxCode['code'].': '.$taxCode['name'];
		}

		return $options;
	}
}
<?php

/**
 * Class DataAccess_CategoryProductsFeaturesGroupsRepository
 */
class DataAccess_CategoryProductsFeaturesGroupsRepository extends DataAccess_BaseRepository
{
	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'product_feature_group_id' => '',
			'cid' => ''
		);
	}

	/**
	 * @param $searchParams
	 * @param string $logic
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$where = array();
		$searchParams = is_array($searchParams) ? $searchParams : array();
		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$innerJoin =
			'INNER JOIN ' . DB_PREFIX . 'products_features_groups_relations AS pfgr ON cfg.product_feature_group_id = pfgr.product_feature_group_id ' .
			'INNER JOIN ' . DB_PREFIX . 'products_features_groups AS pfg ON pfgr.product_feature_group_id = pfg.product_feature_group_id ' .
			'INNER JOIN ' . DB_PREFIX . 'products_features AS pf ON pfgr.product_feature_id = pf.product_feature_id';

		if (isset($searchParams['cid']) && trim($searchParams['cid']) != '')
		{
			$where[] = 'cfg.cid like "%' . $this->db->escape(trim($searchParams['cid'])) . '%"';
		}

		if (isset($searchParams['product_feature_group_id']) && trim($searchParams['product_feature_group_id']) != '')
		{
			$where[] = 'cfg.product_feature_group_id like "%' . $this->db->escape(trim($searchParams['product_feature_group_id'])) . '%"';
		}

		$where[] = 'pfg.feature_group_active = 1 AND pf.feature_active = 1';

		$where = count($where) > 0 ? ' WHERE ' . implode( ' ' . $logic . ' ', $where) . ' ' : '';
		return $innerJoin . $where;
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param string $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 * @return mixed
	 */
	public function getList(
		$start = null,
		$limit = null,
		$orderBy = 'priority_asc',
		$searchParams = null,
		$columns = 'cfg.product_feature_group_id, cfg.cid, pfgr.product_feature_group_relation_id, pf.feature_name, pf.feature_id',
		$logic = 'AND'
	)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		switch ($orderBy)
		{
			case 'priority_asc' : $orderBy = 'cfg.priority'; break;
			case 'priority_desc' : $orderBy = 'cfg.priority DESC'; break;
			case 'product_feature_group_id_asc' : $orderBy = 'cfg.product_feature_group_id'; break;
			case 'product_feature_group_id_desc' : $orderBy = 'cfg.product_feature_group_id DESC'; break;
			case 'cid_asc' : $orderBy = 'cfg.cid'; break;
			case 'cid_desc' : $orderBy = 'cfg.cid DESC'; break;
		}

		return $this->db->selectAll(
			'SELECT ' . $columns . ' ' .
			'FROM ' . DB_PREFIX . 'categories_features_groups AS cfg ' . $this->getSearchQuery($searchParams, $logic) . ' ' .
			'ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get product feature groups by Category ID
	 *
	 * @param $categoryId
	 *
	 * @return array
	 */
	public function getGroupsByCategoryId($categoryId)
	{
		return $this->db->selectAll('
			SELECT cfg.cid, cfg.priority, pfg.*
			FROM ' . DB_PREFIX . 'categories_features_groups AS cfg
			INNER JOIN ' . DB_PREFIX . 'products_features_groups AS pfg ON (pfg.product_feature_group_id = cfg.product_feature_group_id)
			WHERE cfg.cid=' . intval($categoryId)
		);
	}

	/**
	 * Persist category feature group
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		$db->reset();
		$db->assignStr('product_feature_group_id', intval($data['product_feature_group_id']));
		$db->assignStr('cid', intval($data['cid']));
		return $db->insert(DB_PREFIX . 'categories_features_groups');
	}

	/**
	 * @param $cid
	 * @param $productsFeaturesGroups
	 */
	public function updateProductsFeaturesGroups($cid, $productsFeaturesGroups)
	{
		$db = $this->db;

		// get current ones
		$currentGroups = array();
		$result = $db->query('SELECT product_feature_group_id FROM ' . DB_PREFIX . 'categories_features_groups WHERE cid=' . intval($cid));
		while ($c = $db->moveNext($result)) $currentGroups[] = $c['product_feature_group_id'];

		// insert new ones
		$updatedGroups = array();
		foreach ($productsFeaturesGroups as $productFeatureGroupId)
		{
			$productFeatureGroupId = intval($productFeatureGroupId);

			if (!in_array($productFeatureGroupId, $currentGroups))
			{
				$this->persist(array('cid' => $cid, 'product_feature_group_id' => $productFeatureGroupId));
			}

			$updatedGroups[] = $productFeatureGroupId;
		}

		// remove groups that aren't reassigned
		$deletedGroups = array();
		foreach ($currentGroups as $currentGroupId)
		{
			if (!in_array($currentGroupId, $updatedGroups))
			{
				$deletedGroups[] = $currentGroupId;
			}
		}

		if (count($deletedGroups) > 0)
		{
			$this->delete($cid, $deletedGroups);
		}
	}

	/**
	 * Delete currently assigned product feature group in category
	 * @param $cid
	 * @param $productFeatureGroupId
	 * @return bool
	 */
	public function delete($cid, $productFeatureGroupId)
	{
		$ids = is_array($productFeatureGroupId) ? $productFeatureGroupId : array($productFeatureGroupId);
		if (count($ids) == 0) return false;

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		$this->db->query('
			DELETE FROM ' . DB_PREFIX . 'categories_features_groups
			WHERE cid = ' . intval($cid) . ' AND product_feature_group_id IN (' . implode(',', $ids) . ')
		');

		return true;
	}

	/**
	 * Update Priorities by Category Id
	 * @param $categoryId
	 * @param $productFeaturesPriorities
	 */
	public function updatePriorities($categoryId, $productFeaturesPriorities)
	{
		$db = $this->db;
		foreach($productFeaturesPriorities as $categoryFeatureId => $priorityValue)
		{
			$categoryFeatureId = intval($categoryFeatureId);
			$db->reset();
			$db->assignStr('product_feature_group_id', $categoryFeatureId);
			$db->assignStr('cid', $categoryId);
			$db->assignStr('priority', $priorityValue);
			$db->update(DB_PREFIX . 'categories_features_groups', 'WHERE cid=' . intval($categoryId) . ' AND product_feature_group_id=' . $categoryFeatureId);
		}
	}
}
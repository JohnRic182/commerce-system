<?php

interface DataAccess_PromoCodeRepositoryInterface
{
	public function getPromoCode($promoCode);

	public function getPromoCodeByPromoCampaignId($promoCampaignId);

	public function getPromoProducts($oid, $promoCampaignId);
}
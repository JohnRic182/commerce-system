<?php

/**
 * Class DataAccess_CountriesStatesRepository
 */
class DataAccess_CountriesStatesRepository extends DataAccess_BaseRepository
{
	/** @var DataAccess_CountryRepository|null  */
	private $countiesRepository = null;

	/** @var DataAccess_StateRepository|null  */
	private $statesRepository = null;

	/**
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		parent::__construct($db);

		// TODO: convert to dependency injection or get rid of subclasses
		$this->countiesRepository = new DataAccess_CountryRepository($db);
		$this->statesRepository= new DataAccess_StateRepository($db);
	}

	/**
	 * @param $code
	 * @return null
	 */
	public function getCountryByIso2Code($code)
	{
		return $this->countiesRepository->getCountryByIso2Code($code);
	}

	/**
	 * @return int
	 */
	public function getCountriesCount()
	{
		return $this->countiesRepository->getCount();
	}

	/**
	 * @return array
	 */
	public function getCountryDefaults()
	{
		return $this->countiesRepository->getDefaults();
	}

	/**
	 * @param $data
	 * @param $mode
	 * @param null $id
	 * @return array|bool
	 */
	public function getCountryValidationErrors($data, $mode, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['name']) == '')
			{
				$errors['name'] = array('Please enter Name');
			}

			if (trim($data['iso_a2']) == '')
			{
				$errors['iso_a2'] = array('Please enter ISO Alpha 2');
			}
			elseif (strlen(trim($data['iso_a2'])) != 2)
			{
				$errors['iso_a2'] = array('ISO Alpha 2 must be 2 characters');
			}

			if (trim($data['iso_a3']) == '')
			{
				$errors['iso_a3'] = array('Please enter ISO Alpha 3');
			}
			elseif (strlen(trim($data['iso_a3'])) != 3)
			{
				$errors['iso_a3'] = array('ISO Alpha 3 must be 3 characters');
			}

			if (trim($data['iso_number']) == '')
			{
				$errors['iso_number'] = array('Please enter ISO Number');
			}
			elseif(!validate(trim($data['iso_number']), VALIDATE_INT))
			{
				$errors['iso_number'] = array('ISO Number must be integer');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param $data
	 */
	public function deleteCountries($data)
	{
		if (is_array($data))
		{
			if (count($data) < 1) return;

			foreach($data as $id)
			{
				$this->countiesRepository->deleteCountryById($id);
			}
		}
		else if ($data == 'all')
		{
			$this->countiesRepository->deleteAllCountries();
		}
	}

	public function countriesActivation($data, $mode)
	{
		$mode = $mode == 'activate' ? 'Yes' : 'No';
		if (is_array($data))
		{
			if (count($data) < 1) return;

			foreach($data as $id)
			{
				$this->countiesRepository->countriesActivation(array('id' => $id, 'available' => $mode));
			}
		}
		else if ($data == 'all')
		{
			$this->countiesRepository->countriesActivation(array('mode' => 'all', 'available' => $mode));
		}
	}
	
	/**
	 * @param $data
	 * @return int
	 */
	public function persistCountry($data)
	{
		return $this->countiesRepository->persist($data);
	}

	/**
	 * @param $id
	 * @return type
	 */
	public function getCountryById($id)
	{
		return $this->countiesRepository->getCountryById($id);
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @return array
	 */
	public function getCountriesList($start = null, $limit = null, $orderBy = null)
	{
		$result = $this->countiesRepository->getCountries(false,$orderBy);

		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		if (!is_null($start) && !is_null($limit))
		{
			$result = array_slice($result, $start, $limit);
		}

		return $result;
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $coid
	 * @return array
	 */
	public function getStatesListByCountryId($start = null, $limit = null, $orderBy = null, $coid = null)
	{
		$result = $this->statesRepository->getStatesByCountryId($orderBy, $coid);
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		if (!is_null($start) && !is_null($limit))
		{
			$result = array_slice($result, $start, $limit);
		}

		return $result;
	}

	/**
	 * @param $coid
	 * @return int
	 */
	public function getStatesCountByCountryId($coid)
	{
		return $this->statesRepository->getCountByCountryId($coid);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getStateById($id)
	{
		return $this->statesRepository->getById($id);
	}

	/**
	 * Get state by code
	 *
	 * @param $countryId
	 * @param $code
	 *
	 * @return array|bool
	 */
	public function getStateByCode($countryId, $code)
	{
		return $this->statesRepository->getByCode($countryId, $code);
	}

	/**
	 * Get state by name
	 *
	 * @param $countryId
	 * @param $name
	 *
	 * @return mixed
	 */
	public function getStateByName($countryId, $name)
	{
		return $this->statesRepository->getByName($countryId, $name);
	}

	/**
	 * @return array
	 */
	public function getStateDefaults()
	{
		return $this->statesRepository->getDefaults();
	}

	/**
	 * @param $data
	 * @param $mode
	 * @param null $id
	 * @return array|bool
	 */
	public function getStateValidationErrors($data, $mode, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['name']) == '')
			{
				$errors['name'] = array('Please enter Name');
			}

			if (trim($data['short_name']) == '')
			{
				$errors['short_name'] = array('Please enter Code');
			}

			if (!isset($data['coid']) && intval($data['coid']) == 0)
			{
				$errors['coid'] = array('Invalid country id');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param $data
	 * @return mixed
	 */
	public function persistState($data)
	{
		return $this->statesRepository->persist($data);
	}

	/**
	 * @param $data
	 * @param null $coid
	 * @return bool
	 */
	public function deleteStates($data, $coid = null)
	{
		if (is_array($data))
		{
			if (count($data) < 1) return false;

			foreach($data as $id)
			{
				$this->statesRepository->deleteByIds($data);
			}
		}
		else if ($data == 'all')
		{
			if (intval($coid) < 1) return false;

			if(!$this->statesRepository->deleteStatesByCountryID($coid))
			{
				return false;
			}
		}

		return true;
	}
}
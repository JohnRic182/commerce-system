<?php

class DataAccess_FulfillmentRepository extends DataAccess_BaseRepository
{
	public function getFulfillments($orderId)
	{
		$fulfillmentsData = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'orders_fulfillment WHERE oid = '.intval($orderId));

		$fulfillments = array();
		foreach ($fulfillmentsData as $fulfillmentData)
		{
			$fulfillment = new Model_Fulfillment($fulfillmentData);
			$fulfillments[$fulfillment->getId()] = $fulfillment;
		}

		if (count($fulfillments) > 0)
		{
			$itemsData = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'orders_fulfillment_items WHERE oid = '.intval($orderId).' ORDER BY fulfillment_id, id');

			foreach ($itemsData as $item)
			{
				$fulfillmentItem = new Model_FulfillmentItem($item);
				if (isset($fulfillments[$fulfillmentItem->getFulfillmentId()]))
				{
					/** @var Model_Fulfillment $fulfillment */
					$fulfillment = $fulfillments[$fulfillmentItem->getFulfillmentId()];
					$fulfillment->addItem($fulfillmentItem);
				}
			}
		}

		return $fulfillments;
	}

	public function getFulfillmentsCount($orderId)
	{
		$count = $this->db->selectOne('SELECT COUNT(*) as c FROM '.DB_PREFIX.'orders_fulfillment WHERE oid = '.intval($orderId));
		return $count['c'];
	}

	public function getFulfillment($orderId, $id)
	{
		$fulfillmentData = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'orders_fulfillment WHERE oid = '.intval($orderId).' AND id = '.intval($id));

		if (!$fulfillmentData) return null;

		$fulfillment = new Model_Fulfillment($fulfillmentData);

		$itemsData = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'orders_fulfillment_items WHERE oid = '.intval($orderId).' AND fulfillment_id = '.intval($id).' ORDER BY fulfillment_id, id');

		foreach ($itemsData as $item)
		{
			$fulfillment->addItem(new Model_FulfillmentItem($item));
		}

		return $fulfillment;
	}

	public function save(Model_Fulfillment $fulfillment)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('status', $fulfillment->getStatus());
		$db->assignStr('tracking_company', $fulfillment->getTrackingCompany());
		$db->assignStr('tracking_number', $fulfillment->getTrackingNumber());
		$db->assign('updated_at', 'NOW()');

		if ($fulfillment->getId() < 1)
		{
			$db->assign('oid', $fulfillment->getOrderId());
			$db->assign('created_at', 'NOW()');
			$fulfillmentId = $db->insert(DB_PREFIX.'orders_fulfillment');

			$fulfillment->setId($fulfillmentId);
		}
		else
		{
			$db->update(DB_PREFIX.'orders_fulfillment', 'WHERE id = '.$fulfillment->getId());
		}
	
		if ($items = $fulfillment->getItems())
		{
			foreach ($items as $fulfillmentItem)
			{
				if (!isset($fulfillmentId))
				{
					$fulfillmentId = $fulfillmentItem->getFulfillmentId();
				}

				$fulfillmentItem->setFulfillmentId($fulfillmentId);

				$db->reset();
				$db->assign('quantity', $fulfillmentItem->getQuantity());

				if ($fulfillmentItem->getId() < 1)
				{
					$db->assign('oid', $fulfillment->getOrderId());
					$db->assign('fulfillment_id', $fulfillmentId);
					$db->assign('ocid', $fulfillmentItem->getLineItemId());
					$db->assign('updated_at', 'NOW()');
					$db->assign('created_at', 'NOW()');
					$fulfillmentItemId = $db->insert(DB_PREFIX.'orders_fulfillment_items');
					$fulfillmentItem->setId($fulfillmentItemId);
				}
				else
				{
					$db->update(DB_PREFIX.'orders_fulfillment_items', 'WHERE id = '.$fulfillment->getId());
				}
			}
		}
	}
}
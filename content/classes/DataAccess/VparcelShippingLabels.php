<?php
	class DataAccess_VparcelShippingLabels
	{
		/** @var DB */
		protected $db;
		protected $settings;

		public function __construct(DB $db, $settings)
		{
			$this->db = $db;
			$this->settings = $settings;
		}
		
		public function getLabelsByOrderId($id)
		{
			return $this->db->selectAll('
				SELECT *,
				DATE_FORMAT(date_created, "'.$this->settings['LocalizationDateTimeFormat'].'") as date_created
				FROM '.DB_PREFIX.'vparcel_shippinglabels
				WHERE order_id='.intval($id).'
				ORDER BY slid DESC
			');
		}
		
		public function persist($data, $params = null)
		{
			$db = $this->db;

			$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['slid']) ? $data['slid'] : null);
			
			if (isset($data['tracking_number']))
			{
				$db->assignStr('tracking_number', $data['tracking_number']);
			}

			if (isset($data['label_amount']))
			{
				$db->assignStr('label_amount', $data['label_amount']);
			}

			if (isset($data['pic_number'])){
				$db->assignStr('pic_number', $data['pic_number']);
			}

			if (isset($data['order_id']))
			{
				$db->assignStr('order_id', $data['order_id']);
			}

			if (is_null($data['id']))
			{
				$db->assign('date_created','now()');
			}

			$db->assignStr('label_url', isset($data['label_url'])?$data['label_url']:'');
			
			if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
			{
				$db->update(DB_PREFIX.'vparcel_shippinglabels', 'WHERE slid='.intval($data['id']));
				return intval($data['id']);
			}
			else
			{
				return $db->insert(DB_PREFIX.'vparcel_shippinglabels');
			}
		}

		public function deleteByPictNumber($pnumber)
		{
			$db = $this->db;
			$db->query('DELETE FROM '.DB_PREFIX.'vparcel_shippinglabels WHERE pic_number = "'.$db->escape($pnumber).'"');
		}

		public function SaveShippingLabel($imageData, $orderId, $labelId, $trackingNumber)
		{
			$img = file_get_contents($imageData);//get contents of file
			$fileName = 'images/vparcel/label-'.$orderId.'-'.$labelId.'-'.$trackingNumber.'.png';
			$success = file_put_contents($fileName, $img);
			return $fileName;
		}
	}
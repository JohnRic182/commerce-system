<?php

/**
 * Class DataAccess_ProductsRepository
 * TODO: implement interface?
 */
class DataAccess_ProductsRepository extends DataAccess_BaseRepository
{
	const MODE_ADD = 'add';
	const MODE_UPDATE = 'update';

	/**
	 * Get default
	 *
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'cid' => 0,
			'title' => '',
			'product_id' => '',
			'price' => '',
			'is_visible' => 'No',
			'priority' => 0,
			'meta_title' => '',
			'url_default' => '',
			'url_custom' => '',
			'meta_description' => '',
			'product_type' => Model_Product::TANGIBLE,
			'call_for_price' => 'No',
			'image_alt_text' => '',
			'image_url' => '',
			'image_location' => 'Local',
			'zoom_option' => 'global',
			'product_sku' => '',
			'product_gtin' => '',
			'youtube_link' => '',
			'manufacturer_id' => '0',
			'product_mpn' => '',
			'product_upc' => '',
			'products_location_id' => '0',
			'gift_quantity'	=> '1',
			'is_taxable' => 'Yes',
			'tax_class_id' => '1',
			'price2' => null,
			'free_shipping' => 'No',
			'product_level_shipping' => '0',
			'weight' => '0.00',
			'estimated_shipping_price' => -1,
			'min_order' => 1,
			'max_order' => 0,
			'dimension_width' => '',
			'dimension_length' => '',
			'dimension_height' => '',
			'is_hotdeal' => 'No',
			'is_home' => 'No',
			'is_stealth' => 0,
			'overview' => '',
			'description' => '',			

			'is_doba' => 'No',
			'is_locked' => 'No',
			'inventory_control' => 'No',
			'inventory_rule' => 'Hide',
			'stock' => 0,
			'stock_warning' => 0,
			'digital_product_file' => '',
			'price_level_1' => 0,
			'price_level_2' => 0,
			'price_level_3' => 0,
			'avalara_tax_code' => '',
			'exactor_euc_code' => '',
			'enable_recurring_billing' => '0',
			'recurring_billing_data' => '',

			// Data export properties
			'google_item_condition' => '',
			'google_product_type' => '',
			'google_availability' => '',
			'google_gender' => '',
			'google_age_group' => '',
			'google_color' => '',
			'google_size' => '',
			'google_material' => '',
			'google_pattern' => '',
			'google_online_only' => '',
			'amazon_id' => '',
			'amazon_id_type' => 0,
			'amazon_item_condition' => 0,
			'ebay_cat_id' => 0,
			'yahoo_path' => '',
			'pricegrabber_category' => '',
			'pricegrabber_item_condition' => 'New',
			'pricegrabber_part_number' => '',
			'nextag_category' => '',
			'nextag_item_condition' => '',
			'nextag_part_number' => '',

			'same_day_delivery' => '0',

			'search_keywords' => '',

			// Following values are composite, not directly from products table
			'categories' => array(),
			'product_families' => array(),
			'youtube_link' => '',
		);
	}

	/**
	 * Persist product
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		$db->reset();

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pid']) ? $data['pid'] : null);

		// common settings
		$db->assignStr('title', $this->getValue('title', $data, ''));
		$db->assignStr('product_id', $this->getValue('product_id', $data, ''));

		$price2 = $this->getValue('price2', $data, null);
		if ($price2 == '' || is_null($price2))
		{
			$price = normalizeNumber($this->getValue('price', $data, 0));
			$price2 = 'NULL';
		}
		else
		{
			// "price2" from form becoming price - real "price" from form becoming price2 for display purposes
			$price = normalizeNumber($this->getValue('price2', $data, 0));
			$price2 = normalizeNumber($this->getValue('price', $data, 0));
		}

		$db->assign('price', $price);
		$db->assign('price2', $price2);

		$db->assign('cid', isset($data['cid']) ? intval($data['cid']) : 0);

		$db->assignStr('is_visible', $this->getValue('is_visible', $data, 'No'));
		$db->assignStr('overview', $this->getValue('overview', $data, ''));
		$db->assignStr('description', $this->getValue('description', $data, ''));

		$db->assignStr('image_url', $this->getValue('image_url', $data, ''));
		$db->assignStr('image_alt_text', $this->getValue('image_alt_text', $data, ''));
		$db->assignStr('image_location', $this->getValue('image_location', $data, 'Local'));
		$db->assignStr('zoom_option', $this->getValue('zoom_option', $data, ''));

		// advanced settings
		$productType = $this->getValue('product_type', $data, Model_Product::TANGIBLE);
		if (isset($data['product_type'])) $db->assignStr('product_type', $productType);
		if (isset($data['products_location_id'])) $db->assignStr('products_location_id', intval($data['products_location_id']));

		if (isset($data['search_keywords'])) $db->assignStr('search_keywords', $data['search_keywords']);

		$db->assignStr('priority', isset($data['priority']) ? intval($data['priority']) : 0);

		// shipping
		$db->assign('weight', normalizeNumber($this->getValue('weight', $data, 0)));
		$db->assignStr('dimension_width', normalizeNumber($this->getValue('dimension_width', $data, '')));
		$db->assignStr('dimension_length', normalizeNumber($this->getValue('dimension_length', $data, '')));
		$db->assignStr('dimension_height', normalizeNumber($this->getValue('dimension_height', $data, '')));
		$db->assignStr('free_shipping', $productType != Model_Product::TANGIBLE ? 'Yes' : $this->getValue('free_shipping', $data, 'No'));
		$db->assignStr('product_level_shipping', $productType != Model_Product::TANGIBLE ? '0' : $this->getValue('product_level_shipping', $data, '0'));

		$db->assign('same_day_delivery', intval($this->getValue('same_day_delivery', $data, 0)) == 1 ? 1 : 0);

		// tax
		$db->assignStr('is_taxable', $this->getValue('is_taxable', $data, 'No'));
		$db->assign('tax_class_id', intval($this->getValue('tax_class_id', $data, '0')));
		$db->assignStr('avalara_tax_code', $this->getValue('avalara_tax_code', $data, ''));
		$db->assignStr('exactor_euc_code', $this->getValue('exactor_euc_code', $data, ''));

		$db->assignStr('call_for_price', $this->getValue('call_for_price', $data, 'No'));
		$db->assignStr('is_hotdeal', $this->getValue('is_hotdeal', $data, 'No'));
		$db->assignStr('is_home', $this->getValue('is_home', $data, 'No'));
		$db->assignStr('is_stealth', $this->getValue('is_stealth', $data, 0));

		$db->assign('manufacturer_id', intval($this->getValue('manufacturer_id', $data, 0)));
		$db->assignStr('product_mpn', $this->getValue('product_mpn', $data, ''));
		$db->assignStr('product_gtin', $this->getValue('product_gtin', $data, ''));
		$db->assignStr('product_sku', $this->getValue('product_sku', $data, ''));

		$db->assign('min_order', intval($this->getValue('min_order', $data, 1)));
		$db->assign('max_order', intval($this->getValue('max_order', $data, 0)));

		// digital download
		// TODO: finish file upload
		$db->assignStr('digital_product', $productType == Model_Product::DIGITAL ? 'Yes' : 'No');
		$db->assignStr('digital_product_file', $this->getValue('digital_product_file', $data, ''));

		// seo
		$db->assignStr('meta_title', $this->getValue('meta_title', $data, ''));
		$db->assignStr('meta_description', $this->getValue('meta_description', $data, ''));
		$db->assignStr('url_custom', $this->getValue('url_custom', $data, ''));

		// wholesale
		if (isset($data['price_level_1'])) $db->assign('price_level_1', normalizeNumber($data['price_level_1']));
		if (isset($data['price_level_2'])) $db->assign('price_level_2', normalizeNumber($data['price_level_2']));
		if (isset($data['price_level_3'])) $db->assign('price_level_3', normalizeNumber($data['price_level_3']));

		// export settings

		$db->assignStr('google_item_condition', $this->getValue('google_item_condition', $data));
		$db->assignStr('google_product_type', $this->getValue('google_product_type', $data));
		$db->assignStr('google_availability', $this->getValue('google_availability', $data));
		$db->assignStr('google_online_only', $this->getValue('google_online_only', $data));
		$db->assignStr('google_gender', $this->getValue('google_gender', $data));
		$db->assignStr('google_age_group', $this->getValue('google_age_group', $data));
		$db->assignStr('google_color', $this->getValue('google_color', $data));
		$db->assignStr('google_size', $this->getValue('google_size', $data));
		$db->assignStr('google_material', $this->getValue('google_material', $data));
		$db->assignStr('google_pattern', $this->getValue('google_pattern', $data));

		$db->assignStr('amazon_id', $this->getValue('amazon_id', $data));
		$db->assignStr('amazon_id_type', $this->getValue('amazon_id_type', $data, 0));
		$db->assignStr('amazon_item_condition', $this->getValue('amazon_item_condition', $data, 0));

		$db->assignStr('ebay_cat_id', $this->getValue('ebay_cat_id', $data, 0));

		$db->assignStr('yahoo_path', $this->getValue('yahoo_path', $data));

		$db->assignStr('youtube_link', $this->getValue('youtube_link', $data));

		$db->assignStr('pricegrabber_category', $this->getValue('pricegrabber_category', $data));
		$db->assignStr('pricegrabber_part_number', $this->getValue('pricegrabber_part_number', $data));
		$db->assignStr('pricegrabber_item_condition', $this->getValue('pricegrabber_item_condition', $data));

		$db->assignStr('nextag_category', $this->getValue('nextag_category', $data));
		$db->assignStr('nextag_part_number', $this->getValue('nextag_part_number', $data));
		$db->assignStr('nextag_item_condition', $this->getValue('nextag_item_condition', $data));

		$inventoryRule = $this->getValue('inventory_rule', $data);
		$stock = normalizeNumber($this->getValue('stock', $data, 0));
		if ($stock < 0) $stock = 0;

		$db->assignStr('inventory_control', $this->getValue('inventory_control', $data));
		$db->assignStr('inventory_rule', $inventoryRule);

		$db->assign('stock', $stock);
		$stockWarning = normalizeNumber($this->getValue('stock_warning', $data, 0));
		if ($stockWarning < 0) $stockWarning = 0;
		$db->assign('stock_warning', $stockWarning);

		$db->assignStr('gift_quantity', intval($this->getValue('gift_quantity', $data)));
		$db->assignStr('enable_recurring_billing', $this->getValue('enable_recurring_billing', $data));
		$db->assignStr('recurring_billing_data', $this->getValue('recurring_billing_data', $data));

//		$db->assignStr('is_locked', $this->getValue('is_locked', $data, 'No'));
//
		//TODO: Tax fields

		//TODO: Map more fields

		$db->assign("updated", "NOW()");

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products', 'WHERE pid='.intval($data['id']));
			return true;
		}
		else
		{
			$db->assign("added", "NOW()");
			return $db->insert(DB_PREFIX.'products');
		}
	}

	/**
	 * @param $productId
	 */
	public function touch($productId)
	{
		$this->db->query('UPDATE '.DB_PREFIX.'products SET updated=NOW() WHERE pid='.intval($productId));
	}

	/**
	 * @param $pid
	 * @param $rule
	 * @return bool|int
	 */
	public function updateProductInventory($pid, $rule)
	{
		if (!in_array($rule, array('No', 'Yes')))
		{
			$result = $this->db->selectOne('SELECT SUM(stock) AS product_stock FROM '.DB_PREFIX.'products_inventory WHERE pid='.intval($pid).' GROUP BY pid');
			$stock = intval($result['product_stock']);
			$this->db->query('UPDATE '.DB_PREFIX.'products SET stock='.$stock.' WHERE pid='.intval($pid));
			return $stock;
		}

		return false;
	}

	/**
	 * @param $productId
	 * @param $mode
	 * @param $categories
	 * @param $primaryCategoryId
	 */
	public function persistProductCategories($productId, $mode, $categories, $primaryCategoryId)
	{
		$db = $this->db;
		$pid = intval($productId);
		$cid = intval($primaryCategoryId);

		if ($mode == self::MODE_UPDATE) $db->query('DELETE FROM '.DB_PREFIX.'products_categories WHERE pid='.$pid);

		$query = 'INSERT INTO '.DB_PREFIX.'products_categories (pid, cid, is_primary) VALUES ('.$pid.','.$cid.',1)';

		if (in_array($primaryCategoryId, $categories)) unset($categories[array_search($primaryCategoryId, $categories)]);

		foreach ($categories as $categoryId)
		{
			$query .= ',('.$pid.','.intval($categoryId).',0)';
		}

		$db->query($query);
	}

	/**
	 * @param $productId
	 * @param $mode
	 * @param $productFamilies
	 */
	public function persistProductFamilies($productId, $mode, $productFamilies)
	{
		$db = $this->db;
		$pid = intval($productId);

		if ($mode == self::MODE_UPDATE)
		{
			$db->query('DELETE FROM '.DB_PREFIX.'products_families_dependencies WHERE pid='.$pid);
		}
		if (count($productFamilies) > 0)
		{
			$inserts = array();
			foreach ($productFamilies as $productFamily)
			{
				$pf_id = intval($productFamily);
				if ($pf_id > 0)
				{
					$inserts[] = '('.$pid.','.intval($productFamily).')';
				}
			}
			if (count($inserts) > 0)
			{
				$db->query('INSERT INTO '.DB_PREFIX.'products_families_dependencies (pid, pf_id) VALUES ' . implode(',', $inserts));
			}
		}
	}

	/**
	 * @param $productId
	 * @param $mode
	 * @param $shippingMethods
	 */
	public function persistProductShippingMethod($productId, $mode, $shippingMethods)
	{
		$db = $this->db;
		$pid = intval($productId);

		if (count($shippingMethods) > 0)
		{
			$inserts = array();

			foreach ($shippingMethods as $shippingMethodId => $shippingMethod)
			{
				$ssid = intval($shippingMethodId);

				if ($ssid > 0)
				{
					$inserts[] =
						'('.$pid.','.intval($ssid).', '.
						'"'.(isset($shippingMethod['active']) && $shippingMethod['active'] == '1' ? 'Yes' : 'No').'", '.
						'"'.(isset($shippingMethod['price']) && trim($shippingMethod['price']) != '' ? floatval($shippingMethod['price']) : '0.00').'")';
				}
			}

			if (count($inserts) > 0)
			{
				if ($mode == self::MODE_UPDATE) $db->query('DELETE FROM '.DB_PREFIX.'products_shipping_price WHERE pid='.$pid);
				$db->query('INSERT INTO '.DB_PREFIX.'products_shipping_price (pid, ssid, is_price, price) VALUES ' . implode(',', $inserts));
			}
		}
	}

	/**
	 * Delete product level shipping by product ID
	 *
	 * @param $productId
	 */
	public function deleteProductLevelShippingByProductId($productId)
	{
		$db = $this->db;
		$db->query('DELETE FROM ' . DB_PREFIX . 'products_shipping_price WHERE pid=' . intval($productId));
	}

	/**
	 * @param $key
	 * @param $values
	 * @param string $default
	 * @return string
	 */
	protected function getValue($key, $values, $default = '')
	{
		return isset($values[$key]) ? trim($values[$key]) : $default;
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['title']) == '') $errors['title'] = array('Please enter product name');

			if (trim($data['product_id']) == '')
			{
				$errors['product_id'] = array('Please enter product ID');
			}
			else
			{
				$db = $this->db;
				if ($db->selectOne('SELECT pid FROM '.DB_PREFIX.'products WHERE product_id="'.$db->escape($data['product_id'] ).'"'.(!is_null($id) ? ' AND pid <> '.intval($id) : '')))
				{
					$errors['product_id'] = array('Product ID must be unique within products');
				}
			}

			if (isset($data['category_name']) && $data['cid'] == 0 && trim($data['category_name']) == '')
			{
				$errors['category_name'] = array('Please enter category title');
			}
			else if (!isset($data['category_name']) && intval($data['cid']) == 0)
			{
				$errors['cid'] = array('Please select product category');
			}

			if (trim($data['price']) == '')
			{
				$errors['price'] = array('Please enter product price');
			}
			else
			{
				if (!is_numeric($data['price']))
				{
					$errors['price'] = array('Please enter a valid number');
				}
			}

			if (trim($data['price2']) != '')
			{
				if (!is_numeric($data['price2']))
				{
					$errors['price2'] = array('Please enter a valid number');
				}
			}

			if ($data['product_type'] == Model_Product::DIGITAL)
			{
				if (!isset($data['digital_product_file']) || !($data['digital_product_file']) || $data['digital_product_file'] == '')
				{
					if (!isset($data['digital_product_file_upload']) || !is_array($data['digital_product_file_upload']) || !isset($data['digital_product_file_upload']['error']))
					{
						$errors['digital_product_file'] = array('Digital product requires either a file uploaded or a path to an existing file in content/digital/');
					}
					else
					{
						if ($data['digital_product_file_upload']['error'] != UPLOAD_ERR_OK)
						{
							// TODO: add to languages, optimize / replace with messages array
							switch ($data['digital_product_file_upload']['error'])
							{
								case UPLOAD_ERR_INI_SIZE:
								case UPLOAD_ERR_FORM_SIZE:
								{
									$err = 'The uploaded digital product file exceeds file size limit';
									break;
								}
								case UPLOAD_ERR_PARTIAL:
								{
									$err = 'The uploaded digital product file was only partially uploaded';
									break;
								}
								case UPLOAD_ERR_NO_FILE:
								{
									$err = 'Digital product file was not uploaded';
									break;
								}
								case UPLOAD_ERR_NO_TMP_DIR:
								case UPLOAD_ERR_CANT_WRITE:
								{
									$err = 'Failed to save uploaded digital product file';
									break;
								}
								default : $err = 'Cannot save uploaded digital product file'; break;
							}

							$errors['digital_product_file_upload'] = array($err);
						}
					}
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Get product by id
	 *
	 * @param $id
	 * @param bool $asArray
	 *
	 * @return Model_Product|null
	 */
	public function getProductById($id, $asArray = false)
	{
		$productData = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products WHERE pid='.intval($id));

		return $productData ? ($asArray ? $productData : new Model_Product($productData)) : null;
	}

	/**
	 * Get product by product id
	 *
	 * @param $productId
	 * @param bool $asArray
	 *
	 * @return Model_Product|null
	 */
	public function getProductByProductId($productId, $asArray = false)
	{
		$productData = $this->db->selectOne('
			SELECT p.*
			FROM '.DB_PREFIX.'products p
			WHERE p.product_id="'.$this->db->escape($productId).'"
		');

		return $productData ? ($asArray ? $productData : new Model_Product($productData)) : null;
	}

	/**
	 * Get product categories
	 *
	 * @param int $productId
	 *
	 * @return array
	 */
	public function getProductCategoriesByProductId($productId)
	{
		$db = $this->db;
		$result = $db->query('SELECT * FROM '.DB_PREFIX.'products_categories WHERE pid='.intval($productId));

		$categories = array();

		while ($category = $db->moveNext($result))
		{
			$categories[$category['cid']] = $category;
		}

		return $categories;
	}

	/**
	 * @param $productId
	 * @return array
	 */
	public function getProductCategoriesIdsByProductId($productId)
	{
		$db = $this->db;
		$result = $db->query('SELECT * FROM '.DB_PREFIX.'products_categories WHERE pid='.intval($productId));

		$categories = array();
		while ($category = $db->moveNext($result))
		{
			$categories[$category['cid']] = $category['cid'];
		}

		return $categories;
	}

	/**
	 * @param $productId
	 * @return array
	 */
	public function getProductFamiliesIdsByProduct($productId)
	{
		$db = $this->db;

		$result = $db->query('SELECT * FROM '.DB_PREFIX.'products_families_dependencies WHERE pid='.intval($productId));

		$dependencies = array();
		while ($dependency = $db->moveNext($result))
		{
			$dependencies[$dependency['pf_id']] = $dependency['pf_id'];
		}

		return $dependencies;
	}

	/**
	 * Get product inventory
	 *
	 * @param $pid
	 * @param $inventoryList
	 *
	 * @return array|bool
	 */
	public function getProductsInventory($pid, $inventoryList)
	{
		return $this->db->selectOne("SELECT pi_id, product_subid, product_sku FROM ".DB_PREFIX."products_inventory WHERE pid=".intval($pid)." AND attributes_hash='".md5($inventoryList)."'");
	}

	/**
	 * Get product attributes
	 *
	 * @param $pid
	 * @return mixed
	 */
	public function getProductsAttributes($pid)
	{
		return $this->db->selectAll("
			SELECT * FROM ".DB_PREFIX."products_attributes
			WHERE pid = ".intval($pid)." AND is_active = 'Yes'
			ORDER BY priority, name, is_modifier
		");
	}

	/**
	 * Get inventory stock
	 *
	 * @param $inventoryId
	 * @return array|bool
	 */
	public function getInventoryStock($inventoryId)
	{
		return $this->db->selectOne("SELECT stock FROM ".DB_PREFIX."products_inventory WHERE pi_id=".intval($inventoryId));
	}

	/**
	 * @param $db
	 * @param $pid
	 * @return null
	 */
	public static function getPreviousProductId($db, $pid)
	{
		$db->query("SELECT MAX(pid) AS c FROM ".DB_PREFIX."products WHERE pid < ".intval($pid));
		$ret = $db->moveNext();
		if ($ret)
			return $ret["c"];

		return null;
	}

	/**
	 * @param $db
	 * @param $pid
	 * @return null
	 */
	public static function getNextProductId($db, $pid)
	{
		$db->query("SELECT MIN(pid) AS c FROM ".DB_PREFIX."products WHERE pid > ".intval($pid));
		$ret = $db->moveNext();
		if ($ret)
			return $ret["c"];

		return null;
	}

	/**
	 * @param $pid
	 * @param $quantity
	 * @return array|bool
	 */
	public function getProductsQuantityDiscounts($pid, $quantity)
	{
		return $this->db->selectOne("
			SELECT *
			FROM ".DB_PREFIX."products_quantity_discounts
			WHERE pid = ".intval($pid)." AND is_active = 'Yes' AND ".intval($quantity)." BETWEEN range_min AND range_max
			ORDER BY range_min
			LIMIT 1
			");
	}

	/**
	 * Get products count
	 *
	 * @param null $searchParams
	 * @param string $logic
	 *
	 * @return int
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		if(is_null($searchParams))
		{
			$result = $this->db->selectOne('SELECT COUNT(*) AS cnt FROM ' . DB_PREFIX . 'products p');
		}
		else
		{
			$result = $this->db->selectOne('SELECT COUNT(*) AS cnt FROM ' . DB_PREFIX . 'products p ' . $this->getSearchQuery($searchParams, $logic));
		}

		return intval($result['cnt']);
	}

	/**
	 * Get products list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 'p.*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'title';

		switch ($orderBy)
		{
			/**
			 * New order by params
			 */
			case 'product_id_asc' : $orderBy = 'p.product_id, p.title'; break;
			case 'product_id_desc' : $orderBy = 'p.product_id DESC, p.title DESC'; break;
			case 'title_asc' : $orderBy = 'p.title, p.product_id'; break;
			case 'title_desc' : $orderBy = 'p.title DESC, p.product_id DESC'; break;
			case 'is_visible_asc' : $orderBy = 'p.is_visible, p.title, p.product_id'; break;
			case 'is_visible_desc' : $orderBy = 'p.is_visible DESC, p.title DESC, p.product_id DESC'; break;
			case 'price_asc' : $orderBy = 'p.price, p.title, p.product_id'; break;
			case 'price_desc' : $orderBy = 'p.price DESC, p.title DESC, p.product_id DESC'; break;

			/**
			 * Old order by params
			 */
			case 'price_minmax': $orderBy = 'p.call_for_price, p.price, p.title'; break;
			case 'price_maxmin': $orderBy = 'p.call_for_price, p.price desc, p.title'; break;
			case 'product_id': $orderBy = 'p.product_id'; break;
			case 'priority': $orderBy = 'p.priority DESC, p.title'; break;
			case 'stock': $orderBy = 'p.inventory_control, p.stock, p.title'; break;
			case 'pid': $orderBy = 'p.pid'; break;
			default: $orderBy = 'p.title';	break;
		}

		return $this->db->selectAll('
			SELECT ' . $columns . '
			FROM '.DB_PREFIX.'products p
			' . $this->getSearchQuery($searchParams, $logic) . '
			GROUP BY p.pid
			ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * @param $searchStr
	 * @param int $skipProductId
	 * @param int $maxResults
	 * @return mixed
	 */
	public function lookUp($searchStr, $skipProductId = 0, $maxResults = 50)
	{
		$db = $this->db;
		$skipProductId = intval($skipProductId);

		return $db->selectAll('
			SELECT pid, product_id, title
			FROM ' . DB_PREFIX . 'products
			WHERE
				(title LIKE "%' . $db->escape($searchStr) . '%"
				OR product_id LIKE "%' . $db->escape($searchStr) . '%")
				' . ($skipProductId > 0 ? ' AND pid <> ' . $skipProductId : '') . '
				LIMIT 0, ' . intval($maxResults) . '
		');
	}

	/**
	 * @param $searchStr
	 * @param int $skipProductId
	 * @param int $maxResults
	 * @return mixed
	 */
	public function lookUpForOrderForm($searchStr, $skipProductId = 0, $maxResults = 50)
	{
		$db = $this->db;

		$products = $db->selectAll('
			SELECT pid, product_id, title, attributes_count, "attributes" AS ""
			FROM '.DB_PREFIX.'products
			WHERE
				is_visible = "Yes" AND product_id != "gift_certificate" AND
				(title LIKE "%'.$db->escape($searchStr).'%"
				OR product_id LIKE "%'.$db->escape($searchStr).'%")
				'.($skipProductId != '' ? ' AND pid <> '.intval($skipProductId) : '').'
				LIMIT 0, '.intval($maxResults).'
		');

		$pids = array();
		foreach ($products as $one)
		{
			$pids[] = $one['pid'];
		}

		$db->query('SELECT * FROM '.DB_PREFIX. 'products_attributes WHERE is_active = "Yes" AND (attribute_type = "select" OR attribute_type = "radio") AND pid IN ("' . implode('","', $pids) .'") ORDER BY pid, priority, name, is_modifier');

		$attributes = array();
		if ($db->numRows() > 0)
		{
			while ($db->moveNext())
			{

				$parsed = array();
				parseOptions($db->col['options'], 0, 0, $parsed, true);
				$db->col['options_parsed'] = $parsed;

				$attributes[$db->col['pid']][] = $db->col;
			}
			foreach ($products as $key => $val)
			{
				$products[$key]['attributes'] = isset($attributes[$val['pid']]) ? $attributes[$val['pid']] : '';
			}
		}

		return $products;
	}

	/**
	 * Update products variants stock
	 *
	 * @param $productDbId
	 */
	public function updateProductVariantsStock($productDbId)
	{
		$db = $this->db;

		$result = $db->query('SELECT stock FROM ' . DB_PREFIX . 'products_inventory WHERE pid=' . intval($productDbId));

		if ($db->numRows($result) > 0)
		{
			$stock = 0;
			while ($s = $db->moveNext($result)) $stock += $s['stock'];

			$db->reset();
			$db->assignStr('stock', $stock);
			$db->assignStr('stock_warning', 0);
			$db->update(DB_PREFIX.'products', 'WHERE pid=' . intval($productDbId));
		}
	}

	/**
	 * Update product attributes count
	 *
	 * @param $productDbId
	 */
	public function updateProductAttributesCount($productDbId)
	{
		$db = $this->db;
		$productDbId = intval($productDbId);
		$c = $db->selectOne('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'products_attributes WHERE pid=' . $productDbId);
		$db->query('UPDATE '.DB_PREFIX.'products SET attributes_count=' . intval($c['c']) . ' WHERE pid=' . $productDbId);
	}

	/**
	 * Update products attributes count
	 */
	public function updateProductsAttributesCount()
	{
		$db = $this->db;

		$tmpTableName = DB_PREFIX.'attr_count_table_'.substr(md5(time().rand(0,1000)), 4, 8);

		$db->reset();
		$db->query('DROP TABLE IF EXISTS '.$tmpTableName);
		$db->query('UPDATE '.DB_PREFIX.'products SET attributes_count=0');
		$db->query('CREATE TEMPORARY TABLE '.$tmpTableName.' (SELECT pid, COUNT(*) as attr_count FROM '.DB_PREFIX.'products_attributes WHERE is_active = "Yes" GROUP BY pid)');
		$db->query('UPDATE '.DB_PREFIX.'products p, '.$tmpTableName.' act SET p.attributes_count = act.attr_count WHERE p.pid = act.pid');
		$db->query('DROP TABLE IF EXISTS '.$tmpTableName);
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 * @param string $logic
	 *
	 * @return string
	 */
	public function getSearchQuery($searchParams, $logic = 'AND')
	{
		$searchParams = is_array($searchParams) ? $searchParams : array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$where = array();
		$whereAnd = array();

		$leftJoin = '';

		$searchStr = (isset($searchParams['search_str']) ? $searchParams['search_str'] : '');
		$searchStr = trim($searchStr);

		if ($searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$leftJoin = 'LEFT JOIN ' . DB_PREFIX . 'products_inventory as pi ON p.pid = pi.pid';
			$where[] =
				'((p.product_id like "%' . $searchStr . '%")' .
				'OR (p.title like "%' . $searchStr . '%")' .
				'OR (pi.product_subid like "%' . $searchStr . '%")' .
				'OR (p.search_keywords like "%' . $searchStr . '%"))';
		}

		if (isset($searchParams['product_id']) && trim($searchParams['product_id']) != '')
		{
			$where[] = 'p.product_id like "%'.$this->db->escape(trim($searchParams['product_id'])).'%"';
		}

		if (isset($searchParams['sub_id']) && trim($searchParams['sub_id']) != '')
		{
			$leftJoin = $leftJoin != '' ? '' : 'LEFT JOIN '.DB_PREFIX.'products_inventory as pi ON p.pid = pi.pid';
			$where[] = 'pi.product_subid like "%'.$this->db->escape(trim($searchParams['sub_id'])).'%"';
		}

		if (isset($searchParams['title']) && trim($searchParams['title']) != '')
		{
			$where[] = 'p.title like "%'.$this->db->escape(trim($searchParams['title'])).'%"';
		}

		if (isset($searchParams['search_keywords']) && trim($searchParams['search_keywords']) != '')
		{
			$where[] = 'p.search_keywords like "%'.$this->db->escape(trim($searchParams['search_keywords'])).'%"';
		}

		if (isset($searchParams['description']) && trim($searchParams['description']) != '')
		{
			$where[] = 'p.description like "%'.$this->db->escape(trim($searchParams['description'])).'%"';
		}

		if (isset($searchParams['cid']) && intval($searchParams['cid']) > 0)
		{
			$where[] = 'p.cid = '.intval($searchParams['cid']);
		}

		if (isset($searchParams['date_from']) && strlen($searchParams['date_from']) > 0 && isset($searchParams['date_to']) && strlen($searchParams['date_to']) > 0)
		{
			$where[] = 'DATE(p.added) BETWEEN "' . $searchParams['date_from'] . '" AND "' . $searchParams['date_to'] . '"';
		}

		if (isset($searchParams['added_before']) && $searchParams['added_before'] != '')
		{
			$d = new DateTime($searchParams['added_before']);
			$d->setTimezone(new DateTimeZone(date_default_timezone_get()));
			$where[] = 'p.added < "'.$d->format('Y-m-d H:i:s').'"';
		}

		if (isset($searchParams['added_after']) && $searchParams['added_after'] != '')
		{
			$d = new DateTime($searchParams['added_after']);
			$d->setTimezone(new DateTimeZone(date_default_timezone_get()));
			$where[] = 'p.added > "'.$d->format('Y-m-d H:i:s').'"';
		}

		if (isset($searchParams['updated_before']) && $searchParams['updated_before'] != '')
		{
			$d = new DateTime($searchParams['updated_before']);
			$d->setTimezone(new DateTimeZone(date_default_timezone_get()));
			$where[] = 'p.updated >= "'.$d->format('Y-m-d H:i:s').'"';
		}

		if (isset($searchParams['updated_after']) && $searchParams['updated_after'] != '')
		{
			$d = new DateTime($searchParams['updated_after']);
			$d->setTimezone(new DateTimeZone(date_default_timezone_get()));
			$where[] = 'p.updated >= "'.$d->format('Y-m-d H:i:s').'"';
		}

		if (isset($searchParams['status']) && trim($searchParams['status']) != '')
		{
			switch (trim($searchParams['status']))
			{
				case 'Yes':
				case 'No':
				{
					$where[] = 'p.is_visible = "'.$this->db->escape($searchParams['status']).'"';
					break;
				}
				case 'OutOfStock':
				{
					$where[] = 'p.inventory_control != "No" AND p.stock = 0';
					break;
				}
				case 'StockWarning':
				{
					$where[] = 'p.inventory_control != "No" AND p.stock <= p.stock_warning';
					break;
				}
			}
		}

		if (isset($searchParams['is_stealth']) && trim($searchParams['is_stealth']) != '')
		{
			$whereAnd[] = 'p.is_stealth = ' . intval($searchParams['is_stealth']);
		}

		return
			$leftJoin .
			' WHERE p.product_id <> "gift_certificate" ' .
			(count($where) > 0 ? ' AND (' . implode(' '  . $logic . ' ', $where) . ') ' : '') . ' ' .
			(count($whereAnd) > 0 ? ' AND (' . implode(' AND ', $whereAnd) . ') ' : '') . ' ';
	}

	/**
	 * Delete products by ids
	 *
	 * @param $ids
	 * @param $productImagesDir
	 * @param $productThumbsDir
	 * @param $productThumbs2Dir
	 * @param $productSecondaryImagesDir
	 *
	 * @return bool
	 */
	public function delete($ids, $productImagesDir = false, $productThumbsDir = false, $productThumbs2Dir = false, $productSecondaryImagesDir = false)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		if (count($ids) < 1) return false;

		$productsDeleted = true;

		foreach ($ids as $pid)
		{
			if (!$this->deleteProduct($pid, $productImagesDir, $productThumbsDir, $productThumbs2Dir, $productSecondaryImagesDir))
			{
				$productsDeleted = false;
			}
		}

		return $productsDeleted;
	}

	/**
	 * Delete product
	 *
	 * @param $pid
	 * @param $productImagesDir
	 * @param $productThumbsDir
	 * @param $productThumbs2Dir
	 * @param $productSecondaryImagesDir
	 *
	 * @return bool
	 */
	public function deleteProduct($pid, $productImagesDir = false, $productThumbsDir = false, $productThumbs2Dir = false, $productSecondaryImagesDir = false)
	{
		// TODO: add proper error handling

		$db = $this->db;

		if (!$productImagesDir) $productImagesDir = ImageUtility::PRODUCT_IMAGES_DIR;
		if (!$productThumbsDir) $productThumbsDir = ImageUtility::PRODUCT_THUMBS_DIR;
		if (!$productThumbs2Dir) $productThumbs2Dir = ImageUtility::PRODUCT_THUMBS2_DIR;
		if (!$productSecondaryImagesDir) $productSecondaryImagesDir = ImageUtility::PRODUCT_SECONDARY_IMAGES_DIR;;

		$fileUtils = FileUtils::getInstance();

		$pid = intval($pid);

		if ($pid == 0) return false;

		$db->query('SELECT * FROM ' . DB_PREFIX . 'products WHERE pid=' . $pid);

		if ($product = $db->moveNext())
		{
			//TODO: do event here

			$db->query('
				SELECT *
				FROM '.DB_PREFIX.'orders_content AS oc
				INNER JOIN '.DB_PREFIX.'recurring_profiles AS rp ON rp.initial_order_line_item_id = oc.ocid
				WHERE oc.pid='.$pid.'
				LIMIT 1
			');

			if ($db->numRows() > 0)
			{
//				self::setIsError(true);
//				self::setErrorMessage('Cannot delete product because it is used in recurring billing');
				return false;
			}

			$productId = strtolower(trim($product['product_id']));

			$db->query('SELECT filename FROM '.DB_PREFIX.'products_images WHERE pid='.$pid);

			if ($db->numRows() > 0)
			{
				while ($image = $db->moveNext())
				{
					$fileUtils->deleteImages($image['filename'], $productSecondaryImagesDir);
					//TODO: Is this always jpg?
					$fileUtils->unlink($productSecondaryImagesDir.'/thumbs/'.$image['filename'].'.jpg');
				}

				$db->query('DELETE FROM '.DB_PREFIX.'products_images WHERE pid='.$pid);
			}

			$db->query('UPDATE '.DB_PREFIX.'orders_content SET product_removed="Yes" WHERE pid='.$pid);
			$db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE pid='.$pid);
			$db->query('DELETE FROM '.DB_PREFIX.'products_categories WHERE pid='.$pid);
			$db->query('DELETE FROM '.DB_PREFIX.'products_shipping_price WHERE pid='.$pid);
			$db->query('DELETE FROM '.DB_PREFIX.'products_families_dependencies WHERE pid = '.$pid);
			$db->query('DELETE FROM '.DB_PREFIX.'products_families_content WHERE pid = '.$pid);
			$db->query('DELETE FROM '.DB_PREFIX.'products WHERE pid='.$pid);
			$db->query('DELETE FROM '.DB_PREFIX.'orders_content WHERE oid IN (SELECT * FROM (SELECT oid FROM '.DB_PREFIX.'orders WHERE status = "New" OR status = "Abandon") AS ax) AND pid ='.$pid);

			$filename = $fileUtils->stripFilename($productId);
			$fileUtils->deleteImages($filename, $productImagesDir);
			$fileUtils->deleteImages($filename, $productThumbsDir);
			$fileUtils->deleteImages($filename, $productThumbs2Dir);

			$event = new Events_ProductEvent(Events_ProductEvent::ON_DELETED);
			$event->setData('pid', $pid);
			$event->setData('product', $product);
			Events_EventHandler::handle($event);

			return true;
		}

//		self::setIsError(true);
//		self::setErrorMessage('Cannot find product by provided id');

		return false;
	}


	/**
	 * Delete products in the search
	 *
	 * @param $searchParams
	 * @param $productImagesDir
	 * @param $productThumbsDir
	 * @param $productThumbs2Dir
	 * @param $productSecondaryImagesDir
	 *
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams, $productImagesDir, $productThumbsDir, $productThumbs2Dir, $productSecondaryImagesDir)
	{
		$list = null;
		$start = 0;

		$success = true;
		do
		{
			$list = $this->getList($start, 500, 'pid', $searchParams, 'p.pid');

			if (count($list) > 0)
			{
				$pids = array();
				foreach ($list as $v)
				{
					$pids[] = $v['pid'];
				}
				$result = $this->delete($pids, $productImagesDir, $productThumbsDir, $productThumbs2Dir, $productSecondaryImagesDir);

				if (!$result) $success = false;
			}
		} while ($list !== null && count($list) > 0);

		return $success;
	}

	/**
	 * Delete test products
	 *
	 * @param $productImagesDir
	 * @param $productThumbsDir
	 * @param $productThumbs2Dir
	 * @param $productSecondaryImagesDir
	 *
	 * @return bool
	 */
	public function deleteTestProducts($productImagesDir, $productThumbsDir, $productThumbs2Dir, $productSecondaryImagesDir)
	{
		$db = $this->db;

		$success = true;
		$products = $db->selectAll('SELECT p.pid FROM '.DB_PREFIX.'products p WHERE p.product_id LIKE "test\_%"');
		if (count($products) > 0)
		{
			$pids = array();
			foreach ($products as $v)
			{
				$pids[] = $v['pid'];
			}

			$result = $this->delete($pids, $productImagesDir, $productThumbsDir, $productThumbs2Dir, $productSecondaryImagesDir);
			if (!$result) $success = false;
		}

		$categories = $db->selectAll('SELECT c.cid FROM '.DB_PREFIX.'catalog c WHERE c.key_name LIKE "test\_%" ORDER BY c.level DESC');
		if (count($categories) > 0)
		{
			$catRepository = new DataAccess_CategoryRepository($db);

			foreach ($categories as $c)
			{
				$catRepository->delete($c['cid']);
			}
		}

		$manufacturers = $db->selectAll('SELECT m.manufacturer_id FROM '.DB_PREFIX.'manufacturers m WHERE m.manufacturer_code LIKE "test_%"');
		if (count($manufacturers) > 0)
		{
			$manuRepository = new DataAccess_ManufacturerRepository($db);

			foreach ($manufacturers as $m)
			{
				$manuRepository->delete($m['manufacturer_id']);
			}
		}

		return $success;
	}

	/**
	 * Edit products by ids
	 *
	 * @param $ids
	 * @param array $updateData
	 *
	 * @return bool
	 */
	public function edit($ids, $updateData = array())
	{
		$ids = is_array($ids) ? $ids : array($ids);

		if (count($ids) < 1) return false;

		$productsUpdated = true;

		foreach ($ids as $pid)
		{
			if (!$this->editProduct($pid, $updateData))
			{
				$productsUpdated = false;
			}
		}

		return $productsUpdated;
	}

	/**
	 * Edit products in the search
	 *
	 * @param $searchParams
	 * @param $ids
	 * @param array $updateData
	 *
	 * @return bool
	 */
	public function editBySearchParams($searchParams, $ids, $updateData = array())
	{
		$productsUpdated = true;

		if (count($searchParams) > 0)
		{
			if (count($ids) > 0)
			{
				foreach ($ids as $pid)
				{
					$pid = $pid['pid'];

					if (!$this->editProduct($pid, $updateData))
					{
						$productsUpdated = false;
					}
				}
			}
		}

		return $productsUpdated;
	}

	/**
	 * Edit product
	 *
	 * @param $pid
	 * @param array $updateData
	 *
	 * @return bool
	 */
	public function editProduct($pid, $updateData = array())
	{
		$pid = intval($pid);

		if ($pid == 0 || !$updateData || !is_array($updateData) || count($updateData) == 0) return false;

		$db = $this->db;
		$product = $db->selectOne('SELECT price, price2 FROM ' . DB_PREFIX . 'products WHERE pid=' . $pid);

		if (!$product) return false;

		$fullPrice = $originalFullPrice = floatval($product['price2'] != null ? $product['price2'] : $product['price']);
		$salePrice = $originalSalePrice = $product['price2'] != null ? floatval($product['price']) : null;

		if (isset($updateData['is_change_product_price']) && intval($updateData['is_change_product_price']) == 1)
		{
			$fullPrice =
				$fullPrice +
				($updateData['state'] == 'increase' ? 1 : -1) *
				($updateData['product_price_discount_type'] == 'amount' ? floatval($updateData['product_price']) : ($fullPrice / 100 * floatval($updateData['product_price'])));

			if (isset($updateData['round_product_price']) && intval($updateData['round_product_price']) == 1) $fullPrice = floor($fullPrice) + 0.99;

			// TODO: check is this logic correct. For now reset to original price when getting too low price
			if ($fullPrice <= 0) $fullPrice = $originalFullPrice;
		}

		if (isset($updateData['is_change_product_sale_price']) && intval($updateData['is_change_product_sale_price']) > 0)
		{
			$salePrice =
				intval($updateData['is_change_product_sale_price']) == 1 ?
				($fullPrice - ($updateData['product_sale_price_discount_type'] == 'amount' ? floatval($updateData['product_sale_price']) : ($fullPrice / 100 * floatval($updateData['product_sale_price'])))) :
				null;

			if ($salePrice != null && isset($updateData['round_product_sale_price']) && intval($updateData['round_product_sale_price']) == 1) $salePrice = floor($salePrice) + 0.99;

			// TODO: check is this logic correct. For now remove sale price if it looks suspicious
			if ($salePrice != null && ($salePrice >= $fullPrice || $salePrice <= 0)) $salePrice = null;
		}

		// TODO: check is this logic correct. If sale price too high then we remove it
		if ($salePrice != null && $salePrice >= $fullPrice) $salePrice = null;

		$db->reset();
		$db->assign('price', $salePrice != null ? $salePrice : $fullPrice);
		$db->assign('price2', $salePrice != null ? $fullPrice : 'NULL');

		if ($updateData['category'] != 'no_change')	$db->assign('cid', intval($updateData['category']));

		if ($updateData['tax_class'] != 'no_change' && $updateData['tax_class'] != 'no_tax')
		{
			$db->assign('tax_class_id', intval($updateData['tax_class']));
			$db->assignStr('is_taxable', 'Yes');
		}
		else if ($updateData['tax_class'] == 'no_tax')
		{
			$db->assign('tax_class_id', 0);
			$db->assignStr('is_taxable', 'No');
		}

		if (intval($updateData['visibility']) == 1)
		{
			$db->assignStr('is_visible', 'Yes');
		}
		else if(intval($updateData['visibility']) == 2)
		{
			$db->assignStr('is_visible', 'No');
		}

		// Call For Price
		if (intval($updateData['call_for_price']) > 0 && intval($updateData['call_for_price']) == 1)
		{
			$db->assignStr('call_for_price', 'Yes');
		}
		else if ($updateData['call_for_price'] == 2)
		{
			$db->assignStr('call_for_price', 'No');
		}

		// Is Hot Deal
		if (intval($updateData['is_hot_deal']) > 0 && intval($updateData['is_hot_deal']) == 1)
		{
			$db->assignStr('is_hotdeal', 'Yes');
		}
		else if (intval($updateData['is_hot_deal']) > 0 && intval($updateData['is_hot_deal']) == 2)
		{
			$db->assignStr('is_hotdeal', 'No');
		}

		// Show Home Page
		if (intval($updateData['is_home_page']) > 0 && intval($updateData['is_home_page']) == 1)
		{
			$db->assignStr('is_home', 'Yes');
		}
		else if (intval($updateData['is_home_page']) > 0 && intval($updateData['is_home_page']) == 2)
		{
			$db->assignStr('is_home', 'No');
		}

		$db->assign('updated', 'NOW()');
		$db->update(DB_PREFIX.'products', 'WHERE pid=' . $pid);

		return true;
	}

	/**
	 * Get product ids map for bulk loader
	 */
	public function getProductsIdsMap()
	{
		$db = $this->db;

		$existingProducts = array();

		$db->query('SELECT pid, product_id FROM '.DB_PREFIX.'products WHERE product_id <> "gift_certificate"');
		while ($db->moveNext())
		{
			$existingProducts[strtolower(trim($db->col['product_id']))] = $db->col['pid'];
		}

		return $existingProducts;
	}

	/**
	 * @return bool
	 */
	public function hasTestProducts()
	{
		$db = $this->db;

		$count = $db->selectOne('SELECT COUNT(*) as c FROM '.DB_PREFIX.'products WHERE product_id LIKE "test\_%"');

		return $count['c'] > 0;
	}

	/**
	 * @param $url
	 * @param $pid
	 * @return int
	 */
	public function hasUrlDuplicate($url, $pid)
	{
		$escapedUrl = $this->db->escape($url);
		$db = $this->db;

		$result = $db->query('
			SELECT url_custom
			FROM '.DB_PREFIX.'products
			WHERE (url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'")
				AND pid != ' . intval($pid) . '

			UNION

			SELECT url_custom FROM '.DB_PREFIX.'manufacturers
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'catalog
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'pages
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"
		');

		return $db->numRows($result);
	}

	/**
	 * Copies our existing product
	 * @param $id
	 * @param $fields
	 * @return int
	 */
	public function copyProduct($id, $fields, $copyPrefix='copy')
	{
		$default = $fields;
		$prefixFields = array('title', 'product_id');
		$copySufix = $this->getCopySufix();


		foreach($prefixFields AS $prefixField)
		{
			// Array search will return false if field is not included in the selection
			// so we need to check the return and initialize the value when empty on (title and product_id)
			$key = array_search($prefixField, $fields);

			// Add prefix/suffix on copied products fields
			if ($key !== false)
			{
				$fields[$key] = 'CONCAT("' . $copyPrefix . '-",' . $prefixField . ', "-' . $copySufix .' ")';
			}
			else
			{
				$nextIndex = max(array_keys($default));
				$nextIndex++;

				$default[$nextIndex] = $prefixField;
				$fields[$nextIndex] = 'CONCAT("' . $copyPrefix .  '-' . $copySufix .'")';
			}
		}

		$db = $this->db;
		$sql = 'INSERT INTO ' . DB_PREFIX . 'products (is_visible, ' . implode(", ", $default) . ') SELECT "No", ' . implode(", ", $fields) . ' FROM ' . DB_PREFIX . 'products WHERE pid = '. $id;
		$db->query($sql);

		return $db->insertId();
	}

	/**
	 * Get the PID of the copied product row
	 *
	 * @return mixed
	 */
	public function getCopySufix()
	{
		$db = $this->db;
		$sql = 'SELECT AUTO_INCREMENT AS sufix FROM information_schema.TABLES WHERE TABLE_SCHEMA = "' . DB_NAME . '" AND TABLE_NAME = "' . DB_PREFIX . 'products"';
		$result = $db->selectOne($sql);
		return $result['sufix'];
	}

	/**
	 * This will return an array of doba products if has any
	 */
	public function getDobaProducts()
	{
		$db = $this->db;
		return $db->selectAll('SELECT pid, cid, title FROM ' . DB_PREFIX . 'products WHERE is_doba = 1 OR is_doba = \'Yes\'');
	}
}
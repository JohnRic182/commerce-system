<?php

/**
 * Class DataAccess_OauthTokenRepository
 */
class DataAccess_OauthTokenRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $data
	 * @param null $params
	 * @return bool|void
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('auth_code', $data['code']);
		$db->assignStr('client_id', $data['client_id']);

		if ($data['expires_at'] === null)
		{
			$db->assign('expires_at', 'null');
		}
		else
		{
			$db->assignStr('expires_at', $data['expires_at']);
		}

		$db->assignStr('scope', $data['scope']);
		$db->assignStr('token', $data['token']);
		//TODO: store redirect_uri
		$db->insert(DB_PREFIX.'oauth_tokens');
	}

	/**
	 * @param $token
	 * @return array|bool
	 */
	public function getByToken($token)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'oauth_tokens WHERE token = "'.$this->db->escape($token).'"');
	}

	/**
	 * @return string
	 */
	public function generateToken()
	{
		return $this->uuid_v4();
	}

	/**
	 * @return string
	 */
	protected function uuid_v4()
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),

			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}
}
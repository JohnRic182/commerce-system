<?php

/**
 * Class DataAccess_ProductShippingMethodRepository
 */
class DataAccess_ProductShippingMethodRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $productId
	 * @return mixed
	 */
	public function getProductShippingMethods($productId = null)
	{
		$db = $this->db;

		if ($productId == null)
		{
			return $db->selectAll('
				SELECT ss.*, "No" AS is_price, "" AS price
				FROM '.DB_PREFIX.'shipping_selected ss
				WHERE ss.carrier_id="custom" AND ss.method_id="product"
				GROUP BY ss.ssid
				ORDER BY ss.priority, ss.carrier_name
			');
		}
		else
		{
			return $db->selectAll('
				SELECT
					ss.*,
					IF (psp.is_price IS NULL, "No", psp.is_price) AS is_price,
					IF (psp.price IS NULL, "", psp.price) AS price
				FROM ' . DB_PREFIX . 'shipping_selected ss
				LEFT JOIN ' . DB_PREFIX . 'products_shipping_price psp ON psp.pid=' . intval($productId) . ' AND ss.ssid = psp.ssid
				WHERE ss.carrier_id="custom" AND ss.method_id="product"
				GROUP BY ss.ssid
				ORDER BY ss.priority, ss.carrier_name
			');
		}
	}
}
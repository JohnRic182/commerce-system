<?php

/**
 * Class DataAccess_ProductReviewRepository
 */
class DataAccess_ProductReviewRepository extends DataAccess_BaseRepository
{
	protected $settings = array();

	public function __construct(DB $db, $settings)
	{
		if ($settings)
		{
			$this->settings = $settings;
		}
		parent::__construct($db);
	}

	/**
	 * Get defaults
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'status' => 'pending'
		);
	}

	/**
	 * Get product review by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne(
			"SELECT SQL_CALC_FOUND_ROWS
				pr.id AS review_id, pr.pid, p.product_id, pr.user_id, pr.review_title, pr.review_text, pr.rating,  
				DATE_FORMAT(date_created, '".$this->settings["LocalizationDateTimeFormat"]."') date_created, p.title, u.email,
				pr.status
			FROM ".DB_PREFIX."products_reviews AS pr
			JOIN ".DB_PREFIX."products AS p ON p.pid = pr.pid
			JOIN ".DB_PREFIX."users AS u ON u.uid = pr.user_id
			WHERE pr.id=".intval($id)
		);
	}
	
	/**
	 * Get product reviews count
	 * @param null $searchParams
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c 
			FROM ' . DB_PREFIX . 'products_reviews AS pr
			JOIN ' . DB_PREFIX . 'products AS p ON p.pid = pr.pid
			JOIN ' . DB_PREFIX . 'users AS u ON u.uid = pr.user_id	
				' . $this->getSearchQuery($searchParams, $logic));
		return intval($result['c']);
	}

	/**
	 * Get product reviews list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param null $columns
	 * @param null $logic
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 'pr.*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'review_title';

		switch($orderBy)
		{
			case 'title_asc' : $orderBy = 'pr.review_title'; break;
			case 'title_desc' : $orderBy = 'pr.review_title DESC'; break;
			case 'review_asc' : $orderBy = 'pr.review_text'; break;
			case 'review_desc' : $orderBy = 'pr.review_text DESC'; break;
			case 'rating_asc' : $orderBy = 'pr.rating'; break;
			case 'rating_desc' : $orderBy = 'pr.rating DESC'; break;
			case 'reviewer_asc' : $orderBy = 'u.email'; break;
			case 'reviewer_desc' : $orderBy = 'u.email DESC'; break;
			case 'added_asc' : $orderBy = 'pr.date_created'; break;
			case 'added_desc' : $orderBy = 'pr.date_created DESC'; break;
			case 'status_asc' : $orderBy = 'pr.status'; break;
			case 'status_desc' : $orderBy = 'pr.status DESC'; break;
			default: $orderBy = 'pr.review_title';	break;
		}

		return $this->db->selectAll("
			SELECT SQL_CALC_FOUND_ROWS
				$columns,
				DATE_FORMAT(date_created, '" . $this->settings["LocalizationDateTimeFormat"] . "') date_created, p.title, u.email
			FROM ".DB_PREFIX."products_reviews AS pr
			JOIN ".DB_PREFIX."products AS p ON p.pid = pr.pid
			JOIN ".DB_PREFIX."users AS u ON u.uid = pr.user_id
			" . $this->getSearchQuery($searchParams, $logic) . "
			ORDER BY " . $orderBy . (!is_null($start) && !is_null($limit) ? (" LIMIT " . $start . ", " . $limit) : "")
		);
	}
	
	/**
	 * Build query params
	 *
	 * @param array $searchParams
	 * @param string $logic
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$where = array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$searchStr = trim($searchParams['search_str']);

		if (isset($searchParams['search_str']) && $searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$where[] =
				'((p.product_id like "%' . $searchStr . '%")' .
				' OR (pr.review_title like "%' . $searchStr . '%")' .
				' OR (pr.review_text like "%' . $searchStr . '%"))';
		}

		if (isset($searchParams['product_id']) && $searchParams['product_id'] != '') $where[] = 'p.product_id LIKE "%'.$this->db->escape($searchParams['product_id']).'%"';

		if (isset($searchParams['title']) && $searchParams['title'] != '') $where[] = 'p.title LIKE "%'.$this->db->escape($searchParams['title']).'%"';

		if (isset($searchParams['keyword']) && $searchParams['keyword'] != '')
			$where[] = '(pr.review_title  LIKE "%'.$this->db->escape($searchParams['keyword']).'%" OR pr.review_text  LIKE "%'.$this->db->escape($searchParams['keyword']).'%")';

		if (isset($searchParams['reviewer']) && $searchParams['reviewer'] != '')
			$where[] = '(u.email  LIKE "%'.$this->db->escape($searchParams['reviewer']).'%" OR u.lname LIKE "%'.$this->db->escape($searchParams['reviewer']).'%")';

		if (isset($searchParams['status']))
		{
			switch ($searchParams['status'])
			{
				case 'pending' : 
					$where[] = "status='pending'"; 
					break;
				case 'approved' : 
					$where[] = "status='approved'";
					break;
			}
		}
		return count($where) > 0 ? ' WHERE '.implode(' ' . $logic . ' ', $where).' ' : '';
	}
	
	
	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		// no validation here since we only allow to update status
		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Get validation errors for settings form
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationSettingsErrors($data, $mode, $id = null)
	{
		return false;
	}

	/**
	 * Persist product review
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['id']) ? $data['id'] : null);

		$db->reset();
		$db->assignStr('status', $data['status']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products_reviews', 'WHERE id='.intval($data['id']));
			$reviewId = intval($data['id']);
		}
		else
		{
			// TODO: it make no sense here - it creates review that has status only
			$this->db->assign("date_created", "NOW()");
			$reviewId = $db->insert(DB_PREFIX.'products_reviews');
		}

		// TODO: optimize this
		$result = $this->db->query('SELECT pid FROM '.DB_PREFIX.'products_reviews WHERE id='.intval($reviewId));
		while ($review = $this->db->moveNext($result))
		{
			$rating = $this->getProductRating($review['pid']);

			$this->db->reset();
			$this->db->assignStr('rating', $rating['rating']);
			$this->db->assignStr('reviews_count', $rating['total_reviews']);
			$this->db->update(DB_PREFIX.'products', 'WHERE pid='.intval($review['pid']));
		}

		return $reviewId;
	}

	/**
	 * Update product reviews status (all or selected)
	 *
	 * @param $ids
	 * @param $new_status
	 */
	public function updateStatus($ids, $new_status)
	{
		if (preg_match("/^(pending|approved|declined)/", $new_status))
		{
			if ($ids == 'all')
			{
				$where = " 1=1 ";
			}
			else
			{
				$ids = is_array($ids) ? $ids : array($ids);
				foreach ($ids as $key=>$value) $ids[$key] = intval($value);
				if (count($ids) < 1) return;
				$where = " id IN(".implode(',', $ids).") ";
			}

			$this->db->reset();
			$this->db->assignStr('status', $new_status);
			$this->db->update(DB_PREFIX."products_reviews", "WHERE " . $where);

			$reviews = $this->db->selectAll('SELECT pid FROM '.DB_PREFIX.'products_reviews WHERE '.$where);
			foreach ($reviews as $review)
			{
				$this->recalculateProductRating($review['pid']);
			}
		}
	}
	
	public function deleteReviews($ids = array())
	{
		$where = '';
		$pids = array();
		$ids = is_array($ids) ? $ids : array($ids);
		if (!empty($ids))
		{
			foreach ($ids as $key=>$value)
			{
				$ids[$key] = intval($value);
				// get product id
				$review = $this->db->selectOne('SELECT pid FROM '.DB_PREFIX.'products_reviews WHERE id='.intval($value));
				if (!in_array($review['pid'], $pids))
				{
					$pids[] = $review['pid'];
				}
			}
			if (count($ids) < 1) return;
			$where = " id IN(".implode(',', $ids).") ";
		}
		else
		{
			// delete all rows
			$reviews = $this->db->selectAll('SELECT id, pid FROM '.DB_PREFIX.'products_reviews');
			if (count($reviews) > 0)
			{
				$reviewIds = array();
				foreach ($reviews as $review)
				{
					$reviewIds[] = $review['id'];
					if (!in_array($review['pid'], $pids))
					{
						$pids[] = $review['pid'];
					}
				}
				$where = " id IN(".implode(',', $reviewIds).") ";
			}
		}
		if ($where != '')
		{
			// delete review(s)
			$this->db->query('DELETE FROM '.DB_PREFIX.'products_reviews WHERE '.$where);
			//update rating
			foreach($pids as $pid)
			{
				$this->recalculateProductRating($pid);
			}
			return true;
		}
		return false;
	}

	private function recalculateProductRating($id)
	{
		$rating = $this->getProductRating($id);
		$this->db->reset();
		$this->db->assignStr('rating', $rating['rating']);
		$this->db->assignStr('reviews_count', $rating['total_reviews']);
		$this->db->update(DB_PREFIX.'products', 'WHERE pid='.intval($id));
	}
	/**
	 * Get product rating
	 *
	 * @param $productId
	 * @return array|bool
	 */
	public function getProductRating($productId)
	{
		$result = $this->db->selectOne('
			SELECT AVG(rating) as rating, COUNT(0) as total_reviews
			FROM '.DB_PREFIX.'products_reviews
			WHERE pid = '.intval($productId).' AND status = "approved"
		');

		return $result;
	}


	/**
	 * Get product review settings
	 *
	 * @return array
	 */
	public function getAdvancedSettingsData()
	{
		return $this->settings;
	}

	/**
	 * Persist product review settings
	 *
	 * @param $data
	 * @return bool
	 */
	public function persistProductReviewSettings($data)
	{
		if (isset($data['productReviewSettings']))
		{
			$productReviewSettings = $data['productReviewSettings'];

			$productReviewSettings["ReviewsEnable"] = isset($productReviewSettings["ReviewsEnable"]) ? intval($productReviewSettings["ReviewsEnable"]): "0";
			$productReviewSettings["ReviewsAutoApprove"] = isset($productReviewSettings["ReviewsAutoApprove"]) ? intval($productReviewSettings["ReviewsAutoApprove"]): "0";
			$productReviewSettings["ReviewsAutoApprovePurchased"] = isset($productReviewSettings["ReviewsAutoApprovePurchased"]) ? intval($productReviewSettings["ReviewsAutoApprovePurchased"]): "0";

			$productReviewSettings["ReviewsAutoApproveRating"] = intval($productReviewSettings["ReviewsAutoApproveRating"]);

			if (is_array($productReviewSettings) && count($productReviewSettings) > 0)
			{
				$settingsRepository = new DataAccess_SettingsRepository($this->db, $this->settings);
				$settingsRepository->save($productReviewSettings);

				return true;
			}
		}

		return false;
	}
}
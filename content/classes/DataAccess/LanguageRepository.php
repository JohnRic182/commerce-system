<?php

/**
 * Class DataAccess_LanguageRepository
 */

// TODO: cleanup language repository

class DataAccess_LanguageRepository extends DataAccess_BaseRepository
{
	const LANGUAGES_DIR = 'content/languages/';
	
	private $legacy_lang_processor;
	private $lastPersistErrors;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		// TODO: get rig of legacy repo dependency here
		$this->legacy_lang_processor = new Language($db);
		$this->legacy_lang_processor->loadTemplate();
		
		parent::__construct($db);
	}
	
	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'name' => '',
			'code' => '',
			'is_active' => 'Yes',
			'is_default' => 'No',
			'template_lang' => '',
		);
	}

	/**
	 * Merges language into template (message category by message category & message by message, categories and messages missing in the language but existing in the template will be created in result array)
	 *
	 * @param array $template
	 * @param array $language
	 *
	 * @return array
	 */
	public function mergeLanguageIntoTemplate(array $template, array $language)
	{
		$merged = array();

		foreach ($template as $msg_cat => $template_messages)
		{
			$merged[$msg_cat] = array_merge($template_messages, isset($language[$msg_cat])? $language[$msg_cat] : array());

			foreach ($merged[$msg_cat] as $key => $value)
			{
				if (is_string($value))
				{
					$merged[$msg_cat][$key] = trim($value);
				}
			}
		}

		foreach ($language as $msg_cat => $language_messages)
		{
			if (!isset($template[$msg_cat])) $merged[$msg_cat] = $language_messages;
		}

		return $merged;
	}
	
	/**
	 * Get Language by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		$lang = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'languages WHERE language_id='.intval($id));
		
		$template = $this->legacy_lang_processor->templates;
		$lang['template'] = $template;
		
		$messages = $this->legacy_lang_processor->getDictionary($lang['code']);
		$lang['msg'] = $this->mergeLanguageIntoTemplate($template, $messages);
		
		return $lang;
	}
	
	/**
	 * Get language count
	 *
	 * @param array|null $searchParams
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null)
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'languages');
		
		return intval($result['c']);
	}
	
	/**
	 * Get languages list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';
 		
		return $this->db->selectAll(
			'SELECT * FROM '.DB_PREFIX.'languages ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}
	
	/**
	 * Get existing languages for "Use as a template" drop-down
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 *
	 * @return mixed
	 */
	// TODO: do we need all these params here?
	public function getExistingLanguages($start = null, $limit = null, $orderBy = null, $searchParams = null)
	{
		$langs = $this->getList();
		$opts = array();
		foreach ($langs as $lang)
		{
			$opts[$lang['code']] = $lang['name'];
		}

		return $opts;
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '') $errors['name'] = array(trans('language.add_name_required'));
			if (trim($data['code']) == '') $errors['code'] = array(trans('language.add_code_required'));

			if ($mode == 'add')
			{
				// language code must be unique
				if ($this->db->selectOne('SELECT 1 FROM '.DB_PREFIX."languages WHERE code = '" . $this->db->escape( escapeFileName(strtolower($data['code']))) . "'")) $errors['code'] = array('Language code must be unique');

				// if adding language, should be either based on template, or custom language file uploaded
				if ($data['template_lang'] == '' and !$data['template_file'])
				{
					$errors['template'] = array('You must use either existing language as template or upload language file');
				}
				elseif ($data['template_lang'] != '' and $data['template_file'])
				{
					$errors['template'] = array('Either use existing language as template or upload language file, not both at the same time');
				}
				elseif ($data['template_lang'] == '' and $data['template_file'])
				{
					$original_file_name = $data['template_file']['name'];
					if (!preg_match('/^(.+)\.php$/i', $original_file_name))
					{
						$errors['template_file'] = array('Language file must be PHP file');
					}
				}
			}
			elseif ($mode == 'update')
			{
				if ($this->db->selectOne('SELECT 1 FROM '.DB_PREFIX."languages WHERE code = '" . $this->db->escape(escapeFileName(strtolower($data['code']))) . "' AND language_id <> " . intval($id)))
				{
					$errors['code'] = array('Language code must be unique');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist language
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		$this->lastPersistErrors = array();

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['language_id']) ? $data['language_id'] : null);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$oldCodeData = $db->selectOne('SELECT code FROM '.DB_PREFIX.'languages WHERE language_id='.intval($data['id']));
		}

		$db->reset();

		if (isset($data['is_active'])) $db->assignStr('is_active', $data['is_active'] == 'Yes' ? 'Yes' : 'No');
		if (isset($data['is_default'])) $db->assignStr('is_default', $data['is_default'] == 'Yes' ? 'Yes' : 'No');

		$db->assignStr('code', isset($data['code']) ? escapeFileName(strtolower($data['code'])) : '');
		$db->assignStr('name', isset($data['name']) ? $data['name'] : '');

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			if (isset($data['msg']) and is_array($data['msg']))
			{
				$template = $this->legacy_lang_processor->templates;
				$lang = $this->mergeLanguageIntoTemplate($template, $data['msg']);
				
				// TODO: improve this (recreates lang file for each message category, should be one file write operation
//				foreach ($lang as $message_cat => $messages)
//				{
//					$this->legacy_lang_processor->saveMessages($data['id'], $message_cat, $messages);
//				}
				$this->legacy_lang_processor->saveFile($data['id'], $lang);
			}

			if (isset($oldCodeData['code']) && $oldCodeData['code'] != $data['code'])
			{
				$this->legacy_lang_processor->renameLanguage($oldCodeData['code'], $data['code']);
			}

			$db->update(DB_PREFIX.'languages', 'WHERE language_id='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			$res = $db->insert(DB_PREFIX.'languages');
			
			if ($data['template_lang'] != '')
			{
				if (!@copy(self::LANGUAGES_DIR.escapeFileName(strtolower($data['template_lang'])).'.php', self::LANGUAGES_DIR.escapeFileName(strtolower($data['code'])).".php"))
				{
					$this->lastPersistErrors = array('Cannot copy language file. Please check languages folder write permissions.');
					if ($res) $db->query("DELETE FROM ".DB_PREFIX."languages WHERE language_id=".intval($data['id']));
					return false;
				}
			}
			elseif ($data['template_file'])
			{
				$fileUploadService = new Admin_Service_FileUploader();

				if (! $fileUploadService->moveUploaderFile($data['template_file'], self::LANGUAGES_DIR, escapeFileName(strtolower($data['code'])).'.php'))
				{
					$this->lastPersistErrors = array('Cannot store language file. Please check languages folder write permissions.');
					if ($res) $db->query("DELETE FROM ".DB_PREFIX."languages WHERE language_id=".intval($data['id']));

					return false;
				}
			}
			
			return $res;
		}
	}

	/**
	 * Gets error messages from last persist if it wasn't successful
	 *
	 * @return mixed
	 */
	public function getLastPersistErrors()
	{
		return $this->lastPersistErrors;
	}

	/**
	 * Delete language(s) by ids
	 *
	 * @param $ids
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);
		foreach ($ids as $key=>$value) $ids[$key] = intval($value);
		if (count($ids) < 1) return false;
		
		foreach ($ids as $id)
		{
			if ($this->legacy_lang_processor->languages[$id]['is_default'] != 'Yes')
			{
				$this->legacy_lang_processor->deleteLanguage($id);
			}
		}
		return true;
	}
	
	/**
	 * Set selected language as default and make it visible
	 * @param $id
	 */
	public function setDefaultLanguage($id)
	{
		$this->legacy_lang_processor->setDefaultLanguage($id);
	}

	/**
	 * Delete all languages but default
	 */
	public function deleteBySearchParams()
	{
		$languages = $this->getList();

		foreach ($languages as $language)
		{
			if ($language['is_default'] != 'Yes')
			{
				$this->legacy_lang_processor->deleteLanguage($language['language_id']);
			}
		}
	}
}
<?php
/**
 * Class DataAccess_ProductsFeaturesGroupsFeaturesRepository
 */
class DataAccess_ProductsFeaturesGroupsFeaturesRepository extends DataAccess_BaseRepository
{
	/**
	 * List product feature group features by feature group ID
	 *
	 * @param $featureGroupId
	 * @return array
	 */
	public function getListByFeatureGroupId($featureGroupId)
	{
		return $this->db->selectAll('
			SELECT pfgr.*, pf.feature_name, pf.feature_id
			FROM ' . DB_PREFIX . 'products_features_groups_relations AS pfgr
			INNER JOIN ' . DB_PREFIX . 'products_features AS pf ON (pf.product_feature_id = pfgr.product_feature_id)
			WHERE pfgr.product_feature_group_id=' . intval($featureGroupId) .'
			ORDER BY pfgr.priority, pf.feature_name, pf.feature_id
		');
	}

	/**
	 * @param $featureGroupId
	 * @param $productFeatureId
	 * @param $priorityValue
	 * @return bool
	 */
	public function updateFeaturesPriority($featureGroupId, $productFeatureId, $priorityValue)
	{
		$db = $this->db;

		$db->reset();
		$db->assignStr('priority', $priorityValue);
		$db->update(
			DB_PREFIX . 'products_features_groups_relations',
			'WHERE product_feature_id=' . intval($productFeatureId) . ' AND product_feature_group_id=' . intval($featureGroupId))
		;
		return true;
	}

	/**
	 * Assign product feature group feature collection
	 *
	 * @param $featureGroupId
	 * @param $groupFeatures
	 */
	public function updateGroupFeatures($featureGroupId, $groupFeatures)
	{
		$db = $this->db;

		$groupFeatures = is_array($groupFeatures) ? $groupFeatures : array($groupFeatures);

		// get current ones
		$currentFeatures = array();
		$result = $db->query('SELECT product_feature_id FROM ' . DB_PREFIX . 'products_features_groups_relations WHERE product_feature_group_id=' . intval($featureGroupId));
		while ($c = $db->moveNext($result)) $currentFeatures[] = $c['product_feature_id'];

		// insert new ones
		$updatedFeatures = array();
		foreach ($groupFeatures as $groupFeatureId)
		{
			$groupFeatureId = intval($groupFeatureId);
			if (!in_array($groupFeatureId, $currentFeatures))
			{
				$groupFeatureData = array();
				$groupFeatureData['product_feature_group_id'] = $featureGroupId;
				$groupFeatureData['product_feature_id'] = $groupFeatureId;

				$this->persist($groupFeatureData);
			}
			$updatedFeatures[] = $groupFeatureId;
		}

		// remove groups that aren't reassigned
		$deletedFeatures = array();
		foreach ($currentFeatures as $currentFeatureId)
		{
			if (!in_array($currentFeatureId, $updatedFeatures))
			{
				$deletedFeatures[] = $currentFeatureId;
			}
		}

		// delete product feature group collection that are previously
		// selected but remove on current submit action
		if (count($deletedFeatures) > 0)
		{
			$db->query('
				DELETE FROM ' . DB_PREFIX . 'products_features_groups_relations
				WHERE product_feature_group_id=' . intval($featureGroupId) . ' AND product_feature_id IN (' . implode(',', $deletedFeatures) . ')
			');
		}
	}

	/**
	 * @param $ids
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		if (count($ids) == 0) return false;

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		$this->db->query('DELETE FROM ' . DB_PREFIX . 'products_features_groups_relations WHERE product_feature_group_relation_id IN(' . implode(',', $ids). ')');
		return true;
	}


	/**
	 * Get product feature group features by Id
	 */
	public function getFeatureGroupFeatureById($featureGroupFeatureId)
	{
		return $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'products_features_groups_relations WHERE product_feature_group_relation_id=' . intval($featureGroupFeatureId));
	}

	/**
	 * Persist
	 *
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : null;

		$db->reset();
		$db->assignStr('product_feature_group_id', $data['product_feature_group_id']);
		$db->assignStr('product_feature_id', $data['product_feature_id']);
		$db->assignStr('priority', (isset($data['priority'])? $data['priority'] : 0));

		if (!is_null($data['id']) && intval($data['id']) > 0)
		{
			$db->update(DB_PREFIX . 'products_features_groups_relations',  'WHERE product_feature_group_relation_id=' . intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX . 'products_features_groups_relations');
		}
	}
}
<?php
/**
 * Class DataAccess_ManufacturerRepository
 */
class DataAccess_ManufacturerRepository extends DataAccess_BaseRepository
{
	protected $imagesPath = 'images/manufacturers';

	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'manufacturer_name' => '',
			'manufacturer_code' => '',
			'is_visible' => '0',
			'image' => null,
			'meta_title' => null,
			'meta_description' => null,
			'url_custom' => '',
			'url_default' => '',
		);
	}

	/**
	 * Get manufacturer by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		$manufacturer = $this->db->selectOne(
			'SELECT *, IF(url_custom="", url_default, url_custom) AS url FROM '.DB_PREFIX.'manufacturers WHERE manufacturer_id='.intval($id)
		);

		if ($manufacturer)
		{
			$manufacturer['thumb'] = $this->getManufacturerThumb($manufacturer['manufacturer_id']);
		}
		
		return $manufacturer;
	}

	/**
	 * Get manufacturer by id
	 *
	 * @param string $code
	 *
	 * @return array|bool
	 */
	public function getByCode($code)
	{
		return $this->db->selectOne(
			'SELECT *, IF(url_custom="", url_default, url_custom) AS url FROM '.DB_PREFIX.'manufacturers WHERE manufacturer_code="'.$this->db->escape($code).'"'
		);
	}

	/**
	 * Build query params
	 *
	 * @param $searchParams
	 * @param $logic
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$where = array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		if (isset($searchParams['is_visible']))
		{
			switch ($searchParams['is_visible'])
			{
				case 'Yes' : $where[] = 'is_visible=1'; break;
				case 'No' : $where[] = 'is_visible=0'; break;
				default: $where[] = 'is_visible=1'; break;
			}
		}

		$searchStr = trim($searchParams['search_str']);
		if (isset($searchParams['search_str']) && $searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$where[] =
				'(manufacturer_name like "%' . $searchStr . '%")' .
				' OR (manufacturer_code like "%' . $searchStr . '%")';
		}

		return count($where) > 0 ? ' WHERE '.implode( ' ' . $logic . ' ', $where).' ' : '';
	}

	/**
	 * Get manufacturers count
	 *
	 * @param array|null $searchParams
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'manufacturers' . $this->getSearchQuery($searchParams, $logic));

		return intval($result['c']);
	}

	/**
	 * Get manufacturers list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param null $columns
	 * @param null $logic
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = '*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'manufacturer_name';

		switch($orderBy)
		{
			case 'manufacturer_name_asc'  : $orderBy = 'manufacturer_name'; break;
			case 'manufacturer_name_desc' : $orderBy = 'manufacturer_name'; break;
			case 'manufacturer_code_asc'  : $orderBy = 'manufacturer_code'; break;
			case 'manufacturer_code_desc' : $orderBy = 'manufacturer_code'; break;
			case 'visibility_asc' : $orderBy = 'is_visible'; break;
			case 'visibility_desc' : $orderBy = 'is_visible DESC'; break;
			default: $orderBy = 'manufacturer_code';	break;
		}

		return $this->db->selectAll(
			'SELECT ' . $columns . ', 
			IF(url_custom="", url_default, url_custom) AS url 
			FROM ' . DB_PREFIX . 'manufacturers' . $this->getSearchQuery($searchParams) .
			' ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get manufacturer options list
	 *
	 * @return array
	 */
	public function getOptionsList()
	{
		$list = $this->getList();

		$options = array();
		foreach ($list as $manufacturer)
		{
			$options[$manufacturer['manufacturer_id']] = $manufacturer['manufacturer_name'];
		}

		return $options;
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['manufacturer_name']) == '') $errors['manufacturer_name'] = array('Please enter title');
			if (trim($data['manufacturer_code']) == '')
			{
				$errors['manufacturer_code'] = array('Please enter manufacturer id');
			}
			else
			{
				$db = $this->db;
				if ($db->selectOne('SELECT * FROM '.DB_PREFIX.'manufacturers WHERE manufacturer_code="'.$db->escape($data['manufacturer_code'] ).'"'.(!is_null($id) ? ' AND manufacturer_id <> '.intval($id) : '')))
				{
					$errors['manufacturer_code'] = array('Manufacturer code must be unique within manufacturers');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist manufacturer
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['manufacturer_id']) ? $data['manufacturer_id'] : null);

		$db->assignStr('is_visible', isset($data['is_visible']) && $data['is_visible'] == 1 ? '1' : '0');
		$db->assignStr('manufacturer_code', $data['manufacturer_code']);
		$db->assignStr('manufacturer_name', $data['manufacturer_name']);

		//if (isset($data['url_hash'])) $db->assignStr('url_hash', $data['url_hash']);
		//if (isset($data['url_default'])) $db->assignStr('url_default', $data['url_default']);
		if (isset($data['url_custom'])) $db->assignStr('url_custom', $data['url_custom']);

		if (isset($data['meta_title']) && !is_null($data['meta_title'])) $db->assignStr('meta_title', $data['meta_title'] == '' ? null : $data['meta_title']);
		if (isset($data['meta_description']) && !is_null($data['meta_description'])) $db->assignStr('meta_description', $data['meta_description'] == '' ? null : $data['meta_description']);

		if (isset($data['image']) && !is_null($data['image'])) $db->assignStr('image', $data['image']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'manufacturers',  'WHERE manufacturer_id='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'manufacturers');
		}
	}

	/**
	 * Delete manufacturers by ids
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		// delete images
		$result = $this->db->query('SELECT image FROM '.DB_PREFIX.'manufacturers WHERE manufacturer_id IN('.implode(',', $ids).')');
		while ($manufacturer = $this->db->moveNext($result)) @unlink($manufacturer['image']);

		// delete manufacturers & reset products
		$this->db->query('DELETE FROM '.DB_PREFIX.'manufacturers WHERE manufacturer_id IN('.implode(',', $ids).')');
		$this->db->query('UPDATE '.DB_PREFIX.'products SET manufacturer_id=0 WHERE manufacturer_id IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * Delete manufacturers in search results
	 *
	 * @param $searchParams
	 *
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams)
	{
		$searchQuery = $this->getSearchQuery($searchParams);

		$ids = array();

		$result = $this->db->query('SELECT manufacturer_id, image FROM '.DB_PREFIX.'manufacturers'.$searchQuery);

		if ($this->db->numRows($result) < 1) return false;

		while ($manufacturer = $this->db->moveNext($result))
		{
			$ids[] = $manufacturer['manufacturer_id'];
			if ($manufacturer['image'] != '') @unlink($manufacturer['image']);
		}

		// delete manufacturers & reset products
		$this->db->query('DELETE FROM '.DB_PREFIX.'manufacturers'.$searchQuery);
		$this->db->query('UPDATE '.DB_PREFIX.'products SET manufacturer_id=0 WHERE manufacturer_id IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * @return array
	 */
	public function getManufacturersIdsMap()
	{
		$db = $this->db;
		$manufacturers = array();

		$db->query('SELECT manufacturer_id, manufacturer_code FROM '.DB_PREFIX.'manufacturers');

		while ($db->moveNext())
		{
			$manufacturers[strtolower($db->col['manufacturer_code'])] = $db->col['manufacturer_id'];
		}

		return $manufacturers;
	}

	/**
	 * @param $url
	 * @param $manufacturer_id
	 * @return int
	 */
	public function hasUrlDuplicate($url, $manufacturer_id)
	{
		$escapedUrl = $this->db->escape($url);
		$db = $this->db;

		$result = $db->query('
			SELECT url_custom FROM '.DB_PREFIX.'manufacturers
			WHERE (url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'")
				AND manufacturer_id != ' . intval($manufacturer_id) . '

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'products
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'catalog
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'pages
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"
		');

		return $db->numRows($result);
	}

	/**
	 * @param $manufacturerId
	 * @return null|string
	 */
	public function getManufacturerThumb($manufacturerId)
	{
		$id = intval($manufacturerId);
		$extensions = array('jpg', 'png', 'gif', 'jpeg');
		foreach ($extensions as $extension)
		{
			if (is_file($f = $this->imagesPath.'/thumbs/'.$id.'.'.$extension))
			{
				return $f;
			}
		}

		return null;
	}
}
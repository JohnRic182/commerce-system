<?php

/**
 * Class DataAccess_NotificationEmailsRepository
 */
class DataAccess_NotificationEmailsRepository extends DataAccess_BaseRepository
{
	/**
	 * @var array
	 */
	protected $email_templates = array();
	protected $email_classes = array();
	protected $list = array();
	protected $email_top = null;
	protected $email_bottom = null;

	protected $email_path_base = '';
	protected $email_path_skin = '';
	protected $email_path_custom = '';
	protected $email_path_cache = '';

	protected $designSkinName = '';

	/**
	 * @param DB $db
	 * @param $designSkinName
	 */
	public function __construct(DB $db, $designSkinName)
	{
		$this->designSkinName = $designSkinName;

		$this->email_templates = array(
			'signup_admin' => 'New user registered - for administrators',
			'signup_user' => 'New user registered - for customers',
			'order_received_admin' => 'New order - for administrators',
			'order_received_user' => 'New order - for customers',
			'order_shipped' => 'Order shipped - for customers',
			'order_completed' => 'Order completed - for customers',
			'reset_password' => 'Password reset - for customers',
			'to_friend' => 'Email to a friend',
			'admin_password_changed' => 'Password reset - for administrators',
			'admin_product_review' => 'New product review - for administrators',
			'products_location_notification' => 'Product location facility shipment (drop-shipment) notification',
			'order_note_for_customer' => 'Order note - for customers',
			'gift_cert' => 'Gift certificate',
			'wishlist' => 'Wishlist email',
			'outofstock_admin' => 'Product Stock Warning - for low and out of stock',
			'admin_inactive_accounts' => 'Inactive Accounts - for administrators',
			'admin_password_change_reminder' => 'Password Change Reminder - for administrators',
			'admin_password_expired' => 'Password Change Expired - for administrators',
			'custom_form_submitted' => 'Custom Form Notification',
			'drift_email_abandoned_cart_contents' => 'Drift Email Abandoned Cart - for administrators',
			'order_received' => 'Orders received notification'
		);

		if (defined('_ACCESS_RECURRING_BILLING') && _ACCESS_RECURRING_BILLING)
		{
			$this->email_templates = array_merge($this->email_templates, array(
				'recurring_profile_created' => 'Recurring profile created',
				'recurring_profile_canceled' => 'Recurring profile profile',
				'recurring_profile_card_expiration' => 'Recurring profile credit card expiration notice',
				'recurring_profile_completed' => 'Recurring profile completion notice',
				'recurring_profile_due_completion' => 'Recurring profile due completion notice',
				'recurring_profile_reactivated' => 'Recurring profile reactivated notice',
				'recurring_profile_suspended' => 'Recurring profile suspended expiration notice',
				'recurring_profile_transaction_failed' => 'Recurring profile failed transaction notice'
			));
		}

		$this->email_path_base = 'content/engine/design/templates/emails/';
		$this->email_path_skin = 'content/skins/'.escapeFileName($this->designSkinName).'/templates/emails/';
		$this->email_path_custom = 'content/skins/_custom/skin/templates/emails/';
		$this->email_path_cache = 'content/cache/skins/'.escapeFileName($this->designSkinName).'/templates/emails/';

		foreach ($this->email_templates as $id => $classTitle)
		{
			$params = array(
				'id' => $id,
				'title' => $classTitle,
				'email_path_base' => $this->email_path_base,
				'email_path_skin' => $this->email_path_skin,
				'email_path_custom' => $this->email_path_custom,
				'email_path_cache' => $this->email_path_cache
			);
			$email = new Model_NotificationEmailItem($params);

			$this->email_classes[$id] = $email;
			$item['title']= $email->getEmailTitle();
			$item['template_id']= $email->getEmailTemplateId();
			$this->list[$id] = $item;
		}

		//top & bottom
		$params['id'] = 'top';
		$params['title'] = 'Top for Email';
		$params['email_path_base'] = $this->email_path_base.'elements/';
		$params['email_path_skin'] = $this->email_path_skin.'elements/';
		$params['email_path_custom'] = $this->email_path_custom.'elements/';
		$params['email_path_cache'] = $this->email_path_cache.'elements/';
		$this->email_top = new Model_NotificationEmailItem($params);

		$params['id'] = 'bottom';
		$params['title'] = 'Bottom for Email';
		$this->email_bottom = new Model_NotificationEmailItem($params);

		parent::__construct($db);
	}

	/**
	 * @return array
	 */
	public function getHeaderFooterData()
	{
		$result = array();
		if ($this->email_top)
		{
			$result['top'] = $this->email_top->getData();
		}

		if ($this->email_bottom)
		{
			$result['bottom'] = $this->email_bottom->getData();
		}

		return $result;
	}

	/**
	 * @return int
	 */
	public function getCount()
	{
		return count($this->email_classes);
	}

	/**
	 * @param $formData
	 * @return bool
	 */
	public function getValidationErrorsAndPersist($formData)
	{
		$errors = array();

		if ($formData['content'])
		{
			foreach ($formData['content'] as $id => $val)
			{
				if ($id == 'top')
				{
					$item = $this->email_top;
				}
				else if ($id == 'bottom')
				{
					$item = $this->email_bottom;
				}
				else
				{
					$item = $this->email_classes[$id];
				}

				if (!is_dir($item->getEmailPathCustom())) mkdir($item->getEmailPathCustom(), 0777, 1);
				if (!is_dir($item->getEmailPathCache())) mkdir($item->getEmailPathCache(), 0777, 1);

				$i = 0;
				$i += (file_put_contents($item->getEmailPathCustom().$id.'_html.html', html_entity_decode(stripslashes($val))) ? 1 : 0);
				$i += (file_put_contents($item->getEmailPathCache().$id.'_html.html', html_entity_decode(stripslashes($val))) ? 1 : 0);

				if ($i < 2)
				{
					$errors['content['.$id.']'] = array('Can\'t save email template. Pleas check free space and write permissions!');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @return array
	 */
	public function getList($start = null, $limit = null)
	{

		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		$res = $this->list;

		if(!is_null($start) && !is_null($limit))
		{
			$res = array_slice($res, $start, $limit);
		}

		return $res;
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public function getById($id)
	{
		if( $this->email_classes[$id])
		{
			return $this->email_classes[$id]->getData();
		}
		else {
			return false;
		}
	}
}

<?php
/**
 * Class DataAccess_SettingsRepository
 */
class DataAccess_SettingsRepository
{
	protected static $db;
	protected static $settings = null;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param array|null $settings
	 */
	public function __construct(DB $db = null, &$settings = null)
	{
		if (is_null($db))
		{
			global $db;
		}

		self::$db = $db;

		// force pass by reference
		if (is_null($settings))
		{
			global $settings;
			self::$settings = &$settings;
		}
		else
		{
			self::$settings = $settings;
		}
	}

	/**
	 * Get instance
	 *
	 * @return DataAccess_SettingsRepository|null
	 */
	public static function getInstance()
	{
		static $instance = null;
		if (is_null($instance)) $instance = new self;
		return $instance;
	}

	/**
	 * Save settings
	 *
	 * @param array $settings
	 * @param null $params
	 */
	public function persist(array $settings, $params = null)
	{
		$this->save($settings, $params);
	}

	/**
	 * Save settings
	 *
	 * @param $settings
	 * @param mixed $allowed
	 */
	public function save($settings, $allowed = null)
	{
		// filter settings, possibly may be optimized
		if (!is_null($allowed) && is_array($allowed))
		{
			$_settings = array();
			foreach ($settings as $key => $value)
			{
				if (in_array($key, $allowed))
				{
					$_settings[$key] = $value;
				}
			}
			$settings = $_settings;
		}

		foreach ($settings as $key => $value)
		{
			self::$db->query('UPDATE '.DB_PREFIX.'settings SET value="'.self::$db->escape($value).'" WHERE name="'.self::$db->escape($key).'"');
			self::$settings[$key] = $value;
		}
	}

	/**
	 * Set settings value
	 *
	 * @param $key
	 * @param $value
	 */
	public function set($key, $value)
	{
		self::$settings[$key] = $value;
	}

	/**
	 * Get settings value
	 *
	 * @param $key
	 * @param null $default
	 *
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		return array_key_exists($key, self::$settings) ? self::$settings[$key] : $default;
	}

	/**
	 * Check does settings value exist
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	public function has($key)
	{
		return array_key_exists($key, self::$settings);
	}

	/**
	 * Get settings by keys
	 *
	 * @param $keys
	 *
	 * @return array
	 */
	public function getByKeys($keys)
	{
		$result = array();

		foreach ($keys as $key)
		{
			$result[$key] = $this->get($key);
		}

		return $result;
	}

	/**
	 * Get settings array
	 *
	 * @return array
	 */
	public function getAll()
	{
		if (self::$settings == null)
		{
			global $settings;

			$settings = array();
			$db = self::$db;

			$db->query('SELECT name, value FROM ' . DB_PREFIX . 'settings');
			while ($db->moveNext()) $settings[$db->col['name']] = $db->col['value'];

			$db->query('SELECT description FROM ' . DB_PREFIX . 'settings WHERE name="AppVer"');
			if ($db->moveNext()) $settings['AppVer'] = $settings['AppVer'].' '.$db->col['description'];

			if (!defined('APP_VERSION')) define('APP_VERSION', str_replace(array(' ', '.'), '', strtolower($settings['AppVer'])));
			if (!defined('APP_SYSTEM_TIMEZONE')) define('APP_SYSTEM_TIMEZONE', date_default_timezone_get());

			$settings['AdminHttpsUrl'] = isset($settings['AdminHttpsUrl']) && trim($settings['AdminHttpsUrl']) != '' ? $settings['AdminHttpsUrl'] : $settings['GlobalHttpsUrl'];

			self::$settings = &$settings;
		}

		return self::$settings;
	}
}
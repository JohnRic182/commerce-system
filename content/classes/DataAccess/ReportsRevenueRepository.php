<?php
/**
 *  Class DataAccess_ReportsRevenueRepository
 */
class DataAccess_ReportsRevenueRepository
{
	/** @var DB */
	protected $db;

	public function __construct(DB $db)
	{
		$this->db = $db;
	}

	public function getListRevenueByCountry($dateRange)
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$dataResults = array();

		$resultRows = $this->db->selectAll("
			SELECT co.iso_a3, SUM(o.total_amount - o.gift_cert_amount) AS order_total_amount 
			FROM " . DB_PREFIX . "orders o
			LEFT JOIN " . DB_PREFIX . "countries co ON (o.shipping_country = co.coid)			
			WHERE NOT ISNULL(co.iso_a3) 
			AND o.removed = 'No' 
			AND o.status = 'Completed' 
			AND o.payment_status IN('Received', 'Refunded') 
			AND o.placed_date BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "' GROUP BY co.iso_a3"
		);

		foreach($resultRows as $resultRow)
		{
			$dataResults[$resultRow['iso_a3']] = $resultRow['order_total_amount'];
		}
		return $dataResults;
	}

	/**
	 * Get getRecurringByDate
	 *
	 * @param $dateRange
	 * @return mixed
	 */
	public function getRecurringByDate($dateRange)
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		$resultRow = $this->db->selectOne("SELECT IFNULL(SUM(o.total_amount), 0) AS total_amount				
			FROM " . DB_PREFIX . "orders AS o
			INNER JOIN " . DB_PREFIX . "users AS u ON u.uid = o.uid AND u.removed = 'No'
			INNER JOIN " . DB_PREFIX . "recurring_profiles_orders AS rpo ON rpo.order_id = o.oid
			INNER JOIN " . DB_PREFIX . "recurring_profiles AS rp ON rp.recurring_profile_id = rpo.recurring_profile_id
			INNER JOIN " . DB_PREFIX . "orders_content AS oc ON rp.initial_order_line_item_id = oc.ocid
			WHERE o.removed = 'No' 
			AND o.order_type = 'Recurring'
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'			 
			AND o.status = 'Completed' 
			AND o.payment_status = 'Received'");
		return $resultRow['total_amount'];
	}

	/**
	 * Get getRunRateMultiplier of annual run rate.
	 *
	 * @param $rangeType
	 * @param $dateRange
	 * @return float|int
	 */
	public function getRunRateMultiplier($rangeType, $dateRange) {
		$multiplier = 0;
		if (strcmp($rangeType, 'custom') == 0)
		{
			$startDate = new DateTime($dateRange['start']);
			$endDate = new DateTime($dateRange['end']);
			$dateDifference = $startDate->diff($endDate);
			if ($dateDifference->days <= 365)
			{
				$dayCount = (365 / $dateDifference->days);
				$multiplier = round($dayCount, 0, PHP_ROUND_HALF_UP);
			}
		}
		else
		{
			if (strcmp($rangeType, 'week') == 0) $multiplier = 48;
			if (strcmp($rangeType, 'month') == 0) $multiplier = 12;
			if (strcmp($rangeType, 'quarter') == 0) $multiplier = 4;
			if (strcmp($rangeType, 'year') == 0) $multiplier = 1;
		}
		return $multiplier;
	}
}
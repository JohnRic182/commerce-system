<?php

/**
 * Order repository class
 */
class DataAccess_OrderRepository implements DataAccess_OrderRepositoryInterface
{
	protected $db;
	protected $settings;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 * @param null $settings
	 */
	public function __construct(DB $db = null, &$settings = null)
	{
		if (is_null($db))
		{
			global $db;
		}

		if (is_null($settings))
		{
			global $settings;
		}

		$this->db = $db;
		$this->settings = $settings;
	}

	/**
	 * Get last active order
	 *
	 * @param string $dateFormat
	 * @param int $count
	 *
	 * @return array
	 */
	public function getLastActiveOrders($dateFormat = '%m/%d/%Y - %r', $count = 7)
	{
		return $this->db->selectAll('
			SELECT
				'.DB_PREFIX.'orders.*, '.DB_PREFIX.'users.*, DATE_FORMAT(status_date, "'.$dateFormat.'") AS sd
			FROM '.DB_PREFIX.'orders
			LEFT JOIN '.DB_PREFIX.'users ON
				'.DB_PREFIX.'users.uid='.DB_PREFIX.'orders.uid
				AND '.DB_PREFIX.'users.removed="No"
			WHERE
				'.DB_PREFIX.'orders.status <> "Abandon"
				AND '.DB_PREFIX.'orders.status <> "New"
				AND order_num > 0
				AND '.DB_PREFIX.'orders.status <> "Failed"
				AND '.DB_PREFIX.'orders.removed <> "Yes"
			ORDER BY '.DB_PREFIX.'orders.placed_date DESC
			LIMIT '.intval($count).'
		');
	}

	/**
	 * Reset order line item's shipping price
	 *
	 * @param $orderId
	 */
	public function resetLineItemShippingPrices($orderId)
	{
		$this->db->reset();
		$this->db->assignStr('is_shipping_price', 'No');
		$this->db->assignStr('shipping_price', '0.00');
		$this->db->update(DB_PREFIX.'orders_content', 'WHERE oid = '.intval($orderId));
	}

	/**
	 * Reset line item discounts
	 *
	 * @param int $orderId
	 */
	public function resetLineItemDiscounts($orderId)
	{
		$this->db->query('UPDATE '.DB_PREFIX.'orders_content SET discount_amount = 0, promo_discount_amount = 0, discount_amount_with_tax = 0, promo_discount_amount_with_tax = 0 WHERE oid = '.intval($orderId));
	}

	/**
	 * Update line item discount
	 *
	 * @param $orderId
	 * @param $lineId
	 * @param $discountAmount
	 * @param $discountAmountWithTax
	 * @param $promoDiscountAmount
	 * @param $promoDiscountAmountWithTax
	 */
	public function updateLineItemDiscount($orderId, $lineId, $discountAmount, $discountAmountWithTax, $promoDiscountAmount, $promoDiscountAmountWithTax)
	{
		$this->db->query('
			UPDATE '.DB_PREFIX.'orders_content
			SET discount_amount = '.floatval($discountAmount).', discount_amount_with_tax = '.floatval($discountAmountWithTax).',
			 promo_discount_amount = '.floatval($promoDiscountAmount).', promo_discount_amount_with_tax = '.floatval($promoDiscountAmountWithTax).'
			WHERE oid = '.intval($orderId).' AND ocid = '.intval($lineId)
		);
	}

	/**
	 * Find existing line item for a product
	 *
	 * @param ORDER $order
	 * @param $productId
	 * @param $options
	 *
	 * @return mixed
	 */
	public function findExistingLineItemForProduct(ORDER $order, $productId, $options)
	{
		$lineItemValues = $this->db->selectOne('
			SELECT
				oc.ocid, p.price AS final_price
			FROM '.DB_PREFIX.'orders_content oc
			INNER JOIN '.DB_PREFIX.'products p ON oc.pid = p.pid
			WHERE
				oc.oid = '.$order->getId().'
				AND oc.product_id = "'.$this->db->escape($productId).'"
				AND oc.is_gift = "No"
				AND oc.options LIKE "'.$this->db->escape($options).'"'
		);

		if ($lineItemValues)
		{
			if (isset($order->lineItems[$lineItemValues['ocid']]))
			{
				$lineItem = $order->lineItems[$lineItemValues['ocid']];
				$lineItem->setFinalPrice($lineItemValues['final_price']);

				return $lineItem;
			}
		}

		return null;
	}

	/**
	 * Get order items products
	 *
	 * @param int $orderId
	 * @param array $lineItemIds
	 *
	 * @return mixed
	 */
	public function getOrderItemsProducts($orderId, &$lineItemIds)
	{
		if (count($lineItemIds) == 0) return array();

		foreach ($lineItemIds as $key => $lineItemId)
		{
			$lineItemIds[$key] = intval($lineItemId);
		}

		$productsTemp = $this->db->selectAll('
			SELECT DISTINCT p.*
			FROM '.DB_PREFIX.'orders_content AS oc
			INNER JOIN '.DB_PREFIX.'products p ON oc.pid = p.pid
			WHERE
				oc.ocid IN ('.implode(',', $lineItemIds).')
				AND oc.oid = '.intval($orderId).'
				AND p.is_visible = \'Yes\''
		);

		$products = array();
		foreach ($productsTemp as $prod)
		{
			$products[$prod['pid']] = new Model_Product($prod);
		}

		return $products;
	}

	/**
	 * Delete line items
	 *
	 * @param $orderId
	 * @param $lineItemIds
	 */
	public function deleteLineItems($orderId, &$lineItemIds)
	{
		foreach ($lineItemIds as $key => $lineItemId)
		{
			$lineItemIds[$key] = intval($lineItemId);
		}

		if (count($lineItemIds) > 0)
		{
			$this->db->query('DELETE FROM '.DB_PREFIX.'orders_content WHERE oid = '.intval($orderId).' AND ocid IN ('.implode(',', $lineItemIds).')');
			$this->db->query('DELETE FROM '.DB_PREFIX.'orders_content_attributes WHERE oid = '.intval($orderId).' AND ocid IN ('.implode(',', $lineItemIds).')');
		}
	}

	/**
	 * Delete all line items
	 *
	 * @param $orderId
	 */
	public function deleteAllLineItems($orderId)
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'orders_content WHERE oid = '.intval($orderId));
		$this->db->query('DELETE FROM '.DB_PREFIX.'orders_content_attributes WHERE oid = '.intval($orderId));
		$this->db->query('DELETE FROM '.DB_PREFIX.'orders_shipments WHERE oid = '.intval($orderId));
	}

	/**
	 * Persis line item
	 *
	 * @param ORDER $order
	 * @param Model_LineItem $lineItemObj
	 * @param bool $updateAttributes
	 */
	public function persistLineItem(ORDER $order, Model_LineItem $lineItemObj, $updateAttributes = false)
	{
		$this->db->reset();

		$lineItem = $lineItemObj->toArray();

		if (!isset($lineItem['is_gift']) || $lineItem['is_gift'] == 'No')
		{
			$this->db->assign('price', PriceHelper::round($lineItem['price']));
			$this->db->assign('admin_price', PriceHelper::round($lineItem['admin_price']));
			$this->db->assign('price_before_quantity_discount', PriceHelper::round($lineItem['price_before_quantity_discount']));
			$this->db->assignStr('is_gift', 'No');
		}
		else
		{
			$this->db->assign('price', 0);
			$this->db->assign('admin_price', 0);
			$this->db->assignStr('is_gift', 'Yes');
		}

		$this->db->assign('shipment_id', $lineItem['shipment_id']);
		$this->db->assignStr('free_shipping', $lineItem['free_shipping']);
		$this->db->assign('quantity', intval($lineItem['quantity']));
		$this->db->assign('admin_quantity', intval($lineItem['admin_quantity']));
		$this->db->assign('weight', $lineItem['weight']);
		$this->db->assign('product_level_shipping', intval($lineItem['product_level_shipping']));
		$this->db->assign('same_day_delivery', intval($lineItem['same_day_delivery']));

		$this->db->assignStr('is_taxable', $lineItem['is_taxable']);
		$this->db->assignStr('tax_rate', $lineItem['tax_rate']);
		$this->db->assignStr('tax_description', $lineItem['tax_description']);
		$this->db->assign('price_withtax', PriceHelper::round($lineItem['price_withtax']));

		if (isset($lineItem['is_stock_changed'])) $this->db->assignStr('is_stock_changed', $lineItem['is_stock_changed']);
		if (isset($lineItem['quantity_from_stock'])) $this->db->assign('quantity_from_stock', intval($lineItem['quantity_from_stock']));

		$this->db->assignStr('product_id', $lineItem['product_id']);
		$this->db->assignStr('product_sku', $lineItem['product_sku']);
		$this->db->assignStr('product_upc', $lineItem['product_upc']);
		$this->db->assignStr('product_gtin', $lineItem['product_gtin']);
		$this->db->assignStr('product_mpn', $lineItem['product_mpn']);

		$this->db->assignStr('title', $lineItem['title']);

		if ($order->getFulfillmentStatus() == Model_Fulfillment::STATUS_COMPLETED)
		{
			$this->db->assign('price', PriceHelper::round($lineItem['product_price']));
			$this->db->assign('weight', $lineItem['product_weight']);
		}

		// TODO - possibly need some kind of flag here or so - like for attributes
		if (isset($lineItem['options'])) $this->db->assignStr('options', $lineItem['options']);
		if (isset($lineItem['options_clean'])) $this->db->assignStr('options_clean', $lineItem['options_clean']);
		if (isset($lineItem['product_sub_id'])) $this->db->assignStr('product_sub_id', $lineItem['product_sub_id']);
		if (isset($lineItem['inventory_id'])) $this->db->assignStr('inventory_id', $lineItem['inventory_id']);
		if (isset($lineItem['products_location_id'] )) $this->db->assignStr('products_location_id', $lineItem['products_location_id']);

		/** @var RecurringBilling_Model_LineItemRecurringBillingData $recurringBillingData */
		$recurringBillingData = $lineItemObj->getRecurringBillingData();
		$this->db->assignStr('recurring_billing_data', !is_null($recurringBillingData) ? serialize($recurringBillingData->toArray()) : '');

		if (isset($lineItem['fulfillment_status'])) $this->db->assignStr('fulfillment_status', $lineItem['fulfillment_status']);
		if (isset($lineItem['fulfilled_quantity'])) $this->db->assignStr('fulfilled_quantity', $lineItem['fulfilled_quantity']);

		if (isset($lineItem['ocid']) && intval($lineItem['ocid']) > 0)
		{
			$this->db->update(DB_PREFIX.'orders_content', 'WHERE ocid='.intval($lineItem['ocid']).' AND oid='.intval($order->getId()).' AND pid='.intval($lineItem['pid']));

			// remove attributes before updatPersie
			if ($updateAttributes)
			{
				$this->db->query('DELETE FROM '.DB_PREFIX.'orders_content_attributes WHERE ocid='.intval($lineItem['ocid']));
			}
		}
		else
		{
			$this->db->assign('oid', $order->getId());
			$this->db->assign('pid', $lineItem['pid']);

			// some of properties set only when added
			$this->db->assignStr('is_doba', $lineItem['is_doba']);
			$this->db->assignStr('product_type', $lineItem['product_type']);
			$this->db->assignStr('digital_product_file', $lineItem['digital_product_file']);
			$this->db->assignStr('digital_product_key', $lineItem['product_type'] == Model_Product::DIGITAL ? md5(uniqid(rand(),1)) : '');
			$this->db->assignStr('digital_product_downloads', '0');

			$this->db->assign('discount_amount', $lineItem['discount_amount']);
			$this->db->assign('discount_amount_with_tax', $lineItem['discount_amount_with_tax']);
			$this->db->assign('promo_discount_amount', $lineItem['promo_discount_amount']);
			$this->db->assign('promo_discount_amount_with_tax', $lineItem['promo_discount_amount_with_tax']);

			$lineItemObj->setId($this->db->insert(DB_PREFIX.'orders_content'));
			$lineItem['ocid'] = $lineItemObj->getId();
		}

		// store/update attributes
		if ($updateAttributes && isset($lineItem['attributes']) && is_array($lineItem['attributes']) && count($lineItem['attributes']) > 0)
		{
			for ($i=0; $i<count($lineItem['attributes']); $i++)
			{
				if ($lineItem['attributes'][$i]['attribute_value'] * 1 != 0)
				{
					$this->db->reset();
					$this->db->assignStr('ocid', $lineItem['ocid']);
					$this->db->assignStr('oid', $order->getId());
					$this->db->assignStr('attribute_value', $lineItem['attributes'][$i]['attribute_value']);
					$this->db->assignStr('attribute_value_type', $lineItem['attributes'][$i]['attribute_value_type']);
					$this->db->insert(DB_PREFIX.'orders_content_attributes');
				}
			}
		}
	}

	/**
	 * Get next order number
	 *
	 * @param $orderId
	 * @param $minOrderNumber
	 *
	 * @return int
	 */
	public function getNextOrderNumber($orderId, $minOrderNumber)
	{
		$this->db->query('SELECT order_num FROM '.DB_PREFIX.'orders WHERE oid = '.intval($orderId));

		if ($this->db->moveNext())
		{
			if ($this->db->col['order_num'] < 1)
			{
				/**
				 * Set order num - all in one query to avoid duplicates
				 */
				$min = intval($minOrderNumber);
				$q = '
					UPDATE '.DB_PREFIX.'orders
					SET
						order_num = (
							SELECT * FROM (
								SELECT IF(MAX(order_num) >= '.intval($min).', MAX(order_num) + 1, '.intval($min).')
								FROM '.DB_PREFIX.'orders AS o
							) AS new_order_num
						)
					WHERE oid='.intval($orderId);

				$this->db->query($q);

				/**
				 * Return it
				 */
				$this->db->query('SELECT order_num FROM '.DB_PREFIX.'orders WHERE oid = '.intval($orderId));
				$this->db->moveNext();
			}

			return $this->db->col['order_num'];
		}

		return 0;
	}

	/**
	 * Get shipping address
	 *
	 * @param $orderId
	 *
	 * @return array|mixed
	 */
	public function getShippingAddress($orderId)
	{
		$addressData = $this->db->selectOne('
			SELECT
				'.DB_PREFIX.'orders.shipping_address_type AS address_type,
				'.DB_PREFIX.'orders.shipping_name AS name,
				'.DB_PREFIX.'orders.shipping_company AS company,
				'.DB_PREFIX.'orders.shipping_address1 AS address1,
				'.DB_PREFIX.'orders.shipping_address2 AS address2,
				'.DB_PREFIX.'orders.shipping_city AS city,
				'.DB_PREFIX.'orders.shipping_zip AS zip,
				'.DB_PREFIX.'orders.shipping_province AS province,
				'.DB_PREFIX.'orders.shipping_state AS state_id,
				'.DB_PREFIX.'countries.coid AS country_id,
				'.DB_PREFIX.'countries.name AS country,
				'.DB_PREFIX.'countries.name AS country_name,
				'.DB_PREFIX.'countries.iso_a2 AS country_iso_a2,
				'.DB_PREFIX.'countries.iso_a3 AS country_iso_a3,
				'.DB_PREFIX.'countries.iso_number AS country_iso_number,
				'.DB_PREFIX.'states.stid AS state_id,
				'.DB_PREFIX.'states.short_name AS state_abbr,
				'.DB_PREFIX.'states.name AS state
			FROM '.DB_PREFIX.'orders
			LEFT JOIN '.DB_PREFIX.'countries ON '.DB_PREFIX.'countries.coid = '.DB_PREFIX.'orders.shipping_country
			LEFT JOIN '.DB_PREFIX.'states ON '.DB_PREFIX.'states.stid = '.DB_PREFIX.'orders.shipping_state
			WHERE oid = '.intval($orderId));

		return $addressData ? $addressData : array();
	}

	/**
	 * Get line items
	 *
	 * @param $orderId
	 * @param bool $includeRemovedProducts
	 *
	 * @return mixed
	 */
	public function getLineItems($orderId, $includeRemovedProducts = false)
	{
		//TODO: Refactor out query
		return $this->db->selectAll('
			SELECT
				p.*,
				p.enable_recurring_billing as product_enable_recurring_billing,
				p.recurring_billing_data AS product_recurring_billing_data,
				IF(p.url_custom = "", p.url_default, p.url_custom) AS product_url,
				p.free_shipping AS product_free_shipping,
				oc.product_sub_id,
				oc.product_sku,
				oc.ocid,
				oc.oid,
				oc.shipment_id,
				oc.admin_price AS product_price,
				oc.admin_price,
				oc.price,
				oc.options,
				oc.options_clean,
				oc.quantity,
				oc.admin_quantity,
				oc.is_taxable,
				oc.tax_rate,
				oc.tax_description,
				oc.tax_exempt,
				oc.weight AS product_weight,
				oc.is_gift,
				oc.free_shipping,
				oc.same_day_delivery,
				oc.digital_product_key,
				oc.price_before_quantity_discount,
				c.name AS cat_name,
				IF(c.url_custom = "", c.url_default, c.url_custom) AS cat_url,
				c.exactor_euc_code AS cat_exactor_euc_code,
				oc.discount_amount,
				oc.discount_amount_with_tax,
				oc.promo_discount_amount,
				oc.promo_discount_amount_with_tax,
				oc.price_withtax,
				oc.tax_amount,
				oc.inventory_id,
				oc.recurring_billing_data,
				oc.is_stock_changed,
				oc.quantity_from_stock,
				oc.fulfillment_status,
				oc.fulfilled_quantity,
				IF (ISNULL(p.pid), oc.pid, p.pid) AS pid,
				IF (ISNULL(p.price), oc.admin_price, p.price) AS price,
				IF (ISNULL(p.title), oc.title, p.title) AS title,
				IF (ISNULL(p.product_id), oc.product_id, p.product_id) AS product_id,
				IF (ISNULL(p.pid), "Yes", oc.product_removed) AS product_removed
			FROM '.DB_PREFIX.'orders_content AS oc
			'.($includeRemovedProducts ? 'LEFT' : 'INNER').' JOIN '.DB_PREFIX.'products AS p ON p.pid = oc.pid
			LEFT JOIN '.DB_PREFIX.'catalog AS c ON p.cid = c.cid
			WHERE
				oc.oid = '.intval($orderId).'
			ORDER BY ocid
		');
	}

	/**
	 * Get line it by id
	 *
	 * @param $orderLineItemId
	 *
	 * @return mixed
	 */
	public function getLineItemById($orderLineItemId)
	{
		//TODO: Refactor out query
		return $this->db->selectOne('
			SELECT
				p.*,
				p.enable_recurring_billing as product_enable_recurring_billing,
				p.recurring_billing_data AS product_recurring_billing_data,
				IF(p.url_custom = "", p.url_default, p.url_custom) AS product_url,
				p.free_shipping AS product_free_shipping,
				oc.product_sub_id,
				oc.ocid,
				oc.oid,
				oc.shipment_id,
				oc.admin_price AS product_price,
				oc.admin_price,
				oc.price,
				oc.options,
				oc.options_clean,
				oc.quantity,
				oc.admin_quantity,
				oc.is_taxable,
				oc.tax_rate,
				oc.tax_description,
				oc.tax_exempt,
				oc.weight AS product_weight,
				oc.is_gift,
				oc.free_shipping,
				oc.same_day_delivery,
				oc.digital_product_key,
				oc.price_before_quantity_discount,
				c.name AS cat_name,
				IF(c.url_custom = "", c.url_default, c.url_custom) AS cat_url,
				c.exactor_euc_code AS cat_exactor_euc_code,
				oc.discount_amount,
				oc.discount_amount_with_tax,
				oc.promo_discount_amount,
				oc.promo_discount_amount_with_tax,
				oc.price_withtax,
				oc.tax_amount,
				oc.inventory_id,
				oc.recurring_billing_data,
				oc.is_stock_changed,
				oc.quantity_from_stock
			FROM '.DB_PREFIX.'orders_content AS oc
			INNER JOIN '.DB_PREFIX.'products AS p ON p.pid = oc.pid
			LEFT JOIN '.DB_PREFIX.'catalog AS c ON p.cid = c.cid
			WHERE
				oc.ocid = '.intval($orderLineItemId));
	}

	/**
	 * Get order data
	 *
	 * @param $orderId
	 *
	 * @return mixed
	 */
	public function getOrderData($orderId)
	{
		return $this->db->selectOne('
			SELECT o.*,
				DATE_FORMAT(o.status_date, "'.$this->settings['LocalizationDateTimeFormat'].'") as status_date_formatted,
				c.name AS shipping_country_name
			FROM '.DB_PREFIX.'orders o
				LEFT JOIN '.DB_PREFIX.'countries c ON o.shipping_country = c.coid
			WHERE o.oid = '.intval($orderId)
		);
	}

	/**
	 * Persist order data
	 *
	 * @param ORDER $order
	 */
	public function persistOrderData(ORDER $order)
	{
		$db = $this->db;

		$db->reset();

		$db->assign('uid', $order->getUserId());
		$db->assignStr('security_id', $order->getSecurityId());

		if (!is_null($order->getStatusChangeDate())) $db->assignStr('status_date', $order->getStatusChangeDate()->format('Y-m-d H:i:s'));
		if (!is_null($order->getOrderPlacedDate())) $db->assignStr('placed_date', $order->getOrderPlacedDate()->format('Y-m-d H:i:s'));
		if (!is_null($order->getOrderCreatedDate())) $db->assignStr('create_date', $order->getOrderCreatedDate()->format('Y-m-d H:i:s'));

		$db->assignStr('order_type', $order->getOrderType());

		// Subtotal
		$db->assignStr('subtotal_amount', $order->getSubtotalAmount());

		$db->assign('force_recalc', $order->getForceRecalc() ? 1 : 0);

		// Discounts
		$db->assignStr('discount_value', $order->getDiscountValue());
		$db->assignStr('discount_type', $order->getDiscountType());
		$db->assignStr('discount_amount', $order->getDiscountAmount());
		$db->assignStr('discount_amount_with_tax', $order->getDiscountAmountWithTax());

		// Promo Discounts
		$db->assignStr('promo_discount_amount', $order->getPromoDiscountAmount());
		$db->assignStr('promo_discount_amount_with_tax', $order->getPromoDiscountAmountWithTax());
		$db->assignStr('promo_discount_value', $order->getPromoDiscountValue());
		$db->assignStr('promo_discount_type', $order->getPromoDiscountType());
		$db->assignStr('promo_type', $order->getPromoType());
		$db->assignStr('promo_campaign_id', $order->getPromoCampaignId());

		// Handling
		$db->assignStr('handling_separated', $order->getHandlingSeparated() ? '1' : '0');
		$db->assignStr('handling_fee', $order->getHandlingFee());
		$db->assignStr('handling_fee_type', $order->getHandlingFeeType());
		$db->assignStr('handling_fee_when_free_shipping', $order->getHandlingFeeWhenShippingFree());
		$db->assignStr('handling_text', $order->getHandlingText());
		$db->assignStr('handling_amount', $order->getHandlingAmount());
		$db->assignStr('handling_taxable', $order->getHandlingTaxable() ? '1' : '0');
		$db->assignStr('handling_tax_rate', $order->getHandlingTaxRate());
		$db->assignStr('handling_tax_amount', $order->getHandlingTaxAmount());
		$db->assignStr('handling_tax_description', $order->getHandlingTaxDescription());

		//TODO: This isn't persisted anywhere
		//$db->assignStr('handling_tax_class_id', $order->getHandlingTaxClassId());

		// Shipping
		$db->assignStr('shipping_taxable', $order->getShippingTaxable() ? '1' : '0');
		$db->assignStr('shipping_tax_rate', $order->getShippingTaxRate());
		$db->assignStr('shipping_tax_description', $order->getShippingTaxDescription());
		$db->assignStr('shipping_tax_amount', $order->getShippingTaxAmount());
		$db->assignStr('shipping_amount', $order->shippingAmount);

		$db->assignStr('shipping_required', $order->getShippingRequired() ? '1' : '0');

		$db->assignStr('shipping_tracking_number', $order->getShippingTrackingNumber());
		$db->assignStr('shipping_tracking_number_type', $order->getShippingTrackingNumberType());

		$db->assignStr('payment_is_realtime', $order->getPaymentIsRealtime());
		$db->assignStr('payment_method_id', $order->getPaymentMethodId());
		$db->assignStr('payment_method_name', $order->getPaymentMethodName());
		$db->assignStr('payment_method_description', $order->getPaymentMethodDescription());
		$db->assignStr('payment_gateway_id', $order->getPaymentGatewayId());
		$db->assignStr('ofid', $order->getOrderFormID());

		$shippingAddress = $order->shippingAddress;
		if ($shippingAddress && !empty($shippingAddress))
		{
			if (isset($shippingAddress['address_type'])) $db->assignStr('shipping_address_type', $shippingAddress['address_type']);
			$db->assignStr('shipping_name', $shippingAddress['name']);
			$db->assignStr('shipping_company', $shippingAddress['company']);
			$db->assignStr('shipping_address1', $shippingAddress['address1']);
			$db->assignStr('shipping_address2', $shippingAddress['address2']);
			$db->assignStr('shipping_city', $shippingAddress['city']);
			$db->assignStr('shipping_state', intval(isset($shippingAddress['state_id']) ? $shippingAddress['state_id'] : $shippingAddress['state']));
			$db->assignStr('shipping_province', $shippingAddress['province']);
			$db->assignStr('shipping_country', intval(isset($shippingAddress['country_id']) ? $shippingAddress['country_id'] : $shippingAddress['country']));
			$db->assignStr('shipping_zip', $shippingAddress['zip']);
		}

		$db->assign('offsite_campaign_id', intval($order->getOffsiteCampaignId()));
		$db->assignStr('order_source', $order->getOrderSource());

		$shipping_tracking = $order->getShippingTracking();

		if (count($shipping_tracking) == 0)
		{
			$db->assignStr('shipping_tracking', '');
		}
		else
		{
			$db->assignStr('shipping_tracking', serialize($shipping_tracking));
		}

		// Tax
		$db->assignStr('tax_amount', $order->getTaxAmount());
		$db->assignStr('tax_exempt', $order->getTaxExempt());

		// Total
		$db->assignStr('total_amount', $order->getTotalAmount());

		$db->assignStr('status', $order->getStatus());
		$db->assignStr('payment_status', $order->getPaymentStatus());
		$db->assignStr('fulfillment_status', $order->getFulfillmentStatus());

		$db->assignStr('attempts', $order->getPaymentAttemptsCount());

		$userIpAddress = $order->getUserIpAddress();
		if (!is_null($userIpAddress)) $db->assignStr('ipaddress', $userIpAddress);

		$orderSource = $order->getOrderSource();
		if (!is_null($orderSource)) $db->assignStr('order_source', $orderSource);

		$offsiteCampaignId = $order->getOffsiteCampaignId();
		if (!is_null($offsiteCampaignId)) $db->assignStr('offsite_campaign_id', $offsiteCampaignId);

		/**
		 * Check do we have to create a new record or update existing one
		 */
		if (is_null($order->getId()) || $order->getId() == 0)
		{
			$orderId = $db->insert(DB_PREFIX.'orders');
			$order->setId($orderId);
		}
		else
		{
			$db->update(DB_PREFIX.'orders', 'WHERE oid='.intval($order->getId()));
		}
	}

	/**
	 * Update line item quantity pricing
	 *
	 * @param $orderId
	 * @param $lineItemId
	 * @param $finalPrice
	 * @param $priceWithTax
	 * @param $freeShipping
	 */
	public function updateLineItemQuantityPricing($orderId, $lineItemId, $finalPrice, $priceWithTax, $freeShipping)
	{
		$db = $this->db;

		$db->reset();
		$db->assign('price', $finalPrice);
		$db->assign('admin_price', $finalPrice);
		$db->assign('price_withtax', $priceWithTax);
		$db->assignStr('free_shipping', $freeShipping);
		$db->update(DB_PREFIX.'orders_content', 'WHERE oid = '.intval($orderId).' AND ocid = '.intval($lineItemId));
	}

	/**
	 * Get product gifts data
	 *
	 * @param int $orderId
	 * @return mixed
	 */
	public function getProductsGiftsData($orderId)
	{
		return $this->db->selectAll('
			SELECT p.*,
				g.gift_quantity,
				g.gift_shipping_free,
				g.gift_max_quantity,
				p2.pid as original_pid,
				p2.gift_quantity as gift_min_quantity
			FROM '.DB_PREFIX.'orders_content oc
				INNER JOIN '.DB_PREFIX.'products p2 ON oc.pid = p2.pid
				INNER JOIN '.DB_PREFIX.'products_gifts g ON p2.pid = g.pid
				INNER JOIN '.DB_PREFIX.'products p ON g.gift_pid = p.pid AND p.is_visible = "Yes"
			WHERE oc.oid = '.intval($orderId)
		);
	}

	/**
	 * Get orders count
	 *
	 * @param null $searchParams
	 * @param string $logic
	 *
	 * @return int
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$innerJoin = '';

		if (isset($searchParams['state']) && trim($searchParams['state']) != '')
		{
			$innerJoin = $innerJoin . 'INNER JOIN '.DB_PREFIX.'states s ON o.shipping_state = s.stid ';
		}

		if (isset($searchParams['promo_code']) && trim($searchParams['promo_code']) != '')
		{
			$innerJoin = $innerJoin . 'INNER JOIN '.DB_PREFIX.'promo_codes p ON o.promo_campaign_id = p.pid ';
		}

		if (isset($searchParams['notes_content']) && trim($searchParams['notes_content']) != '')
		{
			$innerJoin = $innerJoin . 'INNER JOIN '.DB_PREFIX.'admin_notes adn ON o.oid = adn.oid ';
		}

		$order_content_branch = ($searchParams !== null) &&
			((isset($searchParams['product_id']) && trim($searchParams['product_id']) != '')
				||
			(isset($searchParams['product_name']) && trim($searchParams['product_name']) != ''));

		if ($order_content_branch)
		{
			$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM (SELECT DISTINCT o.oid FROM '.DB_PREFIX.'orders o INNER JOIN '.DB_PREFIX.'users u ON o.uid = u.uid AND u.removed = "No" INNER JOIN '.DB_PREFIX.'orders_content oc ON o.oid = oc.oid '.$innerJoin.' '.$this->getSearchQuery($searchParams, $logic).') t');
		}
		else
		{
			$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'orders o INNER JOIN '.DB_PREFIX.'users u ON o.uid = u.uid AND u.removed = "No" '.$innerJoin.$this->getSearchQuery($searchParams, $logic));
		}

		return intval($result['c']);
	}

	/**
	 * Get orders list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 'o.*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		$innerJoin = '';

		if (isset($searchParams['state']) && trim($searchParams['state']) != '')
		{
			$innerJoin = $innerJoin . 'INNER JOIN '.DB_PREFIX.'states s ON o.shipping_state = s.stid ';
		}

		if (isset($searchParams['promo_code']) && trim($searchParams['promo_code']) != '')
		{
			$innerJoin = $innerJoin . 'INNER JOIN '.DB_PREFIX.'promo_codes p ON o.promo_campaign_id = p.pid ';
		}

		if (isset($searchParams['notes_content']) && trim($searchParams['notes_content']) != '')
		{
			$innerJoin = $innerJoin . 'INNER JOIN '.DB_PREFIX.'admin_notes adn ON o.oid = adn.oid ';
		}

		if ((isset($searchParams['product_name']) && trim($searchParams['product_name']) != '') ||
			(isset($searchParams['product_id']) && trim($searchParams['product_id']) != '')
		)
		{
			$innerJoin = $innerJoin . 'INNER JOIN ' . DB_PREFIX . 'orders_content oc ON o.oid = oc.oid ';
		}

		switch ($orderBy)
		{
			/**
			 * New order by params
			 */
			case 'order_num_asc': $mysqlOrderBy = 'o.oid, o.shipping_name'; break;
			case 'order_num_desc' : $mysqlOrderBy = 'o.oid DESC, o.shipping_name DESC'; break;
			case 'custom_search_asc': $mysqlOrderBy = 'o.shipping_name'; break;
			case 'custom_search_desc' : $mysqlOrderBy = 'o.shipping_name DESC'; break;
			case 'status_date_asc': $mysqlOrderBy = 'o.status_date'; break;
			case 'status_date_desc' : $mysqlOrderBy = 'o.status_date DESC'; break;
			case 'fulfillment_status_asc': $mysqlOrderBy = 'o.fulfillment_status'; break;
			case 'fulfillment_status_desc' : $mysqlOrderBy = 'o.fulfillment_status DESC'; break;
			case 'gift_cert_voucher_asc': $mysqlOrderBy = 'o.gift_cert_voucher'; break;
			case 'gift_cert_voucher_desc' : $mysqlOrderBy = 'o.gift_cert_voucher DESC'; break;
			case 'status_asc': $mysqlOrderBy = 'o.status'; break;
			case 'status_desc' : $mysqlOrderBy = 'o.status DESC'; break;
			case 'payment_status_asc': $mysqlOrderBy = 'o.payment_status'; break;
			case 'payment_status_desc' : $mysqlOrderBy = 'o.payment_status DESC'; break;
			case 'total_amount_without_gift_cert_asc': $mysqlOrderBy = 'total_amount_without_gift_cert'; break;
			case 'total_amount_without_gift_cert_desc' : $mysqlOrderBy = 'total_amount_without_gift_cert DESC'; break;

			/**
			 * Old order by params
			 */
			case 'oid': $mysqlOrderBy = 'o.oid'; break;
			case 'date_placed_desc': $mysqlOrderBy = 'o.placed_date DESC'; break;
			case 'date_placed': $mysqlOrderBy = 'o.placed_date ASC'; break;
			case 'date_changed_desc': $mysqlOrderBy = 'o.status_date DESC'; break;
			case 'date_changed': $mysqlOrderBy = 'o.status_date ASC'; break;
			case 'amount': $mysqlOrderBy = 'total_amount_without_gift_cert ASC'; break;
			case 'amount_desc': $mysqlOrderBy = 'total_amount_without_gift_cert DESC'; break;
			case 'order_num': $mysqlOrderBy = 'o.order_num ASC'; break;
			default: $mysqlOrderBy = 'o.order_num DESC'; break;
		}

		return $this->db->selectAll(
			'SELECT ' . $columns . ', 
				DATE_FORMAT(o.status_date, "' . $this->settings["LocalizationDateTimeFormat"] . '") AS status_date_formatted,
				o.total_amount - o.gift_cert_amount as total_amount_without_gift_cert,
				u.*
			FROM ' . DB_PREFIX . 'orders o
			INNER JOIN ' . DB_PREFIX . 'users u ON o.uid = u.uid AND u.removed = "No"
			' . $innerJoin . ' ' .
			$this->getSearchQuery($searchParams, $logic) . '
			GROUP BY o.oid
			ORDER BY ' . $mysqlOrderBy .
			(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}


	/**
	 * Build query params
	 *
	 * @param $searchParams
	 * @param string $logic
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$whereStr = ' WHERE o.removed = "No" ';

		if (isset($searchParams['status']))
		{
			if (is_array($searchParams['status']))
			{
				$whereStr .= ' AND o.status IN ("'. implode('","', $searchParams['status']).'")';
			}
			else
			{
				if (trim($searchParams['status']) != '' && trim($searchParams['status']) != 'any')
				{
					$whereStr .= ' AND o.status = "'.$this->db->escape(trim($searchParams['status'])).'"';
				}
				else
				{
					$whereStr .= ' AND (o.status <> "Abandon" AND o.status <> "New")';
				}
			}
		}

		$where = array();

		if (isset($searchParams['uid']) && trim($searchParams['uid']) != '' && intval($searchParams['uid']) > 0)
		{
			$where[] = 'o.uid = '.intval($searchParams['uid']);
		}

		if (isset($searchParams['order_num']) && trim($searchParams['order_num']) != '')
		{
			$where[] = 'o.order_num = '.intval(trim($searchParams['order_num']));
		}

		if (isset($searchParams['lname']) && trim($searchParams['lname']) != '')
		{
			$where[] = 'u.lname like "%'.$this->db->escape(trim($searchParams['lname'])).'%"';
		}

		if (isset($searchParams['custom_search']) && trim($searchParams['custom_search']) != '')
		{
			$customData = explode(" ", $this->db->escape(trim($searchParams['custom_search'])));

			for($i=0; $i < count($customData); $i++)
			{
				$where[] = 'u.fname like "%'. $customData[$i] .'%" OR
				u.lname like "%'. $customData[$i] .'%" OR
				u.login like "%'. $customData[$i] .'%" OR
				u.email like "%'. $customData[$i] .'%"';
			}
		}

		if (isset($searchParams['fulfillment_status']))
		{
			if (is_array($searchParams['fulfillment_status']))
			{
				$where[] = 'o.fulfillment_status IN ("'. implode('","', $searchParams['fulfillment_status']).'")';
			}
			else
			{
				if (trim($searchParams['fulfillment_status']) != '' && trim($searchParams['fulfillment_status']) != 'any')
				{
					$where[] = 'o.fulfillment_status = "'.$this->db->escape(trim($searchParams['fulfillment_status'])).'"';
				}
			}
		}

		if (isset($searchParams['min_amount']) && trim($searchParams['min_amount']) != '' && isset($searchParams['max_amount']) && trim($searchParams['max_amount']) != '')
		{
			if(intval($searchParams['min_amount']) <= intval($searchParams['max_amount']))
			{
				$where[] = 'o.total_amount >= "'.$this->db->escape(trim($searchParams['min_amount'])).'" AND o.total_amount <= "'.$this->db->escape(trim($searchParams['max_amount'])).'"';
			}
		}

		if (isset($searchParams['zip_code']) && trim($searchParams['zip_code']) != '')
		{
			$where[] = 'o.shipping_zip = "'.$this->db->escape(trim($searchParams['zip_code'])).'"';
		}

		if (isset($searchParams['product_name']) && trim($searchParams['product_name']) != '')
		{
			$where[] = 'oc.title like "%'.$this->db->escape(trim($searchParams['product_name'])).'%"';
		}

		if (isset($searchParams['state']) && trim($searchParams['state']) != '')
		{
			$where[] = 's.name like "%'.$this->db->escape(trim($searchParams['state'])).'%"';
		}

		if (isset($searchParams['promo_code']) && trim($searchParams['promo_code']) != '')
		{
			if ($searchParams['promo_code'] == '*')
			{
				$where[] = 'o.promo_campaign_id > 0';
			}
			else
			{
				$where[] = 'p.promo_code like "%' . $this->db->escape(trim($searchParams['promo_code'])) . '%"';
			}
		}

		if (isset($searchParams['notes_content']) && trim($searchParams['notes_content']) != '')
		{
			$where[] = 'adn.note_text like "%'.$this->db->escape(trim($searchParams['notes_content'])).'%"';
		}

		if (isset($searchParams['order_type']) && trim($searchParams['order_type']) != '' && trim($searchParams['order_type']) != 'any')
		{
			$where[] = 'o.order_type = "'.$this->db->escape(trim($searchParams['order_type'])).'"';
		}

		if (isset($searchParams['payment_status']))
		{
			if (is_array($searchParams['payment_status']))
			{
				$whereStr .= ' AND o.payment_status IN ("'. implode('","', $searchParams['payment_status']).'")';
			}
			else if (trim($searchParams['payment_status']) != '' && trim($searchParams['payment_status']) != 'any')
			{
				$where[] = 'o.payment_status = "'.$this->db->escape(trim($searchParams['payment_status'])).'"';
			}
		}

		if(isset($searchParams['date_from']) && strlen($searchParams['date_from']) > 0 && isset($searchParams['date_to']) && strlen($searchParams['date_to']) > 0)
		{
			$where[] = 'DATE(o.placed_date) BETWEEN "' . $searchParams['date_from'] . '" AND "' . $searchParams['date_to'] . '"';
		}

		if (isset($searchParams['period']) && trim($searchParams['period']) != '' && trim($searchParams['period']) != 'any')
		{
			$where[] = 'DATE_ADD(o.status_date, INTERVAL '.intval(trim($searchParams['period'])).' HOUR) >= NOW()';
		}

		if (isset($searchParams['product_id']) && trim($searchParams['product_id']) != '')
		{
			$where[] = 'oc.product_id like "%'.$searchParams['product_id'].'%"';
		}

		return $whereStr.(count($where) > 0 ? ' AND ('.implode(' '.$logic.' ', $where).') ' : '').' ';
	}

	/**
	 * Delete orders by ids
	 *
	 * @param $ids
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		$this->db->query('UPDATE '.DB_PREFIX.'orders SET removed = "Yes", status_date = NOW() WHERE oid IN ('.implode(',', $ids).')');

		return true;
	}

	/**
	 * Delete orders in search results
	 *
	 * @param $searchParams
	 *
	 * @return bool
	 */
	public function deleteBySearchParams($searchParams)
	{
		$list = null;
		$start = 0;
		$success = true;
		do
		{
			$list = $this->getList($start, 500, 'oid', $searchParams, 'o.oid');

			if (count($list) > 0)
			{
				$ids = array();
				foreach ($list as $v)
				{
					$ids[] = $v['oid'];
				}
				$result = $this->delete($ids);

				if (!$result) $success = false;
			}
		} while ($list !== null && count($list) > 0);

		return $success;
	}

	/**
	 * @param array $options
	 * @return bool|mixed
	 */
	public function getExportResults(array $options)
	{
		$where = 'o.removed = "No"';

		if ($options['mode'] == 'selected')
		{
			if (!isset($options['ids'])) return false;

			$ids = is_array($options['ids']) ? $options['ids'] : array($options['ids']);

			foreach ($ids as $key=>$value) $ids[$key] = intval($value);

			if (count($ids) < 1) return false;

			$where = 'WHERE o.oid IN ('.implode(',', $ids).')';
		}
		else if ($options['mode'] == 'search')
		{
			$where = $this->getSearchQuery($options['searchParams']);
		}

		return $this->db->query(
			'SELECT o.*,
				DATE_FORMAT(o.placed_date, "%M %e, %Y") AS order_date,
				CONCAT(u.fname, " ", u.lname) AS billing_name,
				u.address1 AS billing_address1,
				u.address2 AS billing_address2,
				u.company AS billing_company,
				u.city AS billing_city,
				IF(u.country = 1 OR u.country = 2, s1.name, u.province) AS billing_state_name,
				u.zip AS billing_zip,
				c1.name AS billing_country_name,
				u.phone AS billing_phone,
				u.email AS billing_email,
				IF(u.country = 1 OR u.country = 2, s2.name, u.province) AS shipping_state_name,
				IF(o.shipping_country = 1 OR o.shipping_country = 2, s2.name, o.shipping_province) AS shipping_state_name,
				c2.name AS shipping_country_name,
				oc.product_id,
				oc.product_sub_id,
				oc.title as product_title,
				oc.quantity as product_quantity,
				oc.price as product_price,
				oc.weight as product_weight,
				oc.options as product_options,
				oc.product_type,
				oc.recurring_billing_data,
				oc.free_shipping,
				of.tracking_number,
				os.*
			FROM '.DB_PREFIX.'orders_content oc
				INNER JOIN '.DB_PREFIX.'orders o ON oc.oid = o.oid
				INNER JOIN '.DB_PREFIX.'users u ON o.uid = u.uid AND u.removed = "No"
				INNER JOIN '.DB_PREFIX.'orders_shipments os ON o.oid = os.oid
				LEFT JOIN '.DB_PREFIX.'orders_fulfillment of ON o.oid = of.oid
				LEFT JOIN '.DB_PREFIX.'countries c1 ON u.country = c1.coid
				LEFT JOIN '.DB_PREFIX.'countries c2 ON o.shipping_country = c2.coid
				LEFT JOIN '.DB_PREFIX.'states AS s1 ON u.state = s1.stid
				LEFT JOIN '.DB_PREFIX.'states AS s2 ON o.shipping_state = s2.stid
			'.$where.'
			ORDER BY o.order_num, oc.pid'
		);
	}

	/**
	 * @param $id
	 * @return array|bool
	 */
	public function getOrderDataForAdmin($id)
	{
		$db = $this->db;

		return $db->selectOne('SELECT o.*,
			DATE_FORMAT(placed_date, "'.$this->settings["LocalizationDateTimeFormat"].'") AS placed_date_formatted,
			DATE_FORMAT(status_date, "'.$this->settings["LocalizationDateTimeFormat"].'") AS status_date_formatted,
			IF(DATE_ADD(placed_date, INTERVAL 3 DAY) < NOW(), "3", "1") AS auth_exp,
			c.name AS country_name,
			c.iso_a2 AS country_iso_a2,
			c.iso_a3 AS country_iso_a3,
			c.iso_number AS country_iso_number,
			s.name AS state_name,
			s.short_name AS state_code,
			oc.name AS offsite_campaign_name,
			u.uid,
			u.fname, u.lname,
			u.company AS billing_company,
			u.address1 AS billing_address1,
			u.address2 AS billing_address2,
			u.city AS billing_city,
			IF(s.name IS NULL, u.province, s.name) as billing_province,
			u.zip as billing_zip,
			c2.name as billing_country_name,
			u.email as billing_email,
			u.phone as billing_phone,
			o.total_amount - o.gift_cert_amount as total_amount_without_gift_cert,
			pm.type as payment_method_type,
			case when pm.type = "custom" then pm.title when pm.type is null then o.payment_method_name else pm.name end as payment_method_name
		FROM '.DB_PREFIX.'orders o
			INNER JOIN '.DB_PREFIX.'users u ON o.uid = u.uid AND u.removed = "No"
			LEFT JOIN '.DB_PREFIX.'countries c ON c.coid = o.shipping_country
			LEFT JOIN '.DB_PREFIX.'countries c2 ON c2.coid = u.country
			LEFT JOIN '.DB_PREFIX.'states s ON s.stid = o.shipping_state
			LEFT JOIN '.DB_PREFIX.'offsite_campaigns oc ON oc.campaign_id = o.offsite_campaign_id
			LEFT JOIN '.DB_PREFIX.'payment_methods pm ON o.payment_method_id = pm.pid
		WHERE
	o.oid='.$id.' AND (o.removed="No")');
	}

	private static $instance;

	/**
	 * @param null $db
	 * @param null $settings
	 * @return DataAccess_OrderRepository
	 */
	public static function getInstance($db = null, &$settings = null)
	{
		if (is_null(self::$instance))
		{
			if (!$db)
			{
				global $db;
			}
			if (!$settings)
			{
				global $settings;
			}

			self::$instance = new self($db, $settings);
		}

		return self::$instance;
	}

	/**
	 * @param DataAccess_OrderRepositoryInterface $instance
	 */
	public static function setInstance(DataAccess_OrderRepositoryInterface $instance)
	{
		self::$instance = $instance;
	}

	/**
	 * @param $id
	 */
	public function getOrderNumber($id)
	{
		$result = $this->db->selectOne('SELECT order_num FROM '.DB_PREFIX.'orders WHERE oid = '. intval($id));

		return $result['order_num'];
	}

	public function updateOrderStatus($orderId, $status)
	{
		if($orderId && trim($status))
		{
			$this->db->query('
			UPDATE '.DB_PREFIX.'orders
			SET status = "'.$status.'" WHERE oid = '.intval($orderId)
			);
		}
	}

	public function updateOrderPaymentStatus($orderId, $status)
	{
		if($orderId && trim($status))
		{
			$this->db->query('
			UPDATE '.DB_PREFIX.'orders
			SET payment_status = "'.$status.'" WHERE oid = '.intval($orderId)
			);
		}
	}
}

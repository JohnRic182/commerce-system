<?php

/**
 * Class DataAccess_DriftMarketingCampaingRepository
 */
class DataAccess_DriftMarketingCampaignRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $mode
	 * @param $companyName
	 * @return array
	 */
	public function getDefaults($mode, $companyName)
	{
		return array(
			'name' => '',
			'htmlEmail' => '
<div style="text-align: center">
    <div style="margin: auto; width: 600px; height: 800px; overflow: auto; border: solid 1px #ababab">
        <h1>' . $companyName .'</h1>
        <hr />
        <div>
            <div>
                {$ItemsInCart}
            </div>
        </div>
        <div>
            <a href="{$CartLink}">Click here to go back to your cart</a>
        </div>
    </div>
</div>',
			'emailSubject' => '',
			'timeframe' => 0
		);
	}

	/**
	 * Get campaign by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'drift_email_campaigns WHERE id='.intval($id));
	}

	/**
	 * Get campaigns count
	 *
	 * @return mixed
	 */
	public function getCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'drift_email_campaigns');
		return intval($result['c']);
	}

	/**
	 * Get campaigns list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';

		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'drift_email_campaigns ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : ''));
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '') $errors['name'] = array('Title is empty');
			if (trim($data['emailSubject']) == '') $errors['emailSubject'] = array('Email subject is empty');
			if (!is_numeric($data['timeframe'])) $errors['timeframe'] = array('Invalid time frame');
			if (trim($data['htmlEmail']) == '') $errors['htmlEmail'] = array('Email content is empty');
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist campaign
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['campaign_id']) ? $data['campaign_id'] : null);

		$db->reset();
		$db->assignStr('name', trim($data['name']));
		$db->assignStr('emailSubject', trim($data['emailSubject']));
		$db->assignStr('htmlEmail', trim($data['htmlEmail']));
		$db->assign('timeframe', is_numeric($data['timeframe']) ? $data['timeframe'] : 0);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'drift_email_campaigns', 'WHERE id='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			return $db->insert(DB_PREFIX.'drift_email_campaigns');
		}
	}

	/**
	 * Delete campaign(s) by id
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);
		foreach ($ids as $key=>$value) $ids[$key] = intval($value);
		if (count($ids) < 1) return;
		$this->db->query('DELETE FROM '.DB_PREFIX.'drift_email_campaigns WHERE id IN('.implode(',', $ids).')');
	}

	/**
	 * Delete all email campaigns
	 */
	public function deleteBySearchParams()
	{
		$this->db->query('DELETE FROM '.DB_PREFIX. 'drift_email_campaigns');
	}

	/**
	 * get Time Frame options
	 */
	public function getTimeFrameOptions()
	{
		return array (
			0 => 'Immediately',
			15 => '15 minutes',
			30 => '30 minutes',
			60 => '1 hour',
			120 => '2 hours',
			240 => '4 hours',
			720 => '12 hours',
			1440 => '24 hours',
			2160 => '36 hours',
			2880 => '48 hours',
			4220 => '3 days',
			5760 => '4 days',
			7200 => '5 days',
			8640 => '6 days',
			10080 => '7 days',
			20160 => '14 days',
			30240 => '21 days'
		);
	}
}
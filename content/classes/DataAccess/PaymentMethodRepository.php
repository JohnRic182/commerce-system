<?php
/**
 * //TODO: Merge with content/classes/Payment/DataAccess/PaymentMethodRepository.php
 * Payment methods access
 */
class DataAccess_PaymentMethodRepository extends DataAccess_Repository
{

	public function __construct(DB $db = null)
	{
		if ($db){
			self::setDatabaseInstance($db);
		}
	}

	/**
	 * Get active payment methods
	 *
	 * @param bool $includeDisabled
	 *
	 * @return array
	 */
	public function getActivePaymentMethods($includeDisabled = false)
	{
		// TODO: check / review getData function
		$_methods = $this->getData('SELECT * FROM `'.DB_PREFIX.'payment_methods` WHERE active="Yes"'.($includeDisabled ? ' OR active="Disabled"' : '').' ORDER BY priority, gateway_name', false);

		$methods = array();

		foreach ($_methods as $method)
		{
			if ($this->methodAvailable($method['id'])) $methods[] = $method;
		}

		return $methods;
	}

	/**
	 * Get ABS list of non-active payment gateways
	 *
	 * @param bool $cache
	 *
	 * @return array
	 */
	public function getPaymentMethods($cache = false)
	{
		$_methods = $this->getData('SELECT *, IF(`id` LIKE "paypal%", 1, 2) `paypals` FROM `'.DB_PREFIX.'payment_methods` ORDER BY `paypals`, `name`', $cache);

		$methods = array();

		foreach ($_methods as $method)
		{
			if ($this->methodAvailable($method['id'])) $methods[] = $method;
		}

		return $methods;
	}
	
	/**
	 * Check is method available
	 * @param string $id
	 * @return boolean 
	 */
	public function methodAvailable($id)
	{
		return
			$this->checkAccess('PAYMENT_ALL') || 
			($this->checkAccess('PAYMENT_PAYPAL_BASIC') && in_array($id, array('paypalec', 'paypalipn', 'paypalwps'))) ||
			($this->checkAccess('PAYMENT_PAYPAL_ALL') && substr($id, 0, 6) == 'paypal') ||
			($this->checkAccess('PAYMENT_WORLDPAY') && substr($id, 0, 8) == 'worldpay') ||
			($this->checkAccess('PAYMENT_AUTHORIZE') && substr($id, 0, 12) == 'authorizenet') ||
			($this->checkAccess('PAYMENT_CHASE') && substr($id, 0, 7) == 'orbital') ||
			($this->checkAccess('CCS') && substr($id, 0, 8) == 'cart_ccs');
	}

	/**
	 * Return payment method groups
	 *
	 * @param bool $cache
	 *
	 * @return array
	 */
	public function getPaymentMethodsGroups($cache = false)
	{
		$groups = array(
			'US' => array('name' => 'United States', 'methods' => array()),
			'GB' => array('name' => 'United Kingdom', 'methods' => array()),
			'CA' => array('name' => 'Canada', 'methods' => array()),
			'AU' => array('name' => 'Australia', 'methods' => array()),
			'global' => array('name' => 'Global / Other Countries', 'methods' => array()),
			'other' => array('name' => 'Other Options', 'methods' => array())
		);
		$methods = $this->getData('SELECT * FROM `'.DB_PREFIX.'payment_methods` ORDER BY `name`', $cache);
		foreach ($methods as $method)
		{
			if ($method['id'] == 'cart_ccs') continue;

			if (trim($method['countries_include']) != '')
			{
				$countries = explode(',', $method['countries_include']);
				foreach ($countries as $country)
				{
					if ($this->methodAvailable($method['id']))
					{
						$groups[trim($country)]['methods'][] = $method;
					}
				}
			}
		}

		$groups['other']['methods'][] = array(
			'pid' => 0,
			'active' => 'No',
			'url_to_gateway' => '',
			'type' => 'custom',
			'priority' => 0,
			'id' => 'custom',
			'class' => '',
			'gateway_name' => '',
			'name' => trans('payment_methods.custom_payment'),
			'title' => '',
			'enable_ccs' => 0,
			'solution_type' => 'custom',
			'countries_include' => null,
			'countries_exclude' => null,
			'message_payment' => '',
			'message_thankyou' => '',
			'description' => '',
			'min_order_amount' => 0,
			'max_order_amount' => 0,
			'countries_filter' => '',
			'supports_recurring_billing' => 0,
		);
		//if (count($groups['other']['methods']) == 0) unset($groups['other']);

		return $groups;
	}
	
	/**
	 * Get payment method's data
	 * @param int $paymentMethodId
	 * @param boolean $activeOnly
	 * @return mixed 
	 */
	public function getPaymentMethodById($paymentMethodId, $activeOnly = false)
	{
		$paymentMethod = $this->getOne($q = 'SELECT * FROM `'.DB_PREFIX.'payment_methods` WHERE id="'.self::$db->escape($paymentMethodId).'"'.($activeOnly ? ' AND active = "Yes"' : ''));
		
		if (!is_null($paymentMethod) && $this->methodAvailable($paymentMethod['id']))
		{
			$paymentMethod['settings'] = $this->getData('SELECT * FROM `'.DB_PREFIX.'settings` WHERE group_name = "payment_'.self::$db->escape($paymentMethod['id']).'" ORDER BY `priority`');
			
			foreach ($paymentMethod['settings'] as $key=>$settingsOption)
			{
				if (trim($settingsOption["caption"]) == "")
				{
					$caption = substr($settingsOption["name"], strlen($paymentMethod["id"]), strlen($settingsOption["name"]) - strlen($paymentMethod["id"]));
					$caption = ucwords(strtolower(str_replace("_", " ", $caption)));
					$paymentMethod['settings'][$key]["caption"] = $caption;
				}
			}

			/**
			 * Get payment methods 
			 */
			$paymentMethod['supported_cards'] = $this->getData('SELECT * FROM `'.DB_PREFIX.'payment_accepted_cc` WHERE pid="'.intval($paymentMethod['pid']).'" ORDER BY `name`');

			/**
			 * Get logo
			 */
			$path = 'content/admin/skins/default/templates/pages/payment/solutions/'.escapeFileName($paymentMethod['id']).'/';

			if (is_file($path.'logo.png'))
				$paymentMethod['logo'] = $path.'logo.png';
			else if (is_file($path.'logo.gif'))
				$paymentMethod['logo'] = $path.'logo.gif';
			else if (is_file($path.'logo.jpg'))
				$paymentMethod['logo'] = $path.'logo.jpg';
			else
				$paymentMethod['logo'] = false;
		}
		
		return $paymentMethod;
	}

	/**
	 * Save the custom payment method
	 *
	 * @param $pid
	 * @param $name
	 * @param $title
	 * @param $messagePayment
	 * @param $messageThankYou
	 * @param bool $active
	 * @param int $priority
	 *
	 * @return int
	 */
	public function saveCustomMethod($pid, $name, $title, $messagePayment, $messageThankYou, $active = true, $priority = 5, $enableFraudService = 1)
	{
		$db = self::$db;

		$db->reset();
		$db->assignStr('type', 'custom');
		$db->assignStr('solution_type', 'custom');
		$db->assignStr('active', $active ? 'Yes' : 'No');
		$db->assignStr('priority', intval($priority));
		$db->assignStr('name', $name);
		$db->assignStr('gateway_name', $name);
		$db->assignStr('title', $title);
		$db->assignStr('message_payment', $messagePayment);
		$db->assignStr('message_thankyou', $messageThankYou);
		$db->assignStr('enable_fraudservice', $enableFraudService);

		$pid = intval($pid);

		if ($pid < 1)
		{
			$pid = $db->insert(DB_PREFIX.'payment_methods');

			$db->reset();
			$db->assignStr('id', 'custom-'.$pid);
			$db->update(DB_PREFIX.'payment_methods', 'WHERE pid='.$pid);
		}
		else
		{
			$db->update(DB_PREFIX.'payment_methods', 'WHERE pid='.$pid);
		}

		return $pid;
	}

	/**
	 * @param $paymentMethodId
	 */
	public function removeCustomMethod($paymentMethodId)
	{
		$db = self::$db;

		$db->query('DELETE FROM '.DB_PREFIX.'payment_methods WHERE `id` = "'.$db->escape($paymentMethodId).'" AND type = "custom"');
	}

	/**
	 * @param $paymentMethodId
	 */
	public function deactivate($paymentMethodId)
	{
		$db = self::$db;

		$paymentMethod = $this->getPaymentMethodById($paymentMethodId);
		if ($paymentMethod['type'] == 'custom')
		{
			$db->query("DELETE FROM ".DB_PREFIX."payment_methods WHERE `id`='".$db->escape($paymentMethodId)."'");
		}
		else if (($paymentMethod['id'] == 'cart_ccs') || ($paymentMethod['id'] == 'braintree') )
		{
			$db->query("UPDATE ".DB_PREFIX."payment_methods SET active='No' WHERE `id`='".$db->escape($paymentMethodId)."'");
		}
		else
		{
			$db->query("UPDATE ".DB_PREFIX."payment_methods SET active='Disabled' WHERE `id`='".$db->escape($paymentMethodId)."'");
		}
	}

	/**
	 * @param $pid
	 * @return bool
	 */
	public function hasActiveRecurringProfiles($pid)
	{
		$db = self::$db;

		$row = $db->selectOne('
						SELECT COUNT(*) AS count
						FROM '.DB_PREFIX.'recurring_profiles rp
						WHERE rp.payment_method_id='.intval($pid).' AND rp.status NOT IN ("Canceled", "Completed")'
		);

		return $row['count'] > 0;
	}

	/**
	 * UPdate payment method
	 *
	 * @param array $paymentMethodData
	 * @param int $pid
	 */
	public function updatePaymentMethod(array $paymentMethodData, $pid = 0)
	{
		$db = self::$db;

		$db->reset();
		if (isset($paymentMethodData['priority'])) $db->assignStr('priority', $paymentMethodData['priority']);
		if (isset($paymentMethodData['name'])) $db->assignStr('name', $paymentMethodData['name']);
		if (isset($paymentMethodData['title'])) $db->assignStr('title', $paymentMethodData['title']);
		if (isset($paymentMethodData['message_payment'])) $db->assignStr('message_payment', $paymentMethodData['message_payment']);
		if (isset($paymentMethodData['message_thankyou'])) $db->assignStr('message_thankyou', $paymentMethodData['message_thankyou']);
		if (isset($paymentMethodData['min_order_amount'])) $db->assign('min_order_amount', $paymentMethodData['min_order_amount']);
		if (isset($paymentMethodData['max_order_amount'])) $db->assign('max_order_amount', $paymentMethodData['max_order_amount']);
		if (isset($paymentMethodData['enable_fraudservice'])) $db->assignStr('enable_fraudservice', $paymentMethodData['enable_fraudservice']);

		if (isset($paymentMethodData['countries_filter']))
		{
			$countries_filter = $paymentMethodData['countries_filter'];

			if (!is_array($countries_filter) && trim($countries_filter) != '')
			{
				$countries_filter = array($countries_filter);
			}
			$db->assignStr('countries_filter', implode(',', $countries_filter));
		}

		if (isset($paymentMethodData['url_to_gateway'])) $db->assignStr('url_to_gateway', $paymentMethodData['url_to_gateway']);
		if (isset($paymentMethodData['enable_ccs'])) $db->assignStr('enable_ccs', $paymentMethodData['enable_ccs']);
		if (isset($paymentMethodData['url_to_gateway'])) $db->assignStr('url_to_gateway', $paymentMethodData['url_to_gateway']);

		$db->update(DB_PREFIX.'payment_methods', 'WHERE pid='.intval($pid));
	}

	/**
	 * Update accelted CC
	 *
	 * @param $pid
	 * @param $ccid
	 * @param $enabled
	 *
	 * @param $cvv2Enabled
	 */
	public function updateAcceptedCC($pid, $ccid, $enabled, $cvv2Enabled)
	{
		$db = self::$db;

		$db->reset();

		$db->assignStr('enable', $enabled ? 'Yes' : 'No');
		$db->assignStr('enable_cvv2', $cvv2Enabled ? 'Yes' : 'No');

		$db->update(DB_PREFIX.'payment_accepted_cc', 'WHERE ccid = '.intval($ccid).' AND pid = '.intval($pid));
	}
}

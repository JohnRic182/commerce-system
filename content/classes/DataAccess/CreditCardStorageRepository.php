<?php

/**
 * Class DataAccess_CreditCardStorageRepository
 */
class DataAccess_CreditCardStorageRepository extends DataAccess_BaseRepository
{
	/**
	 * Get validate certificate params errors
	 *
	 * @param $ccs_username
	 * @param $ccs_password
	 * @param $ccs_password2
	 * @return string
	 */
	public function validateCertParams($ccs_username, $ccs_password,$ccs_password2)
	{
		$errorMessage = '';

		if (strlen($ccs_username) < 8)
		{
			$errorMessage .= 'Unable to generate certificate. Username is too short. Minimum length is 8 characters';
		}
		elseif ($ccs_username == $ccs_password)
		{
			$errorMessage .= 'Unable to generate certificate. Username and password are the same';
		}
		elseif (!pwdStrength($ccs_password))
		{
			$errorMessage .= 'Unable to generate certificate. Password is not strong enough';
		}
		elseif ($ccs_password != $ccs_password2)
		{
			$errorMessage .= 'Unable to generate certificate. Password and confirmation are different';
		}

		return $errorMessage;
	}

	/**
	 * Update Credit Card Storage Certificate and Key in the DB
	 *
	 */
	public function updateCCSkeys($certificate, $private)
	{
		$this->db->query('UPDATE '.DB_PREFIX."settings SET value='".$this->db->escape(base64_encode($certificate))."' WHERE name='SecurityCCSCertificate'");
		$this->db->query('UPDATE '.DB_PREFIX."settings SET value='".$this->db->escape(base64_encode($private))."' WHERE name='SecurityCCSPrivateKey'");
	}
}
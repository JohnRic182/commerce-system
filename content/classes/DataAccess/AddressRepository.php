<?php
/**
 * Class DataAccess_AddressRepository
 */
class DataAccess_AddressRepository extends DataAccess_BaseRepository
{
	/**
	 * Adds missed values to address object
	 *
	 * @param Model_Address $address
	 */
	public function hydrateAddress(Model_Address $address=null)
	{
		if (is_null($address)) return;

		$db = $this->db;

		if ($address->getCountryName() == '' && $address->getCountryId() > 0)
		{
			$data = $db->selectOne('SELECT c.* FROM '.DB_PREFIX.'countries AS c WHERE coid='.intval($address->getCountryId()));
			if ($data)
			{
				$address->setCountryIso2($data['iso_a2']);
				$address->setCountryIso3($data['iso_a3']);
				$address->setCountryIsoNumber($data['iso_number']);
				$address->setCountryName($data['name']);
			}
		}

		if ($address->getStateName() == '' && $address->getStateId() > 0)
		{
			$data = $db->selectOne('SELECT s.* FROM '.DB_PREFIX.'states AS s WHERE s.coid='.intval($address->getCountryId()).' AND s.stid='.intval($address->getStateId()));

			if ($data)
			{
				$address->setStateCode($data['short_name']);
				$address->setStateName($data['name']);
				$address->setProvince($data['name']);
			}
		}
	}
}

<?php

/**
 * Class DataAccess_TextPageRepository
 */
class DataAccess_TextPageRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $mode
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'priority' => '5',
			'custom_form_id' => 0,
			'is_active' => 'No',
			'options' => 'top',
			'options_top' => 0,
			'options_bottom' => 0,
			'url_hash' => '',
			'url_default' => '',
			'url_custom' => '',
			'name' => '',
			'meta_title' => '',
			'meta_description' => '',
			'title' => '',
			'content' => ''
		);
	}

	/**
	 * @param $id
	 * @return array|bool
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT *, IF(url_custom="", url_default, url_custom) AS url FROM ' . DB_PREFIX. 'pages WHERE pid=' . intval($id));
	}

	/**
	 * @param $title
	 * @return array|bool
	 */
	public function getByName($title)
	{
		return $this->db->selectOne('SELECT *, IF(url_custom="", url_default, url_custom) AS url FROM ' . DB_PREFIX . 'pages WHERE title="' . $this->db->escape($title) . '"');
	}

	/**
	 * @param $searchParams
	 * @param string $logic
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$searchParams = is_array($searchParams) ? $searchParams : array();
		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$where = array();
		$searchStr = trim($searchParams['search_str']);

		if (isset($searchParams['search_str']) && $searchStr != '')
		{
			$searchStr = $this->db->escape($searchStr);
			$where[] =
				'((p.title like "%' . $searchStr . '%")' .
				'OR (p.url_default like "%' . $searchStr . '%")' .
				'OR (p.url_custom like "%' . $searchStr . '%")' .
				'OR (p.is_active like "%' . $searchStr . '%"))';
		}

		return (count($where) > 0 ? 'WHERE (' . implode(' '  . $logic . ' ', $where) . ') ' : '') . ' ';
	}

	/**
	 * @param null $searchParams
	 * @param string $logic
	 * @return int
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM ' . DB_PREFIX . 'pages AS p ' . $this->getSearchQuery($searchParams, $logic));

		return intval($result['c']);
	}

	/**
	 * Get pages list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 'p.*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'priority';

		switch ($orderBy)
		{
			/**
			 * New order by params
			 */
			case 'title_asc' : $orderBy = 'p.title'; break;
			case 'title_desc' : $orderBy = 'p.title DESC'; break;
			case 'url_asc' : $orderBy = 'url'; break;
			case 'url_desc' : $orderBy = 'url DESC'; break;
			case 'is_active_asc' : $orderBy = 'p.is_active'; break;
			case 'is_active_desc' : $orderBy = 'p.is_active DESC'; break;

			default: $orderBy = 'p.priority';	break;
		}

		return $this->db->selectAll('
			SELECT ' . $columns . ', IF(url_custom="", url_default, url_custom) AS url 
			FROM ' . DB_PREFIX . 'pages AS p ' . $this->getSearchQuery($searchParams, $logic) . ' 
			ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['title']) == '') $errors['title'] = array(trans('text_page.required_title'));

			if (trim($data['priority']) == '') $errors['priority'] = array(trans('text_page.required_position'));
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist page
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pid']) ? $data['pid'] : null);

		$db->reset();

		$db->assignStr('is_active', isset($data['is_active']) && $data['is_active'] == 'Yes' ? 'Yes' : 'No');
		$db->assignStr('custom_form_id', intval($data['custom_form_id']));
		$db->assignStr('title', isset($data['title']) ? $data['title'] : '');
		$db->assignStr('priority', isset($data['priority'])? $data['priority'] : '0');
		
		$options = array();
		if (isset($data['options_top']) && $data['options_top'] == 1) $options[] = 'top';
		if (isset($data['options_bottom']) && $data['options_bottom'] == 1) $options[] = 'bottom';

		$options_str = (count($options) > 0) ? implode(',', $options) : '';

		$db->assignStr('options', $options_str);
		$db->assignStr('content', isset($data['content']) ? $data['content'] : '');
		
		if (isset($data['url_custom'])) $db->assignStr('url_custom', $data['url_custom']);
		if (isset($data['meta_title']) && !is_null($data['meta_title'])) $db->assignStr('meta_title', $data['meta_title'] == '' ? null : $data['meta_title']);
		if (isset($data['meta_description']) && !is_null($data['meta_description'])) $db->assignStr('meta_description', $data['meta_description'] == '' ? null : $data['meta_description']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX . 'pages',  'WHERE pid=' . intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			$res = $db->insert(DB_PREFIX . 'pages');
			return $res;
		}
	}

	/**
	 * Delete pages by ids
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key => $value) $ids[$key] = intval($value);

		if (count($ids) < 1) return;

		// delete pages
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'pages WHERE pid IN(' . implode(',', $ids) . ')');
	}

	/**
	 * Delete pages by search results
	 *
	 * @param $searchParams
	 */
	public function deleteBySearchParams($searchParams)
	{
		$searchQuery = $this->getSearchQuery($searchParams);

		$ids = array();

		$result = $this->db->query('SELECT pid FROM ' . DB_PREFIX . 'pages ' . $searchQuery);

		if ($this->db->numRows($result) < 1) return;

		while ($page = $this->db->moveNext($result))
		{
			$ids[] = $page['pid'];
		}

		// delete pages
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'pages ' . $searchQuery);
	}

	/**
	 * @param $url
	 * @param $pid
	 * @return int
	 */
	public function hasUrlDuplicate($url, $pid)
	{
		$escapedUrl = $this->db->escape($url);
		$db = $this->db;

		$result = $db->query('
			SELECT url_custom
			FROM '.DB_PREFIX.'pages
			WHERE (url_custom = "' . $escapedUrl . '" OR url_default = "' . $escapedUrl . '") AND pid != ' . intval($pid) . '

			UNION

			SELECT url_custom
			FROM ' . DB_PREFIX . 'products
			WHERE url_custom = "' . $escapedUrl . '" OR url_default = "' . $escapedUrl . '"

			UNION

			SELECT url_custom FROM ' . DB_PREFIX . 'manufacturers
			WHERE url_custom = "' . $escapedUrl . '" OR url_default = "' . $escapedUrl . '"

			UNION

			SELECT url_custom
			FROM ' . DB_PREFIX . 'catalog
			WHERE url_custom = "' . $escapedUrl . '" OR url_default = "' . $escapedUrl . '"
		');

		return $db->numRows($result);
	}

	/**
	 * @return array
	 */
	public function getPagesLinks()
	{
		$db = $this->db;
		$db->query("
			SELECT IF(url_custom='', url_default, url_custom) AS url, name, title, options
			FROM " . DB_PREFIX . "pages
			WHERE is_active = 'Yes'
			ORDER BY priority, title
		");

		$pages = array();

		while ($db->moveNext())
		{
			$pages[] = array(
				"name" => $db->col["name"],
				"title" => $db->col["title"],
				"top" => (strstr($db->col["options"],'top') ? 1 : 0),
				"bottom" => (strstr($db->col["options"],'bottom') ? 1 : 0),
				"url" => UrlUtils::getPageUrl($db->col["name"], $db->col["url"])
			);
		}

		return $pages;
	}
	
	public function publishPages($data, $mode)
	{
		$mode = $mode == 'publish' ? 'Yes' : 'No';
		if (is_array($data))
		{
			if (count($data) < 1) return;
			$this->db->query('UPDATE '.DB_PREFIX.'pages SET is_active="'.$mode.'" WHERE pid IN('.implode(',', $data).')');
		}
		else if ($data == 'all')
		{
			$this->db->query('UPDATE '.DB_PREFIX.'pages SET is_active="'.$mode.'"');
		}
	}
}
<?php

/**
 * Class DataAccess_PromoCodeRepository
 */
class DataAccess_PromoCodeRepository extends DataAccess_BaseRepository implements DataAccess_PromoCodeRepositoryInterface
{
	protected $db;
	protected $settings = array();

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 */
	public function __construct(DB $db, &$settings)
	{
		if ( $settings )
		{
			$this->settings = $settings;
		}
		parent::__construct($db);
	}

	/**
	 * Get promo code from database
	 *
	 * @param string $promoCode
	 *
	 * @return mixed
	 */
	public function getPromoCode($promoCode)
	{
		$result = $this->db->selectOne($this->getActivePromoCodeQuery()." AND promo_code='".$this->db->escape($promoCode)."'");
		return $result ? $result : null;
	}

	/**
	 * Get promo campaign by id
	 *
	 * @param int $promoCampaignId
	 *
	 * @return mixed
	 */
	public function getPromoCodeByPromoCampaignId($promoCampaignId)
	{
		$result = $this->db->selectOne($this->getActivePromoCodeQuery()." AND pid = ".intval($promoCampaignId));
		return $result ? $result :null;
	}

	/**
	 * Get promo products
	 *
	 * @param int $oid
	 * @param int $promoCampaignId
	 *
	 * @return mixed
	 */
	public function getPromoProducts($oid, $promoCampaignId)
	{
		return $this->db->selectAll("
			SELECT o.pid
			FROM ".DB_PREFIX."orders_content o
				INNER JOIN ".DB_PREFIX."orders oz ON o.oid = oz.oid
				INNER JOIN ".DB_PREFIX."products_categories c ON o.pid = c.pid
				LEFT JOIN ".DB_PREFIX."promo_categories pc ON c.cid = pc.cid AND pc.pid = ".intval($promoCampaignId)."
				LEFT JOIN ".DB_PREFIX."promo_products pp ON o.pid = pp.prid AND pp.pid = ".intval($promoCampaignId)."
			WHERE o.oid = ".intval($oid)." AND (pp.pid IS NOT NULL OR pc.pid IS NOT NULL)
			GROUP BY o.pid
		");
	}

	/**
	 * Return query
	 *
	 * @return string
	 */
	protected function getActivePromoCodeQuery()
	{
		return "SELECT * FROM ".DB_PREFIX."promo_codes WHERE date_start <= NOW() AND date_stop >= NOW() AND is_active='Yes'";
	}

	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		$ts = time();
		$t_y = date("Y", $ts);
		$t_m = date("m", $ts);
		$t_d = date("d", $ts);
		$start_date = "$t_m/01/$t_y";
		if ($t_m < 12)
		{
			$t_m = $t_m + 1;
			$end_date = sprintf('%02d', $t_m) . '/01/' . $t_y;
		}
		else
		{
			$t_y = $t_y + 1;
			$end_date = "01/01/$t_y";;
		}
		return array(
			'pid' => '',
			'name' => '',
			'promo_code' => $mode == Admin_Controller::MODE_ADD ? $this->generateVoucher() : '',
			'promo_type' => 'Global',
			'date_start' => '',
			'start_date' => $start_date,
			'start_hour' => '0',
			'start_min' => '0',
			'date_stop' => '',
			'end_date' => $end_date,
			'end_hour' => '0',
			'end_min' => '0',
			'is_active' => 'Yes',
			'min_amount' => '0',
			'min_amount_type' => 'order_subtotal',
			'discount' => '',
			'discount_type' => 'amount',
			'is_global' => 'Yes'
		);
	}
	
	/**
	 * Get promo code by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		$promo_code = $this->db->selectOne('SELECT * FROM ' . DB_PREFIX . 'promo_codes WHERE pid=' . intval($id));

		$ts = strtotime($promo_code['date_start']);
		$promo_code['start_date'] = date('m/d/Y', $ts);
		$promo_code['start_hour'] = date('H', $ts);
		$promo_code['start_min'] = date('i', $ts);

		$ts = strtotime($promo_code['date_stop']);
		$promo_code['end_date'] = date('m/d/Y', $ts);
		$promo_code['end_hour'] = date('H', $ts);
		$promo_code['end_min'] = date('i', $ts);

		return $promo_code;
	}

	/**
	 * Get promo codes count
	 *
	 * @param null $searchParams
	 * @param string $logic
	 * @return mixed
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('
			SELECT COUNT(*) AS cnt
			FROM '.DB_PREFIX.'promo_codes p
			' . $this->getSearchQuery($searchParams, $logic)
		);

		return intval($result['cnt']);
	}

	
	/**
	 * Get promo codes list
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 'p.*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';

		switch ($orderBy)
		{
			/**
			 * New order by params
			 */
			case 'name_asc' : $orderBy = 'p.name'; break;
			case 'name_desc' : $orderBy = 'p.name DESC'; break;
			case 'promo_code_asc' : $orderBy = 'p.promo_code'; break;
			case 'promo_code_desc' : $orderBy = 'p.promo_code DESC'; break;
			case 'discount_asc' : $orderBy = 'p.discount'; break;
			case 'discount_desc' : $orderBy = 'p.discount'; break;
			case 'promo_type_asc' : $orderBy = 'p.promo_type'; break;
			case 'promo_type_desc' : $orderBy = 'p.promo_type DESC'; break;
			case 'discount_type_asc' : $orderBy = 'p.discount_type'; break;
			case 'discount_type_desc' : $orderBy = 'p.discount_type DESC'; break;
			case 'min_amount_asc' : $orderBy = 'p.min_amount'; break;
			case 'min_amount_desc' : $orderBy = 'p.min_amount DESC'; break;
			default: $orderBy = 'p.name';	break;

		}

		return $this->db->selectAll(
				'SELECT ' . $columns . ',
				DATE_FORMAT(date_start, "%m/%d/%Y %I:%i %p") date_start, 
				DATE_FORMAT(date_stop, "%m/%d/%Y %I:%i %p") date_stop 
				FROM '.DB_PREFIX.'promo_codes AS p ' . $this->getSearchQuery($searchParams, $logic) . ' 
				ORDER BY ' . $orderBy . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}
	
	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '')
			{
				$errors['name'] = array('Please enter Title');
			}

			if (trim($data['promo_code']) == '')
			{
				$errors['promo_code'] = array('Promo Code is missing');
			} else {
				// check that the code is unique
				if (
					($mode == 'add' and $this->db->selectOne("SELECT 1 FROM ".DB_PREFIX."promo_codes WHERE promo_code = '" . $this->db->escape(trim($data['promo_code'])) . "'"))
					or
					($mode == 'update' and $this->db->selectOne("SELECT 1 FROM ".DB_PREFIX."promo_codes WHERE promo_code = '" . $this->db->escape(trim($data['promo_code'])) . "' AND pid <> " . intval($id)))
				)
				{
					$errors['code'] = array('Promo Code must be unique');
				}
			}

			if (trim($data['discount']) == '')
			{
				$errors['discount'] = array('Please enter Discount');
			}
			elseif (!is_numeric($data['discount']))
			{
				$errors['discount'] = array('Discount must be numeric');
			}
			elseif ($data['discount'] <= 0) 
			{
				$errors['discount'] = array('Discount must be greater than zero');
			}
			elseif ($data['discount_type'] == 'percent' and $data['discount'] > 100) {
				$errors['discount'] = array('Discount must be between 1 and 100%');
			}

			if (trim($data['min_amount']) == '')
			{
				$errors['min_amount'] = array('Please enter Minimum Order Subtotal');
			}
			elseif (!is_numeric($data['min_amount']))
			{
				$errors['min_amount'] = array('Minimum Order Subtotal must be numeric');
			}
			elseif ($data['min_amount'] < 0)
			{
				$errors['min_amount'] = array('Minimum Order Subtotal must be greater or equal to zero');
			}

			if (trim($data['start_date']) == '') {
				$errors['start_date'] = array('Start date is missing');
			} else {
				if (date("m/d/Y", strtotime($data['start_date'])) != trim($data['start_date'])) {
					$errors['start_date'] = array('Invalid start date');
				}
			}

			if (trim($data['end_date']) == '') {
				$errors['end_date'] = array('End date is missing');
			} else {
				if (date("m/d/Y", strtotime($data['end_date'])) != trim($data['end_date'])) {
					$errors['start_date'] = array('Invalid end date');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}
	
	
	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationSettingsErrors($data, $mode, $id = null)
	{
		return false;
	}

	/**
	 * Persist promo codes
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;
		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			if ($data['promo_type'] != "Product")
			{
				$db->query("DELETE FROM " . DB_PREFIX . "promo_categories WHERE pid = '" . intval($data['id']) . "'");
				$db->query("DELETE FROM " . DB_PREFIX . "promo_products WHERE pid = '" . intval($data['id']) . "'");
			}
			else
			{
				$promo_categories = isset($data['promo_categories']) ? $data['promo_categories'] : array();
				$this->persistPromoCategories($data['id'], $promo_categories);
			}
		}
		
		$data['id'] = isset($data['id']) ? $data['id'] : null;

		$db->reset();
		$db->assignStr('name', $data['name']);
		$db->assignStr('promo_type', $data['promo_type']);
		$db->assignStr('promo_code', $data['promo_code']);
		$db->assignStr('is_active', $data['is_active']);

		$date_start = date('Y-m-d', strtotime($data['start_date'])) . ' ' . sprintf('%02d', $data['start_hour']) . ':' . sprintf('%02d', $data['start_min']) . ':00';
		$date_end = date('Y-m-d', strtotime($data['end_date'])) . ' ' . sprintf('%02d', $data['end_hour']) . ':' . sprintf('%02d', $data['end_min']) . ':00';
		
		$db->assignStr('date_start', $date_start);
		$db->assignStr('date_stop', $date_end);
		$db->assign('min_amount', $data['min_amount']);
		$db->assignStr('min_amount_type', $data['min_amount_type']);
		$db->assign('discount', $data['discount']);
		$db->assignStr('discount_type', $data['discount_type']);
		$db->assignStr('is_global', $data['is_global']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'promo_codes', 'WHERE pid='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			return $db->insert(DB_PREFIX.'promo_codes');
		}
	}

	/**
	 * persist Promo Categories  
	 */
	public function persistPromoCategories($id, $promo_categories)
	{
		$db = $this->db;
		$db->query("DELETE FROM " . DB_PREFIX . "promo_categories WHERE pid = '" . intval($id) . "'");
		
		if (isset($promo_categories) && !is_array($promo_categories))
		{
			$promo_categories = explode(',', $promo_categories);
		}
		
		if (isset($promo_categories) && count($promo_categories) > 0 && is_array($promo_categories))
		{
			foreach ($promo_categories as $val)
			{
				$db->reset();
				$db->assign('pid', intval($id));
				$db->assign('cid', intval($val));
				$db->insert(DB_PREFIX.'promo_categories');
			}
		}
	}

	/**
	 * Get Settings for Promotions Advanced Settings form
	 *
	 * @return array
	 */
	public function getAdvancedSettingsData()
	{
		return $this->settings;
	}

	/**
	 * Delete promo codes(s) by id
	 *
	 * @param $ids
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);
		foreach ($ids as $key=>$value) $ids[$key] = intval($value);
		if (count($ids) < 1) return;
		$this->db->query('DELETE FROM '.DB_PREFIX.'promo_codes WHERE pid IN('.implode(',', $ids).')');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'promo_categories WHERE pid IN('.implode(',', $ids).')');
		$this->db->query('DELETE FROM ' . DB_PREFIX . 'promo_products WHERE pid IN('.implode(',', $ids).')');
	}

	/**
	 * Delete all promo codes
	 *
	 */
	public function deleteBySearchParams()
	{
		$this->db->query("DELETE FROM ".DB_PREFIX. "promo_codes");
		$this->db->query("DELETE FROM " . DB_PREFIX . "promo_categories");
		$this->db->query("DELETE FROM " . DB_PREFIX . "promo_products");
	}

	/**
	 * Persist Promo Code Settings
	 *
	 * @param $data
	 */
	public function persistPromoCodeSettings($data)
	{
		$promoCodeSettings = array();
		if ( isset($data['promoCodesSettings']) )
		{
			$promoCodeSettings['DiscountsPromo'] = $data['promoCodesSettings']['DiscountsPromo'];
		}
		else 
		{
			$promoCodeSettings['DiscountsPromo'] = 'NO';
		}
		
		if (is_array($promoCodeSettings) && count($promoCodeSettings) > 0)
		{
			$settingsRepository = new DataAccess_SettingsRepository($this->db, $this->settings);
			$settingsRepository->save($promoCodeSettings);
			return true;
		}
		return false;
	}

	/**
	 * get product list of products which aren't assigned $promo_id yet
	 */
	public function getProducts($categories, $promo_id)
	{
		$categories = trim($categories) == "" ? "0" : $categories;
		$categories = preg_match("/^([0-9\,])+$/", $categories) ? $categories : '0';
		$where = ($categories == "0") ? (" 1=1 ") : (" p.cid IN (".$categories.") ");
		$q = "
			SELECT p.pid, p.tax_class_id, p.title
			FROM ".DB_PREFIX."products p
			WHERE p.pid NOT IN (
				SELECT prid
				FROM ".DB_PREFIX."promo_products pp
				WHERE pp.pid = " . intval($promo_id) . "
			)
			AND " . $where . "
			AND p.product_id != 'gift_certificate'
			ORDER BY p.title
		";
		$this->db->query($q);
		return array('count'=>$this->db->numRows(), 'products'=>$this->db->getRecords());
	}

	/**
	 * get products assigned to promo code
	 */
	public function getAssignedProducts($categories, $promo_id)
	{
		$q = "
			SELECT p.pid, p.title
			FROM ".DB_PREFIX."products p
			WHERE p.pid in (
				SELECT pp.prid
				FROM ".DB_PREFIX."promo_products pp
				WHERE pp.pid = " . intval($promo_id) . "
			)
			ORDER BY p.title";
		$this->db->query($q);
		return array("count"=>$this->db->numRows(), "products"=>$this->db->getRecords());
	}

	/**
	 * assign products to promo code
	 */
	public function assignProducts($products, $promo_id)
	{	
		if ($promo_id > 0)
		{
			$products = trim($products);
			$products = explode(',', $products);
			foreach ($products as $pid)
			{
				if (is_numeric($pid))
				{
					$this->db->reset();
					$this->db->assign('pid', intval($promo_id));
					$this->db->assign('prid', intval($pid));
					$this->db->insert(DB_PREFIX."promo_products");
				}
			}
			return array('result'=>1);
		}
		else
		{
			return array('result'=>0);
		}
	}

	/**
	 * unassign products from promo code
	 *
	 */
	public function unassignProducts($products, $promo_id)
	{
		if ($promo_id > 0)
		{
			$products = trim($products);
			$products = explode(",",$products);
			foreach ($products as $pid)
			{
				if (is_numeric($pid))
				{
					$this->db->query("DELETE FROM ".DB_PREFIX."promo_products WHERE pid=". intval($promo_id)." AND prid = " . intval($pid));
				}
			}
			return array('result'=>1);
		}
		else
		{
			return array('result'=>0);
		}
	}

	/**
	 * get Assigned Categories
	 * 
	 */
	public function getAssignedCategories($promo_id)
	{
		$categories = array();
		$this->db->query('SELECT cid FROM '.DB_PREFIX.'promo_categories WHERE pid='.intval($promo_id));
		while ($this->db->moveNext())
		{
			$categories[] = $this->db->col['cid'];
		}
		return $categories;
	}

	/**
	 * Generate a promo code
	 *
	 * @return string
	 */
	public function generateVoucher()
	{
		$voucher = '';
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		for ($i = 0; $i < 3; $i++)
		{
			$voucher .= $characters[rand(10, 62 - 1)];
		}
		for ($i=0; $i < 7; $i++)
		{
			$voucher .= rand(0, 9);
		}
		return $voucher;
	}

	/**
	 * @param $searchParams
	 * @param string $logic
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$searchParams = is_array($searchParams) ? $searchParams : array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$where = array();

		if (isset($searchParams['search_str']) && $searchParams['search_str'] != '')
		{
			$searchStr = trim($searchParams['search_str']);
			$searchStr = $this->db->escape($searchStr);
			$where[] =
				'((p.name like "%' . $searchStr . '%")' .
				'OR (p.promo_code like "%' . $searchStr . '%")' .
				'OR (p.promo_type like "%' . $searchStr . '%")' .
				'OR (p.discount_type like "%' . $searchStr . '%")' .
				'OR (p.discount_type like "%' . $searchStr . '%")' .
				'OR (p.discount like "%' . $searchStr . '%"))';

		}

		return (count($where) > 0 ? 'WHERE (' . implode(' '  . $logic . ' ', $where) . ') ' : '') . ' ';
	}
}
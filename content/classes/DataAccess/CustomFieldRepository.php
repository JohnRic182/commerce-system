<?php

/**
 * Class DataAccess_CustomFieldRepository
 */
class DataAccess_CustomFieldRepository extends DataAccess_BaseRepository
{
	protected $customFieldSectionOptions = array();
	protected $customFieldTypeOptions = array();
	protected $customFieldPositionOptions = array();

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		parent::__construct($db);

		$this->customFieldSectionOptions = array(
			'billing' => 'Billing Information',
			'shipping' => 'Shipping Information',
			'account' => 'Account Information',
			'signup' => 'Registration Form',
			'invoice' => 'Invoice Additional Information'
		);

		$this->customFieldTypeOptions = array(
			'text' => 'Text Input',
			'select' => 'Drop-down',
			'radio' => 'Radio Buttons',
			'checkbox' => 'Checkbox',
			'textarea' => 'Text Area'
		);

		for ($i=1; $i<=10; $i++)
		{
			$this->customFieldPositionOptions[$i] = $i;
		}
	}

	/**
	 * @return array
	 */
	public function getCustomFieldSectionOptions()
	{
		return $this->customFieldSectionOptions;
	}

	/**
	 * @return array
	 */
	public function getCustomFieldTypeOptions()
	{
		return $this->customFieldTypeOptions;
	}

	/**
	 * @return array
	 */
	public function getCustomFieldPositionOptions()
	{
		return $this->customFieldPositionOptions;
	}

	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'field_type' => 'text',
			'is_active' => 0,
			'is_required' => 0,
			'validation_type' => 'none',
			'priority' => 1,
			'field_place' => 'signup',
			'field_name' => '',
			'field_id' => '',
			'field_caption' => '',
			'text_length' => 20,
			'value_checked' => 'Yes',
			'value_unchecked' => 'No',
			'options' => ''
		);
	}

	/**
	 * @return int
	 */
	public function getCustomFieldsCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'custom_fields');

		return intval($result['c']);
	}

	/**
	 * @param $id
	 * @return array|bool
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'custom_fields WHERE field_id='.intval($id));
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @return array|mixed
	 */
	public function getCustomFieldsList($start = null, $limit = null, $orderBy = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);

		if (is_null($orderBy)) $orderBy = 'field_place, priority';

		$customfields = $this->db->selectAll("SELECT * FROM ".DB_PREFIX."custom_fields".
			' ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);

		return $customfields;
	}

	/**
	 * @param $data
	 * @param null $id
	 * @return array|bool
	 */
	public function getValidationErrors($data, $id = null)
	{
		$errors = array();

		if (is_array($data))
		{
			if (trim($data['field_place']) == '') $errors['field_place'] = array('Please choose section');
			if (trim($data['field_name']) == '') $errors['field_name'] = array('Please enter field name');
			if (isset($data['field_code']))
			{
				if (trim($data['field_code']) == '') $errors['field_code'] = array('Please enter field code');
			}
			if (trim($data['priority']) == '') $errors['priority'] = array('Please enter field position');
			if (trim($data['field_type']) == '')
			{
				$errors['field_type'] = array('Please select field type');
			}
			else
			{
				switch ($data['field_type'])
				{
					case 'text' :
					case 'textarea' :
					{
						if (trim($data['text_length']) == '' || !is_numeric($data['text_length']))
						{
							$errors['text_length'] = array('Please enter valid character limit');
						}
						break;
					}
					case 'select' :
					case 'radio' :
					{
						if (trim($data['options']) == '') $errors['options'] = array('Please enter options');
						break;
					}
					case 'checkbox' :
					{
						if (trim($data['value_checked']) == '') $errors['value_checked'] = array('Please enter checked value');
						if (trim($data['value_unchecked']) == '')  $errors['value_unchecked'] = array('Please enter unchecked value');
						break;
					}
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param $data
	 * @return array|bool
	 */
	public function getDeleteCustomFieldsValidationErrors($data)
	{
		$db = $this->db;

		$errors = array();
		if (is_array($data))
		{
			foreach ($data as $k => $v)
			{
				if ($db->selectOne('SELECT count(profile_id) as c FROM '.DB_PREFIX.'custom_fields_values WHERE field_id="'.intval($v).'"'))
				{
					if ($db->col['c'] > 0)
					{
						$errors['list_action'] = array('Selected custom field(s) are being used by '.$db->col['c'].' object(s) and cannot be removed.');
					}
				}
			}
		}
		else if ($data == 'all')
		{
			$db = $this->db;
			if ($db->selectOne('SELECT count(profile_id) as c FROM '.DB_PREFIX.'custom_fields_values WHERE field_id <> 0'))
			{
				if ( $db->col['c'] > 0)
				{
					$errors['list_action'] = array("Can't remove all custom fields, in use by '.$db->col['c'].' object(s).");
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param $ids
	 */
	public function delete($ids)
	{
		if ($ids == 'all')
		{
			$this->db->query('DELETE FROM ' . DB_PREFIX . 'custom_fields');
		}
		else
		{
			$ids = is_array($ids) ? $ids : array($ids);
			foreach ($ids as $key => $value) $ids[$key] = intval($value);
			if (count($ids) < 1) return;
			$this->db->query('DELETE FROM ' . DB_PREFIX . 'custom_fields WHERE field_id IN (' . implode(',', $ids) . ')');
		}
	}

	/**
	 * @param $data
	 * @param $params
	 * @return bool
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['field_id']) ? $data['field_id'] : null);

		$db->reset();

		$data['is_active'] = (isset($data['is_active'])&&($data['is_active'] == 1)?1:0);
		$data['is_required'] = (isset($data['is_required'])&&($data['is_required'] == 1)?1:0);

		$db->assignStr('custom_form_id', isset($data['custom_form_id']) ? $data['custom_form_id'] : 0);
		$db->assignStr('is_active', $data['is_active']);
		$db->assignStr('is_required', $data['is_required']);
		$db->assignStr('priority', $data['priority']);
		$db->assignStr('field_place', $data['field_place']);
		$db->assignStr('field_name', $data['field_name']);
		$db->assignStr('field_code', $data['field_code']);
		$db->assignStr('field_caption', $data['field_caption']);
		$field_type = $data['field_type'];

		if ($field_type == 'text'|| $field_type == 'textarea')
		{
			$db->assignStr('text_length', $data['text_length']);
		}

		if ($field_type == 'checkbox')
		{
			$db->assignStr('value_checked', $data['value_checked']);
			$db->assignStr('value_unchecked', $data['value_unchecked']);
		}

		if ($field_type == 'select' || $field_type == 'radio')
		{
			$db->assignStr('options', $data['options']);
		}
		else
		{
			$db->assignStr('options', '');
		}
		
		$db->assignStr('field_type', $field_type);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX . 'custom_fields', 'WHERE field_id=' . intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			return $db->insert(DB_PREFIX . 'custom_fields');
		}
	}

	/**
	 * @param $formId
	 * @return mixed
	 */
	public function getListByFormId($formId)
	{
		return $this->db->selectAll('
			SELECT *
			FROM ' . DB_PREFIX . 'custom_fields
			WHERE field_place="form" AND custom_form_id=' . intval($formId) . '
			ORDER BY priority, field_name
		');
	}
}
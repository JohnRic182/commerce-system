<?php

/**
 * Off-Site Commerce Campaigns class
 */
class DataAccess_OffsiteCommerceCampaignRepository extends DataAccess_BaseRepository
{
	/**
	 * @param $type
	 * @param $dateFormat
	 * @return mixed
	 */
	public function getCampaigns($type, $dateFormat)
	{
		$db = $this->db;

		//$this->_settings["LocalizationDateTimeFormat"]
		return $db->selectAll('
			SELECT
				offsite_campaigns.*,
				DATE_FORMAT(created, "'.$db->escape($dateFormat).'") AS created
			FROM '.DB_PREFIX.'offsite_campaigns AS offsite_campaigns
			WHERE
				type="'.$db->escape($type).'"
			ORDER BY created
		');
	}

	/**
	 * @param $type
	 * @return array
	 */
	public function getCampaignsOptions($type)
	{
		$db = $this->db;

		$options = array();
		$campaigns = $db->selectAll('
			SELECT campaign_id, name FROM '.DB_PREFIX.'offsite_campaigns AS offsite_campaigns
			WHERE type="'.$db->escape($type).'" ORDER BY created
		');
 
		foreach ($campaigns as $campaign)
		{
			$options[$campaign['campaign_id']] = $campaign['campaign_id'].': '.$campaign['name'];
		}

		return $options;
	}
}
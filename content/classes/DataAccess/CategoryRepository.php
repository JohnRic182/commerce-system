<?php
/**
 * Class DataAccess_AddressRepository
 */
class DataAccess_CategoryRepository extends DataAccess_BaseRepository
{
	/** @var array|null $tree */
	protected $cachedTree = null;
	protected $parentTree = array();

	protected $imagesPath = 'images/catalog';

	protected $cachedTreeFieldsHash = null;

	/**
	 * Get default
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		$defaults = array(
			'parent' => '0',
			'name' => '',
			'key_name' => '',
			'description' => '',
			'is_visible' => 'No',
			'is_stealth' => '0',
			'list_subcats' => 'No',
			'list_subcats_images' => '0',
			'is_doba' => '0',
			'doba_category_id' => '0',
			'doba_mapped_category_id' => '0',
			'meta_title' => null,
			'meta_description' => null,
			'url_custom' => '',
			'url_default' => '',
			'priority' => 5,
			'avalara_tax_code' => '',
			'exactor_euc_code' => '',
		);

		return $defaults;
	}

	/**
	 * Get key names
	 *
	 * @return mixed
	 */
	public function getCategoryKeyNames()
	{
		$categories = array();
		$result = $this->db->query('SELECT cid, key_name FROM '.DB_PREFIX.'catalog');

		while ($category = $this->db->moveNext($result))
		{
			$categories[strtolower(trim($category['key_name']))] = $category['cid'];
		}

		return $categories;
	}

	/**
	 * Update categories parent id
	 *
	 * @param $ids
	 *
	 * @param $parentId
	 */
	public function updateCategoriesParent($ids, $parentId)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		$this->db->query('UPDATE '.DB_PREFIX.'catalog SET parent='.intval($parentId).' WHERE cid IN('.implode(',', $ids).')');
	}

	/**
	 * Get category by id
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		$category = $this->db->selectOne('
			SELECT *, IF(url_custom="", url_default, url_custom) AS url
			FROM ' . DB_PREFIX . 'catalog
			WHERE cid=' . intval($id)
		);

		if ($category)
		{
			$category['image'] = $this->getCategoryImage($category['cid']);
			$category['thumb'] = $this->getCategoryThumb($category['cid']);
		}

		return $category;
	}

	/**
	 * get category by keyname
	 *
	 * @param $keyName
	 *
	 * @return array|bool
	 */
	public function getByKeyName($keyName)
	{
		return $this->db->selectOne('
			SELECT *, IF(url_custom="", url_default, url_custom) AS url
			FROM ' . DB_PREFIX.'catalog
			WHERE key_name="' . $this->db->escape($keyName).'"
		');
	}

	/**
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 * @param null $searchParams
	 * @param string $columns
	 * @param string $logic
	 * @return int
	 */
	public function getList($start = null, $limit = null, $orderBy = null, $searchParams = null, $columns = 'c.*', $logic = 'AND')
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';

		switch ($orderBy)
		{
			case 'key_name': $order_by = 'c.key_name'; break;
			default: $order_by = 'c.name'; break;
		}

		return $this->db->selectAll('
			SELECT ' . $columns . '
			FROM ' . DB_PREFIX . 'catalog c ' . $this->getSearchQuery($searchParams, $logic) . '
			ORDER BY ' . $order_by . (!is_null($start) && !is_null($limit) ? (' LIMIT ' . $start . ', ' . $limit) : '')
		);
	}

	/**
	 * Get categories count
	 *
	 * @param null $searchParams
	 * @param string $logic
	 *
	 * @return int
	 */
	public function getCount($searchParams = null, $logic = 'AND')
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS cats_count FROM '.DB_PREFIX.'catalog c '.$this->getSearchQuery($searchParams, $logic));

		return intval($result['cats_count']);
	}

	/**
	 * @param $categoryList
	 * @param $categoryAssigned
	 * @return array
	 */
	public function getParentCategories($categoryList, $categoryAssigned)
	{
		$parentCategoryId = $categoryAssigned['parent'];

		foreach ($categoryList as $category) {
			if ($category['cid'] == $parentCategoryId)
			{
				$this->parentTree[$category['cid']] = $category['cid'];
				if (intval($category['parent']) > 0)
				{
					$this->getParentCategories($categoryList, array('cid' => $category['cid'], 'parent' => $category['parent']));
				}
				if(intval($category['parent']) == 0) break;
			}
		}
	}

	/**
	 * @param null $fields
	 * @param array $designCatalogSettings
	 * @return array
	 */
	public function getCategoriesTree($fields = null, $designCatalogSettings = array())
	{
		$tree = array();

		// check required fields
		if (!is_null($fields))
		{
			if (!in_array('cid', $fields)) $fields[] = 'cid';
			if (!in_array('parent', $fields)) $fields[] = 'parent';
			if (!in_array('name', $fields)) $fields[] = 'name';
			if (!in_array('key_name', $fields)) $fields[] = 'key_name';
			if (!in_array('is_stealth', $fields)) $fields[] = 'is_stealth';
			if (!in_array('parent', $fields)) $fields[] = 'parent';

			foreach ($fields as $key => $field) $fields[$key] = 'c.' . $field;
		}

		$categoryList = $this->db->selectAll('
			SELECT ' . (is_null($fields) ? 'c.*' : implode(',', $fields)) . ', IF(c.url_custom="", c.url_default, c.url_custom) AS url
			FROM ' . DB_PREFIX . 'catalog c WHERE c.key_name <> "gift_cert"
			ORDER BY c.priority, c.name
		');

		if (isset($designCatalogSettings['catalog_hide_empty_categories']) && $designCatalogSettings['catalog_hide_empty_categories'] == 'YES')
		{
			$baseCategoryList = $this->db->selectAll('
				SELECT c.cid, c.parent 
				FROM ' . DB_PREFIX . 'catalog c
				INNER JOIN ' . DB_PREFIX . 'products_categories pc ON (pc.cid = c.cid)
				WHERE c.key_name <> "gift_cert"			
				GROUP BY c.cid, c.parent
				ORDER BY c.cid ASC
			');

			foreach($baseCategoryList as $baseCategory)
			{
				$this->parentTree[$baseCategory['cid']] = $baseCategory['cid'];
				if (intval($baseCategory['parent']) > 0)
				{
					$this->getParentCategories($categoryList, $baseCategory);
				}
			}
		}

		// preset places in array for sorting
		$excludeEmptyCategories = array();
		foreach ($categoryList as $categoryIndex => $category)
		{
			if (count($this->parentTree))
			{
				if (in_array($category['cid'], $this->parentTree))
				{
					$tree[$category['cid']] = array_merge($category, array('children' => array(), 'level' => 1));
				}
				else
				{
					$excludeEmptyCategories[] = $categoryIndex;
				}
			}
			else
			{
				$tree[$category['cid']] = array_merge($category, array('children' => array(), 'level' => 1));
			}
		}
		foreach ($excludeEmptyCategories as $excludeEmptyCategory) unset($categoryList[$excludeEmptyCategory]);

		foreach ($categoryList as $category)
		{
			$category['category_url'] = $this->getCategoryUrl($category['url'], $category['cid']);
			//unset($category['url']);
			$category['url'] = $category['category_url'];

			$categoryId = $category['cid'];
			$parentId = $category['parent'];

			$tree[$categoryId] =
				isset($tree[$categoryId]) ?
					array_merge($tree[$categoryId], $category) :
					array_merge($category, array('children' => array(), 'level' => 1));

			if ($parentId > 0)
			{
				if (!isset($tree[$parentId])) $tree[$parentId] = array('children' => array(), 'level' => 1);
				$tree[$parentId]['children'][$categoryId] = &$tree[$categoryId];
			}
		}

		$processed = array(); // used for prevention from infinite loop

		// fix levels
		foreach ($tree as $categoryId => $category)
		{
			if (!isset($category['parent'])) continue;
			if ($category['parent'] == 0)
			{
				$this->setCategoryLevel($tree[$categoryId], 1, $processed);
			}
		}

		return $tree;
	}

	/**
	 * Build category search query
	 *
	 * @param $searchParams
	 * @param string $logic
	 *
	 * @return string
	 */
	protected function getSearchQuery($searchParams, $logic = 'AND')
	{
		$searchParams = is_array($searchParams) ? $searchParams : array();

		$logic = in_array(strtoupper(trim($logic)), array('AND', 'OR')) ? $logic : 'AND';

		$whereStr = 'WHERE key_name <> "gift_cert"';
		$where = array();
		$whereAnd = array();

		if (isset($searchParams['key_name']) && trim($searchParams['key_name']) != '')
		{
			$where[] = 'c.key_name like "%'.$this->db->escape(trim($searchParams['key_name'])).'%"';
		}

		if (isset($searchParams['name']) && trim($searchParams['name']) != '')
		{
			$where[] = 'c.name like "%'.$this->db->escape(trim($searchParams['name'])).'%"';
		}

		if (isset($searchParams['is_stealth']) && trim($searchParams['is_stealth']) != '')
		{
			$whereAnd[] = 'c.is_stealth = ' . intval($searchParams['is_stealth']);
		}

		return
			$whereStr .
			(count($where) > 0 ? (' AND (' . implode(' ' . $logic . ' ', $where) . ')') : '') .
			(count($whereAnd) > 0 ? ' AND (' . implode(' AND ', $whereAnd) . ') ' : '') . ' ';

	}

	/**
	 * @param $url
	 * @param $pid
	 * @param int $parent
	 * @param bool $force_http
	 * @return string
	 */
	protected function getCategoryUrl($url, $pid, $parent = 0, $force_http = false)
	{
		return UrlUtils::getCatalogUrl($url, $pid, $parent, $force_http);
	}

	/**
	 * @param $fields
	 * @return array|null
	 */
	public function getCachedTree($fields, $designCatalogSettings = array())
	{
		$_fields = $fields;
		sort($_fields);

		$fieldsHash = !is_null($fields) ? md5(implode(',', $_fields)) : null;

		if (is_null($this->cachedTree) || $this->cachedTreeFieldsHash != $fieldsHash)
		{
			$this->cachedTree = $this->getCategoriesTree($fields, $designCatalogSettings);
			$this->cachedTreeFieldsHash = $fieldsHash;
		}

		return $this->cachedTree;
	}

	/**
	 * @param $tree
	 * @param $parentId
	 * @param bool $includeInvisible
	 * @return array|null
	 */
	public function getNestedCategories($tree, $parentId, $includeInvisible = true)
	{
		if (isset($tree[$parentId]))
		{
			$result = array($parentId);

			if (isset($tree[$parentId]['children']) && is_array($tree[$parentId]['children']) && ($includeInvisible || $tree[$parentId]['is_visible'] == 'Yes'))
			{
				$this->getNestedSubcategories($tree[$parentId]['children'], $result, $includeInvisible);
			}

			return $result;
		}

		return null;
	}

	/**
	 * @param $children
	 * @param $result
	 * @param bool $includeInvisible
	 */
	protected function getNestedSubcategories(&$children, &$result, $includeInvisible = true)
	{
		foreach ($children as $category)
		{
			$result[] = $category['cid'];
			if (isset($category['children']) && is_array($category['children']) && ($includeInvisible || $category['is_visible'] == 'Yes'))
			{
				$this->getNestedSubcategories($category['children'], $result);
			}
		}
	}

	/**
	 * @param $tree
	 * @param $categoryId
	 * @return array|bool
	 */
	public function getParsedPath($tree, $categoryId)
	{
		if (isset($tree[$categoryId]))
		{
			$parsedPath = array();
			$tmpId = $categoryId;

			for ($i=0; $i<count($tree); $i++)
			{
				$node = $tree[$tmpId];
				$parsedPath[] = array(
					'href' => isset($node['category_url']) ? $node['category_url'] : '',
					'caption' => isset($node['name']) ? $node['name'] : ''
				);

				if (isset($node['parent']) && $node['parent'] > 0)
				{
					$tmpId = $node['parent'];
				}
				else
				{
					break;
				}
			}
			return array_reverse($parsedPath);
		}
		else
		{
			return false;
		}
	}

	/**
	 * Set category level
	 *
	 * @param array $category
	 * @param int $level
	 * @param array $processed
	 */
	protected function setCategoryLevel(&$category, $level, &$processed)
	{
		$categoryId = $category['cid'];

		if (isset($processed[$categoryId])) return;

		$category['level'] = $level;
		$processed[$categoryId] = true;

		if (count($category['children']) > 0)
		{
			foreach ($category['children'] as $key => $child)
			{
				$this->setCategoryLevel($category['children'][$key], $level + 1, $processed);
			}
		}
	}

	/**
	 * Get categories flattened list
	 *
	 * @param array|null $fields
	 * @param int|null $skipCategoryId
	 *
	 * @return array
	 */
	public function getFlattenedList($fields = null, $skipCategoryId = null)
	{
		// call in admin section of the cart - no need to have secondary parameter
		$tree = $this->getCategoriesTree($fields, array());

		$list = array();

		foreach ($tree as $category)
		{
			if (!isset($category['parent'])) continue;
			if ($category['parent'] == 0)
			{
				$this->flatten($list, $category, $skipCategoryId);
			}
		}

		return $list;
	}

	/**
	 * Flatten category tree
	 *
	 * @param array $list
	 * @param array $category
	 * @param int|null $skipCategoryId
	 */
	protected function flatten(&$list, &$category, $skipCategoryId)
	{
		if ($category['cid'] != $skipCategoryId)
		{
			$list[] = $category;

			if (count($category['children']))
			{
				foreach($category['children'] as $child)
				{
					$this->flatten($list, $child, $skipCategoryId);
				}
			}
		}
	}

	/**
	 * Get options list for selects
	 *
	 * @param null $skipCategoryId
	 * @param null|array $initialValues
	 *
	 * @return array
	 */
	public function getOptionsList($skipCategoryId = null, $initialValues = null)
	{
		$list = $this->getFlattenedList(array('parent', 'cid', 'name'), $skipCategoryId);

		$options = array();
		if ($initialValues !== null && is_array($initialValues))
		{
			$options = array_merge($options, $initialValues);
		}

		foreach ($list as $category)
		{
			if (!isset($category['level']))
			{
				$category['level'] = 1;
			}

			$options[$category['cid']] = ($category['level'] > 0 ? str_repeat('- ', ($category['level'] - 1)) : '').$category['name'];
		}

		return $options;
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode, $id = null)
	{
		$errors = array();
		if (is_array($data))
		{
			if (trim($data['name']) == '') $errors['name'] = array('Please enter category name');

			if (trim($data['key_name']) == '')
			{
				$errors['key_name'] = array('Please enter category code');
			}
			else
			{
				$db = $this->db;
				if ($db->selectOne('SELECT * FROM '.DB_PREFIX.'catalog WHERE key_name="'.$db->escape($data['key_name'] ).'"'.($mode == 'update' && !is_null($id) ? ' AND cid <> '.intval($id) : '')))
				{
					$errors['key_name'] = array('Category code must be unique within categories');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Save category
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		// get level base on parent
		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['cid']) ? $data['cid'] : null);

		$db->assignStr('parent', $data['parent']);
		$db->assignStr('is_visible', isset($data['is_visible']) && ($data['is_visible'] == 1) || $data['is_visible'] == 'Yes' ? 'Yes' : 'No');
		$db->assignStr('is_stealth', intval($data['is_stealth']));
		$db->assignStr('key_name', $data['key_name']);
		$db->assignStr('name', $data['name']);
		$db->assignStr('list_subcats', isset($data['list_subcats']) && ($data['list_subcats'] == 1) || $data['list_subcats'] == 'Yes' ? 'Yes' : 'No');
		$db->assignStr('list_subcats_images', (isset($data['list_subcats_images']) && ($data['list_subcats_images'] == 1)) || $data['list_subcats_images'] === 'Yes' ? '1' : '0');

		if (isset($data['is_doba'])) $db->assignStr('is_doba', intval($data['is_doba']));
		if (isset($data['doba_category_id'])) $db->assignStr('doba_category_id', intval($data['doba_category_id']));
		if (isset($data['doba_mapped_category_id'])) $db->assignStr('doba_mapped_category_id', intval($data['doba_mapped_category_id']));

		if (isset($data['description'])) $db->assignStr('description', $data['description']);
		if (isset($data['priority'])) $db->assignStr('priority', $data['priority']);
		if (isset($data['url_custom'])) $db->assignStr('url_custom', $data['url_custom']);

		if (isset($data['meta_title']) && !is_null($data['meta_title'])) $db->assignStr('meta_title', $data['meta_title'] == '' ? null : $data['meta_title']);
		if (isset($data['meta_description']) && !is_null($data['meta_description'])) $db->assignStr('meta_description', $data['meta_description'] == '' ? null : $data['meta_description']);

		if (isset($data['category_header'])) $db->assignStr('category_header', $data['category_header']);
		if (isset($data['description_bottom'])) $db->assignStr('description_bottom', $data['description_bottom']);
		if (isset($data['category_path'])) $db->assignStr('category_path', $data['category_path']);
		if (isset($data['exactor_euc_code'])) $db->assignStr('exactor_euc_code', $data['exactor_euc_code']);
		if (isset($data['avalara_tax_code'])) $db->assignStr('avalara_tax_code', $data['avalara_tax_code']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'catalog', 'WHERE cid='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'catalog');
		}
	}

	/**
	 * Delete categories by ids
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		/** @var DB $db */
		$db = $this->db;

		$result = $db->selectOne('SELECT count(*) AS c FROM '.DB_PREFIX.'products WHERE cid IN('.implode(',', $ids).')');
		if ($result['c'] > 0) return false;

		$result = $db->selectOne('SELECT count(*) AS c FROM '.DB_PREFIX.'catalog WHERE parent IN('.implode(',', $ids).')');
		if ($result['c'] > 0) return false;

		// delete category images
		foreach ($ids as $id)
		{
			if (!is_null($image = $this->getCategoryImage($id))) @unlink($image);
			if (!is_null($thumb = $this->getCategoryThumb($id))) @unlink($thumb);
		}

		$db->query('DELETE FROM '.DB_PREFIX.'products_categories WHERE cid IN('.implode(',', $ids).')');
		$db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes WHERE cid IN('.implode(',', $ids).')');
		$db->query('DELETE FROM '.DB_PREFIX.'catalog WHERE cid IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * Delete all categories
	 *
	 * @return bool
	 */
	public function deleteAll()
	{
		/** @var DB $db */
		$db = $this->db;

		$result = $db->selectOne('SELECT count(*) AS c FROM '.DB_PREFIX.'products WHERE product_id <> "gift_certificate"');

		if ($result['c'] > 0) return false;

		// delete category images
		$result = $db->query('SELECT cid FROM '.DB_PREFIX.'catalog');
		while ($category = $db->moveNext($result))
		{
			if (!is_null($image = $this->getCategoryImage($category['cid']))) @unlink($image);
			if (!is_null($thumb = $this->getCategoryThumb($category['cid']))) @unlink($thumb);
		}

		$db->query('DELETE FROM '.DB_PREFIX.'products_categories');
		$db->query('DELETE FROM '.DB_PREFIX.'catalog_attributes;');
		$db->query('DELETE FROM '.DB_PREFIX.'catalog WHERE key_name <> "gift_cert"');

		return true;
	}

	/**
	 * @return array
	 */
	public function getCategoriesIdsMap()
	{
		$db = $this->db;
		$category_keys = array();
		$db->query('SELECT cid, key_name FROM '.DB_PREFIX.'catalog WHERE key_name<>"" ORDER BY priority DESC, name DESC');

		while ($db->moveNext())
		{
			$ck = explode(',', $db->col["key_name"]);
			for ($i=0; $i<count($ck); $i++)
			{
				if (trim($ck[$i]) != '') $category_keys[trim($ck[$i])] = $db->col['cid'];
			}
		}

	 	return $category_keys;
	}

	/**
	 * Update categories paths
	 */
	public function updateCategoriesPath()
	{
		$tree = $this->getCategoriesTree(array('cid', 'name', 'parent'), array());
		$paths = array();

		foreach ($tree as $category)
		{
			if (isset($category['parent']) && ($category['parent'] == 0)) $this->setCategoryPath($category, $paths);
		}

		$db = $this->db;

		foreach ($paths as $cid => $path)
		{
			$db->query('UPDATE '.DB_PREFIX.'catalog SET category_path="'.$db->escape($path).'" WHERE cid='.intval($cid));
		}
	}

	/**
	 * @param $category
	 * @param $paths
	 */
	protected function setCategoryPath(&$category, &$paths)
	{
		$paths[$category['cid']] = (isset($paths[$category['parent']]) ? ($paths[$category['parent']] . '/') : '') . str_replace('/', ' ', $category['name']);
		if (count($category['children']))
		{
			foreach ($category['children'] as $child)
			{
				$this->setCategoryPath($child, $paths);
			}
		}
	}

	/**
	 * @param $categoryId
	 * @return null|string
	 */
	public function getCategoryImage($categoryId)
	{
		$id = intval($categoryId);
		$extensions = array('jpg', 'png', 'gif', 'jpeg');
		foreach ($extensions as $extension)
		{
			if (is_file($f = $this->imagesPath.'/'.$id.'.'.$extension))
			{
				return $f;
			}
		}

		return null;
	}

	/**
	 * @param $categoryId
	 * @return null|string
	 */
	public function getCategoryThumb($categoryId)
	{
		$id = intval($categoryId);
		$extensions = array('jpg', 'png', 'gif', 'jpeg');
		foreach ($extensions as $extension)
		{
			if (is_file($f = $this->imagesPath.'/thumbs/'.$id.'.'.$extension))
			{
				return $f;
			}
		}

		return null;
	}

	/**
	 * @param $cid
	 * @return array|bool
	 */
	public function getCategoryDetails($cid)
	{
		return $this->db->selectOne('SELECT url_default, key_name, name FROM '.DB_PREFIX.'catalog WHERE cid = '. intval($cid));
	}

	/**
	 * @param $url
	 * @param $cid
	 * @return int
	 */
	public function hasUrlDuplicate($url, $cid)
	{
		$escapedUrl = $this->db->escape($url);
		$db = $this->db;

		$result = $db->query('
			SELECT url_custom
			FROM '.DB_PREFIX.'catalog
			WHERE (url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'")
				AND cid != ' . intval($cid) . '

			UNION

			SELECT url_custom FROM '.DB_PREFIX.'manufacturers
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'products
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"

			UNION

			SELECT url_custom
			FROM '.DB_PREFIX.'pages
			WHERE url_custom = "'. $escapedUrl .'" OR url_default = "'. $escapedUrl .'"
		');

		return $db->numRows($result);
	}

	/**
	 * @param $fromCid
	 * @param $toCid
	 */
	public function moveProducts($fromCid, $toCid)
	{
		$from = intval($fromCid);
		$to = intval($toCid);

		if ($from == $to) return;

		$db = $this->db;

		$pids = array();
		$result = $db->query('SELECT pid FROM ' . DB_PREFIX . 'products WHERE cid=' . $from);
		while ($product = $db->moveNext($result))
		{
			$pids[] = $product['pid'];
		}

		$db->query('DELETE FROM ' . DB_PREFIX . 'products_categories WHERE cid IN (' . $from . ', ' . $to . ')');
		$db->query('UPDATE ' . DB_PREFIX .'products SET cid=' . $to . ' WHERE cid=' . $from);

		foreach ($pids as $pid)
		{
			$db->query('INSERT INTO ' . DB_PREFIX . 'products_categories (pid, cid, is_primary) VALUES (' . $pid . ', ' . $to . ', 1)');
		}
	}
}
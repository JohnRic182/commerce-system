<?php

/**
 * Class DataAccess_ShippingMethodRepository
 */
class DataAccess_ShippingMethodRepository extends DataAccess_BaseRepository
{
	/**
	 * Get shipping methods count
	 * @return int
	 */
	public function getShippingMethodsCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'shipping_selected');
		return intval($result['c']);
	}

	/**
	 * @param $shippingOriginCountryId
	 * @return array
	 */
	public function getShippingMethods($shippingOriginCountryId)
	{
		$db = $this->db;
		$result = $db->query('SELECT * FROM '.DB_PREFIX.'shipping_selected ORDER BY carrier_name, method_name');

		$methods = array(
			'custom-domestic' => array(),
			'custom-international' => array(),
			'realtime-domestic' => array(),
			'realtime-international' => array()
		);

		while ($method = $db->moveNext($result))
		{
			$methodType = $method['carrier_id'] == 'custom' ? 'custom-' : 'realtime-';
			$countries = explode(',', $method['country']);
			$countriesExclude = explode(',', $method['country_exclude']);
			$methodType .= in_array($shippingOriginCountryId, $countries) && !in_array($shippingOriginCountryId, $countriesExclude) ? 'domestic' : 'international';

			$c = explode('_', $method['carrier_id']);
			$method['carrier_code'] = $c[0] == 'canada' ? 'canada-post' : $c[0];

			$methods[$methodType][] = $method;
		}

		if (count($methods['custom-domestic']) == 0) unset($methods['custom-domestic']);
		if (count($methods['realtime-domestic']) == 0) unset($methods['realtime-domestic']);
		if (count($methods['custom-international']) == 0) unset($methods['custom-international']);
		if (count($methods['realtime-international']) == 0) unset($methods['realtime-international']);

		return count($methods) > 0 ? $methods : null;
	}

	/**
	 * @param $carrier
	 */
	public function enableShippingMethodsByCarrier($carrier)
	{
		if (in_array($carrier, array('ups'))) $carrier = 'ups';
		else if (in_array($carrier, array('usps', 'usps_domestic', 'usps_international'))) $carrier = 'usps';
		else if (in_array($carrier, array('fedex', 'fedex_api_express_us', 'fedex_api_freight_us', 'fedex_api_ground_us', 'fedex_api_intr', 'fedex_api_intr_freight'))) $carrier = 'fedex';
		else if (in_array($carrier, array('canada', 'canadapost', 'canada_post', 'canada-post'))) $carrier = 'canada';
		else $carrier = false;

		if ($carrier)
		{
			$this->db->query('UPDATE '.DB_PREFIX.'shipping_selected SET hidden="No" WHERE carrier_id LIKE "'.$this->db->escape($carrier).'%"');
		}
	}

	/**
	 * @param $shippingOriginCountryId
	 * @param $flatDomestic
	 * @param $flatInternational
	 * @param $realTimeDomestic
	 * @param $realTimeInternational
	 * @param $realTimeCarriers
	 * @param $doba
	 */
	public function initShippingMethods($shippingOriginCountryId, $flatDomestic, $flatInternational, $realTimeDomestic,
		$realTimeInternational, $realTimeCarriers, $doba)
	{
		if ($flatDomestic)
		{
			$flatDomesticMethod = array(
				'carrier_id' => 'custom',
				'carrier_name' => 'Flat rate domestic',
				'method_id' => 'flat',
				'method_calc_mode' => 'before_discount',
				'method_name' => 'Flat (per item)',
				'country' => $shippingOriginCountryId,
				'country_exclude' => '0',
				'state' => '0',
				'weight_min' => '0.01',
				'weight_max' => '100.00'
			);

			$this->persist($flatDomesticMethod);
		}

		if ($flatInternational)
		{
			$flatInternational = array(
				'carrier_id' => 'custom',
				'carrier_name' => 'Flat rate international',
				'method_id' => 'flat',
				'method_calc_mode' => 'before_discount',
				'method_name' => 'Flat (per item)',
				'country' => '0',
				'country_exclude' => $shippingOriginCountryId,
				'state' => '0',
				'weight_min' => '0.01',
				'weight_max' => '100.00'
			);

			$this->persist($flatInternational);
		}

		if ($realTimeDomestic)
		{
			if (isset($realTimeCarriers['ups'])) $this->persist($this->getRealTimeMethod('ups', '03')); // UPS Ground
			if (isset($realTimeCarriers['usps'])) $this->persist($this->getRealTimeMethod('usps_domestic', 'Priority')); // USPS Priority
			if (isset($realTimeCarriers['fedex'])) $this->persist($this->getRealTimeMethod('fedex_api_express_us', 'FEDEX_GROUND')); // FedEx Ground
			if (isset($realTimeCarriers['canada-post'])) $this->persist($this->getRealTimeMethod('canada_post', '1010')); // Canada Post Regular
		}

		if ($realTimeInternational)
		{
			if (isset($realTimeCarriers['ups'])) $this->persist($this->getRealTimeMethod('ups', '07')); // UPS Worldwide Express
			if (isset($realTimeCarriers['usps'])) $this->persist($this->getRealTimeMethod('usps_international', 'Package=1')); // USPS Express Mail International (EMS)
			if (isset($realTimeCarriers['fedex'])) $this->persist($this->getRealTimeMethod('fedex_api_intr', 'INTERNATIONAL_ECONOMY')); // FedEx International Economy
			if (isset($realTimeCarriers['canada-post'])) $this->persist($this->getRealTimeMethod('canada_post', '3015')); // Small Packet Air International
		}

		if ($doba)
		{
			$dobaMethod = array(
				'carrier_id' => 'doba',
				'carrier_name' => 'Shipping',
				'method_id' => 'doba',
				'method_calc_mode' => 'before_discount',
				'method_name' => 'Flat rate',
				'country' => $shippingOriginCountryId,
				'country_exclude' => '0',
				'state' => '0',
				'weight_min' => '0.01',
				'weight_max' => '100.00'
			);

			$this->persist($dobaMethod);
		}
	}

	/**
	 * @param $shippingMethodId
	 * @return array|bool
	 */
	public function getRealTimeMethodById($shippingMethodId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'shipping_default WHERE sdid='.intval($shippingMethodId));
	}

	/**
	 * @param $carrierId
	 * @param $methodId
	 * @return array|bool
	 */
	public function getRealTimeMethod($carrierId, $methodId)
	{
		$db = $this->db;
		return $db->selectOne('SELECT * FROM '.DB_PREFIX.'shipping_default WHERE carrier_id="'.$db->escape($carrierId).'" AND method_id="'.$db->escape($methodId).'"');
	}

	/**
	 * @param $carrierId
	 * @param $methodId
	 * @return array|bool
	 */
	public function getActiveMethodByCarrierMethod($carrierId, $methodId)
	{
		$db = $this->db;
		return $db->selectOne('SELECT * FROM '.DB_PREFIX.'shipping_selected WHERE carrier_id="'.$db->escape($carrierId).'" AND method_id="'.$db->escape($methodId).'"');
	}

	/**
	 * Get shipping method
	 *
	 * @param int $shippingMethodId
	 *
	 * @return array|bool
	 */
	public function getShippingMethodById($shippingMethodId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'shipping_selected WHERE ssid='.intval($shippingMethodId));
	}

	/**
	 * @param $mode
	 * @return array
	 */
	public function getDefaults($mode)
	{
		return array(
			'carrier_id' => 'custom',
			'carrier_name' => '',
			'method_id' => 'flat',
			'method_calc_code' => 'before_discount',
			'method_name' => '',
			'priority' => '0',
			'country' => '0',
			'country_exclude' => '',
			'state' => '',
			'weight_min' => '',
			'weight_max' => '',
			'fee' => '',
			'fee_type' => '',
			'exclude' => '',
			'hidden' => '',
			'notes' => ''
		);
	}

	/**
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['ssid']) ? $data['ssid'] : null);

		if (isset($data['carrier_id'])) $db->assignStr('carrier_id', $data['carrier_id']);
		if (isset($data['carrier_name'])) $db->assignStr('carrier_name', $data['carrier_name']);
		if (isset($data['method_id'])) $db->assignStr('method_id', $data['method_id']);
		if (isset($data['method_calc_mode'])) $db->assignStr('method_calc_mode', $data['method_calc_mode']);
		if (isset($data['method_name'])) $db->assignStr('method_name', $data['method_name']);
		if (isset($data['priority'])) $db->assignStr('priority', $data['priority']);
		if (isset($data['country'])) $db->assignStr('country', $data['country']);
		if (isset($data['country_exclude'])) $db->assignStr('country_exclude', $data['country_exclude']);
		if (isset($data['state'])) $db->assignStr('state', $data['state']);
		if (isset($data['weight_min'])) $db->assignStr('weight_min', $data['weight_min']);
		if (isset($data['weight_max'])) $db->assignStr('weight_max', $data['weight_max']);
		if (isset($data['fee'])) $db->assignStr('fee', $data['fee']);
		if (isset($data['fee_type'])) $db->assignStr('fee_type', $data['fee_type']);
		if (isset($data['exclude'])) $db->assignStr('exclude', $data['exclude']);
		if (isset($data['hidden'])) $db->assignStr('hidden', $data['hidden']);
		if (isset($data['notes'])) $db->assignStr('notes', $data['notes']);

		if (isset($data['id']) && !is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'shipping_selected',  'WHERE ssid='.intval($data['id']));
			return intval($data['id']);
		}
		else
		{
			$res = $db->insert(DB_PREFIX.'shipping_selected');
			return $res;
		}
	}

	/**
	 * @param integer $id
	 */
	public function delete($id)
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'shipping_selected WHERE ssid='.intval($id));
	}
}
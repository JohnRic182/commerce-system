<?php

/**
 * Class DataAccess_OrderShipmentRepository
 */
class DataAccess_OrderShipmentRepository extends DataAccess_BaseRepository
{
	protected $settings;

	public function __construct(DB $db, DataAccess_SettingsRepository $settings)
	{
		parent::__construct($db);

		$this->settings = $settings;
	}

	/**
	 * @param Model_LineItem $lineItem
	 * @return int
	 */
	public function assignShipment(Model_LineItem $lineItem)
	{
		$db = $this->db;
		// TODO: check condition to filter same day delivery. Add time check
		// TODO: create separate shipments for Doba items, Digital downloads, Virtual products, Product level shipping, Bongo?

		if ($lineItem->getIsDoba() === true || $lineItem->getIsDoba() == 'Yes')
		{
			$shipmentType = 'doba';
		}
		else if ($lineItem->getProductType() == Model_Product::DIGITAL)
		{
			$shipmentType = 'digital';
		}
		else if ($lineItem->getProductType() == Model_Product::VIRTUAL)
		{
			$shipmentType = 'virtual';
		}
		else if ($lineItem->getSameDayDelivery() && $this->settings->get('PikflyEnabled') == 'Yes')
		{
			$shipmentType = 'local';
		}
		else if ($lineItem->getProductLevelShipping())
		{
			$shipmentType = 'product';
		}
		else
		{
			$shipmentType = '';
		}

		$oid = intval($lineItem->getOrderId());
		$pid = intval($lineItem->getPid());

		$shipment = $db->selectOne('
			SELECT os.*
			FROM '.DB_PREFIX.'orders_shipments os
			'.($shipmentType == 'product' ? 'INNER JOIN '.DB_PREFIX.'orders_content oc ON oc.shipment_id=os.id AND oc.oid='.$oid.' AND oc.pid='.$pid : '').'
			WHERE os.oid='.$oid.' AND os.shipping_type="'.$db->escape($shipmentType).'"');

		if (!$shipment)
		{
			$db->reset();
			$db->assign('oid', $lineItem->getOrderId());
			$db->assignStr('shipping_type', $shipmentType);
			$db->assignStr('shipping_ssid', '');
			$db->assignStr('shipping_carrier_id', '');
			$db->assignStr('shipping_method_id', '');
			$db->assignStr('shipping_cm_name', '');
			$db->assignStr('shipping_amount', '0.00');
			$db->assign('updated_at', 'NOW()');
			$db->insert(DB_PREFIX.'orders_shipments');
			$shipmentId = $db->insertId();
		}
		else
		{
			$shipmentId = $shipment['id'];
		}

		$lineItem->setShipmentId($shipmentId);

		return $shipmentId;
	}

	/**
	 * @param $orderDbId
	 * @return mixed
	 */
	public function getShipmentsByOrderId($orderDbId)
	{
		$results = $this->db->selectAll('SELECT os.* FROM ' . DB_PREFIX . 'orders_shipments os WHERE os.oid=' . intval($orderDbId) . ' ORDER BY id');

		$shipments = array();

		foreach ($results as $shipment) $shipments[$shipment['id']] = $shipment;

		return $shipments;
	}

	/**
	 * @param $orderDbId
	 */
	public function cleanupShipments($orderDbId)
	{
		$this->db->query('
			DELETE shipment FROM '.DB_PREFIX.'orders_shipments AS shipment
			LEFT JOIN '.DB_PREFIX.'orders_content AS item ON shipment.id=item.shipment_id
			WHERE shipment.oid='.intval($orderDbId).' AND item.ocid IS NULL
		');
	}

	/**
	 * @param $orderDbId
	 */
	public function updateStatus($orderDbId)
	{
		$db = $this->db;

		$orderItems = $db->selectAll('SELECT shipment_id, fulfillment_status FROM '.DB_PREFIX.'orders_content WHERE oid='.intval($orderDbId));
		$statuses = array();

		foreach ($orderItems as $orderItem)
		{
			$shipmentId = $orderItem['shipment_id'];
			if (!isset($statuses[$shipmentId])) $statuses[$shipmentId] = array();
			$statuses[$shipmentId][$orderItem['fulfillment_status']] = true;
		}

		foreach ($statuses as $shipmentId => $fulfillmentStatuses)
		{
			if (isset($fulfillmentStatuses['pending']) && !isset($fulfillmentStatuses['partial']) && !isset($fulfillmentStatuses['completed']))
			{
				$shipmentStatus = 'pending';
			}
			else if (isset($fulfillmentStatuses['completed']) && !isset($fulfillmentStatuses['pending']) && !isset($fulfillmentStatuses['partial']))
			{
				$shipmentStatus = 'completed';
			}
			else
			{
				$shipmentStatus = 'partial';
			}

			$db->query('UPDATE '.DB_PREFIX.'orders_shipments SET fulfillment_status="'.$db->escape($shipmentStatus).'" WHERE id='.intval($shipmentId).' AND oid='.intval($orderDbId));
		}
	}
}
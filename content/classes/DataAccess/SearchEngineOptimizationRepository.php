<?php

/**
 * Class DataAccess_SearchEngineOptimizationRepository
 */
class DataAccess_SearchEngineOptimizationRepository extends DataAccess_BaseRepository
{
	protected $settings = array();

	public function __construct(DB $db, $settings)
	{
		if ( $settings )
		{
			$this->settings = $settings;
		}
		parent::__construct($db);
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 * @param null $id
	 *
	 * @return array|bool
	 */
	public function getValidationSettingsErrors($data, $mode, $id = null)
	{
		return false;
	}

	/**
	 * Get SEO Settings
	 *
	 * @return array
	 */
	public function getAdvancedSettingsData()
	{
		return $this->settings;
	}

	/**
	 * Get Content of robots.txt
	 * @return string
	 */
	public function getRobots()
	{
		$f = @fopen('robots.txt', 'r');
		if ($f)
		{
			$robots = '';
			while(($s = fgets($f, 1024)) != false) $robots.=$s;
			fclose($f);
		}
		return $robots;
	}

	/**
	 * Persist SEO Settings
	 *
	 * @param $data
	 */
	public function persistSearchOptimizationSettings($data)
	{
		if ( isset($data['searchSettings']) )
		{
			$searchSettings = $data['searchSettings'];

			$searchSettings['USE_MOD_REWRITE'] = isset($searchSettings['USE_MOD_REWRITE']) ? $searchSettings['USE_MOD_REWRITE']: 'NO';
			$searchSettings['SearchAutoGenerateLowercase'] = isset($searchSettings['SearchAutoGenerateLowercase']) ? $searchSettings['SearchAutoGenerateLowercase']: 'NO';
			$searchSettings['SearchAutoGenerate'] = isset($searchSettings['SearchAutoGenerate']) ? $searchSettings['SearchAutoGenerate']: 'NO';
			$searchSettings['seo_www_preference'] = isset($searchSettings['seo_www_preference']) ? $searchSettings['seo_www_preference']: 'default';

			if (isset($searchSettings['robots'])) 
			{
				!@file_put_contents('robots.txt', $searchSettings['robots']);
			}

			if (is_array($searchSettings) && count($searchSettings) > 0)
			{
				$settingsRepository = new DataAccess_SettingsRepository($this->db, $this->settings);
				$settingsRepository->save($searchSettings);
				return true;
			}
		}
		return false;
	}
}
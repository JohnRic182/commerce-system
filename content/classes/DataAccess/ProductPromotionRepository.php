<?php

/**
 * Class DataAccess_ProductPromotionRepository
 */
class DataAccess_ProductPromotionRepository extends DataAccess_BaseRepository
{
	/**
	 * Get defaults
	 *
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'id' => null,
			'product_gift_id' => null,
			'pid' => '',
			'gift_pid' => '',
			'gift_quantity' => '',
			'gift_shipping_free' => 'No',
			'gift_max_quantity' => ''
		);
	}

	/**
	 * Get variants by product
	 *
	 * @param $productId
	 *
	 * @return mixed
	 */
	public function getListByProductId($productId)
	{
		return $this->db->selectAll('
			SELECT pg.*, p.product_id, p.title
			FROM '.DB_PREFIX.'products_gifts AS pg
				INNER JOIN '.DB_PREFIX.'products AS p ON pg.gift_pid = p.pid
			WHERE pg.pid='.intval($productId).' ORDER BY pg.gift_pid
		');
	}

	/**
	 * Get product promotion by id
	 *
	 * @param $promotionId
	 *
	 * @return array|bool
	 */
	public function getById($promotionId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products_gifts WHERE product_gift_id='.intval($promotionId));
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode)
	{
		$errors = array();
		if (is_array($data))
		{
			if (!isset($data['pid']) || intval($data['pid']) == 0)
			{
				$errors['pid'] = array('Cannot find product id');
			}

			if (!isset($data['gift_quantity']) || trim($data['gift_quantity']) == '')
			{
				$errors['gift_quantity'] = array('Please enter free promo product quantity');
			}

			if (!isset($data['gift_max_quantity']) || trim($data['gift_max_quantity']) == '')
			{
				$errors['gift_max_quantity'] = array('Please enter free product quantity limit');
			}

			if (!isset($data['gift_pid']) || trim($data['gift_pid']) == '')
			{
				$errors['gift_pid'] = array('Cannot find gift product id');
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Save changes
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['product_gift_id']) ? $data['product_gift_id'] : null);

		$db->reset();

		$db->assignStr('pid', $data['pid']);
		$db->assignStr('gift_pid', intval($data['gift_pid']));
		$db->assignStr('gift_quantity', intval($data['gift_quantity']));
		$db->assignStr('gift_shipping_free', $data['gift_shipping_free']);
		$db->assignStr('gift_max_quantity', intval($data['gift_max_quantity']));

		if (!is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products_gifts', 'WHERE product_gift_id='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'products_gifts');
		}
	}

	/**
	 * Delete product promotions
	 *
	 * @param $ids
	 *
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		$this->db->query('DELETE FROM '.DB_PREFIX.'products_gifts WHERE product_gift_id IN('.implode(',', $ids).')');

		return true;
	}
}
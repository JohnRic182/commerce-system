<?php
/**
 * 
 */
class DataAccess_DiscountRepository implements DataAccess_DiscountRepositoryInterface
{
	protected $db;

	public function __construct($db = null)
	{
		if (is_null($db))
		{
			global $db;
		}

		$this->db = $db;
	}

	public function getActiveDiscount($subtotal)
	{
		$data = $this->getData("SELECT * FROM ".DB_PREFIX."discounts WHERE active='Yes' AND ".$subtotal.">=min AND ".$subtotal."<=max ORDER BY did");

		if (isset($data[0]))
		{
			return $data[0];
		}

		return false;
	}

	protected function getData($query)
	{
		$result = $this->db->query($query);
		
		$data = array();
		
		while ($this->db->moveNext($result))
		{
			$data[] = $this->db->col;
		}

		return $data;
	}
}
<?php
/**
 * Class DataAccess_BaseRepository
 */
class DataAccess_BaseRepository
{
	/** @var DB $db */
	protected $db;

	/**
	 * Class constructor
	 *
	 * @param DB $db
	 */
	public function __construct(DB $db)
	{
		$this->db = $db;
	}

	/**
	 * @param $data
	 * @param $params
	 * @return bool
	 */
	public function persist($data, $params = null)
	{
		return false;
	}
}
<?php

/**
 * Class DataAccess_QuickStartGuideRepository
 */
class DataAccess_QuickStartGuideRepository extends DataAccess_SettingsRepository
{
	public $stepOneFormDataKeys = array(
		'CompanyName', 'CompanyAddressLine1', 'CompanyAddressLine2',
		'CompanyCountry', 'CompanyState', 'CompanyProvince',
		'CompanyCity', 'CompanyZip', 'CompanyPhone',
		'CompanyFax', 'CompanyEmail', 'CompanyIndustry',
		'CompanyRevenue', 'CompanyEmployees',
	);

	public $stepTwoFormDataKeys = array(
		'facebookPageAccount', 'twitterAccount', 'pinterestUsername',
		'instagramAccount', 'youtubePageURL', 'snapChatAccount',
		'googlePlusAccount', 'bloggerAccount', 'facebookLikeButtonProduct',
		'twitterTweetButtonProduct', 'googlePlusButtonProduct', 'pinItButtonProduct',
		'instagramButtonProduct', 'youtubeButtonProduct', 'snapChatButtonProduct',
		'bloggerButtonProduct',
	);

	public $originAddressDataKeys = array(
		'ShippingOriginName', 'ShippingOriginAddressLine1', 'ShippingOriginAddressLine2',
		'ShippingOriginCity', 'ShippingOriginCountry', 'ShippingOriginState',
		'ShippingOriginProvince', 'ShippingOriginZip', 'ShippingOriginPhone'
	);

	public $upsSettingsKeys = array(
		'ShippingUPSAccessKey', 'ShippingUPSUserID', 'ShippingUPSUserPassword',
		'ShippingUPSRateChart', 'ShippingUPSContainer', 'ShippingUPSUrl', 'ShippingUPSTrackingUrl'
	);

	public $uspsSettingsKeys = array(
		'ShippingUSPSUserID', 'ShippingUSPSUrl', 'ShippingUSPSTrackingUrl',
		'ShippingUSPSLabelingUrl', 'ShippingUSPSPackageSize', 'ShippingUSPSPackageWidth',
		'ShippingUSPSPackageLength', 'ShippingUSPSPackageHeight'
	);

	public $fedExSettingsKeys = array(
		'ShippingFedExAccountNumber', 'ShippingFedExBillingAccountNumber', 'ShippingFedExFreightAccountNumber',
		'ShippingFedExApiKey', 'ShippingFedExApiPassword', 'ShippingFedExMeter',
		'ShippingFedExRateRule', 'ShippingFedExRateRequestType', 'ShippingFedExFreightClass',
		'ShippingFedExFreightPackaging', 'ShippingFedExFreightPackagingWeight','ShippingFedExDropOffType'
	);

	public $canadaPostSettingsKeys = array(
		'ShippingCPMerchantID', 'ShippingCPUrlRating', 'ShippingCPDeliveryDate',
		'ShippingCPItemsValue', 'ShippingCPTAT', 'ShippingUSPSPackageWidth',
		'ShippingUSPSPackageLength', 'ShippingUSPSPackageHeight'
	);


	public $vipParcelSettingsKeys = array(
		'ShippingVIPparcelauthToken', 'ShippingVIPparcelUrl', 'ShippingVIPparcelTestmode', 'ShippingVIPparcelService'
	);

	/**
	 * Validation for General Information data
	 * @param $data
	 * @return array|bool
	 */
	public function getGeneralInformationValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			$companyName = trim($data['companyName']);
			if (empty($companyName))
			{
				$errors['CompanyName'] = 'Please enter a Company Name';
			}

			$companyAddressLine1 = trim($data['companyAddressLine1']);
			if (empty($companyAddressLine1))
			{
				$errors['CompanyAddressLine1'] = 'Please enter a Street Address';
			}

			$companyCountry = trim($data['companyCountry']);
			if (empty($companyCountry))
			{
				$errors['CompanyCountry'] = 'Please select a Country';
			}
		}
		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Validation for Contact Information data
	 * @param $data
	 * @return array|bool
	 */
	public function getContactInformationValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			$companyPhone = trim($data['companyPhone']);
			if (empty($companyPhone))
			{
				$errors['CompanyPhone'] = 'Please enter a Phone Number';
			}
		}
		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Validation for Industry Specifics data
	 * @param $data
	 * @return array|bool
	 */
	public function getIndustrySpecificsValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			$companyIndustry = trim($data['companyIndustry']);
			if (empty($companyIndustry))
			{
				$errors['CompanyIndustry'] = 'Please enter Industry';
			}

			$companyRevenue = trim($data['companyRevenue']);
			if (empty($companyRevenue))
			{
				$errors['CompanyRevenue'] = 'Please enter Revenue';
			}

			$companyEmployees = trim($data['companyEmployees']);
			if (empty($companyEmployees))
			{
				$errors['CompanyEmployees'] = 'Please enter Employees';
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Validation for Social Media Accounts data
	 * @param $data
	 * @return array|bool
	 */
	public function getSocialMediaAccountsValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			$facebookPageAccount = trim($data['facebookPageAccount']);
			if (!empty($facebookPageAccount) && !validate($facebookPageAccount, VALIDATE_URL))
			{
				$errors['facebookPageAccount'] = 'Facebook Page URL must be a valid URL';
			}

			$twitterAccount = trim($data['twitterAccount']);
			if (!empty($twitterAccount) && !validate($twitterAccount, VALIDATE_URL))
			{
				$errors['twitterAccount'] = 'Twitter Account URL must be a valid URL';
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Validation for Sharing Settings data
	 * @param $data
	 * @return array|bool
	 */
	public function getSharingSettingsValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{

		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Validation for Site Information data
	 * @param $data
	 * @return array|bool
	 */
	public function getSiteInformationValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{
			$siteTitle = trim($data['searchSettings']['SearchTitle']);
			if (empty($siteTitle))
			{
				$errors['searchSettings-SearchTitle'] = 'Please enter a Site Title';
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Validation for SEO Settings data
	 * @param $data
	 * @return array|bool
	 */
	public function getSeoSettingsValidationErrors($data)
	{
		$errors = array();
		if (is_array($data))
		{

		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Validation for Address data
	 * @param $data
	 * @param $db
	 * @return array
	 */
	public function validateAddress($data, $db)
	{
		$sql = 'SELECT * FROM ' . DB_PREFIX . 'countries WHERE iso_a2 = "' . $data['companyCountry'] . '"';
		$res = $db->selectOne($sql);

		$errors = array();
		if ($res)
		{
			if ($res['zip_required'])
			{
				$data['companyZip'] = trim($data['companyZip']);
				if (empty($data['companyZip']))
				{
					$errors['CompanyZip'] = 'Please enter a Zip / Postal Code';
				}
				else
				{
					if ($res['iso_a3'] == 'USA' && !validate($data['companyZip'], VALIDATE_ZIP_USA))
					{
						$errors['CompanyZip'] = 'Please enter a valid Zip / Postal Code';
					}
					elseif ($res['iso_a3'] == 'CAN' && !validate($data['companyZip'], VALIDATE_ZIP_CANADA))
					{
						$errors['CompanyZip'] = 'Please enter a valid Zip / Postal Code';
					}
					elseif ($res['iso_a3'] == 'GBP' && !validate($data['companyZip'], VALIDATE_ZIP_UK))
					{
						$errors['CompanyZip'] = 'Please enter a valid Zip / Postal Code';
					}
					elseif ($res['zip_required'] && !validate($data['companyZip'], VALIDATE_ZIP))
					{
						$errors['CompanyZip'] = 'Please enter a valid Zip / Postal Code';
					}
				}
			}

			if ($res['has_provinces'])
			{
				if (isset($data['companyProvince']))
				{
					$data['companyProvince'] = trim($data['companyProvince']);
					if (empty($data['companyProvince']))
					{
						$errors['CompanyProvince'] = 'Please enter a Province';
					}
				}

				if (isset($data['companyState']))
				{
					$data['companyState'] = trim($data['companyState']);
					if (strlen($data['companyState']) == 0 || empty($data['companyState']))
					{
						$errors['CompanyState'] = 'Please select a State';
					}
				}
			}
		}

		return $errors;
	}
}

<?php
/**
 * Class DataAccess_CountryGroupsRepository
 */
class DataAccess_CountryGroupsRepository extends DataAccess_Repository
{
	/**
	 * Get country groups
	 *
	 * @return type
	 */
	public function getAllCountryGroups()
	{
		return $this->getData('SELECT * FROM '.DB_PREFIX.'countries_groups ORDER BY name', false);
	}

	/**
	 * Get active country groups
	 *
	 * @return type
	 */
	public function getActiveCountryGroups()
	{
		return $this->getData('SELECT * FROM '.DB_PREFIX.'countries_groups WHERE is_available=1 ORDER BY name', false);
	}

	/**
	 * Select country group by id
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return self::$db->selectOne('SELECT * FROM '.DB_PREFIX.'countries_groups WHERE country_group_id='.intval($id));
	}

	public function checkDuplicateName($id, $name)
	{
		$result = self::$db->selectOne('SELECT 1 FROM '.DB_PREFIX.'countries_groups WHERE name = \''.self::$db->escape($name).'\' AND country_group_id <> '.intval($id));

		return $result ? true : false;
	}

	/**
	 * Add a new country group
	 *
	 * @param array $data
	 *
	 * @return integer
	 */
	public function addCountryGroup($data)
	{
		$db = self::$db;
		$db->reset();
		$db->assignStr('name', $data['name']);
		$db->assignStr('is_available', $data['is_available']);
		$db->assignStr('countries', implode(',', $data['countries']));
		return $db->insert(DB_PREFIX.'countries_groups');
	}

	/**
	 * Update a new country group
	 *
	 * @param integer $id
	 * @param array $data
	 */
	public function updateCountryGroup($id, $data)
	{
		$db = self::$db;
		$db->reset();
		$db->assignStr('name', $data['name']);
		$db->assignStr('is_available', $data['is_available']);
		$db->assignStr('countries', implode(',', $data['countries']));
		$db->update(DB_PREFIX.'countries_groups', 'WHERE country_group_id='.intval($id));
	}

	/**
	 * Add a new country group
	 *
	 * @param integer $id
	 */
	public function deleteCountryGroup($id)
	{
		self::$db->query('DELETE FROM '.DB_PREFIX.'countries_groups WHERE country_group_id='.intval($id));
	}
}
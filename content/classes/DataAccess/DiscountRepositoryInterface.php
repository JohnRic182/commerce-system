<?php

interface DataAccess_DiscountRepositoryInterface
{
	public function getActiveDiscount($subtotal);
}
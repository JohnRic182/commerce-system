<?php

/**
 * Class DataAccess_ProductAttributeRepository
 */
class DataAccess_ProductAttributeRepository extends DataAccess_BaseRepository
{
	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'id' => null,
			'paid' => null,
			'pid' => 0,
			'name' => '',
			'caption' => '',
			'priority' => '5',
			'is_active' => 'No',
			'attribute_type' => 'select',
			'text_length' => '20',
			'track_inventory' => '0',
			'attribute_required' => 'No',
			'options' => ''
		);
	}

	/**
	 * Get attributes by product
	 *
	 * @param $productId
	 * @return mixed
	 */
	public function getListByProductId($productId)
	{
		$attributes = $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'products_attributes WHERE pid='.intval($productId).' ORDER BY priority, name, is_modifier');
		if ($attributes && is_array($attributes))
		{
			foreach ($attributes as $key => $attribute)
			{
				if (in_array($attribute['attribute_type'], array('select', 'radio')))
				{
					$parsed = array();
					parseOptions($attribute['options'], 0, 0, $parsed, true);
					$attributes[$key]['options_parsed'] = $parsed;
				}
				else
				{
					$attributes[$key]['options_parsed'] = null;
				}
			}
		}
		return $attributes;
	}

	public function getRequiredByProductId($productId)
	{
		$requiredAttributes = array();

		$attributes = $this->db->selectAll('SELECT * FROM ' . DB_PREFIX . 'products_attributes WHERE pid =' . intval($productId) . ' AND attribute_type = "select"');
		if ($attributes && is_array($attributes))
		{
			foreach ($attributes as $productAttribute)
			{
				$requiredAttributes[$productAttribute['paid']] = array(
					'name' => $productAttribute['name'],
					'caption' => $productAttribute['caption'],
					'attribute_type' => $productAttribute['attribute_type']
				);
			}
		}
		return $requiredAttributes;
	}

	public function getByProductIds(array $pids)
	{
		if (count($pids) == 0) return array();

		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'products_attributes WHERE pid IN ('.implode(',', $pids).') ORDER BY pid, priority, name, is_modifier');
	}

	/**
	 * Get attribute by id
	 *
	 * @param $attributeId
	 * @return array|bool
	 */
	public function getById($attributeId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products_attributes WHERE paid='.intval($attributeId));
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode)
	{
		$errors = array();
		if (is_array($data))
		{
			if (!isset($data['pid']) || intval($data['pid']) == 0)
			{
				$errors['pid'] = array('Cannot find parent product id');
			}

			if (!isset($data['name']) || trim($data['name']) == '')
			{
				$errors['name'] = array('Please enter attribute name');
			}

			if (!isset($data['caption']) || trim($data['caption']) == '')
			{
				$errors['caption'] = array('Please enter attribute caption');
			}

			if (!isset($data['attribute_type']) || trim($data['attribute_type']) == '')
			{
				$errors['attribute_type'] = array('Please select attribute type');
			}
			else
			{
				if (in_array($data['attribute_type'], array('select', 'radio')) && (!isset($data['options']) || trim($data['options']) == ''))
				{
					$errors['options'] = array('Please enter attribute options');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * Persist
	 *
	 * @param $data
	 * @param null $params
	 *
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['paid']) ? $data['paid'] : null);

		$db->reset();
		$db->assignStr('attribute_type', $data['attribute_type']);
		$db->assignStr('pid', intval($data['pid']));
		if (isset($data['gaid'])) $db->assignStr('gaid', intval($data['gaid']));
		$db->assignStr('is_modifier', $this->checkIsModifier($data['attribute_type'], $data['options']) ? 'Yes' : 'No');
		$db->assignStr('is_active', $data['is_active']);
		$db->assignStr('priority', intval($data['priority']));
		$db->assignStr('track_inventory', (($data['attribute_type'] == 'select') || ($data['attribute_type'] == 'radio')) ? intval($data['track_inventory']) : 0);
		$db->assignStr('name', trim($data['name']));
		$db->assignStr('caption', trim($data['caption']));
		$db->assignStr('text_length', intval($data['text_length']));
		$db->assignStr('options', preg_replace_callback('/(?:\d+)(\.[0-9]+)|(\.[0-9]+)/', array($this, 'formatNumberModifier'), trim($data['options'])));
		$db->assignStr('attribute_required', trim($data['attribute_required']));

		if ($data['id'] !== null && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products_attributes',  'WHERE paid='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'products_attributes');
		}
	}

	/**
	 * Delete product attributes
	 *
	 * @param $ids
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		if (count($ids) < 1) return false;

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if ($this->isAttributesUsedInVariants($ids)) return false;

		// delete manufacturers & reset products
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE paid IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * @param $ids
	 * @return bool
	 */
	public function isAttributesUsedInVariants($ids)
	{
		$attributesData = $this->db->selectAll('SELECT * FROM ' . DB_PREFIX . 'products_attributes WHERE paid IN (' . implode(',', $ids) . ')');
		if ($attributesData && is_array($attributesData))
		{
			foreach ($attributesData as $key => $data)
			{
				$pids[] = $data['pid'];
				$names[] = "attributes_list LIKE '%{$data['name']}%'";
			}
		}
		$res = $this->db->selectOne('SELECT COUNT(pid) AS total FROM '.DB_PREFIX.'products_inventory WHERE pid IN (' . implode(',', $pids) . ') AND (' . implode(' OR ', $names) . ')');
		return ($res['total'] < 1) ? 0 : 1;
	}

	/**
	 * Formats the number of our price modifier
	 * @param $matches
	 * @return string
	 */
	public function formatNumberModifier($matches)
	{
		return number_format($matches[0], 2);
	}

	/**
	 * @param $attributeType
	 * @param $options
	 * @return bool
	 */
	public function checkIsModifier($attributeType, $options)
	{
		if ($attributeType == 'select' || $attributeType == 'radio')
		{
			$opt = explode("\n", $options);

			for ($i=0; $i < count($opt); $i++)
			{
				if (preg_match("/^.+\(\s*([+-]?)\s*((\d+)|(\d+\.\d+))\s*([%]?)\s*((\,([+-]?)\s*((\d+)|(\d+\.\d+))\s*([%]?))?)\s*\)\s*$/", $opt[$i]))
				{
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * @param $productId
	 * @param $globalAttributes
	 */
	public function addGlobalAttributes($productId, $globalAttributes)
	{
		$globalAttributes = is_array($globalAttributes) ? $globalAttributes : array($globalAttributes);
		if (count($globalAttributes) < 1) return;

		$db = $this->db;

		$result = $db->query('SELECT * FROM '.DB_PREFIX.'global_attributes WHERE gaid IN ('.implode(',', $globalAttributes).')');

		while ($attribute = $db->moveNext($result))
		{
			$attribute['id'] = null;
			$attribute['pid'] = $productId;
			$attribute['is_active'] = 'Yes';
			$attribute['track_inventory'] = '0';

			$attribute = array_merge($this->getDefaults(), $attribute);

			$this->persist($attribute);
		}
	}

	/**
	 * @param $productId
	 */
	public function deleteAttributesByProductId($productId)
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE pid='.intval($productId));
	}

	/**
	 * @param $pid
	 * @param $names
	 */
	public function deleteAttributesByProductIdAndNames($pid, $names)
	{
		$db = $this->db;
		$escaped = array();
		foreach ($names as $name) $a[] = '"'.$db->escape($name).'"';
		if (count($escaped) > 0)
		{
			$db->query('DELETE FROM '.DB_PREFIX.'products_attributes WHERE pid='.intval($pid).' AND name NOT IN ('.implode(',', $names).')');
		}
	}

	/**
	 * @param $pid
	 * @return array
	 */
	public function getProductAttributesMap($pid)
	{
		$db = $this->db;
		$db->query('SELECT paid, name FROM '.DB_PREFIX.'products_attributes WHERE pid='.intval($pid));
		$attributeMap = array();
		while ($row = $db->moveNext()) $attributeMap[$row['name']] = $row['paid'];

		return $attributeMap;
	}

	/**
	 * Copy product variants
	 * @param $productId
	 * @param $newProductId
	 */
	public function copyProductAttributes($productId, $newProductId)
	{
		$db = $this->db;
		$sql = 'INSERT INTO ' . DB_PREFIX . 'products_attributes (pid,' . implode(", ", self::getAttributeFields()) . ')
			SELECT
				'. $newProductId . ',
				' . implode(", ", self::getAttributeFields()) . '
			FROM ' . DB_PREFIX . 'products_attributes WHERE pid = '. $productId;
		$db->query($sql);
	}

	/**
	 * Product attribute fields
	 * @return array
	 */
	public static function getAttributeFields()
	{
		return array('attribute_type', 'gaid', 'is_modifier', 'is_active', 'priority', 'track_inventory',
			'name', 'caption', 'text_length', 'options', 'attribute_required');
	}
}
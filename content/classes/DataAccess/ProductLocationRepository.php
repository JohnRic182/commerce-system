<?php

/**
 * Class DataAccess_ProductLocationRepository
 */
class DataAccess_ProductLocationRepository extends DataAccess_BaseRepository
{
	/**
	 * Get defaults
	 *
	 * @param $mode
	 *
	 * @return array
	 */
	public function getDefaults($mode)
	{
		// TODO: finish product locations repository, add persist
		return array(
			'id' => '',
			'name' => ''
		);
	}

	/**
	 * Get product locations by id
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public function getById($id)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products_locations WHERE products_location_id='.intval($id));
	}

	/**
	 * Get product locations count
	 *
	 * @return mixed
	 */
	public function getCount()
	{
		$result = $this->db->selectOne('SELECT COUNT(*) AS c FROM '.DB_PREFIX.'products_locations');

		return intval($result['c']);
	}

	/**
	 * Get product locations for pagination
	 *
	 * @param null $start
	 * @param null $limit
	 * @param null $orderBy
	 *
	 * @return mixed
	 */
	public function getList($start = null, $limit = null, $orderBy = null)
	{
		if (!is_null($start)) $start = intval($start);
		if (!is_null($limit)) $limit = intval($limit);
		if (is_null($orderBy)) $orderBy = 'name';

		return $this->db->selectAll(
			'SELECT * FROM '.DB_PREFIX.'products_locations ORDER BY '.$orderBy.(!is_null($start) && !is_null($limit) ? (' LIMIT '.$start.', '.$limit) : '')
		);
	}

	/**
	 * Get product locations options list
	 *
	 * @return array
	 */
	public function getOptionsList()
	{
		$list = $this->getList();

		$options = array();
		foreach ($list as $productLocation)
		{
			$options[$productLocation['products_location_id']] = $productLocation['name'];
		}

		return $options;
	}

	/**
	 * Get locations
	 */
	public function getLocationsIdsMap()
	{
		$db = $this->db;

		$locations = array();
		$db->query('SELECT products_location_id, code FROM '.DB_PREFIX.'products_locations');

		while ($db->moveNext())
		{
			$locations[strtolower($db->col["code"])] = $db->col["products_location_id"];
		}

		return $locations;
	}
}
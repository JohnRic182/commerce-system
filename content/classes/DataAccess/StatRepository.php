<?php

/**
 *  Class DataAccess_StatRepository
 */
class DataAccess_StatRepository
{
	/** @var Date Range filters */
	const DATE_RANGE_TODAY = 'today';
	const DATE_RANGE_YESTERDAY = 'yesterday';
	const DATE_RANGE_WEEK = 'week';
	const DATE_RANGE_LAST_WEEK = 'last_week';
	const DATE_RANGE_MONTH = 'month';
	const DATE_RANGE_LAST_MONTH = 'last_month';
	const DATE_RANGE_QUARTER = 'quarter';
	const DATE_RANGE_LAST_QUARTER = 'last_quarter';
	const DATE_RANGE_YEAR = 'year';
	const DATE_RANGE_LAST_YEAR = 'last_year';
	const DATE_RANGE_CUSTOM = 'custom';

	/** @var DB */
	protected $db;

	public function __construct(DB $db)
	{
		$this->db = $db;
	}


	/**
	 * @param DateTime $from
	 * @param DateTime $to
	 * @return mixed
	 */
	public function getPlacedOrdersCountPerPeriod(DateTime $from, DateTime $to)
	{
		$result = $this->db->selectOne('
			SELECT COUNT(*) AS c
			FROM ' . DB_PREFIX . 'orders
			WHERE
				removed = "No"
				AND status IN ("Process", "Backorder", "Completed")
				AND placed_date >= "' . $from->format('Y-m-d') . ' 00:00:00"
				AND placed_date < "' . $to->format('Y-m-d') . ' 23:59:59"
		');

		return $result['c'];
	}

	/**
	 * @param DateTime $from
	 * @param DateTime $to
	 * @return mixed
	 */
	public function getCheckoutAttemptsCountPerPeriod(DateTime $from, DateTime $to)
	{
		$result = $this->db->selectOne('
			SELECT COUNT(*) AS c
			FROM ' . DB_PREFIX . 'orders
			WHERE
				removed = "No"
				AND status != "New"
				AND placed_date >= "' . $from->format('Y-m-d') . ' 00:00:00"
				AND placed_date < "' . $to->format('Y-m-d') . ' 23:59:59"
		');
		return $result['c'];
	}

	/**
	 * @param DateTime $from
	 * @param DateTime $to
	 * @return mixed
	 */
	public function getTotalRevenuePerPeriod(DateTime $from, DateTime $to)
	{
		$result = $this->db->selectOne('
			SELECT SUM(total_amount - gift_cert_amount) AS c
			FROM ' . DB_PREFIX . 'orders
			WHERE
				removed = "No"
				AND status IN ("Process", "Backorder", "Completed")
				AND placed_date >= "' . $from->format('Y-m-d') . ' 00:00:00"
				AND placed_date < "' . $to->format('Y-m-d') . ' 23:59:59"
		');

		return $result['c'];
	}


	public function getOrdersMonthData()
	{
		$year = date('Y');
		$month = date('n');

		$mysqlFrom = $year . "-" . $month . "-01 00:00:00";
		$mysqlTo = ($month == 12 ? $year + 1 : $year) . "-" . ($month == 12 ? 1 : $month + 1) . "-01 00:00:00";

		$monthStat = array();

		//orders placed per month
		$d = $this->db->selectOne("SELECT COUNT(*) AS c
			FROM " . DB_PREFIX . "orders
			WHERE removed <> 'Yes' AND status IN ('Process', 'Backorder', 'Completed') AND placed_date >= '" . $mysqlFrom . "' AND placed_date < '" . $mysqlTo . "'");
		$monthStat["ordersCount"] = $d['c'];

		//orders total per month
		$d = $this->db->selectOne("SELECT SUM(total_amount - gift_cert_amount) AS c
			FROM " . DB_PREFIX . "orders
			WHERE removed <> 'Yes' AND status IN ('Process', 'Backorder', 'Completed') AND placed_date >= '" . $mysqlFrom . "' AND placed_date < '" . $mysqlTo . "'");
		$monthStat["ordersTotal"] = $d['c'];

		//items count per month
		$d = $this->db->selectOne("SELECT SUM(oc.admin_quantity) AS c
			FROM " . DB_PREFIX . "orders o
			INNER JOIN " . DB_PREFIX . "orders_content oc ON o.oid = oc.oid AND o.removed <> 'Yes' AND o.status IN ('Process', 'Backorder', 'Completed')
			WHERE placed_date >= '" . $mysqlFrom . "' AND placed_date < '" . $mysqlTo . "'");
		$monthStat["itemsCount"] = $d['c'];

		return $monthStat;
	}

	public function getCustomersMonthData()
	{
		$year = date('Y');
		$month = date('n');

		$mysqlFrom = $year . "-" . $month . "-01 00:00:00";
		$mysqlTo = ($month == 12 ? $year + 1 : $year) . "-" . ($month == 12 ? 1 : $month + 1) . "-01 00:00:00";

		$monthStat = array();

		$d = $this->db->selectOne("SELECT COUNT(*) AS c FROM " . DB_PREFIX . "users WHERE removed = 'No' AND login <> 'ExpressCheckoutUser'");
		$monthStat['registeredCount'] = $d['c'];

		$d = $this->db->selectOne("SELECT COUNT(*) AS c FROM " . DB_PREFIX . "users WHERE removed = 'No' AND created_date >= '" . $mysqlFrom . "' AND created_date < '" . $mysqlTo . "' AND login <> 'ExpressCheckoutUser'");
		$monthStat['newRegisteredCount'] = $d['c'];

		$d = $this->db->selectOne("SELECT COUNT(*) AS c FROM " . DB_PREFIX . "users WHERE removed = 'No' AND login = 'ExpressCheckoutUser'");
		$monthStat['expressCount'] = $d['c'];

		$d = $this->db->selectOne("SELECT COUNT(*) AS c FROM " . DB_PREFIX . "users WHERE removed = 'No' AND created_date >= '" . $mysqlFrom . "' AND created_date < '" . $mysqlTo . "' AND login = 'ExpressCheckoutUser'");
		$monthStat['newExpressCount'] = $d['c'];

		return $monthStat;
	}

	public function getProductsData()
	{
		$stat = array();

		$d = $this->db->selectOne("SELECT COUNT(*) AS c FROM " . DB_PREFIX . "products WHERE product_id <> 'gift_certificate'");
		$stat['productsCount'] = $d['c'];

		$d = $this->db->selectOne("SELECT COUNT(*) AS c FROM " . DB_PREFIX . "products WHERE product_id <> 'gift_certificate' AND inventory_control <> 'No' AND stock = 0");
		$stat['outOfStockCount'] = $d['c'];

		return $stat;
	}

	/**
	 * Get Testimonials Data
	 *
	 * @return array
	 */
	public function getTestimonialsData()
	{
		$stat = array();

		$d = $this->db->selectOne("SELECT COUNT(*) AS c FROM " . DB_PREFIX . "testimonials WHERE status = 'approved'");
		$stat['testimonialsCount'] = $d['c'];

		$d = $this->db->selectOne("SELECT AVG(rating) AS `rating` FROM " . DB_PREFIX . "testimonials WHERE status = 'approved' AND rating > 0");
		$stat['testimonialAverageRatings'] = $d['rating'];

		$d = $this->db->selectOne("SELECT AVG(rating) AS `rating` FROM " . DB_PREFIX . "testimonials WHERE status = 'approved' AND date_created > DATE_SUB(now(), INTERVAL 3 MONTH)");
		$stat['testimonialsCount3'] = $d['rating'];

		return $stat;
	}

	/**
	 * Get report product statistics by date range
	 *
	 * @param string $reportType
	 * @param DateTime|null $startDate
	 * @param DateTime|null $endDate
	 * @return array
	 */
	public function getReportProductStatistics($reportType = 'custom', DateTime $startDate = null, DateTime $endDate = null)
	{
		$stat = array();
		$previousTypes = array('today' => self::DATE_RANGE_YESTERDAY,
			'week' => self::DATE_RANGE_LAST_WEEK,
			'month' => self::DATE_RANGE_LAST_MONTH,
			'quarter' => self::DATE_RANGE_LAST_QUARTER,
			'year' => self::DATE_RANGE_LAST_YEAR,
			'custom' => self::DATE_RANGE_CUSTOM
		);
		$previousType = $previousTypes[$reportType];

		$dateDifference = $startDate->diff($endDate);
		$previousEndDate = new DateTime($startDate->format("Y-m-d H:i:s"));
		$previousEndDate->sub(new DateInterval("P1D"));

		$previousStartDate = new DateTime($previousEndDate->format("Y-m-d H:i:s"));
		$previousStartDate->sub(new DateInterval("P{$dateDifference->days}D"));

		//top performing
		$stat['top_performing'] =  $this->getTopPerformingProducts($reportType, $startDate, $endDate);

		//products low on inventory
		$stat['low_on_inventory'] = $this->getLowOnInventoryProducts($reportType, $startDate, $endDate);

		//non performing
		$stat['non_performing'] = $this->getNonPerformingProducts($reportType, $startDate, $endDate);

		//popular manufacturers
		$stat['popular_manufacturers'] = $this->getPopularManufacturers($reportType, $startDate, $endDate);

		//popular categories
		$stat['popular_categories'] = $this->getPopularCategories($reportType, $startDate, $endDate);

		return $stat;
	}

	/**
	 * Get report order statistics by date range
	 *
	 * @param string $reportType
	 * @param DateTime|null $startDate
	 * @param DateTime|null $endDate
	 * @return array
	 */
	public function getReportOrderStatistics($reportType = 'custom', DateTime $startDate = null, DateTime $endDate = null)
	{
		$stat = array();
		$previousTypes = array('today' => self::DATE_RANGE_YESTERDAY,
			'week' => self::DATE_RANGE_LAST_WEEK,
			'month' => self::DATE_RANGE_LAST_MONTH,
			'quarter' => self::DATE_RANGE_LAST_QUARTER,
			'year' => self::DATE_RANGE_LAST_YEAR,
			'custom' => self::DATE_RANGE_CUSTOM
		);
		$previousType = $previousTypes[$reportType];

		$dateDifference = $startDate->diff($endDate);
		$previousEndDate = new DateTime($startDate->format("Y-m-d H:i:s"));
		$previousEndDate->sub(new DateInterval("P1D"));

		$previousStartDate = new DateTime($previousEndDate->format("Y-m-d H:i:s"));
		$previousStartDate->sub(new DateInterval("P{$dateDifference->days}D"));

		return $stat;
	}

	/**
	 * Get Popular Categories
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getPopularCategories($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		return $this->db->selectAll("SELECT c.cid, c.key_name, c.name, SUM(oc.admin_quantity) as products_sold
			FROM " . DB_PREFIX . "orders o
			INNER JOIN " . DB_PREFIX . "orders_content oc ON oc.oid = o.oid
			INNER JOIN " . DB_PREFIX . "products p ON p.pid = oc.pid
			INNER JOIN " . DB_PREFIX . "catalog c ON c.cid = p.cid
			WHERE o.payment_Status='Received' AND o.status = 'completed' AND c.cid IS NOT NULL
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
			GROUP BY c.cid
			ORDER BY products_sold DESC, c.key_name DESC
			LIMIT 10");
	}

	/**
	 * Get Top Performing Products
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getTopPerformingProducts($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		return  $this->db->selectAll("SELECT oc.pid, oc.product_id, oc.title, SUM(oc.admin_quantity) as quantity, SUM(oc.admin_price * oc.admin_quantity) AS product_amount 
		FROM " . DB_PREFIX . "orders_content oc
		INNER JOIN " . DB_PREFIX . "orders o ON oc.oid = o.oid AND o.payment_Status='Received' AND o.status = 'completed'
		AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
		GROUP BY oc.pid 
		ORDER BY quantity DESC, product_amount DESC, oc.title DESC
		LIMIT 10");

	}

	/**
	 * Get Low On Inventory Products
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getLowOnInventoryProducts($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		return  $this->db->selectAll('SELECT DISTINCT p.pid, p.product_id, p.title, stock as quantity_on_hand
			FROM ' . DB_PREFIX . 'products p
			WHERE p.inventory_control <> "No"
			AND DATE(p.added) BETWEEN "' . $dateFrom . '" AND  "' . $dateTo . '"
			ORDER BY stock, p.title
			LIMIT 10');
	}

	/**
	 * Get Non Performing Products
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getNonPerformingProducts($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		return  $this->db->selectAll("SELECT oc.pid, oc.product_id, oc.title, SUM(oc.admin_price * oc.admin_quantity) AS amount_sold 
			FROM " . DB_PREFIX . "orders_content oc
			INNER JOIN orders o ON oc.oid = o.oid AND o.payment_Status = 'Received' AND o.status = 'completed' 
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
			GROUP BY oc.pid 
			ORDER BY amount_sold, oc.title
			LIMIT 10");

	}

	/**
	 * Get Popular Manufacturers
	 *
	 * @param array $dateRange
	 * @return mixed
	 */
	public function getPopularManufacturers($dateRange = array())
	{
		$dateFrom = $this->db->escape($dateRange['start']);
		$dateTo = $this->db->escape($dateRange['end']);

		return  $this->db->selectAll("SELECT m.manufacturer_id, m.manufacturer_code, m.manufacturer_name, SUM(oc.admin_quantity) as products_sold
			FROM " . DB_PREFIX . "orders o
			INNER JOIN " . DB_PREFIX . "orders_content oc ON oc.oid = o.oid
			INNER JOIN " . DB_PREFIX . "products p ON p.pid = oc.pid
			INNER JOIN " . DB_PREFIX . "manufacturers m ON m.manufacturer_id = p.manufacturer_id
			WHERE o.payment_Status='Received' AND o.status = 'completed' AND m.manufacturer_id IS NOT NULL
			AND DATE(o.placed_date) BETWEEN '" . $dateFrom . "' AND '" . $dateTo . "'
			GROUP BY m.manufacturer_id 
			ORDER BY products_sold DESC, m.manufacturer_name DESC
			LIMIT 10");
	}

	/**
	 * @param $where
	 * @return array
	 */
	public function getOrderDiscounts($where)
	{
		$sql = 'SELECT ROUND(COALESCE(SUM(o.discount_amount + o.promo_discount_amount), 0),2) AS total_discount_amount 
			FROM ' . DB_PREFIX . 'orders o 
			WHERE 
				o.removed = "No" AND 
				o.status = "Completed" AND 
				o.payment_status = "Received" AND 
				DATE(o.placed_date) >= "' . $where['start'] . '" AND DATE(o.placed_date) <= "' . $where['end'] . '"';

		$res = $this->db->selectOne($sql);
		return $res['total_discount_amount'];;
	}

	/**
	 * @param $where
	 * @return mixed
	 */
	public function getQrCampaigns($where)
	{
		$sql = 'SELECT
				o.offsite_campaign_id AS campaign_id,
				oc.name AS campaign_name,
				count(*) AS orders_count,
				COALESCE(SUM(o.total_amount - o.gift_cert_amount), 0) AS orders_total_amount
			FROM ' . DB_PREFIX . 'orders AS o
			INNER JOIN ' . DB_PREFIX . 'offsite_campaigns oc ON o.offsite_campaign_id = oc.campaign_id
			WHERE
				o.status = "Completed" AND
				o.payment_status = "Received" AND
				o.removed = "No" AND
				oc.type = "qr" AND 
				DATE(o.placed_date) >= "' . $where['start'] . '" AND DATE(o.placed_date) <= "' . $where['end'] . '"
			GROUP BY o.offsite_campaign_id
			ORDER BY orders_total_amount DESC, campaign_name ASC';

		return $this->db->selectAll($sql);
	}

	/**
	 * @param string $where
	 * @return mixed
	 */
	public function getTopPromoCodes($where)
	{
		$sql = 'SELECT pc.*, count(*) AS orders_count, COALESCE(SUM(o.total_amount - o.gift_cert_amount), 0) AS orders_total_amount FROM ' . DB_PREFIX . 'promo_codes pc
		    INNER JOIN ' . DB_PREFIX . 'orders o ON o.promo_campaign_id = pc.pid
			WHERE 
				o.removed = "No" AND 
				o.status = "Completed" AND 
				o.payment_status = "Received" AND
				DATE(o.placed_date) >= "' . $where['start'] . '" AND DATE(o.placed_date) <= "' . $where['end'] . '"
			GROUP BY pc.pid 
			ORDER BY orders_total_amount DESC, pc.promo_code ASC LIMIT 10';

		return $this->db->selectAll($sql);
	}

	/**
	 * @param string $where
	 * @return mixed
	 */
	public function getTopOffsiteCamapaigns($where)
	{
		$sql = 'SELECT oc.*, count(*) AS orders_count, COALESCE(SUM(o.total_amount - o.gift_cert_amount), 0) AS orders_total_amount FROM ' . DB_PREFIX . 'offsite_campaigns oc
		    INNER JOIN ' . DB_PREFIX . 'orders o ON o.offsite_campaign_id = oc.campaign_id
			WHERE 
				o.removed = "No" AND 
				o.status = "Completed" AND 
				o.payment_status = "Received" AND 
				DATE(o.placed_date) >= "' . $where['start'] . '" AND DATE(o.placed_date) <= "' . $where['end'] . '" 
			GROUP BY oc.campaign_id 
			ORDER BY orders_total_amount DESC, oc.name ASC LIMIT 10';

		return $this->db->selectAll($sql);
	}
}
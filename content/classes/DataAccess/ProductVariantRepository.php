<?php

/**
 * Class DataAccess_ProductVariantRepository
 */
class DataAccess_ProductVariantRepository extends DataAccess_BaseRepository
{
	/**
	 * @return array
	 */
	public function getDefaults()
	{
		return array(
			'id' => null,
			'pi_id' => null,
			'pid' => 0,
			'product_subid' => '',
			'product_sku' => '',
			'stock' => '10',
			'stock_warning' => '1',
			'is_active' => '0',
			'attributes_hash' => '',
			'attributes_list' => '',
		);
	}

	/**
	 * Get variants by product
	 *
	 * @param $productId
	 * @return mixed
	 */
	public function getListByProductId($productId)
	{
		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'products_inventory WHERE pid='.intval($productId).' ORDER BY pi_id');
	}

	public function getByProductIds(array $pids)
	{
		if (count($pids) == 0) return array();

		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'products_inventory WHERE pid IN ('.implode(',', $pids).') ORDER BY pid, pi_id');
	}

	/**
	 * Get variant by id
	 *
	 * @param $variantId
	 * @return array|bool
	 */
	public function getById($variantId)
	{
		return $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'products_inventory WHERE pi_id='.intval($variantId));
	}

	/**
	 * @param $productDbId
	 * @param $productSubId
	 * @return mixed
	 */
	public function getProductVariantBySubId($productDbId, $productSubId)
	{
		$db = $this->db;
		return $db->selectOne('SELECT * FROM '.DB_PREFIX.'products_inventory WHERE pid='.intval($productDbId).' AND product_subid="'.$db->escape($productSubId).'"');
	}

	/**
	 * Get validation errors
	 *
	 * @param $data
	 * @param $mode
	 *
	 * @return array|bool
	 */
	public function getValidationErrors($data, $mode)
	{
		$errors = array();
		if (is_array($data))
		{
			$inventoryControl = isset($data['inventory_control']) ? $data['inventory_control'] : 'No';

			if (!isset($data['pid']) || intval($data['pid']) == 0)
			{
				$errors['pid'] = array('Cannot find parent product id');
			}

			if (isset($data['required-attributes']) && is_array($data['required-attributes']))
			{
				foreach ($data['required-attributes'] as $attributeId => $requiredAttribute)
				{
					if(isset($data['attributes']) && is_array($data['attributes']))
					{
						if(!key_exists($attributeId, $data['attributes']))
						{
							$errors['attribute-' . $attributeId] = array('Please ' . ($requiredAttribute['attribute_type'] == 'select' ? 'choose' : 'enter') . ' a ' .  $requiredAttribute['caption']);
						}
					}
					else
					{
						$errors['attribute-' . $attributeId] = array('Please ' . ($requiredAttribute['attribute_type'] == 'select' ? 'choose' : 'enter') . ' a ' .  $requiredAttribute['caption']);
					}
				}
			}

			if (!isset($data['product_subid']) || trim($data['product_subid']) == '')
			{
				$errors['product_subid'] = array('Please enter product sub id');
			}

			$db = $this->db;

			$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pi_id']) ? $data['pi_id'] : null);

			$productSubId = $db->selectOne('
				SELECT * FROM '.DB_PREFIX.'products_inventory
				WHERE pid='.intval($data['pid']).' AND product_subid="'.$db->escape($data['product_subid']).'"'.($mode == 'update' ? ' AND pi_id <> '.intval($data['id']) : '')
			);

			if ($productSubId)
			{
				$errors['product_subid'] = array('Entered product sub ID already exists!');
			}

			if (!in_array($inventoryControl, array('Yes', 'No')))
			{
				if (!isset($data['stock']) || trim($data['stock']) == '')
				{
					$errors['stock'] = array('Please enter product variant stock');
				}

				if (!isset($data['stock_warning']) || trim($data['stock_warning']) == '')
				{
					$errors['stock_warning'] = array('Please enter product variant stock warning');
				}
			}
		}

		return count($errors) > 0 ? $errors : false;
	}

	/**
	 * @param $data
	 * @param null $params
	 * @return bool|int
	 */
	public function persist($data, $params = null)
	{
		$productAttributes = is_array($params) && isset($params['productAttributes']) ? $params['productAttributes'] : array();
		$db = $this->db;

		$data['id'] = isset($data['id']) ? $data['id'] : (isset($data['pi_id']) ? $data['pi_id'] : null);

		$db->reset();

		if (is_null($data['id']) || (isset($data['attributes']) && is_array($data['attributes']) && count($data['attributes']) > 0))
		{
			$attributesList = '';
			$attributesHash = '';

			foreach ($productAttributes as $productAttribute)
			{
				if ($productAttribute['is_active'] == 'Yes' && $productAttribute['track_inventory'] == '1' && in_array($productAttribute['attribute_type'], array('radio', 'select')))
				{
					$attributesHash = $attributesHash . ($attributesHash == '' ? '' : "\n") . $productAttribute['paid'] . ': ' . (isset($data['attributes']) && is_array($data['attributes']) && isset($data['attributes'][$productAttribute['paid']]) ? htmlspecialchars_decode($data['attributes'][$productAttribute['paid']]) : '');
					$attributesList = $attributesList . ($attributesList == '' ? '' : "\n") . $productAttribute['name'] . ': ' . (isset($data['attributes']) && is_array($data['attributes']) && isset($data['attributes'][$productAttribute['paid']]) ? htmlspecialchars_decode($data['attributes'][$productAttribute['paid']]) : '');
				}
			}

			$db->assignStr('attributes_hash', md5($attributesHash));
			$db->assignStr('attributes_list', $attributesList);
		}

		$db->assignStr('pid', $data['pid']);
		$db->assignStr('product_subid', $data['product_subid']);
		$db->assignStr('product_sku', $data['product_sku']);
		$db->assignStr('stock', isset($data['stock']) ? intval($data['stock']) : 0);
		$db->assignStr('stock_warning', isset($data['stock_warning']) ? intval($data['stock_warning']) : 0);
		$db->assignStr('is_active', $data['is_active']);

		if (!is_null($data['id']) && $data['id'] && $data['id'] != '')
		{
			$db->update(DB_PREFIX.'products_inventory',  'WHERE pi_id='.intval($data['id']));
			return true;
		}
		else
		{
			return $db->insert(DB_PREFIX.'products_inventory');
		}
	}

	/**
	 * Delete product variants
	 *
	 * @param $ids
	 * @return bool
	 */
	public function delete($ids)
	{
		$ids = is_array($ids) ? $ids : array($ids);

		foreach ($ids as $key=>$value) $ids[$key] = intval($value);

		if (count($ids) < 1) return false;

		// delete manufacturers & reset products
		$this->db->query('DELETE FROM '.DB_PREFIX.'products_inventory WHERE pi_id IN('.implode(',', $ids).')');

		return true;
	}

	/**
	 * Copy product attributes
	 * @param $productId
	 * @param $newProductId
	 */
	public function copyProductVariants($productId, $newProductId)
	{
		$db = $this->db;
		$sql = 'INSERT INTO ' . DB_PREFIX . 'products_inventory (pid,' . implode(", ", self::getVariantsFields()) . ')
			SELECT
				'. $newProductId . ',
				' . implode(", ", self::getVariantsFields()) . '
			FROM ' . DB_PREFIX . 'products_inventory WHERE pid = '. $productId;
		$db->query($sql);
	}

	/**
	 * Product variants fields
	 * @return array
	 */
	public static function getVariantsFields()
	{
		return array('product_subid', 'product_sku', 'stock', 'stock_warning',
			'is_active', 'attributes_hash', 'attributes_list');
	}
}
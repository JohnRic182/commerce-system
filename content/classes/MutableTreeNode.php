<?php

class MutableTreeNode
{
	/** @var bool */
	protected $allowsChildren;
	/** @var MutableTreeNode */
	protected $parent;
	/** @var array */
	protected $children;
	/** @var object|array|null */
	protected $userObject;

	/** @var int */
	protected $level;

	public function __construct($userObject = null, $allowsChildren = true)
	{
		$this->userObject = $userObject;
		$this->allowsChildren = $allowsChildren;
		$this->children = array();
		$this->level = 0;
	}

	public function add(MutableTreeNode $child)
	{
		$this->children[] = $child;
		$child->setParent($this);
	}

	public function getChildren()
	{
		return $this->children;
	}
	public function getChildCount()
	{
		return count($this->children);
	}

	public function getUserObject()
	{
		return $this->userObject;
	}
	public function setUserObject($userObject)
	{
		$this->userObject = $userObject;
	}

	public function getAllowsChildren()
	{
		return $this->allowsChildren;
	}
	public function setAllowsChildren($allowsChildren)
	{
		$this->allowsChildren = $allowsChildren;
	}

	public function getLevel()
	{
		return $this->level;
	}
	protected function setLevel($level)
	{
		$this->level = $level;

		foreach ($this->children as $child)
		{
			/** @var MutableTreeNode $child */
			$child->setLevel($level + 1);
		}
	}

	public function removeNode(MutableTreeNode $node)
	{
		$temp = array();
		foreach ($this->children as $child)
		{
			if ($child !== $node)
			{
				$temp[] = $child;
			}
		}
		$this->children = $temp;
	}

	public function isRoot()
	{
		return $this->parent == null;
	}

	public function getParent()
	{
		return $this->parent;
	}
	public function setParent(MutableTreeNode $parent)
	{
		$this->parent = $parent;
		$this->setLevel($parent->getLevel() + 1);
	}
	public function removeFromParent()
	{
		if ($this->parent !== null) $this->parent->removeNode($this);
		$this->parent = null;
		$this->setLevel(0);
	}
}
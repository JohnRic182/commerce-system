<?php

	//current working dir must be cart root folder

	//include libraries
	if (!defined('__DIR__')) define('__DIR__', dirname(__FILE__));

	require_once 'content/vendors/autoload.php';

	Config::loadApplicationConfig();

	//init db
	$db = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

	$settingsRepository = new DataAccess_SettingsRepository($db);
	$settings = $settingsRepository->getAll();

	require_once 'content/engine/engine_label.php';

	Timezone::setApplicationDefaultTimezone($db, $settings);

	UrlUtils::init($settings);
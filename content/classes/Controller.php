<?php
/**
 * Base controller class
 */
class Controller
{
	protected $db = null;
	protected $settings = null;
	protected $view = null;
	
	public function __construct(DB $db = null, $settings = null, $view = null)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->view = $view;
	}
	
	public function setDb($db)
	{
		$this->db = $db;
	}
	
	public function setSettings(&$settings)
	{
		$this->settings = $settings;
	}
	
	public function setView($view)
	{
		$this->view = $view;
	}
}

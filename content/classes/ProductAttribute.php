<?php

define("PRODUCT_ATTRIBUTE_TYPE_TEXT", "text");
define("PRODUCT_ATTRIBUTE_TYPE_TEXTAREA", "textarea");
define("PRODUCT_ATTRIBUTE_TYPE_SELECT", "select");
define("PRODUCT_ATTRIBUTE_TYPE_RADIO", "radio");

class ProductAttribute // extends Model
{
	protected $_db = null;

	/**
	 * ProductAttribute constructor.
	 * @param $db
	 */
	public function __construct($db)
	{
		$this->_db = $db;
		return $this;
	}
	
	protected function db()
	{
		return $this->_db;
	}
	
	/**
	 * Add new product attribute
	 * @param type $data 
	 */
	public function add($product_id, $data)
	{
		$this->db()->reset();
		$this->db()->assign("pid", intval($product_id));
		$this->db()->assign("gaid", isset($data["gaid"]) ? intval($data["gaid"]) : 0);
		$this->db()->assignStr("attribute_type", $data["attribute_type"]);
		$this->db()->assignStr("name", $data["name"]);
		$this->db()->assignStr("caption",$data["caption"]);
		$this->db()->assignStr("priority", $data["priority"]);
		$this->db()->assignStr("is_active", isset($data["is_active"]) && $data['is_active'] == 'Yes' ? "Yes" : "No");
		$this->db()->assignStr("track_inventory", isset($data["track_inventory"]) && intval($data['track_inventory']) != 0 ? $data["track_inventory"] : "0");
		
		if ($data["attribute_type"] == "select" || $data["attribute_type"] == "radio")
		{
			$is_modifier = $this->isModifier($data["options"]);
			$this->db()->assignStr("options", $data["options"]);
			$this->db()->assignStr("text_length", 0);
		}
		else
		{
			$is_modifier = false;
			$this->db()->assignStr("options", "");
			$this->db()->assignStr("text_length", $data["text_length"]);
		}
		
		$this->db()->assignStr("is_modifier", $is_modifier ? "Yes" : "No");
		
		return $this->db()->insert(DB_PREFIX."products_attributes");
	}
	
	/**
	 * Update product attribute
	 * @param type $product_attribute_id
	 * @param type $data 
	 */
	public function update($product_id, $product_attribute_id, $data)
	{
		$this->db()->reset();
		
		if (isset($data["attribute_type"]))	$this->db()->assignStr("attribute_type", $data["attribute_type"]);
		
		if (isset($data["gaid"])) $this->db()->assign("gaid", 0);
		
		$this->db()->assignStr("attribute_type", $data["attribute_type"]);
		$this->db()->assignStr("is_modifier", in_array($data["attribute_type"], array("select", "radio")) && isset($data["options"]) && $this->isModifier($data["options"]) ? "Yes" : "No");
		$this->db()->assignStr("is_active", isset($data["is_active"]) ? "Yes" : "No");
		$this->db()->assignStr("priority", $data["priority"]);
		$this->db()->assignStr("track_inventory", isset($data["track_inventory"]) ? "1" : "0");
		$this->db()->assignStr("name", $data["name"]);
		$this->db()->assignStr("caption",$data["caption"]);
		
		if (isset($data["text_length"])) $this->db()->assignStr("text_length", $data["text_length"]);
		
		if (isset($data["options"])) $this->db()->assignStr("options", $data["options"]);
		
		$this->db()->update(DB_PREFIX."products_attributes", "WHERE pid='".intval($product_id)."' AND paid='".intval($product_attribute_id)."'");
	}
	
	/**
	 * Delete product attribute
	 * @param type $product_id
	 * @param type $product_attribute_id 
	 */
	public function delete($product_id, $product_attribute_id)
	{
		$this->db()->query("DELETE FROM ".DB_PREFIX."products_attributes WHERE pid='".intval($product_id)."' AND paid='".intval($product_attribute_id)."'");
	}
	
	/**
	 * Check does option is a price / weight modifier
	 */
	public function optionIsModifier($option)
	{
		return preg_match("/^.+\(\s*([+-]?)\s*((\d+)|(\d+\.\d+))\s*([%]?)\s*((\,\s*([+-]?)\s*((\d+)|(\d+\.\d+))\s*([%]?))?)\s*\)\s*$/", $option);
	}
	
	/**
	 * 
	 */
	public function parseOptions($options)
	{
		return explode("\n", $options);
	}
	
	/**
	 * 
	 */
	public function isModifier($options)
	{
		$is_modifier = false;
		
		$parsed_options = $this->parseOptions($options);
		
		foreach ($parsed_options as $option)
		{
			if ($this->optionIsModifier($option))
			{
				$is_modifier = true;
			}
		}
		
		return $is_modifier;
	}
	
	/**
	 * Checks if a product inventory sub id is unique
	 *
	 * @param integer $productId Product pid
	 * @param integer $productInventoryId Product inventory id
	 * @param string $subId Product sub ID string
	 * @return boolean
	 */
	public function isInventorySubIdUnique($productId, $productInventoryId, $subId)
	{
		$this->db()->query('SELECT COUNT(0) FROM '.DB_PREFIX.'products_inventory WHERE pid = '.intval($productId).' AND pi_id != '.intval($productInventoryId). ' AND product_subid = "'.$this->db()->escape($subId).'"');
		
		$this->db()->moveNext();
		
		return ($this->db()->col[0] == 0);
	}

	public static function parseOptions2($options, $price, $weight, &$result, $admin_call = false)
	{
		global $settings;

		$result = array();
		$options_array = explode("\n", $options);
		for ($i=0; $i<count($options_array); $i++)
		{
			$option = trim($options_array[$i]);
			if ($option != "")
			{
				//
				$option_parts = array();
				$option_parts = explode("(", $option);
				$option_name = trim($option_parts[0]);
				$option_price = $price;
				$option_difference = 0;
				$option_weight = $weight;
				$option_modifier = "";
				$option_value = 0;
				$option_value_type = "amount";
				$weight_option_value = 0;
				$weight_option_value_type = "amount";
				$weight_difference = 0;
				if (count($option_parts)>1)
				{
					//check price modifier
					$modifier = trim($option_parts[1]);
					if (strlen($modifier) > 2)
					{
						if ($modifier[strlen($modifier)-1] == ")")
						{
							$modifier = trim(substr($modifier, 0, strlen($modifier)-1));
							$mod_parts = explode(",", $modifier);
							
							$price_modifier = $mod_parts[0];
							
							if ($price_modifier[strlen($price_modifier)-1] == "%"){
								$procentage = substr($price_modifier, 0, strlen($price_modifier)-1)*1;
								$option_difference = round($price / 100 * $procentage, 2);
								$option_price = $price + $option_difference;
								if ($option_difference != 0.00 && $procentage != 0)
								{
									$option_modifier = ($procentage > 0 ? "+" : "").$procentage."%";
								}
								else
								{
									$option_modifier = "";
								}
								$option_value = $procentage;
								$option_value_type = "percentage";
							}
							else
							{
								$price_modifier = round($price_modifier*1, 2);
								$option_difference = $price_modifier;
								$option_price = $price + $price_modifier;
								if ($option_difference != 0 && $option_price != 0)
								{
									if ($admin_call)
									{
										$option_modifier = ($price_modifier>0?"+":"-")."".getAdminPrice($price_modifier*($price_modifier>0?1:-1));
									}
									else
									{
										$option_modifier = ($price_modifier>0?"+":"-")."".GetPrice($price_modifier*($price_modifier>0?1:-1));
									}
								}
								else
								{
									$option_modifier = "";
								}
								$option_value = $price_modifier;
								$option_value_type = "amount";
							}
							
							if (count($mod_parts) > 1)
							{
								$weight_modifier = $mod_parts[1];
								if ($weight_modifier[strlen($weight_modifier)-1] == "%")
								{
									$weight_procentage = substr($weight_modifier, 0, strlen($weight_modifier)-1)*1;
									$weight_difference = $weight / 100 * $weight_procentage;
									$option_weight = $weight + $weight / 100 * $weight_procentage;
									if ($settings["ShippingShowWeight"] == "YES" && $weight_procentage != 0)
									{
										$option_modifier = (strlen(trim($option_modifier))>0?$option_modifier.", ":$option_modifier).($weight_procentage>0?"+":"").$weight_procentage."%";
									}
									$weight_option_value = $weight_procentage;
									$weight_option_value_type = "percentage";
								}
								else
								{
									$weight_modifier *=1;
									$weight_difference = $weight_modifier;
									$option_weight = $weight + $weight_modifier;
									if ($settings["ShippingShowWeight"] == "YES" && $weight_modifier != 0)
									{
										$option_modifier = (strlen(trim($option_modifier))>0?$option_modifier.", ":$option_modifier). ($weight_modifier>0?"+":"-")."".GetWeight($weight_modifier*($weight_modifier>0?1:-1));
									}
									$weight_option_value = $weight_modifier;
									$weight_option_value_type = "amount";
								}
							}
							if ($option_modifier != "")
							{
								$option_modifier = " (".$option_modifier.")";
							}
						}
					}
					
				}
				$result[$option_name] = array(
					"full"=>$option,
					"name"=>$option_name,
					"price"=>$option_price,
					"price_difference"=>$option_difference,
					"weight"=>$option_weight,
					"modifier"=>$option_modifier,
					"option_value"=>$option_value,
					"option_value_type"=>$option_value_type,
					"weight_option_value"=>$weight_option_value,
					"weight_option_value_type"=>$weight_option_value_type,
					"weight_difference"=>$weight_difference,
					"first"=>count($result)==0
				);
			}
		}
		return count($result)>0;
	}
}
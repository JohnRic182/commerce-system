<?php

class Widgets
{
	/**
	 * Return view
	 * @return VIEW
	 */
	protected function view()
	{
		return view();
	}

	/**
	 *
	 */
	public function renderProductStatic(
		$product_style,
		$product_title,
		$product_price,
		$product_overview,
		$product_link,
		$product_link_text,
		$product_image_url,
		$product_image_alt
	) {
		$this->view()->assign('product_title', $product_title);
		$this->view()->assign('product_price', $product_price);
		$this->view()->assign('product_overview', $product_overview);
		$this->view()->assign('product_link', $product_link);
		$this->view()->assign('product_link_text', $product_link_text);
		$this->view()->assign('product_image_url', $product_image_url);
		$this->view()->assign('product_image_alt', $product_image_alt);
		$html = $this->view()->fetch('templates/admin/widgets/product-static/style'.$product_style.'.html');
		return $html;
	}
}
<?php

/**
 * Class Events_ProductEvent
 */
class Events_TaxEvent extends Events_Event
{
	const ON_TAX_RATE_ADDED = 'on-tax-rate-added';
	const ON_TAX_RATE_UPDATED = 'on-tax-rate-updated';
	const ON_TAX_RATE_DELETED = 'on-tax-rate-deleted';

	const ON_TAX_CLASS_ADDED = 'on-tax-class-added';
	const ON_TAX_CLASS_UPDATED = 'on-tax-class-updated';
	const ON_TAX_CLASS_DELETED = 'on-tax-class-deleted';
}
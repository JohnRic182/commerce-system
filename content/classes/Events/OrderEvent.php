<?php
/**
 * Class Events_OrderEvent
 */
class Events_OrderEvent extends Events_Event
{
	const ON_STATUS_CHANGE = 'on-order-status-change';
	const ON_CREATED = 'on-order-created';
	const ON_BEFORE_PAYMENT = 'on-order-before-payment';
	const ON_FAILED_PAYMENT = 'on-failed-payment';
	const ON_PLACED = 'on-order-placed';
	const ON_COMPLETED = 'on-order-completed';
	const ON_DELETED = 'on-order-deleted';
	const ON_ADD_ITEM = 'on-order-add-item';
	const ON_FIND_EXISTING_LINE_ITEM = 'on-order-find-existing-line-item';
	const ON_UPDATE_ITEMS = 'on-order-update-items';
	const ON_REMOVE_ITEM = 'on-order-remove-item';
	const ON_PRE_UPDATE_LINE_ITEM = 'on-order-pre-update-line-item';

	/**
	 * Set order
	 * 
	 * @param ORDER $order
	 * 
	 * @return Events_OrderEvent
	 */
	public function setOrder(&$order)
	{
		$this->setData('order', $order);
		return $this;
	}

	/**
	 * Returns order
	 * 
	 * @return ORDER
	 */
	public function getOrder()
	{
		return $this->getData('order');
	}

	/**
	 * Handle on order status change
	 *
	 * @param ORDER $order
	 * @param $oldOrderStatus
	 * @param $oldPaymentStatus
	 */
	public static function dispatchStatusChangeEvent(ORDER $order, $oldOrderStatus, $oldPaymentStatus)
	{
		if ($order->getStatus() != $oldOrderStatus || $order->getPaymentStatus() != $oldPaymentStatus)
		{
			/**
			 * Handle on status change event
			 */
			$event = new Events_OrderEvent(Events_OrderEvent::ON_STATUS_CHANGE);

			$event
				->setOrder($order)
				->setData('oldOrderStatus', $oldPaymentStatus)
				->setData('oldPaymentStatus', $oldPaymentStatus)
				->setData('newOrderStatus', $order->getStatus())
				->setData('newPaymentStatus', $order->getPaymentStatus());

			Events_EventHandler::handle($event);
		}
	}
}

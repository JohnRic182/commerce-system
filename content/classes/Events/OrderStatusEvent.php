<?php

class Events_OrderStatusEvent extends Events_OrderEvent
{
	const ON_PROCESS_PAYMENT = 'on-process-payment';

	/**
	 * Set order status
	 *
	 * @param string $orderStatus
	 *
	 * @return Events_OrderStatusEvent
	 */
	public function setOrderStatus($orderStatus)
	{
		$this->setData('orderStatus', $orderStatus);
		return $this;
	}

	/**
	 * Returns order status
	 *
	 * @return string
	 */
	public function getOrderStatus()
	{
		return $this->getData('orderStatus');
	}

	/**
	 * Set payment status
	 *
	 * @param string $paymentStatus
	 *
	 * @return Events_OrderStatusEvent
	 */
	public function setPaymentStatus($paymentStatus)
	{
		$this->setData('paymentStatus', $paymentStatus);
		return $this;
	}

	/**
	 * Returns payment status
	 *
	 * @return string
	 */
	public function getPaymentStatus()
	{
		return $this->getData('paymentStatus');
	}
}
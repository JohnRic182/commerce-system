<?php
/**
 * Class Events_OpcEvent
 */
class Events_OpcEvent extends Events_Event
{
	const ON_LOGIN = 'on-opc-login';
	const ON_SAVE_BILLING_DATA = 'on-opc-save-billing-data';
	const ON_SAVE_SHIPPING_ADDRESS = 'on-opc-save-shipping-address';
	const ON_GET_SHIPPING_METHODS = 'on-opc-get-shipping-methods';
	const ON_SET_SHIPPING_METHOD = 'on-opc-set-shipping-method';
	const ON_CHECK_PROMO_CODE = 'on-opc-check-promo-code';
	const ON_APPLY_GIFT_CERTIFICATE = 'on-opc-apply-git-certificate';
	const ON_GET_PAYMENT_FORM = 'on-opc-get-payment-form';
	const ON_PROCESS_PAYMENT = 'on-opc-process-payment';

	/**
	 * @var OPC_Response
	 */
	protected $response = null;

	/**
	 * @param OPC_Response $response
	 */
	public function setOpcResponse(OPC_Response $response)
	{
		$this->response = $response;
	}

	/**
	 * @return OPC_Response
	 */
	public function getOpcResponse()
	{
		return $this->response;
	}
}
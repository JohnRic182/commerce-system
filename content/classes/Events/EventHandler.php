<?php
/**
 * Class Events_EventHandler
 */
class Events_EventHandler
{
	const PRIORITY_LOW = 100;
	const PRIORITY_MEDIUM = 50;
	const PRIORITY_HIGH = 1;

	/**
	 * @var array Contains events listeners
	 */
	protected static $listeners = array();

	/**
	 * @var array Contains listeners that were sorter
	 */
	protected static $sorted = array();

	/**
	 * Register event listener
	 *
	 * @param $eventName
	 * @param $eventListener
	 * @param int $eventPriority
	 */
	public static function registerListener($eventName, $eventListener, $eventPriority = Events_EventHandler::PRIORITY_MEDIUM)
	{
		self::$listeners[$eventName][$eventPriority][] = $eventListener;
	}

	/**
	 * Handle events
	 *
	 * @param Events_Event $event
	 *
	 * @return Events_Event
	 */
	public static function handle(Events_Event $event)
	{
		$eventName = $event->getEventName();

		if (!isset(self::$listeners[$eventName])) return $event;
		
		if (!isset(self::$sorted[$eventName]))
		{
			ksort(self::$listeners[$eventName]);
			self::$sorted[$eventName] = true;
		}
		
		foreach (self::$listeners[$eventName] as $priorities)
		{
			foreach ($priorities as $listener)
			{
				try
				{
					call_user_func_array($listener, array($event));

					if ($event->propagationIsStopped())
					{
						return $event;
					}
				}
				catch (Exception $e)
				{
					if (defined('DEVMODE') && DEVMODE)
					{
						@file_put_contents(
							dirname(dirname(dirname(__FILE__))).'/cache/log/events.txt',
							date("Y/m/d-H:i:s")." - ".$e->getMessage()."\n\n\n", 
							FILE_APPEND
						);
					}
				}
			}
		}

		return $event;
	}
}
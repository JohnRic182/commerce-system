<?php

class Events_FulfillmentEvent extends Events_Event
{
	const ON_COMPLETED = 'on-fulfillment-completed';

	/** @var Model_Fulfillment */
	protected $fulfillment;
	/** @var  ORDER */
	protected $order;

	public function setFulfillment(Model_Fulfillment $fulfillment)
	{
		$this->fulfillment = $fulfillment;
	}
	public function getFulfillment()
	{
		return $this->fulfillment;
	}

	public function setOrder(ORDER $order)
	{
		$this->order = $order;
	}
	public function getOrder()
	{
		return $this->order;
	}
}
<?php

	if ($registry === null)
	{
		$registry = new Framework_Registry();
		$registry->set('db', $db);
		$registry->setParameter('settings', $settings);
	}

	$request = Framework_Request::createFromGlobals();
	$registry->set('request', $request);

	if (isset($settings['AvalaraEnableTax']) && $settings['AvalaraEnableTax'] == 'Yes')
	{
		Events_EventHandler::registerListener(
			Events_OrderEvent::ON_STATUS_CHANGE,
			array('Tax_Avalara_AvalaraListener', 'onOrderStatusChange')
		);

		Events_EventHandler::registerListener(
			Events_OrderEvent::ON_DELETED,
			array('Tax_Avalara_AvalaraListener', 'onOrderDeleted')
		);
	}
	else if (isset($settings['ExactorEnableTax']) && $settings['ExactorEnableTax'] == 'Yes')
	{
		Events_EventHandler::registerListener(
			Events_OrderEvent::ON_STATUS_CHANGE,
			array('Tax_Exactor_ExactorListener', 'onOrderStatusChange')
		);

		Events_EventHandler::registerListener(
			Events_OrderEvent::ON_DELETED,
			array('Tax_Exactor_ExactorListener', 'onOrderDeleted')
		);
	}

	Events_EventHandler::registerListener(
		Events_FulfillmentEvent::ON_COMPLETED,
		array('Listener_GiftCertificate', 'onFulfillmentCompleted')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_STATUS_CHANGE,
		array('Listener_StockControl', 'onOrderStatusChange')
	);

	if ($settings['InventoryStockUpdateAt'] == 'Add To Cart')
	{
		Events_EventHandler::registerListener(
			Events_OrderEvent::ON_ADD_ITEM,
			array('Listener_StockControl', 'checkItemStockLevels')
		);
	}
	else if ($settings['InventoryStockUpdateAt'] == 'Order Placed' || $settings['InventoryStockUpdateAt'] == 'Add To Cart')
	{
		Events_EventHandler::registerListener(
			Events_OrderEvent::ON_BEFORE_PAYMENT,
			array('Listener_StockControl', 'onOrderStatusChange')
		);
		if ($settings['InventoryStockUpdateAt'] == 'Order Placed')
		{
			Events_EventHandler::registerListener(
				Events_OrderEvent::ON_FAILED_PAYMENT,
				array('Listener_StockControl', 'onOrderStatusChange')
			);
		}
	}

	Events_EventHandler::registerListener(
		FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_PAYMENT_METHOD_DATA_SET,
		array('FraudService_Events_Listener_FraudServiceListener', 'setPaymentDataForFraudServiceTransaction')
	);

	Events_EventHandler::registerListener(
		FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_ORDER_CHECK,
		array('FraudService_Events_Listener_FraudServiceListener', 'performCheckOrder')
	);

	Events_EventHandler::registerListener(
		FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_REVIEW_ACTION,
		array('FraudService_Events_Listener_FraudServiceListener', 'reviewStatusAction')
	);

	Events_EventHandler::registerListener(
		FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_REJECT_ACTION,
		array('FraudService_Events_Listener_FraudServiceListener', 'rejectStatusAction')
	);

	Events_EventHandler::registerListener(
		FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_APPROVE_ORDER_ACTION,
		array('FraudService_Events_Listener_FraudServiceListener', 'approveOrderAction')
	);

	Events_EventHandler::registerListener(
		FraudService_Events_FraudEvent::ON_FRAUD_SERVICE_DENY_ORDER_ACTION,
		array('FraudService_Events_Listener_FraudServiceListener', 'denyOrderAction')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_UPDATE_ITEMS,
		array('Listener_StockControl', 'checkItemStockLevels')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_REMOVE_ITEM,
		array('Listener_StockControl', 'checkItemStockLevels')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_STATUS_CHANGE,
		array('Listener_ProductLocations', 'onOrderStatusChange')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_ADD_ITEM,
		array('Listener_QuantityDiscounts', 'checkQuantityDiscounts')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_UPDATE_ITEMS,
		array('Listener_QuantityDiscounts', 'checkQuantityDiscounts')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_REMOVE_ITEM,
		array('Listener_QuantityDiscounts', 'checkQuantityDiscounts')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_ADD_ITEM,
		array('Listener_ProductGifts', 'checkFreeProducts')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_UPDATE_ITEMS,
		array('Listener_ProductGifts', 'checkFreeProducts')
	);

	Events_EventHandler::registerListener(
		Events_OrderEvent::ON_REMOVE_ITEM,
		array('Listener_ProductGifts', 'checkFreeProducts')
	);

	if (isset($settings['RecurringBillingEnabled']) && $settings['RecurringBillingEnabled'])
	{
		RecurringBilling_Plugin::init($registry);
	}

	PaymentProfiles_Plugin::init($registry);
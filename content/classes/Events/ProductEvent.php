<?php

/**
 * Class Events_ProductEvent
 */
class Events_ProductEvent extends Events_Event
{
	const ON_DELETED = 'on-product-deleted';
	const ON_UPDATED = 'on-product-updated';
}
<?php
/**
 * Class Events_OpcEvent
 */
class Events_CronEvent extends Events_Event
{
	const ON_EVERY_5MIN = 'on-cron-every-5min-event';
	const ON_EVERY_HOUR = 'on-cron-every-hour-event';
	const ON_EVERY_12HOURS = 'on-cron-every-12hours-event';
	const ON_EVERY_DAY = 'on-cron-every-day-event';
	const ON_EVERY_WEEK = 'on-cron-every-week-event';
	const ON_EVERY_2WEEKS = 'on-cron-every-week-event';
	const ON_EVERY_MONTH = 'on-cron-every-month-event';
}
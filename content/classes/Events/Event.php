<?php
/**
 * Class Events_Event
 */
class Events_Event
{
	/**
	 * @var string event name
	 */
	protected $eventName;

	/**
	 * @var array event data
	 */
	protected $data = array();

	/** @var array */
	protected $errors = array();

	/**
	 * @var bool propagation stopped
	 */
	protected $stopped = false;

	/**
	 * Class constructor
	 *
	 * @param $eventName
	 */
	public function __construct($eventName)
	{
		$this->setEventName($eventName);
	}

	/**
	 * Event name setter
	 *
	 * @param $eventName
	 *
	 * @return $this
	 */
	public function setEventName($eventName)
	{
		$this->eventName = $eventName;
		return $this;
	}

	/**
	 * Even name getter
	 * 
	 * @return string
	 */
	public function getEventName()
	{
		return $this->eventName;
	}

	/**
	 * Set event data
	 * 
	 * @param string $name
	 * @param mixed $value
	 * 
	 * @return Events_Event
	 */
	public function setData($name, $value)
	{
		$this->data[$name] = $value;
		return $this;
	}

	/**
	 * Get even data
	 *
	 * @param $name
	 * @param null $default
	 *
	 * @return null
	 */
	public function getData($name, $default = null)
	{
		return isset($this->data[$name]) ? $this->data[$name] : $default;
	}

	/**
	 * Stops event propagation
	 */
	public function stopPropagation($stopped)
	{
		$this->stopped = (bool) $stopped;
	}

	/**
	 * Returns propagation status
	 * 
	 * @return boolean
	 */
	public function propagationIsStopped()
	{
		return $this->stopped;
	}

	/**
	 * Set error
	 *
	 * @param $errorMessage
	 */
	public function setError($errorMessage)
	{
		$this->errors[] = $errorMessage;
	}

	/**
	 * Get error
	 *
	 * @return array
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Check is there error in event
	 *
	 * @return bool
	 */
	public function hasErrors()
	{
		return count($this->errors) > 0;
	}

	public function clearErrors()
	{
		$this->errors = array();
	}
}
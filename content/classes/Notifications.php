<?php

/**
 * Class Notifications
 */
class Notifications
{
	// Used parts from phpmailer library
	// details on http://phpmailer.codeworxtech.com/

	/**
	 * Send mail
	 *
	 * @param $email
	 * @param $subject
	 * @param $message
	 * @param $from
	 * @return bool
	 * @throws Exception
	 */
	public static function sendMail($email, $subject, $message, $from)
	{
		global $settings;

		$from_header_name = stripslashes($settings["GlobalSiteName"]);
		$from_header_email = $settings["GlobalNotificationEmail"];

		$eol_character = self::getMailHeaderEolCharacter();

		// if it is an array, we have a name and email address
		if (is_array($from))
		{
			if (!array_key_exists('email', $from) || !array_key_exists('name', $from))
				throw new Exception('Expecting keys "name" and "email"');

			$from_header_name = $from['name'];
			$from_header_email = $from['email'];

		} // if it is a string, this should only be an email address, mark name as blank
		else if (trim($from) != '')
		{
			$from_header_name = '';
			$from_header_email = $from;
		}
		$message_id = time() .'-' . md5($from_header_email . $from_header_name) . '@' . $_SERVER['SERVER_NAME'];
		if ($settings["EmailSendmailEngine"] == "mail")
		{
			$headers = "Content-type: text/html; charset=utf-8" . $eol_character;
			$headers .= "From: \"" . $from_header_name . "\"<" . $from_header_email . ">" . $eol_character;
			$headers .= "Reply-To: \"" . $from_header_name . "\"<" . $from_header_email . ">" . $eol_character;
			$headers .= "Return-Path: " . $settings["GlobalNotificationEmail"] . $eol_character;
			$headers .= "Organization: " . $settings["CompanyName"] . $eol_character;
			$headers .= "MIME-Version: 1.0" . $eol_character;
			$headers .= "Content-Transfer-Encoding: binary" . $eol_character;
			$headers .= "X-Priority: 3" . $eol_character;
			$headers .= "X-Mailer: PHP/". phpversion() . $eol_character;
			$headers .= "Message-ID: <" . $message_id . ">" ;

			try
			{
				return @mail($email, $subject, $message . $eol_character, $headers, "-f" . $from_header_email);
			}
			catch (Exception $e)
			{
				return false;
			}
		}
		else
		{
			try
			{
				$mail = new PHPMailer(true); // set to throw exception instead of error message output

				$mail->CharSet = 'utf-8';
				$mail->SetLanguage("en", "content/classes/");

				$mail->IsSMTP();

				$mail->SMTPSecure = $settings["EmailSMTPSecure"]; // sets the prefix to the servier
				$mail->Host = $settings["EmailSMTPServer"]; // sets GMAIL as the SMTP server
				$mail->Port = $settings["EmailSMTPPort"]; // set the SMTP port

				if ($settings["EmailSMTPLogin"] != "" && $settings["EmailSMTPPassword"] != "")
				{
					$mail->SMTPAuth = true;                  // enable SMTP authentication
					$mail->Username = $settings["EmailSMTPLogin"];  // GMAIL username
					$mail->Password = $settings["EmailSMTPPassword"];            // GMAIL password
				}

				$from = is_array($from) ? $from["email"] : $from;

				$mail->From = $from_header_email;
				$mail->FromName = $from_header_name;
				$mail->Subject = $subject;
				$mail->Body = $message;

				$mail->Encoding = "base64";

				//$mail->AltBody    = "This is the body when user views in plain text format"; //Text Body
				//$mail->WordWrap   = 50; // set word wrap

				$mail->AddAddress($email, $email);
				$mail->AddReplyTo($from_header_email, $from_header_name);
				$mail->IsHTML(true); // send as HTML

				return @$mail->Send();
			}
			catch (Exception $e)
			{
				return false;
			}
			catch (phpmailerException $pe)
			{
				return false;
			}
		}
	}

	/**
	 * Returns the literal end of line (EOL) character set in the admin area to use for mail headers.
	 *
	 * @return string The end of line literal as set in the admin area
	 */
	public static function getMailHeaderEolCharacter()
	{
		$settings = settings();

		switch ($settings['EmailSendmailEolCharacter'])
		{
			case "\r": $eolCharacter = "\r"; break;
			case "\n": $eolCharacter = "\n"; break;
			default: $eolCharacter = "\r\n"; break;
		}

		return $eolCharacter;
	}

	/**
	 * @param $admin
	 * @param $new_password
	 * @throws Exception
	 */
	public static function emailAdminNewPassword($admin, $new_password)
	{
		global $settings;

		view()->assign('admin', $admin);
		view()->assign('new_password', $new_password);
		view()->assign('EmailTitle', 'Password Changed');

		$message_html = view()->fetch("templates/emails/admin_password_changed_html.html");

		@set_time_limit(300);

		//send email to user
		self::sendMail($admin['email'], stripslashes($settings["CompanyName"] . ": Password Changed"), $message_html, "");
	}

	/**
	 * @param $db
	 * @param $formFields
	 * @throws Exception
	 */
	public static function emailCustomFormSubmitted(&$db, $formData, $formFields)
	{
		global $msg;

		$result = $db->selectAll('SELECT email FROM ' . DB_PREFIX . 'admins WHERE active="Yes" AND receive_notifications LIKE "%custom_forms%"');

		if ($result)
		{
			view()->assign('form_data', $formData);
			view()->assign('form_fields', $formFields);

			$message_html = view()->fetch('templates/emails/custom_form_submitted_html.html');

			$message_subject = trim($formData['email_notification_subject']) == '' ? $msg['email']['subject_custom_form_submitted'] . ' (' . gs($formData['title']) . ')' : $formData['email_notification_subject'];

			foreach ($result as $admin)
			{
				self::sendMail($admin['email'], stripslashes($message_subject), $message_html, '');
			}
		}
	}

	/**
	 * Send email on new user registered
	 *
	 * @param $db
	 * @param $uid
	 * @param $customFields
	 */
	public static function emailNewUserRegistered(&$db, $uid, &$customFields)
	{
		global $settings;
		global $msg;
		$db->query("
			SELECT " . DB_PREFIX . "users.*, " . DB_PREFIX . "countries.name AS country_name
			FROM " . DB_PREFIX . "users
			LEFT JOIN " . DB_PREFIX . "countries ON " . DB_PREFIX . "users.country = " . DB_PREFIX . "countries.coid
			WHERE uid='" . $uid . "' AND removed='No'
		");

		if ($db->moveNext())
		{
			$user_data = $db->col;

			$cf_billing = $customFields->getCustomFieldsValues("billing", $uid, 0, 0);
			$cf_account = $customFields->getCustomFieldsValues("account", $uid, 0, 0);
			$cf_signup = $customFields->getCustomFieldsValues("signup", $uid, 0, 0);

			view()->assign("user_data", $user_data);

			view()->assign("cf_billing", $cf_billing);
			view()->assign("cf_account", $cf_account);
			view()->assign("cf_signup", $cf_signup);

			view()->assign("EmailTitle", "Registration Details");

			$message_user = view()->fetch("templates/emails/signup_user_html.html");
			$message_html = view()->fetch("templates/emails/signup_admin_html.html");

			@set_time_limit(300);

			//send email to user
			self::sendMail($user_data["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_new_user_welcome"]), $message_user, "");

			//send to admins
			$db->query("SELECT * FROM " . DB_PREFIX . "admins WHERE active='Yes' AND (receive_notifications LIKE '%signup%')");
			while ($db->moveNext())
			{
				header("Pong: ping");
				self::sendMail($db->col["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_new_user_registered"]), $message_html, "");
				header("Ping: pong");
			}
		}
	}

	/**
	 * Send new product review email
	 *
	 * @param DB $db
	 * @param $reviewId
	 */
	public static function emailNewProductReview(DB $db, $reviewId)
	{
		global $settings;

		$reviewData = $db->selectOne('
			SELECT
				r.review_title,
				r.rating AS review_rating,
				p.pid,
				p.title AS product_name,
				u.uid AS user_id,
				u.login AS user_login,
				u.email AS user_email,
				CONCAT(u.fname, " ", u.lname) AS user_name
			FROM ' . DB_PREFIX . 'products_reviews AS r
			INNER JOIN ' . DB_PREFIX . 'products AS p ON r.pid = p.pid
			INNER JOIN ' . DB_PREFIX . 'users AS u ON r.user_id = u.uid
			WHERE r.id=' . intval($reviewId)
		);

		if ($reviewData)
		{
			view()->assign('review_data', $reviewData);

			$message_html = view()->fetch('templates/emails/admin_product_review_html.html');

			@set_time_limit(300);

			//send to admins
			$db->query('SELECT * FROM ' . DB_PREFIX . 'admins WHERE active="Yes" AND (receive_notifications LIKE "%product_review%")');

			while ($db->moveNext())
			{
				header('Pong: ping');

				self::sendMail($db->col['email'], stripslashes($settings['CompanyName'] . ': New product review for ' . $reviewData['product_name']), $message_html, '');

				header('Ping: pong');
			}
		}
	}

	/**
	 * Send password reset email
	 *
	 * @param $user_data
	 * @param $uid
	 * @param $new_password
	 */
	public static function emailUserResetPassword($user_data, $uid, $new_password)
	{
		global $settings;
		global $msg;
		view()->assign("EmailTitle", "Your New Password");
		view()->assign("fname", $user_data["fname"]);
		view()->assign("lname", $user_data["lname"]);
		view()->assign("username", $user_data["login"]);
		view()->assign("password", $new_password);
		$message = view()->fetch("templates/emails/reset_password_html.html");

		@set_time_limit(300);
		self::sendMail($user_data["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_new_password"]), $message, "");
	}

	/**
	 * Send out of stock email
	 *
	 * @param $db
	 * @param $pid
	 * @param $new_stock
	 * @param $inventory_id
	 */
	public static function emailOutOfStock(&$db, $pid, $new_stock, $inventory_id = 0)
	{
		global $settings;
		global $msg;
		$db->query("SELECT * FROM " . DB_PREFIX . "products WHERE pid='" . $pid . "'");
		if ($db->moveNext())
		{
			$product = $db->col;
			$product_attributes_stock = false;
			if ($inventory_id > 0)
			{
				$db->query("SELECT * FROM " . DB_PREFIX . "products_inventory WHERE pid='" . $pid . "' AND pi_id='" . $inventory_id . "'");
				if ($db->moveNext())
				{
					$product_attributes_stock = $db->col;
				}
			}
			view()->assign("EmailTitle", "Product Out Of Stock: " . htmlspecialchars($product["title"] . ($product_attributes_stock ? (" - " . str_replace("\n", ",", $product_attributes_stock["attributes_list"])) : "")));
			view()->assign("product", $product);
			view()->assign("new_stock", $new_stock);
			view()->assign("product_attributes_stock", $product_attributes_stock);
			$message_html = view()->fetch("templates/emails/outofstock_admin_html.html");

			//send to admins
			$db->query("SELECT * FROM " . DB_PREFIX . "admins WHERE active='Yes' AND (receive_notifications LIKE '%outofstock%')");
			while ($db->moveNext())
			{
				header("Pong: ping");
				self::sendMail($db->col["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_out_of_stock"] . " (" . gs($product["title"]) . ")"), $message_html, "");
				header("Ping: pong");
			}
		}
	}

	/**
	 * Send order received email
	 *
	 * @param DB $db
	 * @param ORDER $order
	 * @param USER $user
	 * @param $customFields
	 * @param $payment_status
	 *
	 * @return bool
	 */
	public static function emailOrderReceived(DB $db, ORDER $order, USER $user, &$customFields, $payment_status)
	{
		global $settings, $msg;

		if ($order->paymentGatewayId == 'bongocheckout') return true;

		view()->assign("EmailTitle", "Order Details");

		$order->getOrderData();
		view()->assign("order", $order);

		$orderViewModel = new View_OrderLinesViewModel($settings, $msg);
		$orderViewModel->build($order, false);

		view()->assign('orderView', $orderViewModel->asArray());

		$user_data = $user->getUserData();
		$order_items = $order->getOrderItemsExtended($msg);
		$shipping_address = $order->getShippingAddress();

		view()->assign("user_data", $user_data);
		view()->assign("billing_address", $user_data);
		view()->assign("shipping_address", $shipping_address);
		view()->assign("order_items", $order_items);

		// TODO: get from registry
		$settingsRepository = new DataAccess_SettingsRepository($db, $settings);

		$shipments = new DataAccess_OrderShipmentRepository($db, $settingsRepository);
		$shipmentsView = new View_OrderShipmentsViewModel($settings, $msg);
		view()->assign('shipments', $shipmentsView->getShipmentsView($shipments->getShipmentsByOrderId($order->getId())));

		$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($db, $settingsRepository);
		$paymentMethod = $paymentMethodRepository->getById($order->getPaymentMethodId());

		if (!is_null($paymentMethod))
		{
			view()->assign('payment_method_thankyou', $paymentMethod->getMessageThankYou());

			if ($order->paymentIsRealtime == 'Yes')
			{
				view()->assign('payment_realtime', 'yes');
				view()->assign('payment_method_name', $paymentMethod->getType() . ' (' . $paymentMethod->getName() . ')');
			}
			else
			{
				view()->assign('payment_realtime', 'no');
				view()->assign('payment_method_name', $paymentMethod->getName());
			}
		}

		$cf_billing = $customFields->getCustomFieldsValues("billing", $user->id, 0, 0);
		$cf_shipping = $customFields->getCustomFieldsValues("shipping", $user->id, $order->oid, 0);
		$cf_invoice = $customFields->getCustomFieldsValues("invoice", $user->id, $order->oid, 0);

		view()->assign("cf_billing", $cf_billing);
		view()->assign("cf_shipping", $cf_shipping);
		view()->assign("cf_invoice", $cf_invoice);

		view()->assign("payment_status", $payment_status);

		$contentDir = dirname(dirname(__FILE__));
		if (!class_exists('Payment_Provider'))
		{
			require_once $contentDir . '/classes/Payment/Provider.php';
		}

		// TODO: get from registry
		$paymentProvider = new Payment_Provider(new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings)));
		$pp = $paymentProvider->getPaymentProcessor($order);

		$transaction = false;
		if ($pp)
		{
			$transaction = $pp->getTransactionDetails($order);
		}
		view()->assign('payment_transaction', $transaction);

		view()->assign('status_date', $order->status_date_formatted);

		$message_user = view()->fetch("templates/emails/order_received_user_html.html");
		$message_html = view()->fetch("templates/emails/order_received_admin_html.html");

		@set_time_limit(300);

		//send email to users
		self::sendMail($user->data["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_your_order"] . " - " . $order->order_num), $message_user, "");

		//send to admins
		$db->query("SELECT * FROM " . DB_PREFIX . "admins WHERE active='Yes' AND (receive_notifications LIKE '%invoice%')");
		while ($db->moveNext())
		{
			self::sendMail($db->col["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_new_order"] . " - " . $order->order_num), $message_html, "");
		}
	}

	/**
	 * Send order completed email$orderViewModel
	 *
	 * @param $db
	 * @param ORDER $order
	 *
	 * @return bool
	 */
	public static function emailOrderCompleted(&$db, ORDER $order)
	{
		global $settings;
		global $msg;

		$db->query("
			SELECT
				" . DB_PREFIX . "orders.*,
				shipping_countries.name AS shipping_country_name
			FROM " . DB_PREFIX . "orders
			LEFT JOIN " . DB_PREFIX . "countries AS shipping_countries ON " . DB_PREFIX . "orders.shipping_country = shipping_countries.coid
			WHERE oid=" . intval($order->getId())
		);

		if ($db->moveNext())
		{
			$order_data = $db->col;

			if ($order_data['payment_method_id'] == 'bongocheckout') return true;

			$db->query("SELECT * FROM " . DB_PREFIX . "orders_content WHERE oid=" . intval($order->getId()));
			$order_items = $db->getRecords();

			for ($i = 0; $i < count($order_items); $i++)
			{
				if ($order_items[$i]["product_sub_id"] != "")
				{
					$order_items[$i]["product_id"] = $order_items[$i]["product_id"] . " / " . $order_items[$i]["product_sub_id"];
				}
			}

			$db->query("
				SELECT
					" . DB_PREFIX . "users.*,
					billing_countries.name AS country_name
				FROM " . DB_PREFIX . "users
				LEFT JOIN " . DB_PREFIX . "countries AS billing_countries ON " . DB_PREFIX . "users.country = billing_countries.coid
				WHERE uid='" . intval($order_data["uid"]) . "'"
			);

			if ($db->moveNext())
			{
				$user_data = $db->col;

				$orderViewModel = new View_OrderLinesViewModel($settings, $msg);
				$orderViewModel->build($order, false, true);

				view()->assign('orderView', $orderViewModel->asArray());

				view()->assign("EmailTitle", "Order Completed");

				$order_data["shipping_tracking"] = $order_data["shipping_tracking"] != "" ? unserialize($order_data["shipping_tracking"]) : false;

				view()->assign("order_data", $order_data);
				view()->assign("order_items", $order_items);
				view()->assign("user_data", $user_data);

				// TODO: get from registry
				$settingsRepository = new DataAccess_SettingsRepository($db, $settings);

				$shipments = new DataAccess_OrderShipmentRepository($db, $settingsRepository);
				$shipmentsView = new View_OrderShipmentsViewModel($settings, $msg);
				view()->assign('shipments', $shipmentsView->getShipmentsView($shipments->getShipmentsByOrderId($order->getId())));

				// credit to ...zone members
				$currencies = new Currencies($db);
				$currencies->getCurrentCurrency();

				if (!isset($_SESSION['default_currency']))
				{
					$_SESSION['default_currency'] = $currencies->getDefaultCurrency();
				}

				$message_user = view()->fetch("templates/emails/order_completed_html.html");
				self::sendMail($user_data["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_order_completed"] . " - " . $order_data["order_num"]), $message_user, "");
			}
		}
		return true;
	}


	/**
	 * Send order shipped email
	 *
	 * @param $db
	 * @param ORDER $order
	 * @param Model_Fulfillment $fulfillment
	 *
	 * @return bool
	 */
	public static function emailOrderShipped(&$db, ORDER $order, Model_Fulfillment $fulfillment)
	{
		global $settings;
		global $msg;

		$db->query("
			SELECT
				" . DB_PREFIX . "orders.*,
				shipping_countries.name AS shipping_country_name
			FROM " . DB_PREFIX . "orders
			LEFT JOIN " . DB_PREFIX . "countries AS shipping_countries ON " . DB_PREFIX . "orders.shipping_country = shipping_countries.coid
			WHERE oid=" . intval($order->getId())
		);

		if ($db->moveNext())
		{
			$order_data = $db->col;

			if ($order_data['payment_method_id'] == 'bongocheckout') return true;

			$db->query("SELECT * FROM " . DB_PREFIX . "orders_content WHERE oid=" . intval($order->getId()));
			$order_items = $db->getRecords();

			for ($i = 0; $i < count($order_items); $i++)
			{
				if ($order_items[$i]["product_sub_id"] != "")
				{
					$order_items[$i]["product_id"] = $order_items[$i]["product_id"] . " / " . $order_items[$i]["product_sub_id"];
				}
			}

			$db->query("
				SELECT
					" . DB_PREFIX . "users.*,
					billing_countries.name AS country_name
				FROM " . DB_PREFIX . "users
				LEFT JOIN " . DB_PREFIX . "countries AS billing_countries ON " . DB_PREFIX . "users.country = billing_countries.coid
				WHERE uid='" . intval($order_data["uid"]) . "'"
			);

			if ($db->moveNext())
			{
				$user_data = $db->col;

				$orderViewModel = new View_OrderLinesViewModel($settings, $msg);

				$lineItems = $order->lineItems;

				$fulfillmentItems = $fulfillment->getItems();
				$order_items2 = array();
				$fulfillmentLineItems = array();

				foreach ($order_items as $order_item)
				{
					$lineItemId = $order_item['ocid'];
					/** @var Model_FulfillmentItem $fulfillmentItem */
					$fulfillmentItem = isset($fulfillmentItems[$lineItemId]) ? $fulfillmentItems[$lineItemId] : false;

					if ($fulfillmentItem && isset($lineItems[$lineItemId]))
					{
						/** @var Model_LineItem $oldLineItem */
						$oldLineItem = $lineItems[$lineItemId];
						$oldArray = array_merge($oldLineItem->toArray());
						$newLineItem = new Model_LineItem($oldArray);
						$newLineItem->setFinalQuantity($fulfillmentItem->getQuantity());

						$fulfillmentLineItems[$lineItemId] = $newLineItem;

						$order_item['admin_quantity'] = $fulfillmentItem->getQuantity();
						$order_items2[] = $order_item;
					}
				}
				$order_items = $order_items2;
				$order->lineItems = $fulfillmentLineItems;
				$hasDigital = false;
				// check if there is a Digital order
				foreach ($order->lineItems as $item)
                {
					if ($item->getProductType() == Model_Product::DIGITAL)
					{
							$hasDigital = true;
					}
                }
				$order_data['shipping_tracking_number_type'] = $fulfillment->getTrackingCompany();
				$order_data['shipping_tracking_number'] = $fulfillment->getTrackingNumber();

				$orderViewModel->build($order, false, true);

				view()->assign('orderView', $orderViewModel->asArray());

				view()->assign("EmailTitle", $hasDigital == false ? "Your order is ready for shipping" : "Your digital order is ready for download");

				$order_data["shipping_tracking"] = $order_data["shipping_tracking"] != "" ? unserialize($order_data["shipping_tracking"]) : false;

				view()->assign("order_data", $order_data);
				view()->assign("order_items", $order_items);
				view()->assign("user_data", $user_data);

				// credit to ...zone members
				$currencies = new Currencies($db);
				$currencies->getCurrentCurrency();

				if (!isset($_SESSION['default_currency']))
				{
					$_SESSION['default_currency'] = $currencies->getDefaultCurrency();
				}

				$message_user = view()->fetch("templates/emails/order_shipped_html.html");

				self::sendMail($user_data["email"], stripslashes($settings["CompanyName"] . ": " . $msg["email"]["subject_order_shipped"] . " - " . $order_data["order_num"]), $message_user, "");

				$order->lineItems = $lineItems;
			}
		}
		return true;
	}

	/**
	 * Send email to a friend
	 *
	 * @param $db
	 * @param $yname
	 * @param $yemail
	 * @param $fname
	 * @param $femail
	 * @param $pid
	 * @param $msg
	 *
	 * @return bool
	 */
	public static function emailToFriend(&$db, $yname, $yemail, $fname, $femail, $pid, $msg)
	{
		global $settings;
		global $msg;

		if ($settings["CatalogEmailToFriend"] == "Disabled")
		{
			return false;
		}

		$_products = new ShoppingCartProducts($db, $settings);
		$_product = $_products->getProductById($pid, 0);

		view()->assign("EmailTitle", $msg["email"]["title_to_friend"]);
		view()->assign("product", $_product);
		view()->assign("user_name", $yname);
		view()->assign("user_email", $yemail);
		view()->assign("friend_name", $fname);
		view()->assign("friend_email", $femail);

		@set_time_limit(300);

		if ($settings["CatalogEmailToFriend"] == "HTML")
		{
			//send HTML email
			$message_html = view()->fetch("templates/emails/to_friend_html.html");
			self::sendMail($femail, sprintf($msg["email"]["subject_to_friend"], $fname, $yname), $message_html, array('name' => stripslashes($yname), 'email' => $yemail));
		}
	}

	/**
	 * Email wish list
	 *
	 * @param $db
	 * @param $mail_subject
	 * @param $your_email
	 * @param $uid
	 * @param $wishlist
	 */
	public static function emailWishlist(&$db, $mail_subject, $your_email, $uid, $wishlist)
	{
		global $settings;

		$db->query("
			SELECT fname, lname
			FROM " . DB_PREFIX . "users
			WHERE uid='" . intval($uid) . "'
		");

		if ($user = $db->moveNext())
		{
			view()->assign("fname", $user["fname"]);
			view()->assign("lname", $user["lname"]);
			view()->assign("EmailTitle", $mail_subject);
			view()->assign("wishlist", $wishlist);
			view()->assign("GlobalHttpUrl", $settings["GlobalHttpUrl"]);

			//assign smarty vars
			view()->assign("products", $wishlist);
			view()->assign("products_file", strtolower("catalog_thumb1.html"));

			@set_time_limit(300);

			//send HTML email
			$message_html = view()->fetch("templates/emails/wishlist_html.html");
			self::sendMail($your_email, sprintf($mail_subject, $user["fname"], $user["lname"]), $message_html, "");
		}
	}

	/**
	 * Email order note for a customer
	 *
	 * @param ORDER $order
	 * @param USER $user
	 * @param $emailSubject
	 * @param $emailMessage
	 */
	public static function emailOrderNoteForCustomer(ORDER $order, USER $user, $emailSubject, $emailMessage)
	{
		global $settings;

		$userData = $user->getUserData();

		view()->assign('order', $order);
		view()->assign('user', $user);
		view()->assign('emailMessage', $emailMessage);
		view()->assign("EmailTitle", $emailSubject);

		$message = view()->fetch('templates/emails/order_note_for_customer_html.html');
		self::sendMail($userData['email'], $settings['CompanyName'] . ': ' . $emailSubject, $message, '');
	}

	/**
	 * EMail notification of drop ship products
	 *
	 * @param $db
	 * @param $orderId
	 */
	public static function emailProductsLocationsNotify(&$db, $orderId)
	{
		global $settings;
		global $msg;

		if (isset($settings['ProductsLocationsSendOption']) && $settings['ProductsLocationsSendOption'] == 'Off')
			return;

		//get order data
		$order = $db->selectOne("
		SELECT
			orders.*,
			countries.name AS shipping_country_name,
			states.name AS shipping_state_name
		FROM ".DB_PREFIX."orders AS orders
		LEFT JOIN ".DB_PREFIX."countries AS countries ON orders.shipping_country = countries.coid
		LEFT JOIN ".DB_PREFIX."states AS states ON orders.shipping_state = states.stid
		WHERE orders.oid='".intval($orderId)."'
	");

		//get order content and locations
		$items = $db->selectAll("
		SELECT
			orders_content.ocid,
			orders_content.title,
			orders_content.product_id,
			orders_content.product_sub_id,
			orders_content.product_upc,
			orders_content.product_gtin,
			orders_content.product_mpn,
			orders_content.admin_quantity,
			orders_content.options,
			orders_content.shipment_id,
			products_locations.*
		FROM ".DB_PREFIX."orders_content AS orders_content
		INNER JOIN ".DB_PREFIX."products_locations AS products_locations
			ON products_locations.products_location_id = orders_content.products_location_id
		LEFT JOIN ".DB_PREFIX."countries AS countries ON products_locations.country = countries.coid
		LEFT JOIN ".DB_PREFIX."states AS states ON products_locations.state = states.stid
		WHERE
			orders_content.oid='".intval($orderId)."'
		ORDER BY
			products_locations.products_location_id,
			orders_content.shipment_id,
			orders_content.title
	");

		$locations = array();
		$locationsShipments = array();

		foreach ($items as $item)
		{
			$locations[$item['products_location_id']] = $item;

			$_shipment_id = $item['shipment_id'] ? $item['shipment_id'] : 0;
			$locationsShipments[$item['products_location_id']][$_shipment_id][$item['ocid']] = $item;
		}

		view()->assign('order', $order);

		$settings_repository = new DataAccess_SettingsRepository();
		$shipments_repository = new DataAccess_OrderShipmentRepository($db, $settings_repository);
		$shipments = $shipments_repository->getShipmentsByOrderId($orderId);
		view()->assign('shipments', $shipments);

		foreach ($locations as $location)
		{
			view()->assign('location', $location);
			view()->assign('location_shipments', $locationsShipments[$location['products_location_id']]);
			$email = view()->fetch("templates/emails/products_location_notification_html.html");
			self::sendMail($location["notify_email"], $settings["GlobalSiteName"] . ": " . $msg["email"]["new_shipment_notification"], $email, "");
		}
	}

	/**
	 * Send email to the customer when cart is abandon
	 *
	 * @param $db
	 * @param $orderRow
	 * @param $orderItemRows
	 * @param $userRow
	 * @param $wrappingContent
	 * @param $subject
	 */
	public static function emailCartAbandoned($db, $orderRow, $orderItemRows, $userRow, $wrappingContent, $subject)
	{
		global $settings;
		global $msg;

		view()->assign('user_data', $userRow);
		view()->assign('order_items', $orderItemRows);

		$itemsInCartContent = view()->fetch("templates/emails/drift_email_abandoned_cart_contents_html.html");
		$message = $wrappingContent;
		$message = str_replace("{\$ItemsInCart}", $itemsInCartContent, $message);
		$message = str_replace("{\$CartLink}", $settings['GlobalHttpUrl'] . '/?oa=RestoreCart&id=' . $orderRow['security_id'], $message);
		self::sendMail($userRow["email"], $subject, $message, '');
	}

	/**
	 * @param $gift_data
	 * @throws Exception
	 */
	public static function emailGiftCertificate($gift_data)
	{
		// TODO: remove global dependency here
		global $settings;
		global $msg;

		view()->assign('voucher', $gift_data['voucher']);
		view()->assign('gift_amount', $gift_data['gift_amount']);
		view()->assign('first_name', $gift_data['first_name']);
		view()->assign('last_name', $gift_data['last_name']);
		view()->assign('from_name', $gift_data['from_name']);
		view()->assign('message', $gift_data['message']);

		$message_user = view()->fetch('templates/emails/gift_cert_html.html');

		set_time_limit(300);
		//send email to user
		self::sendMail(
			$gift_data['email'],
			$settings['GlobalSiteName'] . ': ' . $msg['email']['subject_new_gift_certificate'],
			$message_user, ''
		);
	}
}

<?php

/**
 * Wrapper for openssl libraries
 *
 * @author DDM
 */
class openSSL{
	
	public $private_key_bits;
	public $private_key_type;
	public $csr_expiration_days;
	
	public $csr_pem;
	public $private_key_pem;
	
	public $config_path = "";
	
	/**
	 * class contructor
	 *
	 * @param int $private_key_bits
	 * @param int $private_key_type
	 * @param int $csr_expiration_days
	 * @return openSSL
	 */
	public function openSSL ($private_key_bits = 4096, $private_key_type = OPENSSL_KEYTYPE_RSA, $csr_expiration_days = 3650, $config_path = "")
	{
		$this->private_key_bits = $private_key_bits;
		$this->private_key_type = $private_key_type;
		$this->csr_expiration_days = $csr_expiration_days;
		$this->config_path = $config_path;
		$openssl_conf = getenv("OPENSSL_CONF");
		if (!$openssl_conf || $openssl_conf == "")
		{
			putenv("OPENSSL_CONF=".$this->config_path."");
		}
		return $this;
	}
	
	/**
	 * generates certificate and encripted private key
	 * saves result in $csr_pem and $private_key_pem class vars
	 *
	 * @param array $dn
	 * @param string $private_key_password
	 * @return bool
	 */
	public function generate($dn, $private_key_password)
	{
		set_time_limit(300);
		$config = array(
			"private_key_bits"=>$this->private_key_bits, 
			"private_key_type"=>$this->private_key_type
		);
		
		//set config
		$openssl_conf = getenv("OPENSSL_CONF");
		
		if (!$openssl_conf || $openssl_conf == "")
		{
			$config["config"] = $this->config_path;
		}

		//try to generate
		$private_key_resource = @openssl_pkey_new($config);
		if (!$private_key_resource)
		{
			//try other way with predefined config
			$config["config"] = $this->config_path;
			$private_key_resource = @openssl_pkey_new($config);
		}

		if ($private_key_resource)
		{
			$csr_resource = @openssl_csr_new($dn, $private_key_resource, $config);
			$csr_signed = @openssl_csr_sign($csr_resource, null, $private_key_resource, $this->csr_expiration_days, $config); // 10 years
			@openssl_x509_export($csr_signed, $this->csr_pem);
			@openssl_pkey_export($private_key_resource, $this->private_key_pem, $private_key_password, $config);
			$this->csr_pem == "" || $this->private_key_pem == "" ? false : true;
			return $this->csr_pem;
		}
		return false;
	}
	
	/**
	 * crypt data uses public key extracted from certificate
	 *
	 * @param string $data_decrypted
	 * @param string or false $csr_pem
	 * @return string
	 */
	public function crypt($data_decrypted, $csr_pem = false)
	{
		$public_key_resource = @openssl_get_publickey($csr_pem ? $csr_pem : $this->csr_pem);
		$data_crypted = "";
		@openssl_public_encrypt($data_decrypted, $data_crypted, $public_key_resource);
		return $data_crypted;
	}
	
	/**
	 * decrypt data uses private key with password
	 *
	 * @param string $data_crypted
	 * @param string $private_key_password
	 * @param string or false $private_key_pem
	 * @return string
	 */
	public function decrypt($data_crypted, $private_key_password, $private_key_pem = false)
	{
		$private_key_resource = @openssl_get_privatekey($private_key_pem ? $private_key_pem : $this->private_key_pem, $private_key_password);
		@openssl_private_decrypt($data_crypted, $data_decrypted, $private_key_resource);
		//while ($msg = openssl_error_string())
	    //echo $msg . "<br />\n";
		return $data_decrypted;
	}
}


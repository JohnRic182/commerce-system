<?php

abstract class Cache
{
	public $resyncCache = false;

	/**
	 * [getResyncCache description]
	 * @return [type]
	 */
	public function getResyncCache()
	{
		return $this->resyncCache;
	}

	/**
	 * [setResyncCache description]
	 * @param [type] $resyncCache [description]
	 */
	public function setResyncCache($resyncCache)
	{
		$this->resyncCache = $resyncCache;
	}

	/**
	 * Retrivieve cached data
	 * @param string $id cache id
	 * @param integet $cacheLifeTime in seconds
	 * @param [type] $default default value for cached data
	 * @return mixed
	 */
	public abstract function getCachedData($id, $cacheLifeTime, $default = null);

	/**
	 * Store data into cache
	 * @param string $id cache id
	 * @param mixed $data data to be cached
	 */
	public abstract function cacheData($id, $data);

}
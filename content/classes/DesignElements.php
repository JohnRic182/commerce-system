<?php

class DesignElements
{
	public static function getImage($elementId)
	{
		global $db;

		$db->query("SELECT * FROM ".DB_PREFIX."design_elements WHERE group_id='image' and element_id = '".$db->escape($elementId)."'");
		if($db->moveNext())
		{
			return $db->col['content'];
		}

		return null;
	}
}
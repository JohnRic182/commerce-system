<?php
/**
 * @class DB
 */
class DB
{
	protected $host;
	protected $user;
	protected $password;
	protected $dbName;

	protected $cacheDir = 'content/cache/tmp/';

	/**
	 * @var PDO $pdo
	 */
	protected $pdo = null;

	/**
	 * @var PDOStatement $result
	 */
	protected $result = null;

	public $query;
	public $fields;

	public $col;

	/**
	 * DB constructor.
	 * @param null $host
	 * @param null $user
	 * @param null $password
	 * @param null $dbName
	 */
	public function __construct($host = null, $user = null, $password = null, $dbName = null)
	{
		if ($host !== null && $user !== null && $password !== null && $dbName !== null)
		{
			$this->init($host, $user, $password, $dbName);
		}
	}

	/**
	 * Creates a new database connection
	 *
	 * @param $host
	 * @param $user
	 * @param $password
	 * @param $dbName
	 * @return bool
	 */
	public function init($host, $user, $password, $dbName)
	{
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		$this->dbName = $dbName;
		$this->fields = array();

		if (defined('DEVMODE') && DEVMODE)
		{
			if (!defined('__DIR__')) define('__DIR__', dirname(__FILE__));
			@file_put_contents(dirname(__DIR__) . '/cache/log/mysql-page-sql.txt', "PAGE SQL:\n");
		}

		return $this->reconnect();
	}

	/**
	 * (Re)connect to a database
	 */
	public function reconnect()
	{
		try
		{
			$this->pdo = new PDO(
				'mysql:host='.$this->host.';dbname='.$this->dbName, $this->user, $this->password,
				array(
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
					PDO::ATTR_PERSISTENT => true,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
				)
			);

			return true;
		}
		catch (PDOException $e)
		{
			$this->error(
				'Can not connect to database. Please check database access information.',
				'Error code: '.$e->getCode()."\nError info: ".$e->getMessage()
			);
		}

		return false;
	}

	/**
	 * @return PDO
	 */
	public function &getConnection()
	{
		return $this->pdo;
	}

	/**
	 * Begin a transaction
	 *
	 * @return bool
	 */
	public function beginTransaction()
	{
		return $this->pdo->beginTransaction();
	}

	/**
	 * Rollback a transaction
	 *
	 * @return bool
	 */
	public function rollbackTransaction()
	{
		return $this->pdo->rollBack();
	}

	/**
	 * Commit a transaction
	 *
	 * @return bool
	 */
	public function commitTransaction()
	{
		return $this->pdo->commit();
	}

	/**
	 * Is the current connection in a transaction
	 *
	 * @return bool
	 */
	public function inTransaction()
	{
		return $this->pdo->inTransaction();
	}

	/**
	 * Show database error
	 *
	 * @param $msg
	 * @param string $details
	 */
	public function error($msg, $details = '')
	{
		echo
			'<html><body><div style="border:2px solid black;font-family:tahoma;font-size:16px;padding:20px;text-align:left;color:yellow;background-color:#CC0000;">'.
				nl2br(htmlspecialchars($msg)).
				(defined('DEVMODE') && DEVMODE ? '<br>Details:<br>'.nl2br(htmlspecialchars($details)) : '').
			'</div></body></html>';

		$this->log($msg."\n".$details);

		die();
	}

	/**
	 * @param $msg
	 */
	protected function log($msg)
	{
		if (!defined('__DIR__')) define('__DIR__', dirname(__FILE__));
		file_put_contents(dirname(__DIR__) . '/cache/log/mysql-failures.txt', date('r').' : '.$msg."\n\n", FILE_APPEND);
	}

	/**
	 * Assign value
	 *
	 * @param string $field
	 * @param mixed $value
	 */
	public function assign($field, $value)
	{
		$this->fields[$field] = $value == '' ? "''" : $value;
	}

	/**
	 * Assigns string value
	 *
	 * @param string $field
	 * @param mixed $value
	 */
	public function assignStr($field, $value)
	{
		$this->fields[$field] = "'".$this->escape($value)."'";
	}

	/**
	 * Escapes mysql string
	 *
	 * @param string $str
	 * @return string
	 */
	public function escape($str)
	{
		$result = $this->pdo->quote($str);
		return substr($result, 1, -1);
	}

	/**
	 * Resets fields for insert/update
	 */
	public function reset()
	{
		$this->fields = array();
	}

	/**
	 * Insert new value into table
	 *
	 * @param string $table
	 *
	 * @return int
	 */
	public function insert($table)
	{
		$f = '';
		$v = '';

		reset($this->fields);

		foreach ($this->fields as $field => $value)
		{
			$f .= ($f != '' ? ', ' : '').$field;
			$v .= ($v != '' ? ', ' : '').$value;
		}

		if (count($this->fields) > 0)
		{
			$this->query('INSERT INTO '.$table.' ('.$f.') VALUES ('.$v.')');
			return $this->insertId();
		}

		return null;
	}

	/**
	 * Updates database record
	 *
	 * @param $table
	 * @param $where
	 */
	public function update($table, $where)
	{
		$f = '';

		reset($this->fields);

		foreach ($this->fields as $field=>$value)
		{
			$f .= ($f != '' ? ', ' : '').$field.'='.$value;
		}

		if (count($this->fields) > 0)
		{
			$this->query('UPDATE '.$table.' SET '.$f.' '.$where);
		}
	}

	public function clearLastStatement()
	{
		$this->result = null;
	}

	/**
	 * Execute mysql query
	 *
	 * @param string $query
	 *
	 * @return mixed
	 */
	public function query($query)
	{
		$this->query = $query;

		if (defined('DEVMODE') && DEVMODE)
		{
			if (!defined('__DIR__')) define('__DIR__', dirname(__FILE__));
			@file_put_contents(dirname(__DIR__) . '/cache/log/mysql-page-sql.txt', $query . "\n\n\n", FILE_APPEND);
		}

		try
		{
			$this->result = @$this->pdo->query($query);

			return $this->result;
		}
		catch (PDOException $e)
		{
			$this->error(
				'Can not execute database query. ',
				'Error code: '.$e->getCode()."\nError info: ".$e->getMessage()."\nQuery: ".$query
			);
		}
		catch (Exception $e)
		{
			$this->error(
				'Can not connect to database. .',
				'Error code: '.$e->getCode()."\nError info: ".$e->getMessage()."\nQuery: ".$query
			);
		}
	}

	/**
	 * @return bool
	 */
	public function ping()
	{
		try
		{
			@$this->pdo->query('SELECT 1');
		}
		catch (PDOException $e)
		{
			return false;
		}
		catch (Exception $e)
		{
			return false;
		}

		return true;
	}

	/**
	 * Select all records or false
	 *
	 * @param $query
	 * @return mixed
	 */
	public function selectAll($query)
	{
		return $this->getRecords($this->query($query));
	}

	/**
	 * Select all records / cached
	 *
	 * @param string $query
	 * @param int $cacheLifeTime in seconds
	 *
	 * @return mixed
	 */
	public function selectAllCached($query, $cacheLifeTime = 3600)
	{
		$hash = md5($query);
		$filename = $this->cacheDir.$hash.'.cache';

		if (is_file($filename) && filemtime($filename) + $cacheLifeTime > time())
		{
			$f = @fopen($filename, "rb");
			if ($f)
			{
				flock($f, LOCK_SH);
				$data = @unserialize(@fread($f, filesize($filename)));
				flock($f, LOCK_UN);
				@fclose($f);

				return $data ? $data : array();
			}
		}

		$this->query($query);
		$data = $this->getRecords();

		$f = @fopen($filename, "wb");
		if ($f && @flock($f, LOCK_EX) && @fwrite($f, serialize($data)))
		{
			flock($f, LOCK_UN);
			@fclose($f);
		}
		else
		{
			if ($f)
			{
				flock($f, LOCK_UN);
				@fclose($f);
			}

			unlink($filename);
		}

		return $data;
	}

	/**
	 * Select only one record
	 *
	 * @param $query
	 * @param int $fetchType
	 * @return array|bool
	 */
	public function selectOne($query, $fetchType = PDO::FETCH_ASSOC)
	{
		$result = $this->query($query);

		if (!$result) return false;

		if (($record = $this->moveNext($result, $fetchType)) !== false)
		{
			return $record;
		}

		return false;
	}

	/**
	 * Returns all records from last select
	 *
	 * @param bool $result
	 *
	 * @return array
	 */
	public function getRecords($result = false)
	{
		/** @var PDOStatement $result */
		if (!$result) $result = $this->result;

		if ($result)
		{
			return $result->fetchAll(PDO::FETCH_ASSOC);
		}

		return array();
	}

	/**
	 * Fetches all all
	 *
	 * @param bool $result
	 *
	 * @return array
	 */
	public function getObjects($result = false)
	{
		/** @var PDOStatement $result */
		if (!$result) $result = $this->result;

		if ($result)
		{
			return $result->fetchAll(PDO::FETCH_OBJ);
		}

		return array();
	}

	/**
	 * Returns tables status
	 *
	 * @return array
	 */
	public function getTablesStatus()
	{
		$this->query("SHOW TABLE STATUS FROM `".$this->db_name."`");

		if ($this->numRows() > 0)
		{
			$tables = array();

			while ($this->moveNext())
			{
				$tables[$this->col["Name"]] = $this->col;
			}

			return $tables;
		}

		return false;
	}

	/**
	 * Number of rows in last result
	 *
	 * @param bool $result
	 *
	 * @return int
	 */
	public function numRows($result = false)
	{
		/** @var PDOStatement $result */
		if (!$result) $result = $this->result;

		return $result ? $result->rowCount() : 0;
	}

	/**
	 * @param bool $result
	 * @return int
	 */
	public function numRowsAffected($result = false)
	{
		/** @var PDOStatement $result */
		if (!$result) $result = $this->result;

		return $result ? $result->rowCount() : 0;
	}

	/**
	 * Return fetched array
	 *
	 * @param bool $result
	 * @param int $fetchType
	 * @return mixed|null
	 */
	public function moveNext($result = false, $fetchType = PDO::FETCH_ASSOC)
	{
		/** @var PDOStatement $result */
		if (!$result) $result = $this->result;

		//if (!$result) return null;

		try
		{
			$this->col = $result->fetch($fetchType);
		}
		catch (PDOException $e)
		{
			$this->col = null;
		}
		catch (Exception $e)
		{
			$this->col = null;
		}

		return $this->col;
	}

	/**
	 * Shuts mysql connection down
	 */
	public function done()
	{
		$this->pdo = null;
	}

	/**
	 * Return return id
	 * @return int
	 */
	public function insertId()
	{
		return $this->pdo->lastInsertId();
	}
}

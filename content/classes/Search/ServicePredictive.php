<?php
/**
 * Class Search_ServicePredictive
 */
class Search_ServicePredictive implements Search_ServiceInterface
{
	/**
	 * @var DataAccess_ProductsRepository
	 */
	protected $productsRepository;

	/**
	 * @var DataAccess_CategoryRepository
	 */
	protected $categoriesRepository;

	/**
	 * @var DataAccess_CategoryRepository
	 */
	protected $manufacturerRepository;

	/**
	 * @var Settings
	 */
	protected $settings;

	/**
	 * Search_ServicePredictive constructor.
	 * @param DataAccess_ProductsRepository $productsRepository
	 * @param DataAccess_CategoryRepository $categoriesRepository
	 * @param DataAccess_ManufacturerRepository $manufacturerRepository
	 * @param $settings
	 */
	public function __construct(
		DataAccess_ProductsRepository $productsRepository,
		DataAccess_CategoryRepository $categoriesRepository,
		DataAccess_ManufacturerRepository $manufacturerRepository,
		$settings
	)
	{
		$this->productsRepository = $productsRepository;
		$this->categoriesRepository = $categoriesRepository;
		$this->manufacturerRepository = $manufacturerRepository;
		$this->settings = $settings;
	}

	/**
	 * @param $searchStr
	 * @param array $options
	 * @return array
	 */
	public function search($searchStr, $options = array())
	{
		$options['logic'] = isset($options['logic']) && in_array($options['logic'], array('AND', 'OR')) ? $options['logic'] : 'OR';
		$result = array('status' => 1, 'count' => 0, 'results' => array());

		if ($searchStr != '')
		{
			$count = 0;
			$results = array();

			/**
			 * Search in products
			 */
			$productsSearchParams = array('product_id' => $searchStr, 'title' => $searchStr, 'search_keywords' => $searchStr, 'is_stealth' => 0);

			if ($this->settings['PredictiveSearchInProductsDescription'])
			{
				$productsSearchParams['description'] = $searchStr;
			}

			$productsCount = $this->productsRepository->getCount($productsSearchParams, $options['logic']);

			if ($productsCount > 0)
			{
				$count += $productsCount;
				$products = $this->productsRepository->getList(0, $this->settings['PredictiveSearchResultsCount'], 'title', $productsSearchParams, 'p.pid, p.product_id, p.title, p.url_default, p.url_custom', $options['logic']);

				$results['products'] = array(
					'type' => 'group',
					'title' => 'Products',
					'url' => $this->settings["GlobalHttpUrl"] . '/?p=catalog&mode=search&search_str=' . $searchStr, // links to results
					'count' => $productsCount,
					'items' => array()
				);

				foreach ($products as $product)
				{
					$results['products']['items'][] = array(
						'type' => 'item',
						'url' => $this->settings["GlobalHttpUrl"] . '/' . (trim($product['url_custom']) == '') ? $product['url_default'] : $product['url_custom'],
						'title' => $product['title']
					);
				}
			}

			/**
			 * Search in Categories
			 */
			if ($this->settings['PredictiveSearchInCategories'])
			{
				$categoriesSearchParams = array('key_name' => $searchStr, 'name' => $searchStr, 'is_stealth' => 0);
				$categoriesCount = $this->categoriesRepository->getCount($categoriesSearchParams, $options['logic']);

				if ($categoriesCount > 0)
				{
					$count += $categoriesCount;
					$categories = $this->categoriesRepository->getList(0, $this->settings['PredictiveSearchResultsCount'], 'title', $categoriesSearchParams, 'c.cid, c.key_name, c.name, c.url_default, c.url_custom', $options['logic']);

					$results['categories'] = array(
						'type' => 'group',
						'title' => 'Categories',
						'count' => $categoriesCount,
						'items' => array()
					);

					foreach ($categories as $category)
					{
						$results['categories']['items'][] = array(
							'type' => 'item',
							'url' => $this->settings["GlobalHttpUrl"] . '/' . (trim($category['url_custom']) == '') ? $category['url_default'] : $category['url_custom'],
							'title' => $category['name']
						);
					}
				}
			}

			/**
			 * Search in manufacturer
			 */
			if ($this->settings['PredictiveSearchInManufacturers'])
			{
				$manufacturerSearchParams = array($searchStr, 'search_str' => $searchStr);
				$manufacturerCount = $this->manufacturerRepository->getCount($manufacturerSearchParams, $options['logic']);

				if ($manufacturerCount > 0)
				{
					$count += $manufacturerCount;
					$manufacturers = $this->manufacturerRepository->getList(0, $this->settings['PredictiveSearchResultsCount'], 'title', $manufacturerSearchParams, 'manufacturer_name, manufacturer_code, manufacturer_id', $options['logic']);

					$results['manufacturers'] = array(
						'type' => 'group',
						'title' => 'Manufacturers',
						'count' => $manufacturerCount,
						'items' => array()
					);

					foreach ($manufacturers as $manufacturer)
					{
						$results['manufacturers']['items'][] = array(
							'type' => 'item',
							'url' => 'index.php?p=catalog&mode=manufacturer&mid=' . $manufacturer['manufacturer_id'],
							'title' => $manufacturer['manufacturer_name']
						);
					}
				}
			}

			$result['count'] = $count;
			$result['results'] = $results;
		}

		return $result;
	}
}
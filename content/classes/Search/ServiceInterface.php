<?php
/**
 * Interface Predictive Search
 */
interface Search_ServiceInterface
{
	public function search($searchStr, $options = array());
}
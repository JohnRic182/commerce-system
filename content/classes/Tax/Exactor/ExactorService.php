<?php

/**
 * Class Tax_Exactor_ExactorService
 */
class Tax_Exactor_ExactorService implements Tax_TaxServiceInterface
{
	protected $db;
	protected $settings;
	protected $taxClient;

	/**
	 * Class constructor
	 *
	 * @param $db
	 * @param $settings
	 */
	public function __construct($db, &$settings)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->taxClient = Tax_Exactor_ExactorClient::getInstance($db, $settings);
	}

	/**
	 * Calculate tax
	 *
	 * @param ORDER $order
	 *
	 * @return float
	 */
	public function calculateTax(ORDER $order)
	{
		$this->taxClient->calculate_exactor_tax($order);

		return $order->getTaxAmount();
	}

	/**
	 * Save invoice
	 *
	 * @param ORDER $order
	 */
	public function saveInvoice(ORDER $order)
	{
		$transactionId = $this->taxClient->get_transaction_id($order->getId());

		if (!empty($transactionId))
		{
			$this->taxClient->ExactorDBHelper_save_invoice($transactionId, $order->getId());
		}
	}

	/**
	 * Commit tax
	 *
	 * @param ORDER $order
	 */
	public function commitTax(ORDER $order)
	{
		$this->taxClient->ExactorAPIHelper_commit_order($order->getId(), $order->getOrderNumber());
	}

	/**
	 * Refund tax
	 *
	 * @param ORDER $order
	 * @param $orderStatus
	 * @param $paymentStatus
	 */
	public function refundTax(ORDER $order, $orderStatus, $paymentStatus)
	{
		$this->taxClient->ExactorAPIHelper_refund_order($order->getId());

		$transactionId = $this->taxClient->get_transaction_id($order->getId());

		if (!empty($transactionId))
		{
			$this->taxClient->ExactorDBHelper_save_invoice($transactionId, $order->getId());
		}
	}

	/**
	 * Void tax
	 *
	 * @param ORDER $order
	 * @param $orderStatus
	 * @param $paymentStatus
	 */
	public function voidTax(ORDER $order, $orderStatus, $paymentStatus)
	{
		$this->taxClient->ExactorAPIHelper_refund_order($order->getId());

		$transactionId = $this->taxClient->get_transaction_id($order->getId());

		if (!empty($transactionId))
		{
			$this->taxClient->ExactorDBHelper_save_invoice($transactionId, $order->getId());
		}
	}

	/**
	 * Delete tax
	 *
	 * @param ORDER $order
	 */
	public function deleteTax(ORDER $order)
	{
		$this->taxClient->ExactorAPIHelper_refund_order($order->getId());
	}
}
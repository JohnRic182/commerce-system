<?php

class Tax_Exactor_ExactorListener
{
	/**
	 * Handle on order status change event
	 * 
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderStatusChange(Events_OrderEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();

		$oldOrderStatus = $event->getData('oldOrderStatus');
		$oldPaymentStatus = $event->getData('oldPaymentStatus');
		$newOrderStatus = $event->getData('newOrderStatus');
		$newPaymentStatus = $event->getData('newPaymentStatus');

		/**
		 * Refactoring to use service
		 */
		$exactorService = new Tax_Exactor_ExactorService($db, $settings);

		// new order placed
		if (in_array($oldOrderStatus, array(ORDER::STATUS_ABANDON, ORDER::STATUS_NEW)) && in_array($newOrderStatus, array(ORDER::STATUS_PROCESS, ORDER::STATUS_COMPLETED)))
		{
			$exactorService->saveInvoice($order);
		}

		// payment received
		if ($oldPaymentStatus != ORDER::PAYMENT_STATUS_RECEIVED && $newPaymentStatus == ORDER::PAYMENT_STATUS_RECEIVED)
		{
			$exactorService->commitTax($order);
		}
		else if ($oldPaymentStatus != ORDER::PAYMENT_STATUS_REFUNDED && $newPaymentStatus == ORDER::PAYMENT_STATUS_REFUNDED)
		{
			$exactorService->refundTax($order, $newOrderStatus, $newPaymentStatus);
		}
		else if (($oldOrderStatus != ORDER::STATUS_CANCELED && $newOrderStatus == ORDER::STATUS_CANCELED) || ($oldPaymentStatus != ORDER::PAYMENT_STATUS_CANCELED && $newPaymentStatus == ORDER::PAYMENT_STATUS_CANCELED))
		{
			$exactorService->voidTax($order, $newOrderStatus, $newPaymentStatus);
		}
	}

	/**
	 * Handle on order deleted event
	 * 
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderDeleted(Events_OrderEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();

		$exactorService = new Tax_Exactor_ExactorService($db, $settings);
		$exactorService->deleteTax($order);
	}
}
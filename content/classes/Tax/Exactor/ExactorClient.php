<?php

/**
 * Class Tax_Exactor_ExactorClient
 */
class Tax_Exactor_ExactorClient
{
	/**
	 * Store the single instance of Database
	 */
	private static $m_pInstance;
	private $ExactorAccountVerifyUrl = 'https://merchant.exactor.com/account/plugin/authenticate_url';
	private $ExactorCommitUrl = 'https://taxrequest.exactor.com/request/xml';
	private $ExactorInvoiceUrl = 'https://taxrequest.exactor.com/request/xml';
	private $ExactorUrl = 'http://www.exactor.com/ns';

	/**
	 * Define the error array variable that will hold all the error messages
	 */
	protected $error = array();
	protected $iserror = false;
	protected $db = null;
	protected $settings = array();
	protected $exactorRepository;

	/**
	 * Declare the constructor as private so as to create a singleton object
	 *
	 * @param $db
	 * @param $settings
	 */
	private function __construct($db, &$settings)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->exactorRepository = new Tax_Exactor_ExactorRepository($this->db);
	}

	/**
	 * Get the Exactor instance value in order to have a singleton class
	 *
	 * @param $db
	 * @param $settings
	 *
	 * @return Tax_Exactor_ExactorClient
	 */
	public static function getInstance($db, &$settings)
	{
		if (!self::$m_pInstance)
		{
			self::$m_pInstance = new Tax_Exactor_ExactorClient($db, $settings);
		}

		return self::$m_pInstance;
	}

	/**
	 * Validate the admin form for Exactor settings information
	 *
	 * @return array
	 */
	public function validate_exactor_info()
	{
		$ExactorInfoChange = false;
		$exactor_post_array = (isset($_POST["exactor"])) ? $_POST["exactor"] : array();

		// update_option('test_exactor', "Exactor2");
		if (count($exactor_post_array) > 0)
		{
			// trim all field value except password
			foreach ($exactor_post_array as $key => $value)
			{
				$exactor_post_array[$key] = trim($value);
			}

			// create the Exactor user string to validate exactor details at exactor API
			$data_string = "merchant_id=" . htmlspecialchars($exactor_post_array['exactor_merchant_id']) . "&username=" . htmlspecialchars($exactor_post_array['exactor_user_id']) . "&password=" . htmlspecialchars($exactor_post_array['exactor_password']);
			$response = $this->exactor_do_post_request($this->ExactorAccountVerifyUrl, $data_string, 'string');

			if (strtolower($response) == 'denied')
			{
				// set the error message in admin error Array
				$this->error[] = 'Invalid Account #, User Name or Password. Please setup the correct Exactor Account Information.';
				$this->iserror = 1;
			}
			else
			{
				// check if form fields contains empty data
				// if no errors are encountered send the merchant data for verification
				$this->check_empty_data_if_exists($exactor_post_array);
				if (count($this->error) == 0)
				{
					$this->validate_exactor_information_address($exactor_post_array);
				}
			}

			// check if the Exactor settings have been changed
			// TODO: looks like this code never used
			foreach ($exactor_post_array as $exactor_field => $exactor_value)
			{
				if ($this->settings[$exactor_field] != $exactor_value)
				{
					$ExactorInfoChange = true;
					break;
				}
			}
		}
		else
		{
			// get the Exactor info data
			$exactor_post_array = $this->settings;
		}

		return array($this->error, $exactor_post_array);
	}

	/**
	 * Check if the data is empty
	 *
	 * @param array $exactor_post_array
	 */
	protected function check_empty_data_if_exists($exactor_post_array = array())
	{
		// check if there is empty data
		// add func to check the state
		if (isset($exactor_post_array['exactor_state']) && $exactor_post_array['exactor_state'] == '0')
		{
			$this->error[] = 'Exactor state field can not be empty.';
		}

		foreach ($exactor_post_array as $field_name=>$field_value)
		{
			if (trim($field_value) == '' && trim($field_name) != 'street2' && trim($field_name) != 'exactor_plugin_version')
			{
				$this->error[] = ucwords($field_name).' field can not be empty';
			}
		}
	}

	/**
	 * Get taxrequest string
	 *
	 * @return string
	 */
	protected function get_taxrequest_string()
	{
		global $label_app_name;

		return " plugin='".$label_app_name.' '.$this->settings['AppVer']."' version='".htmlspecialchars($this->settings['exactor_plugin_version'])."' ";
	}

	/**
	 * Validate the Exactor information address
	 *
	 * @param $data_form
	 */
	protected function validate_exactor_information_address($data_form)
	{
		global $label_app_name;

		// get the exactor url details here
		$ExactorUrl = $this->ExactorUrl;
		$ExactorInvoiceUrl = $this->ExactorInvoiceUrl;

		$xml = "<TaxRequest xmlns=\"".$ExactorUrl."\" ".$this->get_taxrequest_string()." >";
		$xml .= "<MerchantId>".htmlspecialchars($data_form['exactor_merchant_id'])."</MerchantId>";
		$xml .= "<UserId>".htmlspecialchars($data_form['exactor_user_id'])."</UserId>";
		$xml .= "<InvoiceRequest>";
		$xml .= "<SaleDate>" . date('Y-m-d') . "</SaleDate>";
		$xml .= "<CurrencyCode>" . htmlspecialchars($data_form['exactor_currency_code']) . "</CurrencyCode>";

		$exactor_info = "<FullName>".$label_app_name." Test Transaction</FullName>";
		$exactor_info .= "<Street1>".htmlspecialchars($this->settings['CompanyAddressLine1'])."</Street1>";
		$exactor_info .= "<City>".htmlspecialchars($this->settings['CompanyCity'])."</City>";
		$exactor_info .= "<StateOrProvince>".htmlspecialchars($this->settings['CompanyState'])."</StateOrProvince>";
		$exactor_info .= "<PostalCode>".htmlspecialchars($this->settings['CompanyZip'])."</PostalCode>";
		$exactor_info .= "<Country>".htmlspecialchars($this->settings['CompanyCountry'])."</Country>";

		$xml .= "<BillTo>";
		$xml .= $exactor_info;
		$xml .= "</BillTo>";

		$xml .= "<ShipTo>";
		$xml .= $exactor_info;
		$xml .= "</ShipTo>";

		$xml .= "<ShipFrom>";
		$xml .= $exactor_info;
		$xml .= "</ShipFrom>";

		$xml .= "<LineItem id=\"_1\">";
		$xml .= "<Description>Exactor Account Verification. Plug-in version: " . $this->settings['exactor_plugin_version'] ."</Description>";
		$xml .= "<Quantity>1</Quantity>";
		$xml .= "<GrossAmount>0</GrossAmount>";
		$xml .= "</LineItem>";

		$xml .= "</InvoiceRequest>";
		$xml .= "</TaxRequest>";

		// sent the xml data to exactor to verify the merchant address
		$response = $this->exactor_do_post_request($ExactorInvoiceUrl, $xml);

		// convert the xml response into array
		$xml_responseArray = $this->xml2ary($response);

		// check if error exists
		$this->check_if_error_exists_in_xmlresponse($xml_responseArray, true);
	}

	/**
	 * Calculate the Exactor tax called from engine_order.php
	 *
	 * @param ORDER $order
	 */
	public function calculate_exactor_tax(ORDER $order)
	{
		$user = $order->getUser();

		$exactorOrderInfoArray = array();
		$exactorOrderInfoArray['billing_address'] = $user->data;
		$exactorOrderInfoArray['shipping_address'] = $order->getShippingAddress();
		$exactorOrderInfoArray['order_line_items'] = $order->lineItems;
		$exactorOrderInfoArray['order_info']['discount_amount'] = $order->getDiscountAmount();
		$exactorOrderInfoArray['order_info']['promo_discount_amount'] = $order->getPromoDiscountAmount();
		$exactorOrderInfoArray['order_info']['shipping_is_free'] = $order->getShippingIsFree();
		$exactorOrderInfoArray['order_info']['shipping_is_virtual'] = !$order->getShippingRequired();
		$exactorOrderInfoArray['order_info']['shipping_amount'] = $order->getShippingAmount();
		$exactorOrderInfoArray['order_info']['shipping_ssid'] = '';
		$exactorOrderInfoArray['order_info']['shipping_cm_name'] = '';
		$exactorOrderInfoArray['order_info']['handling_separated'] = $order->getHandlingSeparated();
		$exactorOrderInfoArray['order_info']['handling_text'] = $order->getHandlingText();
		$exactorOrderInfoArray['order_info']['handling_amount'] = $order->getHandlingAmount();

		if (!$order->getShippingRequired())
		{
			if (!$order->getBillingCompleted())
			{
				return;
			}

			$exactorOrderInfoArray['shipping_address'] = $user->getBillingInfo();
		}
		else if (!$order->getShippingComplete())
		{
			return;
		}

		$orderId = $order->getId();
		$customerId = $user->getId();

		$exactorTaxRateArray = array();
		$exactorTax = 0;
		$exactorTaxExempt = array();

		if (!empty($user->data['uid']))
		{
			list ($exactorTax, $exactorTaxRateArray, $exactorTaxExempt) = $this->exactor_invoice_request($exactorOrderInfoArray, $customerId, $orderId);

			if (count($this->error) >0)
			{
				foreach ($this->error as $errval)
				{
					if (!empty($errval))
					{
						$order->setError($errval);
					}
				}
			}

			$order->setTaxExempt($exactorTaxExempt ? ORDER::TAX_EXEMPT_YES : ORDER::TAX_EXEMPT_NO);
			$order->setTaxAmount($exactorTax);

			// handling tax
			if (isset($exactorTaxRateArray['handling']))
			{
				$hand_tax_desc = 'Handling Sales Tax ('.round($exactorTaxRateArray['handling']*1, 2).'%)';

				$order->setHandlingTaxable(true);
				$order->setHandlingTaxRate(round($exactorTaxRateArray['handling'] * 1, 2));
				$order->setHandlingTaxAmount(round($exactorTaxRateArray['handlingTaxAmount'] * 1, 2));
				$order->setHandlingTaxDescription($hand_tax_desc);

				$order->taxRates[$order->getHandlingTaxClassId()] = array(
					'tax_rate' => round($exactorTaxRateArray['handling'], 2),
					'description' => $hand_tax_desc
				);
			}
			else
			{
				$order->setHandlingTaxable(false);
				$order->setHandlingTaxAmount(0);
				$order->setHandlingTaxDescription('');
				$order->setHandlingTaxRate(0);
			}

			// shipping tax
			if (isset($exactorTaxRateArray['shipping']))
			{
				$shippingTaxDescription = "Shipping Sales Tax (".round($exactorTaxRateArray['shipping'], 2)."%)";

				$order->setShippingTaxable(true);
				$order->setShippingTaxRate(round($exactorTaxRateArray['shipping'] * 1, 2));
				$order->setShippingTaxAmount(round($exactorTaxRateArray['shippingTaxAmount'] * 1, 2));
				$order->setShippingTaxDescription($shippingTaxDescription);

				$order->taxRates[$order->getShippingTaxClassId()] = array(
					'tax_rate' => round($exactorTaxRateArray['shipping'] * 1, 2),
					'description' => $shippingTaxDescription
				);
			}
			else
			{
				$order->setShippingTaxable(false);
				$order->setShippingTaxDescription('');
				$order->setShippingTaxRate(0);
				$order->setShippingTaxAmount(0);
			}
		}

		// Update Line item Exactor tax in DB For here
		if (!empty($exactorOrderInfoArray['shipping_address']['name']) && !empty($user->data['uid']) )
		{
			$this->update_line_item_exactor_tax($exactorTaxRateArray, $orderId, $order);
		}

		$discountAmountWithTax = 0;
		$promoDiscountAmountWithTax = 0;
		foreach ($order->lineItems as $item)
		{
			/** @var Model_LineItem $item */
			$discountAmountWithTax += $item->getDiscountAmountWithTax();
			$promoDiscountAmountWithTax += $item->getPromoDiscountAmountWithTax();
		}
		$order->setDiscountAmountWithTax($discountAmountWithTax);
		$order->setPromoDiscountAmountWithTax($promoDiscountAmountWithTax);
	}

	/**
	 * Check is this an admin area
	 *
	 * @return bool
	 */
	protected function isAdminArea()
	{
		return isset($_SESSION['admin_auth_id']);
	}

	/**
	 * Get transaction id
	 *
	 * @param $orderId
	 *
	 * @return null
	 */
	public function get_transaction_id($orderId)
	{
		return $this->exactorRepository->get_transaction_id($orderId);
	}

	/**
	 * Handle tax invoice to Exactor API. Returns xml response from Exactor API for One Page Shipping
	 *
	 * @param array $exactorOrderInfoArray
	 * @param null $customerId
	 * @param $orderId
	 *
	 * @return array
	 */
	protected function exactor_invoice_request($exactorOrderInfoArray = array(), $customerId = null, $orderId)
	{
		//get the exactor transaction here
		$exactor_tax_rate_array = array();
		$exactor_transaction_Id = '';
		$tax_exempt = false;

		$merchant_id = $this->settings['exactor_merchant_id'];
		$user_id = $this->settings['exactor_user_id'];
		$currency_code = $this->settings['exactor_currency_code'];
		$sku_source = $this->settings['exactor_sku_source'];

		$shipping_am = $exactorOrderInfoArray['order_info']['shipping_amount'];
		$shipping_id = $exactorOrderInfoArray['order_info']['shipping_ssid'];
		$shipping_nm = $exactorOrderInfoArray['order_info']['shipping_cm_name'];
		$handling_sp = $exactorOrderInfoArray['order_info']['handling_separated'];
		$handling_ds = $exactorOrderInfoArray['order_info']['handling_text'];
		$handling_am = $exactorOrderInfoArray['order_info']['handling_amount'];
		$TotalDiscountAmount = $exactorOrderInfoArray['order_info']['discount_amount'] + $exactorOrderInfoArray['order_info']['promo_discount_amount'];

		// discount calculation for admin side
		if ($this->isAdminArea())
		{
			$TotalDiscountAmount = $exactorOrderInfoArray['order_info']['discount_amount'] + $exactorOrderInfoArray['order_info']['promo_discount_amount'];
			$handling_am = $exactorOrderInfoArray['order_info']['handling_amount'];
		}

		$shipto_fn = htmlspecialchars(strtoupper($exactorOrderInfoArray['shipping_address']['name']));
		$shipto_ad = htmlspecialchars(strtoupper($exactorOrderInfoArray['shipping_address']['address1']));
		$shipto_ad2 = htmlspecialchars(strtoupper($exactorOrderInfoArray['shipping_address']['address2']));
		$shipto_ct = htmlspecialchars(strtoupper($exactorOrderInfoArray['shipping_address']['city']));
		$shipto_st = htmlspecialchars(strtoupper($exactorOrderInfoArray['shipping_address']['state_abbr']));
		$shipto_zc = htmlspecialchars(strtoupper($exactorOrderInfoArray['shipping_address']['zip']));
		$shipto_co = htmlspecialchars(strtoupper($exactorOrderInfoArray['shipping_address']['country_iso_a2']));

		$billto_fn = htmlspecialchars(strtoupper($exactorOrderInfoArray['billing_address']['name']));
		$billto_ad = htmlspecialchars(strtoupper($exactorOrderInfoArray['billing_address']['address1']));
		$billto_ad2 = htmlspecialchars(strtoupper($exactorOrderInfoArray['billing_address']['address2']));
		$billto_ct = htmlspecialchars(strtoupper($exactorOrderInfoArray['billing_address']['city']));
		$billto_st = htmlspecialchars(strtoupper($exactorOrderInfoArray['billing_address']['state_abbr']));
		$billto_zc = htmlspecialchars(strtoupper($exactorOrderInfoArray['billing_address']['zip']));
		$billto_co = htmlspecialchars(strtoupper($exactorOrderInfoArray['billing_address']['country_iso_a2']));

		$shipfrom_fn = htmlspecialchars($this->settings['exactor_full_name']);
		$shipfrom_ad = htmlspecialchars($this->settings['CompanyAddressLine1']);
		$shipfrom_ct = htmlspecialchars($this->settings['CompanyCity']);

		$shipfrom_st = htmlspecialchars($this->settings['CompanyState']);
		$shipfrom_zc = htmlspecialchars($this->settings['CompanyZip']);
		$shipfrom_co = htmlspecialchars($this->settings['CompanyCountry']);

		$xml = "<TaxRequest xmlns=\"".$this->ExactorUrl."\" ".$this->get_taxrequest_string()." >";
		$xml .= "<MerchantId>".htmlspecialchars($merchant_id)."</MerchantId>";
		$xml .= "<UserId>".htmlspecialchars($user_id)."</UserId>";
		$xml .= "<InvoiceRequest>";
		$xml .= "<SaleDate>".date('Y-m-d')."</SaleDate>";
		//$xml .= "<PurchaseOrderNumber>". htmlspecialchars($orderId) ."</PurchaseOrderNumber>";
		$xml .= "<CurrencyCode>".htmlspecialchars($currency_code)."</CurrencyCode>";

		// check exemption
		if ($this->settings['TaxExemptionsActive'] == 1)
		{
			$exemption_id = htmlspecialchars(trim($exactorOrderInfoArray['billing_address']['exactor_tax_exemption_id']));

			if ($exemption_id == '' && ((int)$customerId > 0)) $exemption_id = $customerId;

			if ($exemption_id != '')
			{
				$xml .= "<ExemptionId>".htmlspecialchars($exemption_id)."</ExemptionId>";
			}
		}

		$xml .= "<BillTo>";
		$xml .= "<FullName>".$billto_fn."</FullName>";
		$xml .= "<Street1>".$billto_ad."</Street1>";
		$xml .= "<Street2>".$billto_ad2."</Street2>";
		$xml .= "<City>".$billto_ct ."</City>";
		$xml .= "<StateOrProvince>".$billto_st."</StateOrProvince>";
		$xml .= "<PostalCode>".$billto_zc."</PostalCode>";
		$xml .= "<Country>" .$billto_co."</Country>";
		$xml .= "</BillTo>";

		if (!$exactorOrderInfoArray['order_info']['shipping_is_virtual'])
		{
			$xml .= "<ShipTo>";
			$xml .= "<FullName>".$shipto_fn."</FullName>";
			$xml .= "<Street1>".$shipto_ad."</Street1>";
			$xml .= "<Street2>".$shipto_ad2."</Street2>";
			$xml .= "<City>".$shipto_ct."</City>";
			$xml .= "<StateOrProvince>".$shipto_st."</StateOrProvince>";
			$xml .= "<PostalCode>".$shipto_zc."</PostalCode>";
			$xml .= "<Country>" .$shipto_co."</Country>";
			$xml .= "</ShipTo>";
		}

		$xml .= "<ShipFrom>";
		$xml .= "<FullName>".$shipfrom_fn."</FullName>";
		$xml .= "<Street1>".$shipfrom_ad."</Street1>";
		$xml .= "<City>".$shipfrom_ct."</City>";
		$xml .= "<StateOrProvince>".$shipfrom_st."</StateOrProvince>";
		$xml .= "<PostalCode>".$shipfrom_zc."</PostalCode>";
		$xml .= "<Country>".$shipfrom_co."</Country>";
		$xml .= "</ShipFrom>";


		foreach ($exactorOrderInfoArray['order_line_items'] as $ocid => $lineItem)
		{
			/** @var Model_LineItem $lineItem */
			$lineItem = $exactorOrderInfoArray['order_line_items'][$ocid];

			$xml .= '<LineItem id="OCID_'.htmlspecialchars($ocid).'">';

			if (trim($lineItem->getExactorEucCode()) != '' && trim($lineItem->getExactorEucCode()) != '0')
			{
				$xml .= '<SKU>'.htmlspecialchars($lineItem->getExactorEucCode()).'</SKU>';
			}
			else if (trim($lineItem->getExactorCategoryEucCode()) != '' && trim($lineItem->getExactorCategoryEucCode()) != '0')
			{
				$xml .= '<SKU>'.htmlspecialchars($lineItem->getExactorCategoryEucCode()).'</SKU>';
			}
			else if ($sku_source == 'PID')
			{
				$xml .= '<SKU>'.htmlspecialchars($lineItem->getProductId()).'</SKU>';
			}
			else
			{
				$xml .= '<SKU>'.htmlspecialchars($lineItem->getProductSku()).'</SKU>';
			}

			$xml .= '<Description>'.htmlspecialchars($lineItem->getTitle()).'</Description>';
			$xml .= '<Quantity>'.$lineItem->getAdminQuantity().'</Quantity>';
			$xml .= '<GrossAmount>' .(strtolower($lineItem->getIsGift()) == 'yes' ? 0 : $lineItem->getSubtotal()). '</GrossAmount>';
			$xml .= '</LineItem>';

			if ($lineItem->getDiscountTotal() > 0)
			{
				$TotalDiscountAmount -= $lineItem->getDiscountTotal();
				$xml .= '<LineItem id="discount_'.htmlspecialchars($ocid).'">';
				$xml .= '<SKU>EUC-99010101</SKU>';
				$xml .= '<Description>Discount on Products</Description>';
				$xml .= '<Quantity>1</Quantity>';
				$xml .= '<GrossAmount>-'.$lineItem->getDiscountTotal().'</GrossAmount>';
				$xml .= '</LineItem>';
			}
		}

		if ($handling_am > 0 && $handling_sp)
		{
			$xml .= '<LineItem id="handling_'.$shipping_id.'">';
			$xml .= '<SKU>EUC-13010301</SKU>';
			$xml .= '<Description>'.htmlspecialchars($handling_ds).'</Description>';
			$xml .= '<Quantity>1</Quantity>';
			$xml .= '<GrossAmount>'.$handling_am.'</GrossAmount>';
			$xml .= '</LineItem>';
		}

		if ($shipping_am > 0)
		{
			$xml .= '<LineItem id="shipping_'.$shipping_id.'">';
			$xml .= '<SKU>'.$this->get_shipping_EUC_code($shipping_nm).'</SKU>';
			$xml .= '<Description>'.htmlspecialchars($shipping_nm).'</Description>';
			$xml .= '<Quantity>1</Quantity>';
			$xml .= '<GrossAmount>'.$shipping_am.'</GrossAmount>';
			$xml .= '</LineItem>';
		}

		if ((float)$TotalDiscountAmount > 0)
		{
			$xml .= '<LineItem id="discount_'.$shipping_id.'">';
			$xml .= '<SKU>EUC-99010101</SKU>';
			$xml .= '<Description>Discount on Shipping</Description>';
			$xml .= '<Quantity>1</Quantity>';
			$xml .= '<GrossAmount>-'.$TotalDiscountAmount.'</GrossAmount>';
			$xml .= '</LineItem>';
		}

		$xml .= '</InvoiceRequest>';
		$xml .= '</TaxRequest>';

		$response = $this->exactorRepository->get_cache_response($orderId, md5($xml));

		if (!$response)
		{
			$response = $this->exactor_do_post_request($this->ExactorInvoiceUrl, $xml);
			$this->exactorRepository->save_cache_response($orderId, md5($xml), $response);
		}

		// convert the xml response to array
		$xml_responseArray = $this->xml2ary($response);

		if (isset($xml_responseArray['TaxResponse']['_c']['InvoiceResponse']['_c']['TotalTaxAmount']['_v']))
		{
			$exactor_tax = (float)$xml_responseArray['TaxResponse']['_c']['InvoiceResponse']['_c']['TotalTaxAmount']['_v'];

			$tax_exempt =
				isset($xml_responseArray['TaxResponse']['_c']['InvoiceResponse']['_c']['ExemptionId']['_v'])
				&&
				trim($xml_responseArray['TaxResponse']['_c']['InvoiceResponse']['_c']['ExemptionId']['_v']) != ''
				&& $exactor_tax == 0;

			$exactor_transaction_Id = $xml_responseArray['TaxResponse']['_c']['InvoiceResponse']['_c']['TransactionId']['_v'];
			$exactor_line_items = $xml_responseArray['TaxResponse']['_c']['InvoiceResponse']['_c']['LineItem'];
			$exactor_tax_rate_array = array();

			// enter the comment here
			foreach ($exactor_line_items as $arr_key=>$tax_array)
			{
				$exactor_tax_rate = 0;
				$exactor_tax_amount = 0;

				// check the variables to prevent call nullable variables for legacy code
				if (isset($tax_array['_c']))
				{
					if ($tax_array['_c']['GrossAmount']['_v'] != 0)
					{
						$exactor_ocid = isset($tax_array['_a']['id']) ? str_replace('OCID_', '', $tax_array['_a']['id']) : 0;

						if (isset($exactorOrderInfoArray['order_line_items'][$exactor_ocid]))
						{
							$exactor_tax_rate = ($tax_array['_c']['TotalTaxAmount']['_v'] / $exactorOrderInfoArray['order_line_items'][$exactor_ocid]->getTotal()) * 100;
						}
						else
						{
							$exactor_tax_rate = ($tax_array['_c']['TotalTaxAmount']['_v'] / $tax_array['_c']['GrossAmount']['_v']) * 100;
						}
					}
					$exactor_tax_amount = $tax_array['_c']['TotalTaxAmount']['_v'];
				}

				// check the variables to prevent call nullable variables for legacy code
				if (isset($tax_array['_a']))
				{
					if (strstr($tax_array['_a']['id'], 'handling'))
					{
						$exactor_tax_rate_array['handling'] = $exactor_tax_rate;
						$exactor_tax_rate_array['handlingTaxAmount'] = $exactor_tax_amount;
					}
					else if (strstr($tax_array['_a']['id'], 'shipping'))
					{
						$exactor_tax_rate_array['shipping'] = $exactor_tax_rate;
						$exactor_tax_rate_array['shippingTaxAmount'] = $exactor_tax_amount;
					}
					else
					{
						$exactor_ocid = str_replace('OCID_', '', $tax_array['_a']['id']);

						$exactor_tax_rate_array[$exactor_ocid] = $exactor_tax_rate;
						$exactor_tax_rate_array[$exactor_ocid.'TaxAmount'] = $exactor_tax_amount;
					}
				}
			}
		}
		else
		{
			$exactor_tax = 0.0;
		}

		$this->exactorRepository->set_transaction_id($orderId, $exactor_transaction_Id);

		// check if error exists
		$this->check_if_error_exists_in_xmlresponse($xml_responseArray);

		return array($exactor_tax, $exactor_tax_rate_array, $tax_exempt);
	}

	/**
	 * Get the shipping EUC Codes
	 *
	 * @param $shipping_name
	 *
	 * @return string
	 */
	protected function get_shipping_EUC_code($shipping_name)
	{
		if (trim($shipping_name) == '') $shipping_name ="Default";

		// if shipping and handling charges are applied from Axactor admin side
		// then set it to default sku code of EUC-13010301 that has been assigned for ahiiping and handling fees
		if ($this->settings['ShippingHandlingSeparated'] == '0' && floatval($this->settings['ShippingHandlingFee']) > 0)
		{
			$shipping_code = 'EUC-13010101';
		}
		else if (stristr($shipping_name, 'USPS') || stristr($shipping_name, 'U.S.P.S') || stristr($shipping_name, 'Mail')  || (stristr($shipping_name, 'Post') && stristr($shipping_name, 'US')))
		{
			$shipping_code = 'EUC-13030202';
		}
		else
		{
			$shipping_code = 'EUC-13010204';
		}

		return htmlspecialchars($shipping_code);
	}

	/**
	 * Update Order items with Exactor Tax
	 *
	 * @param $taxRates
	 *
	 * @param $orderId
	 */
	protected function update_line_item_exactor_tax($taxRates, $orderId, ORDER $order)
	{
		if (isset($taxRates))
		{
			foreach ($taxRates as $lineItemId => $taxRate)
			{
				if (is_numeric($lineItemId))
				{
					$priceWithTax = false;
					$discountAmountWithTax = false;
					$promoDiscountAmountWithTax = false;
					if (isset($order->lineItems[$lineItemId]))
					{
						/** @var Model_LineItem $item */
						$item = $order->lineItems[$lineItemId];
						$item->setIsTaxable($taxRate > 0 ? 'Yes' : 'No');
						$item->setTaxRate(round($taxRates[$lineItemId] * 1, 2));
						$item->setTaxAmount(PriceHelper::round($taxRates[$lineItemId.'TaxAmount']));
						$item->setTaxDescription('Sales Tax ('.round($taxRate, 2) . '%)');

						$priceWithTax = PriceHelper::priceWithTax($item->getFinalPrice(), $item->getTaxRate());
						$item->setPriceWithTax($priceWithTax);

						$discountAmountWithTax = PriceHelper::priceWithTax($item->getDiscountAmount(), $item->getTaxRate());
						$item->setDiscountAmountWithTax($discountAmountWithTax);

						$promoDiscountAmountWithTax = PriceHelper::priceWithTax($item->getPromoDiscountAmount(), $item->getTaxRate());
						$item->setPromoDiscountAmountWithTax($promoDiscountAmountWithTax);
					}

					$db = $this->db;

					$db->reset();
					$db->assignStr('is_taxable', $taxRate > 0 ? 'Yes' : 'No');
					$db->assign('tax_rate', round($taxRates[$lineItemId] * 1, 2));
					$db->assign('tax_amount', PriceHelper::round($taxRates[$lineItemId.'TaxAmount']));
					$db->assignStr('tax_description', 'Sales Tax ('.round($taxRate, 2) . '%)');
					if ($priceWithTax !== false)
					{
						$db->assign('price_withtax', $priceWithTax);
					}
					if ($discountAmountWithTax !== false)
					{
						$this->db->assign('discount_amount_with_tax', $discountAmountWithTax);
					}
					if ($promoDiscountAmountWithTax !== false)
					{
						$this->db->assign('promo_discount_amount_with_tax', $promoDiscountAmountWithTax);
					}
					$db->update(DB_PREFIX.'orders_content', 'WHERE oid='.intval($orderId).' AND ocid='.intval($lineItemId));
				}
			}
		}
	}

	/**
	 * Retrieve the user data from the User table
	 *
	 * @param $userId
	 *
	 * @return array
	 */
	protected function get_user_details($userId)
	{
		$userData = array();

		if ($userId > 0)
		{
			$this->db->query("
				SELECT
					concat(".DB_PREFIX."users.fname,' ',".DB_PREFIX."users.lname) AS name,
					".DB_PREFIX."users.address1,
					".DB_PREFIX."users.address2,
					".DB_PREFIX."users.city,
					".DB_PREFIX."users.zip,
					".DB_PREFIX."users.province,
					".DB_PREFIX."users.state AS state_id,
					".DB_PREFIX."countries.coid AS country_id,
					".DB_PREFIX."countries.name AS country,
					".DB_PREFIX."countries.name AS country_name,
					".DB_PREFIX."countries.iso_a2 AS country_iso_a2,
					".DB_PREFIX."countries.iso_a3 AS country_iso_a3,
					".DB_PREFIX."countries.iso_number AS country_iso_number,
					".DB_PREFIX."states.short_name AS state_abbr,
					".DB_PREFIX."states.name AS state
				FROM ".DB_PREFIX."users
				LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."users.country
				LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."users.state
				WHERE uid='".intval($userId)."'
			");

			if ($this->db->moveNext())
			{
				$userData = $this->db->col;
			}

			return $userData;
		}
	}

	/**
	 * Region ExactorDBHelper group of methods
	 * contains few methods to work with invoices
	 * Save invoice in Exactor plug-in invoice table.
	 * If invoice already exists - update if no - create new one.
	 *
	 * @param $transactionID
	 * @param $orderID
	 */
	public function ExactorDBHelper_save_invoice($transactionID, $orderID)
	{
		$invoice = $this->exactorRepository->get_invoice_transaction($orderID);

		// if result empty - insert new invoice
		if (empty($invoice))
		{
			$this->exactorRepository->insert_invoice($transactionID, $orderID);
			// if result does not empty - update existing invoice
		}
		else
		{
			$this->exactorRepository->update_invoice($transactionID, $orderID);
		}
	}

	/**
	 * gets invoice from invoice table,
	 * commits it and move transaction from invoice table to commit table
	 * @param $orderID - Order id
	 * @param string $invoiceNumber - Exactor Invoice number
	 */
	public function ExactorAPIHelper_commit_order($orderID, $invoiceNumber = '')
	{
		// get transaction from invoiced table
		$invoice = $this->exactorRepository->get_invoice_transaction($orderID);

		if (!empty($invoice))
		{
			$transactionID = $invoice['TransactionID'];

			// commit transaction on Exactor
			$this->commitExactor_Transaction($transactionID, $invoiceNumber);

			// move transaction from invoiced to committed
			$this->exactorRepository->move_invoice_transaction_to_committed($orderID);

			// remove previous response to prevent using committed transaction as invoice
			$this->exactorRepository->delete_cache_responses($orderID);
		}
	}

	/**
	 * Do committed transaction refund on Exactor side
	 *
	 * @param $orderID - OrderId
	 */
	public function ExactorAPIHelper_refund_order($orderID)
	{
		// get committed transaction from committed table
		$committed = $this->exactorRepository->get_committed_transaction($orderID);

		if (!empty($committed))
		{
			$transactionID = $committed['TransactionID'];

			// send refund transaction
			$this->refundExactorTransaction($transactionID);

			//delete transaction from committed table
			$this->exactorRepository->delete_committed_transaction($orderID);
		}
	}

	/**
	 * Commit transaction from Exactor system given the transaction id
	 *
	 * @param $priorTransactionId
	 * @param string $invoiceNumber
	 */
	protected function commitExactor_Transaction($priorTransactionId, $invoiceNumber = '')
	{
		$merchantId = htmlspecialchars($this->settings['exactor_merchant_id']);
		$exactorUserId = htmlspecialchars($this->settings['exactor_user_id']);

		if (!empty($priorTransactionId))
		{
			$xml =
				'<TaxRequest xmlns="'.$this->ExactorUrl.'" '.$this->get_taxrequest_string().' >'.
					'<MerchantId>'.htmlspecialchars($merchantId).'</MerchantId>'.
					'<UserId>'.htmlspecialchars($exactorUserId).'</UserId>'.
					'<CommitRequest>'.
						'<CommitDate>'.date('Y-m-d').'</CommitDate>'.
						'<InvoiceNumber>'.htmlspecialchars($invoiceNumber).'</InvoiceNumber>'.
						'<PriorTransactionId>'.htmlspecialchars($priorTransactionId).'</PriorTransactionId>'.
					'</CommitRequest>'.
				'</TaxRequest>';

			// if the commit transaction option has been set from the admin side then only commit it
			// otherwise don not commit the transaction after placing the order successfully
			if ($this->settings['exactor_commit_taxes'] == '1')
			{
				$this->exactor_do_post_request($this->ExactorCommitUrl, $xml);
			}
		}
	}

	/**
	 * Refund the transaction of the older ID
	 *
	 * @param $transactionID
	 */
	protected function refundExactorTransaction($transactionID)
	{
		// set the Exactor info here, this function will set the exactor information
		$merchant_id = htmlspecialchars($this->settings['exactor_merchant_id']);
		$exactor_user_id = htmlspecialchars($this->settings['exactor_user_id']);

		// refund the transaction at the Exactor end
		if (!empty($transactionID))
		{
			$xml =
				'<TaxRequest xmlns="'.$this->ExactorUrl.'" '.$this->get_taxrequest_string().' >'.
					'<MerchantId>'.htmlspecialchars($merchant_id).'</MerchantId>'.
					'<UserId>'.htmlspecialchars($exactor_user_id).'</UserId>'.
					'<RefundRequest>'.
						'<RefundDate>'.date('Y-m-d').'</RefundDate>'.
						'<PriorTransactionId>'.htmlspecialchars($transactionID).'</PriorTransactionId>'.
					'</RefundRequest>'.
				'</TaxRequest>';

			$this->exactor_do_post_request($this->ExactorCommitUrl, $xml);
		}
	}

	/**
	 * Check for errors int xml response at the Exactor API and throw an exception while doing so
	 *
	 * @param $xml_reponse_array
	 * @param bool $reassign_error_code
	 */
	protected function check_if_error_exists_in_xmlresponse($xml_reponse_array, $reassign_error_code = false)
	{
		$errorCode = "";

		if (isset($xml_reponse_array['TaxResponse']['_c']['ErrorResponse']['_c']['ErrorCode']['_v']))
		{
			$errorCode = (string)$xml_reponse_array['TaxResponse']['_c']['ErrorResponse']['_c']['ErrorCode']['_v'];

			//this condition will replace the error code for admin side as the error messages that are generate for
			//the admin side differs from the customer side
			$errorCode = ($reassign_error_code == true && $errorCode == '14') ? '45' : $errorCode;

			//this condition will replace the error code for admin side as the error messages that are generate for
			//the admin side differs from the customer side
			$errorCode = ($reassign_error_code == true && $errorCode == '03') ? '46' : $errorCode;
		}

		$errorMessage = array(
			'01' => 'Taxable Location determination failed because the Ship To Address is invalid. Please provide a valid Ship To Address',
			'03' => 'An error has occurred during the checkout process. Please contact customer support for assistance',
			'11' => 'Invalid currency code',
			'12' => 'Invalid StateOrProvince Id. Please setup the Exactor Account Information',
			'13' => 'Invalid PostalCode. Please setup the Exactor Account Information',
			'14' => 'Missing or Invalid Ship To Address',
			'15' => 'Missing Line Items',
			'16' => 'Invalid SKU Code. Please contact our technical support',
			'17' => 'Missing or Invalid Gross Amount',
			'42' => 'Transaction Does not Exist',
			'44' => 'Transaction Already Refunded',
			'45' => 'Invalid company address. Please correct the <a href="admin.php?p=settings&group=1">company address</a> to ensure that it is set to a valid address',
			'46' => 'Invalid Account #, User Name or Password. Please setup the correct Exactor Account Information'
		);

		$message = isset($errorMessage[$errorCode]) ? $errorMessage[$errorCode] : '';

		// return the message that is generated here.
		// set Exactor error message for one page checkout
		unset($_SESSION['Exactor_error_message']);

		if (!empty($message))
		{
			$_SESSION['Exactor_error_message'] = $message;
			$this->error[] = $message;
		}
	}

	/**
	 * Sends curl request and returns response given the url and xml data
	 *
	 * @param $url
	 * @param $data
	 * @param string $type
	 *
	 * @return mixed
	 */
	protected function exactor_do_post_request($url, $data, $type = 'xml')
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HEADER, false);

		if ($type == 'xml')
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/xml', 'Content-Type: application/xml; charset=utf-8'));
		}
		else
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('application/x-www-form-urlencoded; charset=utf-8'));
		}

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);

		$this->log($data);

		$response = curl_exec($ch);
		curl_close($ch);

		$this->log($response);

		return $response;
	}

	/**
	 * Log
	 *
	 * @param $s
	 */
	public function log($s)
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			$logFilePath = dirname(__FILE__).'/../../../cache/log/';
			file_put_contents(
				$logFilePath.'/taxes-exactor.txt',
				date("Y/m/d-H:i:s")." - \n".(is_array($s) ? array2text($s) : $s)."\n\n\n",
				FILE_APPEND
			);
		}
	}

	/**
	 * Convert xml data to array
	 *
	 * @param $string
	 *
	 * @return array
	 */
	protected function xml2ary(&$string)
	{
	    $parser = xml_parser_create();
	    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
	    xml_parse_into_struct($parser, $string, $vals, $index);
	    xml_parser_free($parser);

	    $mnary = array();
	    $ary = &$mnary;
	    foreach ($vals as $r)
		{
	        $t = $r['tag'];

	        if ($r['type']=='open')
			{
	            if (isset($ary[$t]))
				{
	                if (isset($ary[$t][0])) $ary[$t][] = array(); else $ary[$t] = array($ary[$t], array());
	                $cv = &$ary[$t][count($ary[$t])-1];
	            }
				else
				{
					$cv = &$ary[$t];
				}

	            if (isset($r['attributes'])) {foreach ($r['attributes'] as $k => $v) $cv['_a'][$k] = $v;}
	            $cv['_c'] = array();
	            $cv['_c']['_p'] = &$ary;
	            $ary=&$cv['_c'];

	        }
			elseif ($r['type']=='complete')
			{
	            if (isset($ary[$t]))
				{ // same as open
	                if (isset($ary[$t][0])) $ary[$t][] = array(); else $ary[$t] = array($ary[$t], array());
	                $cv = &$ary[$t][count($ary[$t]) - 1];
	            }
				else
				{
					$cv=&$ary[$t];
				}
	            if (isset($r['attributes'])) {foreach ($r['attributes'] as $k => $v) $cv['_a'][$k] = $v;}
	            $cv['_v'] = (isset($r['value']) ? $r['value'] : '');

	        }
			elseif ($r['type'] == 'close')
			{
	            $ary = &$ary['_p'];
	        }
	    }

	    $this->_del_p($mnary);

	    return $mnary;
	}

	/**
	 * Remove recursion in result array
	 *
	 * @param $ary
	 */
	protected function _del_p(&$ary)
	{
	    foreach ($ary as $k => $v)
		{
	        if ($k === '_p')
			{
				unset($ary[$k]);
			}
	        elseif (is_array($ary[$k]))
			{
				$this->_del_p($ary[$k]);
			}
	    }
	}

	/**
	 * Array to XML
	 *
	 * @param $cary
	 * @param int $d
	 * @param string $forcetag
	 *
	 * @return string
	 */
	protected function ary2xml($cary, $d = 0, $forcetag = '')
	{
	    $res = array();
	    foreach ($cary as $tag => $r)
		{
	        if (isset($r[0]))
			{
	            $res[] = ary2xml($r, $d, $tag);
	        }
			else
			{
	            if ($forcetag)
				{
					$tag = $forcetag;
				}

	            $sp = str_repeat("\t", $d);
	            $res[] = "$sp<$tag";

	            if (isset($r['_a']))
				{
					foreach ($r['_a'] as $at=>$av) $res[] = " $at=\"$av\"";
				}
	            $res[] = ">".((isset($r['_c'])) ? "\n" : '');

	            if (isset($r['_c']))
				{
					$res[] = ary2xml($r['_c'], $d + 1);
				}
	            elseif (isset($r['_v']))
				{
					$res[] = $r['_v'];
				}
	            $res[] = (isset($r['_c']) ? $sp : '')."</$tag>\n";
	        }

	    }

	    return implode('', $res);
	}

	/**
	 * Insert element into array
	 *
	 * @param $ary
	 * @param $element
	 * @param $pos
	 */
	protected function ins2ary(&$ary, $element, $pos)
	{
	    $ar1 = array_slice($ary, 0, $pos); $ar1[] = $element;
	    $ary = array_merge($ar1, array_slice($ary, $pos));
	}
}

<?php

class Tax_Exactor_TaxCodeCategoryNodeRenderer
{
	public function render(MutableTreeNode $node)
	{
		echo '<ul>';
		$this->renderNode($node);
		echo '</ul>';
	}

	protected function renderNode(MutableTreeNode $node)
	{
		if ($node->isRoot())
		{
			foreach ($node->getChildren() as $child)
			{
				$this->renderNode($child);
			}
		}
		else
		{
			$level = $node->getLevel();
			echo str_repeat("\t", $level)."<li".($level > 1 ? ' style="display:none;"' : '').">\n";

			/** @var Tax_Exactor_TaxCodeCategory $category */
			$category = $node->getUserObject();
			if ($node->getChildCount() > 0)
			{
				echo str_repeat("\t", $level+1).'<span><i class="glyphicon glyphicon-plus-sign"></i> '.gs($category->description)."</span>\n";
				echo str_repeat("\t", $level+1)."<ul>\n";
				foreach ($node->getChildren() as $child)
				{
					$this->renderNode($child);
				}
				echo str_repeat("\t", $level+1)."</ul>\n";
			}
			else
			{
				echo str_repeat("\t", $level+1)."<a class=\"euc_category\" href=\"#\" id=\"category-".$category->id."\"><span>".gs($category->description)."</span></a>\n";
			}

			echo str_repeat("\t", $level)."</li>\n";
		}
	}
}
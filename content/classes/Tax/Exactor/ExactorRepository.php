<?php
/**
 * Class Tax_Exactor_ExactorRepository
 */
class Tax_Exactor_ExactorRepository extends DataAccess_BaseRepository
{
	/**
	 * Get cache response
	 *
	 * @param $OrderID
	 * @param $request_hash
	 * @return null
	 */
	public function get_cache_response($OrderID, $request_hash)
	{
		$cache_value = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'exactor_cache WHERE oid='.intval($OrderID).' AND request_hash = \''.$this->db->escape($request_hash).'\' AND NOW() < DATE_ADD(request_date, INTERVAL 12 HOUR) ORDER BY id DESC');

		if ($cache_value)
		{
			return $cache_value['response_xml'];
		}

		return null;
	}

	/**
	 * Set transaction id
	 *
	 * @param $OrderID
	 * @param $transactionId
	 */
	public function set_transaction_id($OrderID, $transactionId)
	{
		$this->db->reset();
		$this->db->assignStr('transaction_id', $transactionId);
		$this->db->update(DB_PREFIX.'exactor_cache', ' WHERE oid='.intval($OrderID));

		$this->db->reset();
		$this->db->assignStr('TransactionID', $transactionId);
		$this->db->update(DB_PREFIX.'exactor_invoices', ' WHERE OrderID='.intval($OrderID));
	}

	/**
	 * Get transaction id
	 *
	 * @param $OrderID
	 *
	 * @return mixed
	 */
	public function get_transaction_id($OrderID)
	{
		$cache_value = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'exactor_cache WHERE oid='.intval($OrderID).' ORDER BY id DESC');

		if ($cache_value)
		{
			return $cache_value['transaction_id'];
		}

		return null;
	}

	/**
	 * @param $OrderID
	 */
	public function delete_cache_responses($OrderID)
	{
		$this->db->query('DELETE FROM '.DB_PREFIX.'exactor_cache WHERE oid='.intval($OrderID));
	}

	/**
	 * Save cache response
	 *
	 * @param $OrderID
	 * @param $request_hash
	 * @param $response_xml
	 */
	public function save_cache_response($OrderID, $request_hash, $response_xml)
	{
		$this->delete_cache_responses($OrderID);

		$this->db->reset();
		$this->db->assignStr('oid', $OrderID);
		$this->db->assignStr('request_hash', $request_hash);
		$this->db->assignStr('response_xml', $response_xml);
		$this->db->assign('request_date', 'NOW()');
		$this->db->insert(DB_PREFIX.'exactor_cache');
	}

	/**
	 * Contains CRUD operation to work with invoice and committed transaction with low level of abstraction.
	 * If you need higher level of abstraction to work with invoices, check ExactorDBHelper_ group of methods
	 * Invoice table group of methods
	 */

	/**
	 * Insert new invoice in Exactor plug-in invoices table
	 *
	 * $TransactionID - ExactorTransactionID
	 * OrderID - Order id
	 */
	public function insert_invoice($TransactionID, $OrderID)
	{
		$this->db->query("INSERT INTO ".DB_PREFIX."exactor_invoices(TransactionID,OrderID,DateOfOrder) VALUES('{$TransactionID}','{$OrderID}','" . date("Y-m-d")."')");
	}

	/**
	 * Update existing invoice in Exactor plug-in invoices table
	 *
	 * $TransactionID - ExactorTransactionID
	 * OrderID - Order id
	 */
	public function update_invoice($TransactionID, $OrderID)
	{
		$this->db->query("UPDATE ".DB_PREFIX."exactor_invoices SET TransactionID='{$TransactionID}' , OrderID ='{$OrderID}', DateOfOrder='" . date("Y-m-d") . "' WHERE OrderID = '{$OrderID}'");
	}

	/**
	 * Delete invoice from Exactor plug-in invoices table
	 * OrderID - Order id
	 */
	public function delete_invoice($OrderID)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."exactor_invoices WHERE OrderID = '{$OrderID}'");
	}

	/**
	 * Get invoice data from Exactor plug-in invoices table
	 *
	 * OrderID - Order id
	 */
	public function get_invoice_transaction($orderID)
	{
		return $this->db->selectOne("SELECT * FROM ".DB_PREFIX."exactor_invoices WHERE  OrderID =" . intval($orderID));
	}

	/**
	 * Group of methods to work with Committed Transactions
	 */

	/**
	 * Insert transaction in Exactor plug-in committed transaction table
	 *
	 * OrderID - Order id
	 * $TransactionID - ExactorTransactionID
	 */
	public function insert_committed_transaction($TransactionID, $OrderID)
	{
		$this->db->query("INSERT INTO ".DB_PREFIX."exactor_transaction_order(TransactionID,OrderID,DateOfOrder) VALUES('{$TransactionID}','{$OrderID}','".date("Y-m-d")."')");
	}

	/**
	 * Delete committed transaction from Exactor plug-in committed transactions table
	 *
	 * OrderID - Order id
	 */
	public function delete_committed_transaction($OrderID)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."exactor_transaction_order WHERE OrderID = '{$OrderID}'");
	}

	/**
	 * Get Committed Transaction from Exactor plug-in committed transactions table
	 *
	 * OrderID - Order id
	 */
	public function get_committed_transaction($OrderID)
	{
		return $this->db->selectOne("SELECT * FROM ".DB_PREFIX."exactor_transaction_order WHERE  OrderID ='{$OrderID}'");
	}

	/**
	 * Move invoice from invoice transaction table to committed transaction table.
	 * If committed transaction for this order already exist, deletes previous
	 * committed transaction and insert this invoice.
	 *
	 * @param $OrderID - Order id
	 */
	public function move_invoice_transaction_to_committed($OrderID)
	{
		// check if invoice for this order is exist in DB
		$invoice = $this->get_invoice_transaction($OrderID);

		if (!empty($invoice))
		{
			// check if committed transaction is exist in this order
			$transaction_id = $invoice['TransactionID'];
			$committed = $this->get_committed_transaction($OrderID);

			// if exist - delete this transaction, send refund transaction + one invoice
			if (empty($committed))
			{
				// save committed transaction in committed table
				$this->insert_committed_transaction($transaction_id, $OrderID);

				// delete invoice transaction from table
				$this->delete_invoice($OrderID);
			}
			else
			{
				// send refund request
				// TODO: send here refund transaction for old committed transaction

				// delete current committed transaction
				$this->delete_committed_transaction($OrderID);

				// send invoice request
				// this->insert_committed_transaction($transaction_id, $OrderID);

				// delete invoice transaction from table
				$this->delete_invoice($OrderID);
				//TODO: send new invoice request
			}
		}
	}

	/**
	 * Get Exactor tax codes
	 * @return mixed
	 */
	public function getTaxCodesList()
	{
		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'exactor_tax_codes');
	}

	/**
	 * Get Exactor tax codes options
	 * @return array
	 */
	public function getTaxCodesOptions()
	{
		$list = $this->getTaxCodesList();

		$options = array();
		foreach ($list as $taxCode)
		{
			$options[$taxCode['code']] = $taxCode['code'].': '.$taxCode['name'];
		}

		return $options;
	}

	public function getTaxCodeCategoriesTree()
	{
		$tree = new MutableTreeNode();
		$nodesById = array();

		$db = $this->db;
		$db->query('SELECT * FROM '.DB_PREFIX.'exactor_euc_categories ORDER BY parent, description');
		while ($db->moveNext())
		{
			$taxCodeCategory = new Tax_Exactor_TaxCodeCategory();
			$taxCodeCategory->description = trim($db->col['description']);

			$taxCodeCategory->id = intval($db->col['id']);
			$taxCodeCategory->parentId = intval($db->col['parent']);

			$node = new MutableTreeNode($taxCodeCategory);

			$nodesById[$taxCodeCategory->id] = $node;

			if ($taxCodeCategory->parentId < 1)
			{
				$tree->add($node);
			}
			else
			{
				/** @var MutableTreeNode $parentNode */
				$parentNode = $nodesById[$taxCodeCategory->parentId];
				$parentNode->add($node);
			}
		}

		return $tree;
	}

	public function getEUCProductsServices()
	{
		return $this->db->selectAll('SELECT euc_code, description FROM '.DB_PREFIX.'exactor_euc_products_services WHERE category_id = '.intval($_GET['cid'].' ORDER BY description'));
	}
}
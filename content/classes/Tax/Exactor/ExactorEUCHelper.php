<?php

class Tax_Exactor_ExactorEUCHelper
{
	function map_categories($path_parts, &$categories_map)
	{
		global $db;

		$category_path = implode(' / ', $path_parts);
		if (!array_key_exists($category_path, $categories_map))
		{
			$category = $path_parts[count($path_parts) - 1];
			unset($path_parts[count($path_parts) - 1]);

			$parent = 0;
			$level = 0;

			if (count($path_parts) > 0)
			{
				$parent_path = implode(' / ', $path_parts);
				if (array_key_exists($parent_path, $categories_map))
				{
					$parent_values = $categories_map[$parent_path];
					$parent = $parent_values['id'];
					$level = $parent_values['level'] + 1;
				}
			}

			$db->reset();
			$db->assignStr('parent', $parent);
			$db->assignStr('level', $level);
			$db->assignStr('category_path', $category_path);
			$db->assignStr('description', $category);
			$db->insert(DB_PREFIX.'exactor_euc_categories');
			$id = $db->insertId();

			$categories_map[$category_path] = array('id' => $id, 'description' => $category, 'parent' => $parent, 'level' => $level, 'new' => true);
		}

		return $categories_map[$category_path]['id'];
	}

	public static function updateEUCCodes()
	{
		global $db, $settings;

		$content_dir = dirname(__FILE__).'/../../../';

		$c = curl_init('http://www.exactor.com/euc/exactor_euc_list.csv');
	
		if ($settings["ProxyAvailable"] == "YES")
		{
			if (defined("CURLOPT_PROXYTYPE") && defined("CURLPROXY_HTTP") && defined("CURLPROXY_SOCKS5"))
			{
				curl_setopt($c, CURLOPT_PROXYTYPE, $settings["ProxyType"] == "HTTP" ? CURLPROXY_HTTP : CURLPROXY_SOCKS5);
			}
			curl_setopt($c, CURLOPT_PROXY, $settings["ProxyAddress"].":".$settings["ProxyPort"]);
			if ($settings["ProxyRequiresAuthorization"] == "YES")
			{
				curl_setopt($c, CURLOPT_PROXYUSERPWD, $settings["ProxyUsername"].":".$settings["ProxyPassword"]);
			}

			curl_setopt($c, CURLOPT_TIMEOUT, 120);
		}
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		$buffer = curl_exec($c);

		@file_put_contents($content_dir.'cache/tmp/exactor_euc_list.csv', $buffer);

		$f = @fopen($content_dir.'cache/tmp/exactor_euc_list.csv', 'r');

		$categories_map = array();
		$products_services_map = array();

		$db->query('SELECT * FROM '.DB_PREFIX.'exactor_euc_categories');
		while ($row = $db->moveNext())
		{
			$categories_map[$row['category_path']] = array('id' => $row['id'], 'description' => $row['description'], 'parent' => $row['parent'], 'level' => $row['level'], 'new' => false);
		}

		$db->query('SELECT * FROM '.DB_PREFIX.'exactor_euc_products_services');
		while ($row = $db->moveNext())
		{
			$products_services_map[$row['euc_code']] = array('id' => $row['id'], 'description' => $row['description'], 'euc_code' => $row['euc_code'], 'category_id' => $row['category_id'], 'euc_type' => '', 'new' => false);
		}

		// header line
		$line = fgetcsv ($f, 4096*2, ',');

		while ($line = fgetcsv ($f, 4096*2, ','))
		{
			//print_r($line);
			$euc_code = $line[0];
			$euc_type = $line[1];

			if (!array_key_exists($euc_code, $products_services_map))
			{
				$path = $line[2];

				$path_parts = explode(' / ', $path);

				$path_parts = array_map('trim', $path_parts);

				$product_description = $path_parts[count($path_parts) - 1];

				unset($path_parts[count($path_parts) - 1]);

				if (count($path_parts) > 0)
				{
					for ($i = 1; $i < count($path_parts); $i++)
					{
						self::map_categories(array_slice($path_parts, 0, $i), $categories_map);
					}
				}

				$category_id = self::map_categories($path_parts, $categories_map);

				$db->reset();
				$db->assignStr('category_id', $category_id);
				$db->assignStr('euc_code', $euc_code);
				$db->assignStr('description', $product_description);
				$db->insert(DB_PREFIX.'exactor_euc_products_services');
				$id = $db->insertId();

				$products_services_map[$euc_code] = array('id' => $id, 'description' => str_replace("'", "''", $product_description), 'category_id' => $category_id, 'euc_code' => $euc_code, 'euc_type' => $euc_type, 'new' => true);
			}
		}

		@unlink($content_dir.'cache/tmp/exactor_euc_list.csv');
	}
}
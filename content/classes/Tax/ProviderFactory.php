<?php

class Tax_ProviderFactory
{
	protected static $taxProvider = null;
	protected static $taxService = null;

	public static function getTaxService()
	{
		if (is_null(self::$taxService))
		{
			global $db, $settings;

			if (isset($settings['AvalaraEnableTax']) && $settings['AvalaraEnableTax'] == 'Yes')
			{
				self::$taxService = new Tax_Avalara_AvalaraService($db, $settings);
			}
			else if (isset($settings['ExactorEnableTax']) && $settings['ExactorEnableTax'] == 'Yes')
			{
				self::$taxService = new Tax_Exactor_ExactorService($db, $settings);
			}
			else
			{
				return self::getTaxProvider();
			}
		}

		return self::$taxService;
	}

	/**
	 * Returns instance of tax provider
	 *
	 * @return Tax_ProviderInterface
	 */
	public static function getTaxProvider()
	{
		if (is_null(self::$taxProvider))
		{
			global $db, $settings;

			$orderRepository = DataAccess_OrderRepository::getInstance();
			self::$taxProvider = new Tax_CustomProvider(
				$orderRepository,
				$settings,
				new TaxRates(
					new DataAccess_TaxRateRepository($db),
					$settings
				)
			);
		}

		return self::$taxProvider;
	}

	public static function setTaxProvider(Tax_ProviderInterface $taxProvider)
	{
		self::$taxProvider = $taxProvider;
	}
}
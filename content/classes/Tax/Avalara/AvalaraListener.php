<?php

class Tax_Avalara_AvalaraListener
{
	/**
	 * Handle on order status change event
	 * 
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderStatusChange(Events_OrderEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();

		$oldOrderStatus = $event->getData('oldOrderStatus');
		$oldPaymentStatus = $event->getData('oldPaymentStatus');
		$newOrderStatus = $event->getData('newOrderStatus');
		$newPaymentStatus = $event->getData('newPaymentStatus');

		$avalaraService = new Tax_Avalara_AvalaraService($db, $settings);

		// Order just placed
		if ($oldPaymentStatus != ORDER::PAYMENT_STATUS_RECEIVED && $newPaymentStatus == ORDER::PAYMENT_STATUS_RECEIVED)
		{
			$avalaraService->commitTax($order);
		}
		else if ($oldPaymentStatus != ORDER::PAYMENT_STATUS_REFUNDED && $newPaymentStatus == ORDER::PAYMENT_STATUS_REFUNDED)
		{
			$avalaraService->refundTax($order, $newOrderStatus, $newPaymentStatus);
		}
		else if (($oldOrderStatus != ORDER::STATUS_CANCELED && $newOrderStatus == ORDER::STATUS_CANCELED) || ($oldPaymentStatus != ORDER::PAYMENT_STATUS_CANCELED && $newPaymentStatus == ORDER::PAYMENT_STATUS_CANCELED))
		{
			$avalaraService->voidTax($order, $newOrderStatus, $newPaymentStatus);
		}
	}

	/**
	 * Handle on order deleted event
	 * 
	 * @param Events_OrderEvent $event
	 */
	public static function onOrderDeleted(Events_OrderEvent $event)
	{
		global $db, $settings;

		$order = $event->getOrder();

		$avalaraService = new Tax_Avalara_AvalaraService($db, $settings);

		$avalaraService->deleteTax($order);
	}
}
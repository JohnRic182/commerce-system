<?php

class Tax_Avalara_AvalaraAccountClient
{
	protected $db;
	protected $settings;
	protected $wsdl_path;
	protected $wsdlClient;
	protected $soapClient = null;

	protected $curl_handle;
	protected $request_gateway;
	protected $last_request = array();
	protected $last_response = array();
	protected $last_document_id = false;

	/**
	 * Avalara client class constructor
	 */
	public function __construct(&$db, &$settings)
	{
		require_once(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/content/vendors/nusoap/nusoap.php');

		$this->db = $db;
		$this->settings = $settings;

		$this->setWSDL(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/content/vendors/avalara/accountsvc.wsdl');
	}

	/**
	 * Set WSDL file
	 */
	public function setWSDL($wsdl_path)
	{
		unset($this->wsdlClient);
		unset($this->soapClient);

		$this->wsdl_path = $wsdl_path;
		$this->wsdlClient = new wsdl($this->wsdl_path);
		$this->soapClient = new nusoap_client($this->wsdlClient,true);
		$this->soapClient->soap_defencoding = 'UTF-8';
		$this->soapClient->setUseCurl(1);

		if (trim($this->settings['AvalaraURL']) != '')
		{
			$this->soapClient->setEndpoint($this->settings['AvalaraURL'].'/Account/AccountSvc.asmx');
		}
	}

	/**
	 * Get tax codes
	 *
	 * @param $username
	 * @param $password
	 *
	 * @return array
	 */
	public function getTaxCodes($username, $password)
	{
		$taxCodes = array();
		$pageIndex = 0;

		do
		{
			$request = array('FetchRequest' => array(
				'Fields' => 'TaxCodeValue,Description,ParentTaxCode',
				'Filters' => 'IsActive=true',
				'PageIndex' => $pageIndex,
				'PageSize' => 100
			));

			$request = array(
				'parameters' => $request,
			);

			$response = $this->sendRequest('TaxCodeFetch', $request, $username, $password);

			$hadResult = false;
			if ($response){
				if (isset($response['TaxCodeFetchResult']['TaxCodes']['TaxCode']))
				{
					if (count($response['TaxCodeFetchResult']['TaxCodes']['TaxCode']) > 0)
					{
						$hadResult = true;
						foreach ($response['TaxCodeFetchResult']['TaxCodes']['TaxCode'] as $taxCode)
						{
							if ($taxCode){
								$taxCodes[$taxCode['TaxCodeValue']] = array(
									'TaxCodeValue' => $taxCode['TaxCodeValue'],
									'ParentTaxCode' => $taxCode['ParentTaxCode'],
									'Description' => $taxCode['Description']
								);
							}
							else{
								$hadResult = false;
							}
						}
					}
					else{
						$hadResult = false;
					}
				}
				else{
					$hadResult = false;
				}
			}
			$pageIndex++;

		} while ($hadResult);

		return count($taxCodes) > 0 ? $taxCodes : false;
	}

	/**
	 *
	 */
	private function getHeaderStr($username, $password)
	{
		preg_match('!(\d+)\.(\d+)!',$this->settings['AppVer'],$app_ver_data);

		$client = getpp(true).' Avalara Tax system '.$app_ver_data[1].'.'.$app_ver_data[2];

		$xml =<<<XML
            <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" SOAP-ENV:mustUnderstand="1">
                <wsse:UsernameToken>
                    <wsse:Username>{$username}</wsse:Username>
                    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">{$password}</wsse:Password>
                </wsse:UsernameToken>
            </wsse:Security>
            <Profile xmlns="http://avatax.avalara.com/services" SOAP-ENV:actor="http://schemas.xmlsoap.org/soap/actor/next" SOAP-ENV:mustUnderstand="0">
                <Client>{$client}</Client>
            </Profile>
XML;

		return $xml;
	}

	/**
	 * Send request
	 */
	protected function sendRequest($request_type, $request, $username, $password)
	{
		$this->last_request = $request;
		$header = $this->getHeaderStr($username, $password);

		$this->log(print_r($request, 1));
		$response = $this->soapClient->call($request_type,$request,'http://tempuri.org','',$header);
		$this->log($this->soapClient->response);

		$this->last_response = $response;

		return $response;
	}

	public function log($s)
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			$logFilePath = dirname(dirname(dirname(dirname(__FILE__)))).'/cache/log/';
			file_put_contents(
				$logFilePath."/taxes-avalara.txt",
				date("Y/m/d-H:i:s")." - \n".(is_array($s) ? array2text($s) : $s)."\n\n\n",
				FILE_APPEND
			);
		}
	}
}
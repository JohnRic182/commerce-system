<?php
/**
 * Class Tax_Avalara_AvalaraTaxCodeHelper
 */
class Tax_Avalara_AvalaraTaxCodeHelper
{
	/**
	 * Return Avalara tax codes count
	 *
	 * @return mixed
	 */
	public static function getTaxCodesCount()
	{
		global $db;
		$result = $db->selectOne('SELECT COUNT(*) as c FROM '.DB_PREFIX.'avalara_tax_codes_categories');
		return $result['c'];
	}

	/**
	 * Update Avalara tax codes
	 *
	 * @param $username
	 * @param $password
	 *
	 * @return bool
	 */
	public static function updateTaxCodes($username, $password)
	{
		global $db, $settings;

		$dbIds = array();

		$queryResult = $db->query('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes_categories');
		while ($row = $db->moveNext($queryResult))
		{
			$row['updated'] = false;
			$dbIds[$row['tax_code']] = $row;
		}

		$avalaraAccountClient = new Tax_Avalara_AvalaraAccountClient($db, $settings);
		$taxCodes = @$avalaraAccountClient->getTaxCodes($username, $password);

		if ($taxCodes === false)
		{
			return false;
		}

		$brokenTaxCodes = array();
		foreach ($taxCodes as $taxCodeValue => $taxCode)
		{
			if ($taxCode['TaxCodeValue'] == $taxCode['ParentTaxCode'])
			{
				$brokenTaxCodes[] = $taxCodeValue;
			}
		}

		foreach ($brokenTaxCodes as $brokenTaxCode)
		{
			unset($taxCodes[$brokenTaxCode]);
		}

//		if (isset($taxCodes['SC090303']) && $taxCodes['SC090303']['TaxCodeValue'] == $taxCodes['SC090303']['ParentTaxCode'])
//		{
//			unset($taxCodes['SC090303']);
//		}


		$taxCodes['Undefined'] = array(
			'TaxCodeValue' => 'Undefined',
			'ParentTaxCode' => '',
			'Description' => 'Unclassified'
		);

		// build tree

		$tree = array();

		foreach ($taxCodes as $taxCodeId => $taxCode)
		{
			$taxCode['level'] = 1;
			$taxCode['saved'] = false;
			$taxCode['dbId'] = isset($dbIds[$taxCodeId]) ? $dbIds[$taxCodeId]['id'] : false;

			if (isset($tree[$taxCodeId]))
			{
				$tree[$taxCodeId] = array_merge($tree[$taxCodeId], $taxCode);
			}
			else
			{
				$taxCode['children'] = array();
				$tree[$taxCodeId] = $taxCode;
			}

			$parentId = $taxCode['ParentTaxCode'];

			if($parentId != '')
			{
				if ($parentId == $taxCodeId){
					$parentId = 'Undefined';
					$taxCode['ParentTaxCode'] = $parentId;
					$tree[$taxCodeId]['ParentTaxCode'] = 'Undefined';
				}
				else{
					if (!isset($taxCodes[$parentId]))
					{
						$tree[$taxCodeId]['ParentTaxCode'] = $parentId = 'Undefined';
					}

					if (!isset($tree[$parentId]['children']))
					{
						$tree[$parentId]['children'] = array();
					}
				}

				$tree[$parentId]['children'][$taxCodeId] = &$tree[$taxCodeId];
			}
		}

		$newTree = array();

		$maxLevel = 1;
		// set levels

		foreach ($tree as $taxCodeId => $taxCode)
		{
			$parentId = $taxCode['ParentTaxCode'];
			$level = 1;
			while ($parentId != '')
			{
				$level++;
				$newparentId = $tree[$parentId]['ParentTaxCode'];
				if ($newparentId == $parentId){
					$parentId = '';
				}
				else{
					$parentId = $newparentId;
				}
			}
			$tree[$taxCodeId]['level'] = $level;

			$newTree[$taxCodeId] = $taxCode;
			$newTree[$taxCodeId]['level'] = $level;

			if ($level > $maxLevel) $maxLevel = $level;
		}

		// save
		for ($level = 1; $level <= $maxLevel; $level++)
		{
			foreach ($tree as $taxCodeId => $taxCode)
			{
				if ($taxCode['level'] == $level && !$taxCode['saved'])
				{
					$db->reset();
					$db->assignStr('parent', $taxCode['ParentTaxCode'] == '' ? 0 : $tree[$taxCode['ParentTaxCode']]['dbId']);
					$db->assignStr('level', $level);
					$db->assignStr('tax_code', $taxCodeId);
					$db->assignStr('description', $taxCode['Description']);

					if ($taxCode['dbId'])
					{
						$db->update(DB_PREFIX.'avalara_tax_codes_categories', 'WHERE id="'.intval($taxCode['dbId']).'"');
						$dbIds[$taxCodeId]['updated'] = true;
					}
					else
					{
						$tree[$taxCodeId]['dbId'] = $db->insert(DB_PREFIX.'avalara_tax_codes_categories');
					}

					$tree[$taxCodeId]['saved'] = true;
				}
			}
		}

		foreach ($dbIds as $row)
		{
			if (!$row['updated'])
			{
				$db->query('DELETE FROM '.DB_PREFIX.'avalara_tax_codes_categories WHERE id='.intval($row['id']));
				$db->query('UPDATE '.DB_PREFIX.'products SET avalara_tax_code="" WHERE avalara_tax_code="'.$db->escape($row['tax_code']).'"');
			}
		}

		return true;
	}
}

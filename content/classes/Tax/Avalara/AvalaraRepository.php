<?php

/**
 * Class Tax_Avalara_AvalaraRepository
 */
class Tax_Avalara_AvalaraRepository extends DataAccess_BaseRepository
{
	/**
	 * @return mixed
	 */
	public function getTaxCodesList()
	{
		return $this->db->selectAll('SELECT * FROM '.DB_PREFIX.'avalara_tax_codes');
	}

	/**
	 * @return array
	 */
	public function getTaxCodesOptions()
	{
		$list = $this->getTaxCodesList();

		$options = array();
		foreach ($list as $taxCode)
		{
			$options[$taxCode['code']] = $taxCode['code'].': '.$taxCode['code'];
		}

		return $options;
	}

	public function getTaxCodeCategoriesTree()
	{
		$tree = new MutableTreeNode();
		$nodesById = array();

		$db = $this->db;
		$db->query('SELECT cat.* FROM '.DB_PREFIX.'avalara_tax_codes_categories cat LEFT JOIN '.DB_PREFIX.'avalara_tax_codes tc ON cat.tax_code = tc.code WHERE tc.code IS NULL ORDER BY cat.level, cat.description');
		while ($db->moveNext())
		{
			$taxCodeCategory = new Tax_Avalara_TaxCodeCategory();
			$taxCodeCategory->taxCode = $db->col['tax_code'];
			$taxCodeCategory->description = trim($db->col['description']) == '' ? $db->col['tax_code'] : $db->col['description'];

			$taxCodeCategory->id = intval($db->col['id']);
			$taxCodeCategory->parentId = intval($db->col['parent']);

			$node = new MutableTreeNode($taxCodeCategory);

			$nodesById[$taxCodeCategory->id] = $node;

			if ($taxCodeCategory->parentId < 1)
			{
				$tree->add($node);
			}
			else
			{
				/** @var MutableTreeNode $parentNode */
				$parentNode = $nodesById[$taxCodeCategory->parentId];
				$parentNode->add($node);
			}
		}

		return $tree;
	}
}
<?php

class Tax_Avalara_AvalaraClient
{
	protected $db;
	protected $settings;
	protected $wsdl_path;
	protected $wsdlClient;
	protected $soapClient = null;

	protected $curl_handle;
	protected $request_gateway;
	protected $last_request = array();
	protected $last_response = array();
	protected $last_document_id = false;

	protected $document_type_hash = array(
		'SO' => "SalesOrder",
		'SI' => "SalesInvoice",
		'PO' => "PurchaseOrder",
		'PI' => "PurchaseInvoice",
		'RO' => "ReturnOrder",
		'RI' => "ReturnInvoice",
		'PA' => "PendingAdjustment",
		'A' => "Any",
	);

	protected $cancel_code_hash = array(
		'U' => "Unspecified",
		'PF' => "PostFailed",
		'DD' => "DocDeleted",
		'DV' => "DocVoided",
		'AC' => "AdjustmentCancelled",
	);

	/**
	 * Avalara client class constructor
	 */
	public function __construct(&$db, &$settings)
	{
		require_once($settings['GlobalServerPath'].'/content/vendors/nusoap/nusoap.php');

		$this->db = $db;
		$this->settings = $settings;

		$this->wsdl_path = $settings['GlobalServerPath'].'/content/vendors/avalara/taxsvc.wsdl';

		$this->wsdlClient = new wsdl($this->wsdl_path);
		$this->soapClient = new nusoap_client($this->wsdlClient,true);
		$this->soapClient->soap_defencoding = 'UTF-8';
		$this->soapClient->setUseCurl(1);

		if ($settings['AvalaraURL'])
		{
			$this->soapClient->setEndpoint($settings['AvalaraURL'].'/Tax/TaxSvc.asmx');
		}
	}

	/**
	 * Set WSDL file
	 */
	public function setWSDL($wsdl_path)
	{
		unset($this->wsdlClient);
		unset($this->soapClient);

		$this->wsdl_path = $wsdl_path;
		$this->wsdlClient = new wsdl($this->wsdl_path);
		$this->soapClient = new nusoap_client($this->wsdlClient,true);
		$this->soapClient->soap_defencoding = 'UTF-8';
		$this->soapClient->setUseCurl(1);

		if ($this->settings['AvalaraURL'])
		{
			$this->soapClient->setEndpoint($this->settings['AvalaraURL'].'/tax/taxsvc.asmx');
		}
	}

	/**
	 * Check for errors
	 */
	public function hasError($message)
	{
		$message = array_shift($message);

		if ($message['ResultCode'] == 'Error')
		{
			return true;
		}
		
		return false;
	}

	/**
	 * Get tax request
	 */
	public function getTaxRequest($customer_code, $products, $addresses, $options = array())
	{
		if (empty($products) || empty($addresses) || !is_array($products) || !is_array($addresses)) return false;

		$request = $this->processGetTaxRequest($customer_code, $products, $addresses, $options);

		$oid = $options['oid'];
		$request_hash = md5(print_r($request,1));
		$cache = $this->db->selectOne('SELECT * FROM '.DB_PREFIX.'avalara_cache WHERE oid='.$oid.' AND request_hash = \''.$request_hash.'\' AND NOW() < DATE_ADD(request_date, INTERVAL 12 HOUR)');

		if (!$cache)
		{
			$response = $this->sendRequest('GetTax', $request);

			$this->db->query('DELETE FROM '.DB_PREFIX.'avalara_cache WHERE oid='.$oid);
			$this->db->query('INSERT INTO '.DB_PREFIX.'avalara_cache (oid, request_hash, response, request_date) VALUES ('.$oid.', \''.$this->db->escape($request_hash).'\',\''.$this->db->escape(serialize($response)).'\', NOW())');
		}
		else
		{
			$response = unserialize($cache['response']);
		}

		if ($this->hasError($response))
		{
			if (isset($response['GetTaxResult']['Messages']['Message']['Summary']))
			{
				$_SESSION['Avalara_error_message'] = 'Error: '.$response['GetTaxResult']['Messages']['Message']['Summary'];
			}

			return false; 
		}

		return $this->processGetTaxResult($response);
	}

	/**
	 * Process tax request
	 */
	public function processGetTaxRequest($customer_code, $products, $addresses, $options = array())
	{
		if (empty($products) || empty($addresses) || !is_array($products) || !is_array($addresses)) return false;

		$request = array();

		$request['CustomerCode'] = $customer_code;

		if (!empty($this->settings['AvalaraCompanyCode']))
		{
			$request['CompanyCode'] = $this->settings['AvalaraCompanyCode'];
		}
		if (!empty($this->settings['AvalaraCurrencyCode']))
		{
			$request['CurrencyCode'] = $this->settings['AvalaraCurrencyCode'];
		}
	
		$this->_set_data($request,'DocCode',$options,'document_code');
	
		$this->_set_data($request,'DocDate',$options,'date',time());
		$request['DocDate'] = date('Y-m-d',$request['DocDate']);

		$this->_set_data($request,'HashCode',$options,'hash_code');
		$this->_set_data($request,'DocType',$options,'document_type','SO',$this->document_type_hash,true);
		$this->_set_data($request,'Commit',$options,'commit_order');
		$this->_set_data($request,'PurchaseOrderNo',$options,'order_num');
		$this->_set_data($request,'ExemptionNo',$options,'tax_exempt_no');

		$request['DetailLevel'] = 'Tax';

		$this->_set_data($request,'CurrencyCode',$options,'currency_code');

		$request['ServiceMode'] = 'Automatic';

		$orderDiscount = $options['discount_amount'];

		$this->_set_data($request,'ExchangeRate',$options,'exchange_rate');
		$this->_set_data($request,'ExchangeRateEffDate',$options,'exchange_rate_date');

		$lines = array();

		foreach ($products as $p)
		{
			$line_product = array();

			$line_product['No'] = $p['ocid'];
			$line_product['DestinationCode'] = $p['destination_code'];
			$line_product['OriginCode'] = $p['origin_code'];

			$this->_set_data($line_product,'ItemCode',$p,'product_code');

			$line_product['Qty'] = $p['quantity'];
			$line_product['Amount'] = $p['product_total'];

			$this->_set_data($line_product,'TaxCode',$p,'tax_code');
			$this->_set_data($line_product,'Description',$p,'description');

			$lines[] = $line_product;

			if ($p['discount_amount'] > 0)
			{
				$orderDiscount -= $p['discount_amount'];

				$line_product = array();
				$line_product['No'] = 'Disc_'.$p['ocid'];
				$line_product['DestinationCode'] = $p['destination_code'];
				$line_product['OriginCode'] = $p['origin_code'];

				$this->_set_data($line_product,'ItemCode',$p,'product_code');
				$line_product['Qty'] = 1;
				$line_product['Amount'] = 0 - $p['discount_amount'];

				$this->_set_data($line_product,'TaxCode',$p,'tax_code');
				$this->_set_data($line_product,'Description',$p,'description');

				$lines[] = $line_product;
			}
		}

		$orderDiscount = round($orderDiscount, 2);
		if ($orderDiscount > 0)
		{
			$lines[] = array(
				'No' => 'shipping_disc',
				'DestinationCode' => '2',
				'OriginCode' => '1',
				'Qty' => 1,
				'Amount' => 0 - $orderDiscount,
				'TaxCode' => 'FR',
				'Description' => 'Shipping discount',
			);
		}

		$request['Lines'] = array(
			'Line' => $lines,
		);

		$address_array = array();

		foreach ($addresses as $addr)
		{
			$address_item = array();

			$address_item['AddressCode'] = $addr['address_code'];

			$this->_set_data($address_item,'TaxRegionId',$addr,'tax_region_code',0);

			if (!$address_item['TaxRegionId']) 
			{
				$address_item['Line1'] = $addr['address1'];
		
				$this->_set_data($address_item,'Line1',$addr,'address1');
				$this->_set_data($address_item,'Line2',$addr,'address2');
				$this->_set_data($address_item,'PostalCode',$addr,'zip');
				$this->_set_data($address_item,'City',$addr,'city');
				$this->_set_data($address_item,'Region',$addr,'province');
				$this->_set_data($address_item,'Country',$addr,'country');
			}


			$address_array[] = $address_item;
		}

		$request['Addresses'] = array(
			'BaseAddress' => $address_array,
		);

		$request = array(
			'parameters' => array(
				'GetTaxRequest' => $request,
			),
		);

		return $request;
	}

	/**
	 * Process get tax result
	 */
	public function processGetTaxResult($response)
	{
		$response = $response['GetTaxResult'];

		$taxes = array();

		$taxes['document_id'] = $response['DocId'];
		$taxes['document_code'] = $response['DocCode'];
		$taxes['tax_amount'] = $response['TotalTax'];

		$taxes['product_taxes'] = array();

		if (isset($response['TaxLines']['TaxLine']['No']))
		{
			$response['TaxLines']['TaxLine'] = array($response['TaxLines']['TaxLine']);
		}

		// settings order as tax exempt by default
		$is_item_with_tax_exemption = false;
		$is_item_without_tax_exemption = false;

		foreach ($response['TaxLines']['TaxLine'] as $tl)
		{
			$tax_item = array();
			$tax_item['ocid'] = $tl['No'];
			$tax_item['rate'] = $tl['Rate'];
			$tax_item['tax_amount'] = $tl['Tax'];
			$tax_item['is_taxable'] = $tl['Taxability'];
			$tax_item['tax_exempt'] = $tl['TaxCode'] != 'FR' && isset($tl['Exemption']) && $tl['Exemption'] != '' && $tl['Exemption'] != '0' && $tl['Rate'] > 0 && $tl['Tax'] == 0;

			// if there is any item that is not tax exempt, unsetting it
			if ($tax_item['tax_exempt'])
			{
				$is_item_with_tax_exemption = true;
			}
			else
			{
				$is_item_without_tax_exemption = true;
			}

			$tax_item['tax_details'] = array();

			if (isset($response['TaxLines']['TaxLine']['TaxType']))
			{
				$response['TaxLines']['TaxLine'] = array($response['TaxLines']['TaxLine']);
			}

			$line_jurisdiction_name = '';

			$taxDetails = isset($tl['TaxDetails']['TaxDetail'][0]) ? $tl['TaxDetails']['TaxDetail'] : array($tl['TaxDetails']['TaxDetail']);
			foreach ($taxDetails as $td)
			{
				$tax_details_item = array();

				$tax_details_item['rate'] = $td['Rate'];
				$tax_details_item['tax_amount'] = $td['Tax'];
				$tax_details_item['jurisdiction_name'] = $td['JurisName'];

				if (trim($line_jurisdiction_name) != '')
				{
					$line_jurisdiction_name .= ' : ';
				}
				$line_jurisdiction_name .= $td['JurisName'];

				$tax_details_item['tax_name'] = $td['TaxName'];

				$tax_item['tax_details'][] = $tax_details_item;
			}

			$tax_item['jurisdiction_name'] = $line_jurisdiction_name;

			$taxes['product_taxes'][] = $tax_item;
		}

		$taxes['tax_exempt'] = $is_item_with_tax_exemption ? ($is_item_without_tax_exemption ? ORDER::TAX_EXEMPT_PARTIAL : ORDER::TAX_EXEMPT_YES) : ORDER::TAX_EXEMPT_NO;

		return $taxes;
	}

	/**
	 * Commit tax
	 */
	public function commitTax($customer_code,$document_code,$options = array()) 
	{
		$request = array();
		
		$request['CustomerCode'] = $customer_code;
		$request['DocCode'] = $document_code;
		
		$this->_set_data($request,'DocId',$options,'document_id');
		$this->_set_data($request,'DocType',$options,'document_type','SI',$this->document_type_hash,true);

		$request = array(
			'parameters' => array(
				'CommitTaxRequest' => $request,
			),
		);

		$response = $this->sendRequest('CommitTax',$request);


		return $this->_getDocumentId($response);
	}

	/**
	 * Cancel tax
	 */
	public function cancelTax($document_code,$options = array())
	{
		$request = array();

		$request['DocCode'] = $document_code;
		//$this->_set_data($request,'CustomerCode',$options,'customer_code');
		$this->_set_data($request,'CompanyCode',$this->settings, 'AvalaraCompanyCode');
		$this->_set_data($request,'DocId',$options,'document_id');

		$this->_set_data($request,'DocDate',$options,'date',time());
		$request['DocDate'] = timestamp_to_iso8601($request['DocDate']);

		$this->_set_data($request,'DocType',$options,'document_type','A',$this->document_type_hash,true);
		$this->_set_data($request,'CancelCode',$options,'cancel_code','U',$this->cancel_code_hash,true);

		$request = array(
			'parameters' => array(
				'CancelTaxRequest' => $request
			),
		);

		$response = $this->sendRequest('CancelTax',$request);

		return $this->_getDocumentId($response);
	}

	/**
	 * Ping
	 */
	public function ping($message = '')
	{
		$request = array();

		if (isset($message))
			$request['Message'] = $message;

		$request = array(
			'parameters' => $request,
		);

		$response = $this->sendRequest('Ping',$request);

		return $response['PingResult']['Version'];
	}

	/**
	 * Get document id from response
	 */
	protected function _getDocumentId($response)
	{
		if (empty($response))
			return false;

		if (isset($response['DocId']))
		{
			$this->last_document_id = $response['DocId'];
			return $response['DocId'];
		}

		return $this->_getDocumentId(array_shift($response));
	}

	/**
	 * Get tax override array
	 */
	public function getTaxOverideArray($tax_info) 
	{
		$tax_override = array();

		$tax_override['TaxOverrideType'] = 'Exemption';
		$tax_override['TaxAmount'] = $tax_info['amount'];
		$tax_override['TaxDate'] = timestamp_to_iso8601(time());
		$this->_set_data($tax_override,'Reason',$tax_info,'description');

		return $tax_override;
	}

	/**
	 * Validate address
	 */
	public function validateAddress($address)
	{
		$request = array();

		$request['Line1'] = $address['addressess1'];

		if (isset($address['addressess2']))
			$request['Line2'] = $address['addressess2'];

		$request['PostalCode'] = $address['zip'];
		$request['City'] = $address['city'];
		$request['Region'] = $address['province'];
		$request['Country'] = $address['country'];

		$response = $this->sendRequest($request,$this->request_gateway.'/address/validate','GET');

		$validate_data = array();

		if ($request['ResultCode'] == 'Success' || $request['ResultCode'] == 'Warning')
		{
			$validate_data['validated'] = true; 

			$validate_data['address_info'] = array();

			$validate_data['address_info']['address1'] = $request['Address']['Line1'];

			if (isset($request['Address']['Line2']))
				$validate_data['address_info']['address2'] = $request['Address']['Line2'];

			$validate_data['address_info']['city'] = $request['Address']['City'];
			$validate_data['address_info']['province'] = $request['Address']['Region'];
			$validate_data['address_info']['country'] = $request['Address']['Country'];
			$validate_data['address_info']['zip'] = $request['Address']['PostalCode'];
			$validate_data['address_info']['address_type'] = $request['Address']['AddressType'];
			$validate_data['address_info']['fips_code'] = $request['Address']['FipsCode'];
			$validate_data['address_info']['carrier_route'] = $request['Address']['CarrierRoute'];
			$validate_data['address_info']['post_net'] = $request['Address']['POSTNet'];
		}
		else
		{
			$validate_data['validated'] = false; 
		}

		return $validate_data;
	}

	/**
	 * 
	 */
	private function getHeaderStr()
	{
		preg_match('!(\d+)\.(\d+)!',$this->settings['AppVer'],$app_ver_data);

		$client = getpp(true).' Avalara Tax system '.$app_ver_data[1].'.'.$app_ver_data[2];

		$xml =<<<XML
            <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" SOAP-ENV:mustUnderstand="1">
                <wsse:UsernameToken>
                    <wsse:Username>{$this->settings['AvalaraAccountNumber']}</wsse:Username>
                    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">{$this->settings['AvalaraLicenseKey']}</wsse:Password>
                </wsse:UsernameToken>
            </wsse:Security>
            <Profile xmlns="http://avatax.avalara.com/services" SOAP-ENV:actor="http://schemas.xmlsoap.org/soap/actor/next" SOAP-ENV:mustUnderstand="0">
                <Client>{$client}</Client>
            </Profile>
XML;

		return $xml;
	}

	/**
	 * Send request
	 */
	protected function sendRequest($request_type, $request)
	{
		$this->last_request = $request;
		$header = $this->getHeaderStr();

		$this->log(print_r($request,1));

		$response = $this->soapClient->call($request_type,$request,'http://tempuri.org','',$header);

		$this->log($this->soapClient->response);

		$this->last_response = $response;

		return $response;
	}

	/**
	 * Set data
	 */
	protected function _set_data(&$set_data,$set_key,$get_data,$get_key,$default = null,$possible_values = false,$is_hash = false)
	{
		if (!isset($get_data[$get_key]))
		{
			if (isset($default))
			{
				$get_data[$get_key] = $default;
			}
			else 
			{
				return false;
			}
		}

		if (is_array($possible_values) && !empty($possible_values))
		{
			if ($is_hash) 
			{
				if (isset($possible_values[$get_data[$get_key]]))
				{
					$set_data[$set_key] = $possible_values[$get_data[$get_key]];
					return true;
				}
			}
			else
			{
				if (in_array($get_data[$get_key],$possible_values))
				{
					$set_data[$set_key] = $get_data[$get_key];
					return true;
				}
			}

			return false;
		}

		$set_data[$set_key] = $get_data[$get_key];

		return true;
	}

	public function log($s)
	{
		if (defined('DEVMODE') && DEVMODE)
		{
			$logFilePath = dirname(__FILE__).'/../../../cache/log/';
			file_put_contents(
				$logFilePath."/taxes-avalara.txt", 
				date("Y/m/d-H:i:s")." - \n".(is_array($s) ? array2text($s) : $s)."\n\n\n", 
				FILE_APPEND
			);
		}
	}
}
<?php

class Tax_Avalara_AvalaraService implements Tax_TaxServiceInterface
{
	/** @var DB */
	protected $db;
	protected $settings;
	/** @var Tax_Avalara_AvalaraClient */
	protected $avalaraTaxClient;

	public function __construct($db, &$settings)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->avalaraTaxClient = new Tax_Avalara_AvalaraClient($db, $settings);
	}

	/**
	 * Ping avalara server
	 *
	 * @param string $message
	 *
	 * @return mixed
	 */
	public function ping($message = "")
	{
		return $this->avalaraTaxClient->ping($message);
	}

	/**
	 * Calculate tax
	 */
	public function calculateTax(ORDER $order)
	{
		$order_avalara_tax = $this->db->selectOne("SELECT * FROM ".DB_PREFIX."orders_avalara_tax WHERE oid = ".$order->getId());
		
		if (!$this->hasOrderRequirement($order))
		{
			if ($order_avalara_tax && isset($order_avalara_tax['document_code']) && !empty($order_avalara_tax['document_code']))
			{
				$this->deleteTax($order, $order_avalara_tax['document_code']);
			}

			return false;
		}

		$order_data = $this->formatOrderCalcData($order);

		$products = $order_data["products"];
		$addresses = $order_data["addresses"];
		$options = $order_data["options"];

		if (!$order_avalara_tax)
		{
			$order_avalara_tax = array('status' => 'uncommitted');
		}

		$options['document_type'] = 'SO';
		$options['document_code'] = $order->getOrderNumber();
		$options['oid'] = $order->getId();

		$tax_information = $this->avalaraTaxClient->getTaxRequest($this->getUserEmail($order->getId()), $products, $addresses, $options);

		if (!$tax_information) return false;

		return $this->saveAvalaraOrderData($order, $tax_information, $order_avalara_tax['status']);
	}

	/**
	 * Commit tax
	 */
	public function commitTax(ORDER $order)
	{
		$order_data = $this->formatOrderCalcData($order);

		$products = $order_data["products"];
		$addresses = $order_data["addresses"];
		$options = $order_data["options"];

		$options['document_type'] = 'SI';
		$options['commit_order'] = true;
		$options['document_code'] = $order->getOrderNumber();

		$result = $this->avalaraTaxClient->getTaxRequest($this->getUserEmail($order->getId()), $products, $addresses, $options);

		if (!$result) return false;

        //$this->avalaraTaxClient->applyPayment($order->getOrderNumber());

		return $this->saveAvalaraOrderData($order, $result, 'committed'); 
	}

	/**
	 * Refund Avalara tax
	 */
	public function refundTax(ORDER $order, $orderStatus, $paymentStatus)
	{
		if ($paymentStatus == 'Refunded')
		{
			$order_avalara_tax = $this->db->selectOne("SELECT * FROM ".DB_PREFIX."orders_avalara_tax WHERE oid = ".$order->getId());

			if (!$order_avalara_tax || $order_avalara_tax['status'] != 'committed')
			{
				return false;
			}

			$order_data = $this->formatOrderCalcData($order);

			$products = $order_data["products"];
			$addresses = $order_data["addresses"];
			$options = $order_data["options"];

			$options['commit_order'] = true;
			$options['document_type'] = 'RI'; 
			$options['document_code'] = $order_avalara_tax['document_code'];

			$tax_information = $this->avalaraTaxClient->getTaxRequest($this->getUserEmail($order->getId()), $products, $addresses, $options);

			$this->db->query("UPDATE ".DB_PREFIX."orders_avalara_tax SET status = 'returned' WHERE document_code = '".$this->db->escape($options['document_code'])."'");

			return $tax_information;
		}

		return false;
	}

	/**
	 * Void Avalara tax
	 */
	public function voidTax(ORDER $order, $orderStatus, $paymentStatus)
	{
		if ($orderStatus == 'Canceled')
		{
			$document_code = $this->db->selectOne("SELECT document_code FROM ".DB_PREFIX."orders_avalara_tax WHERE oid = ".$order->getId()." AND status = 'committed'");

			if ($document_code)
			{
				$document_code = array_shift($document_code);
				$document_type = 'SI';
				$options = array(
					'cancel_code' => 'DV',
					'document_type' => $document_type,
				);

				$this->db->query("UPDATE ".DB_PREFIX."orders_avalara_tax SET status = 'void' WHERE document_code = '".$this->db->escape($document_code)."'");	

				$this->avalaraTaxClient->cancelTax($document_code, $options);

				return true;
			}
		}

		return false;
	}

	/**
	 * Delete Avalara tax
	 */
	public function deleteTax(ORDER $order)
	{
		$document_code = $this->db->selectOne("SELECT document_code FROM ".DB_PREFIX."orders_avalara_tax WHERE oid = ".$order->getId()." AND status = 'committed'");

		if ($document_code)
		{
			$document_code = array_shift($document_code);
			$document_type = 'SI';
			$options = array(
				'cancel_code' => 'DD',
				'document_type' => $document_type,
			);

			$this->avalaraTaxClient->cancelTax($document_code, $options);

			$this->db->query("DELETE FROM ".DB_PREFIX."orders_avalara_tax WHERE document_code = '".$this->db->escape($document_code)."'");

			return true;
		}

		return false;
	}

	/**
	 * Check does order fits requirements
	 */
	protected function hasOrderRequirement(ORDER $order)
	{
		$shippingRequired = $order->getShippingRequired();
		$shippingAddress = $order->getShippingAddress();
		$billingAddress = $order->getUser()->getBillingInfo();
		$orderItems = $order->getOrderItems();

		return !(empty($orderItems) || !$order->user()->id ||
			($shippingRequired && (!$shippingAddress || empty($shippingAddress['zip']) || empty($shippingAddress['province']) || empty($shippingAddress['country']))) ||
			($shippingRequired && (!$billingAddress || empty($billingAddress['zip']) || empty($billingAddress['province']) || empty($billingAddress['country'])))
		);
	}

	/**
	 * Format data for request
	 */
	protected function formatOrderCalcData(ORDER $order)
	{
		$order_num = $order->getOrderNumber();
		$user_id = $order->user()->getId();
		$order_items = $order->lineItems;

		if ($order->getShippingRequired())
		{
			$shipping_address = $order->getShippingAddressFromDb();
		}
		else
		{
			$shipping_address = $order->getUser()->getBillingInfo();
		}

		$origin_code = 1;
		$destination_code = 2;

		$addresses = array(
			array(
				'address_code' => $origin_code,
				'address1' => strtoupper($this->settings['CompanyAddressLine1']),
				'address2' => strtoupper($this->settings['CompanyAddressLine2']),
				'zip' => strtoupper($this->settings['CompanyZip']),
				'city' => strtoupper($this->settings['CompanyCity']),
				'province' => strtoupper($this->settings['CompanyState']),
				'country' => strtoupper($this->settings['CompanyCountry']),
			),
			array(
				'address_code' => $destination_code,
				'address1' => strtoupper($shipping_address['address1']),
				'address2' => strtoupper($shipping_address['address2']),
				'zip'	   => strtoupper($shipping_address['zip']),
				'city'     => strtoupper($shipping_address['city']),
				'province' => strtoupper($shipping_address['province']),
				'country'  => strtoupper($shipping_address['country_name']),
			),
		);

		$products = array(); 

		if (is_array($order_items))
		{
			foreach($order_items as $item)
			{
				/** @var Model_LineItem $item $p */
				$p = $item->toArray();

				$prod = array(
					'ocid' => $p['ocid'],
					'product_code' => $p['product_id'],
					'origin_code' => $origin_code,
					'destination_code' => $destination_code,
					'quantity' => $p['admin_quantity'],
					'product_total' => $p['admin_price'] * $p['admin_quantity'],
					'description' => $p['title'],
					'discount_amount' => $p['discount_amount'] + $p['promo_discount_amount'],
				);

				$tax_code_values = $this->db->selectOne("SELECT p.avalara_tax_code, c.avalara_tax_code as category_tax_code FROM ".DB_PREFIX."products p INNER JOIN ".DB_PREFIX."catalog c ON p.cid = c.cid WHERE pid = ".intval($p['pid']));
				
				if ($tax_code_values)
				{
					$tax_code = '';
					if (trim($tax_code_values['category_tax_code']))
					{
						$tax_code = $tax_code_values['category_tax_code'];
					}
					if (trim($tax_code_values['avalara_tax_code']))
					{
						$tax_code = $tax_code_values['avalara_tax_code'];
					}
					$prod['tax_code'] = $tax_code;
				}
			
				$products[] = $prod;
			}
		}

		if ($order->getShippingAmount())
		{
			$products[] = array(
				'ocid' => 'shipping',
				'product_code' => 'shipping',
				'origin_code' => $origin_code,
				'destination_code' => $destination_code,
				'quantity' => 1,
				'tax_code' => 'FR',
				'product_total' => $order->getShippingAmount(),
				'description' => 'Shipping',
				'discount_amount' => 0,
			);
		}

		if ($order->getHandlingSeparated() && $order->getHandlingAmount() > 0)
		{
			$products[] = array(
				'ocid' => 'handling',
				'product_code' => 'handling',
				'origin_code' => $origin_code,
				'destination_code' => $destination_code,
				'quantity' => 1,
				'tax_code' => 'FR',
				'product_total' => $order->getHandlingAmount(),
				'description' => 'Handling',
				'discount_amount' => 0,
			);
		}

		$options = array(
			'order_num' => $order_num,
			'oid' => $order->getId(),
			'discount_amount' => $order->getDiscountAmount() + $order->getPromoDiscountAmount(),
		);

		if ($this->settings['TaxExemptionsActive'] == 1)
		{
			$tax_exempt_no = $this->db->selectOne("SELECT avalara_tax_exempt_number FROM ".DB_PREFIX."users WHERE uid = ".intval($user_id));

			if ($tax_exempt_no)
			{
				$tax_exempt_no = array_shift($tax_exempt_no);
				$options['tax_exempt_no'] = $tax_exempt_no;
			}
		}

		return array('products' => $products, 'addresses' => $addresses, 'options' => $options);
	}

	/**
	 * Save avalara order data / taxes
	 */
	protected function saveAvalaraOrderData(ORDER $order, $tax_information, $status)
	{
		$order->setHandlingTaxable(false);
		$order->setHandlingTaxAmount(0);
		$order->setHandlingTaxClassId(0);
		$order->setHandlingTaxDescription('');
		$order->setHandlingTaxRate(0);

		$order->setShippingTaxable(false);
		$order->setShippingTaxDescription('');
		$order->setShippingTaxRate(0);
		$order->setShippingTaxAmount(0);
		$order->setShippingTaxClassId(0);

		$all_tax_rates = array();

		foreach ($tax_information['product_taxes'] as $pt)
		{
			if ($pt['ocid'] == 'shipping')
			{
				$order->setShippingTaxable($pt['is_taxable'] ? true : false);
				$order->setShippingTaxRate($pt['rate'] * 100);
				$order->setShippingTaxAmount($pt['tax_amount']);
				$order->setShippingTaxDescription($pt['jurisdiction_name'].' : ('.($pt['rate'] * 100).'%)');
			}
			else if (!preg_match('![^\d\.]!', $pt['ocid']))
			{
				$priceWithTax = false;
				$discountAmountWithTax = false;
				$promoDiscountAmountWithTax = false;
				if (isset($order->lineItems[$pt['ocid']]))
				{
					/** @var Model_LineItem $item */
					$item = $order->lineItems[$pt['ocid']];
					$item->setIsTaxable($pt['is_taxable'] ? 'Yes' : 'No');
					$item->setTaxExempt($pt['tax_exempt'] ? 1 : 0);
					$item->setTaxRate($pt['rate'] * 100);
					$item->setTaxAmount($pt['tax_amount']);
					$item->setTaxDescription($pt['jurisdiction_name'].' : ('.($pt['rate'] * 100).'%)');

					$priceWithTax = PriceHelper::priceWithTax($item->getFinalPrice(), $item->getTaxRate());
					$item->setPriceWithTax($priceWithTax);

					$discountAmountWithTax = PriceHelper::priceWithTax($item->getDiscountAmount(), $item->getTaxRate());
					$item->setDiscountAmountWithTax($discountAmountWithTax);

					$promoDiscountAmountWithTax = PriceHelper::priceWithTax($item->getPromoDiscountAmount(), $item->getTaxRate());
					$item->setPromoDiscountAmountWithTax($promoDiscountAmountWithTax);
				}

				$this->db->reset();
				$this->db->assignStr('is_taxable', $pt['is_taxable'] ? 'Yes' : 'No');
				$this->db->assign('tax_exempt', $pt['tax_exempt'] ? 1 : 0);
				$this->db->assign('tax_rate', $pt['rate'] * 100);
				$this->db->assign('tax_amount', $pt['tax_amount']);
				$this->db->assignStr('tax_description', $pt['jurisdiction_name'].' : ('.($pt['rate'] * 100).'%)');
				if ($priceWithTax !== false)
				{
					$this->db->assign('price_withtax', $priceWithTax);
				}
				if ($discountAmountWithTax !== false)
				{
					$this->db->assign('discount_amount_with_tax', $discountAmountWithTax);
				}
				if ($promoDiscountAmountWithTax !== false)
				{
					$this->db->assign('promo_discount_amount_with_tax', $promoDiscountAmountWithTax);
				}
				$this->db->update(DB_PREFIX.'orders_content', 'WHERE ocid = '.intval($pt['ocid']));

				foreach($pt['tax_details'] as $td)
				{
					$all_tax_rates[$td['tax_name']] = array(
						'multiplier' => 1,
						'tax_rate' => $td['rate'],
						'description' => $td['tax_name'],
					);
				}
			}
		}

		$discountAmountWithTax = 0;
		foreach ($order->lineItems as $item)
		{
			$discountAmountWithTax += $item->getDiscountAmountWithTax();
		}
		$order->setDiscountAmountWithTax($discountAmountWithTax);

		$promoDiscountAmountWithTax = 0;
		foreach ($order->lineItems as $item)
		{
			$promoDiscountAmountWithTax += $item->getPromoDiscountAmountWithTax();
		}
		$order->setPromoDiscountAmountWithTax($promoDiscountAmountWithTax);

		$data_query = array(
			'oid' => $order->getId(),
			'document_code' => $tax_information['document_code'],
			'status' => $status,
		);
	
		$this->db->query("
			REPLACE INTO ".DB_PREFIX."orders_avalara_tax 
				(`".implode("`,`", array_keys($data_query))."`) 
			VALUES
				('".implode("','", array_map(array($this->db, 'escape'), $data_query))."')"
		);

		$taxRates = array_values($all_tax_rates);

		$order->setTaxExempt($tax_information['tax_exempt']);
		$order->setTaxAmount($tax_information['tax_amount']);

		$this->db->reset();
		$this->db->assign('tax_amount', $tax_information['tax_amount']);
		$this->db->assignStr('tax_exempt', $tax_information['tax_exempt']);
		$this->db->assign('discount_amount_with_tax', $discountAmountWithTax);
		$this->db->assign('promo_discount_amount_with_tax', $promoDiscountAmountWithTax);
		$this->db->update(DB_PREFIX.'orders', 'WHERE oid = '.$order->getId());

		return array($taxRates, $order->taxAmount);
	}

	/**
	 * Get order items
	 */
	protected function getOrderItems($orderId)
	{
		$this->db->query("
			SELECT 
				".DB_PREFIX."products.*,
				IF(".DB_PREFIX."products.url_custom='',".DB_PREFIX."products.url_default,".DB_PREFIX."products.url_custom) AS product_url,
				".DB_PREFIX."orders_content.product_sub_id,
				".DB_PREFIX."orders_content.ocid,
				".DB_PREFIX."orders_content.admin_price AS product_price,
				".DB_PREFIX."orders_content.options,
				".DB_PREFIX."orders_content.options_clean,
				".DB_PREFIX."orders_content.admin_quantity quantity,
				".DB_PREFIX."orders_content.is_taxable,
				".DB_PREFIX."orders_content.tax_rate,
				".DB_PREFIX."orders_content.tax_description,
				".DB_PREFIX."orders_content.weight AS product_weight,
				".DB_PREFIX."orders_content.is_gift,
				".DB_PREFIX."orders_content.free_shipping AS product_free_shipping,
				".DB_PREFIX."orders_content.digital_product_key,
				".DB_PREFIX."catalog.name AS cat_name,
				IF(".DB_PREFIX."catalog.url_custom='',".DB_PREFIX."catalog.url_default,".DB_PREFIX."catalog.url_custom) AS cat_url                              
			FROM ".DB_PREFIX."orders_content
			INNER JOIN ".DB_PREFIX."products ON ".DB_PREFIX."products.pid=".DB_PREFIX."orders_content.pid
			LEFT JOIN ".DB_PREFIX."catalog ON ".DB_PREFIX."products.cid=".DB_PREFIX."catalog.cid                                    
			WHERE
				".DB_PREFIX."orders_content.oid=".intval($orderId)."
			ORDER BY ocid
		");

		return $this->db->numRows() > 0 ? $this->db->getRecords() : array();
	}

	/**
	 * Get shipping address
	 */
	protected function getShippingAddress($orderId)
	{
		$this->db->query("
			SELECT 
				".DB_PREFIX."orders.shipping_address_type AS address_type,
				".DB_PREFIX."orders.shipping_name AS name,
				".DB_PREFIX."orders.shipping_company AS company,
				".DB_PREFIX."orders.shipping_address1 AS address1,
				".DB_PREFIX."orders.shipping_address2 AS address2,
				".DB_PREFIX."orders.shipping_city AS city,
				".DB_PREFIX."orders.shipping_zip AS zip,
				".DB_PREFIX."orders.shipping_province AS province,
				".DB_PREFIX."orders.shipping_state AS state_id,
				".DB_PREFIX."countries.coid AS country_id,
				".DB_PREFIX."countries.name AS country,
				".DB_PREFIX."countries.name AS country_name,
				".DB_PREFIX."countries.iso_a2 AS country_iso_a2,
				".DB_PREFIX."countries.iso_a3 AS country_iso_a3,
				".DB_PREFIX."countries.iso_number AS country_iso_number,
				".DB_PREFIX."states.short_name AS state_abbr,
				".DB_PREFIX."states.name AS state
			FROM ".DB_PREFIX."orders 
			LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid = ".DB_PREFIX."orders.shipping_country
			LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid = ".DB_PREFIX."orders.shipping_state
			WHERE oid=".intval($orderId)."
		");

		if ($this->db->moveNext())
		{
			return $this->db->col;
		}

		return false;
	}

	/**
	 * Get user login
	 */
	protected function getUserlogin($orderId)
	{
		$user_login = $this->db->selectOne("SELECT u.login FROM ".DB_PREFIX."orders o LEFT JOIN ".DB_PREFIX."users u on o.uid = u.uid where oid = ".intval($orderId));

		return $user_login?array_shift($user_login):0;
	}

	/**
	 * Get user email
	 */
	protected function getUserEmail($orderId)
	{
		$user_login = $this->db->selectOne("SELECT u.email FROM ".DB_PREFIX."orders o LEFT JOIN ".DB_PREFIX."users u on o.uid = u.uid where oid = ".intval($orderId));

		return $user_login?array_shift($user_login):0;
	}

	/**
	 * Save custom tax
	 *
	 * @param ORDER $order
	 */
	function saveCustomAvalaraTax(ORDER $order)
	{
		$order_avalara_tax = $this->db->selectOne("SELECT * FROM ".DB_PREFIX."orders_avalara_tax WHERE oid = ".$order->getId());

		if (!$order_avalara_tax || empty($order_avalara_tax['document_code']))
		{
			return false;
		}

		return $this->avalaraTaxClient->commitTax($order->user()->id, $order_avalara_tax['document_code']);
	}
}

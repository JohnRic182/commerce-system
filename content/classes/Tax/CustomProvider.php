<?php

class Tax_CustomProvider implements Tax_ProviderInterface, Tax_TaxServiceInterface
{
	protected $orderRepository;
	protected $settings;
	protected $taxRateProvider;

	/**
	 * Class constructor
	 *
	 * @param DataAccess_OrderRepositoryInterface $orderRepository
	 * @param $settings
	 * @param $taxRateProvider
	 */
	public function __construct(DataAccess_OrderRepositoryInterface $orderRepository, &$settings, TaxRatesInterface $taxRateProvider)
	{
		$this->orderRepository = $orderRepository;
		$this->settings = $settings;
		$this->taxRateProvider = $taxRateProvider;
	}

	/**
	 * Get tax rates wrapper
	 *
	 * @param int $taxCountryId
	 * @param int $taxStateId
	 * @param int $userLevel
	 *
	 * @return mixed
	 */
	public function getTaxRates($taxCountryId, $taxStateId, $userLevel)
	{
		return $this->taxRateProvider->getTaxRates($taxCountryId, $taxStateId, $userLevel);
	}

	/**
	 * Check does tax rate exist
	 *
	 * @param int $classId
	 *
	 * @return bool
	 */
	public function hasTaxRate($classId)
	{
		return $this->taxRateProvider->hasTaxRate($classId);
	}

	/**
	 * Get tax rate
	 *
	 * @param int $classId
	 * @param bool $fraction
	 *
	 * @return float
	 */
	public function getTaxRate($classId, $fraction = true)
	{
		return $this->taxRateProvider->getTaxRate($classId, $fraction);
	}

	/**
	 * Return tax rate description
	 *
	 * @param $classId
	 *
	 * @return string
	 */
	public function getTaxRateDescription($classId)
	{
		return $this->taxRateProvider->getTaxRateDescription($classId);
	}

	/**
	 * Calculate item tax
	 *
	 * @param int $classId
	 * @param float $amount
	 *
	 * @return float
	 */
	public function calculateItemTax($classId, $amount)
	{
		return $this->taxRateProvider->calculateTax($classId, $amount);
	}

	/**
	 * Init order tax calculations -find tax zone and get tax rates
	 *
	 * @param ORDER $order
	 */
	public function initTax(ORDER $order)
	{
		$this->findTaxZone($order);
		$this->getTaxRates($order->getTaxCountryId(), $order->getTaxStateId(), $order->getUserLevel());
	}

	public function getDisplayPricesWithTax()
	{
		return $this->taxRateProvider->getDisplayPricesWithTax();
	}

	/**
	 * Calculate tax
	 *
	 * @param ORDER $order
	 *
	 * @return float|int
	 */
	public function calculateTax(ORDER $order)
	{
		$userData = $order->getUser()->data;

		$taxExempt = isset($userData['is_tax_exempt']) && $userData['is_tax_exempt'] == 1;
		$taxAmount = 0;

		if (!$taxExempt)
		{
			$this->initTax($order);

			$displayPricesWithTax = $this->taxRateProvider->getDisplayPricesWithTax();

			$subtotalAmountWithTax = 0;

			/* @var Model_LineItem $item */
			foreach ($order->lineItems as $item)
			{
				$lineItemTotal = $item->getTotal();

				$lineItemTax = 0;

				if ($item->getIsTaxable() == 'Yes')
				{
					if ($item->getTaxRate() >= 0)
					{
						if ($displayPricesWithTax)
						{
							$lineItemTotal = $item->getSubtotal(false);
							$priceWithTax = $item->getPriceWithTax();
							$priceWithoutTax = $item->getFinalPrice();

							$lineItemTax = ($priceWithTax - $priceWithoutTax) * $item->getFinalQuantity();

							// Line Item total price with tax prior to discount
							$lineItemTotal += $lineItemTax;

							$lineItemTax -= ($item->getDiscountTotalWithTax() - $item->getDiscountTotal());

							$item->setTaxAmount($lineItemTax);
						}
						else
						{
							$lineItemTax = PriceHelper::round($lineItemTotal * $item->getTaxRate() / 100);
							$item->setTaxAmount($lineItemTax);
							$lineItemTotal += $lineItemTax;
						}
					}
				}

				$taxAmount += $lineItemTax;

				$subtotalAmountWithTax += $lineItemTotal;
			}

			$taxAmount = PriceHelper::round($taxAmount);

			$taxAmount = $taxAmount < 0 ? 0 : $taxAmount;

			if ($order->getShippingTaxable())
			{
				$taxAmount += $order->getShippingTaxAmount();
				if ($order->getPromoType() == 'Shipping')
				{
					$taxAmount -= ($order->getPromoDiscountAmountWithTax() - $order->getPromoDiscountAmount());
				}
			}

			if ($order->getHandlingTaxable())
			{
				$taxAmount += $order->getHandlingTaxAmount();
			}

			$order->setSubtotalAmountWithTax($subtotalAmountWithTax);
		}

		$order->setTaxExempt($taxExempt ? ORDER::TAX_EXEMPT_YES : ORDER::TAX_EXEMPT_NO);
		$order->setTaxAmount($taxAmount);

		return $taxAmount;
	}

	/**
	 * Find order's tax zone - set order's tax country and state
	 *
	 * @param ORDER $order
	 */
	protected function findTaxZone(ORDER $order)
	{
		$settings = $this->settings;

		// set defaults first
		$this->setTaxLocation($order, $settings["TaxDefaultCountry"], $settings["TaxDefaultState"]);

		if ($order->getUser()->auth_ok && $settings['TaxAddress'] != 'Default')
		{
			$userData = $order->getUser()->data;
			$userCountry = $userData['country'];
			$userState = $userData['state'];

			// if country already set (in case of regular users or express checkout users who entered data
			if ($userCountry != 0)
			{
				// if tax address == billing OR shipping is digital OR order is not started yet - use data from user's billing address
				if ($this->useBillingLocation($order))
				{
					$this->setTaxLocation($order, $userCountry, $userState);
				}
				// else use shipping address
				else
				{
					// find out shipping address
					$shippingAddress = $order->getShippingAddress();
					if ($shippingAddress && $shippingAddress["country_id"] > 0)
					{
						$this->setTaxLocation($order, $shippingAddress["country_id"], $shippingAddress["state_id"]);
					}
					// and if it is empty - use billing (remember that 3 levels up if statement excluded Default shipping tax address)
					else
					{
						$this->setTaxLocation($order, $userCountry, $userState);
					}
				}
			}
		}
	}

	/**
	 * @param ORDER $order
	 * @return bool
	 */
	private function useBillingLocation(ORDER $order)
	{
		// check is shipping available for order
		$order->checkShippingRequired();

		return $this->settings['TaxAddress'] == 'Billing' || !$order->getShippingRequired() || $order->getId() == 0;
	}

	/**
	 * @param ORDER $order
	 * @param $countryId
	 * @param $stateId
	 */
	private function setTaxLocation(ORDER $order, $countryId, $stateId)
	{
		$order->setTaxCountryId($countryId);
		$order->setTaxStateId($stateId);
	}

	/**
	 * Commit tax
	 * For custom provider do nothing
	 */
	public function commitTax(ORDER $order)
	{
		return true;
	}

	/**
	 * Refund tax
	 * For custom provider do nothing
	 */
	public function refundTax(ORDER $order, $orderStatus, $paymentStatus)
	{
		return true;
	}

	/**
	 * Commit tax
	 * For custom provider do nothing
	 */
	public function voidTax(ORDER $order, $orderStatus, $paymentStatus)
	{
		return true;
	}

	/**
	 * Delete tax
	 * For custom provider do nothing
	 */
	public function deleteTax(ORDER $order)
	{
		return true;
	}
}
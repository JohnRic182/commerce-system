<?php

interface Tax_ProviderInterface
{
	public function getTaxRates($taxCountryId, $taxStateId, $userLevel);

	public function hasTaxRate($classId);

	public function getTaxRate($classId, $fraction = true);

	public function getTaxRateDescription($classId);

	public function initTax(ORDER $order);

	public function getDisplayPricesWithTax();

	public function calculateItemTax($classId, $amount);

	/*public function calculateTax(ORDER $order);

	public function commitTax(ORDER $order);

	public function refundTax(ORDER $order, $orderStatus, $paymentStatus);

	public function voidTax(ORDER $order, $orderStatus, $paymentStatus);

	public function deleteTax(ORDER $order);
	*/
}
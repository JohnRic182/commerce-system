<?php 

/**
 * @class CustomFields
 */
class CustomFields
{
	public $db;
	public $is_error = false;
	public $errors = array();

	/**
	 * @param DB $db
	 *//**
	 * Class constructor
	 * @param DB $db
	 */
	public function __construct(&$db)
	{
		$this->db = $db;
		return $this;
	}
	
	/**
	 * Reset errors
	 */
	public function resetErrors()
	{
		$this->errors = array();
		$this->is_error = false;
	}
	
	/**
	 * Sets error
	 * @param $message
	 * @return unknown_type
	 */
	public function setError($message)
	{
		$this->errors[count($this->errors)] = $message;
		$this->is_error = true;
	}

	/**
	 * @param $location
	 * @param $data
	 * @param null $formId
	 * @return array|bool
	 */
	public function getCustomFields($location, $data, $formId = null)
	{
		$db = $this->db;

		$data = is_array($data) ? $data : array();

		$this->db->query('
			SELECT *
			FROM ' . DB_PREFIX . 'custom_fields
			WHERE is_active=1 AND field_place="' . $db->escape($location) . '" ' . ($location == 'form' ? ' AND custom_form_id=' . intval($formId) : '') . '
			ORDER BY priority, field_caption, field_name
		');

		if ($this->db->numRows())
		{
			$f = $this->db->getRecords();

			for ($i=0; $i<count($f); $i++)
			{
				$f[$i]['value'] = array_key_exists($f[$i]['field_id'], $data) ? $data[$f[$i]['field_id']] : ($f[$i]['field_type'] == 'checkbox' ? $f[$i]['value_unchecked'] : '');
				$options = explode("\r\n", $f[$i]['options']);
				$f[$i]['parsed_options'] = array_combine($options, $options);
			}

			return $f;
		}

		return false;
	}

	/**
	 * @param $location
	 * @param $fields
	 * @param null $customFormId
	 * @return bool
	 */
	public function validateCustomFields($location, $fields, $customFormId = null)
	{
		$e = count($this->errors);

		$r = $this->db->query('
			SELECT field_id, field_type, field_name
			FROM ' . DB_PREFIX . 'custom_fields
			WHERE field_place="' . $this->db->escape($location) . '" AND is_active=1 AND is_required=1 ' . ($location == 'form' ? ' AND custom_form_id=' . intval($customFormId) : ''). '
			ORDER BY priority, field_caption
		');

		while ($c = $this->db->moveNext($r))
		{
			if (!isset($fields[$c['field_id']]) || trim($fields[$c['field_id']]) == '')
			{
				$this->setError('Please ' . ($c['field_type'] == 'checkbox' ? 'check' : 'enter') . ' ' . $c['field_name']);
			}
		}

		return count($this->errors) == $e;
	}

	/**
	 * @param $location
	 * @param $profile_id
	 * @param $order_id
	 * @param $address_id
	 * @return array|bool
	 */
	public function getCustomFieldsValues($location, $profile_id, $order_id, $address_id)
	{
		$fields = $this->getCustomFields($location, array());
		if ($fields)
		{
			foreach ($fields as $key=>$field)
			{
				$fields[$key]["value_translated"] = "";
				//check is value already in db
				$query = "SELECT * FROM ".DB_PREFIX."custom_fields_values WHERE profile_id='".intval($profile_id)."' AND field_id='".intval($field["field_id"])."' ";
				switch ($location)
				{
					//billing, one user, one field, one value
					case "billing" : break;
					//shipping, the same field may be different for orders and addresses in address book
					case "shipping" : 
					{
						$query.=" AND order_id='".intval($order_id)."' ";
						$query.=" AND address_id='".intval($address_id)."' ";
						break;
					}
					//account, one user, one field, one value
					case "account" : break;
					//signup, one user, one field, one value
					case "signup" : break;
					//shipping, the same field may be different for orders
					case "invoice" : {
						$query.=" AND order_id=".intval($order_id)." ";
						break;
					}
				}
				//echo "GetCustomFieldsValues Query: ".$query;
				$this->db->query($query);
				if ($this->db->moveNext())
				{
					$fields[$key]["value"] = $this->db->col["field_value"];
					if (in_array($fields[$key]["field_type"], array("text", "textarea", "checkbox")))
					{ 
						$fields[$key]["value_translated"] = $this->db->col["field_value"];
					}
					else
					{
						if (in_array($this->db->col["field_value"],  $fields[$key]["parsed_options"]))
						{
							$fields[$key]["value_translated"] = $this->db->col["field_value"];
						}
						else
						{
							$fields[$key]["value_translated"] = "";
						}
					}
				}
				else
				{
					$fields[$key]["value"] = "";
				}
			}
		}
		
		return $fields;
	}

	/**
	 * @param $location
	 * @param $data
	 * @param int $profileId
	 * @param int $orderId
	 * @param int $addressId
	 * @param int $customFormId
	 */
	public function storeCustomFields($location, $data, $profileId = 0, $orderId = 0, $addressId = 0, $customFormId = 0)
	{
		$db = $this->db;

		/**
		 * Create post for custom form
		 */
		if ($location == 'form')
		{
			$db->reset();
			$db->assign('custom_form_id', intval($customFormId));
			$db->assign('user_id', intval($profileId));
			$db->assign('date_created', 'NOW()');
			$customFormPostId = $db->insert(DB_PREFIX . 'custom_forms_posts');
		}

		/**
		 * Get fieds and store values
		 */
		$fields = $this->getCustomFields($location, $data, $customFormId);
		
		if ($fields)
		{
			foreach ($fields as $key => $field)
			{
				/**
				 * For custom forms, store new valu every time form is submited
				 */
				if ($location == 'form')
				{
					$db->reset();
					$db->assign('profile_id', $profileId);
					$db->assign('order_id', $orderId);
					$db->assign('address_id', $addressId);
					$db->assign('custom_form_id', $customFormId);
					$db->assign('custom_form_post_id', $customFormPostId);
					$db->assign('field_id', $field['field_id']);
					$db->assignStr('field_value', $field['value']);
					$db->insert(DB_PREFIX . 'custom_fields_values');
				}
				/**
				 * Otherwise update value if it exists
				 */
				else
				{
					//check is value already in db
					$query = 'SELECT * FROM ' . DB_PREFIX . 'custom_fields_values WHERE profile_id=' . intval($profileId) . ' AND field_id=' . intval($field['field_id']);

					switch ($location)
					{
						//billing, one user, one field, one value
						case 'billing' :
							break;
						//shipping, the same field may be different for orders and addresses in address book
						case 'shipping' :
						{
							$query .= ' AND order_id=' . intval($orderId) . ' ';
							$query .= ' AND address_id=' . intval($addressId) . ' ';
							break;
						}
						//account, one user, one field, one value
						case 'account' :
							break;
						//signup, one user, one field, one value
						case 'signup' :
							break;
						//shipping, the same field may be different for orders
						case 'invoice' :
						{
							$query .= ' AND order_id=' . intval($orderId) . ' ';
							break;
						}
					}

					$db->query($query);

					if ($db->moveNext())
					{
						$db->reset();
						$db->assignStr('field_value', $field['value']);
						$db->update(
							DB_PREFIX . 'custom_fields_values',
							'WHERE profile_id=' . intval($profileId) . ' AND field_id=' . intval($field['field_id']) . ' AND order_id=' . intval($orderId) . ' AND address_id=' . intval($addressId)
						);
					}
					else
					{
						$db->reset();
						$db->assign('profile_id', $profileId);
						$db->assign('order_id', $orderId);
						$db->assign('address_id', $addressId);
						$db->assign('custom_form_id', $customFormId);
						$db->assign('field_id', $field['field_id']);
						$db->assignStr('field_value', $field['value']);
						$db->insert(DB_PREFIX . 'custom_fields_values');
					}
				}
			}
		}
	}
}


<?php

/**
 *
 */
class ImageUtility
{
	protected static $gd_lib_info = null;

	const PRODUCT_IMAGES_DIR = 'images/products';

	const PRODUCT_SECONDARY_IMAGES_DIR = "images/products/secondary";
	const PRODUCT_THUMBS_DIR = "images/products/thumbs";
	const PRODUCT_THUMBS2_DIR = "images/products/preview";

	const CATALOG_IMAGES_DIR = "images/catalog";
	const CATALOG_THUMBS_DIR = "images/catalog/thumbs";

	/**
	 * @param $productId
	 * @return bool|string
	 */
	public static function productHasImage($productId)
	{
		return self::pathHasImage(self::PRODUCT_IMAGES_DIR, $productId);
	}

	/**
	 * @param $productId
	 * @return bool|string
	 */
	public static function productHasThumb($productId)
	{
		return self::pathHasImage(self::PRODUCT_THUMBS_DIR, $productId);
	}

	/**
	 * @param $productId
	 * @return bool|string
	 */
	public static function productHasThumb2($productId)
	{
		return self::pathHasImage(self::PRODUCT_THUMBS2_DIR, $productId);
	}

	/**
	 * @param $path
	 * @param $id
	 * @return bool|string
	 */
	public static function pathHasImage($path, $id)
	{
		$extensions = array('jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF');

		$id = self::imageFileName($id);

		foreach ($extensions as $extension)
		{
			if (file_exists($path . '/' . $id . '.' . $extension))
			{
				return $path . '/' . $id . '.' . $extension;
			}
		}

		return false;
	}


	/**
	 * @param $productId
	 * @return mixed
	 */
	public static function imageFileName($productId)
	{
		$s = str_replace(array("\\", "/", "*", ":", "?", "<", ">", "|", '"', "'"), array('_', '_', '_', '_', '_', '_', '_', '_', '_', '_'), strtolower(trim($productId)));

		return $s;
	}

	/**
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	public static function generateProductThumb($source, $destination)
	{
		global $settings;

		$error = false;

		return self::getImageThumb($source, $destination, $settings["CatalogThumbSize"], $settings["CatalogThumbType"], $error, $settings["CatalogThumbResizeRule"]);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	public static function generateProductPreview($source, $destination)
	{
		global $settings;

		$error = false;

		return self::getImageThumb($source, $destination, $settings["CatalogThumb2Size"], $settings["CatalogThumb2Type"], $error, $settings["CatalogThumb2ResizeRule"]);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	public static function generateManufacturerImage($source, $destination)
	{
		global $settings;

		$error = false;

		return self::getImageThumb($source, $destination, $settings["CatalogManufacturerImageSize"], $settings["CatalogManufacturerImageType"], $error, $settings["CatalogManufacturerImageResizeRule"]);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	public static function generateProductSecondaryThumb($source, $destination)
	{
		global $settings;

		$error = false;

		return self::getImageThumb($source, $destination, $settings["ProductSecondaryThumbSize"], $settings["ProductSecondaryThumbType"], $error, $settings["ProductSecondaryThumbResizeRule"]);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	public static function generateCatalogImage($source, $destination)
	{
		global $settings;

		$error = false;

		return self::getImageThumb($source, $destination, $settings['CatalogCategoryImageSize'], $settings['CatalogCategoryImageType'], $error, $settings['CatalogCategoryImageResizeRule']);
	}

	/**
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	public static function generateCategoryThumb($source, $destination)
	{
		global $settings;

		$error = false;

		return self::getImageThumb($source, $destination, $settings['CatalogCategoryThumbSize'], $settings['CatalogCategoryThumbType'], $error, $settings['CatalogCategoryThumbResizeRule']);
	}

	/**
	 * Removes an existing image and any similar filenames with different file extensions (.jpg,.png,.gif)
	 *
	 * @param mixed $image_path Accepts a path or array of paths to images to be removed
	 * @return bool Returns TRUE if image was successfully removed, FALSE otherwise
	 */
	public static function removeExistingImagesByName($image_paths)
	{
		if (!is_array($image_paths)) $image_paths = array($image_paths);

		$found = false;
		foreach ($image_paths as $path)
		{
			$image_info = pathinfo($path);
			if (isset($image_info['filename']))
			{
				$destination = $image_info['dirname'].DIRECTORY_SEPARATOR.$image_info['filename'];
			}
			else
			{
				$destination = $image_info['dirname'].DIRECTORY_SEPARATOR.(substr($image_info['basename'], 0, strrpos($image_info['basename'], '.')));
			}
		
			$extensions = array('.jpg', '.png', '.gif');
		
			foreach ($extensions as $ext)
			{
				if (file_exists($destination.$ext))
				{
					$found = true;
					@unlink($destination.$ext);
				}
			}
		}
		
		return $found;
	}
	
	/**
	 * Resizes an image
	 *
	 * @param string $source 
	 * @param string $destination 
	 * @param string $size 
	 * @param string $type The method for resizing the dimensions on the image (Proportionate, Square, Manual)
	 * @return bool 
	 * @throws Exception Exception when the thumbnail cannot be created
	 */
	public static function resizeImage($sourceImageFile, $destinationImageFile, $targetSize, $type, $resizeRule = 'Resize')
	{
		$gdinfo = self::getGdLibInfo();
		
		if ($gdinfo)
		{
			$info = @getimagesize($sourceImageFile);
			$sourceImage = null;
			
			if (!$info)
			{
				throw new Exception("Can't read required image data");
			}
			else
			{
				switch($info[2])
				{
					case IMAGETYPE_GIF:
					{
						if (!$gdinfo['GIF.CREATE']) throw new Exception("Can't create thumbnail image with GIF file. GDLib does not support GIF images format.");
                        $sourceImage = @imagecreatefromgif($sourceImageFile);
						break;
					}
					case IMAGETYPE_JPEG: { $sourceImage = @imagecreatefromjpeg($sourceImageFile); break; }
					case IMAGETYPE_PNG: { $sourceImage = @imagecreatefrompng($sourceImageFile); break; }
					default : throw new Exception('Unsupported file type');
				}
			}
			
			if (is_null($sourceImage))
			{
				throw new Exception("Can't resize source image");
			}
			else
			{
				$sourceImageWidth  = $info[0];
				$sourceImageHeight = $info[1];
				
				$resizedImageDimensions = self::getResizedDimensions($targetSize, $sourceImageWidth, $sourceImageHeight);

				$resizedImageWidth = $resizedImageDimensions['width'];
				$resizedImageHeight = $resizedImageDimensions['height'];

				// Check is resized image bigger that source image
				$sourceIsSmall = ($resizedImageWidth > $sourceImageWidth) || ($resizedImageHeight > $sourceImageHeight);

				if ($sourceIsSmall && $resizeRule == 'Keep')
				{
					// Do not resize image in this case, just copy from source to destination
					$destinationCanvasWidth = $destinationImageWidth = $sourceImageWidth;
					$destinationCanvasHeight = $destinationImageHeight = $sourceImageHeight;
				}
				else
				{
					switch ($type)
					{
						default:
						case 'Proportionate':
						{
							$destinationCanvasWidth = $resizedImageWidth;
							$destinationCanvasHeight = $resizedImageHeight;
							break;
						} 
						case 'Square':
						{
							$destinationCanvasWidth = $targetSize;
							$destinationCanvasHeight = $targetSize;
							break;
						} 
					}

					// Have normal canvas, but smaller original image
					if ($sourceIsSmall && $resizeRule == 'Smart')	
					{
						$destinationImageWidth = $sourceImageWidth;
						$destinationImageHeight = $sourceImageHeight;
					}
					else
					{
						$destinationImageWidth = $resizedImageWidth;
						$destinationImageHeight = $resizedImageHeight;
					}
				}

				$destinationImageOffsetLeft = round(($destinationCanvasWidth - $destinationImageWidth) / 2);
				$destinationImageOffsetTop = round(($destinationCanvasHeight - $destinationImageHeight) / 2);

				// Create the new image canvas resource
				if (function_exists('imagecreatetruecolor'))
				{
					$destinationImageCanvas = @imagecreatetruecolor($destinationCanvasWidth, $destinationCanvasHeight);
					if (!$destinationImageCanvas)
					{
						$destinationImageCanvas = @imagecreate($destinationCanvasWidth, $destinationCanvasHeight);
					}
				}
				else
				{
					$destinationImageCanvas = @imagecreate($destinationCanvasWidth, $destinationCanvasHeight);
				}

				if (!$destinationImageCanvas) throw new Exception('Could not create destinate thumbnail image');

				// Determine if this is a PNG file and if it has transparency
				if ($info[2] == IMAGETYPE_PNG)
				{
					$transparent = imagecolorallocatealpha($destinationImageCanvas, 0, 0, 0, 127);
					imagealphablending($destinationImageCanvas, false);
					imagefill($destinationImageCanvas, 0, 0, $transparent);
					imagesavealpha($destinationImageCanvas, true);
				}
				else
				{
					$colorWhite = imagecolorallocate($destinationImageCanvas, 255, 255, 255);
					imagefill($destinationImageCanvas, 1, 1, $colorWhite);
				}
				
				$copyResampled = false;

				if (function_exists("imagecopyresampled"))
				{
					$copyResampled = imagecopyresampled(
						$destinationImageCanvas, $sourceImage, 
						$destinationImageOffsetLeft, $destinationImageOffsetTop, 0, 0, 
						$destinationImageWidth, $destinationImageHeight, $sourceImageWidth, $sourceImageHeight
					);
				}

				if (!$copyResampled)
				{
					imagecopyresized(
						$destinationImageCanvas, $sourceImage, 
						$destinationImageOffsetLeft, $destinationImageOffsetTop, 0, 0, 
						$destinationImageWidth, $destinationImageHeight, $sourceImageWidth, $sourceImageHeight
					);
				}
	
				// Save jpg file
				if (self::_saveImage($info[2], $destinationImageCanvas, $destinationImageFile, true))
				{
					return true;
				}
				else
				{
					throw new Exception("Can't save destination thumbnail image");
				}
			}
		}
		
		throw new Exception('GD Library is not installed or not available');
	}

	/**
	 * @param $image_type
	 * @param $destination_resource
	 * @param $destination
	 * @param bool $overwrite
	 * @return bool
	 * @throws Exception
	 */
	private static function _saveImage($image_type, $destination_resource, $destination, $overwrite = false)
	{
		if (is_file($destination) && $overwrite)
		{
			@unlink($destination);
		}
		
		switch($image_type)
		{
			case IMAGETYPE_GIF:
			{
				$gdinfo = self::getGdLibInfo();
				
				if (!$gdinfo['GIF.CREATE']) throw new Exception("Can't create thumbnail image with GIF file. GDLib does not support GIF images format.");
				$resource = @imagegif($destination_resource, $destination);
			}
			case IMAGETYPE_JPEG:
			{
				@imageinterlace($destination_resource, true);
				$resource = @imagejpeg($destination_resource, $destination, 75);
				break;
			}
			case IMAGETYPE_PNG: { $resource = @imagepng($destination_resource, $destination, 9); break; }
			default : throw new Exception('Unsupported file type');
		}
		
		return $resource;
	}

	/**
	 * @param $target_size
	 * @param $width
	 * @param $height
	 * @return array
	 */
	public static function getResizedDimensions($target_size, $width, $height)
	{
		$targetWidth = 0;
		$targetHeight = 0;
		
		if (intval($width) == 0 || intval($height) == 0) throw new InvalidArgumentException('Invalid width or height of zero (0)');
		
		if ($height > $width)
		{
			$targetHeight = $target_size;
			$targetWidth  = round($targetHeight / $height * $width);
		}
	
		if ($width > $height)
		{
			$targetWidth  = $target_size;
			$targetHeight = round($targetWidth / $width * $height);
		}
	
		if ($width == $height)
		{
			$targetWidth = $targetHeight = $target_size;
		}
		
		return array(
			'width' => $targetWidth,
			'height' => $targetHeight
		);
	}

	/**
	 * @return array|bool|null
	 */
	public static function getGdLibInfo()
	{
		if (is_null(self::$gd_lib_info))
		{
			// Check for availability of GD Lib extension
			if (!extension_loaded('gd'))
			{
				self::$gd_lib_info = false;
				return self::$gd_lib_info;
			}
			
			
			$gdInfo = gd_info();
			
			self::$gd_lib_info = array(
				'VERSION' => $gdInfo['GD Version'],
				'GIF.READ' => $gdInfo['GIF Read Support'],
				'GIF.CREATE' => $gdInfo['GIF Create Support'],
				'PNG' => $gdInfo['PNG Support'],
				'JPG' => (version_compare(PHP_VERSION, '5.3.0', '<')) ? $gdInfo['JPG Support'] : $gdInfo['JPEG Support'],
				'WBMP' => $gdInfo['WBMP Support']
			);
		}
		
		return self::$gd_lib_info;
	}

	/**
	 * @param $type
	 * @param array|null $image_types
	 * @return bool
	 */
	public static function isValidExtension($type, array $image_types = null)
	{
		if (is_null($image_types))
		{
			$image_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
		}
		
		return in_array($type, $image_types);
	}

	/**
	 * @param $type
	 * @return bool
	 */
	public static function getExtensionByType($type)
	{
		$image_types = array(
			IMAGETYPE_GIF => 'gif',
			IMAGETYPE_JPEG => 'jpg',
			IMAGETYPE_PNG => 'png'
		);
		
		return isset($image_types[$type]) ? $image_types[$type] : false;
	}

	/**
	 * @param $source
	 * @param $destination
	 * @param $size
	 * @param $type
	 * @param $error
	 * @param string $resizeRule
	 * @return bool
	 */
	public static function getImageThumb($source, $destination, $size, $type, &$error, $resizeRule = 'Resize')
	{
		try
		{
			return ImageUtility::resizeImage($source, $destination, $size, $type, $resizeRule);
		}
		catch (Exception $e)
		{
			$error = $e->getMessage();
		}

		return false;
	}
}

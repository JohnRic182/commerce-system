<?php
/**
 * Products locations
 */

class ProductsLocations
{
	/**
	 * Database instance
	 * @var DB
	 */
	private $_db;
	private $_errors = array();
	
	
	
	public function __construct($db)
	{
		$this->_db = $db;
		return $this;
	}
	
	/**
	 * Returns record by id
	 * @param $products_location_id
	 * @return array
	 */
	public function getById($products_location_id)
	{
		return $this->_db->selectOne("SELECT * FROM ".DB_PREFIX."products_locations WHERE products_location_id='".intval($products_location_id)."'");
	}
	
	/**
	 * Returns record by id
	 * @param $products_location_id
	 * @return array
	 */
	public function getByCode($code)
	{
		return $this->_db->selectOne("SELECT * FROM ".DB_PREFIX."products_locations WHERE code = '".$this->_db->escape($code)."'");
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	public function getAll()
	{
		return $this->_db->selectAll("SELECT * FROM ".DB_PREFIX."products_locations ORDER BY name");
	}
	
	/**
	 * Validates input
	 * @param $data
	 * @param $id
	 * @return boolean
	 */
	public function validate(&$data, $id = 0)
	{
		$this->_error = array();
		if (!isset($data["is_active"])) $data["is_active"] = 0;
		if (!isset($data["name"]) || trim($data["name"]) == "") $this->_errors[] = "Please enter location name";
		if (!isset($data["code"]) || trim($data["code"]) == "")
		{
			$this->_errors[] = "Please enter location code";	
		}
		else
		{
			$rec = $this->getByCode($data["code"]);
			if ($rec && $rec["products_location_id"] != $id)
			{
				$this->_errors[] = "Entered code already exists";	
			}
		}		
		if (!isset($data["location_type"]) || trim($data["location_type"]) == "") $this->_errors[] = "Please select location type";
		if (!isset($data["contact_name"]) || trim($data["contact_name"]) == "") $this->_errors[] = "Please enter contact name";
		if (!isset($data["address1"]) || trim($data["address1"]) == "") $this->_errors[] = "Please enter address line 1";
		if (!isset($data["address2"])) $data["address1"] = "";
		if (!isset($data["city"]) || trim($data["city"]) == "") $this->_errors[] = "Please enter city name";
		if ((!isset($data["state"]) || trim($data["state"]) == "") && (!isset($data["province"]) || trim($data["province"]) == "")) $this->_errors[] = "Please select state or enter province namee";
		if (!isset($data["country"]) || trim($data["country"]) == "") $this->_errors[] = "Please select country";
		if (!isset($data["zip"]) || trim($data["zip"]) == "") $this->_errors[] = "Please enter ZIP code";
		if (!isset($data["phone"]) || trim($data["phone"]) == "") $this->_errors[] = "Please enter contact phone number";
		if (!isset($data["contact_email"]) || !isEmail($data["contact_email"])) $this->_errors[] = "Please enter correct contact email address";
		if (isset($data["send_notification"]) && $data["send_notification"] == "1")
		{
			if (!isset($data["notify_email"]) || !isEmail($data["notify_email"])) $this->_errors[] = "Please enter correct notification email";
		}
		else
		{
			$data["send_notification"] = 0;
		}
		if (!isset($data["notes"])) $data["notes"] = "";
		if (!isset($data["email_text"])) $data["email_text"] = "";
		
		return count($this->_errors) == 0;
	}
	
	/**
	 * Returns errors array or false
	 * @return mixed
	 */
	public function errors()
	{
		return count($this->_errors) ? $this->_errors : false;
	}
	
	/**
	 * Create a new record
	 * @param $data
	 * @return int new record id
	 */
	public function add($data)
	{
		$this->_db->reset();
		
		$this->_db->assignStr("is_active", intval($data["is_active"]));
		$this->_db->assignStr("name", $data["name"]);
		$this->_db->assignStr("code", $data["code"]);
		$this->_db->assignStr("location_type", $data["location_type"]);
		$this->_db->assignStr("contact_name", $data["contact_name"]);
		$this->_db->assignStr("address1", $data["address1"]);
		$this->_db->assignStr("address2", $data["address2"]);
		$this->_db->assignStr("city", $data["city"]);
		$this->_db->assignStr("state", intval($data["state"]));
		$this->_db->assignStr("province", $data["province"]);
		$this->_db->assignStr("country", intval($data["country"]));
		$this->_db->assignStr("zip", $data["zip"]);
		$this->_db->assignStr("phone", $data["phone"]);
		$this->_db->assignStr("contact_email", $data["contact_email"]);
		$this->_db->assignStr("send_notification", intval($data["send_notification"]));
		$this->_db->assignStr("notify_email", $data["notify_email"]);
		$this->_db->assignStr("notes", $data["notes"]);
		$this->_db->assignStr("email_text", $data["email_text"]);
		
		return $this->_db->insert(DB_PREFIX."products_locations");
	}
	
	/**
	 * Update product location revord
	 * @param $id
	 * @param $data
	 * @return bool
	 */
	public function update($products_location_id, $data)
	{
		$this->_db->reset();
		
		$this->_db->assignStr("is_active", intval($data["is_active"]));
		$this->_db->assignStr("name", $data["name"]);
		$this->_db->assignStr("code", $data["code"]);
		$this->_db->assignStr("location_type", $data["location_type"]);
		$this->_db->assignStr("contact_name", $data["contact_name"]);
		$this->_db->assignStr("address1", $data["address1"]);
		$this->_db->assignStr("address2", $data["address2"]);
		$this->_db->assignStr("city", $data["city"]);
		$this->_db->assignStr("state", intval($data["state"]));
		$this->_db->assignStr("province", $data["province"]);
		$this->_db->assignStr("country", intval($data["country"]));
		$this->_db->assignStr("zip", $data["zip"]);
		$this->_db->assignStr("phone", $data["phone"]);
		$this->_db->assignStr("contact_email", $data["contact_email"]);
		$this->_db->assignStr("send_notification", intval($data["send_notification"]));
		$this->_db->assignStr("notify_email", $data["notify_email"]);
		$this->_db->assignStr("notes", $data["notes"]);
		$this->_db->assignStr("email_text", $data["email_text"]);
		
		return $this->_db->update(DB_PREFIX."products_locations", "WHERE products_location_id='".intval($products_location_id)."'");
	}
	
	/**
	 * Remove record from database
	 * @param $products_location_id
	 * @return bool
	 */
	public function delete($products_location_id)
	{
		$this->_db->query("DELETE FROM ".DB_PREFIX."products_locations WHERE products_location_id='".intval($products_location_id)."'");
		$this->_db->query("UPDATE ".DB_PREFIX."products SET products_location_id = 0 WHERE products_location_id='".intval($products_location_id)."'");
		return true;
	}
}
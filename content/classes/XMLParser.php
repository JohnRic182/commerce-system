<?php
/**
 * @class XMLParser
 */
class XMLParser
{
	# Parse XML
	function parse($String){
		$Data = array();
		$Encoding = $this->xml_encoding($String);
		$String = $this->xml_deleteelements($String, "?");
		$String = $this->xml_deleteelements($String, "!");
		$String = $this->xml_transform_selfclosed($String);
		$Data = $this->xml_readxml($String, $Data, $Encoding);
		unset($String);
		return($Data);
	}
	
	# Get encoding of xml
	function xml_encoding($String){
		if(substr_count($String, "<?xml")){
			$Start = strpos($String, "<?xml")+5;
			$End = strpos($String, ">", $Start);
			$Content = substr($String, $Start, $End-$Start);
			$EncodingStart = strpos($Content, "encoding=\"") + 10;
			$EncodingEnd = strpos($Content, "\"", $EncodingStart);
			$Encoding = substr($Content, $EncodingStart, $EncodingEnd-$EncodingStart);
		}
		else{
			$Encoding = "";
		}
		return $Encoding;
	}
 
 	# Delete elements
	function xml_deleteelements($String, $Char){
		while(substr_count($String, "<$Char")){
			$Start = strpos($String, "<$Char");
			$End = strpos($String, ">", $Start+1) + 1;
			$String = substr($String, 0, $Start).substr($String, $End);
		}
		return $String;
	}
 
	# Read XML and transform into array
	function xml_readxml($String, $Data, $Encoding = ''){
		while($Node = $this->xml_nextnode($String)){
			$Attr = $this->xml_nodeattributes($String, $Encoding);
			
			$TmpData = "";
			$Start = strpos($String ,">", strpos($String, "<$Node")) + 1;
			$End = strpos($String, "</$Node>", $Start);
			$ThisContent = trim(substr($String, $Start, $End - $Start));
			$String = trim(substr($String,$End+strlen($Node)+3));
		
			if(substr_count($ThisContent, "<")){
				$TmpData = $this->xml_readxml($ThisContent, $TmpData, $Encoding);
				$c = is_array($Data) && array_key_exists($Node, $Data)?count($Data[$Node]):0;
				$Data[$Node][$c] = $TmpData;
				if(count($Attr) > 0){
					$Data[$Node."@"][$c] = $Attr;					
				}
			}
			else{
				if($Encoding == "UTF-8"){
					$ThisContent = utf8_decode($ThisContent);
				}
				$ThisContent = str_replace("&gt;", ">", $ThisContent);
				$ThisContent = str_replace("&lt;", "<", $ThisContent);
				$ThisContent = str_replace("&quote;", "\"", $ThisContent);
				$ThisContent = str_replace("&#39;", "'", $ThisContent);
				$ThisContent = str_replace("&amp;", "&", $ThisContent);
				$c = is_array($Data) && array_key_exists($Node, $Data)?count($Data[$Node]):0;
				$Data[$Node][$c]= $ThisContent;
				if(count($Attr) > 0){
					$Data[$Node."@"][$c] = $Attr;
				}
			}
		}
		unset($String);
		return $Data;
	}
 
	# Get next node
	function xml_nextnode($String){
		if(substr_count($String, "<") != substr_count($String, "/>")){
			$Start = strpos($String, "<") + 1;
			while(substr($String, $Start, 1) == "/"){
				if(substr_count($String, "<")){
					unset($String);
					return "";
				}
				$Start = strpos($String, "<", $Start) + 1;
			}
			$End = strpos($String, ">", $Start);
			$Node = substr($String, $Start, $End - $Start);
			if(strlen($Node)> 0 && $Node[strlen($Node) - 1] == "/"){
				$String = substr($String, $End + 1);
				$Node = $this->xml_nextnode($String);
			}
			else{
				if(substr_count($Node, " ")){
					$Node=substr($Node, 0, strpos($String, " ", $Start) - $Start);
				}
			}
		}
		unset($String);
		return isset($Node)?$Node:"";
	}
	
	# Get node attributes
	function xml_nodeattributes($String, $Encoding = ''){
		if(substr_count($String, "<") != substr_count($String, "/>")){
			$Start = strpos($String, "<") + 1;
			while(substr($String, $Start, 1) == "/"){
				if(substr_count($String, "<")){
					unset($String);
					return "";
				}
				$Start = strpos($String, "<", $Start) + 1;
			}
			$End = strpos($String, ">", $Start);
			$Node = substr($String, $Start, $End - $Start);
			if($Node[strlen($Node) - 1] == "/"){
				$String = substr($String, $End + 1);
				//$Node = $this->xml_nextnode($String);
			}
			else{
				if(substr_count($Node, " ")){
					$aNode = $Node;
					$Node=substr($Node, 0, strpos($String, " ", $Start) - $Start);
					$Attr = trim(substr($aNode, strlen($Node), strlen($aNode) - strlen($Node)));
					
					//parse attributes
					$AttrParts = explode('"', $Attr);
					$Attributes = array();
					for($i=0; $i < floor(count($AttrParts) / 2); $i++){
						$AttrName = trim(substr($AttrParts[$i*2], 0, strlen($AttrParts[$i*2])-1));
						$AttrValue = $AttrParts[$i*2+1];
						
						if($Encoding == "UTF-8"){
							$AttrValue = utf8_decode($AttrValue);
						}
						$AttrValue = str_replace("&gt;", ">", $AttrValue);
						$AttrValue = str_replace("&lt;", "<", $AttrValue);
						$AttrValue = str_replace("&quote;", "\"", $AttrValue);
						$AttrValue = str_replace("&#39;", "'", $AttrValue);
						$AttrValue = str_replace("&amp;", "&", $AttrValue);
						$Attributes[$AttrName] = $AttrValue;
					}
				}
			}
		}
		unset($String);
		return isset($Attributes)?$Attributes:array();
	}
	
	# Converts <tag/> to <tag></tag>
	function xml_transform_selfclosed($xml){
		while($EndPos = strpos($xml, "/>")){ 
			$EndPos = strpos($xml, "/>");
			$PreNode = substr($xml, 0, $EndPos);
			$Node = trim(substr($PreNode, strrpos($PreNode, "<")+1));
			
			if(substr_count($Node, " ")){
				$NodeName = trim(substr($Node, 0, strpos($Node, " ")));
			}
			else{
				$NodeName = trim($Node);
			}
			$xml = substr($xml, 0, $EndPos)."></".$NodeName.">".substr($xml, $EndPos+2);
		}
		
		return $xml;
	}
}
<?php 

/**
 * @class Regions 
 */
class Regions
{
	var $db;
	
	function Regions($db)
	{
		$this->db = $db;
		return $this;
	}
	
	function getCountriesList($sortorder = "name_asc"){
		$order_by = "name, priority DESC";
		switch($sortorder){
			//note - here reverse sort order for priority
			case "name_asc" : $order_by = "name, country_priority DESC"; break;
			case "name_desc" : $order_by = "name DESC, country_priority DESC"; break;
			case "priority_asc" : $order_by = "country_priority DESC, name"; break;
			case "priority_desc" : $order_by = "country_priority, name"; break;
		}
		$this->db->query("SELECT ".DB_PREFIX."countries.* FROM ".DB_PREFIX."countries ORDER BY ".$order_by);
		return $this->db->getRecords();
	}
	
	function getStatesList($coid, $sortorder = "name_asc"){
		$order_by = "name";//, priority DESC";
		switch($sortorder){
			//note - here reverse sort order for priority
			case "name_asc" : $order_by = "name"; break;
			case "name_desc" : $order_by = "name DESC"; break;
			//case "priority_asc" : $order_by = "country_priority DESC, name"; break;
			//case "priority_desc" : $order_by = "country_priority, name"; break;
		}
		$this->db->query("SELECT ".DB_PREFIX."states.* FROM ".DB_PREFIX."states WHERE coid='".intval($coid)."' ORDER BY ".$order_by);
		return $this->db->getRecords();
	}
	
	function getCountriesABC(){
		$this->db->query("SELECT UCASE(LEFT(LTRIM(name), 1)) AS letter FROM ".DB_PREFIX."countries GROUP BY letter ORDER BY letter");
		$abc = array();
		while($this->db->moveNext()){
			$abc[] = $this->db->col["letter"];
		}
		return $abc;
	}
	
	function getStatesABC($coid){
		$this->db->query("SELECT UCASE(LEFT(LTRIM(name), 1)) AS letter FROM ".DB_PREFIX."states WHERE coid='".intval($coid)."' GROUP BY letter ORDER BY letter");
		$abc = array();
		while($this->db->moveNext()){
			$abc[] = $this->db->col["letter"];
		}
		return $abc;
	}
	
	function getCountriesListByLetter($letter, $sortorder = "name_asc"){
		$order_by = "name, priority DESC";
		switch($sortorder){
			//note - here reverse sort order for priority
			case "name_asc" : $order_by = "name, country_priority DESC"; break;
			case "name_desc" : $order_by = "name DESC, country_priority DESC"; break;
			case "priority_asc" : $order_by = "country_priority DESC, name"; break;
			case "priority_desc" : $order_by = "country_priority, name"; break;
		}
		$this->db->query("SELECT ".DB_PREFIX."countries.* FROM ".DB_PREFIX."countries WHERE UCASE(LEFT(LTRIM(name), 1))='".$this->db->escape($letter)."' ORDER BY ".$order_by);
		return $this->db->getRecords();
	}
	
	function getStatesListByLetter($coid, $letter, $sortorder = "name_asc"){
		$order_by = "name";//, priority DESC";
		switch($sortorder){
			//note - here reverse sort order for priority
			case "name_asc" : $order_by = "name"; break;
			case "name_desc" : $order_by = "name DESC"; break;
			//case "priority_asc" : $order_by = "country_priority DESC, name"; break;
			//case "priority_desc" : $order_by = "country_priority, name"; break;
		}
		$this->db->query("SELECT ".DB_PREFIX."states.* FROM ".DB_PREFIX."states WHERE coid='".intval($coid)."' AND UCASE(LEFT(LTRIM(name), 1))='".$this->db->escape($letter)."' ORDER BY ".$order_by);
		return $this->db->getRecords();
	}
	
	function getCountryById($coid){
		$this->db->query("SELECT * FROM ".DB_PREFIX."countries WHERE coid='".$this->db->escape($coid)."'");
		if($this->db->moveNext()){
			return $this->db->col;
		}
		return false;
	}
	
	function getStateById($stid){
		$this->db->query("SELECT * FROM ".DB_PREFIX."states WHERE stid='".$this->db->escape($stid)."'");
		if($this->db->moveNext()){
			return $this->db->col;
		}
		return false;
	}
	
	function addCountry($data)
	{
		$this->db->reset();
		$this->db->assignStr("name", $data["name"]);
		$this->db->assignStr("available", isset($data["available"])?"Yes":"No");
		$this->db->assignStr("country_priority", $data["country_priority"]);
		$this->db->assignStr("iso_a2", $data["iso_a2"]);
		$this->db->assignStr("iso_a3", $data["iso_a3"]);
		$this->db->assignStr("iso_number", $data["iso_number"]);
		return $this->db->insert(DB_PREFIX."countries");
	}
	
	function addState($data)
	{
		$this->db->reset();
		$this->db->assignStr("coid", $data["coid"]);
		$this->db->assignStr("name", $data["name"]);
		$this->db->assignStr("short_name", $data["short_name"]);
		//$this->db->assignStr("country_priority", $data["country_priority"]);
		//$this->db->assignStr("iso_a2", $data["iso_a2"]);
		//$this->db->assignStr("iso_a3", $data["iso_a3"]);
		//$this->db->assignStr("iso_number", $data["iso_number"]);
		return $this->db->insert(DB_PREFIX."states");
	}
	
	function saveCountriesData($data)
	{
		foreach ($data["name"] as $coid=>$name)
		{
			$this->db->reset();
			$this->db->assignStr("name", $data["name"][$coid]);
			$this->db->assignStr("available", isset($data["available"][$coid])?"Yes":"No");
			$this->db->assignStr("country_priority", $data["country_priority"][$coid]);
			$this->db->assignStr("iso_a2", $data["iso_a2"][$coid]);
			$this->db->assignStr("iso_a3", $data["iso_a3"][$coid]);
			$this->db->assignStr("iso_number", $data["iso_number"][$coid]);
			$this->db->update(DB_PREFIX."countries", "WHERE coid='".intval($coid)."'");
		}
	}
	
	function saveStatesData($data)
	{
		foreach($data["name"] as $stid=>$name){
			$this->db->reset();
			$this->db->assignStr("name", $data["name"][$stid]);
			$this->db->assignStr("short_name", $data["short_name"][$stid]);
			$this->db->update(DB_PREFIX."states", "WHERE stid='".intval($stid)."'");
		}
	}
	
	function deleteCountry($coid)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."states WHERE coid='".$coid."'");
		$this->db->query("DELETE FROM ".DB_PREFIX."countries WHERE coid='".$coid."'");
	}
	
	function deleteState($stid)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."states WHERE stid='".$stid."'");
	}

	function getCountriesOptions($show_all = true)
	{
		$this->db->query("
			SELECT coid, name
			FROM ".DB_PREFIX."countries c
			".(!$show_all ? " WHERE c.available = 'Yes'" : "")."
			ORDER BY c.country_priority DESC, c.name
		");

		$r = array();
		while ($row = $this->db->moveNext())
		{
			$r[$row['coid']] = $row['name'];
		}
		return $r;
	}

	function getCountriesStates($show_all = true){
		$this->db->query("
			SELECT 
				".DB_PREFIX."countries.coid, ".DB_PREFIX."countries.name AS country_name,
				".DB_PREFIX."states.stid, ".DB_PREFIX."states.name AS state_name
			FROM ".DB_PREFIX."countries
			LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.coid = ".DB_PREFIX."countries.coid
			".(!$show_all?" WHERE ".DB_PREFIX."countries.available = 'Yes' ":"")."
			ORDER BY ".DB_PREFIX."countries.country_priority DESC, ".DB_PREFIX."countries.name, ".DB_PREFIX."states.name
		");
		$r = array();
		$coid = false;
		while($this->db->moveNext()){
			if(!array_key_exists($this->db->col["coid"], $r)){
				$r[$this->db->col["coid"]] = array(
					"country_name"=>$this->db->col["country_name"],
					"states"=>is_numeric($this->db->col["stid"])?array():false
				);
			}
			if(is_array($r[$this->db->col["coid"]]["states"])){
				$r[$this->db->col["coid"]]["states"][$this->db->col["stid"]] = $this->db->col["state_name"];
			}
			
			$coid = $this->db->col["coid"];
		}
		return $r;
	}
	
	function getCountriesStatesForSite()
	{
		$this->db->query("
			SELECT 
				".DB_PREFIX."countries.coid, ".DB_PREFIX."countries.name AS country_name,
				".DB_PREFIX."states.stid, ".DB_PREFIX."states.name AS state_name, ".DB_PREFIX."countries.zip_required, ".DB_PREFIX."countries.has_provinces
			FROM ".DB_PREFIX."countries
			LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.coid = ".DB_PREFIX."countries.coid
			WHERE ".DB_PREFIX."countries.available = 'Yes'
			ORDER BY ".DB_PREFIX."countries.country_priority DESC, ".DB_PREFIX."countries.name, ".DB_PREFIX."states.name
		");
		
		$r = array();
		
		while ($this->db->moveNext())
		{
			$_c_id = 'c'.str_pad($this->db->col["coid"], 5, '0', STR_PAD_LEFT);
			
			if (!array_key_exists($_c_id, $r))
			{
				$r[$_c_id] = array(
					"country_name"=>$this->db->col["country_name"],
					"states"=>is_numeric($this->db->col["stid"]) ? array() : false,
					"zip_required" => $this->db->col["zip_required"],
					"has_provinces" => $this->db->col["has_provinces"]
				);
			}
			
			if (is_array($r[$_c_id]["states"]))
			{
				$_s_id = 's'.str_pad($this->db->col["stid"], 5, '0', STR_PAD_LEFT);
				$r[$_c_id]["states"][$_s_id] = $this->db->col["state_name"];
			}
		}
		return $r;
	}
	
}
<?php
# $Date: 2007-02-08 21:22:23 +0000 (Thu, 08 Feb 2007) $
# iono Licensing Integration #5 (curl) - http://www.olate.co.uk

/**
* iono License Key File Handling
*
* @copyright Olate Ltd 2007
* @link http://www.olate.co.uk
* @version 1.0.0
* @package iono
*/
class IonoKeys
{
	/**
	* @var string The user's license key
	* @access private
	*/
	var $license_key;
	
	/**
	* @var string The iono root site location
	* @access private
	*/
	var $home_url_site = 'https://account.pinnaclecart.com/account/';
	
	/**
	* @var int The iono root site location port for access
	* @access private
	*/
	var $home_url_port = 80;
	
	/**
	* @var string The iono location
	* @access private
	*/
	var $home_url_iono = 'https://account.pinnaclecart.com/iono/remote.php';

	/**
	* @var string The location of the key file to use
	* @access private
	*/
	var $key_location = 'content/cache/license/license.key';
	
	/**
	* @var string Remote Authentication String from your iono installation
	* @access private
	*/
	var $remote_auth;
	
	/**
	* @var int The maximum age of the key file before it is regenerated (seconds)
	* @access private
	*/
	var $key_age;
	
	/**
	* @var array The data stored in the key
	* @access private
	*/
	var $key_data;
	
	/**
	* @var int Current timestamp. Needs to be constant throughout class so is set here
	* @access private
	*/
	var $now;
	
	/**
	* @var int The result of the key actions
	* @access public
	*/
	var $result;
	var $old_result;
	
	var $status;
	var $old_status;

	
	var $ssl_pem_path = null;
	
	/**
	 * @var proxy curl vars
	 */
	var $proxy_used = false;
	var $proxy_address = "";
	var $proxy_port = 0;
	var $proxy_requires_auth = false;
	var $proxy_username = "";
	var $proxy_password = "";
	
	var $max_product_count = 0;
	var $level = 1;
	var $intuit_anywhere = false;
	var $hosted_by_ddm = false;

	var $recurring_visible = false;
	var $recurring_can_enable = false;

	var $products_filters_visible = false;
	var $products_filters_can_enable = false;

	private $_db = null;
	private $_settings = null;
	
	/**
	 * Please DO NOT INSERT VALUES
	 * APPEND NEW VALUES TO THE END OF ARRAY
	 * c_p = Products count
	 * c_u = Users count
	 * c_n = Newsletters sent
	 * c_nu = Newsletters users
	 * c_e = Newsletters emails
	 */
	private $_list = array(
		"DesignSkinName", "DesignSkinTheme", "DesignTopMenuStyle", "ActiveLanguage",
		"CatalogThumbSize", "CatalogThumbType", "CatalogProductPage", "CatalogProductPageBottom",
		"TaxAddress", "LayoutSiteWidth", "LocalizationWeightUnits", "LocalizationLengthUnits",
		"InventoryStockUpdateAt", "AppVer", "AppEncoder", "c_p",  "c_u",  "c_n",  "c_nu",  "c_e", 
		"EmailSendmailEngine", "GlobalHttpUrl", "GlobalHttpsUrl"
	);
	
	/**
	 * Please DO NOT INSERT/MODIFY VALUES
	 * APPEND NEW VALUES TO THE END OF ARRAY
	 */
	private $_map = array(
		array(
			"DesignTopMenuAvailable"=>"YES", "DiscountsActive"=>"YES", "DiscountsPromo"=>"YES", "CatalogViewText"=>"YES", 
			"CatalogViewThumb1"=>"YES", "CatalogViewThumb1Clean"=>"YES", "CatalogViewThumb2"=>"YES", "CatalogViewDisplay"=>"YES", 
			"CatalogViewDisplayBox"=>"YES", "CatalogViewThumb3"=>"YES", "CatalogViewThumb3Clean"=>"YES", "CatalogViewFlexible"=>"YES", 
			"CatalogShowNested"=>"YES", "CatalogManufacturers"=>"YES", "CatalogProductID"=>"YES"
		),
		array(
			"ShippingCalcEnabled"=>"YES", "ShippingHandlingSeparated"=>"1", "ShippingHandlingTaxable"=>"1", "ShippingPreCalcEnabled"=>"YES", 
			"ShippingInternational"=>"YES", "ShippingWithoutMethod"=>"YES", "ShippingShowWeight"=>"YES", "ShippingShowPriceAtProductLevel"=>"YES", 
			"ShippingAllowGetQuote"=>"YES", "ShippingTaxable"=>"0", "ShippingShowAlternativeOnFree"=>"YES",
			"DisplayPricesWithTax"=>"YES", "SearchAutoGenerate"=>"YES", "SearchAutoGenerateLowercase"=>"YES"
		),
		array(
			// KEEP case/inter pack for backwards compatibility
			"USE_MOD_REWRITE"=>"YES", "AffiliateActive"=>"YES", "ShopzillaROITracker"=>"YES", "WholesaleCasePack"=>"YES", 
			"WholesaleInterPack"=>"YES", "VisitorSeePrice"=>"YES", "VisitorMayAddItem"=>"YES", "AllowCreateAccount"=>"YES", 
			"AfterProductAddedGoTo"=>"Current Page", "DisplayTermsAndConditionsCheckbox"=>"Yes", "EnableWishList"=>"Yes", 
			"ClearCartOnLogout"=>"Yes", "CardTypeMustMatchNumber"=>"Yes"
		),
		array(
			"SecurityAccountBlocking"=>"YES", "SecurityAdminIpRestriction"=>"Allow", "SecurityForceCompliantAdminPasswords"=>"YES", "SecurityAdminActivyLogging"=>"YES", 
			"SecurityCCSActive"=>"0", "captchaMethod"=>"reCaptcha", "facebookLikeButtonProduct"=>"Yes", "facebookCommentsProduct"=>"Yes", 
			"twitterTweetButtonProduct"=>"Yes", "diggButtonProduct"=>"Yes", "googleBuzzButtonProduct"=>"Yes", "deliciousButtonProduct"=>"Yes", 
			"MobileSiteEnabled"=>"No", "ForceMobileRedirect"=>"No"
		),
		array(
			"CatalogBestsellersAvailable"=>"YES", "CustomerAlsoBoughtAvailable"=>"YES", "ProxyAvailable"=>"YES", "ProxyRequiresAuthorization"=>"YES",
			//==========================================================================NOTE - Keep DollarDaysUsername here, need for compatibility
			"ReviewsEnable"=>"1", "ReviewsShowSummary"=>"1", "ReviewsAutoApprove"=>"1", "DollarDaysUsername"=>"!", "GiftCardActive"=>"YES", 
			"DigitalProductsActive"=>"YES", "GoogleConversionActive"=>"YES", "GoogleConversionAsync"=>"YES", "GoogleAnalyticsActive"=>"YES", 
			"GoogleAdwordsConversionActive"=>"YES"
		),
		array(
			"doba_username"=>"!", "doba_autosend_orders"=>"Yes", "doba_autofund_orders"=>"Yes", "doba_payment_type"=>"CC", "doba_callback_mode"=>"!None", 
			"doba_cron_update"=>"Yes", "doba_images"=>"!No", "doba_images_secondary"=>"Yes", "mailchimp_enabled"=>"1", 
			"plugin_openapi_active"=>"YES", "testimonials_Enabled"=>"Yes", "testimonials_Use_Rating"=>"Yes", "testimonials_Use_Captcha"=>"Yes"
		),
		array(
			//Keep FrontEndDemo here for compatibility
			"qr_active"=>"Yes", "CatalogUsePriceRanges"=>"1", "StoreClose"=>"Yes", "FrontEndDemo"=>"Yes", "ForceStripSlashes"=>"Yes",
			"CartTypeMustMatchNumber"=>"Yes", "googlePlusButtonProduct"=>"Yes",
			"enable_gift_cert"=>"Yes", "ProductsLocationsSendOption"=>"!off"
		)
	);
	
	/**
	 * Class constructor
	 * @return iono_keys 
	 */
	public function __construct($db = null, $settings = null)
	{
		$this->_db = $db;
		$this->_settings = $settings;
		
		if (is_null($this->ssl_pem_path))
		{
			$this->ssl_pem_path = str_replace('classes', 'ssl', dirname(__FILE__));
		}

		if (defined('BILLING_TEST_MODE') && BILLING_TEST_MODE)
		{
			$this->home_url_iono = 'https://account-staging.pinnaclecart.com/iono/remote.php';
		}
		return $this;
	}

	/**
	 *
	 * @param type $str
	 * @return type 
	 */
	private	function _crypt($str)
	{
		$key = "Akdjasdasjel12093123sda-4324mg=d02309481234dsflksyr4893471209812093";
		$result = '';
		
		for ($i=0; $i<strlen($str); $i++)
		{
			$char = substr($str, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		return base64_encode($result);
	}

	/**
	 *
	 * @param type $str
	 * @return type 
	 */
	private function _decrypt($str)
	{
		$str = base64_decode($str);
		$result = '';
		$key = "Akdjasdasjel12093123sda-4324mg=d02309481234dsflksyr4893471209812093";
		
		for ($i=0; $i<strlen($str); $i++)
		{
			$char = substr($str, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}
	
	/**
	 * Get usage statistics
	 * @return string 
	 */
	public function getStats()
	{
		if ($this->_db == null || $this->_settings == null)
		{
			return false;
		}
		
		/**
		 * v - version
		 * b - boolean settings data
		 * l - text settings data
		 * p - payment gateways
		 * s - shippint gateways
		 * o - orders
		 */
		$stats = array("v"=>1, "b"=>array(), "l"=>array(), "p"=>array(), "s"=>array(), "o"=>array());

		// settings boolean
		foreach ($this->_map as $index=>$group)
		{
			$bit = 1;
			$stats["b"][$index] = 0;
			foreach ($group as $key=>$match)
			{
				if (isset($this->_settings[$key]))
				{
					if ($match != "" && $match[0] == "!")
					{
						if ($this->_settings[$key] != substr($match, 1))
						{
							$stats["b"][$index] += $bit;
						}
					}
					else
					{
						if ($this->_settings[$key] == $match)
						{
							$stats["b"][$index] += $bit;
						}
					}
				}
				$bit *= 2;
			}
		}
		
		// settings other
		foreach ($this->_list as $key)
		{
			$stats["l"][] = isset($this->_settings[$key]) ? $this->_settings[$key] : '';
		}
		
		// payment gw
		$this->_db->query("SELECT id FROM ".DB_PREFIX."payment_methods WHERE active='Yes'");
		while ($c = $this->_db->moveNext()) $stats["p"][] = trim($c["id"]) == "" ? "custom" : trim($c["id"]);
		
		// shipping gw
		$this->_db->query("SELECT CONCAT(carrier_id, '/', method_id) AS x FROM ".DB_PREFIX."shipping_selected GROUP BY x");
		while ($c = $this->_db->moveNext()) $stats["s"][] = $c["x"];
		
		// orders stats
		$this->_db->query("
			SELECT 
				DATE(placed_date) AS d,
				REPLACE(FORMAT(SUM(".DB_PREFIX."orders.total_amount),2), ',', '') AS s,
				REPLACE(FORMAT(AVG(".DB_PREFIX."orders.total_amount),2), ',', '') AS a,
				COUNT(".DB_PREFIX."orders.oid) AS o
			FROM ".DB_PREFIX."orders 
			WHERE 
				removed='No' AND
				status='Completed' AND 
				payment_status='Received' AND
				DATE_ADD(placed_date, INTERVAL 30 DAY) > NOW() 
			GROUP BY d
			ORDER BY d
		");
		
		if ($this->_db->numRows())
		{
			$stats["o"] = $this->_db->getRecords();
		}
		
		// products count
		$this->_db->query("SELECT COUNT(pid) AS c FROM ".DB_PREFIX."products");
		$c = $this->_db->moveNext();
		$stats["l"][array_search("c_p", $this->_list)] = $c["c"];
		
		// users count
		$this->_db->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."users WHERE removed='No'");
		$c = $this->_db->moveNext();
		$stats["l"][array_search("c_u", $this->_list)] = $c["c"];
		
		// emails newsletters archived - KEEP IT FOR LEGACY COMPATIBILITY
		$stats["l"][array_search("c_n", $this->_list)] = 0;
		
		// subscribed users count
		$this->_db->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."users WHERE removed='No' AND receives_marketing='Yes'");
		$c = $this->_db->moveNext();
		$stats["l"][array_search("c_nu", $this->_list)] = $c["c"];

		// subscribed emails count
		$this->_db->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."emails");
		$c = $this->_db->moveNext();
		$stats["l"][array_search("c_e", $this->_list)] = $c["c"];
		
		return base64_encode($this->_crypt(serialize($stats)));
	}
	
	/**
	 * Sets the class vars and then checks the key file.
	 * @param string $license_key The user's license key
	 * @param string $remote_auth The remote authorisation string from iono settings
	 * @param string $key_location The location of the key file to use
	 * @param int $key_age The maximum age of the key file before it is regenerated (seconds) default 15 days (1296000)
	 */
	public function iono_keys_call4520($license_key, $remote_auth, $key_location = 'content/cache/license/license.key', $key_age = 1296000)  //15 days
	{
		// Set the class vars
		$this->license_key = $license_key;
		$this->remote_auth = $remote_auth;
		$this->key_location =  $key_location;
		$this->key_age = 1296000; 
		$key_age = 1296000;
		$this->now = time();
		
		if (empty($license_key))
		{
			$this->result = 4;
			$this->status = "The licence key for this domain has been removed from the application. Please check your license key.";
			return false;
		}
		
		// Does the key exist? If not, then we need to create it. Else read it.
		if (file_exists($this->key_location) && filesize($this->key_location) != 0)
		{
			$time_diff = time() - filemtime($this->key_location);
		}
		
		if (file_exists($this->key_location) && filesize($this->key_location) != 0 && $time_diff < $key_age)
		{
			$this->result = $this->read_key_1980();
			if ($this->result != 1)
			{
				$this->result = $this->generate_key1978();
			}
		}
		else
		{
			$this->result = $this->generate_key1978();
			if (empty($this->result))
			{
				$this->result = $this->read_key_1980();
			}
		}
		
		/**
		 * Enable this for stats generation testing
		$this->result = $this->generate_key1978();
		if (empty($this->result))
		{
			$this->result = $this->read_key_1980();
		}
		 */
		
		unset($this->remote_auth);
		
		return true;
	}
	
	/**
	* Gef the license details form the iono server and writes to the key file
	*
	* Responses:
	* - 8: License disabled
	* - 9: License suspended
	* - 5: License expired
	* - 10: Unable to open file for writing
	* - 11: Unable to write to file
	* - 12: Unable to communicate with iono
	* @return int Response code
	* @access private
	*/
	public function generate_key1978()
	{
		// Build request
		// $this->license_key;
		$request = 'remote=licenses&type=5&license_key='.urlencode(base64_encode($this->license_key));
		$request .= '&host_ip='.urlencode(base64_encode($_SERVER['SERVER_ADDR'])).'&host_name='.urlencode(base64_encode($_SERVER['SERVER_NAME']));
		$request .= '&url='.urlencode(base64_encode($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']));
		$request .= '&s='.urlencode($this->getStats()); // do not base64_encode - function returns base64-encodes
		$request .= '&hash='.urlencode(base64_encode(md5($request)));
		
		$request = $this->home_url_iono."?".$request;
		
		$this->request = $request;
		// New cURL resource
		$ch = curl_init();
		
		//Check proxy
		if ($this->proxy_used)
		{
			curl_setopt($ch, CURLOPT_PROXY, $this->proxy_address.":".$this->proxy_port);
			if ($this->proxy_requires_auth)
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->proxy_username.":".$this->proxy_password);
			}
		}
		
		$home_url_port = 443;
		// Set options
		@set_time_limit(30);
		
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_URL, $request);
		curl_setopt($ch, CURLOPT_PORT, $home_url_port);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'iono (www.olate.co.uk/iono)');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		// Execute
		$content = curl_exec($ch);

		if (curl_errno($ch) > 0)
		{
			curl_error($ch);
			return 12;
		}
		
		// Close
		curl_close($ch);
		
		if (!$content)
		{
			return 12;
		}
		
		// Split up the content
		$string = urldecode($content);
		
		//$string = '1|key|' .(time()+2000) .'|localhost|127.0.0.1';
		$this->reason = $string;
		
		$exploded = explode('|', $string);		
		
		switch ($exploded[0]) // If we have an inactive license, return the status code
		{
			case 0: // Disabled
			{
				return 8;
				break;
			}
			case 2: // Suspended
			{
				return 9;
				break;
			}
			case 3: // Expired
			{
				return 5;
				break;
			}
			case 10: // Invalid key
			{
				$this->status = "License Key Disabled or Invalid";
				return 4;
				break;
			}
		}
		
		$data['license_key'] = $exploded[1];
		$data['expiry']	= $exploded[2];
		$data['hostname'] = $exploded[3];
		$data['ip']	= $exploded[4];
		
		if (array_key_exists(5, $exploded))
		{
			$data['max_product_count'] = intval($exploded[5]);
			$this->max_product_count = intval($exploded[5]);
		}
		else
		{
			$data['max_product_count'] = 0;
			$this->max_product_count = 0;
		}
		
		if (array_key_exists(6, $exploded))
		{
			$data['level'] = intval($exploded[6]);
			$this->level = intval($exploded[6]);
		}
		else
		{
			$data['level'] = 1;
			$this->level = 1;
		}
		
		if (array_key_exists(7, $exploded))
		{
			$data['intuit_anywhere'] = intval($exploded[7]) == 1;
			$this->intuit_anywhere = intval($exploded[7]) == 1;
		}
		else
		{
			$data['intuit_anywhere'] = false;
			$this->intuit_anywhere = false;
		}
		
		if (array_key_exists(8, $exploded))
		{
			$data['hosted_by_ddm'] = intval($exploded[8]) == 1;
			$this->hosted_by_ddm = intval($exploded[8]) == 1;
		}
		else
		{
			$data['hosted_by_ddm'] = false;
			$this->hosted_by_ddm = false;
		}

		/**
		 * Recurring options
		 */
		if (array_key_exists(10, $exploded))
		{
			$data['recurring_visible'] = intval($exploded[11]);
			$this->recurring_visible = intval($exploded[11]);
		}
		else
		{
			$data['recurring_visible'] = 0;
			$this->recurring_visible = 0;
		}

		if (array_key_exists(11, $exploded))
		{
			$data['recurring_can_enable'] = intval($exploded[12]);
			$this->recurring_can_enable = intval($exploded[12]);
		}
		else
		{
			$data['recurring_can_enable'] = 0;
			$this->recurring_can_enable = 0;
		}

		/**
		 * Products filters options
		 */
		if (array_key_exists(12, $exploded))
		{
			$data['products_filters_visible'] = intval($exploded[13]);
			$this->products_filters_visible = intval($exploded[13]);
		}
		else
		{
			$data['products_filters_visible'] = 0;
			$this->products_filters_visible = 0;
		}

		if (array_key_exists(13, $exploded))
		{
			$data['products_filters_can_enable'] = intval($exploded[14]);
			$this->products_filters_can_enable = intval($exploded[14]);
		}
		else
		{
			$data['products_filters_can_enable'] = 0;
			$this->products_filters_can_enable = 0;
		}

		$data['timestamp'] = $this->now;
		
		// On first generation the hostname and IP will be blank
		// So set to current values
		if (empty($data['hostname']))
		{
			$data['hostname'] = $_SERVER['SERVER_NAME'];
		}
		
		if (empty($data['ip']))
		{
			$data['ip'] = $_SERVER['SERVER_ADDR'];
		}
		
		$data_encoded = serialize($data);
		$data_encoded = base64_encode($data_encoded);
		$data_encoded = md5($this->now.$this->remote_auth).$data_encoded;
		$data_encoded = strrev($data_encoded);
		$data_encoded_hash = sha1($data_encoded.$this->remote_auth);
		// $this->key_location;
		$fp = fopen($this->key_location, 'w+');
		if ($fp)
		{
			$cont = wordwrap($data_encoded.$data_encoded_hash, 40, "\n", true);
			$fp_write = fwrite($fp, $cont);
			
			if (!$fp_write)
			{
				return 11; // Unable to write to file
			}			
			fclose($fp);
			return 1;
		}
		else
		{
			return 10; // Unable to open file for writing
		}
	}
	
	/**
	 * checks does current website allowed for free trial
	 * 
	 * Responses
	 * 21 - allow free trial
	 * 27 - license for this host/ip already exists, trial impossible
	 * 28 - unknown request
	 * 29 - connection problems
	 * @return int Response code
	 */
	public function check_trial_2005()
	{
		// Build request
		// type = 1 - check does host already has license
		$request = 'remote=trial&type=1&host_ip='.urlencode(base64_encode($_SERVER['SERVER_ADDR'])).'&host_name='.urlencode(base64_encode($_SERVER['SERVER_NAME']));
		$request .= '&hash='.urlencode(base64_encode(md5($request)));
		
		$request = $this->home_url_iono."?".$request;
		$this->request = $request;
		$ch = curl_init();
		
		//Check proxy
		if ($this->proxy_used)
			{
			curl_setopt($ch, CURLOPT_PROXY, $this->proxy_address.":".$this->proxy_port);
			if ($this->proxy_requires_auth)
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->proxy_username.":".$this->proxy_password);
			}
		}
		
		$home_url_port = 443;

		// Set options
		@set_time_limit(30);

		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_URL, $request);
		curl_setopt($ch, CURLOPT_PORT, $home_url_port);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'iono (www.olate.co.uk/iono)');

		// Execute
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$content = curl_exec($ch);

		if (curl_errno($ch) > 0 || trim($content) == "")
		{
			return 29; // cant connect to a server
		}

		// Close
		curl_close($ch);
		
		$e = explode("|", $content);
		$trial_status = $e[0];
		return $trial_status;
	}
	
	/**
	 * Validates email address
	 *
	 * @param string $email_address
	 * @return bool
	 */
	public function validate_email($email_address)
	{
		$pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
		if (!preg_match($pattern, $email_address))
		{
    		return false;
		}
		list($Username, $Domain) = explode("@", $email_address);
		
		if (getmxrr($Domain, $MXHost))
		{
			return true;
		}
		else
		{
			if (@fsockopen($Domain, 25, $errno, $errstr, 30))
			{
				return true; 
			}
			else
			{
				return false; 
			}
		}
	}
	
	/**
	 * Generates new trial license key
	 *
	 * @param array $contact
	 * @return int Pesponse status
	 */
	public function create_trial_license($contact)
	{
		if (!is_writeable("content/engine/engine_config.php"))
		{
			$this->status = "Your config file is not writable (content/engine/engine_config.php)";
			return false; 
		}
		
		//if((file_exists($this->key_location) && !is_writeable($this->key_location)) || (!is_writeable(dirname(file_exists($this->key_location))))){ $this->status = "Your license key file is not writeable"; return false; }
		
		if (file_exists($this->key_location))
		{
			if (!is_writeable($this->key_location))
			{
				$this->status = "Your license key file is not writable (content/cache/license/license.key)"; return false; 
			}
		}
		else
		{
			if (!is_writeable(dirname($this->key_location)))
			{
				$this->status = "Your license key file directory is not writable (content/engine)"; return false; 
			}
		}
		
		if ($contact["first_name"] == "") { $this->status = "Please enter first name"; return false; }
		if ($contact["last_name"] == "") { $this->status = "Please enter last name"; return false; }
		if ($contact["company"] == "") { $this->status = "Please enter company name"; return false; }
		if ($contact["address1"] == "") { $this->status = "Please enter address line 1"; return false; }
		if ($contact["city"] == "") { $this->status = "Please enter city"; return false; }
		if ($contact["state"] == "") { $this->status = "Please enter state or province"; return false; }
		if ($contact["country"] == "") { $this->status = "Please select country"; return false; }
		if ($contact["zip"] == "") { $this->status = "Please enter zip code"; return false; }
		if ($contact["phone"] == "") { $this->status = "Please enter phone number"; return false; }
		if (!$this->validate_email($contact["email"])) { $this->status = "Please enter valid email address"; return false; }
		if ($contact["email"] != $contact["email2"]) { $this->status = "Email address and confirmation are different"; return false; }
		if ($contact["hear"] == "") { $this->status = "How did you hear about us?"; return false; }
		
		// Build request
		// type = 2 - add trial user
		$request = 'remote=trial&type=2&host_ip='.urlencode(base64_encode($_SERVER['SERVER_ADDR'])).'&host_name='.urlencode(base64_encode($_SERVER['SERVER_NAME']));
		foreach ($contact as $key=>$value)
		{
			$request.="&".$key."=".urlencode(base64_encode($value));
		}
		
		$request .= '&hash='.urlencode(base64_encode(md5($request)));
		
		$request = $this->home_url_iono."?".$request;
		$this->request = $request;
		$ch = curl_init();
		
		//Check proxy
		if ($this->proxy_used)
		{
			curl_setopt($ch, CURLOPT_PROXY, $this->proxy_address.":".$this->proxy_port);
			if ($this->proxy_requires_auth)
			{
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $this->proxy_username.":".$this->proxy_password);
			}
		}
		
		$home_url_port = 443;
		// Set options
		@set_time_limit(30);
		
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_URL, $request);
		curl_setopt($ch, CURLOPT_PORT, $home_url_port);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'iono (www.olate.co.uk/iono)');

		// Execute
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_setopt($ch, CURLOPT_CAPATH, $this->ssl_pem_path);
		//curl_setopt($ch, CURLOPT_CAINFO, $this->ssl_pem_path.'/cacert.pem');
		$content = curl_exec($ch);
		if (curl_errno($ch) > 0 || trim($content) == "")
		{
			return "29"; // cant connect to a server
		}		
		// Close
		curl_close($ch);
		
		$e = explode("|", $content);
		$trial_status = $e[0];
		if ($trial_status == "21")
		{
			$license_key = base64_decode(urldecode($e[1]));
	
			if (trim($license_key) != "")
			{
				$cf = fopen("content/engine/engine_config.php", "w+");
				if ($cf)
				{
					fputs($cf, "<?php\n");
					fputs($cf, "\tdefine('DB_HOST', '".DB_HOST."');\n");
					fputs($cf, "\tdefine('DB_USER', '".DB_USER."');\n");
					fputs($cf, "\tdefine('DB_PASSWORD', '".DB_PASSWORD."');\n");
					fputs($cf, "\tdefine('DB_NAME', '".DB_NAME."');\n");
					fputs($cf, "\tdefine('DB_PREFIX', '".DB_PREFIX."');\n");
					fputs($cf, "\tdefine('LICENSE_NUMBER', '".$license_key."');\n");
					fputs($cf, "?>");
					fclose($cf);
					$this->license_key = $license_key;
					$this->generate_key1978();
					return "21";
				}
				else
				{
					$this->status = "Error writing config file";
					return "28";
				}
			}
			else
			{
				$this->status = "Returned License key is empty";
				return "28";
			}
		}
		return "28";
	}
	
/**
	* Read the key file and then return a response code
	*
	* Responses:
	* - 0: Unable to read key
	* - 1: Everything is OK
	* - 2: SHA1 hash incorrect (key may have been tampered with)
	* - 3: MD5 hash incorrect (key may have been tampered with)
	* - 4: License key does not match key string in key file
	* - 5: License has expired
	* - 6: Host name does not match key file
	* - 7: IP does not match key file
	* @return int Response code
	* @access private
	*/
	public function read_key_1980()
	{
		$key = file_get_contents($this->key_location);
		
		if ($key !== false)
		{
			$key = str_replace("\n", '', $key); // Remove the line breaks from the key string
			// Split out SHA1 hash from the key data
			$key_string = substr($key, 0, strlen($key)-40);
			$key_sha_hash = substr($key, strlen($key)-40, (strlen($key)));
			$this->key_sha = $key_sha_hash;
			$this->key_sha_comp = sha1($key_string.$this->remote_auth);
			if (sha1($key_string.$this->remote_auth) == $key_sha_hash) // Compare SHA1 hash to the key data
			{
				$key = strrev($key_string); // Back the right way around
				
				$key_hash = substr($key, 0, 32); // Get the MD5 hash of the data from the string
				$key_data = substr($key, 32); // Get the data from the string
				
				$key_data = base64_decode($key_data);
				$key_data = unserialize($key_data);
				if (md5($key_data['timestamp'].$this->remote_auth) == $key_hash) // Check the MD5 hash
				{
					$this->max_product_count = isset($key_data['max_product_count']) ? $key_data['max_product_count'] : 0;
					$this->level = isset($key_data['level']) ? $key_data['level'] : 0;

					$this->recurring_visible = isset($key_data['recurring_visible']) ? $key_data['recurring_visible'] : false;
					$this->recurring_can_enable = isset($key_data['recurring_can_enable']) ? $key_data['recurring_can_enable'] : false;

					$this->products_filters_visible = isset($key_data['products_filters_visible']) ? $key_data['products_filters_visible'] : false;
					$this->products_filters_can_enable = isset($key_data['products_filters_can_enable']) ? $key_data['products_filters_can_enable'] : false;

					$this->intuit_anywhere = isset($key_data['intuit_anywhere']) ? $key_data['intuit_anywhere'] : false;
					$this->hosted_by_ddm = isset($key_data['hosted_by_ddm']) ? $key_data['hosted_by_ddm'] : false;

					// Is it more than $this->key_age seconds old?
					if (($this->now - $key_data['timestamp']) >= $this->key_age){
						if (is_file($this->key_location)) @unlink($this->key_location);
						$this->result = $this->generate_key1978();
						if(empty($this->result)){
							$this->result = $this->read_key_1980();
						}
						return 1; // Have to return here because there is a 1 second delay due to the nature of time()
					}
					else
					{
						$this->key_data = $key_data;
						
						if ($key_data['license_key'] != $this->license_key)
						{
							return 4; // License key does not match key string in key file
						}
						
						if ($key_data['expiry'] <= $this->now && $key_data['expiry'] != 1)
						{
							return 5; // License key does not match key string in key file
						}
						
						// Do we have multiple hostnames?
						if (substr_count($key_data['hostname'], ',') == 0)
						{ // No
							if ($key_data['hostname'] != $_SERVER['SERVER_NAME'] && !empty($key_data['hostname']))
							{
								return 6; // Host name does not match key file
							}
						}
						else
						{ // Yes
							$hostnames = explode(',', $key_data['hostname']);
							
							if (!in_array($_SERVER['SERVER_NAME'], $hostnames))
							{
								return 6; // Host name is not in key file
							}
						}
						

						// Do we have multiple IPs?
						if (substr_count($key_data['ip'], ',') == 0)
						{ // No
							if ($key_data['ip'] != $_SERVER['SERVER_ADDR'] && !empty($key_data['ip']))
							{
								return 7; // IP does not match key file
							}
						}
						else
						{ // yes
							$ips = explode(',', $key_data['ip']);
							
							if (!in_array($_SERVER['SERVER_ADDR'], $ips))
							{
								return 7; // IP is not in key file
							}
						}
						
						return 1;
					}
				}
				else
				{
					return 3; // MD5 hash incorrect (key may have been tampered with)
				}
			}
			else
			{
				return 2; // SHA1 hash incorrect (key may have been tampered with)
			}
		}
		else
		{
			return $this->result = $this->generate_key1978();
		}
	}
	
	/**
	* Returns array of key data
	*
	* @return array Array of data in the key file
	*/
	public function get_data()
	{
		return $this->key_data;
	}
	
	/**
	 * 
	 */
	public function get_status_1980() 
	{
		$this->status = "";
		switch($this->result)
		{
			case 1:
			{
				$this->status = "Success";
				break;
			}
			case 2:
			{
				$this->status = "SHA1 Incorrect (".$this->key_sha." vs. ".$this->key_sha_comp.")";
				break;
			}
			case 3:
			{
				$this->status = "MD5 Incorrect";
				break;
			}
			case 4:
			{
				$this->status = "License Key does not match key string in key file";
				break;
			}
			case 5:
			{
				$this->status = "License Key expired.";
				break;
			}
			case 6:
			{
				$this->status = "Host name does not match key file";
				break;
			}
			case 7:
			{
				$this->status = "IP does not match key file";
				break;
			}
			case 8:
			{
				$this->status = "License Key Disabled or Invalid. Please contact software vendor.";
				break;
			}
			case 9:
			{
				$this->status = "License Key Suspended. Please contact software vendor.";
				break;
			}
			case 10:
			{
				$this->status = "Unable to open license.key file for writing. Please check write permissions.";
				break;
			}
			case 11:
			{
				$this->status = "Unable to write to file. Please check write permissions.";
				break;
			}	
			case 12:
			{
				$this->status = "Unable to verify license key. Please check if your hosting provider requires connecting to a proxy server to communicate outside it's datacenter";
				break;
			}
			default:
			{
				$this->status = "Unknown error";
				break;
			}
		}
		//$this->status .= " (".$this->result.")";
	}
}

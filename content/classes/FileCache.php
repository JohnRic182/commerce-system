<?php


class FileCache extends Cache
{
	public $cacheDir = 'content/cache/tmp/';
	
	/**
	 * 
	 */
	public function getFileName($id)
	{
		return $this->cacheDir.$id.".cache";
	}

	/**
	 * [__construct description]
	 * @param [type] $cacheDir [description]
	 */
	public function __construct($cacheDir = null)
	{
		if (!is_null($cacheDir))
		{
			$this->cacheDir = $cacheDir;
		}
	}
	
	/**
	 * [getCachedData description]
	 * @param  [type] $id            [description]
	 * @param  [type] $cacheLifeTime [description]
	 * @param  [type] $default       [description]
	 * @return [type]
	 */
	public function getCachedData($id, $cacheLifeTime, $default = null)
	{
		$this->setResyncCache(false);

		$filename = $this->getFileName($id);

		if (is_file($filename) && filemtime($filename) + $cacheLifeTime > time())
		{
			$f = @fopen($filename, "rb");
			if ($f)
			{
				flock($f, LOCK_SH);  
				$data = @unserialize(@fread($f, filesize($filename)));
				flock($f, LOCK_UN);
				@fclose($f);

				if ($data)
				{
					return $data;
				}
				else
				{
					return $default;
				}
			}
		}
		
		$this->setResyncCache(true);

		return false;
	}

	/**
	 * [cacheData description]
	 * @param  [type] $id   [description]
	 * @param  [type] $data [description]
	 * @return [type]
	 */
	public function cacheData($id, $data)
	{
		$filename = $this->getFileName($id);

		$f = @fopen($filename, "wb");
		if ($f && @flock($f, LOCK_EX) && @fwrite($f, serialize($data)))
		{
			flock($f, LOCK_UN);
			@fclose($f);
		}
		else
		{
			if ($f)
			{
				flock($f, LOCK_UN);
				@fclose($f);
			}
			unlink($filename);
		}
	}
}
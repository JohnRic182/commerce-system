<?php

/**
 * Class CartPlugin
 */
class CartPlugin
{
	var $plugin_group = "";
	var $plugin_caption = "Plugin";
	var $plugin_description = "";

	var $db = false;
	var $settings = false;
	var $request = false;

	var $interface;

	function cartPlugin(){
		return $this;
	}

	function getPluginGroup(){
		return $this->plugin_group;
	}

	function getPluginCaption(){
		return $this->plugin_caption;
	}

	function getPluginDescription(){
		return $this->plugin_description;
	}

	/**
	 * @param $db
	 * @param $settings
	 * @param $request
	 * @return bool
	 */
	function runPlugin(&$db, &$settings, &$request)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->request = $request;
		return true;
	}

	/**
	 * @return mixed
	 */
	function registerAdminInterface()
	{
		return $this->interface;
	}
}
<?php

class PriceHelper
{
	public static function round($amount, $decimals = 2)
	{
		if (isset($_SESSION['default_currency']))
		{
			$decimals = $_SESSION['default_currency']['decimal_places'];
		}
		// TODO: Need to base this on site default currency's decimal places
		return round($amount, $decimals);
	}

	/**
	 * @param $price the amount to add taxes to
	 * @param $taxRate as a number from 0 to 100
	 *
	 * @return float
	 */
	public static function priceWithTax($price, $taxRate, $decimals = 2)
	{
		if (isset($_SESSION['default_currency']))
		{
			$decimals = $_SESSION['default_currency']['decimal_places'];
		}
		return PriceHelper::round($price * (1 + $taxRate / 100), $decimals);
	}
}
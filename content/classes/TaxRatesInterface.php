<?php

interface TaxRatesInterface
{
	public function getTaxRates($taxCountryId, $taxStateId, $userLevel);

	public function hasTaxRate($classId);

	public function calculateTax($classId, $amount);

	public function getTaxRate($classId, $fraction = true);

	public function getTaxRateDescription($classId);

	public function getDisplayPricesWithTax();
}
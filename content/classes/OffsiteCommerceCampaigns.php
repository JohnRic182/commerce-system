<?php
/**
 * Off-Site Commerce Campaigns class
 */

class OffsiteCommerceCampaigns
{
	protected $_http_url_prefix = "";
	protected $_https_url_prefix = "";
	protected $_db;
	protected $_settings;
	
	/**
	 * Class constructor
	 * @param DB $db
	 * @return OffsiteCommerceCampaigns 
	 */
	public function __construct($db, &$settings)
	{
		$this->_db = $db;
		$this->_settings = $settings;
		$this->_http_url_prefix = $this->_settings["GlobalHttpUrl"]."/index.php?ua=osc";
		$this->_https_url_prefix = $this->_settings["GlobalHttpsUrl"]."/index.php?ua=osc";
		
		return $this;
	}
	
	/**
	 * Add a new campaign
	 * @param type $campaign
	 * @return int 
	 */
	public function addCampaign($campaign, $type)
	{
		$this->_db->reset();
		$this->_db->assignStr("name", $campaign["name"]);
		$this->_db->assignStr("description", $campaign["description"]);
		$this->_db->assignStr("type", $type);
		$this->_db->assign("created", "NOW()");
		return $this->_db->insert(DB_PREFIX."offsite_campaigns");
	}
	
	/**
	 * Delete campaign
	 * @param int $campaign_id
	 */
	public function deleteCampaign($campaign_id)
	{
		$this->_db->query("DELETE FROM ".DB_PREFIX."offsite_campaigns WHERE campaign_id='".$this->_db->escape($campaign_id)."'");
	}
	
	/**
	 * Update campaigns
	 * @param array $campaigns
	 */
	public function updateCampaigns($campaigns)
	{
		foreach ($campaigns as $campaign)
		{
			$this->_db->reset();
			$this->_db->assignStr("name", $campaign["name"]);
			$this->_db->assignStr("description", $campaign["description"]);
			$this->_db->update(DB_PREFIX."offsite_campaigns", "WHERE campaign_id='".$this->_db->escape($campaign["campaign_id"])."'");
		}
	}
	
	/**
	 * Get all qr campaigns
	 * @return mixed
	 */
	public function getCampaigns($type)
	{
		$this->_db->query("
			SELECT 
				offsite_campaigns.*, 
				DATE_FORMAT(created, '".$this->_db->escape($this->_settings["LocalizationDateTimeFormat"])."') AS created 
			FROM ".DB_PREFIX."offsite_campaigns AS offsite_campaigns
			WHERE 
				type='".$this->_db->escape($type)."'
			ORDER BY created
		");
		
		if ($this->_db->numRows() > 0)
		{
			return $this->_db->getRecords();
		}
		return false;
	}
	
	/**
	 * Get QR Campaign by ID
	 * @param type $campaignId
	 * @return type 
	 */
	public function getCampaignById($campaignId)
	{
		$r = $this->_db->query("SELECT * FROM ".DB_PREFIX."offsite_campaigns WHERE campaign_id='".intval($campaignId)."'");
		if ($this->_db->numRows($r) > 0)
		{
			$campaign = $this->_db->moveNext($r);
			return $campaign;
		}
		return false;
	}
	
	/**
	 * Set URL prefix to be used in URLs hidden in QR code
	 * @param url $prefix 
	 */
	public function setHttpUrlPrefix($prefix)
	{
		$this->_http_url_prefix = $prefix;
	}
	
	/**
	 * Return HTTP URL prefix to be used in URLs hidden in QR code
	 * @return string 
	 */
	public function getHttpUrlPrefix()
	{
		return $this->_http_url_prefix;
	}
	
	/**
	 * Set URL prefix to be used in URLs hidden in QR code
	 * @param string $prefix 
	 */
	public function setHttpsUrlPrefix($prefix)
	{
		$this->_https_url_prefix = $prefix;
	}
	
	/**
	 * Return HTTPs URL prefix to be used in URLs hidden in QR code
	 * @return string 
	 */
	public function getHttpsUrlPrefix()
	{
		return $this->_https_url_prefix;
	}
	
	/**
	 * Returns product URL hidden in QR code
	 * @param int $product_id
	 * @param mixed $campaign_id
	 */
	public function getProductUrl($product_id, $campaign_id = false, $source = false)
	{
		return 
			$this->getHttpsUrlPrefix()."&a=p&i=".base_convert($product_id, 10, 36).
			($campaign_id ? ("&c=".base_convert($campaign_id, 10, 36)) : "").
			($source ? ("&s=".$source) : "");
	}
	
	/**
	 * Returns product-add-to-cart URL hidden in QR code
	 * @param int $product_id
	 * @param mixed $campaign_id
	 */
	public function getProductAddToCartUrl($product_id, $campaign_id = false, $source = false, $goToOpc = false)
	{
		return 
			$this->getHttpUrlPrefix()."&a=a&i=".base_convert($product_id, 10, 36).
			($goToOpc?"&g=o":"").
			($campaign_id ? ("&c=".base_convert($campaign_id, 10, 36)) : "").
			($source ? ("&s=".$source) : "");
	}
	
	/**
	 * Returns category URL hidden in QR code
	 * @param int $category_id
	 * @param mixed $campaign_id
	 */
	public function getCategoryUrl($category_id, $campaign_id = false, $source = false)
	{
		return 
			$this->getHttpUrlPrefix()."&a=c&i=".base_convert($category_id, 10, 36).
			($campaign_id ? ("&c=".base_convert($campaign_id, 10, 36)) : "").
			($source ? ("&s=".$source) : "");
	}
}
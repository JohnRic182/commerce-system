<?php

/**
 * Class AddressValidator
 */
class AddressValidator
{
	protected static $lastError = null;
	protected static $addressVariants = null;

	/**
	 * Set last error
	 *
	 * @param $error
	 */
	public static function setLastError($error)
	{
		self::$lastError = $error;
	}

	/**
	 * Get last error
	 *
	 * @return null
	 */
	public static function getLastError()
	{
		return self::$lastError;
	}

	/**
	 * Set address variants
	 *
	 * @param $addressVariants
	 */
	public static function setAddressVariants($addressVariants)
	{
		self::$addressVariants = $addressVariants;
	}

	/**
	 * Get address variants
	 *
	 * @return null
	 */
	public static function getAddressVariants()
	{
		return self::$addressVariants;
	}

	/**
	 * Validate input
	 *
	 * @param DB $db
	 * @param array $settings
	 * @param array $addressData
	 * @param $customFields
	 * @param string $type
	 *
	 * @return array
	 */
	public static function validateInput(DB $db, array &$settings, $type, $addressData, $customFields = null)
	{
		$errors = array();

		if (isset($addressData['address_type']))
		{
			if (trim($addressData['address_type']) == '') $errors['address_type'] = 'Please select address type';
		}

		if (isset($addressData['name']) && $type != Model_Address::ADDRESS_BILLING)
		{
			if (!validate($addressData['name'], VALIDATE_NAME)) $errors['name'] = 'Please enter valid name';
		}
		else
		{
			if (isset($addressData['first_name']) && isset($addressData['last_name']))
			{
				if (!isset($addressData['first_name']) || !validate($addressData['first_name'], VALIDATE_NAME)) $errors['first_name'] = $errors['fname'] = 'Enter valid first name';
				if (!isset($addressData['last_name']) || !validate($addressData['last_name'], VALIDATE_NAME)) $errors['last_name'] = $errors['lname'] = 'Enter valid last name';
			}
			else
			{
				if (!isset($addressData['fname']) || !validate($addressData['fname'], VALIDATE_NAME)) $errors['fname'] = $errors['first_name'] = 'Enter valid first name';
				if (!isset($addressData['lname']) || !validate($addressData['lname'], VALIDATE_NAME)) $errors['lname'] = $errors['last_name'] = 'Enter valid last name';
			}
		}

		if (!isset($addressData['address1']) || !validate($addressData['address1'], VALIDATE_STRING))
		{
			$errors['address1'] = 'Enter valid address line 1';
		}

		if (!isset($addressData['address2'])) $addressData['address2'] = '';

		if ($type == Model_Address::ADDRESS_SHIPPING)
		{
			if ($settings['FormsShippingCompany'] == 'Required' && (!isset($addressData['company']) || !validate($addressData['company'], VALIDATE_STRING)))
			{
				$errors['company'] = 'Enter a valid company name';
			}

			if ($settings['FormsShippingAddressLine2'] == 'Required' && (!isset($addressData['address2']) || !validate($addressData['address2'], VALIDATE_STRING)))
			{
				$errors['address2'] = 'Enter a valid address line 2';
			}
		}
		else if ($type == Model_Address::ADDRESS_BILLING)
		{
			if ($settings['FormsBillingCompany'] == 'Required' && (!isset($addressData['company']) || !validate($addressData['company'], VALIDATE_STRING)))
			{
				$errors['company'] = 'Enter a valid company name';
			}

			if ($settings['FormsBillingAddressLine2'] == 'Required' && (!isset($addressData['address2']) || !validate($addressData['address2'], VALIDATE_STRING)))
			{
				$errors['address2'] = 'Enter a valid address line 2';
			}
		}
		else if ($type == Model_Address::ADDRESS_PAYMENT)
		{
			if ($settings['FormsBillingPhone'] == 'Required' && (!isset($addressData['phone']) || !validate($addressData['phone'], VALIDATE_STRING)))
			{
				$errors['phone'] = 'Enter a valid phone';
			}

			if ($settings['FormsBillingAddressLine2'] == 'Required' && (!isset($addressData['address2']) || !validate($addressData['address2'], VALIDATE_STRING)))
			{
				$errors['address2'] = 'Enter a valid address line 2';
			}
		}

		if (!isset($addressData['city']) || !validate($addressData['city'], VALIDATE_STRING)) $errors['city'] = 'Enter city name';

		//validate country, state and province & zip

		$countryId = null;

		if (isset($addressData['country']) && validate($addressData['country'], VALIDATE_INT))
		{
			$countryId = $addressData['country_id'] = $addressData['country'];
		}
		else if (isset($addressData['country_id']) && validate($addressData['country_id'], VALIDATE_INT))
		{
			$countryId = $addressData['country'] = $addressData['country_id'];
		}

		if (is_null($countryId))
		{
			$errors['country'] = $errors['country_id'] = 'Select country';
		}
		else
		{
			$countryData = $db->selectOne('SELECT iso_a3 FROM '.DB_PREFIX.'countries WHERE coid='.intval($countryId));

			if ($countryData)
			{
				// check are there states in this country
				$statesCount = $db->selectOne('SELECT COUNT(*) AS count FROM '.DB_PREFIX.'states WHERE coid='.intval($countryId));

				if ($statesCount['count'] > 0)
				{
					$stateId = null;

					if (isset($addressData['state']) && validate($addressData['state'], VALIDATE_INT))
					{
						$stateId = $addressData['state_id'] = $addressData['state'];
					}
					else if (isset($addressData['state_id']) && validate($addressData['state_id'], VALIDATE_INT))
					{
						$stateId = $addressData['state'] = $addressData['state_id'];
					}

					// validate state
					if (is_null($stateId))
					{
						$errors['state'] = $errors['state_id'] = 'Select state/province';
					}
					else
					{
						$stateData = $db->selectOne('SELECT * FROM '.DB_PREFIX.'states WHERE coid='.intval($countryId).' AND stid='.intval($stateId));
						if ($stateData)
						{
							$addressData['province'] = $stateData['name'];
						}
						else
						{
							$errors['state'] = $errors['state_id'] = 'Select state/province';
						}
					}
				}
				else
				{
					if (!isset($addressData['province']) || !validate($addressData['province'], VALIDATE_STRING))
					{
						$errors['province'] = 'Enter state/province name';
					}
					else
					{
						$addressData['state'] = $addressData['state_id'] = 0;
					}
				}

				if (isset($addressData['zip']))
				{
					if ($countryData['iso_a3'] == 'USA' && !validate($addressData['zip'], VALIDATE_ZIP_USA))
					{
						$errors['zip'] = 'Enter valid zip code';
					}
					elseif ($countryData['iso_a3'] == 'CAN' && !validate($addressData['zip'], VALIDATE_ZIP_CANADA))
					{
						$errors['zip'] = 'Enter valid postal code';
					}
					elseif ($countryData['iso_a3'] == 'GBP' && !validate($addressData['zip'], VALIDATE_ZIP_UK))
					{
						$errors['zip'] = 'Enter valid postal code';
					}
					elseif (!validate($addressData['zip'], VALIDATE_ZIP))
					{
						$errors['zip'] = 'Enter valid zip/postal code';
					}
				}
				else
				{
					$errors['zip'] = 'Enter valid zip/postal code';
				}
			}
			else
			{
				$errors['country'] = $errors['country_id'] = 'Select country';
			}
		}

		return $errors;
	}

	/**
	 * Validate
	 *
	 * @param DB $db
	 * @param $addressToValidate
	 * @param $msg
	 *
	 * @return bool
	 */
	public static function validate(DB $db, &$addressToValidate, $msg)
	{
		if (!is_null($addressToValidate))
		{
			self::$addressVariants = null;
			self::$lastError = null;

			if (!isset($addressToValidate['name']))
			{
				if (isset($addressToValidate['first_name']) && isset($addressToValidate['last_name']))
				{
					$addressToValidate['name'] = $addressToValidate['first_name'].' '.$addressToValidate['last_name'];
				}
				else
				{
					$addressToValidate['name'] = $addressToValidate['fname'].' '.$addressToValidate['lname'];
				}
			}

			$addressToValidate['country'] = isset($addressToValidate['country_id']) ? $addressToValidate['country_id'] : $addressToValidate['country'];
			$addressToValidate['state'] = isset($addressToValidate['state_id']) ? $addressToValidate['state_id'] : (isset($addressToValidate['state']) ? $addressToValidate['state'] : '0');
			$useUserAddress = isset($addressToValidate['use_user_address']) && $addressToValidate['use_user_address'] == '1';

			// validate only for US addresses and when user override is not active
			if ($addressToValidate['country'] == 1 && !$useUserAddress)
			{
				global $settings;
				$addressValidator = new Shipping_DialAZip('', '', '', $settings);

				// select state code
				$stateCodeResult = $db->query('SELECT short_name FROM '.DB_PREFIX.'states WHERE coid=1 AND stid='.intval($addressToValidate['state']));
				if ($db->moveNext($stateCodeResult))
				{
					$stateCode = $db->col['short_name'];

					// validate address
					$validationResult = $addressValidator->validateAddress(
						$addressToValidate['name'],
						$addressToValidate['address1'],
						$addressToValidate['address2'],
						$addressToValidate['city'],
						$stateCode,
						$addressToValidate['zip']
					);

					switch ($validationResult)
					{
						case 10 : self::setLastError($msg['shipping']['validation_error_invalid_dual_address']); return false; break;
						case 11 : self::setLastError($msg['shipping']['validation_error_invalid_zip_code']); return false; break;
						case 12 : self::setLastError($msg['shipping']['validation_error_invalid_state']); return false; break;
						case 13 : self::setLastError($msg['shipping']['validation_error_invalid_city']); return false; break;
						case 21 : self::setLastError($msg['shipping']['validation_error_address_not_found']); return false; break;
						case 25 : self::setLastError($msg['shipping']['validation_error_street_error']); return false; break;
						case 22 : 
						{
							// preselect state ids
							$db->query('SELECT stid, short_name, name FROM '.DB_PREFIX.'states WHERE coid=1');
							$stateIds = array();
							while ($db->moveNext())
							{
								$stateIds[strtoupper($db->col['short_name'])] = array('id' => $db->col['stid'], 'name' => $db->col['name']);
							}
							
							// build array for address variants
							$addressVariants = $addressValidator->getAddressVariants(
								$addressToValidate['name'], 
								$addressToValidate['address1'], 
								$addressToValidate['address2'], 
								$addressToValidate['city'], 
								$stateCode, 
								$addressToValidate['zip'], 
								$stateIds
							);
							
							if ($addressVariants)
							{
								self::setAddressVariants($addressVariants);
							}

							// technically validation did not pass so return false
							return false;
							break;
						}
						
						case 31 :
						case 32 :
						{
							$validationResponse = $addressValidator->getLastResponse();

							if (is_string($validationResponse['RDI']) && $validationResponse['RDI'] != '')
							{
								$addressToValidate['address_type'] =  $validationResponse['RDI'] == 'B' ? 'Business' : 'Residential';
							}

							$addressToValidate['address1'] = $validationResponse['AddrLine1'];
							$addressToValidate['address2'] = is_string($validationResponse['AddrLine2']) && trim($validationResponse['AddrLine2']) != '' ? $validationResponse['AddrLine2'] : $addressToValidate['address2'];
							$addressToValidate['city'] = $validationResponse['City'];
							$addressToValidate['zip'] = $validationResponse['ZIP5'];

							if (strtoupper($stateCode) != strtoupper($validationResponse['State']))
							{
								$result = $db->query('SELECT stid FROM '.DB_PREFIX.'states WHERE coid=1 AND short_name LIKE "'.strtoupper($validationResponse['State']).'"');

								if ($db->moveNext($result))
								{
									$addressToValidate['state'] = $db->col['stid'];
									$addressToValidate['state_id'] = $db->col['stid'];
								}
							}

							if (is_numeric($validationResponse['Plus4'])) $addressToValidate['zip'].='-'.$validationResponse['Plus4'];

							break;
						}
					}
				}
			}
		}

		return true;
	}
}
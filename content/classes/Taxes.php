<?php 

/**
 * @class Taxes
 */
class Taxes extends Regions
{
	var $db = false;
	
	public $tax_class_key_hash = null;
	
	function __construct(&$db)
	{
		$this->db = $db;
		return $this;
	}
	
	function addZone($data)
	{
		$this->db->reset();
		$this->db->assignStr("zone_name", $data["zone_name"]);
		$this->db->assignStr("zone_description", $data["zone_description"]);
		$this->db->assignStr('display_with_tax', isset($data['display_with_tax']) && intval($data['display_with_tax']) == 1 ? 1 : 0);
		$zone_id = $this->db->insert(DB_PREFIX."tax_zones");
		
		return $zone_id;
	}
	
	function addClass($data)
	{
		$this->db->reset();
		$this->db->assignStr("key_name", $data["key_name"]);
		$this->db->assignStr("class_name", $data["class_name"]);
		$this->db->assignStr("class_description", $data["class_description"]);
		$zone_id = $this->db->insert(DB_PREFIX."tax_classes");
			
		return $zone_id;
	}
	
	function addClassProducts($class_id, $product_id)
	{
		$this->db->reset();
		$this->db->assignStr("tax_class_id", $class_id);		
		$this->db->update(DB_PREFIX."products", "WHERE pid='".$product_id."'");
		return $product_id;
	}
	
	function addZoneRegion($zone_id, $coid, $stid)
	{
		$this->db->query("SELECT * FROM ".DB_PREFIX."tax_zones_regions WHERE zone_id='".intval($zone_id)."' AND coid='".intval($coid)."' AND stid='".intval($stid)."'");
		if ($this->db->moveNext())
		{
			return $this->db->col["zr_id"];
		}
		else
		{
			$this->db->reset();
			$this->db->assignStr("zone_id", intval($zone_id));
			$this->db->assignStr("coid", intval($coid));
			$this->db->assignStr("stid", intval($stid));
			return $this->db->insert(DB_PREFIX."tax_zones_regions");
		}
	}
	
	function addRate($data)
	{
		$this->db->reset();
		$this->db->assignStr("class_id", $data["class_id"]);
		$this->db->assignStr("zone_id", $data["zone_id"]);
		$this->db->assignStr("rate_priority", $data["rate_priority"]);
		$this->db->assignStr("tax_rate", $data["tax_rate"]);
		$this->db->assignStr("user_level", isset($data["user_level"])?implode(",", $data["user_level"]):"");
		$this->db->assignStr("rate_description", $data["rate_description"]);
		$rate_id = $this->db->insert(DB_PREFIX."tax_rates");
		return $rate_id;
	}
	
	function saveZonesData($data)
	{
		foreach ($data["zone_name"] as $zone_id=>$zone_name)
		{
			$this->db->reset();
			$this->db->assignStr("zone_name", $data["zone_name"][$zone_id]);
			$this->db->assignStr("zone_description", $data["zone_description"][$zone_id]);
			$this->db->assignStr('display_with_tax', isset($data['zone_display_with_tax'][$zone_id]) && intval($data['zone_display_with_tax'][$zone_id]) == 1 ? 1 : 0);
			$this->db->update(DB_PREFIX."tax_zones", "WHERE zone_id='".intval($zone_id)."'");
		}
	}
	
	function saveClassesData($data)
	{
		foreach ($data["class_name"] as $class_id=>$class_name)
		{
			$this->db->reset();
			$this->db->assignStr("key_name", $data["key_name"][$class_id]);
			$this->db->assignStr("class_name", $data["class_name"][$class_id]);
			if (isset($data["class_description"][$class_id])) $this->db->assignStr("class_description", $data["class_description"][$class_id]);
			$this->db->update(DB_PREFIX."tax_classes", "WHERE class_id='".intval($class_id)."'");
		}
	}
	
	function saveRate($rate_id, $data)
	{
		$this->db->reset();
		$this->db->assignStr("class_id", $data["class_id"]);
		$this->db->assignStr("zone_id", $data["zone_id"]);
		$this->db->assignStr("rate_priority", $data["rate_priority"]);
		$this->db->assignStr("tax_rate", $data["tax_rate"]);
		$this->db->assignStr("user_level", isset($data["user_level"])?implode(",", $data["user_level"]):"");
		$this->db->assignStr("rate_description", $data["rate_description"]);
		$this->db->update(DB_PREFIX."tax_rates", "WHERE rate_id='".intval($rate_id)."'");
	}
	
	function getTaxClassKeyHash()
	{
		if (is_null($this->tax_class_key_hash))
		{
			$tax_classes = $this->getTaxClassesList();
			$this->tax_class_key_hash = array();
			foreach ($tax_classes as $tax_class)
			{
				$this->tax_class_key_hash[strtolower($tax_class['key_name'])] = $tax_class['class_id'];
			}
		}
		
		return $this->tax_class_key_hash;
	}
	
	function getTaxZoneById($zone_id)
	{
		$this->db->query("SELECT * FROM ".DB_PREFIX."tax_zones WHERE zone_id='".intval($zone_id)."'");
		return $this->db->moveNext()?$this->db->col:false;
	}
	
	function getTaxZoneRegions($zone_id)
	{
		$this->db->query("
			SELECT 
				".DB_PREFIX."tax_zones_regions.*,
				".DB_PREFIX."countries.name AS country_name, 
				".DB_PREFIX."states.name AS state_name 
			FROM ".DB_PREFIX."tax_zones_regions
			LEFT JOIN ".DB_PREFIX."countries ON ".DB_PREFIX."countries.coid=".DB_PREFIX."tax_zones_regions.coid
			LEFT JOIN ".DB_PREFIX."states ON ".DB_PREFIX."states.stid=".DB_PREFIX."tax_zones_regions.stid
			WHERE ".DB_PREFIX."tax_zones_regions.zone_id='".intval($zone_id)."'
			ORDER BY ".DB_PREFIX."countries.country_priority DESC, ".DB_PREFIX."countries.name, ".DB_PREFIX."states.name
		");
		return $this->db->numRows() > 0 ? $this->db->getRecords() : false;
	}
	
	function getTaxZonesList($sortorder = "name_asc"){
		$order_by = "zone_name";//, priority DESC";
		switch($sortorder){
			//note - here reverse sort order for priority
			case "name_asc" : $order_by = "zone_name"; break;
			case "name_desc" : $order_by = "zone_name DESC"; break;
			//case "priority_asc" : $order_by = "country_priority DESC, name"; break;
			//case "priority_desc" : $order_by = "country_priority, name"; break;
		}
		$this->db->query("SELECT ".DB_PREFIX."tax_zones.* FROM ".DB_PREFIX."tax_zones ORDER BY ".$order_by);
		return $this->db->getRecords();
	}
	
	function getTaxClassesList($sortorder = "name_asc"){
		$order_by = "zone_name";//, priority DESC";
		switch($sortorder){
			case "name_asc" : $order_by = "class_name"; break;
			case "name_desc" : $order_by = "class_name DESC"; break;
		}
		$this->db->query("SELECT ".DB_PREFIX."tax_classes.* FROM ".DB_PREFIX."tax_classes ORDER BY ".$order_by);
		return $this->db->getRecords();
	}
	
	function getTaxClassById($class_id){
		$this->db->query("SELECT * FROM ".DB_PREFIX."tax_classes WHERE class_id='".intval($class_id)."'");
		return $this->db->moveNext()?$this->db->col:false;
	}
	
	function getRateById($rate_id){
		$this->db->query("SELECT * FROM ".DB_PREFIX."tax_rates WHERE rate_id='".intval($rate_id)."'");
		return $this->db->moveNext()?$this->db->col:false;
	}
	
	function getTaxRatesList($sort_order = "priority_asc"){
		$this->db->query("
			SELECT ".DB_PREFIX."tax_rates.*, ".DB_PREFIX."tax_classes.class_name, ".DB_PREFIX."tax_classes.key_name, ".DB_PREFIX."tax_zones.zone_name, ".DB_PREFIX."tax_zones.display_with_tax
			FROM ".DB_PREFIX."tax_rates
			INNER JOIN ".DB_PREFIX."tax_classes ON ".DB_PREFIX."tax_classes.class_id = ".DB_PREFIX."tax_rates.class_id
			INNER JOIN ".DB_PREFIX."tax_zones ON ".DB_PREFIX."tax_zones.zone_id = ".DB_PREFIX."tax_rates.zone_id
			ORDER BY ".DB_PREFIX."tax_rates.rate_priority 
		");
		return $this->db->getRecords();
	}
	
	function deleteZone($zone_id)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."tax_zones WHERE zone_id='".intval($zone_id)."'");
		$this->db->query("DELETE FROM ".DB_PREFIX."tax_zones_regions WHERE zone_id='".intval($zone_id)."'");
	}
	
	function deleteClass($class_id)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."tax_classes WHERE class_id='".intval($class_id)."'");
	}
	
	function deleteZoneRegion($zr_id)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."tax_zones_regions WHERE zr_id='".intval($zr_id)."'");
	}
	
	function deleteRate($rate_id)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."tax_rates WHERE rate_id='".intval($rate_id)."'");
	}
	
	function isValidClassKey($key_name, $class_id = null)
	{
		$key_name = trim(strtolower($key_name));
		$tax_class_key_hash = $this->getTaxClassKeyHash();
		
		// if key name is blank, return true OR
		// key name does not exist in hash map OR
		// key name is set to current class id
		return (
			($key_name == '' ) || 
			(!in_array($key_name, array_keys($tax_class_key_hash))) ||
			($tax_class_key_hash[$key_name] == $class_id)
		);
	}
}
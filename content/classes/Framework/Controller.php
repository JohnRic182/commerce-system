<?php
/**
 * Class Framework_Controller
 */
abstract class Framework_Controller
{
	/** @var Framework_Session */
	protected $session = null;

	/** @var Framework_Request */
	protected $request;

	/** @var DB */
	protected $db;

	/** @var FlashMessages */
	protected $flashMessages = null;

	/**
	 * Class constructor
	 *
	 * @param Framework_Request $request
	 * @param DB $db
	 */
	public function __construct(Framework_Request $request, DB $db)
	{
		$this->request = $request;
		$this->db = $db;
	}

	/**
	 * Get request
	 *
	 * @return Framework_Request
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * Get database
	 *
	 * @return DB
	 */
	public function getDb()
	{
		return $this->db;
	}

	/**
	 * Get session class
	 *
	 * @return Framework_Session
	 */
	public function getSession()
	{
		if (is_null($this->session))
		{
			global $session;

			if ($session === null)
			{
				$session = new Framework_Session(new Framework_Session_WrapperStorage());
				$session->start();
			}

			$this->session = $session;
		}

		return $this->session;
	}

	/**
	 * Get flash message
	 */
	public function getFlashMessages()
	{
		// TODO: change to use registry
		if (is_null($this->flashMessages))
		{
			$session = $this->getSession();
			$this->flashMessages = new Model_FlashMessages($session);
		}

		return $this->flashMessages;
	}

	/**
	 * Get view
	 *
	 * @return Ddm_View
	 */
	public abstract function getView();


	/**
	 * Set settings
	 *
	 * @return DataAccess_SettingsRepository|null
	 */
	public function getSettings()
	{
		return DataAccess_SettingsRepository::getInstance();
	}

	/**
	 * @param string $page
	 * @param array $params
	 */
	protected function redirect($page = '', $params = array())
	{
		if (trim($page) != '')
		{
			$params['p'] = $page;
		}

		$query = '';
		if (count($params) > 0)
		{
			$page = isset($params['p']) ? $params['p'] : '';
			unset($params['p']);

			$query = '?'.($page ? 'p='.$page : '').(count($params) ? '&'.http_build_query($params) : '');
		}

		header('Location: '.$this->getBasePath().$query);

		$this->shutDown();
	}

	/**
	 *
	 */
	protected function shutDown()
	{
		$this->db->done();
		session_write_close();
		exit();
	}

	protected abstract function getBasePath();

	/**
	 * Render json response
	 *
	 * @param $data
	 * @param int $status
	 */
	protected function renderJson($data, $status = 200)
	{
		$request = $this->getRequest();

		header('HTTP/1.0 ' . $status);
		header('Content-Type: application/json');

		echo $request->query->has('callback') ? $request->query->get('callback').'('.json_encode($data).');' : json_encode($data);

		$this->db->done();
		session_write_close();
		exit();
	}

	/**
	 * @param $raw
	 * @param int $status
	 */
	protected function renderRaw($raw, $status = 200)
	{
		header('HTTP/1.0 ' . $status);

		echo $raw;

		$this->db->done();
		session_write_close();
		exit();
	}

	/**
	 * Will be executed before action
	 */
	public function onBeforeAction()
	{

	}

	/**
	 * Will be executed after
	 */
	public function onAfterAction()
	{

	}
}
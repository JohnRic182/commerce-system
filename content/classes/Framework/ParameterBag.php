<?php
/**
 * Class Framework_ParameterBag
 */
class Framework_ParameterBag implements IteratorAggregate, Countable
{
	protected $parameters;

	public function __construct(array $parameters = array())
	{
		$this->parameters = $parameters;
	}

	public function all()
	{
		return $this->parameters;
	}

	public function keys()
	{
		return array_keys($this->parameters);
	}

	public function replace(array $parameters = array())
	{
		$this->parameters = $parameters;
	}

	public function add(array $parameters = array())
	{

		if (floatval(phpversion()) >= 5.3)
		{
			$this->parameters = array_replace($this->parameters, $parameters);
		}
		else
		{
			$this->parameters = array_merge($this->parameters, $parameters);
		}
	}

	public function getByKeys(array $keys)
	{
		$ret = array();
		foreach ($keys as $k)
		{
			$ret[$k] = $this->get($k);
		}

		return $ret;
	}

	public function get($key, $default = null)
	{
		if (!array_key_exists($key, $this->parameters))
		{
			return $default;
		}

		return $this->parameters[$key];
	}

	public function set($key, $value)
	{
		$this->parameters[$key] = $value;
	}

	public function has($key)
	{
		return array_key_exists($key, $this->parameters);
	}

	public function remove($key)
	{
		unset($this->parameters[$key]);
	}

	public function count()
	{
		return count($this->parameters);
	}

	public function getIterator()
	{
		return new ArrayIterator($this);
	}

	public function getInt($key, $default = 0)
	{
		if ($this->has($key))
		{
			$value = $this->get($key, $default);

			if (is_numeric($value))
			{
				return intval($value);
			}
		}

		return $default;
	}

	/**
	 * Get an array from parameters
	 *
	 * @param $key
	 * @param array $default
	 *
	 * @return array
	 */
	public function getArray($key, $default = array())
	{
		if ($this->has($key))
		{
			$value = $this->get($key);

			if ($value !== null && is_array($value))
			{
				return $value;
			}
		}

		return $default;
	}
}
<?php
/**
 * Class Framework_Request
 */
class Framework_Request
{
	/**
	 * @var Framework_ParameterBag
	 */
	public $request;

	/**
	 * @var Framework_ParameterBag
	 */
	public $query;

	/**
	 * @var Framework_ParameterBag
	 */
	public $server;

	/**
	 * @var Framework_ParameterBag
	 */
	public $cookies;

	/**
	 * @var Framework_ParameterBag
	 */
	public $files;

	protected $requestUri;

	/**
	 * Class constructor
	 *
	 * @param array $query
	 * @param array $request
	 * @param array $server
	 * @param array $cookies
	 * @param array $files
	 */
	public function __construct(array $query = array(), array $request = array(), array $server = array(), array $cookies = array(), array $files = array())
	{
		$this->query = new Framework_ParameterBag($query);
		$this->request = new Framework_ParameterBag($request);
		$this->server = new Framework_ParameterBag($server);
		$this->cookies = new Framework_ParameterBag($cookies);
		$this->files = new Framework_ParameterBag($files);

		if (isset($server['REQUEST_URI']))
		{
			$components = parse_url($server['REQUEST_URI']);
			if (isset($components['host']))
			{
				$this->server->set('SERVER_NAME', $components['host']);
				$this->server->set('HTTP_HOST', $components['host']);
			}
		}
	}

	/**
	 * @return Framework_ParameterBag
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * @return Framework_ParameterBag
	 */
	public function getQuery()
	{
		return $this->query;
	}

	/**
	 * @return Framework_ParameterBag
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * @param $key
	 * @return bool|null
	 */
	public function getUploadedFile($key)
	{
		$fileData = $this->getFiles()->get($key, null);

		if (!is_null($fileData) && is_array($fileData))
		{
			if (
				isset($fileData['name']) && $fileData['name'] != '' &&
				isset($fileData['error']) && $fileData['error'] == 0 &&
				isset($fileData['tmp_name']) && is_file($fileData['tmp_name'])
			)
			{
				return $fileData;
			}
		}

		return false;
	}

	/**
	 * @return Framework_ParameterBag
	 */
	public function getServer()
	{
		return $this->server;
	}

	/**
	 * Create request from globals
	 *
	 * @return Framework_Request
	 */
	public static function createFromGlobals()
	{
		$request = new self($_GET, $_POST, $_SERVER, $_COOKIE, $_FILES);

		if (strtolower($request->getMethod()) == 'put')
		{
			$post_vars = array();
			parse_str(file_get_contents("php://input"),$post_vars);

			$request->request = new Framework_ParameterBag($post_vars);
		}
		return $request;
	}

	/**
	 * Return request parameter - get first, then post
	 *
	 * @param string $key
	 * @param mixed $default
	 *
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		return $this->query->get($key, $this->request->get($key, $default));
	}

	/**
	 * Checks is request a post request
	 *
	 *
	 * @return bool
	 */
	public function isPost()
	{
		return strtolower($this->server->get('REQUEST_METHOD')) == 'post';
	}

	public function getMethod()
	{
		return strtolower($this->server->get('REQUEST_METHOD'));
	}

	/**
	 * Return the hostname
	 *
	 * @return string
	 */
	public function getHostname()
	{
		return $this->server->get('HTTP_HOST', '');
	}

	/**
	 * Check is the request running SSL
	 *
	 * @return bool
	 */
	public function isSecure()
	{
		//TODO: proxies?

		return strtolower($this->server->get('HTTPS')) === 'on' || $this->server->get('HTTPS') == 1;
	}

	public function getRequestUri()
	{
		if ($this->requestUri === null)
		{
			$requestUri = '';
			if ($this->server->has('REQUEST_URI'))
			{
				$requestUri = $this->server->get('REQUEST_URI');
			}
			if ($this->server->has('SCRIPT_NAME'))
			{
				$scriptName = $this->server->get('SCRIPT_NAME');
				$scriptDir = dirname($scriptName);
				if ($scriptDir != '/' && $scriptDir != '')
				{
					if (strpos($requestUri, $scriptDir) === 0)
					{
						$requestUri = substr($requestUri, strlen($scriptDir));
					}
				}
			}

			$this->requestUri = $requestUri;
		}

		return $this->requestUri;
	}
}
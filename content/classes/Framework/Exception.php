<?php
/**
 * Class Framework_Exception
 */
class Framework_Exception extends Exception
{
	/** @var array $details */
	protected $details = array();

	/**
	 * Class constructor
	 *
	 * @param string $message
	 * @param null $details
	 * @param int $code
	 * @param Exception $previous
	 */
	public function __construct($message, $details = null, $code = 0)
	{
		parent::__construct($message, $code);

		if (!is_null($details)) $this->details = is_array($details) ? $details : array($details);
	}

	/**
	 * Get messages
	 *
	 * @return mixed
	 */
	public function getDetails()
	{
		return $this->details;
	}
}
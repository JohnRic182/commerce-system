<?php

class Framework_Session_WrapperStorage implements Framework_Session_StorageInterface
{
	protected $values;
	protected $started = false;

	public function __construct()
	{
		if (version_compare(phpversion(), '5.4.0', '>='))
		{
			session_register_shutdown();
		}
		else
		{
			register_shutdown_function('session_write_close');
		}
	}

	/**
	 *
	 */
	public function getIterator()
	{
		return new ArrayIterator($this->values);
	}

	/**
	 *
	 */
	public function count()
	{
		return count($this->values);
	}

	public function start()
	{
		if ($this->started) return true;

		// TODO: uncomment when 100% moved on this session class

//		if (version_compare(phpversion(), '5.4.0', '>=') && session_status() == PHP_SESSION_ACTIVE)
//		{
//			//throw new Exception('Failed to start session');
//		}
//		if (version_compare(phpversion(), '5.4.0', '<') && !isset($_SESSION))
//		{
//			//throw new Exception('Failed to start session');
//		}

		$this->values = &$_SESSION;
		$this->started = true;

		return true;
	}

	public function isStarted()
	{
		return $this->started;
	}

	public function has($name)
	{
		return isset($this->values[$name]);
	}

	public function get($name, $default = null)
	{
		return isset($this->values[$name]) ? $this->values[$name] : $default;
	}

	public function set($name, $value)
	{
		$this->values[$name] = $value;
	}

	public function all()
	{
		return $this->values;
	}

	public function replace(array $values)
	{
		foreach ($values as $name => $value)
		{
			$this->set($name, $value);
		}
	}

	public function remove($name)
	{
		unset($this->values[$name]);
	}

	public function clear()
	{
		$_SESSION = array();
		$this->values = &$_SESSION;
	}

	public function regenerate($destroy = false, $lifetime = null)
	{
		if (!is_null($lifetime))
		{
			ini_set('session.cookie_lifetime', $lifetime);
		}

		$ret = session_regenerate_id($destroy);
		session_write_close();
		if (isset($_SESSION))
		{
			$backup = $_SESSION;
			session_start();
			$_SESSION = $backup;
		}
		else
		{
			session_start();
		}

		$this->values = &$_SESSION;

		return $ret;
	}

	public function save()
	{
		session_write_close();
	}

	public function getId()
	{
		return session_id();
	}

	public function setId($id)
	{
		session_id($id);
	}

	public function getName()
	{
		return session_name();
	}

	public function setName($name)
	{
		session_name($name);
	}
}
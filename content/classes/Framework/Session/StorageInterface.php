<?php

interface Framework_Session_StorageInterface extends IteratorAggregate, Countable
{
	public function start();

	public function isStarted();

	public function has($name);

	public function get($name, $default = null);

	public function set($name, $value);

	public function all();

	public function replace(array $values);

	public function remove($name);

	public function clear();

	public function regenerate($destroy = false, $lifetime = null);

	public function save();

	public function getId();

	public function setId($id);

	public function getName();

	public function setName($name);
}
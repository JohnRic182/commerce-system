<?php
/**
 * Class Framework_Session_DefaultStorage
 */
class Framework_Session_DefaultStorage implements Framework_Session_StorageInterface
{
	protected $values;
	protected $started = false;

	/**
	 * Class constructor
	 *
	 * @param null|string $savePath
	 *
	 * @param int $directoryPermissions
	 */
	public function __construct($savePath = null, $directoryPermissions = 0777)
	{
		session_cache_limiter('private');

		if (is_null($savePath))
		{
			$savePath = ini_get('session.save_path');
		}

		$basePath = $savePath;
		if (strpos($savePath, ';') !== false)
		{
			$basePath = substr($savePath, strpos($savePath, ';'));
		}

		if ($basePath && !is_dir($basePath))
		{
			mkdir($basePath, $directoryPermissions, true);
		}

		ini_set('session.save_path', $savePath);
		ini_set('session.use_cookies', 1);

		if (version_compare(phpversion(), '5.4.0', '>='))
		{
			session_register_shutdown();
		}
		else
		{
			register_shutdown_function('session_write_close');
		}

		$this->values = array();
	}

	/**
	 * Get iterator
	 */
	public function getIterator()
	{
		return new ArrayIterator($this->values);
	}

	/**
	 * Get count
	 * @return int
	 */
	public function count()
	{
		//return count($this->values);
		return count($_SESSION);
	}

	/**
	 * Start session
	 *
	 * @return bool
	 *
	 * @throws Exception
	 */
	public function start()
	{
		if ($this->started) return true;

		if (version_compare(phpversion(), '5.4.0', '>=') && session_status() == PHP_SESSION_ACTIVE)
		{
			throw new Exception('Failed to start session');
		}

		if (version_compare(phpversion(), '5.4.0', '<') && !isset($_SESSION))
		{
			throw new Exception('Failed to start session');
		}

		if (ini_get('session.use_cookies') && headers_sent($file, $line))
		{
			throw new Exception('Failed to start session, headers already sent');
		}

		if (!session_start())
		{
			throw new Exception('Failed to start session');
		}

		$this->values = &$_SESSION;
		$this->started = true;

		return true;
	}

	/**
	 * Return is session started
	 *
	 * @return mixed
	 */
	public function isStarted()
	{
		return $this->started;
	}

	/**
	 * Check is there session variable set
	 *
	 * @param $name
	 *
	 * @return bool
	 */
	public function has($name)
	{
		//return isset($this->values[$name]);
		return isset($_SESSION[$name]);
	}

	/**
	 * Get session value by name
	 *
	 * @param string $name
	 * @param mixed $default
	 *
	 * @return null
	 */
	public function get($name, $default = null)
	{
		//return isset($this->values[$name]) ? $this->values[$name] : $default;
		return isset($_SESSION[$name]) ? $_SESSION[$name] : $default;
	}

	/**
	 * Set session variable
	 *
	 * @param $name
	 * @param $value
	 */
	public function set($name, $value)
	{
		//$this->values[$name] = $value;
		$_SESSION[$name] = $value;
	}

	/**
	 * Get all
	 *
	 * @return mixed
	 */
	public function all()
	{
		//return $this->values;
		return $_SESSION;
	}

	/**
	 * Replace values
	 *
	 * @param array $values
	 */
	public function replace(array $values)
	{
		foreach ($values as $name => $value)
		{
			$this->set($name, $value);
		}
	}

	/**
	 * Remove from session
	 *
	 * @param $name
	 */
	public function remove($name)
	{
		//unset($this->values[$name]);
		unset($_SESSION[$name]);
	}

	/**
	 * Clear session
	 */
	public function clear()
	{
		$_SESSION = array();
		$this->values = &$_SESSION;
	}

	/**
	 * Regenerate session id
	 *
	 * @param bool $destroy
	 * @param null $lifetime
	 *
	 * @return bool
	 */
	public function regenerate($destroy = false, $lifetime = null)
	{
		if (!is_null($lifetime))
		{
			ini_set('session.cookie_lifetime', $lifetime);
		}

		$ret = session_regenerate_id($destroy);
		session_write_close();
		if (isset($_SESSION))
		{
			$backup = $_SESSION;
			session_start();
			$_SESSION = $backup;
		}
		else
		{
			session_start();
		}

		$this->values = &$_SESSION;

		return $ret;
	}

	/**
	 * Save and close session
	 */
	public function save()
	{
		session_write_close();
	}

	/**
	 * Set session id
	 *
	 * @return string
	 */
	public function getId()
	{
		return session_id();
	}

	/**
	 * Set session id
	 *
	 * @param $id
	 */
	public function setId($id)
	{
		session_id($id);
	}

	/**
	 * Get session name
	 *
	 * @return string
	 */
	public function getName()
	{
		return session_name();
	}

	/**
	 * Set session name
	 *
	 * @param $name
	 */
	public function setName($name)
	{
		session_name($name);
	}
}

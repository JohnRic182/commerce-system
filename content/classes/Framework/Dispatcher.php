<?php

/**
 * Class Framework_Dispatcher
 */
class Framework_Dispatcher
{
	public static function run($controller, $action)
	{
		// TODO: get rid of global dependency here
		global $db;

		try
		{
			/** @var Framework_Controller $controller */
			$controllerInstance = is_object($controller) ? $controller : new $controller(Framework_Request::createFromGlobals(), $db);
			$controller->onBeforeAction();
			call_user_func_array(array($controllerInstance, $action), array());
			$controller->onAfterAction();
		}
		catch (Exception $e)
		{

		}
	}
}
<?php

class Framework_Registry_Definition
{
	protected $className;
	protected $parameters;

	public function __construct($className, array $parameters = array())
	{
		$this->className = $className;
		$this->parameters = $parameters;
	}

	public function getClassName()
	{
		return $this->className;
	}

	public function getParameters()
	{
		return $this->parameters;
	}
}
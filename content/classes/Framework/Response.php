<?php

class Framework_Response
{
	protected $content;
	protected $status;
	protected $contentType;
	protected $headers = array();

	public function __construct($content = '', $status = 200, $contentType = 'text/html')
	{
		$this->content = $content;
		$this->status = $status;
		$this->contentType = $contentType;
	}

	public function setHeader($header, $value)
	{
		if (strtolower($header) == 'content-type')
		{
			$this->setContentType($value);
		}
		else
		{
			$this->headers[$header] = $value;
		}
	}

	public function unsetHeader($header)
	{
		unset($this->headers[$header]);
	}

	public function getHeader($header)
	{
		return array_key_exists($header, $this->headers) ? $this->headers[$header] : null;
	}

	public function send()
	{
		header('HTTP/1.0 '.$this->status);
		foreach ($this->headers as $header => $value)
		{
			header($header.': '.$value);
		}
		header('Content-Type: '.$this->getContentType());
		echo $this->content;
	}

	public function getContentType()
	{
		return $this->contentType;
	}

	public function setContentType($contentType)
	{
		$this->contentType = $contentType;
	}
}
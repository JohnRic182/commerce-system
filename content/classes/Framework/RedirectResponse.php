<?php

class Framework_RedirectResponse extends Framework_Response
{
	protected $redirectUrl;

	public function __construct($redirectUrl = '', $status = 301, $contentType = 'text/html')
	{
		$this->redirectUrl = $redirectUrl;
		$this->status = $status;
		$this->contentType = $contentType;
	}

	public function send()
	{
		header('HTTP/1.0 '.$this->status);
		foreach ($this->headers as $header => $value)
		{
			if (strtolower($header) != 'location') header($header.': '.$value);
		}
		header('Content-Type: '.$this->getContentType());
		header('Location: '.$this->getRedirectUrl());
	}

	/**
	 * @param string $redirectUrl
	 */
	public function setRedirectUrl($redirectUrl)
	{
		$this->redirectUrl = $redirectUrl;
	}

	/**
	 * @return string
	 */
	public function getRedirectUrl()
	{
		return $this->redirectUrl;
	}
}

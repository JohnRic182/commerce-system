<?php

class Framework_Registry
{
	protected $services = array();
	protected $loading = array();
	protected $serviceInstances = array();
	protected $parameters;

	public function __construct($parameters = array())
	{
		$this->parameters = $parameters;
	}

	public function setService($key, $className, array $parameters = array())
	{
		$this->set($key, new Framework_Registry_Definition($className, $parameters));
	}

	public function getParameter($name)
	{
		return isset($this->parameters[$name]) ? $this->parameters[$name] : null;
	}

	public function hasParameter($name)
	{
		return isset($this->parameters[$name]);
	}

	public function setParameter($name, &$value)
	{
		$this->parameters[$name] = $value;
	}

	public function set($key,  $service)
	{
		if ($service instanceof Framework_Registry_Definition)
		{
			$this->services[$key] = $service;
		}
		else
		{
			$this->serviceInstances[$key] = $service;
		}
	}

	public function has($key)
	{
		return isset($this->services[$key]) || array_key_exists($key, $this->services) ||
		isset($this->serviceInstances[$key]) || array_key_exists($key, $this->serviceInstances);
	}

	public function get($key, $throwExceptionOnNotFound = true)
	{
		if (isset($this->loading[$key]))
		{
			throw new Exception('Circular Reference: '.$key);
		}

		if (!$this->has($key))
		{
			if ($throwExceptionOnNotFound)
			{
				throw new Exception('Service '.$key.' not found');
			}
		}

		$service = null;

		if (isset($this->serviceInstances[$key]))
		{
			$service = $this->serviceInstances[$key];
		}
		else
		{
			$this->loading[$key] = true;

			/** @var Framework_Registry_Definition $definition */
			$definition = $this->services[$key];


			$className = $definition->getClassName();
			if (!class_exists($className))
			{
				throw new Exception('Class '.$className.' not found');
			}

			$values = array();
			foreach ($definition->getParameters() as $parameter)
			{
				if (substr($parameter, 0, 1) == '%' && substr($parameter, -1, 1) == '%')
				{
					$parameter = trim($parameter, '%');
					$values[] = $this->getParameter($parameter);
				}
				else if (substr($parameter, 0, 1) == '@')
				{
					$values[] = $this->get(substr($parameter, 1));
				}
				else
				{
					$values[] = $parameter;
				}
			}

			$ref = new ReflectionClass($className);
			$service = $ref->newInstanceArgs($values);
			$this->serviceInstances[$key] = $service;
		}

		unset($this->loading[$key]);

		return $service;
	}
}
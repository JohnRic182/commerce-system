<?php

class Framework_Session implements IteratorAggregate, Countable
{
	/** @var Framework_Session_StorageInterface $storage */
	protected $storage;

	/**
	 * Session constructor
	 */
	public function __construct(Framework_Session_StorageInterface $storage)
	{
		$this->storage = $storage;
	}

	public function start()
	{
		$this->storage->start();
	}

	public function isStarted()
	{
		return $this->storage->isStarted();
	}

	public function has($name)
	{
		return $this->storage->has($name);
	}

	public function get($name, $default = null)
	{
		return $this->storage->get($name, $default);
	}

	public function set($name, $value)
	{
		$this->storage->set($name, $value);
	}

	public function all()
	{
		return $this->storage->all();
	}

	public function replace(array $values)
	{
		$this->storage->replace($values);
	}

	public function remove($name)
	{
		$this->storage->remove($name);
	}

	public function clear()
	{
		$this->storage->clear();
	}

	public function invalidate($lifetime = null)
	{
		$this->storage->clear();

		return $this->regenerate(true, $lifetime);
	}

	public function regenerate($destroy = false, $lifetime = null)
	{
		return $this->storage->regenerate($destroy, $lifetime);
	}

	public function save()
	{
		$this->storage->save();
	}

	public function getId()
	{
		return $this->storage->getId();
	}

	public function setId($id)
	{
		$this->storage->setId($id);
	}

	public function getName()
	{
		return $this->storage->getName();
	}

	public function setName($name)
	{
		$this->storage->setName($name);
	}

	public function getIterator()
	{
		return $this->storage->getIterator();
	}

	/**
	 *
	 */
	public function count()
	{
		return $this->storage->count();
	}
}
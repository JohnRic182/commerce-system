<?php

class Framework_Translator
{
	protected $msg;

	public function __construct(array &$msg)
	{
		$this->msg = $msg;
	}

	public function trans($id, array $parameters = null)
	{
		$value = $this->get($id, $this->msg);

		return $parameters === null ? $value : strtr($value, $parameters);
	}

	protected function get($id, &$source)
	{
		if (array_key_exists($id, $source))
		{
			return $source[$id];
		}

		return $id;
	}

	protected static $instance;

	public static function getInstance($msg = false)
	{
		if (is_null(self::$instance))
		{
			if (!$msg)
			{
				global $msg;
			}

			self::$instance = new Framework_Translator($msg);
		}

		return self::$instance;
	}

	public static function cache($msg, $cache_location)
	{
		$temp = array();

		self::flatten('', $msg, $temp);

		$output = '<?php
return array(
';
		foreach ($temp as $key => $value)
		{
			$output .= '	\''.str_replace("'", "\\'", $key).'\'=>\''.str_replace("'", "\\'", $value)."',\n";
		}
		$output .= ');
';

		file_put_contents($cache_location, $output);
	}

	protected static function flatten($parentKey, &$msg, &$temp)
	{
		if (is_array($msg))
		{
			foreach ($msg as $key => $value)
			{
				$cur_key = (trim($parentKey) != '' ? $parentKey.'.' : '').$key;

				if (is_array($value))
				{
					self::flatten($cur_key, $value, $temp);
				}
				else
				{
					$temp[$cur_key] = $value;
				}
			}
		}
		else if (trim($parentKey) != '')
		{
			$temp[$parentKey] = $msg;
		}
	}
}
<?php

//TODO: Refactor Ddm_View and Ddm_AdminView to utilize a base class
class Ddm_AdminView
{
	protected $vars = array();
	protected $_settings;
	protected $_view;

	private $_permDir = 0777;
	private $_permFile = 0666;
	private $_cache_map = array();

	protected $_skinName = 'default';

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		global $label_app_name, $label_admin_tracking_script, $label_support_area, $admin, $_SESSION;
		global $label_terms, $label_privacy_policy, $label_help_enabled, $label_help_url, $label_first_data, $label_stamps;

		$this->_settings = settings();

		require_once("content/vendors/smarty/Smarty.class.php");
		$this->_view = new Smarty();

		$this->setTemplateDir("content/admin/skins/".$this->_skinName."/");
		$this->setCompileDir("content/cache/smarty/skins/_admin/".$this->_skinName."/");

		require_once("content/smarty_plugins/function.product_image.php");
		require_once("content/smarty_plugins/function.product_minmax_order.php");
		require_once("content/smarty_plugins/function.lang.php");
		require_once("content/smarty_plugins/function.custom_field.php");
		require_once("content/smarty_plugins/function.script.php");
		require_once("content/smarty_plugins/function.link.php");
		require_once("content/smarty_plugins/function.meta.php");
		require_once("content/smarty_plugins/function.fill_cells.php");
		require_once("content/smarty_plugins/function.class.php");
		require_once("content/smarty_plugins/function.form.php");
		require_once("content/smarty_plugins/function.admin_tabs.php");
		require_once("content/smarty_plugins/function.field.php");
		require_once("content/smarty_plugins/function.button.php");
		require_once("content/smarty_plugins/function.getp.php");
		require_once("content/smarty_plugins/function.getpp.php");
		require_once("content/smarty_plugins/function.trans.php");
		require_once('content/smarty_plugins/modifier.mynum.php');

		$this->_view->register_function("product_image", "smarty_function_product_image");
		$this->_view->register_function("product_minmax_order", "smarty_function_product_minmax_order");
		$this->_view->register_function("lang", "smarty_function_lang");
		$this->_view->register_function("cfield", "smarty_function_custom_field");
		$this->_view->register_function("script", "smarty_function_script");
		$this->_view->register_function("link", "smarty_function_link");
		$this->_view->register_function("meta", "smarty_function_meta");
		$this->_view->register_function("fill_cells", "smarty_function_fill_cells");
		$this->_view->register_function("class", "smarty_function_class");
		$this->_view->register_function("button", "smarty_function_button");
		$this->_view->register_function('field', 'smarty_function_field');
		$this->_view->register_function('form', 'smarty_function_form');
		$this->_view->register_function('admin_tabs', 'smarty_function_admin_tabs');
		$this->_view->register_function('getp', 'smarty_function_getp');
		$this->_view->register_function('getpp', 'smarty_function_getpp');
		$this->_view->register_function('trans', 'smarty_function_trans');
		$this->_view->register_modifier("price", strstr($_SERVER["SCRIPT_FILENAME"], 'admin.php') ? "getAdminPrice" : "getPrice");
		$this->_view->register_modifier("weight", "getWeight");
		$this->_view->register_modifier("gs", "gs");
		$this->_view->register_modifier("stripslashes", "stripslashes");
		$this->_view->register_modifier('gn', 'smarty_modifier_mynum');
		$this->_view->register_modifier("smartround", "myNum");
		$this->_view->register_modifier("htmlspecialchars", "smarty_modifier_htmlspecialchars");

		$this->_view->assign('settings', $this->_settings);
		$this->_view->assign('index_url', $this->_settings['GlobalHttpUrl'] . '/' . $this->_settings['INDEX']);
		$this->_view->assign('label_app_name', $label_app_name);
		$this->_view->assign('label_admin_tracking_script', $label_admin_tracking_script);
		$this->_view->assign('label_support_area', $label_support_area);
		$this->_view->assign('label_terms', $label_terms);
		$this->_view->assign('label_privacy_policy', $label_privacy_policy);
		$this->_view->assign('label_help_enabled', $label_help_enabled);
		$this->_view->assign('label_help_url', $label_help_url);
		$this->_view->assign('label_first_data', $label_first_data);
		$this->_view->assign('label_stamps', $label_stamps);
		$this->_view->assign('is_label', IS_LABEL);

		$admin['avatar_thumb'] = false;
		if (isset($admin['avatar']) && !empty($admin['avatar']))
		{
			$admin['avatar_thumb'] = dirname($admin['avatar']) . '/thumbs/' . basename($admin['avatar']);
		}

		$this->_view->assign('admin', $admin);

		foreach($this->_settings as $key=>$value) $this->_view->assign($key, $value);

		//Intuit anywhere parameters
		global $menu_proxy, $grant_url, $oauth_token;
		require_once dirname(dirname(dirname(__FILE__))) . '/vendors/intuit/oauth/config.php';
		$this->_view->assign('menu_proxy', $menu_proxy);
		$this->_view->assign('grant_url', $grant_url);

		$intuitConnectionActive = false;
		$intuitConnected = false;

		// TODO: check is it require to call every time

		intuit_Services_QBSync::create();
		$oauth_token = isset($oauth_token) ? $oauth_token: '';
		if ($this->_settings['intuit_anywhere_enabled'] == 'Yes')
		{
			if ($this->_settings['intuit_anywhere_connected'] == 'Yes' && $oauth_token != '')
			{
				$intuitConnectionActive = true;
				$intuitConnected = true;
			}
			else
			{
				$intuitConnectionActive = false;
				$intuitConnected = true;
			}
		}

		$this->_view->assign('intuitConnectionActive', $intuitConnectionActive);
		$this->_view->assign('intuitConnected', $intuitConnected);

		$this->_view->assign('APP_VERSION', APP_VERSION);

		$this->_permFile = FileUtils::getFilePermissionMode();
		$this->_permDir = FileUtils::getDirectoryPermissionMode();
	}

	/**
	 * Get static instance
	 *
	 * @staticvar string $instance
	 *
	 * @return self 
	 */
	public static function getInstance()
	{
		static $instance = null;
		if (is_null($instance)) $instance = new self;
		return $instance;
	}

	/**
	 * Sets templates directory
	 * @param string $directory
	 */
	public function setTemplateDir($directory)
	{
		$this->_view->template_dir = $this->_view->config_dir = $directory;
	}
	
	/**
	 * Sets smart template directory
	 *
	 * @param string $directory
	 */
	public function setCompileDir($directory)
	{
		if (!is_dir($directory)) mkdir($directory, $this->_permDir, true);
		$this->_view->compile_dir = $directory;
	}

	/**
	 * Assign variable
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function assign($key, $value)
	{
		$this->vars[$key] = $value;
	}

	/**
	 * Renders admin view
	 *
	 * @param string $view 
	 * @return void
	 */
	public function render($view)
	{
		echo $this->fetch($view);
	}

	/**
	 * @param $view
	 * @return mixed|string|void
	 */
	public function fetch($view)
	{
		//TODO: should not be here
		global $auth_rights;

		//TODO: Event?!
		$flashMessagesByTypes = Admin_Flash::getMessages();
		$flashMessages = array();
		foreach ($flashMessagesByTypes as $flashMessagesByType)
		{
			foreach ($flashMessagesByType as $flashMessage)
			{
				$flashMessages[] = $flashMessage;
			}
		}

		$this->vars['_FLASHES_'] = $flashMessages;

		foreach ($this->vars as $key => $value)
		{
			$this->_view->assign($key, $value);
		}

		if (defined('PRIVILEGE_ALL'))
		{
			// menu
			$adminMenuProvider = new Admin_MenuProvider();
			$adminMenuGenerator = new Admin_MenuGenerator();
			$generatedMenu = $adminMenuGenerator->generate($adminMenuProvider->getMenu(), null, null, getAdminPrivileges($auth_rights));
			$this->_view->assign('leftSideMenu', $generatedMenu);

			$adminSiteMap = Admin_Service_Search::getSiteMap();
			$this->_view->assign('adminSiteMap', json_encode($adminSiteMap));
		}

		return $this->_view->fetch('templates/'.$view.'.html');
	}
}
<?php

/**
* 
*/
class Ddm_Skins
{
	private $_skin_name = null;
	private $_source_skin_name = 'original';
	
	private $_modified_files = array();
	
	private function __construct() {}
	
	public static function getInstance()
	{
		static $_id = null;
		if (is_null($_id)) $_id = new self;
		return $_id;
	}
	
	public function setSkin($skin_name)
	{
		if (!is_dir('content/skins/'.$skin_name)) throw new Exception('The skin does not exist');
		
		$this->_skin_name = $skin_name;
	}
	
	public function setSourceSkin($skin_name)
	{
		$this->_source_skin_name = $skin_name;
	}
	
	public function migrate()
	{
		// skin name must be set before assembling
		if (is_null($this->_skin_name)) throw new Exception('A skin file must be set to migrate');
		if (!is_writable("content/skins")) throw new Exception('content/skins must be writable. Please correct permissions and try again.');
		
		
		$skins = array();
		if ($skins_dir_object = dir('content/skins'))
		{
			while ($skin_dir = $skins_dir_object->read())
			{
				if ($skin_dir{0} != '_' && !in_array($skin_dir, array('.', '..', '.svn', 'original')))
				{
					try
					{
						if (is_dir('content/skins/'.$skin_dir))
						{
							if (is_writable('content/skins/'.$skin_dir))
							{
								$skins[] = $skin_dir;
							} else {
								throw new Exception("$skin_dir is not writable");
							}
						}
					} catch (Exception $e)
					{
						var_dump($e);
						exit;
					}
				}
			}
		}
		
		foreach ($skins as $skin_name)
		{
			$this->_migrateFiles($skin_name);
		}
		
		// finally rename original to legacy
		$this->_migrateOriginalFiles();
	}
	
	/**
	 * Migrates the original directory to the _legacy directory which is used when assembling skins
	 *
	 * @return void
	 */
	private function _migrateOriginalFiles()
	{
		// if the directory already exists, return immediately
		if (file_exists('content/skins/_legacy/')) return;
		
		// create temporary directory
		mkdir('content/skins/_tmp_legacy', 0777, true);
		copy_dir('content/skins/original', 'content/skins/_tmp_legacy');
		remove_dir('content/skins/original');
		rename('content/skins/_tmp_legacy', 'content/skins/_legacy');
	}
	
	/**
	 * Migrates the skin files into the new skin directory structure /skin_name/templates/files...
	 *
	 * @param string $skin_name 
	 * @return void
	 */
	private function _migrateFiles($skin_name)
	{
		// traverse the directories
		$this->_traverse();
		
		remove_dir("content/skins/_tmp_{$skin_name}/");
		
		// copy any modified files and directories to a temp directory
		foreach ($this->_modified_files as $source)
		{
			$destination = str_replace("content/skins/{$skin_name}/", "content/skins/_tmp_{$skin_name}/", $source);
			
			// get path info for destination and create directories as needed
			$pathinfo = pathinfo($destination);
			if (!is_dir($pathinfo['dirname'])) mkdir($pathinfo['dirname'], 0777, true);
			
			// copy the file
			copy($source, $destination);
		}
		
		// remove the skin directory
		remove_dir('content/skins/'.$skin_name);
		
		// rename the temp directory back
		rename("content/skins/_tmp_{$skin_name}", "content/skins/{$skin_name}");
	}
	
	/**
	 * Traverses directory structure and gets differences between two skins
	 *
	 * @param string $starting_directory 
	 * @return array Array of different files from source
	 */
	private function _traverse($starting_directory = null)
	{ 
		static $root_directory = null;
		
		// initialize the starting directory and root directory as the skin we are assembling
		if (is_null($starting_directory)) $starting_directory = 'content/skins/'.$this->_skin_name;
		if (is_null($root_directory)) $root_directory = $starting_directory;

		if ($d_obj = dir($starting_directory))
		{ 
			while ($this_entry = $d_obj->read())
			{ 
				if (!in_array($this_entry, array('.', '..', '.svn')))
				{
					// Check to see if the entry we've pulled is a directory or a file
					if (is_dir("$starting_directory/$this_entry"))
					{ 
						// descend into this directory
						$this->_traverse("$starting_directory/$this_entry"); 
					}
					else
					{ 
						// compare the file with the source file for modifications
						$target = "{$starting_directory}/{$this_entry}";
						$source = str_replace('skins/'.$this->_skin_name, 'skins/'.$this->_source_skin_name, $target);
						if (!file_exists($source) || file_get_contents($target) !== file_get_contents($source))
						{
							$this->_modified_files[] = $target;
						}
					}
				} 
			}
			
			$d_obj->close();
		} else {
			echo "Error!  Couldn't list directory $starting_directory for some reason!";
			exit;
		}
	}
}
<?php

class Ddm_ProductReviews
{
	private $_db;
	private $_settings;
	
	/**
	 * The constructor
	 *
	 * @param DB $db
	 * @param array $settings
	 */
	public function __construct(DB $db = null, $settings = null)
	{
		$this->_db = is_null($db) ? $db : db();
		$this->_settings = is_null($settings) ? settings() : $settings;
	}

	/**
	 * Validate review data
	 *
	 * @param $review_data
	 *
	 * @return bool
	 */
	protected function validateReviewData($review_data)
	{
		return
			is_array($review_data) && isset($review_data['pid']) && isset($review_data['user_id'])
			&& isset($review_data['review_title']) && isset($review_data['review_text']) && isset($review_data['rating']);
	}

	/**
	 * Adds a product review to the database
	 *
	 * @param array $review_data
	 *
	 * @return int Returns review id on success, FALSE on failure
	 */
	public function addReview($review_data)
	{
		// make sure we have review data
		if (!$this->validateReviewData($review_data)) return false;

		$pid = $review_data['pid'];
		$user_id = $review_data['user_id'];

		$purchased = $this->isPurchasedProduct($pid, $user_id) ? 1 : 0;
	
		// auto approve the review if the admin has enabled these settings
		$status =	(
			$this->_settings['ReviewsAutoApprove'] == '1' &&
			(
				intval($review_data['rating']) >= intval($this->_settings['ReviewsAutoApproveRating']) ||
				($this->_settings['ReviewsAutoApprovePurchased'] == '1' && $purchased)
			)
		) ? 'approved' : 'pending';
		
		// reset the database handler and prepare the insert
		$this->_db->reset();
		$this->_db->assign("pid", intval($pid));
		$this->_db->assign("user_id", intval($user_id));
		$this->_db->assign("date_created", "NOW()");
		$this->_db->assignStr("review_title", $review_data['review_title']);
		$this->_db->assignStr("review_text", $review_data['review_text']);
		$this->_db->assignStr("rating", $review_data['rating']);
		$this->_db->assignStr("status", $status);
		$this->_db->assignStr("purchased", $purchased);
		
		// insert and return review id
		$reviewId = $this->_db->insert(DB_PREFIX."products_reviews");
		
		$rating = $this->getProductRating($pid);
		
		$this->_db->reset();
		$this->_db->assignStr("rating", $rating["rating"]);
		$this->_db->assignStr("reviews_count", $rating["total_reviews"]);
		$this->_db->update(DB_PREFIX."products", "WHERE pid='".intval($pid)."'");

		Notifications::emailNewProductReview($this->_db, $reviewId);
		
		return $reviewId;
	}
	
	/**
	 * Checks if product has been purchased (order status 'Completed') by the user
	 *
	 * @param int $product_id
	 * @param int $user_id
	 *
	 * @return bool 
	 */
	public function isPurchasedProduct($product_id, $user_id)
	{
		$this->_db->query('
			SELECT COUNT(0) 
			FROM '.DB_PREFIX.'orders_content oc
			JOIN '.DB_PREFIX.'orders o ON o.oid = oc.oid
			WHERE 
				o.status = "Completed"
				AND oc.pid = '.intval($product_id).'
				AND o.uid = '.intval($user_id).'
		');
		
		return (bool)current($this->_db->moveNext());
	}
	
	/**
	 * Gets the reviews for the given product ids
	 *
	 * @param mixed $product_ids
	 *
	 * @return array Returns an array of reviews
	 */
	public function getReviews($product_ids)
	{
		$ids = is_array($product_ids) ? $product_ids : array($product_ids);
		
		$this->_db->query('
			SELECT
				products_reviews.*,
				date_created as date_created1,
				DATE_FORMAT(date_created, "'.$this->_settings["LocalizationDateTimeFormat"].'") AS date_created,
				users.login AS user_login
			FROM '.DB_PREFIX.'products_reviews AS products_reviews
			LEFT JOIN '.DB_PREFIX.'users AS users ON users.uid=products_reviews.user_id AND users.removed="No"
			WHERE 
				pid IN ('.implode(',', $ids).")
				AND status = 'approved'
			ORDER BY date_created1 DESC
		");
		
		return $this->_db->getRecords();
	}
	
	/**
	 * Returns reviews coung by given product id
	 * 
	 * @param int $product_id
	 *
	 * @return int
	 */
	public function getReviewsCount($product_id)
	{
		$this->_db->query('
			SELECT 
				COUNT(*) AS c
			FROM '.DB_PREFIX.'products_reviews 
			WHERE 
				pid='.intval($product_id)."
				AND status = 'approved'
		");
		return $this->_db->movenext() ? $this->_db->col["c"] : 0;	
	}
	
	/**
	 * Gets a product rating along with other useful information like total number of reviews
	 *
	 * @param int $product_id
	 *
	 * @return array Rating details
	 */
	public function getProductRating($product_id)
	{
		$this->_db->query('
			SELECT
				AVG(rating) as rating,
				COUNT(0) as total_reviews
			FROM '.DB_PREFIX.'products_reviews
			WHERE
				pid = '.intval($product_id)."
			  	AND status = 'approved'
		");
		
		return current($this->_db->getRecords());
	}
	
	/**
	 * Checks if the user has written a product review already for the given product id
	 *
	 * @param int $product_id 
	 * @param int $user_id
	 *
	 * @return bool
	 */
	public function hasWrittenReview($product_id, $user_id)
	{
		$this->_db->query('
			SELECT COUNT(0) 
			FROM '.DB_PREFIX.'products_reviews pr
			WHERE 
				pr.pid = '.intval($product_id).'
				AND pr.user_id = '.intval($user_id).'
		');
		
		return (bool)current($this->_db->moveNext());
	}
}

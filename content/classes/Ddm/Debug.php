<?php

/**
 * undocumented class
 *
 * @package default
 * @author Sebastian Nievas
 **/

class Ddm_Debug
{
	private $_fp = null;
	private $_debug = array();
	private $_log_file = '';
	private $_settings = null;
	
	
	/**
	 * Forces use of public method getInstance
	 *
	 */
	private function __construct()
	{
		$this->_settings = settings();
		$this->_log_file = $this->_settings['GlobalServerPath'].'/content/cache/log/debug.txt';
	}
	
	/**
	 * Returns a singleton for this class
	 *
	 * @return singleton Gets singleton for Ddm_Debug
	 */
	public function getInstance()
	{
		static $_id = null;
		if (is_null($_id)) $_id = new self();
		return $_id;
	}
	
	/**
	 * 
	 *
	 * @return void
	 */
	public function var_dump()
	{
		ob_start();
		foreach (func_get_args() as $arg)
		{
			var_dump($arg);
		}
		$this->_debug[] = ob_get_contents();
		ob_end_clean();
	}
	
	/**
	 * Flushes the output to file
	 *
	 * @return void
	 */
	public function flush()
	{
		if (is_null($this->_fp)) $this->_open();
		// the following does not have tabs for print purposes
		$log_string = "\n\n".date("Y-m-d H:i:s T")."\n=======================================================\n\n".$this->_getDebugString();
		$this->clearStack();
		fwrite($this->_fp, $log_string);
	}
	
	/**
	 * Returns the string
	 *
	 * @return string
	 */
	private function _getDebugString()
	{
		return implode("\r\n", $this->_debug);
	}
	
	
	/**
	 * Sends the current log stack that hasn't been flushed to log file
	 *
	 * @param string $to Valid email address to send email to
	 * @param string $subject The email's subject
	 * @param bool $html Send email with HTML headers
	 * @return void
	 */
	public function mail($to, $subject, $html = false)
	{
		$headers = 'From: Ddm_Debug<nobody>'."\r\n";
		if ($html)
		{
			$headers .= 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		}
		
		mail($to, $subject, $this->_getDebugString(), $headers);
	}
	
	/**
	 * Appends a message to the log stack
	 *
	 * @param string $string 
	 * @return void
	 */
	public function log($string)
	{
		$this->_debug[] = $string;
	}
	
	
	/**
	 * Truncates the log file to zero length
	 *
	 * @return void
	 */
	public function clearLogFile()
	{
		if (!is_null($this->_fp)) $this->close();
		$this->_fp = fopen($this->_log_file, 'w');
		$this->close();
	}
	
	/**
	 * Clears the log stack
	 *
	 * @return void
	 */
	public function clearStack()
	{
		$this->_debug = array();
	}
	
	/**
	 * Closes and writes to file
	 *
	 * @return void
	 */
	public function close()
	{
		if (is_null($this->_fp)) return;
		fclose($this->_fp);
		$this->_fp = null;
	}
	
	/**
	 * Opens the log file for writing in append mode (a+)
	 *
	 * @return void
	 */
	private function _open()
	{
		$this->_fp = fopen($this->_log_file, 'a+') or die('Could not open the debug log file for writing');
	}
}
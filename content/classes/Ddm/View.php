<?php
/**
 * @package default
 * @author Sebastian Nievas
 * @author Victor Falendysh
 **/

class Ddm_View
{
	private $_view = null;
	private $_layout = 'default';
	private $_title = '';
	private $_metatags = array();
	private $_stylesheets = array();
	private $_javascripts = array();
	private $_layout_elements = array();
	private $_layout_page = 'catalog';
	private $_cache_map = array();
	private $_settings = array(); 
	private $_images = false;
	private $_packJS = false;
	private $_permDir = 0777;
	private $_permFile = 0666;
	
	/**
	 * Forces singleton use via getInstance method
	 */
	private function __construct()
	{

		$this->_settings = settings();

		require_once("content/vendors/smarty/Smarty.class.php");
		$this->_view = new Smarty();
		
		$this->setTemplateDir("content/cache/skins/".escapeFileName($this->_settings["DesignSkinName"])."/");
		$this->setCompileDir("content/cache/smarty/skins/".escapeFileName($this->_settings["DesignSkinName"])."/");
		
		$this->_layout_elements = unserialize($this->_settings['LayoutElements']);
		
		// load additional smarty plugins & functions
		require_once("content/smarty_plugins/function.product_image.php");
		require_once("content/smarty_plugins/function.product_minmax_order.php");
		require_once("content/smarty_plugins/function.lang.php");
		require_once("content/smarty_plugins/function.custom_field.php");
		require_once("content/smarty_plugins/function.script.php");
		require_once("content/smarty_plugins/function.link.php");
		require_once("content/smarty_plugins/function.meta.php");
		require_once("content/smarty_plugins/function.fill_cells.php");
		require_once("content/smarty_plugins/function.class.php");
		require_once("content/smarty_plugins/function.button.php");
		require_once('content/smarty_plugins/modifier.mynum.php');

		require_once ('content/smarty_plugins/function.bodyclose_js_snippets.php');
		require_once ('content/smarty_plugins/prefilter.bodyclose_js_snippets.php');
		require_once ('content/smarty_plugins/function.headclose_css_snippets.php');
		require_once ('content/smarty_plugins/prefilter.headclose_css_snippets.php');

		$this->_view->register_function("product_image", "smarty_function_product_image");
		$this->_view->register_function("product_minmax_order", "smarty_function_product_minmax_order");
		$this->_view->register_function("lang", "smarty_function_lang");
		$this->_view->register_function("cfield", "smarty_function_custom_field");
		$this->_view->register_function("script", "smarty_function_script");
		$this->_view->register_function("link", "smarty_function_link");
		$this->_view->register_function("meta", "smarty_function_meta");
		$this->_view->register_function("fill_cells", "smarty_function_fill_cells");
		$this->_view->register_function("class", "smarty_function_class");
		$this->_view->register_function("button", "smarty_function_button");
		$this->_view->register_modifier("price", strstr($_SERVER["SCRIPT_FILENAME"], 'admin.php') ? "getAdminPrice" : "getPrice");
		$this->_view->register_modifier("weight", "getWeight");
		$this->_view->register_modifier("gs", "gs");
		$this->_view->register_modifier("stripslashes", "stripslashes");
		$this->_view->register_modifier('gn', 'smarty_modifier_mynum');
		$this->_view->register_modifier("smartround", "myNum");
		$this->_view->register_modifier('price_round', 'PriceHelper::round');

		$this->_view->register_function('bodyclose_js_snippets', 'smarty_function_bodyclose_js_snippets');
		$this->_view->register_prefilter('smarty_prefilter_bodyclose_js_snippets');
		$this->_view->register_function('headclose_css_snippets', 'smarty_function_headclose_css_snippets');
		$this->_view->register_prefilter('smarty_prefilter_headclose_css_snippets');

		$this->_view->register_modifier("htmlspecialchars", "smarty_modifier_htmlspecialchars");

		if (is_file('content/smarty_plugins/function.product_pack_title.php'))
		{
			require_once 'content/smarty_plugins/function.product_pack_title.php';
			$this->_view->register_function("product_pack_title", "smarty_function_product_pack_title");
		}

		if (is_file('content/smarty_plugins/function.product_pack_title.php'))
		{
			require_once 'content/smarty_plugins/function.product_pack_value.php';
			$this->_view->register_function("product_pack_value", "smarty_function_product_pack_value");
		}

		$this->_view->assign("settings", $this->_settings);
		$this->_view->assign("index_url", $this->_settings["GlobalHttpUrl"]."/".$this->_settings["INDEX"]);
		foreach($this->_settings as $key=>$value) $this->_view->assign($key, $value);

		$this->_permFile = FileUtils::getFilePermissionMode();
		$this->_permDir = FileUtils::getDirectoryPermissionMode();
	}

	public function setLayout($l = 'default')
	{
		$this->_layout = $l;
	}

	public function loadSkinSettings($skin_name = null)
	{
		global $settings;

		if (is_null($skin_name)) $skin_name = $settings['DesignSkinName'];

		$template_xml = simplexml_load_string(file_get_contents('content'.DS.'skins'.DS.escapeFileName($skin_name).DS.'skin.xml'), 'SimpleXMLElement', LIBXML_NOCDATA);

		$layoutSiteWidth = $template_xml->xpath("/skin/settings/option[@name='LayoutSiteWidth']");
		$settings['LayoutSiteWidth'] = (count($layoutSiteWidth) && $layoutSiteWidth !== false)
				? $layoutSiteWidth[0]->attributes()->value
				: $settings['LayoutSiteWidthDefault'];

		$layoutElements = $settings['LayoutElementsDefault'];
		if (isset($template_xml->defaultLayoutElements) && (string) $template_xml->defaultLayoutElements !== '')
		{
			// suppress any possible errors when unserializing, and check if this is a serialized array
			if (($default_layout = @unserialize($template_xml->defaultLayoutElements)) !== false && is_array($default_layout))
			{
				$settings['LayoutElements'] = $template_xml->defaultLayoutElements;
			}
		}
	
		if (isset($template_xml->settings))
		{
			foreach ($template_xml->settings->children() as $setting)
			{
				$settings[$setting->getName()] = (String)$setting;
			}
		}

		$this->_settings = settings();
	}

	/**
	 * Returns template dir
	 * @return string
	 */
	public function templateDir()
	{
		return $this->_view->template_dir;
	}
	
	/**
	 * Sets templates directory
	 * @param string $directory
	 */
	public function setTemplateDir($directory)
	{
		$this->_view->template_dir = $this->_view->config_dir = $directory;

		// assign skin variables
		$this->_view->assign("skin_images", $this->_view->template_dir."images/");
		$this->_view->assign("skin_styles", $this->_view->template_dir."styles/");
		$this->_view->assign("skin_themes", $this->_view->template_dir.'styles/');
		$this->_view->assign("skin_javascript", $this->_view->template_dir."javascript/");
	}
	
	/**
	 * Sets smart template directory
	 * @param string $directory
	 */
	public function setCompileDir($directory)
	{
		if (!is_dir($directory)) mkdir($directory, $this->_permDir, true);
		$this->_view->compile_dir = $directory;
	}
	
	/**
	 * Gets single instance
	 *
	 * @var string Optional Namespace to use. Default is 'default'
	 * @return void
	 */
	public static function getInstance($namespace = 'default')
	{
		static $_instances = array();
		if (!array_key_exists($namespace, $_instances)) $_instances[$namespace] = new self;
		return $_instances[$namespace];
	}


	/**
	 *
	 */
	public function initNotificationsSmarty()
	{
		global $settings, $msg;

		$this->assign('env', 'email');
		require_once($settings['GlobalServerPath'].'/content/languages/'.escapeFileName($settings['ActiveLanguage']).".php");
		$this->assign('msg', $msg);
		$this->assign('designImages', $this->getImages());
	}

	/**
	 * Magic Method Setter for view
	 *
	 * @param string $name 
	 * @param string $value 
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->_view->$name = $value;
	}
	
	/**
	 * Magic Method Getter for view
	 *
	 * @param string $name 
	 * @return mixed
	 */
	public function __get($name)
	{
		return $this->_view->$name;
	}
	
	/**
	 * Dynamically calls the view's functions
	 *
	 * @param string $method The method to call
	 * @param array $args An array of arguments passed
	 * @return mixed
	 */
	public function __call($method, $args)
	{
		return call_user_func_array(array($this->_view, $method), $args);
	}
	
	/**
	 * Renders layout
	 *
	 * @param string $layout 
	 * @return void
	 */
	public function render($layout)
	{
		$this->_layout_elements = unserialize($this->_settings['LayoutElements']);
		
		$current_page = $this->_view->get_template_vars('current_page');
		
		if (!isset($this->_layout_elements["pages"]['_global']))
		{
			$this->_layout_elements["pages"]['_global'] =  array();
		}
		
		$page_elements = (array_key_exists($current_page, $this->_layout_elements["pages"]))
			? $this->_layout_elements["pages"][$current_page]
			: $this->_layout_elements["pages"]['_global'];
		
		$this->_view->assign('layout', $page_elements);
		
		// finally assign the page
		$this->_view->assign('page', $p = array(
			'layout' => 'templates/layouts/'.$this->_layout.'.html',
			'title' => $this->_title,
			'elements' => $page_elements
		));
		
		// Load the toolbar elements only if we need them in design mode
		$this->_view->assign('toolbar', $this->getRegisteredElements());

		// finally display the layout
		echo Minify::minifyHTML($this->_view->fetch($layout));
	}


	/**
	 * Get all valid registered elements to display on the design-mode toolbar
	 */
	public function getRegisteredElements()
	{
		db()->query('SELECT name,title,type FROM '.DB_PREFIX.'layout_elements WHERE is_active = 1');
		
		$elements = array();
		while (db()->movenext())
		{
			$elements[db()->col['type']][] = array(
					'id' => db()->col['name'],
					'title' => db()->col['title']
				);
		}
		return $elements;
	}

	/**
	 * @param $language all|javascript|css
	 * @return mixed
	 */
	public function getActiveSnippets($language)
	{
		if (in_array($language, array('javascript', 'css')))
		{
			return db()->selectAll('SELECT * FROM ' . DB_PREFIX . 'snippets WHERE is_visible="Yes" AND language = "'.db()->escape($language).'" ORDER BY priority');
		}
		return db()->selectAll('SELECT * FROM ' . DB_PREFIX . 'snippets WHERE is_visible="Yes" ORDER BY priority');
	}

	/**
	 * Stylesheets getter
	 *
	 * @return array
	 */
	
	public function getStylesheets()
	{
		return $this->_stylesheets;
	}
	
	/**
	 * javascripts getter
	 *
	 * @return array
	 */
	public function getJavascripts()
	{
		return $this->_javascripts;
	}
	
	/**
	 * Metatags getter
	 *
	 * @return array
	 */
	public function getMetatags()
	{
		return $this->_metatags;
	}
	
	/**
	 * Gets a properly formatted script tag
	 *
	 * @param string $file 
	 * @param string $override_path 
	 * @return void
	 */
	public function getJavascriptString($file, $params)
	{
		return '<script type="text/javascript" src="'.$file.'" '.$this->parseParams($params).'></script>';
	}
	
	/**
	 * Appends a javascript file to the end of the javascript display stack
	 *
	 * @param string $file The javascript filename
	 * @param string $path The path to the javascript file if overriding
	 * @return void
	 */
	public function appendJavascript($files, $params = array())
	{
		if (!is_array($files)) $files = array($files);
		
		// $params = $this->parseParams($params);
		
		foreach ($files as $file)
		{
			$filename = $this->getJavascriptString($file, $params);
			
			if (!in_array($filename, array_values($this->_javascripts)))
				$this->_javascripts[] = $filename;
		}
	}
	
	/**
	 * Prepends a javascript file to the beginning of the javascript display stack
	 *
	 * @param string $file 
	 * @return void
	 */
	public function prependJavascript($files, $params = array())
	{
		if (!is_array($files)) $files = array($files);
		
		// $files = array_reverse($files);
		
		foreach ($files as $file)
		{
			$filename = $this->getJavascriptString($file, $params);

			if (!in_array($filename, array_values($this->_javascripts)))
				array_unshift($this->_javascripts, $filename);
		}
	}
	
	/**
	 * Remove a javascript file from the javascript display stack
	 *
	 * @param string $file 
	 * @return void
	 */
	public function removeJavascript($files, $params = array())
	{
		if (!is_array($files)) $files = array($files);
		
		$params = $this->parseParams($params);
		
		foreach ($files as $file)
		{
			unset($this->_javascripts[array_search($this->getJavascriptString($file, $params), $this->_javascripts)]);
		}
	}
	
	/**
	 * Gets a properly formatted css stylesheet link tag
	 *
	 * @param string $file 
	 * @param string $media 
	 * @param string $override_path 
	 * @return void
	 * @author Sebastian Nievas
	 */
	public function getStylesheetString($file, $params)
	{
		if (!is_array($params)) throw new Exception('Expecting array for $params');
		
		static $defaults = array(
			'rel' => 'stylesheet',
			'type' => 'text/css'
		);
		
		// forces a stylesheet to not be cached
		$nocache = '';
		if (isset($params['nocache']))
		{
			$nocache =  ($params['nocache'] == true) ? uniqid() : '';
			
			$file .= (strstr($file, '?'))
				? '&'.$nocache
				: '?'.$nocache;
				
			unset($params['nocache']);
		}
		
		return '<link href="'.htmlspecialchars($file).'" '.$this->parseParams(array_merge($defaults, $params)).' />';
	}
	
	/**
	 * Append a css stylesheet file to the end of the stylesheet display stack
	 *
	 * @param string $file 
	 * @param string $path 
	 * @return void
	 */
	public function appendStylesheet($sources, $params = array())
	{
		if (!is_array($sources)) $sources = array($sources);
		
		foreach ($sources as $file)
		{
			$filename = $this->getStylesheetString($file, $params);
			if (!in_array($filename, array_values($this->_stylesheets)))
				$this->_stylesheets[] = $filename;
		}
	}
	
	/**
	 * Prepend a css stylesheet file to the beginning of the stylesheet display stack
	 *
	 * @param string $file 
	 * @return void
	 */
	public function prependStylesheet($sources, $params = array())
	{
		if (!is_array($sources)) $sources = array($sources);
		
		foreach ($sources as $file)
		{
			$filename = $this->getStylesheetString($file, $params);
			if (!in_array($filename, array_values($this->_stylesheets)))
				array_unshift($this->_stylesheets, $filename);
		}
	}
	
	/**
	 * Removes the stylesheet from the stylesheet display stack
	 *
	 * @param string $file 
	 * @param string $override_path 
	 * @return void
	 * @author Sebastian Nievas
	 */
	public function removeStylesheet($files, $params = array())
	{
		if (!is_array($files)) $files = array($files);
		
		foreach ($files as $file)
		{
			unset($this->_javascripts[array_search($this->getJavascriptString($file, $params), $this->_stylesheets)]);
		}
	}
	
	private function _getMetatagString($name, $content, $is_http_equiv = false, $type = 'name')
	{
		// backwards compatibility before this is depracated. A warning will be issued in next release
		if ($is_http_equiv) $type = 'http-equiv';
		
		return '<meta '.$type.'="'.htmlspecialchars($name).'" content="'.htmlspecialchars($content).'" />';
	}
	
	/**
	 * Append a metatag to the end of the metatag display stack
	 *
	 * @param string $name 
	 * @param string $content 
	 * @param string $is_http_equiv 
	 * @return void
	 */
	public function appendMetatag($name, $content, $is_http_equiv = false, $type = 'name')
	{
		$this->_metatags[] = $this->_getMetatagString($name, $content, $is_http_equiv, $type);
	}
	
	/**
	 * Prepend a metatag to the beginning of the metatag display stack
	 *
	 * @param string $name 
	 * @param string $content 
	 * @param string $is_http_equiv 
	 * @return void
	 */
	public function prependMetatag($name, $content, $is_http_equiv = false, $type = 'name')
	{
		array_unshift($this->_metatags, $this->_getMetatagString($name, $content, $is_http_equiv, $type));
	}
	
	/**
	 * Removes a metatag from the metatag display stack
	 *
	 * @param string $name 
	 * @param string $content 
	 * @param string $is_http_equiv 
	 * @return void
	 */
	public function removeMetatag($name, $content, $is_http_equiv = false, $type = 'name')
	{
		unset($this->_metatags[array_search($this->_getMetatagString($name, $content, $is_http_equiv, $type), $this->_metatags)]);
	}
	
	/**
	 * Sets the page title via the title tag
	 *
	 * @param string $title 
	 * @return void
	 * @author Sebastian Nievas
	 */
	public function setTitle($title)
	{
		$this->_title = $title;
	}
	
	public function setvar($name, $value = null)
	{
		static $_mode = null;
		if (is_null($_mode))
		{
			$this->_view->assign($name, $value);
		}
	}
	
	/**
	 * Parses an array of parameters and returns a trimmed concantenated string in the form of name1="value1" name2="value2" ...
	 *
	 * @param string $params 
	 * @return string
	 */
	public function parseParams($params)
	{
		$string = '';
		foreach ($params as $name => $value)
		{
			$string .= htmlspecialchars($name).'="'.htmlspecialchars($value).'" ';
		}
		
		return trim($string);
	}
	
	/**
	 * Assembles view files from different parts into one folder
	 *
	 * @param array $sources
	 * @param string $destination
	 */
	public function assemble($destination, $sources = null, $custom = true, $force = false)
	{
		$this->_settings = settings();
		set_time_limit(1000);

		if (is_null($sources)) $sources = $this->_getAssembleDefaultSources($custom);

		//assemble skins
		// remove_dir($destination);
		if (!is_dir($destination)) mkdir($destination, $this->_permDir, true);
		
		$types = array("jpg", "png", "gif");
		$images = $this->getImages(true, true);
		
		foreach ($images as $imageId=>$image) $images[$imageId]["content"] = "";
		
		foreach ($sources as $source)
		{
			foreach ($images as $imageId=>$image)
			{
				foreach ($types as $type)
				{
					$s = $source."images/".escapeFileName($imageId).".".$type;
					if (is_file($s))
					{
						$images[$imageId]["content"] = str_replace("\\", "/", $destination)."images/".escapeFileName($imageId).".".$type;
						break;
					}
				}	
			}
			$this->_assembleCopyDir($source, $destination, array('overwrite' => true));
		}
		
		$paths_to_chmod = array();

		foreach ($this->_cache_map as $_destination => $_source)
		{
			if (is_file($_source))
			{
				if ($force || (!file_exists($_destination) || (filemtime($_destination) < filemtime($_source))))
				{
					$pi = pathinfo($_source);
					$destination_info = pathinfo($_destination);
					
					if (!is_dir($destination_info['dirname']))
					{
						
						$dd = @dir($destination_info['dirname']);
						if (!$dd)
						{
							mkdir($destination_info['dirname'], $this->_permDir, true);
							$paths_to_chmod[$destination_info['dirname']] = $destination_info['dirname'];
						}
					}

					if (!array_key_exists('extension', $pi))
					{
						$pi['extension'] = '';
					}

					switch ($pi["extension"])
					{
						//optimize JS
						case "js" :
						{
							if (defined('SKIP_JSMIN') && SKIP_JSMIN)
							{
								file_put_contents($_destination, file_get_contents($_source));
								break;
							}
							if (!class_exists("JSMin")) include("content/vendors/jsmin/jsmin.php");
							file_put_contents($_destination, JSMin::minify(file_get_contents($_source)));
							break;
						}
					
						//optimize CSS
						case "css" :
						{
							file_put_contents($_destination, file_get_contents($_source));//Minify::minifyCSS(file_get_contents($_source)));
							break;
						}
					
						//optimize HTML
						case "html" :
						{
							file_put_contents($_destination, Minify::minifyHTML(file_get_contents($_source)));
							break;
						}
					
						default:
						{
							copy($_source, $_destination);
							break;
						}
					}
					@chmod($_destination, $this->_permFile);
				}
			}
		}
		
		// set proper permissions on newly created directories
		if (!empty($paths_to_chmod)) FileUtils::setDirectoryPermissions(array_keys($paths_to_chmod), $this->_permDir);

		$this->updateImages($images);
		
		$allCss = '';
		$importArray = array();

		$this->appendCssFile($destination . 'styles' . DS . 'base.css', $allCss, $importArray);
		$this->appendCssFile($destination . 'styles' . DS . 'skin.css', $allCss, $importArray);
		$this->appendCssFile($destination . 'styles' . DS . $this->_settings['DesignSkinTheme'] . '.css', $allCss, $importArray);
		$this->appendCssFile($destination . 'styles' . DS . 'designmode.css', $allCss, $importArray);
		$this->appendCssFile($destination . 'styles' . DS . 'custom.css', $allCss, $importArray);

		file_put_contents(
			$destination . 'styles' . DS . 'all.css',
			(count($importArray) > 0 ? implode("\n", $importArray) . "\n" : '') . trim(Minify::minifyCSS($allCss))
		);

		@chmod($destination . 'styles' . DS . 'all.css', $this->_permFile);
	}

	/**
	 * @param $fileName
	 * @param $allCss
	 * @param $importArray
	 */
	protected function appendCssFile($fileName, &$allCss, &$importArray)
	{
		if (is_file($fileName))
		{
			$fs = filesize($fileName);

			if ($fs == 0) return;

			$f = fopen($fileName, 'r');

			while ($s = fgets($f, $fs))
			{
				if (strpos($s, '@import') === false)
				{
					$allCss = $allCss . "\n" . trim($s);
				}
				else
				{
					$importArray[] = trim($s);
				}
			}

			fclose($f);

			$allCss .= "\n";
		}
	}

	/**
	 * @param $text
	 * @param $importArray
	 */
	protected function extractImportCss(&$text, &$importArray)
	{

	}
	
	/**
	 * Returns an array of default assembly filepaths
	 *
	 * @return array An array of default directories that need to be compiled
	 * @author Sebastian Nievas
	 */
	private function _getAssembleDefaultSources($custom)
	{
		$a = array(
			'content'.DS.'engine'.DS.'design'.DS
		);
		
		$a[] = 'content'.DS.'skins'.DS.escapeFileName($this->_settings['DesignSkinName']).DS;
		
		if ($custom) $a[] = 'content'.DS.'skins'.DS.'_custom'.DS.'skin'.DS;
		return $a;
	}

	/**
	 * @param $source
	 * @param $destination
	 */
	private function _assembleCopyDir($source, $destination)
	{
		if (strlen($source) > 0)
		{
			if (!in_array($source[strlen($source) - 1], array("/", "\\")))
			{
				$source = $source.DS;
			}
		}
		
		if (strlen($destination) > 0)
		{
			if (!in_array($destination[strlen($destination) - 1], array("/", "\\")))
			{
				$destination = $destination.DS;
			}
		}
		
		$d = dir($source);
		if ($d)
		{
			while (false !== ($entry = $d->read()))
			{
				if (!in_array($entry, array(".", "..", ".svn")))
				{	
					$_source = $source.$entry;
					$_destination = $destination.$entry;
					
					$this->_cache_map[$_destination] = $_source;
					
					if (is_dir($_source))
					{
						$this->_assembleCopyDir($_source, $_destination);
					}
				}
			}
			$d->close();
		}
	}
		
	/**
	 * Override assign method by bridging specific vars
	 *
	 * @param string $name 
	 * @param mixed $value 
	 * @return void
	 */
	public function assign($name, $value = null)
	{
		$this->_view->assign($name, $value);
	}
	
	/**
	 * Loads design elements into view
	 * @return void
	 */
	public function getImages($returnHidden = false, $returnUnset = false)
	{
		$this->_images = array();
		db()->query('SELECT * FROM '.DB_PREFIX.'design_elements WHERE type="image"'.($returnHidden ? ' OR type="image-hidden"' : '').' GROUP BY element_id');
		while($image = db()->moveNext())
		{
			if ($image["content"] != ""/**
	 * Copy source to destination
	 * @param string $source
	 * @param string $destination
	 * @param mixed $extend_options
	 */ || $returnUnset) $this->_images[$image["element_id"]] = $image;
		} 
		return $this->_images;
	}
	
	/**
	 * UPdate design elements
	 * @param $images
	 * @return unknown_type
	 */
	public function updateImages($images)
	{
		$this->_images = $images;
		foreach	($images as $imageId => $image)
		{
			db()->reset();
			db()->assignStr("content", $image["content"]);
			db()->assignStr("type", $image["type"]);
			db()->update(DB_PREFIX."design_elements", "WHERE element_id='".db()->escape($imageId)."'");
		}
	}
}

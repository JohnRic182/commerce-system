<?php

if (!defined("DS")) define ("DS", DIRECTORY_SEPARATOR);

/**
 * Handles the front end Cart Designer for the new templating system
 *
 **/

class Ddm_DesignMode
{
	private $_data;
	private $_layouts;
	private $_settings;

	private $_permFile = 0666;
	private $_permDir = 0777;
	
	/**
	 * 
	 * @param unknown_type $dir
	 * @param unknown_type $perm
	 * @param unknown_type $recursive
	 */
	private function _mkdir($dir, $perm, $recursive = true)
	{
		$_dir = str_replace("\\", "/", $dir);	
		$parts = explode("/", $_dir);
		$s = "";
		foreach($parts as $part)
		{
			$s = $s.$part.DS;	
			if (!is_dir($s)) mkdir($s, $perm, $recursive);
			@chmod($s, $perm);
		}
	}
	
	/**
	 * Constructor
	 * @param $settings
	 * @return unknown_type
	 */
	public function __construct(&$settings)
	{
		$this->_settings = $settings;
		$this->_layouts = unserialize($this->_settings['LayoutElements']);

		$this->_permFile = FileUtils::getFilePermissionMode();
		$this->_permDir = FileUtils::getDirectoryPermissionMode();

		return $this;
	}
	
	/**
	 * Returns original empty skin path
	 * @return string
	 */
	public function getBaseSkinPath()
	{
		return "content".DS."engine".DS."design".DS;
	}
	
	/**
	 * Returns original (cool-looking) skin path
	 * @return string
	 */
	public function getRichSkinPath()
	{
		return "content".DS."skins".DS.escapeFileName($this->_settings["DesignSkinName"]).DS; 
	}
	
	
	/**
	 * Returns customizations folder
	 * @return string
	 */
	public function getSkinCustomizationsPath()
	{
		return "content".DS."skins".DS."_custom".DS;
	}
	
	/**
	 * Returns user-customized skin
	 * @return string
	 */
	public function getCustomSkinPath()
	{
		return $this->getSkinCustomizationsPath()."skin".DS;
	}
	
	/**
	 * Returns user-customized css
	 * @return string
	 */
	public function getCustomCssPath()
	{
		return "content".DS."skins".DS."_custom".DS."css".DS;
	}
	
	/**
	 * Returns assembled skin folder
	 * @return string
	 */
	public function getCacheSkinPath()
	{
		return "content".DS."cache".DS."skins".DS.escapeFileName($this->_settings["DesignSkinName"]).DS;
	}
	
	/**
	 * Returns custom.css file path
	 *
	 * @return string
	 */
	public function getCustomSkinStylesPath()
	{
		static $_path = null;
		if (is_null($_path)) $_path = $this->getCustomSkinPath().'styles'.DS;
		return $_path;
	}
	
	/**
	 * Returns image ierarchy info (custom / skin / base)
	 * @param $class
	 * @return array
	 */
	public function getImageInfo($class)
	{
		$skins = array(
			"base" => $this->getBaseSkinPath()."images".DS,
			"skin" => $this->getRichSkinPath()."images".DS,
			"custom" => $this->getCustomSkinPath()."images".DS,
		);
		$images = array();
		$extensions = array("jpg", "jpeg", "png", "gif");
		foreach ($skins as $skinType => $skinPath)
		{
			$images[$skinType] = false;
			foreach ($extensions as $extension)
			{
				$imageFile = $skinPath.$class.".".$extension;
				if (is_file($imageFile))
				{
					$images[$skinType] = $imageFile;
					break;
				}
			}
		}
		return $images;
	}
	
	
	/**
	 * Returns element file name
	 * @param $designElementClass
	 * @return string
	 */ 
	public function getElementFile($designElementClass)
	{
		return "templates/".str_replace('_', '/', substr($designElementClass, 8)).'.html';
	}
	
	/**
	 * @param String The skin name
	 * @param Boolean Whether to return a JSON encoded string (default is FALSE)
	 * @return mixed Array or JSON encoded string 
	 */
	public function getSkinThemes($json = false)
	{
		$templateXml = simplexml_load_string(
			file_get_contents('content'.DS.'skins'.DS.escapeFileName($this->_settings['DesignSkinName']).DS.'skin.xml'), 'SimpleXMLElement', LIBXML_NOCDATA
		);
		
		$themes = array();
		
		if (isset($templateXml->themes) && isset($templateXml->themes->theme))
		{
			foreach ($templateXml->themes->theme as $theme)
			{
				$themes[(string)$theme->id] = (string)$theme->name;
			}
		}
		else
		{
			$themes["theme-default"] = "Default";
		}
		return ($json) ? json_encode($themes) : $themes;
		
	}
	
	/**
	 * Clears the current skin cache and reassembles
	 *
	 * @return json
	 */
	public function clearSkinCache()
	{
		// remove trailing slash if any from skin cache path
		$cacheSkinPath = rtrim($this->getCacheSkinPath(), DS);

		// first we compile the cache into a new folder
		$temp_cache = $cacheSkinPath.'__tmp'.DS;
		view()->assemble($temp_cache);

		// update the design_elements table to not contain the path to the $temp_cache folder
		db()->query('UPDATE '.DB_PREFIX.'design_elements SET content = REPLACE(content, "'.db()->escape($temp_cache).'", "'.db()->escape($cacheSkinPath.DS).'")');

		// fresh cache succeeded, remove skin cache folder
		remove_dir($cacheSkinPath);

		// rename the fresh cache folder to the cache skin path
		rename($temp_cache, $cacheSkinPath);

		return json_encode(array('status' => 1));
	}
	
	/**
	 * Restore site layout
	 * @return json
	 */
	public function restoreLayout()
	{
		$templateXml = simplexml_load_string(
			file_get_contents('content'.DS.'skins'.DS.escapeFileName($this->_settings['DesignSkinName']).DS.'skin.xml'), 'SimpleXMLElement', LIBXML_NOCDATA
		);
		$layoutElements = $this->_settings['LayoutElementsDefault'];
		if (isset($templateXml->defaultLayoutElements) && (string) $templateXml->defaultLayoutElements !== '')
		{
			// suppress any possible errors when unserializing, and check if this is a serialized array
			if (($default_layout = @unserialize($templateXml->defaultLayoutElements)) !== false && is_array($default_layout))
			{
				$layoutElements = $templateXml->defaultLayoutElements;
			}
		}

		if (isset($templateXml->settings))
		{
			foreach ($templateXml->settings->children() as $setting)
			{
				db()->reset();
				db()->assignStr('value', (String) $setting);
				db()->update(DB_PREFIX.'settings', "WHERE group_name='Design Settings' AND name = '".db()->escape($setting->getName())."' LIMIT 1");
			}
		}

		db()->reset();
		db()->assignStr("value", $layoutElements);
		db()->update(DB_PREFIX."settings", "WHERE name='LayoutElements'");

		$layoutSiteWidth = $templateXml->xpath("/skin/settings/option[@name='LayoutSiteWidth']");
		$layoutSiteWidthValue = (count($layoutSiteWidth) && $layoutSiteWidth !== false)
			? $layoutSiteWidth[0]->attributes()->value
			: $this->_settings['LayoutSiteWidthDefault'];

		db()->reset();
		db()->assignStr("value", $layoutSiteWidthValue);
		db()->update(DB_PREFIX."settings", "WHERE name='LayoutSiteWidth'");

		return json_encode(array("status"=>1));
	}
	
	/**
	 * Restore site skin
	 * @return json
	 */
	public function restoreSkin()
	{
		$s = $this->getCustomSkinPath();
		$panels = array();
		db()->query("SELECT CONCAT('panel-custom', SUBSTR(name, 28)) AS panel_id, title FROM ".DB_PREFIX."layout_elements WHERE name LIKE 'element_panels_panel-custom%' ORDER BY title");
		while(($panel = db()->movenext()) != false)
		{
			$p = $s."templates".DS."panels".DS.$panel["panel_id"].".html";
			if (is_file($p)) $panels[] = array($p, file_get_contents($p));
		}

		$custom_css_path = $this->getCustomSkinStylesPath();
		$css_elements_path = $this->getSkinCustomizationsPath()."css";

		// we must all cached skin folders
		remove_dir('content'.DS.'cache'.DS.'skins');
		if (!is_dir('content'.DS.'cache'.DS.'skins')) $this->_mkdir('content'.DS.'cache'.DS.'skins', $this->_permDir, true);

		remove_dir($css_elements_path);
		// remove_dir($s);

		//if (!is_dir($s)) $this->_mkdir($s, $this->_permDir, true);
		if (!is_dir($css_elements_path)) $this->_mkdir($css_elements_path, $this->_permDir, true);
		if (!is_dir($custom_css_path)) $this->_mkdir($custom_css_path, $this->_permDir, true);

		$skin_folders = array(
			'images',
			'javascript',
			'styles',
			'templates'.DS.'emails'.DS.'elements',
			'templates'.DS.'layouts',
			'templates'.DS.'layouts'.DS.'zones',
			'templates'.DS.'layouts'.DS.'zones'.DS.'includes',
			'templates'.DS.'panels',
			'templates'.DS.'panels'.DS.'includes',
			'templates'.DS.'wrappers',
			'templates'.DS.'wrappers'.DS.'elements',
			'templates'.DS.'pages'.DS.'account',
			'templates'.DS.'pages'.DS.'account'.DS.'elements',
			'templates'.DS.'pages'.DS.'catalog',
			'templates'.DS.'pages'.DS.'catalog'.DS.'elements',
			'templates'.DS.'pages'.DS.'catalog'.DS.'views',
			'templates'.DS.'pages'.DS.'checkout',
			'templates'.DS.'pages'.DS.'checkout'.DS.'opc',
			'templates'.DS.'pages'.DS.'checkout'.DS.'traditional-checkout',
			'templates'.DS.'pages'.DS.'gift-certificate',
			'templates'.DS.'pages'.DS.'google',
			'templates'.DS.'pages'.DS.'newsletters',
			'templates'.DS.'pages'.DS.'product',
			'templates'.DS.'pages'.DS.'product'.DS.'elements',
			'templates'.DS.'pages'.DS.'product'.DS.'views',
			'templates'.DS.'pages'.DS.'site',
			'templates'.DS.'pages'.DS.'site'.DS.'elements',
			'templates'.DS.'pages'.DS.'wishlist'
		);

		foreach ($skin_folders as $folder)
		{
			if ($folder != 'templates'.DS.'emails'.DS.'elements')
			{
				remove_dir($this->getCustomSkinPath().$folder);
				if (!is_dir($this->getCustomSkinPath().$folder)) $this->_mkdir($this->getCustomSkinPath().$folder, $this->_permDir, true);
			}
		}

		if (!is_file($custom_css_path.'custom.css'))
		{
			file_put_contents($custom_css_path.'custom.css', '');
			@chmod($custom_css_path.'custom.css', $this->_permFile);
		}

		foreach ($panels as $panel)
		{
			$pi = pathinfo($panel[0]);
			if (!is_dir($pi["dirname"])) $this->_mkdir($pi["dirname"], $this->_permDir, true);
			if (!is_file($panel[0]))
			{
				file_put_contents($panel[0], $panel[1]);
				@chmod($panel[0], $this->_permFile);
			}
		}

		db()->query("UPDATE ".DB_PREFIX."design_elements SET type='image' WHERE type='image-hidden'");

		return json_encode(array("status"=>1));
	}
	
	/**
	 * Save layout
	 * @param $options
	 * @return json
	 */
	public function saveLayout($scope, $page, $pages, $layout, $width, $theme)
	{
		// decode the JSON array that is passed in options[data]
		$_layout = json_decode(stripslashes(html_entity_decode($layout)), true);

		$_layoutFix = array("zones" => array());
		foreach($_layout["zones"] as $zoneId => $zoneData)
		{
			$zoneData = (array)$zoneData;
			if (is_array($zoneData) && count($zoneData) > 0)
			{
				$_zoneData = array();
				foreach($zoneData as $elementKey => $designElementClass)
				{
					$filePath = $this->getElementFile($designElementClass);
					$element = array(
						"class" => $designElementClass,
						"file" => $filePath
					);
					$_zoneData[$elementKey] = $element;
				}
				$_layoutFix["zones"][$zoneId] = $_zoneData;
			}
		}

		$_layout = $_layoutFix;

		if ($scope == "all")
		{
			$this->_layouts["pages"] = array(
				"_global" => array(
					"zones" => $_layout["zones"]
				)
			);
		}
		elseif ($scope == "current")
		{
			$this->_layouts["pages"][$page] = array(
				"zones" => $_layout["zones"]
			);
		}
		elseif ($scope == "selected")
		{
			$pagesAreas = array(
				"site" => array("home", "page", "site_map", "products_reviews", "international"),
				"catalog" => array("catalog", "product", "auth_error", ""),
				"account" => array(
					"account", "login", "signup",  "password_reset", "blocked", "wishlist",
					"manage_wishlist", "profile", "orders", "order", "address_book", "address_edit"
				),
				"checkout" => array(
					"cart", "completed", "shipping_quote", "billing_and_shipping_address",
					"shipping_method", "invoice", "payment_validation", "completed", "one_page_checkout"
				),
				"other" => array("subscribe", "unsubscribe", "email2friend", "gift_certificate")
			);
			$pages = explode(",", $pages);
			foreach($pages as $pageArea)
			{
				foreach ($pagesAreas[$pageArea] as $pageId)
				{
					$this->_layouts["pages"][$pageId] = array(
						"zones" => $_layout["zones"]
					);
				}
			}
		}

		// serialize the data and update database
		if (in_array($scope, array("all", "current", "selected")))
		{
			db()->reset();
			db()->assignStr('value', serialize($this->_layouts));
			db()->update(DB_PREFIX.'settings', 'WHERE name = "LayoutElements" LIMIT 1');
		}

		// set site width
		db()->reset();
		db()->assignStr('value', $width);
		db()->update(DB_PREFIX.'settings', 'WHERE name = "LayoutSiteWidth" LIMIT 1');

		if (!is_null($theme))
		{
			// set site theme
			db()->reset();
			db()->assignStr('value', $theme);
			db()->update(DB_PREFIX.'settings', 'WHERE name = "DesignSkinTheme" LIMIT 1');
		}
		return json_encode(array("status"=>1));
	}
	
	
	/**
	 * Save layout
	 * @param $options
	 * @return json
	 */
	public function saveCleanCheckout()
	{
		$pages = array(
			"cart", "completed", "shipping_quote", "billing_and_shipping_address",
			"shipping_method", "invoice", "payment_validation", "completed", "one_page_checkout"
		);

		foreach ($pages as $pageId)
		{
			$this->_layouts["pages"][$pageId] = array(
				"zones" => array(
						'left' => array(),
						'right' => array(),
						'center' => array()
					)
			);
		}

		db()->reset();
		db()->assignStr('value', serialize($this->_layouts));
		db()->update(DB_PREFIX.'settings', 'WHERE name = "LayoutElements" LIMIT 1');

		return json_encode(array("status"=>1));
	}
	
	/**
	 * Save bulki colors changes
	 * @param unknown_type $data
	 */
	public function saveColors($data)
	{
		try
		{
			$customCssPath = $this->getCustomCssPath();
			if (!is_dir($customCssPath)) $this->_mkdir($customCssPath, $this->_permDir, true);
			foreach ($data["colors"] as $groupData)
			{
				foreach ($groupData["elements"] as $elementData)
				{
					if ($elementData["css"])
					{
						$elementCssFile = $customCssPath.trim(escapeFileName($elementData["selector"])).".css";
						file_put_contents($elementCssFile, $elementData["css"]);
						@chmod($elementCssFile, $this->_permFile);
					}
				}
			}

			$response = array("status"=>1);
			$response["cssFileUrl"] = $this->renderElementsCSSFile();
			return json_encode($response);
		}
		catch (Exception $e)
		{
			$response = array("status"=>0, "message"=> $e->getMessage());
			$continue = false;
		}
	}
	
	/**
	 * Save bulki fonts changes
	 * @param array $data
	 */
	public function saveFonts($data)
	{
		try
		{
			$customCssPath = $this->getCustomCssPath();
			if (!is_dir($customCssPath)) $this->_mkdir($customCssPath, $this->_permDir, true);
			foreach ($data["fonts"] as $groupData)
			{
				foreach ($groupData["elements"] as $elementData)
				{
					if ($elementData["css"])
					{
						$elementCssFile = $customCssPath.trim(escapeFileName($elementData["selector"])).".css";
						file_put_contents($elementCssFile, $elementData["css"]);
						@chmod($elementCssFile, $this->_permFile);
					}
				}
			}

			$response = array("status"=>1);
			$response["cssFileUrl"] = $this->renderElementsCSSFile();
			return json_encode($response);
		}
		catch (Exception $e)
		{
			$response = array("status"=>0, "message"=> $e->getMessage());
			$continue = false;
		}
	}

	/**
	 * Writes HTML for specified element. All changes here will be saved in content/skins/_custom/skin folder
	 * @param $designElementClass
	 * @param $html
	 * @return json
	 */
	public function saveElementHtml($designElementClass, $html)
	{
		$response = array("status"=>1);
		$templateFile = $this->getElementFile($designElementClass);

		$customFileName = $this->getCustomSkinPath().$templateFile;
		$customFilePath = dirname($customFileName);

		$cacheFileName = $this->getCacheSkinPath().$templateFile;
		$cacheFilePath = dirname($cacheFileName);

		$continue = true;
		//create dirs first
		try
		{
			//create dirs first
			if (!is_dir($customFilePath)) $this->_mkdir($customFilePath, $this->_permDir, true);
			if (!is_dir($cacheFilePath)) $this->_mkdir($cacheFilePath, $this->_permDir, true);
		}
		catch (Exception $e)
		{
			$response = array("status"=>0, "message"=> $e->getMessage());
			$continue = false;
		}

		//try to validate
		if ($continue)
		{
			try
			{
				$probeFileName = $cacheFileName."-test";
				if (is_file($probeFileName)) unlink($probeFileName);

				// replaces
				$patterns = array(
					'/{(.*)&lt;(.*)}/',
					'/{(.*)&gt;(.*)}/',
					'/{(.*)&amp;gt;(.*)}/',
					'/{(.*)&amp;&amp;(.*)}/'
				);

				$replacements = array(
					'{$1<$2}',
					'{$1>$2}',
					'{$1>$2}',
					'{$1&&$2}'
				);

				// print $html;

				do {
					$orig_html = $html;
					$html = preg_replace($patterns, $replacements, $html);
				} while ($orig_html != $html);

				file_put_contents($probeFileName, $html);

				function myTmpErrHandler($errno, $errstr, $errfile, $errline)
				{
					global $probeFileName, $templateFile;

					if ($errno == E_USER_ERROR)
					{
						@unlink($probeFileName);
						if (preg_match('/^(Smarty error\: ){1}(\[in (.+) line ((\d){1,})\]\: )(.+)(\((.+)\)){1}$/', $errstr, $m))
						{
							$errstr = $m[6]." in line ".$m[4];
						}
						$response = array(
							"status"=>0,
							"message"=> "Your changes cannot be saved. Smarty returned error:\n".$errstr
						);
						echo json_encode($response);
						session_write_close();
						die();
					}
				}

				set_error_handler("myTmpErrHandler");
				view()->error_reporting =  E_ALL & ~E_NOTICE ;
				require_once dirname(__FILE__) . '/../../engine/engine_user.php';
				require_once dirname(__FILE__) . '/../../engine/engine_order.php';
				$user = USER::getFromSession(db(), settings());
				$order = ORDER::getFromSession(db(), settings(), $user, false, false);
				view()->assign('order', $order);
				view()->fetch($templateFile."-test");
				unlink($probeFileName);
				restore_error_handler();

			}
			catch (Exception $e)
			{
				$response = array("status"=>0, "message"=> $e->getMessage());
				$continue = false;
			}
		}

		//save files
		if ($continue)
		{
			try
			{
				if (is_file($customFileName)) unlink($customFileName);
				file_put_contents($customFileName, $html);
				@chmod($customFileName, $this->_permFile);

				if (is_file($cacheFileName)) unlink($cacheFileName);
				file_put_contents($cacheFileName, $html);
			}
			catch (Exception $e)
			{
				$response = array("status"=>0, "message"=> $e->getMessage());
				$continue = false;
			}
		}

		return json_encode($response);
	}
	
	/**
	 * Restore original file
	 * @param $designElementClass
	 * @return json
	 */
	public function restoreElementHtml($designElementClass)
	{
		$response = array("status"=>1);

		$templateFile = $this->getElementFile($designElementClass);

		$baseFileName = $this->getBaseSkinPath().$templateFile;
		$skinFileName = $this->getRichSkinPath().$templateFile;

		//remove this file
		$customFileName = $this->getCustomSkinPath().$templateFile;

		//and overwrite this file with original one
		$cacheFileName = $this->getCacheSkinPath().$templateFile;
		$cacheFilePath = dirname($cacheFileName);

		try
		{
			if (is_file($customFileName)) unlink($customFileName);
			if (is_file($cacheFileName)) unlink($cacheFileName);
			if (!is_dir($cacheFilePath)) $this->_mkdir($cacheFilePath, $this->_permDir, true);
			if (is_file($skinFileName))
			{
				copy($skinFileName, $cacheFileName);
			}
			elseif (is_file($baseFileName))
			{
				copy($baseFileName, $cacheFileName);
			}
			else throw new Exception("Original file can not be found");
		}
		catch (Exception $e)
		{
			$response = array("status"=>0, "message"=>$e->getMessage());
		}

		return json_encode($response);
	}
	
	/**
	 * Creates a new layout element that can be used in the front end
	 *
	 * @param string $title 
	 * @param string $html 
	 * @return JSON
	 */
	public function createCustomElement($elementType, $elementId, $elementTitle, $elementHtml)
	{
		$response = array("status"=>1, "message"=>"");
		// Sanitize the name to not contain any spaces
		$elementId = preg_replace('/\W/', '-', $elementId);
		$designElementClassName = "element_panels_panel-custom-".$elementId;

		try
		{
			//First we check to see if the ID is already taken
			db()->query($q = "SELECT * FROM ".DB_PREFIX."layout_elements WHERE name= '".db()->escape($designElementClassName)."'");
			if (db()->movenext())
			{
				throw new Exception("A layout element with ID {$elementId} already exists");
			}

			//Save file first. If saved successfull - add to db
			$templateFile = "templates/panels/panel-custom-".$elementId.".html";

			$customFileName = $this->getCustomSkinPath().$templateFile;
			$customFilePath = dirname($customFileName);

			if (is_file($customFileName))
			{
				throw new Exception("File {$customFileName} already exists");
			}

			$cacheFileName = $this->getCacheSkinPath().$templateFile;
			$cacheFilePath = dirname($cacheFileName);

			//create dirs first
			if (!is_dir($customFilePath)) $this->_mkdir($customFilePath, $this->_permDir, true);
			if (!is_dir($cacheFilePath)) $this->_mkdir($cacheFilePath, $this->_permDir, true);

			//Format html and write file
			$html =
				"<div class=\"panel-{$elementId} panel clearfix {class file=\$smarty.template editable=true draggable=true}\">\n".
				"\t<h4 class=\"title {class file=\$smarty.template editable=true}\">{$elementTitle}</h4>\n".
				"\t<div class=\"content {class file=\$smarty.template editable=true}\">\n\n".
				"{**YOUR CODE STARTS HERE**}\n\n".
				"{$elementHtml}\n\n".
				"{**YOUR CODE ENDS HERE**}\n\n".
				"\t</div>\n".
				"</div>\n";

			$probeFileName = $cacheFileName."-test";
			if (is_file($probeFileName)) unlink($probeFileName);

			file_put_contents($probeFileName, $html);

			function myTmpErrHandler($errno, $errstr, $errfile, $errline)
			{
				global $probeFileName;
				if ($errno == E_USER_ERROR)
				{
					@unlink($probeFileName);
					if (preg_match('/^(Smarty error\: ){1}(\[in (.+) line ((\d){1,})\]\: )(.+)(\((.+)\)){1}$/', $errstr, $m))
					{
						$errstr = $m[6]." in line ".$m[4];
					}
					$response = array(
						"status"=>0,
						"message"=> "Your changes cannot be saved. Smarty returned error:\n".$errstr
					);
					echo json_encode($response);
					session_write_close();
					die();
				}
			}

			set_error_handler("myTmpErrHandler");
			view()->error_reporting =  E_ALL & ~E_NOTICE ;
			view()->fetch($templateFile."-test");
			unlink($probeFileName);
			restore_error_handler();

			file_put_contents($customFileName, $html);
			@chmod($customFileName, $this->_permFile);

			if (is_file($cacheFileName)) unlink($cacheFileName);
			file_put_contents($cacheFileName, $html);

			//add to database
			db()->reset();
			db()->assignStr('type', $elementType);
			db()->assignStr('name', $designElementClassName);
			db()->assignStr('title', $elementTitle);
			db()->assign('is_active', 1);
			$id = db()->insert(DB_PREFIX.'layout_elements');
			$response = array("status" => 1, "designElementClassName" => $designElementClassName);
		}
		catch (Exception $e)
		{
			$response = array("status" => 0, "message" => $e->getMessage());
		}

		return json_encode($response);
	}
	
	/**
	 * Delete custom element
	 * @param $designElementClass
	 * @return json
	 */
	public function deleteCustomElement($designElementClass)
	{
		$response = array("status" => 1);

		try
		{
			//remove from database
			db()->query("DELETE FROM ".DB_PREFIX."layout_elements WHERE name='".db()->escape($designElementClass)."'");

			//rebuild layout
			$layouts = $this->_layouts;

			$layouts["pages"] = array();
			foreach ($this->_layouts["pages"] as $pageId => $pageData)
			{
				if (isset ($pageData["zones"]))
				{
					foreach ($pageData["zones"] as $zoneId => $zoneElements)
					{
						foreach ($zoneElements as $zoneElement)
						{
							if ($zoneElement["class"] != $designElementClass)
							{
								$layouts["pages"][$pageId]["zones"][$zoneId][] = $zoneElement;
							}
						}
					}
				}
			}

			$this->_layouts = $layouts;

			unset($layouts);

			$this->_settings['LayoutElements'] = serialize($this->_layouts);

			db()->reset();
			db()->assignStr("value", $this->_settings['LayoutElements']);
			db()->update(DB_PREFIX."settings", "WHERE name='LayoutElements'");

			$templateFile = $this->getElementFile($designElementClass);

			$fileCustom = $this->getCustomSkinPath().$templateFile;
			$fileCache = $this->getCacheSkinPath().$templateFile;

			if (is_file($fileCustom)) @unlink($fileCustom);
			if (is_file($fileCache)) @unlink($fileCache);
		}
		catch (Exception $e)
		{
			$response = array("status" => 0, "message"=>"Panel was removed from database, but some files are not removed");
		}

		return json_encode($response);
	}
	
	/**
	 * Returns contents of the custom.css file
	 * @return json
	 */
	public function getCustomCssFile()
	{
		try
		{
			$css_file = $this->getCustomSkinStylesPath().'custom.css';
			$response = array(
				'status' => 1,
				'data' => (file_exists($css_file) ? file_get_contents($this->getCustomSkinStylesPath().'custom.css') : '')
			);
		}
		catch (Exception $e)
		{
			$response = array('status' => 0, 'message' => $e->getMessage());
		}
		
		return json_encode($response);
	}
	
	/**
	 * Saves the contents of $css_data into the custom.css file
	 * @var string The CSS data to save
	 * @return void
	 */
	public function saveCustomCssFile($css_data)
	{
		$response = array('status' => 1);
		try
		{
			if (!is_dir($this->getCustomSkinStylesPath())) $this->_mkdir($this->getCustomSkinStylesPath(), $this->_permDir, true);
			file_put_contents($this->getCustomSkinStylesPath().'custom.css', $css_data);
			@chmod($this->getCustomSkinStylesPath().'custom.css', $this->_permFile);
		}
		catch (Exception $e)
		{
			$response = array('status' => 0, 'message' => $e->getMessage());
		}

		return json_encode($response);
	}
	
	
	
	/**
	 * Returns css map for elements
	 * @return json
	 */
	public function getElementsCssMap($attachFiles = false)
	{
		//TODO: replace with db settings
		include("content/engine/design/styles/elements/elements-map.php");
		
		$cssMap = isset($cssMap) ? $cssMap : array();
		
		if ($attachFiles)
		{
			$customCssPath = $this->getCustomCssPath();
			foreach ($cssMap["colors"] as $groupKey=>$groupData)
			{
				foreach ($groupData["elements"] as $elementKey=>$elementData)
				{
					$elementCssFile = $customCssPath.trim(escapeFileName($elementData["selector"])).".css";
					$elementCssFileOld = $customCssPath.escapeFileName($elementData["selector"]).".css";
					$cssMap["colors"][$groupKey]["elements"][$elementKey]["css"] = 
						is_file($elementCssFile) ? 
						file_get_contents($elementCssFile) : 
						(is_file($elementCssFileOld) ? file_get_contents($elementCssFileOld) : false);
				}
			}
			foreach ($cssMap["fonts"] as $groupKey=>$groupData)
			{
				foreach ($groupData["elements"] as $elementKey=>$elementData)
				{
					$elementCssFile = $customCssPath.trim(escapeFileName($elementData["selector"])).".css";
					$elementCssFileOld = $customCssPath.escapeFileName($elementData["selector"]).".css";
					$cssMap["fonts"][$groupKey]["elements"][$elementKey]["css"] = 
						is_file($elementCssFile) ? 
						file_get_contents($elementCssFile) : 
						(is_file($elementCssFileOld) ? file_get_contents($elementCssFileOld) : false);
				}
			}	
		}
		
		db()->query("SELECT CONCAT('panel', SUBSTR(name, 28)) AS panel_id, title FROM ".DB_PREFIX."layout_elements WHERE name LIKE 'element_panels_panel-custom%' ORDER BY title");
		while(($panel = db()->movenext()) != false)
		{
			$cssMap["groups"]["panels"]["elements"][] = 
				array("parent-class" => $panel["panel_id"], "parent-selector" => ".".$panel["panel_id"], "title" => $panel["title"], "type" => "panel");
		}
		
		$response = array("status" => 1, "map" => $cssMap);
		return json_encode($response);
	}
	
	/**
	 * Returns element custom css file
	 * @param $parent string (class name)
	 * @param $selector string (element selector)
	 * @return json
	 */
	public function getElementStyles($parentClass, $parentSelector, $class, $selector)
	{
		$response = array("status"=>1, "css"=>false, "image"=>false);
		$customCssPath = $this->getCustomCssPath();
		$elementCssFile = $customCssPath.trim(escapeFileName($parentSelector." ".$selector)).".css";
		$elementCssFileOld = $customCssPath.escapeFileName($parentSelector." ".$selector).".css";
		$response["file"] = $elementCssFile;
		if (is_file($elementCssFile))
		{
			$response["css"] = file_get_contents($elementCssFile);
		}
		elseif (is_file($elementCssFileOld))
		{
			$response["css"] = file_get_contents($elementCssFileOld);
		}
		$images = view()->getImages(true);
		if (isset($images[$class]))
		{
			$response["image"] = $images[$class];
			$response["imageInfo"] = $this->getImageInfo($class);
		}
		return json_encode($response);
	}
	
	/**
	 * Saves css file
	 * @param $parent
	 * @param $selector
	 * @param $css
	 * @return json
	 */
	public function saveElementStyles($parentClass, $parentSelector, $selector, $css)
	{
		$response = array("status"=>1);

		$customCssPath = $this->getCustomCssPath();
		$elementCssFile = $customCssPath.trim(escapeFileName($parentSelector." ".$selector)).".css";
		$response["file"] = $elementCssFile;
		if (!is_dir($customCssPath)) $this->_mkdir($customCssPath, $this->_permDir, true);
		file_put_contents($elementCssFile, $css);
		@chmod($elementCssFile, $this->_permFile);
		$response["cssFileUrl"] = $this->renderElementsCSSFile();

		return json_encode($response);
	}
	
	/**
	 * Save uploaded file in custom folder
	 * @param $parentClass
	 * @param $parentSelector
	 * @param $class
	 * @param $selector
	 * @param $fileData
	 * @return json
	 */
	public function uploadImage($parentClass, $parentSelector, $class, $selector, $fileData)
	{
		$response = array("status"=>0);

		//check is file uploaded
		if (is_file($fileData["tmp_name"]) && filesize($fileData["tmp_name"]) > 0)
		{
			//check is it image
			$imageSize = getimagesize($fileData["tmp_name"]);
			//format new file name
			
			$ext = array(IMAGETYPE_JPEG=>"jpg", IMAGETYPE_PNG=>"png", IMAGETYPE_GIF=>"gif");
			if (in_array($imageSize[2], array_keys($ext)))
			{
				$fileName = $class.".".$ext[$imageSize[2]];
				$fileDir = $this->getCustomSkinPath()."images".DS;
				if (!is_dir($fileDir))
				{
					//create dir
					$this->_mkdir($fileDir, $this->_permDir, true);	
				}
				else
				{
					//or remove old images
					if (is_file($fileDir.$class.".jpg")) @unlink($fileDir.$class.".jpg");
					if (is_file($fileDir.$class.".png")) @unlink($fileDir.$class.".png");
					if (is_file($fileDir.$class.".gif")) @unlink($fileDir.$class.".gif");
				}
				
				$filePath = $fileDir.$fileName; 
				if (is_file($filePath)) unlink($filePath);
				
				if (move_uploaded_file($fileData["tmp_name"], $filePath))
				{
					chmod($filePath, $this->_permFile);
					
					$cacheFileName = $fileName;
					$cacheFileDir = $this->getCacheSkinPath()."images".DS;
					$cacheFilePath = $cacheFileDir.$cacheFileName; 
					if (!is_dir($cacheFileDir))
					{
						//create dir
						$this->_mkdir($cacheFileDir, $this->_permDir, true);	
					}
					else
					{
						//or remove current(old) images
						if (is_file($cacheFileDir.$class.".jpg")) @unlink($cacheFileDir.$class.".jpg");
						if (is_file($cacheFileDir.$class.".png")) @unlink($cacheFileDir.$class.".png");
						if (is_file($cacheFileDir.$class.".gif")) @unlink($cacheFileDir.$class.".gif");
					}
					copy($filePath, $cacheFilePath);
					chmod($cacheFilePath, $this->_permFile);
					
					$element = db()->selectOne("SELECT * FROM ".DB_PREFIX."design_elements WHERE element_id='".db()->escape($class)."'");
					
					$imgUrl = "content/cache/skins/".$this->_settings["DesignSkinName"]."/images/".$fileName;
					
					db()->reset();
					db()->assignStr("group_id", "image");
					db()->assignStr("removable", "yes");
					db()->assignStr("element_id", $class);
					db()->assignStr("name", $class);
					db()->assignStr("type", "image");
					db()->assignStr("content", $imgUrl);
					if ($element)
					{
						db()->update(DB_PREFIX."design_elements", "WHERE did='".intval($element["did"])."'");
					}
					else
					{
						db()->insert(DB_PREFIX."design_elements");
					}
					
					$response = array(
						"status"=>1,
						"imgUrl" => $imgUrl,
						"width" => $imageSize[0],
						"height" => $imageSize[1]
					);
				}
				
				
			}
		}
		return json_encode($response);
	}
	
	/**
	 * Delete custom image and set skin image (if exists)
	 * @param $class
	 * @return mixed
	 */
	public function deleteCustomImage($class)
	{
		$response = array("status"=>0);
		
		$pathCustom = $this->getCustomSkinPath()."images".DS.escapeFileName($class).".";
		$pathSkin = $this->getRichSkinPath()."images".DS.escapeFileName($class).".";
		$pathBase = $this->getBaseSkinPath()."images".DS.escapeFileName($class).".";
		
		$imageSkin = false;
		$imageBase = false;
		
		$types = array("jpg", "png", "gif");
		
		//delete custom image
		foreach($types as $type) @unlink($pathCustom.$type);
		
		//find parent image (if exists)
		foreach($types as $type)
		{
			if (is_file($pathSkin.$type) && !$imageSkin) $imageSkin = $pathSkin.$type;
			if (is_file($pathBase.$type) && !$imageBase) $imageBase = $pathBase.$type;	
		}
		
		if ($imageSkin)	$finalImage = $imageSkin;	
		elseif ($imageBase) $finalImage = $imageBase;	
		else $finalImage = false;
		
		if ($finalImage)
		{
			$p = pathinfo($finalImage);
			$cacheImage = $this->getCacheSkinPath()."images".DS.escapeFileName($class).".".$p['extension'];
			@unlink($cacheImage);
			@copy($finalImage, $cacheImage);
			$content = str_replace("\\", "/", $cacheImage);
		}
		else
		{
			$content = "";
		}
		
		db()->reset();
		db()->assignStr("content", $content);
		db()->assignStr("type", $content == "" ? "image-hidden" : "image");
		db()->update(DB_PREFIX."design_elements", "WHERE element_id='".db()->escape($class)."'");
		$response = array("status"=>1, "newImage"=>$content == "" ? false : $content);
		return json_encode($response);
	}
	
	/**
	 * Hide image
	 * @param $class
	 * @return json
	 */
	public function hideImage($class)
	{
		db()->reset();
		db()->assignStr("type", "image-hidden");
		db()->update(DB_PREFIX."design_elements", "WHERE element_id='".db()->escape($class)."'");
		$response = array("status"=>1);
		return json_encode($response);
	}
	
	/**
	 * Show omage
	 * @param $class
	 * @return json
	 */
	public function showImage($class)
	{
		db()->reset();
		db()->assignStr("type", "image");
		db()->update(DB_PREFIX."design_elements", "WHERE element_id='".db()->escape($class)."'");

		$response = array("status"=>1);
		return json_encode($response);
	}
	
	/**
	 * Save uploaded file in custom folder
	 * @param $parentClass
	 * @param $parentSelector
	 * @param $class
	 * @param $selector
	 * @param $fileData
	 * @return json
	 */
	public function uploadBackground($parentClass, $parentSelector, $class, $selector, $fileData)
	{
		$response = array("status"=>0);
		
		//check is file uploaded
		if (is_file($fileData["tmp_name"]) && filesize($fileData["tmp_name"]) > 0)
		{
			//check is it image
			$imageSize = getimagesize($fileData["tmp_name"]);
			//format new file name
			
			$ext = array(IMAGETYPE_JPEG=>"jpg", IMAGETYPE_PNG=>"png", IMAGETYPE_GIF=>"gif");
			if (in_array($imageSize[2], array_keys($ext)))
			{
				$fileName = $parentClass.((strlen($selector) > 0 && $selector[0] == ".") ? "" : "-").$selector.".".$ext[$imageSize[2]];
				$fileDir = $this->getCustomSkinPath()."images".DS."custom".DS;
				if (!is_dir($fileDir)) $this->_mkdir($fileDir, $this->_permDir, true);
				
				$filePath = $fileDir.$fileName; 
				if (is_file($filePath)) unlink($filePath);
				
				if (move_uploaded_file($fileData["tmp_name"], $filePath))
				{
					chmod($filePath, $this->_permFile);
					$cacheFilePath = $this->getCacheSkinPath()."images/custom/".$fileName;
					if (is_file($cacheFilePath)) unlink($cacheFilePath);
					$d = dirname($cacheFilePath);
					if (!is_dir($d)) $this->_mkdir($d, $this->_permDir, true);
					copy($filePath, $cacheFilePath);
					chmod($cacheFilePath, $this->_permFile);
					
					$response = array(
						"status"=>1,
						"fileName" => $fileName,
						"relativeUrl" => "./../images/custom/".$fileName,
						"skinUrl" => "content/skins/_custom/skin/images/custom/".$fileName,
						"cacheUrl" => str_replace("\\", "/", $this->getCacheSkinPath())."images/custom/".$fileName
					);
				}
			}
		}
		return json_encode($response);
	}
	
	/**
	 * Create css file from parts
	 * @return boolean
	 */
	public function renderElementsCSSFile()
	{
		//TODO: replace with db settings
		include("content/engine/design/styles/elements/elements-map.php");
		
		db()->query("SELECT CONCAT('panel', SUBSTR(name, 28)) AS panel_id, title FROM ".DB_PREFIX."layout_elements WHERE name LIKE 'element_panels_panel-custom%' ORDER BY title");
		while(($panel = db()->movenext()) != false)
		{
			$cssMap["groups"]["panels"]["elements"][] = 
				array("parent-class" => $panel["panel_id"], "parent-selector" => ".".$panel["panel_id"], "title" => $panel["title"], "type" => "panel");
		}	
		
		$customCssPath = $this->getCustomCssPath();
		$css = "";
		
		foreach ($cssMap["groups"] as $groupKey => $groupData)
		{
			foreach ($groupData["elements"] as $elementKey => $elementData)
			{
				if (isset($elementData["type"]))
				{
					$elementData["elements"] = 
						isset($elementData["elements"]) ? 
						array_merge($cssMap["types"][$elementData["type"]]["elements"], $elementData["elements"]) :
						$cssMap["types"][$elementData["type"]]["elements"];
					unset($elementData["type"]);
				}
				$elementData["elements"] = isset($elementData["elements"]) ? $elementData["elements"] : false;
				if ($elementData["elements"])
				{
					foreach ($elementData["elements"] as $subElement)
					{
						$elementCssFile = $customCssPath.trim(escapeFileName($elementData["parent-selector"]." ".$subElement["selector"])).".css";
						$elementCssFileOld = $customCssPath.escapeFileName($elementData["parent-selector"]." ".$subElement["selector"]).".css";
						if (is_file($elementCssFile))
						{
							$elementCss = trim(file_get_contents($elementCssFile));
							
							if ($elementCss != "")
							{
								$css.=$elementCss."\n";
							}
						}
						elseif (is_file($elementCssFileOld))
						{
							$elementCss = trim(file_get_contents($elementCssFileOld));
							
							if ($elementCss != "")
							{
								$css.=$elementCss."\n";
							}
						}	
					}
				}
				
				$cssMap["groups"][$groupKey]["elements"][$elementKey]["elements"] = $elementData["elements"];
			}
		}
		$cssCustomFilePath = $this->getCustomSkinPath()."styles".DS;
		$cssCustomFile = $cssCustomFilePath."designmode.css";
		
		if (!is_dir($cssCustomFilePath)) $this->_mkdir($cssCustomFilePath, $this->_permDir, true);
		if (is_file($cssCustomFile)) unlink($cssCustomFile);
		file_put_contents($cssCustomFile, $css);
		@chmod($cssCustomFile, FileUtils::getFilePermissionMode());
		
		$cssCacheFilePath = $this->getCacheSkinPath()."styles".DS;
		$cssCacheFile = $cssCacheFilePath."designmode.css";
		if (!is_dir($cssCacheFilePath)) $this->_mkdir($cssCacheFilePath, $this->_permDir, true);
		if (is_file($cssCacheFile)) unlink($cssCacheFile);
		copy($cssCustomFile, $cssCacheFile);
		@chmod($cssCacheFile, FileUtils::getFilePermissionMode());
		
		return "content/cache/skins/".escapeFileName($this->_settings["DesignSkinName"])."/styles/designmode.css?".uniqid(rand(1, 100));
	}
	
	
	/**
	 * Gets the HTML for the specified element
	 * @param $designElementClass
	 * @return json
	 */
	public function getElementHtml($designElementClass)
	{
		$templateFile = $this->getElementFile($designElementClass);
	
		$fileCustom = $this->getCustomSkinPath().$templateFile;
		$fileRich = $this->getRichSkinPath().$templateFile;
		$fileBase = $this->getBaseSkinPath().$templateFile;
		
		$response = array("status" => 1, "location" => "custom", "type"=>"native", "html"=>"", "message" => "");
		
		if (substr($designElementClass, 0, 27) == "element_panels_panel-custom")
		{
			$response["type"] = "custom";	
		}
		
		if (is_file($fileCustom))
		{
			$filePath = $fileCustom;
		}
		elseif (is_file($fileRich))
		{
			$filePath = $fileRich;
			$response["location"] = "skin";
		}
		elseif (is_file($fileBase))
		{
			$filePath = $fileBase;
			$response["location"] = "base";
		}
		else
		{
			$response["status"] = 0;
			$response["message"] = "Cannot find file\n{$templateFile}";
		}
		
		if ($response["status"])
		{
			try 
			{
				$html = file_get_contents($filePath);
				$response["html"] = $html;
			}
			catch (Exception $e)
			{
				$response["status"] = 0;
				$response["message"] = $e->getMessage();
			}
		}
		return json_encode($response);
	}
	
	/**
	 * Gets the CSS for the specified element
	 * @param $designElementClass
	 * @return json
	 */
	public function getElementCss($designElementCssClass)
	{
		$response = array("status"=>1, "css_exists"=>0, "css" => "");
		$elementCssFile = $this->getCustomCssPath().escapeFileName($designElementCssClass).".css";
		if (is_file($elementCssFile))
		{
			try
			{
				$response = array("status"=>1, "css_exists"=>1, "css"=>file_get_contents($elementCssFile));
			}
			catch (Exception $e)
			{
				$response = array("status"=>0, "message"=>"Can not read css file. Please check permissions.\n".$elementCssFile);
			}
		}
		return json_encode($response);
	}
	
	/**
	 * Writes CSS for specified element. All changes here will be saved in content/skins/_custom/css folder
	 * @param $designElementClass
	 * @param $html
	 */
	public function saveElementCss($designElementCssClass, $css)
	{
		$response = array("status"=>1);
		
		$customCssPath = $this->getCustomCssPath();
		
		$elementCssFile = $customCssPath.escapeFileName($designElementCssClass).".css";
		try
		{
			file_put_contents($elementCssFile, $css);
			@chmod($elementCssFile, $this->_permFile);
			
			//assemble custom css file
			$d = dir($customCssPath);
			
			$cssCustomFile = $this->getCustomSkinPath()."styles".DS."designmode.css";
			
			$cssCustomFileDir = dirname($cssCustomFile);
			if (!is_dir($cssCustomFileDir)) $this->_mkdir($cssCustomFileDir, $this->_permDir, true);
			
			$cssCacheFile = $this->getCacheSkinPath()."styles".DS."designmode.css";
			
			$f = fopen($cssCustomFile, "w+");
			
			fputs($f, "/** CUSTOM CSS FILE **/\n");
			fputs($f, "/** PLEASE DO NOT CHANGE AS THIS FILE IS AUTOGENERATED **/\n\n");
			
			while (false !== ($entry = $d->read()))
			{
   				if (is_file($customCssPath.DS.$entry) && substr($entry, -4) == ".css")
   				{
   					fputs($f, file_get_contents($customCssPath.DS.$entry)."\n");
   				}
			}
			$d->close();
			fclose($f);
			
			if (is_file($cssCacheFile)) unlink($cssCacheFile);
			copy($cssCustomFile, $cssCacheFile);
			
			$response["cssFileUrl"] = "content/cache/skins/".escapeFileName($this->_settings["DesignSkinName"])."/styles/designmode.css?".uniqid(rand(1, 100));
			
		}
		catch (Exception $e)
		{
			$response = array("status"=>0, "message"=> $e->getMessage()); 
		}
		
		return json_encode($response);
	}
}


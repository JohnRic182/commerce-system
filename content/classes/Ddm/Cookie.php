<?php

/**
* 
*/
class Ddm_Cookie
{
	/**
	 * Global expires setting for class
	 *
	 * @var string
	 */
	var $_expires = '';
	
	/**
	 * Global secure setting for class
	 *
	 * @var string
	 */
	var $_secure = '';
	
	/**
	 * Global domain setting for class
	 *
	 * @var string
	 */
	var $_domain = '';
	
	/**
	 * Global path setting for class
	 *
	 * @var string
	 */
	var $_path   = '';
	
	/**
	 * Cookie expires setting 
	 *
	 * @var string
	 */
	var $expires = '';
	
	/**
	 * Cookie secure setting 
	 *
	 * @var string
	 */
	var $secure = '';
	
	/**
	 * Cookie domain setting 
	 *
	 * @var string
	 */
	var $domain = '';
	
	/**
	 * Cookie path setting 
	 *
	 * @var string
	 */
	var $path   = '';
	
	
	/**
	 * Class constructor
	 *
	 * @param array $options 
	 * @return void
	 */
	function Ddm_Cookie($options = array())
	{
		$this->_expires = isset($options['expires']) ? $options['expires'] : 0;
		$this->_domain  = isset($options['domain']) ? $options['domain'] : '';
		$this->_secure  = isset($options['secure']) ? $options['secure'] : '';
		$this->_path    = isset($options['path']) ? $options['path'] : '';
	}
	
	/**
	 * Overrides any options set via constructor
	 *
	 * @param array $options 
	 * @return void
	 */
	function setOptions($options)
	{
		$this->expires = isset($options['expires']) ? $options['expires'] : $this->_expires;
		$this->domain  = isset($options['domain']) ? $options['domain'] : $this->_domain;
		$this->secure  = isset($options['secure']) ? $options['secure'] : $this->_secure;
		$this->path    = isset($options['path']) ? $options['path'] : $this->_path;
	}
	
	/**
	 * Getter from $_COOKIE
	 *
	 * @param string $name 
	 * @return string
	 */
	function get($name)
	{
		if (isset($_COOKIE[$name])) return $_COOKIE[$name];
	}
	
	/**
	 * Setter for $_COOKIE
	 *
	 * @param string $name The name of the cookie
	 * @param string $value The valud of the cookie
	 * @param array $options 
	 * @return void
	 */
	function set($name, $value, $options = array())
	{
		$this->setOptions($options);
		
		setcookie($name, $value, $this->expires, $this->path, $this->domain, $this->secure);
	}
	
	/**
	 * Removes the cookie permanently
	 *
	 * @param string $name The name of the cookie to remove
	 * @return void
	 */
	function remove($name)
	{
		setcookie($name, '', time() - 3600*24*365);
		unset($_COOKIE[$name]);
	}
}

<?php
/**
 * 
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_function_script($params, &$smarty)
{
	/**
	 * save script data into static var
	 */
	if (is_array($params) && count($params) > 0)
	{
		if (!array_key_exists("src", $params))
		{
			view()->trigger_error("script: src param missed");
			return;
		}
		
		if (array_key_exists("path", $params))
		{
			$params["src"] = $params["path"].$params["src"];
			unset($params["path"]);
		}
		
		$action = (array_key_exists('action', $params)) ? $params['action'] : 'append';
		
		$file = $params['src'];
		
		$file .= (strstr($file, '?'))
			? '&'.APP_VERSION.'=1'
			: '?'.APP_VERSION.'=1';
		
		unset($params['src']);
		unset($params['action']);
		
		switch ($action)
		{
			case 'render':
				return view()->getJavascriptString($file, $params);
				break;
			
			case 'prepend':
				view()->prependJavascript($file, $params);
				break;
				
			case 'remove':
				view()->removeJavascript($file);
				break;
			
			case 'append':
			default:
				view()->appendJavascript($file, $params);
				break;
		}
		
		return;
	}
	
	/**
	 * otherwise render scripts html code 
	 */
	return "\r\n".implode("\r\n", view()->getJavascripts());
}
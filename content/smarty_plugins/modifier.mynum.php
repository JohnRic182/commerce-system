<?php

function smarty_modifier_mynum($num)
{
	if ($num == 0 || $num == '0')
		return '';

	return myNum($num);
}
<?php
function smarty_function_product_image($params, &$smarty){
	extract($params);
	if(!isset($image_size) || !isset($image_location) || !isset($image_url) || !isset($product_id)){
		view()->trigger_error("product_image: not enough params");
		return;
	}
	$image_size = strtolower($image_size);
	$image_location = strtolower($image_location);
	$product_id = strtolower($product_id);
	$http_url = view()->get_template_vars("GlobalHttpUrl");
	$no_image_small = view()->get_template_vars("imageNoImageSmall");
	$no_image_big = view()->get_template_vars("imageNoImageBig");
	
	
	$width_param = "";
	$src = false;
	if($image_location == "local"){
		if($image_size == "small"){
			if(file_exists("images/products/thumbs/".$product_id.".jpg")){
				$src = $http_url."/images/products/thumbs/".$product_id.".jpg";
			}
		}
		elseif(file_exists("images/products/".$product_id.".jpg")){
			$path = "images/products/".$product_id.".jpg";
			$src = $http_url."/images/products/".$product_id.".jpg";
		}
		elseif(file_exists("images/products/".$product_id.".gif")){
			$path = "images/products/".$product_id.".gif";
			$src = $http_url."/images/products/".$product_id.".gif";
		}
		elseif(file_exists("images/products/".$product_id.".png")){
			$path = "images/products/".$product_id.".jpg";
			$src = $http_url."/images/products/".$product_id.".png";
		}
		if($image_size != "small"){
			$img_info = getImageSize($path);
			$width_param = 'width="100%';
		}
	}
	else{
		if($image_url != ""){
			$src = $image_url;
		}
	}
	$img_params = "";
	reset($params);
	foreach($params as $name=>$value){
		if(!in_array($name, array("image_id", "image_location", "image_url", "product_id"))){
			$img_params .= (' '.$name.'="'.$value.'" ');
		}
	}
	if($src){
		return '<img '.$width_param.' src="'.$src.'" '.$img_params.'>';
	}
	else{
		//return view()->get_template_vars($image_size=="small"?"imageNoImageSmall":"imageNoImageBig");
		return view()->get_template_vars($image_size=="small"?"imageNoImageSmall":"imageNoImageBig");
	}
	
}
?>
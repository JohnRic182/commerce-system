<?php
/**
 *
 * @param type $params
 * @param type $smarty
 * @return type 
 */
function smarty_function_admin_tabs($params, &$smarty)
{
	if (isset($params['tabs']))
	{
		$tabs = $params['tabs'];

		$smarty->assign('smarty_tab_items', $tabs->getTabs());
		$smarty->assign('smarty_active_tab', $tabs->getActiveTab());
		$smarty->assign('smarty_tabs_id', $tabs->getId());

		return $smarty->fetch('templates/elements/tabs/tabs.html');
	}
	
	return '';
}
<?php
/**
 *
 * @param type $params
 * @param type $smarty
 * @return type 
 */
function smarty_function_getpp($params, &$smarty)
{
	$space = false;
	if (isset($params['space']))
	{
		$space = $params['space'] == 'true';
	}

	return getpp($space);
}
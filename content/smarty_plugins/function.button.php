<?php
function smarty_function_button($params, $smarty)
{
	if (!array_key_exists("class", $params))
	{
		view()->trigger_error("button: class param missed");
		return;
	}
	
	if (!array_key_exists("text", $params))
	{
		view()->trigger_error("button: text param missed");
		return;
	}
	
	$class = $params["class"]; unset($params["class"]);
	$text = $params["text"]; unset($params["text"]);
	$style = isset($params["style"]) ? $params["style"] : ""; unset($params["style"]);
	$type = isset($params["type"]) ? $params["type"] : "submit"; unset($params["type"]);
	$disabled = isset($params["disabled"]) && $params["disabled"] ? "disabled" : false; unset($params["disabled"]);
	
	
	if ($type == "reset" && !array_key_exists("form", $params))
	{
		view()->trigger_error("button: form param required for reset buttons");
		return;
	}
	
	$form = isset($params["form"]) ? $params["form"] : false; unset($params["form"]);
	
	$designImages = $smarty->get_template_vars("designImages");
	
	$params["editable"] = true;
	
	
	
	$html = null;
	
	// Check each class name that was passed to determine if there is a design element that should be used
	$classes = preg_split('/ /', $class);
	foreach ($classes as $classname)
	{
		// If we find a design element, set $html and break out of the loop
		if (in_array($classname, array_keys($designImages)))
		{
			$html = 
			'<input '.
				'type="image" '.
				'src="'.$designImages[$classname]["content"].'" '.
				'style="'.($style != '' ? $style : 'background-image: none;').'" '.
				($type == "reset" && $form ? 'onclick="document.forms[\''.$form.'\'].reset()" ' : '').
				($disabled ? 'disabled="disabled" ': '').
				'class="'.$class.' '.$type.'-image '.smarty_function_class($params, $smarty).'" '.
				'alt="'.htmlspecialchars($text).'" ';
			break;
		}
	}
	
	if (is_null($html))
	{
		$html = '<input '.
				'type="'.$type.'" '.
				'class="'.$class.' '.$type.' '.smarty_function_class($params, $smarty).'" '.
				($disabled ? 'disabled="disabled" ': '').
				'value="'.htmlspecialchars($text).'" ';
	}

	unset($params["file"]);
	unset($params["editable"]);
	unset($params["dropabe"]);
	
	foreach ($params as $paramName=>$paramValue)
	{
		$html.=
			$paramName.'="'.$paramValue.'" ';
	}
	
	return $html.'/>';
}
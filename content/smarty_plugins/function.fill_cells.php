<?php
/**
 * 
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_function_fill_cells($params, &$smarty)
{
	$content = '<td></td>';
	
	if (!array_key_exists("index", $params) || !array_key_exists("columns", $params))
	{
		view()->trigger_error("fill_cells: index, columns");
		return;
	}
	
	$index = $params['index'];
	$columns = $params['columns'];
	
	if (array_key_exists("content", $params)) $content = $params['content'];
	
	return str_repeat($content, (ceil($index / $columns) * $columns) - $index);
}
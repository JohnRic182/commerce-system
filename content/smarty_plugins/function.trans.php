<?php

function smarty_function_trans($params, &$smarty)
{
	$translator = Framework_Translator::getInstance();

	$key = $params['key'];

	unset($params['key']);

	if (trim($key) != '')
	{
		return $translator->trans($key, $params);
	}

	return $key;
}
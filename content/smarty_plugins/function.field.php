<?php
/**
 *
 * @param type $params
 * @param type $smarty
 * @return type 
 */
function smarty_function_field($params, &$smarty)
{
	if (isset($params['form']) && isset($params['field']))
	{
		$form = $params['form'];
		
		foreach ($form->getItems() as $item)
		{
			if ($item->getName() == $params['field'])
			{
				$template = null;
				
				if ($item->getType() == 'group')
				{
					$template = 'templates/elements/form/field-group.html';
				}
				else
				{
					$template = $item->getTemplate();
				}
				
				if (!is_null($template))
				{
					$item->setRendered();
					$smarty->assign('field', $item->toArray());
					return $smarty->fetch($template);
				}
				
				break;
			}
		}
	}

	return '';
}
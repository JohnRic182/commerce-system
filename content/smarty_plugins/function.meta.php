<?php
/**
 * 
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_function_meta($params, &$smarty)
{
	/**
	 * save meta data into static var
	 */
	if (is_array($params) && count($params) > 0)
	{
		
		if (!array_key_exists("name", $params) && !array_key_exists("http-equiv", $params))
		{
			view()->trigger_error("meta: name or http-equiv param missed. If you are passing http-equiv, please set the type to http-equiv");
			return;
		}
		if (!array_key_exists("content", $params))
		{
			view()->trigger_error("meta: content param missed");
			return;
		}
		
		$type = 'name';
		
		$is_equiv = false;
		if (array_key_exists('http-equiv', $params))
		{
			$is_equiv = true;
			$params['name'] = $params['http-equiv'];
		}
		
		// check if this is an http-equiv metatag
		if (array_key_exists('type', $params))
		{
			$type = $params['type'];
		}
		
		view()->appendMetatag($params['name'], $params['content'], $is_equiv, $type);
		return "";
	}
	
	/**
	 * otherwise render metas html code 
	 */
	return "\r\n".implode("\r\n", view()->getMetatags());
}
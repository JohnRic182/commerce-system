<?php

function smarty_function_lang($params, &$smarty)
{
	extract($params);

	if (!isset($msg))
	{
		view()->trigger_error('lang: msg param missed');
		return;
	}

	foreach($params as $name=>$value)
	{
		if ($name != 'msg') $msg = str_replace('%'.$name, $value, $msg);
	}

	return $msg;
}
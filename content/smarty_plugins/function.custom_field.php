<?php
/**
 * @param $params
 * @param $smarty
 * @return string|void
 */
function smarty_function_custom_field($params, &$smarty)
{
	$field = isset($params['field']) ? $params['field'] : null;

	if ($field == null)
	{
		view()->trigger_error("custom_field: field param missed");
		return;
	}

	$field_type = isset($field['field_type']) ? $field['field_type'] : 'text';
	$field_id = isset($field['field_id']) ? $field['field_id'] : '';
	$text_length = isset($field['text_length']) ? intval($field['text_length']) : 0;
	$value = isset($field['value']) ? $field['value'] : '';
	$value_checked = isset($field['value_checked']) ? $field['value_checked'] : '';
	$parsed_options = isset($field['parsed_options']) ? $field['parsed_options'] : '';
	$is_required = isset($field['is_required']) ? $field['is_required'] : '';
	$field_name = isset($field['field_name']) ? $field['field_name'] : '';
	$field_place = isset($field['field_place']) ? $field['field_place'] : '';
	
	$ctrl = "";

	switch ($field_type)
	{
		case "text" : $ctrl = '<input id="cf' . $field_id . '" class="custom-field" type="text" name="custom_field[' . $field_id . ']" ' . ($text_length > 0 ? ('maxlength="' . $text_length . '"') : '') . ' value="' . htmlspecialchars($value) . '">'; break;
		case "checkbox" : $ctrl = '<input id="cf' . $field_id . '" class="custom-field" type="checkbox" name="custom_field[' . $field_id . ']" value="' . $value_checked . '" ' . ($value == $value_checked ? "checked" : "") . '>'; break;
		case "textarea" : $ctrl = '<textarea id="cf' . $field_id . '" ' . ($text_length > 0 ? ('maxlength="' . $text_length . '"') : '') . ' class="custom-field" name="custom_field[' . $field_id . ']" class="small">' . htmlspecialchars($value) . '</textarea>'; break;
		case "select" : 
		{
			$ctrl = '<select id="cf' . $field_id . '" class="custom-field" name="custom_field[' . $field_id . ']">';

			foreach ($parsed_options AS $key => $option)
			{
				$ctrl .= '<option value="' . htmlspecialchars($option) . '" ' . (($value == $option) ? "selected" : "") . '>' . htmlspecialchars($option) . '</option>';
			}

			$ctrl .= '</select>';

			break;
		}
		case "radio" : 
		{
			for ($i = 0; $i < count($parsed_options); $i++)
			{
				$option = trim($parsed_options[$i]);
				$ctrl .= '<input class="custom-field" type="radio" name="custom_field[' . $field_id . ']" value="' . htmlspecialchars($option) . '" ' . ($value == $option || ($value == '' && $i == 0)  ? "checked" : "") . ' id="cf_' . $field_id . '_' . $i . '">&nbsp;';
				$ctrl .= '<label for="cf_' . $field_id . '_' . $i . '">' . htmlspecialchars($option) . '</label><br/>';
			}
			break;
		}
	}

	if (!($field_type == "radio"))
	{
		$req = $is_required == 1 ? "yes" : "no";
	}
	else
	{
		$req = "no";
	}

	$ctrl .= '<input type="hidden" name="custom_field_required[' . $field_id . ']" value="' . $req . '"/>';
	$ctrl .= '<input type="hidden" name="custom_field_name[' . $field_id . ']" value="' . htmlentities($field_name, ENT_COMPAT) . '"/>';
	$ctrl .= '<input type="hidden" name="custom_field_place[' . $field_id . ']" value="' . $field_place . '"/>';
	
	return $ctrl;
}

<?php
function smarty_function_product_minmax_order($params, &$smarty){
	extract($params);
	if(!isset($min_order) || !isset($max_order)){
		view()->trigger_error("product_minmax_order: not enough params");
		return;
	}
	$min = ($min_order > 1)?$min_order:"1";
	$max = ($max_order > 0)?$max_order:"-";
	return $min." / ".$max;
}
?>
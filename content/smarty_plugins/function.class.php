<?php
/**
 * 
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_function_class($params, &$smarty)
{
	if (isset($_SESSION["admin_session_id"]) && isset($_SESSION["admin_auth_id"]) && isset($_SESSION["admin_auth_time"]) && isset($_SESSION["admin_design_mode"]) && $_SESSION["admin_design_mode"])
	{
		if (!array_key_exists("file", $params))
		{
			view()->trigger_error("class: file param missed");
			return;
		}
		
		$elementClassName = "element".preg_replace(array("/templates/", "/\\\/", "/\//", "/.html/"), array("", "_", "_", ""), $params["file"]);
		
		return 
			$elementClassName.
			(array_key_exists("editable", $params) && $params["editable"] ? " editable":"").
			(array_key_exists("droppable", $params) && $params["droppable"] ? " droppable":"");
	}
	return "";
}
<?php
/**
 * 
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_function_link($params, &$smarty)
{
	/**
	 * save links data into static var
	 */
	if (is_array($params) && count($params) > 0)
	{
		if (!array_key_exists("href", $params))
		{
			view()->trigger_error("link: href param missed");
			return;
		}
		
		if (array_key_exists("path", $params))
		{
			$params["href"] = $params["path"].$params["href"];
			unset($params["path"]);
		}
		
		$action = (array_key_exists('action', $params)) ? $params['action'] : 'append';
		
		$file = $params['href'];
		
		$file .= (strstr($file, '?'))
			? '&'.APP_VERSION.'=1'
			: '?'.APP_VERSION.'=1';
		
		unset($params['href']);
		unset($params['action']);
		
		switch ($action)
		{
			case 'render':
				return view()->getStylesheetString($file, $params);
				break;
				
			case 'prepend':
				view()->prependStylesheet($file, $params);
				break;
				
			case 'remove':
				view()->removeStylsheet($file);
				break;
			
			case 'append':
			default:
				view()->appendStylesheet($file, $params);
				break;
		}
		
		return '';
	}
	
	/**
	 * otherwise render links html code
	 */
	return "\r\n".implode("\r\n", view()->getStylesheets());
}
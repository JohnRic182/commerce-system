<?php
/**
 * 
 * @param $params
 * @param $smarty
 * @return string
 */
function smarty_function_headclose_css_snippets($params, &$smarty)
{
	$html = '';

	$activeSnippets = view()->getActiveSnippets('css');

	if (count($activeSnippets) > 0)
	{
		$current_page = $smarty->get_template_vars('current_page');
		foreach ($activeSnippets as $one)
		{
			$page_types = explode(',', $one['page_types']);
			if (in_array($current_page, $page_types) || in_array('all', $page_types))
			{
				switch ($one['type'])
				{
					case 'include' :
						{
							if (strpos($one['content'], '<style') === FALSE)
							{
								$html .= '<link href="'.$one['content'].'" rel="stylesheet" type="text/css" />';
							}
							else
							{
								$html .= $one['content'];
							}
							$html .= "\n";
							break;
						}
					case 'chunk' :
						{
							$template = $one['content'];
							$content = $template;

							$template =  preg_replace("/\{\\$(.*?)\}/","SMARTY__\\1__SMARTY", $template);
							$template = preg_replace(array("/\{(?!literal)/i", "/(?<!literal)\}/i"),array("{literal}{{/literal}", "{literal}}{/literal}"), $template);
							$template = preg_replace(array("/\SMARTY__/", "/\__SMARTY/"),array("{\$", "}"), $template);

							require_once( $smarty->_get_plugin_filepath( 'function', 'eval' ));
							$content = smarty_function_eval(array('var'=>$template), $smarty);

							if (strpos($one['content'], '<style') === FALSE)
							{
								$html .= '<style type="text/css">' . $content . '</style>';
							}
							else
							{
								$html .=  $content;
							}
							$html .= "\n";
							break;
						}
				}
			}
		}
	}

	return $html;
}
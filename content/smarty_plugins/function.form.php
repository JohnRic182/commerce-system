<?php
/**
 *
 * @param type $params
 * @param type $smarty
 * @return type 
 */
function smarty_function_form($params, &$smarty)
{
	if (isset($params['form']))
	{
		$smarty->assign('smarty_form_items', $params['form']->getItemsToRender());
		return $smarty->fetch('templates/elements/form/form.html');
	}
	
	return '';
}
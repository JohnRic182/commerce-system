<?php

	$cssMap = array(
		/**
		 * Types
		 */
		"types" => array(
			"panel" => array(
				"elements" => array(
					array("class" => "title", "selector" => ".title", "title" => "Widget header", "editors"=>array("font", "align", "background"), "preview"=>"panel"),
					array("class" => "content", "selector" => ".content", "title" => "Widget content", "editors"=>array("font", "align", "background"), "preview"=>"panel"),
					array("class" => "content", "selector" => "a", "title" => "Link styles", "editors"=>array("font"), "preview"=>"link")
				)
			),
			"menu" => array(
				"elements" => array(
					array("class" => "drop-down-menu", "selector" => "ul.drop-down-menu", "title" => "Menu background", "editors"=>array("background"), "preview"=>"menu1"),
					array("class" => "drop-down-menu-item", "selector" => "a.drop-down-menu-item", "title" => "Menu item", "editors"=>array("font", "background"), "preview"=>"menu2"),
					array("class" => "drop-down-menu-item-hover", "selector" => "a.drop-down-menu-item-hover", "title" => "Menu item hover", "editors"=>array("font", "background"), "preview"=>"menu3"),
					array("class" => "drop-down-menu-item-sub", "selector" => "li ul a.drop-down-menu-item", "title" => "Sub menu item", "editors"=>array("font", "background"), "preview"=>"menu2"),
					array("class" => "drop-down-menu-item-hover-sub", "selector" => "li ul a.drop-down-menu-item-hover", "title" => "Sub menu item hover", "editors"=>array("font", "background"), "preview"=>"menu3"),
				)
			),
			"form" => array(
				"elements" => array(
					//array("class" => "fieldset", "selector" => ".fieldset", "title" => "Form background", "editors"=>array("background"), "preview"=>"form"),
					array("class" => "", "selector" => "h3", "title" => "Form header", "editors"=>array("font", "align", "background"), "preview"=>"form"),
					array("class" => "field", "selector" => ".field label", "title" => "Field label", "editors"=>array("font", "align", "background"), "preview"=>"form"),
					array("class" => "required", "selector" => ".field.required label", "title" => "Required field label", "editors"=>array("font", "align", "background"), "preview"=>"form"),
					array("class" => "hint", "selector" => ".field .hint", "title" => "Field comments", "editors"=>array("font"), "preview"=>"form")
				)
			),
			"page" => array(
				"elements" => array(
					array("class" => "", "selector" => "h1", "title" => "Page header", "editors"=>array("font", "align", "background"), "preview"=>"page"),
					array("class" => "layout-zone-content", "selector" => ".layout-zone-content", "title" => "Content", "editors"=>array("font", "align", "background"), "preview"=>"page"),
					array("class" => "", "selector" => ".layout-zone-content a", "title" => "Content links", "editors"=>array("font"), "preview"=>"link")
				)
			),
			"catalog-view" => array(
				"elements" => array(
					array("class" => "catalog-product-title", "selector" => ".catalog-product-title a", "title" => "Product title", "editors"=>array("font", "align"), "preview"=>"catalog-product"),
					array("class" => "catalog-product-title", "selector" => ".catalog-product-title", "title" => "Product title background", "editors"=>array("background"), "preview"=>"catalog-product"),
					array("class" => "", "selector" => ".catalog-product-id", "title" => "Product ID", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "", "selector" => ".catalog-product-image", "title" => "Product image", "editors"=>array("border", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "", "selector" => ".catalog-product-overview", "title" => "Product overview", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "catalog-product-price", "selector" => ".catalog-product-price .price-label", "title" => "Product price label", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "catalog-product-price", "selector" => ".catalog-product-price .price-amount", "title" => "Product price amount", "editors"=>array("font", "background"), "preview"=>"catalog-product"),
					array("class" => "catalog-product-sale-price", "selector" => ".catalog-product-sale-price .sale-price-label", "title" => "Product sale price label", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "catalog-product-sale-price", "selector" => ".catalog-product-sale-price .sale-price-amount", "title" => "Product sale price amount", "editors"=>array("font", "background"), "preview"=>"catalog-product"),
					array("class" => "", "selector" => ".catalog-product-hot-deal", "title" => "Product hot deal", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "", "selector" => ".catalog-product-out-of-stock", "title" => "Product out of stock", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "", "selector" => ".catalog-product-click-to-view a", "title" => "Product click to view", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product"),
					array("class" => "", "selector" => ".catalog-product-more-in-category a", "title" => "Product category link", "editors"=>array("font", "align", "background"), "preview"=>"catalog-product")
				)
			),
			"product-view" => array(
				"elements" => array(
					array("class" => "", "selector" => ".product-title", "title" => "Product title", "editors"=>array("font", "align", "background"), "preview"=>"product"),
					array("class" => "", "selector" => ".product-bread-crumbs", "title" => "Bread crumbs", "editors"=>array("font", "background"), "preview"=>"link"),
					array("class" => "", "selector" => ".product-bread-crumbs a", "title" => "Bread crumbs links", "preview"=>"link", "editors"=>array("font")),
					array("class" => "", "selector" => ".product-id", "title" => "Product ID", "editors"=>array("font", "align", "background"), "preview"=>"product"),
					array("class" => "", "selector" => ".product-image", "title" => "Product image", "editors"=>array("border", "align", "background"), "preview"=>"product"),
					array("class" => "product-price", "selector" => ".product-price .price-label", "title" => "Product price label", "editors"=>array("font", "align", "background"), "preview"=>"product"),
					array("class" => "product-price", "selector" => ".product-price .price-amount", "title" => "Product price amount", "editors"=>array("font", "background"), "preview"=>"product"),
					array("class" => "product-price", "selector" => ".product-price .sale-price-label", "title" => "Product sale price label", "editors"=>array("font", "align", "background"), "preview"=>"product"),
					array("class" => "product-price", "selector" => ".product-price .sale-price-amount", "title" => "Product sale price amount", "editors"=>array("font", "background"), "preview"=>"product"),
					array("class" => "", "selector" => ".product-hot-deal", "title" => "Product hot deal", "editors"=>array("font", "align", "background"), "preview"=>"product"),
					array("class" => "", "selector" => ".product-out-of-stock", "title" => "Product out of stock", "editors"=>array("font", "align", "background"), "preview"=>"product"),
					array("class" => "", "selector" => ".product-overview", "title" => "Overview", "editors"=>array("font", "align", "background"), "preview"=>"text"),
					array("class" => "", "selector" => ".product-description", "title" => "Description", "editors"=>array("font", "align", "background"), "preview"=>"text"),
					array("class" => "", "selector" => ".product-secondary-images", "title" => "Secondary images", "editors"=>array("font", "align", "background"), "preview"=>"text"),
					array("class" => "", "selector" => ".product-reviews", "title" => "Reviews", "editors"=>array("font", "align", "background"), "preview"=>"text"),
					array("class" => "", "selector" => ".product-recommended", "title" => "Recommended products", "editors"=>array("font", "align", "background"), "preview"=>"text")
				)
			)
		),
		
		/**
		 * Previews
		 */
		"previews" => array(
			"text" => '<div class="preview-parent"><div class="preview-element">Sample text</div></div>',
			"menu1" => 
				'<div class="preview-parent clearfix" style="height:70px;">'.
					'<ul class="preview-element drop-down-menu clearfix" style="width:400px;">'.
						'<li><a class="drop-down-menu-item" href="#">Item 1</a></li>'.
						'<li><a class="drop-down-menu-item-hover" href="#">Item 2 (hover)</a></li>'.
						'<li><a class="drop-down-menu-item" href="#">Item 3</a></li>'.
					'</ul>'.
				'</div><br/>&nbsp;',
			"menu2" => 
				'<div class="preview-parent clearfix" style="height:70px;">'.
					'<ul class="drop-down-menu clearfix" style="width:400px;">'.
						'<li><a class="preview-element drop-down-menu-item" href="#">Item 1</a></li>'.
						'<li><a class="drop-down-menu-item-hover" href="#">Item 2 (hover)</a></li>'.
						'<li><a class="preview-element drop-down-menu-item" href="#">Item 3</a></li>'.
					'</ul>'.
				'</div><br/>&nbsp;',
			"menu3" => 
				'<div class="preview-parent clearfix" style="height:70px;">'.
					'<ul class="drop-down-menu clearfix" style="width:400px;">'.
						'<li><a class="drop-down-menu-item" href="#">Item 1</a></li>'.
						'<li><a class="preview-elemen drop-down-menu-item-hover" href="#">Item 2 (hover)</a></li>'.
						'<li><a class="drop-down-menu-item" href="#">Item 3</a></li>'.
					'</ul>'.
				'</div><br/>&nbsp;',
			"link" => '<div class="preview-parent"><div class="preview-element"><a href="#">Link text 1</a> | <a href="#">Link text 2</a></div></div>',
			"form" => 
				'<div class="preview-parent">'.
					'<div class="fieldset">'.
						'<h3>Form Header</h3>'.
						'<div class="field first"><label>Not required</label><div><input type="text"/></div></div>'.
						'<div class="field required"><label >Required</label><div><input type="text"/><div class="hint">Some comments</div></div></div>'.
						'<div class="buttons"><input type="submit" class="button" value="Button"/></div></div>'.
					'</div>'.
				'</div>',
			"page" => '<div class="preview-parent"><div class="page layout-zone-content"><h1>Sample page header</h1><div>Page content</div></div></div>',
			"panel" =>
				'<div class="panel preview-parent">'.
					'<h4 class="title">Widget Header</h4>'.
					'<div class="content">Widget Content</div>'.
				'</div>',
			
			"image" => 
				'<div class="preview-parent">'.
					'<div style="text-align:center;">'.
						'<img class="image preview-element"/>'.
					'</div>'.
					'<div class="preview-image-info"></div>'.
				'</div>',
			"button" => '<div class="preview-parent" style="text-align:center;"><input class="button preview-element" type="button" value="Button" onclick="alert(\'Hello!\');"/></div></div>',
			"catalog-product" => 
				'<div class="preview-parent">'.
					'<div class="catalog-product-title"><a href="#">Product Title</a></div>'.
					'<div class="catalog-product-id">product-id</div>'.
					'<div class="catalog-product-image">image</div>'.
					'<div class="catalog-product-overview">Some sample product overview comes here</div>'.
					'<div class="catalog-product-price"><span class="price-label">Price:</span> <span class="price-amount">$123.00</span></div>'.
					'<div class="catalog-product-sale-price"><span class="sale-price-label">Sale price:</span> <span class="sale-price-amount">$99.00</span></div>'.
					'<div class="catalog-product-hot-deal">Hot Deal!</div>'.
					'<div class="catalog-product-out-of-stock">Out of stock</div>'.
					'<div class="catalog-product-click-to-view"><a href="#">Click to view link</a></div>'.
					'<div class="catalog-product-more-in-category"><a href="#">See more in category</a></div>'.
				'</div><div>Note: it is not actual view</div>',
			"product" => 
				'<div class="preview-parent">'.
					'<div class="product-title">Product name</div>'.
					'<div class="product-id">product-id</div>'.
					'<div class="product-image">image</div>'.
					'<div class="product-overview">Some sample product overview comes here</div>'.
					'<div class="product-price"><span class="price-label">Price:</span> <span class="price-amount">$123.00</span></div>'.
					'<div class="product-price"><span class="sale-price-label">Sale price:</span> <span class="sale-price-amount">$99.00</span></div>'.
					'<div class="product-hot-deal">Hot Deal!</div>'.
					'<div class="product-out-of-stock">Out of stock</div>'.
				'</div><div>Note: it is not actual view</div>'
		),
		
		/**
		 * Fonts for fonts editor
		 */
		"fonts" => array(
			array(
				"group_name" => "Site",
				"elements" => array(
					array("caption" => "Site font", "selector" => ".page"),
				),
			),
			array(
				"group_name" => "Header",
				"elements" => array(
					array("caption" => "Header text", "selector" => ".page .layout-zone-header"),
					array("caption" => "Header links", "selector" => ".page .layout-zone-header a")
				),
			),
			array(
				"group_name" => "Footer",
				"elements" => array(
					array("caption" => "Footer text", "selector" => ".page .layout-zone-footer"),
					array("caption" => "Footer links", "selector" => ".page .layout-zone-footer a")
				),
			),
			array(
				"group_name" => "Navigation",
				"elements" => array(
					array("caption" => "Navigation text", "selector" => ".page .layout-zone-navigation"),
					array("caption" => "Navigation links", "selector" => ".page .layout-zone-navigation a")
				),
			),
			array(
				"group_name" => "Content",
				"elements" => array(
					array("caption" => "Content text", "selector" => ".page .layout-zone-content"),
					array("caption" => "Content links", "selector" => ".page .layout-zone-content a")
				),
			),
			array(
				"group_name" => "Widgets",
				"elements" => array(
					array("caption" => "Widget header text", "selector" => ".panel .title"),
					array("caption" => "Widget content text", "selector" => ".panel .content"),
					array("caption" => "Widget content link", "selector" => ".panel a")
				),
			),
			array(
				"group_name" => "Forms",
				"elements" => array(
					array("caption" => "Form header text", "selector" => ".page h3"),
					array("caption" => "Form field label", "selector" => ".page .field label"),
					array("caption" => "Form field label (required)", "selector" => ".page .field.required label"),
					array("caption" => "Form field comments", "selector" => ".page .field .hint")
				),
			),
			array(
				"group_name" => "Catalog Pages",
				"elements" => array(
					array("caption" => "Bread crumbs text", "selector" => ".page-catalog .catalog-bread-crumbs"),
					array("caption" => "Bread crumbs links", "selector" => ".page-catalog .catalog-bread-crumbs a"),
					array("caption" => "Navigation text color", "selector" => ".page-catalog .catalog-navigation"),
					array("caption" => "Product title", "selector" => ".page .catalog-product-title a"),
					array("caption" => "Product price label", "selector" => ".page .catalog-product-price .price-label"),
					array("caption" => "Product price amount", "selector" => ".page .catalog-product-price .price-amount"),
					array("caption" => "Product sale price label", "selector" => ".page .catalog-product-sale-price .sale-price-label"),
					array("caption" => "Product sale price amount", "selector" => ".page .catalog-product-sale-price .sale-price-amount"),
					array("caption" => "Product overview", "selector" => ".page .catalog-product-overview")
				),
			),
			array(
				"group_name" => "Product Page",
				"elements" => array(
					array("caption" => "Bread crumbs text", "selector" => ".page-product .product-bread-crumbs"),
					array("caption" => "Bread crumbs links", "selector" => ".page-product .product-bread-crumbs a"),
					array("caption" => "Product title", "selector" => ".page-product .product-title"),
					array("caption" => "Product ID", "selector" => ".page-product .product-id"),					
					array("caption" => "Product price label", "selector" => ".page-product .product-price .price-label"),
					array("caption" => "Product price amount", "selector" => ".page-product .product-price .price-amount"),
					array("caption" => "Product sale price label", "selector" => ".page-product .product-price .sale-price-label"),
					array("caption" => "Product sale price amount", "selector" => ".page-product .product-price .sale-price-amount"),
					array("caption" => "Product overview", "selector" => ".page-product .product-overview"),
					array("caption" => "Product description", "selector" => ".page-product .product-description"),
				),
			)
		),
		
		/**
		 * Colors for colors editor
		 */
		"colors" => array(
			array(
				"group_name" => "Header",
				"elements" => array(
					array("caption" => "Header container background", "selector" => ".page .container-header", "type" => "bg"),
					array("caption" => "Header background", "selector" => ".page .layout-zone-header", "type" => "bg"),
					array("caption" => "Header text color", "selector" => ".page .layout-zone-header", "type" => "cl"),
					array("caption" => "Header links", "selector" => ".page .layout-zone-header a", "type" => "cl")
				),
			),
			array(
				"group_name" => "Footer",
				"elements" => array(
					array("caption" => "Footer container background", "selector" => ".page .container-footer", "type" => "bg"),
					array("caption" => "Footer background", "selector" => ".page .layout-zone-footer", "type" => "bg"),
					array("caption" => "Footer text color", "selector" => ".page .layout-zone-footer", "type" => "cl"),
					array("caption" => "Footer links", "selector" => ".page .layout-zone-footer a", "type" => "cl")
				),
			),
			array(
				"group_name" => "Side Menu",
				"elements" => array(
					array("caption" => "Side menu item background", "selector" => ".panel-catalog-categories a.drop-down-menu-item", "type" => "bg"),
					array("caption" => "Side menu item text", "selector" => ".panel-catalog-categories a.drop-down-menu-item", "type" => "cl"),
					array("caption" => "Side menu hover background", "selector" => ".panel-catalog-categories a.drop-down-menu-item-hover", "type" => "bg"),
					array("caption" => "Side menu hover text", "selector" => ".panel-catalog-categories a.drop-down-menu-item-hover", "type" => "cl"),
					array("caption" => "Side menu sub item background", "selector" => ".panel-catalog-categories li ul a.drop-down-menu-item", "type" => "bg"),
					array("caption" => "Side menu sub item text", "selector" => ".panel-catalog-categories li ul a.drop-down-menu-item", "type" => "cl"),
					array("caption" => "Side menu sub item hover background", "selector" => ".panel-catalog-categories li ul a.drop-down-menu-item-hover", "type" => "bg"),
					array("caption" => "Side menu sub item hover text", "selector" => ".panel-catalog-categories li ul a.drop-down-menu-item-hover", "type" => "cl"),
				),
			),
			array(
				"group_name" => "Navigation",
				"elements" => array(
					array("caption" => "Navigation container background", "selector" => ".page .container-navigation", "type" => "bg"),
					array("caption" => "Navigation background", "selector" => ".page .layout-zone-navigation", "type" => "bg"),
					array("caption" => "Navigation text color", "selector" => ".page .layout-zone-navigation", "type" => "cl"),
					array("caption" => "Navigation links", "selector" => ".page .layout-zone-navigation a", "type" => "cl")
				),
			),
			array(
				"group_name" => "Top Menu",
				"elements" => array(
					array("caption" => "Top menu container background", "selector" => ".page .container-menu", "type" => "bg"),
					array("caption" => "Top menu background", "selector" => ".layout-zone-menu ul.drop-down-menu", "type" => "bg"),
					array("caption" => "Top menu item background", "selector" => ".layout-zone-menu a.drop-down-menu-item", "type" => "bg"),
					array("caption" => "Top menu item text", "selector" => ".layout-zone-menu a.drop-down-menu-item", "type" => "cl"),
					array("caption" => "Top menu hover background", "selector" => ".layout-zone-menu a.drop-down-menu-item-hover", "type" => "bg"),
					array("caption" => "Top menu hover text", "selector" => ".layout-zone-menu a.drop-down-menu-item-hover", "type" => "cl"),
					array("caption" => "Top menu sub item background", "selector" => ".layout-zone-menu li ul a.drop-down-menu-item", "type" => "bg"),
					array("caption" => "Top menu sub item text", "selector" => ".layout-zone-menu li ul a.drop-down-menu-item", "type" => "cl"),
					array("caption" => "Top menu sub item hover background", "selector" => ".layout-zone-menu li ul a.drop-down-menu-item-hover", "type" => "bg"),
					array("caption" => "Top menu sub item hover text", "selector" => ".layout-zone-menu li ul a.drop-down-menu-item-hover", "type" => "cl"),
				),
			),
			array(
				"group_name" => "Content",
				"elements" => array(
					array("caption" => "Site background", "selector"=>".page", "type" => "bg"),
					array("caption" => "Cart background", "selector"=>".page .container-wrap", "type" => "bg"),
					array("caption" => "Content background", "selector" => ".page .layout-zone-content", "type" => "bg"),
					array("caption" => "Content text color", "selector" => ".page .layout-zone-content", "type" => "cl"),
					array("caption" => "Content links", "selector" => ".page .layout-zone-content a", "type" => "cl"),
					array("caption" => "place-holder", "selector" => "", "type" => ""),
					array("caption" => "place-holder", "selector" => "", "type" => "")
				),
			),
			array(
				"group_name" => "Widgets",
				"elements" => array(
					array("caption" => "Widget header background", "selector" => ".panel .title", "type" => "bg"),
					array("caption" => "Widget header text", "selector" => ".panel .title", "type" => "cl"),
					array("caption" => "Widget content background", "selector" => ".panel .content", "type" => "bg"),
					array("caption" => "Widget content text", "selector" => ".panel .content", "type" => "cl"),
					array("caption" => "Widget content link", "selector" => ".panel a", "type" => "cl")
				),
			),
			array(
				"group_name" => "Forms",
				"elements" => array(
					array("caption" => "Form header background", "selector" => ".page h3", "type" => "bg"),
					array("caption" => "Form header text", "selector" => ".page h3", "type" => "cl"),
					array("caption" => "Form field label", "selector" => ".page .field label", "type" => "cl"),
					array("caption" => "Form field label (required)", "selector" => ".page .field.required label", "type" => "cl"),
					array("caption" => "Form field comments", "selector" => ".page .field .hint", "type" => "cl")
				),
			),
			array(
				"group_name" => "Catalog Pages",
				"elements" => array(
					array("caption" => "Bread crumbs background", "selector" => ".page-catalog .catalog-bread-crumbs", "type" => "bg"),
					array("caption" => "Bread crumbs text color", "selector" => ".page-catalog .catalog-bread-crumbs", "type" => "cl"),
					array("caption" => "Bread crumbs links", "selector" => ".page-catalog .catalog-bread-crumbs a", "type" => "cl"),
					array("caption" => "Navigation background", "selector" => ".page-catalog .catalog-navigation", "type" => "bg"),
					array("caption" => "Navigation text color", "selector" => ".page-catalog .catalog-navigation", "type" => "cl"),
					array("caption" => "Product title", "selector" => ".page .catalog-product-title a", "type" => "cl"),
					array("caption" => "Product price", "selector" => ".page .catalog-product-price .price-amount", "type" => "cl"),
					array("caption" => "Product sale price", "selector" => ".page .catalog-product-sale-price .sale-price-amount", "type" => "cl"),
					array("caption" => "Product overview", "selector" => ".page .catalog-product-overview", "type" => "cl")
				),
			),
			array(
				"group_name" => "Product Page",
				"elements" => array(
					array("caption" => "Bread crumbs background", "selector" => ".page-product .product-bread-crumbs", "type" => "bg"),
					array("caption" => "Bread crumbs text color", "selector" => ".page-product .product-bread-crumbs", "type" => "cl"),
					array("caption" => "Bread crumbs links", "selector" => ".page-product .product-bread-crumbs a", "type" => "cl"),
					array("caption" => "Product title", "selector" => ".page-product .product-title", "type" => "cl"),
					array("caption" => "Product price title", "selector" => ".page-product .product-price .price-label", "type" => "cl"),
					array("caption" => "Product price amount", "selector" => ".page-product .product-price .price-amount", "type" => "cl"),
					array("caption" => "Product sale price title", "selector" => ".page-product .product-price .sale-price-label", "type" => "cl"),
					array("caption" => "Product sale price amount", "selector" => ".page-product .product-price .sale-price-amount", "type" => "cl"),
					array("caption" => "Product overview", "selector" => ".page-product .product-overview", "type" => "cl"),
					array("caption" => "Product description", "selector" => ".page-product .product-description", "type" => "cl"),
				),
			)
		),
		
		/**
		 * Elements Groups
		 */
		"groups" => array( 
			"common" => array(
				"title" => "Site global styles",
				"elements" => array(
					"page_bg" => array(
						"parent-class" => "page",
						"parent-selector" => ".page",
						"title" => "Background", 
						"elements" => array(
							array("class" => "", "selector" => "", "title" => "Site background", "preview"=>"text", "editors"=>array("background")),
							array("class" => "container-wrap", "selector" => ".container-wrap", "title" => "Cart background", "preview"=>"text", "editors"=>array("background")),
						)
					),
					"page_header" => array(
						"parent-class" => "page",
						"parent-selector" => ".page",
						"title" => "Header", 
						"elements" => array(
							array("class" => "container-header", "selector" => ".container-header", "title" => "Site header container", "preview"=>"text", "editors"=>array("background")),
							array("class" => "layout-zone-header", "selector" => ".layout-zone-header", "title" => "Site header", "preview"=>"text", "editors"=>array("font", "background")),
							array("class" => "layout-zone-header", "selector" => ".layout-zone-header a", "title" => "Site header links", "preview"=>"link", "editors"=>array("font")),
						)
					),
					"page_nav" => array(
						"parent-class" => "page",
						"parent-selector" => ".page",
						"title" => "Navigation", 
						"elements" => array(
							array("class" => "container-navigation", "selector" => ".container-navigation", "title" => "Site navigation container", "preview"=>"text", "editors"=>array("background")),
							array("class" => "layout-zone-navigation", "selector" => ".layout-zone-navigation", "title" => "Site navigation", "preview"=>"text", "editors"=>array("font", "background")),
							array("class" => "layout-zone-navigation", "selector" => ".layout-zone-navigation a", "title" => "Site navigation links", "preview"=>"link", "editors"=>array("font")),
						)
					),
					"page_menu" => array(
						"parent-class" => "page",
						"parent-selector" => ".page",
						"title" => "Menu", 
						"elements" => array(
							array("class" => "container-menu", "selector" => ".container-menu", "title" => "Site menu container", "preview"=>"text", "editors"=>array("background")),
							array("class" => "layout-zone-menu", "selector" => ".layout-zone-menu", "title" => "Site menu", "preview"=>"text", "editors"=>array("font", "background")),
							array("class" => "layout-zone-menu", "selector" => ".layout-zone-menu a", "title" => "Site menu links", "preview"=>"link", "editors"=>array("font")),
						)
					),
					"page_content" => array(
						"parent-class" => "page",
						"parent-selector" => ".page",
						"title" => "Content", 
						"elements" => array(
							array("class" => "layout-zone-content", "selector" => ".layout-zone-content", "title" => "Site content", "preview"=>"text", "editors"=>array("font", "background")),
							array("class" => "layout-zone-content", "selector" => ".layout-zone-content a", "title" => "Site content links", "preview"=>"link", "editors"=>array("font")),
						)
					),
					"page_forms" => array(
						"parent-class" => "page",
						"parent-selector" => ".page",
						"title" => "Forms", 
						"type" => "form"
					),
					"page_footer" => array(
						"parent-class" => "page",
						"parent-selector" => ".page",
						"title" => "Footer", 
						"elements" => array(
							array("class" => "container-footer", "selector" => ".container-footer", "title" => "Site footer container", "preview"=>"text", "editors"=>array("background")),
							array("class" => "layout-zone-footer", "selector" => ".layout-zone-footer", "title" => "Site footer", "preview"=>"text", "editors"=>array("font", "background")),
							array("class" => "layout-zone-footer", "selector" => ".layout-zone-footer a", "title" => "Site footer links", "preview"=>"link", "editors"=>array("font")),
						)
					),
					/**
					array(
						"parent-class" => "page", 
						"parent-selector" => ".page", 
						"title" => "Forms", 
						"elements" => array(
							array("class" => "", "selector" => ".fieldset", "title" => "Frame and Background", "preview"=>"form", "editors"=>array("font", "align", "background")),
							array("class" => "", "selector" => ".fieldset h3", "title" => "Form header", "preview"=>"form", "editors"=>array("font", "align", "background")),
							array("class" => "", "selector" => ".fieldset .field label", "title" => "Field label", "preview"=>"form", "editors"=>array("font", "align", "background")),
							array("class" => "", "selector" => ".fieldset .required label", "title" => "Required field label", "preview"=>"form", "editors"=>array("font", "align", "background"))
						) 
					),
					**/
					"page_images" => array(
						"parent-class" => "page",
						"parent-selector" => ".page", 
						"title" => "Images", 
						"elements" => array(
							array("class" => "image-logo", "selector" => ".image-logo", "title" => "Site logo", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageLogo"),
							array("class" => "image-slogan", "selector" => ".image-slogan", "title" => "Site slogan", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageSlogan"),
							array("class" => "image-home", "selector" => ".image-home", "title" => "Home page image", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageHome"),
							array("class" => "image-hot-deal", "selector" => ".image-hot-deal", "title" => "Hot deal mark", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageHotDeal"),
							array("class" => "image-no-image-small", "selector" => ".image-no-image-small", "title" => "No image mark (small)", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageNoImageSmall"),
							array("class" => "image-no-image-big", "selector" => ".image-no-image-big", "title" => "No image mark (big)", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageNoImageBig"),
							array("class" => "image-cart", "selector" => ".image-cart", "title" => "Cart image in top bar", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageCart"),
							array("class" => "image-delete-item", "selector" => ".image-delete-item", "title" => "Delete item from a cart", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageDeleteItem"),
							array("class" => "image-cvv2", "selector" => ".image-cvv2", "title" => "CVV2 Number help image", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageCvv2"),
							array("class" => "image-check", "selector" => ".image-check", "title" => "Check numbers help image", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageCheck"),
							array("class" => "image-click-to-view", "selector" => ".image-click-to-view", "title" => "Click-to-view", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageClickToView"),
							array("class" => "image-print-version", "selector" => ".image-print-version", "title" => "Print version", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imagePrintVersion"),
							array("class" => "image-gift-certificate", "selector" => ".image-gift-certificate", "title" => "Gift certificate image", "preview"=>"image", "editors"=>array("image", "border"), "lng"=>"imageGiftCertificate")
						)
					),
					"page_buttons" => array(
						"parent-class" => "page",
						"parent-selector" => ".page", 
						"title" => "Buttons", 
						"elements" => array(
							array("class" => "button", "selector" => "input.button,input.submit,input.reset", "title" => "Common style", "preview"=>"button", "editors"=>array("font", "align", "background"), "lng"=>""),
							array("class" => "button-start-search", "selector" => "input.button-start-search", "title" => "Start search", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonStartSearch"),
							array("class" => "button-add-to-cart", "selector" => "input.button-add-to-cart", "title" => "Add to cart", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonAddToCart"),
							array("class" => "button-checkout", "selector" => "input.button-checkout", "title" => "Checkout", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonCheckout"),
							array("class" => "button-subscribe", "selector" => "input.button-subscribe", "title" => "Subscribe", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonSubscribe"),
							array("class" => "button-unsubscribe", "selector" => "input.button-unsubscribe", "title" => "Unsubscribe", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonUnsubscribe"),
							array("class" => "button-continue", "selector" => "input.button-continue", "title" => "Continue", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonContinue"),
							array("class" => "button-continue-with-order", "selector" => "input.button-continue-with-order", "title" => "Continue with order", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonContinueWithOrder"),
							array("class" => "button-login", "selector" => "input.button-login", "title" => "Login", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonLogin"),
							array("class" => "button-register", "selector" => "input.button-register", "title" => "Register", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonRegister"),
							array("class" => "button-reset", "selector" => "input.button-reset", "title" => "Reset", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonReset"),
							array("class" => "button-update-cart", "selector" => "input.button-update-cart", "title" => "Update", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonUpdateCart"),
							array("class" => "button-complete", "selector" => "input.button-complete", "title" => "Complete", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonComplete"),							
							array("class" => "button-purchase", "selector" => "input.button-purchase", "title" => "Purchase", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonPurchase"),
							array("class" => "button-save", "selector" => "input.button-save", "title" => "Save", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonSave"),
							array("class" => "button-add-address", "selector" => "input.button-add-address", "title" => "Add address", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonAddAddress"),
							array("class" => "button-reset-password", "selector" => "input.button-reset-password", "title" => "Reset password", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonResetPassword"),
							array("class" => "button-email-to-friend", "selector" => "input.button-email-to-friend", "title" => "Email to a friend", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonEmailToFriend"),
							array("class" => "button-get-shipping-rates", "selector" => "input.button-get-shipping-rates", "title" => "Get shipping rates", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonGetShippingRates"),
							array("class" => "button-get-shipping-quotes", "selector" => "input.button-get-shipping-quotes", "title" => "Get shipping quotes", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonGetShippingQuotes"),
							array("class" => "button-gift-certificate-balance", "selector" => "input.button-gift-certificate-balance", "title" => "Gift certificate balance", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonGiftCertificateBalance"),
							array("class" => "button-add-wishlist", "selector" => "input.button-add-wishlist", "title" => "Add wish list", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonAddWishlist"),
							array("class" => "button-update-wishlist", "selector" => "input.button-update-wishlist", "title" => "Update wish list", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonUpdateWishlist"),
							array("class" => "button-send-wishlist", "selector" => "input.button-send-wishlist", "title" => "Send wish list", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonSendWishlist"),
							array("class" => "button-review-send", "selector" => "input.button-review-send", "title" => "Send review", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonReviewSend"),
							array("class" => "button-review-close", "selector" => "input.button-review-close", "title" => "Close review", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonReviewClose"),
							array("class" => "button-testimonial-submit", "selector" => "input.button-testimonial-submit", "title" => "Testimonial Submit", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonTestimonialSubmit"),
							array("class" => "button-apply-promo", "selector" => ".button-apply-promo", "title" => "Apply promo", "preview"=>"button", "editors"=>array("font", "align", "background", "image"), "lng"=>"buttonApplyDiscount"),
						) 
					),
				)
			),
			"panels" => array(
				"title" => "Widgets",
				"elements" => array(
					array("parent-class" => "panel", "parent-selector" => ".panel", "title" => "Global styles for all widgets", "type" => "panel"),
					array("parent-class" => "panel-catalog-search", "parent-selector" => ".panel-catalog-search", "title" => "Search catalog", "type" => "panel"),
					array("parent-class" => "panel-catalog-bestsellers", "parent-selector" => ".panel-catalog-bestsellers", "title" => "Bestsellers", "type" => "panel"),
					array("parent-class" => "panel-catalog-categories", "parent-selector" => ".panel-catalog-categories", "title" => "Catalog categories", "type" => "panel"),
					array("parent-class" => "panel-catalog-manufacturers", "parent-selector" => ".panel-catalog-manufacturers", "title" => "Manufacturers", "type" => "panel"),
					array("parent-class" => "panel-catalog-others-bought", "parent-selector" => ".panel-catalog-others-bought", "title" => "Products others bought", "type" => "panel"),
					array("parent-class" => "panel-catalog-recent", "parent-selector" => ".panel-catalog-recent", "title" => "Recently viewed", "type" => "panel"),
					array("parent-class" => "panel-account-profile", "parent-selector" => ".panel-account-profile", "title" => "Account profile",	"type" => "panel"),
					array("parent-class" => "panel-checkout", "parent-selector" => ".panel-checkout", "title" => "Checkout", "type" => "panel"),
					array("parent-class" => "panel-checkout-cart", "parent-selector" => ".panel-checkout-cart", "title" => "View cart", "type" => "panel"),
					array("parent-class" => "panel-newsletters-subscribe", "parent-selector" => ".panel-newsletters-subscribe", "title" => "Newsletter signup", "type" => "panel")
				)
			),
			"menus" => array(
				"title" => "Menus",
				"elements" => array(
					//array("parent-class" => "layout-zone-menu", "parent-selector" => ".layout-zone-menu", "title" => "Top menu", "type" => "menu"),
					//array("parent-class" => "panel-catalog-categories", "parent-selector" => ".layout-zone-menu", "title" => "Side menu", "type" => "menu")
					array("parent-class" => "layout-zone-menu", "parent-selector" => ".layout-zone-menu", "title" => "Top menu", "type" => "menu"),
					array("parent-class" => "panel-catalog-categories", "parent-selector" => ".panel-catalog-categories", "title" => "Side menu", "type" => "menu")
				)
			),
			"catalog" => array(
				"title" => "Catalog pages",
				"elements" => array(
					array(
						"parent-class" => "page-catalog", "parent-selector" => ".page-catalog", "title" => "Common styles", "type" => "page",
						"elements" => array(
							array("class" => "catalog-bread-crumbs", "selector" => ".catalog-bread-crumbs", "title" => "Bread crumbs", "preview"=>"text", "editors"=>array("font", "align", "background")),
							array("class" => "", "selector" => ".catalog-bread-crumbs a", "title" => "Bread crumbs links", "preview"=>"link", "editors"=>array("font")),
							array("class" => "catalog-navigation", "selector" => ".catalog-navigation", "title" => "Navigation", "preview"=>"text", "editors"=>array("font", "align", "background")),
							array("class" => "catalog-page-breaks", "selector" => ".catalog-page-breaks", "title" => "Page breaks", "preview"=>"link", "editors"=>array("font", "align", "background")),
							array("class" => "", "selector" => ".catalog-page-breaks a", "title" => "Page breaks links", "preview"=>"link", "editors"=>array("font")),
							array("class" => "", "selector" => ".catalog-page-breaks .active", "title" => "Page breaks - current page", "preview"=>"text", "editors"=>array("font", "background"))
						) 
					),
					array("parent-class" => "catalog-navigation", "parent-selector" => ".catalog-navigation", "title" => "Sort/View dropdowns", "type" => "menu"),
					array("parent-class" => "page", "parent-selector" => ".page", "title" => "Common product view", "type" => "catalog-view"),

//					array("parent-class" => "catalog-view-text", "parent-selector" => ".catalog-view-text", "title" => "Text view", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-thumb1", "parent-selector" => ".catalog-view-thumb1", "title" => "Thumbnails - single column", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-thumb1-clean", "parent-selector" => ".catalog-view-thumb1-clean", "title" => "Thumbnails - single column clean", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-thumb2", "parent-selector" => ".catalog-view-thumb2", "title" => "Thumbnails - double column", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-display", "parent-selector" => ".catalog-view-display", "title" => "Two column view", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-display", "parent-selector" => ".catalog-view-display", "title" => "Two column box view", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-thumb3", "parent-selector" => ".catalog-view-thumb3", "title" => "Three column view", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-thumb3-clean", "parent-selector" => ".catalog-view-thumb3-clean", "title" => "Three column clean", "type" => "catalog-view"),
//					array("parent-class" => "catalog-view-flexible", "parent-selector" => ".catalog-view-flexible", "title" => "Flexible", "type" => "catalog-view")
				)
			),
			"product" => array(
				"title" => "Product page",
				"elements" => array(
					array("parent-class" => "page-product",	"parent-selector" => ".page-product", "title" => "Common", "type" => "product-view"),
					//array("parent-class" => "product-view-style1", "parent-selector" => "div.product-view-style1", "title" => "Style 1", "type" => "product-view"),
					//array("parent-class" => "product-view-style2", "parent-selector" => "div.product-view-style2", "title" => "Style 2", "type" => "product-view"),
					//array("parent-class" => "product-view-style3", "parent-selector" => "div.product-view-style3", "title" => "Style 2", "type" => "product-view"),
					//array("parent-class" => "product-view-style4", "parent-selector" => "div.product-view-style4", "title" => "Style 4", "type" => "product-view")
				)
			),
			
			"account" => array(
				"title" => "Account pages",
				"elements" => array(
					array("parent-class" => "page-account", "parent-selector" => ".page-account", "title" => "Account home page", "type" => "page"),
					array("parent-class" => "page-address-book", "parent-selector" => ".page-address-book", "title" => "Address book", "type" => "page"),
					array("parent-class" => "page-address-edit", "parent-selector" => ".page-address-edit", "title" => "Address edit", "type" => "page"),
					array("parent-class" => "page-auth-error", "parent-selector" => ".page-auth-error", "title" => "Auth error", "type" => "page"),
					array("parent-class" => "page-blocked", "parent-selector" => ".page-blocked", "title" => "Account blocked page", "type" => "page"),
					array("parent-class" => "page-login", "parent-selector" => ".page-login", "title" => "Login page", "type" => "page"),
					array("parent-class" => "page-orders", "parent-selector" => ".page-orders", "title" => "Orders page", "type" => "page"),
					array("parent-class" => "page-order", "parent-selector" => ".page-order", "title" => "Order page", "type" => "page"),
					array("parent-class" => "page-password-reset", "parent-selector" => ".page-password-reset", "title" => "Password reset", "type" => "page"),
					array("parent-class" => "page-profile", "parent-selector" => ".page-profile", "title" => "Profile", "type" => "page"),
					array("parent-class" => "page-signup", "parent-selector" => ".page-signup", "title" => "Signup", "type" => "page")
				)
			),
			
			"checkout" => array(
				"title" => "Checkout pages",
				"elements" => array(
					array("parent-class" => "page-billing-and-shipping", "parent-selector" => ".page-billing-and-shipping", "title" => "Billing and Shipping", "type" => "page"),
					array("parent-class" => "page-cart", "parent-selector" => ".page-cart", "title" => "Cart page", "type" => "page"),
					array("parent-class" => "page-completed", "parent-selector" => ".page-completed", "title" => "Completed", "type" => "page"),
					array("parent-class" => "page-invoice", "parent-selector" => ".page-invoice", "title" => "Invoice", "type" => "page"),
					array("parent-class" => "page-opc", "parent-selector" => ".page-opc", "title" => "One page checkout", "type" => "page"),
					array("parent-class" => "page-page-payment-validation", "parent-selector" => ".page-payment-validation", "title" => "Payment validation", "type" => "page"),
					array("parent-class" => "page-shipping-method", "parent-selector" => ".page-shipping-method", "title" => "Shipping method", "type" => "page"),
					array("parent-class" => "page-shipping-quote", "parent-selector" => ".page-shipping-quote", "title" => "Shipping quote", "type" => "page"),
				)
			),
			
			"newsletters" => array(
				"title" => "Newsletters pages",
				"elements" => array(
					array("parent-class" => "page-subscribe", "parent-selector" => ".page-subscribe", "title" => "Subscribe", "type" => "page"),
					array("parent-class" => "page-unsubscribe", "parent-selector" => ".page-unsubscribe", "title" => "Unsubscribe", "type" => "page")
				)
			),
			
			"site" => array(
				"title" => "Site pages",
				"elements" => array(
					array("parent-class" => "page-404", "parent-selector" => ".page-404", "title" => "Not found page", "type" => "page"),
					array("parent-class" => "page-home", "parent-selector" => ".page-home", "title" => "Home page", "type" => "page"),
					array("parent-class" => "page-international", "parent-selector" => ".page-international", "title" => "International settings", "type" => "page"),
					array("parent-class" => "page-site-map", "parent-selector" => ".page-site-map", "title" => "Site map", "type" => "page"),
					array("parent-class" => "page-text", "parent-selector" => ".page-text", "title" => "Text pages", "type" => "page"),
					array("parent-class" => "page-testimonials", "parent-selector" => ".page-testimonials", "title" => "Testimonials", "type" => "page")
				)
			)
		)
	);
	
$(document).ready(function(){
	skinInit();
});

/**
 * Init user site
 */
function skinInit()
{
	$('#navigation div.navigation-top').corner('tl tr 15px');
	$('.tabbed-navigation ul.tabs li').corner('tl tr 8px');
	$('.mobile-nav-toggle').click(function() {
		var panel = $('.mobile-navigation-panel');
		if (panel.is(':visible')) {
			panel.hide();
		} else {
			panel.show();
		}
	});
	$('.mobile-nav-subcategories-toggle').click(function() {
		var panel = $('.mobile-nav-subcategories-panel');
		if (panel.is(':visible')) {
			panel.hide();
		} else {
			panel.show();
		}
	});
	$('.product-secondary-images img').click(function() {
		var imgSrc = $(this).attr('data-img-src');
		$('.product-image img').prop('src', imgSrc);
	});
}
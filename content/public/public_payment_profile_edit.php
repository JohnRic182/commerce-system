<?php
/**
 * Show users payment_profiles
 */

	if (!defined("ENV")) exit();

	/** @var Framework_Request $request */
	$request = Framework_Request::createFromGlobals();

	$controller = new PaymentProfiles_Front_Controller_PaymentProfile($request, $db);

	$controller->editAction();

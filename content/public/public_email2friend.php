<?php
/**
 * Send email to friend
 */

	if (!defined('ENV')) exit();

	$pid = isset($_REQUEST['pid']) ? intval($_REQUEST['pid']) : 0;

	$result = array('status' => 0, 'captcha_error' => 0);

	if (Nonce::verify(isset($_POST['nonce']) ? $_POST['nonce'] : '', 'email_to_friend_nonce') &&
		($productData = $db->selectOne('SELECT pid, title FROM ' . DB_PREFIX . 'products WHERE pid=' . $pid . ' AND is_visible="Yes"'))
	)
	{
		$captchaError = false;

		if ($settings['captchaMethod'] != 'None')
		{
			$recaptcha = new \ReCaptcha\ReCaptcha($settings['captchaReCaptchaPrivateKey']);
			$response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER["REMOTE_ADDR"]);
			if (!$response->isSuccess())
			{
				$captchaErrors = $response->getErrorCodes();
				foreach ($captchaErrors as $error)
				{
					$user->setError("Captcha Error: $error");
				}
				$captchaError = true;
			}
			$result['captcha_error'] = 1;
		}

		if (!$captchaError)
		{
			view()->assign('is_product', 'yes');
			view()->assign('product_pid', $pid);
			view()->assign('product_name', $productData['title']);

			$action = array_key_exists('action', $_REQUEST) ? trim($_REQUEST['action']) : '';
			if ($action == 'send')
			{
				Notifications::emailToFriend($db, $_REQUEST['yname'], $_REQUEST['yemail'], $_REQUEST['fname'], $_REQUEST['femail'], $productData['pid'], $msg);

				$result['status'] = 1;
			}
		}
	}

	$result['nonce'] = Nonce::create('email_to_friend_nonce');

	header('Content-Type: application/json');
	echo json_encode($result);
	exit;
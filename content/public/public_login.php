<?php
/**
 * User login page
 */

	if (!defined("ENV")) exit();
	
	//no reasons to show login page when there are no accounts
	if ($settings["AllowCreateAccount"] == "No")
	{
		header("Location: ".$url_http."p=cart");
		die();
	}
	
	/**
	 * Check is fb logged in, - force logout
	 */
	if ($settings["facebookLogin"] == "Yes")
	{
		require_once('content/vendors/facebook/facebook.php');
		
		try
		{
			$facebook = new Facebook(array(
				'appId'  => $settings["facebookAppId"],
				'secret' => $settings["facebookAppSecret"],
				'cookir' => true
			));
			
			if ($facebook->getUser() && $a = $facebook->api("/me"))
			{
				$facebookLogoutUrl = $facebook->getLogoutUrl(array("redirect_url" => $url_https."p=login"));
				header("Location: ".$facebookLogoutUrl);
				die();
			}
		}
		catch (Exception $e)
		{
		}
	}
	
	if ($user->auth_ok && !$user->express)
	{
		header("Location: ".$url_https."p=login&ua=".USER_LOGOUT);
		die();
	}
	
	//assign post / cookie vars
	if (isset($_POST["login"]))
	{
		view()->assign("login", $_POST["login"]);
		view()->assign("remember_me", isset($_POST["remember_me"]));
	}
	elseif (isset($_COOKIE[$settings["SecurityCookiesPrefix"]."LoginUserName"]))
	{
		view()->assign("login", $_COOKIE[$settings["SecurityCookiesPrefix"]."LoginUserName"]);
		view()->assign("remember_me", true);
	}
	
	//assign security vars
	view()->assign('account_blocked', $user->blocked);
	view()->assign('auth_ok', $user->auth_ok ? 'yes' : 'no');
	view()->assign('auth_error', $ua == USER_LOGIN);
	
	//assogn service data
	view()->assign("cart_items_count", $order->itemsCount);
	
	//assign template
	view()->assign("body", "templates/pages/account/login.html");

	$meta_title = trim($settings['login_page_title']) != '' ? $settings['login_page_title'] : $meta_title;
	$meta_description = trim($settings['login_meta_description']) != '' ? $settings['login_meta_description'] : $meta_description;
<?php
/**
 * Show site map
 */

	if (!defined("ENV")) exit();
	view()->assign("body", "templates/pages/site/map.html");

	$meta_title = trim($settings['sitemap_page_title']) != '' ? $settings['sitemap_page_title'] : $meta_title;
	$meta_description = trim($settings['sitemap_meta_description']) != '' ? $settings['sitemap_meta_description'] : $meta_description;
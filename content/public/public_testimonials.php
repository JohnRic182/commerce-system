<?php
/**
 * Testimonials page
 */ 

if (!defined("ENV")) exit();

if ($settings["testimonials_Enabled"] == "Yes" || 1)
{
	require_once 'content/admin/plugins/testimonials/plugin.testimonials.php';

	$objTestimonials = new plugin_testimonials();//$cartPlugins->getPluginObject('testimonials');
	$objTestimonials->setupPlugin($db);

	if (isset($_REQUEST["submitTestimonial"]))
	{
		// save testimonial
		$error = false;
		
		$testimonial = isset($_POST["testimonial"]) ? (is_array($_POST["testimonial"]) ? $_POST["testimonial"] : array()) : array();
		
		if (!isset($testimonial["name"]) || !validate($testimonial["name"], VALIDATE_NAME)) $error = true;
		if ($settings['testimonials_Use_Rating'] == 'Yes' && (!isset($testimonial["rating"]) || !validate($testimonial["rating"], VALIDATE_INT))) $error = true;
		if (!isset($testimonial["testimonial"]) || !validate($testimonial["testimonial"], VALIDATE_NOT_EMPTY)) $error = true;
		
		if ($settings['captchaMethod'] != 'None' && $settings['testimonials_Use_Captcha'] == 'Yes')
		{
			$recaptcha = new \ReCaptcha\ReCaptcha($settings['captchaReCaptchaPrivateKey']);
			$response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER["REMOTE_ADDR"]);
			if (!$response->isSuccess())
			{
				$captchaErrors = $response->getErrorCodes();
				foreach ($captchaErrors as $error)
				{
					$user->setError("Captcha Error: $error");
				}
				$error = true;
			}
		}
	
		if (!$error)
		{
			if ($settings['testimonials_Use_Rating'] != 'Yes') $testimonial["rate"] = 10;
			$testimonial['status'] = 'pending';
			$objTestimonials->saveTestimonial($testimonial);
			view()->assign('testimonialSubmited', true);
		}
		else
		{
			view()->assign('testimonialError', true);
			view()->assign('testimonialToEdit', $testimonial);
		}
	}

	// view testimonials
	$perPage = intval($settings['testimonials_Per_Page']);
	if (isset($_REQUEST["start"]))
	{
		$start = intval($_REQUEST["start"]);
	}
	else
	{
		$start = 0;
	}
	
	$testimonials = $objTestimonials->getTestimonials($start,$perPage,'approved');
	view()->assign('testimonials', $testimonials);

	if ($objTestimonials->totalTestimonials > $perPage+$start)
	{
		view()->assign('nextLink','index.php?p=testimonials&start='.($start+$perPage));
	}
	
	if ($start != 0)
	{
		view()->assign('prevLink','index.php?p=testimonials&start='.($start-$perPage));
	}
	
	if ($settings['testimonials_Use_Rating'] == 'Yes')
	{
		$rating = $objTestimonials->getRating();
		view()->assign('rating',$rating);
	}

	view()->assign('starFull', $objTestimonials->getImage('full'));
	view()->assign('starHalf', $objTestimonials->getImage('half'));
	view()->assign('starEmpty', $objTestimonials->getImage('empty'));
	
	if ($settings['captchaMethod'] != 'None' && $settings['testimonials_Use_Captcha'] == 'Yes')
	{
		view()->assign('captcha', true);
		view()->assign('site_key', $settings['captchaReCaptchaPublicKey']);
	}
	
	view()->assign('body', 'templates/pages/site/testimonials.html');

	$meta_title = trim($settings['testimonials_page_title']) != '' ? $settings['testimonials_page_title'] : $meta_title;
	$meta_description = trim($settings['testimonials_meta_description']) != '' ? $settings['testimonials_meta_description'] : $meta_description;
}
else
{
	header("Location: ".$settings["GlobalHttpUrl"]."/index.php?p=404");
	_session_write_close();
	die();
}

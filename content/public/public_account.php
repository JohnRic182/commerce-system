<?php
/**
 * Show users address book
 */

if (!defined("ENV")) exit();

if ($user->auth_ok)
{
	view()->assign("body", "templates/pages/account/account.html");
}
else
{
	view()->assign("body", "templates/pages/account/auth-error.html");
}
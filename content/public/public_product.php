<?php
/**
 * Show product page
 */

	if (!defined("ENV")) exit();
	
	$id = !empty($_REQUEST["id"]) ? intval($_REQUEST["id"]) : "0";
	
	$parent = 0;
	
	$oa_quantity = isset($_REQUEST["oa_quantity"]) ? intval($_REQUEST["oa_quantity"]) : 0;
	
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
		
	$products = new ShoppingCartProducts($db, $settings);
	$products->parent = $parent;

	$product = $products->getProductById($id, $userLevel, $parent);

	// redirect to 404 if digital product is disabled and product tyep id digital
	if (!$settings['EnableDigitalDownload'] && $product['product_type'] == Model_Product::DIGITAL)
	{
		header("Location: ".$url_dynamic."p=404");
	}
		
	if ($product)
	{
		$parent = intval($product["cid"]);

		$products->cleanupSubProducts($product);

		if ($product['enable_recurring_billing'] && is_array($product['recurring_billing_data']))
		{
			$productRecurringBillingData = new RecurringBilling_Model_ProductRecurringBillingData($product['recurring_billing_data']);

			if ($productRecurringBillingData->getTrialEnabled())
			{
				$product['recurring_billing_data']['trial_description'] = View_ProductViewModel::getRecurringTrialDescription($productRecurringBillingData, 1, $msg);
			}

			$product['recurring_billing_data']['billing_description'] = View_ProductViewModel::getRecurringBillingDescription($productRecurringBillingData, 1, $msg);
		}

		if ($settings['PikflyEnabled'] == 'Yes' && $product['same_day_delivery'] == 1)
		{
			$timeHelper = new Pikfly_TimeHelper($db, $settings);
			$product['can_be_delivered_today'] = $timeHelper->canBeDeliveredToday() ? 1 : 0;
		}

		if ($settings['twitterAccount'] && $settings['twitterTweetButtonProduct'] == 'Yes')
		{
			view()->assign(
				'twitterCards',
				array(
					'card' => 'summary_large_image', // Twitter required this
					'site' => '@' . UrlUtils::lastSegment($settings['twitterAccount'], PHP_URL_PATH), // Twitter required this
					'title' => $product['title'], // Twitter required this
					'description' => substr(trim(strip_tags($product['description'])), 0, 100), // Twitter required this
					'image' => $settings["GlobalHttpUrl"] . '/' . $product['image'], // Twitter required this
				)
			);
		}

		view()->assign("product", $product);

		view()->assign("canonical_url", UrlUtils::getProductUrl($product["url_custom"] == "" ? $product["url_default"] : $product["url_custom"], $product['pid']));

		view()->assign("product_attributes_json", json_encode(isset($product["attributes"]) ? array_values($product["attributes"]) : false));
		view()->assign("product_inventory_hashes_json", json_encode(isset($product["inventory_hashes"]) ? $product["inventory_hashes"] : false));
		
		view()->assign("product_families", $products->getProductFamilies($id, $userLevel));
		view()->assign("product_siblings", $products->getProductSiblings($id, $userLevel, $parent, $settings["SiblingProductsCount"]));
		view()->assign("free_products", $products->getFreeProducts($id, $userLevel));

		// product reviews smarty vars
		$reviews = new Ddm_ProductReviews($db, $settings);

		$product_rating = $reviews->getProductRating($product['pid']);
		
		view()->assign('reviews', $reviews->getReviews($product['pid']));
		view()->assign('product_rating', round($product_rating['rating'], 1));
		view()->assign('product_rating_total', $product_rating['total_reviews']);
		view()->assign('has_written_review', 0);//$reviews->hasWrittenReview($product['pid'], $user->id));

		if (trim($product['meta_title']) != '')
		{
			$meta_title = $product['meta_title'];
		}
		else if ($settings['SearchAutoGenerate'] == 'YES')
		{
			$meta_title = $product['title'].': '.$meta_title;
		}

		if (trim($product['meta_description']) != '')
		{
			$meta_description = $product['meta_description'];
		}
		else if ($settings['SearchAutoGenerate'] == 'YES' && trim($product['overview']) != '')
		{
			$meta_description .= ($meta_description != '' ? ' ' : '') .
				str_replace(array("\r", "\n", '"', "'"), ' ', strip_tags($product["overview"]));
		}

		$categoriesTree = $categoryRepository->getCachedTree(array('cid', 'parent', 'is_visible', 'key_name', 'name'), array('catalog_hide_empty_categories' => $settings['CatalogHideEmptyCategories']));

		view()->assign("parent_id", isset($parent_id) ? $parent_id : "");
		view()->assign("oa_quantity", $oa_quantity);

		view()->assign("page_nav", $categoryRepository->getParsedPath($categoriesTree, $parent));
		view()->assign("ProductViewFile", "templates/pages/product/views/".strtolower($settings["CatalogProductPage"]).".html");
		view()->assign("body", "templates/pages/product/product.html");

		view()->assign("item_link", $product["url"]);
		view()->assign("weight_unit", $settings["LocalizationWeightUnits"]);
		view()->assign("separating_symbol", $settings["LocalizationCurrencySeparatingSymbol"]);

		view()->assign("zoom_option", isset($settings["ZoomOption"]) ? $settings["ZoomOption"] : 'imagelayover');
		view()->assign("zoom_option_secondary", isset($settings["ZoomOptionSecondary"]) ? $settings["ZoomOptionSecondary"] : 'imagelayover');

		if ($settings['CatalogEmailToFriend'] != 'Disabled')
		{
			view()->assign('email_to_friend_nonce', Nonce::create('email_to_friend'));

			if ($settings['captchaMethod'] != 'None')
			{
				view()->assign('captcha', true);
				view()->assign('site_key', $settings['captchaReCaptchaPublicKey']);
			}
		}

		$recentItems->add($product["url"], $product["title"]);
	}
	else
	{
		header("Location: ".$settings["GlobalHttpUrl"]."/index.php?p=404");
		_session_write_close();
		die();
	}
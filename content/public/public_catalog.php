<?php
/**
 * Show products catalog
 */

	if (!defined("ENV")) exit();

	$parent = isset($_REQUEST["parent"]) ? intval($_REQUEST["parent"]) : 0;
	$pg = isset($_REQUEST["pg"]) ? intval($_REQUEST["pg"]): 0 ;
	$mode = isset($_REQUEST["mode"]) ? trim($_REQUEST["mode"]) : "catalog";
	$mid = isset($_REQUEST["mid"]) ? intval($_REQUEST["mid"]) : 0;
	
	if ($mode == "catalog" && $settings["USE_MOD_REWRITE"] == "YES" && isset($_REQUEST["catalog_code"]))
	{
		$parts = explode("-", $_REQUEST["catalog_code"]);
		if (($count = count($parts))>=3)
		{
			$parent = $_REQUEST["parent"] = intval($parts[$count - 2]);
			$pg = $_REQUEST["pg"] = intval($parts[$count - 1]);
		}
		else
		{
			$parent = $_REQUEST["parent"] = "0";
			$pg = $_REQUEST["pg"] = "0";
		}
	}
	else
	{
		$parent = $_REQUEST["parent"] = !empty($_REQUEST["parent"]) ? intval($_REQUEST["parent"]) : "0";
		$pg = $_REQUEST["pg"] = !empty($_REQUEST["pg"]) ? intval($_REQUEST["pg"]) : "0";
	}
	
	//	prepare params
	
	/**
	 * Set user settings cookies - SORTING
	 */
	$cookies = initCookies();
	
	$CatalogSortCookie = $settings["SecurityCookiesPrefix"]."CatalogSortBy";

	if (isset($_REQUEST["CatalogSetSortBy"]))
	{
		$cookies->set($CatalogSortCookie, $_REQUEST["CatalogSetSortBy"]);
		$CatalogSortBy = $_REQUEST["CatalogSetSortBy"];
	}
	else
	{
		$CatalogSortBy = array_key_exists($CatalogSortCookie, $_COOKIE) ? $_COOKIE[$CatalogSortCookie] : $settings["CatalogDefaultSortOrder"];
	}

	$CatalogSortBy = in_array($CatalogSortBy, array("priority", "date", "name", "id", "price", "price_desc")) ? $CatalogSortBy : $settings["CatalogDefaultSortOrder"];
		
	/**
	 * PRICES RANGES
	 */
	if ($settings["CatalogUsePriceRanges"] == "1")
	{
		$price_ranges = unserialize(base64_decode($settings["CatalogPriceRanges"]));
		view()->assign("catalog_price_ranges", $price_ranges);
		$CatalogPriceRangeCookie = $settings["SecurityCookiesPrefix"]."CatalogPriceRange";

		if (isset($_REQUEST["CatalogSetPriceRange"]))
		{
			$cookies->set($CatalogPriceRangeCookie, $_REQUEST["CatalogSetPriceRange"]);
			$CatalogPriceRange = $_REQUEST["CatalogSetPriceRange"];
		}
		else
		{
			$CatalogPriceRange = array_key_exists($CatalogPriceRangeCookie, $_COOKIE)?$_COOKIE[$CatalogPriceRangeCookie]:0;
		}
		
		$CatalogPriceRange = (($CatalogPriceRange >= 1) && ($CatalogPriceRange <=10)) ? intval($CatalogPriceRange) : 0;

		if (array_key_exists($CatalogPriceRange, $price_ranges) && ($price_ranges[$CatalogPriceRange]["min"] == "" || $price_ranges[$CatalogPriceRange]["max"] == ""))
		{
			$CatalogPriceRange = 0;
		}
	}
	else
	{
		$CatalogPriceRange = 0;	
	}
	
	$parent = isset($_REQUEST["parent"]) ? $_REQUEST["parent"] : "0";
	$search_str = isset($_REQUEST["search_str"]) ? trim(preg_replace('/[\s,]+/', ' ', $_REQUEST["search_str"])) : "";
	$search_in = isset($_REQUEST["search_in"]) ? trim($_REQUEST["search_in"]) : "";
	$search_in = in_array($search_in, array("all", "name", "id")) ? $search_in : "all";
		
	$products = new ShoppingCartProducts($db, $settings);
	$products->parent = $parent;
		
	$params = array('frontEndCall' => true);

	
	/**
	 * Page break params
	 */
	$params["CatalogItemsOnPage"] = ($settings["CatalogItemsOnPage"])?$settings["CatalogItemsOnPage"]:10;
	$CatalogPageSizeCookie = $settings["SecurityCookiesPrefix"]."CatalogPageSize";

	if (isset($_REQUEST['pagesize']))
	{
		$params["CatalogItemsOnPage"] = intval($_REQUEST['pagesize']);
		if ($params["CatalogItemsOnPage"] < 0)
			$params["CatalogItemsOnPage"] = -1;
		$cookies->set($CatalogPageSizeCookie, $params["CatalogItemsOnPage"]);
	}
	else
	{
		if (array_key_exists($CatalogPageSizeCookie, $_COOKIE))
		{
			$params["CatalogItemsOnPage"] = intval($_COOKIE[$CatalogPageSizeCookie]);

			if ($params["CatalogItemsOnPage"] < 0)
			{
				$params["CatalogItemsOnPage"] = -1;
			}
		}
	}

	view()->assign("pagesize", $params["CatalogItemsOnPage"]);
	
	$params["Page"] = isset($_REQUEST["pg"]) ? intval($_REQUEST["pg"]) : 1;
	
	/**
	 * Sort
	 */
	$params["CatalogSortBy"] = $CatalogSortBy;

	/**
	 * Price range
	 */
	if ($settings["CatalogUsePriceRanges"] == "1" && $CatalogPriceRange > 0)
	{
		$params["CatalogPriceRangeMin"] = $price_ranges[$CatalogPriceRange]["min"];
		$params["CatalogPriceRangeMax"] = $price_ranges[$CatalogPriceRange]["max"];
	
		view()->assign("CatalogPriceRangeMin", $price_ranges[$CatalogPriceRange]["min"]);
		view()->assign("CatalogPriceRangeMax", $price_ranges[$CatalogPriceRange]["max"]);
	}
	
	$page_not_found = false;
		
	$result = array();
		
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
	
	switch($mode)
	{
		case "search" : 
		{
			$params["mode"] = "search";
			$params["search_in"] = $search_in;

			if (!parseSearchKeywords($search_str, $params["keywords"]))
			{
				view()->assign("is_search_error", "Yes");
			}
			else
			{
				view()->assign("products", $products->getItemsCatalog($params, $userLevel));
			}
			
			$page_title = $msg['catalog']['search_results'];
			$page_nav = array(
				array(
					"href"=>$url_http.'p=catalog&amp;mode=search&amp;search_in='.$search_in.'&amp;search_str='.urlencode($search_str), 
					"caption"=>$msg['catalog']['search_results_for'].' '.htmlspecialchars($search_str)
				)
			);

			$meta_title = trim($settings['search_page_title']) != '' ? $settings['search_page_title'] : $meta_title;
			$meta_description = trim($settings['search_meta_description']) != '' ? $settings['search_meta_description'] : $meta_description;

			break;
		}
		case "hot" :
		{
			$params["mode"] = "hot";
			view()->assign("products", $products->getItemsCatalog($params, $userLevel));
			$page_title = "Hot Deals";
			$page_nav = array(
				array(
					"href"=>$url_http.'p=catalog&amp;mode=hot',
					"caption"=>"Hot Deals"
				)
			);
			break;
		}
		case "new" :
		{
			$params["mode"] = "new";
			$params["CatalogNewestDays"] = $settings["CatalogNewestDays"];
			view()->assign("products", $products->getItemsCatalog($params, $userLevel));
			$page_title = "Newest Updates";
			$page_nav = array(
				array(
					"href"=>$url_http.'p=catalog&amp;mode=new',
					"caption"=>"Newest Updates"
				)
			);
			
			break;
		}
		case "manufacturer" :
		{
			if ($settings["CatalogManufacturers"] == "YES")
			{
				if ($mnf = $manufacturers->getManufacturerById($mid, true))
				{
					$params["mode"] = "manufacturer";
					$params["manufacturer_id"] = $mid;
					view()->assign("manufacturer", $mnf);
					view()->assign("products", $products->getItemsCatalog($params, $userLevel));
					$page_title = $mnf["manufacturer_name"];
					$page_nav = array(
						array(
							"href"=>$url_http.'p=catalog&amp;mode=manufacturer&amp;mid='.$mid,
							"caption"=>htmlspecialchars($mnf["manufacturer_name"])
						)
					);

					//new changes
					$meta_title = trim($mnf["meta_title"]) == "" ? $meta_title : $mnf["meta_title"];
					$meta_description = trim($mnf["meta_description"]) == "" ? $meta_description : $mnf["meta_description"];
				}
				else
				{
					$page_not_found = true;
				}
			}
			break;
		}
		default:
		case "catalog" :
		{
			$mode = "catalog";
			$categoryRepository = new DataAccess_CategoryRepository($db);
			$category = $categoryRepository->getById($parent);

			// check the request category id
			$searchParams['cid'] = $category['cid'];

			if ($category && $category['is_visible'] == 'Yes')
			{
				// templates back compatibility
				$category['img'] = $category['image'];
				//$category['list_subcats'] = 'Yes';
				$category['show_subcategory_image'] = $category['list_subcats_images'];

				$currentCategoryUrl = UrlUtils::getCatalogUrl($category['url_custom'] == '' ? $category['url_default'] : $category['url_custom'], $category['cid']);

				view()->assign('canonical_url', $currentCategoryUrl);
				view()->assign('current_category_url', $currentCategoryUrl);

				$categoriesTree = $categoryRepository->getCachedTree(array('cid', 'parent', 'is_visible', 'key_name', 'name', 'is_stealth'), array('catalog_hide_empty_categories' => $settings['CatalogHideEmptyCategories']));
				$category['subcategories'] = false;

				if (isset($categoriesTree[$parent]))
				{
					$category['level'] = $categoriesTree[$parent]['level'];

					if (isset($categoriesTree[$parent]['children']) && is_array($categoriesTree[$parent]['children']) && count($categoriesTree[$parent]['children']))
					{
						$categoryChildren = array();

						foreach ($categoriesTree[$parent]['children'] as $subCategoryKey => $subCategory)
						{
							if ($subCategory['is_visible'] == 'Yes')
							{
								$subCategory['image'] = $categoryRepository->getCategoryImage($subCategory['cid']);
								$subCategory['thumb'] = $categoryRepository->getCategoryThumb($subCategory['cid']);
								$categoryChildren[$subCategoryKey] = $subCategory;
							}
						}

						if (count($categoryChildren) > 0)
						{
							$category['subcategories'] = $categoryChildren;
						}
					}
				}

				view()->assign('category', $category);

				/**
				 * Get products
				 */
				$params['parent'] = $parent;
				$params['nested'] = $settings['CatalogShowNested'] == 'YES';
				$params['mode'] = 'catalog';
				$params['categories'] = $settings['CatalogShowNested'] == 'YES' ? $categoryRepository->getNestedCategories($categoriesTree, $parent, false) : null;

				$filtersQueryString = '';
				if ($settings['CatalogUseProductFilters'])
				{
					$filters = (isset($_GET['filters']) && !empty($_GET['filters'])) ? $_GET['filters'] : array();
					$allFilters = array();
					$narrowFilters = array();
					$counter = 1;
					foreach ($filters as $key => $filter)
					{
						$amp = ($counter > 1) ? '&' : '';
						$narrowFilters[$key] = explode(',', $filter);
						$filtersQueryString.= $amp . 'filters[' . $key . ']=' . $filter;
						$allFilters = array_merge($allFilters, $narrowFilters[$key]);
						$counter++;
					}

					$allFilters = array_unique($allFilters);

					$categoriesFeaturesGroupsRepository = new DataAccess_CategoryProductsFeaturesGroupsRepository($db);
					$categoriesFeaturesGroups = $categoriesFeaturesGroupsRepository->getList(null, null, 'cfg.product_feature_group_id, cfg.priority', $searchParams);
					$productsFeaturesValueRepository = new DataAccess_ProductsFeaturesValuesRepository($db);
					$productsFeaturesValues = $productsFeaturesValueRepository->getProductsFeaturesValues($categoriesFeaturesGroups, $narrowFilters);

					view()->assign('productsFeaturesValues', $productsFeaturesValues);

					if (count($allFilters) > 0)
					{
						$productsFeaturesGroupsProductsRelationsRepository = new DataAccess_ProductsFeaturesGroupsProductsRelationsRepository($db);
						$params['pids'] = $productsFeaturesGroupsProductsRelationsRepository->getProductsByFiltersAndCategory($allFilters, $narrowFilters, $searchParams['cid']);
						$product_list = $params['pids'] ? $products->getItemsCatalog($params, $userLevel) : false;
					}
					else
					{
						$product_list = $products->getItemsCatalog($params, $userLevel);
					}
				}
				else
				{
					$product_list = $products->getItemsCatalog($params, $userLevel);
				}

				if (!$product_list && $params['Page'] > 1)
				{
					$page_not_found = true;
				}
				else
				{
					view()->assign('products', $product_list);

					$page_title = trim($category['category_header']) == '' ? $category['name'] : $category['category_header'];

					$page_nav = $categoryRepository->getParsedPath($categoriesTree, $parent);

					if (trim($category['meta_title']) != '')
					{
						$meta_title = $category['meta_title'];
					}
					else if ($settings['SearchAutoGenerate'] == 'YES')
					{
						$meta_title = $category['name'] . ': ' . $meta_title;
					}

					if (trim($category['meta_description']) != '')
					{
						$meta_description = $category['meta_description'];
					}
					else if ($settings['SearchAutoGenerate'] == 'YES' && trim($category['description']) != '')
					{
						$meta_description .= ($meta_description != '' ? ' ' : '') .
							str_replace(array("\r", "\n", '"', "'"), ' ', strip_tags($category["description"]));
					}
				}
			}
			else
			{
				$page_not_found = true;
			}

			break;
		}
	}

	if ($page_not_found)
	{
		require 'content/public/public_404.php';
	}
	else
	{
		//params
		//////////////////////////
		//Catalog View Settings
		// Commented the "Clean" versions of the catalog style
		$catalog_views = array();
		if ($settings["CatalogViewText"] == "YES") $catalog_views["Text"] = $msg['catalog']['view_text'];
		if ($settings["CatalogViewThumb1"] == "YES") $catalog_views["Thumb1"] = $msg['catalog']['view_single_row'];
		// if ($settings["CatalogViewThumb1Clean"] == "YES") $catalog_views["Thumb1Clean"] = $msg['catalog']['view_single_row_clean'];
		if ($settings["CatalogViewThumb2"] == "YES") $catalog_views["Thumb2"] = $msg['catalog']['view_double_row'];
		if ($settings["CatalogViewDisplay"] == "YES") $catalog_views["Display"] = $msg['catalog']['view_two_row'];
		if ($settings["CatalogViewDisplayBox"] == "YES") $catalog_views["DisplayBox"] = $msg['catalog']['view_two_row_box'];
		if ($settings["CatalogViewThumb3"] == "YES") $catalog_views["Thumb3"] = $msg['catalog']['view_three_row'];
		if ($settings["CatalogViewFlexible"] == "YES") $catalog_views["Flexible"] = $msg['catalog']['view_flexible'];
		// if ($settings["CatalogViewThumb3Clean"] == "YES") $catalog_views["Thumb3Clean"] = $msg['catalog']['view_three_row_clean'];

		$CatalogViewCookie = $settings["SecurityCookiesPrefix"]."CatalogView";
		if (isset($_REQUEST["CatalogSetView"]))
		{
			$cookies->set($CatalogViewCookie, $_REQUEST["CatalogSetView"]);
			$CatalogView = $_REQUEST["CatalogSetView"];
		}
		else
		{
			$CatalogView = array_key_exists($CatalogViewCookie, $_COOKIE)?$_COOKIE[$CatalogViewCookie]:($settings["CatalogDefaultView"]);
		}

		$CatalogView = array_key_exists($CatalogView, $catalog_views)? $CatalogView : $settings["CatalogDefaultView"];
		
		view()->assign("CatalogView", $CatalogView);
		view()->assign("catalog_views", $catalog_views);
		view()->assign("CatalogViewFile", $f = "templates/pages/catalog/views/".strtolower($CatalogView).".html");
		//echo $f;
		view()->assign("CatalogSortBy", $CatalogSortBy);
		view()->assign("CatalogPriceRange", $CatalogPriceRange);

		view()->assign("items_count", $products->items_count);
		view()->assign("pages_count", $products->pages_count);

		view()->assign("page_title", $page_title);
		view()->assign("page_nav", $page_nav);

		view()->assign("catalog_mode", $mode);

		//for catalog
		view()->assign("parent", $parent);

		//for search
		view()->assign("search_str", $search_str);
		view()->assign("search_in", $search_in);

		$url_prefix = $url_http."p=catalog&amp;mode=".$mode."&amp;parent=".$parent."&amp;mid=".$mid."&amp;search_in=".$search_in."&amp;search_str=".(urlencode($search_str));
		if ($mode == "catalog" && $settings["USE_MOD_REWRITE"] == "YES")
		{
			$url_prefix = UrlUtils::getCatalogUrl($category["url_custom"] == "" ? $category["url_default"] : $category["url_custom"], $parent, "%n%", false, $filtersQueryString);
		}
		
		view()->assign("page_current", $products->current_page);
		view()->assign("pages_count", $products->pages_count);
		
		// urls for sort by and view
		$catalogUrlBase = 
			(($settings['USE_MOD_REWRITE'] == 'YES') ? preg_replace('/\?.+$/', '', $_SERVER['REQUEST_URI']) : '') . "?p=catalog&amp;mode=" . $mode .
			($mode=="catalog" ? "&amp;parent=" . $parent : "").
			($mode=="search" ? "&amp;search_str=" . $search_str . "&amp;search_in=" . $search_in : "") .
			($mode=="manufacturer" ? "&amp;mid=" . $mid : "") .
			(isset($_REQUEST['pg']) ? '&amp;pg=' . htmlspecialchars($_REQUEST['pg']) : '') .
			((isset($filtersQueryString) && !empty($filtersQueryString)) ? '&amp;' . htmlspecialchars($filtersQueryString) : '');
		
		view()->assign("catalog_url_base", $catalogUrlBase);

		// for new skin
		view()->assign("paginator", 
			$paginate = paginate(
				$products->current_page,
				$products->pages_count, // total pages
				$settings["CatalogPagesInList"],
				$url_prefix,
				$mode=="catalog" && $settings["USE_MOD_REWRITE"] == "YES",
				$mode=="catalog" && $settings["USE_MOD_REWRITE"] == "YES" ? UrlUtils::getCatalogUrl($category["url_custom"] == "" ? $category["url_default"] : $category["url_custom"], $parent, false, false, $filtersQueryString) : false
			)
		);
		
		view()->assign("body", "templates/pages/catalog/catalog.html");
	}

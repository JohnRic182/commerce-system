<?php
/**
 * Show users cart
 */
	if (!defined('ENV')) exit();
	
	/**
	 * Check how many gateways other that Google Checkout and PayPal Express Checkout are active.
	 */
	$db->query("SELECT COUNT(*) AS c FROM ".DB_PREFIX."payment_methods WHERE active='Yes' AND id NOT IN ('paypalec')");
	$db->moveNext();
	$activeGateways = $db->col["c"];

	//TODO: The variable $session should already be set in this context, why reset it?
	//$session = new Framework_Session(new Framework_Session_WrapperStorage());
	$flashMessages = new Model_FlashMessages($session);
	view()->assign('flashMessages', $flashMessages->getMessages());

	unset($_SESSION["user_started_checkout"]);

	$cartItems = $order->getOrderItemsExtended($msg);

	view()->assign('order_items', $cartItems);
	
	// check promo
	$promo_code = '';

	if (isset($_REQUEST["promo_code"]))
	{
		$promo_code = $_SESSION["order_promo_code"] = $_REQUEST["promo_code"];
	}
	elseif (isset($_SESSION["order_promo_code"]))
	{
		$promo_code = $_SESSION["order_promo_code"];
	}

	if (isset($promo_error))
	{
		$_SESSION["order_promo_code"] = $promo_code = '';
	}
	
	if (isset($_SESSION["remove_item_promo_error"]))
	{
		$promo_error = $_SESSION["remove_item_promo_error"];
		unset($_SESSION["remove_item_promo_error"]);
	}
	
	view()->assign("promo_error", isset($promo_error));
	view()->assign("promo_code", $promo_code);
	
	//check min subtotal amount
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
	$min_subtotal_amount = normalizeNumber($settings["MinOrderSubtotalLevel".$userLevel]);
	$min_subtotal_error = $min_subtotal_amount > $order->subtotalAmount;
	
	view()->assign("min_subtotal_error", $min_subtotal_error);
	view()->assign("min_subtotal_amount", $min_subtotal_amount);
	
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
	
	if ($order->itemsCount > 0 && $settings["CustomerAlsoBoughtAvailable"] == "YES")
	{
		$bs = new ShoppingCartProducts($db, $settings);
		$bought_bestsellers = $bs->getItemsBestSellersByOrderContent($order->oid, $userLevel);
		
		if ($bought_bestsellers)
		{ 
			view()->assign("bought_bestsellers_file", "boxes/box_products_other_bought.html");
			view()->assign("bought_bestsellers", $bought_bestsellers);
			view()->assign("bought_bestsellers_view", $settings["CustomerAlsoBoughtView"]);
		}
	}

	//check paypal express payments
	view()->assign('paypal_ec_active', false);

	if ($order->totalAmount > 0)
	{
		if (!$order->hasRecurringBillingItems())
		{
			/**
			 * PAYPAL EXPRESS CHECKOUT
			 */
			$db->query("SELECT * FROM ".DB_PREFIX."payment_methods WHERE id='paypalec' AND active='Yes'");

			if ($db->moveNext())
			{
				unset($_SESSION["paypal_express_checkout_details"]);

				view()->assign("paypal_ec_active", true);

				$paypal_ec = new $db->col['class'];
				$paypal_ec->enableLog = defined('DEVMODE') && DEVMODE;
				$paypal_ec->getSettingsData($db);
				//$paypal_ec->getCommonData($user, $order);

				$returnUrl = $url_dynamic.'p=cart&oa='.ORDER_STORE_PAYPAL_EC_INFO;

				if (!$user->auth_ok)
				{
					$returnUrl = $returnUrl."&ua=".USER_EXPRESS_CHECKOUT;
				}

				$cancelUrl = $url_dynamic.'p=cart';

				$expressCheckoutUrl = $paypal_ec->expressCheckout($order, $cartItems, true, $returnUrl, $cancelUrl);

				if ($settings['paypalec_Offer_PayPal_BML'] == 'Yes')
				{
					$bmlExpressCheckoutUrl = $paypal_ec->expressCheckout($order, $cartItems, true, $returnUrl, $cancelUrl, true);
				}
				else
				{
					$bmlExpressCheckoutUrl = false;
				}

				view()->assign("paypal_ec_is_error", $paypal_ec->is_error);

				if ($paypal_ec->is_error)
				{
					view()->assign("paypal_ec_errors", array($paypal_ec->error_message));
				}
				else
				{
					unset($_SESSION['paypal_express_checkout_token_opc']);
					view()->assign("paypal_ec_url", $expressCheckoutUrl);

					unset($_SESSION['paypal_express_checkout_bml_token_opc']);
					view()->assign("paypal_ec_bml_url", $bmlExpressCheckoutUrl);
				}
			}
		}
	}
	else
	{
		$db->query("SELECT * FROM ".DB_PREFIX."payment_methods WHERE id='paypalec' AND active='Yes'");

		if ($db->moveNext())
		{
			$activeGateways = 1;
		}
	}

	view()->assign("SERVER_PORT", $_SERVER["SERVER_PORT"]);

	// set available payment methods count
	view()->assign('totalPaymentMethods', $activeGateways);
	view()->assign("activeGateways", $activeGateways);

	view()->assign("body", "templates/pages/checkout/cart.html");

	$meta_title = trim($settings['cart_page_title']) != '' ? $settings['cart_page_title'] : $meta_title;
	$meta_description = trim($settings['cart_meta_description']) != '' ? $settings['cart_meta_description'] : $meta_description;
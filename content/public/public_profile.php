<?php
/**
 * Updates users profile
 */

if (!defined("ENV")) exit();

if ($user->auth_ok)
{
	//country / state data
	$regions = new Regions($db);
	view()->assign("countries_states", json_encode($regions->getCountriesStatesForSite()));

	view()->assign("user_is_error", ($ua==USER_UPDATE_PROFILE) && ($user->is_error)?"yes":"no");

	$custom_field = isset($_POST["custom_field"]) ? $_POST["custom_field"] : array();
	
	view()->assign("cf_billing", $cf_billing = $customFields->getCustomFieldsValues("billing", $user->id, 0, 0));
	view()->assign("cf_account", $cf_billing = $customFields->getCustomFieldsValues("account", $user->id, 0, 0));

	view()->assign("update_msg","");

	$user_errors = "";
	
	$user_data = $user->getUserData();
	
	if ($ua == USER_UPDATE_PROFILE)
	{
		if ($user->is_error)
		{
			view()->assign("user_errors", $user->errors);
			view()->assign("user", $_POST["form"]);
		}
		else
		{
			view()->assign("update_msg", "Profile has been updated.");
		}
		
		$user_data = array_merge($user_data, $_POST["form"]);
		
		view()->assign("user", $user_data);
	}
	else
	{
		view()->assign("user", $user_data);
	}

	view()->assign('nonce', Nonce::create('user_update_profile'));
	view()->assign("body", "templates/pages/account/profile.html");
}
else
{
	view()->assign("body", "templates/pages/account/auth-error.html");
}

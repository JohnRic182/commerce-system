<?php
/**
 * Cardinal Commerce payment validation
 */

if (!defined("ENV")) exit();

if (
	isset($_SESSION["CardinalPostForm"]) &&
	isset($_SESSION["CardinalTransactionId"]) &&
	isset($_SESSION["CardinalACSUrl"]) &&
	isset($_SESSION["CardinalPayload"]) && 
	isset($_SESSION["CardinalNotifyUrl"]) &&
	isset($_SESSION["CardinalRedirectUrl"]) &&  
	$order->itemsCount > 0
)
{
	
	//Cardinal Commerce data
	view()->assign("CardinalTransactionId", $_SESSION["CardinalTransactionId"]);
	view()->assign("CardinalACSUrl", $_SESSION["CardinalACSUrl"]);
	view()->assign("CardinalPayload", $_SESSION["CardinalPayload"]);
	view()->assign("CardinalNotifyUrl", $_SESSION["CardinalNotifyUrl"]);
	view()->assign("CardinalMD", $_SESSION["CardinalMD"] = md5(rand(1,9999).time()));
	
	//clean uused data
	unset($_SESSION["CardinalACSUrl"]);
	unset($_SESSION["CardinalPayload"]);
	unset($_SESSION["CardinalNotifyUrl"]);
	
	//order data
	view()->assign("order_num", $order->order_num);
	
	//billing
	view()->assign("billing_address", $user->getUserData());
	$cf_billing = $customFields->getCustomFieldsValues("billing", $user->id, 0, 0);
	if ($cf_billing) view()->assign("cf_billing", $cf_billing);

	//shipping
	view()->assign("shipping_address", $order->getShippingAddress());
	$cf_shipping = $customFields->getCustomFieldsValues("shipping", $user->id, $order->oid, 0);
	if ($cf_shipping) view()->assign("cf_shipping", $cf_shipping);
	
	//template
	view()->assign("body", "templates/pages/checkout/payment-validation.html");
}
else
{
	header("Location: ".$url_dynamic."p=cart");
}

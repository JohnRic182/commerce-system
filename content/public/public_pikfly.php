<?php

if (!defined("ENV")) exit();

function &toView(&$deliveryArea)
{
	$before_time = strtotime($deliveryArea['before_time']);
	$last_delivery_time = strtotime($deliveryArea['last_delivery_time']);

	$ret = array(
		'id' => isset($deliveryArea['id']) ? $deliveryArea['id'] : '',
		'ssid' => isset($deliveryArea['ssid']) ? $deliveryArea['ssid'] : '',
		'title' => 'Delivery Area '.$deliveryArea['position'],
		'zip_codes' => explode(',', $deliveryArea['zip_codes']),
		'before_time' => date('g:i A', $before_time),
		'before_time_hour' => date('g', $before_time),
		'before_time_am_pm' => strtolower(date('A', $before_time)),
		'delivery_days' => explode(',', $deliveryArea['delivery_days']),
		'last_delivery_time' => date('g:i A', $last_delivery_time),
		'last_delivery_time_hour' => date('g', $last_delivery_time),
		'last_delivery_time_am_pm' => strtolower(date('A', $last_delivery_time)),
		'delivery_fee' => $deliveryArea['delivery_fee'],
		'delivery_fee_type' => $deliveryArea['delivery_fee_type'],
		'position' => $deliveryArea['position'],
	);

	if (in_array('Monday', $ret['delivery_days']) &&
		in_array('Tuesday', $ret['delivery_days']) &&
		in_array('Wednesday', $ret['delivery_days']) &&
		in_array('Thursday', $ret['delivery_days']) &&
		in_array('Friday', $ret['delivery_days'])
	)
	{
		$ret['delivery_days'] = array('Weekdays');
	}
	else if (
		in_array('Monday', $ret['delivery_days']) &&
		in_array('Tuesday', $ret['delivery_days']) &&
		in_array('Wednesday', $ret['delivery_days']) &&
		in_array('Thursday', $ret['delivery_days']) &&
		in_array('Friday', $ret['delivery_days']) &&
		in_array('Saturday', $ret['delivery_days']) &&
		in_array('Sunday', $ret['delivery_days'])
	)
	{
		$ret['delivery_days'] = array('All');
	}
	else if (in_array('Saturday', $ret['delivery_days']) &&
		in_array('Sunday', $ret['delivery_days'])
	)
	{
		$ret['delivery_days'] = array('Weekends');
	}
	return $ret;
}

if ($settings['PikflyEnabled'] == 'Yes')
{
	$deliveryAreas = $db->selectAll('SELECT m.*
FROM '.DB_PREFIX.'shipping_selected s
	INNER JOIN '.DB_PREFIX.'shipping_zip_methods m ON s.ssid = m.ssid
WHERE carrier_id = "zip_code"
ORDER BY s.ssid ASC');

	$temp = array();
	foreach ($deliveryAreas as &$deliveryArea)
	{
		$areaView = toView($deliveryArea);
		if (count($areaView['zip_codes']) > 0)
		{
			$temp[] = $areaView;
		}
	}

	view()->assign('deliveryAreas', $temp);

	view()->assign("body", "templates/pages/site/pikfly.html");

	//TODO: Meta titles
//	$meta_title = trim($settings['notfound_page_title']) != '' ? $settings['notfound_page_title'] : $meta_title;
//	$meta_description = trim($settings['notfound_meta_description']) != '' ? $settings['notfound_meta_description'] : $meta_description;
}
else
{
	include dirname(__FILE__).'/public_404.php';
}
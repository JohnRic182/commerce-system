<?php
/**
 * Manage user's wishlist
 */

if (!defined('ENV')) exit();

$wlid = isset($_REQUEST['wlid']) ? intval($_REQUEST['wlid']) : 0;
$wl_action = isset($_REQUEST['wl_action']) ? $_REQUEST['wl_action'] : '';
$pid = isset($_REQUEST['pid']) ? intval($_REQUEST['pid']) : 0;
$product_id = isset($_REQUEST['product_id']) ? $_REQUEST['product_id'] : '';
$wlpid = isset($_REQUEST['wlpid']) ? intval($_REQUEST['wlpid']) : 0;

view()->assign('pid', $pid);

if ($user->isAuthenticated())
{
	$wishlist = new Wishlist($db, $user->id);

	switch($wl_action)
	{
		case "manage_wishlist" :
		{
			view()->assign("wish_lists", $wishlist->wishlistList);
			view()->assign("display_mode", "manage_wishlist");
			break;
		}
		case "edit_wishlist" :
		{
			$list = false;
			if ($wlid != 0)
			{
				$list = $wishlist->getWishlistData($wlid);
			}
			if ($list)
			{
				view()->assign("wlid", $wlid);
				view()->assign("wishlist_data", $list);
				view()->assign("display_mode", "edit_wishlist");
			}
			else
			{
				view()->assign("wl_message", $msg["wishlist"]["genralError"]);
			}
			break;
		}

		case "add_wishlist" :
		{
			view()->assign("display_mode", "add_wishlist");
			break;
		}

		case "update_name" :
		{
			$wishlist_name = isset($_REQUEST["wishlist_name"]) ? $_REQUEST["wishlist_name"] : "";
			$wishlist->updateWishlist($wlid,$wishlist_name);
			view()->assign("wl_message", $wishlist->messages);
			break;
		}

		case "delete_wishlist" :
		{
			$wishlist->deleteWishlist($wlid);
			view()->assign("wl_message", $wishlist->messages);
			break;
		}

		case "delete_product" :
		{
			$wishlist->deleteWishlistProduct($wlpid);
			view()->assign("wl_message", $wishlist->messages);
			break;
		}

		case "send_email_form" :
		{
			$list = $wishlist->getWishlistData($wlid);
			if ($list)
			{
				view()->assign("wlid", $wlid);
				view()->assign("display_mode", "send_email_form");
			}
			else
			{
				view()->assign("wl_message", $msg["wishlist"]["genralError"]);
			}
			break;
		}

		case "send_email" :
		{
			$list = $wishlist->getWishlistData($wlid);
			if ($list)
			{
				view()->assign("wlid", $wlid);
				$mail_subject = isset($_POST["mail_subject"]) ? $_POST["mail_subject"] : "";
				$your_email = isset($_POST["your_email"]) ? $_POST["your_email"] : "";
				Notifications::emailWishlist($db, $mail_subject, $your_email, $user->id, $list);
				view()->assign("wl_message", $msg["wishlist"]["sentEmail"]);
			}
			else
			{
				view()->assign("wl_message", $msg["wishlist"]["genralError"]);
			}
			break;
		}
	}
	view()->assign('body', 'templates/pages/wishlist/manage.html');
}
else
{
	view()->assign('body', 'templates/pages/account/auth-error.html');
}

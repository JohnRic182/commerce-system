<?php
/**
 * User order (archived)
 */

if (!defined('ENV')) exit();

if ($user->auth_ok)
{
	$orderId = isset($_REQUEST['order_id']) ? intval($_REQUEST['order_id']) : 0;
	$orderHistory = ORDER::getById($db, $settings, $user, $orderId);

	if ($orderHistory)
	{
		$orderHistory->getOrderData();
		$orderHistory->getOrderItems();

		$userHistoryData = $user->getUserData();

		if ($orderHistory->getShippingTrackingNumber() != '')
		{
			if ($orderHistory->getShippingTrackingNumberType() == 'UPS')
			{
				$ups = new Shipping_UPS();
				$ups->setUserName($settings['ShippingUPSUserID']);
				$ups->setPass($settings['ShippingUPSUserPassword']);
				$ups->setAccessKey($settings['ShippingUPSAccessKey']);
				$ups->setTrackingServer($settings['ShippingUPSTrackingUrl']);
				$info = false; //$ups->getTrackingInfo($orderHistory->getShippingTrackingNumber());
				view()->assign('tracking_info', $info);
			}
			else if ($orderHistory->getShippingTrackingNumberType() == 'USPS')
			{
				$usps = new Shipping_USPS();
				$usps->setUserName($settings['ShippingUSPSUserID']);
				$usps->setPass($settings['ShippingUSPSUserPassword']);
				$usps->setServer($settings['ShippingUSPSTrackingUrl']);
				$result = false; //$usps->track($orderHistory->getShippingTrackingNumber());
				if ($result)
				{
					view()->assign('tracking_info', $result);
				}
			}
		}

		$viewModel = new View_OrderViewModel($settings, $msg);
		$viewModel->build($db, $orderHistory);

		$orderViewModel = new View_OrderLinesViewModel($settings, $msg);
		$orderViewModel->build($orderHistory, false);

		view()->assign('orderView', $orderViewModel->asArray());

		// Check for Bongo order
		$orderData = $db->selectOne('SELECT * FROM '.DB_PREFIX.'orders WHERE oid='.intval($orderHistory->getId()));

		if ($orderData['payment_gateway_id'] == 'bongocheckout')
		{
			$viewModel->setBongoOrder(true);
			$viewModel->setBongoData(false);

			if (trim($orderData['custom2']) != '' && trim($orderData['custom3']) != '')
			{
				$bongoOrderData = @unserialize($orderData['custom2']);

				if ($bongoOrderData && is_array($bongoOrderData))
				{
					$viewModel->setBongoData($bongoOrderData);
				}
			}
		}
		else
		{
			$viewModel->setBongoOrder(false);
		}

		$shipmentsRepository = new DataAccess_OrderShipmentRepository($db, new DataAccess_SettingsRepository($db, $settings));
		$shipmentsView = new View_OrderShipmentsViewModel($settings, $msg);


		$fulfillmentRepository = new DataAccess_FulfillmentRepository($db);

		$orderHistory->fulfillments = $fulfillmentRepository->getFulfillments($orderId);
		$fulfillment = Model_Fulfillment::createFulfillmentFromOrder($orderHistory);

		$fulfillmentItems = array();

		if ($fulfillment)
		{
			/** @var Model_FulfillmentItem $fulfillmentItem */
			foreach ($fulfillment->getItems() as $lineItemId => $fulfillmentItem)
			{
				/** @var Model_LineItem $lineItem */
				$lineItem = $orderHistory->lineItems[$lineItemId];

				if ($lineItem->getFinalQuantity() > $fulfillmentItem->getQuantity())
				{
					$lineItem->setFulfillmentStatus(trans('orders.fulfillment_options.partial'));
				}
				else
				{
					$lineItem->setFulfillmentStatus(trans('orders.not_fulfilled'));
				}

				$fulfillmentItems[] = array(
						'title' => $lineItem->getTitle(),
						'quantity' => $fulfillmentItem->getQuantity(),
						'lineItemId' => $fulfillmentItem->getLineItemId(),
						'shipmentId' => $fulfillmentItem->getShipmentId()
				);
			}
		}

		$fulfillmentViews = Admin_View_FulfillmentViewModel::getFulfillmentViews($orderHistory->fulfillments, $orderHistory);
		view()->assign('fulfillments', $fulfillmentViews);
		view()->assign('fulfillment', $fulfillment);
		view()->assign('fulfillment_items', $fulfillmentItems);

		view()->assign('shipments', $shipmentsView->getShipmentsView($shipmentsRepository->getShipmentsByOrderId($orderHistory->getId())));
		view()->assign('history_order_data', $viewModel);
		view()->assign('history_user_data', $userHistoryData);
		view()->assign('body', 'templates/pages/account/order.html');
	}
	else
	{
		header('Location: '.$settings['GlobalHttpUrl'].'/index.php?p=404');
		_session_write_close();
		die();
	}
}
else
{
	view()->assign('body', 'templates/account/auth-error.html');
}

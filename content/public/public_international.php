<?php
/**
 * Show international settings
 */

	if (!defined("ENV")) exit();

	$meta_title = trim($settings['international_page_title']) != '' ? $settings['international_page_title'] : $meta_title;
	$meta_description = trim($settings['international_meta_description']) != '' ? $settings['international_meta_description'] : $meta_description;

	if (count($active_languages) > 0 && count($currencies_list) > 0)
	{
		view()->assign("body", "templates/pages/site/international.html");
	}
	else
	{
		header("Location: ".$settings["GlobalHttpUrl"]."/index.php?p=404");
		_session_write_close();
		die();
	}

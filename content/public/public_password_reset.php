<?php
/**
 * Reset users password
 */

	if (!defined("ENV")) exit();

	$resetPasswordSent = isset($resetPasswordSent) ? ($resetPasswordSent) : false;
	$resetPasswordProbe = isset($resetPasswordProbe) ? ($resetPasswordProbe) : false;
	
	$loginCookieName = $settings["SecurityCookiesPrefix"]."LoginUserName";
	
	$login = !empty($_POST["login"]) ? $_POST["login"] : (in_array($loginCookieName, array_keys($_COOKIE)) ? $_COOKIE[$loginCookieName] : "");
	
	view()->assign("login", $login);
	view()->assign("email", !empty($_POST["email"]) ? $_POST["email"] : "");
	
	view()->assign("resetPasswordSent", $resetPasswordSent);
	view()->assign("resetPasswordProbe", $resetPasswordProbe);
	
	view()->assign("body", "templates/pages/account/password-reset.html");

	$meta_title = trim($settings['reset_page_title']) != '' ? $settings['reset_page_title'] : $meta_title;
	$meta_description = trim($settings['reset_meta_description']) != '' ? $settings['reset_meta_description'] : $meta_description;
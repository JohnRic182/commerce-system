<?php
/**
 * Displayed when order is completed (thank you page
 */

	if (!defined("ENV")) exit();
	
	$order_id = isset($_SESSION["pc_order_id"])?intval($_SESSION["pc_order_id"]):0;
	$payment_method_id = isset($_SESSION["pc_method_id"])?intval($_SESSION["pc_method_id"]):"";
	$order_quantity = isset($_SESSION["pc_order_quantity"])?intval($_SESSION["pc_order_quantity"]):0;
	$order_num = isset($_SESSION["pc_order_num"])?intval($_SESSION["pc_order_num"]):0;
	$message_thankyou = isset($message_thankyou) ? $message_thankyou : '';

	if ($payment_method_id == 5)
	{
		//Fix for paypal issue
		$db->query("UPDATE ".DB_PREFIX."orders SET status = 'Process' WHERE oid='".intval($order_id)."' AND status IN ('Abandon', 'New')");
		if ($db->numRows() > 0)
		{
			$redirect = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?".$_SERVER["QUERY_STRING"];
			header("Location: ".$redirect);
			die();
		}
	}

	if (isset($_SESSION["LastOrderID"]))
	{
		$order_id = intval($_SESSION["LastOrderID"]);
	}

	if (isset($_SESSION["payment_session_delay_timeout"]))
	{
		$_t_o = $_SESSION["payment_session_delay_timeout"];
		set_time_limit($_t_o * 5);
		$redirect = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?".$_SERVER["QUERY_STRING"];
		sleep($_t_o);
		unset($_SESSION["payment_session_delay_timeout"]);
		_session_write_close();
		header("Location: ".$redirect);
		die();
	}

	unset($_SESSION["pc_order_id"]);
	unset($_SESSION["pc_order_amount"]);
	unset($_SESSION["pc_method_id"]);
	unset($_SESSION["pc_order_quantity"]);
	unset($_SESSION["pc_order_num"]);
	unset($_SESSION["paypal_express_checkout_token"]);
	unset($_SESSION["paypal_express_checkout_details"]);
	unset($_SESSION["opc-shipping-address-id"]);
	unset($_SESSION["order_promo_code"]);
	unset($_SESSION["order_gift_certificate"]);

	$db->query("SELECT * FROM ".DB_PREFIX."orders_content WHERE oid='".intval($order_id)."' AND product_type='Digital'");
	if($db->moveNext())
	{
		$message_thankyou = $message_thankyou."<br/><br/>".$msg['cart']['thank_you_for_order_digital'];
	}

	$db->query("SELECT * FROM ".DB_PREFIX."orders WHERE oid='".intval($order_id)."'");
	if ($db->moveNext())
	{
		$promoCampaignId = intval($db->col['promo_campaign_id']);
		$shipping_amount = $db->col["shipping_amount"];
		$tax = $db->col["tax_amount"];
		$city = $db->col["shipping_city"];
		$stid = $db->col["shipping_state"];
		$coid = $db->col["shipping_country"];
		$order_total = $db->col["total_amount"];
		$order_value = $db->col['subtotal_amount'] - $db->col['discount_amount'] - (strtolower($db->col['promo_type']) != 'shipping' ? $db->col['promo_discount_amount'] : 0);
		$order_num = $db->col["order_num"];
		$payment_method_id = $db->col['payment_method_id'];
		$subtotal_amount = $db->col['subtotal_amount'];
		$user_id = $db->col['uid'];

		$region = getState($db, $stid);
		$country = getCountry($db, $coid);
		view()->assign("shipping_amount", $shipping_amount); /* Shipping Amount */
		view()->assign("city", $city); /* City of Purchaser */
		view()->assign("region", $region); /* State/Region of Purchaser */
		view()->assign("country", $country); /* Country of Purchaser */
		view()->assign("tax", $tax); /* Tax  Amount */


		$promoCode = '';
		if ($promoCampaignId > 0)
		{
			$promoRepository = new DataAccess_PromoCodeRepository($db, $settings);
			$promoCodeData = $promoRepository->getPromoCodeByPromoCampaignId($promoCampaignId);
			$promoCode = $promoCodeData['promo_code'];
		}
		view()->assign('promoCode', $promoCode);

		$items = $db->selectAll("
			SELECT
				".DB_PREFIX."orders_content.ocid as line_item, 
				".DB_PREFIX."products.title as product_name, 
				".DB_PREFIX."products.product_id as sku,
				".DB_PREFIX."orders_content.product_sub_id as attribute_sku,
				".DB_PREFIX."catalog.name as catalog_name, 
				".DB_PREFIX."orders_content.price as price, 
				".DB_PREFIX."orders_content.quantity as quantity,
				".DB_PREFIX."orders.oid,
				".DB_PREFIX."orders.order_type,
				".DB_PREFIX."orders.subtotal_amount,
				".DB_PREFIX."orders.tax_amount,
				".DB_PREFIX."orders.shipping_amount,
				".DB_PREFIX."orders.discount_amount,
				".DB_PREFIX."orders.handling_fee,
				".DB_PREFIX."orders.payment_method_name,
				".DB_PREFIX."orders.total_amount,
				".DB_PREFIX."orders.placed_date,
				".DB_PREFIX."orders.uid,
				".DB_PREFIX."orders.ipaddress,
				".DB_PREFIX."users.fname,
				".DB_PREFIX."users.lname,
				".DB_PREFIX."users.email,
				".DB_PREFIX."users.lname,
				".DB_PREFIX."states.name as states_name,
				".DB_PREFIX."countries.name as country_name
			FROM
				".DB_PREFIX."orders,
				".DB_PREFIX."users,
				".DB_PREFIX."orders_content, 
				".DB_PREFIX."products, 
				".DB_PREFIX."catalog,
				".DB_PREFIX."states,
				".DB_PREFIX."countries
			WHERE
				".DB_PREFIX."orders.oid='".intval($order_id)."' 
				AND ".DB_PREFIX."orders_content.oid=".DB_PREFIX."orders.oid
				AND ".DB_PREFIX."orders_content.pid=".DB_PREFIX."products.pid
				AND ".DB_PREFIX."orders.uid=".DB_PREFIX."users.uid
				AND ".DB_PREFIX."users.state=".DB_PREFIX."states.stid
				AND ".DB_PREFIX."users.country=".DB_PREFIX."countries.coid 
				AND ".DB_PREFIX."catalog.cid=".DB_PREFIX."products.cid");


		view()->assign("items", $items); /* Items Purchased */

		$user_email = "";
		$db->query("SELECT u.email FROM " . DB_PREFIX . "users u WHERE u.uid = '" . $user_id . "'");
		if ($db->moveNext())
		{
			$user_email = $db->col['email'];
		}
		view()->assign('user_email', $user_email);
	}

	$message_thankyou = $msg["cart"]["thank_you_for_order"];
	
	$db->query("SELECT * FROM ".DB_PREFIX."payment_methods WHERE pid='".intval(isset($payment_method_id) ? $payment_method_id : 0)."'");

	if($db->moveNext())
	{
		$is_payment = true;
		$message_thankyou = trim($db->col["message_thankyou"]) == "" ? $message_thankyou : $db->col["message_thankyou"]; 
	}

	view()->assign("affiliation", $settings["GlobalSiteName"]); /* Affiliation Key */
	view()->assign("order_id", $order_id); /* Unique Order ID in Database (oid), assigned to all orders */
	view()->assign("order_total", isset($order_total) ? $order_total : 0); /* Order Total Amount */
	view()->assign("order_value", isset($order_value) ? $order_value : 0); /* Order Value - Affiliates */
	view()->assign("order_amount", isset($order_value) ? $order_value : 0); /* Backwards compatible for order value */
	view()->assign("order_quantity", isset($order_quantity) ? $order_quantity : 0); /* Items Quantity */
	view()->assign("order_num", isset($order_num) ? $order_num : 0); /* Unique Order Number in Database (order_num), assigned ONLY to completed orders */			
	view()->assign("message_thankyou", $message_thankyou);
	
	if ($user->express)
	{
		$user->logOut();
		$user->id = 0;
	}

	//mailchimp e360 integration
	if (isset($_COOKIE["mailchimp_campaign_id"]) )
	{
		$chimpenabled = $settings["mailchimp_enabled"];
		$chimpkey = $settings["mailchimp_apikey"];
		$chimplist = $settings["mailchimp_newsletter_list"];
		if ($chimpenabled && $chimpkey)
		{
			$mcorder = array(
							'id' =>intval($order_id),
							'total'=>floatval($order_total),
							'shipping'=>floatval($shipping_amount),
							'tax'  =>floatval($tax),
							'items'=>array(),
							'store_id'=>$settings["CompanyName"],
							'store_name' => $settings["CompanyName"],
							'campaign_id'=>(isset($_COOKIE['mailchimp_campaign_id'])?$_COOKIE['mailchimp_campaign_id']:""),
							'email_id'=>(isset($_COOKIE['mailchimp_email_id'])?$_COOKIE['mailchimp_email_id']:""),
							'plugin_id'=>9999
							);
			/* Product informations */
			$db->query("SELECT oc.*, c.cid, c.name as category_name
			FROM ".DB_PREFIX."orders_content oc
			 INNER JOIN ".DB_PREFIX."products p ON oc.pid = p.pid
			 INNER JOIN ".DB_PREFIX."catalog c ON p.cid = c.cid
			WHERE oc.oid = ".intval($order_id));
			$products = $db->getRecords();
			foreach ($products AS $product)
			{
				$item = array();
				$item['line_num'] = $product['ocid'];
				$item['product_id'] = $product['pid'];
				$item['product_name'] = $product['product_id'];
				$item['sku'] = isset($product['product_sku']) ? $product['product_sku'] : $product['product_id'];
				$item['category_id'] = $product['cid'];
				$item['category_name'] = $product['category_name'];
				$item['qty'] = intval($product['admin_quantity']);
				$item['cost'] = intval($product['admin_price']);
				$mcorder['items'][] = $item;
			}

			require_once(dirname(dirname(__FILE__))."/vendors/mailchimp/src/Mailchimp.php");

			$mc = new Mailchimp($chimpkey);
			$mc->ecomm->orderAdd($mcorder);
		}
	}

	view()->assign("body", "templates/pages/checkout/completed.html");

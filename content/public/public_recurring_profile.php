<?php
	/**
	 * Show page recurring profile
	 */

	if (!defined("ENV")) exit();

	$controller = new RecurringBilling_Front_Controller_RecurringProfile($request, $db);
	$controller->detailsAction();
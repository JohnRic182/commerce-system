<?php
	/**
	 * Show page recurring profiles
	 */

	if (!defined("ENV")) exit();

	$controller = new RecurringBilling_Front_Controller_RecurringProfile($request, $db);
	$controller->listAction();
<?php
/**
 * Newsletter unsubscribe page
 */

	if (!defined("ENV")) exit();

	$action = isset($_REQUEST["action"]) ? trim($_REQUEST["action"]) : "";

	$status_changed = false;
	$cancel_email = isset($_REQUEST["cancel_email"]) ? trim($_REQUEST["cancel_email"]) : "";

	if ($action=="update")
	{
		$status_changed = NewsletterConnector::unsubscribe($cancel_email);
	}

	if($action=="cancel")
	{
		// Get real users ids
		list($email, $uid, $maillist) = explode('-', $listkey);

		$user_email = $db->escape($email);
		$user_id = intval($uid);
		$user_list =  $db->escape($maillist);
		$db_field = "receives_marketing";

		// Get records by Id
		$db->query("SELECT * FROM ".DB_PREFIX."emails WHERE eid = '".intval($user_id)."' AND ".$db_field." = 'Yes'");
		// Decode Hashed email to check with db email
		if ($db->moveNext())
		{
			if (md5($db->col["email"]) == $user_email)
			{
				$db->query("UPDATE ".DB_PREFIX."emails SET $db_field='No' WHERE eid='".intval($user_id)."'");
				$status_changed = true;
			}
		}
	}

	view()->assign("action", $action);
	view()->assign("status_changed", $status_changed);
	view()->assign("cancel_email", $cancel_email);
	view()->assign("body", "templates/pages/newsletters/unsubscribe.html");

	$meta_title = trim($settings['unsubscribe_page_title']) != '' ? $settings['unsubscribe_page_title'] : $meta_title;
	$meta_description = trim($settings['unsubscribe_meta_description']) != '' ? $settings['unsubscribe_meta_description'] : $meta_description;
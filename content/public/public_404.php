<?php
/**
 * Show page not found messale
 */

	if (!defined("ENV")) exit();

	if (!defined('DESIGN_MODE') || (defined('DESIGN_MODE') && !DESIGN_MODE))
	{
		header("HTTP/1.0 404 Not Found");
	}

	view()->assign("body", "templates/pages/site/404.html");

	$meta_title = trim($settings['notfound_page_title']) != '' ? $settings['notfound_page_title'] : $meta_title;
	$meta_description = trim($settings['notfound_meta_description']) != '' ? $settings['notfound_meta_description'] : $meta_description;
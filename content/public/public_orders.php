<?php
/**
 * User's orders list
 */

if (!defined("ENV")) exit();

if ($user->auth_ok)
{
	$orders = $user->getOrders();
	view()->assign("orders", $orders);
	view()->assign("body", "templates/pages/account/orders.html");
}
else
{
	view()->assign("body", "templates/pages/account/auth-error.html");
}

<?php
/**
 * Get order shipping quote
 */

	if (!defined("ENV")) exit();

	//TODO: add check for empty order
	$meta_title = trim($settings['shippingquote_page_title']) != '' ? $settings['shippingquote_page_title'] : $meta_title;
	$meta_description = trim($settings['shippingquote_meta_description']) != '' ? $settings['shippingquote_meta_description'] : $meta_description;

	/**
	 * Check variable
	 */
	$sq_country = isset($_POST['sq_country']) ? intval($_POST['sq_country']) : '';
	$sq_state = isset($_POST['sq_state']) ? intval($_POST['sq_state']) : '';
	$sq_zip = isset($_POST['sq_zip']) ? trim($_POST['sq_zip']) : '';

	/**
	 * Prepare address
	 */
	if ($sq_country == '' || $sq_zip == '')
	{
		if ($user->auth_ok)
		{
			$orderShippingAddress = $order->getShippingAddress();

			if ($orderShippingAddress && isset($orderShippingAddress['country_id']) && $orderShippingAddress['country_id'] > 0 && trim($orderShippingAddress['zip']) != '')
			{	
				$sq_country = $orderShippingAddress['country_id'];
				$sq_state = $orderShippingAddress['state_id'];
				$sq_zip = $orderShippingAddress['zip'];
			}
			elseif (isset($user->data['country']) && isset($user->data['state']) && isset($user->data['zip']))
			{	
				$sq_country = $user->data['country'];
				$sq_state = $user->data['state'];
				$sq_zip = $user->data['zip'];
			}
		}
		else
		{
			$sq_country = $settings['ShippingOriginCountry'];
		}
	}

	/**
	 * Get shipping quote
	 */
	if (isset($_REQUEST['get_shipping_quote']))
	{
		view()->assign('show_shipping_quote', true);
		view()->assign('shipping_required', $order->getShippingRequired());

		if ($order->getItemsCount() > 0)
		{
			if ($order->isShippingFree())
			{
				view()->assign('shipping_is_free', true);
			}

			// check conditions do we have to calc shipping
			if (!$order->getShippingIsFree() || $settings['ShippingShowAlternativeOnFree'] == 'YES')
			{
				$shippingRepository = new DataAccess_ShippingRepository($db);

				$shippingQuotesAddress = $shippingRepository->makeShippingQuotesAddress($sq_country, $sq_state, $sq_zip);

				$currencies = new Currencies($db);
				$defaultCurrency = $currencies->getDefaultCurrency();
				$currencyExchangeRates = $currencies->getExchangeRates();

				$rates = false;
				$ratesAlternative = false;
				$bongoActive = false;
				$bongoUsed = false;

				if ($order->getShippingIsFree())
				{
					if ($settings['ShippingShowAlternativeOnFree'] == 'YES')
					{
						// only alternative rates available for free shipping order
						$ratesAlternative = Shipping::getShippingQuotes(
							$db, $settings, $user, $order, true, $shippingQuotesAddress, true,
							$defaultCurrency['code'], $currencyExchangeRates,
							$bongoActive, $bongoUsed
						);
					}
				}
				else
				{
					// get standard shipping rates
					$rates = Shipping::getShippingQuotes(
						$db, $settings, $user, $order, true, $shippingQuotesAddress, false,
						$defaultCurrency['code'], $currencyExchangeRates,
						$bongoActive, $bongoUsed
					);

					if ($order->shippingFreeProductsCount > 0 && $settings['ShippingShowAlternativeOnFree'] == 'YES')
					{
						// get alternative shipping methods where order contains free shipping items
						$ratesAlternative = Shipping::getShippingQuotes(
							$db, $settings, $user, $order, true, $shippingQuotesAddress, true,
							$defaultCurrency['code'], $currencyExchangeRates,
							$bongoActive, $bongoUsed
						);

						$ratesAlternative = Shipping::filterAlternativeShippingQuotes($rates, $ratesAlternative);
					}
				}

				if (!$order->getShippingIsFree() || $ratesAlternative['shipping_methods_count'])
				{
					$shippingQuotesViewModel = new View_ShippingQuotesViewModel($settings, $msg);
					$taxProvider = Tax_ProviderFactory::getTaxProvider();

					$ratesView = $shippingQuotesViewModel->getShippingQuotesView($order, $taxProvider, $rates, true, $sq_country, $sq_state);
					$ratesAlternativeView = $shippingQuotesViewModel->getShippingQuotesView($order, $taxProvider, $ratesAlternative, true, $sq_country, $sq_state);

					view()->assign('shippingMethods', $rates && is_array($rates) ? $ratesView : false);
					view()->assign('shippingMethodsAlternative', $ratesAlternative && is_array($ratesAlternative) ? $ratesAlternativeView : false);

					view()->assign('shipping_is_free', $order->getShippingIsFree());
				}
			}
		}
	}
	else
	{
		view()->assign('show_shipping_quote', false);
	}

	//country / state data
	$regions = new Regions($db);
	$countries_states = $regions->getCountriesStatesForSite();
	view()->assign('countries_states', $countries_states);
	
	view()->assign('sq_country', isset($_POST['sq_country']) ? $_POST['sq_country'] : $sq_country);
	view()->assign('sq_state', isset($_POST['sq_state']) ? $_POST['sq_state'] : $sq_state);
	view()->assign('sq_zip', $sq_zip);
	view()->assign('body', 'templates/pages/checkout/shipping-quote.html');

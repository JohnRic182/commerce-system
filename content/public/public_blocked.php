<?php
/**
 * Displayed when used account is blocked
 */

if (!defined("ENV")) exit();

view()->assign("body", "templates/pages/account/blocked.html");

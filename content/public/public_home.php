<?php
/**
 * Show home page
 */

if (!defined("ENV")) exit();

if ($settings["CatalogHomeView"] != "None")
{
	$products = new ShoppingCartProducts($db, $settings);

	$params = array();
	$params["CatalogSortBy"] = $settings["CatalogDefaultSortOrder"];
	$params["CatalogItemsOnPage"] = is_numeric($settings["CatalogHomeItemsOnPage"])?intval($settings["CatalogHomeItemsOnPage"]):10;
	$params["Page"] = isset($_REQUEST["pg"]) ? intval($_REQUEST["pg"]) : 1;
	$params["mode"] = "home";
	
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
	view()->assign("products", $products->getItemsCatalog($params, $userLevel));
	view()->assign("products_file", strtolower("templates/pages/catalog/views/".$settings["CatalogHomeView"].".html"));
}
else
{
	view()->assign("products", false);
}
	
view()->assign("body", "templates/pages/site/home.html");
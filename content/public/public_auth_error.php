<?php
/**
 * Show users auth error
 */

if (!defined("ENV")) exit();

view()->assign("body", "templates/pages/account/auth-error.html");

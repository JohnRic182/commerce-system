<?php
/**
 * Newsletter subscribe page
 */

	if (!defined("ENV")) exit();

	$email = isset($_POST["email"]) ? trim(strtolower($_POST["email"])) : "";
	$email_exists = "no";

	if (validate($email, VALIDATE_EMAIL))
	{
		$response = NewsletterConnector::subscribe($email);
		$email_exists = ($response) ? 'no' : 'yes';
	}

	view()->assign("email_exists", $email_exists);
	view()->assign("body", "templates/pages/newsletters/subscribe.html");

	$meta_title = trim($settings['subscribe_page_title']) != '' ? $settings['subscribe_page_title'] : $meta_title;
	$meta_description = trim($settings['subscribe_meta_description']) != '' ? $settings['subscribe_meta_description'] : $meta_description;
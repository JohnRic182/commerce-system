<?php
/**
 * Wishlist
 */
if (!defined("ENV")) exit();

$wl_action = isset($_REQUEST["wl_action"]) ? $_REQUEST["wl_action"] : "";
$pid = isset($_REQUEST["pid"]) ? intval($_REQUEST["pid"]) : 0;
	
view()->assign("pid", $pid);

if ($user->data['login'] == 'ExpressCheckoutUser')
{
	view()->assign("body", "templates/pages/account/auth-error.html");
}
else
{
	if ($user->auth_ok)
	{
		$wishlist = new Wishlist($db, $user->id);
		switch ($wl_action)
		{
			case "select_wishlist" :
			{
				view()->assign("display_mode", "select_wishlist");
				view()->assign("wish_lists", $wishlist->wishlistList);		
				break;
			}
	
			case "updated_wishlist" :
			{
				view()->assign("wishlist_messgae", $msg["wishlist"]["wishlistUpdated"]);
				view()->assign("display_mode", "updated_wishlist");
				break;
			}
	
			case "added_wishlist" :
			{
				view()->assign("wishlist_messgae", $msg["wishlist"]["wishlistAdded"]);
				view()->assign("display_mode", "added_wishlist");
				break;
			}
	
			case "addnew_wishlist" :
			{
				view()->assign("display_mode", "addnew_wishlist");				
				break;
			}
			
			case "wishlist_error" :
			{
				view()->assign("wishlist_messgae", $msg["wishlist"]["genralError"]);
				view()->assign("display_mode", "wishlist_error");
				break;
			}
	
			default :
			{				
				break;		
			}
		}
	}
	else
	{
		view()->assign("display_mode", "user_error");	
	}
	
	view()->assign("body", "templates/pages/wishlist/wishlist.html");
}
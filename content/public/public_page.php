<?php
/**
 * Shows HTML page
 */

if (!defined("ENV")) exit();

$page_id = isset($_REQUEST["page_id"]) ? $_REQUEST["page_id"] : "";
$pageData = $db->selectOne("SELECT *, IF(url_custom='', url_default, url_custom) AS url FROM " . DB_PREFIX . "pages WHERE name='" . $db->escape($page_id) . "' AND is_active='Yes'");

if ($pageData)
{

	$pageData['url'] = UrlUtils::getPageUrl($pageData['name'], $pageData['url']);
	view()->assign('pageData', $pageData);

	view()->assign('pageTitle', $pageData['title']);
	view()->assign('pageContent', $pageData['content']);
	view()->assign('pid', $pageData['pid']);
	view()->assign('body', 'templates/pages/site/page.html');
	
	if (trim($pageData['meta_title']) != '')
	{
		$meta_title = trim($pageData['meta_title']);
	}
	else
	{
		$meta_title = $pageData['title'] . ': ' . $meta_title;
	}
	
	if (trim($pageData['meta_description']) != '') $meta_description = trim($pageData['meta_description']);

	$canonical_url = $pageData['url_custom'] == '' ? $pageData['url_default'] : $pageData['url_custom'];
	view()->assign('canonical_url', $settings["GlobalHttpUrl"] . '/' . $canonical_url);

	// assign custom fields
	if ($pageData['custom_form_id'] != 0)
	{
		// TODO: move into repository
		$customFormData = $db->selectOne('SELECT * FROM ' . DB_PREFIX . 'custom_forms WHERE id=' . intval($pageData['custom_form_id']) . ' AND is_active = 1');

		if ($customFormData)
		{
			$customFieldsData = isset($_POST['custom_field']) ? $_POST['custom_field'] : array();
			$cf_form = $customFields->getCustomFields('form', $customFieldsData, $customFormData['id']);

			if ($cf_form)
			{
				if ($_SERVER['REQUEST_METHOD'] == 'POST')
				{
					$captchaError = false;

					if ($settings['captchaMethod'] != 'None' && $customFormData['is_recaptcha'] == '1')
					{
						$recaptcha = new \ReCaptcha\ReCaptcha($settings['captchaReCaptchaPrivateKey']);
						$response = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER["REMOTE_ADDR"]);
						if (!$response->isSuccess())
						{
							$captchaErrors = $response->getErrorCodes();
							foreach ($captchaErrors as $error)
							{
								$user->setError("Captcha Error: $error");
							}
							$captchaError = true;
						}
					}

					if (!$captchaError)
					{
						$customFieldFormData = isset($_REQUEST['custom_field']) ? (is_array($_REQUEST['custom_field']) ? $_REQUEST['custom_field'] : array()) : array();

						if (is_array($customFieldFormData) && count($customFieldFormData) > 0)
						{
							$customFields->resetErrors();
							$customFields->validateCustomFields('form', $customFieldFormData, $customFormData['id']);

							if ($customFields->is_error)
							{
								view()->assign('formErrors', $customFields->errors);
							}
							else
							{
								$customFields->storeCustomFields('form', $customFieldFormData, $user->isAuthenticated() ? $user->getId() : 0, 0, 0, $customFormData['id']);

								if ($customFormData['email_notification'] == 1)
								{
									$data = array();
									foreach ($cf_form as $key => $field)
									{
										$data[$field['field_id']] = array(
											'field_name' => $field['field_name'],
											'field_value' => isset($customFieldFormData[$field['field_id']]) ? $customFieldFormData[$field['field_id']] : 'n/a'
										);
									}

									Notifications::emailCustomFormSubmitted($db, $customFormData, $data);
								}

								if ($customFormData['redirect_on_submit'] == 1 && trim($customFormData['redirect_on_submit_url']) != '')
								{
									header('Location: ' . $customFormData['redirect_on_submit_url']);
									_session_write_close();
									$db->done();
									exit();
								}
								else
								{
									view()->assign('formSubmited', true);
								}
							}
						}
					}
				}

				view()->assign('customForm', $customFormData);
				view()->assign('cf_form', $cf_form);

				//add captcha
				if ($settings['captchaMethod'] != 'None' && $customFormData['is_recaptcha'] == 1)
				{
					view()->assign('captcha', true);
					view()->assign('site_key', $settings['captchaReCaptchaPublicKey']);
				}
			}
		}
	}
}
else
{
	header('Location: ' . $settings['GlobalHttpUrl'] . '/index.php?p=404');
	_session_write_close();
	die();
}
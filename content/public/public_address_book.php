<?php
/**
 * Show users address book
 */

if (!defined("ENV")) exit();

if ($user->auth_ok)
{
	view()->assign("addresses", $user->getShippingAddresses());
	$cf_shipping = $customFields->getCustomFields("shipping", isset($_REQUEST["custom_field"])?$_REQUEST["custom_field"]:array());
	
	view()->assign("cf_shipping", $cf_shipping);
	view()->assign("allow_select", "no");
	
	if ($ua == USER_ADD_ADDRESS)
	{
		if ($user->is_error)
		{
			view()->assign("user_errors", $user->errors);
			view()->assign("edit_address", $_POST["form"]);
		}
	}

	$regions = new Regions($db);
	view()->assign("countries_states", json_encode($regions->getCountriesStatesForSite()));

	view()->assign('nonce_add_address', Nonce::create('user_add_address'));
	view()->assign('nonce_delete_address', Nonce::create('user_delete_address'));

	view()->assign("body", "templates/pages/account/address-book.html");
}
else
{
	view()->assign("body", "templates/pages/account/auth-error.html");
}
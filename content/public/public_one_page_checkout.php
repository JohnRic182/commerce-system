<?php
/**
 * One Page Checkout
 */
	if (!defined("ENV")) exit();

	$flashMessages = new Model_FlashMessages($session);

	$ret = OrderProvider::reupdateOrder($order);

	if (!$ret)
	{
		$flashMessages->setMessages($order->errors, Model_FlashMessages::TYPE_ERROR);

		header("Location: ".$settings["GlobalHttpUrl"]."/index.php?p=cart");
		_session_write_close();
		die();
	}

	//check min subtotal amount
	$userLevel = ($user->auth_ok) ? ($user->level) : ($userCookie ? $userCookie["l"] : 0);
	$min_subtotal_amount = normalizeNumber($settings["MinOrderSubtotalLevel".$userLevel]);
	$min_subtotal_error = $min_subtotal_amount > $order->getSubtotalAmount();
	
	if ($order->getItemsCount() > 0 && $user->isAuthorized() && !$min_subtotal_error)
	{
		// Prepare order
		$order->setOrderNum();
		$order->setAbandonStatus();
		
		// Reset gift certificate
		if ($settings['enable_gift_cert'] == "Yes")
		{
			$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));
			$giftCertificateRepository->updateOrderGiftCertAmountApplied($order->getId(), 0);
			$giftCertificateRepository->updateOrderGiftCertificate($order->getId(), '', '', '');
			$order->setGiftCertificateAmount(0);
		}
		
		// Country / state data
		$regions = new Regions($db);
		view()->assign("countries_states", json_encode($regions->getCountriesStatesForSite()));
		
		// Billing data
		$billingData = $user->getBillingInfo();
		
		if (($user->express || $user->data["facebook_user"]) && $billingData["country"] == 0)
		{
			$billingIsEmpty = true;
			$billingData["country"] = "-1";
			$billingData["state"] = "-1";
		}
		else
		{
			$billingIsEmpty = false;
		}

		/**
		 * Handle payment profiles
		 */
		$paymentProfiles = false;
		$paymentProfileId = 'billing';
		$paymentProfileMethodId = 0;

		if ($user->isAuthenticated())
		{
			/** @var PaymentProfiles_DataAccess_PaymentProfileRepository $paymentProfileRepository */
			$paymentProfileRepository = $registry->get('repository_payment_profile');
			$paymentProfiles = $paymentProfileRepository->getByUserId($user->getId());

			if ($paymentProfiles && count($paymentProfiles) > 0)
			{
				// check is payment profile already selected
				if ($session->has('opc-payment-profile-id'))
				{
					//$paymentProfileId = $session->get('opc-payment-profile-id');
				}

				if ($paymentProfileId == 'billing' || !isset($paymentProfiles[$paymentProfileId]))
				{
					/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
					foreach ($paymentProfiles as $paymentProfile)
					{
						if ($paymentProfileId == 'billing')
						{
							$paymentProfileId = $paymentProfile->getId();
						}
						else if ($paymentProfile->getIsPrimary())
						{
							$paymentProfileId = $paymentProfile->getId();
							break;
						}
					}
				}

				if ($paymentProfileId != 'billing')
				{
					/** @var PaymentProfiles_Model_PaymentProfile $paymentProfile */
					$paymentProfile = $paymentProfiles[$paymentProfileId];
					$paymentProfileMethodId = $paymentProfile->getPaymentMethodId();

					$user->updateBillingInfoFromPaymentProfile($paymentProfile);
					$billingData = $user->getBillingInfo();
				}
			}
		}

		$shippingAddress = $user->getShippingAddresses();
		$shipID = $shippingAddress[0]['usid'];

		$custom_field = isset($_REQUEST["custom_field"]) ? $_REQUEST["custom_field"] : array();
		view()->assign("cf_signup", $cf_signup = $customFields->getCustomFieldsValues("signup", $user->id, $order->oid, 0));
		view()->assign("cf_account", $cf_account = $customFields->getCustomFieldsValues("account", $user->id, $order->oid, 0));
		view()->assign("cf_invoice", $cf_invoice = $customFields->getCustomFieldsValues("invoice", $user->id, $order->oid, 0));
		view()->assign("cf_billing", $cf_billing = $customFields->getCustomFieldsValues("billing", $user->id, $order->oid, 0));
		view()->assign("cf_shipping", $cf_shipping = $customFields->getCustomFieldsValues("shipping", $user->id, 0, $shipID));
		
		if ($cf_signup) view()->assign("cf_signup_c1", ceil(count($cf_signup) / 2));
		if ($cf_account) view()->assign("cf_account_c1", ceil(count($cf_account) / 2));
		if ($cf_billing) view()->assign("cf_billing_c1", ceil(count($cf_billing) / 2));
		if ($cf_shipping) view()->assign("cf_shipping_c1", ceil(count($cf_shipping) / 2));
		if ($cf_invoice) view()->assign("cf_invoice_c1", ceil(count($cf_invoice) / 2));
		
		$additionalIsEmpty = false;
		
		if ($cf_invoice)
		{
			foreach ($cf_invoice as $cf)
			{
				if ($cf["is_active"] && $cf["is_required"] && $cf["value"] == "") $additionalIsEmpty = true;
			}
		}
		
		// Shipping Data
		if (isset($_SESSION["opc-shipping-address-id"]))
		{
			$shippingAddressId = $_SESSION["opc-shipping-address-id"];
		}
		else
		{
			$shippingAddressId = ($settings['ShippingAllowSeparateAddress'] == '1' || $settings['OrderShippingAsBillingDefaultOption'] == 'Yes') ? 'billing' : 'new';
			if ($user->auth_ok)
			{
				$primaryAddress = $user->getShippingAdressPrimary();

				if ($primaryAddress && isset($primaryAddress["usid"]))
				{
					$shippingAddressId = $primaryAddress["usid"];

					if ($settings['EndiciaUspsAddressValidation'] == 1)
					{
						// check is address correct, and if it is not, just exit
						if (!AddressValidator::validate($db, $primaryAddress, $msg))
						{
							$primaryAddress = false;
						}
						else
						{
							$addressVariants = AddressValidator::getAddressVariants();
							if (!is_null($addressVariants) && $addressVariants)
							{
								$primaryAddress = false;
							}
						}
					}

					if ($primaryAddress)
					{
						OrderProvider::updateShippingAddress($order, $primaryAddress);
					}
					else
					{
						$shippingAddressId = 'new';
					}
				}
			}
		}

		$shipmentsMethods = array();

		if ($shippingAddressId == "billing")
		{
			$billingData = $user->getBillingInfo();
			if (intval($billingData['country_id']) > 0)
			{
				OrderProvider::updateShippingAddress($order, $billingData, false, $shipmentsMethods);
			}
		}
		
		$shippingAddressBook = $user->getShippingAddresses();
		$shippingAddress = $order->getShippingAddress();
		
		if ($billingIsEmpty || $shippingAddress["country_id"] == 0)
		{
			$shippingIsEmpty = true;
			$shippingAddress["country_id"] = "-1";
			$shippingAddress["state_id"] = "-1";
		}
		else
		{
			$shippingIsEmpty = false;
		}
		
		// TODO: Moneybookers to work in OPC
		// Temporarily keeping moneybookers from being used in OPC
		$moneybookers_count = 0;	
		/**
		foreach ($payment->methods as $method)
		{
			if (in_array($method->id, array('moneybookers', 'moneybookersacc', 'moneybookersewallet'))) $moneybookers_count++;
		}
		**/
			
		// add free payment method (when possible order is free)
		// case 1: we are 100% sure that order is free - subtotal = 0, no fees, no taxes, no shipping costs - when all items have free shipping OR are digital 
		// case 2: we don't know 100% that order is free - subtota = 1, no fess, no taxes, but shipping costs may be added depending on method selected

		/** @var Payment_DataAccess_PaymentMethodRepository $paymentMethodRepository */
		$paymentMethodRepository = new Payment_DataAccess_PaymentMethodRepository($db, new DataAccess_SettingsRepository($db, $settings));
		$paymentMethodsCount = $paymentMethodRepository->getPaymentMethodsCount(true);
		$paymentMethodId = isset($_SESSION["opc-payment-method-id"]) ? $_SESSION["opc-payment-method-id"] : "0";

		$paymentMethodsJS = '';
		$paymentMethods = $paymentMethodRepository->getPaymentMethods(true, null);
		if ($paymentMethods){
			foreach ($paymentMethods as $paymentMethod)
			{
				/**
				 * @var Payment_Method $paymentMethod
				 */

				$processor = $paymentMethod->getPaymentProcessor();

				if ($processor !== null)
				{
					if (trim($paymentMethodsJS) != '') $paymentMethodsJS .= "\n";
					$paymentMethodsJS .= $processor->getCheckoutPageJS();
				}
			}
		}

		/**
		 * Check do we have enough data to calculate everything
		 */
		$shipping_error_message = '';
		$order->recalcTotals($shipping_error_message);

		$order->getOrderData();
		$order->getOrderItems();

		//tax-related for shipping
		$shippingTaxable = $settings["ShippingTaxable"];
		$shippingTaxRate = 0;
		$shippingTaxDescription = "";
		
		if ($shippingTaxable)
		{
			$shippingTaxClassId = $settings["ShippingTaxClassId"];
		
			$taxProvider = Tax_ProviderFactory::getTaxProvider();
			if ($taxProvider->hasTaxRate($shippingTaxClassId))
			{
				$shippingTaxRate = $taxProvider->getTaxRate($shippingTaxClassId, false);
				$shippingTaxDescription = $taxProvider->getTaxRateDescription($shippingTaxClassId);
			}
		}

		$orderViewModel = new View_OrderLinesViewModel($settings, $msg);
		$orderViewModel->build($order);
		$orderView = $orderViewModel->asArray();

		$giftCertificateViewModel = new View_GiftCertificateViewModel($settings, $msg);
		$giftCertificateViewModel->build($order);
		$giftCertificateView = $giftCertificateViewModel->asArray();

		$bongoActive = false;
		$bongoUsed = false;
		if (isset($_SESSION['bongo_data']))
		{
			$bongoActive = $_SESSION['bongo_data']['bongoActive'];
			$bongoUsed = $_SESSION['bongo_data']['bongoUsed'];
		}

		// Assign variables
		$opcData = array(
			"orderData" => array(
				"totalAmount" => $order->getTotalAmount(),
				"giftCertificateAmount" => $order->getGiftCertificateAmount(),
				"totalRemainingAmount" => $order->getRemainingAmountToBePaid(),
				'hasRecurringBillingItems' => $order->hasRecurringBillingItems(),
			),
			'bongoActive' => $bongoActive,
			'bongoUsed' => $bongoUsed,
			"orderView" => $orderView,
			"giftCertificateView" => $giftCertificateView,
			"orderNumber" => intval($order->order_num),
			"expressCheckout" => intval($user->express),
			"billingIsEmpty" => $billingIsEmpty, // || $additionalIsEmpty,  should not be here as additional moved under billing form
			"billingData" => $billingData,
			"paymentProfiles" => $paymentProfiles ? (array)PaymentProfiles_View_PaymentProfileView::getPaymentProfilesView($paymentProfiles) : false,
			"paymentProfileId" => $paymentProfileId,
			"paymentProfileMethodId" => $paymentProfileMethodId,
			"paymentMethodWay" => "card",
			"shippingIsEmpty" => intval($shippingIsEmpty),
			"shippingShowAlternativeOnFree" => strtolower($settings["ShippingShowAlternativeOnFree"]) == "yes" ? true : false,
			"shippingRequired" => $order->getShippingRequired(),
			"shippingAddressId" => $shippingAddressId,
			"shipmentsMethods" => $shipmentsMethods,
			"shippingAddress" => $shippingAddress,
			"shippingAddressBook" => $shippingAddressBook,
			"shippingWithoutMethod" => strtolower($settings["ShippingWithoutMethod"]) == "yes",
			"shippingEnabled" => strtolower($settings["ShippingCalcEnabled"]) == "yes",
			"shippingAsBillingDisabled" =>
				($cf_shipping && count($cf_shipping))
				|| ($settings['FormsShippingCompany'] == 'Required' && $settings['FormsBillingCompany'] != 'Required')
				|| ($settings['FormsShippingAddressLine2'] == 'Required' && $settings['FormsBillingAddressLine2'] != 'Required'),
			"paymentMethodsCount" => $paymentMethodsCount,
			"paymentMethodId" => $paymentMethodId,
			"paymentFormValidatorJS" => '',
			"paymentFormJS" => '',
			"paymentError" => isset($paymentError) && $paymentError && isset($paymentErrorMessage) ? $paymentErrorMessage : false,
			"promoAvailable" => strtolower($settings["DiscountsPromo"]) == "yes",
			"promoCode" => isset($_SESSION["order_promo_code"]) ? $_SESSION["order_promo_code"] : "",
			"giftCertificateEnabled" => strtolower($settings['enable_gift_cert']) == "yes",
			"giftCertificate" => isset($_SESSION["order_gift_certificate"]) ? $_SESSION["order_gift_certificate"] : false,
			"giftMessageLength" => $settings["GiftCardMessageLength"]
		);
		view()->assign('paymentMethodsJS', $paymentMethodsJS);
		$marketing_setting = (isset($settings["ReceivesMarketingDefault"])?$settings["ReceivesMarketingDefault"]:'Yes');
		view()->assign('ReceivesMarketingDefault', $marketing_setting);
		view()->assign("opcData", json_encode($opcData));
		view()->assign("body", "templates/pages/checkout/opc.html");
	}
	else
	{
		header("Location: ".$url_dynamic."p=cart");
	}
<?php
/**
 * Handle user signup
 */

	if (!defined("ENV")) exit();
	
	//no reasons to show register page when there are no accounts
	if ($settings["AllowCreateAccount"] == "No")
	{
		header("Location: ".$url_http."p=cart");
		die();
	}
	
	//todo: temporary fix. check the email if it exist in the mailchimp lists members
	if (isset($_POST['form']) && isset($_POST['form']['email']) && $settings['mailchimp_enabled'] == '1')
	{
		$is_exist = false;
		require_once(dirname(dirname(__FILE__))."/vendors/mailchimp/src/Mailchimp.php");
		$mailchimp = new Mailchimp($settings['mailchimp_apikey']);
		$chimplists = $mailchimp->lists->getList();
		if (isset($chimplists['data']) && !empty($chimplists['data']))
		{
			foreach ($chimplists['data'] as $lists)
			{
				$members = $mailchimp->lists->members($lists['id']);
				if (isset($members['data']) && !empty($members['data']))
				{
					foreach ($members['data'] as $member)
					{
						if ($member['email'] == $_POST['form']['email'])
						{
							$is_exist = true;
							break;
						}
					}
				}
			}
		}
		if (!$is_exist)
		{
			$user->errors[] = "Invalid Email entered";
		}
	}
	
	// remove empty array elements
    $user->errors = array_filter($user->errors);
	
	//assign actions IDs
	view()->assign("USER_SIGNUP", USER_SIGNUP);
	
	//country / state data
	$regions = new Regions($db);
	view()->assign("countries_states", json_encode($regions->getCountriesStatesForSite()));

	//check shipping
	$sc = Shipping::getShippingCountries($db);
	if (is_array($sc))
	{
		view()->assign("ship2countries", in_array(0, $sc) ? "all" : $sc);
	}
	else
	{
		view()->assign("ship2countries", "all");
	}
	
	//check is there registration error on wrong attempt
	view()->assign("is_user_errors", ($ua=USER_SIGNUP) && ($user->is_error)?"yes":"no");
	
	//check captcha
	
	if (($ua=USER_SIGNUP) && ($user->is_error))
	{
		view()->assign("userErrors", $user->errors);
	}
	
	//check / create form data array
	$marketing_setting = (isset($settings["ReceivesMarketingDefault"])?$settings["ReceivesMarketingDefault"]:'Yes');
	$form = isset($_POST["form"]) ? $_POST["form"] : array("receives_marketing"=>$marketing_setting);
	$form = is_array($form) ? $form : array("receives_marketing"=>$marketing_setting);
	view()->assign("form", $form);
	
	//assign custom fields
	$customFieldsData = isset($_POST["custom_field"]) ? $_POST["custom_field"] : array();
	view()->assign("cf_billing", $cf_billing = $customFields->getCustomFields("billing", $customFieldsData));
	view()->assign("cf_signup", $cf_signup = $customFields->getCustomFields("signup", $customFieldsData));
	view()->assign("cf_account", $cf_account = $customFields->getCustomFields("account", $customFieldsData));
	
	//add captcha
	if ($settings['captchaMethod'] != 'None')
	{
		view()->assign('captcha', true);
		view()->assign('site_key', $settings['captchaReCaptchaPublicKey']);
	}

	//finally, join template
	view()->assign("body", "templates/pages/account/signup.html");

	$meta_title = trim($settings['register_page_title']) != '' ? $settings['register_page_title'] : $meta_title;
	$meta_description = trim($settings['register_meta_description']) != '' ? $settings['register_meta_description'] : $meta_description;
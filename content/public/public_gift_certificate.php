<?php
/**
 * Gift certificate
 */

	if (!defined('ENV')) exit();

	if ($settings['enable_gift_cert'] !== 'Yes')
	{
		header('Location: '.$url_dynamic);
		exit();
	}

	$meta_title = trim($settings['giftcert_page_title']) != '' ? $settings['giftcert_page_title'] : $meta_title;
	$meta_description = trim($settings['giftcert_meta_description']) != '' ? $settings['giftcert_meta_description'] : $meta_description;

	$giftCertificateRepository = new DataAccess_GiftCertificateRepository($db, new DataAccess_SettingsRepository($db, $settings));

	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

	$giftCert = $giftCertificateRepository->getGiftCertificateByOrderId($order->getId());

	if ($giftCert)
	{
		view()->assign('update', '1');
		view()->assign('first_name', $giftCert['first_name']);
		view()->assign('last_name', $giftCert['last_name']);
		view()->assign('phone', $giftCert['phone']);
		view()->assign('rep_email', $giftCert['email']);
		view()->assign('from_name', $giftCert['from_name']);
		view()->assign('message', $giftCert['message']);
		view()->assign('gift_amount', $giftCert['gift_amount']);
	}
	else
	{
		view()->assign('update', '0');
	}

	if ($action == 'check_balance')
	{
		view()->assign('check_balance', 'yes');
		if (isset($_REQUEST['get_balance']))
		{
			$gift_data = $giftCertificateRepository->getGiftCertificate($_REQUEST['gift_cert_first_name'], $_REQUEST['gift_cert_last_name'], $_REQUEST['gift_cert_voucher']);
			if ($gift_data)
			{
				view()->assign('your_balance', $gift_data['balance']);
			}
			else
			{
				view()->assign('giftcert_error', 'Voucher number doesn\'t exist in our records.');
			}
		}
	}

	view()->assign('body', 'templates/pages/gift-certificate/gift-certificate.html');

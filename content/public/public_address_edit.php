<?php
/**
 * Update users address
 */

if (!defined("ENV")) exit();

if ($user->auth_ok)
{

	$address_id = isset($_REQUEST["address_id"]) ? intval($_REQUEST["address_id"]) : 0;

	$shipping_address = $user->getShippingAddressById($address_id);
	if ($shipping_address)
	{
		if ($ua == USER_UPDATE_ADDRESS)
		{
			if ($user->is_error)
			{
				view()->assign("user_errors", $user->errors);
				view()->assign("edit_address", $_POST["form"]);
				view()->assign("cf_shipping", $customFields->getCustomFields("shipping", isset($_REQUEST["custom_field"]) ? $_REQUEST["custom_field"] : array()));
			}
		}
		else
		{
			view()->assign("edit_address", $shipping_address);
			view()->assign("cf_shipping", $customFields->getCustomFieldsValues("shipping", $user->id, 0, $address_id));
		}
		
		//address id
		view()->assign("address_id", $address_id);
	
		//country / state data
		$regions = new Regions($db);
		view()->assign("countries_states", json_encode($regions->getCountriesStatesForSite()));
		view()->assign('nonce', Nonce::create('user_update_address'));
		view()->assign("body", "templates/pages/account/address-edit.html");
	}
	else
	{
		header("Location: ".$settings["GlobalHttpUrl"]."/index.php?p=404");
		_session_write_close();
		die();
	}
}
else
{
	view()->assign("body", "templates/pages/account/auth-error.html");
}
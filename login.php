<?php

	define("ENV", true);

	/**
	 * Load libraries
	 */
	require_once 'content/classes/_boot.php';

	$registry = new Framework_Registry();
	$registry->set('db', $db);
	$registry->setParameter('settings', $settings);

	$label_app_name = (!isset($label_app_name) || trim($label_app_name) == '') ? 'Pinnacle Cart' : $label_app_name;

	$ch = curl_init('https://www.cartmanual.com/label/logos/'.md5($label_app_name).'.gif');
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_exec($ch);
	$response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	if ($response != 200) $label_app_name = 'Pinnacle Cart';

	define('LABEL_APP_IMAGE', 'https://www.cartmanual.com/label/logos/'.md5($label_app_name).'.gif');

	define('IS_LABEL', (md5(str_replace(' ', '', strtolower($label_app_name))) != '944c32e5f7d6ab022c1101c963ac1370'));

	if (!file_exists('content/cache/languages/admin_english.php') || filemtime('content/admin/languages/english.php') > filemtime('content/cache/languages/admin_english.php'))
	{
		$admin_msg = include 'content/admin/languages/english.php';

		Framework_Translator::cache($admin_msg, 'content/cache/languages/admin_english.php');
	}

	$admin_msg = include 'content/cache/languages/admin_english.php';

	$translator = Framework_Translator::getInstance($admin_msg);

	Admin_SessionHandler::loginInit($settings);

	/**
	 * Check license with IONO
	 */
	$iono = new IonoKeys($db, $settings);

	/**
	 * Check for proxy
	 */
	if (($settings["ProxyAvailable"] == "YES") != false)
	{
		$iono->proxy_used = true;
		$iono->proxy_address = $settings["ProxyAddress"];
		$iono->proxy_port = $settings["ProxyPort"];
		if ($settings["ProxyRequiresAuthorization"] == "YES")
		{
			$iono->proxy_requires_auth = true;
			$iono->proxy_username = $settings["ProxyUsername"];
			$iono->proxy_password = $settings["ProxyPassword"];
		}
	}

	$request = Framework_Request::createFromGlobals();
	$controller = new Admin_Controller_Login($request, $db);
	$action = $request->get('action');

	switch ($action)
	{
		case 'clear_license':
		{
			$controller->clearLicenseAction();
			break;
		}
		default:
		{
			$controller->indexAction();
			break;
		}
	}

	_session_write_close();

	die();

<?php
	/**
	 * Do not cache output
	 */
	header("Cache-Control: private");
	header("Content-Type: text/html; charset=UTF-8");

	define("ENV", true);

	// TODO: move out of here
	if (file_exists(dirname(__FILE__).'/app_maintenance.htm'))
	{
		echo file_get_contents(dirname(__FILE__).'/app_maintenance.htm');
		exit();
	}

	require_once 'content/classes/_boot.php';


	//$settings['DriftMarketingFlag']

	// redirect to SSL?
	if ($settings['ForceSslRedirection'] == 'Yes' && shouldBeSecure())
	{
		header('Location: ' . $settings['GlobalHttpsUrl'] . getRequestUri(), true, 301);
		exit;
	}

	$registry = new Framework_Registry();
	$registry->set('db', $db);
	$registry->setParameter('settings', $settings);

if($settings['DriftMarketingFlag']){
	echo 'int';
}

if($settings['DriftMarketingFlag'] == '1'){
	echo 'char';
}


$db->reset();
$db->assignStr('value', (1 > 0));
//$db->update(DB_PREFIX.'settings', 'WHERE name = "DriftMarketingFlag"');

	// TODO: move into composer
	require_once("content/classes/Events/Register.php");

	/**
	 * Mailchimp stuff
	 */
	if ($settings["mailchimp_e360"] == "1")
	{
		$thirty_days = time()+60*60*24*30;
		if (isset($_REQUEST['mc_cid']))
		{
			setcookie(
				'mailchimp_campaign_id', trim($_REQUEST['mc_cid']), $thirty_days,
				"/", isSslMode() ? $settings["GlobalHttpsCookieDomain"] : $settings["GlobalHttpCookieDomain"]
			);
		}
		if (isset($_REQUEST['mc_eid']))
		{
			setcookie(
				'mailchimp_email_id', trim($_REQUEST['mc_eid']), $thirty_days,
				"/", isSslMode() ? $settings["GlobalHttpsCookieDomain"] : $settings["GlobalHttpCookieDomain"]
			);
		}
	}

	if (isset($_GET["url"])) require_once("content/engine/engine_url_parser.php");

	/**
	 * Start session
	 */
	$userCookie = false;

	if (isset($_COOKIE["_pcud"]))
	{
		$userCookie = unserialize(base64_decode($_COOKIE["_pcud"]));
		if (!$userCookie || !is_array($userCookie)) $userCookie = false;
		if ($userCookie)
		{
			$db->query("SELECT * FROM ".DB_PREFIX."users WHERE uid='".$db->escape($userCookie["i"])."' AND removed='No' AND level='".$db->escape($userCookie["l"])."' AND MD5(session_id)='".(isset($userCookie["s"]) ? $db->escape($userCookie["s"]) : '')."'");
			if (!$db->moveNext()) $userCookie = false;
		}
	}

	setSessionSavePath();

	/**
	 * Handle session in HTTPS mode
	 */
	$isDesignMode = false;
	if ((isset($_COOKIE["_admc"]) && $_COOKIE["_admc"] == "true") || isset($_GET["_admc"]) || isset($_POST["_admc"]))
	{
		$isDesignMode = true;
		$_COOKIE["_admc"] = "true";
		setcookie(
			"_admc", "true", time() + 60*60, "/",
			isSslMode() ? $settings["GlobalHttpsCookieDomain"] : $settings["GlobalHttpCookieDomain"]
		);
	}
	else
	{
		$isDesignMode = false;
	}

	/**
	 * Handle session
	 */
	$pcsid = isset($_REQUEST["pcsid"]) ? $_REQUEST["pcsid"] : false;

	if ($pcsid && preg_match("/[0-9a-zA-Z]{1,}/", $pcsid))
	{
		session_id($pcsid);
	}

	if (isset($_GET["_pcod"]))
	{
		setcookie(
			"_pcod", $_GET["_pcod"], time() + 60*60*24*30*6, "/",
			isSslMode() ? $settings["GlobalHttpsCookieDomain"] : $settings["GlobalHttpCookieDomain"]
		);
		$_COOKIE["_pcod"] = $_GET["_pcod"];
	}

	if ($isDesignMode)
	{
		if (shouldBeSecure())
		{
			header('Location: '.$settings['AdminHttpsUrl'].getRequestUri());
			exit();
		}

		Admin_SessionHandler::designModeInit($settings);

		if (!(isset($_SESSION['admin_auth_id']) && isset($_SESSION['admin_session_id'])))
		{
			$isDesignMode = false;
			if (array_key_exists('_admc', $_COOKIE))
			{
				setcookie('_admc', 'false', time() - 31536000, '/', isSslMode() ? $settings["GlobalHttpsCookieDomain"] : $settings["GlobalHttpCookieDomain"]);
			}
		}

//		define('DESIGN_MODE', $isDesignMode ? true : false);

		$settings['GlobalHttpsUrl'] = $settings['AdminHttpsUrl'];
		$settings['GlobalHttpUrl'] = $settings['AdminHttpsUrl'];
	}
	else
	{
		ini_set('session.gc_maxlifetime', $settings["SecurityUserTimeout"] + 500); // set max lifetime

		if (isSslMode())
		{
			$host = $settings['GlobalHttpsCookieDomain'] != '' ? $settings['GlobalHttpsCookieDomain'] : parse_url($settings["GlobalHttpsUrl"], PHP_URL_HOST);
		}
		else
		{
			$host = $settings['GlobalHttpCookieDomain'] != '' ? $settings['GlobalHttpCookieDomain'] : parse_url($settings["GlobalHttpUrl"], PHP_URL_HOST);
		}

		if ($host == 'localhost')
		{
			$host = '';
		}

		if (floatval(phpversion()) >= 5.2)
		{
			session_set_cookie_params(0, '/', $host, false, true);
		}
		else
		{
			session_set_cookie_params(0, '/', $host);
		}

		session_cache_expire(round($settings["SecurityUserTimeout"]/60));
		session_name("ShoppingCartSession");
		session_start();

		$session = new Framework_Session(new Framework_Session_WrapperStorage());
		$session->start();
		$registry->set('session', $session);
	}

	/**
	 * Currency
	 */
	$currencies = new Currencies($db);

	if (isset($_POST["set_current_currency"]))
	{
		$currencies->setCurrentCurrency(intval($_POST["set_current_currency"]));
	}
	else
	{
		$currencies->getCurrentCurrency();
	}

	if (!isset($_SESSION["default_currency"]))
	{
		$_SESSION["default_currency"] = $currencies->getDefaultCurrency();
	}

	/**
	 * Current page
	 */
	$p = isset($_REQUEST["p"]) ? trim($_REQUEST["p"]) : (isset($_REQUEST['P']) ? trim($_REQUEST['P']) : "home");
	$p = preg_match("/^([a-z0-9_-])+$/", $p) ? $p : "home";

	/**
	 * Init smarty
	 */
	view()->debugging = false;
	view()->use_sub_dirs = false;
	view()->assign("isDevMode", defined("DEVMODE") && DEVMODE);

	// FIXME: Assemble through backend only
	if (
		!is_dir('content/cache/skins/'.escapeFileName($settings['DesignSkinName'])) ||
		(isset($_SESSION['admin_auth_id']) && $_SESSION['admin_auth_id'] && isset($_SESSION['admin_design_mode']) && $_SESSION['admin_design_mode'])
		|| (defined("DEVMODE") && DEVMODE)
	)
	{
		view()->assemble(
			'content'.DS.'cache'.DS.'skins'.DS.escapeFileName($settings['DesignSkinName']).DS,
			null, true, isset($_REQUEST['_admc']) && $_REQUEST['_admc'] == 'true'
		);
	}

	/**
	 * Languages
	 */
	$languages = new Language($db);

	view()->assign("languages", $active_languages = $languages->getActiveLanguages());
	view()->assign("languages_count", count($active_languages));

	// check / set language for a first time
	if (!isset($_SESSION["current_language_id"]))
	{
		$def_lang = $languages->getDefaultLanguage();
		$_SESSION["current_language_id"] = $def_lang["language_id"];
	}
	//change current active language
	if (isset($_POST["set_language"]))
	{
		if (array_key_exists(intval($_POST["set_language"]), $active_languages))
		{
			$_SESSION["current_language_id"] = intval($_POST["set_language"]);
		}
	}

	//check / set current active language
	$current_language = $languages->getActiveLanguageById($_SESSION["current_language_id"]);
	view()->assign("current_language", $current_language);
	view()->assign("msg", $msg = $languages->getDictionary($current_language["code"]));
	view()->prependJavascript($languages->getJavascriptCache($current_language["code"]));

	// pass settings to smarty
	foreach ($settings as $key=>$value) view()->assign($key, $value);

	if (isset($settings['GoogleAnalytics']))
	{
		$trackingCode = $settings['GoogleAnalytics'];
		if (strstr($trackingCode, '_getTracker(') !== false)
		{
			view()->assign('GoogleAnalyticsAsync', 'No');
		}
		else
		{
			view()->assign('GoogleAnalyticsAsync', 'Yes');
		}
	}

	// by default user is unknown
	view()->assign("user_cookie", false);//$userCookie);
	view()->assign("auth_ok", "no");
	view()->assign("current_page", $p);

	view()->assign("url_admin", $settings["GlobalHttpsUrl"]);

	// Added url request uri
	view()->assign("url_request_uri", htmlentities($_SERVER['REQUEST_URI']));

	// assign most used user and cart actions
	view()->assign("USER_EXPRESS_CHECKOUT", USER_EXPRESS_CHECKOUT);
	view()->assign("USER_START_CHECKOUT", USER_START_CHECKOUT);
	view()->assign("USER_LOGIN", USER_LOGIN);
	view()->assign("USER_LOGOUT", USER_LOGOUT);
	view()->assign("USER_PASSWORD_RESET", USER_PASSWORD_RESET);
	view()->assign("USER_UPDATE_ADDRESS", USER_UPDATE_ADDRESS);
	view()->assign("USER_DELETE_ADDRESS", USER_DELETE_ADDRESS);
	view()->assign("USER_ADD_TO_WISHLIST", USER_ADD_TO_WISHLIST);
	view()->assign("USER_UPDATE_PROFILE", USER_UPDATE_PROFILE);
	view()->assign("USER_ADD_ADDRESS", USER_ADD_ADDRESS);
	view()->assign("USER_ADD_PAYMENT_PROFILE", USER_ADD_PAYMENT_PROFILE);
	view()->assign("USER_UPDATE_PAYMENT_PROFILE", USER_UPDATE_PAYMENT_PROFILE);
	view()->assign("USER_DELETE_PAYMENT_PROFILE", USER_DELETE_PAYMENT_PROFILE);
	view()->assign("ORDER_ADD_ITEM", ORDER_ADD_ITEM);
	view()->assign("ORDER_REMOVE_ITEM", ORDER_REMOVE_ITEM);
	view()->assign("ORDER_CLEAR_ITEMS", ORDER_CLEAR_ITEMS);
	view()->assign("ORDER_UPDATE_ITEMS", ORDER_UPDATE_ITEMS);
	view()->assign("ORDER_COMPLETE_ORDER", ORDER_COMPLETE_ORDER);
	view()->assign("ORDER_WISHLIST_TO_CART", ORDER_WISHLIST_TO_CART);
	view()->assign("ORDER_COMPLETE_ORDER", ORDER_COMPLETE_ORDER);
	view()->assign("ORDER_PROCESS_PAYMENT", ORDER_PROCESS_PAYMENT);
	view()->assign("ORDER_PROCESS_PAYMENT_VALIDATION", ORDER_PROCESS_PAYMENT_VALIDATION);
	view()->assign("ORDER_ADD_GIFT_CERT", ORDER_ADD_GIFT_CERT);
	view()->assign("USER_PREDICTIVE_SEARCH", USER_PREDICTIVE_SEARCH);

	view()->assign('isDesignMode', $isDesignMode);

	/**
	 * Define page zones
	 */
	$pages_public = array(
		'home', 'page', 'catalog', 'product', 'cart', 'site_map',
		'login', 'signup', 'subscribe', 'unsubscribe', 'password_reset', 'blocked', 'completed',
		'email2friend', 'shipping_quote', 'international', 'wishlist', 'manage_wishlist', 'gift_certificate',
		'testimonials', 'smartsuggest', '404', 'paypal_callback', 'pikfly',
	);
	$pages_public_only = array('!login', '!signup', '!password_reset', 'blocked');

	$pages_protected = array(
		'account', 'profile', 'orders', 'order', 'address_book', 'address_edit',
		'one_page_checkout', 'payment_validation', 'payment_profiles', 'payment_profile_edit',
		'recurring_profiles', 'recurring_profile','order_form',
	);

	$pages_protected_express = array('one_page_checkout', 'payment_validation', 'order_form');

	$pages_for_https = array(
		'account', 'profile', 'orders', 'order',
		'payment_validation', 'completed', 'address_book', 'address_edit', 'one_page_checkout','order_form',
		'cart',	'login', 'signup', 'password_reset', 'paypal_callback',
		'recurring_profiles', 'recurring_profile',
	);

	/**
	 * Prepare dynamic URLs engine
	 */
	if (isSslMode())
	{
		if ($isDesignMode)
		{
			$url_dynamic = $url_http = $url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?_admc=true&";
		}
		else
		{
			if ($settings['ForceSslRedirection'] == 'Yes')
			{
				$url_http = $url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?";
				$url_dynamic = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?";
			}
			else
			{
				$url_http = $settings["GlobalHttpUrl"]."/".$settings["INDEX"]."?";
				$url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?";
				$url_dynamic = (isset($_SESSION["UserID"]) && intval($_SESSION["UserID"]) > 0 ? $settings["GlobalHttpsUrl"] : $settings["GlobalHttpUrl"])."/".$settings["INDEX"]."?";
			}
		}
		$url_base = $settings["GlobalHttpsUrl"]."/";
	}
	else
	{
		if ($isDesignMode)
		{
			$url_http = $url_dynamic = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?";

			if (parse_url($settings["GlobalHttpUrl"], PHP_URL_HOST) != parse_url($settings["GlobalHttpsUrl"], PHP_URL_HOST))
			{
				$_pcod = isset($_COOKIE["_pcod"]) ? ("&_pcod=".$_COOKIE["_pcod"]) : "";
				$url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id().$_pcod."&_admc=true&";
			}
			else
			{
				$url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?_admc=true&";
			}
			$url_base = $settings["GlobalHttpsUrl"]."/";
		}
		else
		{
			if ($settings['ForceSslRedirection'] == 'Yes')
			{
				$url_http = $url_dynamic = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?";

				// transfer session from http to https when domains are different for http and https
				if (parse_url($settings["GlobalHttpUrl"], PHP_URL_HOST) != parse_url($settings["GlobalHttpsUrl"], PHP_URL_HOST))
				{
					$_pcod = isset($_COOKIE["_pcod"]) ? ("&_pcod=".$_COOKIE["_pcod"]) : "";
					$url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id().$_pcod."&";
				}
				else
				{
					$url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?";
				}
				$url_base = $settings["GlobalHttpsUrl"]."/";
			}
			else
			{
				$url_http = $url_dynamic = $settings["GlobalHttpUrl"]."/".$settings["INDEX"]."?";

				// transfer session from http to https when domains are different for http and https
				if (parse_url($settings["GlobalHttpUrl"], PHP_URL_HOST) != parse_url($settings["GlobalHttpsUrl"], PHP_URL_HOST))
				{
					$_pcod = isset($_COOKIE["_pcod"]) ? ("&_pcod=".$_COOKIE["_pcod"]) : "";
					$url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?pcsid=".session_id().$_pcod."&";
				}
				else
				{
					$url_https = $settings["GlobalHttpsUrl"]."/".$settings["INDEX"]."?";
				}
				$url_base = $settings["GlobalHttpUrl"]."/";
			}
		}
	}

	$url_rel = $settings["INDEX"]."?";
	$url_checkout = $url_https."ua=".USER_START_CHECKOUT."&checkout_start_page=".$p;

	view()->assign("url_http", $url_http);
	view()->assign("url_https", $url_https);
	view()->assign("url_dynamic", $url_dynamic);
	view()->assign("url_dinamic", $url_dynamic); // backwards compatibility
	view()->assign("url_base", $url_base);
	view()->assign("url_rel", $url_rel);
	view()->assign("url_checkout", htmlentities($url_checkout));
	view()->assign("url_ajax", $url_ajax = (isSslMode() || $settings['ForceSslRedirection'] == 'Yes' ? $settings["GlobalHttpsUrl"] : $settings["GlobalHttpUrl"]) . '/'.$settings['INDEX'].'?pcsid='.session_id());
	view()->assign("url_ajax_admin", $url_ajax_admin = $settings["AdminHttpsUrl"]);

	// custom fields
	$customFields = new CustomFields($db);

	// load and start shopping cart engine
	require_once("content/engine/engine_logic.php");

	view()->assign('paypalEnabled', Payment_PayPal_PayPalApi::isPayPalEnabled());

	// check redirects and generate global back URL
	$backurl = isset($backurl) ? $backurl : "";

	//complete here in case of ajax or backurl
	if ($backurl && $backurl != "")
	{
		$_SESSION["HTTP_REFERER"] = (isSslMode() ? "https://" :"http://").$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

		$_mysession = $_SESSION;
		foreach ($_mysession as $_mysessionvar=>$_mysessionkey)
		{
			$_SESSION[$_mysessionvar] = $_mysessionkey;
		}

		_session_write_close();

		$db->done();

		header("Location: ".$backurl."\n");
		die();
	}

	// assign user security / order vars
	view()->assign("auth_ok", $user->auth_ok?"yes":"no");
	view()->assign("user_level", $user->auth_ok ? $user->level : ($userCookie ? $userCookie["l"] : 0));
	view()->assign("auth_express", $user->express?"yes":"no");

	view()->assign("order", $order);
	view()->assign("order_items", $order->items);

	foreach ($_GET as $_name => $_value)
	{
		if ($_name != "set_language" && !is_array($_name) && !is_array($_value))
		{
			$backurl .= urlencode($_name)."=".urlencode($_value)."&";
		}
	}
	view()->assign("backurl", $backurl);

	//////////////////////////////
	// HERE STARTS BLACK WOOD :)
	// I started to develop this application 5 years ago.
	// 5 years. It is pretty big kid now.
	// Well. 3 more years. It is 2011 already. Future here.

	//////////////////////////////////////////////////////////
	// DISPLAY PART

	//catalog menu
	if (isset($_REQUEST["parent"]))
	{
		$parent = intval($_REQUEST["parent"]);
	}
	elseif ($settings["USE_MOD_REWRITE"] == "YES" && isset($_REQUEST["catalog_code"]))
	{
		$parts = explode("-", $_REQUEST["catalog_code"]);
		$parent = count($parts) == 3 ? intval($parts[1]) : "0";
	}
	else
	{
		$parent = "0";
	}

	$categoryRepository = new DataAccess_CategoryRepository($db);
	$categoriesTree = $categoryRepository->getCachedTree(array('cid', 'parent', 'is_visible', 'key_name', 'name', 'is_stealth'), array('catalog_hide_empty_categories' => $settings['CatalogHideEmptyCategories']));

	view()->assign('parent', $parent);
	view()->assign('categories', $categoriesTree);

	//manufacturers
	if ($settings["CatalogManufacturers"] == "YES")
	{
		$manufacturers = new Manufacturers($db, $settings);
		$manufacturers_list = $manufacturers->getManufacturers(true);

		if ($manufacturers_list)
		{
			view()->assign("manufacturers", $manufacturers_list);
			$is_manufacturers = true;
		}
		else
		{
			view()->assign("manufacturers", false);
			$is_manufacturers = false;
		}
	}
	else
	{
		view()->assign("manufacturers", false);
		$is_manufacturers = false;
	}

	view()->assign("current_currency", $_SESSION["current_currency"]);
    view()->assign("default_currency", $_SESSION["default_currency"]);

	$currencies_list = $currencies->getCurrencies();

	if ($currencies_list)
	{
		view()->assign("currencies", $currencies_list);
		view()->assign("currencies_count", count($currencies_list));
		$is_currencies = true;
	}
	else
	{
		view()->assign("currencies", array());
		view()->assign("currencies_count", 1);
		$is_currencies = false;
	}

	//bestsellers
	if (
		(isset($settings["CatalogBestsellersAvailable"]) && $settings["CatalogBestsellersAvailable"] == "YES")
		||
		(isset($settings["CatalogBestSellersAvailable"]) && $settings["CatalogBestSellersAvailable"] == "YES")
	)
	{
		$params = $settings;

		if ($parent)
		{
			if (in_array($parent, array_keys($categoriesTree)))
			{
				$params['categories'][] = $parent;

				if (is_array($categoriesTree[$parent]['children']))
				{
					reset($categoriesTree[$parent]['children']);

					foreach ($categoriesTree[$parent]['children'] as $id=>$node)
					{
						$params['categories'][] = $id;
					}
				}
			}
		}
		else
		{
			$params['categories'] = array();
		}

		$bs = new ShoppingCartProducts($db, $settings);

		view()->assign("bestsellers", $bs->getItemsBestSellers($params, $user->auth_ok ? $user->level : ($userCookie ? $userCookie["l"] : 0)));
		view()->assign("bestsellers_view", $settings["CatalogBestsellersView"]);
		$is_bestsellers = $bs->bestsellersCount > 0;
	}
	else
	{
		view()->assign("bestsellers", false);//
		$is_bestsellers = false;
	}

	if ($user->auth_ok)
	{
		view()->assign("user_name", gs($user->data["fname"]." ".$user->data["lname"]));
	}

	//top & bottom links
	$page_id = isset($_REQUEST["page_id"]) ? $_REQUEST["page_id"] : "";

	$textPagesRepository = new DataAccess_TextPageRepository($db);

	view()->assign("pages_links", $textPagesRepository->getPagesLinks());

	//recent items
	$recentItems = new ShoppingCartRecentItems($settings);
	view()->assign("recent_items", $recentItems->getItems());

	view()->assign("designImages", view()->getImages());

	view()->assign("label_app_name", $label_app_name);
	view()->assign("label_home_site", $label_home_site);

	$meta_title = $settings["SearchTitle"];
	$meta_description = $settings["SearchMetaDescription"];

	// check page exists and page protection
	$isPublicPage = in_array($p, $pages_public);
	$isProtectedPage = in_array($p, $pages_protected);

	if ($isPublicPage || $isProtectedPage)
	{
		$body = "content/public/public_" . $p . ".php";

		$userAuthOk = $user->auth_ok;
		$userExpress = $user->express;

		if ($p == 'order_form')
		{
			if (!$user->auth_ok)
			{
				$user->expressLogin();
				$body = "content/public/public_" . $p . ".php";
			}
		}
		else if ($isPublicPage || ($userAuthOk && !$userExpress && $isProtectedPage) || ($userAuthOk && $userExpress && in_array($p, $pages_protected_express)))
		{
			$body = "content/public/public_" . $p . ".php";
		}
		else
		{
			$_SESSION['after_login_url'] = UrlUtils::currentPageURL();

			$_mysession = $_SESSION;
			foreach ($_mysession as $_mysessionvar=>$_mysessionkey) $_SESSION[$_mysessionvar] = $_mysessionkey;
			_session_write_close();

			$db->done();

			header('Location: '.$url_https.'p=login'."\n");

			die();
		}
	}
	else
	{
		$p = "404";
		$body = "content/public/public_404.php";
	}

	if ($user->auth_ok && in_array($p, $pages_public_only))
	{
		header("Location: ".$url_dynamic."p=cart");
	}

	view()->assign("site_map_url", UrlUtils::getCommonUrl(false, 'site_map'));
	view()->assign("testimonial_url", UrlUtils::getCommonUrl(false, 'testimonials'));
	view()->assign("international_url", UrlUtils::getCommonUrl(false, 'international'));

	view()->assign("enable_payment_profiles", $settings['paymentProfilesEnabled'] == 1);

	// load executable module (by $p variable)
	require($body);

	if (isset($_REQUEST['p']) && $_REQUEST['p'] == 'catalog')
	{
		view()->assign("canonical_url", '');
		$tmp = (substr(preg_replace('/-page-[0-9]/', '', UrlUtils::currentPageURL(true)), 0, -1));
		if ($_REQUEST['pg'] == 1 && $products->pages_count > 1)
		{
			view()->assign("rel_next", $tmp . '-page-' . ($_REQUEST['pg'] + 1) . '/');
		}
		else if ($_REQUEST['pg'] > 1 && $_REQUEST['pg'] < $products->pages_count)
		{
			view()->assign("rel_next", $tmp . '-page-' . ($_REQUEST['pg'] + 1) . '/');
			view()->assign("rel_prev", $tmp . '-page-' . ($_REQUEST['pg'] - 1) . '/');
		}
		else if ($_REQUEST['pg'] == $products->pages_count)
		{
			if (($_REQUEST['pg'] - 1) < $_REQUEST['pg'] && ($_REQUEST['pg'] - 1) != 0)
			{
				view()->assign("rel_prev",  $tmp .  '-page-' . ($_REQUEST['pg'] - 1) . '/');
			}
			else
			{
				view()->assign("canonical_url", $tmp);
			}
		}
	}
	else
	{
		view()->assign("canonical_url", htmlentities(UrlUtils::getCanonicalUrl($p)));
	}

	
	// assign $meta_xxx vars
	view()->assign("meta_title", $meta_title);
	view()->assign("meta_description", $meta_description);
	
	// meta pre-assign
	view()->appendMetatag('Content-Type', 'text/html; charset='.$msg['common']['charset'], true, 'http-equiv');
	view()->appendMetatag('description', $meta_description);
	view()->setTitle($meta_title);
	
	// forces IE7 compatibility mode for IE8 browsers
	view()->appendMetatag('X-UA-Compatible', 'IE=EmulateIE7', true, 'http-equiv');

	// sets the metatags for use with designmode
	view()->appendMetatag('ddm-page', htmlentities($p));
	view()->appendMetatag('ddm-lang', $current_language["code"]);
	
	$doRender = ((array_key_exists('norender', $_GET) && $_GET['norender'] == 'true') === false);
	
	require_once("content/engine/engine_design_mode.php");

	if (in_array($p, array("completed", "invoice", "payment_validation", "cart", "billing", "one_page_checkout")))
	{
		view()->force_compile = true;
	}

	/**
	 * Render output
	 */
	if ($doRender)
	{
		$wrapperToRender = "default.html";
		if ($p == 'order_form')
		{
			view()->setLayout('order_form');
		}
		else
		{
			view()->setLayout();
		}

		if ($settings["StoreClosed"] == "Yes" && !$isDesignMode)
		{
			view()->assign("ClosedMessage", $settings["StoreClosedMessage"]);
			$wrapperToRender = "store_closed.html";
		}
		else
		{		
			$is_print_version = isset($_REQUEST["is_print_version"])?$_REQUEST["is_print_version"]:false;
			view()->assign("is_print_version", $is_print_version);
		
			if ($is_print_version == "true")
			{
				view()->assign("site_title", $settings["GlobalSiteName"]);
				$wrapperToRender = "print.html";
			}
		}
		
		view()->render("templates/wrappers/{$wrapperToRender}");
	}
	
	/**
	 * Done
	 */
	$db->done();
	
	if ($p != "email2friend" && $p != "404")
	{
		$_SESSION["HTTP_REFERER"] = (isSslMode() ? "https://" : "http://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
	}

	/**
	 * Save data from HTTPS to HTTP session
	 */
	
	_session_write_close();
	
/**
 * The End. To be continued. See you next time.
 */
